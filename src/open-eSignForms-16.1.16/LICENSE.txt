File: LICENSE.txt

Open eSignForms - Web-based electronic contracting software by Yozons, Inc.

All source code in this project is licensed under the GNU Affero General Public License. See below for details which
is the intended licensing text for all files in this project. Please report any files that specify any other license.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program in the 'agpl.txt' file.  
If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.

** For third-party licenses, please see http://open.esignforms.com/thirdPartySoftware.jsp

Open eSignForms is not suitable for military use, or for any high-security, high-risk, life-or-death purposes.
