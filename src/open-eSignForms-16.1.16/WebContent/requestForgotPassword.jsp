<!DOCTYPE html>
<%@page session="false" contentType="text/html; charset=UTF-8" 
import="com.esignforms.open.config.Literals,
		com.esignforms.open.data.EsfBoolean,
		com.esignforms.open.util.StringReplacement,
        com.esignforms.open.user.User,
        com.esignforms.open.user.UserLoginInfo"
%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(request.getSession(false),request,response);
if ( bean.isGet() )
{
	if ( bean.doGet() == BeanData.PageProcessing.DONE )
		return;
}
else
{
	if ( bean.doPost() == BeanData.PageProcessing.DONE )
		return;
}
%>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - Forgot my password, request it be reset</title>
	<meta name="Description" content="Allows a user to request the password reset process- Version 12/8/2015"/>
	<meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <base href="<%=bean.getExternalUrlContextPath()%>/"/>
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="shortcut icon" />
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="icon" />
	<script type="text/javascript" src="./static/esf/esf.js" ></script>
<script type="text/javascript">
<!--
function init()
{
  <% if ( ! bean.loginPageAllowEmbedded ) { %>
  if (window != window.top) {
	top.location.href = location.href;
  }
  <% } %>
	
  return true;
}
// -->
</script>

<style type="text/css">
<jsp:include page="./static/esf/esf.css" flush="true" />
</style>
</head>

<body onload="init()" class="esf" style="background-color: #f5f5f5;">
<jsp:include page="./header.jsp" flush="true" />
<%=bean.errors.toHtml()%>

<form name="aform" method="post" action="<%=bean.toHtml(bean.getMySessionUrl())%>" enctype="application/x-www-form-urlencoded" autocomplete="OFF" onsubmit="esf.hideAllSubmitInputFields()">
<table class="w100">
<tr>
	<td class="boB"><h1 class="pagename"><img src="./VAADIN/themes/openesf/icons/fatcow16/user.png" width="16" height="16" alt="Users"/>Forgot my password (request password reset)</h1></td>
</tr>
</table>

<% if ( bean.emailSent ) { %>

<p>Please look for the email just sent to you containing the unique link to reset your password.</p>  

<% } else { %>

<p>If you have forgotten your password, you can establish a new one online.</p>  
<p class="bold">For added security, a unique email with instructions will be sent to you to complete the process.</p>
<p><em>Please do this request only once and then wait for the email that will be sent to you.</em></p>  
<p>After you click the 'Send email to reset my password' button below:</p>
<ol>
	<li>Click on the link in the email sent to you</li>
	<li>Answer your previously chosen security question</li>
	<li>Set a new password</li>
</ol>
<p>If you do not want to reset your password at this time, click cancel.</p>  

<table style="margin-top: 1em;">
<tr>
	<td colspan="2"><label for="email" title="The system will send a unique link to this email address to reset your password">Enter your email address</label><br/>
		<input name="email" value="<%=bean.toHtml(bean.email)%>" size="100" maxlength="<%=Literals.EMAIL_ADDRESS_MAX_LENGTH%>" class="required" placeholder="you@example.com"/>
	</td>
</tr>
</table>

<table class="button">
<tr>
	<td class="padH"><input type="submit" class="button" name="<%=BeanData.CANCEL_BUTTON%>" value="Cancel"/></td>
	<td class="padH"><input type="submit" class="button" name="<%=BeanData.SEND_BUTTON%>" value="Send email to reset my password"/></td>
</tr>
</table>

<% } %>

</form>

<p style="margin-left: 4em;"><jsp:include page="./footer.jsp" flush="true" /></p>

</body>
</html>
<!-- "Bean" methods should be placed here -->
<%!
public class BeanData extends com.esignforms.open.jsp.PageBean {

    public BeanData() {	}

    // static values that are buttons
	public static final String CANCEL_BUTTON  = "bCancel";
    public static final String SEND_BUTTON    = "bSend";

    public static final String COOKIE_EMAIL   = "ce";

    public String email = "";
    public boolean emailSent = false;
    public User user = null;

    public boolean loginPageAllowEmbedded = false;


public PageProcessing doGet() throws java.io.IOException {
	if ( ! doInit() ) {
		return PageProcessing.DONE;
	}

	loadFormDataDoGet();

	return PageProcessing.CONTINUE;
}

public PageProcessing doPost()	throws java.io.IOException {
	if ( ! doInit() ) {
		return PageProcessing.DONE;
	}

	loadFormData();
	
    String button = getParam(SEND_BUTTON,null);
    if ( button != null ) {
        return doSendResetPasswordEmail();
    }

    button = getParam(CANCEL_BUTTON,null);
    if ( button != null ) {
        sendRedirect("./");
        return PageProcessing.DONE;
    }

    errors.addError("You pressed an unexpected button. Please try again.");
	return PageProcessing.CONTINUE;
}

private boolean doInit() throws java.io.IOException {
	endSession(); // kill any previously active session

	if ( ! app.hasPasswordManager() ) {
	    warning("Attempted to request a forgotten password reset when passwords are not enabled.");
	    sendRedirect("./");
	    return false;
	}
	
	String loginPageAllowEmbeddedValue = StringReplacement.replace("${property:ESF.LoginPageAllowEmbedded}");
	loginPageAllowEmbedded = EsfBoolean.toBoolean(loginPageAllowEmbeddedValue);
	
	if ( ! loginPageAllowEmbedded )
	{
		response.setHeader("X-Frame-Options","DENY");
		response.setHeader("Content-Security-Policy","frame-ancestors 'none'");
	}

	return true;
}

private void loadFormDataDoGet() {
    String cookieEmail = getCookie(COOKIE_EMAIL);
    if ( isBlank(cookieEmail) ) {
        email = "";
    } else {
        email = cookieEmail;
        saveCookie(COOKIE_EMAIL,email);
    }
}

private void loadFormData() {
    email = getParam("email","");
}	

private boolean validateFormData() {
    if ( isBlank(email) )
        errors.addError("Please enter your email address.","email");
    else if ( ! isValidEmail(email) )
        errors.addError("Please enter a valid email address.","email");
    else
    {
        user = User.Manager.getByEmail(email);
        if ( user == null )
            errors.addError("Sorry, please provide the email address to your Open eSignForms account.","email");
        else if ( ! user.isEnabled() )
            errors.addError("Sorry, your account is not enabled. Please contact your system administrator to enable your account first.","email");
        else if ( ! user.hasPassword() )
            errors.addError("Sorry, your account does not have a password set yet. Please contact your system administrator to set your initial password.","email");
    }

    return ! errors.hasError();
}

private PageProcessing doSendResetPasswordEmail()
{
    if ( ! validateFormData() )
        return PageProcessing.CONTINUE;
    
    try
    {
        app.getPasswordManager().requestForgotPassword(user,getExternalUrlContextPath(),getIP(),getUserAgent());
        errors.addSuccess("Thank you. A unique email has been sent to " + user.getEmail() + ". Please click on the link inside that email to reset your password.");
        emailSent = true;
    }
    catch( Exception e )
    {
        errors.addError(e.getMessage());
    }
    
    return PageProcessing.CONTINUE;
}

	
}
%>
