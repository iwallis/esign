/* Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// Used to configure the CKEditor's styles dropdown list.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
*/
CKEDITOR.stylesSet.add('esfStyleSet',[
/* Inline: all non-block, non-object selections */
{name:'Marker: Yellow',element:'span',styles:{'background-color':'Yellow'}},
{name:'Marker: Green',element:'span',styles:{'background-color':'Lime'}},
{name:'Signature',element:'span',styles:{'font-family':'\'Pacifico\', \'Bradley Hand ITC\', \'Lucida Handwriting\', \'Comic Sans MS\', cursive','font-size':'14pt'}},
{name:'Signature Blue',element:'span',styles:{'font-family':'\'Pacifico\', \'Bradley Hand ITC\', \'Lucida Handwriting\', \'Comic Sans MS\', cursive','font-size':'14pt','color':'blue'}},
{name:'Small Caps',element:'span',styles:{'font-variant':'small-caps'}},
{name:'Subscript',element:'sub'},
{name:'Superscript',element:'sup'},
{name:'Computer Code',element:'code'},
{name:'Deleted Text',element:'del'},
{name:'Inline Quotation',element:'q'},
/* Block: address, div, h1, h2, h3, h4, h5, h6, p and pre (none defined here) */
/* Object: a, embed, hr, img, li, object, ol, table, td, tr and ul */
{name:'Image on Left',element:'img',attributes:{style:'padding: 5px; margin-right: 5px',border:'2',align:'left'}},
{name:'Image on Right',element:'img',attributes:{style:'padding: 5px; margin-left: 5px',border:'2',align:'right'}},
{name:'Borderless Table',element:'table',styles:{'border-style':'hidden','background-color':'#E6E6FA'}},
{name:'Table Col top',element:'td',styles:{'vertical-align':'top'}},
{name:'Table Col middle',element:'td',styles:{'vertical-align':'middle'}},
{name:'Table Col bottom',element:'td',styles:{'vertical-align':'bottom'}},
{name:'Square Bulleted List',element:'ul',styles:{'list-style-type':'square'}},
{name:'Circle Bulleted List',element:'ul',styles:{'list-style-type':'circle'}}
]);