<!DOCTYPE html">
<%@page contentType="text/html; charset=UTF-8" 
	import="com.esignforms.open.data.EsfName
	       ,com.esignforms.open.data.EsfPathName
	       ,com.esignforms.open.data.EsfValue
	       ,com.esignforms.open.prog.TransactionTemplate
	       ,com.esignforms.open.runtime.reports.TodoListingInfo
	       ,com.esignforms.open.user.PermissionOption
	       "%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(session,request,response);
if ( bean.doGet() == BeanData.PageProcessing.DONE )
	return;
%>

<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - Start a transaction</title>
	<meta name="Description" content="Start transactions without using the regular Web 2.0 application, suitable for simplified access via tablets and phones - Version 12/16/2015"/>
	<meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <base href="<%=bean.getExternalUrlContextPath()%>/"/>
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="shortcut icon" />
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="icon" />

<style type="text/css">
<jsp:include page="../static/esf/esf.css" flush="true" />
</style>
</head>

<body class="esf">
<table class="w100 boB">
<tr>
	<td><img alt="logo" src="./images/LogoForApp/" /></td>
	<td class="nowrap" style="padding-left: 20px;"><b><esf:out value="<%=bean.getLoggedInUser().getFullDisplayName()%>"/></b></td>
	<td style="padding-left: 20px; padding-right: 10px; vertical-align: middle;"><a href="<%=bean.getMySessionUrl()%>" title="Refresh this page."><img src="./VAADIN/themes/openesf/icons/fatcow16/arrow_refresh_small.png" alt="Refresh"/> <b>Refresh</b></a></td>
	<td style="padding-left: 20px; padding-right: 10px; vertical-align: middle;"><a href="./logoff.jsp?t=startlo" title="Please logoff to secure your account when done."><img src="./VAADIN/themes/openesf/icons/esf/logoff.png" alt="Logoff" /> <b>Logoff</b></a></td>
</tr>
</table>

<% if ( bean.errors.hasErrorOrWarning() ) { %>
<%=bean.errors.toHtml()%>
<% } %>

<% if ( bean.todoListingInfoList.size() > 0 ) { %>
<div style="background-color: #e9e7e8; margin-top: 1em; padding: 5px; width: 98%; border-radius: 10px; border: 2px solid #105AA5;">
<h2 style="margin-top: 10px;">To Do Listing: <span class="f11 italic">Please complete your pending transactions...</span></h2>
<ol>
<% for( TodoListingInfo todoListingInfo : bean.todoListingInfoList ) { %>
	<li class="nowrap"><img src="./VAADIN/themes/openesf/icons/fatcow16/pencil.png" alt="To Do icon" align="bottom" /> 
		<a href="<%=bean.getExternalUrlContextPath()%>/P/<%=todoListingInfo.getPickupCode()%>" title="Template: <%=todoListingInfo.getTemplatePathName().toHtml()%> - Status: <%=todoListingInfo.getStatusText()%>" target="_blank"><strong><esf:out value="<%=todoListingInfo.getTemplateDisplayName()%>"/></strong> &nbsp; <em>Party '<esf:out value="<%=todoListingInfo.getPartyDisplayName()%>"/>' last updated on <esf:out value="<%=todoListingInfo.getLastUpdatedTimestamp().toString(bean.getLoggedInUser())%>"/></em></a>
	</li>
<% } %>
</ol>
</div>
<% } %>

<% if ( bean.templates.size() == 0 ) { %>
<h2 style="color: red;"><img src="./VAADIN/themes/openesf/icons/fatcow16/cog_go.png" alt="Start tran icon" align="bottom"/> <em>Sorry, but you are not authorized to start any transactions.</em></h2>
<% } else { %>
<table class="cellpadding5 cellspacing1">
	<% for( TransactionTemplate template : bean.templates ) { %>
	<tr>
	<% if ( template.isProductionEnabled() || bean.includeTestLinks) { %>
		<td class="nowrap">
		<img src="./VAADIN/themes/openesf/icons/fatcow16/cog_go.png" alt="Start tran icon" align="bottom"/> 
		<% if ( template.isProductionEnabled() ) {%>
			<a href="<%=template.getExternalStartUrl(bean.getExternalUrlContextPath(),true,false) %>" title="<esf:out value="<%=template.getPathName().toString()%>"/> - <esf:out value="<%=template.getDescription()%>"/>" target="_blank"><esf:out value="<%=template.getDisplayName()%>"/></a>
		<%}else{%>
			<span title="<esf:out value="<%=template.getPathName().toString()%>"/> - <esf:out value="<%=template.getDescription()%>"/>"><esf:out value="<%=template.getDisplayName()%>"/></span>
		<%}%>
		</td>
	<% } %>
	<% if ( bean.includeTestLinks) { %>
		<td class="nowrap f8">
		<% if ( template.isTestEnabled() ) {%>
			<a href="<%=template.getExternalStartUrl(bean.getExternalUrlContextPath(),false,true) %>" title="Run it as a test transaction" target="_blank">Test</a>
		<%}else{%>
			<span title="Testing this transaction is not allowed.">(n/a)</span>
		<%}%>
		</td>
		<td class="nowrap f8">
			<% if ( template.isProductionEnabled() && template.isTestEnabled() ) {%>
				<a href="<%=template.getExternalStartUrl(bean.getExternalUrlContextPath(),true,true) %>" title="Run it as a test like production transaction" target="_blank">Test like production</a>
			<%}else{%>
				<span title="Testing this transaction like production is not allowed.">&nbsp;</span>
			<%}%>
		</td>
	<% } %>
	</tr>
	<% } %>
</table>
<% } %>

<p style="margin-left: 4em;"><jsp:include page="../footer.jsp" flush="true" /></p>

</body>
</html>

<!-- "Bean" methods should be placed here -->
<%!
public class BeanData extends com.esignforms.open.jsp.PageBean {
	public BeanData() {}
	
	public String companyName;
	public java.util.List<TransactionTemplate> templates;
	public java.util.List<TodoListingInfo> todoListingInfoList;
	public boolean includeTestLinks = false;
	
public PageProcessing doGet() throws java.io.IOException {
	if ( ! checkLoggedIn() )
		return PageProcessing.DONE;
	
	String esftestParam = getParam(com.esignforms.open.servlet.StartTransaction.PARAM_ESFTEST);
	if ( esftestParam != null )
	{
		// We assume if the ESFTEST param is present, that we want test mode, unless it's set to an "off" value explicitly (starts with N, n, F, f or 0).
    	if ( esftestParam.length() > 0 )
    	{
        	char firstChar = Character.toLowerCase(esftestParam.charAt(0));
        	includeTestLinks = ! (firstChar == 'n' || firstChar == 'f' || firstChar == '0'); 
    	}
    	else
    		includeTestLinks = true; 
	}
	
	EsfValue value = com.esignforms.open.prog.Library.Manager.getTemplate().getPropertyValue(new EsfName("MyCompany"), new EsfPathName("InformalName"), false, false);
	companyName = value == null ? "" : value.toString();
	
	templates = TransactionTemplate.Manager.getForUserWithPermission(getLoggedInUser(), PermissionOption.TRAN_PERM_OPTION_START);
	templates.remove(TransactionTemplate.Manager.getTemplate());
	
	java.util.Iterator<TransactionTemplate> iter = templates.iterator();
	while( iter.hasNext() )
	{
		TransactionTemplate template = iter.next();
		if ( template.isProductionDisabled() && template.isTestDisabled() ) // remove if neither production nor test is enabled
			iter.remove();
		else if ( template.isProductionDisabled() && ! includeTestLinks ) // remove if production is off and they are not testing
			iter.remove();
	}
	if ( templates.size() > 1 ) {
		// Let's show these in display name order
		java.util.Collections.sort(templates, TransactionTemplate.Manager.ComparatorByDisplayName);
	}

	todoListingInfoList = TodoListingInfo.Manager.getMatching(true,getLoggedInUser().getId(),getLoggedInUser().getEmailAddress().getEmailAddress());
	
	return PageProcessing.CONTINUE;
}

}
%>
