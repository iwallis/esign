<!DOCTYPE html>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%@page session="false" contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>TEST Start Precise Hire</title>
	<meta name="Description" content="Start a Precise Hire background check" />
</head>

<body>

<form method="post" action="https://dot.precisehire.com/beta/out/HROSBeginOrder.php" enctype="application/x-www-form-urlencoded" name="aform">

<p>Please ensure all values are entered as needed.  No validation takes place on this form.  What is entered is what will be posted to Precise Hire.</p>

<div style="border: 1px solid black; margin-top: 1em;">
<input type="text" size="50" name="username" value=""/> username
<br /><input type="text" size="50" name="password" value=""/> password
</div>

<div style="border: 1px solid black; margin-top: 1em;">
<input type="text" size="50" name="postbackurl" value="https://esign-hr.com/demo/HttpVariableTester.jsp?c=1234"/> postbackurl
</div>

<div style="border: 1px solid black; margin-top: 1em;">
<input type="text" size="50" name="applicantinfo__email" value=""/> applicantinfo__email
<br /><input type="text" size="50" name="applicantinfo__firstname" value="Mark"/> applicantinfo__firstname
<br /><input type="text" size="50" name="applicantinfo__middlename" value="A"/> applicantinfo__middlename
<br /><input type="text" size="50" name="applicantinfo__lastname" value="Smitt"/> applicantinfo__lastname
<br /><input type="text" size="11" name="applicantinfo__ssn" value="111-22-3333"/> applicantinfo__ssn
<br /><input type="text" size="10" name="applicantinfo__dob" value="04/14/1967"/> applicantinfo__dob
<br /><input type="text" size="50" name="applicantinfo__streetaddress" value="123 Main St."/> applicantinfo__streetaddress
<br /><input type="text" size="5" name="applicantinfo__zipcode" value="76022"/> applicantinfo__zipcode
</div>

<div style="border: 1px solid black; margin-top: 1em;">
<input type="text" size="2" name="mvr__state" value="TX"/> mvr__state
<br /><input type="text" size="50" name="mvr__licensenumber" value="01234567"/> mvr__licensenumber
</div>

<div style="border: 1px solid black; margin-top: 1em;">
<input type="text" size="2" name="state_criminal__state" value="TX"/> state_criminal__state
<br /><input type="text" size="2" name="county_criminal__state" value="TX"/> county_criminal__state
<br /><input type="text" size="50" name="county_criminal__county" value="Travis"/> county_criminal__county
</div>

<div style="border: 1px solid black; margin-top: 1em;"> Employer 1
<br /><input type="text" size="50" name="employment__companyname[]" value="Acme"/> employment__companyname[]
<br /><input type="text" size="50" name="employment__manager[]" value="Ben Stiller"/> employment__manager[]
<br /><input type="text" size="50" name="employment__telephone[]" value="425-555-1111"/> employment__telephone[]
<br /><input type="text" size="50" name="employment__positionheld[]" value="Coyote tracker"/> employment__positionheld[]
<br /><input type="text" size="50" name="employment__salary[]" value="$32,000"/> employment__salary[]
<br /><input type="text" size="50" name="employment__employmentdates[]" value="10/20/2005-now"/> employment__employmentdates[]
<br /><input type="text" size="50" name="employment__employmentcity[]" value="Austin"/> employment__employmentcity[]
<br /><input type="text" size="2" name="employment__employmentstate[]" value="TX"/> employment__employmentstate[]
<br /><input type="text" size="50" name="employment__reasonforleaving[]" value="Better opportunity"/> employment__reasonforleaving[]
</div>

<div style="border: 1px solid black; margin-top: 1em;"> Employer 2
<br /><input type="text" size="50" name="employment__companyname[]" value="Walmart"/> employment__companyname[]
<br /><input type="text" size="50" name="employment__manager[]" value="B.B. King"/> employment__manager[]
<br /><input type="text" size="50" name="employment__telephone[]" value="408-555-2222"/> employment__telephone[]
<br /><input type="text" size="50" name="employment__positionheld[]" value="Sales clerk"/> employment__positionheld[]
<br /><input type="text" size="50" name="employment__salary[]" value="$48,000"/> employment__salary[]
<br /><input type="text" size="50" name="employment__employmentdates[]" value="01/01/2004-10/20/2005"/> employment__employmentdates[]
<br /><input type="text" size="50" name="employment__employmentcity[]" value="Dallas"/> employment__employmentcity[]
<br /><input type="text" size="2" name="employment__employmentstate[]" value="TX"/> employment__employmentstate[]
<br /><input type="text" size="50" name="employment__reasonforleaving[]" value="Acme called"/> employment__reasonforleaving[]
</div>

<div style="border: 1px solid black; margin-top: 1em;"> School 1
<br /><input type="text" size="50" name="education__schoolname[]" value="University of Texas at Austin"/> education__schoolname[]
<br /><input type="text" size="50" name="education__campusname[]" value="Austin"/> education__campusname[]
<br /><input type="text" size="50" name="education__city[]" value="Austin"/> education__city[] 
<br /><input type="text" size="2" name="education__state[]" value="TX"/> education__state[] 
<br /><input type="text" size="50" name="education__courseofstudy[]" value="Business"/> education__courseofstudy[] (major)
<br /><input type="text" size="50" name="education__degree[]" value="BA"/> education__degree[]
<br /><input type="text" size="1" name="education__graduated[]" value="1"/> education__graduated[]
<br /><input type="text" size="50" name="education__graduationdate[]" value="05/15/1989"/> education__graduationdate[]
</div>

<div style="border: 1px solid black; margin-top: 1em;"> School 2
<br /><input type="text" size="50" name="education__schoolname[]" value="Bellevue College"/> education__schoolname[]
<br /><input type="text" size="50" name="education__campusname[]" value="Bellevue"/> education__campusname[]
<br /><input type="text" size="50" name="education__city[]" value="Bellevue"/> education__city[] 
<br /><input type="text" size="2" name="education__state[]" value="WA"/> education__state[] 
<br /><input type="text" size="50" name="education__courseofstudy[]" value="English"/> education__courseofstudy[] (major)
<br /><input type="text" size="50" name="education__degree[]" value="BA"/> education__degree[]
<br /><input type="text" size="1" name="education__graduated[]" value="0"/> education__graduated[]
<br /><input type="text" size="50" name="education__graduationdate[]" value=""/> education__graduationdate[]
</div>

<p><input type="submit" name="testButton" value="TEST POST to Precise Hire"/></p>

</form>

</body>
</html>

