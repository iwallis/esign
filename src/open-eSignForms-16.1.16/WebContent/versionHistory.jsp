<!DOCTYPE html>
<%@page session="false" contentType="text/html; charset=UTF-8" isELIgnored="true"
import="com.esignforms.open.config.Literals,
        com.esignforms.open.Version"
%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(null,request,response);
if ( bean.isGet() )
{
	if ( bean.doGet() == BeanData.PageProcessing.DONE )
		return;
}
else
{
	if ( bean.doPost() == BeanData.PageProcessing.DONE )
		return;
}
%>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - Version history</title>
	<meta name="Description" content="Lists information about all versions of Yozons Open eSignForms software. Updated 1/15/2016"/>
	<meta name="License" content="Yozons (Commercial) and GNU Affero General Public License (AGPL)"/>
	<meta name="robots" content="index, follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <base href="<%=bean.getExternalUrlContextPath()%>/"/>
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="shortcut icon" />
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="icon" />
<style type="text/css">
<jsp:include page="./static/esf/esf.css" flush="true" />
</style>

</head>

<body class="esf" style="background-color: #f5f5f5;">
<jsp:include page="./header.jsp" flush="true" />
<%=bean.errors.toHtml()%>

<h1 class="pagename">Current version is <esf:out value="<%=Version.getReleaseString()%>"/></h1>

<p><em>Per the <a href="http://open.esignforms.com/agpl.txt">AGPL</a>, the original (&quot;unmodified&quot;) source code for Yozons Open eSignForms can be retrieved from <a href="http://open.esignforms.com" target="_blank">open.esignforms.com</a>.</em></p>

<table class="w100" style="margin-top: 1em;">

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 16.1.16 - 16 January 2016</td></tr>
<tr><td><ul>
	<li>The 'export to Excel' and 'Download CSV' buttons on reports no longer include non-data columns (like those that are buttons).
	Such non-data columns using the CSV export servlet are also no longer included.</li>
	<li>Fixed reports to only allow transaction templates that a user has 'List' permission to be included. For report that include
	a list of transaction templates, only those that the user has permission to list can be included.</li>
	<li>Added new built-in 'Resume tran' button report field template.  If this report field column is added to your report, and the transaction
	is currently suspended and the user has 'resume' permission, a button will appear to allow the user to resume the suspended	transaction.</li>
	<li>For custom logic rule events Timer Expired, Transaction Canceled, Transaction Resumed, Transaction Started, 
	and Transaction Suspended and Transaction Update API, we can match a package's rule that limits based on party if the
	selected party is currently Active.  This allows special actions to take place based on which party is active when these
	events occur.  This is most useful for the Transaction Resumed event should you suspend a transaction and later resume it
	and want to inform the active party he or she can try again now.</li>
	<li>The Send Email Action will attempt to notify all users who play the role of a package party defined as having a To Do Group
	and the 'notify all' flag is set.  This only takes place if the Send Email Action specifies a party link (to know which package party)
	to check for To Do groups) but no email specification, and the party has not yet been locked to a given user or email address.</li>
	<li>Upgraded to libphonenumber 7.2.3.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>YES</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 15.12.19 - 19 December 2015</td></tr>
<tr><td><ul>
	<li>Changed application's JSP pages to use HTML 5.  This does not affect documents yet, but the future will generate documents
	in HTML 5.</li>
	<li>Added 'Skip document party' to the change status action to remove a document from the processing flow for a specified party.</li>
	<li>Added beta code for light integration with APS Payroll's EIP API.</li>
	<li>Upgraded to CKEditor for Vaadin 7.10.6 which uses CKEditor 4.5.6.</li>
	<li>Upgraded to libphonenumber 7.2.2.</li>
	<li>Upgraded to Vaadin 7.5.10</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 15.11.21 - 21 November 2015</td></tr>
<tr><td><ul>
	<li>Per <a href="https://github.com/OpenESignForms/openesignforms/issues/157">issue 157</a>, added ability to create overriding
	property sets on a per-user basis.</li>
	<li>Per <a href="https://github.com/OpenESignForms/openesignforms/issues/158">issue 158</a>, added a calculate date interval
	action available to the custom logic rules for documents and/or packages.</li>
	<li>Remember user preferences for string-based report search field values for user-defined report fields that are searchable.
	Also, you can just enter '=' to mean search where that field's value is blank or null. Also, the '!' prefix can be used to negate the matching.</li>
	<li>Per <a href="https://github.com/OpenESignForms/openesignforms/issues/160">issue 160</a>, allow file upload fields to specify
	whether non-image files may be uploaded, and if so whether to show uploaded images inline using a maximum width of 200, 400, 600 or 800 pixels.
	Note that when an image is shown inline, that file is assumed to have been viewed by the party.</li>
	<li>Allow field templates to be marked as 'Mask on input' to specify that field should work like a password field and not show
	field input as it's entered.</li>
	<li>When creating a new button message set in a library, start with the default button message set to have initial values
	that can be tweaked rather than being entirely blank.</li>
	<li>Allow the button message set and package+disclosure document to override the package's values for a given package party.</li>
	<li>Put a fixed limit of 20,000 activity log records being retrieved when viewing user, system and transaction logs.</li>
	<li>Upgraded to libphonenumber 7.1.1.</li>
	<li>Upgraded to Vaadin 7.5.9</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>YES</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 15.10.17 - 17 October 2015</td></tr>
<tr><td><ul>
	<li>Per <a href="https://github.com/OpenESignForms/openesignforms/issues/159">issue 159</a>, fixed the System config->Deployment view to no longer attempt to parse/convert the displayed installation date, regardless of the default date format,
	back to a date field.</li>
	<li>Upgraded to CKEditor for Vaadin 7.10.5 which uses CKEditor 4.5.4.</li>
	<li>Upgraded to libphonenumber 7.1.0.</li>
	<li>Upgraded to BouncyCastle JCE and PKIX 1.53.</li>
	<li>Upgraded to Vaadin 7.5.7</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>OPTIONAL</b> - Changed default heartbeatInterval to 1800. 
			Commented out the filter and filter-mapping for setCharacterEncodingFilter as we set
			it in Tomcat's conf/web.xml.  But for those who cannot set this across all webapps, you'll
			want to keep it. Added the context-param org.atmosphere.cpr.sessionSupport.</li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 15.8.23 - 23 August 2015</td></tr>
<tr><td><ul>
	<li>This release is dated to honor David's mother, Marcella Joan Wall, who would be celebrating her 84rd birthday.</li>
    <li>Per <a href="https://github.com/OpenESignForms/openesignforms/issues/156">issue 156</a>, a new transaction administration
    API has been added. Details are in the <a href="https://github.com/OpenESignForms/openesignforms/wiki/ProgrammingGuide_Integration_API#tranadmin">online API documentation</a>.
    Special thanks to the <a href="http://www.icco.com">International Computer Consulting Organization (ICCO)</a> 
    for sponsoring this API's expedited development.</li>
	<li>Upgraded to CKEditor for Vaadin 7.10.4 which uses CKEditor 4.5.3.</li>
	<li>Upgraded to PDFBox 1.8.10 (with companion fontbox 1.8.10 and jempbox 1.8.10).</li>
	<li>Upgraded to libphonenumber 7.0.9.</li>
	<li>Upgraded to javax.mail 1.5.4.</li>
	<li>Upgraded to ConfirmDialog 2.1.3.</li>
	<li>Upgraded to PopupButon 2.6.0.</li>
	<li>Upgraded to JDOM 2.0.6.</li>
	<li>Upgraded to Vaadin 7.5.4</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>YES</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 15.7.11 - 11 July 2015</td></tr>
<tr><td><ul>
	<li>Added find limits to the tran search and reports. By default, it will limit the results to 100 transactions, which
	is generally more than a user wants to see. If there are more transactions than the chosen limit, the
	find button will show the count with a '+' to indicate more exist. Of course, you can still run the report to show all matches.</li>
    <li>There are times when an optional field should be styled as if it were required (generally a field that is not always displayed, 
    but when it is, it's enforced to be required through custom logic rules). Changed the field definition from a checkbox for required or optional, 
    to a select list that allows for required, optional and optional style as if required.</li>
    <li>No longer suppress the 'Test' button in the document page editor when not in edit mode, showing it in the page editor 
    when it's also visible in the document version view.</li>
    <li>Per <a href="https://github.com/OpenESignForms/openesignforms/issues/146">issue 146</a>, transaction templates 
    have a new permission to allow the use of the Update API on transactions. The /U/ path references the update API.
    After a successful update, the transaction update API event is fired.  There's a new Condition for custom logic
    to check if the Update API event name matches for those cases in which there are more than one type of updates taking place via the API.</li>
    <li>Per <a href="https://github.com/OpenESignForms/openesignforms/issues/154">issue 154</a>, if a package party requires a logged-in user 
    and specifies a To Do group, ensure the logged in user actually belongs to that group or matches the party's specified email address.</li>
	<li>Transactions started via the API mode to auto-complete the party now create a document snapshot as each is processed.</li>
	<li>Upgraded to CKEditor for Vaadin 7.10.0 which uses CKEditor 4.5.1.</li>
	<li>Upgraded to libphonenumber 7.0.7.</li>
	<li>Upgraded to Vaadin 7.5.1</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>YES</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 15.5.23 - 23 May 2015</td></tr>
<tr><td><ul>
    <li>Migrated from the dying Google Code to GitHub, including <a href="https://github.com/OpenESignForms/openesignforms/wiki">wikis</a> 
    and <a href="https://github.com/OpenESignForms/openesignforms/issues">issues</a>. Google Code is no longer being used.</li>
	<li>Per <a href="https://github.com/OpenESignForms/openesignforms/issues/153">issue 153</a>, Added last accessed timestamp to the transaction party assignment so it can be shown in the transaction details. 
	It is updated each time the transaction is accessed using the party's unique pick up code/link.
	Prior party accesses on previous transactions will show an estimated last accessed timestamp based on the 
	last update timestamp for documents the party completed.</li>
	<li>Per <a href="https://github.com/OpenESignForms/openesignforms/issues/152">issue 152</a>, fixed the File access tags when created using the
	file link feature of CKEditor. Also, removed the warning about leaving the page when clicking on file links.</li>
	<li>Per <a href="https://github.com/OpenESignForms/openesignforms/issues/151">issue 151</a>, reloading transaction report fields will limit
	itself to transactions that match the specific package version (instead of all that match the package without regard
	to the version). So after updating the 'map report fields' in a package version, save it, and then reload and only transactions that
	use that package version's set of report fields will be reloaded.</li>
	<li>Under certain error conditions when processing workflow events, if the event is a significant error, no longer
	will any user-defined actions in custom logic be run for the event. Bad events are essentially skipped to avoid
	custom logic from trying to make sense of it.</li>
	<li>Changed timer expiration event so if it fires when the transaction is canceled, the timer name is renamed to
	have "-Canceled" appended to the end; or if the transaction is suspended, the timer name is renamed to have "-Suspended" append to the end.
	This allows you to handle a timer event for canceled or suspended transactions if you really need to do so, but
	it won't match your expired timer name if not specifically tested in a condition for normal operations.</li>
	<li>When trying to set or calculate a field value in custom logic, if the field template exists, but there is no such field created, try
	to create all missing fields to see if that causes the field to become available. This issue occurs if you start a transaction and then
	subsequently add a field to an existing document version referenced by that transaction.</li>
	<li>Added the current database size to the System config->Deployment view when using a commercial DB license.</li>
	<li>Added an Auto-Post Event for custom logic in a document. This allows you to take actions when a user selects a dropdown configured for Auto-Post
	or the like.  This event is similar to the Save button being pressed except no data validations take place.</li>
	<li>Added the ability to suspend and resume transactions via the 'Change tran/party status' action.</li>
	<li>Added custom logic events for suspended and resumed transactions.</li>
	<li>Fixed DbSetup to boot itself after doing the 'initdb' so the key generation will have its properties in place.</li>
	<li>Added the Vaadin push mode to the System config->Stats view.</li>
	<li>Upgraded to PDFBox 1.8.9 (with companion fontbox 1.8.9 and jempbox 1.8.9).</li>
	<li>Upgraded to libphonenumber 7.0.4.</li>
	<li>Upgraded to javax.mail 1.5.3.</li>
	<li>Upgraded to DragDropLayouts 1.1.3.</li>
	<li>Upgraded to ResetButtonForTextField 1.2.1.</li>
	<li>Upgraded to Vaadin 7.4.7</li>
	<li>Upgraded to run on Java 8.0.45 with its respective JCE policy files. Note that development still compiles targeting Java 7.</li>
	<li>Upgraded to Tomcat 8.0.23 (note that 8.0.21 has websocket issues). Updated the TLS section for the HTTPS connector to use a more secure list of ciphers, though it means
	dropping support for Java 6 clients and Microsoft IE6.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>YES</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 15.3.12 - 12 March 2015</td></tr>
<tr><td><ul>
	<li>This release is dated to honor David's father Ed's 90th birthday.</li>
	<li>Fixed multi-page review of documents using <code>${document:page.number}</code> notation per <a href="http://code.google.com/p/openesignforms/issues/detail?id=143">issue 143</a>.</li>
	<li>Fixed re-resolving of document versions in an existing transaction per <a href="http://code.google.com/p/openesignforms/issues/detail?id=149">issue 149</a>.
	Document versions are fixed at transaction start to prevent custom logic issues.</li>
	<li>Changed <code>&lt;meta name=&quot;viewport&quot; content=&quot;initial-scale=1, maximum-scale=1&quot; /&gt;</code> 
	to <code>&lt;meta name=&quot;viewport&quot; content=&quot;width=device-width, initial-scale=1&quot; /&gt;</code> to better
	support responsive design for mobile needs.</li>
	<li>When doing a CANCEL TRANSACTION 'Status Change' action, we now allow the expiration date to be extended as well as reduced. Previously
	it would ignore requests to make the expiration date later.</li>
	<li>A File Confirm Click field can be used in any of the 'Blank' condition checks on a custom logic action to determine if an optional file confirm click field
	has been clicked by the current party or not.</li>
	<li>Added methods <code>stringToInt</code> and <code>stringToBool</code> to the EsfDocumentPageInterface to help in condition testing for Strings field values to be converted into
	integers or booleans.</li>
	<li>Allow web sessions to be ended per <a href="http://code.google.com/p/openesignforms/issues/detail?id=147">issue 147</a>. The UI
	for UserSessionView must have delete permission set for users in that group to be able to end a web session.</li>
	<li>Allow caches to be cleared per <a href="http://code.google.com/p/openesignforms/issues/detail?id=148">issue 148</a>. A button was added to the deployment properties when in edit mode.</li>
	<li>Added transaction detail activity logs for each button pressed by a party.</li>
	<li>Merged code from the VAADIN7 branch in CVS back to the HEAD. The project named open-eSignformsVaadin7 is now renamed back to open-eSignFormsVaadin to match the name in CVS.
	Also, the widgetset Open-esignformsvaadin7Widgetset has been renamed to Open-esignformsvaadinWidgetset, so be sure to remove the old
	VAADIN/widgetsets and replace with the new.  Related to this, the web.xml file changes a bit for these names:<br/>
	<code>&lt;servlet-name&gt;OpenESignFormsVaadinApplication&lt;/servlet-name&gt;</code> replaces <code>&lt;servlet-name&gt;OpenESignFormsVaadin7Application&lt;/servlet-name&gt;</code> in the 'servlet' and 'servlet-mapping' entries; and<br/>
	<code>&lt;param-value&gt;com.esignforms.open.vaadin.widgetset.Open_esignformsvaadinWidgetset&lt;/param-value&gt;</code> replaces <code>&lt;param-value&gt;com.esignforms.open.vaadin.widgetset.Open_esignformsvaadin7Widgetset&lt;/param-value&gt;</code>.</li>
	<li>Upgraded to BouncyCastle JCE and PKIX 1.52.</li>
	<li>Upgraded to Vaadin 7.3.10</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>YES</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 15.2.7 - 7 February 2015</td></tr>
<tr><td><ul>
	<li>Fixed a bug that failed to correctly display the drop down option label when using a dynamic drop down and the 
	value comes from anything but the default drop down.</li>
	<li>Fixed issue with ESF_reports_access pseudo party getting an email notification when it's activated
	on an in-progress transaction. The email contained a link that would then process as if the party were
	configured. No email notification is sent for this party.</li>
	<li>Fixed issue with email templates containing field specifications such as ${out:fieldname} in which
	no document name is included. The code will first attempt to use the current document, if available,
	otherwise it will find the first document that has a value for the named field.</li>
	<li>Fixed exception in SendEmailAction when sending an email on a canceled transaction event.</li>
	<li>Added ability to set and cancel transaction timers and the ability to do custom logic on a timer expired event.</li>
	<li>Upgraded to libphonenumber 7.0.2.</li>
	<li>Upgraded to CKEditor for Vaadin 7.9.6 which uses CKEditor 4.4.7.</li>
	<li>Upgraded to Vaadin 7.3.9.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>YES</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 15.1.3 - 3 January 2015</td></tr>
<tr><td><ul>
	<li>Moved the status messages from the lower right to the upper right corner and added a yellow border to help make it stand out.</li>
	<li>Upgraded to PDFBox 1.8.8 (with companion fontbox 1.8.8 and jempbox 1.8.8).</li>
	<li>Upgraded to PopupButton 2.5.0.</li>
	<li>Upgraded to Vaadin 7.3.7.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 14.12.6 - 6 December 2014</td></tr>
<tr><td><ul>
	<li>This release is dated to honor David's godson Alex who just turned 13!</li>
	<li>Added ability to mass start transactions by uploading a CSV file with the initial data to use, one row for each transaction, with the first row
	specifying the field names to set using the data values in the subsequent rows. 
	This resolves <a href="http://code.google.com/p/openesignforms/issues/detail?id=141">issue 141</a></li>
	<li>Added the ability to view/append field specs (${xxx}) from fields that allow them, as well as from the document page editor to make it a bit 
	easier to look at document fields, propertsets, serial generators and document/transaction built-in fields across libraries.</li>
	<li>Due to various bugs discovered in wkhtmltopdf 0.12.1, we strip <code>border-radius</code> CSS from the HTML before converting to PDF. Also, added code to report errors/warnings
	emitted by wkhtmltopdf even when it ends with a successful exit status of 0. We also strip the esf.js from captured documents as finalized documents
	and PDFs have no need for our JavaScript.  Lastly, for multi-file HTML to PDF, we added <code>--javascript-delay 1000</code> to prevent certain
	errors in the rendering even though no javascript is actually used.</li>
	<li>Created new document download or view button that allows for selecting the documents to view/download as HTML, PDF or in a ZIP file and makes use
	of a new servlet for serving back the file to be more reliable. We recommend using this button (esf_download_selected_snapshots) in your reports 
	instead of the prior esf_download_pdf_selected_snapshots which is deprecated.</li>
	<li>The prior report button esf_download_pdf_latest_snapshots has been updated to be a Link that uses the more reliable servlet for retrieving the PDFs.</li>
	<li>Added the production retention specification to the transaction templates list.</li>
	<li>Experimental: Enable Vaadin server push. The code currently disables push for browsers older than IE 10, Safari 5, Firefox 9, Opera 11 and Chrome 13.
	As always, we recommend using the latest version of your browser to enhance security and functionality. 
	The following changes to web.xml are needed: Add listener for Atmosphere used in push:
<pre><code>&lt;listener&gt;
    &lt;listener-class&gt;org.atmosphere.cpr.SessionSupport&lt;/listener-class&gt;
&lt;/listener&gt;</code></pre>
	Enable Vaadin's automatic push mode for the OpenESignFormsVaadin7Application servlet entry:
<pre><code>&lt;init-param&gt;
    &lt;param-name&gt;pushmode&lt;/param-name&gt;
    &lt;param-value&gt;automatic&lt;/param-value&gt;
&lt;/init-param&gt;
&lt;async-supported&gt;true&lt;/async-supported&gt;</code></pre>
	</li>
	<li>Updated web.xml to indicate it's using Servlet 3.1 (JSP 2.3) that comes with Tomcat 8 (not required if you keep push disabled):
<pre><code>&lt;web-app xmlns=&quot;http://xmlns.jcp.org/xml/ns/javaee&quot;
  xmlns:xsi=&quot;http://www.w3.org/2001/XMLSchema-instance&quot;
  xsi:schemaLocation=&quot;http://xmlns.jcp.org/xml/ns/javaee
                      http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd&quot;
  version=&quot;3.1&quot;&gt;</code></pre>
	</li>
	<li>Upgraded to ConfirmDialog 2.1.2.</li>
	<li>Upgraded to DragDropLayouts 1.1.1.</li>
	<li>Upgraded to PopupButton 2.4.1.</li>
	<li>Upgraded to TableExport 1.5.1.5.</li>
	<li>Upgraded to libphonenumber 7.0.1.</li>
	<li>Upgraded to CKEditor for Vaadin 7.9.4 which uses CKEditor 4.4.6.</li>
	<li>Upgraded to Vaadin 7.3.6.</li>
	<li>Upgraded to run on Java 8.0.25 with its respective JCE policy files. Note that development still compiles targeting Java 7 (not required if you keep push disabled, but works even if you do).</li>
	<li>Upgraded to Tomcat 8.0.15 (not required if you keep push disabled, but works even if you do).</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>YES</b></li>
			<li>web.xml changes required: <b>YES</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 14.11.1 - 01 November 2014</td></tr>
<tr><td><ul>
	<li>Show messages to parties who close their browser before completing the document(s) in their package.</li>
	<li>Suppress ENTER key being used to auto-submit forms instead of having parties click on the appropriate buttons shown on each page.</li>
	<li>Automatically include Google WOSS font 'Pacifico' for use as a handwritten signature in esf.css. 
	To use it in existing systems, you'll need to update your ESF_Fonts drop down and document styles.</li>
	<li>Annotate the electronic signature process record for Test transactions to make it clear it is for testing only (non-binding).</li>
	<li>Added a Save button for parties completing documents resolving <a href="http://code.google.com/p/openesignforms/issues/detail?id=136">issue 136</a></li>
	<li>Upgraded to PDFBox 1.8.7 (with companion fontbox 1.8.7 and jempbox 1.8.7).</li>
	<li>Upgraded to Vaadin 7.3.3.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 14.9.27 - 27 September 2014</td></tr>
<tr><td><ul>
	<li>Allow PDF to have working file download links for File and FileConfirmClick fields. Also allows access from the original HTML document snapshots.</li>
	<li>Allow a report to limit which documents can be viewed/downloaded. By default, all documents in the selected transaction templates are available,
	but this allows a report to restrict the documents that can be accessed.</li>
	<li>Allow a party to have a distinct To Do Group setting for test transactions. This is useful when a given party shouldn't be involved in test
	transactions.</li>
	<li>Changed the File field in documents to be an integer value containing the number of files uploaded. Previously it was a dummy string value.</li>
	<li>Search field values entered in reports are now trimmed of leading and trailing whitespace.</li>
	<li>Send Email Action now supports multiple email addresses that replace ${EMAIL} in email templates.</li>
	<li><b>* NOTE</b>: For SET FIELD VALUE actions, transforms to get numbers, alpha or alphanumeric no longer remove whitespace.
	If your transform needs this previous behavior, you must update the action to also include the 'strip whitespace' transform.</li>
	<li>Report templates have two new permission options: 1) view live transactions and 2) update live transactions, via
	the esf_report_access special party. Users in groups with these permissions will have an icon shown in the report listings that
	will provide access to the transaction directly from the report using the esf_reports_access party. Access is 'view only optional'
	to the documents allowed by the report.  If the update permission is given to a user and the transaction points to a package that
	defines the esf_reports_access party, documents may be updated via the reports regardless of the status of the transaction, 
	including changes to canceled or completed transactions.  Generally, the esf_reports_access party is only defined for documents
	that are being used for the purpose of on-going record management and not for general contracts, though some may use it
	to correct data, though of course the corrections will not change the digitally signed snapshots of the data and documents
	that took place earlier. This capability resolves <a href="http://code.google.com/p/openesignforms/issues/detail?id=131">issue 131</a>.</li>
	<li>Updated the Button Message Set to include ability to configure an 'Edit' button on the package that is used when updating
	a transaction via the reports. Also added a 'Save' button for documents in edit mode that is also used when updating a document via
	the reports.</li>
	<li>Document and package custom logic now supports the PartySavedDocumentEvent to add actions when the 'Save' button is used.</li>
	<li>Upgraded to CKEditor for Vaadin 7.9.3 that supports CKEditor 4.4.4.</li>
	<li>Upgraded to Vaadin 7.3.1.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>YES</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>YES</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 14.8.23 - 23 August 2014</td></tr>
<tr><td><ul>
	<li>This release is dated to honor David's mother, Marcella Joan Wall, who would be celebrating her 83rd birthday! We miss you, Sally!</li>
	<li>Fixed <a href="http://code.google.com/p/openesignforms/issues/detail?id=132">issue 132</a> to fire the PartyCreatedEvent,
	PartyActivatedEvent and PartyTransferredEvent when the party defined in the event matches the custom logic rule party name 
	rather than using the current party which is generally not the correct party (as these tend to fire when another is the party and
	the process flow is moving ahead or changing).</li>
	<li>Added <a href="http://code.google.com/p/openesignforms/issues/detail?id=133">issue 133</a> to allow _TEST suffix property name checks when
	in test modes. This allows a property set to be created that has both production values (normal) and special test-only values.  The normal
	resolution of propertysets takes place for Production, Test Like Production and Test modes.  But if you define the same property name
	with an _TEST suffix and you are in Test Like Production or Test modes, it will return that property instead.  This is useful
	for property values that include more sensitive information that should not be used for tests (such as SSN/EIN, email addresses, passwords)
	and is used by the PayPal plugin so when testing you go against the PayPal test site rather than a real credit card charge.</li>
	<li>Fixed <a href="http://code.google.com/p/openesignforms/issues/detail?id=134">issue 134</a> to allow the EIN prefix of 47.</li>
	<li>Fixed <a href="http://code.google.com/p/openesignforms/issues/detail?id=137">issue 137</a> to allow report searches against date fields using date ranges.</li>
	<li>Fixed <a href="http://code.google.com/p/openesignforms/issues/detail?id=138">issue 138</a> to allow a condition to check against each
	selected value in a multi-select dropdown rather than just against the first selected value.</li>
	<li>Fixed <a href="http://code.google.com/p/openesignforms/issues/detail?id=139">issue 139</a> to disable the transaction status
	checkboxes on 'tran search' and 'reports' when 'only stalled' was previously selected.</li>
	<li>Changed HTTP Send Action's URL to allow a field spec rather than fixed URL.  This allows for dynamic URLs, especially when
	switching between Test and Production modes.</li>
	<li>Allow a file upload field on a document to be pre-populated from initial HTTPS POST data that starts a transaction. The file data must
	be Base-64 encoded and then URL encoded (like all HTTPS POST data). You may	also include additional params <code>FileParamName__fileName</code> 
	that contains the file's name, and <code>FileParamName__fileMimeType</code> that contains the file's mime/content type
	(in this example, the Base-64 encoded file data itself is sent using the param name <code>FileParamName</code>).
	The file will be assumed to have been uploaded and accessed by the first party in the package.  If the file name isn't included, the
	FileParamName will be used.  If the mime type isn't included, it will be assumed to be a binary file. Multiple files may be attached.</li>
	<li>Upgraded to BouncyCastle JCE and PKIX 1.51.</li>
	<li>Upgraded to CKEditor for Vaadin 7.9.2.</li>
	<li>Upgraded to Vaadin 7.2.6.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 14.7.26 - 26 July 2014</td></tr>
<tr><td><ul>
	<li>Fixed the date selector in document date fields for Internet Explorer when the date field is on right/bottom
	edge and the popup wouldn't normally fit.</li>
	<li>Made checkbox and radio button images have transparent backgrounds.</li>
	<li>Changed esf.css for signatures and signature meta data to not wrap 
	text (<code>white-space: nowrap;</code>). Made changes to print styles to ensure 90% font-size is used.</li>
	<li>Changed package report field mapping to allow a specific report field name to be entered to create a new one. 
	Also, the drop down of possible choices no longer includes all report template fields, just those that have not
	already been mapped in the package.</li>
	<li>Added ability to specify a package party is "view optional" for a given document. This is similar to the current
	"view only" party except that the party is not required to view the document. This allows a party to see a document 
	if they want, but not be compelled to click through them in order to complete the party's step. A "view only" party
	still must view the document to complete it.</li>
	<li>Fixed issue with connection pool's JDBC driver being double-registered.</li>
	<li>Allow the document and field to be changed in the HTTP Send Action configuration of Document Fields to send.</li>
	<li>Added method <code>getFieldSpecValueOrBlankIfNotFound(String fieldSpec)</code> to <code>EsfDocumentPageInterface</code> to get
	the value of the fieldSpec expansion, or blank if it doesn't expand to anything but itself.</li>
	<li>Added convenience method <code>isFieldSpecBlank(String fieldSpec)</code> to <code>EsfDocumentPageInterface</code> to check if
	the value of the fieldSpec expansion is blank (essentially <code>isBlank(getFieldSpecValueOrBlankIfNotFound(fieldSpec))</code>).</li>
	<li>Added convenience method <code>isFieldSpecNonBlank(String fieldSpec)</code> to <code>EsfDocumentPageInterface</code> to check if
	the value of the fieldSpec expansion is nonblank (essentially <code>isNonBlank(getFieldSpecValueOrBlankIfNotFound(fieldSpec))</code>).</li>
	<li>Upgraded to DragDropLayouts 1.1.</li>
	<li>Upgraded to libphonenumber 6.2.</li>
	<li>Upgraded to wkhtmltopdf 0.12.1.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>YES</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 14.6.7 - 7 June 2014</td></tr>
<tr><td><ul>
	<li>Closed <a href="http://code.google.com/p/openesignforms/issues/detail?id=64">issue 64</a> as the current ComparisonCondition already handles date and datetime comparisons. 
	Added an activity log error message for comparison operators that do not make sense for the type.  Changed the comparison condition setup
	to only allow the appropriate operators based on the type of the field being checked.
	To avoid any issues, be sure that the default date format for dates/datetimes, as defined in the deployment configuration, that you compare against (i.e. using a comparison against a field specification like 
	<code>${DocumentName.DateFieldName}</code>) use one of the top three date formats (i.e. 15-Feb-2011, 02/15/2011 or 2011-02-15) as these string formats for a date
	can all be parsed back into a date object as required to perform the date comparison.</li>
	<li>Fixed <a href="http://code.google.com/p/openesignforms/issues/detail?id=65">issue 65</a> to allow custom logic calculations on date and datetime fields.</li>
	<li>Fixed <a href="http://code.google.com/p/openesignforms/issues/detail?id=126">issue 126</a> to allow a percentage to be entered with or without a trailing '%' in a Decimal field.</li>
	<li>Fixed <a href="http://code.google.com/p/openesignforms/issues/detail?id=127">issue 127</a> to allow an Integer, Money and Decimal field to be blanked out using the SET FIELD VALUE action in custom logic.</li>
	<li>Fixed <a href="http://code.google.com/p/openesignforms/issues/detail?id=128">issue 128</a> ensure the e-signature process record is shown -- if so set on the document -- on multi-page documents in which
	the signature is not on the last page.</li>
	<li>Added Tomcat's SetCharacterEncodingFilter filter and filter-mapping to set UTF-8 to web.xml.</li>
	<li>Added URIEncoding="UTF-8" to both the HTTP and HTTPS Connector elements in Tomcat's server.xml.</li>
	<li>For CSS styling of individual pages, each page is now output with a DIV class attribute set to the page's name.
	Of course, the original DIV 'id' attribute is still set to the page's transformed UUID. This is true
	for both single and multi-page documents.</li>
	<li>Upgraded to libphonenumber 6.1.</li>
	<li>Upgraded to Animator 1.7.4.</li>
	<li>Upgraded to TableExport 1.5.1.3.</li>
	<li>Upgraded to DragDropLayouts 1.0.1.</li>
	<li>Upgraded to PDFBox 1.8.5 (with companion fontbox 1.8.5 and jempbox 1.8.5).</li>
	<li>There's a new configuration element in the openesignforms.properties file that can be used to ensure
	randomly generated strings, such as in UUIDs, pickup codes for URLs, etc. do not contain any undesirable
	substrings.  While rare, random generators can produce possibly offensive strings, so this setting will ensure
	our random strings do not include the specified ones (using a semicolon-separated list of strings).  
	In this example, this setting won't allow the words "bad" or "word": <code>RandomKey.undesirable.string.segments=bad;word</code></li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>YES</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 14.4.12 - 12 April 2014</td></tr>
<tr><td><ul>
	<li>Fixed <a href="http://code.google.com/p/openesignforms/issues/detail?id=121">issue 121</a> so an empty 
	AND or OR condition will show it has no subconditions including any negated NOT.
	Also, if the top level condition (AND or OR condition) on an Action has no subconditions, we just assume it passes regardless if it's negated.</li>
	<li>Ensure HTML files written for input to wkhtmltopdf are written as UTF-8.</li>
	<li>Ensure JSPs and CSS files are written as UTF-8.</li>
	<li>Allow dropdown option labels to be up to 100 characters instead of 50.</li>
	<li>Allow report field labels to be up to 100 characters instead of 50.</li>
	<li>Allow a company name to be placed on the main page title area for added branding. It is defined in the ESF/Library/Template
	HTML property <code>ESF.AppTitleInHtml</code>.  It will default to the company's informal name centered in a 12pt bold blue font.</li>
	<li>Reverted wkhtmltopdf 0.12.0 back to using 0.11.0rc1 as we had unexpected blank pages appearing in 0.12.0.</li>
	<li>Upgraded to TableExport 1.5.1.1.</li>
	<li>Upgraded to Apache POI 3.10-FINAL.</li>
	<li>Upgraded to CKEditor for Vaadin 7.9.1.</li>
	<li>Upgraded to Vaadin 7.1.13.</li>
	<li>Upgraded to Apache PDFBox 1.8.4 along with its dependency releases JempBox 1.8.4 and FontBox 1.8.4.</li>
	<li>Fixed <a href="http://code.google.com/p/openesignforms/issues/detail?id=123">issue 122</a> to not crash creating a field template in a document like a reusable library-defined field template.</li>
	<li>Fixed <a href="http://code.google.com/p/openesignforms/issues/detail?id=123">issue 123</a> to block attempts to create new libraries like reserved libraries, prevent
	exporting if they cannot view the details, and prevent importing if they do not have update permission.</li>
	<li>Fixed <a href="http://code.google.com/p/openesignforms/issues/detail?id=124">issue 124</a> to not starting a transaction in which it cannot find its component documents, most often the result
	of running a production transaction -- or test like production -- with a document in the package having no production version available.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>YES</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>YES</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 14.3.15 - 15 March 2014</td></tr>
<tr><td><ul>
	<li>Fixed report template's esf_download_html_selected_snapshot built-in field when many documents are listed so it has a scrollbar. 
	Also, it opens the HTML documents in a popup window instead of doing a download.</li>
	<li>Added download HTML snapshots button to transaction details from tran search and reports.</li>
	<li>Added method <code>isPackageParty(String pkgPartyName)</code> to <code>EsfDocumentPageInterface</code> which is used for simple JSP-based 
	programming in a document. Normally, a document only knows about document parties, but the package+disclosure document is not tied
	to document parties and thus generally needs to rely on the package party name to enable variable disclosures based on party. Note that
	this implies that the package+disclosure document is non-generic and built to work specifically for your package.</li>
	<li>Added method <code>isPartyCompleted()</code> to <code>EsfDocumentPageInterface</code> to check if the current party is completed or not.
	The default package+disclosure was updated to take advantage of this.</li>
	<li>Added method <code>isPartyLoggedIn()</code> to <code>EsfDocumentPageInterface</code> to check if the current party is a logged in user or not.</li>
	<li>Added method <code>isTransactionCompleted()</code> to <code>EsfDocumentPageInterface</code> to check if the transaction is completed or not.</li>
	<li>Added method <code>isTransactionDeleted()</code> to <code>EsfDocumentPageInterface</code> to check if the transaction is deleted or not.</li>
	<li>Added method <code>isBlank(String)</code> to <code>EsfDocumentPageInterface</code> to check if the specified is string is blank (null, empty string or all whitespace) or not.</li>
	<li>Added method <code>isNonBlank(String)</code> to <code>EsfDocumentPageInterface</code> to check if the specified is string is not blank (not null, not empty string and not all whitespace) or not.
	Most likely this is used on package+disclosure pages if you want to suppress some content if the user has deleted the transaction. 
	The default package+disclosure was updated for this very purpose.</li>
	<li>Clean up orphaned outbound email attachments and fixed transaction deletion to also remove email attachments.</li>
	<li>Upgraded to ConfirmDialog 2.0.5.</li>
	<li>Upgraded to Vaadin 7.1.12.</li>
	<li>Various stability, security and bug fixes with other minor enhancements.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>YES</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>YES</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 14.2.8 - 8 February 2014</td></tr>
<tr><td><ul>
	<li>To reduce confusion, we clear the transaction's auto-cancel-timestamp when the transaction is completed or canceled. Note that
	auto-cancel only works on in-progress transactions, so this has no effect on how it works. However, in the unlikely event
	a canceled transaction is later re-activated, the prior auto-cancel timestamp will no longer be in effect, and if the retention
	was changed on cancel, that new retention value will remain.  We recommend starting a new transaction like an existing one
	when a canceled transaction really should be done again.</li>
	<li>Allow FileConfirmClick fields to set other File names to use beside the default configured in the field template. This allows
	for one party to select from a set of Files in the Library so that particular file will be used for confirmed clicks.</li>
	<li>Increased outbound email limits for the text part from 8K to 16K and the HTML part from 64K to 512K.</li>
	<li>Added an example PickupNotification email template to the default company library created on installation.</li>
	<li>Clean up orphaned transaction activity log records, and block such records from being written in the future. Such log records
	were created when using the Test button on the document page editor when it includes custom logic.</li>
	<li>Added report column type for downloading document snapshots in its native HTML format (already support PDF).</li>
	<li>Added ButtonGroup widget 2.3 and use to group buttons when appropriate.</li>
	<li>Allow CTRL-S in the document page editor to effect a SAVE as if clicking the SAVE button.</li>
	<li>Upgraded to CKEditor for Vaadin 7.9.0.</li>
	<li>Upgraded to Vaadin 7.1.10.</li>
	<li>For Windows as well as CentOS 6 or new Linux systems, updated to wkhtmltopdf 0.12.0.</li>
	<li>Various stability, security and bug fixes with other minor enhancements.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>YES</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>YES</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 14.1.4 - 4 January 2014</td></tr>
<tr><td><ul>
	<li>Updated look for the default e-sign disclosure and package listing document &quot;StandardPackageDisclosures&quot; in the standard ESF template library.</li>
	<li>Updated look for the default e-sign invitation email &quot;DefaultPickupNotification&quot; in the standard ESF template library, as well as 
	the system emails for setting passwords and the like.</li>
	<li>New image LogoForEmail that's used in the updated emails. The DbSetup program will create this file based on the current Logo, albeit
	set to allow it embedded using a data URI.</li>
	<li>No longer save and restore tabs when accessing via iOS or Android to avoid mobile browser performance issues.</li>
	<li>No longer append a 'CREATED_LIKE' suffix to names when creating a new object. Instead, the original name will be shown, but will
	be in error until a unique name is entered.</li>
	<li>Upgraded to PopupButton 2.3.0.</li>
	<li>Various stability, security and bug fixes with other minor enhancements.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>YES</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 13.12.11 - 11 December 2013</td></tr>
<tr><td><ul>
	<li>On Production-Test mode changes, update the reports list based on user permissions like we do for starting transactions. Also, the report
	will now block if a user tries to run it against production transactions when the report template doesn't allow it. Also, no longer
	remove all menu items under reports/start-tran on a mode change so as not to inadvertently remove other choices like the 'Tips' entry.</li>
	<li>Reports can now also list transactions in which a user was a party to it along with the previous ability for the user who started it. 
	By default, if a user can run a report, that user can see transactions that user started or was a party to. 
	Added a new permission to the report templates to set the groups that can also see transactions in which other users 
	(only those users the user has permission to see) were a party to it.</li>
	<li>No longer include fully skipped documents (all parties to the document were skipped) among the document listing on the transaction details view 
	of reports and tran search to reduce clutter and speed up rendering for packages that skip large numbers of documents.  
	Skipped documents, if present, are listed at the bottom.</li>
	<li>Upgraded to libphonenumber 5.9.</li>
	<li>Upgraded to BouncyCastle JCE and PKIX/CMS 1.50.</li>
	<li>Upgraded to DragDropLayouts 1.0.</li>
	<li>Upgraded to Vaadin 7.1.9.</li>
	<li>Upgraded to Apache Tomcat 7.0.47.</li>
	<li>Various stability, security and bug fixes with other minor enhancements.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>YES</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 13.11.9 - 9 November 2013</td></tr>
<tr><td><ul>
	<li>Added capability to specify that field templates overlay an image in a document for instances in which
	a document's page is an image/scan of the original document and the fields appear to be entered into locations
	in that image. Overlay fields add CSS-like positions for left, top, width and height to specify the area of the 
	image where the field should appear. You can position a field (like <code>${field:XXX}</code>), field+label (like <code>${XXX}</code> or <code>${fieldlabel:XXX}</code>) 
	or even just a display field value (like <code>${out:XXX}</code>).
	Note that a document page can have more than one image and mix and match
	images with regular HTML. The upper left corner of the image is consider left: 0 and top: 0.</li>
	<li>Add ability to specify if an image should be embedded in the HTML using data URIs, such as for the overlay feature,
	or whether they should just use the standard IMG tag that retrieves the image using a separate HTTP request.
	In documents, the use of data URIs is ignored for IE5, IE6, IE7 and IE8 if the image is more than 32k when encoded.
	All modern browsers support data URIs.</li>
	<li>Give the above two features a try using our <a href="https://open.esignforms.com/demo/S/Free/W9" target="_blank">Demo W-9</a></li>
	<li>The non-Web 2.0 '/start/' page's logoff button will now redirect to the login page so that on re-login
	it will take you back to the start page rather than to the normal Web 2.0 page.</li>
	<li>Removed default buttons (click by pressing ENTER) as Vaadin has a security issue with 
	delivering such pseudo-clicks to the correct button, even delivering them to buttons on non-visible
	tabs and non-visible areas of the page.</li>
	<li>Added search result counts to To Do, custom reports and Transaction Search tabs, as well as number of sessions for the User Sessions tab.</li>
	<li>Added ability to search by party pickup code from general Transaction Search. It works like a tran-id
	search in that all other values are ignored in the search criteria as it is a unique key.</li>
	<li>Added friendlier error pages for parties who get errors or exceptions while processing a document to replace the
	standard error pages generated by Tomcat.</li>
	<li>Upgraded to PostgreSQL JDBC driver 9.2-1003.jdbc4.</li>
	<li>Upgraded to CKEditor for Vaadin 7.8.7.</li>
	<li>Upgraded to ResetButtonForTextField 1.1.1.</li>
	<li>Upgraded to Vaadin 7.1.7.</li>
	<li>Various stability, security and bug fixes with other minor enhancements.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>YES</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>YES</b> (five error-page entries)</li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 13.10.5 - 5 October 2013</td></tr>
<tr><td><ul>
	<li>Added support for including one or more completed documents in the HTTP Send action. You can choose the type of information to send for such documents,
	such as sending the HTML document(s), sending the selected documents merged as a single digitally signed PDF file, 
	sending the XML data for easy access to all data fields in a document, or sending the XML digitally signed document and data snapshots.
	All files are sent use Base-64 encoding. It is recommended to use POST when sending document data.</li>
	<li>Added support for File field types to be included in the HTTP Send action. Any uploaded/attached file(s) will be sent over using Base-64 encoding.
	Two additional param-values are automatically included: <code>YourFileParamName__fileName</code> and <code>YourFileParamName__fileMimeType</code> that 
	represent the uploaded file's file name and mime type. Note that two underbars (<code>__</code>) are used as to append the suffix to whatever 
	param name you have specified ('YourFileParamName') for the File field.  It is recommended to use POST when sending file data.</li>
	<li>Tip: Base-64 encoding converts data into an ASCII format, essentially emitting 4 ASCII characters for every 3 input bytes. It has only
	three special characters: <code>+</code>, <code>/</code> and <code>=</code> (the equals sign for padding at the end only), but oddly enough all three have special meaning in
	URLs (and for URL encoding for name-value params for HTTPS GET/POST). Therefore, when viewing the Base-64 encoded data in the HTTP Send logs,
	you will see URL-encode Base-64, so <code>+</code> is replaced by <code>%2B</code>, <code>/</code> is replaced by <code>%2F</code> 
	and <code>=</code> is replaced by <code>%3D</code>.</li>
	<li>Added support for including party pick up LINK(s) in the HTTP Send action.</li>
	<li>No longer enable main menu tabs to be reordered via drag and drop on Apple iPad, iPod and iPhones since their Safari browser does not
	work like the desktop Safari (and all other) browser. Previously, mobile Safari allowed you to drag and reorder tabs, but you 
	couldn't click them to activate them.</li>
	<li>Added To Do transactions pending for a user at the top of the 'start' page for non-Web 2.0 users.</li>
	<li>Added <code>&lt;meta name=&quot;viewport&quot; content=&quot;initial-scale=1, maximum-scale=1&quot; /&gt;</code> to document
	pages to better scale it for viewing on mobile devices.</li>
	<li>Added optional location field to the user record, with programmatic access via built-in field specs
	<code>$ {transaction:ESF_Started_By_User_Location}</code> and <code>$ {transaction:user.location}</code>.</li>
	<li>Allow a package to specify a 'download filename spec' to override the default names used when users download
	their documents in their package, as well as in reports, etc.</li>
	<li>Changed the order transaction templates are listed in menus and the start page to use the display name
	rather than it's unique path name.</li>
	<li>Changed the order report templates are listed in menus to use the display name rather than it's unique path name.</li>
	<li>Upgraded to Vaadin 7.1.6.</li>
	<li>Various stability, security and bug fixes with other minor enhancements.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>YES</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 13.9.7 - 7 September 2013</td></tr>
<tr><td><ul>
	<li>Added calls to reseed our secure random number generator periodically based on events that have no fixed time, such
	as users logging in, emails being sent or received, etc. This helps preclude attempts to back-trace the seed established
	when the application is started.</li>
	<li>Updated the menu/tabs so that as tabs are closed, the underlying view is also cleared (no more view caching). 
	Then if the tab is re-opened, a fresh instance is created.</li>
	<li>Allow the menu/tabs to be re-ordered using drag and drop so they appear in the order you prefer. New tabs are still put
	at the end, but you can now move it to a different position as desired.</li>
	<li>If a transaction party is activated to a given user, block pick-ups by other users or external parties.</li>
	<li>When unlocking a transaction via the To Do screen, transfer the party assignment to keep track of the unlocking user as well as 
	whoever processes it next.</li>
	<li>Added Vaadin add-on DragDropLayouts 1.0.0.alpha4.</li>
	<li>Upgraded to Vaadin 7.1.3.</li>
	<li>Upgraded to CKEditor for Vaadin 7.8.6.</li>
	<li>Upgraded to Apache PDFBox 1.8.2 along with its dependency releases JempBox 1.8.2 and FontBox 1.8.2.</li>
	<li>Upgraded to libphonenumber 5.8.</li>
	<li>Upgraded to JavaMail 1.5.0.</li>
	<li>Various stability, security and bug fixes with other minor enhancements.</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>NO</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>NO</b></li>
			<li>web.xml changes required: <b>NO</b></li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 13.8.10 - 10 August 2013</td></tr>
<tr><td><ul>
	<li>When starting a transaction with initial POST data, if the field in the document has an initial value specified, it will be
	used (as before), but now if no value is set and the field name matches the name of an initial POST param, 
	it will be set to the value from the initial POST data.</li>
	<li>Can now download all of the latest documents as a PDF with a single selection on the transaction detail page 
	via Transaction Search or a custom Report PDF download button.</li>
	<li>Custom reports can now include a button to download all of the latest documents as a PDF.</li>
	<li>Custom reports can now include a button to select specific document versions (or all latest) to download as a PDF.</li>
	<li>New HTTP SEND Action can be specified in a package's custom logic. This allows for outbound integration to 
	update external systems. Supports HTTP/HTTPS, GET/POST, and sending name-value pairs based on data in your package of documents.</li>
	<li>Added HTTP Send request and response logs to the transaction detail page from Tran Search and Reports.</li>
	<li>Updated deployment stats page includes a check for the HTTP Send thread.</li>
	<li>Added setting <code>upload.file.maxMB=100</code> to the openesignforms.properties file. This was the hard-coded default value, but some low-memory
	servers may need to set this lower, such as to 10MB. Also settings for <code>upload.image.maxMB=5</code> and <code>upload.config.maxMB=10</code>.</li>
	<li>Added create-like button to the Action list for both package custom logic as well as document custom logic.</li>
	<li>Added £ and € to the default money drop down formats, as well as one with no currency symbol, in ESF/Library/Template.</li>
	<li>Added ability to get the ordinal day number for a date format, so we now support formats like: 10th day of August, 2013</li>
	<li>Upgraded to Vaadin 7.1.2, which includes Atmosphere for websocket-based server push.</li>
	<li>Upgraded to PopupButton 2.2.1.</li>
	<li>Upgraded to CKEditor for Vaadin 7.8.5.</li>
	<li>Upgraded to Java 7.0.25. Required unexpected fix to XML digital signature generation and verification to specify 
	the QualifyingProperties Id attribute as the "id" for the element.</li>
	<li>Upgraded to Tomcat 7.0.42. We found issues with earlier versions that used the NIO connectors and had to use only BIO. With
	this upgrade, we are able to resume using the NIO connectors.</li>
	<li>Upgraded to libphonenumber 5.6.</li>
	<li>Various stability, security and bug fixes with other minor enhancements.</li>
	<li>Special Lucky #13 Happy Birthday wishes to Yozons -- you are now a teenager company!</li>
	<li><em>OPERATIONS VERSION UPGRADE NOTICE:</em>
		<ul>
			<li>Database updates required (pre.sql/update.sql/post.sql): <b>YES</b></li>
			<li>rundbsetup/DbSetup DB conversion required: <b>YES</b></li>
			<li>web.xml changes required: <b>YES</b>  See new context-param heartbeatInterval, servlet init-param pushmode,
			and servlet async-supported. These are for the Vaadin 7.1 update.</li>
		</ul>
	</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 13.6.8 - 8 June 2013</td></tr>
<tr><td><ul>
	<li>Created a new main user interface layout that we hope is simpler and cleaner for our users. The main view
	opens activities in a tab sheet. On logoff, the view tab sheets are saved and then restored on the next login.
	<a href="http://code.google.com/p/openesignforms/wiki/OpenESF_UI_Changes_v13_6_8" target="_blank">You can read more about the new user interface here</a>.</li>
	<li>Created a new document page editor layout that we hope is simpler and cleaner for document programmers.</li>
	<li>Ported to Vaadin 7.0.7.</li>
	<li>Added Vaadin add-on ResetButtonForTextField 1.0.5.</li>
	<li>Upgraded to ConfirmDialog 2.0.4 (updated for Vaadin 7).</li>
	<li>Upgraded to Animator 1.7.3 (updated for Vaadin 7).</li>
	<li>Upgraded to TableExport 1.4.0 (updated for Vaadin 7).</li>
	<li>Upgraded to PopupButton 2.2.0 (updated for Vaadin 7).</li>
	<li>Upgraded to CKEditor for Vaadin 7.8.3 (updated for Vaadin 7).</li>
	<li>Upgraded to Java 7.0.21.</li>
	<li>Upgraded to Apache Commons Logging 1.1.3.</li>
	<li>Upgraded to libphonenumber 5.5.</li>
	<li>Upgraded to BouncyCastle JCE and PKIX/CMS 1.49.</li>
	<li>Database updates are not required, nor is a rundbsetup/DbSetup DB conversion required.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 13.5.5 - 5 May 2013</td></tr>
<tr><td><ul>
	<li>Added ability to block further party edits to fields when their value is set. This makes it easier for documents that are also used in packages where some data values
	are set from prior fields in the package. When the field is set through custom logic and this option is specified, subsequent parties will no longer be able to change it even if they otherwise would
	have field edit permission.</li>
	<li>Added ability to jump directly to any completed document from a multi-document package listing.</li>
	<li>Added data transforms to the SET FIELD VALUE Action, including a substring feature with an offset and length, plus a set of built-in transforms including:
		to uppercase, to lowercase, capitalize the first character, capitalize the first character and make the others lowercase, compress whitespace, strip whitespace,
		trim whitespace, get numbers only, get alphabetic only, get alphaNumeric only, and get first character only (which is just a simpler way to do a substring offset 0 length 1).</li>
	<li>Added ability to limit the number of files that can be uploaded on a File Upload field. The default is unlimited files.</li>
	<li>Added ability to make the view/download of uploaded files (via the File upload field) optional for a given party, or even to block all access for a given party through
	custom logic in either the document or the package.</li>
	<li>Allow passwords (pass phrases) for login to be as long as 100 characters.</li>
	<li>Upgraded to CKEditor for Vaadin 1.8.2. Fixes issues with editor removing extra code added through the SOURCE mode because of Advanced Content Filtering.</li>
	<li>Upgraded to PostgreSQL 9.2.4 to resolve a potentially serious security flaw, though unlikely to be an issue if deployed in a standard way.</li>
	<li>Upgraded to PDFBox 1.8.1 along with its dependency releases JempBox 1.8.1 and FontBox 1.8.1.</li>
	<li>Upgraded to libphonenumber 5.4.</li>
	<li>Upgraded to BouncyCastle JCE and PKIX/CMS 1.48.</li>
	<li>Upgraded to UrlRewriteFilter 4.0.3.</li>
	<li>Upgraded to JDOM 2.0.5.</li>
	<li>Database updates are not required, while a rundbsetup/DbSetup DB conversion is required.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 13.4.13 - 13 April 2013</td></tr>
<tr><td><ul>
	<li>Added mass transaction exports via the reports so you can take all of your data with you should you need to keep documents longer
	than you'd like to keep them online, or should you stop using the software.</li>
	<li>Added ability to set the placeholder attribute of input/textarea field -- called an input prompt -- that shows a tip as long as the field is empty.</li>
	<li>Augmented the logic behind the 'initial value' specification for field templates. Previously, these values were set at transaction startup to initialize
	any document field values (typically using the initiator's information as well as any data posted to the transaction's start URL).  Now, along with that
	setting, it will also be set when a party retrieves a document if no value has been set in the field, allowing a document's field to be more easily set based
	on a prior document's field value. Of course, the standard custom action for SET FIELD VALUE will work complete with conditions and at specific processing steps
	when finer control is needed.</li>
	<li>Added an 'unlock' button to the To Do listing. If a To Do user starts to process the transaction (and thus locked it so others cannot also do so) but decides
	another user in the group should process it, it can be unlocked and returned so that anybody within the To Do Group specified will be able to process it.</li>
	<li>Added a new permission for a Group's member users to allow the viewing and managing the To Do queue of other users.</li>
	<li>For users with permission to manage the To Do queue of other users, the To Do listing allows for the selection of which user's To Do queue to list.</li>
	<li>Upgraded to CKEditor for Vaadin 1.8.0.</li>
	<li>Upgraded to Apache Commons Logging 1.1.2.</li>
	<li>Upgraded to Apache Commons FileUpload 1.3.</li>
	<li>Upgraded to Vaadin 6.8.10.</li>
	<li>Database updates are required, and it requires a rundbsetup/DbSetup DB conversion.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 13.3.12 - 12 March 2013</td></tr>
<tr><td><ul>
	<li><strong><em>Please be sure to have installed the previous 13.2.2 release and run your deployment at least once to ensure the boot
	key has been properly encoded to support future upgrading.</em></strong> In this release, the default salt is going from 8 bytes to 16 bytes and the iteration
	count is going from 51 to 50000.</li>
	<li>This release is dated to honor David's father, <a href="http://aepwall.blogspot.com/">Ed (A.E.P) Wall</a>, who is celebrating his 88th birthday!</li>
	<li>Added a File Confirm Click field type that allows you to add a link to a file uploaded in your library so that it will track whether a given
	party has clicked on the link to download/read it or not. If such a field is required for a party, the party will not be able to move forward
	until clicking on the link.</li>
	<li>Allow an SSN field to be specified as an EIN field for normal or masked display.</li>
	<li>Allow a package version to be exported using XML as the transfer format. Package permissions are not included.</li>
	<li>Allow a package version's contents to be replaced by the imported XML from a previous export.</li>
	<li>Added the ability to defined drop down lists inside a document version (rather than just in a Library). This is useful for drop downs that really are just
	for the specific document.</li>
	<li>Added icons to show with error messages to users to draw more attention to them, one for okay and another for errors.</li>
	<li>Allow the main application logo shown after login to be branded.</li>
	<li>Created a SystemDown servlet that allows for a standard message or one customized by an HTML file (defaults to webdown.html) by restarting
	the web app after swapping the WEB-INF/webdown.xml for WEB-INF/web.xml so that all requests are processed to return the same message.</li>
	<li>Added additional configurations to allow the login page title and welcome header to be fully customized. Also, allow the
	login page to be embedded in an IFRAME (the login page by default will ensure it's the top page to prevent embedding), 
	though it does introduce a security issue for those who choose this path as the login page will not show the domain name and SSL certificate.</li>
	<li>Upgraded to CKEditor for Vaadin 1.7.5.</li>
	<li>Upgraded to Vaadin 6.8.9.</li>
	<li>Database updates are not required, but it requires a rundbsetup/DbSetup DB conversion.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 13.2.2 - 2 February 2013</td></tr>
<tr><td><ul>
	<li>Created an HTML page (add <code>/start/</code> to your login URL) that will provide links to all transactions a user is allowed to start, 
	but without having to use the full Web 2.0 web application. This is suitable for use to access these links from tablets and phones.
	Test transactions can also be started if you use <code>/start/?ESFTEST=Yes</code>.</li>
	<li>Added salt length and iteration counts to the stored password-based-encrypted boot key to allow these to be changed over time. 
	<strong><em>Every deployment should be started at least once with this release to ensure the boot key is properly encoded to support
	changes in the future.</em></strong></li>
	<li>Upgraded to CKEditor for Vaadin 1.7.4 with support for CKEditor 4.0.1.</li>
	<li>Upgraded to libphonenumber 5.3.</li>
	<li>Upgraded to Vaadin 6.8.8.</li>
	<li>Database updates are not required, and it does not require a rundbsetup/DbSetup DB conversion.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 13.1.12 - 12 January 2013</td></tr>
<tr><td><ul>
    <li>Allow a drop down change in value to auto-post the form data to the server to update values and redisplay the same page without any data validations.</li>
    <li>Allow a drop down to have a dynamic name specification to use if a suitably named drop down can be found, using the defined drop down if not.</li>
    <li>If a user uses a transaction template 'Start URL' but is not logged in and the transaction cannot be started by external parties (non-users) and
    the request is not an API request, redirect the user to the login page instead of showing the not-authorized error message.</li>
    <li>Introduced the 'esf' object available for custom Java programming inside a document. It represents the current document/page state.
    For more info see the <a href="http://code.google.com/p/openesignforms/wiki/ProgrammingGuide#Advanced_programming_of_a_document">advanced programming section of the programming guide</a>.</li>
	<li>DbSetup initialization includes a company-specific report template set up so the company programming group has full permissions to it. Previously the system administrator
	had to create a report template suitable for any distinct groups that needed to create reports.</li>
	<li>Added optional fields employeeId, jobTitle, department and phone to the user record.</li>
	<li>Added the new user fields to the transaction data record for the user who starts the transaction using notation like <code>$ {transaction:ESF_Started_By_User_Job_Title}</code>
	as well as created new fields to access the current user who is acting as a party using notation like <code>$ {transaction:user.job_title}</code>.</li>
	<li>Upgraded to Apache Commons IO 2.4.</li>
	<li>Upgraded to Apache Log4J 1.2.17.</li>
	<li>Upgraded to Apache POI 3.9.</li>
	<li>Upgraded to JavaMail 1.4.5.</li>
	<li>Database updates are required, and does not require a rundbsetup/DbSetup DB conversion.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 12.12.15 - 15 December 2012</td></tr>
<tr><td><ul>
	<li>Each library now has a button/message set option to customize button labels and related messages shown to users when the buttons
	are present during package/document rendering. The default button message set is in the ESF/Library/Template named ESF_DefaultButtonMessage.
	Each package can specify the button message set to use, but if the same button message set is defined in the transaction template's branding library
	that set will be used for those transactions. NOTE: If your deployment customized the buttons using the previous ESF_Buttons property sets (now obsolete),
	you will need to update the appropriate button message sets to have the customized values.</li>
	<li>Changed from the ChameleonTheme to the Reindeer theme.</li>
	<li>Upgraded to Vaadin 6.8.7.</li>
	<li>Upgraded to CKEditor for Vaadin 1.7.2 built on the new CKEditor 4.0.</li>
	<li>Upgraded to JDOM 2.0.4.</li>
	<li>Upgraded to PostgreSQL 9.2.2. Requires a backup/pg_dump on the previous version 8.4/9.0/9.1 database, 
	followed by a pg_restore under 9.2.2 (after doing a create_db to build the new database, 
	but skip the table creation second step since the restore will recreate all tables from the backup).</li>
	<li>Upgraded to PostgreSQL JDBC Driver 9.2 Build 1002.</li>
	<li>Database updates are required, and requires a rundbsetup/DbSetup DB conversion.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 12.10.20 - 20 October 2012</td></tr>
<tr><td><ul>
	<li>Removed the &quot;Party completed page&quot; event from the package rules. Page events are only configured in the document rules now.</li>
	<li>Page events now can run on selected page(s) rather than all pages.</li>
	<li>Fixed various issues related to the multi-page document processing.</li>
	<li>Added document and page status/mode to top and bottom of page so party knows how far along they are at any given step.</li>
	<li>Updated showSigningKeys.jsp to put the X.509 key and certificate onto its own row so the display is not so wide. Also added the
	subject distinguished name (DN) embedded in the X.509 certificate to the display.</li>
	<li>Allow a new view only package party to be created so that you can include parties who only have to view the documents to
	complete their step. This allows for such parties without having to define such pseudo-parties into documents so they can be 
	made available in the package. Of course, you can reassign the &quot;view-only&quot; nature so they can act as any pre-defined
	party in a document, too.</li>
	<li>The Set Auto-Cancel action allows an optional new retention spec. This allows a transaction to be auto-canceled and removed sooner than the standard
	retention specification defined in the transaction template. This is generally used to cleanup transactions that are started but never get beyond the initial step.</li>
	<li>Upgraded to Vaadin 6.8.5. Fixes two bugs we noted that were introduced in 6.8.3 and 6.8.4.</li>
	<li>Upgraded to CKEditor for Vaadin 1.6.7, which includes the latest CKEditor 3.6.5.</li>
	<li>Upgraded to PDFBox 1.7.1.</li>
	<li>Upgraded to libphonenumber 5.2.</li>
	<li>Database updates are required, and does not require a rundbsetup/DbSetup DB conversion.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 12.9.29 - September 29, 2012</td></tr>
<tr><td><ul>
	<li>Every signature key now has a self-signed X.509 certificate associated with it.</li>
	<li>All PDF exports are now digitally signed using the X.509 self-signed certificate.</li>
	<li>XML digital signature now includes the X.509 self-sign certificate.</li>
	<li>XML digital signature now supports XAdES-BES as specified in 
		<a href="http://uri.etsi.org/01903/v1.4.1/ts_101903v010401p.pdf" target="_blank">ETSI TS 101 903 v1.4.1 XML Advanced Electronic Signatures</a> 
		by including the QualifyingProperties, SignedProperties, SignedSignatureProperties, SigningTime, SignedDataObjectProperties and DataObjectFormat elements, 
		as well as signing the ds:KeyInfo element.</li>
	<li>Updated showSigningKeys.jsp to include the X.509 certificate in its display for each key.</li>
	<li>Allow a package party to have a &quot;not completed link/URL&quot; and a &quot;completed link/URL&quot; 
	specification. If the &quot;not completed link/URL&quot; is specified and the party is on the package page
	and hasn't yet completed the work, a button will be shown to allow them to say they do not intend to complete
	the package and it will redirect them to the specified URL.  If the &quot;completed link/URL&quot; is specified
	the party will be redirected to the specified URL when all documents are completed in the package (instead of
	showing them the package page again).</li>
	<li>Created a new &quot;ESF_Buttons&quot; property set with various buttons labels and footers that can be 
	defined. This is a preliminary implementation so we do not recommend making lots of customizations. The button labels to use are
	found using this basic search algorithm starting with the transaction's branding library, then the document's library and finally the template library: 
		<ol>
			<li>Check for a propertyset named ESF_Buttons_DocumentEsfName_DocPartyEsfName (Will use DocPartyEsfName &quot;ViewOnly&quot; if a view only party to the document.)</li>
			<li>Check for a propertyset named ESF_Buttons_DocumentEsfName</li>
			<li>Check for a propertyset named ESF_Buttons_PickupPartyEsfName (package party EsfName)</li>
			<li>Lastly, check for a propertyset named ESF_Buttons (one should always be found in the template library)</li>
		</ol>
	</li>
	<li>Added a button to the package page that allows a completed party to download all documents as a digitally signed PDF. Enables the party to have a copy of their documents.</li>
	<li>Fixed generated HTML documents and most JSPs to include a meta tag to instruct robots (search engines) to not index or follow links.
	It's no guarantee, but helps if a user mistakenly posts a document in a public area.</li>
	<li>Put a max number of requests for the forgotten password feature to avoid fraudulent requests that will cause an email to be sent to the user's email address, 
	or when a user who is not retrieving the link emailed from the previous requests. If the max is reached, it will allow for another attempt after 5 minutes, and will be reset entirely if the
	password is successfully reset.</li>
	<li>Made the Electronic Signature Process Record a bit more compact.</li>
	<li>Added BouncyCastle PKIX/CMS 1.47 JAR for being able to do PDF digital signatures using X.509 certificates.</li>
	<li>Upgraded to Vaadin 6.8.3.</li>
	<li>Upgraded to JDOM 2.0.3.</li>
	<li>Upgraded to ConfirmDialog 1.2.1.</li>
	<li>Upgraded to Apache POI 3.8-20120326.</li>
	<li>Database updates are required, and requires a rundbsetup/DbSetup DB conversion.</li>
	<li>Renamed the Linux command to run when converting HTML to PDF from wkhtmltopdf-i386 to just wkhtmltopdf (it remains wkhtmltopdf.exe on Windows). Please be sure to rename the tool in your bin 
	subdirectory (or wherever it is so long as it's in your PATH when starting Tomcat) or use a softlink. This removes the i386 architecture specification we used though 64-bit versions
	are fine if they work for you.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 12.8.25 - August 25, 2012</td></tr>
<tr><td><ul>
	<li>Allow the PDF attachment file name to be optionally specified, as a fixed name or mixed with field/properties values, when attaching documents to an email in the Send Email Action.</li>
	<li>Allow a Document file name to be optionally specified, as a fixed name or mixed with field/property values, so when that document is downloaded, it will use that file name.</li>
	<li>Added libphonenumber 5.0 to support flexible international phone numbers. This has replaced our prior "USA/Canada" phone number. If you used any non-standard phone formats
	for your fields, you will want to revisit them to use these more standard display formats and specify the correct default country.</li>
	<li>Related to the phone number changes, added a default country code to the deployment properties. If you are not in the USA, you will want to set the correct default to use.</li>
	<li>While the signer's IP address has always been a part of the electronic signature process record appended to the document, now enable
	the XML digital signature seal to optionally include the signer's IP address/hostname and/or browser type (user-agent) without regard to the contents of what's being signed.</li>
	<li>Allow a document version to specify whether the electronic signature process record will print on a page by itself (current default) or whether it should just appear
	at the bottom of the last page of the document.</li>
	<li>Added styling to the electronic signature process record, as well as internationalized the verbiage.</li>
	<li>Added jBCrypt 0.3 to make offline attacks on password hashes much more expensive to crack should the password data ever be stolen. On our desktop PC, 100 SHA-512 hashes
	could be generated in 7 msecs, whereas with a salt value of 11 on jBCrypt, 100 hashes took 26032 msecs. This will slow down an offline attack by a factor of 3718. The salt value
	can double every 18 months or so (just add 1 to the jBCrypt.gensalt.iterations property) to keep up with ever faster cracking processors.</li>
	<li>Updated the sample XML digital signature verification code (XmlDigitalSignatureSimpleVerifier) to include a main method so it can be run
	as a Java program that takes one or or file names to check. The files can be either a snapshots file that contains one or more snapshot elements,
	or it can just be a standalone snapshot element.</li>
	<li>Added a Change Password feature under the Access Control menu item so a user can easily change his/her password. 
	Previously, passwords could only be reset via the User admin screen.</li>
	<li>Created new showSigningKeys.jsp page that will list all of the signature public keys for your deployment. This essentially lets the world
	know what your public keys are, formatted both in the XML Digital Signature RSAKeyValue Exponent and Modulus values (this will match the
	values seen in the Snapshot XML files created as each party completes a document) as well as the X.509 standard format.</li>
	<li>Upgraded to Vaadin 6.8.2.</li>
	<li>Upgraded to Apache Tomcat 7.0.29.</li>
	<li>Database updates are required, and requires a rundbsetup/DbSetup DB conversion.</li>
	<li>For existing deployments, add the following to your openesignforms.properties file:<pre><code># For jBCrypt salt generation. Generally speaking, if processing power doubles every 18 months, you can add one to this
# number every 18 months. Because the hash value contains the number used, there's no issue with respect to previous
# passwords as this number increases. In 2010, the default value was 10. So by 2012, we'll just start with 11.
jBCrypt.gensalt.iterations=11</code></pre>
	</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 12.8.4 - August 4, 2012</td></tr>
<tr><td><ul>
	<li>Added multi-page documents for those who prefer a paged-experience to a scrolling document experience. Related, added new Party Completed Page event to add
	hooks when a page is submitted, including page-level validations.</li>
	<li>Added ability to calculate a monthly payment based on the amount financed (total amount), APR (interest rate) and number of monthly payments (term in months).</li>
	<li>Created action to submit credit card information to the PayPal DoDirectPayment "Sale" method of the PayPal NVP API version 92.0. Can be used by
	PayPal &quot;Payments Pro&quot; (Web accept payments) customer. Most will register for the NVP API credentials using. Test transactions use the PayPal Sandbox while production transactions
	will use your configured PayPal API credentials for real credit card charges.</li>
	<li>Added country name to two-letter ISO-3166-1 codes drop down list to the standard template library.</li>
	<li>Allow MM/YYYY date selection using the data selector tool. The actual date will be the last day of the selected month and year. Useful for credit card expirations
	and some human resource needs where job start/end dates just require a month and year.</li>
	<li>Changes to the retention values for a transaction template will now automatically update existing transactions to
	use the new value. This type of change is logged to the user and system logs to record the retention change made.</li>
	<li>Fixed XSS bug in the login page's 'saveEmail' checkbox found by the Tinfoil Security beta vulnerability scanning service. Oops! That is a bit embarrassing...</li>
	<li>Upgraded to CKEditor for Vaadin 1.6.6. Documents can now contain JSP code and tags when entered in SOURCE mode.</li>
	<li>Upgraded to Vaadin 6.8.1.</li>
	<li>Database updates are required, and requires a rundbsetup/DbSetup DB conversion.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 12.7.7 - July 7, 2012</td></tr>
<tr><td><ul>
	<li>New serial number generators for generating unique numbers, like order numbers, customer numbers, etc. referenced as a field specification with the 'serial' prefix.</li>
	<li>Added new DateTime field type. This is not generally used for input mode by a party filling out a form but is used to capture the current date-time and possibly add/subtract days from it.</li>
	<li>Added new date and date-time formats to the ESF/Library/Template drop downs to allow for numbers-only formats.</li>
	<li>Added ability to update report field templates from both the report template's &quot;Fields in report&quot; and the package's &quot;Map report fields&quot; screens.</li>
	<li>Added ability to activate a party out of normal sequential order. It is most useful when you have multiple notification parties at the end of
	a process flow who can all be activated together.</li>
	<li>Added a refresh button to the report and general transaction search details screen.</li>
	<li>Allow the HTML email text to contain $ {image:ImageName} field specifications. Note that images may not be shown unless the recipient's email client allows for remote images to be loaded or the
	user clicks to show images. Like before, you can still use the CKEditor's image insertion tool.</li>
	<li>Fixed transaction and report searches against party email addresses.</li>
	<li>When a condition or action is being configured, if has no specifications, put the user immediately into creating a new specification.</li>
	<li>Allow a condition to be negated from its main edit view. For conditions with multiple specifications, this is done above
	the list of specifications. Otherwise it appears above the single specification form.</li>
	<li>Expanded Comparison Condition operators (less than equals, greater than equals, same length, shorter or same length, longer or same length) and allow 
	appropriate ones (equals, less than, greater than, less than equals to, greater than equals to) to operate on integers and decimal numbers including money values.</li>
	<li>Upgraded to Vaadin 6.8.0.</li>
	<li>Upgraded to JDOM 2.0.2.</li>
	<li>Upgraded to CKEditor for Vaadin 1.6.5.</li>
	<li>Database updates are required, and requires a rundbsetup/DbSetup DB conversion.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 12.6.2 - June 2, 2012</td></tr>
<tr><td><ul>
	<li>On 17 May 2012, the U.S. Patent Office re-confirmed the validity of all 45 claims of our <a href="http://www.yozons.com/patents.jsp">U.S. Patent No. 7,360,079</a> 
	with no amendments.</li>
	<li>Fixed the default ENTER key problem with forms that contained CKEditor and/or TextArea fields (like for comments)
	where hitting the ENTER key should have just made a new line rather than savings the contents.</li>
	<li>Allow transaction templates to have a Display Name used for showing this transaction's name in the menus.</li>
	<li>Allow report templates to have a Display Name used for showing the report's name in the menus.</li>
	<li>Changed the 'Cancel transaction' action to allow for a future date to be set for auto-cancellation.</li>
	<li>Added 'Clear cancel transaction' action to turn off auto-cancellation, such as when the transaction reaches a processing step
	within a given amount of time and now can progress or complete normally.</li>
	<li>Added 'Skip document' to the change status action to remove a document from the processing flow for all parties.</li>
	<li>Added 'Now' to time intervals drop down list.</li>
	<li>Added a comparison condition to test if a field has a specific value or matches another field's value. It supports
	equals, starts with, ends with, and contains comparisons. Case-sensitive tests are supported.</li>
	<li>Allow custom programming to be applied to a document. This is for logic that always makes sense for the document, regardless of the package it's in.</li>
	<li>Added Show Error (or other message) action to display a field validation error using custom logic to validate it.</li>
	<li>Allow Conditions on Actions to be re-ordered using drag and drop.</li>
	<li>Added ConfirmDialog to the property set delete button to avoid confusion with removing selected properties with deleting the entire set. Also added
	to the Package Version, Transaction Template, Report Template and all library configuration components.</li>
	<li>Added ability to start a new transaction like an existing transaction, from both the general transaction and report details view.</li>
	<li>Added tooltips to all menu items.</li>
	<li>Allow package process rules (custom logic) to be re-ordered.</li>
	<li>Field specifications like <code>${DocumentName.fieldName}</code> retrieve a simple display version of the value of 'fieldName' in document 'DocumentName'.
	Added the ability with the 'out:' prefix before the document name to instruct the returned value to use the output format specified in the corresponding field template instead.
	This is particularly useful in things like Email Templates when you want to show the selected label that goes with a drop down's value.</li>
	<li>Upgraded to BouncyCastle 1.47.</li>
	<li>Upgraded to Vaadin 6.7.9.</li>
	<li>Database updates are required, and requires a rundbsetup/DbSetup DB conversion.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 12.5.5 - May 5, 2012</td></tr>
<tr><td><ul>
	<li>Allow PDF generation to be in landscape form when the corresponding document is defined as such. If multiple documents are merged, then it 
	will be landscape if any of those documents is defined as landscape, so mixing landscape and portrait documents will generally not lead to optimal results.</li>
	<li>Added ability to upload files into a library or a document, similar to how images are done, so they can be referenced via links in documents.</li>
	<li>Added ability to add actions when a transaction is canceled.</li>
	<li>Added ability to cancel a transaction and skip a party during the process flow.</li>
	<li>Upgraded to CKEditor for Vaadin 1.6.3 which includes CKEDitor 3.6.3, and ability to specify file links from within the CKEditor link feature.</li>
	<li>Upgraded to JDOM 2.0.1.</li>
	<li>Upgraded to Vaadin 6.7.8.</li>
	<li>Upgraded to Animator 1.6.6.</li>
	<li>Updated all to use annotations and removed Open eSignForms filter/filter-mapping/listener/servlet/servlet-mapping from the web.xml. 
	This makes it easier going forward to include new filter/listener/servlet without needing to update the otherwise deployment-customized web.xml files.</li>
	<li>Ran <a href="http://findbugs.sourceforge.net">FindBugs</a> in Eclipse to create cleaner, more reliable code.</li>
	<li>Because we allow for deployment customization of drop downs, style sheets, etc., we did not force the following changes through a DB update.
	But if you want to stay close to the base, we recommend you consider updating your ESF/Library/Template:
		<ul>
			<li>DropDown <code>ESF_Font</code>: Rename <code>New Century Schoolbook</code> to <code>Bradley, Lucida Handwriting</code> and change its value to <code>font-family: 'Bradley Hand ITC', 'Lucida Handwriting', 'Comic Sans MS', cursive;</code></li>
			<li>DropDown <code>ESF_Font</code>: Rename <code>Comic Sans</code> to <code>Comic Sans MS</code> and change its value to <code>font-family: 'Comic Sans MS', cursive;</code></li>
			<li>Document style <code>ESF_DefaultDocumentStyle</code> for 'Signatures': select <code>Bradley, Lucida Handwriting</code>.</li>
		</ul>
	</li>
	<li>Database updates are required, but no DB conversion.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 12.4.14 - April 14, 2012</td></tr>
<tr><td><ul>
	<li>Added ability to specify conditions that must be met before a package's action takes place. 
	Any condition can be negated to mean NOT whatever it's condition test results in.
	Initial set of Conditions include: AND, OR, All Blank (all specified fields have no value), Any Blank (at least one field specified has no value), and
	Some But Not All Blank (at least one field specified is blank and at least other has a value -- false if you specify fewer than two fields to check). 
	Note that 'NOT All Blank' is equivalent to 'Any Non-Blank' (at least one field has a value), 
	and 'NOT Any Blank' is equivalent to 'All Non-Blank' (all fields have a value)</li>
	<li>Added ability to access any attached files included in an outbound email from the Email Log detail view.</li>
	<li>Fixed outbound emails to specify charset=UTF-8 in the subject, text and html body parts.</li>
	<li>Made buttons stand out more in the main application, as well as in the document process flow (thanks to Leslie Strom for this good idea).</li>
	<li>Upgraded to Vaadin 6.7.6.</li>
	<li>Upgraded to Vaadin Add-on TableExport 1.3.0.</li>
	<li>No database update/conversion is required.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 12.3.2 - March 2, 2012</td></tr>
<tr><td><ul>
	<li>Provide direct access to any file(s) uploaded into the transaction via the general transaction search and report details.</li>
	<li>Add ability to attach any file(s) uploaded into the transaction to an email.</li>
	<li>Allow File Upload fields (that support uploading files into your document) to be included in Reports and To Do listings as a
	report field (requires DB schema change).</li>
	<li>For documents with File Upload fields, we now auto-scroll the page to this field on Upload/Remove so the user's focus remain
	in the same area rather than having to scroll from the top.</li>
	<li>Completed the new online <a href="http://code.google.com/p/openesignforms/wiki/ProgrammingGuide">Programming Guide</a>.
	All guides should be complete now, but of course will be updated to reflect user questions, tips, new features, etc.</li>
	<li>Added new (Default) value to various style drop downs that basically emit no CSS. This replaces the previous default of 'inherit',
	which was not as useful because it took the style from the parent element rather than the system defaults.
	Previously new document styles had to redefine all styles rather than just override the default values.</li>
	<li>Allow Integer fields to be specified to show the number in 'ordinal' format, like 1st, 2nd, 3rd, 4th.</li>
	<li>Allow a field label to be suppressed when the field is not in EDIT mode, mostly used for fields defined inside a paragraph of text
	so that the label appears for the person entering the data, but then is not shown thereafter.</li>
	<li>Allow a field template to have a value to show if it's field value is blank.</li>
	<li>Upgraded to CKEditor for Vaadin 1.6.1. Special thanks to sascha.broich76 for his code contribution. Added font Calibri to the 
	editor's built-in Fonts.</li>
	<li>Upgraded to JDOM 1.1.3.</li>
	<li>Database updates are required, and requires a rundbsetup/DbSetup DB conversion.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 12.2.10 - February 10, 2012</td></tr>
<tr><td><ul>
	<li>Added ability to remotely start new transactions on behalf of a user (previously, only external user starts were supported).</li>
	<li>Added API ability to remotely start new transactions and complete the first party step and receive an OK or ERROR response.</li>
	<li>Changed web.xml and our cookie generators to specify HttpOnly cookies, which requires the use Servlet 3.0 APIs in Tomcat 7.</li>
	<li>Created a new online <a href="http://code.google.com/p/openesignforms/wiki/HowToGuides">Concepts, Tips &amp; Tricks Guide</a>.</li>
	<li>Created a new online <a href="http://code.google.com/p/openesignforms/wiki/UserGuide">User's Guide</a>.</li>
	<li>Created a new online <a href="http://code.google.com/p/openesignforms/wiki/SystemAdminGuide">System Administrator's Guide</a>.</li>
	<li>Created a new online <a href="http://code.google.com/p/openesignforms/wiki/ProgrammingGuide">Programming Guide</a>.</li>
	<li>Created a new online <a href="http://code.google.com/p/openesignforms/wiki/ProgrammingTutorial">Programming Tutorial</a>.</li>
	<li>Upgraded to Vaadin 6.7.5.</li>
	<li>No database update/conversion is required.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 12.1.12 - January 12, 2012</td></tr>
<tr><td><ul>
	<li>Added a calculate action to do basic arithmetic with integers, decimals and money values.</li>
	<li>Added &quot;party review document&quot; event to do special processing when the party goes from EDIT to REVIEW mode.</li>
	<li>Allow the retrieval of subscripted transaction data, for example sampleName[4], to retrieve the 4th 
	element in the array of params named 'sampleName' when the transaction was started 
	(i.e. sampleName=First&amp;sampleName=Second&amp;sampleName=Third&amp;sampleName=Fourth).</li>
	<li>Added ability to reload report fields from existing transactions. This is mostly used when new fields are defined, 
	so they appear blank in reports for those transactions last updated before the new report fields were defined.</li>
	<li>On startup, regenerate all Document Style (CSS) and DocumentVersion (JSP) files defined in all libraries.</li>
	<li>Ensure JSP/Servlet uses UTF-8 encoding to support input fields in forms having non-western characters.</li>
	<li>Added party email filtering to the general transaction search view.</li>
	<li>Upgraded to Vaadin 6.7.4.</li>
	<li>No database update/conversion is required.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 11.12.9 - December 9, 2011</td></tr>
<tr><td><ul>
	<li>Added ability to send emails at any step of the work flow. Previously, emails were optionally sent only as each party's step in the workflow was activated.
	Email actions allow emails to be sent with specified document snapshots attached, or merged together as a PDF attachment.</li>
	<li>Fixed the Report Template 'create like' so if a user has permission to create like a given template, but doesn't have permission to list all of the
	transaction templates specified in the original report, then those extra transaction templates are not included in the new report template.</li>
	<li>Added HTML to PDF generation, which is now used by general transaction search as well as custom reports when viewing a document snapshot &quot;as PDF&quot;.</li>
	<li>Added ability to download selected snapshots as a combined PDF in report details as well as the general search details.</li>
	<li>Installed wkhtmltopdf 0.11.0rc1 for HTML to PDF generation.</li>
	<li>Update the document editor as well as the Tips editors to allow CKEditor to adjust its height to fill the page.</li>
	<li>Upgraded to Vaadin 6.7.2.</li>
	<li>Upgraded to CKEditor for Vaadin 1.6.0. Special thanks to Stefan Meißner, davengo GmbH, for his code contributions.</li>
	<li>Upgraded to TableExport 1.2.9.</li>
	<li>Upgraded to Apache log4j 1.2.16.</li>
	<li>Database updates are required, but no DB conversion.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 11.11.11 - November 11, 2011</td></tr>
<tr><td><ul>
	<li>Added the ability to export the configuration of library objects documents, document styles, drop down lists, email templates, images/logos and property sets
	to an XML file suitable for transferring to another library, possibly in another deployment. This is the first step for the Forms Store.</li>
	<li>Added the ability to import the configuration of library objects documents, document styles, drop down lists, email templates, images/logos and property sets
	from an XML file previously exported from another library, possibly in another deployment.</li>
	<li>Several bug fixes related to both User and Group caches.</li>
	<li>Upgraded to JDOM 1.1.2.</li>
	<li>No database updates are required, but requires a rundbsetup/DbSetup DB conversion.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 11.10.14 - October 14, 2011</td></tr>
<tr><td><ul>
	<li>Added ability to define and run custom reports, pulling any data required from any documents defined in any transactions/packages.</li>
	<li>Added ability to export report data to an Excel or CSV file using add-on TableExport 1.2.6, which itself relies on Apache POI 3.8.</li>
	<li>Renamed the original 'Search/Reports' menu item to 'General tran search' to reflect it's general nature compared to reports that are customized.</li>
	<li>Moved the To Do menu item to the top position for easy, consistent access.</li>
	<li>Upgraded to Vaadin 6.7.1. This obsoletes add-on JARs for ChameleonTheme and TreeTable.</li>
	<li>Upgraded to CKEditor for Vaadin 1.4.3.</li>
	<li>Upgraded to Apache Commons IO 2.1.</li>
	<li>Upgraded to Apache Fileupload 1.2.2.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 11.9.10 - September 10, 2011</td></tr>
<tr><td><ul>
	<li>Added ability to specify those fields in a package of documents that will be used in custom reports (next release) or To Do listings.</li>
	<li>On transaction save, data fields for reports/To Do are stored for indexed access.</li>
	<li>Upgraded the To Do list to show custom report fields specified for it. Since report fields are defined in the package version, a To Do list is created
	for all transactions by package version.</li>
	<li>Upgraded to Vaadin 6.6.6.</li>
	<li>Upgraded to PopupButton 1.2.1.</li>
	<li>Upgraded to Apache Tomcat 7.0.20.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 11.8.9 - August 9, 2011</td></tr>
<tr><td><ul>
	<li>Celebrating Yozons' 11th birthday today with this release. Yozons was incorporated on August 9, 2000. 
	It feels like the stock market is at about the same price point, too!</li>
	<li>Allow a field to be specified to as required, yet still be optional for certain parties. This use case appears often when
	one party pre-fills a form for another and the fields should be required for a completed document, but must remain optional for the pre-fill step.</li>
	<li>Added ability to apply a full, left-side or right-side mask on general fields when in display mode, for things best not shown in 
	the digitally signed snapshots. These were already present in known field types like SSN and credit card numbers.</li>
	<li>Allow sensitive information to be masked in the data snapshots, such as for a credit card security verification code that otherwise shouldn't be stored 
	longer than necessary to process it. Of course, all data is encrypted all of the time, but once processed, the live data can be blanked out or masked,
	but now even the digitally signed snapshots of the data will not contain such (rare) data.</li>
	<li>Allow a field to be specified to allow or disallow cloning its value when a new transaction is created like an existing transaction.</li>
	<li>Link to the version history (this page) from the main window's footer where the version number is displayed.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 11.8.1 - August 1, 2011</td></tr>
<tr><td><ul>
	<li>Added ability to define custom logic to alter the straight-through default workflow of process each party
	in order, with each party processing the documents assigned to them in order. Added the ability to set
	document field values, with more actions coming soon that use the framework developed for this.</li>
	<li>Use logo and company name from the template library's property set MyCompany on the login page.</li>
	<li>Upgraded to URL Rewrite 3.2.0.</li>
	<li>Upgraded to Vaadin 6.6.3.</li>
	<li>Switched to a date-based (y.m.d) version numbering scheme for Open eSignForms.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 1.6 - July 12, 2011</td></tr>
<tr><td><ul>
	<li>Allow a package party to delete the transaction from the package document if no signatures have been applied on any document by any party, 
	which is generally useful for the first party who starts a new transaction but decides not to submit it for further processing.</li>
	<li>Allow an inactive party's email to be changed, and if active, to transfer the party to a new email address while invalidating access using the link sent previously.</li>
	<li>Allow a party to be renotified by sending the email again.</li>
	<li>Upgraded to PopupButton 1.2.0.</li>
	<li>Upgraded to Chameleon Theme 1.0.2.</li>
	<li>Upgraded to TreeTable 1.2.2.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 1.5 - June 23, 2011</td></tr>
<tr><td><ul>
	<li>Added credit card field type, including masking for display.</li>
	<li>Allow the retention of various log records to be specified in the deployment configuration.</li>
	<li>Daily statistics now include the number of transactions in the database.</li>
	<li>Upgraded to Vaadin 6.6.2.</li>
	<li>Upgraded to CKEditor for Vaadin 1.4.1.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 1.4 - June 17, 2011</td></tr>
<tr><td><ul>
	<li>Added ability to set the license type and size for a deployment: Open Source License (AGPL) or Commercial License (DBSIZE or NUMUSERS).</li>
	<li>Added ability to set the default session timeout as well as a different value for logged in users.</li>
	<li>Added a system status check hook for 24x7 monitoring, such as with Pingdom.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 1.3 - June 14, 2011</td></tr>
<tr><td><ul>
	<li>Added link to built-in 'esf.css' file from the document builder for easy reference.</li>
	<li>Added transaction data fields for the creator's email address, name, etc. if they are logged in. They are described in the document programming tips page,
	along with new links to advanced programming concepts for decimal formats, date formats and regular expression patterns.</li>
	<li>Added auto-complete option to fields. If off, autocomplete="off" is added to the input field. This is useful for 
	sensitive fields, like SSN, credit card info, etc. where you don't want the browser to cache answers.</li>
	<li>When a signature field's value is saved, we automatically generate and save related data elements that include the timestamp and IP address.</li>
	<li>Upgraded to CKEditor for Vaadin 1.4.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 1.2 - June 8, 2011</td></tr>
<tr><td><ul>
	<li>When editing a document, can select a party to auto-assign any auto-created fields so the party can update them.</li>
	<li>When a new document is created, a &quot;first party&quot; is automatically added.</li>
	<li>Can set the landing page for a package party (package+disclosures, first document, first to do or first document if nothing to do, or first to do or package if nothing to do) to 
	control which document is first seen when a given party accesses the package.</li>
	<li>Created Radio Button Group field type to hold the value of its radio buttons. Radio buttons configuration allows for a drop down list of available radio button groups to belong to.
	The radio button group holds the data value of the select radio button and is specified for party update or not. The radio button group 'reference count' is the number of its radio buttons.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 1.1 - June 3, 2011</td></tr>
<tr><td><ul>
	<li>Changed Integer, Decimal and Money fields to use drop down lists so their display formats are fully customizable.</li>
	<li>Added radio button fields.</li>
	<li>Added rich text editor (HTML Editor) fields.</li>
	<li>Upgraded to Vaadin 6.6.1.</li>
	<li>Upgraded to CKEditor for Vaadin 1.3 which includes CKEditor 3.6.</li>
	<li>Fixed Date field so that the calendar icon stays with the text input field when in edit mode.</li>
	<li>Various bug fixes and minor enhancements.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 1.0 - June 1, 2011</td></tr>
<tr><td><ul>
	<li>Initial commercial release.</li>
	<li>User and group management, including seeing who is logged in and who is using the system as an external party.</li>
	<li>Library support grouping related documents as well as for providing a branding opportunity for multi-company deployments.</li>
	<li>Transaction report and To Do listings.</li>
	<li>Package configuration to allow one or more documents to be processed as a package of forms.</li>
	<li>Transaction configuration to specify who can start and cancel transactions, which package will be used, and to specify a branding library.</li>
	<li>Document builder.</li>
	<li>Document style editor.</li>
	<li>Drop down editor for select boxes.</li>
	<li>Email notification editor.</li>
	<li>Image/logo upload.</li>
	<li>Property set editor to allow for easy externalization of configurable elements, like company name, address, phone, etc.</li>
	<li>Field configuration including general data input, checkbox, date, decimal numbers, email address, file upload/download, integer numbers,
		money, phone numbers, drop down lists, signature, signature date, SSN, text area and zip code.</li>
	<li>Party configuration including associating a group of users who may all be allowed to process a given type of transaction in that role.</li>
	<li>XML Digital Signature snapshots of the data and documents on each processing step (as each party submits a document as completed).</li>
	<li>Integrated email notifications so that bounces and replies are automatically correlated and available for review online.</li>
	<li>Basic transaction flow from the first party through the last party, with each party seeing the document(s) available to them in order.</li>
	<li>System configuration elements, statistics, system activity logs and the ability to update various &quot;tips&quot; pages.</li>
</ul></td></tr>

<tr style="margin-top: 1em;"><td class="boB flarge bold">Version 0.0 through 0.9 - June 2009 through May 2011</td></tr>
<tr><td><ul>
	<li>In May 2008, we inquired with the <a href="http://www.fsf.org">Free Software Foundation</a> (FSF) regarding possible interest in building an open source product and 
	how the various licenses worked. We settled on the <a href="http://www.gnu.org/licenses/agpl.html">GNU Affero GPL</a> (AGPL) to ensure the code always remains open and free.</li>
	<li>The <a href="http://open.esignforms.com">Open eSignForms project</a> was launched in June 2009, with the first code base established on July 6, 2009. We also activated our <a href="http://twitter.com/OpenESignForms">Twitter @OpenESignForms</a> then.</li>
	<li>Early research of various Web 2.0 technologies that we evaluated included the Dojo, EXT-JS and YUI javascript libraries in September 2009.</li>
	<li>Next we evaluated Google Web Toolkit (GWT), EXT-GWT and SmartGWT in November 2009, including building some initial pages using GWT.</li>
	<li>We finally settled on <a href="http://www.vaadin.com">Vaadin</a> in January 2010. Coding on Vaadin+GWT began in earnest in May 2010.</li>
	<li>Added <a href="http://ckeditor.com">CKEditor</a> in July 2010.</li>
	<li>Added email sending and bounce processing, 4096-bit RSA keypairs and RSA+SHA-512 XML Digital Signatures in April 2011.</li>
	<li>Released the 0.9 BETA to partner developers in May 2011.</li>
	<li>Created our <a href="http://www.facebook.com/eSignForms">Facebook page</a> in May 2011.</li>
</ul></td></tr>

</table>

<p>** For third-party licenses used by Open eSignForms, please see <a href="http://open.esignforms.com/thirdPartySoftware.jsp">http://open.esignforms.com/thirdPartySoftware.jsp</a></p>

<p>Open eSignForms is not suitable for military use, or for any high-security, high-risk, life-or-death purposes.</p>

<p>U.S. Patent 7,360,079</p>

<p style="margin-left: 4em;"><jsp:include page="./footer.jsp" flush="true" /></p>

</body>
</html>
<%!
public class BeanData extends com.esignforms.open.jsp.PageBean {
	public BeanData() { }

	public PageProcessing doGet()
	throws java.io.IOException
	{
		return PageProcessing.CONTINUE;
	}

	public PageProcessing doPost()
	throws java.io.IOException
	{
		return doGet();
	}
}
%>
