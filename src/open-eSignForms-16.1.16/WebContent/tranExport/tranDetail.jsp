<!DOCTYPE html>
<%@page contentType="text/html; charset=UTF-8" import="com.esignforms.open.prog.ReportTemplate
													  ,com.esignforms.open.runtime.Transaction
													  ,com.esignforms.open.runtime.TransactionDocument
													  ,com.esignforms.open.runtime.TransactionFile
													  ,com.esignforms.open.runtime.TransactionParty
													  ,com.esignforms.open.runtime.TransactionPartyAssignment
													  ,com.esignforms.open.runtime.TransactionPartyDocument
													  ,com.esignforms.open.user.User
													  ,com.esignforms.open.vaadin.config.PrettyCode
													  "%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(session,request,response);
if ( bean.doGet() == BeanData.PageProcessing.DONE )
	return;
%>
<html>
<head>
    <meta charset="UTF-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - Exported transaction details</title>
	<meta name="Description" content="Shows the exported transaction details for Open eSignForms - Version 12/9/2015"/>
	<meta name="robots" content="noindex, nofollow" />
    <style type="text/css">
 		<jsp:include page="../static/esf/esf.css" flush="true" />
 		.padH { padding-left: 5px; padding-right: 5px; }
 		.caution { color: #FF8000; }
	</style>
</head>

<body class="esf" style="background-color: #f5f5f5;">
<% if ( bean.errors.hasErrorOrWarning() ) { %>
<%=bean.errors.toHtml()%>
<% } else { %>

<table class="cellpadding5 cellspacing5">
<tr>
	<td class="nowrap">
		<b><% if (bean.transaction.isProduction()){%>Production<%}else{%>Test<%}%> Transaction</b><br/>
		<%=bean.transaction.getTransactionTemplate().getPathName().toHtml()%>
	</td>
	<td class="nowrap">
		<b>Package</b><br/>
		<esf:out value="<%=bean.transaction.getPackageVersion().getPackagePathNameVersionWithLabel()%>"/>
	</td>
	<td class="nowrap">
		<b>Brand library</b><br/>
		<%=bean.transaction.getBrandLibrary().getPathName().toHtml()%>
	</td>
	<td class="nowrap">
		<b>Id</b><br/>
		<%=bean.transaction.getId().toHtml()%>
	</td>
</tr>
<tr>
	<td class="nowrap">
		<b>Status</b><br/>
		<span class="<%=bean.getTranStatusClass(bean.transaction.getStatus())%>"><esf:out value="<%=bean.transaction.getStatusLabel()%>"/></span> [<esf:out value="<%=bean.transaction.getStatusText()%>"/>]
	</td>
	<td class="nowrap">
		<b>Expires</b><br/>
		<%if (bean.transaction.hasExpireTimestamp()){%><esf:out value="<%=bean.transaction.getExpireTimestamp().toLogString(bean.getLoggedInUser())%>"/><%}else{%>Never<%}%>
	</td>
	<td class="nowrap">
		<b>Auto-cancels</b><br/>
		<%if (bean.transaction.hasCancelTimestamp()){%><esf:out value="<%=bean.transaction.getCancelTimestamp().toLogString(bean.getLoggedInUser())%>"/><%}else{%>Never<%}%>
	</td>
	<td class="nowrap">
		<b>Stalled</b><br/>
		<%if (bean.transaction.isStalled()){%><span class="caution"><esf:out value="<%=bean.transaction.getStallTimestamp().toLogString(bean.getLoggedInUser())%>"/></span><%}else{%>No<%}%>
	</td>
</tr>
<tr>
	<td class="nowrap">
		<b>Started</b><br/>
		<esf:out value="<%=bean.transaction.getCreatedTimestamp().toLogString(bean.getLoggedInUser())%>"/>
	</td>
	<td class="nowrap">
		<b>Started by</b><br/>
		<%if (bean.transaction.hasCreatedByUserId()){%><%if(bean.startedByUser==null){%>???<%}else{%><esf:out value="<%=bean.startedByUser.getFullDisplayName()%>"/><%}%><%}else{%>&nbsp;<%}%>
	</td>
	<td class="nowrap">
		<b>Last updated</b><br/>
		<esf:out value="<%=bean.transaction.getLastUpdatedTimestamp().toLogString(bean.getLoggedInUser())%>"/>
	</td>
	<td class="nowrap">
		<b>Last updated by</b><br/>
		<%if (bean.transaction.hasLastUpdatedByUserId()){%><%if(bean.startedByUser==null){%>???<%}else{%><esf:out value="<%=bean.startedByUser.getFullDisplayName()%>"/><%}%><%}else{%>&nbsp;<%}%>
	</td>
</tr>
<tr>
	<td class="nowrap">
		<% if (bean.reportTemplate.hasViewSnapshotDataPermission(bean.getLoggedInUser())){%>
		<a href="tranData.xml" title="See the transaction data in XML">Transaction data</a>
		<%}else{%>
			&nbsp;
		<%}%>
	</td>
	<td class="nowrap">
		<% if (bean.reportTemplate.hasViewActivityLogPermission(bean.getLoggedInUser())){%>
		<a href="activityLog.html" title="See the activity log">Activity log</a>
		<%}else{%>
			&nbsp;
		<%}%>
	</td>
	<td class="nowrap">
		<% if (bean.reportTemplate.hasViewEmailLogPermission(bean.getLoggedInUser())){%>
		<a href="emailLog.html" title="See the email log">Email log</a>
		<%}else{%>
			&nbsp;
		<%}%>
	</td>
	<td class="nowrap">&nbsp;</td>
</tr>
</table>

<table class="box cellpadding5 cellspacing5">
<tr>
	<th class="left nowrap">Party</th>
	<th class="left nowrap">Status</th>
	<th class="center nowrap"># Documents</th>
	<th class="left nowrap">Email</th>
	<th class="left nowrap">User</th>
	<th class="left nowrap">Created</th>
</tr>
<% for (TransactionParty tranParty : bean.tranPartyList) { 
	java.util.List<TransactionPartyAssignment> tranPartyAssignmentList = TransactionPartyAssignment.Manager.getAllByTransactionPartyId(tranParty.getId());
    for ( TransactionPartyAssignment tranPartyAssignment : tranPartyAssignmentList ) {
%>
<tr>
	<td class="nowrap"><%=tranParty.getPackageVersionPartyTemplate().getEsfName().toHtml()%></td>
	<td class="nowrap <%=bean.getTranPartyStatusClass(tranPartyAssignment.getStatus())%>"><esf:out value='<%=tranPartyAssignment.getStatusLabel()%>'/></td>
	<td class="nowrap center"><%=tranParty.getNonSkippedTransactionPartyDocuments().size()%></td>
	<td class="nowrap"><esf:out value='<%=tranPartyAssignment.hasEmailAddress() ? tranPartyAssignment.getEmailAddress() : ""%>'/></td>
	<td class="nowrap"><esf:out value='<%=tranPartyAssignment.hasUserId() ? tranPartyAssignment.getUser().getFullDisplayName() : ""%>'/></td>
	<td class="nowrap"><%=tranPartyAssignment.getCreatedTimestamp().toHtml()%></td>
</tr>
<% } /* end for each tranPartyAssignment*/ %>
<% } /* end for each tranParty*/ %>
</table>

<% for ( TransactionDocument tranDoc : bean.tranDocList ) { %>
	<p>Document: <esf:out value="<%=tranDoc.getDocumentVersion().getDocumentNameVersionWithLabel()%>"/> 
		(id: <%=tranDoc.getId().toHtml()%>) 
		<% if (bean.reportTemplate.hasViewSnapshotDataPermission(bean.getLoggedInUser())){%>
		&nbsp; <a href="docdata-<%=tranDoc.getId().toHtml()%>.xml" title="See the document data in XML">Document data</a>
		<%}%>
	
		<% java.util.List<TransactionFile> tranFileList = bean.transaction.getAllTransactionFilesForDocument(tranDoc.getId()); 
		   if ( tranFileList.size() > 0 ) {
		%>
		<br/> &nbsp; Uploaded file(s): 
			<% for ( TransactionFile tranFile : tranFileList ) {
				String linkFileName = "tranfile-"+tranFile.getId()+"/"+tranFile.getFileName();
			%>
				<br/> &nbsp; &nbsp; &nbsp;<a href="<esf:out value="<%=linkFileName%>"/>"><%=tranFile.getFieldName().toHtml()%> - <esf:out value="<%=tranFile.getFileName()%>"/> (<esf:out value="<%=com.esignforms.open.data.EsfInteger.byteSizeInUnits(tranFile.getFileSize())%>"/>)</a>
			<% }/* endforeach tranFile*/ %>
		<% }/* endif tranFileList.size > 0*/ %>
	</p>
	
	<table class="box cellpadding5 cellspacing5">
	<tr>
		<th class="left nowrap">Party</th>
		<th class="left nowrap">Status</th>
		<th class="left nowrap">Document status</th>
		<th class="left nowrap">Last updated</th>
		<% if ( bean.reportTemplate.hasViewSnapshotDataPermission(bean.getLoggedInUser()) || bean.reportTemplate.hasViewSnapshotDocumentPermission(bean.getLoggedInUser()) ) { %>
		<th class="left nowrap">Snapshot taken</th>
		<th class="left nowrap">Snapshot</th>
		<% } %>
	</tr>
	<% for (TransactionParty tranParty : bean.tranPartyList) 
	   { 
		TransactionPartyDocument foundTranPartyDoc = null;
		java.util.List<TransactionPartyDocument> transactionPartyDocuments = tranParty.getNonSkippedTransactionPartyDocuments();
    	for( TransactionPartyDocument tpd : transactionPartyDocuments ) 
    	{
    		if ( tpd.getTransactionDocumentId().equals(tranDoc.getId()) ) 
    		{
    			foundTranPartyDoc = tpd;
    			break;
    		}
    	}
    	if ( foundTranPartyDoc == null ) 
    		continue;
	%>
	<tr>
		<td class="nowrap"><%=tranParty.getPackageVersionPartyTemplate().getEsfName().toHtml()%></td>
		<td class="nowrap <%=bean.getTranPartyStatusClass(tranParty.getStatus())%>"><esf:out value='<%=tranParty.getStatusLabel()%>'/></td>
		<td class="nowrap <%=bean.getTranPartyDocumentStatusClass(foundTranPartyDoc.getStatus())%>"><esf:out value='<%=foundTranPartyDoc.getStatusLabel()%>'/></td>
		<td class="nowrap"><esf:out value="<%=foundTranPartyDoc.getLastUpdatedTimestamp().toLogString(bean.getLoggedInUser())%>"/></td>
		<% if ( bean.reportTemplate.hasViewSnapshotDataPermission(bean.getLoggedInUser()) || bean.reportTemplate.hasViewSnapshotDocumentPermission(bean.getLoggedInUser()) ) { %>
		<td class="nowrap">
			<% if ( foundTranPartyDoc.hasSnapshotXmlOrBlobId() ) { %>
				<esf:out value='<%=foundTranPartyDoc.getSnapshotTimestamp().toLogString(bean.getLoggedInUser())%>'/>
			<% } else { %>&nbsp;<%}%>
		</td>
		<td class="nowrap">
			<% if ( foundTranPartyDoc.hasSnapshotXmlOrBlobId() ) { %>
				<% if ( bean.reportTemplate.hasViewSnapshotDataPermission(bean.getLoggedInUser()) ) { 
					String fileLink = "datasnapshot-"+foundTranPartyDoc.getTransactionPartyId()+"/"+foundTranPartyDoc.getTransactionDocumentId()+".xml";
				%>
					<a href="<esf:out value='<%=fileLink%>'/>">Data snapshot</a>
				<% } %>
				<% if ( bean.reportTemplate.hasViewSnapshotDocumentPermission(bean.getLoggedInUser()) ) { 
					String fileLink = "docsnapshot-"+foundTranPartyDoc.getTransactionPartyId()+"/"+foundTranPartyDoc.getTransactionDocumentId()+".html";
				%>
					<a href="<esf:out value='<%=fileLink%>'/>">Document snapshot</a>
				<% } %>
				<% if ( bean.reportTemplate.hasViewSnapshotDataPermission(bean.getLoggedInUser()) && bean.reportTemplate.hasViewSnapshotDocumentPermission(bean.getLoggedInUser()) ) { 
					String fileLink = "dsigsnapshots-"+foundTranPartyDoc.getTransactionPartyId()+"/"+foundTranPartyDoc.getTransactionDocumentId()+".xml";
				%>
					<a href="<esf:out value='<%=fileLink%>'/>">Snapshot XML (Digital Signatures)</a>
				<% } %>
			<% } else { %>&nbsp;<%}%>
		</td>
		<% } %>
	</tr>
	<% } /* end for each tranParty*/ %>
	</table>
	
<% } /*endfor each tranDoc*/ %>


<% } /* no errors */ %>

<%@ include file="footer.jsp"%>
</body>
</html>
<% bean.endSession(); /* okay to do since this is run outside of the user's session */%>
<%!
public class BeanData
	extends com.esignforms.open.jsp.PageBean
{
    public transient Transaction transaction = null;
    public transient ReportTemplate reportTemplate = null;
    public transient User user = null;
    
    public transient User startedByUser = null;
    public transient User lastUpdatedByUser = null;
    public transient java.util.List<TransactionParty> tranPartyList;
    public transient java.util.List<TransactionDocument> tranDocList;

    public BeanData()
	{
	}
    
    public String getTranStatusClass(String status)
    {
    	if ( Transaction.TRAN_STATUS_IN_PROGRESS.equals(status) ) 
    		return "green"; 
    	if ( Transaction.TRAN_STATUS_COMPLETED.equals(status) )
    		return "black"; 
    	if ( Transaction.TRAN_STATUS_CANCELED.equals(status) ) 
    		return "red"; 
    	return "gray";
    }

    public String getTranPartyStatusClass(String status)
    {
    	if ( TransactionPartyAssignment.STATUS_ACTIVATED.equals(status) || TransactionPartyAssignment.STATUS_RETRIEVED.equals(status) ) 
    		return "green"; 
    	if ( TransactionPartyAssignment.STATUS_COMPLETED.equals(status) )
    		return "black"; 
    	if ( TransactionPartyAssignment.STATUS_REJECTED.equals(status) || TransactionPartyAssignment.STATUS_REJECTED.equals(status) ) 
    		return "red"; 
    	if ( TransactionPartyAssignment.STATUS_TRANSFERRED.equals(status) )
    		return "gray italic"; 
    	return "gray";
    }

    public String getTranPartyDocumentStatusClass(String status)
    {
    	if ( TransactionPartyDocument.STATUS_RETRIEVED.equals(status) ) 
    		return "green"; 
    	if ( TransactionPartyDocument.STATUS_COMPLETED.equals(status) )
    		return "black"; 
    	if ( TransactionPartyDocument.STATUS_FIXED_REQUESTED.equals(status) || TransactionPartyDocument.STATUS_REJECTED.equals(status) ) 
    		return "red"; 
    	return "gray italic";
    }

	public PageProcessing doGet()
		throws java.io.IOException
	{
		String exportAuthCode = getParam("exportAuthCode",null);
		if ( ! isBlank(exportAuthCode) )
		{
	        Object[] objects = (Object[])getApplicationAttribute(exportAuthCode);
	        if ( objects == null || objects.length != 3 )
				errors.addError("Server error: exportAuthCode specified, but unable to retrieve the required object array parameters.");
	        else
	        {
	        	transaction = (Transaction)objects[0];
	        	reportTemplate = (ReportTemplate)objects[1];
		        loggedInUser = (User)objects[2];
			    if ( transaction == null )
					errors.addError("Server error: exportAuthCode specified was missing transaction.");
			    else
			    {
		        	startedByUser = transaction.hasCreatedByUserId() ? User.Manager.getById(transaction.getCreatedByUserId()) : null;
		        	lastUpdatedByUser = transaction.hasLastUpdatedByUserId() ? User.Manager.getById(transaction.getLastUpdatedByUserId()) : null;
		        	tranPartyList = transaction.getAllTransactionParties();
		        	tranDocList = transaction.getAllTransactionDocuments();
			    }
			    if ( reportTemplate == null )
					errors.addError("Server error: exportAuthCode specified was missing reportTemplate, unable to retrieve the transaction.");
			    if ( loggedInUser == null )
					errors.addError("Server error: exportAuthCode specified was missing loggedInUser, unable to retrieve the transaction.");
	        }
		}
		
		return PageProcessing.CONTINUE;
	}
}
%>
