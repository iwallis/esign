<!DOCTYPE html>
<%@page contentType="text/html; charset=UTF-8" import="com.esignforms.open.data.EsfDate
													  ,com.esignforms.open.data.EsfDateTime
													  ,com.esignforms.open.data.EsfDecimal
													  ,com.esignforms.open.data.EsfInteger
													  ,com.esignforms.open.data.EsfString
													  ,com.esignforms.open.data.EsfUUID
													  ,com.esignforms.open.data.EsfValue
													  ,com.esignforms.open.prog.ReportFieldTemplate
													  ,com.esignforms.open.prog.ReportTemplate
													  ,com.esignforms.open.prog.ReportTemplateReportField
													  ,com.esignforms.open.runtime.Transaction
													  ,com.esignforms.open.runtime.reports.ReportListingInfo
													  ,com.esignforms.open.user.User
													  "%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(session,request,response);
if ( bean.doGet() == BeanData.PageProcessing.DONE )
	return;
%>
<html>
<head>
    <meta charset="UTF-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - List of exported transactions from Open eSignForms</title>
	<meta name="Description" content="Lists the exported transactions for Open eSignForms - Version 1/12/2016"/>
	<meta name="robots" content="noindex, nofollow" />
    <style type="text/css">
 		<jsp:include page="../static/esf/esf.css" flush="true" />
 		.padH { padding-left: 5px; padding-right: 5px; }
	</style>
</head>

<body class="esf" style="background-color: #f5f5f5;">
<% if ( bean.errors.hasErrorOrWarning() ) { %>
<%=bean.errors.toHtml()%>
<% } else { %>

<p class="bold">Transactions exported on <%=(new com.esignforms.open.data.EsfDateTime()).toLogString(bean.getLoggedInUser())%> via report: <%=bean.reportTemplate.getPathName().toHtml()%></p>

<% if ( bean.reportListingInfoList != null && bean.reportListingInfoList.size() > 0 ) { 
	boolean isFirstTransaction = true; 
	String BGCOLOR = "bgGreenbarLight"; 
	String STATUS_COLOR;
	String ALIGN;
%>

<table class="w100 cellpadding0 cellspacing0">
<% for( ReportListingInfo reportListingInfo : bean.reportListingInfoList ) {
    if ( isFirstTransaction ) { isFirstTransaction = false; %>
<tr>
	<th class="boB padH">&nbsp;</th>
	<% for ( ReportTemplateReportField reportField : bean.reportTemplate.getReportTemplateReportFieldList() ) { 
		ReportFieldTemplate reportFieldTemplate = reportField.getReportFieldTemplate(); 
		if ( reportFieldTemplate == null )
			continue;
		if ( reportFieldTemplate.isFieldRendersAsButton() )
			continue;
    	if ( reportFieldTemplate.isFieldTypeInteger() || reportFieldTemplate.isFieldTypeDecimal() )
    		ALIGN = "right";
    	else if ( reportFieldTemplate.isFieldTypeDate() || reportFieldTemplate.isFieldTypeFile() )
    		ALIGN = "center";
    	else
    		ALIGN = "left";
	    %>
	<th class="boB padH <%=ALIGN%>" title='<esf:out value="<%=reportFieldTemplate.getFieldTooltip()%>"/>'><esf:out value="<%=reportField.getFieldLabel()%>"/></th>
	<% } /* end for loop ReportTemplateReportField */ %>
</tr>
<% } /* end if first */ %>
<%
  String tranPath = "tran-"+reportListingInfo.getId()+"/";
  if ( reportListingInfo.getStatus().equals(Transaction.TRAN_STATUS_IN_PROGRESS) ) 
	  STATUS_COLOR = "green"; 
  else if ( reportListingInfo.getStatus().equals(Transaction.TRAN_STATUS_COMPLETED) )
	  STATUS_COLOR = "black"; 
  else if ( reportListingInfo.getStatus().equals(Transaction.TRAN_STATUS_CANCELED) ) 
	  STATUS_COLOR = "red"; 
  else 
	  STATUS_COLOR = "gray";
%>
<tr class="<%=BGCOLOR%>">
	<td class="f8 center padH"><a href="<%=(tranPath+"tranDetail.html")%>" title="See the transaction details">Details</a></td>
	<% for ( ReportTemplateReportField reportField : reportListingInfo.getReportFieldValueList() ) { 
		ReportFieldTemplate reportFieldTemplate = reportField.getReportFieldTemplate();
		if ( reportFieldTemplate == null )
			continue;
		if ( reportFieldTemplate.isFieldRendersAsButton() )
			continue;
    	if ( reportFieldTemplate.isFieldTypeInteger() || reportFieldTemplate.isFieldTypeDecimal() )
    		ALIGN = "right";
    	else if ( reportFieldTemplate.isFieldTypeDate() || reportFieldTemplate.isFieldTypeFile() )
    		ALIGN = "center";	
    	else
    		ALIGN = "left";
		EsfValue fieldValue = reportField.getFieldValue();
		String displayFieldValue = fieldValue == null || fieldValue.isNull() ? "" : fieldValue.toString();

		String fieldName = reportFieldTemplate.getFieldName().toString();

		if ( reportFieldTemplate.isFieldTypeStringOrRelated() )
		{
			if ( "left4mask".equals(reportField.getOutputFormatSpec()) )
				displayFieldValue = bean.app.maskLeft(displayFieldValue);
			else if ( "right4mask".equals(reportField.getOutputFormatSpec()) )
				displayFieldValue = bean.app.maskRight(displayFieldValue);
		}
		else if ( reportFieldTemplate.isFieldTypeDate() )
		{
			EsfDate dateValue = (EsfDate)fieldValue;
			if ( dateValue != null && ! dateValue.isNull() )
			{
				String dateFormatSpec = reportField.hasOutputFormatSpec() ? reportField.getOutputFormatSpec() : bean.app.getDefaultDateFormat();
				displayFieldValue = dateValue.format(dateFormatSpec);
			}
		}
		else if ( reportFieldTemplate.isFieldTypeDecimal() )
		{
			EsfDecimal decimalValue = (EsfDecimal)fieldValue;
			if ( decimalValue != null && ! decimalValue.isNull() )
			{
				if ( reportField.hasOutputFormatSpec() )
					displayFieldValue = decimalValue.format(reportField.getOutputFormatSpec());
			}
		}
		else if ( reportFieldTemplate.isFieldTypeInteger() )
		{
			EsfInteger integerValue = (EsfInteger)fieldValue;
			if ( integerValue != null && ! integerValue.isNull() )
			{
				if ( reportField.hasOutputFormatSpec() )
					displayFieldValue = integerValue.format(reportField.getOutputFormatSpec());
			}
		}
		else if ( reportFieldTemplate.isFieldTypeBuiltIn() )
		{
			if ( "esf_last_updated_timestamp".equals(fieldName) ||
				 "esf_start_timestamp".equals(fieldName) ||
				 "esf_cancel_timestamp".equals(fieldName) ||
				 "esf_expire_timestamp".equals(fieldName) ||
				 "esf_stall_timestamp".equals(fieldName)
				) 
			{
				EsfDateTime dateTimeValue = (EsfDateTime)fieldValue;
				if ( dateTimeValue != null && ! dateTimeValue.isNull() )
				{
			   		String dateFormatSpec = reportField.hasOutputFormatSpec() ? reportField.getOutputFormatSpec() : bean.app.getDefaultDateTimeFormat();
			   		displayFieldValue = dateTimeValue.format(bean.getLoggedInUser(),dateFormatSpec);
				}
			}
			else if ( "esf_last_updated_by_user".equals(fieldName) || "esf_created_by_user".equals(fieldName) )
			{
				EsfUUID userId = (EsfUUID)fieldValue;
				if ( userId != null && ! userId.isNull() )
				{
					if ( "id".equals(reportField.getOutputFormatSpec()) )
						displayFieldValue = userId.toString();
					else
					{
						User user = User.Manager.getById(userId);
						if ( user == null )
							displayFieldValue = "???";
						else if ( "displayname".equals(reportField.getOutputFormatSpec()) )
							displayFieldValue = user.getDisplayName();
						else if ( "fulldisplayname".equals(reportField.getOutputFormatSpec()) )
							displayFieldValue = user.getFullDisplayName();
						else if ( "personalname".equals(reportField.getOutputFormatSpec()) )
							displayFieldValue = user.getPersonalName();
						else if ( "familyname".equals(reportField.getOutputFormatSpec()) )
							displayFieldValue = user.getFamilyName();
						else
							displayFieldValue = user.getEmail();
					}
				}
			}
			else if ( "esf_status".equals(fieldName) )
			{
				EsfString status = (EsfString)fieldValue;
				if ( status != null && ! status.isNull() )
				{
					if ( status.equals(Transaction.TRAN_STATUS_IN_PROGRESS) ) 
						displayFieldValue = bean.app.getServerMessages().getString("transaction.status.inProgress");
					else if ( status.equals(Transaction.TRAN_STATUS_COMPLETED) ) 
						displayFieldValue = bean.app.getServerMessages().getString("transaction.status.completed");
					else if ( status.equals(Transaction.TRAN_STATUS_CANCELED) ) 
						displayFieldValue = bean.app.getServerMessages().getString("transaction.status.canceled");
					else if ( status.equals(Transaction.TRAN_STATUS_SUSPENDED) ) 
						displayFieldValue = bean.app.getServerMessages().getString("transaction.status.suspended");
				}
			}
		}
	%>
		<td class="f8 padH <%=STATUS_COLOR%> <%=ALIGN%>"><esf:out value="<%=displayFieldValue%>"/></td>
	<% } /* end for loop ReportTemplateReportField */ %>
</tr>
<% BGCOLOR = "bgGreenbarLight".equals(BGCOLOR) ? "bgGreenbarDark" : "bgGreenbarLight"; 
   } /* end for each ReportListingInfo */
%>

</table>

<p class="fsmall nomargin">(<%=bean.makePrettyNumber(bean.reportListingInfoList.size())%> transaction<%=(bean.reportListingInfoList.size()==1?"":"s")%> matched)</p>

<% } /* has report listings */ %>

<% } /* has no errors */ %>

<%@ include file="footer.jsp"%>
</body>
</html>
<% bean.endSession(); /* okay to do since this is run outside of the user's session */%>
<%!
public class BeanData
	extends com.esignforms.open.jsp.PageBean
{
    public transient java.util.List<ReportListingInfo> reportListingInfoList = null;
    public transient ReportTemplate reportTemplate = null;
    public transient User user = null;

    public BeanData()
	{
	}

    @SuppressWarnings("unchecked") 
	public PageProcessing doGet()
		throws java.io.IOException
	{
		String exportAuthCode = getParam("exportAuthCode",null);
		if ( ! isBlank(exportAuthCode) )
		{
	        Object[] objects = (Object[])getApplicationAttribute(exportAuthCode);
	        if ( objects == null || objects.length != 3 )
				errors.addError("Server error: exportAuthCode specified, but unable to retrieve the required object array parameters.");
	        else
	        {
	        	reportListingInfoList = (java.util.List<ReportListingInfo>)objects[0];
	        	reportTemplate = (ReportTemplate)objects[1];
		        loggedInUser = (User)objects[2];
			    if ( reportListingInfoList == null || reportListingInfoList.size() == 0 )
					errors.addInfo("No matching transactions were found.");
			    if ( reportTemplate == null )
					errors.addError("Server error: exportAuthCode specified was missing reportTemplate, unable to retrieve the transaction.");
			    if ( loggedInUser == null )
					errors.addError("Server error: exportAuthCode specified was missing loggedInUser, unable to retrieve the transaction.");
	        }
		}
		
		return PageProcessing.CONTINUE;
	}
}
%>
