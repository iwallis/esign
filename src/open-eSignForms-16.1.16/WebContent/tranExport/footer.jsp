<table class="w100 f8">
<tr>
	<td align="center" title="Copyright notice and version. Open eSignForms was started in 2009 and first commercially deployed in 2011. Yozons, Inc. was founded in 2000.">
		<%=com.esignforms.open.Version.getHtmlCopyright()%> - <%=com.esignforms.open.Version.getReleaseString()%>
		<br/>Exported from deployment id <%=com.esignforms.open.Application.getInstance().getDeployId().toHtml()%> at <%=com.esignforms.open.util.HtmlUtil.toDisplayHtml(com.esignforms.open.Application.getInstance().getExternalContextPath())%>
	</td>
</tr>
</table>
