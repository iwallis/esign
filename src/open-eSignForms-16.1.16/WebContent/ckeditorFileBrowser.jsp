<!DOCTYPE html>
<%@page session="true" contentType="text/html; charset=UTF-8" 
import="com.esignforms.open.config.CKEditorContext,
		com.esignforms.open.prog.DocumentVersion,
		com.esignforms.open.data.EsfInteger,
		com.esignforms.open.data.EsfName,
		com.esignforms.open.prog.File,
		com.esignforms.open.prog.FileInfo,
		com.esignforms.open.prog.FileVersion,
		com.esignforms.open.user.User"
%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(session,request,response);
bean.doGet();
%>

<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - CKEditor File Browser</title>
	<meta name="Description" content="CKEditor File Browser - Version 12/8/2015"/>
	<meta name="robots" content="noindex, nofollow" />
<script type="text/javascript">
<!--
function setCKEditorUrl(CKEditorFuncNum, url) {
	window.opener.CKEDITOR.tools.callFunction(CKEditorFuncNum,url);
	window.close();
}
// -->
</script>

<style type="text/css">
<jsp:include page="./static/esf/esf.css" flush="true" />
</style>
</head>

<body class="esf">

<% if ( bean.errors.hasError() ) { %>
<%=bean.errors.toHtml()%>
<% } else if ( bean.fileInfoMap.size() == 0 ) { %>
<p>Strange, but no files were found.</p>
<% } else { %>
<p>Here is the list of files available. Click on any one to choose it.</p>
<% 
for( FileInfo fileInfo : bean.fileInfoMap.values() ) 
{ 
	FileVersion fileVersion = FileVersion.Manager.getByVersion(fileInfo.getId(),fileInfo.getTestVersion());
	if ( fileVersion == null ) {
		bean.err("Failed to find FileVersion by FileInfo ID: " + fileInfo.getId() + "; test version: " + fileInfo.getTestVersion());
		continue;
	}
	// No filename used at the end of the path since the actual file filename depends on which file+version is used at runtime.
	// And the 'fid' is only if the file can't be found by name.
	// Note that this format is relied upon by DocumentVersionPage when it generates the JSP and finds these IMG tags to be turned into tags.
	// And the FileOut JSP tag uses the same URL syntax so the servlet works as expected
	String url = bean.getContextPath() + "/files/" + fileInfo.getEsfName() + "/" + bean.toHtml(fileVersion.getFileFileName()) + "?fid=" + fileInfo.getId(); 
	StringBuilder fileListing = new StringBuilder(128);
	fileListing.append(fileInfo.getEsfName());
	if ( fileInfo.hasDisplayName() )
		fileListing.append(" (").append(fileInfo.getDisplayName()).append(")");
	fileListing.append(" - ").append(fileVersion.getFileFileName());
	fileListing.append(" (").append(EsfInteger.byteSizeInUnits(fileVersion.getFileSize())).append(")");
	if ( fileInfo.hasDescription() )
		fileListing.append(" - ").append(fileInfo.getDescription());
	%>
<p><a href="#" onclick="setCKEditorUrl(<%=bean.CKEditorFuncNum%>,'<%=url%>')"><%=fileListing.toString()%></a></p>
<% } %>
<% } /* no errors */ %>

<p style="margin-left: 4em;"><jsp:include page="./footer.jsp" flush="true" /></p>

</body>
</html>

<!-- "Bean" methods should be placed here -->
<%!
public class BeanData extends com.esignforms.open.jsp.PageBean {
	public BeanData() {}
	
	public String CKEditorFuncNum;
	public String ccid;
	public CKEditorContext ckeditorContext;
	public java.util.TreeMap<EsfName,FileInfo> fileInfoMap;
	
public PageProcessing doGet() throws java.io.IOException {
	if ( ! isUserLoggedIn() ) {
		errors.addError("You must be logged in to list files for use by the HTML editor.");
		return PageProcessing.CONTINUE;
	}
	
	CKEditorFuncNum = getParam("CKEditorFuncNum");
	if ( isBlank(CKEditorFuncNum) ) {
		errors.addError("The required param 'CKEditorFuncNum' is missing.");
		return PageProcessing.CONTINUE;
	}
	
	// Get our context so we know which libraries to search for files
	ccid = getParam("ccid");
	if ( isBlank(ccid) ) {
		errors.addError("The required param 'ccid' is missing.");
		return PageProcessing.CONTINUE;
	}
	
	ckeditorContext = getLoggedInUser().getCKEditorContext(session,ccid);
	if ( ckeditorContext == null ) {
		errors.addError("No CKEditorContext could be found for the specified 'ccid'.  You may need to re-login before trying again.");
		return PageProcessing.CONTINUE;
	}
	
	fileInfoMap = new java.util.TreeMap<EsfName,FileInfo>();
	
	// First, let's put in the images from our document version's library and document version itself
	if ( ckeditorContext.hasDocumentVersion() ) {
		DocumentVersion documentVersion = ckeditorContext.getDocumentVersion();
		for( FileInfo fileInfo : FileInfo.Manager.getAll(documentVersion.getDocument().getLibrary().getId()) ) {
			if ( ! fileInfoMap.containsKey(fileInfo.getEsfName()) ) {
				fileInfoMap.put(fileInfo.getEsfName(),fileInfo);
			}
		}		
		for( FileInfo fileInfo : FileInfo.Manager.getAll(documentVersion.getId()) ) {
			if ( ! fileInfoMap.containsKey(fileInfo.getEsfName()) ) {
				fileInfoMap.put(fileInfo.getEsfName(),fileInfo);
			}
		}		
	}
	
	// Then, we'll put in anything from a specified library
	if ( ckeditorContext.hasLibrary() ) {
		for( FileInfo fileInfo : FileInfo.Manager.getAll(ckeditorContext.getLibrary().getId()) ) {
			if ( ! fileInfoMap.containsKey(fileInfo.getEsfName()) ) {
				fileInfoMap.put(fileInfo.getEsfName(),fileInfo);
			}
		}		
	}
	
	// Finally, we'll put in anything from our template library
	for( FileInfo fileInfo : FileInfo.Manager.getAll(ckeditorContext.getTemplateLibrary().getId()) ) {
		if ( ! fileInfoMap.containsKey(fileInfo.getEsfName()) ) {
			fileInfoMap.put(fileInfo.getEsfName(),fileInfo);
		}
	}		
	
	return PageProcessing.CONTINUE;
}

}
%>
