<!DOCTYPE html>
<%@page session="false" contentType="text/html; charset=UTF-8" 
import="com.esignforms.open.config.Literals,
		com.esignforms.open.data.EsfBoolean,
		com.esignforms.open.util.StringReplacement,
        com.esignforms.open.user.User,
        com.esignforms.open.user.UserLoginInfo"
%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(request.getSession(false),request,response);
if ( bean.isGet() )
{
	if ( bean.doGet() == BeanData.PageProcessing.DONE )
		return;
}
else
{
	if ( bean.doPost() == BeanData.PageProcessing.DONE )
		return;
}
%>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - Forgot my password</title>
	<meta name="Description" content="Allows a user to reset his/her own password via the forgotten question and answer check - Version 12/8/2015"/>
	<meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <base href="<%=bean.getExternalUrlContextPath()%>/"/>
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="shortcut icon" />
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="icon" />
	<script type="text/javascript" src="./static/esf/esf.js" ></script>
<script type="text/javascript">
<!--
function init()
{
  <% if ( ! bean.loginPageAllowEmbedded ) { %>
  if (window != window.top) {
	top.location.href = location.href;
  }
  <% } %>
	
  return true;
}
// -->
</script>
	
<style type="text/css">
<jsp:include page="./static/esf/esf.css" flush="true" />
</style>
</head>

<body onload="init()" class="esf" style="background-color: #f5f5f5;">
<%@ include file="./header.jsp" %>
<%=bean.errors.toHtml()%>

<form name="aform" method="post" action="<%=bean.toHtml(bean.getMySessionUrl())%>" enctype="application/x-www-form-urlencoded" autocomplete="OFF" onsubmit="esf.hideAllSubmitInputFields()">
<table class="w100">
<tr>
	<td class="boB"><h1 class="pagename"><img src="./VAADIN/themes/openesf/icons/fatcow16/user.png" width="16" height="16" alt="Users"/>Forgot my password<%if ( bean.hasTargetUser() ) { %> <span class="esfId">(<esf:out value="<%=bean.userLoginInfo.getUser().getFullDisplayName()%>"/>)</span><%} %></h1></td>
	<% if ( bean.isUserLoggedIn() ) { %>
	<td class="boB right">
		<a href="<%=bean.encodeSession("./ui/")%>"><span class="logoColor">Return to menu</span></a>
	</td>
	<% } %>
</tr>
</table>

<% if ( bean.hasTargetUser() ) { %>

<p>When you last established your password, you set a forgotten password question and answer.</p>
<p>Before we allow you to choose a new password, we will check to see if you know the answer to that question.</p>

<div style="width: 700px; background-color: #FDE8A5; padding: 0 1ex 0 1ex; border: 1px solid black; border-radius: 5px;">
<p><label for="forgottenQuestion" title="This is the forgotten question you set previously">Your question:</label><br/>
	<b><esf:out value="<%=bean.forgottenQuestion%>"/></b></p>

<p style="margin-top: 1em;"><label for="forgottenAnswer" title="Enter the answer to the forgotten question you set previously">Answer to your question:</label><br/>
	<input name="forgottenAnswer" value="<%=bean.toHtml(bean.forgottenAnswer)%>" size="50" maxlength="<%=bean.getMaxForgotPasswordAnswerLength()%>" class="bold required" />
</p>
<p>Tip: <esf:out value="<%=bean.getForgotPasswordAnswerTip()%>"/></p>
<p><input type="submit" class="button" name="<%=BeanData.CONTINUE_BUTTON%>" value="Continue"/></p>
</div>

<% } %>
</form>

<p style="margin-left: 4em;"><jsp:include page="./footer.jsp" flush="true" /></p>

</body>
</html>
<!-- "Bean" methods should be placed here -->
<%!
public class BeanData extends com.esignforms.open.jsp.PageBean {

    public BeanData() {	}

	// static values that are buttons
	public static final String CONTINUE_BUTTON  = "bContinue";
	
    public UserLoginInfo userLoginInfo = null;
	
    public String pickupCode;
    public String forgottenQuestion;
    public String forgottenAnswer;

    public boolean loginPageAllowEmbedded = false;

    
public PageProcessing doGet() throws java.io.IOException {
	if ( ! doInit() ) {
		return PageProcessing.DONE;
	}

	loadFormDataDoGet();

	return PageProcessing.CONTINUE;
}

public PageProcessing doPost()	throws java.io.IOException {
	if ( ! doInit() ) {
		return PageProcessing.DONE;
	}

	loadFormData();
	
    String button = getParam(CONTINUE_BUTTON,null);
    if ( button != null ) {
        return doCheckForgottenPasswordAnswer();
    }

    errors.addError("You pressed an unexpected button. Please try again.");
	return PageProcessing.CONTINUE;
}

private boolean doInit() throws java.io.IOException {
	if ( ! app.hasPasswordManager() ) {
	    warning("Attempted to answer the forgotten password reset question when passwords are not enabled.");
	    sendRedirect("./");
	    return false;
	}

	String loginPageAllowEmbeddedValue = StringReplacement.replace("${property:ESF.LoginPageAllowEmbedded}");
	loginPageAllowEmbedded = EsfBoolean.toBoolean(loginPageAllowEmbeddedValue);
	
	if ( ! loginPageAllowEmbedded )
	{
		response.setHeader("X-Frame-Options","DENY");
		response.setHeader("Content-Security-Policy","frame-ancestors 'none'");
	}

	pickupCode = getParam("c");
    if ( isBlank(pickupCode) ) {
        errors.addError("You have not provided a valid confirmation code in order to set your password.");
        return true;
    }
    
   	endSession(); // kill any active non-logged request to set a new password
   	
	if ( ! app.getRandomKey().isCorrectPickupCodeLength(pickupCode) ) {
       	errors.addError("The confirmation code you provided is not the correct length. It should be " + com.esignforms.open.util.RandomKey.PICKUP_CODE_LENGTH + " characters.");
       	errors.addError("If the link in your email was split into two lines, please copy the rest from the second line and paste it to the end of the link in your browser's address or location field.");
        return true;
    }
       
    userLoginInfo = app.getPasswordManager().getUserLoginInfoByForgottenPasswordPickupCode(pickupCode);
    if ( userLoginInfo == null ) {
    	errors.addError("Sorry, the confirmation code provided is not valid. Please ask your administrator to reset your password and then click on the link in the email that will be sent to you.");
        return true;
    }

    if ( isGet() && userLoginInfo.hasForgottenQuestion() ) {
       	userLoginInfo.getUser().logSecurity("Forgotten password Q&A page shown after providing the correct confirmation code. IP: " + getIP());
    }
    
    if ( ! userLoginInfo.hasForgottenQuestion() ) {
    	errors.addError("Sorry, you have not established a forgotten password question and answer. Please ask your administrator to reset your password and then click on the link in the email that will be sent to you.");
        userLoginInfo = null;
        return true;
    }
	
	return true;
}

private void loadFormDataDoGet() {
    if ( userLoginInfo != null && userLoginInfo.hasForgottenQuestion() ) {
        forgottenQuestion = userLoginInfo.getForgottenQuestion();
    }
}

private void loadFormData() {
	forgottenAnswer = getParam("forgottenAnswer");
    if ( userLoginInfo != null && userLoginInfo.hasForgottenQuestion() ) {
    	forgottenQuestion = userLoginInfo.getForgottenQuestion();
    }
}	

private boolean validateFormData() {
    if ( isBlank(forgottenAnswer) ) {
        errors.addError("Please enter your forgotten password answer.","forgottenAnswer");
    }

    return ! errors.hasError();
}

private PageProcessing doCheckForgottenPasswordAnswer()
{
    if ( ! validateFormData() ) {
        return PageProcessing.CONTINUE;
    }
    
    try {
		app.getPasswordManager().checkForgottenPasswordAnswer(userLoginInfo.getUser(), forgottenAnswer);
		userLoginInfo.getUser().logSecurity("User answered the forgotten password correctly.  Redirecting to set a new password. IP: " + getIP());
		sendRedirect("./setMyPassword.jsp?c="+userLoginInfo.getResetPickupCode());
		return PageProcessing.DONE;
    }
    catch( Exception e ) {
        errors.addError(e.getMessage());
    }
    
    return PageProcessing.CONTINUE;
}

public boolean hasTargetUser() { return userLoginInfo != null; }

public int getMaxPasswordLength() {
    return app.getPasswordManager().getMaxPasswordLength();
}

public int getMaxForgotPasswordAnswerLength() {
    return app.getPasswordManager().getMaxForgotPasswordAnswerLength();
}

public String getPasswordTip() {
    return app.getPasswordManager().getTip();
}

public String getForgotPasswordAnswerTip() {
    return app.getPasswordManager().getForgotPasswordAnswerTip();
}

}
%>
