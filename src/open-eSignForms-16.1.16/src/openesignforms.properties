# Copyright (C) 2009-2014 Yozons, Inc.
# Open eSignForms - Web-based electronic contracting software
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
# as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
# See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License along with this program.  
# If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
# Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
#
# Properties file for Open eSignForms

# Subdirectory name to use inside the deployed webapp's context path where generated "library" code/JSP/images are written.
# The other subdirectories are all under this base subdirectory.
library.generated.base.subdirectory=LIBRARYGENERATED-KEEP
library.generatedJSP.document.subdirectory=document
library.generatedCSS.subdirectory=css

# Set the max size for a file upload. Previous default was 100MB, but noted on low memory servers a smaller value like 10MB may be all that works
upload.file.maxMB=100
# Max size for uploading an image
upload.image.maxMB=5
# Max size for upload a config file, such as importing XML
upload.config.maxMB=20

# For unlocking the initial boot key when a database is being used; set to no value if they will
# be provided externally (manually) at application startup, at which time the above "allow" options
# can also be changed.
boot.password.1=test1
boot.password.2=test2
# When booting, don't create a new session key unless the last session created is at least this old in days. 
boot.sessionkey.createNewNoSoonerThanDays=90

# The BC provider is assumed to be present, but others may be used instead by configuring them here
# or ensuring they are defined in the jre/lib/security/java.security file.  If we did BC, it would be:
# security.provider.N=org.bouncycastle.jce.provider.BouncyCastleProvider
jce.provider.name=BC
jce.provider.class=org.bouncycastle.jce.provider.BouncyCastleProvider

# Sets the SecureRandom info; if you leave these defined, but empty, the default SecureRandom will be used
#java.security.SecureRandom.algorithm=SHA1PRNG
#java.security.SecureRandom.provider=SUN
java.security.SecureRandom.algorithm=
java.security.SecureRandom.provider=
# A semicolon separated list of undesirable string segments (will use case insensitive matching) that shouldn't appear in 
# any random strings produced for EsfUUIDs and pickup codes and the like created by RandomKey. 
# Received our first complaint about a generated random id containing a "bad word" on 19 May 2014.
RandomKey.undesirable.string.segments=butt;cock;cunt;dick;fuck;piss;prick;shit;suck;tits

# Sets the SecureHash info. Our default is to use SHA-512.
java.security.MessageDigest.algorithm=SHA-512
java.security.MessageDigest.provider=BC

# Sets the KeyPairGenerator for digital signatures. Our default is to use RSA 4096-bits.
java.security.KeyPairGenerator.algorithm=RSA
#java.security.KeyPairGenerator.provider=SunRsaSign
java.security.KeyPairGenerator.provider=BC
java.security.KeyPairGenerator.keysize=4096

# Sets the PBE info to use with dual passwords to secure the boot key. Our default is the PBE using SHA-256 and 256-bit AES.
PBE.javax.crypto.SecretKeyFactory.algorithm=PBEWithSHA256And256BitAES-CBC-BC
PBE.javax.crypto.SecretKeyFactory.provider=BC
PBE.javax.crypto.Cipher.algorithm=PBEWithSHA256And256BitAES-CBC-BC
PBE.javax.crypto.Cipher.provider=BC
# Like the BCrypt iterations, we recommend that this be doubled every 18 months. Set to 50000 in March 2013.
PBE.iterations=50000
# The salt is calculated as the MessageDigest (i.e. SHA-512 as set above by java.security.MessageDigest.algorithm) of the password
# and then using this number of bytes. It is not recommended be less than 8.
PBE.saltSize=16

# For jBCrypt salt generation. Generally speaking, if processing power doubles every 18 months, you can add one to this
# number every 18 months. Because the hash value contains the number used, there's no issue with respect to previous
# passwords as this number increases. In 2010, the default value was 10. So by 2012, we'll just start with 11.
jBCrypt.gensalt.iterations=11

# Sets the symmetric KeyGenerator info as well as the cipher that uses them. Our default is use 256-bit AES.
javax.crypto.KeyGenerator.algorithm=AES
javax.crypto.KeyGenerator.provider=BC
javax.crypto.KeyGenerator.keysize=256
javax.crypto.KeyGenerator.cipher.algorithm=AES/CBC/PKCS7Padding
javax.crypto.KeyGenerator.cipher.provider=BC
javax.crypto.KeyGenerator.cipher.blocksize=16
