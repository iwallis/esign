// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.email;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.EmailTemplateVersion;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.user.User;
import com.esignforms.open.util.StringReplacement;

/**
* OutboundEmailMessage holds all the info needed for the OutboundEmailProcessor to send the email on our behalf.
* This object lets us track all emails we send, as well as allows us to create these in a DB transaction
* so it on failure, we can essentially rollback emails too.
* 
* @author Yozons, Inc.
*/
public class OutboundEmailMessage
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<OutboundEmailMessage>
{
	private static final long serialVersionUID = 2362869774044077043L;

	public final static String LINK_TYPE_USER = "user";
	public final static String LINK_TYPE_TRANSACTION = "transaction";
	public final static String LINK_TYPE_TRANSACTION_PARTY = "transactionParty";

    private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(OutboundEmailMessage.class);

    protected final EsfUUID id;
    protected final String bounceCorrelationCode; // A random code we use in the return path so we can map bounces back to this message, but only lowercase
    protected final EsfUUID transactionId; // The optional transaction associated with this outbound email
    protected final EsfUUID transactionPartyId; // The optional transaction party associated with this outbound email (was sent to them)
    protected final EsfUUID userId; // The optional user associated with this outbound email (was sent to/from this user -- associates the email to the user)
    
    protected final EsfDateTime createdTimestamp; // date object created and queued to the email system
    protected EsfDateTime sentTimestamp; // date object was sent by the email system
    protected String sendStatus; // send status, generally an error message when the send fails
    
    protected String emailFrom;
    protected String emailTo;
    protected String emailCc;
    protected String emailBcc;
    protected String emailSubject;
    protected String emailText;
    protected String emailHtml;
    
    protected List<OutboundEmailMessageAttachment> attachments;
    
    
    /**
     * This creates a OutboundEmailMessage object from data retrieved from the DB.
     */
    protected OutboundEmailMessage(EsfUUID id, String bounceCorrelationCode, EsfUUID transactionId, EsfUUID transactionPartyId, EsfUUID userId, 
    		    EsfDateTime createdTimestamp, EsfDateTime sentTimestamp, String sendStatus,
    			String emailFrom, String emailTo, String emailCc, String emailBcc, String emailSubject, String emailText, String emailHtml,
    			List<OutboundEmailMessageAttachment> attachments
    		)
    {
        this.id = id;
        this.bounceCorrelationCode = bounceCorrelationCode;
    	this.transactionId = transactionId;
        this.transactionPartyId = transactionPartyId;
        this.userId = userId;
        this.createdTimestamp = createdTimestamp;
        this.sentTimestamp = sentTimestamp;
        setSendStatus(sendStatus);
        setEmailFrom(emailFrom);
        setEmailTo(emailTo);
        setEmailCc(emailCc);
        setEmailBcc(emailBcc);
        setEmailSubject(emailSubject);
        setEmailText(emailText);
        setEmailHtml(emailHtml);
        this.attachments = attachments;
    }
    
    protected OutboundEmailMessage(EsfUUID userId, EmailTemplateVersion emailTemplateVersion, List<StringReplacement.FromToSpec> replacementSpecs)
    {
    	this.id = new EsfUUID();
    	this.bounceCorrelationCode = Application.getInstance().getRandomKey().getEmailBounceCorrelationCodeString();
    	
    	this.transactionId = null;
    	this.transactionPartyId = null;
    	this.userId = userId; // may be null
    	
    	this.createdTimestamp = new EsfDateTime();
    	this.sentTimestamp = null;
    	setSendStatus(Application.getInstance().getServerMessages().getString("OutboundEmailMessage.sendStatus.queued"));
    	
    	setEmailFrom(StringReplacement.replace(emailTemplateVersion.getEmailFrom(),replacementSpecs));
    	setEmailTo(StringReplacement.replace(emailTemplateVersion.getEmailTo(),replacementSpecs));
    	setEmailCc(StringReplacement.replace(emailTemplateVersion.getEmailCc(),replacementSpecs));
    	setEmailBcc(StringReplacement.replace(emailTemplateVersion.getEmailBcc(),replacementSpecs));
    	setEmailSubject(StringReplacement.replace(emailTemplateVersion.getEmailSubject(),replacementSpecs));
    	setEmailText(StringReplacement.replace(emailTemplateVersion.getEmailText(),replacementSpecs));
    	setEmailHtml(getCompleteHtml(new EsfString(emailSubject),emailTemplateVersion.getEmailHtml()));
    	setEmailHtml(StringReplacement.replace(this.emailHtml,replacementSpecs));
    	this.attachments = new LinkedList<OutboundEmailMessageAttachment>();
    }

   
    protected OutboundEmailMessage(EsfUUID transactionId, DocumentVersion documentVersion, EsfUUID transactionPartyId, EsfUUID userId, EmailTemplateVersion emailTemplateVersion, List<StringReplacement.FromToSpec> replacementSpecs)
    {
    	this.id = new EsfUUID();
    	this.bounceCorrelationCode = Application.getInstance().getRandomKey().getEmailBounceCorrelationCodeString();
    	
    	this.transactionId = transactionId;
    	this.transactionPartyId = transactionPartyId;
    	this.userId = userId;
    	
    	this.createdTimestamp = new EsfDateTime();
    	this.sentTimestamp = null;
    	setSendStatus(Application.getInstance().getServerMessages().getString("OutboundEmailMessage.sendStatus.queued"));
    	
    	User user = userId == null ? null : User.Manager.getById(userId);
    	Transaction tran = transactionId == null ? null : Transaction.Manager.getById(this.transactionId);
    	if ( tran != null )
    	{
    		setEmailFrom(StringReplacement.replace(emailTemplateVersion.getEmailFrom(),user,tran,documentVersion,replacementSpecs));
    		setEmailTo(StringReplacement.replace(emailTemplateVersion.getEmailTo(),user,tran,documentVersion,replacementSpecs));
    		setEmailCc(StringReplacement.replace(emailTemplateVersion.getEmailCc(),user,tran,documentVersion,replacementSpecs));
    		setEmailBcc(StringReplacement.replace(emailTemplateVersion.getEmailBcc(),user,tran,documentVersion,replacementSpecs));
    		setEmailSubject(StringReplacement.replace(emailTemplateVersion.getEmailSubject(),user,tran,documentVersion,replacementSpecs));
        	if ( emailTemplateVersion.hasEmailText() )
        		setEmailText(StringReplacement.replace(emailTemplateVersion.getEmailText(),user,tran,documentVersion,replacementSpecs));
        	else
        		setEmailText(null);
        	if ( emailTemplateVersion.hasEmailHtml() )
        	{
        		setEmailHtml(getCompleteHtml(new EsfString(emailSubject),emailTemplateVersion.getEmailHtml()));
        		setEmailHtml(StringReplacement.replace(this.emailHtml,user,tran,documentVersion,replacementSpecs));
        	}
        	else
        		setEmailHtml(null);
    	}
    	else
    	{
    		setEmailFrom(StringReplacement.replace(emailTemplateVersion.getEmailFrom(),replacementSpecs));
    		setEmailTo(StringReplacement.replace(emailTemplateVersion.getEmailTo(),replacementSpecs));
    		setEmailCc(StringReplacement.replace(emailTemplateVersion.getEmailCc(),replacementSpecs));
    		setEmailBcc(StringReplacement.replace(emailTemplateVersion.getEmailBcc(),replacementSpecs));
    		setEmailSubject(StringReplacement.replace(emailTemplateVersion.getEmailSubject(),replacementSpecs));
        	if ( emailTemplateVersion.hasEmailText() )
        		setEmailText(StringReplacement.replace(emailTemplateVersion.getEmailText(),replacementSpecs));
        	else
        		setEmailText(null);
        	if ( emailTemplateVersion.hasEmailHtml() )
        	{
        		setEmailHtml(getCompleteHtml(new EsfString(emailSubject),emailTemplateVersion.getEmailHtml()));
        		setEmailHtml(StringReplacement.replace(this.emailHtml,replacementSpecs));
        	}
        	else
        		setEmailHtml(null);
    	}
    	this.attachments = new LinkedList<OutboundEmailMessageAttachment>();
    }

   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final String getBounceCorrelationCode()
    {
        return bounceCorrelationCode;
    }
    
    public final EsfUUID getTransactionId()
    {
        return transactionId;
    }

    public final EsfUUID getTransactionPartyId()
    {
        return transactionPartyId;
    }
    
    public final EsfUUID getUserId()
    {
        return userId;
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }
    
    public EsfDateTime getSentTimestamp()
    {
    	return sentTimestamp;
    }
    public boolean hasBeenSent()
    {
    	return sentTimestamp != null;
    }
    public void setMessageHasBeenSent()
    {
    	if ( sentTimestamp == null )
    	{
    		sentTimestamp = new EsfDateTime();
    		setSendStatus(Application.getInstance().getServerMessages().getString("OutboundEmailMessage.sendStatus.sent"));
    		objectChanged();
    	}
    }
    public void setMessageFailedToBeSent(String errorSendStatus)
    {
    	setMessageHasBeenSent(); // actually sets sendStatus to "Sent"
    	setSendStatus(errorSendStatus); // then we overwrite with our error status
    }
    
    public String getSendStatus()
    {
    	return sendStatus;
    }
    public void setSendStatus(String v)
    {
    	sendStatus = EsfString.truncateTrimmedString(v,Literals.LOG_MAX_LENGTH);
        objectChanged();
    }
    
    public String getEmailFrom()
    {
    	return emailFrom;
    }
    public boolean hasEmailFrom()
    {
    	return EsfString.isNonBlank(emailFrom);
    }
    public void setEmailFrom(String v)
    {
    	EsfEmailAddress addr = new EsfEmailAddress(v);
    	if ( addr.isValid() )
    	{
    		emailFrom = EsfString.ensureTrimmedLength(addr.toString(),Literals.EMAIL_ADDRESS_MAX_LENGTH);
    		objectChanged();
    	}
    }
    
    public String getEmailTo()
    {
    	return emailTo;
    }
    public void setEmailTo(String v)
    {
    	RecipientList recipientList = new RecipientList(v);
    	if ( ! recipientList.hasInvalid() )
    	{
        	emailTo = EsfString.ensureTrimmedLength(recipientList.getEmailText(),Literals.EMAIL_ADDRESS_LIST_MAX_LENGTH);
        	objectChanged();
    	}
    }
    
    public String getEmailCc()
    {
    	return emailCc;
    }
    public void setEmailCc(String v)
    {
    	RecipientList recipientList = new RecipientList(v);
    	if ( ! recipientList.hasInvalid() )
    	{
        	emailCc = EsfString.ensureTrimmedLength(recipientList.getEmailText(),Literals.EMAIL_ADDRESS_LIST_MAX_LENGTH);
        	objectChanged();
    	}
    }
    
    public String getEmailBcc()
    {
    	return emailBcc;
    }
    public void setEmailBcc(String v)
    {
    	RecipientList recipientList = new RecipientList(v);
    	if ( ! recipientList.hasInvalid() )
    	{
        	emailBcc = EsfString.ensureTrimmedLength(recipientList.getEmailText(),Literals.EMAIL_ADDRESS_LIST_MAX_LENGTH);
        	objectChanged();
    	}
    }
    
    public String getEmailSubject()
    {
    	return emailSubject;
    }
    public void setEmailSubject(String v)
    {
    	emailSubject = EsfString.truncateTrimmedString(v,Literals.EMAIL_SUBJECT_MAX_LENGTH);
        objectChanged();
    }
    
    public String getEmailText()
    {
    	return emailText;
    }
    public void setEmailText(String v)
    {
    	emailText = EsfString.truncateTrimmedString(v,Literals.EMAIL_TEXT_MAX_LENGTH);
        objectChanged();
    }
    
    public String getEmailHtml()
    {
    	return emailHtml;
    }
    public static String getCompleteHtml(EsfString subject, String innerHtml)
    {
    	if ( subject.isBlank() )
    		subject.setValue("(no subject)");
    	String standardStyles = Application.getInstance().getServletResourceAsString("/static/esf/esf.css");
    	return "<html><head><title>" + subject.toHtml() + "</title><base href=\"${CONTEXTPATH}/\"/><style type=\"text/css\">" + standardStyles +
    			"</style></head><body class=\"esf\"><div class=\"pagediv\">" + innerHtml + "</div></body></html>";
    }
    public void setEmailHtml(String v)
    {
    	emailHtml = EsfString.truncateTrimmedString(v,Literals.EMAIL_HTML_MAX_LENGTH);
        objectChanged();
    }
    
    /**
     * This is a "minimalist" validation check to ensure that we have enough to do a send.
     * @return
     */
    public boolean isSendAllowed(Errors errors)
    {
    	if ( EsfString.isBlank(emailSubject) )
    	{
    		if ( errors != null )
    			errors.addError("You need to supply the SUBJECT.");
    		return false;
    	}
    	
    	if ( EsfString.areAllBlank(emailTo,emailCc,emailBcc) )
    	{
    		if ( errors != null )
    			errors.addError("You need to supply a TO, CC and/or BCC recipient.");
    		return false;
    	}
    	
    	if ( EsfString.areAllBlank(emailText, emailHtml) )
    	{
    		if ( errors != null )
    			errors.addError("You need to supply a the TEXT and/or HTML message to send.");
    		return false;
    	}
    	
    	return true;
    }
    public final boolean isSendAllowed()
    {
    	return isSendAllowed(null);
    }
    
    public List<OutboundEmailMessageAttachment> getAttachments()
    {
    	return attachments;
    }
    public boolean hasAttachments()
    {
    	return attachments != null && attachments.size() > 0;
    }
    public int getNumAttachments()
    {
    	return attachments == null ? 0 : attachments.size();
    }
    public void addAttachment(OutboundEmailMessageAttachment oneAttachment)
    {
    	oneAttachment.setOutboundEmailMessageId(id);
    	attachments.add(oneAttachment);
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof OutboundEmailMessage )
        {
        	OutboundEmailMessage other = (OutboundEmailMessage)o;
            return getId().equals(other.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(OutboundEmailMessage o)
    {
    	return getId().compareTo(o.getId());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; insert: " + doInsert());
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( doInsert() )
            {                    
        		short attOrder = 1;
                for( OutboundEmailMessageAttachment att : attachments )
                	att.save(con,attOrder++);

                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_outbound_email_message (id,transaction_id,transaction_party_id,user_id,created_timestamp,sent_timestamp,send_status,bounce_correlation_code," +
                		"email_from,email_to,email_cc,email_bcc,email_subject,email_text,email_html) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(transactionId);
                stmt.set(transactionPartyId);
                stmt.set(userId);
                stmt.set(createdTimestamp);
                stmt.set(sentTimestamp);
                stmt.set(sendStatus);
                stmt.set(bounceCorrelationCode);
                stmt.set(emailFrom);
                stmt.set(emailTo);
                stmt.set(emailCc);
                stmt.set(emailBcc);
                stmt.set(emailSubject);
                stmt.set(emailText);
                stmt.set(emailHtml);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for transactionId: " + transactionId + "; id: " + id);
                    return false;
                }

                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                
                Application.getInstance().notifyOutboundEmailProcessorMessageQueued(); // let the outbound email processor know a new message has been queued up
                
                return true;
            }
            
            if ( hasChanged() )
            {
            	// No changes to attachments allowed
            	
                stmt = new EsfPreparedStatement(con, "UPDATE esf_outbound_email_message SET sent_timestamp=?,send_status=? WHERE id=?");
                stmt.set(sentTimestamp);
                stmt.set(sendStatus);
                stmt.set(id);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for id: " + id);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; insert: " + doInsert());
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }

    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    
    
    boolean delete(final Connection con, final Errors errors)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id);
        
        clearLastSQLException();
    
        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of OutboundEmailMessage that was pending an INSERT id: " + id);
            objectDeleted();
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
            for( OutboundEmailMessageAttachment att : attachments )
            {
            	att.delete(con);
            }

            OutboundEmailMessageResponse.Manager.deleteAllForOutboundEmailMessage(con, id);
            
            stmt = new EsfPreparedStatement(con,"DELETE FROM esf_outbound_email_message WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            objectDeleted();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on id: " + id);
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public boolean delete()
    {
        Errors nullErrors = null;
        return delete(nullErrors);
    }
        
   	public static class Manager
   	{
   		public static void deleteAllForTransaction(Connection con, EsfUUID transactionId)
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	List<EsfUUID> ids = new LinkedList<EsfUUID>();
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_outbound_email_message WHERE transaction_id=? AND user_id IS NULL");
   	        	stmt.set(transactionId);
   	        	EsfResultSet rs = stmt.executeQuery();
   	        	while( rs.next() )
   	        		ids.add(rs.getEsfUUID());
   	        	
    	        stmt.close();
   	        	stmt = new EsfPreparedStatement( con, "DELETE FROM esf_outbound_email_message WHERE id=?" );
   	        	for( EsfUUID id : ids )
   	        	{
   	        		// Delete all of the responses to our email
   	        		OutboundEmailMessageResponse.Manager.deleteAllForOutboundEmailMessage(con, id);

   	        		// Delete all of the attachments to our email
   	   	        	List<OutboundEmailMessageAttachment> attachments = OutboundEmailMessageAttachment.Manager.getAllForOutboundEmailMessage(con, id);
   	   	        	for( OutboundEmailMessageAttachment att : attachments )
   	   	        		att.delete(con);

   	        		// Delete the email itself
   	    	        stmt.set(id);
   	    	        stmt.executeUpdate();
   	        	}
   	        	
    	        // For any that remain, remove the link to the transaction
    	        stmt.close();
   	        	stmt = new EsfPreparedStatement( con, "UPDATE esf_outbound_email_message SET transaction_id=NULL WHERE transaction_id=?" );
    	        stmt.set(transactionId);
    	        stmt.executeUpdate();
    	    }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.deleteAllForTransaction(con) transactionId: " + transactionId);
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }
   		}
   		
   		public static void deleteAllForUser(Connection con, EsfUUID userId)
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	List<EsfUUID> ids = new LinkedList<EsfUUID>();
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_outbound_email_message WHERE user_id=? AND transaction_id IS NULL");
   	        	stmt.set(userId);
   	        	EsfResultSet rs = stmt.executeQuery();
   	        	while( rs.next() )
   	        	{
   	        		ids.add(rs.getEsfUUID());
   	        	}

    	        stmt.close();
   	        	stmt = new EsfPreparedStatement( con, "DELETE FROM esf_outbound_email_message WHERE id=?" );
   	        	for( EsfUUID id : ids )
   	        	{
   	        		// Delete all of the responses to our email
   	        		OutboundEmailMessageResponse.Manager.deleteAllForOutboundEmailMessage(con, id);
   	        		// Delete all of the attachments to our email
   	        		
   	        		// Delete all of the attachments to our email
   	   	        	List<OutboundEmailMessageAttachment> attachments = OutboundEmailMessageAttachment.Manager.getAllForOutboundEmailMessage(con, id);
   	   	        	for( OutboundEmailMessageAttachment att : attachments )
   	   	        		att.delete(con);

   	   	        	// Delete the email itself
   	    	        stmt.set(id);
   	    	        stmt.executeUpdate();
   	        	}
   	        	
    	        // For any that remain, remove the link to the user
    	        stmt.close();
   	        	stmt = new EsfPreparedStatement( con, "UPDATE esf_outbound_email_message SET user_id=NULL WHERE user_id=?" );
    	        stmt.set(userId);
    	        stmt.executeUpdate();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.deleteAllForUser(con) userId: " + userId);
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }
   		}
   		
   		public static OutboundEmailMessage getByBounceCorrelationCode(String bounceCorrelationCode)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
  	        	// they are only stored in lowercase, but the outside may not have done this since they are parsed from email addresses
   	        	bounceCorrelationCode = bounceCorrelationCode.toLowerCase(); 
   	        	
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT id,transaction_id,transaction_party_id,user_id,created_timestamp,sent_timestamp,send_status," +
   	        			"email_from,email_to,email_cc,email_bcc,email_subject,email_text,email_html " +
   	        			"FROM esf_outbound_email_message WHERE bounce_correlation_code=?"
   	        									);
  	        	stmt.set(bounceCorrelationCode); 
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	EsfUUID transactionId = rs.getEsfUUID();
	            	EsfUUID transactionPartyId = rs.getEsfUUID();
	            	EsfUUID userId = rs.getEsfUUID();
	            	EsfDateTime createdTimestamp = rs.getEsfDateTime();
	            	EsfDateTime sentTimestamp = rs.getEsfDateTime();
	            	String sendStatus = rs.getString();
	            	String emailFrom = rs.getString();
	            	String emailTo = rs.getString();
	            	String emailCc = rs.getString();
	            	String emailBcc = rs.getString();
	            	String emailSubject = rs.getString();
	            	String emailText = rs.getString();
	            	String emailHtml = rs.getString();
	            	
	            	List<OutboundEmailMessageAttachment> attachments = OutboundEmailMessageAttachment.Manager.getAllForOutboundEmailMessage(id);
	            	
	            	OutboundEmailMessage msg = new OutboundEmailMessage(id,bounceCorrelationCode,transactionId,transactionPartyId,userId,createdTimestamp,sentTimestamp,sendStatus,
	            			emailFrom,emailTo,emailCc,emailBcc,emailSubject,emailText,emailHtml,attachments);
	            	msg.setLoadedFromDb();
	            	con.commit();
	            	
	            	return msg;
	            }
	            
   	            con.rollback();
   	            
   	            return null;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByBounceCorrelationCode() - bounceCorrelationCode: " + bounceCorrelationCode);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }
	            
   	        return null;
   		}
   		
   		public static List<OutboundEmailMessage> getAllUnsent(Connection con, boolean forUpdate)
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<OutboundEmailMessage> list = new LinkedList<OutboundEmailMessage>();
   	        	
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT id,transaction_id,transaction_party_id,user_id,created_timestamp,sent_timestamp,send_status,bounce_correlation_code," +
   	        			"email_from,email_to,email_cc,email_bcc,email_subject,email_text,email_html " +
   	        			"FROM esf_outbound_email_message WHERE sent_timestamp IS NULL ORDER BY created_timestamp" +
   	        			(forUpdate ? " FOR UPDATE" : "")
   	        									);
    	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	EsfUUID transactionId = rs.getEsfUUID();
	            	EsfUUID transactionPartyId = rs.getEsfUUID();
	            	EsfUUID userId = rs.getEsfUUID();
	            	EsfDateTime createdTimestamp = rs.getEsfDateTime();
	            	EsfDateTime sentTimestamp = rs.getEsfDateTime();
	            	String sendStatus = rs.getString();
	            	String bounceCorrelationCode = rs.getString();
	            	String emailFrom = rs.getString();
	            	String emailTo = rs.getString();
	            	String emailCc = rs.getString();
	            	String emailBcc = rs.getString();
	            	String emailSubject = rs.getString();
	            	String emailText = rs.getString();
	            	String emailHtml = rs.getString();
	            	
	            	List<OutboundEmailMessageAttachment> attachments = OutboundEmailMessageAttachment.Manager.getAllForOutboundEmailMessage(id);
	            	
	            	OutboundEmailMessage msg = new OutboundEmailMessage(id,bounceCorrelationCode,transactionId,transactionPartyId,userId,createdTimestamp,sentTimestamp,sendStatus,
	            			emailFrom,emailTo,emailCc,emailBcc,emailSubject,emailText,emailHtml,attachments);
	            	msg.setLoadedFromDb();
	            	list.add(msg);
	            }
	            
   	            return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAllUnsent(con)");
   	            list.clear();
   	            return list;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }
   		}
   		
   		public static List<OutboundEmailMessage> getAllUnsent()
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        List<OutboundEmailMessage> list = null;
   	        	
   	        try
   	        {
   	        	list = getAllUnsent(con,false);
   	            con.commit();
   	            
   	            return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAllUnsent()");
   	            pool.rollbackIgnoreException(con,e);
   	            return list;
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }
   		}
   		
   		public static List<OutboundEmailMessage> getMatching(Connection con, EsfDateTime fromDateTime, EsfDateTime toDateTime, String linkIdType, EsfUUID linkId)
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<OutboundEmailMessage> list = new LinkedList<OutboundEmailMessage>();
   	        	
			StringBuilder sqlBuf = new StringBuilder(1000);
			sqlBuf.append("SELECT id,transaction_id,transaction_party_id,user_id,created_timestamp,sent_timestamp,send_status,bounce_correlation_code,");
			sqlBuf.append("email_from,email_to,email_cc,email_bcc,email_subject,email_text,email_html ");
			sqlBuf.append("FROM esf_outbound_email_message ");
			sqlBuf.append("WHERE ");
			
			if ( LINK_TYPE_USER.equals(linkIdType) )
				sqlBuf.append("user_id=? ");
			else if ( LINK_TYPE_TRANSACTION.equals(linkIdType) )
				sqlBuf.append("transaction_id=?");
			else if ( LINK_TYPE_TRANSACTION_PARTY.equals(linkIdType) )
				sqlBuf.append("transaction_party_id=?");
			else
				return list;
			
			if ( fromDateTime != null )
				sqlBuf.append("AND created_timestamp>=? ");
			
			if ( toDateTime != null )
				sqlBuf.append("AND created_timestamp<=? ");

			sqlBuf.append("ORDER BY created_timestamp DESC");
			
			try
   	        {
   	        	stmt = new EsfPreparedStatement( con, sqlBuf.toString());
   	        	stmt.set(linkId);
   	        	if ( fromDateTime != null )
   	        		stmt.set(fromDateTime);
   	        	if ( toDateTime != null )
   	        		stmt.set(toDateTime);
    	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	EsfUUID transactionId = rs.getEsfUUID();
	            	EsfUUID transactionPartyId = rs.getEsfUUID();
	            	EsfUUID userId = rs.getEsfUUID();
	            	EsfDateTime createdTimestamp = rs.getEsfDateTime();
	            	EsfDateTime sentTimestamp = rs.getEsfDateTime();
	            	String sendStatus = rs.getString();
	            	String bounceCorrelationCode = rs.getString();
	            	String emailFrom = rs.getString();
	            	String emailTo = rs.getString();
	            	String emailCc = rs.getString();
	            	String emailBcc = rs.getString();
	            	String emailSubject = rs.getString();
	            	String emailText = rs.getString();
	            	String emailHtml = rs.getString();
	            	
	            	List<OutboundEmailMessageAttachment> attachments = OutboundEmailMessageAttachment.Manager.getAllForOutboundEmailMessage(id);
	            	
	            	OutboundEmailMessage msg = new OutboundEmailMessage(id,bounceCorrelationCode,transactionId,transactionPartyId,userId,createdTimestamp,sentTimestamp,sendStatus,
	            			emailFrom,emailTo,emailCc,emailBcc,emailSubject,emailText,emailHtml,attachments);
	            	msg.setLoadedFromDb();
	            	list.add(msg);
	            }
	            
   	            return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getMatching(con)");
   	            list.clear();
   	            return list;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }
   		}
   		
   		public static List<OutboundEmailMessage> getMatching(EsfDateTime fromDateTime, EsfDateTime toDateTime, String idType, EsfUUID id)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        List<OutboundEmailMessage> list = null;
   	        	
   	        try
   	        {
   	        	list = getMatching(con, fromDateTime, toDateTime, idType, id);
   	            con.commit();
   	            
   	            return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getMatching()");
   	            pool.rollbackIgnoreException(con,e);
   	            return list;
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }
   		}
   		
   		public static OutboundEmailMessage createNew(Transaction transaction, DocumentVersion documentVersion, TransactionParty tranParty, EsfUUID userId, EmailTemplateVersion emailTemplateVersion, List<StringReplacement.FromToSpec> replacementSpecs)
   		{
   			OutboundEmailMessage email = new OutboundEmailMessage(transaction.getId(), documentVersion, tranParty == null ? null : tranParty.getId(), userId, emailTemplateVersion,replacementSpecs);
   			return email;
   		}

   		public static OutboundEmailMessage createNew(EsfUUID userId, EmailTemplateVersion emailTemplateVersion, List<StringReplacement.FromToSpec> replacementSpecs)
   		{
   			OutboundEmailMessage email = new OutboundEmailMessage(userId,emailTemplateVersion,replacementSpecs);
   			return email;
   		}
   		public static OutboundEmailMessage createNew(EsfUUID userId, EmailTemplateVersion emailTemplateVersion, String fromString, String toString)
   		{
   			List<StringReplacement.FromToSpec> replacementSpecs = new LinkedList<StringReplacement.FromToSpec>();
   			replacementSpecs.add( new StringReplacement.FromToSpec(fromString,toString) );
   			return createNew(userId,emailTemplateVersion,replacementSpecs);
   		}

   	} // Manager
   	
}