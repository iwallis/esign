// (c) Copyright 2011 Yozons, Inc. All rights reserved.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.email;

import java.io.ByteArrayInputStream;

/**
 * A byte array data source (per Java Activation framework) used in adding byte array data parts.
 *
 * @author Yozons, Inc.
 */
public class ByteArrayDataSource 
	implements javax.activation.DataSource
{
	protected byte[] barray      = null;
	protected String contentType = null;
	protected String name        = null;
	
	/**
	 * Creates a ByteArrayDataSource using the specified byte array with the content type of
	 * application/octet-stream
	 * @param b the byte array to be the data source
	 */
	public ByteArrayDataSource(byte[] b)
	{
		this(b,"application/octet-stream","unnamed");
	}
	
	/**
	 * Creates a ByteArrayDataSource using the specified byte array and content type.
	 * @param b the byte array to be the data source
	 * @param contentType the String content type that represents the data in the byte array
	 * @param name the String name of this data (like a filename if it represented file data)
	 */
	public ByteArrayDataSource(byte[] b, String contentType, String name)
	{
		barray           = b;
		this.contentType = contentType;
		this.name        = name;
	}

	public java.io.InputStream getInputStream()
		throws java.io.IOException
	{
		return new ByteArrayInputStream(barray);
	}
	
	public java.io.OutputStream getOutputStream()
		throws java.io.IOException
	 {
	 	throw new java.io.IOException("Not implemented for com.esignforms.open.email.ByteArrayDataSource");
	 }
	 
	public java.lang.String getContentType()
	{
		return contentType;
	}
	
	public java.lang.String getName()
	{
		return name;
	}
} 
