// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.email;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;

/**
* OutboundEmailMessageAttachment holds all the info needed for a file attachment to the OutboundEmailMessage.
* 
* @author Yozons, Inc.
*/
public class OutboundEmailMessageAttachment
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<OutboundEmailMessageAttachment>
{
 	private static final long serialVersionUID = -6536154548324771271L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(OutboundEmailMessageAttachment.class);

    protected final EsfUUID id;
    protected EsfUUID outboundEmailMessageId; // The outbound email message this attachment is associated with, set when attached to an outbound message
    
    protected int fileSize;
    protected String fileName;
    protected String fileMimeType;
    protected EsfUUID fileDataBlobId;
    
    protected byte[] fileData; // only loaded when needed
    
    
    /**
     * This creates a OutboundEmailMessageAttachment object from data retrieved from the DB.
     */
    protected OutboundEmailMessageAttachment(EsfUUID id, EsfUUID outboundEmailMessageId, int fileSize, String fileName, String fileMimeType, EsfUUID fileDataBlobId )
    {
        this.id = id;
        this.outboundEmailMessageId = outboundEmailMessageId;
    	this.fileSize = fileSize;
        this.fileName = EsfString.ensureTrimmedLength(fileName,Literals.FILE_NAME_MAX_LENGTH);
        this.fileMimeType = EsfString.ensureTrimmedLength(fileMimeType,Literals.MIME_TYPE_MAX_LENGTH);
        this.fileDataBlobId = fileDataBlobId;
        this.fileData = null;
    }
    
    protected OutboundEmailMessageAttachment(String fileName, String fileMimeType, byte[] fileData)
    {
    	this.id = new EsfUUID();
    	this.outboundEmailMessageId = null;
        this.fileSize = fileData.length;
        this.fileName = EsfString.ensureTrimmedLength(fileName,Literals.FILE_NAME_MAX_LENGTH);
        this.fileMimeType = EsfString.ensureTrimmedLength(fileMimeType,Literals.MIME_TYPE_MAX_LENGTH);
        this.fileDataBlobId = new EsfUUID();
        this.fileData = fileData;
    }

   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getOutboundEmailMessageId()
    {
        return outboundEmailMessageId;
    }
    protected void setOutboundEmailMessageId(EsfUUID outboundEmailMessageId)
    {
    	this.outboundEmailMessageId = outboundEmailMessageId;
    }
    
	public int getFileSize()
	{
		return fileSize;
	}
    
    public String getFileName()
    {
    	return fileName;
    }

    public String getFileMimeType()
    {
    	return fileMimeType;
    }
    public boolean isContentTypeBrowserSafe()
    {
        return Application.getInstance().isContentTypeBrowserSafe(fileMimeType);
    }
    public boolean isContentTypePDF()
    {
    	return Application.CONTENT_TYPE_PDF.equals(fileMimeType);
    }
    
    public EsfUUID getFileDataBlobId()
    {
        return fileDataBlobId;
    }

    public byte[] getFileData()
    {
        return fileData;
    }
    public boolean isFileDataLoaded()
    {
    	return fileData != null;
    }
    
    public byte[] getFileDataFromDatabase(Connection con)
    	throws SQLException
    {
    	return Application.getInstance().getBlobDb().select(con, fileDataBlobId);
    }
	public byte[] getFileDataFromDatabase()
	{
		if ( isFileDataLoaded() )
			return fileData;
		
	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        byte[] data = getFileDataFromDatabase(con);
	        con.commit();
	        return data;
	    }
	    catch(SQLException e) 
	    {
	        pool.rollbackIgnoreException(con,e);
	        return null;
	    }
	    finally
	    {
	    	Application.getInstance().cleanupPool(pool,con,null);
	    }
	}
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof OutboundEmailMessageAttachment )
        {
        	OutboundEmailMessageAttachment other = (OutboundEmailMessageAttachment)o;
            return getId().equals(other.getId());
        }
        return false;
    }
    
    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(OutboundEmailMessageAttachment o)
    {
    	return getId().compareTo(o.getId());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, short order)
	    throws SQLException
	{
		_logger.debug("save(con) on id: " + id + "; outboundEmailMessageId: " + outboundEmailMessageId + "; fileName: " + fileName + "; order: " + order + "; insert: " + doInsert());
	    
	    clearLastSQLException();
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        if ( doInsert() )
	        {
				getBlobDb().insert(con, fileDataBlobId, fileData, BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE);
				
				stmt = new EsfPreparedStatement(con, 
	            	"INSERT INTO esf_outbound_email_message_attachment (id,outbound_email_message_id,file_order,file_name,file_mime_type,file_blob_id) VALUES (?,?,?,?,?,?)");
	            stmt.set(id);
	            stmt.set(outboundEmailMessageId);
	            stmt.set(order);
	            stmt.set(fileName);
	            stmt.set(fileMimeType);
	            stmt.set(fileDataBlobId);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Insert failed for id: " + id + "; outboundEmailMessageId: " + outboundEmailMessageId + "; fileName: " + fileName + "; order: " + order + "; insert: " + doInsert());
	                return false;
	            }
	            
	            // Let's clear out our byte buffer so as not to consume memory holding the file any longer
	            fileData = null;
	            
	            // Now we mark this object as if it were loaded fresh from the database
	            setLoadedFromDb();
	            
	            return true;
	        }
	        
	        // There should be no way to update a file attachment -- just insert and delete
	        
	        // Now we mark this object as if it were loaded fresh from the database
	        setLoadedFromDb();
	
	        return true;
	    }
	    catch(SQLException e)
	    {
	    	_logger.sqlerr(e,"save(con) on id: " + id + "; outboundEmailMessageId: " + outboundEmailMessageId + "; fileName: " + fileName + "; order: " + order + "; insert: " + doInsert());
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}
	
	public boolean save(short order)
	{
	    ConnectionPool    pool = getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        boolean result = save(con,order);
			if ( result )
				con.commit();
			else
				con.rollback();
	        return result;
	    }
	    catch(SQLException e) 
	    {
	        pool.rollbackIgnoreException(con,e);
	        return false;
	    }
	    finally
	    {
	        cleanupPool(pool,con,null);
	    }
	}
	
	
	public synchronized boolean delete(final Connection con)
	    throws SQLException
	{
		_logger.debug("delete(con) on id: " + id + "; outboundEmailMessageId: " + outboundEmailMessageId + "; fileName: " + fileName);
	    
	    clearLastSQLException();
	
	    if ( doInsert() )
	    {
	    	_logger.warn("delete(con) - Ignored delete of file that was pending an INSERT id: " + id + "; outboundEmailMessageId: " + outboundEmailMessageId + "; fileName: " + fileName);
	        objectDeleted();
	        return true;
	    }
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        // Delete the file data itself
	    	getBlobDb().delete(con,fileDataBlobId);
	        
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_outbound_email_message_attachment WHERE id=?");
	        stmt.set(id);
	        stmt.executeUpdate();
	        
	        objectDeleted();
	
	        return true;
	    }
	    catch(SQLException e)
	    {
	    	_logger.sqlerr(e,"delete(con) on id: " + id + "; outboundEmailMessageId: " + outboundEmailMessageId + "; fileName: " + fileName);
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}
	
	public boolean delete()
	{
	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        boolean result = delete(con);
	        con.commit();
	        return result;
	    }
	    catch(SQLException e) 
	    {
	        pool.rollbackIgnoreException(con,e);
	        return false;
	    }
	    finally
	    {
	    	Application.getInstance().cleanupPool(pool,con,null);
	    }
	}

	public static class Manager
	{
		public static List<OutboundEmailMessageAttachment> getAllForOutboundEmailMessage(Connection con, EsfUUID outboundEmailMessageId) throws SQLException
		{
			EsfPreparedStatement stmt = null;

			LinkedList<OutboundEmailMessageAttachment> list = new LinkedList<OutboundEmailMessageAttachment>();
			
	        try
	        {
	        	stmt = new EsfPreparedStatement( con,
	        			"SELECT esf_outbound_email_message_attachment.id,file_name,file_mime_type,file_blob_id,esf_blob.orig_size " +
	        			"FROM esf_outbound_email_message_attachment,esf_blob WHERE outbound_email_message_id=? AND file_blob_id=esf_blob.id ORDER BY file_order"
	        									);
	        	stmt.set(outboundEmailMessageId);
	        	
            EsfResultSet rs = stmt.executeQuery();
            while ( rs.next() )
            {
            	EsfUUID id = rs.getEsfUUID();
	            String fileName = rs.getString();
	            String fileMimeType = rs.getString();
	            EsfUUID fileDataBlobId = rs.getEsfUUID();
	            int fileSize = rs.getInt();
	            
	            // We don't load the file itself unless requested specifically (for a download or send of email itself)
	            
	            OutboundEmailMessageAttachment att = new OutboundEmailMessageAttachment(id,outboundEmailMessageId,fileSize,fileName,fileMimeType,fileDataBlobId);
	            att.setLoadedFromDb();

	            list.add(att);
            }
	        }
	        catch(SQLException e) 
	        {
	        	_logger.sqlerr(e,"Manager.getAllForOutboundEmailMessage() - outboundEmailMessageId: " + outboundEmailMessageId);
	        	throw e;
	        }
	        finally
	        {
	            cleanupStatement(stmt);
	        }

	        return list;
		}
		
		public static List<OutboundEmailMessageAttachment> getAllForOutboundEmailMessage(EsfUUID outboundEmailMessageId) 
		{
	        ConnectionPool    pool = getConnectionPool();
	        Connection        con  = pool.getConnection();
	        
	        try
	        {
	        	List<OutboundEmailMessageAttachment> list = getAllForOutboundEmailMessage(con,outboundEmailMessageId);
	        	con.commit();
	        	return list;
	        }
	        catch(SQLException e) 
	        {
	            pool.rollbackIgnoreException(con,e);
	        }
	        finally
	        {
	            cleanupPool(pool,con,null);
	        }

	        return null; 
		}
		
		public static OutboundEmailMessageAttachment createNew(String fileName, String fileMimeType, byte[] fileData)
		{
			return new OutboundEmailMessageAttachment(fileName, fileMimeType, fileData);
		}
	    
	} // Manager
	
}