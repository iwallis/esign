// (c) Copyright 2011 Yozons, Inc. All rights reserved.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//

package com.esignforms.open.email;

import java.util.LinkedList;
import java.util.TreeSet;

import javax.mail.internet.InternetAddress;

import com.esignforms.open.Application;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfString;


/**
 *  Contains the list of recipients (RecipientUser) objects for TO, CC and BCC.  It is guaranteed
 *  that the list does not contain duplicates.  Duplicates are REMOVED in order of preference, such
 *  that: if is FROM, then not in TO, CC or BCC; if in TO, the not in CC or BCC; if in CC, then not in BCC.
 *  It is possible to retrieve the list of duplicates, as well as the list of email addresses that
 *  were not valid if any.
 *  
 * @author Yozons, Inc.
 */
public class RecipientList
{
    protected TreeSet<EsfEmailAddress> emailSet     = new TreeSet<EsfEmailAddress>();
    protected TreeSet<EsfEmailAddress> duplicateSet = null;
    protected TreeSet<EsfEmailAddress> invalidSet   = null;
   
    /**
     * Use of this constructor means you have to call the addRecipientUser() method for each
     * one, then call addComplete() before you can use the object.
     */
    public RecipientList()
    {
    }
   
    public RecipientList(String emailList)
    {
        createSet(emailList);
    }
    
    /**
     * Only should be called if using the no-arg constructor.
     */
    public void add(EsfEmailAddress user)
    {
        addToEmailSet(user);
    }
    
    public EsfEmailAddress[] getEmailAddressesArray()
    {
        return createArray(emailSet);
    }
    public java.util.Set<EsfEmailAddress> getEmailAddresses()
    {
        return emailSet;
    }
    public int getNumRecipients()
    {
        return emailSet.size();
    }
    public boolean hasEmailAddresses()
    {
        return getNumRecipients() > 0;
    }
    
    public InternetAddress[] getEmailInternetAddresses()
    {
        if ( ! hasEmailAddresses() )
            return new InternetAddress[0];
        
        InternetAddress[] iaddrs = new InternetAddress[emailSet.size()];

        int i = 0;
        for( EsfEmailAddress addr : emailSet )
        {
            iaddrs[i++] = SimpleEmailer.CreateInternetAddress(addr.getQuotedFullEmailAddress());
        }
        
        return iaddrs;
    }

    
    public String getEmailText()
    {
        if ( ! hasEmailAddresses() )
            return "";
        
        StringBuilder buf = new StringBuilder(Literals.EMAIL_ADDRESS_MAX_LENGTH * emailSet.size());
        for( EsfEmailAddress addr : emailSet )
        {
            if ( buf.length() > 0 )
                buf.append("; ");
            buf.append(addr.getQuotedFullEmailAddress());
        }
        return buf.toString();
    }
    
    public String getBasicEmailText()
    {
        if ( ! hasEmailAddresses() )
            return "";
        
        StringBuilder buf = new StringBuilder(Literals.EMAIL_ADDRESS_MAX_LENGTH * emailSet.size());
        for( EsfEmailAddress addr : emailSet )
        {
            if ( buf.length() > 0 )
                buf.append("; ");
            buf.append(addr.getEmailAddress());
        }
        return buf.toString();
    }
    
    
    public boolean hasDuplicates()
    {
        return duplicateSet != null && duplicateSet.size() > 0;
    }
    public EsfEmailAddress[] getDuplicatesArray()
    {
        return createArray(duplicateSet);
    }
    public java.util.Set<EsfEmailAddress> getDuplicates()
    {
        return duplicateSet;
    }
    public void clearDuplicates()
    {
        if ( duplicateSet != null )
            duplicateSet.clear();
        duplicateSet = null;
    }
    
    public boolean hasInvalid()
    {
        return invalidSet != null && invalidSet.size() > 0;
    }
    public EsfEmailAddress[] getInvalidArray()
    {
        return createArray(invalidSet);
    }
    public java.util.Set<EsfEmailAddress> getInvalid()
    {
        return invalidSet;
    }
    public void clearInvalid()
    {
        if ( invalidSet != null )
            invalidSet.clear();
        invalidSet = null;
    }
    
    protected EsfEmailAddress[] createArray(TreeSet<EsfEmailAddress> set)
    {
        if ( set == null )
            return new EsfEmailAddress[0];
    
        EsfEmailAddress[] array = new EsfEmailAddress[set.size()];
        set.toArray(array);
        return array;
    }
    
    protected void createSet(String list)
    {
    	if ( EsfString.isBlank(list) )
    		return;
    	
    	String[] emailAddresses = splitEmailAddresses(list);
    	for( String email : emailAddresses )
        {
            if ( EsfString.isBlank(email) ) // completely ignore empty tokens
                continue;
                
            email = email.trim();
            
            EsfEmailAddress addr = new EsfEmailAddress(email);
            
            if ( addr.isValid() )
                addToEmailSet(addr);
            else
            {
                if ( invalidSet == null )
                    invalidSet = new TreeSet<EsfEmailAddress>();
                invalidSet.add(addr);
            }
        }
    }
    
    
    // This is a specialty parser since a comma and semicolon is only a separator if not in a quoted 
    // portion of a valid email address, such as:  "Wall, David" <david.wall@yozons.com>
    protected String[] splitEmailAddresses(String list)
    {
    	boolean inQuote = false;
    	
    	StringBuilder email = new StringBuilder(Literals.EMAIL_ADDRESS_MAX_LENGTH);
    	LinkedList<String> emailList = null;
    	for( int i=0; i < list.length(); ++i )
    	{
    		char c = list.charAt(i);
    		
    		// Newlines are always separators -- can't be embedded in quotes
    		// Comma and semicolon are separates if we're not in a quoted region
    		if ( c == '\n' || c == '\r' || ( ! inQuote && (c == ',' || c == ';') ) ) 
    		{
    			inQuote = false;
    			if ( email.length() > 0 )
    			{
    				if ( emailList == null )
    					emailList = new LinkedList<String>();
    				emailList.add(email.toString().trim());
    			}
    			email.setLength(0);
    			continue;
    		}
    		else if ( c == '\"' )
    			inQuote = ! inQuote;
    		else if ( c == '\\' && i < list.length() ) // If we have a backslash there's another character following, we append both
    		{
    			++i;
    			email.append(c);
    			c = list.charAt(i);
    		}
    		
    		email.append(c);
    	}
    	
    	// If we have some data, then let's add this to the list, unless we don't have a list (only one email address)
    	// in which case we can short circuit and just return the string.
    	if ( email.length() > 0 )
    	{
    		if ( emailList == null )
    		{
    			String[] a = new String[1];
    			a[0] = email.toString();
    			return a;
    		}
    		emailList.add(email.toString().trim());
    	}
    	
    	// Convert the list to an array and return it all
    	String[] emailArray = new String[emailList.size()];
    	emailList.toArray(emailArray);
    	emailList.clear();
    	return emailArray;
    }
    
    
    protected boolean isDuplicate(EsfEmailAddress addr)
    {
        return emailSet.contains(addr);
    }
    
    
    protected final void addToEmailSet( EsfEmailAddress addr )
    {
        if ( isDuplicate(addr) )
        {
            if ( duplicateSet == null )
                duplicateSet = new TreeSet<EsfEmailAddress>();
            duplicateSet.add(addr);
        }
        else 
        {
            emailSet.add(addr);
        }
    }

    public static void main(String[] args)
    	throws Exception
    {
    	Application.getInstallationInstance("/");
    	java.io.BufferedReader in = new java.io.BufferedReader( new java.io.InputStreamReader(System.in) );
    	
    	while(true)
    	{
    		System.out.println();
    		System.out.print("Enter an email list: ");
    		String list = in.readLine();
    		RecipientList rl = new RecipientList(list);
    		if ( rl.hasDuplicates() )
    			System.out.println(">>> Has duplicates");
    		if ( rl.hasInvalid() )
    			System.out.println(">>> Has invalid");
    		EsfEmailAddress[] addrs = rl.getEmailAddressesArray();
    		for( EsfEmailAddress addr : addrs )
    		{
    			System.out.println(">>> Found email: " + addr.getQuotedFullEmailAddress());
    			System.out.println("    Name: " + addr.getDisplayName());
    			System.out.println("    Email: " + addr.getEmailAddress());
    		}
    	}
    }
}
