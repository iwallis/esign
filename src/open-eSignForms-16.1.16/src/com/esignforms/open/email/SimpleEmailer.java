// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.email;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.Multipart;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.exception.EsfException;

/**
 * This accepts sends a basic email based on FROM, TO, SUBJECT and MESSAGE parameters.
 *
 * @author Yozons, Inc.
 */
public class SimpleEmailer 
{
    private Session     session;
    private Transport   transport;
    private Application app = Application.getInstance();
    private String      smtpServer;
    private String 		smtpServerPort;
    private String      returnPath;
    private String      authUser;
    private String      authPassword;
    private boolean     doStartTLS;
    
    private OutboundEmailMessage outboundEmailMessage;
    
    public enum PRIORITY { LOW, NORMAL, HIGH }
    
    static /* initialization */
    {
    	System.setProperty("mail.mime.encodefilename","true"); // encodes attachment file names
    }

    /**
     * Instantiate a SimpleEmailer that will send out the email specified.
     * @throws Exception if there's a problem obtaining an initial SMTP transport that is needed to successfully send a message
     */
    public SimpleEmailer(OutboundEmailMessage outboundEmailMessage)
    	throws Exception
	{
    	this.outboundEmailMessage = outboundEmailMessage;
        loadSystemData();
        try
        {
            // Just for grins to ensure we have a valid setup.
            getTransport();
        }
        catch( Exception e )
        {
            app.except(e,"SimpleEmailer() Could not get initial SMTP transport to: " + smtpServer + " : " + smtpServerPort + "; returnPath: " + returnPath);
            throw e;
        }

    }
    
    private void loadSystemData()
    {
        smtpServer       		= app.getSmtpServer().toString();
        String returnPathHostName = app.getSmtpReturnPathHostName().toString();
        String bouncePrefix		= normalizePrefix(app.getImapUser().toString());
        
        returnPath 				= bouncePrefix + "_" + outboundEmailMessage.getBounceCorrelationCode() + "@" + returnPathHostName;
        smtpServerPort 			= app.getSmtpPort().toPlainString();
        authUser                = app.getSmtpAuthUser().toString();
        authPassword            = app.getSmtpAuthPassword().toString();
        doStartTLS              = app.getSmtpStartTLS().isTrue();
    }
    
    public static String normalizePrefix(String prefix)
    {
    	prefix = prefix.toLowerCase();
		StringBuilder normalizedPrefix = new StringBuilder(prefix.length());
		
		for( char c : prefix.toCharArray() )
		{
			if ( Character.isLetterOrDigit(c) || c == '.' || c == '_' || c == '-' )
				normalizedPrefix.append(c);
			else
				normalizedPrefix.append('_');
		}
		return normalizedPrefix.toString();    	
    }
    
    private Transport getTransport()
        throws javax.mail.NoSuchProviderException, javax.mail.MessagingException
    {
    	// If we have a transport, let's reuse it.
    	if ( transport != null )
    		return transport;
    	
        Properties prop = new Properties();
        prop.put("mail.smtp.host",smtpServer);
        if ( EsfString.isNonBlank(smtpServerPort) )
            prop.put("mail.smtp.port",smtpServerPort);
        if ( EsfString.isNonBlank(returnPath) )
            prop.put("mail.smtp.from",returnPath);
        if ( EsfString.isNonBlank(authUser) )
        	prop.put("mail.smtp.auth","true");
        
        session = Session.getInstance(prop);
        
        synchronized(this)
        {
			try
			{
				app.debug("SimpleEmailer.getTransport() - Getting an SMTP transport to: " + smtpServer + " : " + smtpServerPort + "; returnPath: " + returnPath);
				transport = session.getTransport("smtp");
				
				if ( transport instanceof com.sun.mail.smtp.SMTPTransport )
				{
					((com.sun.mail.smtp.SMTPTransport)transport).setStartTLS(doStartTLS);
				}
				
				if ( EsfString.isNonBlank(authUser) )
				{
					transport.connect(authUser,authPassword);
					app.info("SimpleEmailer.getTransport() - Connected SMTP transport to: " + smtpServer + "; STARTTLS: " + doStartTLS + "; SMTP AUTH user: " + authUser);
				}
				else
				{
					transport.connect();
					app.info("SimpleEmailer.getTransport() - Connected SMTP transport to: " + smtpServer + "; STARTTLS: " + doStartTLS);
				}
				return transport;
			}
			catch( javax.mail.MessagingException e )
			{
				transport = null;
				throw e;
			}
        }
    }
    
    
	private synchronized Transport getNewTransport()
        throws javax.mail.NoSuchProviderException, javax.mail.MessagingException
    {
        if ( transport != null )
        {
        	try
        	{
				app.info("SimpleEmailer.getNewTransport(): Closing old SMTP transport. Reconnecting to SMTP transport...");
				transport.close();
        	}
        	catch( Exception e ) {} // ignore any exception on the close since it's most likely down/broken anyway (that's why we're reconnecting!)
			finally
			{
				transport = null;
				app.sleep(500);
			}
        }
            
        return getTransport();
    }
    
    
    public static boolean IsValidEmailAddress( String addr )
    {
        return CreateInternetAddress(addr) != null;
    }
    

    
    public static InternetAddress CreateInternetAddress( String addr )
    {
        InternetAddress address = null;
        try
        {
            address = new InternetAddress(addr);
            return address;
        }
        catch( javax.mail.internet.AddressException e )
        {
            return null;
        }
    }
    
    public static InternetAddress[] CreateInternetAddresses( String addrList )
    {
    	RecipientList list = new RecipientList(addrList);
    	if ( list.getNumRecipients() < 1 || list.hasInvalid() )
    		return null;
    	return list.getEmailInternetAddresses();
    }


    
	public void sendHtml(String emailFrom, String emailToList, String emailSubject, String text, String html)
		throws EsfException
	{
		sendHtml(emailFrom, emailToList, emailSubject, text, html, PRIORITY.NORMAL);
	}
	
	public void sendHtml(String emailFrom, String emailToList, String emailSubject, String text, String html, PRIORITY priority)
		throws EsfException
	{
		if ( EsfString.isBlank(html) ) // if no html, just do a text one
		{
			send(emailFrom, emailToList, emailSubject, text);
			return;
		}
		
	    InternetAddress[] toAddresses = CreateInternetAddresses(emailToList);
	    if ( toAddresses == null )
			throw new EsfException("Invalid email TO address: " + emailToList );

		InternetAddress fromAddress = null;
		try
		{
			fromAddress = new InternetAddress(emailFrom);
		}
		catch( javax.mail.internet.AddressException e )
		{
			throw new EsfException("Invalid FROM email address (" + emailFrom + ") " + e.getMessage() );
		}

		sendHtmlEmail( fromAddress, toAddresses, null, null, emailSubject, text, html, priority );
	}
    
    
    
    public void send(String emailFrom, String emailToList, String emailSubject, String message)
	    throws EsfException
	{
    	send(emailFrom, emailToList, emailSubject, message, PRIORITY.NORMAL);
	}
    
    public void send(String emailFrom, String emailToList, String emailSubject, String message, PRIORITY priority)
        throws EsfException
    {
	    InternetAddress[] toAddresses = CreateInternetAddresses(emailToList);
	    if ( toAddresses == null )
			throw new EsfException("Invalid email TO address: " + emailToList );

        InternetAddress fromAddress = null;
        try
        {
            fromAddress = new InternetAddress(emailFrom);
        }
        catch( javax.mail.internet.AddressException e )
        {
            throw new EsfException("Invalid FROM email address (" + emailFrom + ") " + e.getMessage() );
        }

        sendEmail( fromAddress, toAddresses, null, null, emailSubject, message, priority );
    }
    
    
    
    public void sendBCC(String emailFrom, String[] emailTo, String emailSubject, String message)
        throws EsfException
    {
	    InternetAddress[] bccAddresses = new InternetAddress[emailTo.length];
	    int i = 0;
        try
        {
            for( i=0; i < emailTo.length; ++i )
                bccAddresses[i] = new InternetAddress(emailTo[i]);
        }
        catch( javax.mail.internet.AddressException e )
        {
            throw new EsfException("Invalid email TO address: " + emailTo[i] + " - " + e.getMessage() + " at index " + i );
        }

        InternetAddress fromAddress = null;
        try
        {
            fromAddress = new InternetAddress(emailFrom);
        }
        catch( javax.mail.internet.AddressException e )
        {
            throw new EsfException("Invalid FROM email address (" + emailFrom + ") " + e.getMessage() );
        }

        sendEmail( fromAddress, null, null, bccAddresses, emailSubject, message, PRIORITY.NORMAL );
    }
    
    public void sendEmail( String            emailFrom,
                           InternetAddress[] toAddresses,
                           InternetAddress[] ccAddresses,
                           InternetAddress[] bccAddresses,
                           String subject, 
                           String message
                         )
	    throws EsfException
    {
        InternetAddress fromAddress = null;
        try
        {
            fromAddress = new InternetAddress(emailFrom);
        }
        catch( javax.mail.internet.AddressException e )
        {
            throw new EsfException("Invalid FROM email address (" + emailFrom + ") " + e.getMessage() );
        }

        sendEmail( fromAddress, toAddresses, ccAddresses, bccAddresses, subject, message, PRIORITY.NORMAL );
    }
    
    
    public void sendEmail( InternetAddress   fromAddress,
                           InternetAddress[] toAddresses,
                           InternetAddress[] ccAddresses,
                           InternetAddress[] bccAddresses,
                           String subject, 
                           String message,
					       PRIORITY priority
                         )
    	throws EsfException
    {
    	try
        {
            MimeMessage msg = createMimeMessage();
            
			if ( priority == PRIORITY.HIGH )
			{
	            msg.setHeader("X-Priority","1");
	            msg.setHeader("X-MSMail-Priority","High");
	            msg.setHeader("Importance","High");
			}
			else if ( priority == PRIORITY.LOW )
			{
	            msg.setHeader("X-Priority","5");
	            msg.setHeader("X-MSMail-Priority","Low");
	            msg.setHeader("Importance","Low");
			}

			msg.setHeader("X-OPEN-ESIGNFORMS-AUTH","Open eSignForms e-contracting automated email from deploymentId: " + app.getDeployId());
            
			msg.setFrom(fromAddress);
            
            if ( EsfString.isNonBlank(returnPath) && ! fromAddress.getAddress().equalsIgnoreCase(returnPath) )
            {
            	InternetAddress returnPathInternetAddress = new InternetAddress(returnPath);
               	InternetAddress[] replyToInternetAddresses = new InternetAddress[1];
               	replyToInternetAddresses[0] = returnPathInternetAddress;
               	msg.setSender(returnPathInternetAddress);
                msg.setReplyTo(replyToInternetAddresses);
            }
            
            if ( toAddresses != null )
                msg.setRecipients(Message.RecipientType.TO, toAddresses);
            if ( ccAddresses != null )
                msg.setRecipients(Message.RecipientType.CC,ccAddresses);
            if ( bccAddresses != null )
                msg.setRecipients(Message.RecipientType.BCC,bccAddresses);
            if ( subject == null )
            	subject = "";
            msg.setSubject(subject,EsfString.CHARSET_UTF_8);
            msg.setContent(message, Application.CONTENT_TYPE_TEXT+Application.CONTENT_TYPE_CHARSET_UTF_8 );
            msg.saveChanges(); // implicit with send(), but not sendMessage()
            
            try
            {
            	Transport t = getTransport();
            	if ( t == null )
            		throw new Exception("SimpleEmailer.sendEmail() - Couldn't get an SMTP transport.");
                t.sendMessage( msg, msg.getAllRecipients() );
            }
            catch( Exception e2 )
            {
				Transport t = getNewTransport();
				if ( t == null )
					throw new EsfException("SimpleEmailer.sendEmail() - Couldn't get a new SMTP transport.");
                t.sendMessage( msg, msg.getAllRecipients() );
            }
        }
        catch( javax.mail.MessagingException e )
        {
            app.except(e, "SimpleEmailer.sendEmail(): Failed to send email from: " + 
                    fromAddress.getAddress() + "; subject: " + subject);
            throw new EsfException("Failed to send Email: " + e.getMessage());
        }
    }
    
    
	public void sendOutboundEmailMessage()
		throws EsfException
	{
		String fromEmail = outboundEmailMessage.hasEmailFrom() ? outboundEmailMessage.getEmailFrom() : returnPath;
    	 
		if ( outboundEmailMessage.hasAttachments() )
		{
			SendAttachmentInfo[] attachmentInfos = new SendAttachmentInfo[outboundEmailMessage.getNumAttachments()];
			int i = 0;
			for( OutboundEmailMessageAttachment att : outboundEmailMessage.getAttachments() )
				attachmentInfos[i++] = new SendAttachmentInfo(att.getFileName(),att.getFileDataFromDatabase());
    		 
			sendHtmlEmailWithAttachments( 
        			fromEmail,
  			 	    CreateInternetAddresses(outboundEmailMessage.getEmailTo()),
  			 	    CreateInternetAddresses(outboundEmailMessage.getEmailCc()),
  			 	    CreateInternetAddresses(outboundEmailMessage.getEmailBcc()),
  			 	    outboundEmailMessage.getEmailSubject(),
  			 	    outboundEmailMessage.getEmailText(),
  			 	    outboundEmailMessage.getEmailHtml(),
  			 	    attachmentInfos,
  			 	    PRIORITY.NORMAL
 			     	  					);
		}
		else
		{
			sendHtmlEmail( 
        			fromEmail,
 			 	    CreateInternetAddresses(outboundEmailMessage.getEmailTo()),
 			 	    CreateInternetAddresses(outboundEmailMessage.getEmailCc()),
 			 	    CreateInternetAddresses(outboundEmailMessage.getEmailBcc()),
 			 	    outboundEmailMessage.getEmailSubject(),
 			 	    outboundEmailMessage.getEmailText(),
 			 	    outboundEmailMessage.getEmailHtml(),
 			 	    PRIORITY.NORMAL
		     	         );
		}
	}
 
	public void sendHtmlEmail( String            emailFrom,
						       InternetAddress[] toAddresses,
						       InternetAddress[] ccAddresses,
						       InternetAddress[] bccAddresses,
						       String subject, 
						       String text,
						       String html,
						       PRIORITY priority
						     )
		throws EsfException
	{
		InternetAddress fromAddress = null;
		try
		{
			fromAddress = new InternetAddress(emailFrom);
		}
		catch( javax.mail.internet.AddressException e )
		{
			throw new EsfException("Invalid FROM email address (" + emailFrom + ") " + e.getMessage() );
		}

		sendHtmlEmail( fromAddress, toAddresses, ccAddresses, bccAddresses, subject, text, html, priority );
	}
    
    
    
	public void sendHtmlEmail( InternetAddress   fromAddress,
						       InternetAddress[] toAddresses,
						       InternetAddress[] ccAddresses,
						       InternetAddress[] bccAddresses,
						       String subject, 
						       String text,
						       String html,
						       PRIORITY priority
						     )
		throws EsfException
	{
		try
		{
			Multipart mp = new MimeMultipart("alternative");
			MimeBodyPart textPart = new MimeBodyPart();
			MimeBodyPart htmlPart = new MimeBodyPart();
			textPart.setContent(text,Application.CONTENT_TYPE_TEXT+Application.CONTENT_TYPE_CHARSET_UTF_8);
			htmlPart.setContent(html,Application.CONTENT_TYPE_HTML+Application.CONTENT_TYPE_CHARSET_UTF_8);
			if ( EsfString.isNonBlank(text) )
				mp.addBodyPart(textPart);
			if ( EsfString.isNonBlank(html) )
				mp.addBodyPart(htmlPart);
			
			MimeMessage msg = createMimeMessage();
            
			if ( priority == PRIORITY.HIGH )
			{
	            msg.setHeader("X-Priority","1");
	            msg.setHeader("X-MSMail-Priority","High");
	            msg.setHeader("Importance","High");
			}
			else if ( priority == PRIORITY.LOW )
			{
	            msg.setHeader("X-Priority","5");
	            msg.setHeader("X-MSMail-Priority","Low");
	            msg.setHeader("Importance","Low");
			}
			
			msg.setHeader("X-OPEN-ESIGNFORMS-AUTH","Open eSignForms e-contracting automated email from deploymentId: " + app.getDeployId());

			msg.setFrom(fromAddress);

            if ( EsfString.isNonBlank(returnPath) && ! fromAddress.getAddress().equalsIgnoreCase(returnPath) )
            {
            	InternetAddress returnPathInternetAddress = new InternetAddress(returnPath);
               	InternetAddress[] replyToInternetAddresses = new InternetAddress[1];
               	replyToInternetAddresses[0] = returnPathInternetAddress;
               	msg.setSender(returnPathInternetAddress);
                msg.setReplyTo(replyToInternetAddresses);
            }

            if ( toAddresses != null )
				msg.setRecipients(Message.RecipientType.TO, toAddresses);
			if ( ccAddresses != null )
				msg.setRecipients(Message.RecipientType.CC,ccAddresses);
			if ( bccAddresses != null )
				msg.setRecipients(Message.RecipientType.BCC,bccAddresses);
            if ( subject == null )
            	subject = "";
			msg.setSubject(subject,EsfString.CHARSET_UTF_8);
			msg.setContent(mp);
			msg.saveChanges(); // implicit with send(), but not sendMessage()
            
			try
			{
				Transport t = getTransport();
				if ( t == null )
					throw new Exception("SimpleEmailer.sendHtmlEmail() - Couldn't get an SMTP transport.");
				t.sendMessage( msg, msg.getAllRecipients() );
			}
			catch( Exception e2 )
			{
				Transport t = getNewTransport();
				if ( t == null )
					throw new EsfException("SimpleEmailer.sendHtmlEmail() - Couldn't get a new SMTP transport.");
				t.sendMessage( msg, msg.getAllRecipients() );
			}
		}
		catch( javax.mail.MessagingException e )
		{
			app.except(e, "SimpleEmailer.sendHtmlEmail(): Failed to send email from: " + 
					fromAddress.getAddress() + "; subject: " + subject);

			StringBuilder buf = new StringBuilder(1024);
			buf.append("Send email failed: ");
			buf.append(e.getMessage());
			Exception nextException = e.getNextException();
			if ( nextException != null )
			{
				buf.append(" - ");
				buf.append(nextException.getMessage());
			}
			throw new EsfException(buf.toString());
		}
	}
    
    
	public void sendHtmlEmailWithAttachments( 
			   String emailFrom,
		       InternetAddress[] toAddresses,
		       InternetAddress[] ccAddresses,
		       InternetAddress[] bccAddresses,
		       String subject, 
		       String text,
		       String html,
		       SendAttachmentInfo[] attachmentInfos,
		       PRIORITY priority
		     )
		throws EsfException
	{
		InternetAddress fromAddress = null;
		try
		{
			fromAddress = new InternetAddress(emailFrom);
		}
		catch( javax.mail.internet.AddressException e )
		{
			throw new EsfException("Invalid FROM email address (" + emailFrom + ") " + e.getMessage() );
		}

		sendHtmlEmailWithAttachments( fromAddress, toAddresses, ccAddresses, bccAddresses, subject, text, html, attachmentInfos, priority );
	}


	public void sendHtmlEmailWithAttachments( 
			   InternetAddress fromAddress,
		       InternetAddress[] toAddresses,
		       InternetAddress[] ccAddresses,
		       InternetAddress[] bccAddresses,
		       String subject, 
		       String text,
		       String html,
		       SendAttachmentInfo[] attachmentInfos,
		       PRIORITY priority
		     )
		throws EsfException
	{
		try
		{
			MimeMultipart mp = new MimeMultipart(); // our main multipart for the message

	    	// Since we have an attachment, our first "part" is actually going to be a MultiPart "alternative"
	    	// to hold the text and HTML message, and the second part will be the attachment.
			Multipart mpAlternative = new MimeMultipart("alternative");
			MimeBodyPart alternativePart = new MimeBodyPart();
			
			MimeBodyPart textPart = new MimeBodyPart();
			MimeBodyPart htmlPart = new MimeBodyPart();
			textPart.setContent(text,Application.CONTENT_TYPE_TEXT+Application.CONTENT_TYPE_CHARSET_UTF_8);
			htmlPart.setContent(html,Application.CONTENT_TYPE_HTML+Application.CONTENT_TYPE_CHARSET_UTF_8);
			if ( EsfString.isNonBlank(text) )
				mpAlternative.addBodyPart(textPart);
			if ( EsfString.isNonBlank(html) )
				mpAlternative.addBodyPart(htmlPart);
			alternativePart.setContent(mpAlternative);
			mp.addBodyPart(alternativePart);
	
			MimeMessage msg = createMimeMessage();
	
			if ( priority == PRIORITY.HIGH )
			{
				msg.setHeader("X-Priority","1");
				msg.setHeader("X-MSMail-Priority","High");
				msg.setHeader("Importance","High");
			}
			else if ( priority == PRIORITY.LOW )
			{
				msg.setHeader("X-Priority","5");
				msg.setHeader("X-MSMail-Priority","Low");
				msg.setHeader("Importance","Low");
			}
	
			msg.setHeader("X-OPEN-ESIGNFORMS-AUTH","Open eSignForms e-contracting automated email from deploymentId: " + app.getDeployId());
	
			msg.setFrom(fromAddress);
	
			if ( EsfString.isNonBlank(returnPath) && ! fromAddress.getAddress().equalsIgnoreCase(returnPath) )
			{
				InternetAddress returnPathInternetAddress = new InternetAddress(returnPath);
				InternetAddress[] replyToInternetAddresses = new InternetAddress[1];
				replyToInternetAddresses[0] = returnPathInternetAddress;
				msg.setSender(returnPathInternetAddress);
				msg.setReplyTo(replyToInternetAddresses);
			}
	
			if ( toAddresses != null )
				msg.setRecipients(Message.RecipientType.TO, toAddresses);
			if ( ccAddresses != null )
				msg.setRecipients(Message.RecipientType.CC,ccAddresses);
			if ( bccAddresses != null )
				msg.setRecipients(Message.RecipientType.BCC,bccAddresses);
			if ( subject == null )
				subject = "";
			msg.setSubject(subject,EsfString.CHARSET_UTF_8);
			
	    	// Add the attachments
	    	for( SendAttachmentInfo attachmentInfo : attachmentInfos )
	    	{
	    		MimeBodyPart attachmentPart = new MimeBodyPart();
		    	ByteArrayDataSource source = new ByteArrayDataSource(attachmentInfo.attachment,app.getContentTypeByFileName(attachmentInfo.attachmentFileName),attachmentInfo.attachmentFileName);
		    	attachmentPart.setDataHandler(new javax.activation.DataHandler(source));
		    	attachmentPart.setFileName(attachmentInfo.attachmentFileName);
		    	mp.addBodyPart(attachmentPart);
	    	}
			
			msg.setContent(mp);
			msg.saveChanges(); // implicit with send(), but not sendMessage()
	
			try
			{
				Transport t = getTransport();
				if ( t == null )
					throw new Exception("SimpleEmailer.sendHtmlEmailWithAttachments() - Couldn't get an SMTP transport.");
				t.sendMessage( msg, msg.getAllRecipients() );
			}
			catch( Exception e2 )
			{
				Transport t = getNewTransport();
				if ( t == null )
					throw new EsfException("SimpleEmailer.sendHtmlEmailWithAttachments() - Couldn't get a new SMTP transport.");
				t.sendMessage( msg, msg.getAllRecipients() );
			}
		}
		catch( javax.mail.MessagingException e )
		{
			app.except(e, "SimpleEmailer.sendHtmlEmailWithAttachments(): Failed to send email from: " + 
					fromAddress.getAddress() + "; subject: " + subject);
	
			StringBuilder buf = new StringBuilder(1024);
			buf.append("Send email failed: ");
			buf.append(e.getMessage());
			Exception nextException = e.getNextException();
			if ( nextException != null )
			{
				buf.append(" - ");
				buf.append(nextException.getMessage());
			}
			throw new EsfException(buf.toString());
		}
	}


    public MimeMessage createMimeMessage()
    {
		return new MimeMessage(session);

    }
    
	public void sendMimeMessage( MimeMessage mimeMessage )
		throws EsfException
	{
		try
		{
			mimeMessage.saveChanges(); // implicit with send(), but not sendMessage()
            
			try
			{
				getTransport().sendMessage( mimeMessage, mimeMessage.getAllRecipients() );
			}
			catch( Exception e2 )
			{
				getNewTransport().sendMessage( mimeMessage, mimeMessage.getAllRecipients() );
			}
		}
		catch( javax.mail.MessagingException e )
		{
			app.except(e, "SimpleEmailer.sendMimeMessage(): Failed to send mime email");
			throw new EsfException("Failed to send MimeMessage: " + e.getMessage());
		}
	}
    
    
	public void sendAttachment(String emailFrom, String emailToList, String emailSubject, String message, String attachmentFileName, String attachment)
	 	throws EsfException
	{
		SendAttachmentInfo[] infos = new SendAttachmentInfo[1];
		infos[0] = new SendAttachmentInfo(attachmentFileName,attachment);
		sendAttachments(emailFrom, emailToList, emailSubject, message, infos);
	}
   
	public void sendAttachment(String emailFrom, String emailToList, String emailSubject, String message, String attachmentFileName, byte[] attachment)
   		throws EsfException
   	{
		SendAttachmentInfo[] infos = new SendAttachmentInfo[1];
		infos[0] = new SendAttachmentInfo(attachmentFileName,attachment);
		sendAttachments(emailFrom, emailToList, emailSubject, message, infos);
   	}

    public class SendAttachmentInfo
    {
    	public SendAttachmentInfo(String fileName, byte[] attachmentData)
    	{
    		attachmentFileName = fileName;
    		attachment = attachmentData;
    	}
    	public SendAttachmentInfo(String fileName, String attachmentData)
    	{
    		attachmentFileName = fileName;
    		attachment = EsfString.stringToBytes(attachmentData);
    	}
    	
    	public String attachmentFileName;
    	public byte[] attachment;
    }
	public void sendAttachments(String emailFrom, String emailToList, String emailSubject, String message, SendAttachmentInfo[] attachmentInfos)
		throws EsfException
	{
		InternetAddress[] toAddresses = CreateInternetAddresses(emailToList);
	    if ( toAddresses == null )
			throw new EsfException("Invalid email TO address: " + emailToList );
	
	    InternetAddress fromAddress = null;
	    try
	    {
	        fromAddress = new InternetAddress(emailFrom);
	    }
	    catch( javax.mail.internet.AddressException e )
	    {
	        throw new EsfException("Invalid FROM email address (" + emailFrom + ") " + e.getMessage() );
	    }
	    
	    if ( attachmentInfos == null || attachmentInfos.length == 0 )
	    	throw new EsfException("No attachment info specified");
	
	    try
	    {
	    	MimeMessage mimeMessage = createMimeMessage();
	       
	    	mimeMessage.setHeader("X-OPEN-ESIGNFORMS-AUTH","Open eSignForms e-contracting automated email from deploymentId: " + app.getDeployId());
	    	mimeMessage.setFrom(fromAddress);
	    	mimeMessage.setRecipients(Message.RecipientType.TO,toAddresses);
	
	    	mimeMessage.setSubject(emailSubject,EsfString.CHARSET_UTF_8); 
	       
	    	// This is going to be a multi-part message, with the note being the first part and the attachment following.
	    	MimeMultipart multipart = new MimeMultipart();
	
	    	// Create the message body part 
	    	MimeBodyPart bodyPart = new MimeBodyPart();
	    	bodyPart.setContent(message,Application.CONTENT_TYPE_TEXT+Application.CONTENT_TYPE_CHARSET_UTF_8);
	    	multipart.addBodyPart(bodyPart);
	
	    	// Add the attachments
	    	for( SendAttachmentInfo attachmentInfo : attachmentInfos )
	    	{
		    	bodyPart = new MimeBodyPart();
		    	ByteArrayDataSource source = new ByteArrayDataSource(attachmentInfo.attachment,app.getContentTypeByFileName(attachmentInfo.attachmentFileName),attachmentInfo.attachmentFileName);
		    	bodyPart.setDataHandler(new javax.activation.DataHandler(source));
		    	bodyPart.setFileName(attachmentInfo.attachmentFileName);
		    	multipart.addBodyPart(bodyPart);
	    	}
	       
	    	mimeMessage.setContent(multipart);
	
	    	try
	    	{
	    		sendMimeMessage( mimeMessage );
	    	}
	    	catch( com.esignforms.open.exception.EsfException e )
	    	{
	    		app.debug("sendAttachments() initial exception: " + e.getMessage() );
	    		app.sleep(5000); // sleep for 5 seconds
	    		sendMimeMessage( mimeMessage );
	    	}
	    }
	    catch( javax.mail.MessagingException e)
	    {
	       	app.except(e,"sendAttachments() Failed to create multipart MimeMessage");
	       	throw new EsfException("Failed to create multipart MimeMessage: "  + e.getMessage());
	    }
	    catch( Exception e )
	    {
	    	app.except(e,"sendAttachments() Failed to send MimeMessage");
	    	throw new EsfException("Failed to send MimeMessage: "  + e.getMessage());
	    }
	}


    public void close()
    {
   		try
   		{
			if ( transport != null )
			{
				app.debug("SimpleEmailer.close() - Closing the SMTP transport...");
				transport.close();
			}
   		}
   		catch( Exception e ) 
   		{
   			// do nothing special
   		}
   		finally
   		{
			transport = null;
			session   = null;
   		}
   	}
    
} 
