// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.email;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;

/**
* OutboundEmailMessageResponse holds an email response for what our IMAP system retrieves.
* 
* @author Yozons, Inc.
*/
public class OutboundEmailMessageResponse
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<OutboundEmailMessageResponse>
{
	private static final long serialVersionUID = 5519235479938308970L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(OutboundEmailMessageResponse.class);

    protected final EsfUUID outboundEmailMessageId;
    protected final EsfDateTime receivedTimestamp; // date email was received
    
    protected String emailFrom;
    protected String emailTo;
    protected String emailSubject;
    protected String emailText;
    protected String emailHtml;
    
    
    /**
     * This creates a OutboundEmailMessage object from data retrieved from the DB or from the 
     * InboundEmailProcessor.
     */
    protected OutboundEmailMessageResponse(EsfUUID outboundEmailMessageId, EsfDateTime receivedTimestamp, 
    			String emailFrom, String emailTo, String emailSubject, String emailText, String emailHtml
    		)
    {
        this.outboundEmailMessageId = outboundEmailMessageId;
        this.receivedTimestamp = receivedTimestamp;
        this.emailFrom = EsfString.ensureTrimmedLength(emailFrom,Literals.EMAIL_ADDRESS_MAX_LENGTH);
        this.emailTo = EsfString.ensureTrimmedLength(emailTo,Literals.EMAIL_ADDRESS_LIST_MAX_LENGTH);
        this.emailSubject = EsfString.truncateTrimmedString(emailSubject,Literals.EMAIL_SUBJECT_MAX_LENGTH);
        this.emailText = EsfString.truncateTrimmedString(emailText,Literals.EMAIL_TEXT_MAX_LENGTH);
        this.emailHtml = EsfString.truncateTrimmedString(emailHtml,Literals.EMAIL_HTML_MAX_LENGTH);
    }
    
    public final EsfUUID getOutboundEmailMessageId()
    {
        return outboundEmailMessageId;
    }
    
    public EsfDateTime getReceivedTimestamp()
    {
    	return receivedTimestamp;
    }
    
    public String getEmailFrom()
    {
    	return emailFrom;
    }
    
    public String getEmailTo()
    {
    	return emailTo;
    }
    
    public String getEmailSubject()
    {
    	return emailSubject;
    }
    
    public String getEmailText()
    {
    	return emailText;
    }
    
    public String getEmailHtml()
    {
    	return emailHtml;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof OutboundEmailMessageResponse )
        {
        	OutboundEmailMessageResponse other = (OutboundEmailMessageResponse)o;
            return getOutboundEmailMessageId().equals(other.getOutboundEmailMessageId()) && getReceivedTimestamp().equals(other.getReceivedTimestamp());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getOutboundEmailMessageId().hashCode() + getReceivedTimestamp().hashCode();
    }
    
    @Override
    public int compareTo(OutboundEmailMessageResponse o)
    {
    	int diff = getOutboundEmailMessageId().compareTo(o.getOutboundEmailMessageId());
    	return ( diff == 0 ) ? getReceivedTimestamp().compareTo(o.getReceivedTimestamp()) : diff;
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con)
        throws SQLException
    {
    	_logger.debug("save(con) on outboundEmailMessageId: " + outboundEmailMessageId + "; receivedTimestamp: " + receivedTimestamp.toDateTimeMsecString() + "; insert: " + doInsert());
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( doInsert() )
            {                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_outbound_email_message_response (outbound_email_message_id,received_timestamp,email_from,email_to,email_subject,email_text,email_html) " + 
                	"VALUES (?,?,?,?,?,?,?)");
                stmt.set(outboundEmailMessageId);
                stmt.set(receivedTimestamp);
                stmt.set(emailFrom);
                stmt.set(emailTo);
                stmt.set(emailSubject);
                stmt.set(emailText);
                stmt.set(emailHtml);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for outboundEmailMessageId: " + outboundEmailMessageId + "; receivedTimestamp: " + receivedTimestamp.toDateTimeMsecString() + "; insert: " + doInsert());
                    return false;
                }

                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                return true;
            }
            
        	// We don't currently have an update function
        	
            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            return true;
        }
        catch(SQLException e)
        {
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }

    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"save() on outboundEmailMessageId: " + outboundEmailMessageId + "; receivedTimestamp: " + receivedTimestamp + "; insert: " + doInsert());
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    
    /**
     * It's rare, but we sometimes get a duplicate key and this attempts to resolve that by forcing the 
     * timestamp up by a millisecond until it succeeds.
     * @return true if saved, otherwise false
     */
    public boolean saveRetryDuplicateKeys()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection 		  con  = pool.getConnection();
        
        while( true )
        {
            try
            {
                boolean result = save(con);
    			if ( result )
    				con.commit();
    			else
    				con.rollback();
                return result;
            }
            catch(SQLException e) 
            {
            	if ( pool.isDuplicateKey(e) )
            	{
                	_logger.warn("saveRetryDuplicateKeys() DUPLICATE KEY on outboundEmailMessageId: " + outboundEmailMessageId + "; receivedTimestamp: " + receivedTimestamp.toDateTimeMsecString() + "; insert: " + doInsert() + "; retrying...");
            		pool.rollbackIgnoreException(con,e);
            		receivedTimestamp.addMsecs(1);
            		// by not returning, we'll try again...with a our timestamp incremented by 1 millisecond
            	}
            	else
            	{
                	_logger.sqlerr(e,"saveRetryDuplicateKeys() exception not duplicate key on outboundEmailMessageId: " + outboundEmailMessageId + "; receivedTimestamp: " + receivedTimestamp.toDateTimeMsecString() + "; insert: " + doInsert());
            		return false;
            	}
            }
            finally
            {
                cleanupPool(pool,con,null);
            }
        }
        
    }
    
    // DELETE is controlled by OutputEmailMessage - so when it's deleted, all related responses are deleted with it.


   	public static class Manager
   	{
   		public static void deleteAllForOutboundEmailMessage(Connection con, EsfUUID outboundEmailMessageId)
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
  	        	// Delete all records that only point to the user id and not also a transaction
   	        	stmt = new EsfPreparedStatement( con,
   	        			"DELETE FROM esf_outbound_email_message_response WHERE outbound_email_message_id=?"
   	        									);
    	        stmt.set(outboundEmailMessageId);
    	        stmt.executeUpdate();
    	    }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.deleteAllForOutboundEmailMessage(con) outbound_email_message_id: " + outboundEmailMessageId);
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }
   		}
   		
   		public static List<OutboundEmailMessageResponse> getAll(Connection con, EsfUUID outboundEmailMessageId)
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<OutboundEmailMessageResponse> list = new LinkedList<OutboundEmailMessageResponse>();
   	        	
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT received_timestamp,email_from,email_to,email_subject,email_text,email_html " +
   	        			"FROM esf_outbound_email_message_response WHERE outbound_email_message_id=? ORDER BY received_timestamp"
   	        									);
    	        stmt.set(outboundEmailMessageId);
    	        
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfDateTime receivedTimestamp = rs.getEsfDateTime();
	            	String emailFrom = rs.getString();
	            	String emailTo = rs.getString();
	            	String emailSubject = rs.getString();
	            	String emailText = rs.getString();
	            	String emailHtml = rs.getString();
	            	
	            	OutboundEmailMessageResponse msg = new OutboundEmailMessageResponse(outboundEmailMessageId,receivedTimestamp,emailFrom,emailTo,emailSubject,emailText,emailHtml);
	            	msg.setLoadedFromDb();
	            	list.add(msg);
	            }
	            
   	            return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll(con) outboundEmailMessageId: " + outboundEmailMessageId);
   	            list.clear();
   	            return list;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }
   		}
   		
   		public static List<OutboundEmailMessageResponse> getAll(EsfUUID outboundEmailMessageId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        List<OutboundEmailMessageResponse> list = null;
   	        	
   	        try
   	        {
   	        	list = getAll(con,outboundEmailMessageId);
   	            con.commit();
   	            
   	            return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() outboundEmailMessageId: " + outboundEmailMessageId);
   	            pool.rollbackIgnoreException(con,e);
   	            return list;
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }
   		}
   		
   		public static OutboundEmailMessageResponse createNew(EsfUUID outboundEmailMessageId,EsfDateTime receivedTimestamp,String emailFrom,String emailTo,String emailSubject,String emailText,String emailHtml)
   		{
   			if ( receivedTimestamp == null )
   				receivedTimestamp = new EsfDateTime();
   			return new OutboundEmailMessageResponse(outboundEmailMessageId,receivedTimestamp,emailFrom,emailTo,emailSubject,emailText,emailHtml);
   		}
   	} // Manager
   	
}