// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.email;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfString;

/**
 * This retrieves emails via IMAP.
 *
 * @author Yozons, Inc.
 */
public class SimpleEmailRetriever 
{
    private Session     session;
    private Store   	store;
    private Application app = Application.getInstance();
    private String      imapServer;
    private String 		imapServerPort;
    private String      imapUser;
    private String      imapPassword;
    private boolean     doSSL;
    private String		recipientEmailAddressPrefix;
    
    /**
     * Instantiate a SimpleEmailRetriever that will retrieve and process all emails in the inbox.
     * @throws Exception if there's a problem obtaining an initial IMAP transport that is needed to successfully retrieve a message
     */
    public SimpleEmailRetriever()
    	throws Exception
	{
        loadSystemData();
        try
        {
            // Just for grins to ensure we have a valid setup.
            getStore();
        }
        catch( Exception e )
        {
            app.except(e,"SimpleEmailRetriever() Could not get initial IMAP store to: " + imapServer + " : " + imapServerPort + "; imapUser: " + imapUser);
            throw e;
        }

    }
    
    private void loadSystemData()
    {
        imapServer       		= app.getImapServer().toString();
        imapServerPort 			= app.getImapPort().toPlainString();
        imapUser				= app.getImapUser().toString();
        imapPassword            = app.getImapPassword().toString();
        doSSL                   = app.getImapSSL().isTrue();
        recipientEmailAddressPrefix = SimpleEmailer.normalizePrefix(imapUser) + "_";
    }
    
    private Store getStore()
        throws javax.mail.NoSuchProviderException, javax.mail.MessagingException
    {
    	// If we have a store, let's reuse it.
    	if ( store != null )
    		return store;
    	
        Properties prop = new Properties();
        if ( doSSL )
        {
            prop.put("mail.imaps.host",imapServer);
        	if ( EsfString.isNonBlank(imapServerPort) )
        		prop.put("mail.imaps.port",imapServerPort);
        	prop.put("mail.store.protocol","imaps");
        }
        else
        {
            prop.put("mail.imap.host",imapServer);
            if ( EsfString.isNonBlank(imapServerPort) )
                prop.put("mail.imap.port",imapServerPort);
        }
        
        session = Session.getInstance(prop);
        
        synchronized(this)
        {
			try
			{
				if ( doSSL )
					store = session.getStore("imaps");
				else
					store = session.getStore("imap");
				
				store.connect(imapUser,imapPassword);
				return store;
			}
			catch( javax.mail.MessagingException e )
			{
				store = null;
				app.except(e,"SimpleEmailRetriever.getStore() - Failed to get IMAP store @ server: " + imapServer + " : " + imapServerPort + "; SSL: " + doSSL + "; user: " + imapUser);
				throw e;
			}
        }
    }
    
    public List<OutboundEmailMessageResponse> processInbox()
    {
    	LinkedList<OutboundEmailMessageResponse> list = null;
    	
    	Folder inbox = null;
    	try
    	{
        	inbox = store.getFolder("INBOX");
        	inbox.open(Folder.READ_WRITE);
        	
        	Message[] messages = inbox.getMessages();
        	for( Message msg : messages )
        	{
        		String emailText = null, emailHtml = null, emailTo = null, bounceCorrelationCode = null;
        		
        		Address[] fromAddresses = msg.getFrom();
        		String emailFrom = fromAddresses != null && fromAddresses.length > 0 ? fromAddresses[0].toString() : "";
        		
        		String emailSubject = msg.getSubject();

        		EsfDateTime receivedTimestamp = new EsfDateTime(msg.getReceivedDate());
        		
        		Address[] allRecipients = msg.getAllRecipients(); // Sometimes returns null
        		if ( allRecipients != null )
        		{
    				for( Address a : allRecipients )
            		{
            			EsfEmailAddress emailAddress = new EsfEmailAddress(a.toString());
            			String addr = emailAddress.getEmailAddress();
            			if ( addr.toLowerCase().startsWith(recipientEmailAddressPrefix) )
            				bounceCorrelationCode = addr.substring(recipientEmailAddressPrefix.length(),addr.indexOf('@'));
            			
            			emailTo = emailTo == null ? addr : (emailTo + "; " + addr);
            		}
        		}
        		
        		Object msgContent = msg.getContent();
        		if ( msgContent instanceof String )
        		{
        			emailText = (String)msgContent;
					app.warning("SimpleEmailRetriever.processInbox() - Message content was simple String: " + emailText + "; emailTo: " + emailTo);
        		} 
        		else if ( msgContent instanceof Multipart )
        		{
            		Multipart mp = (Multipart)msgContent;
            		for( int i=0, n=mp.getCount(); i < n; ++i )
            		{
            			Part part = mp.getBodyPart(i);
            			String disposition = part.getDisposition();
            			
        				if ( disposition == null )
        				{
        					MimeBodyPart mbp = (MimeBodyPart)part;
        					
        					int mbpSize = mbp.getSize();
        					if ( mbpSize < 1 )
        					{
            					String contentType = mbp.getContentType();
            					app.warning("SimpleEmailRetriever.processInbox() - Other IGNORED part unexpectedly received content type: " + contentType + "; with part size: " + mbpSize + "; emailTo: " + emailTo);
        					}
        					else
        					{
        						InputStream mbpInputStream = mbp.getInputStream();
                				byte[] partData = new byte[mbpSize];
                				int partDataOffset = 0;
                				int numRead;
                				int bytesToRead = mbpSize;
                				while( bytesToRead > 0 && (numRead = mbpInputStream.read(partData,partDataOffset,bytesToRead)) > 0 ) 
                				{
                					partDataOffset += numRead;
                					bytesToRead -= numRead;
                				}

            					String contentType = mbp.getContentType();

                				String charset = getCharset(contentType);
            					app.debug("SimpleEmailRetriever.processInbox() - Received MimeBodyPart content type: " + contentType + "; encoding: " + mbp.getEncoding() + "; COMPUTED CHARSET: " + charset + "; emailTo: " + emailTo + "; size: " + mbpSize);

                				String stringPartData = EsfString.sanitizeByCharset(partData,charset);
                				if ( stringPartData == null )
                					stringPartData = "";
                				else
                					stringPartData = stringPartData.trim();

                				if (mbp.isMimeType(Application.CONTENT_TYPE_TEXT))
                					emailText = emailText == null ? stringPartData : emailText + "\n-- more --\n" + stringPartData;
                				else if ( mbp.isMimeType(Application.CONTENT_TYPE_HTML) )
                					emailHtml = emailHtml == null ? stringPartData : emailHtml + "<br/>" + stringPartData;
                				else if ( mbp.isMimeType("message/delivery-status") )
                					emailText = emailText == null ? stringPartData : emailText + "\n-- Message Delivery Status --\n" + stringPartData;
                				else if ( mbp.isMimeType("text/rfc822-headers") )
                					; // We are purposely ignoring the "text/rfc822-headers" which in theory is the original email message headers sent out
                				else if ( mbp.isMimeType("message/rfc822") )
                					; // We are purposely ignoring the "message/rfc822" which in theory is the original email message sent out
                				else
                				{
                					app.warning("SimpleEmailRetriever.processInbox() - Other IGNORED part unexpectedly received content type: " + contentType + "; emailTo: " + emailTo);
                				}
        					}
        				}
        				else if ( Part.ATTACHMENT.equals(disposition) || Part.INLINE.equals(disposition) )
            			{
        					String fileName = part.getFileName();
        					String contentType = part.getContentType();
        					String desc = part.getDescription();
        					app.debug("SimpleEmailRetriever.processInbox() - IGNORED " + disposition + "; filename: " + fileName + "; contentType: " + contentType + "; desc: " + desc + "; size: " + part.getSize() + "; emailTo: " + emailTo);
            				//byte[] partData = new byte[part.getSize()];
            				//part.getInputStream().read(partData);
            			}
        				else
        				{
        					app.debug("SimpleEmailRetriever.processInbox() - IGNORED UNEXPECTED DISPOSITION " + disposition  + "; emailTo: " + emailTo);
        				}
            		}
        		}
        		
        		boolean showDebugInfo = true;
        		
        		if ( EsfString.isNonBlank(bounceCorrelationCode) )
        		{
        			OutboundEmailMessage origMessage = OutboundEmailMessage.Manager.getByBounceCorrelationCode(bounceCorrelationCode);
        			if ( origMessage != null )
        			{
        				OutboundEmailMessageResponse response = 
        					OutboundEmailMessageResponse.Manager.createNew(origMessage.getId(), receivedTimestamp, emailFrom, emailTo, emailSubject, emailText, emailHtml); 
        				if ( list == null )
        					list = new LinkedList<OutboundEmailMessageResponse>();
        				list.add(response);
        				//showDebugInfo = false;
        			}
        			else
        				app.warning("SimpleEmailRetriever.processInbox() - IGNORING bounce we cannot correlate with: " + bounceCorrelationCode  + "; emailTo: " + emailTo);
        		}
        		else
    				app.warning("SimpleEmailRetriever.processInbox() - IGNORING email that has no correlation code");
        		
        		if ( showDebugInfo )
        		{
            		app.debug("SimpleEmailRetriever.processInbox() - Bounce correlation: " + bounceCorrelationCode);
            		app.debug("SimpleEmailRetriever.processInbox() - From: " + emailFrom);
            		app.debug("SimpleEmailRetriever.processInbox() - To: " + emailTo);
            		app.debug("SimpleEmailRetriever.processInbox() - Subject: " + emailSubject);
            		app.debug("SimpleEmailRetriever.processInbox() - Received: " + receivedTimestamp);
            		EsfString.SHOW_BIG_STRING("SimpleEmailRetriever.processInbox() - emailText",emailText);
            		EsfString.SHOW_BIG_STRING("SimpleEmailRetriever.processInbox() - emailHtml",emailHtml);
        		}

        		// We always delete the email message once we've processed it
        		msg.setFlag(Flags.Flag.DELETED,true);
        	}
        	
        	return list;
    	}
    	catch(javax.mail.MessagingException e)
    	{
    		app.except(e, "SimpleEmailRetriever.processInbox()");
    		return list;
    	}
    	catch(java.io.IOException e)
    	{
    		app.except(e, "SimpleEmailRetriever.processInbox()");
    		return list;
    	}
    	finally
    	{
    		if ( inbox != null )
    		{
    			try { inbox.close(true); } catch(Exception e){}
    		}
    	}
    }
    
    // Normally a charset=XXXX; is the format we expect somewhere in the contentType
    String getCharset(String contentType) 
    {
    	if ( EsfString.isNonBlank(contentType) )
    	{
        	int charsetIndex = contentType.indexOf("charset=");
        	if ( charsetIndex >= 0 )
        	{
        		int charsetSemicolon = contentType.indexOf(";",charsetIndex+8);
        		if ( charsetSemicolon == -1 )
        			charsetSemicolon = contentType.length();
        		return contentType.substring(charsetIndex+8,charsetSemicolon);
        	}
    	}
    	return EsfString.CHARSET_UTF_8;
    }
    
    /*
	private synchronized Store getNewStore()
        throws javax.mail.NoSuchProviderException, javax.mail.MessagingException
    {
        if ( store != null )
        {
        	try
        	{
				app.info("SimpleEmailRetriever.getNewStore(): Closing old IMAP store. Reconnecting to IMAP store...");
				store.close();
        	}
        	catch( Exception e ) {} // ignore any exception on the close since it's most likely down/broken anyway (that's why we're reconnecting!)
			finally
			{
				store = null;
				app.sleep(500);
			}
        }
            
        return getStore();
    }
    */
    
    public void close()
    {
   		try
   		{
			if ( store != null )
			{
				store.close();
			}
   		}
   		catch( Exception e ) 
   		{
   			// do nothing special
   		}
   		finally
   		{
			store = null;
			session = null;
   		}
   	}
    
} 
