// Copyright (C) 2009-2011 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.exception.EsfException;

/**
 * Returns serial numbers by category esfname.
 * This can be configure with Global Properties (TO DO: MAKE THIS ITS OWN CONFIGURATION OPTION RATHER THAN GLOBAL PROPS):
 *    UserSerial.categoryesfname.startNumber = 1000
 *           If the category is not defined in the table, use this as the starting number and create the row; else will fail with an exception.
 *    UserSerial.categoryesfname.format = ###000000
 *           If call the routine that wants a string formatted number and this is defined, it will use this DecimalFormat, otherwise just the number 
 *           as string.
 * @author Yozons Inc.
 */
public class UserSerial
{
	protected static Application _app = Application.getInstance();
	
	public synchronized static EsfInteger GetNextNumber(EsfName esfname)
		throws EsfException
	{
		ConnectionPool    pool = _app.getConnectionPool();
		Connection        con  = null;
		PreparedStatement stmt = null;
        
		try 
		{
			EsfInteger nextNumber;
			
			con = pool.getConnection();
            
			stmt = con.prepareStatement("SELECT next_number FROM esf_user_serial WHERE esfname = ? FOR UPDATE");
			stmt.setString(1,esfname.toString());
			ResultSet rs = stmt.executeQuery();
			if ( rs.next() )
			{
				nextNumber = new EsfInteger(rs.getLong(1));
				
				// Update the value
				stmt.close();
				stmt = con.prepareStatement("UPDATE esf_user_serial SET next_number=next_number+1 WHERE esfname = ?");
				stmt.setString(1,esfname.toString());
				int updateCount = stmt.executeUpdate();
				if ( updateCount != 1 )
				{
					_app.err("UserSerial.GetNextNumber(" + esfname + ") got unexpected UPDATE count: " + updateCount);
					con.rollback();
					throw new EsfException("esf_user_serial UPDATE failed");
				}
			}
			else
			{
				String propName = "UserSerial."+esfname+".startNumber";
				nextNumber = _app.getGlobalProperties().getIntegerByName(propName);
				if ( nextNumber.isNull() )
				{
					con.rollback();
					throw new EsfException("No esf_user_serial found with esfname: " + esfname + "; and no global property: " + propName);
				}

				long dbNextNumber = nextNumber.toLong() + 1;

				stmt.close();
				stmt = con.prepareStatement("INSERT INTO esf_user_serial VALUES (?,?)");
				stmt.setString(1,esfname.toString());
				stmt.setLong(2,dbNextNumber);
				int updateCount = stmt.executeUpdate();
				if ( updateCount != 1 )
				{
					_app.err("UserSerial.GetNextNumber(" + esfname + ") got unexpected update count: " + updateCount + 
													   " trying to insert new serial starting at: " + dbNextNumber);
					con.rollback();
					throw new EsfException("esf_user_serial INSERT failed");
				}
			}
	        
			con.commit();
			
			return nextNumber;
		} 
		catch(SQLException e) 
		{
			_app.sqlerr(e,"UserSerial.GetNextNumber(" + esfname + ")");
			pool.rollbackIgnoreException(con,e);
			throw new EsfException("Failed to load the esf_user_serial (" + esfname + ") next_number: " + e.getMessage());
		}
		finally
		{
			_app.cleanupPool(pool, con, stmt);
		}
	}
	
	public static String GetNextNumberFormatted(EsfName esfname)
		throws EsfException
	{
		EsfInteger next = GetNextNumber(esfname);

		// Because number formats often begin with #, we can ignore comments for this particular field
		EsfString propFormat = _app.getGlobalProperties().getStringByName("UserSerial."+esfname+".format"); 
		if ( propFormat.isBlank() )
			return next.toPlainString();
		
		return next.format(propFormat.toPlainString());
	}
}