// Copyright (C) 2009-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.UUID;

import javax.crypto.SecretKey;

import com.esignforms.open.Application;
import com.esignforms.open.admin.SessionKey;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ByteBlob;
import com.esignforms.open.util.Base64;

/**
 * Stores blobs in the database, with it being optionally compressed and optionally encrypted.
 * Most of our blobs are both compressed and encrypted for privacy and efficient storage.
 * 
 * @author Yozons Inc.
 */
public class BlobDb
{
	public static enum CompressOption { DISABLE, ENABLE };
	public static enum EncryptOption { DISABLE, ENABLE };

	protected Application app      = null;
	protected boolean     isOracle = false;
	
	/**
	 * Constructs a BlobDb object.  Note that the isOracle value is
	 * set based on the Application.getConnectionPool().isOracle() call, so if you are using this
	 * object with anything but the default connection pool, you will need to call setIsOracle()
	 * with the correct value.
	 */
	public BlobDb()
	{
		app      = Application.getInstance();
		isOracle = app.getConnectionPool().isOracle();
	}
	
	public boolean isOracle()
	{
		return isOracle;
	}
	public void setIsOracle(boolean v)
	{
		isOracle = v;
	}
	
    
    protected byte[] encrypt(SecretKey key, EsfUUID blobId, byte[] clearData)
    {
        byte[] encryptedData = app.getSecretKeyGenerator().encryptIv(key, blobId.toString(), clearData);
        return encryptedData;
    }

    protected byte[] decrypt(SecretKey key, EsfUUID blobId, byte[] encryptedData)
    {
        byte[] clearData = app.getSecretKeyGenerator().decryptIv(key, blobId.toString(), encryptedData);
        return clearData;
    }

	
	/**
	 * Retrieves the specified encrypted blob and gives it back decrypted and decompressed based on how it was stored.
	 * @param con the Connection to use for JDBC and transaction control
	 * @param blobId the EsfUUID id of the blob to retrieve
	 * @return the byte array of the retrieved blob, or null if no such blob is found
	 * @throws SQLException if there's an exception doing the JDBC calls
	 */
    public byte[] select(Connection con, EsfUUID blobId)
		throws SQLException
	{
		//if ( app.isDebugEnabled() )
		//	app.debug("BlobDb.select() - id: " + blobId);
	        
		PreparedStatement stmt = null;
		try 
		{
			stmt = con.prepareStatement("SELECT session_key_id,key_data,orig_size,compressed_size,stored_size,blob_data FROM esf_blob WHERE id=?");
			stmt.setObject(1,blobId.getUUID(),Types.OTHER);
            
			ResultSet rs = stmt.executeQuery();
			if ( ! rs.next() )
			{
				app.err("BlobDb.select() - No blob found for id: " + blobId);
				return null;
			}
            
			UUID sessionKeyUUID = (UUID)rs.getObject(1);
			if ( rs.wasNull() )
				sessionKeyUUID = null;
			
			String keyData	= rs.getString(2);
			if ( rs.wasNull() )
				keyData = null;
			
			int origSize = rs.getInt(3);
            int compressedSize = rs.getInt(4);
			int storedSize = rs.getInt(5);
			java.sql.Blob dbBlob = rs.getBlob(6);
			
			ByteBlob bb = new ByteBlob(dbBlob);
            byte[] dataBytes = bb.getBytes();
            if ( dataBytes.length != storedSize )
            {
                app.err("BlobDb.select() - Blob found for id: " + blobId + " but retrieved data with length: " + dataBytes.length +
                		"; stored_size: " + storedSize);
                return null;
            }
			
            // If we have a session key with key data, it's been encrypted, so let's decrypt it first
            if ( sessionKeyUUID != null && keyData != null )
            {
                // Get the session key used to encrypt
                EsfUUID sessionKeyId = new EsfUUID(sessionKeyUUID);
                SessionKey sessKey = app.getSessionKey(sessionKeyId);
                if ( sessKey == null )
                {
                    app.err("BlobDb.select() - Blob found for id: " + blobId + " but there's no matching session key with id: " + sessionKeyId);
                    return null;
                }
                
                // Our unique key for this blob is encrypted and base-64 encoded, so let's decode first.
                byte[] encryptedKeyBytes = Base64.decode(keyData);
                
                // Then let's decrypt the encryption key using our session key
    			byte[] keyBytes = decrypt(sessKey.getKey(),sessKey.getKeyId(),encryptedKeyBytes);  
                if ( keyBytes == null )
                {
                    app.err("BlobDb.select() - Encrypted Blob found for id: " + blobId + " but could not decrypt unique encryption key using session key id: " + sessKey.getKeyId());
                    return null;
                }
 
                // Rebuild our secret key.  Note that at this point, we still only assume a single secret key generator.
                SecretKey key = app.getSecretKeyGenerator().getFromRawEncoded(keyBytes);

    			dataBytes = decrypt(key,blobId,dataBytes);
                if ( dataBytes == null )
                {
                    app.err("BlobDb.select() - Encrypted Blob found for id: " + blobId + " but could not decrypt data");
                    return null;
                }
            }
            
            // If our compressed size is not the original size, it must also be compressed, so let's decompress next
            if ( compressedSize != origSize )
            	dataBytes = app.decompress(dataBytes,origSize);

            if ( dataBytes.length != origSize )
            {
                app.warning("BlobDb.select() - Blob found for id: " + blobId + " but ended up with length: " + dataBytes.length +
                            "; stored_size: " + origSize);
            }    
            
            return dataBytes;
		} 
		catch(java.sql.SQLException e) 
		{
			app.sqlerr(e,"BlobDb.select()");
			throw e;
		}
		finally
		{
			app.cleanupStatement(stmt);
		}
	}
	
    public String selectAsString(Connection con, EsfUUID blobId)
		throws SQLException
	{
		byte[] stringBytes = select(con,blobId);
		if ( stringBytes == null )
			return null;
		
		return EsfString.bytesToString(stringBytes);
	}
	
	/**
	 * Inserts the specified blob into the database.  If doCompress is requested, the data will first be compressed.
	 * If compression is not smaller than the original size, we ignore compression (so no decompression on retrieval).
	 * If doEncrypt is requested, the data will be encrypted using the current session key.
	 * @param con the Connection to use for JDBC and transaction control
     * @param blobId the EsfUUID blob id to use for storing; if null, a new blob id will be assigned
	 * @param b they byte array of the blob to store; must not be null and must be at least 1 byte long (no empty blobs!)
	 * @param compressOption the CompressOption if ENABLE means compression should be done before storage
	 * @param encryptOption the EncryptOption if ENABLE means to encrypt before storage
	 * @return the EsfUUID unique blob id as inserted; if the insert fails for a non-SQL error, returns null
	 * @throws SQLException if there's any SQL error on the insert
	 */
	public EsfUUID insert(Connection con, EsfUUID blobId, byte[] b, CompressOption compressOption, EncryptOption encryptOption)
		throws SQLException
	{
		if ( b == null || b.length < 1 )
		{
			app.err("BlobDb.insert() - Attempted to insert an empty blob");
			return null;
		}
		
        if ( blobId == null )
            blobId = new EsfUUID();
		
		if ( app.isDebugEnabled() )
			app.debug("BlobDb.insert() - using blob id: " + blobId + "; size: " + b.length + "; compressOption: " + compressOption + "; encryptOption: " + encryptOption);
		
		PreparedStatement stmt = null;
		try
		{
			String sqlCommand;
			if ( isOracle )
				sqlCommand = "INSERT INTO esf_blob (id,session_key_id,key_data,orig_size,compressed_size,stored_size,blob_data) VALUES (?,?,?,?,?,?,empty_blob())";
			else
				sqlCommand = "INSERT INTO esf_blob (id,session_key_id,key_data,orig_size,compressed_size,stored_size,blob_data) VALUES (?,?,?,?,?,?,?)";
            
            int origSize = b.length;
            
            // If requested, let's try to compress.  If after compression, we are not at least smaller than the original
            // data, we skip storing it as compressed data
            byte[] storeData;
            if ( compressOption == CompressOption.ENABLE )
            {
            	storeData = app.compress(b);
                if ( storeData.length >= b.length )
                {
                	app.warning("BlobDb.insert() - Requested compression of original data with length: " + b.length + " did not get smaller. Compression ignored.");
                	storeData = b;
                }
            }
            else
            	storeData = b;
            
            int compressedSize = storeData.length;
            
            // If encryption requested, let's encrypt it now.  We use a unique key to encrypt our data to hinder cryptanalysis of repeated use keys
            String keyData;
            SessionKey sessKey;
            if ( encryptOption == EncryptOption.ENABLE ) 
            {
                SecretKey key = app.getSecretKeyGenerator().generateKey();
            	storeData = encrypt(key,blobId,storeData);

            	// Now, let's encrypt this key using the current session key
                sessKey = app.getSessionKey();
            	byte[] encryptedKeyBytes = encrypt(sessKey.getKey(),sessKey.getKeyId(),key.getEncoded());
            	keyData = Base64.encodeToString(encryptedKeyBytes);
            }
            else
            {
            	sessKey = null;
            	keyData = null;
            }
            
            int storedSize = storeData.length;
            
            // Let's insert it now
			stmt = con.prepareStatement(sqlCommand);
			stmt.setObject(1,blobId.getUUID(),Types.OTHER);
			
			if ( sessKey != null && keyData != null )
			{
				stmt.setObject(2,sessKey.getKeyId().getUUID(),Types.OTHER);
				stmt.setString(3,keyData);
			}
			else
			{
				stmt.setNull(2,Types.OTHER);
				stmt.setNull(3,Types.VARCHAR);
			}
			
			stmt.setInt(4, origSize);
			stmt.setInt(5, compressedSize);
            stmt.setInt(6, storedSize);

			ByteBlob bb = new ByteBlob(storeData);
			if ( ! isOracle )
				stmt.setBlob(7, bb);
            
			int num = stmt.executeUpdate();
        
			if ( num != 1 )
			{
				app.err("BlobDb.insert() - Failed to insert new blob id: " + blobId + "; origSize: " + origSize + "; insert count: " + num +
						"; compressOption: " + compressOption + "; encryptOption: " + encryptOption);
				return null;
			}
 
			/**
			 * For crappy Oracle, we need to insert an empty blob first, then select it back out to get the
			 * "handle" to it, then write the actual blob into it.
			 */
            /*
			if ( isOracle )
			{
				stmt.close();
				stmt = con.prepareStatement("SELECT esf_blob FROM esf_blob WHERE id=? FOR UPDATE");
				stmt.setObject(1,blobId.getUUID(),Types.OTHER);
				ResultSet rs = stmt.executeQuery();
				if ( rs.next() )
				{
					ybb.updateOracleBlob(rs.getBlob(1));
				}
				else
				{
					app.err("Blob.insert() - Failed to find/insert the oracle blob with id: " + id);
					return null;
				}
			}
            */
            
			return blobId;
		}
		catch( java.sql.SQLException e )
		{
			app.sqlerr(e,"BlobDb.insert() blobId: " + blobId + "; compressOption: " + compressOption + "; encryptOption: " + encryptOption);
			throw e;
		}
		finally
		{
			app.cleanupStatement(stmt);
		}
	}
	
    /**
     * Inserts the string as a blob.  If the input 'id' is null, an ID will be assigned.
     * @param con
     * @param blobId the EsfUUID blob id to use; if null, a new id will be assigned
     * @param data the String data to be stored
	 * @param compressOption the CompressOption if ENABLE means compression should be done before storage
	 * @param encryptOption the EncryptOption if ENABLE means to encrypt before storage
     * @return the EsfUUID id of this blob
     * @throws SQLException throw if there's a database error
     */
    public EsfUUID insert(Connection con, EsfUUID blobId, String data, CompressOption compressOption, EncryptOption encryptOption)
		throws SQLException
	{
		byte[] b = EsfString.stringToBytes(data);

		return insert(con,blobId,b,compressOption,encryptOption);
	}
	
	
	/**
	 * Updates an existing blob with new data.
	 * @param con the Connection to use for JDBC and transaction control
	 * @param blobId the EsfUUID of the blob to be updated
	 * @param b the byte array of the new data to be stored for this blob; may not be null and may not have a length < 1 (no empty blobs!)
	 * @param compressOption the CompressOption if ENABLE means compression should be done before storage
	 * @param encryptOption the EncryptOption if ENABLE means to encrypt before storage
	 * @return the boolean indicating whether the update occurred or not
	 * @throws SQLException if there's an SQL exception during the update
	 */
    public boolean update(Connection con, EsfUUID blobId, byte[] b, CompressOption compressOption, EncryptOption encryptOption)
		throws SQLException
	{
		if ( b == null || b.length < 1 )
		{
			app.err("BlobDb.update() - Attempted to update using an empty blob for id: " + blobId);
			return false;
		}

		if ( app.isDebugEnabled() )
			app.debug("BlobDb.update() - blob id: " + blobId + "; size: " + b.length + "; compressOption: " + compressOption + "; encryptOption: " + encryptOption);
	    
		PreparedStatement stmt = null;
		try
		{
			String sqlCommand;
			if ( isOracle )
				sqlCommand = "UPDATE esf_blob SET orig_size=?,compressed_size=?,stored_size=?,session_key_id=?,key_data=?,blob_data=empty_blob() WHERE id=?";
			else
				sqlCommand = "UPDATE esf_blob SET orig_size=?,compressed_size=?,stored_size=?,session_key_id=?,key_data=?,blob_data=? WHERE id=?";
            
			int origSize = b.length;
			
            // If requested, let's try to compress.  If after compression, we are not at least smaller than the original
            // data, we skip storing it as compressed data
            byte[] storeData;
            if ( compressOption == CompressOption.ENABLE )
            {
            	storeData = app.compress(b);
                if ( storeData.length >= b.length )
                {
                	app.warning("BlobDb.insert() - Requested compression of original data with length: " + b.length + " did not get smaller. Compression ignored.");
                	storeData = b;
                }
            }
            else
            	storeData = b;
            
            int compressedSize = storeData.length;
            
            // If encryption requested, let's encrypt it now.  We use a unique key to encrypt our data to hinder cryptanalysis of repeated use keys
            String keyData;
            SessionKey sessKey;
            if ( encryptOption == EncryptOption.ENABLE ) 
            {
                SecretKey key = app.getSecretKeyGenerator().generateKey();
            	storeData = encrypt(key,blobId,storeData);

            	// Now, let's encrypt this key using the current session key
                sessKey = app.getSessionKey();
            	byte[] encryptedKeyBytes = encrypt(sessKey.getKey(),sessKey.getKeyId(),key.getEncoded());
            	keyData = Base64.encodeToString(encryptedKeyBytes);
            }
            else
            {
            	sessKey = null;
            	keyData = null;
            }
            
            int storedSize = storeData.length;
            
			stmt = con.prepareStatement(sqlCommand);
			int pos = 1;
			stmt.setInt(pos++, origSize);
            stmt.setInt(pos++, compressedSize);
			stmt.setInt(pos++, storedSize);
			
			if ( sessKey != null && keyData != null )
			{
				stmt.setObject(pos++,sessKey.getKeyId().getUUID(),Types.OTHER);
				stmt.setString(pos++,keyData);
			}
			else
			{
				stmt.setNull(pos++,Types.OTHER);
				stmt.setNull(pos++,Types.VARCHAR);
			}

			ByteBlob bb = new ByteBlob(storeData);
			if ( ! isOracle )
				stmt.setBlob(pos++, bb);
            
			stmt.setObject(pos++,blobId.getUUID(),Types.OTHER);
            
			int num = stmt.executeUpdate();
        
			if ( num != 1 )
			{
				app.warning("BlobDb.update() - Failed to update blob id: " + blobId + "; origSize: " + origSize + "; update count: " + num +
						"; compressOption: " + compressOption + "; encryptOption: " + encryptOption);
			}
 
			/**
			 * For crappy Oracle, we need to store an empty blob first, then select it back out to get the
			 * "handle" to it, then write the actual blob into it.
			 */
            /*
			if ( isOracle )
			{
				stmt.close();
				stmt = con.prepareStatement("SELECT blob_data FROM esf_blob WHERE id=? FOR UPDATE");
				stmt.setObject(1,blobId.getUUID(),Types.OTHER);
				ResultSet rs = stmt.executeQuery();
				if ( rs.next() )
				{
					ybb.updateOracleBlob(rs.getBlob(1));
				}
				else
				{
					app.err("Blob.update() - Failed to find/update the oracle blob with id: " + blobId);
					return false;
				}
			}
            */
            
			return true;
		}
		catch( java.sql.SQLException e )
		{
			app.sqlerr(e,"BlobDb.update() blobId: " + blobId + "; compressOption: " + compressOption + "; encryptOption: " + encryptOption);
			throw e;
		}
		finally
		{
			app.cleanupStatement(stmt);
		}
	}
	
    public boolean update(Connection con, EsfUUID blobId, String data, CompressOption compressOption, EncryptOption encryptOption)
		throws SQLException
	{
		byte[] b = EsfString.stringToBytes(data);
	
		return update(con,blobId,b,compressOption,encryptOption);
	}

	/**
	 * Deletes an existing blob.
	 * @param con the Connection to use for JDBC and transaction control
	 * @param blobId the EsfUUID of the blob to be deleted
	 * @return the boolean indicating whether the delete occurred or not
	 * @throws SQLException if there's an SQL exception during the delete
	 */
	public boolean delete(Connection con, EsfUUID blobId)
		throws SQLException
	{
		if ( app.isDebugEnabled() )
			app.debug("BlobDb.delete() - blob id: " + blobId);
	    
		PreparedStatement stmt = null;
		try
		{
			stmt = con.prepareStatement("DELETE FROM esf_blob WHERE id=?");
			stmt.setObject(1, blobId.getUUID(), Types.OTHER);
            
			int num = stmt.executeUpdate();
        
			if ( num != 1 )
			{
				app.err("BlobDb.delete() - Failed to delete blob id: " + blobId);
				// TODO: CLEANUP NEXT TWO LINES ONCE WE KNOW WHAT IS HAPPENING
				(new Exception()).printStackTrace(); 
				app.err("BlobDb.delete() - Check Tomcat logs for stack trace for blob id: " + blobId);
				return false;
			}
 
			return true;
		}
		catch( java.sql.SQLException e )
		{
			app.sqlerr(e,"BlobDb.delete() blobId: " + blobId);
			throw e;
		}
		finally
		{
			app.cleanupStatement(stmt);
		}
	}
}