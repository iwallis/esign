// Copyright (C) 2009-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.ListIterator;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.exception.EsfException;


/**
* Maintains a pool of database connections based on the parameters stored
* in property bundle file ConnectionPool.
* @author Yozons, Inc.
*/
public class ConnectionPool
{
	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ConnectionPool.class);

	public static final String POSTGRESQL_LABEL = "postgresql"; // as defined in connectionpools.properties
	public static final String ORACLE_LABEL     = "oracle";
	protected static final short POSTGRESQL  	= 0;
	protected static final short ORACLE      	= 1;
	
	protected Application app       = Application.getInstance();
	 
	protected String name           = null;
	protected LinkedList<MyConnection> availPool  = new LinkedList<MyConnection>();
	protected LinkedList<MyConnection> inusePool  = new LinkedList<MyConnection>();
	
	protected static int _maxInuseConnections = 0;
	 
	// For database access.
	protected java.sql.Driver driver = null;
	protected String driverName     = null;
	protected String userName       = null;
	protected String password       = null;
	protected String url            = null;
	protected short  dbType;
	protected int    maxConnections;
	protected int    maxIdleMinutes;
 
	 /** 
	  * Creates a ConnectionPool to a given database.
	  * @throws EsfException if the pool cannot be created
	  */
  	 public ConnectionPool( String name,
  	 						String dbType,
							String driverName,
							String userName,
							String password,
							String url,
							int    maxConnections,
							int    maxIdleMinutes
						  )
  	 	throws EsfException
  	 {
  	 	try
		{
	  	 	this.name           = name;
		    setDbType(dbType);
		    this.driverName     = driverName;
		    this.userName       = userName;
		    this.password       = password;
		    this.url            = url;
		    this.maxConnections = maxConnections;
		    this.maxIdleMinutes = maxIdleMinutes;
		    
		    createPools();
		}
  	 	catch( Exception e )
		{
  	 		throw new EsfException("Unable to create ConnectionPool to db: " + name);
		}
	 }
	
	
	private void setDbType(String v)
	{
	    if ( v.equalsIgnoreCase(ORACLE_LABEL) )
	        dbType = ORACLE;
	    else if ( v.equalsIgnoreCase(POSTGRESQL_LABEL) )
	        dbType = POSTGRESQL;
	    else
	    {
	    	_logger.error("setDbType(" + name + ") called with unexpected type: " + v);
	        dbType = POSTGRESQL;
	    }
	}
	
	public String getDbName()
	{
		int lastSlash = url.lastIndexOf("/");
		return url.substring(lastSlash+1);
	}
	
	public boolean isPostgresql()
	{
	    return dbType == POSTGRESQL;
	}
	public boolean isOracle()
	{
	    return dbType == ORACLE;
	}
	public String getDbTypeString()
	{
	    if ( dbType == ORACLE )
	        return ORACLE_LABEL;
	    return POSTGRESQL_LABEL;
	}
	
	public String getDbVersionString()
	{
        Connection con = getConnection();
        try
        {
        	// PG specific: select version();
        	return con.getMetaData().getDatabaseProductName() + " " + con.getMetaData().getDatabaseProductVersion();
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"getDbVersionString()");
            rollbackIgnoreException(con,e);
            return "Error getting DB version";
        }
        finally
        {
            app.cleanupPool(this,con,null);
        }
	}
	
	public String getUserName()
	{
		return userName;
	}
	
	public String getName()
	{
		return name;
	}
    
    public int getMaxConnections()
    {
        return _maxInuseConnections;
    }
    
    public int getMaxConnectionsAllowed()
    {
        return maxConnections;
    }
    
    public int getMaxIdleMinutes()
    {
        return maxIdleMinutes;
    }
    
    public int getNumInuseConnections()
    {
        return inusePool.size();
    }
	
    public int getNumAvailableConnections()
    {
        return availPool.size();
    }
    
	
 	/**
 	 * Gets a connection from the pool, waiting until one is available if none are.
 	 * @return Connection to use for db access.  Call putConnection() when you are done.
 	 * @see #putConnection(Connection)
  	*/
	public Connection getConnection()
 	{
	    synchronized(availPool)
	    {
	        while ( availPool.size() == 0 )
	        {
	     	    if ( inusePool.size() < maxConnections )
	     	    {
	     	    	_logger.info("getConnection(" + name + ") - Creating another Connection...");
	     	        MyConnection c = makeConnection();
	     	        if ( c != null )
	     	            availPool.addFirst(c);
	     	       _logger.debug("getConnection(" + name + ") - Created another Connection; availPool.size: " + availPool.size() + "; inusePool.size: " + inusePool.size());
	     	    }
	     	    else
	     	    {
	     	        try
	     	        {
	     	        	_logger.warn("getConnection(" + name + ") - waiting for connection to become available; availPool.size: " + availPool.size() + "; inusePool.size: " + inusePool.size());
	     	            availPool.wait();
	     	           _logger.warn("getConnection(" + name + ") - connection became available; availPool.size: " + availPool.size() + "inusePool.size: " + inusePool.size());
	     	        }
	     	        catch( InterruptedException e ) {}
	     	    }
	        }
	         
	        MyConnection c = availPool.removeFirst();
	         
	        if ( ! isConnectionOpen(c.getConnectionInternal()) )
	        {
	             // The connection is stale, so get a replacement
	             // installed and return that one.
	             c = makeConnection();
	             if ( c == null )
	             {
	            	 _logger.warn("getConnection(" + name + ") - failed to get a replacement connection on attempt 1; waiting 10 seconds...availPool.size: " + availPool.size() + "; inusePool.size: " + inusePool.size());
	             	app.sleep(10000); // wait 10 seconds and we'll try again...
					c = makeConnection();
					if ( c == null )
					{
						_logger.warn("getConnection(" + name + ") - failed to get a replacement connection on attempt 2; waiting 20 seconds...availPool.size: " + availPool.size() + "; inusePool.size: " + inusePool.size());
						app.sleep(20000); // wait 20 seconds and we'll try one more time...
						c = makeConnection();
						if ( c == null )
						{
							_logger.error("getConnection(" + name + ") - failed to get a replacement connection after attempt 3; availPool.size: " + availPool.size() + "; inusePool.size: " + inusePool.size());
							if ( availPool.size() > 0 )
								return getConnection(); // try recursion to get another connection that we already have
							return null;
						}
					}
	             }
	         }

	         // Add to the front since the odds are the most recently used will also be returned soon
	         inusePool.addFirst(c);
             
             if ( inusePool.size() > _maxInuseConnections )
                 _maxInuseConnections = inusePool.size();
	         
	         try
	         {
	             if ( c.getConnectionInternal().getAutoCommit() )
	            	 _logger.warn("getConnection(" + name + ") - autoCommit=" + c.getConnectionInternal().getAutoCommit());
	         } 
	         catch(SQLException se) {}
	         
	         return c.getConnection();
	     }
 	}
    
 
 
	/**
	 * Puts the connection back into the pool.
	 */
	public void putConnection( Connection c )
	{
		if ( c == null )
			return;
         
		try
		{
			java.sql.SQLWarning warnings = c.getWarnings();
			if( warnings != null )
			{
				_logger.sqlerr(warnings,"putConnection(" + name + ") - found SQLWarnings");
			}
			c.clearWarnings();
		}
     	catch( SQLException se ) {}
     
     	synchronized(availPool)
		{
     	    MyConnection con = null;
            ListIterator<MyConnection> iter = inusePool.listIterator();
            while ( iter.hasNext() )
            {
                con = iter.next();
                if ( con.isMatchingConnection(c) )
                {
                    iter.remove();
                    break;
                }
            }
            
            if ( con == null )
            {
                try
                {
                	_logger.warn("putConnection(" + name + ") - Connection was not previously in use; closing it and not saving it back in the pool");
                    c.close();
                }
                catch( SQLException e ) {}
            }
            else
            {
                availPool.addFirst(con);
                availPool.notifyAll();
            }
		}
	}
 
 
 	/**
 	 * Establish a connection to the database.
 	 */
 	private MyConnection makeConnection()
 	{
	    try 
	    {
	    	_logger.info("makeConnection(" + name + ") - Creating database connection to db " + getDbTypeString() );
	        // Establish Connection to the database at URL with userName and password
	        Connection c = DriverManager.getConnection(url, userName, password);
	        c.setAutoCommit(false);
	        c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
	        return new MyConnection(c);
	    }
		// print out decent error messages
		catch(SQLException ex) 
		{
			_logger.sqlerr(ex,"makeConnection(" + name + ") to URL: " + url);
	    }
	
	    return null;
 }
 
 
 	/**
 	 * Verifies that a Connection is still open.
  	*/
 	private boolean isConnectionOpen( Connection c )
 	{
	    if ( c != null )
	    {
	        try
	        {
	            return ! c.isClosed();
	        }
	        catch ( SQLException e ) {}
	    }
	     
	    return false;
 	}
 	
 	
 	java.sql.Driver getDriverForMyUrl()
 	{
 		try
 		{
 	    	java.util.Enumeration<java.sql.Driver> existingDrivers = DriverManager.getDrivers();
 	    	while( existingDrivers.hasMoreElements() )
 	    	{
 	    		java.sql.Driver existingDriver = existingDrivers.nextElement();
 	    		if ( existingDriver != null && existingDriver.acceptsURL(url) )
 	    			return existingDriver;
 	    	}
 		}
 		catch( SQLException e )
 		{
    		_logger.error("getDriverForMyUrl() - DriverManager.getDrivers() exception: " + url, e);
 		}
    	return null;
 	}


 	/**
 	 * Returns the pool if it's been created.  If not, it creates the pool of initial
 	 * connections.
 	 */
 	void createPools()
 		throws Exception
    {
 		java.sql.Driver existingDriver = getDriverForMyUrl(); 
 		if ( existingDriver != null )
    		_logger.info("createPools(" + name + ") BEFORE LOADING DriverManager.getDrivers() unexpectedly returned a suitable driver: " + existingDriver + 
    				"; name: " + existingDriver.getClass().getName() + "; accepts URL: " + url);
	    	
	    try
	    {
	 		Class.forName(driverName);
	    }	    	
	    catch(ClassNotFoundException e) 
	    {
	    	_logger.error("createPools(" + name + ") JDBC Driver " + driverName + ", ClassNotFoundException", e);
	        throw e;
	    }
    		
 		existingDriver = getDriverForMyUrl(); 
		if ( existingDriver == null )
		{
    		_logger.error("createPools(" + name + ") AFTER LOADING DriverManager.getDrivers() returned no suitable driver that accepts URL " + url);
    		throw new EsfException("No JDBC driver for URL: " + url);
		}
		
		driver = existingDriver;

	    synchronized(availPool)
		{
		    inusePool.clear();
		    
		    while( availPool.size() < 1 )
		    {
		    	// Establish Connection to the database at URL with userName and password
		    	availPool.addLast(makeConnection());
		    }
		}
 }
 
 	protected boolean isPostgresqlDuplicateKey(SQLException e)
 	{
		if ( app.isDebugEnabled() )
			_logger.sqlerr(e,"DEBUG: isPostgresqlDuplicateKey(" + name + ")");
		String msg = e.getMessage();
		if ( msg == null )
			return false;
		return msg.indexOf("duplicate key") > 0;
 	}
 
 	protected boolean isOracleDuplicateKey(SQLException e)
 	{
		if ( app.isDebugEnabled() )
			_logger.sqlerr(e,"DEBUG: isOracleDuplicateKey(" + name + ")");
		return e.getErrorCode() == 1;
 	}
 
 	public boolean isDuplicateKey(SQLException e)
 	{
	     if ( e == null )
	         return false;
	     if ( isOracle() )
	         return isOracleDuplicateKey(e);
	     return isPostgresqlDuplicateKey(e);
 	}
 
 /**
  * Simple routine used when a rollback is desired, but reporting of rollback errors
  * is not wanted.  This is primarily used by code in exception catch blocks in
  * which an exception is thrown and we just want to ensure the transaction is 
  * rolled back even if it's not in the right state to rollback.  Normal business
  * logic rollbacks typically should not call this as an exception on a rollback
  * should be detected.
  * 
  * @param con the Connection object that should be rolled back.
  */
 	public final void rollbackIgnoreException(Connection con)
 	{
		if ( con != null )
		{
			try 
			{ 
				con.rollback(); 
			} 
			catch(SQLException e) 
			{
				if ( app.isDebugEnabled() )
					_logger.sqlerr(e,"DEBUG: rollbackIgnoreException(" + name + ")");
			}
		}
 	}

	/**
	 * Same as rollbackIgnoreException(Connection) followed by a call to closeIfBadConnection(Connection,SQLException).
	 * This routine should be used in the case where an SQLException occurs, the transaction therefore should be rolled
	 * back, and we check the SQLException to determine if it implies a bad connection or not.
	 *
	 * @param con the Connection object in question; may be closed
	 *        if the exception is determined to be the result of a dead connection.
	 * @param e the SQLException that arose that caused 'con' to fail
	 */
	public final void rollbackIgnoreException(Connection con, SQLException e)
	{
		rollbackIgnoreException(con);
		closeIfBadConnection(con,e);
		// If this connection is closed, then let's check all other connections that aren't in use
		if ( ! isConnectionOpen( con ) ) 
			checkAllUnusedConnections();
	}   
	
	 
	/**
	 * This routine first checks the SQLException to see if the SQLState indicates an
	 * I/O exception that would imply the Connection is bad and can be closed.  If 
	 * the exception doesn't show it, we submit a dummy SQL command 'select 1;' 
	 * to see if we can still communicate with the database or not, and if not, it is closed.
	 * 
	 * @param con the Connection object in question; may be closed
	 *        if the exception is determined to be the result of a dead connection.
	 * @param e the SQLException that arose that caused 'con' to fail
	 */
	public final void closeIfBadConnection(Connection con, SQLException e)
	{
		if ( con != null )
		{
			// First, let's try to the SQLState check since it's straightforward.
			if ( e != null )
			{
				if ( e.getSQLState() != null && e.getSQLState().startsWith("08") ) // 08 class is for connection exceptions
				{
					if ( app.isDebugEnabled() )
						_logger.sqlerr(e,"closeIfBadConnection(" + name + ") SQLState (" + e.getSQLState() + ") detected problematic Connection; closing it");
					else
						_logger.error("closeIfBadConnection(" + name + ") SQLState (" + e.getSQLState() + ") detected problematic Connection; closing it - " +
									  e.getMessage());
					
					try
					{
						con.close();  // it's no good, and our pool will reopen it when it's requested again later
					}
					catch( Exception e2 ) {}
					
					return; // we already handled matters here
				}
			}
			
			// Well, we're not sure, so let's do a dummy query and see if anything bad happens.
			PreparedStatement stmt = null;
			String dummyQuery = ( isOracle() ) ? "SELECT 1 FROM esf_deployment" : "SELECT 1";
			try 
			{ 
				// HACK - This only works for databases with the 'deployment' table, which is true for Open eSignForms
				// but wouldn't be if we attempted to use this ConnectionPool in other Oracle applications.
				stmt = con.prepareStatement(dummyQuery);
				stmt.executeQuery(); 
			} 
			catch(SQLException e2) 
			{
				if ( app.isDebugEnabled() )
					_logger.sqlerr(e2,"closeIfBadConnection(" + name + ") dummy SQL (" + dummyQuery + ") detected problematic Connection; closing it");
				else
					_logger.error("closeIfBadConnection(" + name + ") dummy SQL (" + dummyQuery + ") detected problematic Connection; closing it",e);
				try
				{
					con.close();  // it's no good, and our pool will reopen it when it's requested again later
				}
				catch( SQLException e3 ) {}
			}
			finally
			{
				if ( stmt != null )
					try { stmt.close(); } catch( SQLException e4 ) {}
			}
		}
	}    
	
	
	// Utility routine that checks all Connection objects that are in the pool
	// to see if they are okay.  This is called only when a bad connection is
	// detected, since if one is bad, there's a good chance the others have gone stale as well.
	void checkAllUnusedConnections()
	{
		synchronized(availPool)
		{
            for( MyConnection con : availPool )
				closeIfBadConnection(con.getConnectionInternal(),null);
		}		
	}
	
    public void resizeConnections(EsfDateTime sinceDateTime)
    {
        synchronized(availPool)
        {
            int startingAvailSize = availPool.size();
            MyConnection con = null;
            ListIterator<MyConnection> iter = availPool.listIterator();
            while ( iter.hasNext() )
            {
                con = iter.next();
                if ( con.closeIfUnusedSince(sinceDateTime) )
                    iter.remove();
            }
            
            if ( availPool.size() != startingAvailSize )
                _logger.debug("resizeConnections(" + name + ") - availPool.size: " + availPool.size() + 
                          "; previous availPool.size: " + startingAvailSize + "; inusePool.size: " + inusePool.size());
        }
    }
	
	public void destroy()
	{
		if ( name == null )
			return;
		
		if ( availPool != null )
		{
            for( MyConnection con : availPool )
			{
            	_logger.info("destroy(" + name + ") - Closing unused database connection to db " + getDbTypeString() );
				con.close();
			}
            availPool.clear();
		}
        
		availPool = null;
		
		if ( inusePool != null )
		{
            for( MyConnection con : inusePool )
			{
				rollbackIgnoreException(con.getConnectionInternal()); // Odd if true that we're shutting down with active connections
				_logger.warn("destroy(" + name + ") - Closing IN USE database connection to db " + getDbTypeString() );
				con.close();
			}
            inusePool.clear();
		}
		
    	if ( driver != null ) 
    	{
		    try 
		    {
		    		DriverManager.deregisterDriver(driver);
		    		_logger.debug("destroy() - DriverManager.deregisterDriver() " + driver);
		    } 
		    catch(java.sql.SQLException e) 
		    {
				_logger.sqlerr(e,"destroy(" + name + ") - deregistering driver exception to db " + getDbTypeString() + "; driver: " + driver );
		    }
		}
		inusePool = null;
		name = null;
	    driver = null;
	}
	
    
	public void finalize()
		throws Throwable
	{
		super.finalize();
		destroy();
	}
    
    
    public class MyConnection
    {
        private Connection	con;
        private EsfDateTime	lastUsedDateTime;
        
        public MyConnection(Connection con)
        {
            this.con = con;
            lastUsedDateTime = new EsfDateTime();
        }
        
        public Connection getConnection()
        {
        	lastUsedDateTime = new EsfDateTime();
            return con;
        }
        
        public Connection getConnectionInternal()
        {
            return con;
        }
        
        public boolean isMatchingConnection(Connection con)
        {
            return this.con == con;
        }
        
        public void close()
        {
            try
            {
                if ( con != null )
                    con.close();
            }
            catch( SQLException e ) {}
            finally
            {
                con = null;
                lastUsedDateTime = null;
            }
        }
        
        public boolean closeIfUnusedSince(EsfDateTime sinceDateTime)
        {
            if ( lastUsedDateTime.isBefore(sinceDateTime) )
            {
                _logger.debug("closeIfUnusedSince(" + name + ") closing unused connection...");
                close();
                return true;
            }
            return false;
        }
    }
}