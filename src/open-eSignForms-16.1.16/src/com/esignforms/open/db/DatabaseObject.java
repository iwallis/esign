// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.db;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.UUID;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfDecimal;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.util.XmlUtil;

/**
* DatabaseObject is the base class for all objects that want to store themselves
* in the database.
* 
* @author Yozons, Inc.
*/


public class DatabaseObject 
	implements java.io.Serializable
{
	private static final long serialVersionUID = -392183163282100101L;

	protected boolean doInsert;   // true=INSERT; false=UPDATE when on the database
    protected boolean hasChanged; // true if the data in the object doesn't match the database
    protected boolean isDeleted;  // true if the object this represents was deleted from the database
    
    protected SQLException lastSQLException = null;
    
    protected DatabaseObject()
    {
        setNewObject();
    }
    
    public void setDatabaseObjectLike(DatabaseObject o)
    {
        doInsert   = o.doInsert;
        hasChanged = o.hasChanged;
        isDeleted  = o.isDeleted;
        lastSQLException = o.lastSQLException;
    }
    
    /**
     * Marks the object as freshly created (not yet stored in the database).
     * This implies it's "updated" so we need to insert it should it be saved.
     */
    protected void setNewObject()
    {
        doInsert   = true;
        hasChanged = false;
        isDeleted  = false;
        lastSQLException = null;
    }
    
    /**
     * Marks the object as loaded from the database.
     * This implies that any save will use an "update" should it be changed.
     */
    protected void setLoadedFromDb()
    {
        doInsert   = false;
        hasChanged = false;
        isDeleted  = false;
        lastSQLException = null;
    }
    
    public boolean doInsert()
    {
        return doInsert;
    }

    public boolean doUpdate()
    {
        return ! doInsert;
    }

    public boolean hasChanged()
    {
        return hasChanged;
    }

    protected void objectChanged()
    {
        hasChanged = true;
    }
    
    public boolean isDeleted()
    {
        return isDeleted;
    }

    protected void objectDeleted()
    {
        isDeleted = true;
    }
    
    protected final boolean isBlank(String s)
    {
        return EsfString.isBlank(s);
    }
    
    protected final boolean isBlank(String[] s)
    {
        return EsfString.isBlank(s);
    }
    
    protected final boolean isValidEmail(String e)
    {
        return EsfEmailAddress.isValidEmail(e);
    }
    
    public SQLException getLastSQLException()
    {
        return lastSQLException;
    }
    public boolean hasLastSQLException()
    {
        return lastSQLException != null;
    }
    protected void setLastSQLException(SQLException e)
    {
        lastSQLException = e;
    }
    protected void clearLastSQLException()
    {
        lastSQLException = null;
    }
    public boolean lastSQLExceptionIsDuplicateKey()
    {
        if ( ! hasLastSQLException() )
            return false;
        return Application.getInstance().getConnectionPool().isDuplicateKey(lastSQLException);
    }
    
    public static class EsfResultSet
    {
    	int col;
    	ResultSet rs;
    	public EsfResultSet( ResultSet rs )
    	{
    		col = 1;
    		this.rs = rs;
    	}
    	
    	public boolean next()
    		throws SQLException
    	{
    		col = 1;
    		return rs.next();
    	}
    	
    	public EsfDateTime getEsfDateTime()
			throws SQLException
		{
			Timestamp v = rs.getTimestamp(col++);
			return ( rs.wasNull() ) ? null : new EsfDateTime(v);
		}

    	public EsfDate getEsfDate()
			throws SQLException
		{
			java.sql.Date v = rs.getDate(col++);
			return ( rs.wasNull() ) ? null : new EsfDate(v);
		}

    	public EsfDecimal getEsfDecimal()
			throws SQLException
		{
			BigDecimal v = rs.getBigDecimal(col++);
			return rs.wasNull() ? null : new EsfDecimal(v);
		}

    	public EsfInteger getEsfInteger()
			throws SQLException
		{
			long v = rs.getLong(col++);
			return ( rs.wasNull() ) ? null : new EsfInteger(v);
		}

    	public EsfName getEsfName()
			throws SQLException
		{
			String v = rs.getString(col++);
			return ( rs.wasNull() ) ? null : new EsfName(v);
		}
		
    	public EsfPathName getEsfPathName()
    		throws SQLException
    	{
    		String v = rs.getString(col++);
    		return ( rs.wasNull() ) ? null : new EsfPathName(v);
    	}
    	
    	public EsfString getEsfString()
			throws SQLException
		{
			String v = rs.getString(col++);
			return ( rs.wasNull() ) ? null : new EsfString(v);
		}
	
    	public EsfUUID getEsfUUID()
			throws SQLException
		{
    		UUID v = (UUID)rs.getObject(col++);
    		return ( rs.wasNull() ) ? null : new EsfUUID(v);
		}
    	
    	public BigDecimal getBigDecimal()
			throws SQLException
		{
			return rs.getBigDecimal(col++);
		}

    	public long getLong()
			throws SQLException
		{
			return rs.getLong(col++);
		}
	
       	public Long getLongObject()
			throws SQLException
		{
			long v = rs.getLong(col++);
			return ( rs.wasNull() ) ? null : new Long(v);
		}

    	public int getInt()
			throws SQLException
		{
			return rs.getInt(col++);
		}
	
	   	public Integer getInteger()
			throws SQLException
		{
			int v = rs.getInt(col++);
			return ( rs.wasNull() ) ? null : new Integer(v);
		}

    	public short getShort()
			throws SQLException
		{
			return rs.getShort(col++);
		}
	
	   	public Short getShortObject()
			throws SQLException
		{
			short v = rs.getShort(col++);
			return ( rs.wasNull() ) ? null : new Short(v);
		}

    	public String getString()
			throws SQLException
		{
			String v = rs.getString(col++);
			return ( rs.wasNull() ) ? null : v;
		}

    	public void close()
    		throws SQLException
    	{
    		rs.close();
    		rs = null;
    	}
    }
    
    public static class EsfPreparedStatement
    {
    	int col;
    	PreparedStatement stmt;
    	public EsfPreparedStatement(Connection con, String sqlString)
    		throws SQLException
    	{
    		col = 1;
    		stmt = con.prepareStatement(sqlString);
    	}
    	
    	public EsfResultSet executeQuery()
    		throws SQLException
    	{
    		col = 1;
    		return new EsfResultSet( stmt.executeQuery() );
    	}
    	
    	public int executeUpdate()
			throws SQLException
		{
			col = 1;
			return stmt.executeUpdate();
		}
		
    	public void close()
    		throws SQLException
    	{
    		stmt.close();
    		stmt = null;
    	}
    	
        public void set(int pos, String v)
			throws SQLException
		{
			if ( v == null )
				stmt.setNull(pos,Types.VARCHAR);
			else
				stmt.setString(pos,v);
		}
        public void set(String v)
			throws SQLException
		{
        	set(col++,v);
		}
	
	    public void set(int pos, EsfString v)
	    	throws SQLException
	    {
	    	if ( v == null || v.isNull() )
	    		stmt.setNull(pos,Types.VARCHAR);
	    	else
	    		stmt.setString(pos,v.toPlainString());
	    }
        public void set(EsfString v)
			throws SQLException
		{
	    	set(col++,v);
		}
	    
	    public void set(int pos, EsfDate v)
			throws SQLException
		{
			if ( v == null || v.isNull() )
				stmt.setNull(pos,Types.DATE);
			else
				stmt.setDate(pos,v.toSqlDate());
		}
	    public void set(EsfDate v)
			throws SQLException
		{
	    	set(col++,v);
		}

	    public void set(int pos, EsfDateTime v)
			throws SQLException
		{
			if ( v == null || v.isNull() )
				stmt.setNull(pos,Types.TIMESTAMP);
			else
				stmt.setTimestamp(pos,v.toSqlTimestamp());
		}
        public void set(EsfDateTime v)
			throws SQLException
		{
	    	set(col++,v);
		}
	
	    public void set(int pos, EsfInteger v)
			throws SQLException
		{
			if ( v == null || v.isNull() )
				stmt.setNull(pos,Types.BIGINT);
			else
				stmt.setLong(pos,v.toLong());
		}
	    public void set(EsfInteger v)
			throws SQLException
		{
	    	set(col++,v);
		}

	    public void set(int pos, EsfName v)
			throws SQLException
		{
			if ( v == null || v.isNull() )
				stmt.setNull(pos,Types.VARCHAR);
			else
				stmt.setString(pos,v.toPlainString());
		}
        public void set(EsfName v)
			throws SQLException
		{
	    	set(col++,v);
		}

	    public void set(int pos, EsfDecimal v)
			throws SQLException
		{
			if ( v == null || v.isNull() )
				stmt.setNull(pos,Types.NUMERIC);
			else
				stmt.setBigDecimal(pos,v.toBigDecimal());
		}
	    public void set(EsfDecimal v)
			throws SQLException
		{
	    	set(col++,v);
		}

	    public void set(int pos, EsfPathName v)
			throws SQLException
		{
			if ( v == null || v.isNull() )
				stmt.setNull(pos,Types.VARCHAR);
			else
				stmt.setString(pos,v.toPlainString());
		}
        public void set(EsfPathName v)
			throws SQLException
		{
	    	set(col++,v);
		}
	
	    public void set(int pos, EsfUUID v)
			throws SQLException
		{
			if ( v == null || v.isNull() )
				stmt.setNull(pos,Types.OTHER);
			else
				stmt.setObject(pos,v.getUUID(),Types.OTHER);
		}
        public void set(EsfUUID v)
			throws SQLException
		{
	    	set(col++,v);
		}

        public void set(int pos, int v)
			throws SQLException
		{
        	stmt.setInt(pos,v);
		}
	    public void set(int v)
			throws SQLException
		{
	    	set(col++,v);
		}

        public void set(int pos, long v)
			throws SQLException
		{
	    	stmt.setLong(pos,v);
		}
	    public void set(long v)
			throws SQLException
		{
	    	set(col++,v);
		}

        public void set(int pos, short v)
			throws SQLException
		{
	    	stmt.setShort(pos,v);
		}
	    public void set(short v)
			throws SQLException
		{
	    	set(col++,v);
		}

    	public String toString()
    	{
    		return stmt.toString();
    	}
    }
    
    public static ConnectionPool getConnectionPool()
    {
        return Application.getInstance().getConnectionPool();
    }

    public static void cleanupPool(ConnectionPool pool, Connection con, EsfPreparedStatement stmt)
    {
        cleanupStatement(stmt);
        Application.getInstance().cleanupPool(pool, con, null);
    }

    public static void cleanupStatement(EsfPreparedStatement stmt)
    {
        if ( stmt != null )
            try { stmt.close(); } catch( Exception e ) {}
    }
    
    protected EsfUUID getEsfUUID()
    {
        return new EsfUUID();
    }
    
    protected BlobDb getBlobDb()
    {
        return Application.getInstance().getBlobDb();
    }
    
    /**
     * Converts the string to be an XML safe string.
     */
    public final String escapeXml(String s)
    {
        return XmlUtil.toEscapedXml(s);
    }
    public final String base64encode(byte[] v)
    {
    	return com.esignforms.open.util.Base64.encodeToString(v);
    }
    
    public final static String escapeLIKE(String s)
    {
		if ( s == null )
			return "";

		// The gamble here is that most strings will not contain the special characters,
		// so we optimize things in that case by returning the original string without creating any new objects.
		int length = s.length();
		int currentPos;

		for ( currentPos = 0; currentPos < length; ++currentPos )
		{
			char c = s.charAt( currentPos );

			if ( c == '%' || c == '_' || c == '[' )
			{
				break; // we'll need to escape this one
			}
		}

		// No quoting necessary, so just return the original string back.
		if ( currentPos >= length )
		{
			return s;
		}
     
		StringBuilder buf = new StringBuilder(length);
		buf.append(s.substring( 0, currentPos ));

		for ( ; currentPos < length; ++currentPos )
		{
			char c = s.charAt( currentPos );

			switch ( c )
			{
			case '%':
			case '_':
			case '[':
				buf.append( '\\' );
				buf.append(c);
				break;

			default:
				buf.append( c );
				break;
			}
		}

		return buf.toString();
    }

}