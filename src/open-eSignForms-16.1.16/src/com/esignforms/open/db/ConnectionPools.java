// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2009 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.db;

import java.util.LinkedList;
import java.util.StringTokenizer;

import com.esignforms.open.config.PropertiesFile;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.exception.EsfException;

/**
 * Maintains a list of ConnectionPool objects based on the parameters stored
 * in property bundle file ConnectionPool.
 * @author Yozons, Inc.
 */
public class ConnectionPools
{
	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ConnectionPools.class);

	protected ConnectionPool[] pools = null;
    
    /** 
     * Creates all pools defined in the connectionpools.properties file.
     * @throws EsfException if any of the pools could not be created
     */
	public ConnectionPools()
		throws EsfException
	{
        String bundleName = "connectionpools";
            
        try
        {
    		LinkedList<ConnectionPool> list = new LinkedList<ConnectionPool>();
    		
            PropertiesFile bundle = new PropertiesFile(bundleName);
            
            String poolList = bundle.getString("pools","");
            StringTokenizer st = new StringTokenizer(poolList,",");
            while( st.hasMoreTokens() )
            {
            	String poolName = st.nextToken();
            	
            	String driverName  = bundle.getString(poolName + ".driverName");
            	String userName    = bundle.getString(poolName + ".dbUserName");
            	String password    = bundle.getString(poolName + ".dbPassword");
            	String url         = bundle.getString(poolName + ".dbURL");
                String dbType      = bundle.getString(poolName + ".dbType",ConnectionPool.POSTGRESQL_LABEL);
                int maxConnections = bundle.getInteger(poolName + ".maxConnections",10);
                int maxIdleMinutes = bundle.getInteger(poolName + ".maxIdleMinutes",10);

                if ( maxConnections < 1 )
                	maxConnections = 1;
                if ( maxIdleMinutes < 1 )
                	maxIdleMinutes = 1;
               
                _logger.info("Creating connection pool: " + poolName + "; type: " + dbType + 
                		     "; maxConnections: " + maxConnections + "; maxIdleMinutes: " + maxIdleMinutes);
                ConnectionPool pool = new ConnectionPool( poolName, dbType, driverName, userName, password, url, maxConnections, maxIdleMinutes);
                list.add(pool);
            }
            
            pools = new ConnectionPool[list.size()];
            list.toArray(pools);
            list.clear();
        }
        catch( Exception e )
        {
        	_logger.error("Failed to read/parse connectionpools.properties file '" + bundleName + "'",e);
            throw new EsfException("Unable to create db pools: " + e.getMessage());
        }            
    }
	
	/**
	 * Gets the ConnectionPool that matches a given pool/db name.  The name is set in the 'pools' entry of ConnectionPool.properties.
	 * @param name the String name of the db/pool to retrieve
	 * @return the ConnectionPool object that matches the name; null if not found
	 */
	public ConnectionPool getPool( String name )
	{
		if ( name == null || pools == null )
			return null;
		
		for( int i=0; i < pools.length; ++i )
		{
			if ( name.equals(pools[i].getName()) )
				return pools[i];
		}
		
		return  null;
	}
	
	public void destroy()
	{
		if ( pools == null )
			return;
		
		for( int i=0; i < pools.length; ++i )
		{
			_logger.info("destroy() - Closing connection pool: " + pools[i].getName());
			pools[i].destroy();
		}
		
		pools = null;
	}
    
    public void resizeConnections()
    {
        if ( pools == null )
            return;
        
        for( int i=0; i < pools.length; ++i )
        {
            com.esignforms.open.data.EsfDateTime sinceDateTime = new com.esignforms.open.data.EsfDateTime();
            sinceDateTime.addMinutes(-1 * pools[i].getMaxIdleMinutes());
            pools[i].resizeConnections(sinceDateTime);
        }
    }
 }