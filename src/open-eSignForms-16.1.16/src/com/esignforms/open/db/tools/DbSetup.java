// Copyright (C) 2009-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.db.tools;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.crypto.SecretKey;

import com.esignforms.open.Application;
import com.esignforms.open.Version;
import com.esignforms.open.admin.BootService;
import com.esignforms.open.admin.PasswordManager;
import com.esignforms.open.admin.SignatureKey;
import com.esignforms.open.config.Literals;
import com.esignforms.open.crypto.SecretKeyGenerator;
import com.esignforms.open.crypto.PBE;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfHtml;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.data.NameValue;
import com.esignforms.open.data.Record;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.db.DatabaseObject;
import com.esignforms.open.db.DatabaseObject.EsfPreparedStatement;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.image.Thumbnail;
import com.esignforms.open.integration.paypal.NvpDirectPayment;
import com.esignforms.open.prog.ButtonMessage;
import com.esignforms.open.prog.ButtonMessageVersion;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentStyle;
import com.esignforms.open.prog.DocumentStyleVersion;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DocumentVersionPage;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.DropDownVersionOption;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.EmailTemplateVersion;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PropertySet;
import com.esignforms.open.prog.PropertySetVersion;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.Permission;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.UI;
import com.esignforms.open.user.User;
import com.esignforms.open.util.Base64;
import com.esignforms.open.util.DateUtil;

/**
 * Sets up the Open eSignForms database 
 * 
 * @author Yozons, Inc.
 */
public final class DbSetup
{
	private Application app     = null;
    private PBE			pbe     = null;

    public DbSetup(String contextPath)
    	throws Exception
    {
    	app = Application.getInstallationInstance(contextPath);
    	pbe = app.getPBE();
    }
    
    public Application getApplication()
    {
        return app;
    }
    
    public void destroy()
    {
    	app.destroy();
        app = null;
        pbe = null;
    }
    
    public void display(String msg)
    {
    	app.info(msg);
    	System.out.println(msg);
    }

    public void displayError(String msg)
    {
    	app.err(msg);
    	System.out.println(msg);
    }
    
	private User getASuperUser()
	{
		Group superGroup = Group.Manager.getSuperGroup();
		for( User superUser : superGroup.getMemberUsers(null) )
			return superUser;
		return null; // shouldn't happen!
	}
	
	

	// Keep all convert routines for historical reasons, even if we no longer make them available via the command line prompt.
	public void convert16_1_16() throws Exception
	{
		ReportFieldTemplate rft = ReportFieldTemplate.Manager.getByName(ReportFieldTemplate.BUILT_IN_RESUME_TRANSACTION_BUTTON);
		if ( rft == null )
		{
			rft = new ReportFieldTemplate( new EsfUUID(), ReportFieldTemplate.BUILT_IN_RESUME_TRANSACTION_BUTTON, ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Resume tran", "Resume this suspended transaction.", (short)0);
			if ( rft.save() )
				display("Created report field template: " + ReportFieldTemplate.BUILT_IN_RESUME_TRANSACTION_BUTTON);
		}
		else
			display("Report field template already exists: " + ReportFieldTemplate.BUILT_IN_RESUME_TRANSACTION_BUTTON);		
	}

	public void convert15_8_23() throws Exception
	{
		for ( TransactionTemplate tt : TransactionTemplate.Manager._dbsetup_getAll() )
		{
			if ( tt._dbsetup_add_UseAdminApiPermissionOption15_8_23() ) 
			{
				if ( tt.save() )
					display("Updated transaction template '" + tt.getPathName() + "' for Use Admin API permission option");
				else
					display("FAILED to update transaction template '" + tt.getPathName() + "' for Use Admin API permission option");
			}
			else
				display("Transaction template '" + tt.getPathName() + "' already has the Use Admin API permission option");				
		}
	}
	
	public void convert15_7_11() throws Exception
	{
		for ( TransactionTemplate tt : TransactionTemplate.Manager._dbsetup_getAll() )
		{
			if ( tt._dbsetup_add_UseUpdateApiPermissionOption15_7_11() ) 
			{
				if ( tt.save() )
					display("Updated transaction template '" + tt.getPathName() + "' for Use Update API permission option");
				else
					display("FAILED to update transaction template '" + tt.getPathName() + "' for Use Update API permission option");
			}
			else
				display("Transaction template '" + tt.getPathName() + "' already has the Use Update API permission option");				
		}
	}
	
	public void convert14_12_6() throws Exception
	{
		Group systemAdminGroup = Group.Manager.getSystemAdminGroup();
		
		Permission p = Permission.Manager.getByPathName(UI.UI_PERM_TRANSACTION_CSV_START_VIEW);
		if ( p == null )
		{
			p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_TRANSACTION_CSV_START_VIEW, "Access the CSV Upload screen to start transactions");
			p.addGroupToAllOptions(systemAdminGroup);
			if ( p.save() )
				display("For any users who need CSV upload to start transactions, be sure to give them permission to this new view.");
			else
				display("Did not save the permission: " + UI.UI_PERM_TRANSACTION_CSV_START_VIEW);
		}
		else
			display("Permission already exists: " + UI.UI_PERM_TRANSACTION_CSV_START_VIEW);
			
		
		ReportFieldTemplate rft = ReportFieldTemplate.Manager.getByName(ReportFieldTemplate.BUILT_IN_DOWNLOAD_SELECTED_SNAPSHOTS);
		if ( rft == null )
		{
			rft = new ReportFieldTemplate( new EsfUUID(), ReportFieldTemplate.BUILT_IN_DOWNLOAD_SELECTED_SNAPSHOTS, ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Documents", "Download selected documents.", (short)0);
			if ( rft.save() )
				display("Created report field template: " + ReportFieldTemplate.BUILT_IN_DOWNLOAD_SELECTED_SNAPSHOTS);
		}
		else
			display("Report field template already exists: " + ReportFieldTemplate.BUILT_IN_DOWNLOAD_SELECTED_SNAPSHOTS);
	}
	
	public void convert14_9_27() throws Exception
	{
		// Add the new PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_VIEW and PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE to all report templates
		for( ReportTemplate rt : ReportTemplate.Manager.getAll() )
		{
   	    	String normalizedId = rt.getId().toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(ReportTemplate.PERM_PATH_PREFIX+normalizedId);

   	    	Permission perm = Permission.Manager.getByPathName(permPathName);
   	    	if ( perm == null )
   	    		display("ERROR: Could not find report template Permission with pathname: " + permPathName);
   	    	else
   	    	{
        		if ( perm.hasOptionByName(PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_VIEW) )
        			display("Report template: " + rt.getPathName() + "; already has permission option: " + PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_VIEW + "; skipping...." );
        		else
        		{
        			boolean createdNewPermOption = perm.createOptionByNameForSuperGroup(PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_VIEW, 
        					PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_VIEW + " - View transaction documents directly from a report via esf_reports_access party");
        			if ( createdNewPermOption )
        			{
        				if ( perm.save() )
        					display("Added permission option: " + PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_VIEW + "; for report template: " + rt.getPathName());
        				else
        					display("ERROR: Failed to save added permission option: " + PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_VIEW + "; for report template: " + rt.getPathName());
        			}
        			else
    					display("ERROR: Failed to create new permission option: " + PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_VIEW + "; for report template: " + rt.getPathName());
        		}
        		
        		if ( perm.hasOptionByName(PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE) )
        			display("Report template: " + rt.getPathName() + "; already has permission option: " + PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE + "; skipping...." );
        		else
        		{
        			boolean createdNewPermOption = perm.createOptionByNameForSuperGroup(PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE, 
        					PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE + " - Update transaction documents directly from a report via esf_reports_access party");
        			if ( createdNewPermOption )
        			{
        				if ( perm.save() )
        					display("Added permission option: " + PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE + "; for report template: " + rt.getPathName());
        				else
        					display("ERROR: Failed to save added permission option: " + PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE + "; for report template: " + rt.getPathName());
        			}
        			else
    					display("ERROR: Failed to create new permission option: " + PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE + "; for report template: " + rt.getPathName());
        		}
   	    	}
		}
		
		User superUser = getASuperUser();
		EsfName[] buttonNames = { ButtonMessageVersion.PACKAGE_BUTTON_EDIT_DOCUMENT_ESFNAME, ButtonMessageVersion.PACKAGE_BUTTON_EDIT_DOCUMENT_TITLE_ESFNAME,
				ButtonMessageVersion.DOCUMENT_EDIT_BUTTON_SAVE_ESFNAME, ButtonMessageVersion.DOCUMENT_EDIT_BUTTON_SAVE_TITLE_ESFNAME};
		EsfString[] buttonValues = { new EsfString("Edit"), new EsfString("Click to edit/update this document's data."),
				new EsfString("Save"), new EsfString("Click to save the incomplete data in this document so you can review and submit it later.")};
		
		for( ButtonMessage bm : ButtonMessage.Manager.getAll() )
			convert13_5_5_AddButtonMessages(superUser, bm, buttonNames,  buttonValues);
	}


	public void convert14_4_12() throws Exception
	{
		User superUser = getASuperUser();
		
		EsfName[] newPropNames = { new EsfName("AppTitleInHtml") };
		EsfString[] newPropValues = { new EsfString("<div style=\"color: #105aa5; font-size: 12pt; font-weight: bold; text-align: center; width: 100%\">${property:MyCompany.InformalName}</div>") };
		String[] newPropComments = { "Allows HTML syntax - make sure it's correct!" };
		
		convert13_3_12_AddPropertySetProperties(superUser, Library.Manager.getTemplate(),app.getPropertySetESFEsfName(),newPropNames,newPropValues,newPropComments);
	}

	public void convert14_3_15() throws Exception
	{
		User superUser = getASuperUser();
		Library templateLibrary = Library.Manager.getTemplate();

		EsfHtml pageHtml = new EsfHtml(
"<style type=\"text/css\"> body.esf div.pagediv { max-width: 800px; padding-left: 1ex; padding-right: 1ex; margin-left: auto; margin-right: auto; } </style>\n" +
"<p style=\"margin-left: 25px;\">${image:Logo}</p>\n" +
"<% if ( esf.isTransactionDeleted() ) { %>\n" +	
"<div style=\"background-color: #fda514; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">\n" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px;\">This transaction has been deleted...</h2>\n" +
"</div>\n" +
"<% } else { %>\n" +
"<% if ( esf.isPartyCompleted() ) { %>\n" +
"<div style=\"background-color: #669966; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">\n" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px;\">You are done. Thank you!</h2>\n" +
"</div>\n" +
"<% } else { %>\n" +
"<div style=\"background-color: #2691ce; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">\n" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px;\">Welcome to our electronic signature service...</h2>\n" +
"</div>\n" +
"<% } %>\n" +
"<div style=\"padding-left: 1em; padding-right: 1em;\">\n" +
"<p>Please note that your continued use of this service constitutes your agreement to use electronic signatures in lieu of a paper document with a traditional hand-written signature. Electronic signatures are legally recognized throughout the United States. Your electronic signature will take place when you type your name and/or initials into the marked areas on the subsequent document(s) and then you click both the Review and Submit buttons on each document to indicate your agreement and/or authorization.</p>\n" +
"<p>You also certify that these documents are intended for you and that you are authorized to sign the documents. If you have received these by mistake, please do not continue and <a href=\"mailto:${property:MyCompany.CustomerSupportEmail}\">email us</a> or call ${property:MyCompany.CustomerSupportPhone} to report our error.</p>\n" +
"<p>If you do not wish to sign these documents electronically, please contact us and do not continue with this process. However, we expect that you will prefer this free, easy-to-use, fast and environmentally sound option.</p>\n" +
"</div>\n" +
"<div style=\"padding: 1em; background-color: #eceeee; border-radius: 5px;\">\n" +
"<p style=\"margin-top: 0;\"><strong><u>Document(s) for your review:</u></strong></p>\n" +
"${transaction:documents.todo}</div>\n" +
"<% } %>\n");
		convert14_1_4_UpdateDocumentPage1(superUser, templateLibrary, Document.STANDARD_PACKAGE_DOCUMENT_ESFNAME, pageHtml);
	}	
	
	public void convert14_2_8() throws Exception
	{
		ReportFieldTemplate rft = ReportFieldTemplate.Manager.getByName(ReportFieldTemplate.BUILT_IN_DOWNLOAD_HTML_SELECTED_SNAPSHOT);
		if ( rft == null )
		{
			rft = new ReportFieldTemplate( new EsfUUID(), ReportFieldTemplate.BUILT_IN_DOWNLOAD_HTML_SELECTED_SNAPSHOT, ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Documents", "Download selected document as HTML.", (short)0);
			if ( rft.save() )
				display("Created report field template: " + ReportFieldTemplate.BUILT_IN_DOWNLOAD_HTML_SELECTED_SNAPSHOT);
		}
		else
			display("Report field template already exists: " + ReportFieldTemplate.BUILT_IN_DOWNLOAD_HTML_SELECTED_SNAPSHOT);
	}	
	
	public void convert14_1_4() throws Exception
	{
		User superUser = getASuperUser();
		Library templateLibrary = Library.Manager.getTemplate();
		
		EsfHtml pageHtml = new EsfHtml(
"<style type=\"text/css\"> body.esf div.pagediv { max-width: 800px; padding-left: 1ex; padding-right: 1ex; margin-left: auto; margin-right: auto; } </style>\n" +
"<p style=\"margin-left: 25px;\">${image:Logo}</p>\n" +
"<div style=\"background-color: #2691ce; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">\n" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px;\">Welcome to our electronic signature service...</h2>\n" +
"</div>\n" +
"<div style=\"padding-left: 1em; padding-right: 1em;\">\n" +
"<p>Please note that your continued use of this service constitutes your agreement to use electronic signatures in lieu of a paper document with a traditional hand-written signature. Electronic signatures are legally recognized throughout the United States. Your electronic signature will take place when you type your name and/or initials into the marked areas on the subsequent document(s) and then you click both the Review and Submit buttons on each document to indicate your agreement and/or authorization.</p>\n" +
"<p>You also certify that these documents are intended for you and that you are authorized to sign the documents. If you have received these by mistake, please do not continue and <a href=\"mailto:${property:MyCompany.CustomerSupportEmail}\">email us</a> or call ${property:MyCompany.CustomerSupportPhone} to report our error.</p>\n" +
"<p>If you do not wish to sign these documents electronically, please contact us and do not continue with this process. However, we expect that you will prefer this free, easy-to-use, fast and environmentally sound option.</p>\n" +
"</div>\n" +
"<div style=\"padding: 1em; background-color: #eceeee; border-radius: 5px;\">\n" +
"<p style=\"margin-top: 0;\"><strong><u>Document(s) for your review:</u></strong></p>\n" +
"${transaction:documents.todo}</div>\n");
		convert14_1_4_UpdateDocumentPage1(superUser, templateLibrary, Document.STANDARD_PACKAGE_DOCUMENT_ESFNAME, pageHtml);
		
		
		String emailHtml = 
"<style type=\"text/css\"> body.esf div.pagediv { max-width: 800px; padding-left: 1ex; padding-right: 1ex; margin-left: auto; margin-right: auto; } </style>" +
"<p style=\"margin-left: 25px;\">${image:LogoForEmail}</p>" +
"<div style=\"background-color: #2691ce; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px; font-family: Georgia, serif;\">An electronic document needs your attention...</h2></div>" +
"<div style=\"padding-left: 1em; padding-right: 1em;\">" +
"<p>An electronic document has been created for you to process on the Open eSignForms system for ${property:MyCompany.InformalName}.</p>" +
"<h2><a href=\"${LINK}\">Click here to view and/or process this document</a></h2>" +
"<p>Our system will guide you through this process so it will only take a few minutes.</p>" +
"<p>Thank you.<br />" +
"${property:MyCompany.FormalName}<br />" +
"${property:MyCompany.MailingAddressStreetAddress}<br />" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}<br />" +
"<a href=\"mailto:${property:MyCompany.UserSupportEmail}\">${property:MyCompany.UserSupportEmail}</a></p>" +
"<div style=\"margin-top: 1ex; padding: 5px; background-color: #eceeee; border-radius: 5px;\">" +
"<p style=\"font-size: 10px;\">If the above link is not active (e.g. nothing happens when you click on it), please copy the link below exactly as shown into your web browser&#39;s address or location field:</p>" +
"<p style=\"font-size: 10px;\">${LINK}</p></div></div>";
		convert14_1_4_UpdateEmailHtml(superUser, templateLibrary, getApplication().getEmailTemplateDefaultPickupNotificationEsfName(), emailHtml);
		
		emailHtml = 
"<style type=\"text/css\"> body.esf div.pagediv { max-width: 800px; padding-left: 1ex; padding-right: 1ex; margin-left: auto; margin-right: auto; } </style>" +
"<p style=\"margin-left: 25px;\">${image:LogoForEmail}</p>" +
"<div style=\"background-color: #2691ce; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px; font-family: Georgia, serif;\">Set your password...</h2></div>" +
"<div style=\"padding-left: 1em; padding-right: 1em;\">" +
"<p>Dear ${NAME},</p>" +
"<p>You are requested to set a password for your ${property:MyCompany.InformalName} Open eSignForms account <b>${EMAIL}</b>.</p>" +
"<h2><a href=\"${LINK}\">Click here to set your password</a></h2>" +
"<p>Thank you.<br />" +
"${property:MyCompany.FormalName}<br />" +
"${property:MyCompany.MailingAddressStreetAddress}<br />" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}<br />" +
"<a href=\"mailto:${property:MyCompany.UserSupportEmail}\">${property:MyCompany.UserSupportEmail}</a></p>" +
"<div style=\"margin-top: 1ex; padding: 5px; background-color: #eceeee; border-radius: 5px;\">" +
"<p style=\"font-size: 10px;\">If the above link is not active (e.g. nothing happens when you click on it), please copy the link below exactly as shown into your web browser&#39;s address or location field:</p>" +
"<p style=\"font-size: 10px;\">${LINK}</p></div></div>";
		convert14_1_4_UpdateEmailHtml(superUser, templateLibrary, getApplication().getEmailTemplateSetPasswordEsfName(), emailHtml);
		
		
		emailHtml = 
"<style type=\"text/css\"> body.esf div.pagediv { max-width: 800px; padding-left: 1ex; padding-right: 1ex; margin-left: auto; margin-right: auto; } </style>" +
"<p style=\"margin-left: 25px;\">${image:LogoForEmail}</p>" +
"<div style=\"background-color: #fda514; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px; font-family: Georgia, serif;\">Forgot your password?</h2></div>" +
"<div style=\"padding-left: 1em; padding-right: 1em;\">" +
"<p>Dear ${NAME},</p>" +
"<p>You have requested to reset your forgotten password for your ${property:MyCompany.InformalName} Open eSignForms account <b>${EMAIL}</b>.</p>" +
"<h2><a href=\"${LINK}\">Click here to reset your forgotten password</a></h2>" +
"<p>Thank you.<br />" +
"${property:MyCompany.FormalName}<br />" +
"${property:MyCompany.MailingAddressStreetAddress}<br />" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}<br />" +
"<a href=\"mailto:${property:MyCompany.UserSupportEmail}\">${property:MyCompany.UserSupportEmail}</a></p>" +
"<div style=\"margin-top: 1ex; padding: 5px; background-color: #eceeee; border-radius: 5px;\">" +
"<p style=\"font-size: 10px;\">If the above link is not active (e.g. nothing happens when you click on it), please copy the link below exactly as shown into your web browser&#39;s address or location field:</p>" +
"<p style=\"font-size: 10px;\">${LINK}</p></div></div>";
		convert14_1_4_UpdateEmailHtml(superUser, templateLibrary, getApplication().getEmailTemplateForgotPasswordEsfName(), emailHtml);
		
		emailHtml = 
"<style type=\"text/css\"> body.esf div.pagediv { max-width: 800px; padding-left: 1ex; padding-right: 1ex; margin-left: auto; margin-right: auto; } </style>" +
"<p style=\"margin-left: 25px;\">${image:LogoForEmail}</p>" +
"<div style=\"background-color: #fda514; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px; font-family: Georgia, serif;\">Your password has been changed...</h2></div>" +
"<div style=\"padding-left: 1em; padding-right: 1em;\">" +
"<p>Dear ${NAME},</p>" +
"<p>The password for your ${property:MyCompany.InformalName} Open eSignForms account <b>${EMAIL}</b> has been changed. If you did not change your password, please contact your system administrator.</p>" +
"<p>Thank you.<br />" +
"${property:MyCompany.FormalName}<br />" +
"${property:MyCompany.MailingAddressStreetAddress}<br />" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}<br />" +
"<a href=\"mailto:${property:MyCompany.UserSupportEmail}\">${property:MyCompany.UserSupportEmail}</a></p>" +
"</div>";
		convert14_1_4_UpdateEmailHtml(superUser, templateLibrary, getApplication().getEmailTemplatePasswordChangedEsfName(), emailHtml);

		emailHtml = 
"<style type=\"text/css\"> body.esf div.pagediv { max-width: 800px; padding-left: 1ex; padding-right: 1ex; margin-left: auto; margin-right: auto; } </style>" +
"<p style=\"margin-left: 25px;\">${image:LogoForEmail}</p>" +
"<div style=\"background-color: #fda514; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px; font-family: Georgia, serif;\">Multiple failed login attempts...</h2></div>" +
"<div style=\"padding-left: 1em; padding-right: 1em;\">" +
"<p>Dear ${NAME},</p>" +
"<p>There have been multiple, failed login attempts on your ${property:MyCompany.InformalName} Open eSignForms account <b>${EMAIL}</b>. " +
"If you were not having trouble, you may want to contact your system administrator.  If you have forgotten your password, you may unlock your own password by clicking below:</p>" +
"<h2><a href=\"${LINK}\">Click here to reset your forgotten password</a></h2>" +
"<p>Thank you.<br />" +
"${property:MyCompany.FormalName}<br />" +
"${property:MyCompany.MailingAddressStreetAddress}<br />" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}<br />" +
"<a href=\"mailto:${property:MyCompany.UserSupportEmail}\">${property:MyCompany.UserSupportEmail}</a></p>" +
"<div style=\"margin-top: 1ex; padding: 5px; background-color: #eceeee; border-radius: 5px;\">" +
"<p style=\"font-size: 10px;\">If the above link is not active (e.g. nothing happens when you click on it), please copy the link below exactly as shown into your web browser&#39;s address or location field:</p>" +
"<p style=\"font-size: 10px;\">${LINK}</p></div></div>";
		convert14_1_4_UpdateEmailHtml(superUser, templateLibrary, getApplication().getEmailTemplatePasswordLockoutEsfName(), emailHtml);

		
		EsfName logoForEmailEsfname = app.getImageDefaultLogoForEmailEsfName();
		Image image = Image.Manager.getByName(templateLibrary.getId(), logoForEmailEsfname);
		if ( image != null )
			display("New template library image for email already exists with the name: " + logoForEmailEsfname);
		else
		{
			Image logoImage = Image.Manager.getByName(templateLibrary.getId(), app.getImageDefaultLogoEsfName());
			if ( logoImage == null )
				display("Unexpectedly missing image: " + app.getImageDefaultLogoEsfName() + "; in library: " + templateLibrary.getPathName());
			else
			{
				ImageVersion logoImageVersion = logoImage.getProductionImageVersion();
				if ( logoImageVersion == null )
				{
					display("Unexpectedly missing production version of image: " + app.getImageDefaultLogoEsfName() + "; in library: " + templateLibrary.getPathName() + "; using the test version.");
					logoImageVersion = logoImage.getTestImageVersion();
				}
				
				Image logoImageForEmail = Image.Manager.createLike(templateLibrary.getId(), logoImage, logoForEmailEsfname);
				ImageVersion logoImageVersionForEmail = ImageVersion.Manager.createLike(logoImageForEmail, logoImageVersion, superUser);
				logoImageForEmail.setDescription("The logo shown/embedded in standard emails.");
				logoImageForEmail.setComments("This is the default logo included in standard emails, embedded in the HTML.");
				logoImageForEmail.promoteTestVersionToProduction();
				logoImageForEmail.save();
				logoImageVersionForEmail.setUseDataUri(Literals.Y);
				logoImageVersionForEmail.save();
				display("Created embedded dataURI logo for email image " + logoImageForEmail.getEsfName() + "; in library: " + templateLibrary.getPathName());
			}
		}
	}
	
	void convert14_1_4_UpdateDocumentPage1(User superUser, Library library, EsfName documentName, EsfHtml html) 
	{
		Document document = Document.Manager.getByName(library, documentName);
		if ( document == null )
			display("Unexpectedly missing document: " + documentName + "; in library: " + library.getPathName());
		else
		{
			DocumentVersion docVerProduction = document.getProductionDocumentVersion();
			if ( docVerProduction == null )
				display("Unexpectedly missing PRODUCTION version document: " + documentName + "; in library: " + library.getPathName() + "; this should have been created at installation.");
			else
			{
				// If there's no distinct test version, we'll create it.
				boolean newTestVersionCreated = false;
				DocumentVersion docVerTest = document.getTestDocumentVersion();

				if ( docVerProduction.getVersion() == docVerTest.getVersion() )
				{
					docVerTest = DocumentVersion.Manager.createLike(document, docVerProduction, superUser);
					newTestVersionCreated = true;
				}
				
				DocumentVersionPage page = docVerTest.getPageNumber(1);
				page.setHtml(html);
				docVerTest.save(superUser);
				page.save(superUser);
				
				if ( newTestVersionCreated )
				{
					document.bumpTestVersion();
					document.promoteTestVersionToProduction();
					document.save(superUser);
					display("Your document: " + documentName + "; in library: " + library.getPathName() + " was updated to the next production version with the page HTML.");
				}
				else
				{
					document.save(superUser);
					display("Your TEST document: " + documentName + "; in library: " + library.getPathName() + "; already existed, so it was not put into production since we updated your test version and it may not be ready yet.");
				}
			}
		}
	}

	void convert14_1_4_UpdateEmailHtml(User superUser, Library library, EsfName emailName, String html) 
	{
		EmailTemplate email = EmailTemplate.Manager.getByName(library.getId(), emailName);
		if ( email == null )
			display("Unexpectedly missing email: " + emailName + "; in library: " + library.getPathName());
		else
		{
			EmailTemplateVersion emailVerProduction = email.getProductionEmailTemplateVersion();
			if ( emailVerProduction == null )
				display("Unexpectedly missing PRODUCTION version email: " + emailName + "; in library: " + library.getPathName() + "; this should have been created at installation.");
			else
			{
				// If there's no distinct test version, we'll create it.
				boolean newTestVersionCreated = false;
				EmailTemplateVersion emailVerTest = email.getTestEmailTemplateVersion();

				if ( emailVerProduction.getVersion() == emailVerTest.getVersion() )
				{
					emailVerTest = EmailTemplateVersion.Manager.createLike(email, emailVerProduction, superUser);
					newTestVersionCreated = true;
				}
				
				emailVerTest.setEmailHtml(html);
				emailVerTest.save(superUser);
				
				if ( newTestVersionCreated )
				{
					email.bumpTestVersion();
					email.promoteTestVersionToProduction();
					email.save(superUser);
					display("Your email: " + emailName + "; in library: " + library.getPathName() + " was updated to the next production version with the updated HTML.");
				}
				else
				{
					email.save(superUser);
					display("Your TEST email: " + emailName + "; in library: " + library.getPathName() + "; already existed, so it was not put into production since we updated your test version and it may not be ready yet.");
				}
			}
		}
	}
	
	
	public void convert13_12_11() throws Exception
	{
		Group systemAdminGroup = Group.Manager.getSystemAdminGroup();

		// Add the new PermissionOption.REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO to all report templates
		for( ReportTemplate rt : ReportTemplate.Manager.getAll() )
		{
   	    	String normalizedId = rt.getId().toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(ReportTemplate.PERM_PATH_PREFIX+normalizedId);

   	    	Permission perm = Permission.Manager.getByPathName(permPathName);
   	    	if ( perm == null )
   	    		display("ERROR: Could not find report template Permission with pathname: " + permPathName);
   	    	else
   	    	{
        		if ( perm.hasOptionByName(PermissionOption.REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO) )
        			display("Report template: " + rt.getPathName() + "; already has permission option: " + PermissionOption.REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO + "; skipping...." );
        		else
        		{
        			boolean createdNewPermOption = perm.createOptionByNameForSuperGroup(PermissionOption.REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO, 
        					PermissionOption.REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO + " - View transactions for any user who is Party To");
        			if ( createdNewPermOption )
        			{
        				perm.addGroupToOptions(systemAdminGroup, PermissionOption.REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO);
        				if ( perm.save() )
        					display("Added permission option: " + PermissionOption.REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO + "; for report template: " + rt.getPathName());
        				else
        					display("ERROR: Failed to save added permission option: " + PermissionOption.REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO + "; for report template: " + rt.getPathName());
        			}
        			else
    					display("ERROR: Failed to create new permission option: " + PermissionOption.REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO + "; for report template: " + rt.getPathName());
        		}
   	    	}
		}
	}

	@SuppressWarnings("deprecation")
	public void convert13_8_10() throws Exception
	{
		User superUser = getASuperUser();
		Library templateLibrary = Library.Manager.getTemplate();

		ReportFieldTemplate rft = ReportFieldTemplate.Manager.getByName(ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_LATEST_SNAPSHOTS);
		if ( rft == null )
		{
			rft = new ReportFieldTemplate( new EsfUUID(), ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_LATEST_SNAPSHOTS, ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Documents", "Download all of the latest documents as a PDF.", (short)0);
			if ( rft.save() )
				display("Created report field template: " + ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_LATEST_SNAPSHOTS);
		}
		else
			display("Report field template already exists: " + ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_LATEST_SNAPSHOTS);
		
		rft = ReportFieldTemplate.Manager.getByName(ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_SELECTED_SNAPSHOTS);
		if ( rft == null )
		{
			rft = new ReportFieldTemplate( new EsfUUID(), ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_SELECTED_SNAPSHOTS, ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Documents", "Download selected documents as a PDF.", (short)0);
			if ( rft.save() )
				display("Created report field template: " + ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_SELECTED_SNAPSHOTS);
		}
		else
			display("Report field template already exists: " + ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_SELECTED_SNAPSHOTS);

		// Fix up various drop down lists to have (Default) labels with no value.
		String[] moneyOptions = { "123456789.12","�123,456,789","�123,456,789.12","�123,456,789","�123,456,789.12"};
		String[] moneyValues  = { "##############0.00","'�'###,###,###,###,##0","'�'###,###,###,###,##0.00","'�'###,###,###,###,##0","'�'###,###,###,###,##0.00"};
		convert12_7_7_AddDropDownOptions(superUser, templateLibrary.getId(), getApplication().getDropDownMoneyFormatEsfName(), moneyOptions, moneyValues);

		String[] dateOptions = { "15th day of February, 2011"};
		String[] dateValues  = { "d'ORD day of' MMMM',' yyyy"};
		convert12_7_7_AddDropDownOptions(superUser, templateLibrary.getId(), getApplication().getDropDownDateFormatEsfName(), dateOptions, dateValues);
	}

	public void convert13_5_5() throws Exception
	{
		User superUser = getASuperUser();
		Library templateLibrary = Library.Manager.getTemplate();
		
		createStringTransformsDropDown(templateLibrary,superUser);
		
		EsfName[] buttonNames = { ButtonMessageVersion.PACKAGE_BUTTON_VIEW_COMPLETED_DOCUMENT_ESFNAME, ButtonMessageVersion.PACKAGE_BUTTON_VIEW_COMPLETED_DOCUMENT_TITLE_ESFNAME };
		EsfString[] buttonValues = { new EsfString("View"), new EsfString("Click to view this document you previously completed.") };
		
		for( ButtonMessage bm : ButtonMessage.Manager.getAll() )
			convert13_5_5_AddButtonMessages(superUser, bm, buttonNames,  buttonValues);
	}

	void convert13_5_5_AddButtonMessages(User superUser, ButtonMessage bm, EsfName[] buttonNames, EsfString[] buttonValues) 
	{
		// If there's no distinct test version, we'll create it.
		boolean newTestVersionCreated = false;
		boolean propsAdded = false;
		ButtonMessageVersion bmvProduction = bm.getProductionButtonMessageVersion();
		ButtonMessageVersion bmvTest = bm.getTestButtonMessageVersion();

		if ( bmvProduction != null && bmvProduction.getVersion() == bmvTest.getVersion() )
		{
			bmvTest = ButtonMessageVersion.Manager.createLike(bm, bmvProduction, superUser);
			newTestVersionCreated = true;
		}
		
		Record properties = bmvTest.getRecord(); // we update the bmvTest record directly
		
		for( int i=0; i < buttonNames.length; ++i )
		{
			EsfValue value = properties.getValueByName(buttonNames[i]);
			if ( value == null )
			{
	        	NameValue nv = new NameValue( buttonNames[i], buttonValues[i] );
	            properties.addUpdate( nv );
	            propsAdded = true;
				display("TEST button message: " + bm.getEsfName() + "; in library: " + bm.getLibrary().getPathName() + "; added button named: " + buttonNames[i]);
			}
			else
				display("TEST button message: " + bm.getEsfName() + "; in library: " + bm.getLibrary().getPathName() + "; already has a button named: " + buttonNames[i] + "; not changing it.");
		}
			
		if ( ! propsAdded )
		{
			display("Your button message: " + bm.getEsfName() + "; in library: " + bm.getLibrary().getPathName() + " was not updated since no new buttons were added.");
			if ( newTestVersionCreated ) // delete if we created it but didn't add any new properties/buttons
				bmvTest.delete();
		}
		else
		{
			bmvTest.save(superUser);
			if ( newTestVersionCreated )
			{
				bm.bumpTestVersion();
				bm.promoteTestVersionToProduction();
				bm.save(superUser);
				display("Your button message: " + bm.getEsfName() + "; in library: " + bm.getLibrary().getPathName() + " was updated to the next production version with the new button(s).");
			}
			else
				display("Your TEST button message: " + bm.getEsfName() + "; in library: " + bm.getLibrary().getPathName() + "; already existed, so it was not put into production since we updated your test version and it may not be ready yet.");
		}
	}
	
	public void convert13_4_13() throws Exception
	{
		// Add in the new group permission that allows some to manage the To Do listing for other users.
		int count = Group.Manager._dbsetup_addManageToDoPermissionToMemberUsersFor13_4_13();
		display("Added the ManagedToDo permission option to " + count + " group(s).");
	}


	public void convert13_3_12() throws Exception
	{
		User superUser = getASuperUser();
		
		EsfName[] newPropNames = { new EsfName("LoginPageAllowEmbedded"), new EsfName("LoginPageTitle"), new EsfName("LoginWelcomeHeader") };
		EsfString[] newPropValues = { new EsfString("false"), new EsfString("Open eSignForms for ${property:MyCompany.InformalName} - Login"), new EsfString("Login to Open eSignForms for ${property:MyCompany.InformalName}") };
		String[] newPropComments = { "If true, the login page won't force itself to be the top page, thus allowing it to be embedded, but at the cost of security since the login page won't show the URL and SSL cert status and can introduce spoofing hacks. Recommend this be false.", "The page title of the login page.", "The login page welcome header." };
		
		convert13_3_12_AddPropertySetProperties(superUser, Library.Manager.getTemplate(),app.getPropertySetESFEsfName(),newPropNames,newPropValues,newPropComments);
		
		createInitialImages(superUser); // add new bangOrange.png and bangGreen.png for error messages as well as the ability to brand the logo for the app
		
		// Let's disable the old ESF_Buttons property set if it's still defined. In a later release, we can finally remove this obsolete property set.
		@SuppressWarnings("deprecation")
		EsfName buttonsPropertySetName = app.getPropertySetEsfButtonsEsfName();
		PropertySet ps = PropertySet.Manager.getByName(Library.Manager.getTemplate().getId(), buttonsPropertySetName);
		if ( ps != null )
		{
			if ( ps.isEnabled() )
			{
				ps.makeDisabled();
				ps.save(superUser);
				display("Disabled obsolete property set: " + buttonsPropertySetName + "; in library: " + Library.Manager.getTemplate().getPathName());
			}
			else
				display("Obsolete property set: " + buttonsPropertySetName + "; in library: " + Library.Manager.getTemplate().getPathName() + " is already disabled as expected.");
		}
		else
			display("Obsolete property set: " + buttonsPropertySetName + "; in library: " + Library.Manager.getTemplate().getPathName() + "; was not found, which is okay since no longer needed.");			
	}

	void convert13_3_12_AddPropertySetProperties(User superUser, Library library, EsfName propertySetName, EsfName[] propNames, EsfString[] propValues, String[] propComments) 
	{
		PropertySet ps = PropertySet.Manager.getByName(library.getId(), propertySetName);
		if ( ps == null )
			display("Unexpectedly missing property set: " + propertySetName + "; in library: " + library.getPathName());
		else
		{
			PropertySetVersion psvProduction = ps.getProductionPropertySetVersion();
			if ( psvProduction == null )
				display("Unexpectedly missing PRODUCTION version property set: " + propertySetName + "; in library: " + library.getPathName() + "; this should have been created at installation.");
			else
			{
				// If there's no distinct test version, we'll create it.
				boolean newTestVersionCreated = false;
				boolean propsAdded = false;
				PropertySetVersion psvTest = ps.getTestPropertySetVersion();

				if ( psvProduction.getVersion() == psvTest.getVersion() && ! newTestVersionCreated )
				{
					psvTest = PropertySetVersion.Manager.createLike(ps, psvProduction, superUser);
					newTestVersionCreated = true;
				}
				
				Record properties = psvTest.getProperties();
				
				for( int i=0; i < propNames.length; ++i )
				{
					EsfValue value = properties.getValueByName(propNames[i]);
					if ( value == null )
					{
			        	NameValue nv = new NameValue( propNames[i], propValues[i] );
			            nv.setComment(propComments[i]);
			            properties.addUpdate( nv );
			            propsAdded = true;
						display("TEST property set: " + propertySetName + "; in library: " + library.getPathName() + "; added property named: " + propNames[i]);
					}
					else
						display("TEST property set: " + propertySetName + "; in library: " + library.getPathName() + "; already has a property named: " + propNames[i] + "; not changing it.");
				}
					
				if ( ! propsAdded )
				{
					display("Your property set: " + propertySetName + "; in library: " + library.getPathName() + " was not updated since no new properties were added.");
					if ( newTestVersionCreated ) // delete if we created it but didn't add any new properties
						psvTest.delete();
				}
				else
				{
					psvTest.setProperties(properties);
					psvTest.save(superUser);
					if ( newTestVersionCreated )
					{
						ps.bumpTestVersion();
						ps.promoteTestVersionToProduction();
						ps.save(superUser);
						display("Your property set: " + propertySetName + "; in library: " + library.getPathName() + " was updated to the next production version with the new properties.");
					}
					else
						display("Your TEST property set: " + propertySetName + "; in library: " + library.getPathName() + "; already existed, so it was not put into production since we updated your test version and it may not be ready yet.");
				}
			}
		}
	}
	

	public void convert12_12_15() throws Exception
	{
		User superUser = getASuperUser();
		
		// Plan on removing the ESF_Buttons property set from the template library in the next release since it may have been modified since the previous
		// release and the system admin will need to reference it to make similar changes to the new button message set.

		createInitialButtonMessageSet(superUser);
	}


	public void createX509Certificate() throws Exception
	{
		List<SignatureKey> signatureKeys = SignatureKey.Manager.getAll();
		for( SignatureKey signatureKey : signatureKeys )
		{
			if ( signatureKey.getX509Certificate() == null )
			{
   				try
   				{
   			        ConnectionPool    pool = app.getConnectionPool();
   			        Connection        con  = pool.getConnection();
   			        EsfPreparedStatement stmt = null;
   			        
   			        X509Certificate cert = app.getPublicKeyGenerator().createSelfSignedCertificate(
	   						"CN=Open_eSignForms_" + app.getExternalContextPath() + 
	   						", O=DeploymentID/" + app.getDeployId() + 
	   						", OU=SignatureKeyID/" + signatureKey.getId() +
	   						", C=" + app.getDefaultCountryCode(), 
	   						signatureKey.getKeyPair(), signatureKey.getCreatedTimestamp(), signatureKey.getDeactivationTimestamp());
	   				
	   				try
	   				{
		   				String base64Cert = app.getPublicKeyGenerator().toBase64EncodeX509Certificate(cert);
		   				if ( base64Cert == null )
		   					display("Failed to get the encoded X509 certificate for signature key id: " + signatureKey.getId());
		   				else
		   				{
			                stmt = new EsfPreparedStatement( con, "UPDATE esf_signature_key SET x509_certificate_data=? WHERE id=?" );
			                stmt.set(base64Cert);
			                stmt.set(signatureKey.getId());
			                int num = stmt.executeUpdate();
			                if ( num != 1 )
			                {
			                	display("Failed to UPDATE the X509 certificate for signature key id: " + signatureKey.getId());
			                	con.rollback();
			                }
			                else
			                {
			                	display("Saved the X509 certificate for signature key id: " + signatureKey.getId());
			                	con.commit();
			                }
		   				}
	   				}
	   				catch(SQLException e)
	   				{
	   					display("Failed to save the X509 certificate for signature key id: " + signatureKey.getId());
	   					pool.rollbackIgnoreException(con, e);
	   					e.printStackTrace();
	   				}
	   		        finally
	   		        {
	   		        	DatabaseObject.cleanupStatement(stmt);
	   		            app.cleanupPool(pool,con,null);
	   		        }
   				}
   				catch( Exception e )
   				{
   					display("Failed to create the X509 certificate for signature key id: " + signatureKey.getId());
   					e.printStackTrace();
   				}
			}
			else
			{
				display("An X509 certificate is already present on SignatureKey id: " + signatureKey.getId());
			}
		}
	}


	public void convert12_9_29() throws Exception
	{
		//User superUser = getASuperUser();
		//Library templateLibrary = Library.Manager.getTemplate();
		
		createX509Certificate();
		
		// This was obsoleted and thus is no longer used.
		//createEsfButtonsPropertySet(templateLibrary,superUser,true);
	}


	public void convert12_8_25() throws Exception
	{
		Application app = Application.getInstance();
		
		EsfString currDefaultCountryCode = app.getDeploymentProperties().getStringByName(app.getDefaultCountryCodeEsfName());
		if ( currDefaultCountryCode == null )
		{
			NameValue defaultCountryCode = new NameValue( app.getDefaultCountryCodeEsfName(), new EsfString("US") );
			defaultCountryCode.setComment("This is the OpenESF default country code.");
			app.getDeploymentProperties().addUpdate(defaultCountryCode);
			app.saveDeploymentProperties();
			display("Added deployment property: " + app.getDefaultCountryCodeEsfName());
		}
		else
			display("Skipped adding deployment property: " + app.getDefaultCountryCodeEsfName() + "; already present with value: " + currDefaultCountryCode);
		
		if ( Permission.Manager.getByPathName(UI.UI_PERM_USER_CHANGE_PASSWORD_VIEW) == null )
		{
	    	Group systemAdminGroup = Group.Manager.getSystemAdminGroup();
	    	Group allUsersGroup = Group.Manager.getAllUsersGroup();
			Permission p = Permission.Manager._dbsetup_createForAllUsersGroup(UI.UI_PERM_USER_CHANGE_PASSWORD_VIEW, "Access to Change My Password screen",allUsersGroup);
			p.addGroupToAllOptions(systemAdminGroup);
			p.save();
			display("Created the user change password permission: " + UI.UI_PERM_USER_CHANGE_PASSWORD_VIEW);
		}
		else
			display("Skipped adding the already existing user change password permission: " + UI.UI_PERM_USER_CHANGE_PASSWORD_VIEW);
			
	}
	

	public void convert12_8_4() throws Exception
	{
		User superUser = getASuperUser();
		Library templateLibrary = Library.Manager.getTemplate();
	
		// Fix up various drop down lists to have (Default) labels with no value.
		String[] decimalOptions = { "123456789[.12]%"};
		String[] decimalValues  = { "##############0.##%"};
		convert12_7_7_AddDropDownOptions(superUser, templateLibrary.getId(), getApplication().getDropDownDecimalFormatEsfName(), decimalOptions, decimalValues);

    	createCountryDropDown(templateLibrary,superUser);
    	
    	createPayPalCardTypesDropDown(templateLibrary,superUser);
    	
    	createPayPalNvpApiPropertySet(templateLibrary,superUser); // Existing customers may need to export/import this to their company library
	}

	public void convert12_7_7() throws Exception
	{
		User superUser = getASuperUser();
		Library templateLibrary = Library.Manager.getTemplate();
	
		// Fix up various drop down lists to have (Default) labels with no value.
		String[] dateOptions = { "02/12 (mm/yy)", "02/2012 (mm/yyyy)", "20110215 (yyyymmdd)", "110215 (yymmdd)", "1102 (yymm)"};
		String[] dateValues  = { "MM/yy",         "MM/yyyy",           "yyyyMMdd",             "yyMMdd",          "yyMM"};
		convert12_7_7_AddDropDownOptions(superUser, templateLibrary.getId(), getApplication().getDropDownDateFormatEsfName(), dateOptions, dateValues);

		String[] dateTimeOptions = { "20110215134515", "1102151345"};
		String[] dateTimeValues  = { "yyyyMMddHHmmss", "yyMMddHHmm"};
		convert12_7_7_AddDropDownOptions(superUser, templateLibrary.getId(), getApplication().getDropDownDateTimeFormatEsfName(), dateTimeOptions, dateTimeValues);
	}
	
	void convert12_7_7_AddDropDownOptions(User superUser, EsfUUID libraryId, EsfName dropDownName, String[] options, String[] values) 
	{
		DropDown dd = DropDown.Manager.getByName(libraryId, dropDownName);
		if ( dd == null )
			display("Unexpectedly missing template library dropdown: " + dropDownName);
		else
		{
			DropDownVersion ddvProduction = dd.getProductionDropDownVersion();
			if ( ddvProduction == null )
				display("Unexpectedly missing template library dropdown PRODUCTION version: " + dd.getEsfName() + "; this should have been created at installation.");
			else
			{
				// If there's no distinct test version, we'll create it.
				boolean newTestVersionCreated = false;
				boolean optionsAdded = false;
				DropDownVersion ddvTest = dd.getTestDropDownVersion();

				if ( ddvProduction.getVersion() == ddvTest.getVersion() && ! newTestVersionCreated )
				{
					ddvTest = DropDownVersion.Manager.createLike(dd, ddvProduction, superUser);
					newTestVersionCreated = true;
				}
				
				for( int i=0; i < options.length; ++i )
				{
					String optionName = ddvTest.getOptionByValue(values[i]);
					if ( EsfString.isBlank(optionName) )
					{
						List<DropDownVersionOption> optionList = ddvTest.getOptions();
						
						DropDownVersionOption newOption = DropDownVersionOption.Manager.createNew(ddvTest, (short)1);
						newOption.setOption(options[i]);
						newOption.setValue(values[i]);
						optionList.add(newOption);
						ddvTest.setOptions(optionList);
						optionsAdded = true;
					}
					else
						display("Template library dropdown TEST version: " + dd.getEsfName() + "; already has '" + values[i] + "' value for option: " + optionName + "; not creating '" + options[i] + "' entry.");
				}
					
				if ( ! optionsAdded )
				{
					display("Your template library dropdown: " + dd.getEsfName() + " was not updated since no new options were added.");
					if ( newTestVersionCreated ) // delete if we created it but didn't add any new options
						ddvTest.delete();
				}
				else
				{
					ddvTest.save(superUser);
					if ( newTestVersionCreated )
					{
						dd.bumpTestVersion();
						dd.promoteTestVersionToProduction();
						dd.save(superUser);
						display("Your template library dropdown: " + dd.getEsfName() + " was updated to the next production version with the new options.");
					}
					else
						display("Your template library dropdown TEST version: " + dd.getEsfName() + "; already existed, so it was not put into production since we updated your test version and it may not be ready yet.");
				}
			}
		}
	}
	
	public void convert12_6_2() throws Exception
	{
		User superUser = getASuperUser();
		Library templateLibrary = Library.Manager.getTemplate();
	
		// Fix up various drop down lists to have (Default) labels with no value.
		convert12_6_2_AddNowDropDownOption(superUser, templateLibrary.getId(), getApplication().getDropDownTimeIntervalUnitsEsfName());
	}
	
	void convert12_6_2_AddNowDropDownOption(User superUser, EsfUUID libraryId, EsfName dropDownName) 
	{
		DropDown dd = DropDown.Manager.getByName(libraryId, dropDownName);
		if ( dd == null )
			display("Unexpectedly missing template library dropdown: " + dropDownName);
		else
		{
			DropDownVersion ddvProduction = dd.getProductionDropDownVersion();
			if ( ddvProduction == null )
				display("Unexpectedly missing template library dropdown production version: " + dd.getEsfName() + "; this should have been created at installation.");
			else
			{
				String optionName = ddvProduction.getOptionByValue(Literals.TIME_INTERVAL_UNIT_NOW);
				if ( "Now".equals(optionName) )
					display("Your template library dropdown production version: " + dd.getEsfName() + "; already has Now option with 'now' value so no change needed.");
				else if ( EsfString.isNonBlank(optionName) )
					display("Unexpectedly template library dropdown production version: " + dd.getEsfName() + "; already has 'now' value for option: " + optionName + "; not creating 'Now' entry.");
				else
				{
					DropDownVersion ddvTest = dd.getTestDropDownVersion();

					// If there's no distinct test version, we'll create it.
					boolean newTestVersionCreated = false;
					if ( ddvProduction.getVersion() == ddvTest.getVersion() )
					{
						ddvTest = DropDownVersion.Manager.createLike(dd, ddvProduction, superUser);
						newTestVersionCreated = true;
					}
					
					optionName = ddvTest.getOptionByValue(Literals.TIME_INTERVAL_UNIT_NOW);
					if ( EsfString.isBlank(optionName) )
					{
						LinkedList<DropDownVersionOption> currOptions = new LinkedList<DropDownVersionOption>( ddvTest.getOptions() );
						
						String foreverOptionName = ddvTest.getOptionByValue(Literals.TIME_INTERVAL_UNIT_FOREVER);
						if ( EsfString.isBlank(foreverOptionName) ) // odd, no forever option, so we'll just add to top
						{
							DropDownVersionOption nowOption = DropDownVersionOption.Manager.createNew(ddvTest, (short)1);
							nowOption.setOption("Now");
							nowOption.setValue(Literals.TIME_INTERVAL_UNIT_NOW);
							currOptions.addFirst(nowOption);
						}
						else
						{
							ListIterator<DropDownVersionOption> iter = currOptions.listIterator();
							while( iter.hasNext() )
							{
								DropDownVersionOption option = iter.next();
								if ( option.getValue().equals(Literals.TIME_INTERVAL_UNIT_FOREVER) )
								{
									DropDownVersionOption nowOption = DropDownVersionOption.Manager.createNew(ddvTest, (short)1);
									nowOption.setOption("Now");
									nowOption.setValue(Literals.TIME_INTERVAL_UNIT_NOW);
									iter.add(nowOption);
									break;
								}
							}
						}
						
						ddvTest.setOptions(currOptions);
						ddvTest.save(superUser);
						
						if ( newTestVersionCreated )
						{
							dd.bumpTestVersion();
							dd.promoteTestVersionToProduction();
							dd.save(superUser);
							display("Your template library dropdown: " + dd.getEsfName() + " was updated to the next production version with the Now option.");
						}
						else
							display("Your template library dropdown test version: " + dd.getEsfName() + "; already existed. New Now option with 'now' value added, but not put into production since not sure about your other changes in your test version.");
					}
					else if ( "Now".equals(optionName) )
						display("Your template library dropdown test version: " + dd.getEsfName() + "; already has Now option with 'now' value so no change needed.");
					else
						display("Unexpectedly template library dropdown test version: " + dd.getEsfName() + "; already has 'now' value for option: " + optionName + "; not creating 'Now' entry.");
				}
			}
		}
	}

	public void convert12_3_2() throws Exception
	{
		User superUser = getASuperUser();
		Library templateLibrary = Library.Manager.getTemplate();
	
		createIntegerExtraOptionsDropDown(templateLibrary, superUser);
		
		// Fix up various drop down lists to have (Default) labels with no value.
		convert12_3_2_AddDefaultDropDownOption(superUser, templateLibrary.getId(), getApplication().getDropDownBackgroundColorEsfName());
		convert12_3_2_AddDefaultDropDownOption(superUser, templateLibrary.getId(), getApplication().getDropDownFontColorEsfName());
		convert12_3_2_AddDefaultDropDownOption(superUser, templateLibrary.getId(), getApplication().getDropDownFontEsfName());
		convert12_3_2_AddDefaultDropDownOption(superUser, templateLibrary.getId(), getApplication().getDropDownFontStyleEsfName());
		convert12_3_2_AddDefaultDropDownOption(superUser, templateLibrary.getId(), getApplication().getDropDownFontSizeEsfName());
	}
	
	void convert12_3_2_AddDefaultDropDownOption(User superUser, EsfUUID libraryId, EsfName dropDownName) 
	{
		DropDown dd = DropDown.Manager.getByName(libraryId, dropDownName);
		if ( dd == null )
			display("Unexpectedly missing template library dropdown: " + dropDownName);
		else
		{
			DropDownVersion ddvProduction = dd.getProductionDropDownVersion();
			if ( ddvProduction == null )
				display("Unexpectedly missing template library dropdown production version: " + dd.getEsfName() + "; this should have been created at installation.");
			else
			{
				String optionName = ddvProduction.getOptionByValue("");
				if ( "(Default)".equals(optionName) )
					display("Your template library dropdown production version: " + dd.getEsfName() + "; already has (Default) option with blank value so no change needed.");
				else if ( EsfString.isNonBlank(optionName) )
					display("Unexpectedly template library dropdown production version: " + dd.getEsfName() + "; already has blank value for option: " + optionName + "; not creating (Default) now.");
				else
				{
					DropDownVersion ddvTest = dd.getTestDropDownVersion();

					// If there's no distinct test version, we'll create it.
					boolean newTestVersionCreated = false;
					if ( ddvProduction.getVersion() == ddvTest.getVersion() )
					{
						ddvTest = DropDownVersion.Manager.createLike(dd, ddvProduction, superUser);
						newTestVersionCreated = true;
					}
					
					optionName = ddvTest.getOptionByValue("");
					if ( EsfString.isBlank(optionName) )
					{
						LinkedList<DropDownVersionOption> currOptions = new LinkedList<DropDownVersionOption>( ddvTest.getOptions() );
						DropDownVersionOption defaultOption = DropDownVersionOption.Manager.createNew(ddvTest, (short)1);
						defaultOption.setOption("(Default)");
						defaultOption.setValue("");
						currOptions.addFirst(defaultOption);
						ddvTest.setOptions(currOptions);
						ddvTest.save(superUser);
						if ( newTestVersionCreated )
						{
							dd.bumpTestVersion();
							dd.promoteTestVersionToProduction();
							dd.save(superUser);
							display("Your template library dropdown: " + dd.getEsfName() + " was updated to the next production version with the (Default) option.");
						}
						else
							display("Your template library dropdown test version: " + dd.getEsfName() + "; already existed. New (Default) option with blank value added, but not put into production since not sure about your other changes in your test version.");
					}
					else if ( "(Default)".equals(optionName) )
						display("Your template library dropdown test version: " + dd.getEsfName() + "; already has (Default) option with blank value so no change needed.");
					else
						display("Unexpectedly template library dropdown test version: " + dd.getEsfName() + "; already has blank value for option: " + optionName + "; not creating (Default) now.");
				}
			}
		}
	}

	public void convert11_11_11() throws Exception
	{
		User superUser = getASuperUser();
    	Library templateLibrary = Library.Manager.getTemplate();

    	createCanadaPostalProvincesDropDown(templateLibrary,superUser);
    	
    	createInitialImages(superUser); // add in new television.png image
	}
	
	public void convert11_10_14() throws Exception
	{
		User superUser = getASuperUser();
		Group systemAdminGroup = Group.Manager.getSystemAdminGroup();
		Group allUsersPseudoGroup = Group.Manager.getAllUsersGroup();
    	Library templateLibrary = Library.Manager.getTemplate();
		
		createDateTimeFormatDropDown(templateLibrary,superUser);
		
		createEsfTemplateReportTemplate(superUser,null,null);
		
		// Only system admin should be able to edit
		Permission p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_REPORT_TIPS_EDIT_VIEW, "Access the Reports link tips edit screen");
		p.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.save();
		
		// Anybody can view it
		p = Permission.Manager.create(UI.UI_PERM_REPORT_TIPS_VIEW, "View the Reports Link Tips page",
				PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.addGroupToOptions(allUsersPseudoGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.save();
		
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_REPORT_TEMPLATE_VIEW, "Access the Report Template screen");
		p.addGroupToAllOptions(systemAdminGroup);
		p.save();
		display("You may want to add your company group to be Report Template UI's LIST permission when you next login so that reports created like it will be available to members of your company.");
		
		createBuiltInReportFieldTemplates();
	}

	public void convert1_5() throws Exception
	{
		Application app = Application.getInstance();
		
    	app.setRetainSystemConfigChangeDays(new EsfInteger(365) ); 
    	app.setRetainSystemUserActivityDays(new EsfInteger(90) ); 
    	app.setRetainSystemStartStopDays(new EsfInteger(365) ); 
    	app.setRetainStatsDays(new EsfInteger(365) ); 
    	app.setRetainUserConfigChangeDays(new EsfInteger(365) ); 
    	app.setRetainUserSecurityDays(new EsfInteger(365) ); 
    	app.setRetainUserLoginLogoffDays(new EsfInteger(90) ); 
		
		app.saveDeploymentProperties();
	}

	public void convert1_4() throws Exception
	{
		Application app = Application.getInstance();
		
		app.setDefaultSessionTimeoutMinutes( new EsfInteger(30) );
		app.setLoggedInSessionTimeoutMinutes( new EsfInteger(30) );
		
		app.setLicenseType( Application.LICENSE_TYPE_DEFAULT );
		app.setLicenseSize( new EsfInteger(0) );
		
		app.saveDeploymentProperties();
	}

	// Keep for historical reasons
	public void convert1_0_1() throws Exception
	{
		Library templateLibrary = Library.Manager.getTemplate();
		User superUser = getASuperUser();
		createDecimalFormatDropDown(templateLibrary, superUser);
		createIntegerFormatDropDown(templateLibrary, superUser);
		createMoneyFormatDropDown(templateLibrary, superUser);
	}

	// Keep for historical reasons
	public void convert098()
	{
		Group systemAdminGroup = Group.Manager.getSystemAdminGroup();
		
		Permission p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_USER_SESSION_VIEW, "Access to Who's on Now? screen");
		p.addGroupToAllOptions(systemAdminGroup);
		p.save();
	}

	
	// ***************  General new installation routines *********************************
	
	public EsfUUID insertDeployment() throws Exception
	{
	    ConnectionPool    pool = app.getConnectionPool();
	    if ( pool == null )
	    {
        	displayError("ERROR: insertDeployment - Could not find the database connection pool. Is your connectionpools.properties file in WEB-INF/classes configured?");
    		throw new Exception("No connection pool available");
	    }
	    Connection        con  = pool.getConnection();
	    if ( con == null )
	    {
        	displayError("ERROR: insertDeployment - Could not get a database connection from the pool. Is your connectionpools.properties file in WEB-INF/classes configured correctly?");
    		throw new Exception("No connection in the pool is available");
	    }
	    PreparedStatement stmt = null;
	    
	    try
	    {
	    	stmt = con.prepareStatement("SELECT id FROM esf_deployment");
	    	ResultSet rs = stmt.executeQuery();
	    	if ( rs.next() )
	    	{
	    		displayError("ERROR: insertDeployment - There is already a deployment row with id: " + rs.getObject(1));
	    		pool.rollbackIgnoreException(con);
	    		throw new Exception("deployment already configured");
	    	}
	    	stmt.close();
	    	
	    	// get a new UUID that we'll use for this deployment
	    	EsfUUID deployId = new EsfUUID();
	    	
	        stmt = con.prepareStatement("INSERT INTO esf_deployment (id,created_timestamp) VALUES (?,?)");
	        stmt.setObject(1,deployId.getUUID(),Types.OTHER);
            stmt.setTimestamp(2,DateUtil.getCurrentSqlTimestamp());
	        int num = stmt.executeUpdate();
	        if ( num != 1 )
	        {
	        	displayError("ERROR: insertDeployment - Could not insert new deployment id: " + deployId);
                pool.rollbackIgnoreException(con);
	    		throw new Exception("deployment could not be created");
	        }
	        
	        display("insertDeployment - Created deployment with id: " + deployId);
	        
            con.commit();

            return deployId;
	    }
	    catch(SQLException e) 
	    {
            display("insertDeployment - SQL Error: " + e.getMessage());
			app.sqlerr(e,"ERROR: insertDeployment()");
            pool.rollbackIgnoreException(con,e);
            throw new Exception("deployment could not be created because of SQL failure");
	    }
	    finally
	    {
	        app.cleanupPool(pool,con,stmt);
	    }
	}

	

    private String makeFullBootPassword(EsfUUID deployId,String pass1,String pass2)
    {
    	return com.esignforms.open.admin.BootService.MakeFullBootPassword(deployId,pass1,pass2);
    }
    
    /**
     * Inserts a PBE-encrypted boot key into the session_key table with the deployId as its id.
     */
    public void insertBootKey(EsfUUID deployId,String pass1,String pass2) throws Exception
    {
        app.setBootPassword1(pass1);
        app.setBootPassword2(pass2);
        
        SecretKeyGenerator keyGen= app.getSecretKeyGenerator();
        
        SecretKey newBootKey     = keyGen.generateKey();
        byte[] newBootKeyEncoded = newBootKey.getEncoded();
        String fullPassword      = makeFullBootPassword(deployId,pass1,pass2);
        byte[] encryptedBootKey  = pbe.encrypt(fullPassword,newBootKeyEncoded);
        if ( encryptedBootKey == null ) 
        {
        	String error = "Failed to encrypt the boot key. Are the 'Java Cryptography Extension (JCE) Unlimited Strength Jurisdiction Policy Files' installed in the jre/lib/security for your version of Java?";
        	displayError(error);
        	throw new Exception(error);
        }
        
        insertBootKey(deployId, encryptedBootKey, pbe.getNumIterations(), pbe.getSaltSize());
        
        if ( ! app.bootSystem() )
        	displayError("ERROR: insertBootKey - Could not boot the system after inserting our new boot key.");
    }
    
	private void insertBootKey( EsfUUID id, byte[] encryptedBootKey, int numIterations, int saltSize ) 
	{
	    ConnectionPool    pool = app.getConnectionPool();
	    Connection        con  = pool.getConnection();
	    PreparedStatement stmt = null;
	    
	    try
	    {
	        stmt = con.prepareStatement("INSERT INTO esf_session_key (id,created_timestamp,key_data) VALUES (?,?,?)");

	        // Insert the new symmetric boot key, PBE encrypted, base64 encoded
	        stmt.setObject(1,id.getUUID(),Types.OTHER);
            stmt.setTimestamp(2,DateUtil.getCurrentSqlTimestamp());
            stmt.setString(3,BootService.getIterationAndSaltPrefix(numIterations,saltSize) + Base64.encodeToString(encryptedBootKey));
	        int num = stmt.executeUpdate();
	        if ( num != 1 )
	        {
	        	displayError("ERROR: insertBootKey - Could not insert new boot key");
                pool.rollbackIgnoreException(con);
	        	return;
	        }
	        display("insertBootKey - Added new boot key");
	        
            con.commit();
	    }
	    catch(SQLException e) 
	    {
            display("insertBootKey - SQL Error: " + e.getMessage());
			app.sqlerr(e,"ERROR: insertBootKey()");
            pool.rollbackIgnoreException(con,e);
	        return;
	    }
	    finally
	    {
	        app.cleanupPool(pool,con,stmt);
	    }
	}

    /**
     * Inserts the initial signature keypair to use.
     */
    public void insertSignatureKeyPair(EsfUUID deployId) throws Exception
    {
    	display("Generating digital signature keypair (can take a few minutes)...");
    	SignatureKey initialSignatureKey = SignatureKey.Manager.createNew();
    	
        if ( ! initialSignatureKey.save() )
        	displayError("ERROR: insertSignatureKeyPair() - Could not create an initial digital signature keypair to use.");
    }
    

	public void createInitialPermissions(Group systemAdminGroup)
	{
		Group allUsersPseudoGroup = Group.Manager.getAllUsersGroup();
		
		// Add in UI permissions
		Permission p = Permission.Manager.create(UI.UI_PERM_DEPLOYMENT_PROPERTIES_VIEW, "Access the Deployment Properties screen",
				PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.save();
		
		p = Permission.Manager.create(UI.UI_PERM_DEPLOYMENT_STATS_VIEW, "Access the Deployment Stats screen",
				PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.save();
		
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_GROUP_VIEW, "Access the Groups screen");
		p.addGroupToAllOptions(systemAdminGroup);
		p.save();
		
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_USER_SESSION_VIEW, "Access to Who's on Now? screen");
		p.addGroupToAllOptions(systemAdminGroup);
		p.save();

		// Only system admin should be able to edit
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_LIBRARY_DOCUMENT_PROGRAMMING_TIPS_EDIT_VIEW, "Access the Library Document Programming Tips edit screen");
		p.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.save();
		
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_LIBRARY_PROGRAMMING_TIPS_EDIT_VIEW, "Access the Library Programming Tips edit screen");
		p.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.save();
		
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_LIBRARY_VIEW, "Access the Libraries screen");
		p.addGroupToAllOptions(systemAdminGroup);
		p.save();
		
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_PACKAGE_VIEW, "Access the Package screen");
		p.addGroupToAllOptions(systemAdminGroup);
		p.save();
		
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_REPORT_TEMPLATE_VIEW, "Access the Report Template screen");
		p.addGroupToAllOptions(systemAdminGroup);
		p.save();
		
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_SYSTEM_LOG_VIEW, "Access the system activity log screen");
		p.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.save();
		
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_TODO_LISTING_VIEW, "Access the default To Do listing report");
		p.addGroupToAllOptions(systemAdminGroup);
		p.addGroupToOptions(allUsersPseudoGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.save();
		
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_TRANSACTION_LISTING_VIEW, "Access the default transaction listing report");
		p.addGroupToAllOptions(systemAdminGroup);
		p.save();
		
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_TRANSACTION_TEMPLATE_VIEW, "Access the Transaction Template screen");
		p.addGroupToAllOptions(systemAdminGroup);
		p.save();
		
		p = Permission.Manager.create(UI.UI_PERM_UI_VIEW, "Access the UI View screen",
				PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.save();
		
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_USER_VIEW, "Access the Users screen");
		p.addGroupToAllOptions(systemAdminGroup);
		p.save();
		
		p = Permission.Manager._dbsetup_createForAllUsersGroup(UI.UI_PERM_USER_CHANGE_PASSWORD_VIEW, "Access to Change My Password screen", allUsersPseudoGroup);
		p.addGroupToAllOptions(systemAdminGroup);
		p.save();
		
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_TRANSACTION_CSV_START_VIEW, "Access the CSV Upload screen to start transactions");
		p.addGroupToAllOptions(systemAdminGroup);
		p.save();
		
		// THE FOLLOWING ARE FOR TipsView and TipsForm views...
		
		// Only system admin should be able to edit
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_ACCESS_CONTROL_TIPS_EDIT_VIEW, "Access the Access Control link tips edit screen");
		p.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.save();
		
		// Anybody can view it
		p = Permission.Manager.create(UI.UI_PERM_ACCESS_CONTROL_TIPS_VIEW, "View the Access Control Link Tips page",
				PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.addGroupToOptions(allUsersPseudoGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.save();		
		
		// Only system admin should be able to edit
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_PROGRAMMING_TIPS_EDIT_VIEW, "Access the Programming link tips edit screen");
		p.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.save();
		
		// Anybody can view it
		p = Permission.Manager.create(UI.UI_PERM_PROGRAMMING_TIPS_VIEW, "View the Programming Link Tips page",
				PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.addGroupToOptions(allUsersPseudoGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.save();		
		
		// Only system admin should be able to edit
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_REPORT_TIPS_EDIT_VIEW, "Access the Reports link tips edit screen");
		p.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.save();
		
		// Anybody can view it
		p = Permission.Manager.create(UI.UI_PERM_REPORT_TIPS_VIEW, "View the Reports Link Tips page",
				PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.addGroupToOptions(allUsersPseudoGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.save();		
		
		// Only system admin should be able to edit
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_START_TRANSACTION_TIPS_EDIT_VIEW, "Access the Start Transaction link tips edit screen");
		p.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.save();
		
		// Anybody can view it
		p = Permission.Manager.create(UI.UI_PERM_START_TRANSACTION_TIPS_VIEW, "View the Start Transaction link ",
				PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.addGroupToOptions(allUsersPseudoGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.save();		
		
		// Only system admin should be able to edit
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_SYSTEM_CONFIG_TIPS_EDIT_VIEW, "Access the System Config link tips edit screen");
		p.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.save();
		
		// Anybody can view it
		p = Permission.Manager.create(UI.UI_PERM_SYSTEM_CONFIG_TIPS_VIEW, "View the System Config Link Tips page",
				PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.addGroupToOptions(allUsersPseudoGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.save();		
		
		// Only system admin should be able to edit
		p = Permission.Manager.createWithStandardOptions(UI.UI_PERM_WELCOME_TIPS_EDIT_VIEW, "Access the Welcome Message edit screen");
		p.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, PermissionOption.PERM_OPTION_UPDATE);
		p.save();
		
		// Anybody can view it
		p = Permission.Manager.create(UI.UI_PERM_WELCOME_TIPS_VIEW, "View the Welcome Message page",
				PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.addGroupToOptions(allUsersPseudoGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
		p.save();		
	}

	
	

    /**
     * Creates the initial deployment application and global properties Records.  Since these are encrypted,
     * we cannot created them when the esf_deployment row is first inserted and wait until the boot keys are 
     * also created and we have our initial session key.
     */
    public void createInitialProperties(EsfUUID deployId, Group superGroup) throws Exception
    {
	    ConnectionPool    pool = app.getConnectionPool();
	    String dbUserName = pool.getUserName();

	    Record deployProps	= new Record( new EsfName("DeploymentProperties"), BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE );
    	
    	// We'll set up the defaults to be Yozons-general to match drop down lists, but since we run multiple OpenESF webapps in the same Tomcat JVM,
    	// we don't use Java's defaults in our code because each webapp would override the defaults of the other if set in the JVM.
    	NameValue defaultLocale = new NameValue( app.getDefaultLocaleEsfName(), new EsfString("en_US") );
    	defaultLocale.setComment("This is the OpenESF default locale to use when no user, document or transaction override is in place.");
    	deployProps.addUpdate( defaultLocale );
    	
    	NameValue defaultTimeZone = new NameValue( app.getDefaultTimeZoneEsfName(), new EsfString("PST8PDT") );
    	defaultTimeZone.setComment("This is the OpenESF default time zone to use when no user, document or transaction override is in place.");
    	deployProps.addUpdate( defaultTimeZone  );
    	
    	NameValue defaultCountryCode = new NameValue( app.getDefaultCountryCodeEsfName(), new EsfString("US") );
    	defaultCountryCode.setComment("This is the OpenESF default country code.");
    	deployProps.addUpdate( defaultCountryCode  );
    	
    	NameValue defaultDateFormat = new NameValue( app.getDefaultDateFormatEsfName(), new EsfString("dd-MMM-yyyy") );
    	defaultDateFormat.setComment("This is the OpenESF default date format.");
    	deployProps.addUpdate( defaultDateFormat  );
    	
    	NameValue defaultTimeFormat = new NameValue( app.getDefaultTimeFormatEsfName(), new EsfString("h:mm a z") );
    	defaultTimeFormat.setComment("This is the OpenESF default time format.");
    	deployProps.addUpdate( defaultTimeFormat  );
    	
    	NameValue defaultLogDateFormat = new NameValue( app.getDefaultLogDateFormatEsfName(), new EsfString("yyyy-MM-dd") );
    	defaultLogDateFormat.setComment("This is the OpenESF default log/detailed date format.");
    	deployProps.addUpdate( defaultLogDateFormat  );
    	
    	NameValue defaultLogTimeFormat = new NameValue( app.getDefaultLogTimeFormatEsfName(), new EsfString("HH:mm:ss z") );
    	defaultLogTimeFormat.setComment("This is the OpenESF default log/detailed time format.");
    	deployProps.addUpdate( defaultLogTimeFormat  );
    	
    	EsfDate today = new EsfDate();
    	NameValue installDate = new NameValue( app.getInstallDateEsfName(), today );
    	installDate.setComment("The date this system was installed.");
    	deployProps.addUpdate( installDate );
    	
    	NameValue installYear = new NameValue( app.getInstallYearEsfName(), new EsfInteger(today.getYearString()) );
    	installYear.setComment("The year this system was installed, generally for selection lists and calendars related to transactions/documents that clearly cannot be earlier than this year.");
    	deployProps.addUpdate( installYear );

    	// Now for some items related to SMTP sending and IMAP email bounce handling
    	String hostname = java.net.InetAddress.getLocalHost().getHostName();
    	deployProps.addUpdate( new NameValue( app.getDefaultUrlProtocolAddressEsfName(), new EsfString("https://"+hostname) ) );
    	deployProps.addUpdate( new NameValue( app.getSmtpReturnPathHostNameEsfName(), new EsfString(hostname) ) );
    	deployProps.addUpdate( new NameValue( app.getSmtpServerEsfName(), new EsfString("localhost.localdomain") ) );
    	deployProps.addUpdate( new NameValue( app.getSmtpPortEsfName(), new EsfInteger("25") ) );
    	deployProps.addUpdate( new NameValue( app.getSmtpAuthUserEsfName(), new EsfString("") ) );
    	deployProps.addUpdate( new NameValue( app.getSmtpAuthPasswordEsfName(), new EsfString("") ) );
    	deployProps.addUpdate( new NameValue( app.getSmtpStartTLSEsfName(), new EsfBoolean(false) ) );
    	deployProps.addUpdate( new NameValue( app.getImapServerEsfName(), new EsfString("localhost.localdomain") ) );
    	deployProps.addUpdate( new NameValue( app.getImapPortEsfName(), new EsfInteger("143") ) );
    	deployProps.addUpdate( new NameValue( app.getImapUserEsfName(), new EsfString(dbUserName) ) );
    	deployProps.addUpdate( new NameValue( app.getImapPasswordEsfName(), new EsfString(dbUserName) ) );
    	deployProps.addUpdate( new NameValue( app.getImapSSLEsfName(), new EsfBoolean(false) ) );
    	deployProps.addUpdate( new NameValue( app.getDefaultEmailFromEsfName(), new EsfString("no-reply@"+hostname) ) );
    	
    	deployProps.addUpdate( new NameValue( app.getDefaultSessionTimeoutMinutesEsfName(), new EsfInteger(30) ) ); 
    	deployProps.addUpdate( new NameValue( app.getLoggedInSessionTimeoutMinutesEsfName(), new EsfInteger(30) ) ); 

    	deployProps.addUpdate( new NameValue( app.getRetainSystemConfigChangeDaysEsfName(), new EsfInteger(365) ) ); 
    	deployProps.addUpdate( new NameValue( app.getRetainSystemUserActivityDaysEsfName(), new EsfInteger(90) ) ); 
    	deployProps.addUpdate( new NameValue( app.getRetainSystemStartStopDaysEsfName(), new EsfInteger(365) ) ); 
    	deployProps.addUpdate( new NameValue( app.getRetainStatsDaysEsfName(), new EsfInteger(365) ) ); 
    	deployProps.addUpdate( new NameValue( app.getRetainUserConfigChangeDaysEsfName(), new EsfInteger(365) ) ); 
    	deployProps.addUpdate( new NameValue( app.getRetainUserSecurityDaysEsfName(), new EsfInteger(365) ) ); 
    	deployProps.addUpdate( new NameValue( app.getRetainUserLoginLogoffDaysEsfName(), new EsfInteger(90) ) ); 

    	deployProps.addUpdate( new NameValue( app.getLicenseTypeEsfName(), Application.LICENSE_TYPE_DEFAULT ) ); // Open Source by default
    	deployProps.addUpdate( new NameValue( app.getLicenseSizeEsfName(), new EsfInteger(0) ) ); // Unlimited

    	deployProps.addUpdate( new NameValue( app.getAllowProductionTransactionsEsfName(), new EsfBoolean(true) ) );
    	deployProps.addUpdate( new NameValue( app.getAllowTestTransactionsEsfName(), new EsfBoolean(true) ) );
    	
    	// Global properties are for user-defined properties so that they are distinct from system properties that users shouldn't have to touch
    	Record globalProps	= new Record( new EsfName("GlobalProperties"), BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE);
    	NameValue sampleGlobalProp = new NameValue( new EsfName("SampleGlobalPropertyName"), new EsfString("sample global property string value") );
    	sampleGlobalProp.setComment("This is just a placeholder sample to create the global properties that can be shared across all transactions.");
    	globalProps.addUpdate(sampleGlobalProp);
    	
	    Connection        con  = pool.getConnection();
	    PreparedStatement stmt = null;
	    
	    try
	    {
	    	globalProps.save(con);
	    	deployProps.save(con);
	    	
	        stmt = con.prepareStatement("UPDATE esf_deployment SET global_properties_id=?,deploy_properties_id=? WHERE id=?");
	        stmt.setObject(1,globalProps.getId().getUUID(),Types.OTHER);
	        stmt.setObject(2,deployProps.getId().getUUID(),Types.OTHER);
	        stmt.setObject(3,deployId.getUUID(),Types.OTHER);
	        int num = stmt.executeUpdate();
	        if ( num != 1 )
	        {
	        	displayError("ERROR: createInitialProperties - Unexpected update count on update deployment with properties: " + num);
                pool.rollbackIgnoreException(con);
	    		throw new Exception("deployment properties could not be set");
	        }
	        
	        display("createInitialProperties - Updated deployment with global properties id: " + globalProps.getId() + 
	        		"; deployment properties id: " + deployProps.getId());
	        
            con.commit();
	    }
	    catch(SQLException e) 
	    {
            display("createInitialProperties - SQL Error: " + e.getMessage());
			app.sqlerr(e,"ERROR: createInitialProperties()");
            pool.rollbackIgnoreException(con,e);
            throw new Exception("deployment properties could not be created because of SQL failure");
	    }
	    finally
	    {
	        app.cleanupPool(pool,con,stmt);
	    }
    }
    
    /**
     * Inserts a user record into the esf_user table and give this "super" user all permissions.
     */
    public User insertSuperUser(String email, String firstName, String lastName, String password) throws Exception
    {
        ConnectionPool    pool = app.getConnectionPool();
        Connection        con  = pool.getConnection();
        PreparedStatement stmt  = null;
        PreparedStatement stmt2 = null;
        
        try
        {
        	PasswordManager pm = new PasswordManager(false);
        	EsfUUID userId = new EsfUUID();

        	String passwordHash = pm.createPasswordHash(userId,password);

        	User user = User.Manager._dbsetup_createNewSuperUser(userId, email, firstName, lastName, passwordHash);
        	
        	if ( ! user.save(con) )
        	{
                displayError("ERROR: insertSuperUser - Could not insert new super user: " + email);
                pool.rollbackIgnoreException(con);
                return null;
        	}

            String question     = "What is the password";
            String answerHash   = pm.createForgotPasswordAnswerHash(user.getId(),password);
            Timestamp setTimestamp = DateUtil.getCurrentSqlTimestamp();
            
            stmt = con.prepareStatement(
                    "INSERT INTO esf_userlogin (user_id,pwd_hash,forgotten_question,forgotten_answer_hash," +
                    "reset_pickup_code,forgot_pickup_code,last_login_timestamp,last_login_ip,password_set_timestamp,hash_version,invalid_login_count,invalid_answer_count,request_forgot_count) " +
                    "VALUES (?,?,?,?,NULL,NULL,NULL,NULL,?,?,0,0,0)"
                                       );
            stmt.setObject(1,user.getId().getUUID(),Types.OTHER);
            stmt.setString(2,passwordHash);
            stmt.setString(3,question);
            stmt.setString(4,answerHash);
            stmt.setTimestamp(5,setTimestamp);
            stmt.setShort(6,pm.getCurrentHashVersion());
            stmt.executeUpdate();
            
            stmt.close(); stmt = null;
            
            // Add this info to the history, too
            stmt = con.prepareStatement(
                    "INSERT INTO esf_userlogin_history (user_id,password_set_timestamp,pwd_hash,forgotten_answer_hash) VALUES (?,?,?,?)"
                                       );
            stmt.setObject(1,user.getId().getUUID(),Types.OTHER);
            stmt.setTimestamp(2,setTimestamp);
            stmt.setString(3,passwordHash);
            stmt.setString(4,answerHash);
            stmt.executeUpdate();
            
            display("insertSuperUser - Added new super user: " + email);
            
            con.commit();
            
            return user;
        }
        catch(SQLException e) 
        {
            display("insertSuperUser - SQL Error: " + e.getMessage());
            app.sqlerr(e,"ERROR: insertSuperUser()");
            pool.rollbackIgnoreException(con,e);
            return null;
        }
        finally
        {
            app.cleanupStatement(stmt2);
            app.cleanupPool(pool,con,stmt);
        }
    }
    
    /**
     * Inserts a super group record into the esf_group table (if it doesn't already exist) and give the specified 
     * "super" group all permissions, and assigns the specified User to be a member of this group.
     */
    public Group insertUserIntoSuperGroup(User superUser) throws Exception
    {
        ConnectionPool    pool = app.getConnectionPool();
        Connection        con  = pool.getConnection();
        PreparedStatement stmt  = null;
        
        try
        {
        	Group superGroup = Group.Manager.getSuperGroup();
        	
        	// Let's add the user to the super group now
            superGroup.addMemberUser(superUser);
        	if ( ! superGroup.save(con) )
        	{
                displayError("ERROR: insertUserIntoSuperGroup - Could not insert the user into the super group: " + superGroup.getPathName());
                pool.rollbackIgnoreException(con);
                return null;
        	}

            con.commit();
            display("insertUserIntoSuperGroup - Added new super user: " + superUser.getFullDisplayName() + "; to super group: " + superGroup.getPathName());
            
            return superGroup;
        }
        catch(SQLException e) 
        {
            display("insertUserIntoSuperGroup - SQL Error: " + e.getMessage());
            app.sqlerr(e,"ERROR: insertUserIntoSuperGroup()");
            pool.rollbackIgnoreException(con,e);
            return null;
        }
        finally
        {
            app.cleanupPool(pool,con,stmt);
        }
    }
    
    public User setPassword(String email, String password) throws Exception
	{
	    try
	    {
	    	User user = User.Manager.getByEmail(email);
	    	if ( user == null )
	    	{
	    		displayError("ERROR: setPassword - User not found: " + email);
	            return null;
	    	}

	    	PasswordManager pm = new PasswordManager(true);
	    	
	    	pm.requestSetPassword(user, null, null, "DbSetup"); // force account to be in state to allow a password to be set
	    	pm.setPassword(user, password, "What's the password it was reset to", password, null, "DbSetup");

	        display("setPassword - Changed password for user: " + user.getFullDisplayName());
	        
	        return user;
	    }
	    catch(EsfException e) 
	    {
	        display("setPassword - Failed: " + e.getMessage());
	        return null;
	    }
	}
    
    public void createTemplatePackage(User user)
    {
    	Package templatePackage = Package.Manager.getTemplate();
    	if ( templatePackage != null )
    		return;
    	
    	Group superGroup = Group.Manager.getSuperGroup();
    	Group systemAdminGroup = Group.Manager.getSystemAdminGroup();
    	
    	templatePackage = Package.Manager._dbsetup_createTemplate(superGroup,systemAdminGroup);
    	PackageVersion templatePackageVersion = PackageVersion.Manager.createTest(templatePackage,user);
    	templatePackage.promoteTestVersionToProduction();
    	templatePackage.save(user);
    	templatePackageVersion.save(user);
    	display("Added template package and version: " + templatePackage.getPathName());
    }
    
    public Package createCompanyTemplatePackage(User user, Group companyProgrammingGroup, String companyName)
    {
    	Package companyTemplatePackage = Package.Manager.createLike(Package.Manager.getTemplate(), new EsfPathName("Package/Template"), user);
    	companyTemplatePackage.setComments("Template package for " + companyName);
    	PackageVersion companyTemplatePackageVersion = PackageVersion.Manager.createTest(companyTemplatePackage,user);
    	companyTemplatePackage.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_LIST, companyProgrammingGroup);
    	companyTemplatePackage.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_VIEWDETAILS, companyProgrammingGroup);
    	companyTemplatePackage.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_CREATELIKE, companyProgrammingGroup);
    	companyTemplatePackage.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_UPDATE, companyProgrammingGroup);
    	companyTemplatePackage.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_DELETE, companyProgrammingGroup);
    	companyTemplatePackage.save(user);
    	companyTemplatePackageVersion.save(user);
    	display("Added template package and version: " + companyTemplatePackage.getPathName());
    	return companyTemplatePackage;
    }
    
    public void createEsfTemplateTransactionTemplate(User user)
	{
        TransactionTemplate templateTransactionTemplate = TransactionTemplate.Manager.getTemplate();
        if ( templateTransactionTemplate == null )
        {
        	templateTransactionTemplate = TransactionTemplate.Manager._dbsetup_createTemplate(Group.Manager.getSuperGroup(),Group.Manager.getSystemAdminGroup());
        	templateTransactionTemplate.save(user);
        	display("Added ESF template transaction template: " + templateTransactionTemplate.getPathName());
        }
	}

    public void createEsfTemplateReportTemplate(User user, Group companyGroup, Group companyProgrammingGroup)
	{
        ReportTemplate templateReportTemplate = ReportTemplate.Manager.getTemplate();
        if ( templateReportTemplate == null )
        {
        	templateReportTemplate = ReportTemplate.Manager._dbsetup_createTemplate(Group.Manager.getSuperGroup(),Group.Manager.getSystemAdminGroup());
        	templateReportTemplate.save(user);
        	display("Added ESF template report template: " + templateReportTemplate.getPathName());       	
        }
        
    	if ( companyGroup != null && companyProgrammingGroup != null )
    	{
    		EsfPathName companyReportPathName = new EsfPathName(companyGroup.getPathName()+"/Report/Template");
    		
    		ReportTemplate companyReportTemplate = ReportTemplate.Manager.getByPathName(companyReportPathName);
    		if ( companyReportTemplate == null )
    		{
        		companyReportTemplate = ReportTemplate.Manager.createLike(templateReportTemplate, companyReportPathName, user);
            	companyReportTemplate.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_LIST, companyProgrammingGroup);
            	companyReportTemplate.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_VIEWDETAILS, companyProgrammingGroup);
            	companyReportTemplate.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_CREATELIKE, companyProgrammingGroup);
            	companyReportTemplate.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_UPDATE, companyProgrammingGroup);
            	companyReportTemplate.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_DELETE, companyProgrammingGroup);
        		companyReportTemplate.save(user);
            	display("Added company template report template: " + companyReportTemplate.getPathName());
    		}
    	}
	}
    
    void createBuiltInReportFieldTemplates()
    {
		// Insert our built-in report fields
		ReportFieldTemplate rft;
		rft = new ReportFieldTemplate( new EsfUUID(), new EsfName("esf_transaction_id"), ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Id", "The unique transaction id.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), new EsfName("esf_transaction_template_name"), ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Tran template", "The transaction template.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), new EsfName("esf_package_name"), ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Package", "The transaction template's package.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), new EsfName("esf_status"), ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Status", "The current transaction status.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), new EsfName("esf_status_text"), ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Last status", "The last transaction status text.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), new EsfName("esf_start_timestamp"), ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Started", "The date/time the transaction was started.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), new EsfName("esf_last_updated_timestamp"), ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Last updated", "The date/time the transaction was last updated.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), new EsfName("esf_created_by_user"), ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Started by", "The user, if any, who started the transaction.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), new EsfName("esf_last_updated_by_user"), ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Last updated by", "The user, if any, who last updated the transaction.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), new EsfName("esf_expire_timestamp"), ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Expires", "The date/time the transaction expires and will be deleted.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), new EsfName("esf_stall_timestamp"), ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Stalled", "The date/time the transaction stalled: in progress but without an active party.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), new EsfName("esf_cancel_timestamp"), ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Cancels", "The date/time the transaction automatically will cancel.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_LATEST_SNAPSHOTS, ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Documents", "Download all of the latest documents as a PDF.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), ReportFieldTemplate.BUILT_IN_DOWNLOAD_SELECTED_SNAPSHOTS, ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Documents", "Download selected documents.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), ReportFieldTemplate.BUILT_IN_DOWNLOAD_HTML_SELECTED_SNAPSHOT, ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Documents", "Download selected document as HTML.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), ReportFieldTemplate.BUILT_IN_RESUME_TRANSACTION_BUTTON, ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Resume tran", "Resume this suspended transaction.", (short)0);
		rft.save();
		rft = new ReportFieldTemplate( new EsfUUID(), new EsfName("esf_literal"), ReportFieldTemplate.FIELD_TYPE_BUILT_IN, "Literal", "Includes a specified literal value.", (short)0);
		rft.save();
    }

    public void createCompanyTemplateTransactionTemplate(User user, Group companyGroup, Group companyProgrammingGroup, Library companyLib, Package companyPackage)
	{
    	TransactionTemplate templateTransactionTemplate = TransactionTemplate.Manager.createLike(TransactionTemplate.Manager.getTemplate(), new EsfPathName("TransactionTemplate/Template"), user);
    	templateTransactionTemplate.setBrandLibraryId(companyLib.getId());
    	templateTransactionTemplate.setPackageId(companyPackage.getId());
    	//templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_LIST, companyProgrammingGroup); -- don't want it to appear in lists, but detail lets them see it in ACID view
    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_VIEWDETAILS, companyProgrammingGroup);
    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_CREATELIKE, companyProgrammingGroup);
    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_UPDATE, companyProgrammingGroup);
    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_DELETE, companyProgrammingGroup);

    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.TRAN_PERM_OPTION_START, companyGroup);
    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.TRAN_PERM_OPTION_START, companyProgrammingGroup);
    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.TRAN_PERM_OPTION_CANCEL, companyGroup);
    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.TRAN_PERM_OPTION_CANCEL, companyProgrammingGroup);
    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.TRAN_PERM_OPTION_REACTIVATE, companyGroup);
    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.TRAN_PERM_OPTION_REACTIVATE, companyProgrammingGroup);
    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.TRAN_PERM_OPTION_SUSPEND, companyGroup);
    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.TRAN_PERM_OPTION_SUSPEND, companyProgrammingGroup);
    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.TRAN_PERM_OPTION_RESUME, companyGroup);
    	templateTransactionTemplate.addPermissionAllowedGroups(PermissionOption.TRAN_PERM_OPTION_RESUME, companyProgrammingGroup);
    	
    	templateTransactionTemplate.save(user);
    	display("Added company template transaction template: " + templateTransactionTemplate.getPathName());
	}

    public void createInitialButtonMessageSet(User user)
	{
        ButtonMessage bm = ButtonMessage.Manager.getDefault();
        if ( bm == null )
        {
        	bm = ButtonMessage.Manager._dbsetup_createDefault();
        	ButtonMessageVersion bmv = ButtonMessageVersion.Manager.createTest(bm, user);
        	bmv.setPackageFooterHtml(new EsfString("<p class=\"f8 gray\">* Bookmark or copy this page location to return later: ${LINK}</p>"));
        	bmv.setDocumentFooterHtml(new EsfString(""));
        	
        	bmv.setPackageButtonContinueToDo(new EsfString("Continue"));
        	bmv.setPackageButtonContinueToDoTitle(new EsfString("Please click to continue to review or process the document(s) in this package."));
        	bmv.setPackageButtonContinueDone(new EsfString("View first document"));
        	bmv.setPackageButtonContinueDoneTitle(new EsfString("Please click to continue to view the completed document(s) in this package."));
        	bmv.setPackageButtonEditDocument(new EsfString("Edit"));
        	bmv.setPackageButtonEditDocumentTitle(new EsfString("Click to edit/update this document's data."));
        	bmv.setPackageButtonViewCompletedDocument(new EsfString("View"));
        	bmv.setPackageButtonViewCompletedDocumentTitle(new EsfString("Click to view this document you previously completed."));
        	bmv.setPackageButtonDelete(new EsfString("Delete this transaction"));
        	bmv.setPackageButtonDeleteTitle(new EsfString("If you click this button you will delete this transaction and related package of documents and data. It cannot be recovered."));
        	bmv.setPackageButtonNotCompleting(new EsfString("Go back (not completing now)"));
        	bmv.setPackageButtonNotCompletingTitle(new EsfString("Click this button to go back and indicate that you are not going to complete the document(s) at this time."));
        	bmv.setPackageButtonDownloadMyDocsAsPdf(new EsfString("Download my document(s) as a PDF"));
        	bmv.setPackageButtonDownloadMyDocsAsPdfTitle(new EsfString("Click this button to download your completed document(s) as a digitally signed PDF file."));
        	
        	bmv.setDocumentButtonGoToPackage(new EsfString("Return to the document list"));
        	bmv.setDocumentButtonGoToPackageTitle(new EsfString("Click this button to return to the package page listing all of the documents."));
        	bmv.setDocumentButtonGoToPackageMessage(new EsfString("Click RETURN TO THE DOCUMENT LIST to return to the package listing of documents prepared for you."));
        	
        	bmv.setDocumentEditButtonContinueNextPage(new EsfString("Continue to next page"));
        	bmv.setDocumentEditButtonContinueNextPageTitle(new EsfString("Click this button to continue to the next page of this document."));
        	bmv.setDocumentEditButtonContinueToReview(new EsfString("Continue to review"));
        	bmv.setDocumentEditButtonContinueToReviewTitle(new EsfString("Click this button to continue to review the complete document."));
        	bmv.setDocumentEditButtonContinueToReviewMessage(new EsfString("Click CONTINUE TO REVIEW to look over the document one more time before submitting it online."));
        	bmv.setDocumentEditButtonPreviousPage(new EsfString("Go back to previous page"));
        	bmv.setDocumentEditButtonPreviousPageTitle(new EsfString("Click this button to go back to the previous page of this document."));
        	bmv.setDocumentEditButtonSave(new EsfString("Save"));
        	bmv.setDocumentEditButtonSaveTitle(new EsfString("Click to save the incomplete data in this document so you can review and submit it later."));
        	
        	bmv.setDocumentReviewViewOnlyButtonContinueNextPage(new EsfString("Continue"));
        	bmv.setDocumentReviewViewOnlyButtonContinueNextPageTitle(new EsfString("Click this button to view the next page of this document."));
        	bmv.setDocumentReviewViewOnlyButtonContinueNextPageMessage(new EsfString("Click CONTINUE after you have read this page."));
        	bmv.setDocumentReviewViewOnlyButtonComplete(new EsfString("Continue"));
        	bmv.setDocumentReviewViewOnlyButtonCompleteTitle(new EsfString("Click this button to indicate you have read the entire document."));
        	bmv.setDocumentReviewViewOnlyButtonCompleteMessage(new EsfString("Click CONTINUE after you have read this document."));
        	bmv.setDocumentReviewViewOnlyButtonPreviousPage(new EsfString("Go back to previous page"));
        	bmv.setDocumentReviewViewOnlyButtonPreviousPageTitle(new EsfString("Click this button to go back and view the previous page of this document."));
        	
        	bmv.setDocumentReviewButtonCompleteSigner(new EsfString("Submit signed document"));
        	bmv.setDocumentReviewButtonCompleteSignerTitle(new EsfString("Click this button to submit this document you have completed including your electronic signature and/or initials."));
        	bmv.setDocumentReviewButtonCompleteSignerMessage(new EsfString("Then click SUBMIT SIGNED DOCUMENT below if it is correct."));
        	bmv.setDocumentReviewButtonCompleteNotSigner(new EsfString("Submit completed document"));
        	bmv.setDocumentReviewButtonCompleteNotSignerTitle(new EsfString("Click this button to submit this document you have completed."));
        	bmv.setDocumentReviewButtonCompleteNotSignerMessage(new EsfString("Then click SUBMIT COMPLETED DOCUMENT below if it is correct."));
        	bmv.setDocumentReviewButtonReturnToEdit(new EsfString("Go back to make changes"));
        	bmv.setDocumentReviewButtonReturnToEditTitle(new EsfString("Click this button to return to make additional changes to the document before you submit it."));
        	bmv.setDocumentReviewButtonReturnToEditMessage(new EsfString("Click GO BACK TO MAKE CHANGES if you see something needs to be fixed before submitting it."));
        	
        	bmv.setDocumentViewOnlyButtonNextPage(new EsfString("Continue to next page"));
        	bmv.setDocumentViewOnlyButtonNextPageTitle(new EsfString("Click this button to view the next page of this document."));
        	bmv.setDocumentViewOnlyButtonPreviousPage(new EsfString("Go back to previous page"));
        	bmv.setDocumentViewOnlyButtonPreviousPageTitle(new EsfString("Click this button to view the previous page of this document."));
        	bmv.setDocumentViewOnlyButtonNextDocument(new EsfString("Go to next document"));
        	bmv.setDocumentViewOnlyButtonNextDocumentTitle(new EsfString("Click this button to view the next document in the package."));
        	bmv.setDocumentViewOnlyButtonPreviousDocument(new EsfString("Go to previous document"));
        	bmv.setDocumentViewOnlyButtonPreviousDocumentTitle(new EsfString("Click this button to view the previous document in the package."));
        	
        	bmv.setDocumentAlreadyCompletedMessage(new EsfString("You have already completed this document."));
        	bmv.setDocumentAlreadyCompletedCannotEditMessage(new EsfString("You have already completed this document. You may not make further changes."));
        	bmv.setViewOnlyDocumentFYIMessage(new EsfString("This document is for your information only."));
        	bmv.setReviewDocumentNotSignerMessage(new EsfString("You are ALMOST DONE. Please review the document carefully."));
        	bmv.setReviewDocumentSignerMessage(new EsfString("You are ALMOST DONE. Please review the electronically signed document carefully."));
        	bmv.setCompleteDocumentEditsMessage(new EsfString("Please complete this document."));
        	bmv.setCompletedAllDocumentsMessage(new EsfString("You have completed all of the documents. There is nothing further to do here. Thank you!"));
        	bmv.setDeletedTransactionMessage(new EsfString("You have successfully deleted this transaction and associated documents. There is nothing further to do here."));

        	bm.promoteTestVersionToProduction();
        	bm.save(user);
        	bmv.save(user);
        	display("Created default button message set and version: " + bm.getEsfName());
        }
	}
    
    
    public void createInitialDocumentStyle(User user, Library companyLibrary)
	{
		Library templateLibrary = Library.Manager.getTemplate();
        DocumentStyle ds = DocumentStyle.Manager.getDefault();
        if ( ds == null )
        {
        	ds = DocumentStyle.Manager._dbsetup_createDefault();
        	DocumentStyleVersion dsv = DocumentStyleVersion.Manager.createTest(ds, user);
        	dsv.setDocumentFont(new EsfString("font-family: Calibri, Arial, Helvetica, sans-serif;"));
        	dsv.setDocumentFontSize(new EsfString("font-size: 11pt;"));
        	dsv.setSignatureFont(new EsfString("font-family: 'Pacifico', cursive;"));
        	dsv.setSignatureFontSize(new EsfString("font-size: 14pt;"));
        	dsv.setSignatureFontStyle(new EsfString("font-style: normal; font-weight: normal; font-variant: normal; text-transform: inherit;"));
        	dsv.setSignatureFontColor(new EsfString("color: blue;"));
        	dsv.setInputFieldRequiredFontStyle(new EsfString("font-style: normal; font-weight: bold; font-variant: normal; text-transform: inherit;"));
        	dsv.setInputFieldRequiredBackgroundColor(new EsfString("background-color: #FFFFE7;"));
        	dsv.setInputFieldRequiredBorderTypes(new EsfString("1px solid #7F9DB9"));
        	dsv.setInputFieldOptionalFontStyle(new EsfString("font-style: normal; font-weight: bold; font-variant: normal; text-transform: inherit;"));
        	dsv.setInputFieldOptionalBackgroundColor(new EsfString("background-color: white;"));
        	dsv.setInputFieldOptionalBorderTypes(new EsfString("1px solid #7F9DB9"));
        	dsv.setInputFieldErrorFontStyle(new EsfString("font-style: normal; font-weight: bold; font-variant: normal; text-transform: inherit;"));
        	dsv.setInputFieldErrorBackgroundColor(new EsfString("background-color: #FFC0CB;"));
        	dsv.setInputFieldErrorBorderTypes(new EsfString("1px solid red"));
        	dsv.setNormalLabelErrorFontColor(new EsfString("color: red;"));
        	dsv.setNormalLabelErrorFontStyle(new EsfString("font-style: normal; font-weight: bold; font-variant: normal; text-transform: inherit;"));
        	dsv.setNormalLabelErrorBackgroundColor(new EsfString("background-color: yellow;"));
        	dsv.setSmallLabelFontSize(new EsfString("font-size: 8pt;"));
        	dsv.setSmallLabelErrorFontSize(new EsfString("font-size: 8pt;"));
        	dsv.setSmallLabelErrorFontColor(new EsfString("color: red;"));
        	dsv.setSmallLabelErrorFontStyle(new EsfString("font-style: normal; font-weight: bold; font-variant: normal; text-transform: inherit;"));
        	dsv.setSmallLabelErrorBackgroundColor(new EsfString("background-color: yellow;"));
        	ds.promoteTestVersionToProduction();
        	ds.save(user);
        	dsv.save(user);
        	display("Created default style and version: " + ds.getEsfName());

        	// Update the template library and sample library to point to this as the default document style
        	templateLibrary.setDefaultDocumentStyleId(ds.getId());
        	templateLibrary.save(user);
        	display("Set default style in template library: " + templateLibrary.getPathName());

        	companyLibrary.setDefaultDocumentStyleId(ds.getId());
        	companyLibrary.save(user);
        	display("Set default style in company library: " + companyLibrary.getPathName());
        }
	}
    
    
    public void createInitialPackageDocument(User user)
	{
		Library templateLibrary = Library.Manager.getTemplate();
	    Document doc = Document.Manager.getStandardPackageDocument();
		if ( doc == null )
		{
			doc = Document.Manager.createNew(templateLibrary.getId());
			doc.setEsfName(Document.STANDARD_PACKAGE_DOCUMENT_ESFNAME);
			doc.setDisplayName("Standard Package & Disclosures");
			doc.setDescription("Standard layout for the package+disclosure page");
			doc.setComments("This is a special type of document that is used to present E-Sign disclosures and the document list to be processed by a given party. It is not part of the package of documents.");
			
			DocumentStyle ds = DocumentStyle.Manager.getDefault();
			DocumentVersion docVer = DocumentVersion.Manager.createTest(doc, user);
			docVer.setDocumentStyleId(ds.getId());
			
			DocumentVersionPage page = docVer.getPageNumber(1);
			page.setHtml( new EsfHtml(
"<style type=\"text/css\"> body.esf div.pagediv { max-width: 800px; padding-left: 1ex; padding-right: 1ex; margin-left: auto; margin-right: auto; } </style>\n" +
"<p style=\"margin-left: 25px;\">${image:Logo}</p>\n" +
"<% if ( esf.isTransactionDeleted() ) { %>\n" +	
"<div style=\"background-color: #fda514; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">\n" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px;\">This transaction has been deleted...</h2>\n" +
"</div>\n" +
"<% } else { %>\n" +
"<% if ( esf.isPartyCompleted() ) { %>\n" +
"<div style=\"background-color: #669966; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">\n" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px;\">You are done. Thank you!</h2>\n" +
"</div>\n" +
"<% } else { %>\n" +
"<div style=\"background-color: #2691ce; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">\n" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px;\">Welcome to our electronic signature service...</h2>\n" +
"</div>\n" +
"<% } %>\n" +
"<div style=\"padding-left: 1em; padding-right: 1em;\">\n" +
"<p>Please note that your continued use of this service constitutes your agreement to use electronic signatures in lieu of a paper document with a traditional hand-written signature. Electronic signatures are legally recognized throughout the United States. Your electronic signature will take place when you type your name and/or initials into the marked areas on the subsequent document(s) and then you click both the Review and Submit buttons on each document to indicate your agreement and/or authorization.</p>\n" +
"<p>You also certify that these documents are intended for you and that you are authorized to sign the documents. If you have received these by mistake, please do not continue and <a href=\"mailto:${property:MyCompany.CustomerSupportEmail}\">email us</a> or call ${property:MyCompany.CustomerSupportPhone} to report our error.</p>\n" +
"<p>If you do not wish to sign these documents electronically, please contact us and do not continue with this process. However, we expect that you will prefer this free, easy-to-use, fast and environmentally sound option.</p>\n" +
"</div>\n" +
"<div style=\"padding: 1em; background-color: #eceeee; border-radius: 5px;\">\n" +
"<p style=\"margin-top: 0;\"><strong><u>Document(s) for your review:</u></strong></p>\n" +
"${transaction:documents.todo}</div>\n" +
"<% } %>\n" ) );
			doc.promoteTestVersionToProduction();
			doc.save(user);
			docVer.save(user);
			page.save(user);
			display("Created standard package document: " + doc.getEsfName());
		}
	}
    
    byte[] getResourceAsByteArray(String resourcePath)
    {
    	BufferedInputStream is = null;
    	ByteArrayOutputStream os = null;
    	try
    	{
    		byte[] block = new byte[4096];
    		
        	is = new BufferedInputStream(getClass().getResourceAsStream(resourcePath));
    		os = new ByteArrayOutputStream( Math.max(is.available(), 100 * Literals.KB) );
    		int size;
    		while( (size = is.read(block)) > 0 )
    			os.write(block, 0, size);
    		os.flush();
    		return os.toByteArray();
    	}
    	catch( Exception e )
    	{
    		display("getResourceAsByteArray() for: " + resourcePath + "; exception: " + e.getMessage());
    		return new byte[0];
    	}
    	finally
    	{
    		if ( os != null ) try { os.close(); } catch(Exception e ) {}
    		if ( is != null ) try { is.close(); } catch(Exception e ) {}
    	}
    }
    
    public void createInitialImages(User user)
	{
		Library templateLibrary = Library.Manager.getTemplate();
		EsfName esfname = app.getImageDefaultLogoEsfName();
		Image image = Image.Manager.getByName(templateLibrary.getId(), esfname);
		if ( image == null )
		{
			image = Image.Manager.createNew(templateLibrary.getId());
			image.setEsfName(esfname);
			image.setDescription("The deployment logo.");
			image.setComments("This is the default logo to show on the standard package as well as the login/logoff pages.");
			
			byte[] imageData = getResourceAsByteArray("installData/OpenESignFormsLogo.gif");
			byte[] thumbData;
			try
			{
				Thumbnail thumb = new Thumbnail(imageData);
				thumb.createThumbnail(100);
				thumbData = thumb.saveThumbnailToBytesByMimeType(Application.CONTENT_TYPE_GIF);
			}
			catch( Exception e )
			{
				display("createInitialImages(Logo) - Failed to create thumbnail of OpenESignFormsLogo.gif");
				thumbData = null;
			}
			ImageVersion imageVer = ImageVersion.Manager.createTest(image, user);
			imageVer.setImageFileName("OpenESignFormsLogo.gif");
			imageVer.setImageData(imageData);
			imageVer.setImageMimeType(Application.CONTENT_TYPE_GIF);
			imageVer.setThumbnailData(thumbData);
			image.promoteTestVersionToProduction();
			image.save(user);
			imageVer.save(user);
			display("Created image: " + image.getEsfName());
		}
		
		esfname = app.getImageDefaultLogoForAppEsfName();
		image = Image.Manager.getByName(templateLibrary.getId(), esfname);
		if ( image == null )
		{
			image = Image.Manager.createNew(templateLibrary.getId());
			image.setEsfName(esfname);
			image.setDescription("The Open eSignForms logo shown for the application.");
			image.setComments("This is the default logo in the upper left corner of the application after you login.");
			
			byte[] imageData = getResourceAsByteArray("installData/OpenESignFormsLogo.gif");
			byte[] thumbData;
			try
			{
				Thumbnail thumb = new Thumbnail(imageData);
				thumb.createThumbnail(100);
				thumbData = thumb.saveThumbnailToBytesByMimeType(Application.CONTENT_TYPE_GIF);
			}
			catch( Exception e )
			{
				display("createInitialImages(LogoForApp) - Failed to create thumbnail of OpenESignFormsLogo.gif");
				thumbData = null;
			}
			ImageVersion imageVer = ImageVersion.Manager.createTest(image, user);
			imageVer.setImageFileName("OpenESignFormsLogo.gif");
			imageVer.setImageData(imageData);
			imageVer.setImageMimeType(Application.CONTENT_TYPE_GIF);
			imageVer.setThumbnailData(thumbData);
			image.promoteTestVersionToProduction();
			image.save(user);
			imageVer.save(user);
			display("Created image: " + image.getEsfName());
		}
		
		esfname = app.getImageDefaultLogoForEmailEsfName();
		image = Image.Manager.getByName(templateLibrary.getId(), esfname);
		if ( image == null )
		{
			image = Image.Manager.createNew(templateLibrary.getId());
			image.setEsfName(esfname);
			image.setDescription("The logo shown/embedded in standard emails.");
			image.setComments("This is the default logo included in standard emails, embedded in the HTML.");
			
			byte[] imageData = getResourceAsByteArray("installData/OpenESignFormsLogo.gif");
			byte[] thumbData;
			try
			{
				Thumbnail thumb = new Thumbnail(imageData);
				thumb.createThumbnail(100);
				thumbData = thumb.saveThumbnailToBytesByMimeType(Application.CONTENT_TYPE_GIF);
			}
			catch( Exception e )
			{
				display("createInitialImages(LogoForEmail) - Failed to create thumbnail of OpenESignFormsLogo.gif");
				thumbData = null;
			}
			ImageVersion imageVer = ImageVersion.Manager.createTest(image, user);
			imageVer.setImageFileName("OpenESignFormsLogo.gif");
			imageVer.setImageData(imageData);
			imageVer.setImageMimeType(Application.CONTENT_TYPE_GIF);
			imageVer.setThumbnailData(thumbData);
			imageVer.setUseDataUri(Literals.Y);
			image.promoteTestVersionToProduction();
			image.save(user);
			imageVer.save(user);
			display("Created image: " + image.getEsfName());
		}
		
		esfname = app.getImageSignHereLeftArrowEsfName();
		image = Image.Manager.getByName(templateLibrary.getId(), esfname);
		if ( image == null )
		{
			image = Image.Manager.createNew(templateLibrary.getId());
			image.setEsfName(esfname);
			image.setDescription("A sign here left arrow.");
			image.setComments("A sign here image that points left and can be placed in documents where appropriate.");
			
			byte[] imageData = getResourceAsByteArray("installData/signHere.gif");
			byte[] thumbData;
			try
			{
				Thumbnail thumb = new Thumbnail(imageData);
				thumb.createThumbnail(100);
				thumbData = thumb.saveThumbnailToBytesByMimeType(Application.CONTENT_TYPE_GIF);
			}
			catch( Exception e )
			{
				display("createInitialImages() - Failed to create thumbnail of signHere.gif");
				thumbData = null;
			}
			
			ImageVersion imageVer = ImageVersion.Manager.createTest(image, user);
			imageVer.setImageFileName("signHere.gif");
			imageVer.setImageData(imageData);
			imageVer.setImageMimeType(Application.CONTENT_TYPE_GIF);
			imageVer.setThumbnailData(thumbData);
			image.promoteTestVersionToProduction();
			image.save(user);
			imageVer.save(user);
			display("Created image: " + image.getEsfName());
		}

		esfname = app.getImagePackageDocumentCompletedEsfName();
		image = Image.Manager.getByName(templateLibrary.getId(), esfname);
		if ( image == null )
		{
			image = Image.Manager.createNew(templateLibrary.getId());
			image.setEsfName(esfname);
			image.setDescription("Document in a package that's completed");
			image.setComments("The icon to show for a document listed in a package that's been completed.");
			
			byte[] imageData = getResourceAsByteArray("installData/fatcow32_application_form_done.png");
			byte[] thumbData;
			try
			{
				Thumbnail thumb = new Thumbnail(imageData);
				thumb.createThumbnail(100);
				thumbData = thumb.saveThumbnailToBytesByMimeType(Application.CONTENT_TYPE_PNG);
			}
			catch( Exception e )
			{
				display("createInitialImages() - Failed to create thumbnail of fatcow32_application_form_done.png");
				thumbData = null;
			}
			
			ImageVersion imageVer = ImageVersion.Manager.createTest(image, user);
			imageVer.setImageFileName("fatcow32_application_form_done.png");
			imageVer.setImageData(imageData);
			imageVer.setImageMimeType(Application.CONTENT_TYPE_PNG);
			imageVer.setThumbnailData(thumbData);
			image.promoteTestVersionToProduction();
			image.save(user);
			imageVer.save(user);
			display("Created image: " + image.getEsfName());
		}

		esfname = app.getImagePackageDocumentFixRequestedEsfName();
		image = Image.Manager.getByName(templateLibrary.getId(), esfname);
		if ( image == null )
		{
			image = Image.Manager.createNew(templateLibrary.getId());
			image.setEsfName(esfname);
			image.setDescription("Document in a package to be fixed");
			image.setComments("The icon to show for a document listed in a package that's waiting for the party to fix it after it was returned.");
			
			byte[] imageData = getResourceAsByteArray("installData/fatcow32_application_form_error.png");
			byte[] thumbData;
			try
			{
				Thumbnail thumb = new Thumbnail(imageData);
				thumb.createThumbnail(100);
				thumbData = thumb.saveThumbnailToBytesByMimeType(Application.CONTENT_TYPE_PNG);
			}
			catch( Exception e )
			{
				display("createInitialImages() - Failed to create thumbnail of fatcow32_application_form_error.png");
				thumbData = null;
			}
			
			ImageVersion imageVer = ImageVersion.Manager.createTest(image, user);
			imageVer.setImageFileName("fatcow32_application_form_error.png");
			imageVer.setImageData(imageData);
			imageVer.setImageMimeType(Application.CONTENT_TYPE_PNG);
			imageVer.setThumbnailData(thumbData);
			image.promoteTestVersionToProduction();
			image.save(user);
			imageVer.save(user);
			display("Created image: " + image.getEsfName());
		}

		esfname = app.getImagePackageDocumentRejectedEsfName();
		image = Image.Manager.getByName(templateLibrary.getId(), esfname);
		if ( image == null )
		{
			image = Image.Manager.createNew(templateLibrary.getId());
			image.setEsfName(esfname);
			image.setDescription("Document in a package that's rejected");
			image.setComments("The icon to show for a document listed in a package that's been marked as rejected (not going to complete).");
			
			byte[] imageData = getResourceAsByteArray("installData/application_form_delete.png");
			byte[] thumbData;
			try
			{
				Thumbnail thumb = new Thumbnail(imageData);
				thumb.createThumbnail(100);
				thumbData = thumb.saveThumbnailToBytesByMimeType(Application.CONTENT_TYPE_PNG);
			}
			catch( Exception e )
			{
				display("createInitialImages() - Failed to create thumbnail of application_form_delete.png");
				thumbData = null;
			}
			
			ImageVersion imageVer = ImageVersion.Manager.createTest(image, user);
			imageVer.setImageFileName("application_form_delete.png");
			imageVer.setImageData(imageData);
			imageVer.setImageMimeType(Application.CONTENT_TYPE_PNG);
			imageVer.setThumbnailData(thumbData);
			image.promoteTestVersionToProduction();
			image.save(user);
			imageVer.save(user);
			display("Created image: " + image.getEsfName());
		}

		esfname = app.getImagePackageDocumentToDoEsfName();
		image = Image.Manager.getByName(templateLibrary.getId(), esfname);
		if ( image == null )
		{
			image = Image.Manager.createNew(templateLibrary.getId());
			image.setEsfName(esfname);
			image.setDescription("Document in a package to be processed");
			image.setComments("The icon to show for a document listed in a package that's waiting for the party to complete it.");
			
			byte[] imageData = getResourceAsByteArray("installData/application_form_edit.png");
			byte[] thumbData;
			try
			{
				Thumbnail thumb = new Thumbnail(imageData);
				thumb.createThumbnail(100);
				thumbData = thumb.saveThumbnailToBytesByMimeType(Application.CONTENT_TYPE_PNG);
			}
			catch( Exception e )
			{
				display("createInitialImages() - Failed to create thumbnail of application_form_edit.png");
				thumbData = null;
			}
			
			ImageVersion imageVer = ImageVersion.Manager.createTest(image, user);
			imageVer.setImageFileName("application_form_edit.png");
			imageVer.setImageData(imageData);
			imageVer.setImageMimeType(Application.CONTENT_TYPE_PNG);
			imageVer.setThumbnailData(thumbData);
			image.promoteTestVersionToProduction();
			image.save(user);
			imageVer.save(user);
			display("Created image: " + image.getEsfName());
		}

		esfname = app.getImagePackageDocumentViewOnlyEsfName();
		image = Image.Manager.getByName(templateLibrary.getId(), esfname);
		if ( image == null )
		{
			image = Image.Manager.createNew(templateLibrary.getId());
			image.setEsfName(esfname);
			image.setDescription("View only document in a package");
			image.setComments("The icon to show for a document listed in a package that's \"view only\" for the party.");
			
			byte[] imageData = getResourceAsByteArray("installData/application_form.png");
			byte[] thumbData;
			try
			{
				Thumbnail thumb = new Thumbnail(imageData);
				thumb.createThumbnail(100);
				thumbData = thumb.saveThumbnailToBytesByMimeType(Application.CONTENT_TYPE_PNG);
			}
			catch( Exception e )
			{
				display("createInitialImages() - Failed to create thumbnail of application_form.png");
				thumbData = null;
			}
			
			ImageVersion imageVer = ImageVersion.Manager.createTest(image, user);
			imageVer.setImageFileName("application_form.png");
			imageVer.setImageData(imageData);
			imageVer.setImageMimeType(Application.CONTENT_TYPE_PNG);
			imageVer.setThumbnailData(thumbData);
			image.promoteTestVersionToProduction();
			image.save(user);
			imageVer.save(user);
			display("Created image: " + image.getEsfName());
		}

		esfname = app.getImageHowToVideoIconEsfName();
		image = Image.Manager.getByName(templateLibrary.getId(), esfname);
		if ( image == null )
		{
			image = Image.Manager.createNew(templateLibrary.getId());
			image.setEsfName(esfname);
			image.setDescription("How To Video icon");
			image.setComments("Shown in various pages with links to the YouTube How-To-Videos.");
			
			byte[] imageData = getResourceAsByteArray("installData/television.png");
			byte[] thumbData;
			try
			{
				Thumbnail thumb = new Thumbnail(imageData);
				thumb.createThumbnail(100);
				thumbData = thumb.saveThumbnailToBytesByMimeType(Application.CONTENT_TYPE_PNG);
			}
			catch( Exception e )
			{
				display("createInitialImages() - Failed to create thumbnail of television.png");
				thumbData = null;
			}
			
			ImageVersion imageVer = ImageVersion.Manager.createTest(image, user);
			imageVer.setImageFileName("television.png");
			imageVer.setImageData(imageData);
			imageVer.setImageMimeType(Application.CONTENT_TYPE_PNG);
			imageVer.setThumbnailData(thumbData);
			image.promoteTestVersionToProduction();
			image.save(user);
			imageVer.save(user);
			display("Created image: " + image.getEsfName());
		}

		esfname = app.getImageIconForErrorEsfName();
		image = Image.Manager.getByName(templateLibrary.getId(), esfname);
		if ( image == null )
		{
			image = Image.Manager.createNew(templateLibrary.getId());
			image.setEsfName(esfname);
			image.setDescription("Icon to show users with error messages");
			image.setComments("Shown in error messages to users that contain errors/warnings.");
			
			byte[] imageData = getResourceAsByteArray("installData/bangOrange.png");
			byte[] thumbData;
			try
			{
				Thumbnail thumb = new Thumbnail(imageData);
				thumb.createThumbnail(100);
				thumbData = thumb.saveThumbnailToBytesByMimeType(Application.CONTENT_TYPE_PNG);
			}
			catch( Exception e )
			{
				display("createInitialImages() - Failed to create thumbnail of bangOrange.png");
				thumbData = null;
			}
			
			ImageVersion imageVer = ImageVersion.Manager.createTest(image, user);
			imageVer.setImageFileName("bangOrange.png");
			imageVer.setImageData(imageData);
			imageVer.setImageMimeType(Application.CONTENT_TYPE_PNG);
			imageVer.setThumbnailData(thumbData);
			image.promoteTestVersionToProduction();
			image.save(user);
			imageVer.save(user);
			display("Created image: " + image.getEsfName());
		}

		esfname = app.getImageIconForInfoEsfName();
		image = Image.Manager.getByName(templateLibrary.getId(), esfname);
		if ( image == null )
		{
			image = Image.Manager.createNew(templateLibrary.getId());
			image.setEsfName(esfname);
			image.setDescription("Icon to show users with non-error messages");
			image.setComments("Shown in messages to users that contain no errors/warnings.");
			
			byte[] imageData = getResourceAsByteArray("installData/bangGreen.png");
			byte[] thumbData;
			try
			{
				Thumbnail thumb = new Thumbnail(imageData);
				thumb.createThumbnail(100);
				thumbData = thumb.saveThumbnailToBytesByMimeType(Application.CONTENT_TYPE_PNG);
			}
			catch( Exception e )
			{
				display("createInitialImages() - Failed to create thumbnail of bangGreen.png");
				thumbData = null;
			}
			
			ImageVersion imageVer = ImageVersion.Manager.createTest(image, user);
			imageVer.setImageFileName("bangGreen.png");
			imageVer.setImageData(imageData);
			imageVer.setImageMimeType(Application.CONTENT_TYPE_PNG);
			imageVer.setThumbnailData(thumbData);
			image.promoteTestVersionToProduction();
			image.save(user);
			imageVer.save(user);
			display("Created image: " + image.getEsfName());
		}
	}

    public void createInitialDropDowns(User user)
	{
    	Library templateLibrary = Library.Manager.getTemplate();
    	createBackgroundColorDropDown(templateLibrary,user);
    	createBorderTypesDropDown(templateLibrary,user);
    	createFontDropDown(templateLibrary,user);
    	createFontColorDropDown(templateLibrary,user);
    	createFontSizeDropDown(templateLibrary,user);
    	createFontStyleDropDown(templateLibrary,user);
    	createTextAlignDropDown(templateLibrary,user);
    	createLocaleDropDown(templateLibrary,user);
    	createTimeZoneDropDown(templateLibrary,user);
    	createCanadaPostalProvincesDropDown(templateLibrary,user);
    	createCountryDropDown(templateLibrary,user);
    	createUSAPostalStatePossessionDropDown(templateLibrary,user);
    	createUSAStatesDropDown(templateLibrary,user);
    	createPartyRenotifyTimesDropDown(templateLibrary,user);
    	createTimeIntervalUnitsDropDown(templateLibrary,user);
    	createDateTimeFormatDropDown(templateLibrary,user);
    	createDateFormatDropDown(templateLibrary,user);
    	createTimeFormatDropDown(templateLibrary,user);
    	createDecimalFormatDropDown(templateLibrary,user);
    	createIntegerFormatDropDown(templateLibrary,user);
    	createIntegerExtraOptionsDropDown(templateLibrary,user);
    	createMoneyFormatDropDown(templateLibrary,user);
    	createPayPalCardTypesDropDown(templateLibrary,user);
    	createStringTransformsDropDown(templateLibrary,user);
	}

    private void createTimeZoneDropDown(Library library, User user) 
    {
        EsfName dropDownName = getApplication().getDropDownTimeZoneEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Default time zone selection box");
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Eastern Time");
        	ddvo.setValue("EST5EDT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Central Time");
        	ddvo.setValue("CST6CDT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Mountain Time");
        	ddvo.setValue("MST7MDT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Pacific Time");
        	ddvo.setValue("PST8PDT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Alaska Time");
        	ddvo.setValue("AST");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Hawaii Time");
        	ddvo.setValue("HST");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("UTC");
        	ddvo.setValue("UTC");
        	optionList.add(ddvo);
        	
        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }
    
    private void createLocaleDropDown(Library library, User user) 
    {
        EsfName dropDownName = getApplication().getDropDownLocaleEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Standard/supported locale selection box");
        	
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("US English");
        	ddvo.setValue("en_US");
        	optionList.add(ddvo);
        	
        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }
    
    // Changes to the drop down can affect DocumentStyleVersion's default creation routines that set values
    // that should match one of these choices.
    private void createFontDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownFontEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Default font selection box");
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("(Default)");
        	ddvo.setValue("");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Inherit");
        	ddvo.setValue("font-family: inherit;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Calibri, Arial");
        	ddvo.setValue("font-family: Calibri, Arial, Helvetica, sans-serif;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Arial");
        	ddvo.setValue("font-family: Arial, Helvetica, sans-serif;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Tahoma, Geneva, Arial");
        	ddvo.setValue("font-family: Tahoma, Geneva, Arial, Helvetica, sans-serif;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Times");
        	ddvo.setValue("font-family: 'Times New Roman', Times, serif;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Georgia, Times");
        	ddvo.setValue("font-family: Georgia, 'Times New Roman', Times, serif;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Bradley, Lucida Handwriting");
        	ddvo.setValue("font-family: 'Bradley Hand ITC', 'Lucida Handwriting', 'Comic Sans MS', cursive;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Comic Sans MS");
        	ddvo.setValue("font-family: 'Comic Sans MS', cursive;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Pacifico");
        	ddvo.setValue("font-family: 'Pacifico', cursive;");
        	optionList.add(ddvo);
        	
        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }
    
    // Changes to the drop down can affect DocumentStyleVersion's default creation routines that set values
    // that should match one of these choices.
    private void createFontSizeDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownFontSizeEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Default font size selection box");
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("(Default)");
        	ddvo.setValue("");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Inherit");
        	ddvo.setValue("font-size: inherit;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("6 point");
        	ddvo.setValue("font-size: 6pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("7 point");
        	ddvo.setValue("font-size: 7pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("8 point");
        	ddvo.setValue("font-size: 8pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("9 point");
        	ddvo.setValue("font-size: 9pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("10 point");
        	ddvo.setValue("font-size: 10pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("11 point");
        	ddvo.setValue("font-size: 11pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("12 point");
        	ddvo.setValue("font-size: 12pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("14 point");
        	ddvo.setValue("font-size: 14pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("16 point");
        	ddvo.setValue("font-size: 16pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("18 point");
        	ddvo.setValue("font-size: 18pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("20 point");
        	ddvo.setValue("font-size: 20pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("22 point");
        	ddvo.setValue("font-size: 22pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("24 point");
        	ddvo.setValue("font-size: 24pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("26 point");
        	ddvo.setValue("font-size: 26pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("28 point");
        	ddvo.setValue("font-size: 28pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("36 point");
        	ddvo.setValue("font-size: 36pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("48 point");
        	ddvo.setValue("font-size: 48pt;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("72 point");
        	ddvo.setValue("font-size: 72pt;");
        	optionList.add(ddvo);
        	
        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }

    // Changes to the drop down can affect DocumentStyleVersion's default creation routines that set values
    // that should match one of these choices.
    private void createFontColorDropDown(Library library, User user) 
    {
        EsfName dropDownName = getApplication().getDropDownFontColorEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Default font color selection box");
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("(Default)");
        	ddvo.setValue("");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Inherit");
        	ddvo.setValue("color: inherit;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Black");
        	ddvo.setValue("color: black;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Gray");
        	ddvo.setValue("color: gray;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Blue");
        	ddvo.setValue("color: blue;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Red");
        	ddvo.setValue("color: red;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Green");
        	ddvo.setValue("color: green;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("White");
        	ddvo.setValue("color: white;");
        	optionList.add(ddvo);
        	
        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }
    
    // Changes to the drop down can affect DocumentStyleVersion's default creation routines that set values
    // that should match one of these choices.
    private void createFontStyleDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownFontStyleEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Describe font styles to choose from.");
        	dd.setComments("This sets the font-style, font-weight, text-transform and font-variant styles.");
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("(Default)");
        	ddvo.setValue("");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Inherit");
        	ddvo.setValue("font-style: inherit; font-weight: inherit; font-variant: inherit; text-transform: inherit;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Normal");
        	ddvo.setValue("font-style: normal; font-weight: normal; font-variant: normal; text-transform: inherit;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Italics");
        	ddvo.setValue("font-style: italic; font-weight: normal; font-variant: normal; text-transform: inherit;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Bold");
        	ddvo.setValue("font-style: normal; font-weight: bold; font-variant: normal; text-transform: inherit;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Bold italics");
        	ddvo.setValue("font-style: italic; font-weight: bold; font-variant: normal; text-transform: inherit;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Small Caps");
        	ddvo.setValue("font-style: normal; font-weight: normal; font-variant: small-caps; text-transform: inherit;");
        	optionList.add(ddvo);
        	ddv.setOptions(optionList);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("All Caps");
        	ddvo.setValue("font-style: normal; font-weight: normal; font-variant: normal; text-transform: uppercase;");
        	optionList.add(ddvo);
        	ddv.setOptions(optionList);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("All Bold Caps");
        	ddvo.setValue("font-style: normal; font-weight: bold; font-variant: normal; text-transform: uppercase;");
        	optionList.add(ddvo);
        	
        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }
    
    // Changes to the drop down can affect DocumentStyleVersion's default creation routines that set values
    // that should match one of these choices.
    private void createTextAlignDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownTextAlignEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Describe text alignments to choose from.");
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Inherit");
        	ddvo.setValue("text-align: inherit;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Left");
        	ddvo.setValue("text-align: left;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Center");
        	ddvo.setValue("text-align: center;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Right");
        	ddvo.setValue("text-align: right;");
        	optionList.add(ddvo);
        	
        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }
    
    // Changes to the drop down can affect DocumentStyleVersion's default creation routines that set values
    // that should match one of these choices.
    private void createBorderTypesDropDown(Library library, User user) 
    {
        EsfName dropDownName = getApplication().getDropDownBorderTypesEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Describes border types with mix of styles.");
        	dd.setComments("Borders are a bit special in that we don't include the full CSS spec because we use them after \"border:\" for all borders, but also \"border-bottom\" and \"border-top:\" etc. to set the attributes based on which borders are desired.");
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("None");
        	ddvo.setValue("0 none inherit");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Clean");
        	ddvo.setValue("1px solid #7F9DB9");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Clean thick");
        	ddvo.setValue("4px solid #7F9DB9");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Clean dotted");
        	ddvo.setValue("1px dotted #7F9DB9");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Clean dashed");
        	ddvo.setValue("1px dashed #7F9DB9");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Black");
        	ddvo.setValue("1px solid black");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Black thick");
        	ddvo.setValue("4px solid black");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Black dotted");
        	ddvo.setValue("1px dotted black");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Black dashed");
        	ddvo.setValue("1px dashed black");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Red");
        	ddvo.setValue("1px solid red");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Red thick");
        	ddvo.setValue("4px solid red");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Red dotted");
        	ddvo.setValue("1px dotted red");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Red dashed");
        	ddvo.setValue("1px dashed red");
        	optionList.add(ddvo);
        	
        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }
    
    // Changes to the drop down can affect DocumentStyleVersion's default creation routines that set values
    // that should match one of these choices.
    private void createBackgroundColorDropDown(Library library, User user) 
    {
        EsfName dropDownName = getApplication().getDropDownBackgroundColorEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Default background color selection box");
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("(Default)");
        	ddvo.setValue("");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Inherit");
        	ddvo.setValue("background-color: inherit;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Black");
        	ddvo.setValue("background-color: black;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Gray");
        	ddvo.setValue("background-color: gray;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Light gray");
        	ddvo.setValue("background-color: #D3D3D3;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Blue");
        	ddvo.setValue("background-color: blue;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Light blue");
        	ddvo.setValue("background-color: #E7E7FB;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Red");
        	ddvo.setValue("background-color: red;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Green");
        	ddvo.setValue("background-color: green;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("White");
        	ddvo.setValue("background-color: white;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Pink");
        	ddvo.setValue("background-color: #FFC0CB;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Yellow");
        	ddvo.setValue("background-color: yellow;");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Light yellow");
        	ddvo.setValue("background-color: #FFFFE7;");
        	optionList.add(ddvo);
        	
        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
       }
    }
    
    private void createUSAPostalStatePossessionDropDown(Library library, User user) 
    {
        EsfName dropDownName = getApplication().getDropDownUSAPostalStatePosessionEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Default USA Postal State/Possession selection box");
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("-SELECT-");
        	ddvo.setValue("");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Alabama");
        	ddvo.setValue("AL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Alaska");
        	ddvo.setValue("AK");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("American Samoa");
        	ddvo.setValue("AS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Arizona");
        	ddvo.setValue("AZ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Arkansas");
        	ddvo.setValue("AR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("California");
        	ddvo.setValue("CA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Colorado");
        	ddvo.setValue("CO");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Connecticut");
        	ddvo.setValue("CT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Delaware");
        	ddvo.setValue("DE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("District of Columbia");
        	ddvo.setValue("DC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Federated-Micronesia");
        	ddvo.setValue("FM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Florida");
        	ddvo.setValue("FL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Georgia");
        	ddvo.setValue("GA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Guam");
        	ddvo.setValue("GU");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Hawaii");
        	ddvo.setValue("HI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Idaho");
        	ddvo.setValue("ID");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Illinois");
        	ddvo.setValue("IL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Indiana");
        	ddvo.setValue("IN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Iowa");
        	ddvo.setValue("IA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Kansas");
        	ddvo.setValue("KS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Kentucky");
        	ddvo.setValue("KY");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Louisiana");
        	ddvo.setValue("LA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Maine");
        	ddvo.setValue("ME");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Marshall Islands");
        	ddvo.setValue("MH");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Maryland");
        	ddvo.setValue("MD");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Massachusetts");
        	ddvo.setValue("MA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Michigan");
        	ddvo.setValue("MI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Minnesota");
        	ddvo.setValue("MN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Mississippi");
        	ddvo.setValue("MS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Missouri");
        	ddvo.setValue("MO");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Montana");
        	ddvo.setValue("MT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Nebraska");
        	ddvo.setValue("NE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Nevada");
        	ddvo.setValue("NV");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("New Hampshire");
        	ddvo.setValue("NH");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("New Jersey");
        	ddvo.setValue("NJ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("New Mexico");
        	ddvo.setValue("NM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("New York");
        	ddvo.setValue("NY");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("North Carolina");
        	ddvo.setValue("NC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("North Dakota");
        	ddvo.setValue("ND");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Northern Mariana Islands");
        	ddvo.setValue("MP");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Ohio");
        	ddvo.setValue("OH");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Oklahoma");
        	ddvo.setValue("OK");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Oregon");
        	ddvo.setValue("OR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Palau");
        	ddvo.setValue("PW");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Pennsylvania");
        	ddvo.setValue("PA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Puerto Rico");
        	ddvo.setValue("PR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Rhode Island");
        	ddvo.setValue("RI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("South Carolina");
        	ddvo.setValue("SC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("South Dakota");
        	ddvo.setValue("SD");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Tennessee");
        	ddvo.setValue("TN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Texas");
        	ddvo.setValue("TX");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Utah");
        	ddvo.setValue("UT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Vermont");
        	ddvo.setValue("VT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Virgin Islands");
        	ddvo.setValue("VI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Virginia");
        	ddvo.setValue("VA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Washington");
        	ddvo.setValue("WA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("West Virginia");
        	ddvo.setValue("WV");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Wisconsin");
        	ddvo.setValue("WI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Wyoming");
        	ddvo.setValue("WY");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Armed Forces Americas");
        	ddvo.setValue("AA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Armed Forces");
        	ddvo.setValue("AE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Armed Forces Pacific");
        	ddvo.setValue("AP");
        	optionList.add(ddvo);
        	
        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }
    
    private void createUSAStatesDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownUSAPostalStatesEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Default USA Postal States and D.C. selection box");
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("-SELECT-");
        	ddvo.setValue("");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Alabama");
        	ddvo.setValue("AL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Alaska");
        	ddvo.setValue("AK");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Arizona");
        	ddvo.setValue("AZ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Arkansas");
        	ddvo.setValue("AR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("California");
        	ddvo.setValue("CA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Colorado");
        	ddvo.setValue("CO");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Connecticut");
        	ddvo.setValue("CT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("District of Columbia");
        	ddvo.setValue("DC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Delaware");
        	ddvo.setValue("DE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Florida");
        	ddvo.setValue("FL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Georgia");
        	ddvo.setValue("GA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Hawaii");
        	ddvo.setValue("HI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Idaho");
        	ddvo.setValue("ID");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Illinois");
        	ddvo.setValue("IL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Indiana");
        	ddvo.setValue("IN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Iowa");
        	ddvo.setValue("IA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Kansas");
        	ddvo.setValue("KS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Kentucky");
        	ddvo.setValue("KY");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Louisiana");
        	ddvo.setValue("LA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Maine");
        	ddvo.setValue("ME");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Maryland");
        	ddvo.setValue("MD");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Massachusetts");
        	ddvo.setValue("MA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Michigan");
        	ddvo.setValue("MI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Minnesota");
        	ddvo.setValue("MN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Mississippi");
        	ddvo.setValue("MS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Missouri");
        	ddvo.setValue("MO");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Montana");
        	ddvo.setValue("MT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Nebraska");
        	ddvo.setValue("NE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Nevada");
        	ddvo.setValue("NV");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("New Hampshire");
        	ddvo.setValue("NH");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("New Jersey");
        	ddvo.setValue("NJ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("New Mexico");
        	ddvo.setValue("NM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("New York");
        	ddvo.setValue("NY");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("North Carolina");
        	ddvo.setValue("NC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("North Dakota");
        	ddvo.setValue("ND");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Ohio");
        	ddvo.setValue("OH");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Oklahoma");
        	ddvo.setValue("OK");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Oregon");
        	ddvo.setValue("OR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Pennsylvania");
        	ddvo.setValue("PA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Rhode Island");
        	ddvo.setValue("RI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("South Carolina");
        	ddvo.setValue("SC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("South Dakota");
        	ddvo.setValue("SD");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Tennessee");
        	ddvo.setValue("TN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Texas");
        	ddvo.setValue("TX");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Utah");
        	ddvo.setValue("UT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Vermont");
        	ddvo.setValue("VT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Virginia");
        	ddvo.setValue("VA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Washington");
        	ddvo.setValue("WA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("West Virginia");
        	ddvo.setValue("WV");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Wisconsin");
        	ddvo.setValue("WI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Wyoming");
        	ddvo.setValue("WY");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }
    
    
    private void createCanadaPostalProvincesDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownCanadaPostalProvincesEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Default Canadian Postal Provinces selection box");
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("-SELECT-");
        	ddvo.setValue("");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Alberta");
        	ddvo.setValue("AB");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("British Columbia");
        	ddvo.setValue("BC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Manitoba");
        	ddvo.setValue("MB");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("New Brunswick");
        	ddvo.setValue("NB");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Newfoundland and Labrador");
        	ddvo.setValue("NL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Northwest Territories");
        	ddvo.setValue("NT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Nova Scotia");
        	ddvo.setValue("NS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Nunavut");
        	ddvo.setValue("NU");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Ontario");
        	ddvo.setValue("ON");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Prince Edward Island");
        	ddvo.setValue("PE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Quebec");
        	ddvo.setValue("QC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Saskatchewan");
        	ddvo.setValue("SK");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Yukon");
        	ddvo.setValue("YT");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }
    
    // List derived from PayPal payments: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_country_codes
    // We only included those without the * indicating they were not supported for payment.
    private void createCountryDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownCountryEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Default Country name to ISO-3166-1 codes selection box");
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("-SELECT-");
        	ddvo.setValue("");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("�LAND ISLANDS");
        	ddvo.setValue("AX");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Albania");
        	ddvo.setValue("AL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("American Samoa");
        	ddvo.setValue("AS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Andorra");
        	ddvo.setValue("AD");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Anguilla");
        	ddvo.setValue("AI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Antigua and Barbuda");
        	ddvo.setValue("AG");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Argentina");
        	ddvo.setValue("AR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Armenia");
        	ddvo.setValue("AM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Aruba");
        	ddvo.setValue("AW");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Australia");
        	ddvo.setValue("AU");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Austria");
        	ddvo.setValue("AT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Azerbaijan");
        	ddvo.setValue("AZ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Bahamas");
        	ddvo.setValue("BS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Bahrain");
        	ddvo.setValue("BH");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Bangladesh");
        	ddvo.setValue("BD");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Barbados");
        	ddvo.setValue("BB");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Belgium");
        	ddvo.setValue("BE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Belize");
        	ddvo.setValue("BZ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Benin");
        	ddvo.setValue("BJ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Bermuda");
        	ddvo.setValue("BM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Bhutan");
        	ddvo.setValue("BT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Bosnia-Herzegovina");
        	ddvo.setValue("BA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Botswana");
        	ddvo.setValue("BW");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Brazil");
        	ddvo.setValue("BR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Brunei Darussalam");
        	ddvo.setValue("BN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Bulgaria");
        	ddvo.setValue("BG");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Burkina Faso");
        	ddvo.setValue("BF");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Canada");
        	ddvo.setValue("CA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Cape Verde");
        	ddvo.setValue("CV");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Cayman Islands");
        	ddvo.setValue("KY");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Chile");
        	ddvo.setValue("CL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("China");
        	ddvo.setValue("CN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Cocos (Keeling) Islands");
        	ddvo.setValue("CC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Columbia");
        	ddvo.setValue("CO");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Cook Islands");
        	ddvo.setValue("CK");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Costa Rica");
        	ddvo.setValue("CR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Cyprus");
        	ddvo.setValue("CY");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Czech Republic");
        	ddvo.setValue("CZ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Denmark");
        	ddvo.setValue("DK");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Djibouti");
        	ddvo.setValue("DJ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Dominica");
        	ddvo.setValue("DM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Ecuador");
        	ddvo.setValue("EC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Egypt");
        	ddvo.setValue("EG");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("El Salvador");
        	ddvo.setValue("SV");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Estonia");
        	ddvo.setValue("EE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Falkland Islands (Malvinas)");
        	ddvo.setValue("FK");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Faroe Islands");
        	ddvo.setValue("FO");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Fiji");
        	ddvo.setValue("FJ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Finland");
        	ddvo.setValue("FI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("France");
        	ddvo.setValue("FR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("French Guiana");
        	ddvo.setValue("GF");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("French Polynesia");
        	ddvo.setValue("PF");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("French Southern Territories");
        	ddvo.setValue("TF");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Gabon");
        	ddvo.setValue("GA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Gambia");
        	ddvo.setValue("GM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Georgia");
        	ddvo.setValue("GE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Germany");
        	ddvo.setValue("DE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Ghana");
        	ddvo.setValue("GH");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Gibraltar");
        	ddvo.setValue("GI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Greece");
        	ddvo.setValue("GR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Greenland");
        	ddvo.setValue("GL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Grenada");
        	ddvo.setValue("GD");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Guadeloupe");
        	ddvo.setValue("GP");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Guam");
        	ddvo.setValue("GU");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Guernsey");
        	ddvo.setValue("GG");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Holy See (Vatican City State)");
        	ddvo.setValue("VA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Hondorus");
        	ddvo.setValue("HN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Hong Kong");
        	ddvo.setValue("HK");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Hungary");
        	ddvo.setValue("HU");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Iceland");
        	ddvo.setValue("IS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("India");
        	ddvo.setValue("IN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Indonesia");
        	ddvo.setValue("ID");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Ireland");
        	ddvo.setValue("IE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Isle of Man");
        	ddvo.setValue("IM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Israel");
        	ddvo.setValue("IL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Italy");
        	ddvo.setValue("IT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Jamaica");
        	ddvo.setValue("JM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Japan");
        	ddvo.setValue("JP");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Jersey");
        	ddvo.setValue("JE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Jordon");
        	ddvo.setValue("JO");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Kazakhstan");
        	ddvo.setValue("KZ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Kiribati");
        	ddvo.setValue("KI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Korea, Republic of");
        	ddvo.setValue("KR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Kuwait");
        	ddvo.setValue("KW");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Kyrgyzstan");
        	ddvo.setValue("KG");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Latvia");
        	ddvo.setValue("LV");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Lesotho");
        	ddvo.setValue("LS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Liechtenstein");
        	ddvo.setValue("LI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Lithuania");
        	ddvo.setValue("LT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Luxembourg");
        	ddvo.setValue("LU");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Macao");
        	ddvo.setValue("MO");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Macedonia");
        	ddvo.setValue("MK");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Madagascar");
        	ddvo.setValue("MG");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Malawi");
        	ddvo.setValue("MW");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Malaysia");
        	ddvo.setValue("MY");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Malta");
        	ddvo.setValue("MT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Marshall Islands");
        	ddvo.setValue("MH");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Martinique");
        	ddvo.setValue("MQ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Mauritania");
        	ddvo.setValue("MR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Mayotte");
        	ddvo.setValue("YT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Mexico");
        	ddvo.setValue("MX");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Micronesia, Federated States of");
        	ddvo.setValue("FM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Moldova, Republic of");
        	ddvo.setValue("MD");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Monaco");
        	ddvo.setValue("MC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Mongolia");
        	ddvo.setValue("MN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Montenegro");
        	ddvo.setValue("ME");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Montserrat");
        	ddvo.setValue("MS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Morocco");
        	ddvo.setValue("MA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Mozambique");
        	ddvo.setValue("MZ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Namibia");
        	ddvo.setValue("NA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Nauru");
        	ddvo.setValue("NR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Netherlands");
        	ddvo.setValue("NL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Netherlands Antilles");
        	ddvo.setValue("AN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("New Caledonia");
        	ddvo.setValue("NC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("New Zealand");
        	ddvo.setValue("NZ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Nicaragua");
        	ddvo.setValue("NI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Niger");
        	ddvo.setValue("NE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Niue");
        	ddvo.setValue("NU");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Norfolk Island");
        	ddvo.setValue("NF");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Northern Mariana Islands");
        	ddvo.setValue("MP");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Norway");
        	ddvo.setValue("NO");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Oman");
        	ddvo.setValue("OM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Palau");
        	ddvo.setValue("PW");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Palestine");
        	ddvo.setValue("PS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Panama");
        	ddvo.setValue("PA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Paraguay");
        	ddvo.setValue("PY");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Peru");
        	ddvo.setValue("PE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Philippines");
        	ddvo.setValue("PH");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Pitcairn");
        	ddvo.setValue("PN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Poland");
        	ddvo.setValue("PL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Portugal");
        	ddvo.setValue("PT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Puerto Rico");
        	ddvo.setValue("PR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Qatar");
        	ddvo.setValue("QA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Reunion");
        	ddvo.setValue("RE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Romania");
        	ddvo.setValue("RO");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Russian Federation");
        	ddvo.setValue("RU");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Rwanda");
        	ddvo.setValue("RW");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Saint Helena");
        	ddvo.setValue("SH");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Saint Kitts and Nevis");
        	ddvo.setValue("KN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Saint Lucia");
        	ddvo.setValue("LC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Saint Pierre and Miquelon");
        	ddvo.setValue("PM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Saint Vincent and the Grenadines");
        	ddvo.setValue("VC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Samoa");
        	ddvo.setValue("WS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("San Marino");
        	ddvo.setValue("SM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Saudi Arabia");
        	ddvo.setValue("SA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Senegal");
        	ddvo.setValue("SN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Serbia");
        	ddvo.setValue("RS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Seychelles");
        	ddvo.setValue("SC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Singapore");
        	ddvo.setValue("SG");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Slovakia");
        	ddvo.setValue("SK");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Slovenia");
        	ddvo.setValue("SI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Solomon Islands");
        	ddvo.setValue("SB");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("South Africa");
        	ddvo.setValue("ZA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("South Georgia and the South Sandwich Islands");
        	ddvo.setValue("GS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Spain");
        	ddvo.setValue("ES");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Suriname");
        	ddvo.setValue("SR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Svalbard and Jan Mayen");
        	ddvo.setValue("SJ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Swaziland");
        	ddvo.setValue("SZ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Sweden");
        	ddvo.setValue("SE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Switzerland");
        	ddvo.setValue("CH");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Taiwan, Province of China");
        	ddvo.setValue("TW");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Tanzania, United Republic of");
        	ddvo.setValue("TZ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Thailand");
        	ddvo.setValue("TH");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Timor-Leste");
        	ddvo.setValue("TL");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Togo");
        	ddvo.setValue("TG");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Tokelau");
        	ddvo.setValue("TK");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Tonga");
        	ddvo.setValue("TO");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Trinidad and Tobago");
        	ddvo.setValue("TT");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Tunisia");
        	ddvo.setValue("TN");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Turkey");
        	ddvo.setValue("TR");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Turkmenistan");
        	ddvo.setValue("TM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Turks and Caicos Islands");
        	ddvo.setValue("TC");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Tuvalu");
        	ddvo.setValue("TV");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Uganda");
        	ddvo.setValue("UG");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Ukraine");
        	ddvo.setValue("UA");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("United Arab Emirates");
        	ddvo.setValue("AE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("United Kingdom");
        	ddvo.setValue("GB");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("United States");
        	ddvo.setValue("US");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("United States Minor Outlying Islands");
        	ddvo.setValue("UM");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Uruguay");
        	ddvo.setValue("UY");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Uzbekistan");
        	ddvo.setValue("UZ");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Vanuato");
        	ddvo.setValue("VU");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Venezuela");
        	ddvo.setValue("VE");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Virgin Islands, British");
        	ddvo.setValue("VG");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Virgin Islands, U.S.");
        	ddvo.setValue("VI");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Wallis and Futuna");
        	ddvo.setValue("WF");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Western Sahara");
        	ddvo.setValue("EH");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Zambia");
        	ddvo.setValue("ZM");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }
    
    private void createPartyRenotifyTimesDropDown(Library library, User user) 
    {
        EsfName dropDownName = getApplication().getDropDownPartyRenotifyTimesEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Standard time interval units for party renotifications");
        	dd.setComments("Allows a party to be renotified periodically until they retrieve the package.");
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Never");
        	ddvo.setValue("0 day");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("4 Hours");
        	ddvo.setValue("4 hour");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("1 Day");
        	ddvo.setValue("1 day");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("2 Days");
        	ddvo.setValue("2 day");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("3 Days");
        	ddvo.setValue("3 day");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("4 Days");
        	ddvo.setValue("4 day");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("5 Days");
        	ddvo.setValue("5 day");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("6 Days");
        	ddvo.setValue("6 day");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("1 Week");
        	ddvo.setValue("7 day");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("2 Weeks");
        	ddvo.setValue("14 day");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("3 Weeks");
        	ddvo.setValue("21 day");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("1 Month");
        	ddvo.setValue("1 month");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	ddv.setAllowMultiSelection(true);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }
    
    private void createTimeIntervalUnitsDropDown(Library library, User user) 
    {
        EsfName dropDownName = getApplication().getDropDownTimeIntervalUnitsEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Standard time interval units");
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Forever");
        	ddvo.setValue("forever");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Now");
        	ddvo.setValue("now");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Minutes");
        	ddvo.setValue("minute");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Hours");
        	ddvo.setValue("hour");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Days");
        	ddvo.setValue("day");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Weekdays");
        	ddvo.setValue("weekday");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Weeks");
        	ddvo.setValue("week");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Months");
        	ddvo.setValue("month");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Years");
        	ddvo.setValue("year");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }
    
    private void createDateTimeFormatDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownDateTimeFormatEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Standard date-time formats here showing date-times for 15 February 2011, 15 seconds after 1:45 PM.");
           	dd.setComments(
        			"These are Java SimpleDateFormats, so any legal specification for a date can be used here. Typical values are:\n" +
        			"y - Year, generally yy or yyyy for 2- or 4-digit years\n" +
        			"M - Month, M and MM are 1- or 2-digit numbers 1-12, MMM is a 3-letter abbreviation and MMMM spells the month out\n" +
        			"d - day of the month 1-31\n" +
        			"D - day of the year/Julian day 1-366\n" +
        			"H - hour of day using 24-hour notation 0-23\n" +
        			"h - hour of day using 12-hour notation 0-11\n" +
        			"m - minute of the hour 0-59\n" +
        			"s - second of the minute 0-59\n" +
        			"S - millisecond of the second 0-999\n" +
        			"a - AM/PM marker\n" +
        			"z - Timezone marker\n" +
        			"Z - Timezone hours-offset marker\n"
        			);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("2011-02-15 13:45:15.249 UTC");
        	ddvo.setValue("yyyy-MM-dd HH:mm:ss.SSS z");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("2011-02-15 13:45:15 UTC");
        	ddvo.setValue("yyyy-MM-dd HH:mm:ss z");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("2011-02-15 13:45 UTC");
        	ddvo.setValue("yyyy-MM-dd HH:mm z");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("02/15/2011 13:45:15 UTC");
        	ddvo.setValue("MM/dd/yyyy HH:mm:ss z");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("02/15/2011 13:45 UTC");
        	ddvo.setValue("MM/dd/yyyy HH:mm z");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("20110215134515");
        	ddvo.setValue("yyyyMMddHHmmss");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("1102151345");
        	ddvo.setValue("yyMMddHHmm");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created drop and version: " + dd.getEsfName());
        }
    }
    
    private void createDateFormatDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownDateFormatEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Standard date formats here showing dates for 15 February 2011.");
           	dd.setComments(
        			"These are Java SimpleDateFormats, so any legal specification for a date can be used here. Typical values are:\n" +
        			"y - Year, generally yy or yyyy for 2- or 4-digit years\n" +
        			"M - Month, M and MM are 1- or 2-digit numbers 1-12, MMM is a 3-letter abbreviation and MMMM spells the month out\n" +
        			"d - day of the month 1-31. Use d'ORD' to get the ordinal day number\n" +
        			"D - day of the year/Julian day 1-366\n"
        			);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("15-Feb-2011");
        	ddvo.setValue("dd-MMM-yyyy");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("02/15/2011");
        	ddvo.setValue("MM/dd/yyyy");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("2011-02-15");
        	ddvo.setValue("yyyy-MM-dd");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("2/15/11");
        	ddvo.setValue("M/d/yy");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("February 15, 2011");
        	ddvo.setValue("MMMM d, yyyy");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Monday, February 15, 2011");
        	ddvo.setValue("EEEE, MMMM d, yyyy");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("15 February 2011");
        	ddvo.setValue("d MMMM yyyy");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("15th day of February, 2011");
        	ddvo.setValue("d'ORD day of' MMMM',' yyyy");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("02/12 (mm/yy)");
        	ddvo.setValue("MM/yy");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("02/2012 (mm/yyyy)");
        	ddvo.setValue("MM/yyyy");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("20110215 (yyyymmdd)");
        	ddvo.setValue("yyyyMMdd");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("110215 (yymmdd)");
        	ddvo.setValue("yyMMdd");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("1102 (yymm)");
        	ddvo.setValue("yyMM");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created drop and version: " + dd.getEsfName());
        }
    }
    
    private void createTimeFormatDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownTimeFormatEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Standard time formats here showing the time 15 seconds after 1:45 PM");
        	dd.setComments(
        			"These are Java SimpleDateFormats, so any legal specification for a time can be used here. Typical values are:\n" +
        			"H - hour of day using 24-hour notation 0-23\n" +
        			"h - hour of day using 12-hour notation 0-11\n" +
        			"m - minute of the hour 0-59\n" +
        			"s - second of the minute 0-59\n" +
        			"S - millisecond of the second 0-999\n" +
        			"a - AM/PM marker\n" +
        			"z - Timezone marker\n" +
        			"Z - Timezone hours-offset marker\n"
        			);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("13:45:15.249 UTC");
        	ddvo.setValue("HH:mm:ss.SSS z");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("13:45:15 UTC");
        	ddvo.setValue("HH:mm:ss z");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("13:45 UTC");
        	ddvo.setValue("HH:mm z");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("1:45:15 PM UTC");
        	ddvo.setValue("h:mm:ss a z");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("1:45 PM UTC");
        	ddvo.setValue("h:mm a z");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("13:45:15.249");
        	ddvo.setValue("HH:mm:ss.SSS");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("13:45:15");
        	ddvo.setValue("HH:mm:ss");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("13:45");
        	ddvo.setValue("HH:mm");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("1:45:15 PM");
        	ddvo.setValue("h:mm:ss a");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("1:45 PM");
        	ddvo.setValue("h:mm a");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created dropdown and version: " + dd.getEsfName());
        }
    }

    
    private void createDecimalFormatDropDown(Library library, User user) 
    {
        EsfName dropDownName = getApplication().getDropDownDecimalFormatEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Standard formats for showing a decimal number.");
           	dd.setComments(
        			"These are Java DecimalFormats, so any legal specification for a time can be used here. Typical values are:\n" +
        			"0 - represents one digit\n" +
        			"# - represents one digit, but if a leading/trailing zero, suppresses it\n" +
        			", - Comma is used for thousands separators\n" +
        			". - The decimal point\n" +
        			"% - Show the number as a percentage"
        			);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123,456,789");
        	ddvo.setValue("###,###,###,###,##0");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123,456,789.1");
        	ddvo.setValue("###,###,###,###,##0.0");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123,456,789.12");
        	ddvo.setValue("###,###,###,###,##0.00");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123,456,789.12[3]");
        	ddvo.setValue("###,###,###,###,##0.00#");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123456789");
        	ddvo.setValue("##############0");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123456789.1");
        	ddvo.setValue("##############0.0");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123456789.12");
        	ddvo.setValue("##############0.00");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123456789.12[3]");
        	ddvo.setValue("##############0.00#");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123,456,789%");
        	ddvo.setValue("###,###,###,###,##0%");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123,456,789.1%");
        	ddvo.setValue("###,###,###,###,##0.0%");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123456789%");
        	ddvo.setValue("##############0%");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123456789.1%");
        	ddvo.setValue("##############0.0%");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123456789[.12]%");
        	ddvo.setValue("##############0.##%");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created drop and version: " + dd.getEsfName());
        }
    }
    
    private void createIntegerFormatDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownIntegerFormatEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Standard formats for showing an integer number.");
           	dd.setComments(
        			"These are Java NumberFormats, so any legal specification for a time can be used here. Typical values are:\n" +
        			"0 - represents one digit\n" +
        			"# - represents one digit, but if a leading zero, suppresses it\n" +
        			", - Comma is used for thousands separators" 
        			);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123,456,789");
        	ddvo.setValue("###,###,###,###,##0");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123456789");
        	ddvo.setValue("##############0");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created drop and version: " + dd.getEsfName());
        }
    }
    
    private void createIntegerExtraOptionsDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownIntegerExtraOptionsEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Extra options for showing an integer number.");
           	dd.setComments(
        			"Values are either normal or ordinal." 
        			);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Normal (1, 2, 3, ...)");
        	ddvo.setValue("");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Ordinal (1st, 2nd, 3rd, ...)");
        	ddvo.setValue("ordinal");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created drop and version: " + dd.getEsfName());
        }
    }
    
    private void createMoneyFormatDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownMoneyFormatEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Standard formats for showing a money value.");
           	dd.setComments(
        			"These are Java DecimalFormats, so any legal specification for a time can be used here. Typical values are:\n" +
        			"'$' - Fixed dollar sign\n" +
        			"'�' - Fixed pound sign\n" +
        			"'�' - Fixed euro sign\n" +
        			"0 - represents one digit\n" +
        			"# - represents one digit, but if a leading/trailing zero, suppresses it\n" +
        			", - Comma is used for thousands separators\n" +
        			". - The decimal point"
        			);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("$123,456,789");
        	ddvo.setValue("'$'###,###,###,###,##0");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("$123,456,789.12");
        	ddvo.setValue("'$'###,###,###,###,##0.00");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("$123,456,789.12[3]");
        	ddvo.setValue("'$'###,###,###,###,##0.00#");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("$123456789");
        	ddvo.setValue("'$'##############0");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("$123456789.12");
        	ddvo.setValue("'$'##############0.00");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("$123456789.12[3]");
        	ddvo.setValue("'$'##############0.00#");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("123456789.12");
        	ddvo.setValue("##############0.00");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("�123,456,789");
        	ddvo.setValue("'�'###,###,###,###,##0");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("�123,456,789.12");
        	ddvo.setValue("'�'###,###,###,###,##0.00");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("�123,456,789");
        	ddvo.setValue("'�'###,###,###,###,##0");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("�123,456,789.12");
        	ddvo.setValue("'�'###,###,###,###,##0.00");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created drop and version: " + dd.getEsfName());
        }
    }
    
    
    private void createPayPalCardTypesDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownPayPalCardTypesEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("Credit card types suitable for PayPal APIs");
           	dd.setComments(
        			"If no credit card type is used in the PayPal NVP API, it will attempt to guess from the card number.\n\n" +
        			"PayPal may require an extra application to accept American Express cards."
        			);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Visa");
        	ddvo.setValue("Visa");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("MasterCard");
        	ddvo.setValue("MasterCard");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("American Express");
        	ddvo.setValue("Amex");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Discover");
        	ddvo.setValue("Discover");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created drop and version: " + dd.getEsfName());
        }
    }
    
    private void createStringTransformsDropDown(Library library, User user)
    {
        EsfName dropDownName = getApplication().getDropDownStringTransformsEsfName();
        DropDown dd = DropDown.Manager.getByName(library.getId(), dropDownName);
        if ( dd == null )
        {
        	dd = DropDown.Manager.createNew(library.getId());
        	DropDownVersion ddv = DropDownVersion.Manager.createTest(dd, user);
        	dd.setEsfName(dropDownName);
        	dd.setDescription("String transforms for setting a field value");
           	dd.setComments(
        			""
        			);
        	short order = 1;
        	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
        	DropDownVersionOption ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("UPPERCASE");
        	ddvo.setValue("toUpper");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("lowercase");
        	ddvo.setValue("toLower");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Capitalize first");
        	ddvo.setValue("capFirst");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Capitalize First, Lowercase Others");
        	ddvo.setValue("capFirstToLowerOthers");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Compress whitespace");
        	ddvo.setValue("compressWhitespace");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Strip whitespace");
        	ddvo.setValue("stripWhitespace");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Trim whitespace");
        	ddvo.setValue("trimWhitespace");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Numbers only");
        	ddvo.setValue("numericOnly");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Alphabetic only");
        	ddvo.setValue("alphaOnly");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("Alphanumeric only");
        	ddvo.setValue("alphaNumericOnly");
        	optionList.add(ddvo);
        	ddvo = DropDownVersionOption.Manager.createNew(ddv, order++);
        	ddvo.setOption("First character only");
        	ddvo.setValue("firstCharOnly");
        	optionList.add(ddvo);

        	ddv.setOptions(optionList);
        	dd.promoteTestVersionToProduction();
        	dd.save(user);
        	ddv.save(user);
        	display("Created drop and version: " + dd.getEsfName());
        }
    }
    
    public void createInitialEmailTemplates(User user, Library companyLibrary)
	{
		Library templateLibrary = Library.Manager.getTemplate();
		createSetPasswordEmailTemplate(templateLibrary,user);
		createForgotPasswordEmailTemplate(templateLibrary,user);
		createPasswordChangedEmailTemplate(templateLibrary,user);
		createPasswordLockoutEmailTemplate(templateLibrary,user);

		createDefaultPickupNotificationEmailTemplate(templateLibrary,user,getApplication().getEmailTemplateDefaultPickupNotificationEsfName(),true);
        EsfName companyEmailTemplateName = new EsfName("PickupNotification");
		createDefaultPickupNotificationEmailTemplate(companyLibrary,user,companyEmailTemplateName,false);
	}

    private void createSetPasswordEmailTemplate(Library library, User user)
    {
        EsfName emailTemplateName = getApplication().getEmailTemplateSetPasswordEsfName();
        EmailTemplate et = EmailTemplate.Manager.getByName(library.getId(), emailTemplateName);
        if ( et == null )
        {
        	et = EmailTemplate.Manager.createNew(library.getId());
        	EmailTemplateVersion etv = EmailTemplateVersion.Manager.createTest(et, user);
        	et.setEsfName(emailTemplateName);
        	et.setDescription("Sends the password reset email");
        	et.setComments(
"${EMAIL} is the user's email address.\n" +
"${NAME} is the user's name\n" +
"${LINK} is the unique link for this request to set a password.\n" +
"${PICKUPCODE} is the unique pickup code for this request\n" +
"${CONTEXTPATH} is the base URL for the web site\n");
        	etv.setEmailFrom("${property:ESF.SendEmailFrom}");
        	etv.setEmailSubject("Set a ${property:MyCompany.InformalName} Open eSignForms password for ${NAME}");
        	etv.setEmailTo("${EMAIL}");
        	etv.setEmailText("Dear ${NAME},\n\n" +
"You are requested to set a password for your ${property:MyCompany.InformalName} Open eSignForms account ${EMAIL}.  To set your password, please click:\n\n" +
"${LINK}\n\n" +
"If the above link is not active (e.g. nothing happens when you click on it), please copy it exactly as shown into your web browser's address or location field.\n\n" +
"Thank you.\n" +
"${property:MyCompany.FormalName}\n" +
"${property:MyCompany.MailingAddressStreetAddress}\n" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}\n" +
"${property:MyCompany.UserSupportEmail}\n");
        	etv.setEmailHtml(
"<style type=\"text/css\"> body.esf div.pagediv { max-width: 800px; padding-left: 1ex; padding-right: 1ex; margin-left: auto; margin-right: auto; } </style>" +
"<p style=\"margin-left: 25px;\">${image:LogoForEmail}</p>" +
"<div style=\"background-color: #2691ce; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px; font-family: Georgia, serif;\">Set your password...</h2></div>" +
"<div style=\"padding-left: 1em; padding-right: 1em;\">" +
"<p>Dear ${NAME},</p>" +
"<p>You are requested to set a password for your ${property:MyCompany.InformalName} Open eSignForms account <b>${EMAIL}</b>.</p>" +
"<h2><a href=\"${LINK}\">Click here to set your password</a></h2>" +
"<p>Thank you.<br />" +
"${property:MyCompany.FormalName}<br />" +
"${property:MyCompany.MailingAddressStreetAddress}<br />" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}<br />" +
"<a href=\"mailto:${property:MyCompany.UserSupportEmail}\">${property:MyCompany.UserSupportEmail}</a></p>" +
"<div style=\"margin-top: 1ex; padding: 5px; background-color: #eceeee; border-radius: 5px;\">" +
"<p style=\"font-size: 10px;\">If the above link is not active (e.g. nothing happens when you click on it), please copy the link below exactly as shown into your web browser&#39;s address or location field:</p>" +
"<p style=\"font-size: 10px;\">${LINK}</p></div></div>");
        	et.promoteTestVersionToProduction();
        	et.save(user);
        	etv.save(user);
        	display("Created email template and version: " + et.getEsfName());
        }
    }

    
    private void createForgotPasswordEmailTemplate(Library library, User user) 
    {
        EsfName emailTemplateName = getApplication().getEmailTemplateForgotPasswordEsfName();
        EmailTemplate et = EmailTemplate.Manager.getByName(library.getId(), emailTemplateName);
        if ( et == null )
        {
        	et = EmailTemplate.Manager.createNew(library.getId());
        	EmailTemplateVersion etv = EmailTemplateVersion.Manager.createTest(et, user);
        	et.setEsfName(emailTemplateName);
        	et.setDescription("Sends the forgot password reset email so the user can try to reset his own password.");
        	et.setComments(
"${EMAIL} is the user's email address.\n" +
"${NAME} is the user's name\n" +
"${LINK} is the unique link for this request to reset a password using the forgotten password Q&A.\n" +
"${PICKUPCODE} is the unique pickup code for this request\n" +
"${CONTEXTPATH} is the base URL for the web site\n");
        	etv.setEmailFrom("${property:ESF.SendEmailFrom}");
        	etv.setEmailSubject("Reset forgotten ${property:MyCompany.InformalName} Open eSignForms password for ${NAME}");
        	etv.setEmailTo("${EMAIL}");
        	etv.setEmailText("Dear ${NAME},\n\n" +
"You have requested to reset your forgotten password for your ${property:MyCompany.InformalName} Open eSignForms account ${EMAIL}.  To reset your password, please click:\n\n" +
"${LINK}\n\n" +
"If the above link is not active (e.g. nothing happens when you click on it), please copy it exactly as shown into your web browser's address or location field.\n\n" +
"Thank you.\n" +
"${property:MyCompany.FormalName}\n" +
"${property:MyCompany.MailingAddressStreetAddress}\n" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}\n" +
"${property:MyCompany.UserSupportEmail}\n");
        	etv.setEmailHtml(
"<style type=\"text/css\"> body.esf div.pagediv { max-width: 800px; padding-left: 1ex; padding-right: 1ex; margin-left: auto; margin-right: auto; } </style>" +
"<p style=\"margin-left: 25px;\">${image:LogoForEmail}</p>" +
"<div style=\"background-color: #fda514; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px; font-family: Georgia, serif;\">Set your password...</h2></div>" +
"<div style=\"padding-left: 1em; padding-right: 1em;\">" +
"<p>Dear ${NAME},</p>" +
"<p>You have requested to reset your forgotten password for your ${property:MyCompany.InformalName} Open eSignForms account <b>${EMAIL}</b>.</p>" +
"<h2><a href=\"${LINK}\">Click here to reset your forgotten password</a></h2>" +
"<p>Thank you.<br />" +
"${property:MyCompany.FormalName}<br />" +
"${property:MyCompany.MailingAddressStreetAddress}<br />" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}<br />" +
"<a href=\"mailto:${property:MyCompany.UserSupportEmail}\">${property:MyCompany.UserSupportEmail}</a></p>" +
"<div style=\"margin-top: 1ex; padding: 5px; background-color: #eceeee; border-radius: 5px;\">" +
"<p style=\"font-size: 10px;\">If the above link is not active (e.g. nothing happens when you click on it), please copy the link below exactly as shown into your web browser&#39;s address or location field:</p>" +
"<p style=\"font-size: 10px;\">${LINK}</p></div></div>");
        	et.promoteTestVersionToProduction();
        	et.save(user);
        	etv.save(user);
        	display("Created email template and version: " + et.getEsfName());
        }
    }

    
    private void createPasswordChangedEmailTemplate(Library library, User user)
    {
        EsfName emailTemplateName = getApplication().getEmailTemplatePasswordChangedEsfName();
        EmailTemplate et = EmailTemplate.Manager.getByName(library.getId(), emailTemplateName);
        if ( et == null )
        {
        	et = EmailTemplate.Manager.createNew(library.getId());
        	EmailTemplateVersion etv = EmailTemplateVersion.Manager.createTest(et, user);
        	et.setEsfName(emailTemplateName);
        	et.setDescription("The notice that the user's password has been changed.");
        	et.setComments(
"${EMAIL} is the user's email address.\n" +
"${NAME} is the user's name\n" +
"${CONTEXTPATH} is the base URL for the web site\n");
        	etv.setEmailFrom("${property:ESF.SendEmailFrom}");
        	etv.setEmailSubject("The ${property:MyCompany.InformalName} Open eSignForms password for ${NAME} has been changed");
        	etv.setEmailTo("${EMAIL}");
        	etv.setEmailText("Dear ${NAME},\n\n" +
"The password for your ${property:MyCompany.InformalName} Open eSignForms account ${EMAIL} has been changed.  " + 
"If you did not change your password, please contact your system administrator.\n\n" +
"Thank you.\n" +
"${property:MyCompany.FormalName}\n" +
"${property:MyCompany.MailingAddressStreetAddress}\n" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}\n" +
"${property:MyCompany.UserSupportEmail}\n");
        	etv.setEmailHtml(
"<style type=\"text/css\"> body.esf div.pagediv { max-width: 800px; padding-left: 1ex; padding-right: 1ex; margin-left: auto; margin-right: auto; } </style>" +
"<p style=\"margin-left: 25px;\">${image:LogoForEmail}</p>" +
"<div style=\"background-color: #fda514; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px; font-family: Georgia, serif;\">Your password has been changed...</h2></div>" +
"<div style=\"padding-left: 1em; padding-right: 1em;\">" +
"<p>Dear ${NAME},</p>" +
"<p>The password for your ${property:MyCompany.InformalName} Open eSignForms account <b>${EMAIL}</b> has been changed. If you did not change your password, please contact your system administrator.</p>" +
"<p>Thank you.<br />" +
"${property:MyCompany.FormalName}<br />" +
"${property:MyCompany.MailingAddressStreetAddress}<br />" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}<br />" +
"<a href=\"mailto:${property:MyCompany.UserSupportEmail}\">${property:MyCompany.UserSupportEmail}</a></p>" +
"</div>");
        	et.promoteTestVersionToProduction();
        	et.save(user);
        	etv.save(user);
        	display("Created email template and version: " + et.getEsfName());
        }
    }

    
    private void createPasswordLockoutEmailTemplate(Library library, User user) 
    {
        EsfName emailTemplateName = getApplication().getEmailTemplatePasswordLockoutEsfName();
        EmailTemplate et = EmailTemplate.Manager.getByName(library.getId(), emailTemplateName);
        if ( et == null )
        {
        	et = EmailTemplate.Manager.createNew(library.getId());
        	EmailTemplateVersion etv = EmailTemplateVersion.Manager.createTest(et, user);
        	et.setEsfName(emailTemplateName);
        	et.setDescription("Sends the password locked out notification.");
        	et.setComments(
"${EMAIL} is the user's email address.\n" +
"${NAME} is the user's name\n" +
"${LINK} is the link to request a forgotten password.\n" +
"${CONTEXTPATH} is the base URL for the web site\n");
        	etv.setEmailFrom("${property:ESF.SendEmailFrom}");
        	etv.setEmailSubject("${property:MyCompany.InformalName} Open eSignForms password failures for ${NAME}");
        	etv.setEmailTo("${EMAIL}");
        	etv.setEmailText("Dear ${NAME},\n\n" +
"There have been multiple, failed login attempts on your ${property:MyCompany.InformalName} Open eSignForms account ${EMAIL}. " + 
"If you were not having trouble, you may want to contact your system administrator.  If you have forgotten your password, you may unlock your own password by clicking below:\n\n" +
"${LINK}\n\n" +
"If the above link is not active (e.g. nothing happens when you click on it), please copy it exactly as shown into your web browser's address or location field.\n\n" +
"Thank you.\n" +
"${property:MyCompany.FormalName}\n" +
"${property:MyCompany.MailingAddressStreetAddress}\n" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}\n" +
"${property:MyCompany.UserSupportEmail}\n");
        	etv.setEmailHtml(
"<style type=\"text/css\"> body.esf div.pagediv { max-width: 800px; padding-left: 1ex; padding-right: 1ex; margin-left: auto; margin-right: auto; } </style>" +
"<p style=\"margin-left: 25px;\">${image:LogoForEmail}</p>" +
"<div style=\"background-color: #fda514; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px; font-family: Georgia, serif;\">Multiple failed login attempts...</h2></div>" +
"<div style=\"padding-left: 1em; padding-right: 1em;\">" +
"<p>Dear ${NAME},</p>" +
"<p>There have been multiple, failed login attempts on your ${property:MyCompany.InformalName} Open eSignForms account <b>${EMAIL}</b>. " +
"If you were not having trouble, you may want to contact your system administrator.  If you have forgotten your password, you may unlock your own password by clicking below:</p>" +
"<h2><a href=\"${LINK}\">Click here to reset your forgotten password</a></h2>" +
"<p>Thank you.<br />" +
"${property:MyCompany.FormalName}<br />" +
"${property:MyCompany.MailingAddressStreetAddress}<br />" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}<br />" +
"<a href=\"mailto:${property:MyCompany.UserSupportEmail}\">${property:MyCompany.UserSupportEmail}</a></p>" +
"<div style=\"margin-top: 1ex; padding: 5px; background-color: #eceeee; border-radius: 5px;\">" +
"<p style=\"font-size: 10px;\">If the above link is not active (e.g. nothing happens when you click on it), please copy the link below exactly as shown into your web browser&#39;s address or location field:</p>" +
"<p style=\"font-size: 10px;\">${LINK}</p></div></div>");
        	et.promoteTestVersionToProduction();
        	et.save(user);
        	etv.save(user);
        	display("Created email template and version: " + et.getEsfName());
        }
    }

    
    private void createDefaultPickupNotificationEmailTemplate(Library library, User user, EsfName emailTemplateName, boolean makeProduction)
    {
        EmailTemplate et = EmailTemplate.Manager.getByName(library.getId(), emailTemplateName);
        if ( et == null )
        {
        	et = EmailTemplate.Manager.createNew(library.getId());
        	EmailTemplateVersion etv = EmailTemplateVersion.Manager.createTest(et, user);
        	et.setEsfName(emailTemplateName);
        	et.setDescription("This is the default pickup notification.");
        	et.setComments(
"This is the default pickup notification sent to parties that become active. It is used if a party is defined without a notification and there's no Group pickup permission set for access via To Do.\n" +
"${EMAIL} is the user's email address.\n" +
"${LINK} is the unique pickup link for this transaction party.\n" +
"${PICKUPCODE} is the unique pickup code for this request\n" +
"${CONTEXTPATH} is the base URL for the web site\n");
        	etv.setEmailFrom("${property:ESF.SendEmailFrom}");
        	etv.setEmailSubject("${property:MyCompany.InformalName} has a document for you");
        	etv.setEmailTo("${EMAIL}");
        	etv.setEmailText("An electronic document has been created for you to process on the Open eSignForms system for ${property:MyCompany.InformalName}.\n\n" +
"To view and/or process this document, please click:\n\n" +
"${LINK}\n\n" +
"Our system will guide you through this process so it will only take a few minutes.\n\n" +
"If the above link is not active (e.g. nothing happens when you click on it), please copy it exactly as shown into your web browser's address or location field.\n\n" +
"Thank you.\n" +
"${property:MyCompany.FormalName}\n" +
"${property:MyCompany.MailingAddressStreetAddress}\n" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}\n" +
"${property:MyCompany.UserSupportEmail}\n");
        	etv.setEmailHtml(
"<style type=\"text/css\"> body.esf div.pagediv { max-width: 800px; padding-left: 1ex; padding-right: 1ex; margin-left: auto; margin-right: auto; } </style>" +
"<p style=\"margin-left: 25px;\">${image:LogoForEmail}</p>" +
"<div style=\"background-color: #2691ce; border-top-left-radius: 2em; border-bottom-left-radius: 2em;\">" +
"<h2 style=\"color: white; padding: 15px 0px 15px 30px; font-family: Georgia, serif;\">An electronic document needs your attention...</h2></div>" +
"<div style=\"padding-left: 1em; padding-right: 1em;\">" +
"<p>An electronic document has been created for you to process on the Open eSignForms system for ${property:MyCompany.InformalName}.</p>" +
"<h2><a href=\"${LINK}\">Click here to view and/or process this document</a></h2>" +
"<p>Our system will guide you through this process so it will only take a few minutes.</p>" +
"<p>Thank you.<br />" +
"${property:MyCompany.FormalName}<br />" +
"${property:MyCompany.MailingAddressStreetAddress}<br />" +
"${property:MyCompany.MailingAddressCity}, ${property:MyCompany.MailingAddressState} ${property:MyCompany.MailingAddressZipCode}<br />" +
"<a href=\"mailto:${property:MyCompany.UserSupportEmail}\">${property:MyCompany.UserSupportEmail}</a></p>" +
"<div style=\"margin-top: 1ex; padding: 5px; background-color: #eceeee; border-radius: 5px;\">" +
"<p style=\"font-size: 10px;\">If the above link is not active (e.g. nothing happens when you click on it), please copy the link below exactly as shown into your web browser&#39;s address or location field:</p>" +
"<p style=\"font-size: 10px;\">${LINK}</p></div></div>");
        	if ( makeProduction )
        		et.promoteTestVersionToProduction();
        	et.save(user);
        	etv.save(user);
        	display("Created email template and version: " + et.getEsfName());
        }
    }

    
    
    
    public void createInitialPropertySets(User user, Library companyLibrary, String companyName, String companyStreet, String companyCity, String companyState, String companyZip, String companyPhone, String defaultEmail)
	{
		Library templateLibrary = Library.Manager.getTemplate();
		createESFPropertySet(templateLibrary,user,companyName,defaultEmail);
		createMyCompanyPropertySet(templateLibrary,user,false,companyName,companyStreet,companyCity,companyState,companyZip,companyPhone,defaultEmail); // we'll create a template library version for password resets and such
		createMyCompanyPropertySet(companyLibrary,user,false,companyName,companyStreet,companyCity,companyState,companyZip,companyPhone,defaultEmail); // we'll create one to be tweaked for this particular library
		createPayPalNvpApiPropertySet(companyLibrary,user);
	}

    private void createESFPropertySet(Library library, User user, String companyName, String defaultEmail)
    {
        EsfName propertySetName = getApplication().getPropertySetESFEsfName();
        PropertySet ps = PropertySet.Manager.getByName(library.getId(), propertySetName);
        if ( ps == null )
        {
        	ps = PropertySet.Manager.createNew(library.getId());
        	PropertySetVersion psv = PropertySetVersion.Manager.createTest(ps, user);
        	ps.setEsfName(propertySetName);
        	ps.setDescription("Common information for Open eSignForms.");
        	ps.setComments(
        			"Used to substitute common platform information into various emails, etc."
        			);

        	Record properties = psv.getProperties();
        	
        	NameValue nv = new NameValue( new EsfName("SendEmailFrom"), new EsfString(companyName+" NO-REPLY <"+defaultEmail+">") );
            nv.setComment("A default email address to use for sending out password resets and other such platform emails unrelated to transactions.");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("LoginPageAllowEmbedded"), new EsfString("false") );
            nv.setComment("If true, the login page won't force itself to be the top page, thus allowing it to be embedded, but at the cost of security since the login page won't show the URL and SSL cert status and can introduce spoofing hacks. Recommend this be false.");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("LoginPageTitle"), new EsfString("Open eSignForms for ${property:MyCompany.InformalName} - Login") );
            nv.setComment("The page title of the login page.");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("LoginWelcomeHeader"), new EsfString("Login to Open eSignForms for ${property:MyCompany.InformalName}") );
            nv.setComment("The login page welcome header.");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("AppTitleInHtml"), new EsfString("<div style=\"color: #105aa5; font-size: 12pt; font-weight: bold; text-align: center; width: 100%\">${property:MyCompany.InformalName}</div>") );
            nv.setComment("Allows HTML syntax - make sure it's correct!");
        	properties.addUpdate( nv );

        	psv.setProperties(properties);
        	ps.promoteTestVersionToProduction();
        	ps.save(user);
        	psv.save(user);
        	display("Created propertyset and version: " + ps.getEsfName());
        }
    }

    private void createMyCompanyPropertySet(Library library, User user, boolean promoteToProduction, String companyName, String companyStreet, String companyCity, String companyState, String companyZip, String companyPhone, String defaultEmail) 
    {
        EsfName propertySetName = getApplication().getPropertySetMyCompanyEsfName();
        PropertySet ps = PropertySet.Manager.getByName(library.getId(), propertySetName);
        if ( ps == null )
        {
        	ps = PropertySet.Manager.createNew(library.getId());
        	PropertySetVersion psv = PropertySetVersion.Manager.createTest(ps, user);
        	ps.setEsfName(propertySetName);
        	ps.setDescription("Common information about the main company " + companyName);
        	ps.setComments(
        			"Used to substitute common company information into various documents, emails, etc."
        			);

        	Record properties = psv.getProperties();
        	
        	// We'll set up the defaults to be Yozons-general to match drop down lists, but since we run multiple OpenESF webapps in the same Tomcat JVM,
        	// we don't use Java's defaults in our code because each webapp would override the defaults of the other if set in the JVM.
        	NameValue nv = new NameValue( new EsfName("CustomerSupportEmail"), new EsfString(defaultEmail) );
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("CustomerSupportPhone"), new EsfString(companyPhone) );
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("FormalName"), new EsfString(companyName) );
        	nv.setComment("The longer, more full name of your company.");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("GeneralEmail"), new EsfString(defaultEmail) );
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("GeneralFaxPhone"), new EsfString("") );
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("GeneralPhone"), new EsfString(companyPhone) );
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("InformalName"), new EsfString(companyName) );
        	nv.setComment("The short, friendly, commonly used name for your company.");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("MailingAddressCity"), new EsfString(companyCity) );
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("MailingAddressState"), new EsfString(companyState) );
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("MailingAddressStreetAddress"), new EsfString(companyStreet) );
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("MailingAddressZipCode"), new EsfString(companyZip) );
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("SendEmailFrom"), new EsfString(companyName+" <"+defaultEmail+">") );
           	nv.setComment("A common FROM display name and email address that can be used for configuring emails for this company.");
        	properties.addUpdate( nv );

           	nv = new NameValue( new EsfName("UserSupportEmail"), new EsfString(defaultEmail) );
           	nv.setComment("The email address for users of this system who need support. Often used in emails for password resets, etc.");
        	properties.addUpdate( nv );

           	nv = new NameValue( new EsfName("WebSiteName"), new EsfString("Example.com") );
           	nv.setComment("The name of your company's web site, typically, often shorter than the actual URL, often without the http:// prefix.");
        	properties.addUpdate( nv );

           	nv = new NameValue( new EsfName("WebSiteURL"), new EsfString("http://www.example.com") );
           	nv.setComment("The URL that takes people to your company web site.");
        	properties.addUpdate( nv );

        	psv.setProperties(properties);
        	if ( promoteToProduction )
        		ps.promoteTestVersionToProduction();
        	ps.save(user);
        	psv.save(user);
        	display("Created propertyset and version: " + ps.getEsfName());
        }
    }

    /* Commented out since this mechanism was removed and is no longer available. ButtonMessage is the replacement mechanism.
    @Deprecated
    private void createEsfButtonsPropertySet(Library library, User user, boolean promoteToProduction) 
    {
        EsfName propertySetName = getApplication().getPropertySetEsfButtonsEsfName();
        PropertySet ps = PropertySet.Manager.getByName(library.getId(), propertySetName);
        if ( ps == null )
        {
        	ps = PropertySet.Manager.createNew(library.getId());
        	PropertySetVersion psv = PropertySetVersion.Manager.createTest(ps, user);
        	ps.setEsfName(propertySetName);
        	ps.setDescription("ESF standard button configuration options");
        	ps.setComments(
        			"Allows you to customize the package and document footers and buttons.\n\n" +
        			"Button property sets are searched in this order:\n" +
        			"1) Check for a propertyset named ESF_Buttons_DocumentEsfName_DocPartyEsfName (Will use DocPartyEsfName \"ViewOnly\" if a view only party to the document.)\n" +
        			"2) Check for a propertyset named ESF_Buttons_DocumentEsfName\n" +
        			"3) Check for a propertyset named ESF_Buttons_PickupPartyEsfName (package party EsfName)\n" +
        			"4) Lastly, check for a propertyset named ESF_Buttons (one should always be found in the template library)\n\n" +
        			"General search order is the transaction's branding library, document's library, then the template library."
        			);

        	Record properties = psv.getProperties();
        	
        	NameValue nv = new NameValue( app.getEsfButtons_PackageFooterHtmlPathName(), new EsfString("<p class=\"f8 gray\">* Bookmark or copy this page location to return later: ${LINK}</p>"));
        	nv.setComment("HTML to show at the bottom of the package. ${LINK} will be replaced with the page's URL/link.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentFooterHtmlPathName(), new EsfString("") );
        	nv.setComment("HTML to show at the bottom of all document pages. ${LINK} will be replaced with the page's URL/link.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_PackageButtonContinueToDoPathName(), new EsfString("Continue") );
        	nv.setComment("Button on package when there's work to do.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_PackageButtonContinueToDoTitlePathName(), new EsfString("Please click to continue to review or process the document(s) in this package.") );
        	nv.setComment("Title/tip for this button");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_PackageButtonContinueDonePathName(), new EsfString("View first document") );
        	nv.setComment("Button on package when it's completed.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_PackageButtonContinueDoneTitlePathName(), new EsfString("Please click to continue to view the completed document(s) in this package.") );
        	nv.setComment("Title/tip for this button");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_PackageButtonDeletePathName(), new EsfString("Delete this transaction") );
        	nv.setComment("Button on package when party is allowed to delete the package.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_PackageButtonDeleteTitlePathName(), new EsfString("If you click this button you will delete this transaction and related package of documents and data. It cannot be recovered.") );
        	nv.setComment("Title/tip for this button");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_PackageButtonNotCompletingPathName(), new EsfString("Go back (not completing now)") );
        	nv.setComment("Button on package when party has an URL to return to if they don't complete it. Allows them to return saying they have not signed.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_PackageButtonNotCompletingTitlePathName(), new EsfString("Click this button to go back and indicate that you are not going to complete the documents at this time.") );
        	nv.setComment("Title/tip for this button");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_PackageButtonDownloadMyDocsAsPdfPathName(), new EsfString("Download my document(s) as a PDF") );
        	nv.setComment("Button on package when party is completed allowing them to download their documents as a PDF file.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_PackageButtonDownloadMyDocsAsPdfTitlePathName(), new EsfString("Click this button to download your completed document(s) as a digitally signed PDF file.") );
        	nv.setComment("Title/tip for this button");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentButtonGoToPackagePathName(), new EsfString("Return to the document list") );
        	nv.setComment("Button on document to return to the package list of all documents.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentEditButtonContinueNextPagePathName(), new EsfString("Continue to next page") );
        	nv.setComment("Button on a document when there's another page to work on.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentEditButtonContinueToReviewPathName(), new EsfString("Continue to review") );
        	nv.setComment("Button on a document when going to the final review.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentEditButtonPreviousPagePathName(), new EsfString("Go back to previous page") );
        	nv.setComment("Button on a document when there's a previous page to work on.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentReviewViewOnlyButtonContinueNextPagePathName(), new EsfString("Continue") );
        	nv.setComment("Button on a document in review mode for a view-only party when there's a next page.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentReviewViewOnlyButtonCompletePathName(), new EsfString("Continue") );
        	nv.setComment("Button on a document in review mode for a view-only party when will complete it.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentReviewViewOnlyButtonPreviousPagePathName(), new EsfString("Go back to previous page") );
        	nv.setComment("Button on a document in review mode for a view-only party when a previous page exists.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentReviewButtonCompleteSignerPathName(), new EsfString("Submit signed document") );
        	nv.setComment("Button on a document in review mode when completing/submitting and the party is a signer.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentReviewButtonCompleteNotSignerPathName(), new EsfString("Submit completed document") );
        	nv.setComment("Button on a document in review mode when completing/submitting and the party is not a signer.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentReviewButtonReturnToEditPathName(), new EsfString("Go back to make changes") );
        	nv.setComment("Button on a document in review mode to return to make changes back in edit mode.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentViewOnlyButtonNextPagePathName(), new EsfString("Continue to next page") );
        	nv.setComment("Button on document in view only mode with a next page to view.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentViewOnlyButtonPreviousPagePathName(), new EsfString("Go back to previous page") );
        	nv.setComment("Button on document in view only mode with a previous page to view.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentViewOnlyButtonNextDocumentPathName(), new EsfString("Go to next document") );
        	nv.setComment("Button on document in view only mode with a next document to view.");
        	properties.addUpdate( nv );

        	nv = new NameValue( app.getEsfButtons_DocumentViewOnlyButtonPreviousDocumentPathName(), new EsfString("Go to previous document") );
        	nv.setComment("Button on document in view only mode with a previous document to view.");
        	properties.addUpdate( nv );

        	psv.setProperties(properties);
        	if ( promoteToProduction )
        		ps.promoteTestVersionToProduction();
        	ps.save(user);
        	psv.save(user);
        	display("Created propertyset and version: " + ps.getEsfName());
        }
    }
    */

    private void createPayPalNvpApiPropertySet(Library library, User user) 
    {
        EsfName propertySetName = NvpDirectPayment.PROPSET_PAYPAL_NVP_API;
        PropertySet ps = PropertySet.Manager.getByName(library.getId(), propertySetName);
        if ( ps == null )
        {
        	ps = PropertySet.Manager.createNew(library.getId());
        	PropertySetVersion psv = PropertySetVersion.Manager.createTest(ps, user);
        	ps.setEsfName(propertySetName);
        	ps.setDescription("Configuration for the PayPal NVP API");
        	ps.setComments(
        			"Used to connect to the PayPal NVP API for doing credit card payments for \"PayPal Web Site Pro\" customers.\n\n" +
        			"For transactions in TEST mode, the _TEST suffix properties will be used so only the PayPal Sandbox is touched.\n\n" +
        			"After an attempt is made, the action will set the document fields PAYPAL_ACK, PAYPAL_TRANSACTIONID (set on a successful payment), PAYPAL_AMT, PAYPAL_AVSCODE, PAYPAL_AVSCODE_MEANING (extra field explaining the AVSCODE), PAYPAL_CVV2MATCH, PAYPAL_CVV2MATCH_MEANING (extra field explaining the CVV2MATCH field) and PAYPAL_EXCEPTION (extra field set on a failure). " + 
        			"These contain the values of the returned fields of the same name (minus the prefix) by the API. " +
        			"You may want to consider creating the PAYPAL_TRANSACTIONID field in your document as a General type field if you'd like to use it elsewhere in custom logic (i.e. ${PAYPAL_TRANSACTIONID} is set on a successful payment)."
        			);

        	Record properties = psv.getProperties();
        	
        	NameValue nv = new NameValue( new EsfName("LogNVP"), new EsfString("false") );
        	nv.setComment("Defaults to false. Set to true to log the NVP API calls to the web application log. Use cautiously as the credit card information and passwords will be logged.");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("LogNVP_TEST"), new EsfString("true") );
        	nv.setComment("Defaults to false. Set to true to log the NVP API calls when in test mode.");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("PWD"), new EsfString("(put your production PayPal NVP API PASSWORD here)") );
        	nv.setComment("Production NVP API password");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("PWD_TEST"), new EsfString("(put your sandbox/test PayPal NVP API PASSWORD here)") );
        	nv.setComment("Sandbox/test NVP API password");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("SIGNATURE"), new EsfString("(put your production PayPal NVP API SIGNATURE here)") );
        	nv.setComment("Production NVP SIGNATURE");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("SIGNATURE_TEST"), new EsfString("(put your sandbox/test PayPal NVP API SIGNATURE here)") );
        	nv.setComment("Sandbox/test NVP SIGNATURE");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("URL"), new EsfString("https://api-3t.paypal.com/nvp") );
        	nv.setComment("Production URL. Used for any production-mode transaction.");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("URL_TEST"), new EsfString("https://api-3t.sandbox.paypal.com/nvp") );
        	nv.setComment("The sandbox URL. Used for any test-mode transaction.");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("USER"), new EsfString("(put your production PayPal NVP API USER here)") );
        	nv.setComment("Production NVP USER");
        	properties.addUpdate( nv );

        	nv = new NameValue( new EsfName("USER_TEST"), new EsfString("(put your sandbox/test PayPal NVP API USER here)") );
        	nv.setComment("Sandbox/test NVP USER");
        	properties.addUpdate( nv );

        	psv.setProperties(properties);
        	//ps.promoteTestVersionToProduction(); -- We don't put into production since it's not usable until the actual values are set.
        	ps.save(user);
        	psv.save(user);
        	display("Created propertyset and version: " + ps.getEsfName());
        }
    }

    public void initSetup(EsfInteger licenseSize, String companyName, String companyStreet, String companyCity, String companyState, String companyZip, String companyPhone, 
    					  String companyGroupName, String defaultEmail, String programmerEmail)
    {
        ConnectionPool    pool = app.getConnectionPool();
        Connection        con  = pool.getConnection();
        PreparedStatement stmt  = null;
        
    	Group superGroup = Group.Manager.getSuperGroup();
    	Group systemAdminGroup = Group.Manager.getSystemAdminGroup();
    	User superUser = getASuperUser();
    	
    	EsfPathName companyGroupPathName = new EsfPathName(companyGroupName);
    	EsfPathName companyProgrammingGroupPathName = new EsfPathName(companyGroupName+"/Programming");
    	EsfPathName companyLibPathName = new EsfPathName("Lib/"+companyGroupName);

    	try
        {
    		// Create a group for members of the company, but they can only list/view the group and its members
        	Group companyGroup = Group.Manager.createLike(systemAdminGroup, companyGroupPathName, superUser);
        	companyGroup.setDescription("Contains members of " + companyName);
        	companyGroup.removeMemberUsersPermissionAllowedGroups(PermissionOption.PERM_OPTION_CREATELIKE, companyGroup);
        	companyGroup.removeMemberUsersPermissionAllowedGroups(PermissionOption.PERM_OPTION_UPDATE, companyGroup);
        	companyGroup.removeMemberUsersPermissionAllowedGroups(PermissionOption.PERM_OPTION_DELETE, companyGroup);
        	companyGroup.removePermissionAllowedGroups(PermissionOption.PERM_OPTION_CREATELIKE, companyGroup);
        	companyGroup.removePermissionAllowedGroups(PermissionOption.PERM_OPTION_UPDATE, companyGroup);
        	companyGroup.removePermissionAllowedGroups(PermissionOption.PERM_OPTION_DELETE, companyGroup);
        	companyGroup.save(con);
        	display("Created company group: " + companyGroup);

    		// Create a group for programmers of the company, but they can only list/view the group and its members
        	Group companyProgrammingGroup = Group.Manager.createLike(systemAdminGroup, companyProgrammingGroupPathName, superUser);
        	companyProgrammingGroup.setDescription("Contains members who do programming for " + companyName);
        	companyProgrammingGroup.removeMemberUsersPermissionAllowedGroups(PermissionOption.PERM_OPTION_CREATELIKE, companyProgrammingGroup);
        	companyProgrammingGroup.removeMemberUsersPermissionAllowedGroups(PermissionOption.PERM_OPTION_UPDATE, companyProgrammingGroup);
        	companyProgrammingGroup.removeMemberUsersPermissionAllowedGroups(PermissionOption.PERM_OPTION_DELETE, companyProgrammingGroup);
        	companyProgrammingGroup.removePermissionAllowedGroups(PermissionOption.PERM_OPTION_CREATELIKE, companyProgrammingGroup);
        	companyProgrammingGroup.removePermissionAllowedGroups(PermissionOption.PERM_OPTION_UPDATE, companyProgrammingGroup);
        	companyProgrammingGroup.removePermissionAllowedGroups(PermissionOption.PERM_OPTION_DELETE, companyProgrammingGroup);
        	companyProgrammingGroup.save(con);
        	display("Created company programming group: " + companyGroup);

        	// Add the programming group to the UIs programmers needs, and the company to list/view a few others
        	Permission p = Permission.Manager.getByPathName(UI.UI_PERM_LIBRARY_VIEW);
        	p.addGroupToAllOptions(companyProgrammingGroup); 
        	p.save(con);
        	p = Permission.Manager.getByPathName(UI.UI_PERM_PACKAGE_VIEW);
        	p.addGroupToAllOptions(companyProgrammingGroup);
        	p.save(con);
        	p = Permission.Manager.getByPathName(UI.UI_PERM_REPORT_TEMPLATE_VIEW);
        	p.addGroupToAllOptions(companyProgrammingGroup);
        	p.save(con);
        	p = Permission.Manager.getByPathName(UI.UI_PERM_TRANSACTION_TEMPLATE_VIEW);
        	p.addGroupToAllOptions(companyProgrammingGroup);
        	p.save(con);
        	p = Permission.Manager.getByPathName(UI.UI_PERM_TRANSACTION_LISTING_VIEW);
        	p.addGroupToOptions(companyProgrammingGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
        	p.addGroupToOptions(companyGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
        	p.save(con);
        	p = Permission.Manager.getByPathName(UI.UI_PERM_TODO_LISTING_VIEW);
        	p.addGroupToOptions(companyProgrammingGroup, PermissionOption.PERM_OPTION_LIST);
        	p.addGroupToOptions(companyGroup, PermissionOption.PERM_OPTION_LIST);
        	p.save(con);
        	display("Added company programming group to Library, Package, Transaction Template, Transaction Listing and To Do views");
        	
        	// Add the programming group to list/view the template library
        	Library templateLibrary = Library.Manager.getTemplate();
        	templateLibrary.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_LIST, companyProgrammingGroup);
        	templateLibrary.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_VIEWDETAILS, companyProgrammingGroup);
        	templateLibrary.save(con);
        	display("Added company groups to list/view the template library");
        	
        	Library companyLibrary = Library.Manager._dbsetup_createSample(superGroup,systemAdminGroup,companyLibPathName);
        	companyLibrary.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_LIST, companyProgrammingGroup);
        	companyLibrary.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_LIST, companyGroup);
        	companyLibrary.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_VIEWDETAILS, companyProgrammingGroup);
        	companyLibrary.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_VIEWDETAILS, companyGroup);
        	companyLibrary.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_CREATELIKE, companyProgrammingGroup);
        	companyLibrary.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_UPDATE, companyProgrammingGroup);
        	companyLibrary.addPermissionAllowedGroups(PermissionOption.PERM_OPTION_DELETE, companyProgrammingGroup);
        	companyLibrary.save(con);
        	display("Created sample company library: " + companyLibrary.getPathName());

        	// Update the deployment property defaults
        	Record deployProps = app.getDeploymentProperties();
        	deployProps.addUpdate(new NameValue( app.getDefaultEmailFromEsfName(), new EsfString(defaultEmail) ) );
        	if ( licenseSize.isGreaterThan(0) )
        	{
            	deployProps.addUpdate( new NameValue( app.getLicenseTypeEsfName(), Application.LICENSE_TYPE_COMMERCIAL_DBSIZE ) ); 
            	deployProps.addUpdate( new NameValue( app.getLicenseSizeEsfName(), licenseSize ) );
            	display("*** Updated deployment properties for " + licenseSize + "MB Commercial DBSIZE license. ***");
        	}
        	deployProps.save(con);

        	con.commit(); // commit what we have now so that the next routines will work as expected
        	
            // Configure the initial button message set if not already there
            createInitialButtonMessageSet(superUser);
            // Configure the initial document style if not already there
            createInitialDocumentStyle(superUser,companyLibrary);
            // Configure the initial package document (must be done after the document style is created)
            createInitialPackageDocument(superUser);
            // Configure the initial template images
            createInitialImages(superUser);
            // Configure the initial (password) email templates
            createInitialEmailTemplates(superUser,companyLibrary);
        	// Configure some default drop down lists if not already present using this super user as the "creator"
            createInitialDropDowns(superUser);
        	// Configure some default property sets if not already present using this super user as the "creator"
            createInitialPropertySets(superUser, companyLibrary, companyName, companyStreet, companyCity, companyState, companyZip, companyPhone, defaultEmail);
            // Configure the initial package template if not already there
            createTemplatePackage(superUser);
            // Create the company package template
            Package companyTemplatePackage = createCompanyTemplatePackage(superUser,companyProgrammingGroup,companyName);
        	// Configure the template transaction templates
            createEsfTemplateTransactionTemplate(superUser);
            createCompanyTemplateTransactionTemplate(superUser,companyGroup,companyProgrammingGroup,companyLibrary,companyTemplatePackage);
            // Configure the template report template and the built-in fields
            createEsfTemplateReportTemplate(superUser,companyGroup,companyProgrammingGroup);
            createBuiltInReportFieldTemplates();
            
            // Create our programming user if requested
            if ( programmerEmail != null )
            {
            	String firstName = programmerEmail.equalsIgnoreCase("open-esign@yozons.com") ? "Yozons" : "AGPL";
                User programmingUser = User.Manager.createNewUserLike(superUser, programmerEmail, firstName, "Open eSignForms Programming", superUser);
                programmingUser.save(con,superUser);
                superGroup.removeMemberUser(programmingUser);
                superGroup.save(con);
                systemAdminGroup.removeMemberUser(programmingUser);
                systemAdminGroup.save(con);
                companyProgrammingGroup.addMemberUser(programmingUser);
                companyProgrammingGroup.save(con);
                programmingUser.save(con);
                String msg = "DbSetup added programming user: " + programmingUser.getFullDisplayName() + "; id: " + programmingUser.getId();
                getApplication().getActivityLog().logSystemUserActivity(msg);
                programmingUser.logConfigChange(msg);
                display("Created programming user: " + programmingUser.getFullDisplayName());
            }
            else
                display("Skipped creating a programming user account as requested");

            con.commit();
        }
        catch(SQLException e) 
        {
            display("initSetup - SQL Error: " + e.getMessage());
            app.sqlerr(e,"ERROR: initSetup()");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            app.cleanupPool(pool,con,stmt);
        }
    }
    
    private boolean ensureSystemBooted(java.io.BufferedReader in)
    	throws Exception
    {
    	if ( ! app.isSystemBooted() )
    	{
    		String pass1 = getFromUser(in,"Enter boot password 1",null);
    		String pass2 = getFromUser(in,"Enter boot password 2",null);
    		
    		app.setBootPassword1(pass1);
    		app.setBootPassword2(pass2);
    		if ( app.bootSystem() )
    		{
    			display("ensureSystemBooted - booted system");
    		}
    		else
    			display("ensureSystemBooted - failed to boot system");
    	}
    	
    	return app.isSystemBooted();
    }

    private boolean loadDeploymentProperties()
		throws Exception
	{
    	EsfUUID deploymentPropertiesId = app.getDeploymentPropertiesId();
    	if ( deploymentPropertiesId == null || deploymentPropertiesId.isNull() )
    		return app.loadDeploymentProperties();
    	return true;
	}
    
    
	
	/* 
    ================================================================================ 
    The following routines are just here to support command-line prompting.
    ================================================================================ 
    */

/*
	private static boolean getTrueFalseFromUser(java.io.BufferedReader in, String prompt)
	    throws Exception
	{
	    return getTrueFalseFromUser(in,prompt,false);
	}

	private static boolean getTrueFalseFromUser(java.io.BufferedReader in,String prompt,boolean def)
	    throws Exception
	{
	    String ans = getFromUser(in,prompt,(def)?"true":"false");
	    if ( ans.equalsIgnoreCase("true") ||
	         ans.equalsIgnoreCase("t")    ||
	         ans.equalsIgnoreCase("yes")  ||
	         ans.equalsIgnoreCase("y")
	       )
	        return true;
	        
	    return false;
	}
	
	private static int getIntFromUser(java.io.BufferedReader in,String prompt,int def)
	    throws Exception
	{
	    String ans = getFromUser(in,prompt,Integer.toString(def));
	    return DataTypes.getInstance().stringToInt(ans,def);
	}
	
	private static int getIntFromUser(java.io.BufferedReader in,String prompt)
	    throws Exception
	{
	    String ans = getFromUser(in,prompt);
	    return DataTypes.getInstance().stringToInt(ans,0);
	}
	
	private static long getLongFromUser(java.io.BufferedReader in,String prompt,long def)
	    throws Exception
	{
	    String ans = getFromUser(in,prompt,Long.toString(def));
	    return DataTypes.getInstance().stringToLong(ans,def);
	}
	
	private static long getLongFromUser(java.io.BufferedReader in,String prompt)
	    throws Exception
	{
	    String ans = getFromUser(in,prompt);
	    return DataTypes.getInstance().stringToLong(ans,0);
	}
	
	private static String getFromUser(java.io.BufferedReader in,String prompt)
	    throws Exception
	{
	    return getFromUser(in,prompt,null);
	}
*/	
	private static String getFromUser(java.io.BufferedReader in,String prompt,String def)
	    throws Exception
	{
	    try
	    {
	        String ans = null;
	            
	        while( ans == null || ans.length() < 1 )
	        {
	            System.out.print(prompt);
	            if ( def != null )
	                System.out.print(" [" + def + "] ");
	            System.out.print(": ");
	                
	            ans = in.readLine();
	
	            if ( (ans == null || ans.length() < 1) && def != null )
	            {
	                ans = def;
	                break;
	            }
	        }
	        return ans;
	    }
	    catch( java.io.IOException e )
	    {
	        System.out.println("I/O Exception: " + e.getMessage());
	        throw e;
	    }
	}
	
	private static void doCommandInitDb(DbSetup dbsetup, java.io.BufferedReader in)
		throws Exception
	{
		EsfUUID deployId = dbsetup.insertDeployment();
		
		String pass1 = getFromUser(in,"Enter boot password 1",null);
		String pass2 = getFromUser(in,"Enter boot password 2",null);
		
        dbsetup.insertBootKey(deployId,pass1,pass2);
        
    	Group superGroup = Group.Manager._dbsetup_createSuperGroup();
    	if ( superGroup == null ) {
    		throw new Exception("Failed to create super group");
    	}
    	if ( ! superGroup.save() ) 
    		throw new Exception("Failed to save the super group");
    	dbsetup.display("Added super group: " + superGroup.getPathName());
        
    	Group systemAdminGroup = Group.Manager._dbsetup_createSystemAdminGroup();
    	if ( systemAdminGroup == null ) {
    		throw new Exception("Failed to create system admin group");
    	}
    	if ( ! systemAdminGroup.save() ) 
    		throw new Exception("Failed to save the system admin group");
    	dbsetup.display("Added system admin group: " + systemAdminGroup.getPathName());
        
    	Group allUsersGroup = Group.Manager._dbsetup_createAllUsersGroup();
    	if ( allUsersGroup == null ) {
    		throw new Exception("Failed to create all users pseudo-group");
    	}
    	if ( ! allUsersGroup.save() ) 
    		throw new Exception("Failed to save the all users pseudo-group");
    	dbsetup.display("Added All Users pseudo-group: " + allUsersGroup.getPathName());
        
    	Group externalUsersGroup = Group.Manager._dbsetup_createExternalUsersGroup(allUsersGroup);
    	if ( externalUsersGroup == null ) {
    		throw new Exception("Failed to create external users pseudo-group");
    	}
    	if ( ! externalUsersGroup.save() ) 
    		throw new Exception("Failed to save the external users pseudo-group");
    	dbsetup.display("Added External Users pseudo-group: " + externalUsersGroup.getPathName());
    	
        dbsetup.createInitialProperties(deployId, superGroup);
        
        dbsetup.createInitialPermissions(systemAdminGroup);
        
    	Library templateLibrary = Library.Manager._dbsetup_createTemplate(superGroup,systemAdminGroup);
    	if ( templateLibrary == null ) {
    		throw new Exception("Failed to create the template library");
    	}
    	if ( ! templateLibrary.save() ) 
    		throw new Exception("Failed to save the template library");
    	dbsetup.display("Added template library: " + templateLibrary.getPathName());
        
    	// Let's do this after we've got our properties in place so that our keypairs will have information based on 
    	// our installation.
    	if ( ! dbsetup.ensureSystemBooted(in) )
    	{
        	dbsetup.display("FATAL: Failed to boot system during initdb and before creating the required signature keys.");
    		return;
    	}
    	
    	if ( ! dbsetup.loadDeploymentProperties() )
    	{
        	dbsetup.display("FATAL: Failed to load deployment properties during initdb and before creating the required signature keys.");
    		return;
    	}
    	
        dbsetup.insertSignatureKeyPair(deployId);
        
        dbsetup.getApplication().getActivityLog().logSystemVersionChange("DbSetup initialized the database for " + Version.getReleaseString() + 
        		" with deployment id " + deployId);
 	}
    
	
    private static void doCommandAddSuperUser(DbSetup dbsetup, java.io.BufferedReader in)
    	throws Exception
    {
    	if ( ! dbsetup.ensureSystemBooted(in) )
    		return;
    	
    	if ( ! dbsetup.loadDeploymentProperties() )
    		return;
    	
    	String email = getFromUser(in,"Enter super user's email address","support@yozons.com");
        while( ! EsfEmailAddress.isValidEmail(email) )
        {
            System.out.println("ERROR: Invalid email address: '" + email + "'; please try again.");
            email = getFromUser(in,"Enter super user's email address","support@yozons.com");
        }
        String firstName= getFromUser(in,"Enter super user's first/personal name","Yozons");
        String lastName = getFromUser(in,"Enter super user's last/family name","Support");
        String password = getFromUser(in,"Initial super user password",null);

        User user = dbsetup.insertSuperUser(email,firstName,lastName,password);
        if ( user != null )
        {
        	String msg = "DbSetup added super user with email: " + email + "; name: " + firstName + " " + lastName + "; id: " + user.getId();
            dbsetup.getApplication().getActivityLog().logSystemConfigChange(msg);
            dbsetup.getApplication().getActivityLog().logSystemUserActivity(msg);
            user.logConfigChange(msg);
            user.logSecurity(msg);
            
            Group group = dbsetup.insertUserIntoSuperGroup(user);
            if ( group == null )
            	System.out.println("The super group could not be saved with the super user added into it.");
        }
    }

    // While available to anybody, this does a basic new setup as preferred by Yozons
	private static void doCommandInitSetup(DbSetup dbsetup, java.io.BufferedReader in)
		throws Exception
	{
    	if ( ! dbsetup.ensureSystemBooted(in) )
    		return;
    	
    	if ( ! dbsetup.loadDeploymentProperties() )
    		return;
    	
		EsfInteger licenseSize = null;
		while( licenseSize == null )
		{
			String v = getFromUser(in,"Enter Commercial DB license size in MB (enter 0 for AGPL deployment)","0");
			licenseSize = new EsfInteger(v);
			if ( licenseSize.isNull() )
			{
				System.out.println("That's not a valid DB size. Enter 0 for AGPL, or a Yozons commercial license size in MB.");
				licenseSize = null;
			}
			else if ( licenseSize.isNegative() )
				licenseSize.setValue(0);
		}
		
		String companyName = getFromUser(in,"Enter company name","Demo Corporation");
		String companyStreet = getFromUser(in,"Enter company street address","1234 Main Ave.");
		String companyCity = getFromUser(in,"Enter company city","Anytown");
		String companyState = getFromUser(in,"Enter company state","WA");
		String companyZip = getFromUser(in,"Enter company zip","98123");
		String companyPhone = getFromUser(in,"Enter company default phone number","800.555.1212");

		String companyGroupEsfName = null;
		while( companyGroupEsfName == null )
		{
			companyGroupEsfName = getFromUser(in,"Enter company group EsfName","DemoCo");
			if ( ! EsfName.isValidEsfName(companyGroupEsfName) )
			{
				System.out.println("That's not a valid ESF Name: " + companyGroupEsfName);
				companyGroupEsfName = null;
			}
		}
		
		String defaultEmail = null;
		while( defaultEmail == null )
		{
			defaultEmail = getFromUser(in,"Enter company default email address","rename@democo.com");
			if ( ! EsfEmailAddress.isValidEmail(defaultEmail) )
			{
				System.out.println("That's not a valid email address: " + defaultEmail);
				defaultEmail = null;
			}
		}
		
		String programmerEmail = null;
		while( programmerEmail == null )
		{
			programmerEmail = getFromUser(in,"Enter initial programmer user's email address","open-esign@yozons.com");
			if ( EsfString.isBlank(programmerEmail) )
			{
				programmerEmail = null;
				break;
			}
			if ( ! EsfEmailAddress.isValidEmail(programmerEmail) )
			{
				System.out.println("That's not a valid email address: " + programmerEmail);
				programmerEmail = null;
			}
		}
		
	    dbsetup.initSetup(licenseSize,companyName,companyStreet,companyCity,companyState,companyZip,companyPhone,companyGroupEsfName,defaultEmail,programmerEmail);
	}


    private static void doCommandSetPassword(DbSetup dbsetup, java.io.BufferedReader in)
    throws Exception
    {
    	if ( ! dbsetup.ensureSystemBooted(in) )
    		return;
    	
        String email = getFromUser(in,"CHANGE PASSWORD - Enter user's email address",null);
        while( ! EsfEmailAddress.isValidEmail(email) )
        {
            System.out.println("ERROR: Invalid email address: '" + email + "'; please try again.");
            email = getFromUser(in,"Enter user's email address",null);
        }

        String password = getFromUser(in,"New password",null);

        User user = dbsetup.setPassword(email,password);
        if ( user != null )
        {
        	String msg = "DbSetup set new password for user: " + user.getFullDisplayName() + "; id: " + user.getId();
            dbsetup.getApplication().getActivityLog().logSystemUserActivity(msg);
            user.logSecurity(msg);
        }
    }

    private static void doCommandCreateX509Certificate(DbSetup dbsetup, java.io.BufferedReader in)
    	throws Exception
    {
    	if ( ! dbsetup.ensureSystemBooted(in) )
    		return;
    	
    	if ( ! dbsetup.loadDeploymentProperties() )
    		return;
    	
    	dbsetup.createX509Certificate();
    }

	private static void doCommandConvertForPriorRelease(DbSetup dbsetup, java.io.BufferedReader in)
		throws Exception
	{
		if ( ! dbsetup.ensureSystemBooted(in) )
			return;
		
		if ( ! dbsetup.loadDeploymentProperties() )
			return;
		
		dbsetup.convert15_8_23();
	
		dbsetup.getApplication().getActivityLog().logSystemVersionChange("DbSetup updated database for release prior to " + Version.getReleaseString());
	}

	private static void doCommandConvertForRelease(DbSetup dbsetup, java.io.BufferedReader in)
		throws Exception
	{
		if ( ! dbsetup.ensureSystemBooted(in) )
			return;
		
		if ( ! dbsetup.loadDeploymentProperties() )
			return;
		
		dbsetup.convert16_1_16();
	
		dbsetup.getApplication().getActivityLog().logSystemVersionChange("DbSetup updated database for " + Version.getReleaseString());
	}

    private static void processCommand(DbSetup dbsetup, java.io.BufferedReader in, String command)
    	throws Exception
    {
    	if ( command.equalsIgnoreCase("initdb") )
    	{
    		doCommandInitDb(dbsetup,in);
    	}
        else if ( command.equalsIgnoreCase("addsuperuser") )
        {
            doCommandAddSuperUser(dbsetup,in);
        }
        else if ( command.equalsIgnoreCase("initsetup") )
        {
            doCommandInitSetup(dbsetup,in);
        }
        else if ( command.equalsIgnoreCase("setpassword") )
        {
            doCommandSetPassword(dbsetup,in);
        }
        else if ( command.equalsIgnoreCase("createX509Cert") )
        {
        	doCommandCreateX509Certificate(dbsetup,in);
        }
        else if ( command.equalsIgnoreCase("convert15.8.23") )
        {
        	doCommandConvertForPriorRelease(dbsetup,in);
        }
        else if ( command.equalsIgnoreCase("convert16.1.16") )
        {
        	doCommandConvertForRelease(dbsetup,in);
        }
    	else
    	{
    		System.out.println("Error: Unrecognized command: " + command);
    	}
    }
    
    public static void main(String[] args)
        throws Exception
    {
    	if ( args.length != 1 )
    	{
    		System.err.println("Usage: java " + DbSetup.class.getName() + " <contextPath>");
    		System.err.println("     where <contextPath> is the webapp context path, such as / or /demo");
    		System.exit(1);
    	}
    	
    	String contextPath = args[0];
    	
        DbSetup dbsetup = null;
    	
        System.out.println(Version.getTextCopyright());
        System.out.println("DbSetup - Sets up the database for " + Version.getReleaseString());

        java.io.BufferedReader in = new java.io.BufferedReader( new java.io.InputStreamReader(System.in) );

        String command = null;
        
        while( ! "quit".equalsIgnoreCase(command) )
        {
        	System.out.println();
        	command = getFromUser(in,"Enter setup command (initdb,addsuperuser,initsetup,setpassword,createX509Cert,quit,convert15.8.23,convert16.1.16)","quit");
        	if ( ! command.equalsIgnoreCase("quit") )
        	{
            	if ( dbsetup == null )
                    dbsetup = new DbSetup(contextPath);
            	processCommand(dbsetup,in,command);
        	}        	
        }
        
        if ( dbsetup != null )
            dbsetup.destroy();
        
        System.exit(0);
    }
    
}