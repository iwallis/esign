// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.db;


/**
 * Screwy wrapper class so that we can insert a Blob into the database from a byte array.
 * Includes more screwy stuff for Oracle specific updating of a blob (the only way to insert a new blob).
 *
 * @author Yozons, Inc.
 */
public class ByteBlob 
    implements java.sql.Blob
{
    byte[] bytes = null;
    
    /**
     * Creates a ByteBlob using the specified byte array.
     */
    public ByteBlob(byte[] b)
    {
        bytes = b;
    }
    
    // My own constructor for taking a Blob of input and returning as an array
    public ByteBlob(java.sql.Blob b)
    {
        java.io.InputStream is = null;
        try
        {
            is = b.getBinaryStream();
            bytes = new byte[(int)b.length()];
            
            int numRead;
            int bytesOffset = 0;
			int bytesToRead = (int)b.length();
			while( bytesToRead > 0 && (numRead = is.read(bytes,bytesOffset,bytesToRead)) > 0 ) 
			{
				bytesOffset += numRead;
				bytesToRead -= numRead;
			}
        }
        catch( java.sql.SQLException e ) 
        {
            bytes = null;
        }
        catch( java.io.IOException e ) 
        {
            bytes = null;
        }
        finally
        {
            try
            {
                if ( is != null )
                    is.close();
            }
            catch( java.io.IOException e ) {}
        }
    }
    
    public long length()
        throws java.sql.SQLException
    {
        return bytes.length;
    }
    
    // My own API call for simplicity
    public byte[] getBytes()
    {
        return bytes;
    }
    
    public byte[] getBytes(long pos, int length)
        throws java.sql.SQLException
    {
        if ( pos == 0 && length == bytes.length )
            return bytes;
            
        try
        {
            byte[] newbytes = new byte[length];
            System.arraycopy(bytes, (int)pos, newbytes, 0, length);
            return newbytes;
        }
        catch( Exception e )
        {
            throw new java.sql.SQLException("Could not get subset of array for ByteBlob");
        }
    }
    
    public java.io.InputStream getBinaryStream()
        throws java.sql.SQLException
    {
        return new java.io.ByteArrayInputStream(bytes);
    }
    
    public java.io.InputStream getBinaryStream(long pos, long length)
    	throws java.sql.SQLException
	{
    	return new java.io.ByteArrayInputStream(getBytes(pos,(int)length));
	}

	public int setBytes(long pos,
						byte[] bytes,
						int offset,
						int len)
				 throws java.sql.SQLException
	 {
		throw new java.sql.SQLException("Unsupported setBytes() for ByteBlob");
	 }

	public int setBytes(long pos,
						byte[] bytes)
				 throws java.sql.SQLException
	{
	   throw new java.sql.SQLException("Unsupported setBytes() for ByteBlob");
	}
				 
	public java.io.OutputStream setBinaryStream(long pos)
		throws java.sql.SQLException
	{
		throw new java.sql.SQLException("Unsupported setBinaryStream() for ByteBlob");
	}
    
	public void truncate(long len)
			 throws java.sql.SQLException
	{
   		throw new java.sql.SQLException("Unsupported truncate() for ByteBlob");
	}
				  
    public long position(byte[] pattern, long start)
        throws java.sql.SQLException
    {
        throw new java.sql.SQLException("Unsupported position() for ByteBlob");
    }
    
    public long position(java.sql.Blob pattern, long start)
        throws java.sql.SQLException
    {
        throw new java.sql.SQLException("Unsupported position() for ByteBlob");
    }
    
    
    /**
     * Routine used to put the "real" object into an Oracle database, which requires
     * creating an empty blob, then retrieving it again and updating it from there.
     */
    /*
    public void updateOracleBlob(java.sql.Blob b)
        throws java.sql.SQLException
    {
        java.io.OutputStream outstream = null;
        
        try
        {
            if ( b == null )
                throw new SQLException("ByteBlob.updateOracleBlob() blob was null");
            if ( ! (b instanceof oracle.sql.BLOB) )
                throw new SQLException("ByteBlob.updateOracleBlob() blob not an oracle.sql.BLOB object; is: " +
                                       b.getClass().getName() );
            if ( bytes == null )
                throw new SQLException("ByteBlob.updateOracleBlob() no blob bytes to write");
                
            oracle.sql.BLOB blob = (oracle.sql.BLOB)b;
            outstream            = blob.getBinaryOutputStream();
        
            int bufSize   = blob.getBufferSize();
            int pos       = 0;
            int remaining = bytes.length;
            while ( remaining > 0 )
            {
                int numOut = Math.min(bufSize,remaining);
                outstream.write(bytes, pos, numOut);
                pos       += numOut;
                remaining -= numOut;
            }
        }
        catch( java.io.IOException e )
        {
            throw new java.sql.SQLException("ByteBlob.updateOracleBlob() I/O failure: " + e.getMessage());
        }
        finally
        {
            try 
            {
                if ( outstream != null )
                    outstream.close();
            }
            catch( java.io.IOException e ) 
            {
                throw new java.sql.SQLException("ByteBlob.updateOracleBlob() close I/O failure: " + e.getMessage());
            }
        }
     }
     */
    
    public void free()
    {
    	bytes = null;
    }
}