// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.user;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.Application;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.util.PathNameUUIDUserListableCache;

/**
* Permission describes a single "feature" and isn't secured independently.
* Security is set via permission options that can independently be assigned to one or more groups.
* 
* In order to prevent permissions from being easily tricked by updating the database directly, Permissions are
* generally stored in encrypted Records (like the DeploymentProperties for built-in system feature permissions, 
* Group for permissions assigned to Groups, etc.).
* 
* @author Yozons, Inc.
*/

public class Permission
	extends com.esignforms.open.db.DatabaseObject
	implements java.lang.Comparable<Permission>, PathNameUUIDUserListableCache.PathNameUUIDUserListableCacheable, java.io.Serializable
{
	private static final long serialVersionUID = 8468535495896336865L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(Permission.class);
	
    protected final EsfUUID id;
    protected EsfPathName pathName;
    protected EsfPathName originalPathName; // derived, not persisted
    protected String status;
    protected String description;
    protected String comments;
    protected LinkedList<PermissionOption> options;
    
    private Permission(EsfUUID id, EsfPathName pathName, String status, String description, String comments)
    {
    	this.id = id;
    	this.pathName = pathName;
    	this.originalPathName = pathName.duplicate();
    	this.status = status;
    	this.description = description;
    	this.comments = comments;
    	this.options = new LinkedList<PermissionOption>();
    }
    
    public Permission duplicate()
    {
    	Permission p = new Permission(id, pathName.duplicate(), status, description, comments);
    	p.options = getDuplicatePermissionOptions();
    	p.setDatabaseObjectLike(this);
    	return p;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public EsfPathName getPathName()
    {
        return pathName;
    }
    public void setPathName(EsfPathName v) {
    	if ( v != null && v.isValid() ) {
    		pathName = v;
    		objectChanged();
    	}
    }
    public EsfPathName getOriginalPathName()
    {
        return originalPathName;
    }
    public void resetOriginalPathName()
    {
    	originalPathName = pathName.duplicate();
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }
    
    public void makeEnabled()
    {
        status = Literals.STATUS_ENABLED;
        objectChanged();
    }
     
    public void makeDisabled()
    {
        status = Literals.STATUS_DISABLED;
        objectChanged();
    }
    
    public void setStatus(String s)
    {
        if ( Literals.STATUS_ENABLED.equals(s) )
            makeEnabled();
        else
            makeDisabled();
    }
    
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String v)
    {
        if ( v != null && v.length() > Literals.DESCRIPTION_MAX_LENGTH )
        	description = v.substring(0,Literals.DESCRIPTION_MAX_LENGTH);
        else
        	description = v;
        objectChanged();
    }
    
    public LinkedList<PermissionOption> getDuplicatePermissionOptions()
    {
    	LinkedList<PermissionOption> list = new LinkedList<PermissionOption>();
    	for( PermissionOption option : options )
    	{
    		list.add(option.duplicate());
    	}
    	return list;
    }
    
    public boolean hasPermission(User user, EsfName optionName)
    {
    	if ( isDisabled() || optionName == null )
    		return false;
    	PermissionOption option = getOptionByName(optionName);
    	return option != null && option.hasPermission(user);
    }
    
    public Collection<Group> getAllowedGroups(User user, EsfName optionName)
    {
    	PermissionOption option = getOptionByName(optionName);
    	return option == null ? new LinkedList<Group>() : option.getAllowedGroups(user);
    }
    
    PermissionOption getOptionByName(EsfName optionName)
    {
    	for( PermissionOption option : options )
    	{
    		if ( option.getEsfName().equals(optionName) )
    			return option;
    	}
    	return null;
    }
    
    synchronized void refreshGroups()
    {
    	for( PermissionOption option : options )
    		option.refreshGroups(this);
    }
    
    public boolean hasOptionByName(EsfName optionName)
    {
    	return getOptionByName(optionName) != null;
    }
    
    public boolean createOptionByNameForSuperGroup(EsfName optionName, String description)
    {
    	if ( hasOptionByName(optionName) )
    		return false;
    	
    	PermissionOption newPermOption = PermissionOption.Manager._dbsetup_createForSuperGroup(optionName, description, Group.Manager.getSuperGroup());
    	
    	options.add(newPermOption);
    	objectChanged();
    	return true;
    }    
    public boolean _dbsetup_createOptionByNameWithNoGroups(EsfName optionName, String description)
    {
    	if ( hasOptionByName(optionName) )
    		return false;
    	
    	PermissionOption newPermOption = PermissionOption.Manager._dbsetup_createWithNoGroups(optionName, description);
    	
    	options.add(newPermOption);
    	objectChanged();
    	return true;
    }
    
    public int addGroupToOptions( Group group, EsfName requiredOptionName, EsfName... otherOptionNames )
    {
    	if ( group == null || requiredOptionName == null )
    		return 0;
    	
    	int numAdded = 0;
    	
    	PermissionOption option = getOptionByName(requiredOptionName);
    	if ( option != null && option.addGroup(group) )
    		++numAdded;

    	if ( otherOptionNames != null ) 
    	{
    		for( EsfName optionName : otherOptionNames ) 
        	{
    			option = getOptionByName(optionName);
            	if ( option != null && option.addGroup(group) )
            		++numAdded;
        	}
    	}

    	if ( numAdded > 0 )
    		objectChanged();
    	
    	return numAdded;
    }

    public boolean addGroupToAllOptions( Group group )
    {
    	if ( group == null )
    		return false;
    	for( PermissionOption option : options )
    	{
    		option.addGroup(group);
    	}
    	objectChanged();
    	return true;
    }

    public boolean addGroupToAllOptionsExceptManageToDo( Group group )
    {
    	if ( group == null )
    		return false;
    	for( PermissionOption option : options )
    	{
    		if ( ! option.getEsfName().equals(PermissionOption.GROUP_MEMBER_USER_PERM_OPTION_MANAGE_TO_DO))
    			option.addGroup(group);
    	}
    	objectChanged();
    	return true;
    }

    public int removeGroupFromOptions( Group group, EsfName requiredOptionName, EsfName... otherOptionNames )
    {
    	if ( group == null || requiredOptionName == null )
    		return 0;
    	
    	int numRemoved = 0;
    	
    	PermissionOption option = getOptionByName(requiredOptionName);
    	if ( option != null && option.removeGroup(group) )
    		++numRemoved;

    	if ( otherOptionNames != null ) 
    	{
    		for( EsfName optionName : otherOptionNames ) 
        	{
    			option = getOptionByName(optionName);
            	if ( option != null && option.removeGroup(group) )
            		++numRemoved;
        	}
    	}

    	if ( numRemoved > 0 )
    		objectChanged();
    	
    	return numRemoved;
    }

    public void removeGroupFromAllOptions( Group group )
    {
    	// We never remove the super group from any permission
    	if ( group == null || group.isSuperGroup() )
    		return;
    	for( PermissionOption option : options )
    	{
    		option.removeGroup(group);
    	}
    	objectChanged();
    }
    
    public synchronized boolean save(Connection con)
	    throws SQLException
	{
    	_logger.debug("save(con) id: " + id + "; pathName: " + pathName + "; doInsert: " + doInsert());
    	
	    clearLastSQLException();
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        if ( doInsert() )
	        {
	            stmt = new EsfPreparedStatement( con, 
	            	"INSERT INTO esf_permission (id,path_name,status,description,comments) VALUES(?,?,?,?,?)");
	            stmt.set(id);
	        	stmt.set(pathName);
	            stmt.set(status);
	            stmt.set(description);
	            stmt.set(comments);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	                _logger.warn("save(con) - Insert failed for id: " + id + "; pathName: " + pathName);
	                return false;
	            }
	            
	            for( PermissionOption option : options )
	            {
	            	option.save(con, id);
	            }
	            
	            // Now we mark this object as if it were loaded fresh from the database
	            setLoadedFromDb();
	            Manager.cache.add(this);
	            return true;
	        }
	        
	        // This must be an update request. But if it hasn't changed, we can just treat this as a null operation
	        if ( ! hasChanged() )
	            return true;
	        
	        // We assume we'll update it instead
	        stmt = new EsfPreparedStatement( con, "UPDATE esf_permission SET path_name=?,status=?,description=?,comments=? WHERE id=?");
	        stmt.set(pathName);
	        stmt.set(status);
	        stmt.set(description);
	        stmt.set(comments);
	        stmt.set(id);
	        int num = stmt.executeUpdate();
	        if ( num != 1 )
	        {
	            _logger.warn("save(con) - Update failed for id: " + id + "; pathName: " + pathName);
	        }

	        // Currently assumes that no options can be removed, only updated
            for( PermissionOption option : options )
            {
            	option.save(con, id);
            }

            // Now we mark this object as if it were loaded fresh from the database
	        setLoadedFromDb();
	        
	        Manager.cache.replace(this);
	        return true;
	    }
	    catch(SQLException e)
	    {
            _logger.sqlerr(e,"save(con) on id: " + id + "; pathName: " + pathName + "; insert: " + doInsert());
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

    public boolean save() 
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    
    public synchronized boolean delete(final Connection con)
	    throws SQLException
	{
		_logger.debug("delete(con) on id: " + id + "; pathName: " + pathName);
	    
	    clearLastSQLException();
	
	    // Let's do some referential integrity checks to be sure it's not in use 
	    if ( ! checkReferential(con) )
	        return false;
	
	    if ( doInsert() )
	    {
	    	_logger.warn("delete(con) - Ignored delete of permission that was pending an INSERT id: " + id + "; pathName: " + pathName);
	        objectDeleted();
	        return true;
	    }
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	    	// Delete the permission option groups
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_permission_option_group WHERE permission_id=?");
	        stmt.set(id);
	        int num = stmt.executeUpdate();
	        if ( num == 0 )
	        	_logger.warn("delete(con) - Failed to delete esf_permission_option_group database row to delete with permission_id: " + id);

	        // Delete the permission options
	        stmt.close();
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_permission_option WHERE permission_id=?");
	        stmt.set(id);
	        num = stmt.executeUpdate();
	        if ( num == 0 )
	        	_logger.warn("delete(con) - Failed to delete esf_permission_option database row to delete with permission_id: " + id);
	        
	        // Delete the permission itself
	        stmt.close();
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_permission WHERE id=?");
	        stmt.set(id);
	        num = stmt.executeUpdate();
	        if ( num == 0 )
	        	_logger.warn("delete(con) - Failed to find esf_permission database row to delete with id: " + id);
	        
	        objectDeleted();
	
	        return true;
	    }
	    catch(SQLException e)
	    {
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}
	
	public boolean delete()
	{
	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        boolean result = delete(con);
	        if ( result )
	        	con.commit();
	        else
	        	con.rollback();
	        return result;
	    }
	    catch(SQLException e) 
	    {
	    	_logger.sqlerr(e,"delete() on id: " + id + "; pathName: " + pathName);
	        pool.rollbackIgnoreException(con,e);
	        return false;
	    }
	    finally
	    {
	    	Application.getInstance().cleanupPool(pool,con,null);
	    }
	}
	
	protected boolean checkReferential(final Connection con)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}


    
    // Comparable
	@Override
	public int compareTo(Permission other) {
		return pathName.compareTo(other.pathName);
	}

    // PathNameUUIDUserListableCacheable
	@Override
	public boolean canUserList(User user) {
		return hasPermission(user, PermissionOption.PERM_OPTION_LIST);
	}


	public static class Manager
    {
    	private static PathNameUUIDUserListableCache<Permission> cache;
    	
    	static 
    	{
    		loadCache();
    	}
    	
    	private static void loadCache()
    	{
        	_logger.debug("Manager.loadCache()");
            
    		cache = new PathNameUUIDUserListableCache<Permission>();
    		
            ConnectionPool    pool = getConnectionPool();
            Connection        con  = pool.getConnection();
            EsfPreparedStatement stmt = null;
            try
            {
            	stmt = new EsfPreparedStatement( con, "SELECT id,path_name,status,description,comments FROM esf_permission");
                EsfResultSet rs = stmt.executeQuery();
                while( rs.next() )
                {
                	EsfUUID id = rs.getEsfUUID();
                	EsfPathName pathName = rs.getEsfPathName();
                	String status = rs.getString();
                	String description = rs.getString();
                	String comments = rs.getString();
                	
                	Permission perm = new Permission(id, pathName, status, description, comments);                	
                	PermissionOption.Manager.load(con,id,perm.options);
                	perm.setLoadedFromDb();
                	cache.add(perm);
                }
                con.commit();
                
                _logger.debug("Manager.loadCache() size: " + cache.size());
            }
            catch(SQLException e) 
            {
            	_logger.sqlerr(e,"Manager.loadCache() - clearing " + cache.size() + " entries added before exception");
                pool.rollbackIgnoreException(con,e);
                cache.clear();
            }
            finally
            {
                cleanupPool(pool,con,stmt);
            }
    	}
    	
   	    public static void reloadCache()
   	    {
   	    	cache.clear();
   	    	loadCache();
   	    }
   	    
   	    // This is called after we reload our caches and we want to be sure our permissions are using the latest
   	    // group objects from its respective cache.
   	    public static void refreshGroups()
   	    {
   	    	for( Permission p : cache.getAllById() )
   	    	{
   	    		p.refreshGroups();
   	    	}
   	    }
    	
        /**
         * Creates a new permission with optional options that is initially accessible only to the
         * SuperGroup.
         * @param pathName
         * @param description
         * @param allowedGroups
         * @param options
         */
        public static Permission create(EsfPathName pathName, String description, EsfName... optionNames)
        {
        	Permission perm = new Permission(new EsfUUID(), pathName, Literals.STATUS_ENABLED, description, null);
        	
        	if ( optionNames != null )
        	{
        		for( EsfName optionName : optionNames )
        		{
        			if ( optionName.isValid() ) {
                		PermissionOption option = PermissionOption.Manager.create(optionName, optionName + " - " + description);
                		if ( ! perm.options.contains(option) )
                			perm.options.add(option);
        			}
        		}
        	}
        	return perm;
        }

        public static Permission createLike(EsfUUID newPermId, EsfPathName pathName, Permission likePermission)
        {
        	Permission perm = new Permission(newPermId, pathName, Literals.STATUS_ENABLED, likePermission.getDescription(), null);
    		for( PermissionOption likeOption : likePermission.options )
    		{
        		PermissionOption option = PermissionOption.Manager.createLike(likeOption);
        		perm.options.add(option);
    		}
        	return perm;
        }
        public static Permission createLike(EsfPathName pathName, Permission likePermission)
        {
        	return createLike( new EsfUUID(), pathName, likePermission);
        }

        
        /**
         * Special create for bootstrapping when we need to create the super group permissions for the newly created super group.
         */
        public static Permission _dbsetup_createForSuperGroup(EsfUUID permId, EsfPathName pathName, String description, Group superGroup)
        {
        	EsfName[] optionNames = {
        							PermissionOption.PERM_OPTION_LIST,
        							PermissionOption.PERM_OPTION_VIEWDETAILS,
        							PermissionOption.PERM_OPTION_CREATELIKE,
        							PermissionOption.PERM_OPTION_UPDATE,
        							PermissionOption.PERM_OPTION_DELETE
        							};
        	Permission perm = new Permission(permId, pathName, Literals.STATUS_ENABLED, description, null);

        	if ( optionNames != null )
        	{
        		for( EsfName optionName : optionNames )
        		{
            		PermissionOption option = PermissionOption.Manager._dbsetup_createForSuperGroup(optionName, optionName + " - " + description, superGroup);
            		perm.options.add(option);
        		}
        	}
        	return perm;
        }
        public static Permission _dbsetup_createForSuperGroup(EsfPathName pathName, String description, Group superGroup)
        {
        	return _dbsetup_createForSuperGroup(new EsfUUID(), pathName, description, superGroup);
        }
        public static Permission _dbsetup_createGroupMemberUserPermissionsForSuperGroup(EsfUUID permId, EsfPathName pathName, String description, Group superGroup)
        {
        	EsfName[] optionNames = {
        							PermissionOption.PERM_OPTION_LIST,
        							PermissionOption.PERM_OPTION_VIEWDETAILS,
        							PermissionOption.PERM_OPTION_CREATELIKE,
        							PermissionOption.PERM_OPTION_UPDATE,
        							PermissionOption.PERM_OPTION_DELETE,
        							PermissionOption.GROUP_MEMBER_USER_PERM_OPTION_MANAGE_TO_DO
        							};
        	Permission perm = new Permission(permId, pathName, Literals.STATUS_ENABLED, description, null);

        	if ( optionNames != null )
        	{
        		for( EsfName optionName : optionNames )
        		{
            		PermissionOption option = PermissionOption.Manager._dbsetup_createForSuperGroup(optionName, optionName + " - " + description, superGroup);
            		perm.options.add(option);
        		}
        	}
        	return perm;
        }
        public static Permission _dbsetup_createGroupMemberUserPermissionsForSuperGroup(EsfPathName pathName, String description, Group superGroup)
        {
        	return _dbsetup_createGroupMemberUserPermissionsForSuperGroup(new EsfUUID(), pathName, description, superGroup);
        }
        public static Permission _dbsetup_createTransactionTemplateForSuperGroup(EsfUUID permId, EsfPathName pathName, String description, Group superGroup)
        {
        	EsfName[] optionNames = {
        							PermissionOption.PERM_OPTION_LIST,
        							PermissionOption.PERM_OPTION_VIEWDETAILS,
        							PermissionOption.PERM_OPTION_CREATELIKE,
        							PermissionOption.PERM_OPTION_UPDATE,
        							PermissionOption.PERM_OPTION_DELETE,
        							PermissionOption.TRAN_PERM_OPTION_START,
        							PermissionOption.TRAN_PERM_OPTION_SUSPEND,
        							PermissionOption.TRAN_PERM_OPTION_RESUME,
        							PermissionOption.TRAN_PERM_OPTION_CANCEL,
        							PermissionOption.TRAN_PERM_OPTION_REACTIVATE
        							};
        	Permission perm = new Permission(permId, pathName, Literals.STATUS_ENABLED, description, null);

        	if ( optionNames != null )
        	{
        		for( EsfName optionName : optionNames )
        		{
            		PermissionOption option = PermissionOption.Manager._dbsetup_createForSuperGroup(optionName, optionName + " - " + description, superGroup);
            		perm.options.add(option);
        		}
        	}
        	// Add these permission options, but with no default groups allowed to do them.
        	perm.options.add( PermissionOption.Manager._dbsetup_createWithNoGroups(PermissionOption.TRAN_PERM_OPTION_USE_UPDATE_API, PermissionOption.TRAN_PERM_OPTION_USE_UPDATE_API+" - "+description) );
        	perm.options.add( PermissionOption.Manager._dbsetup_createWithNoGroups(PermissionOption.TRAN_PERM_OPTION_USE_ADMIN_API, PermissionOption.TRAN_PERM_OPTION_USE_ADMIN_API+" - "+description) );
        	return perm;
        }
        public static Permission _dbsetup_createReportTemplateForSuperGroup(EsfUUID permId, EsfPathName pathName, String description, Group superGroup)
        {
        	EsfName[] optionNames = {
        							PermissionOption.PERM_OPTION_LIST,
        							PermissionOption.PERM_OPTION_VIEWDETAILS,
        							PermissionOption.PERM_OPTION_CREATELIKE,
        							PermissionOption.PERM_OPTION_UPDATE,
        							PermissionOption.PERM_OPTION_DELETE,
        							PermissionOption.REPORT_PERM_OPTION_RUN_REPORT,
        							PermissionOption.REPORT_PERM_OPTION_VIEW_STARTED_BY_EXTERNAL_USERS,
        							PermissionOption.REPORT_PERM_OPTION_VIEW_STARTED_BY_ANY_USER,
        							PermissionOption.REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO,
        							PermissionOption.REPORT_PERM_OPTION_VIEW_PRODUCTION,
        							PermissionOption.REPORT_PERM_OPTION_DOWNLOAD_CSV,
        							PermissionOption.REPORT_PERM_OPTION_DOWNLOAD_ARCHIVE,
        							PermissionOption.REPORT_PERM_OPTION_VIEW_ACTIVITY_LOG,
        							PermissionOption.REPORT_PERM_OPTION_VIEW_EMAIL_LOG,
        							PermissionOption.REPORT_PERM_OPTION_VIEW_SNAPSHOT_DATA,
        							PermissionOption.REPORT_PERM_OPTION_VIEW_SNAPSHOT_DOCUMENT,
        							PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_VIEW
        							};
        	Permission perm = new Permission(permId, pathName, Literals.STATUS_ENABLED, description, null);

        	if ( optionNames != null )
        	{
        		for( EsfName optionName : optionNames )
        		{
            		PermissionOption option = PermissionOption.Manager._dbsetup_createForSuperGroup(optionName, optionName + " - " + description, superGroup);
            		perm.options.add(option);
        		}
        	}
        	// Add these permission options, but with no default groups allowed to do them.
        	perm.options.add( PermissionOption.Manager._dbsetup_createWithNoGroups(PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE, PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE+" - " + description) );
        	return perm;
        }

        public static Permission _dbsetup_createForAllUsersGroup(EsfPathName pathName, String description, Group allUsersGroup)
        {
        	Permission perm = new Permission(new EsfUUID(), pathName, Literals.STATUS_ENABLED, description, null);

        	PermissionOption listOption = PermissionOption.Manager.create(PermissionOption.PERM_OPTION_LIST,"List - All users group");
        	listOption.addGroup(allUsersGroup);
        	perm.options.add(listOption);
        	
        	PermissionOption viewDetailsOption = PermissionOption.Manager.create(PermissionOption.PERM_OPTION_VIEWDETAILS,"View Details - All users group");
        	perm.options.add(viewDetailsOption);
        	
        	PermissionOption updateOption = PermissionOption.Manager.create(PermissionOption.PERM_OPTION_UPDATE,"Update - All users group");
        	perm.options.add(updateOption);

        	return perm;
        }
        public static Permission _dbsetup_createForExternalUsersGroup(EsfPathName pathName, String description, Group allUsersGroup)
        {
        	Permission perm = new Permission(new EsfUUID(), pathName, Literals.STATUS_ENABLED, description, null);

        	PermissionOption listOption = PermissionOption.Manager.create(PermissionOption.PERM_OPTION_LIST,"List - External users group");
        	listOption.addGroup(allUsersGroup);
        	perm.options.add(listOption);
        	
        	PermissionOption viewDetailsOption = PermissionOption.Manager.create(PermissionOption.PERM_OPTION_VIEWDETAILS,"View Details - External users group");
        	perm.options.add(viewDetailsOption);
        	
        	PermissionOption updateOption = PermissionOption.Manager.create(PermissionOption.PERM_OPTION_UPDATE,"Update - External users group");
        	perm.options.add(updateOption);

        	return perm;
        }
        
        public static Permission createWithStandardOptions(EsfPathName pathName, String description)
        {
        	return create( pathName, 
        				   description, 
        				   PermissionOption.PERM_OPTION_LIST,
        				   PermissionOption.PERM_OPTION_VIEWDETAILS,
        				   PermissionOption.PERM_OPTION_CREATELIKE,
        				   PermissionOption.PERM_OPTION_UPDATE,
        				   PermissionOption.PERM_OPTION_DELETE
        				 );
        }
        
        public static Permission createWithStandardGroupMemberOptions(EsfPathName pathName, String description)
        {
        	return create( pathName, 
        				   description, 
        				   PermissionOption.PERM_OPTION_LIST,
        				   PermissionOption.PERM_OPTION_VIEWDETAILS,
        				   PermissionOption.PERM_OPTION_CREATELIKE,
        				   PermissionOption.PERM_OPTION_UPDATE,
        				   PermissionOption.PERM_OPTION_DELETE,
        				   PermissionOption.GROUP_MEMBER_USER_PERM_OPTION_MANAGE_TO_DO
        				 );
        }
        
        public static Permission getById(EsfUUID id)
        {
        	Permission p = cache.getById(id);
        	if ( p == null )
        		_logger.debug("Manager.getById() could not find with id: " + id);
        	return p;
        }
        
        public static Permission getByPathName(EsfPathName pathName)
        {
        	Permission p = cache.getByPathName(pathName);
        	if ( p == null )
            	_logger.debug("Manager.getByPathName() could not find with pathname: " + pathName);
        	return p;
        }
        
        public static boolean hasPermission(User user, EsfPathName pathName, EsfName option)
        {
        	Permission perm = getByPathName(pathName);
        	return perm != null && perm.hasPermission(user, option);	
        }
        public static boolean hasPermission(User user, String pathName, EsfName option)
        {
        	EsfPathName path = new EsfPathName(pathName);
        	return path.isValid() && hasPermission(user, path, option);
        }
        public static boolean hasPermission(User user, String pathName, String option)
        {
        	EsfName opt = new EsfName(option);
        	return opt.isValid() && hasPermission(user, pathName, opt);
        }
        
    } // Manager

}