// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.user;

import java.util.LinkedList;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.user.Permission;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;

/**
* The UI class provides a wrapper to check on Permissions related to the UI itself.
* 
* @author Yozons, Inc.
*/

public class UI implements java.io.Serializable
{
	private static final long serialVersionUID = -7760773289869000300L;

	public static final String UI_PERM_PATH_PREFIX = EsfPathName.ESF_RESERVED_UI_PATH_PREFIX + "Perm" + EsfPathName.PATH_SEPARATOR;
	
	public static final EsfName UI_DEPLOYMENT_PROPERTIES_VIEW = new EsfName("DeploymentPropertiesView");
	public static final EsfName UI_DEPLOYMENT_STATS_VIEW = new EsfName("DeploymentStatsView");
	public static final EsfName UI_GROUP_VIEW = new EsfName("GroupView");
	public static final EsfName UI_LIBRARY_DOCUMENT_PROGRAMMING_TIPS_EDIT_VIEW = new EsfName("LibraryDocumentProgrammingTipsEditView");
	public static final EsfName UI_LIBRARY_PROGRAMMING_TIPS_EDIT_VIEW = new EsfName("LibraryProgrammingTipsEditView");
	public static final EsfName UI_LIBRARY_VIEW = new EsfName("LibraryView");
	public static final EsfName UI_PACKAGE_VIEW = new EsfName("PackageAndVersionsMainView");
	public static final EsfName UI_REPORT_TEMPLATE_VIEW = new EsfName("ReportTemplateView");
	public static final EsfName UI_SYSTEM_LOG_VIEW = new EsfName("SystemLogView");
	public static final EsfName UI_TODO_LISTING_VIEW = new EsfName("TodoListingView");
	public static final EsfName UI_TRANSACTION_CSV_START_VIEW = new EsfName("TransactionCsvStartView");
	public static final EsfName UI_TRANSACTION_LISTING_VIEW = new EsfName("TransactionListingView");
	public static final EsfName UI_TRANSACTION_TEMPLATE_VIEW = new EsfName("TransactionTemplateView");
	public static final EsfName UI_UI_VIEW = new EsfName("UIView");
	public static final EsfName UI_USER_CHANGE_PASSWORD_VIEW = new EsfName("UserChangePasswordView");
	public static final EsfName UI_USER_VIEW = new EsfName("UserView");
	public static final EsfName UI_USER_SESSION_VIEW = new EsfName("UserSessionView");

	public static final EsfPathName UI_PERM_DEPLOYMENT_PROPERTIES_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_DEPLOYMENT_PROPERTIES_VIEW);
	public static final EsfPathName UI_PERM_DEPLOYMENT_STATS_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_DEPLOYMENT_STATS_VIEW);
	public static final EsfPathName UI_PERM_GROUP_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_GROUP_VIEW);
	public static final EsfPathName UI_PERM_LIBRARY_DOCUMENT_PROGRAMMING_TIPS_EDIT_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_LIBRARY_DOCUMENT_PROGRAMMING_TIPS_EDIT_VIEW);
	public static final EsfPathName UI_PERM_LIBRARY_PROGRAMMING_TIPS_EDIT_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_LIBRARY_PROGRAMMING_TIPS_EDIT_VIEW);
	public static final EsfPathName UI_PERM_LIBRARY_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_LIBRARY_VIEW);
	public static final EsfPathName UI_PERM_PACKAGE_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_PACKAGE_VIEW);
	public static final EsfPathName UI_PERM_REPORT_TEMPLATE_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_REPORT_TEMPLATE_VIEW);
	public static final EsfPathName UI_PERM_SYSTEM_LOG_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_SYSTEM_LOG_VIEW);
	public static final EsfPathName UI_PERM_TODO_LISTING_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_TODO_LISTING_VIEW);
	public static final EsfPathName UI_PERM_TRANSACTION_CSV_START_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_TRANSACTION_CSV_START_VIEW);
	public static final EsfPathName UI_PERM_TRANSACTION_LISTING_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_TRANSACTION_LISTING_VIEW);
	public static final EsfPathName UI_PERM_TRANSACTION_TEMPLATE_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_TRANSACTION_TEMPLATE_VIEW);
	public static final EsfPathName UI_PERM_UI_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_UI_VIEW);
	public static final EsfPathName UI_PERM_USER_CHANGE_PASSWORD_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_USER_CHANGE_PASSWORD_VIEW);
	public static final EsfPathName UI_PERM_USER_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_USER_VIEW);
	public static final EsfPathName UI_PERM_USER_SESSION_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_USER_SESSION_VIEW);
	
	// For TipsView and TipsForm pages...
	public static final EsfName UI_ACCESS_CONTROL_TIPS_EDIT_VIEW = new EsfName("AccessControlTipsEditView"); 
	public static final EsfName UI_ACCESS_CONTROL_TIPS_VIEW = new EsfName("AccessControlTipsView"); 
	public static final EsfName UI_PROGRAMMING_TIPS_EDIT_VIEW = new EsfName("ProgrammingTipsEditView"); 
	public static final EsfName UI_PROGRAMMING_TIPS_VIEW = new EsfName("ProgrammingTipsView"); 
	public static final EsfName UI_REPORT_TIPS_EDIT_VIEW = new EsfName("ReportTipsEditView"); 
	public static final EsfName UI_REPORT_TIPS_VIEW = new EsfName("ReportTipsView"); 
	public static final EsfName UI_START_TRANSACTION_TIPS_EDIT_VIEW = new EsfName("StartTransactionTipsEditView"); 
	public static final EsfName UI_START_TRANSACTION_TIPS_VIEW = new EsfName("StartTransactionTipsView"); 
	public static final EsfName UI_SYSTEM_CONFIG_TIPS_EDIT_VIEW = new EsfName("SystemConfigTipsEditView"); 
	public static final EsfName UI_SYSTEM_CONFIG_TIPS_VIEW = new EsfName("SystemConfigTipsView"); 
	public static final EsfName UI_WELCOME_TIPS_EDIT_VIEW = new EsfName("WelcomeTipsEditView"); 
	public static final EsfName UI_WELCOME_TIPS_VIEW = new EsfName("WelcomeTipsView"); 
	
	public static final EsfPathName UI_PERM_ACCESS_CONTROL_TIPS_EDIT_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_ACCESS_CONTROL_TIPS_EDIT_VIEW); 
	public static final EsfPathName UI_PERM_ACCESS_CONTROL_TIPS_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_ACCESS_CONTROL_TIPS_VIEW); 
	public static final EsfPathName UI_PERM_PROGRAMMING_TIPS_EDIT_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_PROGRAMMING_TIPS_EDIT_VIEW); 
	public static final EsfPathName UI_PERM_PROGRAMMING_TIPS_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_PROGRAMMING_TIPS_VIEW); 
	public static final EsfPathName UI_PERM_REPORT_TIPS_EDIT_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_REPORT_TIPS_EDIT_VIEW); 
	public static final EsfPathName UI_PERM_REPORT_TIPS_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_REPORT_TIPS_VIEW); 
	public static final EsfPathName UI_PERM_START_TRANSACTION_TIPS_EDIT_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_START_TRANSACTION_TIPS_EDIT_VIEW); 
	public static final EsfPathName UI_PERM_START_TRANSACTION_TIPS_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_START_TRANSACTION_TIPS_VIEW); 
	public static final EsfPathName UI_PERM_SYSTEM_CONFIG_TIPS_EDIT_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_SYSTEM_CONFIG_TIPS_EDIT_VIEW); 
	public static final EsfPathName UI_PERM_SYSTEM_CONFIG_TIPS_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_SYSTEM_CONFIG_TIPS_VIEW); 
	public static final EsfPathName UI_PERM_WELCOME_TIPS_EDIT_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_WELCOME_TIPS_EDIT_VIEW); 
	public static final EsfPathName UI_PERM_WELCOME_TIPS_VIEW = new EsfPathName(UI_PERM_PATH_PREFIX + UI_WELCOME_TIPS_VIEW); 
	
	// For each view, be sure to configure the cache (pseudo-DB) that contains them

	public UI()
	{
		// Nothing special yet....really just a singleton as used so far
	}
	
    public boolean hasPermission(User user, EsfName viewName, EsfName optionName)
    {
    	Permission perm = getPermission(viewName);
    	return perm != null && perm.hasPermission(user, optionName);
    }

    public boolean canList(User user, EsfName viewName)
    {
    	return hasPermission(user,viewName,PermissionOption.PERM_OPTION_LIST);
    }
    public boolean canViewDetails(User user, EsfName viewName)
    {
    	return hasPermission(user,viewName,PermissionOption.PERM_OPTION_VIEWDETAILS);
    }
    public boolean canCreateLike(User user, EsfName viewName)
    {
    	return hasPermission(user,viewName,PermissionOption.PERM_OPTION_CREATELIKE);
    }
    public boolean canUpdate(User user, EsfName viewName)
    {
    	Permission perm = getPermission(viewName);
    	return perm.hasPermission(user, PermissionOption.PERM_OPTION_UPDATE);
    }
    public boolean canDelete(User user, EsfName viewName)
    {
    	Permission perm = getPermission(viewName);
    	return perm.hasPermission(user, PermissionOption.PERM_OPTION_DELETE);
    }
    
    public boolean showSaveButton(User user, EsfName viewName) {
    	return canCreateLike(user,viewName) || canUpdate(user,viewName);
    }
    
    public EsfPathName getPathNameForView(EsfName viewName) {
    	return new EsfPathName(UI_PERM_PATH_PREFIX + viewName.toPlainString());
    }
    
    public Permission getPermission(EsfName viewName)
    {
    	EsfPathName pathName = getPathNameForView(viewName);
    	return pathName.isValid() ? Permission.Manager.getByPathName(pathName) : null;
    }
    
    public static class Manager {
   	    private static LinkedList<EsfName> cache;
   	    
   	    static 
   	    {
   	    	cache = new LinkedList<EsfName>();
   	    	cache.add(UI_DEPLOYMENT_PROPERTIES_VIEW);
   	    	cache.add(UI_DEPLOYMENT_STATS_VIEW);
   	    	cache.add(UI_GROUP_VIEW);
   	    	cache.add(UI_LIBRARY_DOCUMENT_PROGRAMMING_TIPS_EDIT_VIEW);
   	    	cache.add(UI_LIBRARY_PROGRAMMING_TIPS_EDIT_VIEW);
   	    	cache.add(UI_LIBRARY_VIEW);
   	    	cache.add(UI_PACKAGE_VIEW);
   	    	cache.add(UI_REPORT_TEMPLATE_VIEW);
   	    	cache.add(UI_SYSTEM_LOG_VIEW);
   	    	cache.add(UI_TODO_LISTING_VIEW);
   	    	cache.add(UI_TRANSACTION_CSV_START_VIEW);
   	    	cache.add(UI_TRANSACTION_LISTING_VIEW);
   	    	cache.add(UI_TRANSACTION_TEMPLATE_VIEW);
   	    	cache.add(UI_UI_VIEW);
   	    	cache.add(UI_USER_CHANGE_PASSWORD_VIEW);
   	    	cache.add(UI_USER_VIEW);
   	    	cache.add(UI_USER_SESSION_VIEW);
   	    	
   	    	cache.add(UI_ACCESS_CONTROL_TIPS_EDIT_VIEW);
   	    	cache.add(UI_ACCESS_CONTROL_TIPS_VIEW);
   	    	cache.add(UI_PROGRAMMING_TIPS_EDIT_VIEW);
   	    	cache.add(UI_PROGRAMMING_TIPS_VIEW);
   	    	cache.add(UI_REPORT_TIPS_EDIT_VIEW);
   	    	cache.add(UI_REPORT_TIPS_VIEW);
   	    	cache.add(UI_START_TRANSACTION_TIPS_EDIT_VIEW);
   	    	cache.add(UI_START_TRANSACTION_TIPS_VIEW);
   	    	cache.add(UI_SYSTEM_CONFIG_TIPS_EDIT_VIEW);
   	    	cache.add(UI_SYSTEM_CONFIG_TIPS_VIEW);
   	    	cache.add(UI_WELCOME_TIPS_EDIT_VIEW);
   	    	cache.add(UI_WELCOME_TIPS_VIEW);
   	    }
   	 
   		public static synchronized LinkedList<EsfName> getAllViews() {
   			return cache;
   		}
   		public static synchronized void addView(EsfName newView) {
   			if ( ! cache.contains(newView) ) {
   				cache.add(newView);
   			}
   		}
   		public static synchronized void removeView(EsfName oldView) {
   			if ( cache.contains(oldView) ) {
   				cache.remove(oldView);
   			}
   		}
    }
}