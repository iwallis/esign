// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.user;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.TreeSet;

import javax.servlet.http.HttpSession;

import com.esignforms.open.Errors;
import com.esignforms.open.admin.PasswordManager;
import com.esignforms.open.admin.SessionTracker;
import com.esignforms.open.config.CKEditorContext;
import com.esignforms.open.config.Literals;
import com.esignforms.open.Application;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.data.Record;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.email.OutboundEmailMessage;
import com.esignforms.open.log.ActivityLog;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.PropertySet;
import com.esignforms.open.prog.PropertySetInfo;
import com.esignforms.open.prog.PropertySetVersion;
import com.esignforms.open.util.DateUtil;

/**
* User holds a single Open eSignForms user.
* A user belongs to one or more groups.
* 
* @author Yozons, Inc.
*/

public class User
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<User>, java.io.Serializable
{
	private static final long serialVersionUID = -2872860890266915180L;

	private static final com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(User.class);
	
	protected final EsfUUID id;
    protected String  passwordHash;
    protected EsfDateTime loginTimestamp;
    protected String 	  loginIP;
    protected EsfDateTime lastLoginTimestamp;
    protected String 	  lastLoginIP;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID  createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected String email;
    protected String originalEmail;
    protected String displayName;
    protected String personalName;
    protected String familyName;
    protected String employeeId;
    protected String jobTitle;
    protected String location;
    protected String department;
    protected String phoneNumber;
    protected String status;
    protected Record properties;
    
    protected boolean isIPAD;
    protected boolean isIPHONE;
    
    // Extra features of a User that are stored as properties
    public static final EsfPathName SYSTEM_PROP_COMMENTS = new EsfPathName(EsfPathName.ESF_RESERVED_PATH_PREFIX+"comments");
    
    public static final EsfPathName SYSTEM_PROP_TIMEZONE = new EsfPathName(EsfPathName.ESF_RESERVED_PATH_PREFIX+"timezone");
    protected EsfString timezone;
    
    // Path we put in front of any user-defined properties to added to our properties to keep them distinct from anything else
    public static final EsfPathName USER_DEFINED_PROP_PATH_PREFIX = new EsfPathName("UserDefinedProp"+EsfPathName.PATH_SEPARATOR);

    public enum INCLUDE { ONLY_ENABLED, BOTH_ENABLED_AND_DISABLED };
    

    // Not persisted fields
    protected LinkedList<String> messagesToShowUser = null;

	// Group manages all groups and when users are added to a group, group adds itself to the user.
    protected TreeSet<Group> memberOfGroups = new TreeSet<Group>();
    

    /**
     * This creates a User object from data retrieved from the DB.
     */
    protected User(EsfUUID id, String passwordHash, EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		    EsfDateTime lastLoginTimestamp, String lastLoginIP,
    		    String email, String displayName, String personalName, String familyName, 
    		    String employeeId, String jobTitle, String location, String department, String phoneNumber,
    		    String status, Record properties)
    {
        this.id = id;
        this.passwordHash = passwordHash;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.loginTimestamp = null;
        this.loginIP = null;
        this.lastLoginTimestamp = lastLoginTimestamp;
        this.lastLoginIP = lastLoginIP;
        this.email = email;
        this.originalEmail = email;
        this.displayName = displayName;
        this.personalName = personalName;
        this.familyName = familyName;
        this.employeeId = employeeId;
        this.jobTitle = jobTitle;
        this.location = location;
        this.department = department;
        this.phoneNumber = phoneNumber;
        this.status = status;
        this.properties = properties;
        
        // Load our known properties 
        timezone = properties.getStringByName(SYSTEM_PROP_TIMEZONE);
    }

    public final EsfUUID getId()
    {
        return id;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }
    
    public void makeEnabled()
    {
        status = Literals.STATUS_ENABLED;
        objectChanged();
    }
     
    public void makeDisabled()
    {
        status = Literals.STATUS_DISABLED;
        objectChanged();
    }
    
    public void setStatus(String s)
    {
        if ( Literals.STATUS_ENABLED.equals(s) )
            makeEnabled();
        else
            makeDisabled();
    }
    
    public boolean hasPassword()
    {
    	return EsfString.isNonBlank(passwordHash) && ! passwordHash.startsWith(PasswordManager.RESET_PASSWORD_HASH_PREFIX); // On password reset, PasswordManager creates a bogus password hash starting with this string
    }
    public void setPasswordHash(String v)
    {
    	passwordHash = v;
    }
     
    public void setLoggedInNow(UserLoginInfo loginInfo, String ip)
    {
    	loginTimestamp = new EsfDateTime();
    	loginIP = ip;
    	setPasswordHash(loginInfo.getPasswordHash());
    	lastLoginTimestamp = loginInfo.getLastLoginTimestamp();
    	lastLoginIP = loginInfo.getLastLoginIP();
    	
		Application.getInstance().getSecureRandom().setSeed(id+loginIP); // users login at random times/IPs so we'll use this to help keep our PRNG random from back-guessing its seed
    }
    
    /**
     * Clears info we have about this user being logged in
     */
    public void logoff()
    {
    	if ( hasLoginTimestamp() )
    	{
    		lastLoginTimestamp = loginTimestamp;
    		lastLoginIP = loginIP;
    		saveProperties();
    		loginTimestamp = null;
    	}
    }
    
    public boolean isIPHONE()
    {
    	return isIPHONE;
    }
    public void setIPHONE(boolean v)
    {
    	isIPHONE = v;
    }
    
    public boolean isIPAD()
    {
    	return isIPAD;
    }
    public void setIPAD(boolean v)
    {
    	isIPAD = v;
    }
    
    public String getDisplayName()
    {
        return displayName;
    }
    public void setDisplayName(String v)
    {
    	if ( EsfString.isBlank(v) )
    		return;
    	v = EsfString.ensureTrimmedLength(v, Literals.DISPLAY_NAME_MAX_LENGTH);
        displayName = v.trim();
        objectChanged();
    }
    public String getQuotedDisplayName()
    {
    	EsfEmailAddress addr = new EsfEmailAddress(displayName,email);
    	return addr.getQuotedDisplayName();
    }
    
    public String getEmail()
    {
        return email;
    }
    public void setEmail(String v)
    {
    	v = v.trim();
        if ( isValidEmail(v) )
        {
            email = v;
            objectChanged();
        }
    }
    public String getQuotedEmail()
    {
    	EsfEmailAddress addr = new EsfEmailAddress(displayName,email);
    	return addr.getQuotedEmailAddress();
    }
    public EsfEmailAddress getEmailAddress()
    {
    	return new EsfEmailAddress(displayName,email);
    }

    public final String getOriginalEmail()
    {
        return originalEmail;
    }
    public void resetOriginalEmail()
    {
    	originalEmail = email;
    }
 
    public String getFullDisplayName()
    {
        if ( EsfString.isBlank(displayName) )
            return email;
            
        return displayName + " <" + email + ">";
    }
    
    public String getQuotedFullEmailAddress()
    {
    	EsfEmailAddress addr = new EsfEmailAddress(displayName,email);
    	return addr.getQuotedFullEmailAddress();
    }
    
    public String getPersonalName()
    {
        return personalName;
    }
    public void setPersonalName(String v)
    {
    	if ( EsfString.isBlank(v) )
    		return;
    	v = EsfString.ensureTrimmedLength(v, Literals.NAME_MAX_LENGTH);
        personalName = v.trim();
        objectChanged();
    }

    public String getFamilyName()
    {
        return familyName;
    }
    public void setFamilyName(String v)
    {
    	if ( EsfString.isBlank(v) )
    		return;
    	v = EsfString.ensureTrimmedLength(v, Literals.NAME_MAX_LENGTH);
        familyName = v.trim();
        objectChanged();
    }
    
    public String getEmployeeId()
    {
        return employeeId;
    }
    public boolean hasEmployeeId()
    {
    	return employeeId != null;
    }
    public void setEmployeeId(String v)
    {
    	v = EsfString.ensureTrimmedLength(v, Literals.EMPLOYEE_ID_MAX_LENGTH);
        employeeId = EsfString.isBlank(v) ? null : v.trim();
        objectChanged();
    }
    
    public String getJobTitle()
    {
        return jobTitle;
    }
    public boolean hasJobTitle()
    {
    	return jobTitle != null;
    }
    public void setJobTitle(String v)
    {
    	v = EsfString.ensureTrimmedLength(v, Literals.JOB_TITLE_MAX_LENGTH);
        jobTitle = EsfString.isBlank(v) ? null : v.trim();
        objectChanged();
    }
    
    public String getLocation()
    {
        return location;
    }
    public boolean hasLocation()
    {
    	return location != null;
    }
    public void setLocation(String v)
    {
    	v = EsfString.ensureTrimmedLength(v, Literals.LOCATION_MAX_LENGTH);
    	location = EsfString.isBlank(v) ? null : v.trim();
        objectChanged();
    }
    
    public String getDepartment()
    {
        return department;
    }
    public boolean hasDepartment()
    {
    	return department != null;
    }
    public void setDepartment(String v)
    {
    	v = EsfString.ensureTrimmedLength(v, Literals.DEPARTMENT_MAX_LENGTH);
        department = EsfString.isBlank(v) ? null : v.trim();
        objectChanged();
    }
    
    public String getPhoneNumber()
    {
        return phoneNumber;
    }
    public boolean hasPhoneNumber()
    {
    	return phoneNumber != null;
    }
    public void setPhoneNumber(String v)
    {
    	v = EsfString.ensureTrimmedLength(v, Literals.PHONE_NUMBER_MAX_LENGTH);
        phoneNumber = EsfString.isBlank(v) ? null : v.trim();
        objectChanged();
    }
    
    public EsfString getComments()
    {
    	EsfString comments = properties.getStringByName(SYSTEM_PROP_COMMENTS);
    	return comments == null ? new EsfString() : comments;
    }
    public void setComments(EsfString comments)
    {
    	properties.addUpdate(SYSTEM_PROP_COMMENTS, comments == null ? new EsfString() : comments.trim());
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
        return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
        return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
        return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
        return lastUpdatedByUserId;
    }
        
    public EsfDateTime getLoginTimestamp()
    {
        return loginTimestamp;
    }
    public boolean hasLoginTimestamp() 
    {
    	return loginTimestamp != null;
    }
    
    public String getLoginIP()
    {
        return loginIP;
    }
    
    public EsfDateTime getLastLoginTimestamp()
    {
        return lastLoginTimestamp;
    }
    public boolean hasLastLoginTimestamp()
    {
    	return lastLoginTimestamp != null;
    }
    
    public String getLastLoginIP()
    {
        return lastLoginIP;
    }
    
	/**
	 * Finds the specified property name in the specified propertyset, if provided, as set for this user.
	 * @param propertySetName the name of the property set to look in; if null, all property sets are checked in alpha order
	 * @param propertyName the property name
	 * @param doProductionResolve - resolve property sets for production or testing
	 * @param doCheckForTestProperty - if true, look for propertyName_TEST property before propertyName.
	 * @return the EsfValue of the property
	 */
	public EsfValue getPropertyValue(EsfName propertySetName, EsfPathName propertyName, boolean doProductionResolve, boolean doCheckForTestPropertyFirst)
	{
		if ( propertySetName == null )
		{
			for( PropertySetInfo psInfo : PropertySetInfo.Manager.getAll(getId()) )
			{
				PropertySet propertySet = PropertySet.Manager.getById(psInfo.getId());
				if ( propertySet != null && propertySet.isEnabled() )
				{
					PropertySetVersion propertySetVersion = doProductionResolve ? propertySet.getProductionPropertySetVersion() : propertySet.getTestPropertySetVersion();
					if ( propertySetVersion != null )
					{
						EsfValue value = null;
						// If we doing a test, we'll see if we can find a PropName_TEST version first and use it if we do
						if ( doCheckForTestPropertyFirst && propertyName.getLength() < Library.MAX_PROPERTY_NAME_LENGTH_WITHOUT_TEST_SUFFIX )
							value = propertySetVersion.getPropertyValue( new EsfPathName(propertyName.toString()+Library.TEST_PROPERTY_SUFFIX) );
						if ( value == null )
							value = propertySetVersion.getPropertyValue(propertyName);
						return value;
					}
				}
			}
		}
		else
		{
			PropertySet propertySet = PropertySet.Manager.getByName(getId(), propertySetName);
			if ( propertySet != null && propertySet.isEnabled() )
			{
				PropertySetVersion propertySetVersion = doProductionResolve ? propertySet.getProductionPropertySetVersion() : propertySet.getTestPropertySetVersion();
				if ( propertySetVersion != null )
				{
					EsfValue value = null;
					// If we doing a test, we'll see if we can find a PropName_TEST version first and use it if we do
					if ( doCheckForTestPropertyFirst && propertyName.getLength() < Library.MAX_PROPERTY_NAME_LENGTH_WITHOUT_TEST_SUFFIX )
						value = propertySetVersion.getPropertyValue( new EsfPathName(propertyName.toString()+Library.TEST_PROPERTY_SUFFIX) );
					if ( value == null )
						value = propertySetVersion.getPropertyValue(propertyName);
					return value;
				}
			}
		}
		
		return null;
	}
    

    
    protected Record getProperties()
    {
        return properties;
    }
    public EsfString getUserDefinedStringProperty(EsfName esfname)
    {
    	return properties.getStringByName(USER_DEFINED_PROP_PATH_PREFIX+esfname.toPlainString());
    }
    public EsfString getUserDefinedStringProperty(String pathName)
    {
    	return properties.getStringByName(USER_DEFINED_PROP_PATH_PREFIX+pathName);
    }
    public void setUserDefinedStringProperty(String pathName, EsfString value)
    {
    	properties.addUpdate(USER_DEFINED_PROP_PATH_PREFIX+pathName, value.trim());
    }
    
    public EsfString[] getUserDefinedStringsProperty(EsfName esfname)
    {
    	return properties.getStringsByName(USER_DEFINED_PROP_PATH_PREFIX+esfname.toPlainString());
    }
    public EsfString[] getUserDefinedStringsProperty(String pathName)
    {
    	return properties.getStringsByName(USER_DEFINED_PROP_PATH_PREFIX+pathName);
    }
    public void setUserDefinedStringsProperty(String pathName, EsfString[] value)
    {
    	properties.addUpdate(USER_DEFINED_PROP_PATH_PREFIX+pathName, value);
    }
    
    public EsfInteger getUserDefinedIntegerProperty(String pathName)
    {
    	return properties.getIntegerByName(USER_DEFINED_PROP_PATH_PREFIX+pathName);
    }
    public void setUserDefinedIntegerProperty(String pathName, EsfInteger v)
    {
    	properties.addUpdate(USER_DEFINED_PROP_PATH_PREFIX+pathName, v);
    }
    
    public EsfDate getUserDefinedDateProperty(String pathName)
    {
    	return properties.getDateByName(USER_DEFINED_PROP_PATH_PREFIX+pathName);
    }
    public void setUserDefinedDateProperty(String pathName, EsfDate date)
    {
    	properties.addUpdate(USER_DEFINED_PROP_PATH_PREFIX+pathName, date);
    }
    
    public EsfBoolean getUserDefinedBooleanProperty(String pathName)
    {
    	return properties.getBooleanByName(USER_DEFINED_PROP_PATH_PREFIX+pathName);
    }
    public void setUserDefinedBooleanProperty(String pathName, EsfBoolean value)
    {
    	properties.addUpdate(USER_DEFINED_PROP_PATH_PREFIX+pathName, value);
    }

    public EsfUUID getUserDefinedUUIDProperty(String pathName)
    {
    	return properties.getUUIDByName(USER_DEFINED_PROP_PATH_PREFIX+pathName);
    }
    public void setUserDefinedUUIDProperty(String pathName, EsfUUID value)
    {
    	properties.addUpdate(USER_DEFINED_PROP_PATH_PREFIX+pathName, value);
    }

    public EsfUUID[] getUserDefinedUUIDsProperty(String pathName)
    {
    	return properties.getUUIDsByName(USER_DEFINED_PROP_PATH_PREFIX+pathName);
    }
    public void setUserDefinedUUIDsProperty(String pathName, EsfUUID[] uuids)
    {
    	properties.addUpdate(USER_DEFINED_PROP_PATH_PREFIX+pathName, uuids);
    }

    public void saveProperties()
    {
    	properties.save();
    }
    
    public EsfString getTimezone()
    {
    	return timezone;
    }
    
    public void setTimezone(EsfString s)
    {
    	timezone = s.trim();
    	getProperties().addUpdate( SYSTEM_PROP_TIMEZONE,timezone );
    	objectChanged();
    }

    public java.util.TimeZone getTimezoneTz()
    {
        return java.util.TimeZone.getTimeZone(getTimezone().toString());
    }
    
    public void addMessagesToShowUser(String msg)
    {
    	if ( messagesToShowUser == null )
    		messagesToShowUser = new LinkedList<String>();
    	messagesToShowUser.add(msg);
    }
    public String[] getMessagesToShowUser()
    {
    	if ( messagesToShowUser == null )
    		return null;
    	String[] msgs = new String[messagesToShowUser.size()];
    	messagesToShowUser.toArray(msgs);
    	return msgs;
    }
    public void clearMessagesToShowUser()
    {
    	if ( messagesToShowUser != null )
    	{
        	messagesToShowUser.clear();
        	messagesToShowUser = null;
    	}
    }
    public String[] getAndClearMessagesToShowUser()
    {
    	String[] msgs = getMessagesToShowUser();
    	clearMessagesToShowUser();
    	return msgs;
    }
    
    // Set by Group
    synchronized void addMemberOfGroup(Group group)
    {
    	TreeSet<Group> memberOfGroupsNew = new TreeSet<Group>(memberOfGroups);	
    	if ( memberOfGroupsNew.contains(group) )
    		memberOfGroupsNew.remove(group);
    	memberOfGroupsNew.add(group);
    	memberOfGroups = memberOfGroupsNew;
    }
    // This is basically used only when the Group cache is being loaded
    void _addMemberOfGroupDuringInitializationOnly(Group group)
    {
    	memberOfGroups.add(group);
    }

    public void _addToMemberOfGroupsDuringCreateLikeOnly(Group group)
    {
    	if ( doInsert() && ! memberOfGroups.contains(group) )
    		memberOfGroups.add(group);
    }
    public void _clearMemberOfGroupsDuringCreateLikeOnly()
    {
    	if ( doInsert() )
    		memberOfGroups.clear();
    }
    
    // Set by Group, not directly
    synchronized void removeMemberOfGroup(Group group)
    {
    	TreeSet<Group> memberOfGroupsNew = new TreeSet<Group>(memberOfGroups);	
    	memberOfGroupsNew.remove(group);
    	memberOfGroups = memberOfGroupsNew;
    }
    
	public boolean isMemberOfSuperGroup()
    {
		return memberOfGroups.contains(Group.Manager.getSuperGroup());
    }
    
	public boolean isMemberOfGroup(Group group)
    {
		return group.isEnabled() && memberOfGroups.contains(group);
    }
	
	public int getMemberOfGroupsCount()
	{
		return memberOfGroups.size();
	}
	
    public ArrayList<Group> getMemberOfGroups()
    {
    	ArrayList<Group> groupList = new ArrayList<Group>();
    	for( Group group : memberOfGroups )
    	{
    		if ( group.isEnabled() )
    			groupList.add(group);
    	}
    	return groupList;
    }

    public ArrayList<Group> getMemberOfGroups(User visibleToUser, EsfName permissionOption)
    {
    	ArrayList<Group> groupList = new ArrayList<Group>();
    	for( Group group : memberOfGroups )
    	{
    		if ( group.hasPermission(visibleToUser,PermissionOption.PERM_OPTION_LIST) )
    			groupList.add(group);
    	}
    	return groupList;
    }

    // A uer's permissions are related to the group permissions he's a member of.
    public boolean hasMemberUsersPerm(EsfName permOptionName)
    {
    	for( Group group : memberOfGroups )
    	{
    		if ( group.hasMemberUsersPermission(this, permOptionName))
    			return true;
    	}
    	return false;
    }

    /**
     * Determines whether the specified user can list this user based on whether this
     * user is a member of a group that the specified user can list members on (got that!).
     * @param user the user whose permission is being checked to see if can list this user
     * @return true if the specified user has 'member users list' permission on a group this user is a member of
     */
    public boolean canUserList(User user)
    {
    	if ( user.isMemberOfSuperGroup() )
    		return true;
    	Collection<Group> allGroupsUserCanListMemberUsers = Group.Manager.getForUserWithListMemberUsersPermission(user);
    	for( Group g : allGroupsUserCanListMemberUsers )
    	{
    		if ( isMemberOfGroup(g) )
    			return true;
    	}
		return false;
    }
    
    public boolean canUserUpdate(User user)
    {
    	if ( user.isMemberOfSuperGroup() )
    		return true;
    	Collection<Group> allGroupsUserCanUpdateMemberUsers = Group.Manager.getForUserWithUpdateMemberUsersPermission(user);
    	for( Group g : allGroupsUserCanUpdateMemberUsers )
    	{
    		if ( isMemberOfGroup(g) )
    			return true;
    	}
		return false;
    }
    
    public boolean canUserManageToDo(User user)
    {
    	if ( user.isMemberOfSuperGroup() )
    		return true;
    	Collection<Group> allGroupsUserCanManageToDoMemberUsers = Group.Manager.getForUserWithManageToDoMemberUsersPermission(user);
    	for( Group g : allGroupsUserCanManageToDoMemberUsers )
    	{
    		if ( isMemberOfGroup(g) )
    			return true;
    	}
		return false;
    }
    
    public boolean canUserCreateLike(User user)
    {
    	if ( user.isMemberOfSuperGroup() )
    		return true;
    	Collection<Group> allGroupsUserCanCreateLikeMemberUsers = Group.Manager.getForUserWithCreateLikeMemberUsersPermission(user);
    	for( Group g : allGroupsUserCanCreateLikeMemberUsers )
    	{
    		if ( isMemberOfGroup(g) )
    			return true;
    	}
		return false;
    }
    
    public boolean canUserDelete(User user)
    {
    	if ( user.isMemberOfSuperGroup() )
    		return true;
    	Collection<Group> allGroupsUserCanDeleteMemberUsers = Group.Manager.getForUserWithDeleteMemberUsersPermission(user);
    	for( Group g : allGroupsUserCanDeleteMemberUsers )
    	{
    		if ( isMemberOfGroup(g) )
    			return true;
    	}
		return false;
    }
    
    public CKEditorContext getCKEditorContext(HttpSession session, String id)
    {
    	return (CKEditorContext)session.getAttribute("CKEditorContext-"+id);
    }

    public String createCKEditorContext(HttpSession session)
    {
    	CKEditorContext ctx = new CKEditorContext();
    	session.setAttribute("CKEditorContext-"+ctx.getIdInSession(),ctx);
    	return ctx.getIdInSession();
    }

    public void releaseCKEditorContext(HttpSession session, String id)
    {
    	if ( EsfString.isNonBlank(id) && session != null )
    		session.removeAttribute("CKEditorContext-"+id);
    }

    @Override
    public int compareTo(User u)
    {
    	return getId().compareTo(u.getId());
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof User )
            return getId().equals(((User)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    

    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
    public void logLoginLogoff(String message)
    {
        ActivityLog log = Application.getInstance().getActivityLog();
        log.logUserLoginLogoff(id,message);
    }
    public void logLoginLogoff(Connection con, String message)
        throws SQLException
    {
        ActivityLog log = Application.getInstance().getActivityLog();
        log.logUserLoginLogoff(con,id,message);
    }
    
    public void logSecurity(String message)
    {
        ActivityLog log = Application.getInstance().getActivityLog();
        log.logUserSecurity(id,message);
    }
    public void logSecurity(Connection con, String message)
        throws SQLException
    {
        ActivityLog log = Application.getInstance().getActivityLog();
        log.logUserSecurity(con,id,message);
    }
    
    public void logConfigChange(String message)
    {
        ActivityLog log = Application.getInstance().getActivityLog();
        log.logUserConfigChange(id,message);
    }
    public void logConfigChange(Connection con, String message)
        throws SQLException
    {
        ActivityLog log = Application.getInstance().getActivityLog();
        log.logUserConfigChange(con,id,message);
    }
    
    public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
        _logger.debug("save(con) on id: " + id + "; email: " + email + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	SessionTracker sessionTracker = Application.getInstance().getSessionTracker();
        	if ( sessionTracker != null ) // normally have, but in 'dbsetup' mode it's not there
        		sessionTracker.updateUser(this);
        	
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
    		lastUpdatedTimestamp = new EsfDateTime();
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
        		createdTimestamp = lastUpdatedTimestamp;

            	properties.save(con);
                    
                stmt = new EsfPreparedStatement( con, 
                "INSERT INTO esf_user (id,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,email,display_name,personal_name,family_name,employee_id,job_title,location,department,phone_number,status,properties_blob_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
            	stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(email);
                stmt.set(displayName);
                stmt.set(personalName);
                stmt.set(familyName);
                stmt.set(employeeId);
                stmt.set(jobTitle);
                stmt.set(location);
                stmt.set(department);
                stmt.set(phoneNumber);
                stmt.set(status);
                stmt.set(properties.getId());
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                    _logger.warn("save(con) - Insert failed for id: " + id + "; email: " + email);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.add(this);
                
                // On create, we add this user to any specified member groups
                for( Group g : getMemberOfGroups() ) {
                	g.addMemberUser(this);
                }
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new user " + getFullDisplayName()); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new user " + getFullDisplayName());
                }

                return true;
            }
            
            // This must be an update request. But if it hasn't changed, we can just treat this as a null operation
            if ( ! hasChanged() && ! properties.hasChanged() )
                return true;
            
            if ( properties.hasChanged() )
                properties.save(con);

            // We assume we'll update it instead
            stmt = new EsfPreparedStatement( con, "UPDATE esf_user SET email=?,display_name=?,personal_name=?,family_name=?,employee_id=?,job_title=?,location=?,department=?,phone_number=?,status=?,last_updated_timestamp=?,last_updated_by_user_id=? WHERE id=?");
            stmt.set(email);
            stmt.set(displayName);
            stmt.set(personalName);
            stmt.set(familyName);
            stmt.set(employeeId);
            stmt.set(jobTitle);
            stmt.set(location);
            stmt.set(department);
            stmt.set(phoneNumber);
            stmt.set(status);
        	stmt.set(lastUpdatedTimestamp);
            stmt.set(lastUpdatedByUserId);
            stmt.set(id);
            int num = stmt.executeUpdate();
            if ( num != 1 )
            {
                _logger.warn("save(con) - Update failed for user id: " + id + "; email: " + email);
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated user " + getFullDisplayName() + "; status; " + getStatus() + "; numMemberOfGroups: " + memberOfGroups.size()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated user " + getFullDisplayName() + "; status; " + getStatus() + "; numMemberOfGroups: " + memberOfGroups.size());
            }

            return true;
        }
        catch(SQLException e)
        {
            _logger.sqlerr(e,"save(con) on id: " + id + "; email: " + email + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
    public boolean save(final Connection con)
	    throws SQLException
	{
    	return save(con,(User)null);
	}
    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }

    private synchronized boolean delete(Connection con,Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; user: " + this.getFullDisplayName() + "; by user: " + (user==null?"(none)":user.getFullDisplayName()));

    	clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use in the transaction definition.
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
            _logger.warn("delete(con) - Ignored delete of user that was pending an INSERT id: " + id + "; email: " + email);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        // Delete group membership info
        for( Group g : memberOfGroups ) {
        	g.removeMemberUser(this);
        	g.save(con);
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
            // Delete the user
            stmt = new EsfPreparedStatement( con, "DELETE FROM esf_user WHERE id=?");
            stmt.set(id);
            int num = stmt.executeUpdate();
            if ( num == 0 )
                _logger.warn("delete(con) - Failed to find esf_user database row to delete with id: " + id);
            
            // Delete user login info
            stmt.close(); 
            stmt = new EsfPreparedStatement( con, "DELETE FROM esf_userlogin WHERE user_id=?");
            stmt.set(id);
            stmt.executeUpdate();

            // Delete user login history
            stmt.close(); 
            stmt = new EsfPreparedStatement( con, "DELETE FROM esf_userlogin_history WHERE user_id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            // Delete this user's activity log
            Application.getInstance().getActivityLog().deleteAllForUser(con,id);
            
            // Delete all outbound emails associated with this user
            OutboundEmailMessage.Manager.deleteAllForUser(con, id);
            
            // Delete our properties
            properties.delete(con);
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted user " + getFullDisplayName() + "; status; " + getStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted user " + getFullDisplayName() + "; status; " + getStatus());
            }

            return true;
        }
        catch(SQLException e)
        {
            _logger.sqlerr(e,"delete(con,Errors) on id: " + id + "; email: " + email + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }

    public boolean delete(Errors errors, final User user)
    {
    	ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }

    public boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
       
   protected boolean checkReferential(Connection con,Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        stmt = new EsfPreparedStatement(con,
	                "SELECT COUNT(*) FROM esf_transaction WHERE created_by_user_id=? OR last_updated_by_user_id=?"
	                                   );
	        stmt.set(id);
	        stmt.set(id);
	        EsfResultSet rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	            int count = rs.getInt();
	            if ( count > 0 )
	            {
	                errors.addError("The user is still referenced by " + count + " transaction(s).");
	                return false;
	            }
	        }
	        
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}
   
   public static class Manager
   {
   	    // Cache of all users.  This is put at the bottom of all other static fields to ensure that they
   		// are all created before we load user caches which then bomb out because the other statics are not
   		// yet initialized.
   		protected static UserCache cache;
   	    static 
   	    {
   	    	loadCache();
   	    }
   	    

   	    /**
   	     * Loads the User Caches
   	     */
   	    private static void loadCache()
   	    {
   	    	_logger.debug("Manager.loadCache()");
   	    	
   	    	cache = new UserCache();
   	        
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        try
   	        {
   	            stmt = new EsfPreparedStatement( con,
   	            		"SELECT id,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,email,display_name,personal_name,family_name," +
   	            		"employee_id,job_title,location,department,phone_number,status,properties_blob_id," +
   	            		"esf_userlogin.pwd_hash,esf_userlogin.last_login_timestamp, esf_userlogin.last_login_ip " +
   	    	            "FROM esf_user LEFT JOIN esf_userlogin ON esf_user.id=esf_userlogin.user_id"
   	            						   );
   	            EsfResultSet rs = stmt.executeQuery();
   	            while( rs.next() )
   	            {
    	            EsfUUID id          = rs.getEsfUUID();
    	            EsfDateTime created = rs.getEsfDateTime();
    	            EsfUUID createdBy   = rs.getEsfUUID();
		            EsfDateTime lastUpdated = rs.getEsfDateTime();
		            EsfUUID lastUpdatedBy   = rs.getEsfUUID();
    	            String email		= rs.getString();
    	            String displayName  = rs.getString();
    	            String personalName = rs.getString();
    	            String familyName 	= rs.getString();
    	            String employeeId	= rs.getString();
    	            String jobTitle		= rs.getString();
    	            String location		= rs.getString();
    	            String department	= rs.getString();
    	            String phoneNumber	= rs.getString();
    	            String stat        	= rs.getString();
    	            EsfUUID propId      = rs.getEsfUUID();
    	            String pwdHash      = rs.getString();
    	        	// we use hashes, so if this invalid value is there, it's not real and instead the user has a password reset request outstanding
    	            if ( pwdHash != null && pwdHash.startsWith("Reset ") ) 
    	                pwdHash = null;
    	            EsfDateTime lastLogin = rs.getEsfDateTime();
    	            String lastLoginIP  = rs.getString();
    	            
    	            Record properties = Record.Manager.getById(con,propId);
    	            
    	            User user = new User(id,pwdHash,created,createdBy,lastUpdated,lastUpdatedBy,lastLogin,lastLoginIP,email,displayName,personalName,familyName,
    	            						employeeId,jobTitle,location,department,phoneNumber,stat,properties);
    	            user.setLoadedFromDb();
    	            
    	            cache.add(user);
   	            }
   	            con.commit();
   	            _logger.debug("Manager.loadCache() completed size: " + cache.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	            _logger.sqlerr(e,"Manager.loadCache() - clearing " + cache.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	            cache.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }
   	    }
   	    
   	    public static void reloadCache()
   	    {
   	    	cache.clear();
   	    	loadCache();
   	    }

   	    /**
   	     * Creates a new user like an existing user.  No password will be set until the user selects his own password.
   	     * @param likeUser the User that we'll be cloning from
   	     * @param email the new user's email address
   	     * @param personalName the new user's first name
   	     * @param familyName the new user's last name
   	     * @param createdBy the User who is creating the new user
   	     * @return
   	     */
   	    public static User createNewUserLike(User likeUser, String email, String personalName, String familyName, User createdBy)
   	    {
   	        EsfUUID id = new EsfUUID();
   	        String passwordHash = null;
   	        EsfDateTime nowTimestamp = new EsfDateTime();
   	        EsfDateTime lastLoginTimestamp = null;
   	        String lastLoginIP = null;
   	        String displayName = personalName + " " + familyName;
   	        // Copy over preferences, etc.
   	        Record properties = new Record( new EsfName("userProps"), likeUser.getProperties() );

   	    	User user = new User( id, passwordHash, nowTimestamp, createdBy.getId(), nowTimestamp, createdBy.getId(), lastLoginTimestamp, lastLoginIP,
   	    						  email, displayName, personalName, familyName, 
   	    						  likeUser.getEmployeeId(), likeUser.getJobTitle(), likeUser.getLocation(), likeUser.getDepartment(),likeUser.getPhoneNumber(),likeUser.getStatus(), properties
   	    						);
   	    	
   	    	// For a brand new user, this will hold the groups he's supposed to belong to, but it'll be limited to
   	    	// those the 'like user' had AND that the creating user has permission to update that group.
   	    	for( Group g : likeUser.getMemberOfGroups() ) {
   	    		if ( g.hasPermission(createdBy, PermissionOption.PERM_OPTION_UPDATE) )
   	    			user._addMemberOfGroupDuringInitializationOnly(g);
   	    	}
   	    	
   	    	return user;
   	    }

   	    // Only called by DbSetup
   	    public static User _dbsetup_createNewSuperUser(EsfUUID id, String email, String personalName, String familyName, String passwordHash)
   	    {
   	        EsfDateTime nowTimestamp = new EsfDateTime();
   	        EsfDateTime lastLoginTimestamp = null;
   	        String lastLoginIP = null;
   	        String displayName = personalName + " " + familyName;

   	        Record properties = new Record( new EsfName("userProps"), BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE );
   	        
   	        EsfUUID deployId = Application.getInstance().getDeployId();

   	    	User user = new User( id, passwordHash, nowTimestamp, deployId, nowTimestamp, deployId, lastLoginTimestamp, lastLoginIP,
   	    						  email, displayName, personalName, familyName, null, null, null, null, null, Literals.STATUS_ENABLED, properties
   	    						);

   	    	user.setTimezone( new EsfString(DateUtil.getDefaultTimeZone().getID()) );
   	    	return user;
   	    }

   	    
   	    /**
   	     * 
   	     * Retrieves a User object based on the supplied id.
   	     * @param id the EsfUUID of the user account to retrieve
   	     * @return the User object if found; else null
   	     */
   	    public static User getById(EsfUUID id)
   	    {
   	    	User u = cache.getById(id);
   	    	if ( u == null )
   	   	    	_logger.debug("Manager.getById() could not find User with id: " + id);
        	return u;
   	    }
   	        
   	    /**
   	     * 
   	     * Retrieves a User object based on the supplied email address.
   	     * @param email the String email address of the user account to retrieve
   	     * @return the User if found; else null
   	     */
   	    public static User getByEmail(String email)
   	    {
   	    	User u = cache.getByEmail(email);
   	    	if ( u == null )
   	   	    	_logger.debug("Manager.getByEmail() could not find User with email: " + email);
        	return u;
   	    }
   	        
   	    /**
   	     * Returns the total number of user objects
   	     * @return the total number of user objects
   	     */
   	    public static int getNumUsers()
   	    {
   	    	return cache.size();
   	    }

   	    /**
   	     * Retrieves all User objects ordered by email
   	     * @return the Collection of Users found ordered by email
   	     */
   	    public static Collection<User> getAll()
   	    {
   	    	return cache.getAllByEmail();
   	    }
   	    
   	    /**
   	     * Retrieves all User objects ordered by email that the specified user can see.
   	     * @return the Collection of Users found that the user is allowed to see, ordered by email
   	     */
   	    public static Collection<User> getAll(User user)
   	    {
   	    	return cache.getAllByEmail(user);
   	    }
   	    
   	}

}