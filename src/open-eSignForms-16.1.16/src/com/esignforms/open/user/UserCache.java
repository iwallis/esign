// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2010 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.user;

import java.util.Collection;
import java.util.LinkedList;
import java.util.TreeMap;

import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.User;

/**
* Holds a User object that's known by both its EsfUUID and String email.  The User is stored in two distinct, but otherwise
* synchronized TreeMaps.  Note that the EsfUUID may not be changed, but the email can be changed.  
* 
* @author Yozons, Inc.
*/
public class UserCache
{
    // We create two caches, one searchable by the EsfUUID, and one by the String (email address).  We ensure that adds
	// to the cache are put in both, and deletes are removed from both, and that updates are updated in both.
	// Access is synchronized to prevent iterator issues.
    protected TreeMap<EsfUUID,User> uuidCache = new TreeMap<EsfUUID,User>();
    protected TreeMap<String,User> emailCache = new TreeMap<String,User>();


    public UserCache()
    {
    }
    
    public synchronized void clear()
    {
    	uuidCache.clear();
    	emailCache.clear();
    }
    
    /**
     * Returns the number objects in the cache.
     */
    public synchronized int size()
    {
    	return uuidCache.size();
    }
    
    /**
     * Returns the number users in the cache that the specified user can list.
     */
	public synchronized int size(User user)
    {
    	if ( user == null )
    		return 0;
    	int count = 0;
    	for( User u : uuidCache.values() )
    	{
    		if ( u.canUserList(user) )
    			++count;
    	}
    	return count;
    }
    
    /**
     * Adds the object to the cache if and only if the object's id and name are not already in the cache.
     * @param obj the object to add
     * @return true if the object is added to the cache; false if the cache already contains an object with the id/name
     */
    public synchronized boolean add(final User user)
    {
    	EsfUUID id = user.getId();
    	String email = user.getEmail().toLowerCase();
    	
    	if ( uuidCache.containsKey(id) || emailCache.containsKey(email) )
    		return false;
    	
    	user.resetOriginalEmail();
    	uuidCache.put(id, user);
    	emailCache.put(email, user);
    	return true;
    }

    /**
     * Replaces the specified object in the cache.  The object must already exist with the same id, and that cache entry
     * will be updated to use this new object.  The path of the object updated by id is removed and the new object's path is added
     * so that if you rename the object, it will work as expected (the old path will no longer be there and the new path will).
     * @param obj the object to replace in the cache
     * @return true if the cache is updated with the new object; false if the id doesn't exist in the cache
     */
    public synchronized boolean replace(final User user)
    {
    	EsfUUID id = user.getId();
    	String email = user.getEmail().toLowerCase();
    	
    	if ( ! uuidCache.containsKey(id) )
    		return false;
    	
    	User origObj = uuidCache.remove(id);
    	if ( origObj == null )
    		return false;

    	emailCache.remove(origObj.getOriginalEmail().toLowerCase()); 
    	user.resetOriginalEmail();
    	uuidCache.put(id, user);
    	emailCache.put(email,user);
    	return true;
    }

    /**
     * Removes the specified object from the cache using only the id as the key.  The name is ignored
     * and the name associated with the original named object is used.
     * @param obj the object to remove
     * @return
     */
    public synchronized boolean remove(final User user)
    {
    	EsfUUID id = user.getId();

    	if ( ! uuidCache.containsKey(id) )
    		return false;

    	User origObj = uuidCache.remove(user.getId());
    	if ( origObj == null )
    		return false;
    	emailCache.remove(origObj.getOriginalEmail().toLowerCase());
    	return true;
    }

    /**
     * Gets the object by id.
     * @param id the id of the object to retrieve
     * @return the object found or null if not present
     */
    public synchronized User getById(EsfUUID id)
    {
    	return id == null ? null : uuidCache.get(id);
    }

    /**
     * Gets the object by id only if the user can list it.
     * @param id the id of the object to retrieve
     * @param user the User who is attempting to retrieve it
     * @return the object found or null if not present
     */
    public synchronized User getById(EsfUUID id, User user)
    {
    	if ( user == null )
    		return null;
    	User obj = uuidCache.get(id);
    	return ( obj == null /*|| ! obj.canUserList(user)*/ ) ? null : obj;
    }


    /**
     * Gets all of the objects in a collection sorted by id.
     * @return collection of objects
     */
    public synchronized Collection<User> getAllById()
    {
    	return uuidCache.values();
    }

    /**
     * Gets all of the objects in a collection sorted by id if the user can list it
     * @param user the User who is attempting to retrieve them
     * @return the collection of objects the user can list
     */
    public synchronized Collection<User> getAllById(User user)
    {
    	if ( user == null )
    		return new LinkedList<User>();
    	
    	LinkedList<User> list = new LinkedList<User>();
    	for( User obj : uuidCache.values() )
    	{
    		if ( obj.canUserList(user) )
    			list.add(obj);
    	}
    	return list;
    }


    
    /**
     * Gets the object by name.
     * @param pathName the path name of the object to retrieve
     * @return the object found or null if not present
     */
    public synchronized User getByEmail(String email)
    {
    	return emailCache.get(email.toLowerCase());
    }
    
    /**
     * Gets the object by path name only if the user can list it.
     * @param pathName the path name of the object to retrieve
     * @param user the User who is attempting to retrieve it
     * @return the object found or null if not present
     */
    public synchronized User getByEmail(String email, User user)
    {
    	if ( user == null )
    		return null;
    	User obj = emailCache.get(email.toLowerCase());
    	return ( obj == null || ! obj.canUserList(user) ) ? null : obj;
    }


    /**
     * Gets the all of the objects ordered by path name
     * @return the collection of all objects ordered by path name
     */
    public synchronized Collection<User> getAllByEmail()
    {
    	return emailCache.values();
    }
    
    /**
     * Gets the all of the objects ordered by path name that the user can list
     * @param user the User who is attempting to retrieve them
     * @return the collection of all objects ordered by path name that the user can list
     */
    public synchronized Collection<User> getAllByEmail(User user)
    {
    	if ( user == null )
    		return new LinkedList<User>();
    	LinkedList<User> list = new LinkedList<User>();
    	for( User obj : emailCache.values() )
    	{
    		if ( obj.canUserList(user) )
    			list.add(obj);
    	}
    	return list;
    }
}