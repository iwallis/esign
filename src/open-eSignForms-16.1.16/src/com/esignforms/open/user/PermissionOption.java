// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.user;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;

/**
* PermissionOption describes an option that's associated with a given Permission.
* It is only accessed through the Permission object.
* 
* @author Yozons, Inc.
*/

public class PermissionOption
	extends com.esignforms.open.db.DatabaseObject
	implements java.io.Serializable
{
	private static final long serialVersionUID = 5918264608998575472L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PermissionOption.class);

	// Common List, ACID type options
	public final static EsfName PERM_OPTION_LIST = new EsfName("List"); // See object in a list
	public final static EsfName PERM_OPTION_VIEWDETAILS = new EsfName("ViewDetails"); // See object details
	public final static EsfName PERM_OPTION_CREATELIKE = new EsfName("CreateLike"); // Create a new object like this one
	public final static EsfName PERM_OPTION_UPDATE = new EsfName("Update"); // Update the object
	public final static EsfName PERM_OPTION_DELETE = new EsfName("Delete"); // Delete the object
	
	// Additional option added to Group member objects, giving members the ability to manage the To Do lists of users in the group.
	public final static EsfName GROUP_MEMBER_USER_PERM_OPTION_MANAGE_TO_DO = new EsfName("ManageToDo"); // Delete the object
	
	// Transaction template permission options 
	public final static EsfName TRAN_PERM_OPTION_START = new EsfName("Start"); // Start a transaction
	public final static EsfName TRAN_PERM_OPTION_CANCEL = new EsfName("Cancel"); // Cancel an active transaction
	public final static EsfName TRAN_PERM_OPTION_REACTIVATE = new EsfName("Reactivate"); // Reactivate a canceled transaction
	public final static EsfName TRAN_PERM_OPTION_SUSPEND = new EsfName("Suspend"); // Suspend an active transaction
	public final static EsfName TRAN_PERM_OPTION_RESUME = new EsfName("Resume"); // Resume a suspended transaction
	public final static EsfName TRAN_PERM_OPTION_USE_UPDATE_API = new EsfName("UseUpdateAPI"); // Allows the use of the update API from external systems
	public final static EsfName TRAN_PERM_OPTION_USE_ADMIN_API = new EsfName("UseAdminAPI"); // Allows the use of the tran admin API from external systems
	
	// Report template permission options
	public final static EsfName REPORT_PERM_OPTION_RUN_REPORT = new EsfName("RunReport"); // Run the report; allows view of test transactions and those started by the user (or user was a party to)
	public final static EsfName REPORT_PERM_OPTION_VIEW_STARTED_BY_EXTERNAL_USERS = new EsfName("ViewStartedByExternalUsers"); // Allow user to see transactions started by external users
	public final static EsfName REPORT_PERM_OPTION_VIEW_STARTED_BY_ANY_USER = new EsfName("ViewStartedByAnyUser"); // Allow user to see transactions started by any user (limited by users they can see)
	public final static EsfName REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO = new EsfName("ViewAnyUserPartyTo"); // Allow user to see transactions in which any user is a party (limited by users they can see)
	public final static EsfName REPORT_PERM_OPTION_VIEW_PRODUCTION = new EsfName("ViewProduction"); // View production transactions; otherwise only Test transactions can be seen
	public final static EsfName REPORT_PERM_OPTION_DOWNLOAD_CSV = new EsfName("DownloadCSV"); // Download CSV data
	public final static EsfName REPORT_PERM_OPTION_DOWNLOAD_ARCHIVE = new EsfName("DownloadArchive"); // Download archive format of transactions
	public final static EsfName REPORT_PERM_OPTION_VIEW_ACTIVITY_LOG = new EsfName("ViewActivityLog"); // View activity log
	public final static EsfName REPORT_PERM_OPTION_VIEW_EMAIL_LOG = new EsfName("ViewEmailLog"); // View emails sent
	public final static EsfName REPORT_PERM_OPTION_VIEW_SNAPSHOT_DATA = new EsfName("ViewSnapshotData"); // View snapshot data
	public final static EsfName REPORT_PERM_OPTION_VIEW_SNAPSHOT_DOCUMENT = new EsfName("ViewSnapshotDocument"); // View snapshot documents
	public final static EsfName REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_VIEW = new EsfName("EsfReportsAccessView"); // Allow the report runner to view the live package of documents as party esf_reports_access.
	public final static EsfName REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE = new EsfName("EsfReportsAccessUpdate"); // Allow the report runner to view and update the live package of documents as party esf_reports_access.
	
	// Object is owned/contained by a Permission object
	protected EsfName esfname;
    protected String description;
    protected LinkedList<Group> allowedGroups; // We assume that this is read more than updated, so we don't synchronize except on updates
    
    private PermissionOption(EsfName esfname, String description)
    {
    	this.esfname = esfname;
    	this.description = description;
    	allowedGroups = new LinkedList<Group>();
    }
    
    public PermissionOption duplicate()
    {
    	PermissionOption po = new PermissionOption(esfname.duplicate(),description);
    	po.allowedGroups = new LinkedList<Group>(allowedGroups); // no need to duplicate the groups themselves
    	po.setDatabaseObjectLike(this);
    	return po;
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String v)
    {
        if ( v.length() > Literals.DESCRIPTION_MAX_LENGTH )
        	description = v.substring(0,Literals.DESCRIPTION_MAX_LENGTH);
        else
        	description = v;
        objectChanged();
    }
    
    public boolean hasAllUsersPermission()
    {
    	for( Group g : allowedGroups )
    	{
    		if ( g.isAllUsersGroup() )
    			return true;
    	}
    	return false;
    }
    
    public boolean hasExternalUsersPermission()
    {
    	for( Group g : allowedGroups )
    	{
    		if ( g.isExternalUsersGroup() )
    			return true;
    	}
    	return false;
    }
    
    public boolean hasPermission(User user)
    {
    	if ( user == null  )
    		return hasExternalUsersPermission();
    	
    	for( Group g : allowedGroups )
    	{
    		if ( g.isAllUsersGroup() || user.isMemberOfGroup(g) )
    			return true;
    	}
    	return false;
    }

    public boolean hasPermission(Group group)
    {
    	return group != null && allowedGroups.contains(group);
    }

    /**
     * Gets the allowed groups that the user can list
     * @param user the User to check if has permission to list each of the groups in our allowed groups
     * @return the Collection of Groups from our allowed groups that the user is allowed to list
     */
    public Collection<Group> getAllowedGroups(User user)
    {
    	LinkedList<Group> groupList = new LinkedList<Group>();
    	for( Group g : allowedGroups )
    	{
    		if ( g.canUserList(user) )
    			groupList.add(g);
    	}
    	return groupList;
    }

	protected synchronized boolean addGroup( Group group )
    {
    	if ( allowedGroups.contains(group) )
    		return false;

    	// Clone the list, add to it, then assign it back (we assume updates are rare compared to reads, so we never update the list)
    	LinkedList<Group> newAllowedGroups = new LinkedList<Group>(allowedGroups);
    	newAllowedGroups.add(group);
    	allowedGroups = newAllowedGroups;
    	objectChanged();
    	return true;
    }

	protected synchronized boolean removeGroup( Group group )
    {
    	// We never remove the super group from any permission
    	if ( group.isSuperGroup() || ! allowedGroups.contains(group) )
    		return false;

    	// Clone the list, remove it, then assign it back (we assume updates are rare compared to reads, so we never update the list)
    	LinkedList<Group> newAllowedGroups = new LinkedList<Group>(allowedGroups);
    	newAllowedGroups.remove(group);
    	allowedGroups = newAllowedGroups;
    	objectChanged();
    	return true;
    }
	
	protected synchronized void refreshGroups(Permission permission)
	{
    	LinkedList<Group> newAllowedGroups = new LinkedList<Group>();
    	for( Group oldGroup : allowedGroups )
    	{
    		Group newGroup = Group.Manager.getById(oldGroup.getId());
    		if ( newGroup != null )
    			newAllowedGroups.add(newGroup);
    		else
    			_logger.error("refreshGroups() - failed to find group by id: " + oldGroup.getId() + "; group: " + oldGroup.getPathName() + "; in permission option: " + getEsfName() + "; permissionId: " + permission.getId() + "; permission: " + permission.getPathName());
    	}
    	allowedGroups = newAllowedGroups;
	}

    synchronized boolean save(Connection con, EsfUUID permId)
	    throws SQLException
	{
		_logger.debug("save(con) permId: " + permId + "; esfName: " + esfname + "; doInsert: " + doInsert());
		
	    clearLastSQLException();
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        if ( doInsert() )
	        {
	            stmt = new EsfPreparedStatement( con, "INSERT INTO esf_permission_option (permission_id,esfname,description) VALUES(?,?,?)");
	            stmt.set(permId);
	        	stmt.set(esfname);
	            stmt.set(description);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	                _logger.warn("save(con) - Insert esf_permission_option failed for permId: " + permId + "; esfName: " + esfname);
	                return false;
	            }
	            stmt.close();

	            stmt = new EsfPreparedStatement( con, "INSERT INTO esf_permission_option_group (permission_id,permission_option_esfname,group_id) VALUES (?,?,?)");
	            stmt.set(permId);
	            stmt.set(esfname);
	            for( Group g : allowedGroups )
	            {
	            	stmt.set(3,g.getId());
	            	stmt.executeUpdate();
	            }
	            
	            // Now we mark this object as if it were loaded fresh from the database
	            setLoadedFromDb();
	            return true;
	        }
	        
	        // This must be an update request. But if it hasn't changed, we can just treat this as a null operation
	        if ( ! hasChanged() )
	            return true;
	        
	        // We assume we'll update it instead
            stmt = new EsfPreparedStatement( con, "UPDATE esf_permission_option SET description=? WHERE permission_id=? AND lower(esfname)=?");
            stmt.set(description);
            stmt.set(permId);
        	stmt.set(esfname.toLowerCase());
            
            int num = stmt.executeUpdate();
            if ( num != 1 )
            {
                _logger.warn("save(con) - Update esf_permission_option failed for permId: " + permId + "; esfname: " + esfname);
            }
            stmt.close();

            // Remove all groups assigned to this option (we'll add in next)
            stmt = new EsfPreparedStatement( con, "DELETE FROM esf_permission_option_group WHERE permission_id=? AND lower(permission_option_esfname) = ?");
            stmt.set(permId);
            stmt.set(esfname.toLowerCase());
            stmt.executeUpdate();
            stmt.close();
            
            stmt = new EsfPreparedStatement( con, "INSERT INTO esf_permission_option_group (permission_id,permission_option_esfname,group_id) VALUES (?,?,?)");
            stmt.set(permId);
            stmt.set(esfname);
            for( Group g : allowedGroups )
            {
            	stmt.set(3,g.getId());
            	stmt.executeUpdate();
            }
	
	        // Now we mark this object as if it were loaded fresh from the database
	        setLoadedFromDb();
	        return true;
	    }
	    catch(SQLException e)
	    {
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

	// Only accessed through Permission
    static class Manager
	{    	
		static void load( Connection con, EsfUUID permId, LinkedList<PermissionOption> list)
			throws SQLException
		{
            EsfPreparedStatement stmt = null;
            EsfPreparedStatement stmt2 = null;
            try
            {
            	stmt = new EsfPreparedStatement( con, "SELECT esfname,description FROM esf_permission_option WHERE permission_id = ? ORDER BY lower(esfname)");
            	stmt.set(permId);

            	stmt2 = new EsfPreparedStatement( con, "SELECT group_id FROM esf_permission_option_group WHERE permission_id = ? AND lower(permission_option_esfname) = ?");
            	stmt2.set(permId);

            	EsfResultSet rs = stmt.executeQuery();
                while( rs.next() )
                {
                	EsfName name = rs.getEsfName();
                	String description = rs.getString();
                	
                	PermissionOption option = new PermissionOption(name, description);
                	
                	stmt2.set(2,name.toLowerCase()); // specify param position since we're reusing this statement in a loop
                	EsfResultSet rs2 = stmt2.executeQuery();
                	while( rs2.next() )
                	{
                		EsfUUID groupId = rs2.getEsfUUID();
                		Group group = Group.Manager.getById(groupId);
                		if ( group == null )
                			_logger.error("Manager.load - failed to load by group id: " + groupId + "; for permissionId: " + permId + "; option: " + name);
                		else
                			option.allowedGroups.add(group);
                	}
                	rs2.close();
                	
                	option.setLoadedFromDb();
                	list.add(option);
                }
                con.commit();
                
            }
            finally
            {
            	cleanupStatement(stmt);
            	cleanupStatement(stmt2);
            }
			
		}
		
	    /**
	     * Creates a PermissionOption initially accessible only to the SuperGroup.
	     * @param name
	     * @param description
	     */
	    static PermissionOption create(EsfName name, String description)
	    {
	    	PermissionOption option = new PermissionOption(name,description);
	    	option.allowedGroups.add(Group.Manager.getSuperGroup());
	    	return option;
	    }
	     
	    static PermissionOption createLike(PermissionOption likeOption)
	    {
	    	PermissionOption option = new PermissionOption(likeOption.getEsfName(),likeOption.getDescription());
	    	option.allowedGroups = new LinkedList<Group>(likeOption.allowedGroups);
	    	option.addGroup(Group.Manager.getSuperGroup()); // should always be there, but just in case...
	    	return option;
	    }
	     
	    /**
	     * Special create for bootstrapping when we need to create the super group permissions for the newly created super group.
	     */
	    static PermissionOption _dbsetup_createForSuperGroup(EsfName name, String description, Group superGroup)
	    {
	    	PermissionOption option = new PermissionOption(name,description);
	    	option.allowedGroups.add(superGroup);
	    	return option;
	    }
	    // This one creates with no groups in it at all
	    static PermissionOption _dbsetup_createWithNoGroups(EsfName name, String description)
	    {
	    	PermissionOption option = new PermissionOption(name,description);
	    	return option;
	    }

	}
}