// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.user;

import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;

/**
* UserLoginInfo contains info about a user's login credentials.
* 
* @author Yozons, Inc.
*/

public class UserLoginInfo
{
    protected EsfUUID userId;
    protected String pwdHash;
    protected short pwdHashVersion;
    protected String forgottenQuestion;
    protected String forgottenAnswerHash;
    protected String resetPickupCode;
    protected String forgotPickupCode;
    protected EsfDateTime lastLoginTimestamp;
    protected String lastLoginIP;
    protected EsfDateTime passwordSetTimestamp;
    protected short invalidLoginCount;
    protected short invalidAnswerCount;
    protected short requestForgotCount;
    
    protected UserLoginInfo()
    {
    }
    
    public UserLoginInfo(EsfUUID userId, String pwdHash, String forgottenQuestion, String forgottenAnswerHash, String resetPickupCode, String forgotPickupCode,
    					 EsfDateTime lastLoginTimestamp, String lastLoginIP, EsfDateTime passwordSetTimestamp, 
    					 short pwdHashVersion, short invalidLoginCount, short invalidAnswerCount, short requestForgotCount)
    {
        this.userId = userId;
        this.pwdHash = pwdHash;
        this.pwdHashVersion = pwdHashVersion;
        this.forgottenQuestion = forgottenQuestion;
        this.forgottenAnswerHash = forgottenAnswerHash;
        this.resetPickupCode = resetPickupCode;
        this.forgotPickupCode = forgotPickupCode;
        this.lastLoginTimestamp = lastLoginTimestamp;
        this.lastLoginIP = lastLoginIP;
        this.passwordSetTimestamp = passwordSetTimestamp;
        this.invalidLoginCount = invalidLoginCount;
        this.invalidAnswerCount = invalidAnswerCount;
        this.requestForgotCount = requestForgotCount;
    }
     
    public EsfUUID getUserId()
    {
        return userId;
    }
    public User getUser()
    {
    	return User.Manager.getById(userId);
    }
    
    public String getPasswordHash()
    {
        return pwdHash;
    }
    
    public short getPasswordHashVersion()
    {
    	return pwdHashVersion;
    }
    
    public String getForgottenQuestion()
    {
        return forgottenQuestion;
    }
    public boolean hasForgottenQuestion()
    {
        return EsfString.isNonBlank(forgottenQuestion);
    }
    
    public String getResetPickupCode()
    {
    	return resetPickupCode;
    }
    
    public EsfDateTime getLastLoginTimestamp()
    {
    	return lastLoginTimestamp;
    }

    public String getLastLoginIP()
    {
    	return lastLoginIP;
    }
    
    public short getInvalidLoginCount()
    {
        return invalidLoginCount;
    }
    
    public short getInvalidAnswerCount()
    {
        return invalidAnswerCount;
    }
    
    public short getRequestForgotCount()
    {
        return requestForgotCount;
    }
}