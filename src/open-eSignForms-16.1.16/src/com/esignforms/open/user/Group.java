// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Collection;
import java.util.LinkedList;
import java.util.TreeSet;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;

import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.Record;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.util.PathNameUUIDUserListableCacheReadOptimized;

/**
* Group holds a single Open eSignForms group (of users), along with the permissions to manage the Group itself, and to manage users who belong to that group.
* 
* @author Yozons, Inc.
*/
public class Group
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<Group>, PathNameUUIDUserListableCacheReadOptimized.PathNameUUIDUserListableCacheable, java.io.Serializable
{
	private static final long serialVersionUID = 8959095584516215776L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(Group.class);

    protected final EsfUUID id;
    protected EsfUUID parentGroupId; // by default, this is the id of the group used to duplicate/create this group; null if top level
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected EsfPathName pathName;
    protected EsfPathName originalPathName; // derived, not persisted
    protected String  description;
    protected String  status;
    protected Record  properties;
    protected Permission permission; // permission grants related to this group object (create like, delete, list, update, view details of this group object)
    protected Permission memberUsersPermission; // permission related to this group's member users (create like, delete, list, update, view details of this group's member users) 
    											// (access to member users assumes that the user already has 'list' permission to the group itself since he won't even see the group without it,
    											// so therefore cannot have access to any users in that group if not)
    
    public static final String GROUP_PERM_PATH_PREFIX = EsfPathName.ESF_RESERVED_GROUP_PATH_PREFIX + "GroupPerm" + EsfPathName.PATH_SEPARATOR;  // actual name is this prefix followed group's id
    public static final String MEMBER_USERS_PERM_PATH_PREFIX = EsfPathName.ESF_RESERVED_GROUP_PATH_PREFIX + "MemberUsersPerm" + EsfPathName.PATH_SEPARATOR;  // actual name is this prefix followed group's id
    
    // Special group that can never be removed or renamed or be removed from any permissions.
    public static final EsfPathName SUPER_GROUP_ESFPATHNAME	= new EsfPathName(EsfPathName.ESF_RESERVED_GROUP_PATH_PREFIX+"Deployment"+EsfPathName.PATH_SEPARATOR+"SuperAdmin");
    
    // Pseudo-group that implies all users a given has can list
    public static final EsfPathName ALL_USERS_GROUP_ESFPATHNAME	= new EsfPathName(EsfPathName.ESF_RESERVED_GROUP_PATH_PREFIX+"AllUsers");
    
    // Pseudo-group that implies all external (non-authenticated) users
    public static final EsfPathName EXTERNAL_USERS_GROUP_ESFPATHNAME = new EsfPathName(EsfPathName.ESF_RESERVED_GROUP_PATH_PREFIX+"ExternalUsers");
    
    // Special initial super-group-like group that can be assigned and modified by users.
    public static final EsfPathName SYSTEM_ADMIN_GROUP_ESFPATHNAME	= new EsfPathName("System" + EsfPathName.PATH_SEPARATOR + "Administrator");
    
    public enum INCLUDE { ONLY_ENABLED, ONLY_DISABLED, BOTH_ENABLED_AND_DISABLED };
    
    // Path we put in front of any user-defined properties to added to our properties to keep them distinct from anything else
    public static final EsfPathName USER_DEFINED_PROP_PATH_PREFIX = new EsfPathName("UserDefinedProp"+EsfPathName.PATH_SEPARATOR);

    // Extra features of a Group that are stored as properties
    public static final EsfPathName SYSTEM_PROP_COMMENTS = new EsfPathName(EsfPathName.ESF_RESERVED_PATH_PREFIX+"comments");
    
    // The set of users who are a member of this group
    protected TreeSet<User> memberUsers = new TreeSet<User>();
    
    // Temporary lists of users to add/remove based on requests made until save() is called when these are all
    // done and cleared.
    protected LinkedList<User> onSaveUsersToAdd = new LinkedList<User>();
    protected LinkedList<User> onSaveUsersToRemove = new LinkedList<User>();
    
    /**
     * This creates a Group object from data retrieved from the DB.
     */
    protected Group(EsfUUID id, EsfUUID parentGroupId, EsfPathName pathName, String description, 
    		        EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		        String status, Record properties, Permission permission, Permission memberUsersPermission)
    {
        this.id = id;
        this.parentGroupId = parentGroupId;
        this.pathName = pathName;
        this.originalPathName = pathName.duplicate();
        this.description = description;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.status = status;
        this.properties = properties;
        this.permission = permission;
        this.memberUsersPermission = memberUsersPermission;
    }
    
   
    private void _initialization_loadPermissions()
    {
    	EsfPathName permPathName = getPermPathName();
    	EsfPathName memberUsersPermPathName = getMemberUsersPermPathName();
    	permission = Permission.Manager.getByPathName(permPathName);
    	memberUsersPermission = Permission.Manager.getByPathName(memberUsersPermPathName);
    }
    
    private EsfPathName getPermPathName()
    {
    	return new EsfPathName(GROUP_PERM_PATH_PREFIX+getId().toNormalizedEsfNameString());
    }
    private EsfPathName getMemberUsersPermPathName()
    {
    	return new EsfPathName(MEMBER_USERS_PERM_PATH_PREFIX+getId().toNormalizedEsfNameString());
    }
    
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getParentGroupId()
    {
    	return parentGroupId;
    }
    public Group getParentGroup()
    {
    	return hasParentGroup() ? Group.Manager.getById(parentGroupId) : null;
    }
    public boolean hasParentGroup()
    {
    	return parentGroupId != null;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }
    
    public void makeEnabled()
    {
        status = Literals.STATUS_ENABLED;
        objectChanged();
    }
     
    public void makeDisabled()
    {
        status = Literals.STATUS_DISABLED;
        objectChanged();
    }
    
    public void setStatus(String s)
    {
        if ( Literals.STATUS_ENABLED.equals(s) )
            makeEnabled();
        else
            makeDisabled();
    }
    
    public EsfPathName getPathName()
    {
        return pathName;
    }
    public void setPathName(EsfPathName v)
    {
    	// Block setting a group name to null, an invalid EsfPathName or if the group current is a reserved group name or the new name would be reserved
    	if ( v == null || ! v.isValid() || v.hasReservedPathPrefix() || (pathName != null && pathName.hasReservedPathPrefix()) )
    		return;
        pathName = v;
        objectChanged();
    }
    public EsfPathName getOriginalPathName()
    {
        return originalPathName;
    }
    public void resetOriginalPathName()
    {
    	originalPathName = pathName.duplicate();
    }
    
	public boolean isReserved()
    {
    	return getPathName().hasReservedPathPrefix();
    }
    
    public boolean isSuperGroup()
    {
    	return getPathName().equals(SUPER_GROUP_ESFPATHNAME);
    }
    
    public boolean isAllUsersGroup()
    {
    	return getPathName().equals(ALL_USERS_GROUP_ESFPATHNAME);
    }
    
    public boolean isExternalUsersGroup()
    {
    	return getPathName().equals(EXTERNAL_USERS_GROUP_ESFPATHNAME);
    }

    public String getDescription()
    {
        return description;
    }
    public void setDescription(String v)
    {
    	if ( v == null )
    		description = v;
    	else if ( v.length() > Literals.DESCRIPTION_MAX_LENGTH )
            description = v.substring(0,Literals.DESCRIPTION_MAX_LENGTH).trim();
        else
        	description = v.trim();
        objectChanged();
    }
    
    public EsfString getComments()
    {
    	EsfString comments = properties.getStringByName(SYSTEM_PROP_COMMENTS);
    	return comments == null ? new EsfString() : comments;
    }
    public void setComments(EsfString comments)
    {
    	properties.addUpdate(SYSTEM_PROP_COMMENTS, comments == null ? new EsfString() : comments.trim());
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
        return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
        return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
        return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
        return lastUpdatedByUserId;
    }
        
    protected Record getProperties()
    {
        return properties;
    }

    protected boolean saveProperties()
    {
    	return properties.save();
    }
    

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof Group )
            return getId().equals(((Group)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(Group g)
    {
    	return getId().compareTo(g.getId());
    }
    
    @Override
    public boolean canUserList(User user)
    {
		return isAllUsersGroup() || permission.canUserList(user);
    }
    
    public synchronized void addMemberUser(final User user)
    {
    	if ( isAllUsersGroup() || isExternalUsersGroup() )
    		return;
    	
    	// If he's in our list to remove, remove it from there
    	if ( onSaveUsersToRemove.contains(user) )
    		onSaveUsersToRemove.remove(user);
    	// Skip if the user is already a member or in our 'to be added' list
    	if ( ! memberUsers.contains(user) && ! onSaveUsersToAdd.contains(user) ) // won't be in our list until saved
    		onSaveUsersToAdd.add(user);
    }
    
    public synchronized void removeMemberUser(final User user)
    {
    	if ( isAllUsersGroup() || isExternalUsersGroup() )
    		return;

    	// If he's in our list to add, remove it from there
    	if ( onSaveUsersToAdd.contains(user) )
    		onSaveUsersToAdd.remove(user);
    	if ( ! onSaveUsersToRemove.contains(user) ) // won't be off our list until saved
    		onSaveUsersToRemove.add(user);
    }
    
    public boolean isUserAMember(final User user)
    {
    	if ( isAllUsersGroup() || isExternalUsersGroup()  )
    		return true;
    	return memberUsers.contains(user);
    }
    
    /**
     * Returns the member users of this group who are enabled.
     * @param user the User of reference (in case they perhaps should only see a subset of users); if null, then all users are included
     * @return the collection of users who are a member of this group; if this group is All Users or External Users the list returned will be empty.
     */
    public Collection<User> getMemberUsers(User user)
    {
    	LinkedList<User> userList = new LinkedList<User>();
    	if ( isAllUsersGroup() || isExternalUsersGroup()  )
    		return userList;
    	for( User u : memberUsers )
    	{
    		if ( u.isEnabled() )
    		{
        		// If user is null, we'll just allow them access
        		if ( user == null || u.canUserList(user) )
        			userList.add(u);
    		}
    	}
    	return userList;
    }
    
    public boolean hasAnActiveMemberUser()
    {
    	for( User u : memberUsers )
    	{
    		if ( u.isEnabled() )
    			return true;
    	}
    	return false;
    }
    
    /**
     * Returns the member users of this group regardless of being enabled or not.
     * @param user the User of reference (in case they perhaps should only see a subset of users); if null, then all users are included
     * @return the collection of users who are a member of this group; if this group is All Users or External Users the list returned will be empty.
     */
    public Collection<User> getAllMemberUsers(User user)
    {
    	LinkedList<User> userList = new LinkedList<User>();
    	if ( isAllUsersGroup() || isExternalUsersGroup()  )
    		return userList;
    	for( User u : memberUsers )
    	{
    		// If user is null, we'll just allow them access
    		if ( user == null || u.canUserList(user) )
    			userList.add(u);
    	}
    	return userList;
    }
    
    public final boolean hasPermission(User user, EsfName permOptionName)
    {
    	return permission.hasPermission(user, permOptionName);
    }
    
    public final boolean hasMemberUsersPermission(User user, EsfName permOptionName)
    {
   		// All and External groups cannot be used for membership
   		if ( isAllUsersGroup() || isExternalUsersGroup() )
   			return false;

    	return memberUsersPermission.hasPermission(user, permOptionName);
    }
    
    public final Collection<Group> getPermissionAllowedGroups(User user, EsfName permOptionName)
    {
    	return permission.getAllowedGroups(user, permOptionName);
    }
    public final boolean addPermissionAllowedGroups(EsfName permOptionName, Group group)
    {
    	return permission.addGroupToOptions(group, permOptionName) > 0;
    }
    public final boolean removePermissionAllowedGroups(EsfName permOptionName, Group group)
    {
    	return permission.removeGroupFromOptions(group, permOptionName) > 0;
    }
    
    public final Collection<Group> getMemberUsersPermissionAllowedGroups(User user, EsfName permOptionName)
    {
    	return memberUsersPermission.getAllowedGroups(user, permOptionName);
    }
    public final boolean addMemberUsersPermissionAllowedGroups(EsfName permOptionName, Group group)
    {
    	return memberUsersPermission.addGroupToOptions(group, permOptionName) > 0;
    }
    public final boolean removeMemberUsersPermissionAllowedGroups(EsfName permOptionName, Group group)
    {
    	return memberUsersPermission.removeGroupFromOptions(group, permOptionName) > 0;
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
    @SuppressWarnings("unchecked")
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; pathName: " + pathName + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
    		lastUpdatedTimestamp = new EsfDateTime();
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;

            	permission.save(con);
            	memberUsersPermission.save(con);
            	properties.save(con);
                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_group (id,parent_group_id,path_name,description,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,status,properties_blob_id) VALUES(?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(parentGroupId);
                stmt.set(pathName);
                stmt.set(description);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(status);
                stmt.set(properties.getId());
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; pathName: " + pathName);
                    return false;
                }
                
                // If we have any users to add, let's add them in now. Any in our remove list are ignored since this is a new insert and are not really in it anyway.
                TreeSet<User> userset = new TreeSet<User>(memberUsers);
                onSaveUsersToRemove.clear();
                if ( onSaveUsersToAdd.size() > 0 )
                {
                	stmt.close(); stmt = null;
                    stmt = new EsfPreparedStatement(con,
                            "INSERT INTO esf_group_user (group_id,user_id) VALUES (?,?)");
                    for( User u : onSaveUsersToAdd )
                    {
                        stmt.set(id);
                        stmt.set(u.getId());
                        stmt.executeUpdate();
                        u.addMemberOfGroup(this);
                        userset.add(u);
                    }
                    onSaveUsersToAdd.clear();
                }
                memberUsers = userset;
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.add(this);
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new group " + getPathName()); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new group " + getPathName());
                }
                
                return true;
            }
            
            // We don't allow updates on the reserved pseudo groups since they are not real groups but are placeholders only
            if ( isAllUsersGroup() || isExternalUsersGroup() )
            {
            	_logger.error("save(con,user) INVALID FOR RESERVED PSEUDO-GROUPS - id: " + id + "; pathName: " + pathName + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            	return false;
            }
            
            if ( permission.hasChanged() )
            	permission.save(con);

            if ( memberUsersPermission.hasChanged() )
            	memberUsersPermission.save(con);
            
            if ( properties.hasChanged() )
                properties.save(con);
            
            // If we have any users to remove, let's remove them now
            TreeSet<User> userset = (TreeSet<User>)memberUsers.clone();
            if ( onSaveUsersToRemove.size() > 0 )
            {
                stmt = new EsfPreparedStatement( con, 
                        "DELETE FROM esf_group_user WHERE group_id=? AND user_id=?"
                                           	   );
                for( User u : onSaveUsersToRemove )
                {
                    stmt.set(id);
                    stmt.set(u.getId());
                    stmt.executeUpdate();
                    userset.remove(u);
                    u.removeMemberOfGroup(this);
                }
                onSaveUsersToRemove.clear();
                stmt.close(); stmt = null;
            }

            // If we have any users to add, let's add them in now.
            if ( onSaveUsersToAdd.size() > 0 )
            {
                stmt = new EsfPreparedStatement( con, 
                        "INSERT INTO esf_group_user (group_id,user_id) VALUES (?,?)"
                                           );
                for( User u : onSaveUsersToAdd )
                {
                    stmt.set(id);
                    stmt.set(u.getId());
                    stmt.executeUpdate();
                    userset.add(u);
                    u.addMemberOfGroup(this);
                }
                onSaveUsersToAdd.clear();
                stmt.close(); stmt = null;
            }
            memberUsers = userset;

            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_group SET path_name=?,description=?,status=?,last_updated_timestamp=?,last_updated_by_user_id=? WHERE id=?"
                						   );
                stmt.set(pathName);
                stmt.set(description);
                stmt.set(status);
            	stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for group id: " + id + "; pathName: " + pathName);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated group " + getPathName() + "; status; " + getStatus() + "; numMembers: " + memberUsers.size()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated group " + getPathName() + "; status; " + getStatus() + "; numMembers: " + memberUsers.size());
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; pathName: " + pathName + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    private synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; pathName: " + pathName + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use in the transaction definition.
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of group that was pending an INSERT id: " + id + "; pathName: " + pathName);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        PreparedStatement stmt = null;
        
        try
        {
            // Delete the group
            stmt = con.prepareStatement("DELETE FROM esf_group WHERE id=?");
            stmt.setObject(1,id.getUUID(),Types.OTHER);
            int num = stmt.executeUpdate();
            if ( num == 0 )
            	_logger.warn("delete(con) - Failed to find esf_group database row to delete with id: " + id);
            
            // Delete group membership info
            stmt.close(); stmt = null;
            stmt = con.prepareStatement("DELETE FROM esf_group_user WHERE group_id=?");
            stmt.setObject(1,id.getUUID(),Types.OTHER);
            stmt.executeUpdate();

            // Delete our properties and permissions
            properties.delete(con);
            permission.delete(con);
            memberUsersPermission.delete(con);
            
            // Update any groups that pointed to this group as the parent to point to this group's parent instead
            stmt.close(); stmt = null;
            stmt = con.prepareStatement("UPDATE esf_group SET parent_group_id=? WHERE parent_group_id=?");
            if ( hasParentGroup() )
            	stmt.setObject(1,parentGroupId.getUUID(),Types.OTHER);
            else
            	stmt.setNull(1,Types.OTHER);
            stmt.setObject(2,id.getUUID(),Types.OTHER);
            stmt.executeUpdate();
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted group " + getPathName() + "; status; " + getStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted group " + getPathName() + "; status; " + getStatus());
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on id: " + id + "; pathName: " + pathName + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	Application.getInstance().cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
		// We never allow the reserved groups to be deleted
		if ( isReserved() )
		{
	    	_logger.error("checkReferential() FAILED RESERVE PATH PREFIX on id: " + id + "; pathName: " + pathName);
	    	if ( errors != null )
	    		errors.addError("You cannot delete the reserved special group: " + getPathName());
			return false;
		}

	    EsfPreparedStatement stmt = null;
	    
    	EsfPathName permPathName = getPermPathName();
    	EsfPathName memberUsersPermPathName = getMemberUsersPermPathName();
	    	    
	    try
	    {
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT path_name, permission_option_esfname FROM esf_permission_option_group, esf_permission "  +
	                "WHERE esf_permission_option_group.group_id = ? AND esf_permission_option_group.permission_id = esf_permission.id AND path_name <> ? AND path_name <> ?"
	                                   );
	        stmt.set(id);
	        stmt.set(permPathName); // we don't want to find permissions that belong to the group we're deleting since that will clearly be in use
	        stmt.set(memberUsersPermPathName);
	        EsfResultSet rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	do
	        	{
		        	EsfPathName pathName = rs.getEsfPathName();
		            EsfName optionName = rs.getEsfName();
			    	if ( errors != null )
			    		errors.addError("The group is still referenced in the permission " + pathName + ", option " + optionName + ".");
	        	} while ( rs.next() );
	        	 
	        	return false;
	        }
	        
	        stmt.close();
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT U.email FROM esf_group_user GU, esf_user U WHERE GU.group_id=? AND GU.user_id = U.id"
	                                   );
	        stmt.set(id);
	        rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	do
	        	{
		        	String email = rs.getString();
			    	if ( errors != null )
			    		errors.addError("The group is still linked to member user " + email + ".");
	        	} while ( rs.next() );
	        	 
	        	return false;
	        }
	        
	        stmt.close();
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT PT.esfname, L.path_name FROM esf_party_template PT, esf_library L WHERE PT.todo_group_id=? AND PT.container_id = L.id"
	                                   );
	        stmt.set(id);
	        rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	do
	        	{
		        	EsfName esfname = rs.getEsfName();
		        	EsfPathName libPathName = rs.getEsfPathName();
			    	if ( errors != null )
			    		errors.addError("The group is set as the To Do group for party template '" + esfname + "' in library '" + libPathName + "'.");
	        	} while ( rs.next() );
	        	 
	        	return false;
	        }
	        
	        stmt.close();
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT COUNT(*) FROM esf_transaction_party WHERE todo_group_id=?"
	                                   );
	        stmt.set(id);
	        rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	int numMatches = rs.getInt();
	        	if ( numMatches > 0 )
	        	{
			    	if ( errors != null )
			    		errors.addError("The group is set as the To Do group for " + numMatches + " transaction(s).");
		    		return false;
	        	}
	        }
	        
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   	    private static PathNameUUIDUserListableCacheReadOptimized<Group> cache;
   	    
   	    static 
   	    {
   	    	loadCache();
   	    }
   	    
   	    /**
   	     * Loads the Group Cache
   	     */
   	    private static void loadCache()
   	    {
   	    	_logger.debug("Manager.loadCache()");

   	    	cache = new PathNameUUIDUserListableCacheReadOptimized<Group>();
   	        
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        EsfPreparedStatement stmt2 = null;
   	        
   	        try
   	        {
   	            stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id,parent_group_id,path_name,description,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,status,properties_blob_id FROM esf_group"
   	            						   );
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            EsfUUID parentGroupId = rs.getEsfUUID();
		            EsfPathName pathName = rs.getEsfPathName();
		            String description = rs.getString();
		            EsfDateTime created = rs.getEsfDateTime();
		            EsfUUID createdBy   = rs.getEsfUUID();
		            EsfDateTime lastUpdated = rs.getEsfDateTime();
		            EsfUUID lastUpdatedBy   = rs.getEsfUUID();
		            String  stat = rs.getString();
		            EsfUUID propId      = rs.getEsfUUID();
		            
		            Record properties  = Record.Manager.getById(con,propId);
		            
		            Group group = new Group(id,parentGroupId,pathName,description,created,createdBy,lastUpdated,lastUpdatedBy,stat,properties,null,null);
		            
		            // Load user members now...
		   	        stmt2 = new EsfPreparedStatement( con, "SELECT user_id from esf_group_user where group_id=?");
		            stmt2.set(id);
		            EsfResultSet rs2 = stmt2.executeQuery();
		            while( rs2.next() )
		            {
		            	EsfUUID userId = rs2.getEsfUUID();
		            	User user = User.Manager.getById(userId);
		            	if ( user != null )
		            	{
		            		group.memberUsers.add(user);
		            		user._addMemberOfGroupDuringInitializationOnly(group);
		            	}
		            	else
		            		_logger.error("Manager.loadCache() pathName: " + pathName + "; has MISSING group member user id: " + userId);
		            }
		            stmt2.close(); stmt2 = null;
		            
		            group.setLoadedFromDb();
		            cache._addDuringInitializationOnly(group);
	            }
	            
   	            con.commit();
   	            
   	            // On initial load, our group permission lists are not yet set because permissions rely on the groups
   	            // themselves be accessible, so now we set them up correctly.
   	            for( Group group : cache.getAllById() )
   	            	group._initialization_loadPermissions();
   	            
   	            _logger.debug("Manager.loadCache() size: " + cache.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.loadCache() - clearing " + cache.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	cache.clear();
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt2);
   	            cleanupPool(pool,con,stmt);
   	        }
   	    }

   	    public static void reloadCache()
   	    {
   	    	cache.clear();
   	    	loadCache();
   	    }

   	    /**
   	     * Creates a new group like an existing group.  The new group is automatically added to all of
   	     * the permission lists (it can always list/view/add/change/delete itself unless changed by the creator or others with permission).
   	     * @param likeGroup the Group that we'll be cloning from
   	     * @param esfname the new group's name
   	     * @param createdBy the User who is creating the new group
   	     * @return
   	     */
   	    public static Group createLike(Group likeGroup, EsfPathName pathName, User createdBy)
   	    {
   	    	// We don't let you create like reserved groups or create new reserved groups
   	    	if ( likeGroup.isReserved() || pathName.hasReservedPathPrefix() )
   	    		return null;

   	    	if ( ! likeGroup.permission.hasPermission(createdBy, PermissionOption.PERM_OPTION_CREATELIKE) )
   	    		return null;
   	    	
   	        EsfUUID id = new EsfUUID();
   	        EsfDateTime nowTimestamp = new EsfDateTime();
   	        // Copy over preferences, etc.
   	        Record properties = new Record( new EsfName("groupProps"), likeGroup.getProperties() );
   	        
   	        String normalizedId = id.toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(GROUP_PERM_PATH_PREFIX+normalizedId);
   	    	EsfPathName memberUsersPermPathName = new EsfPathName(MEMBER_USERS_PERM_PATH_PREFIX+normalizedId);
   	        Permission newPermission = Permission.Manager.createLike(permPathName, likeGroup.permission);
   	        Permission newMemberUsersPermission = Permission.Manager.createLike(memberUsersPermPathName, likeGroup.memberUsersPermission);

   	        // Note that the group's permissions can contain groups that even the creator doesn't have access to, but
   	        // they remain in the new group (so they can have permission) and the user can't even see them so 
   	        // cannot modify them away.
   	    	Group group = new Group( id, likeGroup.getId(), pathName, likeGroup.getDescription(), 
   	    							 nowTimestamp, createdBy.getId(), nowTimestamp, createdBy.getId(),
   	    						     likeGroup.getStatus(), properties, newPermission, newMemberUsersPermission
   	    						   );
   	    	
   	    	// Let the new group have full permission to this group and its member users.
   	    	group.permission.addGroupToAllOptions(group);
   	    	group.memberUsersPermission.addGroupToAllOptionsExceptManageToDo(group); // We don't add the new group to manage to do permission.
   	    	return group;
   	    }
   	    
   	    // Only called by DbSetup
   	    public static Group _dbsetup_createSuperGroup()
   	    {
   	        EsfDateTime nowTimestamp = new EsfDateTime();

   	        Record properties = new Record( new EsfName("groupProps"), BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE );

   	        EsfUUID deployId = Application.getInstance().getDeployId();
   	        
   	    	Group group = new Group( new EsfUUID(), null, SUPER_GROUP_ESFPATHNAME, "Contains members of the super group.", nowTimestamp, deployId, 
   	    							 nowTimestamp, deployId, Literals.STATUS_ENABLED, properties, null, null );

   	    	String normalizedId = group.getId().toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(GROUP_PERM_PATH_PREFIX+normalizedId);
   	    	EsfPathName memberUsersPermPathName = new EsfPathName(MEMBER_USERS_PERM_PATH_PREFIX+normalizedId);
   	    	group.permission = Permission.Manager._dbsetup_createForSuperGroup(permPathName, "Super group - permission", group);
   	    	group.memberUsersPermission = Permission.Manager._dbsetup_createGroupMemberUserPermissionsForSuperGroup(memberUsersPermPathName, "Super group - member users permission", group);
   	    	return group;
   	    }
   	    
   	    //////////////////////////////////////////////////////////////
   	    // Only called by DbSetup
   	    //////////////////////////////////////////////////////////////
   	    public static int _dbsetup_addManageToDoPermissionToMemberUsersFor13_4_13() // just for the 13.4.13 release that added a new permission to the Group member users
   	    {
   	    	int numUpdated = 0;
   	    	
   	    	Group superGroup = Manager.getSuperGroup();
   	    	
   	    	for( Group group : Manager.getAll() )
   	    	{
   	    		if ( group.isExternalUsersGroup() || group.isAllUsersGroup() )
   	    			continue;
   	    		
   	    		PermissionOption option = group.memberUsersPermission.getOptionByName(PermissionOption.GROUP_MEMBER_USER_PERM_OPTION_MANAGE_TO_DO);
   	    		if ( option == null )
   	    		{
   	   	    		option = PermissionOption.Manager._dbsetup_createForSuperGroup(PermissionOption.GROUP_MEMBER_USER_PERM_OPTION_MANAGE_TO_DO, PermissionOption.GROUP_MEMBER_USER_PERM_OPTION_MANAGE_TO_DO + " - Super group - member users permission", superGroup);
   	   	    		group.memberUsersPermission.options.add(option);
   	   	    		group.memberUsersPermission.setDescription(group.memberUsersPermission.getDescription()); // dummy to make sure object is marked as updated so save works
   	   	    		if ( group.save() )
   	   	    			++numUpdated;
   	    		}
   	    	}
   	    	
   	    	return numUpdated;
   	    }
   	    
   	    public static Group _dbsetup_createAllUsersGroup()
   	    {
   	        EsfDateTime nowTimestamp = new EsfDateTime();

   	        Record properties = new Record( new EsfName("groupProps"), BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE );

  	        EsfUUID deployId = Application.getInstance().getDeployId();
   	        
   	    	Group group = new Group( new EsfUUID(), null, ALL_USERS_GROUP_ESFPATHNAME, "Pseudo-group contains all users.", nowTimestamp, deployId, 
   	    							 nowTimestamp, deployId, Literals.STATUS_ENABLED, properties, null, null );

   	    	String normalizedId = group.getId().toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(GROUP_PERM_PATH_PREFIX+normalizedId);
   	    	EsfPathName memberUsersPermPathName = new EsfPathName(MEMBER_USERS_PERM_PATH_PREFIX+normalizedId);
   	    	group.permission = Permission.Manager._dbsetup_createForAllUsersGroup(permPathName, "All users group - permission", group);
   	    	group.memberUsersPermission = Permission.Manager.createWithStandardGroupMemberOptions(memberUsersPermPathName, "All users - member users permission");
   	    	return group;
   	    }
   	    
   	    // Only called by DbSetup
   	    public static Group _dbsetup_createExternalUsersGroup(Group allUsersGroup)
   	    {
   	        EsfDateTime nowTimestamp = new EsfDateTime();

   	        Record properties = new Record( new EsfName("groupProps"), BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE );

  	        EsfUUID deployId = Application.getInstance().getDeployId();
   	        
   	    	Group group = new Group( new EsfUUID(), null, EXTERNAL_USERS_GROUP_ESFPATHNAME, "Pseudo-group contains external users.", nowTimestamp, deployId, 
   	    							 nowTimestamp, deployId, Literals.STATUS_ENABLED, properties, null, null );

   	    	String normalizedId = group.getId().toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(GROUP_PERM_PATH_PREFIX+normalizedId);
   	    	EsfPathName memberUsersPermPathName = new EsfPathName(MEMBER_USERS_PERM_PATH_PREFIX+normalizedId);
   	    	group.permission = Permission.Manager._dbsetup_createForExternalUsersGroup(permPathName, "External users group - permission", allUsersGroup);
   	    	group.memberUsersPermission = Permission.Manager.createWithStandardGroupMemberOptions(memberUsersPermPathName, "External users - member users permission");
   	    	return group;
   	    }
   	    
   	    //
   	    

   	    // Only called by DbSetup -- note that this group is an effective super group like user that controls the system, but is one-below the super-group
   	    // so that the deployment user and group are effectively invisible and cannot be locked out by errors in configuration from regular users or even the customer system admin.
   	    public static Group _dbsetup_createSystemAdminGroup()
   	    {
   	        EsfDateTime nowTimestamp = new EsfDateTime();

   	        Record properties = new Record( new EsfName("groupProps"), BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE );

 	        EsfUUID deployId = Application.getInstance().getDeployId();
   	        
   	    	Group group = new Group( new EsfUUID(), null, SYSTEM_ADMIN_GROUP_ESFPATHNAME, "Contains members of the system administrator group.", nowTimestamp, deployId, 
   	    							 nowTimestamp, deployId, Literals.STATUS_ENABLED, properties, null, null );

   	    	String normalizedId = group.getId().toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(GROUP_PERM_PATH_PREFIX+normalizedId);
   	    	EsfPathName memberUsersPermPathName = new EsfPathName(MEMBER_USERS_PERM_PATH_PREFIX+normalizedId);
   	    	group.permission = Permission.Manager.createWithStandardOptions(permPathName, "Group permission");
   	    	group.permission.addGroupToAllOptions(group);
   	    	group.memberUsersPermission = Permission.Manager.createWithStandardGroupMemberOptions(memberUsersPermPathName, "Member users permission");
   	    	group.memberUsersPermission.addGroupToAllOptions(group);
   	    	return group;
   	    }
   	    

   	    /**
   	     * Returns the SuperGroup group 
   	     * @return the Group that represents the SuperGroup
   	     */
   	    public static Group getSuperGroup()
   	    {
   	    	return getByPathName(SUPER_GROUP_ESFPATHNAME);
   	    }
   	     
   	    /**
   	     * Returns the SystemAdmin group 
   	     * @return the Group that represents the SystemAdmin
   	     */
   	    public static Group getSystemAdminGroup()
   	    {
   	    	return getByPathName(SYSTEM_ADMIN_GROUP_ESFPATHNAME);
   	    }
   	     
   	    /**
   	     * Returns the AllUsersGroup group 
   	     * @return the Group that represents all users
   	     */
   	    public static Group getAllUsersGroup()
   	    {
   	    	return getByPathName(ALL_USERS_GROUP_ESFPATHNAME);
   	    }
   	     
   	    /**
   	     * Returns the ExternalUsersGroup group 
   	     * @return the Group that represents all external users
   	     */
   	    public static Group getExternalUsersGroup()
   	    {
   	    	return getByPathName(EXTERNAL_USERS_GROUP_ESFPATHNAME);
   	    }
   	     
   	    /**
   	     * 
   	     * Retrieves a Group object based on the supplied path name.
   	     * @param pathName the EsfPathName name of the group to retrieve
   	     * @return the Group object as loaded from the database if found; else null
   	     */
   	    public static Group getByPathName(final EsfPathName pathName)
   	    {
   	    	_logger.debug("Manager.getByPathName() for pathName: " + pathName);
   	        return cache.getByPathName(pathName);
   	    }
   	    
   	    /**
   	     * 
   	     * Retrieves a Group object based on the supplied pathName.
   	     * @param esfname the EsfPathName pathName of the group to retrieve
   	     * @param u the User to limit visibility list to
   	     * @return the Group object as loaded from the database if found; else null
   	     */
   	    public static Group getByPathName(final EsfPathName pathName, final User u)
   	    {
   	    	_logger.debug("Manager.getByPathName() for pathName: " + pathName + "; for user: " + u.getEmail());
   	        return cache.getByPathName(pathName,u);
   	    }
   	    
   	    
   	    /**
   	     * 
   	     * Retrieves a Group object based on the supplied id.
   	     * @param id the EsfUUID group id to retrieve
   	     * @return the Group object as loaded from the database if found; else null
   	     */
   	    public static Group getById(final EsfUUID id)
   	    {
   	    	//_logger.debug("Manager.getById() for id: " + id);
   	        return cache.getById(id);
   	    }
   	    
   	    /**
   	     * 
   	     * Retrieves a Group object based on the supplied id.
   	     * @param id the EsfUUID group id to retrieve
   	     * @param u the User to limit visibility list to
   	     * @return the Group object as loaded from the database if found; else null
   	     */
   	    public static Group getById(final EsfUUID id, final User u)
   	    {
   	    	_logger.debug("Manager.getById() for id: " + id + "; for user: " + u.getEmail());
   	        return cache.getById(id,u);
   	    }
   	    
   	    /**
   	     * Returns the total number of group objects
   	     * @return the total number of group objects
   	     */
   	    public static int getNumGroups()
   	    {
   	    	return cache.size();
   	    }
   	    public static int getNumGroups(INCLUDE include)
   	    {
   	    	if ( include == INCLUDE.BOTH_ENABLED_AND_DISABLED )
   	    		return getNumGroups();
   	    	
   	    	int count = 0;
   	    	for( Group group : cache.getAllById() )
   	    	{
   	    		if ( include == INCLUDE.ONLY_ENABLED && group.isEnabled() )
   	    			++count;
   	    		else if ( include == INCLUDE.ONLY_DISABLED && group.isDisabled() )
   	    			++count;
   	    	}
   	    	return count;
   	    }

   	    /**
   	     * Retrieves all Group objects 
   	     * @return the Collection of Groups found ordered by path name
   	     */
   	    public static Collection<Group> getAll()
   	    {
   	    	_logger.debug("Manager.getAll() cache size: " + cache.size());
   	       	return cache.getAllByPathName();
   	    }
   	    public static Collection<Group> getAll(INCLUDE include)
   	    {
   	    	if ( include == INCLUDE.BOTH_ENABLED_AND_DISABLED )
   	    		return getAll();
   	    	
   	    	LinkedList<Group> groupList = new LinkedList<Group>();
   	    	for( Group group : cache.getAllByPathName() )
   	    	{
   	    		if ( include == INCLUDE.ONLY_ENABLED && group.isEnabled() )
   	    			groupList.add(group);
   	    		else if ( include == INCLUDE.ONLY_DISABLED && group.isDisabled() )
   	    			groupList.add(group);
   	    	}
   	    	return groupList;
   	    }
   	    
   	    public static Collection<Group> getForUserWithPermission(User user, EsfName permOptionName)
   	    {
   	    	if ( PermissionOption.PERM_OPTION_LIST.equals(permOptionName) )
   	    		return cache.getAllByPathName(user); // Only gets active Groups the user can list
   	    	
   	    	LinkedList<Group> groupList = new LinkedList<Group>();
   	    	for( Group g : cache.getAllByPathName(user) ) 
   	    	{
   	    		if ( g.hasPermission(user, permOptionName) )
   	    			groupList.add(g);
   	    	}
   	    	return groupList;
   	    }
   	    public static Collection<Group> getForUserWithPermission(User user, EsfName permOptionName,INCLUDE include)
   	    {
   	    	if ( include == INCLUDE.BOTH_ENABLED_AND_DISABLED )
   	    		return getForUserWithPermission(user,permOptionName);
   	    	
   	    	LinkedList<Group> groupList = new LinkedList<Group>();
   	    	for( Group g : cache.getAllByPathName(user) ) 
   	    	{
   	    		if ( g.hasPermission(user, permOptionName) )
   	    		{
   	   	    		if ( include == INCLUDE.ONLY_ENABLED && g.isEnabled() )
   	   	    			groupList.add(g);
   	   	    		else if ( include == INCLUDE.ONLY_DISABLED && g.isDisabled() )
   	   	    			groupList.add(g);
   	    		}
   	    	}
   	    	return groupList;
   	    }
   	    public static Collection<Group> getForUserWithListPermission(User user)
   	    {
   	    	return getForUserWithPermission(user, PermissionOption.PERM_OPTION_LIST);
   	    }
   	    public static Collection<Group> getForUserWithListPermission(User user,INCLUDE include)
   	    {
   	    	return getForUserWithPermission(user, PermissionOption.PERM_OPTION_LIST,include);
   	    }
   	    
   	    
   	    public static Collection<Group> getForUserWithMemberUsersPermission(User user, EsfName permOptionName)
   	    {
   	    	if ( user == null )
   	    		return new LinkedList<Group>();

   	    	LinkedList<Group> groupList = new LinkedList<Group>();
   	    	for( Group g : cache.getAllByPathName(user) )
   	    	{
   	    		if ( g.hasMemberUsersPermission(user, permOptionName))
   	    			groupList.add(g);
   	    	}
   	    	return groupList;
   	    }
   	    public static Collection<Group> getForUserWithMemberUsersPermission(User user, EsfName permOptionName,INCLUDE include)
   	    {
   	    	if ( include == INCLUDE.BOTH_ENABLED_AND_DISABLED )
   	    		return getForUserWithMemberUsersPermission(user,permOptionName);
   	    	
   	    	if ( user == null )
   	    		return new LinkedList<Group>();

   	    	LinkedList<Group> groupList = new LinkedList<Group>();
   	    	for( Group g : cache.getAllByPathName(user) )
   	    	{
   	    		if ( g.hasMemberUsersPermission(user, permOptionName))
   	    		{
   	   	    		if ( include == INCLUDE.ONLY_ENABLED && g.isEnabled() )
   	   	    			groupList.add(g);
   	   	    		else if ( include == INCLUDE.ONLY_DISABLED && g.isDisabled() )
   	   	    			groupList.add(g);
   	    		}
   	    	}
   	    	return groupList;
   	    }
   	    public static Collection<Group> getForUserWithListMemberUsersPermission(User user)
   	    {
   	    	return getForUserWithMemberUsersPermission(user, PermissionOption.PERM_OPTION_LIST);
   	    }
   	    public static Collection<Group> getForUserWithListMemberUsersPermission(User user,INCLUDE include)
   	    {
   	    	return getForUserWithMemberUsersPermission(user, PermissionOption.PERM_OPTION_LIST,include);
   	    }
   	    public static Collection<Group> getForUserWithUpdateMemberUsersPermission(User user)
   	    {
   	    	return getForUserWithMemberUsersPermission(user, PermissionOption.PERM_OPTION_UPDATE);
   	    }
   	    public static Collection<Group> getForUserWithUpdateMemberUsersPermission(User user,INCLUDE include)
   	    {
   	    	return getForUserWithMemberUsersPermission(user, PermissionOption.PERM_OPTION_UPDATE,include);
   	    }
   	    public static Collection<Group> getForUserWithCreateLikeMemberUsersPermission(User user)
   	    {
   	    	return getForUserWithMemberUsersPermission(user, PermissionOption.PERM_OPTION_CREATELIKE);
   	    }
   	    public static Collection<Group> getForUserWithCreateLikeMemberUsersPermission(User user,INCLUDE include)
   	    {
   	    	return getForUserWithMemberUsersPermission(user, PermissionOption.PERM_OPTION_CREATELIKE,include);
   	    }
   	    public static Collection<Group> getForUserWithDeleteMemberUsersPermission(User user)
   	    {
   	    	return getForUserWithMemberUsersPermission(user, PermissionOption.PERM_OPTION_DELETE);
   	    }
   	    public static Collection<Group> getForUserWithDeleteMemberUsersPermission(User user,INCLUDE include)
   	    {
   	    	return getForUserWithMemberUsersPermission(user, PermissionOption.PERM_OPTION_DELETE,include);
   	    }
   	    public static Collection<Group> getForUserWithManageToDoMemberUsersPermission(User user)
   	    {
   	    	return getForUserWithMemberUsersPermission(user, PermissionOption.GROUP_MEMBER_USER_PERM_OPTION_MANAGE_TO_DO);
   	    }
   	    public static Collection<Group> getForUserWithManageToDoMemberUsersPermission(User user,INCLUDE include)
   	    {
   	    	return getForUserWithMemberUsersPermission(user, PermissionOption.GROUP_MEMBER_USER_PERM_OPTION_MANAGE_TO_DO,include);
   	    }
   	    
   	    public static Collection<User> getAllMemberUsersForUserWithMemberUsersPermission(User user, EsfName memberUsersPermissionOption)
   	    {
   	    	Collection<Group> memberUserGroups = getForUserWithMemberUsersPermission(user, memberUsersPermissionOption);
   	    	TreeSet<User> memberUsers = new TreeSet<User>(new UserEmailComparator());
   	    	for( Group g : memberUserGroups )
   	    	{
   	    		Collection<User> userList = g.getAllMemberUsers(user);
   	    		for( User u : userList )
   	    		{
   	   	    		if ( ! memberUsers.contains(u) )
   	   	    			memberUsers.add(u);
   	    		}
   	    	}
   	    	return memberUsers;
   	    }
   	    public static Collection<User> getAllMemberUsersForUserWithListMemberUsersPermission(User user)
   	    {
   	    	return getAllMemberUsersForUserWithMemberUsersPermission(user, PermissionOption.PERM_OPTION_LIST);
   	    }
   	    public static Collection<User> getAllMemberUsersForUserWithUpdateMemberUsersPermission(User user)
   	    {
   	    	return getAllMemberUsersForUserWithMemberUsersPermission(user, PermissionOption.PERM_OPTION_UPDATE);
   	    }
   	    public static Collection<User> getAllMemberUsersForUserWithViewDetailsMemberUsersPermission(User user)
   	    {
   	    	return getAllMemberUsersForUserWithMemberUsersPermission(user, PermissionOption.PERM_OPTION_VIEWDETAILS);
   	    }
   	    public static Collection<User> getAllMemberUsersForUserWithManageToDoMemberUsersPermission(User user)
   	    {
   	    	return getAllMemberUsersForUserWithMemberUsersPermission(user, PermissionOption.GROUP_MEMBER_USER_PERM_OPTION_MANAGE_TO_DO);
   	    }
   	    
   	} // Manager
   	
}