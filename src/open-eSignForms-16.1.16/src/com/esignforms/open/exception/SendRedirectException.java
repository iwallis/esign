// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2010 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.exception;

/**
 * This exception is thrown by pages that want the caller to redirect to a new URL.
 *
 * @author Yozons, Inc.
 */
public class SendRedirectException 
	extends EsfException
{
	private static final long serialVersionUID = 5880104659823191487L;

	private String url;
	private boolean isError;

    /**
     * Send a redirect to the specified URL. It is not considered an error, so if possible, we'll commit any DB
     * changes before the redirect takes place
     */
    public SendRedirectException(String url)
    {
    	this(url,false);
    }

    /**
     * Sends a redirect to the specified URL. If isError is true, it will do the redirect after rolling back any DB changes.
     * @param url 
     * @param isError
     */
    public SendRedirectException(String url, boolean isError)
    {
        super();
        this.url = url;
        this.isError = isError;
    }

    /**
     * Returns a string version of the exception such that it shows it was a SendRedirectException.
     * @return the String showing this exception.
     */
    public String toLabeledString()
    {
        return "SendRedirectException: " + super.toString() + "; URL: " + url + "; isError: " + isError;
    }
    
    public String getUrl()
    {
    	return url;
    }
    public boolean isError()
    {
    	return isError;
    }
}