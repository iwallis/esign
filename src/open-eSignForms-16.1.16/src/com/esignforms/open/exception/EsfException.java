// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2009 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.exception;

/**
 * This exception is the base class for all Open eSignForms exceptions. Often, this is thrown directly
 * to tell the caller whether it's some other exception or something occurring in ESF itself.
 *
 * @author Yozons, Inc.
 */
public class EsfException 
	extends java.lang.Exception
{
    static final long serialVersionUID = 1731397468224492757L;

    /**
     * Constructs an "empty" exception with no text
     */
    public EsfException()
    {
        super();
    }
    public EsfException(Exception e)
    {
        super(e);
    }

    /**
     * Constructs an exception with the specified text.
     * @param errorText the String that describes the exception that occurred
     */
    public EsfException(String errorText)
    {
        super(errorText);
    }
     
    /**
     * Returns a string version of the exception such that it shows it was a EsfException.
     * @return the String showing this exception.
     */
    public String toLabeledString()
    {
        return "EsfException: " + super.toString();
    }
}