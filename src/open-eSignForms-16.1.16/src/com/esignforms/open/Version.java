// Copyright (C) 2009-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open;

/**
 * Holds information about the version of the system that's running.
 * 
 * The MAJOR number reflects the year of the release.
 * 
 * The MINOR number reflects the month of the release.
 * 
 * The UPDATE string reflects the day of the month of the release, whether is a pre-release or subsequent patch.
 * 
 * @author Yozons, Inc. 
 */
public class Version 
{
	// History of release names and versions:
	// Open eSignForms 0.0 - Start of the Open eSignForm custom development toolkit on 2009-07-06.
	// Open eSignForms 0.1 - Begin Dojo test login/dashboard on 2009-09-23.
	// Open eSignForms 0.2 - Database and basic encryption routines in place on 2009-10-13.
	// Open eSignForms 0.3 - Merged server code into consolidated GWT project and CVS with 
	//						 ability to GWT login/view main menu framework/logout in place on 2009-11-08.
	// Open eSignForms 0.4 - Switched to Vaadin for GUI tool for building GWT clients on 2010-01-20. 
	// Open eSignForms 0.5 - Begin with new code base for User/Group/Permission after testing out Vaadin on 2010-05-01. 
	// Open eSignForms 0.6 - Begin build out of Library on 2010-07-12, integration with CKEditor. Document+version builder and tester. Basic test package and transaction template and taglibs.
	// Open eSignForms 0.7 - Continue build out of Library on 2010-11-11, with parties, fields, re-org caches, styles, drop down list builder, more full run-time object build-out of the configuration templates.
	// Open eSignForms 0.8 - Packages and transaction configuration on 2011-01-04. Includes allowing production and/or test transactions to be started from the navigation tree. Simple transaction report.
	//                       Added email send and bounce-handler components. Added 4096-bit RSA keypair generation. Added RSA+SHA-512 XML Digital Signatures. 
	//                       Standard transaction workflow (parties process in order, each processing documents in order).
	// Open eSignForms 0.9 - Work to add finalizing features while fixing bugs in place on 2011-04-15.
	//                       Added transaction detail view and data and snapshot download to transaction listing report.
	//                       Added signature field, sign date field, electronic signature process record, checkbox field, drop down field, date field, email field, file upload field, SSN field, 
	//						 	zip code field, integer field, textarea field.
	//						 Added To Do listing. Added user session listing. Updated document page editor.
	//                       First release shown to partner forms developers on the demo system for feedback, basic training.
	// Open eSignForms 1.0 - First commercial release on 2011-06-01.
	// Open eSignForms 1.1 - Added radio buttons and rich text editor field on 2011-06-03.
	// (See versionHistory.jsp for details on releases 1.2 and later as it has more details. After 1.6, we switched to a year.month.day[_patch] version scheme.)
	protected final static int    MAJOR  		= 16;
	protected final static int    MINOR  		= 1;
	protected final static String UPDATE 		= "16";
	protected final static String APP_NAME     	= "Open eSignForms";
	
	protected static String ReleaseString      	= APP_NAME + " v" + MAJOR + "." + MINOR + "." + UPDATE;
	protected static String VersionString      	= MAJOR + "." + MINOR + "." + UPDATE;
	
	// Copyright notice is also embedded in our JARs and this class file.
	protected final static String HTML_COPYRIGHT = "&copy; 2016 Yozons, Inc."; // Our convention for web display is to show only the latest copyright year (when it's actually displayed).
    protected final static String TEXT_COPYRIGHT = "Copyright (c) 2009-2016 Yozons, Inc."; // Our convention for embedded copyright includes our full term of copyrights as it relates to this application.
    protected final static String PROPRIETARY = 
       	"This software was jumpstarted, with permission, by using the properietary software originally written by Yozons starting in 2001, including Signed & Secure and eSignForms which are protected by U.S. Patent No. 7,360,079.\n" +
    	"This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License\n" +
    	"as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\n"  +
    	"This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;\n" +
    	"without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.\n" +
    	"See the GNU Affero General Public License for more details.\n\n" +
    	"You should have received a copy of the GNU Affero General Public License along with this program.\n" +
    	"If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.\n" +
    	"Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.\n" +
    	"This software is also commercially licensed by Yozons, Inc., for non-AGPL uses.\n" +
    	"** For third-party licenses, please see http://open.esignforms.com/thirdPartySoftware.jsp\n";
    
    protected final static String DOCTYPE_XHTML1_0_TRANSITIONAL = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
    protected final static String DOCTYPE_HTML5 = "<!DOCTYPE html>";
    
	private Version()
	{
	}
    
	public static final int getMajor()
	{
		return MAJOR;
	}
	public static final int getMinor()
	{
		return MINOR;
	}
	public static final String getUpdate()
	{
		return UPDATE;
	}
	public static final String getVersionString()
	{
		return VersionString;
	}

	public static final String getReleaseString()
	{
		return ReleaseString;
	}

	public static final String getHtmlCopyright()
	{
		return HTML_COPYRIGHT;
	}
    public static final String getTextCopyright()
    {
        return TEXT_COPYRIGHT;
    }
    
    public static final String getProprietary()
    {
        return PROPRIETARY;
    }
    
    public static final String getCopyrightAndProprietary()
    {
        return getTextCopyright() + "\n" + getProprietary();
    }
    
    public static final String getDocTypeXhtml10Transitional()
    {
    	return DOCTYPE_XHTML1_0_TRANSITIONAL;
    }
}