// Copyright (C) 2010-2011 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.config;

import com.esignforms.open.Application;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.Library;

/**
 * We use this pass along CKEditorContext configuration on a per-user, per-session basis.
 * Initially it was created to support the image file browser so we can pass in the
 * document version and library to search for images to choose from.
 */
public class CKEditorContext implements java.io.Serializable
{
	private static final long serialVersionUID = 7043125191906675538L;

	protected String idInSession; // We use this id to retrieve this context from the user's session
	protected DocumentVersion documentVersion; // if present, we use this to find stuff (first in the document version's library, then in the document version itself)
	protected Library library; // if present, we use this to find stuff 
	
	public CKEditorContext()
	{
		idInSession = Application.getInstance().getRandomKey().getPickupCodeString();
	}
	
	public String getIdInSession()
	{
		return idInSession;
	}
	
	public DocumentVersion getDocumentVersion()
	{
		return documentVersion;
	}
	public void setDocumentVersion(DocumentVersion documentVersion)
	{
		this.documentVersion = documentVersion;
	}
	public boolean hasDocumentVersion()
	{
		return documentVersion != null;
	}
	
	public Library getLibrary()
	{
		return library;
	}
	public void setLibrary(Library library)
	{
		this.library = library;
	}
	public boolean hasLibrary()
	{
		return library != null;
	}
	
	public Library getTemplateLibrary()
	{
		return Library.Manager.getTemplate();
	}
}
