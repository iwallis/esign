// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2009 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.config;

import java.text.MessageFormat;

import com.esignforms.open.Application;


/**
 * Wrapper routine around the ResourceBundle to handle some common needs, like default values, etc.
 * @author Yozons, Inc.
 */
public class PropertiesFile 
{
    protected java.util.ResourceBundle bundle = null;
    
    
    public PropertiesFile(String bundleName)
        throws java.util.MissingResourceException
    {
        try
        {
            bundle = java.util.ResourceBundle.getBundle(bundleName);
        }
        catch( java.util.MissingResourceException e ) 
        {
            String msg = "PropertiesFile - Could not load properties file '" + bundleName + "': " + e.getMessage();
            Application.getInstance().err(msg);
            throw e;
        }
    }
    
    public java.util.Enumeration<String> getKeys()
    {
        return bundle.getKeys();
    }
    
    public String getString(String key)
        throws java.util.MissingResourceException
    {
        return bundle.getString(key);
    }
    
    public String getString(String key,String def)
    {
        if ( key == null )
            return def;
            
        try
        {
            return bundle.getString(key);
        }
        catch( java.util.MissingResourceException e )
        {
            return def;
        }
    }
    
    public String getContextPathUrlString(String key)
        throws java.util.MissingResourceException
    {
        String s = getString(key);
        return s == null ? null : s.replace("<contextPath>",Application.getInstance().getContextPath());
    }

    public String getContextPathUrlString(String key,String def)
    {
        String s = getString(key,def);
        if ( s != null )
            return s.replace("<contextPath>",Application.getInstance().getContextPath());
        return null;
    }
    
    public short getShort(String key,short def)
    {
        if ( key == null )
            return def;
            
        try
        {
            return Application.getInstance().stringToShort(getString(key), def);
        }
        catch( java.util.MissingResourceException e )
        {
            return def;
        }
    }
    
    public int getInteger(String key,int def)
    {
        if ( key == null )
            return def;
            
        try
        {
            return Application.getInstance().stringToInt(getString(key), def);
        }
        catch( java.util.MissingResourceException e )
        {
            return def;
        }
    }
    
    public long getLong(String key,long def)
    {
        if ( key == null )
            return def;
            
        try
        {
            return Application.getInstance().stringToLong(getString(key), def);
        }
        catch( java.util.MissingResourceException e )
        {
            return def;
        }
    }
    
    /**
     * Gets a boolean property value. 
     * @returns true if the 'key' is translated into 'true', 'yes' or '1' -- regardless of case.
     */
    public boolean getBoolean(String key,boolean def)
    {
        if ( key == null )
            return def;
            
        try
        {
            String s = getString(key);
            if ( s.equalsIgnoreCase("true") ||
                 s.equalsIgnoreCase("yes")  ||
                 s.equals("1")
               )
                return true;
                
            return false;
            
        }
        catch( java.util.MissingResourceException e )
        {
            return def;
        }
    }
    
    
    /**
     * Convenience routine to handle positional parameter substitution in the format string associated with the 
     * key.  The argument list much match the appropriate substitution parameters as defined by
     * java.text.MessageFormat
     * @see java.text.MessageFormat#format(String,Object[])
     */
    public String format( String key, Object[] args)
        throws java.util.MissingResourceException
    {
        String f = getString(key);
        return MessageFormat.format(f,args);
    }

    public String format( String key, String arg1)
        throws java.util.MissingResourceException
    {
        Object[] oa = new Object[1];
        oa[0] = arg1;
        return format(key,oa);
    }

    public String format( String key, String arg1, String arg2)
        throws java.util.MissingResourceException
    {
        Object[] oa = new Object[2];
        oa[0] = arg1;
        oa[1] = arg2;
        return format(key,oa);
    }

    public String format( String key, int arg1)
        throws java.util.MissingResourceException
    {
        Object[] oa = new Object[1];
        oa[0] = new Integer(arg1);
        return format(key,oa);
    }


    public String format( String key, String defaultFormat, Object[] args)
        throws java.util.MissingResourceException
    {
        String f = getString(key,defaultFormat);
        return MessageFormat.format(f,args);
    }
    
    public MessageFormat getFormat( String key )
        throws java.util.MissingResourceException
    {
        String f = getString(key);
        return new MessageFormat(f);
    }
    
    public MessageFormat getFormat( String key, String defaultFormat )
    {
        String f = getString(key,defaultFormat);
        return new MessageFormat(f);
    }
    
    public static final String format( MessageFormat fmt, Object[] args )
    {
        return fmt.format(args);
    }
    
    public static final String format( MessageFormat fmt, String arg1 )
    {
        Object[] oa = new Object[1];
        oa[0] = arg1;
        return fmt.format(oa);
    }
    
    public static final String format( MessageFormat fmt, String arg1, String arg2 )
    {
        Object[] oa = new Object[2];
        oa[0] = arg1;
        oa[1] = arg2;
        return fmt.format(oa);
    }
    
    public static final String format( MessageFormat fmt, int arg1 )
    {
        Object[] oa = new Object[1];
        oa[0] = new Integer(arg1);
        return fmt.format(oa);
    }
}