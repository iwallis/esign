// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.config;

import com.esignforms.open.Application;

/**
 * Various general purpose literals that are not tied directly to a specific class.
 */
public class Literals
{
	public static final String STATUS_ENABLED  = "E";
    public static final String STATUS_DISABLED = "D";
    
	public static final String Y = "Y";
    public static final String N = "N";
    
	public static final String DATE_STARTED  = "S";
    public static final String DATE_LAST_UPDATED = "U";
	public static final String DATE_CANCELS  = "C";
    public static final String DATE_EXPIRES = "X";

	public static final String DATE_TODAY  = "T";
	public static final String DATE_YESTERDAY  = "Y";
	public static final String DATE_LAST_7_DAYS  = "-7";
	public static final String DATE_LAST_30_DAYS  = "-30";
	public static final String DATE_LAST_90_DAYS  = "-90";
	public static final String DATE_LAST_365_DAYS  = "-365";
	public static final String DATE_RANGE  = "R";

    public static final int COMMENTS_MAX_LENGTH	= 1000000;
    public static final int DEPARTMENT_MAX_LENGTH = 50;
    public static final int DESCRIPTION_MAX_LENGTH = 250;
    public static final int DISPLAY_NAME_MAX_LENGTH	= 100;
    public static final int EMAIL_ADDRESS_MAX_LENGTH = 100;
    public static final int EMAIL_ADDRESS_LIST_MAX_LENGTH = 4000;
    public static final int EMAIL_SUBJECT_MAX_LENGTH = 200;
    public static final int EMAIL_TEXT_MAX_LENGTH = 16000;
    public static final int EMAIL_HTML_MAX_LENGTH = 512000;
    public static final int EMPLOYEE_ID_MAX_LENGTH = 50;
    public static final int ESFNAME_MAX_LENGTH = 50;
    public static final int EXTRA_OPTIONS_MAX_LENGTH = 4096;
    public static final int FIELD_EXPRESSION_MAX_LENGTH	= 4096;
    public static final int FILE_NAME_MAX_LENGTH = 256;
    public static final int FORGOTTEN_PASSWORD_QUESTION_MAX_LENGTH = 100;
    public static final int FORMAT_SPEC_MAX_LENGTH = 100;
    public static final int HTML_MAX_LENGTH	= 1000000;
    public static final int HTTP_RESPONSE_MAX_LENGTH = 64000;
    public static final int INPUT_PROMPT_MAX_LENGTH = 100;
    public static final int IP_HOST_ADDRESS_LENGTH = 200;
    public static final int JOB_TITLE_MAX_LENGTH = 50;
    public static final int LABEL_MAX_LENGTH = 100;
    public static final int LOCATION_MAX_LENGTH = 50;
    public static final int LOG_MAX_LENGTH = 8000;
    public static final int MIME_TYPE_MAX_LENGTH = 100;
    public static final int NAME_MAX_LENGTH	= 50;
    public static final int PATH_ESFNAME_MAX_LENGTH	= 1024; // Supports 20 levels of paths if each esfname is maxed out at 50 chars
    public static final int PHONE_NUMBER_MAX_LENGTH = 50;
    public static final int RENOTIFY_SPEC_MAX_LENGTH	= 100; // Allows all default defined renotifies to be selected, each separated by a semicolon
    public static final int REPORT_FIELD_STRING_MAX_LENGTH = 4000;
    public static final int SELECTION_OPTION_MAX_LENGTH	= 100;
    public static final int SELECTION_VALUE_MAX_LENGTH	= 250;
    public static final int SERIAL_FORMAT_MAX_LENGTH = 20;
    public static final int STATUS_TEXT_MAX_LENGTH	= 50;
    public static final int TIMER_NAME_MAX_LENGTH = 100;
    public static final int TOOLTIP_MAX_LENGTH = 100;
    
    // Default search is 'contains'.
    public static final char SEARCH_STARTS_WITH_PREFIX = '^';
    public static final char SEARCH_EXACT_PREFIX = '=';
    
    // Source types for ${} fields
    public static final String FIELD_SOURCE_TYPE_DOCUMENT = "document";
    public static final String FIELD_SOURCE_TYPE_FIELD_LABEL = "fieldlabel";
    public static final String FIELD_SOURCE_TYPE_DEFAULT = FIELD_SOURCE_TYPE_FIELD_LABEL; // by default, we'll do a field and label as defined in the field template
    public static final String FIELD_SOURCE_TYPE_FIELD = "field";
    public static final String FIELD_SOURCE_TYPE_FILE = "file";
    public static final String FIELD_SOURCE_TYPE_HTML = "html";
    public static final String FIELD_SOURCE_TYPE_HTML_PROPERTY = "htmlproperty";
    public static final String FIELD_SOURCE_TYPE_IMAGE = "image";
    public static final String FIELD_SOURCE_TYPE_LABEL = "label";
    public static final String FIELD_SOURCE_TYPE_FIELD_OUT = "out";
    public static final String FIELD_SOURCE_TYPE_PROPERTY = "property";
    public static final String FIELD_SOURCE_TYPE_SERIAL = "serial";
    public static final String FIELD_SOURCE_TYPE_TRANSACTION = "transaction";
    
    public static final String TIME_INTERVAL_UNIT_NOW = "now";
    public static final String TIME_INTERVAL_UNIT_FOREVER = "forever";
    public static final String TIME_INTERVAL_UNIT_MINUTE = "minute";
    public static final String TIME_INTERVAL_UNIT_HOUR = "hour";
    public static final String TIME_INTERVAL_UNIT_DAY = "day";
    public static final String TIME_INTERVAL_UNIT_WEEKDAY = "weekday";
    public static final String TIME_INTERVAL_UNIT_WEEK = "week";
    public static final String TIME_INTERVAL_UNIT_MONTH = "month";
    public static final String TIME_INTERVAL_UNIT_YEAR = "year";
    
    public static final String VERSION_LABEL_PRODUCTION = Application.getInstance().getServerMessages().getString("version.production.label");
    public static final String VERSION_LABEL_TEST = Application.getInstance().getServerMessages().getString("version.test.label");
    public static final String VERSION_LABEL_NOT_CURRENT = Application.getInstance().getServerMessages().getString("version.notcurrent.label");

    public static final String JSP_BEGIN_REMOVE_COMMENT = "<!-- ESF_BEGIN_REMOVE -->";
    public static final String JSP_END_REMOVE_COMMENT 	= "<!-- ESF_END_REMOVE -->";
    
    // For decimal units (typically used for disk storage)
    public static final int KB = 1000;
    public static final int MB = 1000 * 1000;
    public static final long GB = 1000L * 1000L * 1000L;
    public static final long TB = 1000L * 1000L * 1000L * 1000L;
    
    // For binary units (typically used for RAM)
    public static final int KiB = 1024;
    public static final int MiB = 1024 * 1024;
    public static final long GiB = 1024L * 1024L * 1024L;
    public static final long TiB = 1024L * 1024L * 1024L * 1024L;
}
