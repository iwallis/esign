// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2010 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.config;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.TreeMap;

import com.esignforms.open.config.PropertiesFile;


/**
 * Class that sucks in the format strings from a properties file and then saves the 
 * MessageFormat objects associated with such string as its used.  Used for creating
 * I18N capable messages with param substitutions.
 * @author Yozons, Inc.
 */
public class MessageFormatFile
	implements java.io.Serializable
{
	private static final long serialVersionUID = 665569090627374684L;

	protected TreeMap<String,MessageFormat> map = new TreeMap<String,MessageFormat>();
    
    public MessageFormatFile(String bundleName, Locale locale)
    {
    	PropertiesFile pf = new PropertiesFile(bundleName);
        
        java.util.Enumeration<String> keys = pf.getKeys();
        while( keys.hasMoreElements() )
        {
            String key = keys.nextElement();
            map.put(key,new MessageFormat(pf.getString(key),locale));
        }
        
        pf = null;  // we don't need the underlying properties file anymore
    }
    
    protected final MessageFormat getMessageFormat(String key)
    {
        return map.get(key);
    }
    
    /**
     * Gets a formatted string based on the 'key' and inserting the 
     * the params specified in {0}, {1}, etc.
     * @param key the String message key
     * @param params the object array/list to substitute in
     */
    public String getString( String key, Object... params)
    {
        MessageFormat mf = getMessageFormat(key);
        if ( mf == null )
            return "";
            
        return mf.format(params);
    }

    /**
     * Convenience routine for getString(String,Object[]) when no parameters are to be substituted.
     * @see #getString(String,Object[])
     */
    public String getString( String key )
    {
        MessageFormat mf = getMessageFormat(key);
        if ( mf == null )
            return "";
            
        return mf.format(null);
    }

    /**
     * Retrieves a string and does a String.split(splitRegex)
     * @param the String key to lookup
     * @param the String regex to be passed to String.split()
     * @return the String array after the split.  If the lookup fails, returns String[0]
     */
    public String[] getStringArray( String key, String splitRegex )
    {
        MessageFormat mf = getMessageFormat(key);
        if ( mf == null )
            return new String[0];
            
        return mf.format(null).split(splitRegex);
    }
    /**
     * Utility routine that passes ";" as the splitRegex.
     * @param the String key to lookup
     * @return the String array after the split.  If the lookup fails, returns String[0]
     * @see #getStringArray(String,String)
     */
    public String[] getStringArray( String key )
    {
    	return getStringArray(key,";");
    }

    /**
     * Convenience routine for getString(String,Object...) so that an integer
     * parameter can be substituted.
     * @see #getString(String,Object...)
     */
    public String getString( String key, int param0)
    {
        Object[] oa = new Object[1];
        oa[0] = new Integer(param0);
        return getString(key,oa);
    }
    
    /**
     * Convenience routine for getString(String,Object...) so that two integer
     * parameters can be substituted.
     * @see #getString(String,Object...)
     */
    public String getString( String key, int param0, int param1)
    {
        Object[] oa = new Object[2];
        oa[0] = new Integer(param0);
        oa[1] = new Integer(param1);
        return getString(key,oa);
    }
}