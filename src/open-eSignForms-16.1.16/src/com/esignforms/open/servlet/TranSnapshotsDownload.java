// Copyright (C) 2014-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.integration.wkhtmltopdf.HtmlToPdf;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentIdPartyId;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.util.XmlUtil;

/**
 * Handles the retrieval/download of one or more document snapshots.  It works in conjunction with the popup button TranSnapshotsDownloadPopupButton.
 * The parameters for the servlet are passed using a pickup code that maps to a context of the selected documents and format.
 * 
 * @author Yozons, Inc.
 */
@WebServlet("/transnapshotsdownload/*") 
public class TranSnapshotsDownload extends HttpServlet 
{
	private static final long serialVersionUID = -7950927699544833462L;

	public static final String PARAM_PICKUP_CODE = "c";
	
	public static final String PARAM_MODE = "m";
	public static final String MODE_VIEW = "view";
	public static final String MODE_DOWNLOAD = "download";
	
	public static final String FORMAT_HTML = "html";
	public static final String FORMAT_HTML_ZIP = "htmlZip";
	public static final String FORMAT_PDF_PORTRAIT = "pdfPortrait";
	public static final String FORMAT_PDF_LANDSCAPE = "pdfLandscape";
	public static final String FORMAT_XML_DATA_SNAPSHOT = "xmlDataSnapshot";
	public static final String FORMAT_XML_SNAPSHOTS = "xmlSnapshots";
	
	
	public static class DownloadContext implements java.io.Serializable
	{
		private static final long serialVersionUID = -5058807900146167229L;

		public EsfUUID transactionId;
		
		public String formatType = FORMAT_HTML;
		public boolean isHTML() { return FORMAT_HTML.equals(formatType); }
		public boolean isHTMLZip() { return FORMAT_HTML_ZIP.equals(formatType); }
		public boolean isAnyHTML() { return isHTML() || isHTMLZip(); }
		
		public boolean isPDFPortrait() { return FORMAT_PDF_PORTRAIT.equals(formatType); }
		public boolean isPDFLandscape() { return FORMAT_PDF_LANDSCAPE.equals(formatType); }
		public boolean isAnyPDF() { return isPDFPortrait() || isPDFLandscape(); }
		
		public boolean isXmlDataSnapshot() { return FORMAT_XML_DATA_SNAPSHOT.equals(formatType); }
		public boolean isXmlSnapshots() { return FORMAT_XML_SNAPSHOTS.equals(formatType); }
		public boolean isAnyXmlSnapshot() { return isXmlDataSnapshot() || isXmlSnapshots(); }
		
		public java.util.Set<DocumentIdPartyId> docAndPartyList = new java.util.HashSet<DocumentIdPartyId>();
		
		public String downloadFilename; // if a download file name is set, we'll use this name with .html, .zip or .pdf extension assured.
		public boolean hasDownloadFilename() { return EsfString.isNonBlank(downloadFilename); }
		
		public boolean isValid()
		{
			return transactionId != null && ! transactionId.isNull() && ( isAnyHTML() || isAnyPDF() || isAnyXmlSnapshot() ) && docAndPartyList.size() > 0;
		}
	}


	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
        HttpSession initialSession = request.getSession(false);
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init( initialSession == null ? request.getSession() : initialSession, request, response);
 
        if ( ! docPage.isUserLoggedIn() ) // Sets our logged in user name, plus they must be logged in to download transaction's document snapshots
        {
        	docPage.err("TranSnapshotsDownload.doGet() - User is not logged in; IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "You must be logged in to download document snapshots.");
        	return;
        }
                    
        try
        {
        	String pickupCode = docPage.getParam(PARAM_PICKUP_CODE);
            if ( EsfString.isBlank(pickupCode) )
            {
            	docPage.err("TranSnapshotsDownload.doGet() - No pickup code param was specified on the request; IP: " + docPage.getIP() + "; user: " + docPage.getLoggedInUser().getFullDisplayName());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No download context pickup code was specified.");
            	return;
            }
            if ( ! docPage.app.getRandomKey().isCorrectEsfReportsAccessPickupCodeLength(pickupCode) )
            {
            	docPage.err("TranSnapshotsDownload.doGet() - The pickup code param specified '" + pickupCode + "' on the request is invalid; IP: " + docPage.getIP() + "; user: " + docPage.getLoggedInUser().getFullDisplayName());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. An unexpected download context pickup code was found.");
            	return;
            }
            
            DownloadContext downloadContext = (DownloadContext)docPage.getSessionAttribute(pickupCode);
            if ( downloadContext == null )
            {
            	docPage.err("TranSnapshotsDownload.doGet() - The pickup code param specified '" + pickupCode + "' on the request had no associated download context; IP: " + docPage.getIP() + "; user: " + docPage.getLoggedInUser().getFullDisplayName());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "The specified download link was not found. Most likely your session ended. Please re-login and try again.");
            	return;
            }
            if ( ! downloadContext.isValid() )
            {
            	docPage.err("TranSnapshotsDownload.doGet() - The pickup code param specified '" + pickupCode + "' on the request had an invalid download context; IP: " + docPage.getIP() + "; user: " + docPage.getLoggedInUser().getFullDisplayName());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The specified download context was not valid.");
            	return;
            }
            
            String mode = docPage.getParam(PARAM_MODE);
            if ( EsfString.isBlank(mode) )
            {
            	docPage.err("TranSnapshotsDownload.doGet() - No mode param was specified on the request; IP: " + docPage.getIP() + "; user: " + docPage.getLoggedInUser().getFullDisplayName());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No download mode was found.");
            	return;
            }
            
            if ( ! MODE_VIEW.equals(mode) )
            	mode = MODE_DOWNLOAD;
            
            Transaction transaction = Transaction.Manager.getById(downloadContext.transactionId);
            if ( transaction == null )
            {
            	docPage.err("TranSnapshotsDownload.doGet() - Could not find the transaction with id: " + downloadContext.transactionId + "; IP: " + docPage.getIP() + "; user: " + docPage.getLoggedInUser().getFullDisplayName());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link and retry. The transaction associated with the request could not be found.");
            	return;
            }
            
			List<TransactionPartyDocument> tranPartyDocumentList = transaction.getTransactionPartyDocumentsWithNonSkippedSnapshot(new LinkedList<DocumentIdPartyId>(downloadContext.docAndPartyList));
        	if ( tranPartyDocumentList.size() < 1 )
        	{
            	docPage.err("TranSnapshotsDownload.doGet() - No tran party documents were found to view or download for tranId: " + downloadContext.transactionId + "; IP: " + docPage.getIP() + "; user: " + docPage.getLoggedInUser().getFullDisplayName());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "No documents were found to view or download.");
            	return;
        	}
			
            if ( downloadContext.isAnyHTML() )
            	doHtml(docPage,downloadContext,mode,transaction,tranPartyDocumentList);
            else if ( downloadContext.isAnyPDF() )
            	doPdf(docPage,downloadContext,mode,transaction,tranPartyDocumentList);
            else if ( downloadContext.isAnyXmlSnapshot() )
            	doXml(docPage,downloadContext,mode,transaction,tranPartyDocumentList);
            else
            {
            	docPage.err("TranSnapshotsDownload.doGet() - Unexpected format type: " + downloadContext.formatType + "; IP: " + docPage.getIP() + "; user: " + docPage.getLoggedInUser().getFullDisplayName());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The download format type was unexpected.");
            	return;
            }
        }
        finally
        {
        	if ( initialSession == null )
        	{
        		docPage.endSession();
        	}
        }

	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    String removeFilenameExtension(String name)
    {
    	name = name.trim();
    	String lowerName = name.toLowerCase();
    	if ( lowerName.endsWith(".htm") || lowerName.endsWith(".pdf") || lowerName.endsWith(".zip") )
    		return name.substring(0, name.length()-4);
    	if ( lowerName.endsWith(".html") )
    		return name.substring(0, name.length()-5);
    	return name;
    }
    
    String ensureFilenameExtension(DocumentPageBean docPage, String name, String extension) 
    {
    	name = removeFilenameExtension(name);
    	if ( EsfString.isBlank(name) )
    		name = docPage.app.getServerMessages().getString("TranSnapshotsDownload.singleFile.defaultFilename");
		return name + extension;
    }
    
    void doHtml(DocumentPageBean docPage, DownloadContext downloadContext, String mode, Transaction transaction, List<TransactionPartyDocument> tranPartyDocumentList) 
    	throws ServletException, java.io.IOException
    {
    	if ( downloadContext.isHTMLZip() || tranPartyDocumentList.size() > 1 )
    		doHtmlZip(docPage,downloadContext,mode,transaction,tranPartyDocumentList);
    	else
    	{
    		TransactionPartyDocument tpd = tranPartyDocumentList.get(0);
    		String html = tpd.getSnapshotDocument();
    		
        	byte[] content = EsfString.stringToBytes(html);
        	if ( MODE_VIEW.equals(mode) )
        	{
            	docPage.response.setContentType(Application.CONTENT_TYPE_HTML);
            }
            else
            {
    			String htmlFileName = ensureFilenameExtension(docPage, downloadContext.hasDownloadFilename() ? downloadContext.downloadFilename : docPage.app.getServerMessages().getString("TranSnapshotsDownload.singleFile.defaultFilename"), ".html");
            	docPage.response.setContentType(Application.CONTENT_TYPE_BINARY);
            	String disposition = "attachment; filename=\""+ htmlFileName + "\"";
                docPage.response.setHeader("Content-Disposition", disposition );
            }
        	docPage.response.setContentLength(content.length);
        	docPage.response.setHeader("Cache-Control","private,max=age=0");  
            ServletOutputStream sos = docPage.response.getOutputStream();
            sos.write(content);
            sos.close();    
    	}
    }
    
    void doHtmlZip(DocumentPageBean docPage, DownloadContext downloadContext, String mode, Transaction transaction, List<TransactionPartyDocument> tranPartyDocumentList) 
       	throws ServletException, java.io.IOException
    {
		String[] htmlFiles = new String[tranPartyDocumentList.size()];
		String[] htmlFilenames = new String[tranPartyDocumentList.size()];
		int i = 0;
		for( TransactionPartyDocument tpd : tranPartyDocumentList )
		{
			htmlFiles[i] = tpd.getSnapshotDocument();
			if ( tpd.hasDocumentFileName() )
			{
				htmlFilenames[i] = tpd.getDocumentFileName();
			}
			else
			{
    			TransactionDocument td = transaction.getTransactionDocument(tpd.getTransactionDocumentId());
	    		if ( td != null )
	    		{
	    			Document document = td.getDocument();
	    			DocumentVersion docVer = td.getDocumentVersion();
	    			if ( document != null )
	    			{
	    				if ( document.hasFileNameSpec() )
	    					htmlFilenames[i] = transaction.getFieldSpecWithSubstitutions(docPage.getLoggedInUser(), docVer, document.getFileNameSpec());
	    				else
	    					htmlFilenames[i] = document.getEsfName().toString();
	    			}
	    			else
	    				htmlFilenames[i] = docPage.app.getServerMessages().getString("TranSnapshotsDownload.singleFile.defaultZipEntryFilename",i+1);
	    		}
	    		else
    				htmlFilenames[i] = docPage.app.getServerMessages().getString("TranSnapshotsDownload.singleFile.defaultZipEntryFilename",i+1);
			}
			++i;
		}
		
		String zipFileName = ensureFilenameExtension(docPage, downloadContext.hasDownloadFilename() ? downloadContext.downloadFilename : docPage.app.getServerMessages().getString("TranSnapshotsDownload.multiFile.defaultFilename"), ".zip");
		
		HashSet<String> htmlFileNameSet = new HashSet<String>();
		
		ZipOutputStream zos = null;
        try
        {
            // Set the response to be a ZIP
            docPage.response.setContentType(Application.CONTENT_TYPE_ZIP);
            docPage.response.setHeader("Content-Disposition", "attachment; filename=\"" + zipFileName + "\"" );
        	docPage.response.setHeader("Cache-Control","private,max=age=0");  
            zos = new ZipOutputStream(docPage.response.getOutputStream());
            zos.setComment("Open eSignForms document zip downloaded on " + (new EsfDate()).toYMDString());
            zos.setLevel(9);

            i = 0;
            for( String html : htmlFiles ) 
            {
            	byte[] content = EsfString.stringToBytes(html);
            	String htmlFilename = ensureFilenameExtension(docPage, htmlFilenames[i], ".html");
            	
            	int uniqueIndex = 1;
            	while ( htmlFileNameSet.contains(htmlFilename) )
            	{
            		htmlFilename = removeFilenameExtension(htmlFilenames[i]);
            		htmlFilename = ensureFilenameExtension(docPage, htmlFilename + uniqueIndex, ".html");
            		++uniqueIndex;
            	}
            	htmlFileNameSet.add(htmlFilename);
            	
                ZipEntry zipEntry = new ZipEntry( htmlFilename );
                zipEntry.setSize(content.length);
                zos.putNextEntry(zipEntry);
                zos.write(content);
                zos.closeEntry();
                ++i;
            }
        }
        finally
        {
        	if ( zos != null )
        	{
        		try { zos.close(); } catch( Exception e2 ) {}
            }
        }
    }
    
    void doPdf(DocumentPageBean docPage, DownloadContext downloadContext, String mode, Transaction transaction, List<TransactionPartyDocument> tranPartyDocumentList) 
       	throws ServletException, java.io.IOException
    {
		boolean useLandscape = downloadContext.isPDFLandscape();
		
		String[] htmlFiles = new String[tranPartyDocumentList.size()];
		int i = 0;
		for( TransactionPartyDocument tpd : tranPartyDocumentList )
		{
			TransactionDocument td = transaction.getTransactionDocument(tpd.getTransactionDocumentId());
			if ( td.getDocumentVersion().isLandscape() )
				useLandscape = true;
			htmlFiles[i++] = tpd.getSnapshotDocument();
		}
		
		HtmlToPdf htmlToPdf = new HtmlToPdf();
		byte[] content = useLandscape ? htmlToPdf.generateLandscapeSignedPdf(htmlFiles) : htmlToPdf.generateSignedPdf(htmlFiles);

    	if ( MODE_VIEW.equals(mode) )
    		docPage.response.setContentType(Application.CONTENT_TYPE_PDF);
    	else
    	{
			String pdfFileName = ensureFilenameExtension(docPage, downloadContext.hasDownloadFilename() ? downloadContext.downloadFilename : docPage.app.getServerMessages().getString("TranSnapshotsDownload.singleFile.defaultFilename"), ".pdf");
        	docPage.response.setContentType(Application.CONTENT_TYPE_BINARY);
        	String disposition = "attachment; filename=\""+ pdfFileName + "\"";
            docPage.response.setHeader("Content-Disposition", disposition );
    	}
		
    	docPage.response.setContentLength(content.length);
    	docPage.response.setHeader("Cache-Control","private,max=age=0");  
        ServletOutputStream sos = docPage.response.getOutputStream();
        sos.write(content);
        sos.close();    
    }
    
    void doXml(DocumentPageBean docPage, DownloadContext downloadContext, String mode, Transaction transaction, List<TransactionPartyDocument> tranPartyDocumentList) 
    	throws ServletException, java.io.IOException
    {
		TransactionPartyDocument tpd = tranPartyDocumentList.get(0);
		String xml;
		
		if ( downloadContext.isXmlDataSnapshot() )
			xml = XmlUtil.XML_DECLARATION_UTF_8 + tpd.getSnapshotData();
		else
			xml = XmlUtil.XML_DECLARATION_UTF_8 + tpd.getSnapshotXml();
		
    	byte[] content = EsfString.stringToBytes(xml);
		String xmlFileName = ensureFilenameExtension(docPage, downloadContext.hasDownloadFilename() ? downloadContext.downloadFilename : docPage.app.getServerMessages().getString("TranSnapshotsDownload.singleFile.defaultFilename"), ".xml");
    	docPage.response.setContentType(Application.CONTENT_TYPE_BINARY);
    	String disposition = "attachment; filename=\""+ xmlFileName + "\"";
        docPage.response.setHeader("Content-Disposition", disposition );
    	docPage.response.setHeader("Cache-Control","private,max=age=0");  
    	docPage.response.setContentLength(content.length);
        ServletOutputStream sos = docPage.response.getOutputStream();
        sos.write(content);
        sos.close();    
    }
        
    public String getServletInfo() 
    {
        return "TranSnapshotsDownload " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}