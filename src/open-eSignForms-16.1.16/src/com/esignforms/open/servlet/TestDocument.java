// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
 * Takes care of showing the Test version of a generated document.  It's job is to create a read-only package on the fly, add the requested
 * document to it, then create a read-only test transaction that uses that package where it can be picked up using our standard pickup facility.
 * @author Yozons, Inc.
 */
@WebServlet("/TestDocument/*") 
public class TestDocument extends HttpServlet 
{
	private static final long serialVersionUID = 4478673194436007107L;


	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
        HttpSession session = request.getSession();
        
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init(session,request,response);
        
        if ( session == null )
        {
        	docPage.err("Could not get a session.");
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Your login session could not be created. IP: " + docPage.getIP());
            return;
        }
        
        // All testing of documents requires logged in users.
        if ( ! docPage.isUserLoggedIn() )
        {
        	docPage.err("User is not logged in.");
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Your login session has terminated. Please login to test the document. IP: " + docPage.getIP());
            return;
        }
           
        if ( ! docPage.app.allowTestTransactions() )
        {
        	docPage.err("Test transactions are not allowed on this deployment.");
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Test transactions are not currently allowed on this deployment. IP: " + docPage.getIP());
            return;
        }
        
        String pathInfoDocId = request.getPathInfo();
        if ( docPage.isBlank(pathInfoDocId) )
        {
        	docPage.err("There is no document id in the path info of the current link. IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No document id was specified to test.");
        	return;
        }
        pathInfoDocId = pathInfoDocId.substring(1);
        
        EsfUUID documentId = new EsfUUID(pathInfoDocId);
        if ( documentId.isNull() )
        {
        	docPage.err("The document id in the path info of the current link is not valid: " + pathInfoDocId + "; IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. The document id (" + pathInfoDocId + ") specified to test is not valid.");
        	return;
        }
        
        Document document = Document.Manager.getById(documentId);
        if ( document == null )
        {
        	docPage.err("No document with the id in the path info of the current link was found: " + pathInfoDocId + "; IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. No document with the id (" + pathInfoDocId + ") specified to test was found.");
        	return;
        }
        
        if ( ! document.isEnabled() )
        {
        	docPage.err("The document to be tested is not enabled for pathInfoDocId: " + pathInfoDocId + "; IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "The document must be ENABLED before it can be tested.");
        	return;
        }
        
        DocumentVersion documentVersion = document.getTestDocumentVersion();
        if ( documentVersion == null )
        {
        	docPage.err("The document version to be tested was not found for pathInfoDocId: " + pathInfoDocId + "; version: " + document.getTestVersion() + "; IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "The document version for testing was not found.");
        	return;
        }
        
        String targetPageNumber = request.getParameter("page");
        int pageNumber = docPage.app.stringToInt(targetPageNumber, 1);
        if ( pageNumber < 1 || pageNumber > documentVersion.getNumPages() )
        	pageNumber = 1;
        
        // If a party was specified, we'll be that party, otherwise we'll use the first party we find.
        String targetPartyName = request.getParameter("party");
        PartyTemplate partyTemplate = EsfString.isBlank(targetPartyName) ? null : documentVersion.getPartyTemplate(new EsfName(targetPartyName));
        if ( partyTemplate == null )
        	partyTemplate = documentVersion.getFirstPartyTemplate();
        
        // Okay, we know the document we're going to use, so let's create a package on the fly.
        Package testPackage = Package.Manager.createForTestDocument(docPage.getLoggedInUser());
        PackageVersion testPackageVersion = PackageVersion.Manager.createForTestDocument(testPackage, docPage.getLoggedInUser());
        testPackageVersion.addDocumentAndPartiesToTestPackage(document);
        // when testing, a document may have zero parties configured, so we'll fake it with a package party that maps to the document, albeit as an undefined party that should function like "view only".
        EsfName packagePartyName = ( partyTemplate == null ) ? testPackageVersion._createTestPartyToAllDocumentsIfNoPackagePartiesSet() : partyTemplate.getEsfName();
        testPackageVersion.getPackageVersionPartyTemplateByName(packagePartyName).setLandingPageFirstDoc(); // for testing, just go to the first document
        
        // Now create a transaction template on the fly that uses this package, without a brand library.
        Library brandLibrary = null;
        TransactionTemplate template = TransactionTemplate.Manager.createForTestDocument(testPackage, brandLibrary, docPage.getLoggedInUser());
        
        // Now create the temporary test transaction that uses this transaction template
        Transaction transaction;
        try
        {
        	transaction = Transaction.Manager.createForTestDocument(template, docPage.getLoggedInUser(), request);
        }
        catch( EsfException e )
        {
        	docPage.except(e,"The testing transaction template could not be created; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The requested testing transaction cannot be started at this time.");
        	return;
        }
        
        // Retrieve the first party defined and activate it
        TransactionParty tranParty = transaction.getTransactionParty(packagePartyName);
        tranParty.setStatusActive();
        tranParty.getCurrentAssignment().setUserId(docPage.getLoggedInUser().getId());
        tranParty.getCurrentAssignment().setEmailAddress(docPage.getLoggedInUser().getEmail());
        
        String pickupCode = tranParty.getCurrentAssignment().getPickupCode();
        TransactionContext context = DocumentPageBean.TransactionPickupManager.registerPickupCode(docPage.getLoggedInUser(), session, pickupCode, packagePartyName, transaction.getId(), 
        																						  docPage.getIPOnly(), docPage.getIP(), docPage.getUserAgent());
        context.goToPageNumberForTesting(pageNumber); // special so we can test and land on multi-page review page if necessary
        
        docPage.sendRedirect(docPage.getContextPath()+"/P/"+pickupCode);
	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    
    public String getServletInfo() 
    {
        return "TestDocument " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}