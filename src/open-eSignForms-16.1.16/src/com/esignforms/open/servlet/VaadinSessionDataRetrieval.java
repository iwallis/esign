// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.data.EsfString;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;

/**
 * Handles the retrieval of data stored in the session for Vaadin UI widgets.
 * Initial known usage is in com.esignforms.open.vaadin.log.email.EmailView.
 * @author Yozons, Inc.
 */
@WebServlet("/VaadinSessionDataRetrieval/*") 
public class VaadinSessionDataRetrieval extends HttpServlet 
{
	private static final long serialVersionUID = 3208022909623241663L;


	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
        HttpSession session = request.getSession();
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init(session,request,response);
        
        if ( session == null )
        {
        	docPage.err("Could not get a session.");
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Your login session to retrieve data could not be created. IP: " + docPage.getIP());
            return;
        }
        
        String typeRequested = docPage.getParam("type");
        if ( EsfString.isBlank(typeRequested) )
        {
        	docPage.err("Missing required 'type' param.");
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Your data cannot be retrieved because it is missing the 'type' parameter.");
            return;
        }
        
        String nameRequested = docPage.getParam("name");
        if ( EsfString.isBlank(nameRequested) )
        {
        	docPage.err("Missing required 'name' param.");
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Your data cannot be retrieved because it is missing the 'name' parameter.");
            return;
        }
        
        String contentTypeRequested = docPage.getParam("contentType");
        
        if ( "string".equalsIgnoreCase(typeRequested) ) {
        	String data = docPage.getSessionStringAttribute(nameRequested, "");
            byte[] content = EsfString.stringToBytes(data);
            response.setCharacterEncoding(EsfString.CHARSET_UTF_8);
            if ( EsfString.isNonBlank(contentTypeRequested) )
            	response.setContentType(contentTypeRequested);
            response.setContentLength(content.length);
            ServletOutputStream sos = response.getOutputStream();
            sos.write(content);
            sos.close();
        } else if ( "bytes".equalsIgnoreCase(typeRequested) ) {
        	byte[] content = (byte[])docPage.getSessionAttribute(nameRequested);
        	if ( content == null )
        		content = new byte[0];
            response.setCharacterEncoding(EsfString.CHARSET_UTF_8);
            if ( EsfString.isNonBlank(contentTypeRequested) )
            	response.setContentType(contentTypeRequested);
            response.setContentLength(content.length);
            ServletOutputStream sos = response.getOutputStream();
            sos.write(content);
            sos.close();
        } else {
        	docPage.err("Unexpected 'type' param: " + typeRequested);
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Your data cannot be retrieved because the 'type' parameter value is unexpected: " + typeRequested);
        }
	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    

    
    public String getServletInfo() 
    {
        return "VaadinSessionDataRetrieval " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}