// Copyright (C) 2013-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import javax.servlet.*;
import javax.servlet.http.*;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfString;

/**
 * Standard System Down servlet used to display a common error when the system is
 * undergoing maintenance. It is generally only used when the special WEB-INF/webdown.xml
 * is installed as WEB-INF/web.xml so that the normal Open eSignForms app is not started
 * and this servlet is installed to handle all requests.
 * 
 * @author Yozons, Inc.
 */
public class SystemDown
    extends    HttpServlet 
{
	private static final long serialVersionUID = 7894728662092677478L;

	private static final com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(SystemDown.class);

	private String html =
       "<!DOCTYPE html>" +
       "<html><head><title>System is currently down for maintenance</title><meta name=\"robots\" content=\"noindex, nofollow\" /><meta charset=\"utf-8\"></head><body>" +
       "<h1 style=\"background-color: pink;\">System unavailable</h1>" +
       "<h2>The electronic signature system is currently down for maintenance.</h2>" +
       "<p>We are sorry that you cannot continue with your work at this time.</p>" +
       "<p><em>Thank you for your patience, and please try again shortly....</em></p>" +
       "</body></html>";	
	
    public void doGet(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        java.io.PrintWriter w = res.getWriter();
        w.println(html);
        res.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
    }
    
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    public void init( ServletConfig config ) 
	    throws ServletException
	{
	    super.init(config);
	    
	    _logger.warn("init() - SystemDown servlet has been activated. All access to the application will produce a system down response.");
	    
	    String htmlFileParam = config.getInitParameter("html-file");
	    if ( htmlFileParam != null )
	    {
    		java.io.ByteArrayOutputStream baos = null;
    		java.io.InputStream is = null;
	    	try
	    	{
		    	is = config.getServletContext().getResourceAsStream(htmlFileParam);
		    	if ( is != null )
		    	{
		    		baos = new java.io.ByteArrayOutputStream(10*Literals.KB);
		    		byte[] buf = new byte[4096];
		    		int readCount;
		    		while( (readCount = is.read(buf)) > 0 )
		    		{
		    			baos.write(buf,0,readCount);
		    		}
		    		baos.flush();
		    		html = baos.toString(EsfString.CHARSET_UTF_8);
		    		baos.close();
		    	}
		    	else
		    	    _logger.warn("init() - Could not open resource html-file: " + htmlFileParam + "; using built-in message");
	    	}
	    	catch( java.io.IOException e )
	    	{
	    	    _logger.error("init() - Exception processing resource html-file: " + htmlFileParam,e);
	    	}
	    	finally
	    	{
	    		try
	    		{
		    		if ( is != null )
		    			is.close();
		    		if ( baos != null )
		    			baos.close();
	    		}
	    		catch(Exception e) 
	    		{
	    			// ignore exceptions on closing streams
	    		}
	    	}
	    }
	}
    
    public String getServletInfo() 
    {
        return "SystemDown " + com.esignforms.open.Version.getReleaseString();
    }
}