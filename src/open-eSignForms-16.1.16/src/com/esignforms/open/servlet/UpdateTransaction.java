// Copyright (C) 2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.runtime.workflow.TransactionEngine;
import com.esignforms.open.user.User;

/**
 * Updates an existing transaction.
 * @author Yozons, Inc.
 */
@WebServlet("/U/*") 
public class UpdateTransaction extends HttpServlet 
{
	private static final long serialVersionUID = 1220912915611777348L;

	public static final String PARAM_ESFLOGINUSER      			= StartTransaction.PARAM_ESFLOGINUSER;
    public static final String PARAM_ESFLOGINPASSWORD  			= StartTransaction.PARAM_ESFLOGINPASSWORD;
    public static final String PARAM_ESFTRANSACTIONID  			= "ESFTID";
    public static final String PARAM_ESFDOCUMENT	  			= "ESFDOCUMENT";
    public static final String PARAM_ESFPARTY	  				= "ESFPARTY";
    public static final String PARAM_ESFEVENTNAME  				= "ESFEVENTNAME";


	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
		
        HttpSession session = request.getSession();
        if ( session != null && ! session.isNew() )  // Force all sessions to be new for updates
        {
        	session.invalidate();
        	session = request.getSession();
        }
        
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init(session,request,response);
        docPage.setApiMode(true);
        
        if ( session == null )
        {
        	docPage.err("Could not get a session. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "System error: A session could not be created to support updating your transaction.");
            return;
        }
        
        // Ensure we have the login info needed to authenticate
        String esfLoginUserParam = docPage.getParam(PARAM_ESFLOGINUSER);
        String esfLoginPasswordParam = docPage.getParam(PARAM_ESFLOGINPASSWORD);
        if ( EsfString.areAnyBlank(esfLoginUserParam,esfLoginPasswordParam) )
        {
        	docPage.err("Missing user/password params. External users are not authorized to update transactions; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You must provide authentication credentials to update a transaction. Your IP address has been logged.");
        	docPage.endSession();
        	return;        	
        }
        
        // The transaction id is required to identify the transaction to be updated.
        String transactionIdParam = docPage.getParam(PARAM_ESFTRANSACTIONID);
        if ( EsfString.isBlank(transactionIdParam) )
        {
        	docPage.err("The transaction id param is missing; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. The transaction id param is missing. Your IP address has been logged.");
        	docPage.endSession();
        	return;        	
        }
        EsfUUID transactionId = new EsfUUID(transactionIdParam);
        if ( transactionId.isNull() )
        {
        	docPage.err("The requested transaction id: " + transactionIdParam + "; is not in a valid format; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. The transaction id (" + transactionIdParam + ") specified is not in a valid format. Your IP address has been logged.");
        	docPage.endSession();
        	return;
       	
        }
        
        // Optional document name to update; if missing, we attempt to update all documents
        String documentNameParam = docPage.getParam(PARAM_ESFDOCUMENT);
        EsfName documentName = null;
        if ( EsfString.isNonBlank(documentNameParam) )
        {
        	documentName = new EsfName(documentNameParam);
        	if ( ! documentName.isValid() )
        	{
            	docPage.err("Invalid document name in param; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. Invalid document name param (" + documentNameParam + "). Your IP address has been logged.");
            	docPage.endSession();
            	return;
        	}
        }
        
        String partyNameParam = docPage.getParam(PARAM_ESFPARTY);
        EsfName partyName = null;
        if ( EsfString.isNonBlank(partyNameParam) )
        {
        	partyName = new EsfName(partyNameParam);
        	if ( ! partyName.isValid() )
        	{
            	docPage.err("Invalid party name in param; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. Invalid party name param (" + partyNameParam + "). Your IP address has been logged.");
            	docPage.endSession();
            	return;
        	}
        }
        
        String eventName = docPage.getParam(PARAM_ESFEVENTNAME, "UpdateAPI");
        
        User user = docPage.loginUser(esfLoginUserParam, esfLoginPasswordParam);
		if ( user == null )
		{
        	docPage.err("Login failure; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You must provide valid authentication credentials to update a transaction. Your IP address has been logged.");
        	docPage.endSession();
			return;
		}
        
		Transaction transaction = Transaction.Manager.getById(transactionId);
		if ( transaction == null )
		{
			user.logSecurity("Tried update API for transaction id: " + transactionId + "; but no such transaction was found. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. No transaction was found with the specified id (" + transactionId + "). Your IP address has been logged.");
        	docPage.endSession();
        	return;
		}
		
		// Ensure the user has permission to update this transaction
		TransactionTemplate transactionTemplate = transaction.getTransactionTemplate();
		if ( ! transactionTemplate.hasUseUpdateApiPermission(user) )
		{
			user.logSecurity("Tried using the Update API for transaction id: " + transactionId + "; but is not authorized. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
			transaction.logGeneral("User '" + user.getFullDisplayName() + "' tried to use the Update API, but is not authorized.");
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Please check your link. You are not authorized to update transaction id (" + transactionId + "). Your IP address has been logged.");
        	docPage.endSession();
			return;
		}
		
		TransactionDocument tranDoc = null;
		if ( documentName != null )
		{
			tranDoc = transaction.getTransactionDocument(documentName);
			if ( tranDoc == null )
			{
				user.logSecurity("Tried using the Update API for transaction id: " + transactionId + "; on unknown document: " + documentName + "; eventName: " + eventName + ". IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
				transaction.logGeneral("User '" + user.getFullDisplayName() + "' tried to use the Update API on unknown document: " + documentName + "; eventName: " + eventName);
				docPage.errors.addError("Unknown document with name: " + documentName);
				sendAPIError(docPage);
	        	docPage.endSession();
				return;
			}
		}
		
		TransactionParty tranParty = null;
		if ( partyName != null )
		{
			tranParty = transaction.getTransactionParty(partyName);
			if ( tranParty == null )
			{
				user.logSecurity("Tried using the Update API for transaction id: " + transactionId + "; on unknown party: " + partyName + "; eventName: " + eventName + ". IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
				transaction.logGeneral("User '" + user.getFullDisplayName() + "' tried to use the Update API on unknown party: " + partyName + "; eventName: " + eventName);
				docPage.errors.addError("Unknown party with name: " + partyName);
				sendAPIError(docPage);
	        	docPage.endSession();
				return;
			}
		}
		
        // Now let's do all this work in a DB transaction 
        ConnectionPool    pool = docPage.app.getConnectionPool();
        Connection        con  = pool.getConnection();
        
        try
        {
        	transaction.updateApi(con,user,request,tranDoc,tranParty);

        	docPage.context = new TransactionContext(user, transaction.getId(), eventName);
        	TransactionEngine engine = new TransactionEngine(docPage.context, docPage);
            engine.queueTransactionUpdateApiEvent(eventName);
            engine.doWork(con, docPage.errors);
            
            if ( transaction.save(con) )
            {
                con.commit();
            	if ( docPage.errors.hasError() )
            		sendAPIError(docPage);
            	else
            		sendAPISuccess(docPage);
            }
            else
            {
            	con.rollback();
            	sendAPIError(docPage);
            }
        }
        catch(EsfException e) 
        {
            pool.rollbackIgnoreException(con);
        	docPage.except(e,"The transaction: " + transaction.getId() + "; cannot process the update API request for eventName: " + eventName + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The transaction cannot be updated by API at this time.");
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
        	docPage.except(e,"The transaction: " + transaction.getId() + "; cannot save the updated transaction for eventName: " + eventName + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The transaction cannot be updated by API at this time.");
        }
        finally
        {
        	docPage.app.cleanupPool(pool,con,null);
        	docPage.endSession();
        }
	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    void sendAPISuccess(DocumentPageBean docPage) throws IOException
    {
    	docPage.response.setContentType(Application.CONTENT_TYPE_TEXT+Application.CONTENT_TYPE_CHARSET_UTF_8);
    	docPage.response.getWriter().println("OK:"+docPage.context.transactionId);
    	docPage.logoffUser();
    }

    void sendAPIError(DocumentPageBean docPage) throws IOException
    {
    	docPage.response.setContentType(Application.CONTENT_TYPE_TEXT+Application.CONTENT_TYPE_CHARSET_UTF_8);
    	docPage.response.getWriter().println("ERROR:"+docPage.errors.toString());
    	docPage.logoffUser();
    }
    
    
    public String getServletInfo() 
    {
        return "UpdateTransaction " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}