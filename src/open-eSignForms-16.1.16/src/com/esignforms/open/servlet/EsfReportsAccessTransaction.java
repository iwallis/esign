// Copyright (C) 2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.jsp.PageBean.PageProcessing;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.user.User;

/**
 * Handles the pickup of a transaction via the reports for the built-in party ESF_reports_access.
 * @author Yozons, Inc.
 */
@WebServlet("/ESF_reports_access/*") 
public class EsfReportsAccessTransaction
    extends    HttpServlet 
{
	private static final long serialVersionUID = -4194370120799662324L;


	public static class EsfReportsAccessContext
	{
		User user;
		EsfUUID transactionId;
		ReportTemplate reportTemplate;
		boolean forUpdate;
		
		private String pickupCode;
		private EsfDateTime lastAccessTimestamp;
		
		public EsfReportsAccessContext(User user, EsfUUID transactionId, ReportTemplate reportTemplate, boolean forUpdate)
		{
			assert(user != null);
			assert(user.isEnabled());
			assert(transactionId != null);
			assert(!transactionId.isNull());
			assert(reportTemplate != null);
			assert(reportTemplate.isEnabled());
			
			this.user = user;
			this.transactionId = transactionId;
			this.reportTemplate = reportTemplate;
			this.forUpdate = forUpdate;
			
			this.pickupCode = Application.getInstance().getRandomKey().getEsfReportsAccessPickupCodeString();
			this.lastAccessTimestamp = new EsfDateTime();
		}
		
		public boolean ensurePickupBySameUser(User u, String puc)
		{
			if ( pickupCode.equals(puc) && user.equals(u) )
			{
				lastAccessTimestamp = new EsfDateTime(); // we use this for each request, so we update our last accessed here
				return true;
			}
			
			return false;
		}
		
		public final EsfUUID getTransactionId()
		{
			return transactionId;
		}
		
		public final boolean isForView()
		{
			return ! forUpdate;
		}
		public final boolean isForUpdate()
		{
			return forUpdate;
		}
		
		public final String getPickupCode()
		{
			return pickupCode;
		}
		
		public final EsfDateTime getLastAccessTimestamp()
		{
			return lastAccessTimestamp;
		}
		public boolean hasAccessedRecently()
		{
			EsfDateTime past = new EsfDateTime();
			past.addHours(-1);
			
			return lastAccessTimestamp.isAfter(past);
		}
		
		public ReportTemplate getReportTemplate()
		{
			return reportTemplate;
		}
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
        HttpSession session = request.getSession(false);
        
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init(session,request,response);
        
        if ( session == null )
        {
        	docPage.err("Could not get an existing session. IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Your web contracting session ended so you may not access the transaction via the reports until you re-login.");
            return;
        }
        
        String pickupCode = request.getPathInfo();
        if ( docPage.isBlank(pickupCode) || pickupCode.length() < 2 ) // it is longer than 2, but we do this to ensure our substring next doesn't fail
        {
        	docPage.err("There is no reports access pickup code in the path info of the current link. IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The expected unique reports access pickup code to identify your request was not specified.");
        	return;
        }
        pickupCode = pickupCode.substring(1);
        
        // Get our reports context for this pickup code
        EsfReportsAccessContext reportsAccessContext = (EsfReportsAccessContext)docPage.session.getAttribute(pickupCode);
        if ( reportsAccessContext == null )
        {
        	docPage.err("There is no reports access context for the pickup code in the path info of the current link. IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Please check your link or re-login and try again. The unique reports access pickup code to identify your request is missing its required access context.");
        	return;
        }
        
        // Sets our logged in user name and ensures all reports access is via a logged in user.
        if ( ! docPage.isUserLoggedIn() )
        {
        	docPage.err("The requesting user is not logged in. Reports access expected to be by user: " + reportsAccessContext.user.getFullDisplayName() + "; IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You must be logged in to access transactions via the reports.");
        	docPage.removeSessionAttribute(pickupCode);
        	return;
        }
        
        // Must be the same user, and we also update our last access timestamp
        if ( ! reportsAccessContext.ensurePickupBySameUser(docPage.getLoggedInUser(),pickupCode) )
        {
        	docPage.err("Invalid access to this report by user: " + docPage.getLoggedInUser().getFullDisplayName() + "; expecting user: " +
        			reportsAccessContext.user.getFullDisplayName() + "; IP: " + docPage.getIP());
        	docPage.getLoggedInUser().logSecurity("EsfReportsAccessTransaction: User attempted to access transaction id: " + reportsAccessContext.transactionId + "; via report: " + reportsAccessContext.reportTemplate.getPathName() + "; that's only valid for user: " + reportsAccessContext.user.getFullDisplayName());
        	reportsAccessContext.user.logSecurity("EsfReportsAccessTransaction: User appears to have attempted to give access transaction id: " + reportsAccessContext.transactionId + "; via report: " + reportsAccessContext.reportTemplate.getPathName() + "; to user: " + docPage.getLoggedInUser().getFullDisplayName());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Security violation logged. You are not the authorized user to access this transaction via the reports.");
        	docPage.removeSessionAttribute(pickupCode);
        	return;
        }
        
        if ( ! reportsAccessContext.hasAccessedRecently() )
        {
        	docPage.err("Expired report access by user: " + docPage.getLoggedInUser().getFullDisplayName() + "; last accessed: " + reportsAccessContext.getLastAccessTimestamp() + "; IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Your authorization to access this transaction via a report has expired. Please try again from the report.");
        	docPage.removeSessionAttribute(pickupCode);
        	return;
        }
        
        if ( ! docPage.pickupTransaction(reportsAccessContext) )
        	return;
        
        // We don't want to do any more if the request has been processed fully (most likely a redirect took place).
        if ( docPage.processRequest() == PageProcessing.DONE )
        	return;
        
        request.setAttribute("docPage", docPage); // Save this for the page that services this request
        
        if ( ! docPage.context.transaction.isDeleted() )
        	docPage.context.transaction.logGeneral("Reports party " + docPage.context.pickupPartyName + " on document " + docPage.context.currDocument.getEsfName() + " " + docPage.getDocumentAndPageNumbersInfo() + "; forUpdate: " + reportsAccessContext.forUpdate + "; user: " + docPage.getLoggedInUser().getFullDisplayName() + "; IP: " + docPage.getIP());
        
        String docUrl = docPage.context.getCurrentDocumentRequestDispatcherUrl();
		RequestDispatcher rd = request.getRequestDispatcher(docUrl);
		if ( rd == null )
		{
			docPage.err("The ESF reports access via pickupCode: " + pickupCode + "; found tran id: " + docPage.context.transactionId + "; resulted in the invalid URL: " + docUrl + "; IP: " + docPage.getIP());
			response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Sorry, we are not able to access the requested transaction via the reports at this time.");
        	return;
		}
		rd.forward(request, response); // include doesn't allow headers to be set
	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    
    public String getServletInfo() 
    {
        return "EsfReportsAccessTransaction " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}