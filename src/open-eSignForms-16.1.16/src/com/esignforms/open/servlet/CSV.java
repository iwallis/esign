// Copyright (C) 2014-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.Application;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfDecimal;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.ReportTemplateReportField;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.reports.ReportListingInfo;
import com.esignforms.open.user.User;
import com.esignforms.open.util.CSVParser;

/**
 * Handles the retrieval and generation of CSV report data using an API.
 * Base URL: https://example.com/WEBAPP/csv/YourReportPathNameHere
 * Params:
 *   rid - Optional; if present, use this report template ID if the YourReportPathNameHere is not found.
 *   ESFLOGINUSER - Required; email address of the user who will effectively run the report (cannot change between invocations on a single session)
 *   ESFLOGINPASSWORD - Required; password for the user account
 *   ESFTEST - Optional; assumes Production mode, but if ESFTEST is present, assumes Test mode unless first char is N or F or 0
 *   dateType - Optional; assumes "started" unless set to "lastUpdated"
 *   startDate - Optional; assumes "now" (today at 00:00:00 in user's preferred timezone). Use -1 for yesterday or number of days previously or YYYY-MM-DD format.
 *   endDate - Optional; assumes "now" (today at 23:59:59 in user's preferred timezone).  Use -1 for yesterday or number of days previously or YYYY-MM-DD format.
 *   startedByMe - Optional; assumes "Y". If "N", you must selected one of the other two 'startedBy' options or 'startedByMe=Y' will be assumed.
 *   startedByAnyUser - Optional; assumes "Y" if the user has permission, else "N"
 *   startedByExternalParties - Optional; assumes "Y" if the user has permission, else "N"
 *   showInProgress - Optional; assumes "N"
 *   showCompleted - Optional; assumes "Y"
 *   showCanceled - Optional; assumes "N"
 *   showSuspended - Optional; assumes "N"
 *   includeHeaderLine - Optional; assumes "Y" (if "Y" include a first line that is the field name headers before the actual data elements)
 *   
 * @author Yozons, Inc.
 */
@WebServlet("/csv/*") 
public class CSV extends HttpServlet 
{
	private static final long serialVersionUID = 3985262855537002717L;


	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
        HttpSession session = request.getSession();
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init(session,request,response);
        
        if ( session == null )
        {
        	docPage.err("Could not get a session. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Your login session for the CSV report could not be created. IP: " + docPage.getIP());
            return;
        }
        
        String esfLoginUserParam = docPage.getParam(StartTransaction.PARAM_ESFLOGINUSER);
        String esfLoginPasswordParam = docPage.getParam(StartTransaction.PARAM_ESFLOGINPASSWORD);
        if ( docPage.areAnyBlank(esfLoginUserParam,esfLoginPasswordParam) )
        {
        	docPage.err("Missing authentication credentials. " + StartTransaction.PARAM_ESFLOGINUSER + ": " + esfLoginUserParam + 
        			"; " + StartTransaction.PARAM_ESFLOGINPASSWORD + " isBlank: " + docPage.isBlank(esfLoginPasswordParam) + 
        			"; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please provide both the " + StartTransaction.PARAM_ESFLOGINUSER + 
        			" and " + StartTransaction.PARAM_ESFLOGINPASSWORD + " credentials.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }

        // The report template id is optional, but if present, it's used if we cannot find the report by name.
        String reportTemplateIdParam = docPage.getParam("rid");
        
        String reportTemplatePathNameParam = request.getPathInfo();
        if ( docPage.isBlank(reportTemplatePathNameParam) )
        {
        	docPage.err("There is no report template path name in the path info of the current link. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No report template path name was specified so we do not know what you'd like to do.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }
        reportTemplatePathNameParam = reportTemplatePathNameParam.substring(1);
        
        EsfPathName reportTemplatePathName = new EsfPathName(reportTemplatePathNameParam);
        if ( ! reportTemplatePathName.isValid() )
        {
        	docPage.err("The report template path name in the path info of the current link is not in a valid format: " + reportTemplatePathNameParam + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. The report template path name (" + reportTemplatePathNameParam + ") specified to is not in a valid format. Your IP address has been logged.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }

        // Get the report in question.  If not found and we have an id, try that next.
        ReportTemplate reportTemplate = ReportTemplate.Manager.getByPathName(reportTemplatePathName);
        if ( reportTemplate == null && docPage.isNonBlank(reportTemplateIdParam) ) 
        {
        	EsfUUID reportTemplateId = new EsfUUID(reportTemplateIdParam);
            if ( reportTemplateId.isNull() )
            {
            	docPage.err("The report template path name in the path info of the current link: " + reportTemplatePathName + "; was not found and the rid is not in a valid format: " + reportTemplateIdParam + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. The report template path name '" + reportTemplatePathName + "' was not found and the rid (" + reportTemplateIdParam + ") specified is not in a valid format. Your IP address has been logged.");
            	if ( session.isNew() )
            		docPage.endSession();
            	return;
            }
            reportTemplate = ReportTemplate.Manager.getById(reportTemplateId);
        }
        
        if ( reportTemplate == null )
        {
        	docPage.err("The report template could not be found by path name: " + reportTemplatePathNameParam + "; or rid: " + reportTemplateIdParam + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. The report template path name (" + reportTemplatePathNameParam + ") and/or rid (" + reportTemplateIdParam + ") specified could not be found. Your IP address has been logged.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }
        
        if ( ! reportTemplate.isEnabled() )
        {
        	docPage.err("The specified report is not enabled. Report: " + reportTemplate.getPathName() + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "The specified report is not enabled.");
        	if ( session.isNew() )
        		docPage.endSession();
            return;
        }
        
        String esftestParam = docPage.getParam(StartTransaction.PARAM_ESFTEST);
        boolean testRequested = false;
        if ( esftestParam != null )
        {
    		// We assume if the ESFTEST param is present, that we want test mode, unless it's set to an "off" value explicitly (starts with N, n, F, f or 0).
        	if ( esftestParam.length() > 0 )
        	{
            	char firstChar = Character.toLowerCase(esftestParam.charAt(0));
            	testRequested = ! (firstChar == 'n' || firstChar == 'f' || firstChar == '0'); 
        	}
        	else
        		testRequested = true; 
        }
        
        docPage.isUserLoggedIn(); // If we have a logged in user, we'll check that user for permissions
        User user = docPage.getLoggedInUser();
    	if ( user != null )
    	{
    		if ( ! user.getEmail().equalsIgnoreCase(esfLoginUserParam) )
    		{
        		user.logSecurity("Attempted to run CSV report: " + reportTemplate.getPathName() + " via API; as login user: " + esfLoginUserParam + "; while already logged in; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	docPage.err("User: " + user.getFullDisplayName() + "; attempted to run CSV report: " + reportTemplate.getPathName() + "; using a different email address: " + esfLoginUserParam + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You are not allowed to run CSV reports using an email address different than your currently logged in email address. Your IP address has been logged.");
            	if ( session.isNew() )
            		docPage.endSession();
            	return;
    		}
    	}
    	else
    	{
    		user = docPage.loginUser(esfLoginUserParam, esfLoginPasswordParam);
    		if ( user == null )
    		{
				response.sendError(HttpServletResponse.SC_FORBIDDEN, "Your automated CSV report request to login as '" + esfLoginUserParam + "' failed. The username or password was invalid. Please check the system requesting the CSV report to ensure it has correct values. Your IP address has been logged.");
	        	if ( session.isNew() )
	        		docPage.endSession();
    			return;
    		}
    	}
     
        if ( ! reportTemplate.hasRunReportPermission(user) )
        {
       		String msg = "User: " + user.getFullDisplayName() + " is not authorized to run report template: " + reportTemplate.getPathName() + " via API; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent();
        	docPage.err(msg);
        	user.logSecurity("No permission to run CSV report template via API: " + reportTemplate.getPathName());
        	docPage.app.getActivityLog().logSystemUserActivity(msg);
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You (" + user.getFullDisplayName() + ") are not allowed to run this report. Your IP address has been logged.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }
        
        if ( ! reportTemplate.hasDownloadCsvPermission(user) )
        {
       		String msg = "User: " + user.getFullDisplayName() + " is not authorized to download CSV using report template: " + reportTemplate.getPathName() + " via API; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent();
        	docPage.err(msg);
        	user.logSecurity("No permission to download CSV using report template: " + reportTemplate.getPathName() + " via API.");
        	docPage.app.getActivityLog().logSystemUserActivity(msg);
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You (" + user.getFullDisplayName() + ") are not allowed to download CSV using this report. Your IP address has been logged.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }
        
        if ( ! testRequested && ! reportTemplate.hasViewProductionPermission(user) )
        {
       		String msg = "User: " + user.getFullDisplayName() + " is not authorized to download production CSV using report template: " + reportTemplate.getPathName() + " via API; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent();
        	docPage.err(msg);
        	user.logSecurity("No permission to download production CSV using report template: " + reportTemplate.getPathName() + " via API.");
        	docPage.app.getActivityLog().logSystemUserActivity(msg);
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You (" + user.getFullDisplayName() + ") are not allowed to download production CSV using this report. Your IP address has been logged.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }
        
        // Okay, should be able to do something with this report
        doCsvReport(reportTemplate,testRequested,docPage);
	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    void doCsvReport(ReportTemplate reportTemplate, boolean isTestRequested, DocumentPageBean docPage)
    	throws ServletException, java.io.IOException
    {
    	// Set the response to be a CSV
    	docPage.response.setContentType(Application.CONTENT_TYPE_CSV+Application.CONTENT_TYPE_CHARSET_UTF_8);
    	docPage.response.setHeader("Content-Disposition", "inline; filename=\"report.csv\"" );
    	
    	// Set up the date type/range information. 
    	String dateType;
    	String dateTypeParam = docPage.getParam("dateType","started");
    	if ( "lastUpdated".equalsIgnoreCase(dateTypeParam) )
    		dateType = Literals.DATE_LAST_UPDATED;
    	else
    		dateType = Literals.DATE_STARTED;
    	
    	EsfDate startDate = EsfDate.CreateGuessFormat(docPage.getParam("startDate","now"));
    	EsfDate endDate = EsfDate.CreateGuessFormat(docPage.getParam("endDate","now"));
    	
    	EsfDateTime esfFromDate = new EsfDateTime(startDate.toDate());
    	esfFromDate.normalizeTimeToZero(docPage.getLoggedInUser().getTimezoneTz());
    	EsfDateTime esfToDate = new EsfDateTime(endDate.toDate());
    	esfToDate.normalizeTimeToEndOfDay(docPage.getLoggedInUser().getTimezoneTz());
    	
    	// We'll run using all transaction ids set for the report that the user can list
    	List<EsfUUID> transactionTemplateIdList = reportTemplate.getTransactionTemplateIdListForUser(docPage.getLoggedInUser());

    	// Set up the started by filter
    	boolean isStartedByMe = docPage.stringToBool(docPage.getParam("startedByMe","Y"));
    	boolean isStartedByAnyUser = docPage.stringToBool(docPage.getParam("startedByAnyUser","Y"));
    	boolean isStartedByExternalParties = docPage.stringToBool(docPage.getParam("startedByExternalParties","Y"));
    	if ( ! reportTemplate.hasViewStartedByAnyUserPermission(docPage.getLoggedInUser()) )
    		isStartedByAnyUser = false;
    	if ( ! reportTemplate.hasViewStartedByExternalUsersPermission(docPage.getLoggedInUser()) )
    		isStartedByExternalParties = false;
    	List<EsfUUID> startedByUserIdList = new LinkedList<EsfUUID>();
    	if ( isStartedByExternalParties )
    		startedByUserIdList.add( new EsfUUID((String)null) );
    	if ( isStartedByAnyUser )
    	{
			for( User u : User.Manager.getAll(docPage.getLoggedInUser()) ) 
			{
				EsfUUID userId = u.getId();
				if ( ! startedByUserIdList.contains(userId) )
					startedByUserIdList.add(userId);
			}
    	}
    	if ( isStartedByMe || startedByUserIdList.size() == 0 ) // if chosen, or we have nothing selected, put us in
    	{
    		if ( ! startedByUserIdList.contains(docPage.getLoggedInUser().getId()) )
    			startedByUserIdList.add(docPage.getLoggedInUser().getId());
    	}
    	
    	// Set up the tran status filters
    	boolean showInProgress = docPage.stringToBool(docPage.getParam("showInProgress","N"));
    	boolean showCompleted = docPage.stringToBool(docPage.getParam("showCompleted","Y"));
    	boolean showCanceled = docPage.stringToBool(docPage.getParam("showCanceled","N"));
    	boolean showSuspended = docPage.stringToBool(docPage.getParam("showSuspended","N"));
    	
    	// Set up the report params based on what they've selected
    	List<ReportListingInfo> foundTranList = ReportListingInfo.Manager.getMatching(reportTemplate,null,
				dateType,esfFromDate,esfToDate,transactionTemplateIdList,
				startedByUserIdList,null,null,! isTestRequested,
				showInProgress,showCompleted,showCanceled,showSuspended,false,null,null,0);
    	
    	String csvResponseText = generateCSV(reportTemplate, foundTranList, docPage);
    	docPage.response.setContentLength(csvResponseText.length());
    	docPage.response.getWriter().print(csvResponseText);
    }
    
	private String generateCSV(ReportTemplate reportTemplate, List<ReportListingInfo> foundReportListingInfoList, DocumentPageBean docPage)
    {
    	List<ReportTemplateReportField> reportTemplateReportFieldList = reportTemplate.getReportTemplateReportFieldList();
    	
    	int estimatedSize = (reportTemplateReportFieldList.size()+1) * 50;  // estimate 50 chars per field
    	estimatedSize *= foundReportListingInfoList == null ? 1 : (foundReportListingInfoList.size()+1); // multiple by number of rows found
    	StringBuilder buf = new StringBuilder(estimatedSize);
    	
    	boolean includeHeaderLine = docPage.stringToBool(docPage.getParam("includeHeaderLine","Y"));
    	
    	boolean needsComma = false;
    	if ( includeHeaderLine )
    	{
    		for( ReportTemplateReportField rtrf : reportTemplateReportFieldList ) 
    		{
    			ReportFieldTemplate reportFieldTemplate = rtrf.getReportFieldTemplate();
    			if ( reportFieldTemplate == null ) 
    				continue;
    			if ( reportFieldTemplate.isFieldRendersAsButton() )
    				continue;
    			
    			if ( needsComma )
    				buf.append(",");
    			else
    				needsComma = true;

    			buf.append(CSVParser.EncodeField(rtrf.getFieldLabel()));
    		}
    		buf.append("\r\n");
    	}
    	
    	if ( foundReportListingInfoList != null )
    	{
        	// Add one line for each tran found
    		for( ReportListingInfo reportList : foundReportListingInfoList ) 
    		{
    	    	needsComma = false;
    			for( ReportTemplateReportField rtrf : reportList.getReportFieldValueList() ) 
    			{
        			ReportFieldTemplate reportFieldTemplate = rtrf.getReportFieldTemplate();
        			if ( reportFieldTemplate == null ) 
        				continue;
        			if ( reportFieldTemplate.isFieldRendersAsButton() )
        				continue;
        			
        			if ( needsComma )
        				buf.append(",");
        			else
        				needsComma = true;

    				if ( reportFieldTemplate.isFieldTypeStringOrRelated() )
    				{
    					EsfString fieldValue = (EsfString)rtrf.getFieldValue();
    					if ( fieldValue != null && ! fieldValue.isNull() )
    					{
    						if ( "left4mask".equals(rtrf.getOutputFormatSpec()) ) 
    							buf.append(CSVParser.EncodeField(docPage.app.maskLeft(fieldValue.toString())));
    						else if ( "right4mask".equals(rtrf.getOutputFormatSpec()) ) 
    							buf.append(CSVParser.EncodeField(docPage.app.maskRight(fieldValue.toString())));
    						else
        						buf.append(CSVParser.EncodeField(fieldValue.toString()));
    					}
    				}
    				else if ( reportFieldTemplate.isFieldTypeDate() )
    				{
    					EsfDate fieldValue = (EsfDate)rtrf.getFieldValue();
    					if ( fieldValue != null && ! fieldValue.isNull() )
    					{
    			   			String dateFormatSpec = rtrf.hasOutputFormatSpec() ? rtrf.getOutputFormatSpec() : docPage.app.getDefaultDateFormat();
    						buf.append(CSVParser.EncodeField(fieldValue.format(dateFormatSpec)));
    					}
    				}
    				else if ( reportFieldTemplate.isFieldTypeDecimal() )
    				{
    					EsfDecimal fieldValue = (EsfDecimal)rtrf.getFieldValue();
    					if ( fieldValue != null && ! fieldValue.isNull() )
    					{
    						if ( rtrf.hasOutputFormatSpec() )
        						buf.append(CSVParser.EncodeField(fieldValue.format(rtrf.getOutputFormatSpec())));
    						else
    							buf.append(CSVParser.EncodeField(fieldValue.toString()));
    					}
    				}
    				else if ( reportFieldTemplate.isFieldTypeInteger() )
    				{
    					EsfInteger fieldValue = (EsfInteger)rtrf.getFieldValue();
    					if ( fieldValue != null && ! fieldValue.isNull() )
    					{
    						if ( rtrf.hasOutputFormatSpec() )
        						buf.append(CSVParser.EncodeField(fieldValue.format(rtrf.getOutputFormatSpec())));
    						else
    							buf.append(CSVParser.EncodeField(fieldValue.toString()));
    					}
    				}
    				else if ( reportFieldTemplate.isFieldTypeBuiltIn() )
    				{
            			String fieldName = reportFieldTemplate.getFieldName().toString();
    					if ( "esf_last_updated_timestamp".equals(fieldName) ||
    						 "esf_start_timestamp".equals(fieldName) ||
    						 "esf_cancel_timestamp".equals(fieldName) ||
    						 "esf_expire_timestamp".equals(fieldName) ||
    						 "esf_stall_timestamp".equals(fieldName)
    					   ) 
    					{
    						EsfDateTime fieldValue = (EsfDateTime)rtrf.getFieldValue();
        					if ( fieldValue != null && ! fieldValue.isNull() )
        					{
        			   			String dateFormatSpec = rtrf.hasOutputFormatSpec() ? rtrf.getOutputFormatSpec() : docPage.app.getDefaultDateTimeFormat();
        						buf.append(CSVParser.EncodeField(fieldValue.format(docPage.getLoggedInUser(),dateFormatSpec)));
        					}
    					}
    					else if ( "esf_last_updated_by_user".equals(fieldName) || "esf_created_by_user".equals(fieldName) ) 
    					{
    						EsfUUID userId = (EsfUUID)rtrf.getFieldValue();
    						if ( userId != null && ! userId.isNull() )
    						{
        						if ( rtrf.getOutputFormatSpec().equals("id") ) 
            						buf.append(CSVParser.EncodeField(userId.toString()));
        						else
        						{
            						User user = User.Manager.getById(userId);
            						if ( user == null )
            							buf.append(CSVParser.EncodeField("???"));
            						else if ( "displayname".equals(rtrf.getOutputFormatSpec()) ) 
            							buf.append(CSVParser.EncodeField(user.getDisplayName()));
            						else if ( "fulldisplayname".equals(rtrf.getOutputFormatSpec()) ) 
            							buf.append(CSVParser.EncodeField(user.getFullDisplayName()));
            						else if ( "personalname".equals(rtrf.getOutputFormatSpec()) ) 
            							buf.append(CSVParser.EncodeField(user.getPersonalName()));
            						else if ( "familyname".equals(rtrf.getOutputFormatSpec()) ) 
            							buf.append(CSVParser.EncodeField(user.getFamilyName()));
            						else
            							buf.append(CSVParser.EncodeField(user.getEmail()));
        						}
    						}
    					}
    					else if ( "esf_status".equals(fieldName) ) 
    					{
    						EsfString status = (EsfString)rtrf.getFieldValue();
    						if ( status != null && ! status.isNull() )
    						{
    							String statusValue;
        						if ( status.equals(Transaction.TRAN_STATUS_IN_PROGRESS) ) 
        							statusValue = docPage.app.getServerMessages().getString("transaction.status.inProgress");
        						else if ( status.equals(Transaction.TRAN_STATUS_COMPLETED) ) 
        							statusValue = docPage.app.getServerMessages().getString("transaction.status.completed");
        						else if ( status.equals(Transaction.TRAN_STATUS_CANCELED) ) 
        							statusValue = docPage.app.getServerMessages().getString("transaction.status.canceled");
        						else if ( status.equals(Transaction.TRAN_STATUS_SUSPENDED) ) 
        							statusValue = docPage.app.getServerMessages().getString("transaction.status.suspended");
        						else
        							statusValue = "???";
        						buf.append(CSVParser.EncodeField(statusValue));
    						}
    					}
    					else
        				{
        					EsfValue fieldValue = rtrf.getFieldValue();
        					buf.append(CSVParser.EncodeField(fieldValue.toString()));
        				}
    				}
    				else 
    				{
    					EsfValue fieldValue = rtrf.getFieldValue();
    					buf.append(CSVParser.EncodeField(fieldValue.toString()));
    				}
    			}
	    		buf.append("\r\n");
    		}
    	}
    	
    	return buf.toString();
    }
    
    
    public String getServletInfo() 
    {
        return "CSV " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}