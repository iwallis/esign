// Copyright (C) 2009-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;

import com.esignforms.open.Application;

/**
 * This class keeps track of sessions as they start and stop.
 * 
 * The following needs to be configured into the web.xml
 * <pre><code>
 * <listener>
 *     <listener-class>com.esignforms.open.servlet.SessionListener</listener-class>
 * </listener>
 * </code></pre>
 * 
 * @author Yozons, Inc.
 */
@WebListener
public class SessionListener
    implements javax.servlet.http.HttpSessionListener
{
    private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(SessionListener.class);
    
	public SessionListener()
	{
		_logger.debug("SessionListener() instantiated");
	}
    
    // For HttpSessionListener interface
	public void sessionCreated(HttpSessionEvent se)
	{
		_logger.debug("SessionListener() - Session created: " + se.getSession().getId());
		//System.err.println((new EsfDateTime()).toDateTimeMsecString() + " *DEBUG* SessionListener() - Session created: " + se.getSession().getId());
		//(new Throwable()).printStackTrace();
        Application.getInstance().getSessionTracker().addSession(se.getSession());
	}
	
    // For HttpSessionListener interface
	public void sessionDestroyed(HttpSessionEvent se)
	{
		_logger.debug("SessionListener() - Session destroyed: " + se.getSession().getId());
		//System.err.println((new EsfDateTime()).toDateTimeMsecString() + " *DEBUG* SessionListener() - Session destroyed: " + se.getSession().getId());
		//(new Throwable()).printStackTrace();
        Application.getInstance().getSessionTracker().removeSession(se.getSession());
	}
}