// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.jsp.PageBean.PageProcessing;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;

/**
 * Handles the pickup of a transaction by pickup code and shows the appropriate page.
 * @author Yozons, Inc.
 */
@WebServlet("/P/*") 
public class PickupTransaction
    extends    HttpServlet 
{
	private static final long serialVersionUID = -4872314748884093081L;


	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
        HttpSession session = request.getSession();
        
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init(session,request,response);
        
        if ( session == null )
        {
        	docPage.err("Could not get a session. IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Your web contracting session could not be created.");
            return;
        }
        
        String pickupCode = request.getPathInfo();
        if ( docPage.isBlank(pickupCode) || pickupCode.length() < 2 ) // it is longer than 2, but we do this to ensure our substring next doesn't fail
        {
        	docPage.err("There is no pickup code in the path info of the current link. IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The expected unique pickup code to identify your request was not specified.");
        	return;
        }
        pickupCode = pickupCode.substring(1);
        
        docPage.isUserLoggedIn(); // Sets our logged in user name
        if ( ! docPage.pickupTransaction(pickupCode) )
        	return;
        
        // We don't want to do any more if the request has been processed fully (most likely a redirect took place).
        if ( docPage.processRequest() == PageProcessing.DONE )
        	return;
        
        request.setAttribute("docPage", docPage); // Save this for the page that services this request
        
        if ( ! docPage.context.transaction.isDeleted() )
        {
        	if ( docPage.isUserLoggedIn() )
                docPage.context.transaction.logGeneral("Party " + docPage.context.pickupPartyName + " on document " + docPage.context.currDocument.getEsfName() + " " + docPage.getDocumentAndPageNumbersInfo() + "; user: " + docPage.getLoggedInUser().getFullDisplayName() + "; IP: " + docPage.getIP());
        	else
        		docPage.context.transaction.logGeneral("Party " + docPage.context.pickupPartyName + " on document " + docPage.context.currDocument.getEsfName() + " " + docPage.getDocumentAndPageNumbersInfo() + "; IP: " + docPage.getIP());
        }
        
        String docUrl = docPage.context.getCurrentDocumentRequestDispatcherUrl();
		RequestDispatcher rd = request.getRequestDispatcher(docUrl);
		if ( rd == null )
		{
			docPage.err("The pickupCode: " + pickupCode + "; found tran id: " + docPage.context.transactionId + "; resulted in the invalid URL: " + docUrl + "; IP: " + docPage.getIP());
			response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Sorry, we are not able to perform the requested transaction at this time.");
        	return;
		}
		rd.forward(request, response); // include doesn't allow headers to be set
	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    
    public String getServletInfo() 
    {
        return "PickupTransaction " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}