// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.runtime.workflow.TransactionEngine;
import com.esignforms.open.user.User;

/**
 * Takes care of starting a new transaction and then redirecting to the pickup code page for the first party.
 * When doing an API start, the result when receiving an HTTP status 200 (all other HTTP status codes indicate a fundamental error) is either:
 *    OK:transaction-id text response where 'transaction-id' is the new transaction's ID; or 
 *    ERROR:error-message response where 'error-message' explains what went wrong
 * @author Yozons, Inc.
 */
@WebServlet("/S/*") 
public class StartTransaction extends HttpServlet 
{
	private static final long serialVersionUID = 1010701324415388723L;

	public static final String PARAM_ESFTEST           			= "ESFTEST";
    public static final String PARAM_ESFLIKEPRODUCTION 			= "ESFLIKEPRODUCTION";
    public static final String PARAM_ESFLOGINUSER      			= "ESFLOGINUSER";
    public static final String PARAM_ESFLOGINPASSWORD  			= "ESFLOGINPASSWORD";
    public static final String PARAM_ESFAUTOCOMPLETEFIRSTPARTY 	= "ESFAUTOCOMPLETEFIRSTPARTY";

	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
		
        HttpSession session = request.getSession();
        
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init(session,request,response);
        
        if ( session == null )
        {
        	docPage.err("Could not get a session. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "System error: A session could not be created to support your new transaction.");
            return;
        }
        
        // The transaction template id is optional, but if present, it's used if we cannot find the transaction by name.
        String transactionTemplateIdParam = docPage.getParam("ttid");
        
        String esftestParam = docPage.getParam(PARAM_ESFTEST);
        boolean testRequested = false;
        if ( esftestParam != null )
        {
    		// We assume if the ESFTEST param is present, that we want test mode, unless it's set to an "off" value explicitly (starts with N, n, F, f or 0).
        	if ( esftestParam.length() > 0 )
        	{
            	char firstChar = Character.toLowerCase(esftestParam.charAt(0));
            	testRequested = ! (firstChar == 'n' || firstChar == 'f' || firstChar == '0'); 
        	}
        	else
        		testRequested = true; 
        }
        
        String esftestLikeProductionParam = docPage.getParam(PARAM_ESFLIKEPRODUCTION);
        boolean testLikeProductionRequested = false;
        if ( testRequested && esftestLikeProductionParam != null )
        {
    		// We assume if the ESFLIKEPRODUCTION param is present, that we want test like production mode, unless it's set to an "off" value explicitly (starts with N, n, F, f or 0).
        	if ( esftestLikeProductionParam.length() > 0 )
        	{
            	char firstChar = Character.toLowerCase(esftestLikeProductionParam.charAt(0));
            	testLikeProductionRequested = ! (firstChar == 'n' || firstChar == 'f' || firstChar == '0'); 
        	}
        	else
        		testLikeProductionRequested = true; 
        }
        
        
        String esfLoginUserParam = docPage.getParam(PARAM_ESFLOGINUSER);
        String esfLoginPasswordParam = docPage.getParam(PARAM_ESFLOGINPASSWORD);
        String esfAutoCompleteFirstPartyParam = docPage.getParam(PARAM_ESFAUTOCOMPLETEFIRSTPARTY);
        boolean autoCompleteFirstPartyLoggedIn = false;
        boolean autoCompleteFirstPartyRequested = esfAutoCompleteFirstPartyParam != null;
        // We assume if the ESFAUTOCOMPLETEFIRSTPARTY param is present, that we want auto-complete mode, unless it's set to an "off" value explicitly (starts with N, n, F, f or 0).
    	if ( esfAutoCompleteFirstPartyParam != null && esfAutoCompleteFirstPartyParam.length() > 0 )
    	{
        	char firstChar = Character.toLowerCase(esfAutoCompleteFirstPartyParam.charAt(0));
        	autoCompleteFirstPartyRequested = ! (firstChar == 'n' || firstChar == 'f' || firstChar == '0'); 
    	}
    	
    	if ( autoCompleteFirstPartyRequested )
    	{
    		docPage.setApiMode(true);
    	}
        
        
        if ( testRequested && ! docPage.app.allowTestTransactions() )
        {
        	docPage.err("Test transactions are not allowed on this deployment. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Test transactions are not currently allowed on this deployment.");
        	if ( session.isNew() )
        		docPage.endSession();
            return;
        }
        
        if ( ! testRequested && ! docPage.app.allowProductionTransactions() )
        {
        	docPage.err("Production transactions are not allowed on this deployment. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Production transactions are not currently allowed on this deployment.");
        	if ( session.isNew() )
        		docPage.endSession();
            return;
        }
        
        String transactionTemplatePathNameParam = request.getPathInfo();
        if ( docPage.isBlank(transactionTemplatePathNameParam) )
        {
        	docPage.err("There is no transaction template path name in the path info of the current link. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No transaction template path name was specified so we do not know what you'd like to do.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }
        // Remove leading /
        transactionTemplatePathNameParam = transactionTemplatePathNameParam.substring(1);
        // If it ends in a /, remove it too
        if ( transactionTemplatePathNameParam.endsWith("/") )
        	transactionTemplatePathNameParam = transactionTemplatePathNameParam.substring(0, transactionTemplatePathNameParam.length()-1);
        
        EsfPathName transactionTemplatePathName = new EsfPathName(transactionTemplatePathNameParam);
        if ( ! transactionTemplatePathName.isValid() )
        {
        	docPage.err("The transaction template path name in the path info of the current link is not in a valid format: " + transactionTemplatePathNameParam + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. The transaction template path name (" + transactionTemplatePathNameParam + ") specified to is not in a valid format. Your IP address has been logged.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }
        
        // Get the transaction template in question.  If not found and we have an id, try that next.
        TransactionTemplate transactionTemplate = TransactionTemplate.Manager.getByPathName(transactionTemplatePathName);
        if ( transactionTemplate == null && docPage.isNonBlank(transactionTemplateIdParam) ) 
        {
        	EsfUUID transactionTemplateId = new EsfUUID(transactionTemplateIdParam);
            if ( transactionTemplateId.isNull() )
            {
            	docPage.err("The transaction template path name in the path info of the current link: " + transactionTemplatePathName + "; was not found and the tid is not in a valid format: " + transactionTemplateIdParam + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. The transaction template path name '" + transactionTemplatePathName + "' was not found and the tid (" + transactionTemplateIdParam + ") specified is not in a valid format. Your IP address has been logged.");
            	if ( session.isNew() )
            		docPage.endSession();
            	return;
            }
            transactionTemplate = TransactionTemplate.Manager.getById(transactionTemplateId);
        }
        
        if ( transactionTemplate == null )
        {
        	docPage.err("The transaction template could not be found by path name: " + transactionTemplatePathNameParam + "; or tid: " + transactionTemplateIdParam + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. The transaction template path name (" + transactionTemplatePathNameParam + ") and/or tid (" + transactionTemplateIdParam + ") specified could not be found. Your IP address has been logged.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }
        
        
        if ( testRequested && ! transactionTemplate.isTestEnabled() )
        {
        	docPage.err("Test transactions are not allowed on this type of transaction. transactionTemplate: " + transactionTemplate.getPathName() + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Test transactions are not currently allowed on this type of transaction.");
        	if ( session.isNew() )
        		docPage.endSession();
            return;
        }
        
        if ( ! testRequested && ! transactionTemplate.isProductionEnabled() )
        {
        	docPage.err("Production transactions are not allowed on this type of transaction. transactionTemplate: " + transactionTemplate.getPathName() + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Production transactions are not currently allowed on this type of transaction.");
        	if ( session.isNew() )
        		docPage.endSession();
            return;
        }
        
        if ( testLikeProductionRequested && ! transactionTemplate.isProductionEnabled() )
        {
        	docPage.err("Test like Production transactions are not allowed on this type of transaction. transactionTemplate: " + transactionTemplate.getPathName() + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Test like Production transactions are not currently allowed on this type of transaction.");
        	if ( session.isNew() )
        		docPage.endSession();
            return;
        }
        
        docPage.isUserLoggedIn(); // If we have a logged in user, we'll check that user for start permission, otherwise start must be give to "External Users"
        User user = docPage.getLoggedInUser();

        // If requested a login, we check any current logins and then attempt the login if needed
        if ( EsfString.areAllNonBlank(esfLoginUserParam,esfLoginPasswordParam) )
        {
        	if ( user != null )
        	{
        		if ( ! user.getEmail().equalsIgnoreCase(esfLoginUserParam) )
        		{
            		user.logSecurity("Attempted to start transaction template: " + transactionTemplate.getPathName() + "; as login user: " + esfLoginUserParam + "; while already logged in; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
                	docPage.err("User: " + user.getFullDisplayName() + "; attempted to start transactionTemplate: " + transactionTemplate.getPathName() + "; using a different email address: " + esfLoginUserParam + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
                	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You are not allowed to start transactions using an email address different than your currently logged in email address. Your IP address has been logged.");
                	if ( session.isNew() )
                		docPage.endSession();
                	return;
        		}
        	}
        	else
        	{
        		user = docPage.loginUser(esfLoginUserParam, esfLoginPasswordParam);
        		if ( user == null )
        		{
        			if ( docPage.isApiMode() )
                		sendAPIError(docPage,autoCompleteFirstPartyLoggedIn);
        			else
        			{
        				response.sendError(HttpServletResponse.SC_FORBIDDEN, "Your automated request to login as '" + esfLoginUserParam + "' failed. The username or password was invalid. Please check the system starting the transaction to ensure it has correct values. Your IP address has been logged.");
        	        	if ( session.isNew() )
        	        		docPage.endSession();
        			}
        			return;
        		}
                if ( docPage.isApiMode() )
                	autoCompleteFirstPartyLoggedIn = true; // we logged them in for auto-complete, so we'll auto-logoff at the end
        	}
        }
        
        // Let's check if the user has permission to start this transaction.
        if ( ! transactionTemplate.hasStartPermission(user) )
        {
        	if ( user == null )
        	{
        		// If this is an API call or they provided either the login user or password params, we just block, otherwise we'll redirect to the login page
        		if ( docPage.isApiMode() )
        		{
                	docPage.err("External API users are not authorized to start transaction templateTemplate: " + transactionTemplate.getPathName() + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
                	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You are not allowed to start this type of transaction via the API without providing user authentication credentials. Your IP address has been logged.");
                	if ( session.isNew() )
                		docPage.endSession();
        		}
        		else if ( EsfString.areAnyNonBlank(esfLoginUserParam,esfLoginPasswordParam) )
        		{
                	docPage.err("External users are not authorized to start transactionTemplate: " + transactionTemplate.getPathName() + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
                	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You are not allowed to start this type of transaction. If you believe you are authorized, please login first. Your IP address has been logged.");
                	if ( session.isNew() )
                		docPage.endSession();
        		}
        		else
        			docPage.checkLoggedIn();  // will do the redirect to the login page
        	}
        	else
        	{
        		String msg = "User: " + user.getFullDisplayName() + " is not authorized to start transaction template: " + transactionTemplate.getPathName() + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent();
            	docPage.err(msg);
            	user.logSecurity("No permission to start transaction template: " + transactionTemplate.getPathName());
            	docPage.app.getActivityLog().logSystemUserActivity(msg);
            	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You (" + user.getFullDisplayName() + ") are not allowed to start this type of transaction. Your IP address has been logged.");
            	if ( session.isNew() )
            		docPage.endSession();
        	}
            return;
        }

        Package pkg = transactionTemplate.getPackage();
        if ( pkg == null )
        {
        	docPage.err("The transactiontemplate: " + transactionTemplate.getPathName() + " is misconfigured; missing packageId: " + transactionTemplate.getPackageId() + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The requested transaction/package cannot be started at this time.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }
        
        PackageVersion pkgVersion = (testRequested && ! testLikeProductionRequested) ? pkg.getTestPackageVersion() : pkg.getProductionPackageVersion();
        if ( pkgVersion == null )
        {
        	docPage.err("Production transactions are not allowed on transactionTemplate (" + transactionTemplate.getPathName() + ") yet because its package of documents (" + 
        			    pkg.getPathName() + ") is still marked for testing only. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Production transactions are not currently allowed on the package of documents defined for this type of transaction.");
        	if ( session.isNew() )
        		docPage.endSession();
            return;
        }
        
        PackageVersionPartyTemplate packagePartyTemplate = pkgVersion.getFirstPackageVersionPartyTemplate();
        if ( packagePartyTemplate == null )
        {
        	docPage.err("The transactionTemplate: " + transactionTemplate.getPathName() + "; has no parties defined; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The transaction template has no parties defined, so it cannot be started at this time.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }

        // See if this is a start based on cloning an existing transaction
        String cloneFromTransactionId = docPage.getSessionStringAttribute(Transaction.SESSION_ATTR_CLONE_TRANSACTION_ID);
        Transaction cloneFromTransaction = null;
        if ( EsfString.isNonBlank(cloneFromTransactionId) )
        {
        	docPage.removeSessionAttribute(Transaction.SESSION_ATTR_CLONE_TRANSACTION_ID);
        	cloneFromTransaction = Transaction.Manager.getById( new EsfUUID(cloneFromTransactionId) );
        	if ( cloneFromTransaction == null )
        	{
            	docPage.err("The transactionTemplate: " + transactionTemplate.getPathName() + "; was to be cloned from transaction id: " + cloneFromTransactionId + "; but no such transaction was found; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The transaction could not be cloned because the original transaction could not be found.");
            	if ( session.isNew() )
            		docPage.endSession();
            	return;
        	}
        	if ( ! cloneFromTransaction.getTransactionTemplateId().equals(transactionTemplate.getId()) )
        	{
            	docPage.err("The transactionTemplate: " + transactionTemplate.getPathName() + "; was to be cloned from transaction id: " + cloneFromTransactionId + "; but they are not the same transaction type; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The transaction could not be cloned because the original transaction specified is not the same type.");
            	if ( session.isNew() )
            		docPage.endSession();
            	return;
        	}
        }

        // Now create the transaction 
        Transaction transaction;
        try
        {
	        if ( testLikeProductionRequested )
	        	transaction = Transaction.Manager.createTestLikeProduction(transactionTemplate, user, request, cloneFromTransaction);
	        else
	        	transaction = testRequested ? Transaction.Manager.createTest(transactionTemplate, user, request, cloneFromTransaction) : Transaction.Manager.createProduction(transactionTemplate, user, request, cloneFromTransaction);
        }
        catch( EsfException e )
        {
        	docPage.except(e,"The transactionTemplate: " + transactionTemplate.getPathName() + "; cannot be created; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The requested transaction cannot be started at this time.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }
        
        // Retrieve the first party defined and activate it
        TransactionParty tranParty = transaction.getTransactionParty(packagePartyTemplate.getEsfName());
        if ( tranParty == null )
        {
        	docPage.err("The transactionTemplate: " + transactionTemplate.getPathName() + " is misconfigured; TransactionParty could not be found for PackagePartyTemplate: " + packagePartyTemplate.getEsfName() + "; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The requested transaction/party cannot be started at this time.");
        	if ( session.isNew() )
        		docPage.endSession();
        	return;
        }
        
        String pickupCode = tranParty.getCurrentAssignment().getPickupCode();
        TransactionContext context = DocumentPageBean.TransactionPickupManager.registerPickupCode(user, session, pickupCode, packagePartyTemplate.getEsfName(), transaction.getId(), 
        																						  docPage.getIPOnly(), docPage.getIP(), docPage.getUserAgent());
        
        // Now let's do all this work in a DB transaction 
        ConnectionPool    pool = docPage.app.getConnectionPool();
        Connection        con  = pool.getConnection();
        
        try
        {
            TransactionEngine engine = new TransactionEngine(context);
            engine.queueTransactionStartedEvent();
            engine.queuePartyCreatedEvent(packagePartyTemplate.getEsfName());
            engine.queuePartyActivatedEvent(packagePartyTemplate.getEsfName());
            engine.doWork(con, docPage.errors);
            
            if ( ! transaction.save(con) )
            {
            	docPage.err("The transactionTemplate: " + transactionTemplate.getPathName() + "; cannot save the newly started transaction; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The newly created transaction cannot be saved at this time.");
            	if ( session.isNew() )
            		docPage.endSession();
            	return;
            }
            
            con.commit();

            if ( docPage.isApiMode() )
            {
            	if ( docPage.processPartyAutomatically(pickupCode) )
            		sendAPISuccess(docPage,autoCompleteFirstPartyLoggedIn);
            	else
            		sendAPIError(docPage,autoCompleteFirstPartyLoggedIn);
            }
            else
            	docPage.sendRedirect(docPage.getContextPath()+"/P/"+pickupCode);  // redirect starting user to the pickup page
        }
        catch(EsfException e) 
        {
            pool.rollbackIgnoreException(con);
        	docPage.except(e,"The transactionTemplate: " + transactionTemplate.getPathName() + "; cannot process the newly started transaction; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The transaction cannot be started at this time.");
        	if ( session.isNew() )
        		docPage.endSession();
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
        	docPage.except(e,"The transactionTemplate: " + transactionTemplate.getPathName() + "; cannot save the newly started transaction; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "The transaction cannot be started at this time.");
        	if ( session.isNew() )
        		docPage.endSession();
        }
        finally
        {
        	docPage.app.cleanupPool(pool,con,null);
        }
	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    void sendAPISuccess(DocumentPageBean docPage, boolean autoCompleteFirstPartyLoggedIn) throws IOException
    {
    	docPage.response.setContentType(Application.CONTENT_TYPE_TEXT+Application.CONTENT_TYPE_CHARSET_UTF_8);
    	docPage.response.getWriter().println("OK:"+docPage.context.transactionId);
    	
    	if ( autoCompleteFirstPartyLoggedIn )
    		docPage.logoffUser();
    }

    void sendAPIError(DocumentPageBean docPage, boolean autoCompleteFirstPartyLoggedIn) throws IOException
    {
    	docPage.response.setContentType(Application.CONTENT_TYPE_TEXT+Application.CONTENT_TYPE_CHARSET_UTF_8);
    	docPage.response.getWriter().println("ERROR:"+docPage.errors.toString());
    	
    	if ( autoCompleteFirstPartyLoggedIn )
    		docPage.logoffUser();
    }
    
    
    public String getServletInfo() 
    {
        return "StartTransaction " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}