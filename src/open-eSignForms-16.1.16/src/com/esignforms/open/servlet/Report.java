// Copyright (C) 2013-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.Application;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.email.OutboundEmailMessageAttachment;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionFile;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.runtime.reports.ReportListingInfo;

/**
 * Handles the retrieval of reports using an API.
 * @author Yozons, Inc.
 */
@WebServlet("/reports/*") 
public class Report extends HttpServlet 
{
	private static final long serialVersionUID = -8877880675037044334L;


	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
        HttpSession session = request.getSession();
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init(session,request,response);
        
        if ( session == null )
        {
        	docPage.err("Could not get a session. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Your login session for the report could not be created. IP: " + docPage.getIP());
            return;
        }
        
        String typeRequested = docPage.getParam("type");
        if ( EsfString.isBlank(typeRequested) )
        {
        	docPage.err("Missing required 'type' param.");
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Your report data cannot be retrieved because it is missing the 'type' parameter.");
            return;
        }
        
        if ( "export-transaction-list".equals(typeRequested) )
        {
        	// This request is only valid for users who are logged in and just ran a report to retrieve transactions to be exported.
            if ( ! docPage.isUserLoggedIn() )
            {
            	docPage.err("User is not logged in. IP: " + docPage.getIP());
            	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Your login session has terminated. Please login to export transactions via a report.");
                return;
            }
        	
        	String reportTemplateIdParam = docPage.getParam("rtid");
        	if ( EsfString.isBlank(reportTemplateIdParam) ) 
        	{
            	docPage.err("Missing required 'rtid' param for exporting transactions found by running a report.");
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Your report data cannot be exported because it is missing the 'rtid' parameter.");
                return;
        	}

        	String foundTranListPickupCodeParam = docPage.getParam("c");
        	if ( EsfString.isBlank(foundTranListPickupCodeParam) ) 
        	{
            	docPage.err("Missing required 'c' param for exporting transactions found by running a report.");
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Your report data cannot be exported because it is missing the 'c' parameter.");
                return;
        	}

        	@SuppressWarnings("unchecked")
			List<ReportListingInfo> foundTranList = (List<ReportListingInfo>)docPage.getSessionAttribute(foundTranListPickupCodeParam);
        	if ( foundTranList == null || foundTranList.size() < 1 )
        	{
            	docPage.err("The 'c' param for exporting transactions found by running a report did not point to any report listing records.");
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Your report data cannot be exported because no report listing records could be found.");
                return;
        	}
        	
        	EsfUUID reportTemplateId = new EsfUUID(reportTemplateIdParam);
        	if ( reportTemplateId.isNull() )
        	{
            	docPage.err("The report template id param rtid was not a valid UUID: " + reportTemplateIdParam);
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Your report data cannot be exported because the request report template id is not valid.");
                return;
        	}
        	
        	ReportTemplate reportTemplate = ReportTemplate.Manager.getById(reportTemplateId);
        	if ( reportTemplate == null )
        	{
            	docPage.err("The report template id param rtid: " + reportTemplateIdParam + "; did not match an existing report template id.");
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Your report data cannot be exported because the request report template is not for an existing report template.");
                return;
        	}
        	
        	doTransactionExport(reportTemplate,foundTranList,docPage);
        }
        
	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    void doTransactionExport(ReportTemplate reportTemplate, List<ReportListingInfo> foundTranList, DocumentPageBean docPage)
    	throws ServletException, java.io.IOException
    {
    	if ( ! reportTemplate.isEnabled() )
    	{
        	docPage.err("The report template is not enabled; id: " + reportTemplate.getId());
        	docPage.getLoggedInUser().logSecurity("User attempted to export transactions for disabled report template id: " + reportTemplate.getId());
        	docPage.response.sendError(HttpServletResponse.SC_FORBIDDEN, "Your report data cannot be exported because the requested report template is not enabled.");
            return;
    	}

    	if ( ! reportTemplate.hasDownloadArchivePermission(docPage.getLoggedInUser()) )
    	{
        	docPage.err("The user: " + docPage.getLoggedInUser().getFullDisplayName() + " is not authorized to run the report template id: " + reportTemplate.getId());
        	docPage.getLoggedInUser().logSecurity("User attempted to export transactions without permission for report template id: " + reportTemplate.getId());
        	docPage.response.sendError(HttpServletResponse.SC_FORBIDDEN, "Your report data cannot be exported because the requested report template is not enabled.");
            return;
    	}
    	
        String reportName = reportTemplate.getPathName().toString().replaceAll("/", "-");
        String exportFileName = reportName + ".zip";
        
        ZipOutputStream zos = null;

        ConnectionPool pool = docPage.app.getConnectionPool();
        Connection     con  = pool.getConnection();
            
        try
        {
            // Set the response to be a ZIP
            docPage.response.setContentType(Application.CONTENT_TYPE_ZIP);
            docPage.response.setHeader("Content-Disposition", "attachment; filename=\"" + exportFileName + "\"" );
            zos = new ZipOutputStream(docPage.response.getOutputStream());
            zos.setComment("Open eSignForms transaction archive exported on " + (new EsfDate()).toYMDString());
            zos.setLevel(9);

            // Get the index listing for all trans in this export archive
            String exportAuthCode = docPage.app.getRandomKey().getLoginAuthorizationString();
            Object[] objects = new Object[3];
            objects[0] = foundTranList;
            objects[1] = reportTemplate;
            objects[2] = docPage.getLoggedInUser();
            docPage.setApplicationAttribute(exportAuthCode,objects);
            byte[] htmlBytes = getHtmlFromPage(docPage,docPage.getExternalUrlContextPath()+"/tranExport/tranIndex.jsp?exportAuthCode="+docPage.encode(exportAuthCode));   
            docPage.removeApplicationAttribute(exportAuthCode);
            if ( htmlBytes != null )
            {
                ZipEntry zipEntry = new ZipEntry( "index.html" );
                zipEntry.setSize(htmlBytes.length);
                zos.putNextEntry(zipEntry);
                zos.write(htmlBytes);
                zos.closeEntry();
            }

            // For each tran we have in our report listing, let's export it
            for( ReportListingInfo reportListingInfo : foundTranList )
            {
                Transaction transaction = Transaction.Manager.getById(con,reportListingInfo.getId());  
                if ( transaction == null )
                {
                    docPage.warning("Could not load transaction id: " + reportListingInfo.getId() + "; skipping inclusion in exported transactions.");
                    continue;
                }
                
                String tranPath = "tran-"+transaction.getId()+"/";
                exportTransaction(docPage,con,zos,tranPath,transaction,reportTemplate);
    			Transaction.Manager.removeFromCache(transaction);
            }                

            con.commit();
        }
        catch(SQLException e) 
        {
            docPage.app.sqlerr(e,"Report.doTransactionExport()");
            pool.rollbackIgnoreException(con,e);
        }
        catch( Exception e )
        {
            docPage.except(e,"Failed for ZIP: " + exportFileName);
        }
        finally
        {
            if ( zos != null )
            {
                try { zos.close(); } catch( Exception e2 ) {}
            }
            docPage.app.cleanupPool(pool,con,null);
        }
    }
    
    private static void exportTransaction(DocumentPageBean docPage, Connection con, ZipOutputStream zos, String tranPath, Transaction transaction, ReportTemplate reportTemplate)
	    throws Exception
	{
    	if ( reportTemplate.hasViewSnapshotDataPermission(docPage.getLoggedInUser()) )
    	{
	        byte[] data = transaction.getRecord().toXmlByteArray();
	        ZipEntry zipEntry = new ZipEntry( tranPath + "tranData.xml" );
	        zipEntry.setTime(transaction.getLastUpdatedTimestamp().toNumber());
	        zipEntry.setSize(data.length);
	        zos.putNextEntry(zipEntry);
	        zos.write(data);
	        zos.closeEntry();
    	}
    	
    	if ( reportTemplate.hasViewActivityLogPermission(docPage.getLoggedInUser()) )
    	{
        	String exportAuthCode = docPage.app.getRandomKey().getLoginAuthorizationString();
    	    Object[] objects = new Object[3];
    	    objects[0] = transaction;
    	    objects[1] = reportTemplate;
    	    objects[2] = docPage.getLoggedInUser();	    
    	    docPage.setApplicationAttribute(exportAuthCode,objects);
    	    byte[] htmlBytes = getHtmlFromPage(docPage,docPage.getExternalUrlContextPath()+"/tranExport/activityLog.jsp?exportAuthCode="+docPage.encode(exportAuthCode));
    	    docPage.removeApplicationAttribute(exportAuthCode);
    	    if ( htmlBytes != null )
    	    {
    	        ZipEntry zipEntry = new ZipEntry( tranPath + "activityLog.html" );
    	        zipEntry.setTime(transaction.getLastUpdatedTimestamp().toNumber());
    	        zipEntry.setSize(htmlBytes.length);
    	        zos.putNextEntry(zipEntry);
    	        zos.write(htmlBytes);
    	        zos.closeEntry();
    	    }
    	}
    	
    	if ( reportTemplate.hasViewEmailLogPermission(docPage.getLoggedInUser()) )
    	{
        	String exportAuthCode = docPage.app.getRandomKey().getLoginAuthorizationString();
    	    Object[] objects = new Object[3];
    	    objects[0] = transaction;
    	    objects[1] = reportTemplate;
    	    objects[2] = docPage.getLoggedInUser();	    
    	    docPage.setApplicationAttribute(exportAuthCode,objects);
    	    byte[] htmlBytes = getHtmlFromPage(docPage,docPage.getExternalUrlContextPath()+"/tranExport/emailLog.jsp?exportAuthCode="+docPage.encode(exportAuthCode));
    	    docPage.removeApplicationAttribute(exportAuthCode);
    	    if ( htmlBytes != null )
    	    {
    	        ZipEntry zipEntry = new ZipEntry( tranPath + "emailLog.html" );
    	        zipEntry.setTime(transaction.getLastUpdatedTimestamp().toNumber());
    	        zipEntry.setSize(htmlBytes.length);
    	        zos.putNextEntry(zipEntry);
    	        zos.write(htmlBytes);
    	        zos.closeEntry();
    	    }
    	    // Let's see if they gave us any email attachments to export
    	    @SuppressWarnings("unchecked")
			List<OutboundEmailMessageAttachment> attList = (List<OutboundEmailMessageAttachment>)docPage.getApplicationAttribute(exportAuthCode+"-email-attList");
    	    if ( attList != null )
    	    {
    	    	docPage.removeApplicationAttribute(exportAuthCode+"-email-attList");
    	    	for( OutboundEmailMessageAttachment att : attList )
    	    	{
    	    		byte[] attData = att.getFileDataFromDatabase();
        	        ZipEntry zipEntry = new ZipEntry( tranPath + "email-att-"+att.getId()+"/"+att.getFileName() );
        	        zipEntry.setTime(transaction.getLastUpdatedTimestamp().toNumber());
        	        zipEntry.setSize(attData.length);
        	        zos.putNextEntry(zipEntry);
        	        zos.write(attData);
        	        zos.closeEntry();
    	    	}
    	    }
    	}
    	
	    // Set the objects for tranDetail.jsp to use
    	String exportAuthCode = docPage.app.getRandomKey().getLoginAuthorizationString();
	    Object[] objects = new Object[3];
	    objects[0] = transaction;
	    objects[1] = reportTemplate;
	    objects[2] = docPage.getLoggedInUser();	    
	    docPage.setApplicationAttribute(exportAuthCode,objects);
	    byte[] htmlBytes = getHtmlFromPage(docPage,docPage.getExternalUrlContextPath()+"/tranExport/tranDetail.jsp?exportAuthCode="+docPage.encode(exportAuthCode));
	    docPage.removeApplicationAttribute(exportAuthCode);
	    if ( htmlBytes != null )
	    {
	        ZipEntry zipEntry = new ZipEntry( tranPath + "tranDetail.html" );
	        zipEntry.setTime(transaction.getLastUpdatedTimestamp().toNumber());
	        zipEntry.setSize(htmlBytes.length);
	        zos.putNextEntry(zipEntry);
	        zos.write(htmlBytes);
	        zos.closeEntry();
	    }
	    
	    // Output each transaction document XML data as well as digitally signed snapshots
	    for( TransactionDocument tranDoc : transaction.getAllTransactionDocuments() )
	    {
	    	if ( reportTemplate.hasViewSnapshotDataPermission(docPage.getLoggedInUser()) )
	    	{
		        byte[] data = tranDoc.getRecord().toXmlByteArray();
		        ZipEntry zipEntry = new ZipEntry( tranPath + "docdata-"+tranDoc.getId()+".xml" );
		        zipEntry.setTime(transaction.getLastUpdatedTimestamp().toNumber());
		        zipEntry.setSize(data.length);
		        zos.putNextEntry(zipEntry);
		        zos.write(data);
		        zos.closeEntry();
	    	}
	    	for ( TransactionFile tranFile : transaction.getAllTransactionFilesForDocument(tranDoc.getId()) ) 
	    	{
		        byte[] data = tranFile.getFileDataFromDatabase();
		        ZipEntry zipEntry = new ZipEntry( tranPath + "tranfile-"+tranFile.getId()+"/"+tranFile.getFileName() );
		        zipEntry.setTime(transaction.getLastUpdatedTimestamp().toNumber());
		        zipEntry.setSize(data.length);
		        zos.putNextEntry(zipEntry);
		        zos.write(data);
		        zos.closeEntry();
	    	}
	    	if ( reportTemplate.hasViewSnapshotDataPermission(docPage.getLoggedInUser()) || reportTemplate.hasViewSnapshotDocumentPermission(docPage.getLoggedInUser()) )
	    	{
	    		List<TransactionParty> tranPartyList = transaction.getAllTransactionParties();
	    		for ( TransactionParty tranParty : tranPartyList )
	    		{
	    			TransactionPartyDocument foundTranPartyDoc = null;
	    			List<TransactionPartyDocument> transactionPartyDocuments = tranParty.getNonSkippedTransactionPartyDocuments();
	            	for( TransactionPartyDocument tpd : transactionPartyDocuments ) 
	            	{
	            		if ( tpd.getTransactionDocumentId().equals(tranDoc.getId()) ) 
	            		{
	            			foundTranPartyDoc = tpd;
	            			break;
	            		}
	            	}
                	if ( foundTranPartyDoc == null ) {
                		continue;
                	}
                	if ( foundTranPartyDoc.hasSnapshotXmlOrBlobId() )
                	{
                		if ( reportTemplate.hasViewSnapshotDataPermission(docPage.getLoggedInUser()) )
                		{
                    		String dataSnapshot = foundTranPartyDoc.getSnapshotData();
                    		if ( EsfString.isNonBlank(dataSnapshot) )
                    		{
                		        byte[] data = EsfString.stringToBytes(dataSnapshot);
                		        ZipEntry zipEntry = new ZipEntry( tranPath + "datasnapshot-"+foundTranPartyDoc.getTransactionPartyId()+"/"+foundTranPartyDoc.getTransactionDocumentId()+".xml" );
                		        zipEntry.setTime(transaction.getLastUpdatedTimestamp().toNumber());
                		        zipEntry.setSize(data.length);
                		        zos.putNextEntry(zipEntry);
                		        zos.write(data);
                		        zos.closeEntry();
                    		}
                		}

                		if ( reportTemplate.hasViewSnapshotDocumentPermission(docPage.getLoggedInUser()) )
                		{
                    		String documentSnapshot = foundTranPartyDoc.getSnapshotDocument();
                    		if ( EsfString.isNonBlank(documentSnapshot) )
                    		{
                		        byte[] data = EsfString.stringToBytes(documentSnapshot);
                		        ZipEntry zipEntry = new ZipEntry( tranPath + "docsnapshot-"+foundTranPartyDoc.getTransactionPartyId()+"/"+foundTranPartyDoc.getTransactionDocumentId()+".html" );
                		        zipEntry.setTime(transaction.getLastUpdatedTimestamp().toNumber());
                		        zipEntry.setSize(data.length);
                		        zos.putNextEntry(zipEntry);
                		        zos.write(data);
                		        zos.closeEntry();
                    		}
                		}

                		// If can download data and doc snapshots, give them the digitally signed XML
            	    	if ( reportTemplate.hasViewSnapshotDataPermission(docPage.getLoggedInUser()) && reportTemplate.hasViewSnapshotDocumentPermission(docPage.getLoggedInUser()) )
            	    	{
            	    		String snapshotXml = foundTranPartyDoc.getSnapshotXml();
                    		if ( EsfString.isNonBlank(snapshotXml) )
                    		{
                		        byte[] data = EsfString.stringToBytes(snapshotXml);
                		        ZipEntry zipEntry = new ZipEntry( tranPath + "dsigsnapshots-"+foundTranPartyDoc.getTransactionPartyId()+"/"+foundTranPartyDoc.getTransactionDocumentId()+".xml" );
                		        zipEntry.setTime(transaction.getLastUpdatedTimestamp().toNumber());
                		        zipEntry.setSize(data.length);
                		        zos.putNextEntry(zipEntry);
                		        zos.write(data);
                		        zos.closeEntry();
                    		}
            	    	}
                	} // end has snapshots
	    		} // end foreach tranParty
	    	} // end has permission
	    } // end foreach tranDoc
	}

    
    static byte[] getHtmlFromPage(DocumentPageBean docPage, String urlString)
    {
        java.net.HttpURLConnection con = null;
        java.io.BufferedReader     br  = null;
        
        try
        {
            URL url = new URL(urlString);
            
            // Create the connection and set the connection properties
            con = (java.net.HttpURLConnection)url.openConnection();
            con.setRequestMethod("GET");
            con.setAllowUserInteraction(false);  // this is program, so no popups
            con.setDoInput(true);      // we will read from the URL
            con.setDoOutput(false);     // we will not write to the URL
            con.setIfModifiedSince(0); // always get
            con.setUseCaches(false);   // don't use cache
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Accept", "*/*"); 
            con.connect();
            
            java.io.InputStream is = con.getInputStream();
            if ( is == null )
            {
            	docPage.err("Could not retrieve page using URL: " + urlString);
                return null;
            }
                
            int contentLength = con.getContentLength();
                
            if ( contentLength < 1 )
                contentLength = 10 * Literals.KB;
                
            StringBuilder response = new StringBuilder(contentLength+32);
                
            br = new java.io.BufferedReader( new java.io.InputStreamReader(con.getInputStream()));
            String str = null;
            while ( (str = br.readLine()) != null )
            {
                response.append(str);
            }
            str = response.toString();
            
            if ( EsfString.isBlank(str) )
            {
            	docPage.err("Nothing retrieved using URL: " + urlString);
                return null;
            }
            
            return EsfString.stringToBytes(str);
        }
        catch( java.io.IOException e )
        {
        	docPage.except(e," Could not retrieve page using URL: " + urlString);
            return null;
        }
        finally
        {
            if ( br != null )
                try { br.close(); } catch( Exception e2 ) {}
            if ( con != null )
                con.disconnect();
        }
    }

    
    public String getServletInfo() 
    {
        return "Report " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}