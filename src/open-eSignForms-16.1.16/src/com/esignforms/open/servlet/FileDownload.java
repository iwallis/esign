// Copyright (C) 2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.runtime.TransactionFile;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
 * Handles the retrieval/download of file fields (uploaded files) by id embedded in a document.  This is generally used when clicking the
 * link in review mode or in a document capture link (such as from reports and parties clicking on the files from the downloaded copy).
 * @author Yozons, Inc.
 */
@WebServlet("/filedownload/*") 
public class FileDownload extends HttpServlet 
{
	private static final long serialVersionUID = -2861549313315134976L;


	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
        HttpSession initialSession = request.getSession(false);
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init( initialSession == null ? request.getSession() : initialSession, request, response);
        
        try
        {
            String fileFieldName = request.getPathInfo();
            if ( EsfString.isBlank(fileFieldName) )
            {
            	docPage.err("FileDownload.doGet() - No file field name was specified on the request; IP: " + docPage.getIP());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No file field name was specified.");
            	return;
            }
            fileFieldName = fileFieldName.substring(1,fileFieldName.indexOf("/", 1));
            
        	String tfidParam = docPage.getParam("tfid"); 
            if ( EsfString.isBlank(tfidParam) )
            {
            	docPage.err("FileDownload.doGet() - No 'tfid' param was specified on the request; IP: " + docPage.getIP());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No file id was specified.");
            	return;
            }
        	
        	String pickupCodeParam = docPage.getParam("puc"); // This will identify the transaction and party whose document was used to upload/download the file
            if ( ! docPage.app.getRandomKey().isCorrectPickupCodeLength(pickupCodeParam) )
            {
            	docPage.err("FileDownload.doGet() - No 'puc' param was specified on the request; IP: " + docPage.getIP());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The pickup code specified to identify your request is missing or not valid.");
            	return;
            }
        	
        	String tdidParam = docPage.getParam("tdid"); // This will identify the transaction document
            if ( EsfString.isBlank(tdidParam) )
            {
            	docPage.err("FileDownload.doGet() - No 'tdid' param was specified on the request; IP: " + docPage.getIP());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No transaction document id was specified.");
            	return;
            }
        	
            docPage.isUserLoggedIn(); // Sets our logged in user name
            if ( ! docPage.pickupTransaction(pickupCodeParam,false) )
            	return;
                        
            docPage.context = new TransactionContext(docPage.context); // Create our own copy so we don't update the document/page status elsewhere
            
            // Let's ensure this party can access the requested document
            EsfUUID tdid = new EsfUUID(tdidParam);
            docPage.context.goToDocumentForTransactionDocumentAndParty(tdid,docPage.context.transactionParty.getId());
            if ( docPage.context.isPackageDocument() )
            {
            	docPage.context.transaction.logGeneral("FileDownload ERROR - File not found for party. Party: " + docPage.context.pkgVersionPartyTemplate.getEsfName() +
            			"tdid: " + tdid + "; tfid: " + tfidParam + "; transactionPartyId: " + docPage.context.transactionParty.getId() + 
            			"; document: " + docPage.context.currDocument.getEsfName() +  
            			"; user: " + (docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getFullDisplayName() : " (party not logged in)") +
            			"; IP: " + docPage.getIP()); 
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The specified document is not available to this party.");
            	return;
            }
            
            // Let's confirm that this party has access to the specified document and file field
            FieldTemplate ft = docPage.context.getFieldTemplate(docPage.context.currDocument, new EsfName(fileFieldName) );
            if ( ft == null )
            {
            	docPage.context.transaction.logGeneral("FileDownload ERROR - Field not found. Party: " + docPage.context.pkgVersionPartyTemplate.getEsfName() +
            			"tdid: " + tdid + "; tfid: " + tfidParam + "; transactionPartyId: " + docPage.context.transactionParty.getId() + 
            			"; document: " + docPage.context.currDocument.getEsfName() + "; file fieldName: " + fileFieldName + 
            			"; user: " + (docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getFullDisplayName() : " (party not logged in)") +
            			"; IP: " + docPage.getIP()); 
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The specified file field is not defined.");
            	return;
            }
            
            if ( ! ft.isTypeFile() )
            {
            	docPage.context.transaction.logGeneral("FileDownload ERROR - Field is not a file field. Field type: " + ft.getType() +
            			"; party: " + docPage.context.pkgVersionPartyTemplate.getEsfName() +
            			"tdid: " + tdid + "; tfid: " + tfidParam + "; transactionPartyId: " + docPage.context.transactionParty.getId() + 
            			"; document: " + docPage.context.currDocument.getEsfName() + "; file fieldName: " + fileFieldName + 
            			"; user: " + (docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getFullDisplayName() : " (party not logged in)") +
            			"; IP: " + docPage.getIP()); 
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The specified field is not a file field.");
            	return;
            }
            
            EsfValue blockViewDownload = docPage.context.transaction.getFieldValue(docPage.context.currDocument.getEsfName(), new EsfPathName(ft.getEsfName(),docPage.context.pkgVersionPartyTemplate.getId().toEsfName(),FieldTemplate.VIEW_DOWNLOAD_PROHIBITED_PATH_SUFFIX));
            if ( blockViewDownload != null )
            {
            	docPage.context.transaction.logGeneral("FileDownload ERROR - File field access is blocked for party: " + docPage.context.pkgVersionPartyTemplate.getEsfName() +
            			"tdid: " + tdid + "; tfid: " + tfidParam + "; transactionPartyId: " + docPage.context.transactionParty.getId() + 
            			"; document: " + docPage.context.currDocument.getEsfName() + "; file fieldName: " + fileFieldName + 
            			"; user: " + (docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getFullDisplayName() : " (party not logged in)") +
            			"; IP: " + docPage.getIP()); 
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The specified file is not accessible to you.");
            	return;
            }
            
            // Ensure the requested file matches our document and field
            EsfUUID tfid = new EsfUUID(tfidParam);
            TransactionFile downloadTranFile = null;
            for( TransactionFile tranFile : docPage.context.transaction.getAllTransactionFilesForDocumentField(tdid, ft.getEsfName()) )
            {
            	if ( tranFile.getId().equals(tfid) )
            	{
            		downloadTranFile = tranFile;
            		break;
            	}
            }

            if ( downloadTranFile == null )
            {       	
            	docPage.context.transaction.logGeneral("FileDownload ERROR - No file found with id: " + tfidParam +
            			"; party: " + docPage.context.pkgVersionPartyTemplate.getEsfName() +
            			"tdid: " + tdid + "; transactionPartyId: " + docPage.context.transactionParty.getId() + 
            			"; document: " + docPage.context.currDocument.getEsfName() + "; fieldName: " + ft.getEsfName() + 
            			"; user: " + (docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getFullDisplayName() : " (party not logged in)") +
            			"; IP: " + docPage.getIP()); 
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. No file with the specified id was found.");
            	return;
            }
            
            // Now, let's check if this party has recorded the file has been downloaded.  If not, we'll mark it as being read, but otherwise won't update it.
			// Record in our variable the date/time it was downloaded by this party
			EsfPathName fileAccessPathName = docPage.createPartyFileLastAccessTimestampFieldPathName(downloadTranFile.getId());
			if ( docPage.getFieldValue(fileAccessPathName) == null )
			{
				docPage.setFieldValue(fileAccessPathName, new EsfDateTime(), false);
				docPage.context.transaction.save();
			}
            
        	docPage.context.transaction.logGeneral("FileDownload - Party '" + docPage.context.pkgVersionPartyTemplate.getEsfName() +
        			"' downloaded file field name: " + downloadTranFile.getFieldName() + "; orig file name: " + downloadTranFile.getFileName() +
        			"; tran file id: " + tfidParam +
            		"; document: " + docPage.context.currDocument.getEsfName() + 
        			"; user: " + (docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getFullDisplayName() : " (party not logged in)") +
        			"; IP: " + docPage.getIP()); 

        	byte[] content = downloadTranFile.getFileDataFromDatabase();
            if ( downloadTranFile.isContentTypeBrowserSafe() || downloadTranFile.isContentTypePDF() ) 
            {
                response.setContentType(downloadTranFile.getFileMimeType());
            	String disposition = "inline; filename=\""+ downloadTranFile.getFileName() + "\"";
                response.setHeader("Content-Disposition", disposition );
            }
            else
            {
            	response.setContentType(Application.CONTENT_TYPE_BINARY);
            	String disposition = "attachment; filename=\""+ downloadTranFile.getFileName() + "\"";
                response.setHeader("Content-Disposition", disposition );
            }
            response.setContentLength(content.length);
            ServletOutputStream sos = response.getOutputStream();
            sos.write(content);
            sos.close();        	
        }
        finally
        {
        	if ( initialSession == null )
        	{
        		docPage.endSession();
        	}
        }

	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    
    public String getServletInfo() 
    {
        return "FileDownload " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}