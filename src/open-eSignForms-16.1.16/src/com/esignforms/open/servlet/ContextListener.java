// Copyright (C) 2009-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;

import com.esignforms.open.Application;
import com.esignforms.open.Version;
import com.esignforms.open.admin.SessionTrackerInfo;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DocumentInfo;
import com.esignforms.open.prog.DocumentInfo.INCLUDE;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentStyleInfo;
import com.esignforms.open.prog.DocumentStyleVersion;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FileVersion;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.prog.Library;

/**
 * This is a bootstrapping and shutdown handling class for the Open eSignForms web application.  It is called
 * before the system is activated, and after all servlets have been shutdown.
 * 
 * The following needs to be configured into the web.xml
 * <pre><code>
 * <listener>
 *     <listener-class>com.esignforms.open.servlet.ContextListener</listener-class>
 * </listener>
 * </code></pre>
 * 
 * @author Yozons, Inc.
 */
@WebListener
public class ContextListener
    implements ServletContextListener 
{
    private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ContextListener.class);
    private Application app = null;
    
	public ContextListener()
	{
	    _logger.debug("ContextListener() instantiated");
	}
    
	public void contextInitialized(ServletContextEvent sce)
	{
        app = Application.getInitialInstance(sce.getServletContext());
        
        _logger.debug("ContextListener() contextInitialized: " + app.getContextPath());
        _logger.info("Started " + Version.getReleaseString());
        _logger.info(Version.getCopyrightAndProprietary());
        // Yozons patent is provided to all users of Open eSignForms regardless of software license type.
        _logger.info("U.S. Patent No. 7,360,079 - Yozons grants non-exclusive, royalty-free use of this patent within Open eSignForms"); 
        // DocuSign patent licensed by Yozons (no charge if licensee doesn't charge transactionally)
        // No longer showing as of 2013 because it turns out that patent lost 19 of its 20 claims, with claim 20 having no value and thus will no longer be licensed by Yozons.
        //_logger.info("U.S. Patent No. 6,289,460 - Yozons grants non-exclusive, royalty-free use of this DocuSign patent if your deployment does not charge a transaction-based fee (fee per use)."); 
        _logger.info("DB Version: " + app.getConnectionPool().getDbVersionString());
        _logger.info("Java Version: " + System.getProperty("java.vm.name") + " " + System.getProperty("java.version"));
        try
        {
        	_logger.info("Host name: " + java.net.InetAddress.getLocalHost().getHostName());
        }
        catch( Exception e )
        {
        	_logger.info("Host name: UNKNOWN!");
        }

        
        // If we're doing session tracking, load any sessions activated before our context was initialized
        if ( app.getSessionTracker() != null )
        {
        	_logger.info("Creating sessions re-activated by the servlet container...");
            HttpSession[] activatedSessions = SessionTrackerInfo.getAndClearActivatedSessions();
            if ( activatedSessions != null )
            {
            	for( HttpSession sess : activatedSessions )
            	{
            		app.getSessionTracker().addSession(sess);
            	}
            }
        }
        
        // Re-generate all CSS/JSPs in its own thread so as not to delay startup
        Thread recompileThread = new Thread() {
        	public void run() {
            	_logger.info("Re-generating all CSS and JSPs in startup background thread...");
                
            	for( Library library : Library.Manager.getAll() )
                {
                	for( DocumentInfo docInfo : DocumentInfo.Manager.getAll(library, INCLUDE.BOTH_ENABLED_AND_DISABLED) )
                	{
                		for( DocumentVersion docVer : DocumentVersion.Manager.getAllByDocumentId(docInfo.getId()) )
                		{
                			docVer.regenerateCodeWithoutVersionSave();
                		}
                	}
                	
                	for( DocumentStyleInfo docStyleInfo : DocumentStyleInfo.Manager.getAll(library) )
                	{
                		for( DocumentStyleVersion docStyleVer : DocumentStyleVersion.Manager.getAllByDocumentStyleId(docStyleInfo.getId()) )
                		{
                			docStyleVer.generateCSS();
                		}
                	}
                }
            	
            	// Since we pretty much have loaded everything for this, we'll clear them on startup.
            	Document.Manager.clearCache();
            	DocumentVersion.Manager.clearCache();
            	com.esignforms.open.prog.File.Manager.clearCache();
            	FileVersion.Manager.clearCache();
            	com.esignforms.open.prog.Image.Manager.clearCache();
            	ImageVersion.Manager.clearCache();

            	_logger.info("Re-generation complete for all CSS and JSPs in startup background thread...");
        	}
        };
        recompileThread.start();
        
        if ( app.hasActivityLog() )
        {
            app.getActivityLog().logSystemStartStop("Web application context '" + app.getContextPath() +
            		"/' initialized for " + Version.getReleaseString() + 
                    //" for license to " + app.getLicensedTo() + 
                    //" for database up to " + app.getLicensedMb() +
                    //"MB using unique deployment id " + app.getDeployId()
                    " on deployment id " + app.getDeployId()
                    );
        }
        
    }
	
	public void contextDestroyed(ServletContextEvent sce)
	{
		if ( app != null )
		{
	        _logger.debug("ContextListener() contextDestroyed: " + app.getContextPath());

	        if ( app.hasActivityLog() )
	        {
	            app.getActivityLog().logSystemStartStop("Web application stopped " + Version.getReleaseString() + 
	                    //" for license to " + app.getLicensedTo() + 
	                    //" for database up to " + app.getLicensedMb() +
	                    //"MB using unique deployment id " + app.getDeployId()
	                    " for deployment id " + app.getDeployId()
	                    );
	        }

	        EsfUUID deployId = app.getDeployId();
	        app.destroy();
	        
	        _logger.info("Stopped " + Version.getReleaseString() + " using unique deployment id " + deployId);
	        
	        // Finally, release Log4J
	        LogManager.shutdown();
		}
	}
    
}