// Copyright (C) 2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.integration.wkhtmltopdf.HtmlToPdf;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentIdPartyId;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyAssignment;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.runtime.workflow.TransactionEngine;
import com.esignforms.open.user.User;
import com.esignforms.open.util.Base64;
import com.esignforms.open.util.XmlUtil;

/**
 * Administration API for an existing transaction.
 * @author Yozons, Inc.
 */
@WebServlet("/tranadmin/*") 
public class AdminTransaction extends HttpServlet 
{
	private static final long serialVersionUID = 4325411646384406645L;

	public static final String PARAM_ESFLOGINUSER      			= UpdateTransaction.PARAM_ESFLOGINUSER;
    public static final String PARAM_ESFLOGINPASSWORD  			= UpdateTransaction.PARAM_ESFLOGINPASSWORD;
    public static final String PARAM_ESFTRANSACTIONID  			= UpdateTransaction.PARAM_ESFTRANSACTIONID;
    public static final String PARAM_ESFDOCUMENT	  			= UpdateTransaction.PARAM_ESFDOCUMENT;
    public static final String PARAM_ESFPARTY	  				= UpdateTransaction.PARAM_ESFPARTY;
    public static final String PARAM_FORMAT		  				= "FORMAT";
    public static final String PARAM_INCLUDEDATA  				= "INCLUDEDATA";
    public static final String PARAM_TRANSFERTOEMAIL			= "TRANSFERTOEMAIL";
    
    static final String FORMAT_HTML = "HTML";
    static final String FORMAT_PDF = "PDF";
    static final String FORMAT_XMLDSIG = "XMLDSIG";
    static final String FORMAT_PDFMERGE = "PDFMERGE";
    
    static final String INCLUDEDATA_XMLDSIG = "XMLDSIG";
    static final String INCLUDEDATA_XML = "XML";
    static final String INCLUDEDATA_NONE = "NONE";


    
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
		
        HttpSession session = request.getSession();
        if ( session != null && ! session.isNew() )  // Force all sessions to be new for admin calls
        {
        	session.invalidate();
        	session = request.getSession();
        }
        
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init(session,request,response);
        docPage.setApiMode(true);
        
        if ( session == null )
        {
        	docPage.err("Could not get a session. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "System error: A session could not be created to support administration on your transaction.");
            return;
        }
        
        // All of the TranAdmin API requires 3 params: ESFLOGINUSER, ESFLOGINPASSWORD and ESFTID.
        
        try
        {
            // Ensure we have the login info needed to authenticate
            String esfLoginUserParam = docPage.getParam(PARAM_ESFLOGINUSER);
            String esfLoginPasswordParam = docPage.getParam(PARAM_ESFLOGINPASSWORD);
            if ( EsfString.areAnyBlank(esfLoginUserParam,esfLoginPasswordParam) )
            {
            	docPage.err("Missing user/password params. External users are not authorized to administer transactions; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You must provide authentication credentials to administer a transaction. Your IP address has been logged.");
            	return;        	
            }
            
            // The transaction id is required to identify the transaction to be administered.
            String transactionIdParam = docPage.getParam(PARAM_ESFTRANSACTIONID);
            if ( EsfString.isBlank(transactionIdParam) )
            {
            	docPage.err("The transaction id param is missing; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "The transaction id param is missing. Your IP address has been logged.");
            	return;        	
            }
            EsfUUID transactionId = new EsfUUID(transactionIdParam);
            if ( transactionId.isNull() )
            {
            	docPage.err("The requested transaction id: " + transactionIdParam + "; is not in a valid format; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "The transaction id (" + transactionIdParam + ") specified is not valid. Your IP address has been logged.");
            	return;
           	
            }

            User user = docPage.loginUser(esfLoginUserParam, esfLoginPasswordParam);
    		if ( user == null )
    		{
            	docPage.err("Login failure; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You must provide valid authentication credentials to administer a transaction. Your IP address has been logged.");
    			return;
    		}
            
    		Transaction transaction = Transaction.Manager.getById(transactionId);
    		if ( transaction == null )
    		{
    			user.logSecurity("Tried admin API for transaction id: " + transactionId + "; but no such transaction was found. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "No transaction was found with the specified id (" + transactionId + "). Your IP address has been logged.");
            	return;
    		}
    		
    		// Ensure the user has permission to admin this transaction
    		TransactionTemplate transactionTemplate = transaction.getTransactionTemplate();
    		if ( ! transactionTemplate.hasUseAdminApiPermission(user) )
    		{
    			user.logSecurity("Tried using the TranAdmin API for transaction id: " + transactionId + "; but is not authorized. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
    			transaction.logGeneral("User '" + user.getFullDisplayName() + "' tried to use the TranAdmin API, but it not authorized.");
            	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You are not authorized to administer transaction id (" + transactionId + "). Your IP address has been logged.");
    			return;
    		}
    		
    		// Get the Admin API request name
            String requestName = request.getPathInfo();
            if ( docPage.isBlank(requestName) )
            {
    			user.logSecurity("Tried using the TranAdmin API for transaction id: " + transactionId + "; but no request-name specified. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "No Tran Admin API request-name was specified so we do not know what you'd like to do.");
            	return;
            }
            // Remove leading /
            requestName = requestName.substring(1);
            // If it ends in a /, remove it too
            if ( requestName.endsWith("/") )
            	requestName = requestName.substring(0, requestName.length()-1);

            // Handle the request if we know what it is.
            if ( "GetStatus".equalsIgnoreCase(requestName) )
            {
            	doGetStatus(docPage,user,transaction);
            }
            else if ( "GetDocumentSnapshot".equalsIgnoreCase(requestName) )
            {
            	doGetDocumentSnapshot(docPage,user,transaction);
            }
            else if ( "RenotifyParty".equalsIgnoreCase(requestName) )
            {
            	doRenotifyParty(docPage,user,transaction);
            }
            else
            {
    			user.logSecurity("Tried using the TranAdmin API for transaction id: " + transactionId + "; but unexpected request-name (" + requestName + ") specified. IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. Unknown Tran Admin API request-name (" + requestName + ") was specified.");
            }
        }
        finally
        {
        	if ( docPage.isUserLoggedIn() )
        		docPage.logoffUser();
        	docPage.endSession();
        }
	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    
    LinkedList<EsfName> getDocumentNames(DocumentPageBean docPage, Transaction transaction) throws IOException
    {
        String[] documentNamesParam = docPage.getMultiParam(PARAM_ESFDOCUMENT);
        LinkedList<EsfName> documentNames = new LinkedList<EsfName>();
        
        // If no params given, or one is given but the value is blank, we assume all documents
        if ( documentNamesParam == null || documentNamesParam.length == 0 ||
        	(documentNamesParam.length == 1 && EsfString.isBlank(documentNamesParam[0]))
           )
        {
        	for( TransactionDocument tranDoc : transaction.getAllTransactionDocuments() )
        	{
        		documentNames.add(tranDoc.getDocument().getEsfName());
        	}
        }
        else // verify valid document names are provided
        {
        	for( String documentName : documentNamesParam )
        	{
        		EsfName name = new EsfName(documentName);
        		if ( ! name.isValid() )
        		{
                	docPage.err("Invalid document name (" + documentName + ") in param; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
                	docPage.response.sendError(HttpServletResponse.SC_NOT_FOUND, "Invalid ESFDOCUMENT param (" + documentName + ").");
                	return null;
        		}
        		if ( ! documentNames.contains(name) )
        			documentNames.add(name);
        	}
        }

        return documentNames;
    }
    
    LinkedList<TransactionDocument> getTransactionDocuments(DocumentPageBean docPage, Transaction transaction, LinkedList<EsfName> documentNames)
    {
    	LinkedList<TransactionDocument> tranDocs = new LinkedList<TransactionDocument>();
    	
    	for( EsfName docName : documentNames)
    	{
    		TransactionDocument tranDoc = transaction.getTransactionDocument(docName);
    		if ( tranDoc == null )
    			docPage.errors.addError("Document name not found: " + docName);
    		else
    			tranDocs.add(tranDoc);
    	}
    	return tranDocs;
    }
    
    TransactionDocument getTransactionDocumentForTransactionPartyDocument(TransactionPartyDocument tpd, LinkedList<TransactionDocument> tranDocs)
    {
    	for ( TransactionDocument tranDoc : tranDocs )
    	{
    		if ( tranDoc.getId().equals(tpd.getTransactionDocumentId()) )
    			return tranDoc;
    	}
    	return null;
    }
    
    boolean isPartyParamSpecified(DocumentPageBean docPage)
    {
    	String[] partyNamesParam = docPage.getMultiParam(PARAM_ESFPARTY);
        // If no params given, or one is given but the value is blank, we assume no party is specified
        if ( partyNamesParam == null || partyNamesParam.length == 0 ||
        	(partyNamesParam.length == 1 && EsfString.isBlank(partyNamesParam[0]))
           )
        	return false;
        return true;
    }
    
    LinkedList<EsfName> getPartyNames(DocumentPageBean docPage, Transaction transaction) throws IOException
    {
        String[] partyNamesParam = docPage.getMultiParam(PARAM_ESFPARTY);
        LinkedList<EsfName> partyNames = new LinkedList<EsfName>();
        
        // If no params given, or one is given but the value is blank, we assume all parties
        if ( partyNamesParam == null || partyNamesParam.length == 0 ||
        	(partyNamesParam.length == 1 && EsfString.isBlank(partyNamesParam[0]))
           )
        {
        	for( TransactionParty tranParty : transaction.getAllTransactionParties() )
        	{
        		if ( ! tranParty.isReports() )
        			partyNames.add(tranParty.getPackageVersionPartyTemplate().getEsfName());
        	}
        }
        else // verify valid party names are provided
        {
        	for( String partyName : partyNamesParam )
        	{
        		EsfName name = new EsfName(partyName);
        		if ( ! name.isValid() )
        		{
                	docPage.err("Invalid party name (" + partyName + ") in param; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
                	docPage.response.sendError(HttpServletResponse.SC_NOT_FOUND, "Invalid ESFPARTY param (" + partyName + ").");
                	return null;
        		}
        		if ( ! partyNames.contains(name) )
        			partyNames.add(name);
        	}
        }

        return partyNames;
    }
    
    LinkedList<TransactionParty> getTransactionParties(DocumentPageBean docPage, Transaction transaction, LinkedList<EsfName> partyNames)
    {
    	LinkedList<TransactionParty> tranParties = new LinkedList<TransactionParty>();
    	
    	for( EsfName partyName : partyNames)
    	{
    		TransactionParty tranParty = transaction.getTransactionParty(partyName);
    		if ( tranParty == null )
    			docPage.errors.addError("Party name not found: " + partyName);
    		else
    			tranParties.add(tranParty);
    	}
    	return tranParties;
    }
    
    String getFormat(DocumentPageBean docPage) throws IOException
    {
    	String format = docPage.getParam(PARAM_FORMAT);
    	if ( EsfString.isBlank(format) )
    		format = FORMAT_XMLDSIG;

    	if ( FORMAT_HTML.equalsIgnoreCase(format) || FORMAT_PDF.equalsIgnoreCase(format) || FORMAT_XMLDSIG.equalsIgnoreCase(format) || FORMAT_PDFMERGE.equalsIgnoreCase(format) )
    		return format.toUpperCase();
    	
    	docPage.err("Invalid FORMAT (" + format + ") in param; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
    	docPage.response.sendError(HttpServletResponse.SC_NOT_FOUND, "Invalid FORMAT param (" + format + ").");
    	return null;
    }
    
    String getIncludeData(DocumentPageBean docPage) throws IOException
    {
    	String includeData = docPage.getParam(PARAM_INCLUDEDATA);
    	if ( EsfString.isBlank(includeData) )
    		includeData = INCLUDEDATA_NONE;

    	if ( INCLUDEDATA_XMLDSIG.equalsIgnoreCase(includeData) || INCLUDEDATA_XML.equalsIgnoreCase(includeData) || INCLUDEDATA_NONE.equalsIgnoreCase(includeData) )
    		return includeData.toUpperCase();
    	
    	docPage.err("Invalid INCLUDEDATA (" + includeData + ") in param; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
    	docPage.response.sendError(HttpServletResponse.SC_NOT_FOUND, "Invalid INCLUDEDATA param (" + includeData + ").");
    	return null;
    }

    
    boolean doTransferParty(DocumentPageBean docPage, User user, Transaction transaction, EsfName partyName, TransactionParty tranParty, TransactionPartyAssignment assign, String transferToEmail)
    {		
    	ConnectionPool    pool = docPage.app.getConnectionPool();
    	Connection        con  = pool.getConnection();
        try 
        {
        	boolean needsPartyActivation = assign.isActivated() || assign.isRetrieved(); // we'll only activate the new party if the original was
        	
        	TransactionContext pseudoContext = new TransactionContext(user,"",partyName,transaction.getId(),
        															  docPage.getIPOnly(), docPage.getIP(), docPage.getUserAgent());
        	TransactionEngine engine = new TransactionEngine(pseudoContext);
        	engine.queuePartyTransferredEvent(partyName, transferToEmail);
        	
        	if ( needsPartyActivation )
        		engine.queuePartyActivatedEvent(partyName);
        	
        	Errors errors = new Errors();
        	engine.doWork(con, errors);
        	pseudoContext.transaction.save(con,user);
			con.commit();
			return true;
        } 
        catch( EsfException e ) 
        {
        	pool.rollbackIgnoreException(con,null);
        	docPage.except(e,"AdminTransaction.doTransferParty(" + partyName + ") - Transfer party failed");
        } 
        catch(SQLException e) 
        {
        	docPage.sqlerr(e,"AdminTransaction.doTransferParty(" + partyName + ") - Transfer party failed");
            pool.rollbackIgnoreException(con,e);
        } 
        finally 
        {
        	docPage.app.cleanupPool(pool,con,null);
        }

        return false;
    }
    
    boolean doRenotifyParty(DocumentPageBean docPage, EsfName partyName, TransactionParty tranParty)
    {		
    	ConnectionPool    pool = docPage.app.getConnectionPool();
    	Connection        con  = pool.getConnection();
        try 
        {
        	tranParty.renotifyParty(con);
			con.commit();
			return true;
        } 
        catch(SQLException e) 
        {
        	docPage.sqlerr(e,"AdminTransaction.doRenotifyParty(" + partyName + ") - Renotify party failed");
            pool.rollbackIgnoreException(con,e);
        } 
        finally 
        {
        	docPage.app.cleanupPool(pool,con,null);
        }

        return false;
    }
    
    /**
     * Handles the GetStatus request
     * @param docPage
     * @param user
     * @param transaction
     * @throws IOException
     */
    void doGetStatus(DocumentPageBean docPage, User user, Transaction transaction) throws IOException
    {
    	LinkedList<EsfName> documentNames = getDocumentNames(docPage,transaction);
    	if ( documentNames == null ) // error returned instead
    		return;
    	
    	LinkedList<EsfName> partyNames = getPartyNames(docPage,transaction);
    	if ( partyNames == null ) // error returned instead
    		return;
    	
    	LinkedList<TransactionDocument> tranDocs = getTransactionDocuments(docPage, transaction, documentNames);
    	if ( docPage.errors.hasError() )
    	{
    		sendAPIError(docPage, transaction.getId(), "GetStatus");
    		return;
    	}

    	LinkedList<TransactionParty> tranParties = getTransactionParties(docPage, transaction, partyNames);
    	if ( docPage.errors.hasError() )
    	{
    		sendAPIError(docPage, transaction.getId(), "GetStatus");
    		return;
    	}
    	
    	user.logConfigChange("TranAdmin API GetStatus done for transaction id: " + transaction.getId() + "; from IP: " + docPage.getIP());
    	
    	docPage.response.setContentType(Application.CONTENT_TYPE_XML);
    	
    	StringBuilder buf = new StringBuilder(4096);
        buf.append("<GetStatusResponse xmlns=\"").append(XmlUtil.getXmlNamespaceTranAdminApi2015()).append("\">\n");
        
        buf.append(" <Transaction>\n");
        buf.append("  <id>").append(transaction.getId().toXml()).append("</id>\n");
        buf.append("  <status>").append(XmlUtil.toEscapedXml(transaction.getStatusLabel())).append("</status>\n");
        buf.append("  <statusCode>").append(XmlUtil.toEscapedXml(transaction.getStatus())).append("</statusCode>\n");
        buf.append("  <statusText>").append(XmlUtil.toEscapedXml(transaction.getStatusText())).append("</statusText>\n");
        buf.append("  <lastUpdatedTimestamp>").append(XmlUtil.toEscapedXml(transaction.getLastUpdatedTimestamp().toXml())).append("</lastUpdatedTimestamp>\n");
        buf.append("  <cancelTimestamp>").append(transaction.hasCancelTimestamp() ? XmlUtil.toEscapedXml(transaction.getCancelTimestamp().toXml()) : "").append("</cancelTimestamp>\n");
        buf.append("  <expireTimestamp>").append(transaction.hasExpireTimestamp() ? XmlUtil.toEscapedXml(transaction.getExpireTimestamp().toXml()) : "").append("</expireTimestamp>\n");
        
        for( TransactionParty tranParty : tranParties )
        {
            buf.append("  <Party>\n");
            buf.append("   <id>").append(tranParty.getId().toXml()).append("</id>\n");
            buf.append("   <esfname>").append(tranParty.getPackageVersionPartyTemplate().getEsfName().toXml()).append("</esfname>\n");
            TransactionPartyAssignment assign = tranParty.getCurrentAssignment();
            if ( assign == null )
            {
                buf.append("   <status>").append(XmlUtil.toEscapedXml(tranParty.getStatusLabel())).append("</status>\n");
                buf.append("   <statusCode>").append(XmlUtil.toEscapedXml(tranParty.getStatus())).append("</statusCode>\n");
            }
            else
            {
            	String pickupUrl =  docPage.getExternalUrlContextPath() + "/P/" + assign.getPickupCode();
                buf.append("   <status>").append(XmlUtil.toEscapedXml(assign.getStatusLabel())).append("</status>\n");
                buf.append("   <statusCode>").append(XmlUtil.toEscapedXml(assign.getStatus())).append("</statusCode>\n");
                buf.append("   <emailAddress>").append(XmlUtil.toEscapedXml(assign.getEmailAddress())).append("</emailAddress>\n");
                buf.append("   <pickupCode>").append(XmlUtil.toEscapedXml(assign.getPickupCode())).append("</pickupCode>\n");
                buf.append("   <pickupUrl>").append(XmlUtil.toEscapedXml(pickupUrl)).append("</pickupUrl>\n");
                buf.append("   <lastAccessedTimestamp>").append(assign.hasLastAccessedTimestamp() ? XmlUtil.toEscapedXml(assign.getLastAccessedTimestamp().toXml()) : "").append("</lastAccessedTimestamp>\n");
            }
            
            for( TransactionPartyDocument tpd : tranParty.getTransactionPartyDocuments() )
            {
            	TransactionDocument tranDoc = getTransactionDocumentForTransactionPartyDocument(tpd,tranDocs);
            	if ( tranDoc != null ) // if null, then we're not doing this document
            	{ 
                    buf.append("   <PartyDocument>\n");
                    buf.append("    <esfname>").append(tranDoc.getDocument().getEsfName().toXml()).append("</esfname>\n");
                    buf.append("    <documentNumber>").append(tpd.getDocumentNumber()).append("</documentNumber>\n");
                    buf.append("    <status>").append(XmlUtil.toEscapedXml(tpd.getStatusLabel())).append("</status>\n");
                    buf.append("    <statusCode>").append(XmlUtil.toEscapedXml(tpd.getStatus())).append("</statusCode>\n");
                    buf.append("    <lastUpdatedTimestamp>").append(XmlUtil.toEscapedXml(tpd.getLastUpdatedTimestamp().toXml())).append("</lastUpdatedTimestamp>\n");
                    buf.append("    <esignIpHostAddr>").append(XmlUtil.toEscapedXml(tpd.getEsignIpHostAddr())).append("</esignIpHostAddr>\n");
                    buf.append("    <esignTimestamp>").append(tpd.isEsigned() ? tpd.getEsignTimestamp().toXml() : "").append("</esignTimestamp>\n");
                    buf.append("   </PartyDocument>\n");
            	}
            }
            buf.append("  </Party>\n");
        }

        buf.append(" </Transaction>\n");
        buf.append("</GetStatusResponse>\n");
        
        docPage.response.getWriter().print(buf.toString());
    }
    
    /**
     * Handles the GetDocumentSnapshot request
     * @param docPage
     * @param user
     * @param transaction
     * @throws IOException
     */
    void doGetDocumentSnapshot(DocumentPageBean docPage, User user, Transaction transaction) throws IOException
    {
    	LinkedList<EsfName> documentNames = getDocumentNames(docPage,transaction);
    	if ( documentNames == null ) // error returned instead
    		return;
    	
    	LinkedList<EsfName> partyNames = getPartyNames(docPage,transaction);
    	if ( partyNames == null ) // error returned instead
    		return;
    	
    	boolean doLatestParty = ! isPartyParamSpecified(docPage);
    	
    	if ( ! doLatestParty && partyNames.size() != 1 )
    	{
        	docPage.err("Multiple (" + partyNames.size() + ") party name params not supported; IP: " + docPage.getIP() + "; browser: " + docPage.getUserAgent());
        	docPage.response.sendError(HttpServletResponse.SC_NOT_FOUND, "Invalid multiple ESFPARTY params.");
        	return;
    	}
    	
    	String format = getFormat(docPage);
    	if ( format == null ) // error returned instead
    		return;
    	
    	String includeData = getIncludeData(docPage);
    	if ( includeData == null ) // error returned instead
    		return;
    	
    	getTransactionDocuments(docPage, transaction, documentNames); // we don't care about actual trandocs, just ensure document names have a corresponding trandoc
    	if ( docPage.errors.hasError() )
    	{
    		sendAPIError(docPage, transaction.getId(), "GetDocumentSnapshot");
    		return;
    	}

    	TransactionParty tranParty = null;
    	if ( ! doLatestParty )
    	{
        	LinkedList<TransactionParty> tranParties = getTransactionParties(docPage, transaction, partyNames);
        	if ( docPage.errors.hasError() )
        	{
        		sendAPIError(docPage, transaction.getId(), "GetDocumentSnapshot");
        		return;
        	}
        	tranParty = tranParties.get(0);
    	}
    	
    	List<DocumentIdPartyId> allDocAndParty = new LinkedList<DocumentIdPartyId>();
    	for( EsfName docName : documentNames )
    	{
    		DocumentIdPartyId dp;
    		TransactionDocument tranDoc = transaction.getTransactionDocument(docName);
    		Document doc = tranDoc.getDocument();
    		if ( doLatestParty )
    			dp = new DocumentIdPartyId(doc.getId(),null);
    		else
    			dp = new DocumentIdPartyId(doc.getId(),doc.getEsfName(),tranParty.getPackageVersionPartyTemplateId(),tranParty.getPackageVersionPartyTemplate().getEsfName());
    		allDocAndParty.add(dp);
    	}
    	
		List<TransactionPartyDocument> tranPartyDocumentList = transaction.getTransactionPartyDocumentsWithNonSkippedSnapshot(allDocAndParty);
    	if ( tranPartyDocumentList.size() < 1 )
    	{
        	docPage.err("AdminTransaction.doGetDocumentSnapshot() - No tran party documents were found for tranId: " + transaction.getId() + 
        			"; party: " + (doLatestParty ? "latest" : partyNames.get(0)) + "; IP: " + docPage.getIP() + "; user: " + docPage.getLoggedInUser().getFullDisplayName());
        	if ( doLatestParty )
        		docPage.errors.addError("No latest completed document(s) match your request.");
        	else
        		docPage.errors.addError("Party " + partyNames.get(0) + " has no matching completed document(s).");
    		sendAPIError(docPage, transaction.getId(), "GetDocumentSnapshot");
        	return;
    	}
    	
    	int estimatedDocSize = 0;
    	for ( TransactionPartyDocument tpd : tranPartyDocumentList )
    	{
    		estimatedDocSize += tpd.getSnapshotXml().length();
    	}
    	
    	user.logConfigChange("TranAdmin API GetDocumentSnapshot done for transaction id: " + transaction.getId() + "; from IP: " + docPage.getIP());

    	docPage.response.setContentType(Application.CONTENT_TYPE_XML);
    	
    	StringBuilder buf = new StringBuilder(4096 + estimatedDocSize);
        buf.append("<GetDocumentSnapshotResponse xmlns=\"").append(XmlUtil.getXmlNamespaceTranAdminApi2015()).append("\">\n");
        
        buf.append(" <Transaction>\n");
        buf.append("  <id>").append(transaction.getId().toXml()).append("</id>\n");
        buf.append("  <status>").append(XmlUtil.toEscapedXml(transaction.getStatusLabel())).append("</status>\n");
        buf.append("  <statusCode>").append(XmlUtil.toEscapedXml(transaction.getStatus())).append("</statusCode>\n");
        buf.append("  <statusText>").append(XmlUtil.toEscapedXml(transaction.getStatusText())).append("</statusText>\n");
        buf.append("  <lastUpdatedTimestamp>").append(XmlUtil.toEscapedXml(transaction.getLastUpdatedTimestamp().toXml())).append("</lastUpdatedTimestamp>\n");
        
        String[] htmlFilesForPdfMerge = FORMAT_PDFMERGE.equals(format) ? new String[tranPartyDocumentList.size()] : null;
        int htmlFilesForPdfMergeIndex = 0;
        
        for( TransactionPartyDocument tpd : tranPartyDocumentList )
        {
        	TransactionDocument tranDoc = transaction.getTransactionDocument(tpd.getTransactionDocumentId());
            buf.append("  <PartyDocument>\n");
            buf.append("   <esfname>").append(tranDoc.getDocument().getEsfName().toXml()).append("</esfname>\n");
            buf.append("   <documentNumber>").append(tpd.getDocumentNumber()).append("</documentNumber>\n");
            buf.append("   <status>").append(XmlUtil.toEscapedXml(tpd.getStatusLabel())).append("</status>\n");
            buf.append("   <statusCode>").append(XmlUtil.toEscapedXml(tpd.getStatus())).append("</statusCode>\n");
            buf.append("   <lastUpdatedTimestamp>").append(XmlUtil.toEscapedXml(tpd.getLastUpdatedTimestamp().toXml())).append("</lastUpdatedTimestamp>\n");
            buf.append("   <esignIpHostAddr>").append(XmlUtil.toEscapedXml(tpd.getEsignIpHostAddr())).append("</esignIpHostAddr>\n");
            buf.append("   <esignTimestamp>").append(tpd.isEsigned() ? tpd.getEsignTimestamp().toXml() : "").append("</esignTimestamp>\n");

            if ( FORMAT_PDFMERGE.equals(format) ) // for merge, we just save them and add later outside of the PartyDocument element
            {
            	htmlFilesForPdfMerge[htmlFilesForPdfMergeIndex++] = tpd.getSnapshotDocument();
            }
            else
            {
                buf.append("   <snapshotDocument>");
            	if ( FORMAT_HTML.equals(format) )
            	{
            		String html = tpd.getSnapshotDocument();
            		buf.append(XmlUtil.toEscapedXml(html));
            	}
            	else if ( FORMAT_PDF.equals(format) )
            	{
            		String html = tpd.getSnapshotDocument();
            		if ( EsfString.isNonBlank(html) )
            		{
	            		HtmlToPdf htmlToPdf = new HtmlToPdf();
	            		byte[] pdf = htmlToPdf.generateSignedPdf(html);
	            		String base64 = Base64.encodeToString(pdf);
	            		buf.append(XmlUtil.toEscapedXml(base64));
            		}
            	}
            	else if ( FORMAT_XMLDSIG.equals(format) )
            	{
            		String xml = tpd.getDigitallySignedSnapshotDocument();
            		buf.append(xml);
            	}
                buf.append("</snapshotDocument>\n");
            }
            
            if ( ! INCLUDEDATA_NONE.equals(includeData) )
            {
                buf.append("   <snapshotData>");
                if ( INCLUDEDATA_XML.equals(includeData) )
                {
            		String xml = tpd.getSnapshotData();
            		buf.append(xml);
                }
                else if ( INCLUDEDATA_XMLDSIG.equals(includeData) )
                {
            		String xml = tpd.getDigitallySignedSnapshotData();
            		buf.append(xml);
                }
                buf.append("</snapshotData>\n");
            }
            buf.append("  </PartyDocument>\n");
        }
        
        if ( htmlFilesForPdfMerge != null )
        {
            buf.append("  <PDFMerge>");
    		HtmlToPdf htmlToPdf = new HtmlToPdf();
    		byte[] pdf = htmlToPdf.generateSignedPdf(htmlFilesForPdfMerge);
    		String base64 = Base64.encodeToString(pdf);
    		buf.append(XmlUtil.toEscapedXml(base64));
            buf.append("</PDFMerge>\n");
        }

        buf.append(" </Transaction>\n");
        buf.append("</GetDocumentSnapshotResponse>\n");
        
        docPage.response.getWriter().print(buf.toString());
    	
    }

    /**
     * Handles the RenotifyParty request
     * @param docPage
     * @param user
     * @param transaction
     * @throws IOException
     */
    void doRenotifyParty(DocumentPageBean docPage, User user, Transaction transaction) throws IOException
    {
    	if ( transaction.isCanceled() )
    	{
			docPage.errors.addError("Transaction is canceled.");
    		sendAPIError(docPage, transaction.getId(), "RenotifyParty");
			return;
    	}
    	if ( transaction.isSuspended() )
    	{
			docPage.errors.addError("Transaction is suspended.");
    		sendAPIError(docPage, transaction.getId(), "RenotifyParty");
			return;
    	}
    	
    	LinkedList<EsfName> partyNames = new LinkedList<EsfName>();

        String partyParam = docPage.getParam(PARAM_ESFPARTY);
        if ( EsfString.isBlank(partyParam) )
        {
        	for( EsfName partyName : transaction.getPackageVersion().getPackageVersionPartyNameList() ) 
        	{
        		TransactionParty tranParty = transaction.getTransactionParty(partyName);
        		if ( tranParty != null && ! tranParty.isReports() && tranParty.isActive() )
        			partyNames.add(partyName);
        	}
        }
        else
        {
            if ( partyParam.contains("@") )
        	{
        		if ( ! EsfEmailAddress.isValidEmail(partyParam) )
        		{
        			docPage.errors.addError("Invalid email address (" + partyParam + ") specified for the ESFPARTY param.");
            		sendAPIError(docPage, transaction.getId(), "RenotifyParty");
        			return;
        		}
        		for( TransactionParty tranParty : transaction.getAllTransactionParties() )
        		{
        			if ( tranParty.getCurrentAssignment().getEmailAddress().equalsIgnoreCase(partyParam) )
        			{
        				partyNames.add(tranParty.getPackageVersionPartyTemplate().getEsfName());
        			}
        		}
        	}
            else
            {
            	EsfName partyName = new EsfName(partyParam);
            	if ( ! partyName.isValid() )
            	{
        			docPage.errors.addError("Invalid party name (" + partyParam + ") specified for the ESFPARTY param.");
            		sendAPIError(docPage, transaction.getId(), "RenotifyParty");
        			return;
            	}
            	partyNames.add(partyName);
            }
        }
        
        if ( partyNames.size() == 0 )
        {
			docPage.errors.addError("No valid parties found with party name (" + partyParam + ") specified for the ESFPARTY param.");
    		sendAPIError(docPage, transaction.getId(), "RenotifyParty");
			return;
        }
        
        
    	LinkedList<TransactionParty> tranParties = getTransactionParties(docPage, transaction, partyNames);
    	if ( docPage.errors.hasError() )
    	{
    		sendAPIError(docPage, transaction.getId(), "RenotifyParty");
    		return;
    	}
    	
    	String transferToEmail = docPage.getParam(PARAM_TRANSFERTOEMAIL);
    	if ( EsfString.isNonBlank(transferToEmail) )
    	{
    		if ( ! EsfEmailAddress.isValidEmail(transferToEmail) )
    		{
    			docPage.errors.addError("Invalid email address (" + transferToEmail + ") specified for the TRANSFERTOEMAIL param.");
        		sendAPIError(docPage, transaction.getId(), "RenotifyParty");
    			return;
    		}
    	}
    	
    	user.logConfigChange("TranAdmin API RenotifyParty done for transaction id: " + transaction.getId() + "; from IP: " + docPage.getIP());
    	
    	docPage.response.setContentType(Application.CONTENT_TYPE_XML);
    	
    	StringBuilder buf = new StringBuilder(4096);
        buf.append("<RenotifyPartyResponse xmlns=\"").append(XmlUtil.getXmlNamespaceTranAdminApi2015()).append("\">\n");
        
        buf.append(" <Transaction>\n");
        buf.append("  <id>").append(transaction.getId().toXml()).append("</id>\n");
        buf.append("  <status>").append(XmlUtil.toEscapedXml(transaction.getStatusLabel())).append("</status>\n");
        buf.append("  <statusCode>").append(XmlUtil.toEscapedXml(transaction.getStatus())).append("</statusCode>\n");
        buf.append("  <statusText>").append(XmlUtil.toEscapedXml(transaction.getStatusText())).append("</statusText>\n");
        buf.append("  <lastUpdatedTimestamp>").append(XmlUtil.toEscapedXml(transaction.getLastUpdatedTimestamp().toXml())).append("</lastUpdatedTimestamp>\n");
        
        for( TransactionParty tranParty : tranParties )
        {
        	EsfName partyName = tranParty.getPackageVersionPartyTemplate().getEsfName();
            buf.append("  <Party>\n");
            buf.append("   <id>").append(tranParty.getId().toXml()).append("</id>\n");
            buf.append("   <esfname>").append(partyName.toXml()).append("</esfname>\n");
            TransactionPartyAssignment assign = tranParty.getCurrentAssignment();
            if ( assign == null )
            {
                buf.append("   <status>").append(XmlUtil.toEscapedXml(tranParty.getStatusLabel())).append("</status>\n");
                buf.append("   <statusCode>").append(XmlUtil.toEscapedXml(tranParty.getStatus())).append("</statusCode>\n");
            }
            else
            {
            	boolean transferDone = false;
            	if ( EsfString.isNonBlank(transferToEmail) ) // transfer will also send out notification
            	{
            		if ( assign.isActivated() || assign.isRetrieved() )
            		{
            			if ( ! transferToEmail.equalsIgnoreCase(assign.getEmailAddress()) ) 
            			{
            				transferDone = doTransferParty(docPage,user,transaction,partyName,tranParty,assign,transferToEmail);
            				if ( transferDone )
                                buf.append("   <transferPartyStatus>").append(XmlUtil.toEscapedXml("Completed: " + transferToEmail)).append("</transferPartyStatus>\n");
            				else
                                buf.append("   <transferPartyStatus>").append(XmlUtil.toEscapedXml("Failed: " + transferToEmail)).append("</transferPartyStatus>\n");
            				transaction = Transaction.Manager.getById(transaction.getId());
            				tranParty = transaction.getTransactionParty(partyName);
            				assign = tranParty.getCurrentAssignment();
            			}
            			else
            			{
                            buf.append("   <transferPartyStatus>").append(XmlUtil.toEscapedXml("Skipped: email address unchanged")).append("</transferPartyStatus>\n");
            			}
            		}
            		else
            		{
                        buf.append("   <transferPartyStatus>").append(XmlUtil.toEscapedXml("Skipped: status not Activated or Retrieved")).append("</transferPartyStatus>\n");
            		}
            	}
            	
            	if ( transferDone )
            	{
                    buf.append("   <renotifyPartyStatus>").append(XmlUtil.toEscapedXml("Completed: transferred")).append("</renotifyPartyStatus>\n");
            	}
            	else
            	{
            		boolean renotifyDone = false;
            		
			    	// If we have an email address and we're either in progress or have already been completed, allow an email renotification to take place
			    	if ( ! tranParty.hasTodoGroupId() && ! assign.hasEmailAddress() ) 
			    	{
                        buf.append("   <renotifyPartyStatus>").append(XmlUtil.toEscapedXml("Skipped: no To Do group or email address assigned")).append("</renotifyPartyStatus>\n");
			    	}
			    	else if ( ! assign.isActivated() && ! assign.isRetrieved()  && ! assign.isCompleted() ) 
			    	{
                        buf.append("   <renotifyPartyStatus>").append(XmlUtil.toEscapedXml("Skipped: status not Activated, Retrieved or Completed")).append("</renotifyPartyStatus>\n");
			    	}
			    	else
			    	{
			    		renotifyDone = doRenotifyParty(docPage,partyName,tranParty);
        				if ( renotifyDone )
                            buf.append("   <renotifyPartyStatus>").append(XmlUtil.toEscapedXml("Completed")).append("</renotifyPartyStatus>\n");
        				else
                            buf.append("   <renotifyPartyStatus>").append(XmlUtil.toEscapedXml("Failed")).append("</renotifyPartyStatus>\n");
        				transaction = Transaction.Manager.getById(transaction.getId());
        				tranParty = transaction.getTransactionParty(partyName);
        				assign = tranParty.getCurrentAssignment();
			    	}
            	}
            	           	
            	String pickupUrl =  docPage.getExternalUrlContextPath() + "/P/" + assign.getPickupCode();
                buf.append("   <status>").append(XmlUtil.toEscapedXml(assign.getStatusLabel())).append("</status>\n");
                buf.append("   <statusCode>").append(XmlUtil.toEscapedXml(assign.getStatus())).append("</statusCode>\n");
                buf.append("   <emailAddress>").append(XmlUtil.toEscapedXml(assign.getEmailAddress())).append("</emailAddress>\n");
                buf.append("   <pickupCode>").append(XmlUtil.toEscapedXml(assign.getPickupCode())).append("</pickupCode>\n");
                buf.append("   <pickupUrl>").append(XmlUtil.toEscapedXml(pickupUrl)).append("</pickupUrl>\n");
                buf.append("   <lastAccessedTimestamp>").append(assign.hasLastAccessedTimestamp() ? XmlUtil.toEscapedXml(assign.getLastAccessedTimestamp().toXml()) : "").append("</lastAccessedTimestamp>\n");
            }
            
            buf.append("  </Party>\n");
        }

        buf.append(" </Transaction>\n");
        buf.append("</RenotifyPartyResponse>\n");
        
        docPage.response.getWriter().print(buf.toString());
    }

    
    void sendAPIError(DocumentPageBean docPage, EsfUUID tranId, String requestName) throws IOException
    {
    	docPage.response.setContentType(Application.CONTENT_TYPE_XML);
    	
    	StringBuilder buf = new StringBuilder(4096);
        buf.append("<Error xmlns=\"").append(XmlUtil.getXmlNamespaceTranAdminApi2015()).append("\">\n");
        
        buf.append("  <transactionId>").append(tranId.toXml()).append("</transactionId>\n");
        buf.append("  <requestName>").append(requestName).append("</requestName>\n");
        buf.append("  <reason>").append(XmlUtil.toEscapedXml(docPage.errors.toString())).append("</reason>\n");
        buf.append("</Error>\n");
        
        docPage.response.getWriter().print(buf.toString());
    }
    
    
    public String getServletInfo() 
    {
        return "AdminTransaction " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}