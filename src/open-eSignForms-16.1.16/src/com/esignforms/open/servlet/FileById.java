// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.FileVersion;

/**
 * Handles the retrieval of files by id, typically used internally to retrieve files outside of a transaction/document context.
 * @author Yozons, Inc.
 */
@WebServlet("/filesById/*") 
public class FileById extends HttpServlet 
{
	private static final long serialVersionUID = -4544347217399797544L;


	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
        HttpSession session = request.getSession();
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init(session,request,response);
        
        if ( session == null )
        {
        	docPage.err("Could not get a session.");
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Your login session could not be created. IP: " + docPage.getIP());
            return;
        }
        
        // All such access to files requires logged in users.
        if ( ! docPage.isUserLoggedIn() )
        {
        	docPage.err("User is not logged in. IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Your login session has terminated. Please login to retrieve an image by id.");
            return;
        }
           
        String fileVersionId = request.getPathInfo();
        if ( EsfString.isBlank(fileVersionId) )
        {
        	docPage.err("There is no file version id in the path info of the current link. IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No file version id was specified.");
        	return;
        }
        fileVersionId = fileVersionId.substring(1,fileVersionId.indexOf("/", 1));
        
        FileVersion fileVersion = FileVersion.Manager.getById( new EsfUUID(fileVersionId) );
        if ( fileVersion == null )
        {
        	docPage.err("No file version with the id in the path info of the current link was found: " + fileVersionId + "; IP: " + docPage.getIP());
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. No file version with the id (" + fileVersionId + ") specified was found.");
        	return;
        }
        
        byte[] content = fileVersion.getFileData();
        response.setContentType(fileVersion.getFileMimeType());
        if ( ! docPage.app.isContentTypeBrowserSafe(fileVersion.getFileMimeType()) ) 
        {
            String disposition = "attachment;filename=\""+ fileVersion.getFileFileName() + "\"";
            response.setHeader("Content-Disposition", disposition );
        }
        response.setContentLength(content.length);
        ServletOutputStream sos = response.getOutputStream();
        sos.write(content);
        sos.close();
	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    
    public String getServletInfo() 
    {
        return "FileById " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}