// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.ImageVersion;

/**
 * Handles the retrieval of Thumbnail images by id.
 * @author Yozons, Inc.
 */
@WebServlet("/thumbnails/*") 
public class ThumbnailImage extends HttpServlet 
{
	private static final long serialVersionUID = 7750516181616475727L;


	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
        HttpSession session = request.getSession();
        if ( session == null )
        {
        	response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE, "Your login session to retrieve the thumbnail image could not be created.");
            return;
        }
        
        String imageId = request.getPathInfo();
        if ( EsfString.isBlank(imageId) )
        {
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No thumbnail image id was specified.");
        	return;
        }
        imageId = imageId.substring(1);
        
        Image image = Image.Manager.getById( new EsfUUID(imageId) );
        if ( image == null )
        {
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. No thumbnail image with the specified id was found.");
        	return;
        }
        
        ImageVersion imageVersion = image.getTestImageVersion(); // If we have a test version, send that back, else the production one...
        
        byte[] content = imageVersion.getThumbnailData();
        response.setCharacterEncoding(EsfString.CHARSET_UTF_8);
        response.setContentType(imageVersion.getImageMimeType());
        response.setContentLength(content.length);
        ServletOutputStream sos = response.getOutputStream();
        sos.write(content);
        sos.close();
	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    
    public String getServletInfo() 
    {
        return "ThumbnailImage " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}