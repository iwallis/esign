// Copyright (C) 2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.FileVersion;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
 * Handles the retrieval of file confirm click fields by id embedded in a document.  This is generally used when clicking the
 * link in review mode or in a document capture link (such as from reports and parties clicking on the files from the downloaded copy).
 * @author Yozons, Inc.
 */
@WebServlet("/fileconfirmclick/*") 
public class FileConfirmClick extends HttpServlet 
{
	private static final long serialVersionUID = -4510389915612477445L;


	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
        HttpSession initialSession = request.getSession(false);
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init( initialSession == null ? request.getSession() : initialSession, request, response);
        
        try
        {
            String fileName = request.getPathInfo();
            if ( EsfString.isBlank(fileName) )
            {
            	docPage.err("FileConfirmClick.doGet() - No filename was specified on the request; IP: " + docPage.getIP());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No file name was specified.");
            	return;
            }
            fileName = fileName.substring(1,fileName.indexOf("/", 1));
            
            FileVersion fileVersion = null;        
        	String fvidParam = docPage.getParam("fvid"); // We only return this particular file version as it was the one used in the document at the time it was retrieved
            if ( EsfString.isBlank(fvidParam) )
            {
            	docPage.err("FileConfirmClick.doGet() - No 'fvid' param was specified on the request; IP: " + docPage.getIP());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No file version id was specified.");
            	return;
            }
        	
        	String pickupCodeParam = docPage.getParam("puc"); // This will identify the transaction and party whose document was used to download the file
            if ( ! docPage.app.getRandomKey().isCorrectPickupCodeLength(pickupCodeParam) )
            {
            	docPage.err("FileConfirmClick.doGet() - No 'puc' param was specified on the request; IP: " + docPage.getIP());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The pickup code specified to identify your request is missing or not valid.");
            	return;
            }
        	
        	String tdidParam = docPage.getParam("tdid"); // This will identify the transaction document
            if ( EsfString.isBlank(tdidParam) )
            {
            	docPage.err("FileConfirmClick.doGet() - No 'tdid' param was specified on the request; IP: " + docPage.getIP());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No transaction document id was specified.");
            	return;
            }
        	
        	String fieldNameParam = docPage.getParam("fieldName"); // This will identify the field in the document
            if ( EsfString.isBlank(fieldNameParam) )
            {
            	docPage.err("FileConfirmClick.doGet() - No 'fieldName' param was specified on the request; IP: " + docPage.getIP());
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No field name was specified.");
            	return;
            }
            
            docPage.isUserLoggedIn(); // Sets our logged in user name
            if ( ! docPage.pickupTransaction(pickupCodeParam,false) )
            	return;
                        
            docPage.context = new TransactionContext(docPage.context); // Create our own copy so we don't update the document/page status elsewhere
            
            // Let's ensure this party can access the requested document
            EsfUUID tdid = new EsfUUID(tdidParam);
            docPage.context.goToDocumentForTransactionDocumentAndParty(tdid,docPage.context.transactionParty.getId());
            if ( docPage.context.isPackageDocument() )
            {
            	docPage.context.transaction.logGeneral("FileConfirmClick ERROR - Document not found for party. Party: " + docPage.context.pkgVersionPartyTemplate.getEsfName() +
            			"tdid: " + tdid + "; transactionPartyId: " + docPage.context.transactionParty.getId() + 
            			"; document: " + docPage.context.currDocument.getEsfName() + "; fieldName: " + fieldNameParam + 
            			"; user: " + (docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getFullDisplayName() : " (party not logged in)") +
            			"; IP: " + docPage.getIP()); 
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The specified document is not available to this party.");
            	return;
            }
            
            // Let's confirm that this party has access to the specified document and field
            FieldTemplate ft = docPage.context.getFieldTemplate(docPage.context.currDocument, new EsfName(fieldNameParam) );
            if ( ft == null )
            {
            	docPage.context.transaction.logGeneral("FileConfirmClick ERROR - Field not found. Party: " + docPage.context.pkgVersionPartyTemplate.getEsfName() +
            			"tdid: " + tdid + "; transactionPartyId: " + docPage.context.transactionParty.getId() + 
            			"; document: " + docPage.context.currDocument.getEsfName() + "; fieldName: " + fieldNameParam + 
            			"; user: " + (docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getFullDisplayName() : " (party not logged in)") +
            			"; IP: " + docPage.getIP()); 
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The specified field is not defined.");
            	return;
            }
            
            if ( ! ft.isTypeFileConfirmClick() )
            {
            	docPage.context.transaction.logGeneral("FileConfirmClick ERROR - Field is not a file confirm link. Field type: " + ft.getType() +
            			"; party: " + docPage.context.pkgVersionPartyTemplate.getEsfName() +
            			"tdid: " + tdid + "; transactionPartyId: " + docPage.context.transactionParty.getId() + 
            			"; document: " + docPage.context.currDocument.getEsfName() + "; fieldName: " + fieldNameParam + 
            			"; user: " + (docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getFullDisplayName() : " (party not logged in)") +
            			"; IP: " + docPage.getIP()); 
            	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The specified field is not a file confirm link field.");
            	return;
            }
            
    		fileVersion = com.esignforms.open.prog.FileVersion.Manager.getById(new EsfUUID(fvidParam));
            if ( fileVersion == null )
            {       	
            	docPage.context.transaction.logGeneral("FileConfirmClick ERROR - No file version found with id: " + fvidParam +
            			"; party: " + docPage.context.pkgVersionPartyTemplate.getEsfName() +
            			"tdid: " + tdid + "; transactionPartyId: " + docPage.context.transactionParty.getId() + 
            			"; document: " + docPage.context.currDocument.getEsfName() + "; fieldName: " + fieldNameParam + 
            			"; user: " + (docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getFullDisplayName() : " (party not logged in)") +
            			"; IP: " + docPage.getIP()); 
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. No file with the specified version id was found.");
            	return;
            }
            
            EsfValue fieldValue = docPage.context.transaction.getFieldValue(docPage.context.currDocument.getEsfName(), ft.getEsfName());
    		// If the value is set to an EsfName, we use that name for the file. Otherwise we'll default to how it was originally configured.
    		EsfName targetFileName = (fieldValue instanceof EsfName) ? (EsfName)fieldValue : new EsfName(ft.getInputFormatSpec());
    		// If the names don't match, the fvid file does not match the expected file to retrieve
    		if ( ! targetFileName.toPlainString().equals(fileName) )
    		{
            	docPage.context.transaction.logGeneral("FileConfirmClick ERROR - Requested fileName '" + fileName + "' does not match expected name '" + targetFileName +
            			". Party '" + docPage.context.pkgVersionPartyTemplate.getEsfName() +
            			"' blocked access to file name: " + fileVersion.getFile().getEsfName() + "; orig file name: " + fileVersion.getFileFileName() +
            			"; file version id: " + fvidParam +
                		"; document: " + docPage.context.currDocument.getEsfName() + "; fieldName: " + fieldNameParam + 
            			"; user: " + (docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getFullDisplayName() : " (party not logged in)") +
            			"; IP: " + docPage.getIP()); 
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. The specified file name does not match the expected file name.");
            	return;
    		}

        	docPage.context.transaction.logGeneral("FileConfirmClick - Party '" + docPage.context.pkgVersionPartyTemplate.getEsfName() +
        			"' downloaded file name: " + fileVersion.getFile().getEsfName() + "; orig file name: " + fileVersion.getFileFileName() +
        			"; file version id: " + fvidParam +
            		"; document: " + docPage.context.currDocument.getEsfName() + "; fieldName: " + fieldNameParam + 
        			"; user: " + (docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getFullDisplayName() : " (party not logged in)") +
        			"; IP: " + docPage.getIP()); 

        	byte[] content = fileVersion.getFileData();
            response.setContentType(fileVersion.getFileMimeType());
            if ( ! docPage.app.isContentTypeBrowserSafe(fileVersion.getFileMimeType()) ) 
            {
                String disposition = "attachment;filename=\""+ fileVersion.getFileFileName() + "\"";
                response.setHeader("Content-Disposition", disposition );
            }
            response.setContentLength(content.length);
            ServletOutputStream sos = response.getOutputStream();
            sos.write(content);
            sos.close();        	
        }
        finally
        {
        	if ( initialSession == null )
        	{
        		docPage.endSession();
        	}
        }

	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    
    public String getServletInfo() 
    {
        return "FileConfirmClick " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}