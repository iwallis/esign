// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.prog.Library;

/**
 * Handles the retrieval of regular images by name embedded in a document.
 * @author Yozons, Inc.
 */
@WebServlet("/images/*") 
public class Image extends HttpServlet 
{
	private static final long serialVersionUID = -3847549559993477187L;


	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init(request.getSession(false),request,response);

        String imageName = request.getPathInfo();
        if ( EsfString.isBlank(imageName) )
        {
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No image name was specified.");
        	return;
        }
        imageName = imageName.substring(1,imageName.indexOf("/", 1));
        
        ImageVersion imageVersion = null;        
    	String ivid = docPage.getParam("ivid"); // the imageout tag gives us the correct image version to use based on the transaction context, so when present, use it.
    	if ( EsfString.isNonBlank(ivid) )
    	{
    		imageVersion = com.esignforms.open.prog.ImageVersion.Manager.getById(new EsfUUID(ivid));
            if ( imageVersion == null )
            {       	
            	docPage.err("Image.doGet() - Failed to find image version with ivid: " + ivid + "; IP: " + docPage.getIP());
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. No image with the specified version id was found.");
            	return;
            }
    	}
    	else
    	{
        	// Okay, no version id, so let's at least see if we have the image id
            String iid = docPage.getParam("iid");
        	if ( EsfString.isNonBlank(iid) )
        	{
        		com.esignforms.open.prog.Image image = com.esignforms.open.prog.Image.Manager.getById(new EsfUUID(iid));
        		if ( image == null )
        		{
                	docPage.err("Image.doGet() - Failed to find image with iid: " + iid + "; IP: " + docPage.getIP());
                	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. No image with the specified id was found.");
                	return;
        		}
                imageVersion = image.getTestImageVersion(); // If we have a test version, send that back, else the production one...
        	}
        	else
        	{
           		com.esignforms.open.prog.Image image = com.esignforms.open.prog.Image.Manager.getByName(Library.Manager.getTemplate().getId(), new EsfName(imageName));
        		if ( image == null )
        		{
                	docPage.err("Image.doGet() - Failed to find image with name: " + imageName + "; IP: " + docPage.getIP());
                	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. No image with the specified name was found.");
                	return;
        		}
                imageVersion = image.getTestImageVersion(); // If we have a test version, send that back, else the production one...
        	}
    	}

        byte[] content = imageVersion.getImageData();
        response.setContentType(imageVersion.getImageMimeType());
        response.setContentLength(content.length);
        ServletOutputStream sos = response.getOutputStream();
        sos.write(content);
        sos.close();
	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    
    public String getServletInfo() 
    {
        return "Image " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}