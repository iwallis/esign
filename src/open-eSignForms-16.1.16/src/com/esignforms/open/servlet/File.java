// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.FileVersion;
import com.esignforms.open.prog.Library;

/**
 * Handles the retrieval of files by name embedded in a document.
 * @author Yozons, Inc.
 */
@WebServlet("/files/*") 
public class File extends HttpServlet 
{
	private static final long serialVersionUID = -6928120272425692254L;

	public void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, java.io.IOException
    {
        DocumentPageBean docPage = new DocumentPageBean();
        docPage.init(request.getSession(false),request,response);

        String fileName = request.getPathInfo();
        if ( EsfString.isBlank(fileName) )
        {
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. No file name was specified.");
        	return;
        }
        fileName = fileName.substring(1,fileName.indexOf("/", 1));
        
        FileVersion fileVersion = null;        
    	String fvid = docPage.getParam("fvid"); // the filelink tag gives us the correct file version to use based on the transaction context, so when present, use it.
    	if ( EsfString.isNonBlank(fvid) )
    	{
    		fileVersion = com.esignforms.open.prog.FileVersion.Manager.getById(new EsfUUID(fvid));
            if ( fileVersion == null )
            {       	
            	docPage.err("File.doGet() - Failed to find file version with fvid: " + fvid + "; IP: " + docPage.getIP());
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. No file with the specified version id was found.");
            	return;
            }
    	}
    	else
    	{
        	// Okay, no version id, so let's at least see if we have the file id
            String fid = docPage.getParam("fid");
        	if ( EsfString.isNonBlank(fid) )
        	{
        		com.esignforms.open.prog.File file = com.esignforms.open.prog.File.Manager.getById(new EsfUUID(fid));
        		if ( file == null )
        		{
                	docPage.err("File.doGet() - Failed to find file with fid: " + fid + "; IP: " + docPage.getIP());
                	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. No file with the specified id was found.");
                	return;
        		}
                fileVersion = file.getTestFileVersion(); // If we have a test version, send that back, else the production one...
        	}
        	else
        	{
           		com.esignforms.open.prog.File file = com.esignforms.open.prog.File.Manager.getByName(Library.Manager.getTemplate().getId(), new EsfName(fileName));
        		if ( file == null )
        		{
                	docPage.err("File.doGet() - Failed to find file with name: " + fileName + "; IP: " + docPage.getIP());
                	response.sendError(HttpServletResponse.SC_NOT_FOUND, "Please check your link. No file with the specified name was found.");
                	return;
        		}
                fileVersion = file.getTestFileVersion(); // If we have a test version, send that back, else the production one...
        	}
    	}

        byte[] content = fileVersion.getFileData();
        response.setContentType(fileVersion.getFileMimeType());
        if ( ! docPage.app.isContentTypeBrowserSafe(fileVersion.getFileMimeType()) ) 
        {
            String disposition = "attachment;filename=\""+ fileVersion.getFileFileName() + "\"";
            response.setHeader("Content-Disposition", disposition );
        }
        response.setContentLength(content.length);
        ServletOutputStream sos = response.getOutputStream();
        sos.write(content);
        sos.close();
	}
    
    public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, java.io.IOException
    {
        doGet(req,res);
    }
    
    
    public String getServletInfo() 
    {
        return "File " + com.esignforms.open.Version.getReleaseString();
    }
    

    /**
     * Called whenever the servlet is installed into the web server.
     */
    public void init( ServletConfig config ) 
        throws ServletException
    {
        super.init(config);
    }
    
    
    /**
     * Called whenever the servlet is removed from the web server.
     */
    public void destroy()
    {
        super.destroy();
    }
}