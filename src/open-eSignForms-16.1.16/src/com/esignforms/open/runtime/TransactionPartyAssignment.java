// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.User;

/**
* TransactionPartyAssignment holds information about a specific assignment of someone to a given TransactionParty. This is done because
* a single transaction party may be reassigned, so only one is ever the current assignment.
* 
* @author Yozons, Inc.
*/
public class TransactionPartyAssignment
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<TransactionPartyAssignment>, java.io.Serializable
{
	private static final long serialVersionUID = 888086317176104114L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TransactionPartyAssignment.class);
    
    public static final String STATUS_UNDEFINED = "U"; // not yet activated
    public static final String STATUS_ACTIVATED = "A"; // activated, email notified and/or queued for To Do
    public static final String STATUS_RETRIEVED = "R"; // active and retrieved
    public static final String STATUS_REJECTED = "X"; // retrieved and then rejected (an end state)
    public static final String STATUS_COMPLETED = "C"; // retrieved and then completed
    public static final String STATUS_TRANSFERRED = "T"; // transferred to another TransactionPartyAssignment
    public static final String STATUS_SKIPPED = "S"; // Skipped

    protected final EsfUUID transactionPartyId; // The transaction party this assignment is for
    protected final String pickupCode;
    protected EsfUUID userId; // If set, the user who plays this party role
    protected String emailAddress;
    protected String status;
    protected final EsfDateTime createdTimestamp;
    protected EsfDateTime lastAccessedTimestamp;
    
    
    /**
     * This creates a TransactionPartyAssignment object from data retrieved from the DB.
     */
    protected TransactionPartyAssignment(EsfUUID transactionPartyId, String pickupCode, EsfUUID userId, String emailAddress, String status, EsfDateTime createdTimestamp, EsfDateTime lastAccessedTimestamp)
    {
        this.transactionPartyId = transactionPartyId;
    	this.pickupCode = pickupCode;
    	this.userId = userId;
        this.emailAddress = EsfString.isBlank(emailAddress) ? null : emailAddress;
        this.status = status;
        this.createdTimestamp = createdTimestamp;
        this.lastAccessedTimestamp = lastAccessedTimestamp;
    }
    
    protected TransactionPartyAssignment(EsfUUID transactionPartyId, String pickupCode, String emailAddress)
    {
        this.transactionPartyId = transactionPartyId;
    	this.pickupCode = pickupCode;
    	this.userId = null;
        this.emailAddress = EsfString.isBlank(emailAddress) ? null : emailAddress.toLowerCase();
        this.status = STATUS_UNDEFINED;
        this.createdTimestamp = new EsfDateTime();
        this.lastAccessedTimestamp = null;
    }

   
    public final EsfUUID getTransactionPartyId()
    {
        return transactionPartyId;
    }
    
    public final String getPickupCode()
    {
        return pickupCode;
    }

    public EsfUUID getUserId()
    {
        return userId;
    }
    public User getUser()
    {
    	return User.Manager.getById(userId);
    }
    public boolean hasUserId()
    {
    	return userId != null && ! userId.isNull();
    }
    public void setUserId(EsfUUID v)
    {
    	userId = v;
    	objectChanged();
    }
    public void unlockUser()
    {
    	if ( hasUserId() )
    	{
    		setUserId(null);
    		if ( isRetrieved() )
    			setStatusActivated();
    	}
    }
    
    public String getEmailAddress()
    {
        return emailAddress;
    }
    public boolean hasEmailAddress()
    {
    	return EsfString.isNonBlank(emailAddress);
    }
    public void setEmailAddress(String v)
    {
    	emailAddress = EsfString.isBlank(v) ? null : v.toLowerCase().trim();
    	objectChanged();
    }
    
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String v)
    {
    	status = v;
    	objectChanged();
    }
    public boolean isUndefined()
    {
    	return STATUS_UNDEFINED.equals(status);
    }
    public boolean isActivated()
    {
    	return STATUS_ACTIVATED.equals(status);
    }
    public void setStatusActivated()
    {
    	setStatus(STATUS_ACTIVATED);
    }
    public boolean isRetrieved()
    {
    	return STATUS_RETRIEVED.equals(status);
    }
    public void setStatusRetrieved()
    {
    	setStatus(STATUS_RETRIEVED);
    }
    public boolean isRejected()
    {
    	return STATUS_REJECTED.equals(status);
    }
    public boolean isCompleted()
    {
    	return STATUS_COMPLETED.equals(status);
    }
    public void setStatusCompleted()
    {
    	setStatus(STATUS_COMPLETED);
    }
    public boolean isTransferred()
    {
    	return STATUS_TRANSFERRED.equals(status);
    }
    public void setStatusTransferred()
    {
    	setStatus(STATUS_TRANSFERRED);
    }
    public boolean isSkipped()
    {
    	return STATUS_SKIPPED.equals(status);
    }
    public void setStatusSkipped()
    {
    	setStatus(STATUS_SKIPPED);
    }
    public boolean isEndState()
    {
    	return isRejected() || isCompleted() || isSkipped();
    }
    
    public String getStatusLabel()
    {
    	if ( isCompleted() )
    		return Application.getInstance().getServerMessages().getString("transaction.party.assignment.status.completed");
    	if ( isActivated() )
    		return Application.getInstance().getServerMessages().getString("transaction.party.assignment.status.activated");
    	if ( isRetrieved() )
    		return Application.getInstance().getServerMessages().getString("transaction.party.assignment.status.retrieved");
    	if ( isRejected() )
    		return Application.getInstance().getServerMessages().getString("transaction.party.assignment.status.rejected");
    	if ( isTransferred() )
    		return Application.getInstance().getServerMessages().getString("transaction.party.assignment.status.transferred");
    	if ( isSkipped() )
    		return Application.getInstance().getServerMessages().getString("transaction.party.assignment.status.skipped");
    	if ( isUndefined() )
    		return Application.getInstance().getServerMessages().getString("transaction.party.assignment.status.undefined");
    	return "??"+status+"??";
    	
    }

    public final EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }

    public final EsfDateTime getLastAccessedTimestamp()
    {
    	return lastAccessedTimestamp;
    }
    public final boolean hasLastAccessedTimestamp()
    {
    	return lastAccessedTimestamp != null;
    }
    public void setLastAccessedTimestampToNow()
    {
    	lastAccessedTimestamp = new EsfDateTime();
    	objectChanged();
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof TransactionPartyAssignment )
        {
        	TransactionPartyAssignment other = (TransactionPartyAssignment)o;
            return getPickupCode().equals(other.getPickupCode());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getPickupCode().hashCode();
    }
    
    @Override
    public int compareTo(TransactionPartyAssignment o)
    {
    	return getPickupCode().compareTo(o.getPickupCode());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con)
        throws SQLException
    {
    	_logger.debug("save(con,user) on transactionPartyId: " + transactionPartyId + "; emailAddress: " + emailAddress + "; status: " + status + "; insert: " + doInsert());
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( doInsert() )
            {                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_transaction_party_assignment (transaction_party_id,user_id,pickup_code,email_address,status,created_timestamp,last_accessed_timestamp) VALUES (?,?,?,?,?,?,?)");
                stmt.set(transactionPartyId);
                stmt.set(userId);
                stmt.set(pickupCode);
                stmt.set(emailAddress);
                stmt.set(status);
                stmt.set(createdTimestamp);
                stmt.set(lastAccessedTimestamp);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for transactionPartyId: " + transactionPartyId + "; emailAddress: " + emailAddress + "; status: " + status);
                    return false;
                }

                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                return true;
            }
            
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_transaction_party_assignment SET user_id=?,email_address=?,status=?,last_accessed_timestamp=? WHERE pickup_code=?"
                						   );
                stmt.set(userId);
                stmt.set(emailAddress);
                stmt.set(status);
                stmt.set(lastAccessedTimestamp);
                stmt.set(pickupCode);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for transactionPartyId: " + transactionPartyId + "; emailAddress: " + emailAddress + "; status: " + status); 
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on transactionPartyId: " + transactionPartyId + "; emailAddress: " + emailAddress + "; status: " + status + "; insert: " + doInsert());
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }

    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    
    
    synchronized boolean delete(final Connection con, final Errors errors)
        throws SQLException
    {
    	_logger.debug("delete(con) on transactionPartyId: " + transactionPartyId + "; emailAddress: " + emailAddress + "; status: " + status);
        
        clearLastSQLException();
    
        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of transaction party that was pending an INSERT transactionPartyId: " + transactionPartyId + "; emailAddress: " + emailAddress + "; status: " + status);
            objectDeleted();
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_transaction_party_assignment WHERE pickup_code=?");
            stmt.set(pickupCode);
            stmt.executeUpdate();
            
            objectDeleted();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on transactionPartyId: " + transactionPartyId + "; emailAddress: " + emailAddress + "; status: " + status);
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete()
    {
        Errors nullErrors = null;
        return delete(nullErrors);
    }
        
   	public static class Manager
   	{
   		public static List<TransactionPartyAssignment> getAllByTransactionPartyId(Connection con, EsfUUID transactionPartyId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<TransactionPartyAssignment> list = new LinkedList<TransactionPartyAssignment>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT user_id, pickup_code, email_address, status, created_timestamp, last_accessed_timestamp " +
   	        			               					"FROM esf_transaction_party_assignment WHERE transaction_party_id=? ORDER BY created_timestamp DESC" );
   	        	stmt.set(transactionPartyId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID userId = rs.getEsfUUID();
	            	String pickupCode = rs.getString();
	            	String emailAddress = rs.getString();
	            	String status = rs.getString();
	            	EsfDateTime createdTimestamp = rs.getEsfDateTime();
	            	EsfDateTime lastAccessedTimestamp = rs.getEsfDateTime();
	            	
	            	TransactionPartyAssignment tranParty = new TransactionPartyAssignment(transactionPartyId,pickupCode,userId,emailAddress,status,createdTimestamp,lastAccessedTimestamp);
	            	tranParty.setLoadedFromDb();
	            	list.add(tranParty);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   		public static List<TransactionPartyAssignment> getAllByTransactionPartyId(EsfUUID transactionPartyId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<TransactionPartyAssignment> list = getAllByTransactionPartyId(con,transactionPartyId);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   		public static TransactionPartyAssignment getByPickupCode(Connection con, String pickupCode)
			throws SQLException
		{
	        EsfPreparedStatement stmt = null;
	        
	        try
	        {
	        	stmt = new EsfPreparedStatement( con, "SELECT transaction_party_id, user_id, email_address, status, created_timestamp, last_accessed_timestamp " +
	        			               					"FROM esf_transaction_party_assignment WHERE pickup_code = ?" );
	        	stmt.set(pickupCode);
	        	
	        	EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID transactionPartyId = rs.getEsfUUID();
	            	EsfUUID userId = rs.getEsfUUID();
	            	String emailAddress = rs.getString();
	            	String status = rs.getString();
	            	EsfDateTime createdTimestamp = rs.getEsfDateTime();
	            	EsfDateTime lastAccessedTimestamp = rs.getEsfDateTime();
	            	
	            	TransactionPartyAssignment tranParty = new TransactionPartyAssignment(transactionPartyId,pickupCode,userId,emailAddress,status,createdTimestamp,lastAccessedTimestamp);
	            	tranParty.setLoadedFromDb();
	            	return tranParty;
	            }
	        }
	        finally
	        {
	            cleanupStatement(stmt);
	        }

            return null;
		}
   		public static TransactionPartyAssignment getByPickupCode(String pickupCode)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	TransactionPartyAssignment tranParty = getByPickupCode(con,pickupCode);
   	        	if ( tranParty != null ) 
   	        	{
		            con.commit();
	   	        	return tranParty;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		

   		public static int deleteAllByTransactionPartyId(Connection con, EsfUUID transactionPartyId)
			throws SQLException
		{
	        EsfPreparedStatement stmt = null;
	        int numDeleted = 0;
	        
	        try
	        {
	        	stmt = new EsfPreparedStatement( con, "DELETE FROM esf_transaction_party_assignment WHERE transaction_party_id = ?" );
	        	stmt.set(transactionPartyId);
	        	numDeleted = stmt.executeUpdate();
	        }
	        finally
	        {
	            cleanupStatement(stmt);
	        }

	        return numDeleted; 
		}
		
		
   		public static TransactionPartyAssignment createNew(TransactionParty tranParty, String emailAddress)
   		{
   			String pickupCode = Application.getInstance().getRandomKey().getPickupCodeString();
   			TransactionPartyAssignment assignment = new TransactionPartyAssignment(tranParty.getId(), pickupCode, emailAddress);
   			return assignment;
   		}
   	    
   	} // Manager
   	
}