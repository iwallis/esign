// Copyright (C) 2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;


/**
* TransactionTimer holds the definition of a timer that will expire in the future as it relates
* to a given transaction.
* 
* @author Yozons, Inc.
*/
public class TransactionTimer
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<TransactionTimer>, java.io.Serializable
{
	private static final long serialVersionUID = -2482904324157715766L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TransactionTimer.class);
    
	public static String DEFAULT_NAME = "Generic";
	
    protected final EsfUUID transactionId;
    protected final String timerName;
    protected EsfDateTime expireTimestamp;
   
    /**
     * This creates a TransactionTimer object from data retrieved from the DB.
     */
    protected TransactionTimer(EsfUUID transactionId, String timerName, EsfDateTime expireTimestamp)
    {
        this.transactionId = transactionId;
        this.timerName = EsfString.isBlank(timerName) ? DEFAULT_NAME : timerName.trim();
        this.expireTimestamp = expireTimestamp == null ? new EsfDateTime() : expireTimestamp;
    }
    
    public final EsfUUID getTransactionId()
    {
        return transactionId;
    }
    
	public String getTimerName()
    {
    	return timerName;
    }
    
	public EsfDateTime getExpireTimestamp()
	{
		return expireTimestamp;
	}
	public void setExpireTimestamp(EsfDateTime v)
	{
		if ( v != null && ! v.isNull() )
		{
			expireTimestamp = v;
			objectChanged();
		}
	}
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof TransactionTimer )
        {
        	TransactionTimer other = (TransactionTimer)o;
            return getTransactionId().equals(other.getTransactionId()) &&
            	getTimerName().equalsIgnoreCase(other.getTimerName());
        }
        return false;
    }
    
    @Override
    public int hashCode() {
    	return getTransactionId().hashCode() + getTimerName().toLowerCase().hashCode();
    }
    
    @Override
    public int compareTo(TransactionTimer o)
    {
    	int diff = getTransactionId().compareTo(o.getTransactionId());
    	if ( diff != 0 )
    		return diff;
    	return getTimerName().compareToIgnoreCase(o.getTimerName());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con)
        throws SQLException
    {
    	_logger.debug("save(con) on transactionId: " + transactionId + "; timerName: " + timerName + "; expireTimestamp: " + expireTimestamp + "; insert: " + doInsert());
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
            if ( doInsert() )
            {
				stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_transaction_timer (transaction_id,timer_name,expire_timestamp) VALUES (?,?,?)");
                stmt.set(transactionId);
                stmt.set(timerName);
                stmt.set(expireTimestamp);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for transactionId: " + transactionId + "; timerName: " + timerName + "; expireTimestamp: " + expireTimestamp);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                
                return true;
            }
            
			stmt = new EsfPreparedStatement(con, 
                	"UPDATE esf_transaction_timer SET expire_timestamp=? WHERE transaction_id=? AND timer_name=?");
			stmt.set(expireTimestamp);
			stmt.set(transactionId);
			stmt.set(timerName);
                
			int num = stmt.executeUpdate();
            if ( num != 1 )
            {
            	_logger.warn("save(con) - Update failed for transactionId: " + transactionId + "; timerName: " + timerName + "; expireTimestamp: " + expireTimestamp);
            }
                
			// Now we mark this object as if it were loaded fresh from the database
			setLoadedFromDb();

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on transactionId: " + transactionId + "; timerName: " + timerName + "; expireTimestamp: " + expireTimestamp);
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }

    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    
    
    public synchronized boolean delete(final Connection con)
	    throws SQLException
	{
		_logger.debug("delete(con) on transactionId: " + transactionId + "; timerName: " + timerName + "; expireTimestamp: " + expireTimestamp);
	    
	    clearLastSQLException();
	
	    if ( doInsert() )
	    {
	    	_logger.warn("delete(con) - Ignored delete of timer that was pending an INSERT transactionId: " + transactionId + "; timerName: " + timerName + "; expireTimestamp: " + expireTimestamp);
	        objectDeleted();
	        return true;
	    }
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_transaction_timer WHERE transaction_id=? AND lower(timer_name)=?");
	        stmt.set(transactionId);
	        stmt.set(timerName.toLowerCase());
	        stmt.executeUpdate();
	        
	        objectDeleted();
	
	        return true;
	    }
	    catch(SQLException e)
	    {
	    	_logger.sqlerr(e,"delete(con) on transactionId: " + transactionId + "; timerName: " + timerName + "; expireTimestamp: " + expireTimestamp);
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}

	public boolean delete()
	{
	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        boolean result = delete(con);
	        con.commit();
	        return result;
	    }
	    catch(SQLException e) 
	    {
	        pool.rollbackIgnoreException(con,e);
	        return false;
	    }
	    finally
	    {
	    	Application.getInstance().cleanupPool(pool,con,null);
	    }
	}

   	public static class Manager
   	{
   		public static List<TransactionTimer> getAllForTransaction(Connection con, EsfUUID transactionId) throws SQLException
   		{
   			EsfPreparedStatement stmt = null;

   			LinkedList<TransactionTimer> list = new LinkedList<TransactionTimer>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT timer_name,expire_timestamp FROM esf_transaction_timer WHERE transaction_id=?"
   	        									);
   	        	stmt.set(transactionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	String timerName = rs.getString();
		            EsfDateTime expireTimestamp = rs.getEsfDateTime();
		            
		            TransactionTimer tranTimer = new TransactionTimer(transactionId,timerName,expireTimestamp);
		            tranTimer.setLoadedFromDb();

		            list.add(tranTimer);
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAllForTransaction() - transactionId: " + transactionId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list;
  		}
   		
   		public static List<TransactionTimer> getAllForTransaction(EsfUUID transactionId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<TransactionTimer> list = getAllForTransaction(con,transactionId);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   		public static List<TransactionTimer> getExpired(Connection con, EsfDateTime expirationDateTime) throws SQLException
   		{
   			EsfPreparedStatement stmt = null;

   			LinkedList<TransactionTimer> list = new LinkedList<TransactionTimer>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT transaction_id,timer_name,expire_timestamp FROM esf_transaction_timer WHERE expire_timestamp<=?"
   	        									);
   	        	stmt.set(expirationDateTime);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID transactionId = rs.getEsfUUID();
	            	String timerName = rs.getString();
		            EsfDateTime expireTimestamp = rs.getEsfDateTime();
		            
		            TransactionTimer tranTimer = new TransactionTimer(transactionId,timerName,expireTimestamp);
		            tranTimer.setLoadedFromDb();

		            list.add(tranTimer);
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getExpired() - expirationDateTime: " + expirationDateTime);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list;
  		}
   		
   		public static List<TransactionTimer> getExpired(EsfDateTime expirationDateTime)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<TransactionTimer> list = getExpired(con,expirationDateTime);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   		public static TransactionTimer getByNameForTransaction(Connection con, EsfUUID transactionId, String timerName) throws SQLException
   		{
   			EsfPreparedStatement stmt = null;
   			if ( EsfString.isBlank(timerName) )
   				timerName = DEFAULT_NAME;
   			else
   				timerName = timerName.trim();

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT expire_timestamp FROM esf_transaction_timer WHERE transaction_id=? AND lower(timer_name)=?"
   	        									);
   	        	stmt.set(transactionId);
   	        	stmt.set(timerName.toLowerCase());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
		            EsfDateTime expireTimestamp = rs.getEsfDateTime();
		            
		            TransactionTimer tranTimer = new TransactionTimer(transactionId,timerName,expireTimestamp);
		            tranTimer.setLoadedFromDb();

		            return tranTimer;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByNameForTransaction() - transactionId: " + transactionId + "; timerName: " + timerName);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return null;
  		}
   		
   		public static TransactionTimer getByNameForTransaction(EsfUUID transactionId, String timerName)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	TransactionTimer timer = getByNameForTransaction(con,transactionId,timerName);
   	        	con.commit();
   	        	return timer;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   		public static int deleteAllForTransaction(Connection con, EsfUUID transactionId) throws SQLException
   		{
   			EsfPreparedStatement stmt = null;
   			
   			int numDeleted = 0;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"DELETE FROM esf_transaction_timer WHERE transaction_id=?"
   	        									);
   	        	stmt.set(transactionId);
   	        	
   	        	numDeleted = stmt.executeUpdate();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.deleteAllForTransaction() - transactionId: " + transactionId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return numDeleted;
  		}
   		

   		
   		public static TransactionTimer createNew(EsfUUID transactionId, String timerName, EsfDateTime expireTimestamp)
   		{
   			return new TransactionTimer(transactionId, timerName, expireTimestamp);
   		}
   	    
   	} // Manager
   	
}