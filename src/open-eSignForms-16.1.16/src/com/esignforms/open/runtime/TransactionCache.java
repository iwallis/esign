// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime;

import java.util.TreeMap;

import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfUUID;

/**
* Holds a Transaction object that's known by its EsfUUID, but can be retrieved by its various other pickup mechanisms.
* This uses a time-based algorithm to keep transactions in the cache so long as it has been accessed within a particular
* amount of time.
* 
* @author Yozons, Inc.
*/
public class TransactionCache
{
    protected TreeMap<EsfUUID,Transaction> uuidCache = new TreeMap<EsfUUID,Transaction>();


    public TransactionCache()
    {
    }
    
    public synchronized void clear()
    {
    	uuidCache.clear();
    }
    
    /**
     * Returns the number objects in the cache.
     */
    public synchronized int size()
    {
    	return uuidCache.size();
    }
    
    /**
     * Adds the object to the cache if and only if the object's id is not already in the cache.
     * @param obj the object to add
     * @return true if the object is added to the cache; false if the cache already contains an object with the id/name
     */
    public synchronized boolean add(final Transaction tran)
    {
    	EsfUUID id = tran.getId();
    	
    	if ( uuidCache.containsKey(id) )
    		return false;
    	
    	tran.lastAccessedFromCache = new EsfDateTime();
    	uuidCache.put(id, tran);
    	return true;
    }

    /**
     * Replaces the specified Transaction in the cache. If the transaction is not in the cache it'll be added unless it's marked as deleted or brand new. If new, add it.
     * @return true if the cache is updated with the new object; false if the id doesn't exist in the cache and the tran is deleted/never-yet-saved.
     */
    public synchronized boolean replace(final Transaction tran)
    {
    	EsfUUID id = tran.getId();
    	
    	if ( ! uuidCache.containsKey(id) )
    	{
    		if ( tran.isDeleted() || tran.doInsert() ) 
    		{
	    		tran.lastAccessedFromCache = null;
	    		return false;
    		}
    	}
    	
    	tran.lastAccessedFromCache = new EsfDateTime();
    	uuidCache.put(id, tran);
    	return true;
    }

    /**
     * Removes the specified object from the cache using only the id as the key. 
     * @param tran the Transaction to remove
     * @return true if the tran is removed; false if not found
     */
    public synchronized boolean remove(final Transaction tran)
    {
    	Transaction delTran = uuidCache.remove(tran.getId());
    	return delTran != null;
    }

    /**
     * Gets the Transaction by id.
     * @param id the id of the transaction to retrieve
     * @return the object found or null if not present
     */
    public synchronized Transaction getById(EsfUUID id)
    {
    	Transaction tran = uuidCache.get(id);
    	if ( tran != null ) 
        	tran.lastAccessedFromCache = new EsfDateTime();
    	return tran;
    }

    /**
     * Finds all transactions that have not been accessed within the specified number of minutes.
     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
     * @return the number of transactions flushed from the cache
     */
	public int flushNotAccessedWithinMinutes(int numMinutes)
    {
    	if ( numMinutes == 0 )
    		numMinutes = -1;
    	else if ( numMinutes > 0 )
    		numMinutes *= -1;
    	
    	EsfDateTime checkTime = new EsfDateTime();
    	checkTime.addMinutes(numMinutes);
    	
    	TreeMap<EsfUUID,Transaction> origTreeMap = new TreeMap<EsfUUID,Transaction>(uuidCache);
    	
    	int numFlushed = 0;
    	for( Transaction tran : origTreeMap.values() )
    	{
    		if ( tran.lastAccessedFromCache == null ||  tran.lastAccessedFromCache.isBefore(checkTime) )
    		{
    			remove(tran);
    			++numFlushed;
    		}
    	}
    	
    	return numFlushed;
    }

}