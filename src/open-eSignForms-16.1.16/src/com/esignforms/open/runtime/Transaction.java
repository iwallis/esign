// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDecimal;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.data.Record;
import com.esignforms.open.email.OutboundEmailMessage;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.integration.httpsend.HttpSendRequest;
import com.esignforms.open.jsp.PageBean;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentIdFieldName;
import com.esignforms.open.prog.DocumentIdPartyId;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.File;
import com.esignforms.open.prog.FileVersion;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.prog.PackageVersionReportField;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.runtime.workflow.TransactionEngine;
import com.esignforms.open.servlet.StartTransaction;
import com.esignforms.open.servlet.UpdateTransaction;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.User;
import com.esignforms.open.util.StringReplacement;

/**
* Transaction represents an activated instance of a TransactionTemplate.
* It should be multi-thread safe since it's likely that a given transaction will be access by multiple users concurrently.
* 
* @author Yozons, Inc.
*/
public class Transaction
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<Transaction>, java.io.Serializable
{
	private static final long serialVersionUID = 469993732360645528L;

	public static final String TRAN_TYPE_PRODUCTION = "P";
	public static final String TRAN_TYPE_TEST = "T";
	public static final String TRAN_TYPE_TEST_PRODUCTION = "t"; // this is a test mode, but we resolve as if production (used to create a test transaction using the production settings)
	
	public static final String TRAN_STATUS_CANCELED = "X";
	public static final String TRAN_STATUS_COMPLETED = "C";
	public static final String TRAN_STATUS_IN_PROGRESS = "I";
	public static final String TRAN_STATUS_SUSPENDED = "S";
	
	public static final EsfName RECORD_CLONED_FROM_TRANSACTION_ESFNAME = new EsfName("ESF_Cloned_From_Transaction_ID"); // added to the tran and trandocs that are cloned
	public static final EsfName RECORD_CLONED_FROM_TRANSACTION_DOCUMENT_ESFNAME = new EsfName("ESF_Cloned_From_Transaction_Document_ID"); // added to the trandocs that are cloned
	
	public static final EsfName TRAN_RECORD_START_TIMESTAMP_ESFNAME = new EsfName("ESF_Start_Timestamp");
	public static final EsfName TRAN_RECORD_STARTED_API_MODE_ESFNAME = new EsfName("ESF_Started_API_Mode");
	public static final EsfName TRAN_RECORD_STARTED_BY_USER_ID_ESFNAME = new EsfName("ESF_Started_By_User_Id");
	public static final EsfName TRAN_RECORD_STARTED_BY_USER_EMAIL_ESFNAME = new EsfName("ESF_Started_By_User_Email");
	public static final EsfName TRAN_RECORD_STARTED_BY_USER_EMAIL_ADDRESS_ESFNAME = new EsfName("ESF_Started_By_User_Email_Address");
	public static final EsfName TRAN_RECORD_STARTED_BY_USER_PERSONAL_NAME_ESFNAME = new EsfName("ESF_Started_By_User_Personal_Name");
	public static final EsfName TRAN_RECORD_STARTED_BY_USER_FAMILY_NAME_ESFNAME = new EsfName("ESF_Started_By_User_Family_Name");
	public static final EsfName TRAN_RECORD_STARTED_BY_USER_EMPLOYEE_ID_ESFNAME = new EsfName("ESF_Started_By_User_Employee_ID");
	public static final EsfName TRAN_RECORD_STARTED_BY_USER_JOB_TITLE_ESFNAME = new EsfName("ESF_Started_By_User_Job_Title");
	public static final EsfName TRAN_RECORD_STARTED_BY_USER_LOCATION_ESFNAME = new EsfName("ESF_Started_By_User_Location");
	public static final EsfName TRAN_RECORD_STARTED_BY_USER_DEPARTMENT_ESFNAME = new EsfName("ESF_Started_By_User_Department");
	public static final EsfName TRAN_RECORD_STARTED_BY_USER_PHONE_NUMBER_ESFNAME = new EsfName("ESF_Started_By_User_Phone_Number");
	public static final EsfName TRAN_RECORD_REQUEST_METHOD_ESFNAME = new EsfName("ESF_Request_Method");
	public static final EsfName TRAN_RECORD_REQUEST_CONTENT_TYPE_ESFNAME = new EsfName("ESF_Request_ContentType");
	public static final EsfName TRAN_RECORD_REQUEST_REQUEST_URL_ESFNAME = new EsfName("ESF_Request_RequestURL");
	public static final EsfName TRAN_RECORD_REQUEST_QUERY_STRING_ESFNAME = new EsfName("ESF_Request_QueryString");
	public static final EsfName TRAN_RECORD_REQUEST_REMOTE_ADDR_ESFNAME = new EsfName("ESF_Request_RemoteAddr");
	
	// For update API calls
	public static final EsfName TRAN_RECORD_UPDATE_API_PREFIX = new EsfName("ESF_UPDATE_API");
	public static final EsfName TRAN_RECORD_UPDATE_API_TIMESTAMP_ESFNAME = new EsfName("ESF_UpdateAPI_Timestamp");
	public static final EsfName TRAN_RECORD_UPDATE_API_USER_ID_ESFNAME = new EsfName("ESF_UpdateAPI_By_User_Id");
	public static final EsfName TRAN_RECORD_UPDATE_API_USER_EMAIL_ESFNAME = new EsfName("ESF_UpdateAPI_By_User_Email");

	// Used for CSV uploads -- mostly for tracing and debugging -- added to the transaction's data record
	public static final EsfName TRAN_RECORD_CSV_UPLOAD_STARTED_BY_EMAIL_ESFNAME = new EsfName("ESF_CSV_Upload_Started_By_Email");
	public static final EsfName TRAN_RECORD_CSV_UPLOAD_FILENAME_ESFNAME = new EsfName("ESF_CSV_Upload_Filename");
	public static final EsfName TRAN_RECORD_CSV_UPLOAD_FILE_LINE_NUMBER_ESFNAME = new EsfName("ESF_CSV_Upload_File_Line_Number");
	public static final EsfName TRAN_RECORD_CSV_UPLOAD_HEADER_LINE_ESFNAME = new EsfName("ESF_CSV_Upload_Header_Line");
	public static final EsfName TRAN_RECORD_CSV_UPLOAD_LINE_ESFNAME = new EsfName("ESF_CSV_Upload_Line");

	public static final EsfName TRAN_RECORD_CANCELED_TIMESTAMP_ESFNAME = new EsfName("ESF_Canceled_Timestamp");
	public static final EsfName TRAN_RECORD_CANCELED_REASON_ESFNAME = new EsfName("ESF_Canceled_Reason");
	public static final EsfName TRAN_RECORD_AUTO_CANCEL_REASON_ESFNAME = new EsfName("ESF_Auto_Cancel_Reason");
	
	public static final EsfName TRAN_RECORD_SUSPENDED_TIMESTAMP_ESFNAME = new EsfName("ESF_Suspended_Timestamp");
	public static final EsfName TRAN_RECORD_SUSPENDED_REASON_ESFNAME = new EsfName("ESF_Suspended_Reason");
	public static final EsfName TRAN_RECORD_RESUMED_TIMESTAMP_ESFNAME = new EsfName("ESF_Resumed_Timestamp");
	public static final EsfName TRAN_RECORD_RESUMED_REASON_ESFNAME = new EsfName("ESF_Resumed_Reason");

	public static final String SESSION_ATTR_CLONE_TRANSACTION_ID = "ESFCLONE-TRANSACTION-ID";

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(Transaction.class);

    
    protected final EsfUUID id;
    protected final EsfUUID transactionTemplateId;
    protected final EsfUUID packageId;
    protected EsfUUID packageVersionId;
    protected final EsfUUID brandLibraryId;
    protected final String tranType; 
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected EsfDateTime stallTimestamp;
    protected EsfDateTime expireTimestamp;
    protected EsfDateTime cancelTimestamp;
    protected String cancelRetentionSpec; // optional spec, if present and transaction cancels, sets the new retention as long as it is sooner than the current expireTimestamp.
    protected int cancelRetentionNumTimeIntervalUnits; // derived, not persisted
    protected String cancelRetentionTimeIntervalUnits; // derived, not persisted
    protected String status;
    protected String statusText;
    protected Record record;
    
    Map<EsfName,TransactionDocument> transactionDocumentMap = new TreeMap<EsfName,TransactionDocument>(); // keeps track of the documents 
    Map<EsfName,TransactionParty> transactionPartyMap = new TreeMap<EsfName,TransactionParty>(); // keeps track of the parties
    
    List<TransactionFile> transactionFileList;
    List<TransactionFile> transactionFilesToDeleteOnSave;

    // Used only by the Transaction cache -- not persisted
    protected EsfDateTime lastAccessedFromCache;

    // Used to mark a transaction as temporary and thus won't be persisted 
    protected boolean isTemporary = false;

    /**
     * This creates a Transaction object from data retrieved from the DB.
     */
    protected Transaction(EsfUUID id, EsfUUID transactionTemplateId, EsfUUID packageId, EsfUUID packageVersionId, EsfUUID brandLibraryId, String tranType, 
    				EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId, EsfDateTime stallTimestamp,
    				EsfDateTime expireTimestamp, EsfDateTime cancelTimestamp, String cancelRetentionSpec, String status, String statusText, Record record, List<TransactionFile> transactionFileList)
    {
        this.id = id;
        this.transactionTemplateId = transactionTemplateId;
        this.packageId = packageId;
        this.packageVersionId = packageVersionId;
        this.brandLibraryId = brandLibraryId;
        this.tranType = tranType;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.stallTimestamp = stallTimestamp;
        this.expireTimestamp = expireTimestamp;
        this.cancelTimestamp = cancelTimestamp;
        this.setCancelRetentionSpec(cancelRetentionSpec);
        this.status = status;
        this.statusText = statusText;
        this.record = record;
        this.transactionFileList = transactionFileList;
    }
    
    protected Transaction(TransactionTemplate transactionTemplate, String tranType, User createdBy)
	{
    	this.id = new EsfUUID();
    	this.transactionTemplateId = transactionTemplate.getId();
    	this.packageId = transactionTemplate.getPackageId();
    	this.packageVersionId = null;
    	this.brandLibraryId = transactionTemplate.getBrandLibraryId();
    	this.tranType = tranType;
    	this.createdTimestamp = new EsfDateTime();
    	this.createdByUserId = createdBy == null ? null : createdBy.getId();
    	this.lastUpdatedTimestamp = createdTimestamp;
    	this.lastUpdatedByUserId = createdByUserId;
    	this.stallTimestamp = null;
    	this.expireTimestamp = null;
    	this.cancelTimestamp = null;
    	this.setCancelRetentionSpec(null);
    	this.status = TRAN_STATUS_IN_PROGRESS;
    	this.statusText = "Started";
	    this.record = new Record( new EsfName("data"), BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE);
	    this.transactionFileList = new LinkedList<TransactionFile>();
	}
   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getTransactionTemplateId()
    {
        return transactionTemplateId;
    }
    public TransactionTemplate getTransactionTemplate()
    {
    	return TransactionTemplate.Manager.getById(transactionTemplateId);
    }
    
    public final EsfUUID getPackageId()
    {
        return packageId;
    }
    public Package getPackage()
    {
    	return Package.Manager.getById(packageId);
    }
    
    public EsfUUID getPackageVersionId()
    {
        return packageVersionId;
    }
    public PackageVersion getPackageVersion()
    {
    	return PackageVersion.Manager.getById(packageVersionId);
    }
    
    public String getPackageDownloadFileName(User user, DocumentVersion currentDocumentVersion)
    {
    	String downloadFileName = "";
    	Package pkg = getPackage();
    	if ( pkg.hasDownloadFileNameSpec() )
    		downloadFileName = this.getFieldSpecWithSubstitutions(user, currentDocumentVersion, pkg.getDownloadFileNameSpec());
    	if ( EsfString.isBlank(downloadFileName) || ".pdf".equalsIgnoreCase(downloadFileName) )
    		downloadFileName = Application.getInstance().getServerMessages().getString("Package.defaultDownloadFileName");
    	if ( ! downloadFileName.toLowerCase().endsWith(".pdf") )
    		downloadFileName += ".pdf";
    	return downloadFileName.trim();
    }
    
    public final EsfUUID getBrandLibraryId()
    {
        return brandLibraryId;
    }
    public boolean hasBrandLibraryId()
    {
    	return brandLibraryId != null && ! brandLibraryId.isNull();
    }
    public Library getBrandLibrary()
    {
		return hasBrandLibraryId() ? Library.Manager.getById(brandLibraryId) : null;
    }

    public String getStatus()
    {
        return status;
    }
    public void setStatus(String v)
    {
    	status = v;
    	objectChanged();
    }
    public boolean isCanceled()
    {
    	return isCanceled(status);
    }
    public final static boolean isCanceled(String checkStatus)
    {
    	return TRAN_STATUS_CANCELED.equals(checkStatus);
    }
    public void setStatusCanceled()
    {
    	setStatus(TRAN_STATUS_CANCELED);
    }
    public boolean isCompleted()
    {
    	return TRAN_STATUS_COMPLETED.equals(status);
    }
    public void setStatusCompleted()
    {
    	setStatus(TRAN_STATUS_COMPLETED);
    }
    public boolean isEndState()
    {
    	return isCanceled() || isCompleted();
    }
    public boolean isInProgress()
    {
    	return TRAN_STATUS_IN_PROGRESS.equals(status);
    }
    public void setStatusInProgress()
    {
    	setStatus(TRAN_STATUS_IN_PROGRESS);
    }
    public final static boolean isInProgress(String checkStatus)
    {
    	return TRAN_STATUS_IN_PROGRESS.equals(checkStatus);
    }
    public boolean isSuspended()
    {
    	return TRAN_STATUS_SUSPENDED.equals(status);
    }
    public final static boolean isSuspended(String checkStatus)
    {
    	return TRAN_STATUS_SUSPENDED.equals(checkStatus);
    }
    public void setStatusSuspended()
    {
    	setStatus(TRAN_STATUS_SUSPENDED);
    }
    public String getStatusLabel()
    {
    	if ( isCompleted() )
    		return Application.getInstance().getServerMessages().getString("transaction.status.completed");
    	if ( isCanceled() )
    		return Application.getInstance().getServerMessages().getString("transaction.status.canceled");
    	if ( isInProgress() )
    		return Application.getInstance().getServerMessages().getString("transaction.status.inProgress");
    	if ( isSuspended() )
    		return Application.getInstance().getServerMessages().getString("transaction.status.suspended");
    	return "??"+status+"??";
    }
    
    public String getStatusText()
    {
    	return statusText;
    }
    public void setStatusText(String v)
    {
       	if ( v == null )
       		statusText = v;
    	else if ( v.length() > Literals.STATUS_TEXT_MAX_LENGTH )
    		statusText = v.substring(0,Literals.STATUS_TEXT_MAX_LENGTH).trim();
        else
        	statusText = v.trim();
    	objectChanged();
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
        return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
        return createdByUserId;
    }
    public boolean hasCreatedByUserId()
    {
    	return createdByUserId != null;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
        return lastUpdatedTimestamp;
    }
    public void _setChangedForInternalUseOnly()
    {
    	objectChanged();
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
        return lastUpdatedByUserId;
    }
    public boolean hasLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId != null;
    }
    
    public EsfDateTime getStallTimestamp()
    {
        return stallTimestamp;
    }
    public void setStalled()
    {
    	stallTimestamp = new EsfDateTime();
    	objectChanged();
    }
    public void clearStalled()
    {
    	stallTimestamp = null;
    	objectChanged();
    }
    public boolean isStalled()
    {
    	return stallTimestamp != null;
    }
    
    public EsfDateTime getExpireTimestamp()
    {
        return expireTimestamp;
    }
    public void setExpireTimestamp(EsfDateTime v)
    {
    	expireTimestamp = v;
    	objectChanged();
    }
    public boolean hasExpireTimestamp()
    {
    	return expireTimestamp != null;
    }

    public EsfDateTime getCancelTimestamp()
    {
        return cancelTimestamp;
    }
    public void setCancelTimestamp(EsfDateTime v)
    {
    	cancelTimestamp = v;
    	objectChanged();
    }
    public void clearCancelTimestamp()
    {
    	setCancelTimestamp(null);
    }
    public boolean hasCancelTimestamp()
    {
    	return cancelTimestamp != null;
    }
    
    public String getCancelRetentionSpec()
    {
    	return cancelRetentionSpec;
    }
    public boolean hasCancelRetentionSpec()
    {
    	return EsfString.isNonBlank(cancelRetentionSpec);
    }
    public void setCancelRetentionSpec(String v)
    {
    	v = v == null ? null : v.trim();
    	if ( EsfString.isBlank(v) )
    	{
    		if ( cancelRetentionSpec != null ) // did it change?
    		{
    			cancelRetentionSpec = null;
    			objectChanged();
    		}
    	}
    	else
    	{
        	String[] parts = v.split(" ");
        	if ( parts.length == 2 )
        	{
        		int numUnits = Application.getInstance().stringToInt(parts[0],0);
        		if ( numUnits < 1 )
        			numUnits = 1;
        		String units = parts[1];
        		String newSpec = numUnits + " " + units;
        		if ( ! newSpec.equals(cancelRetentionSpec) ) // check if it's different from before
        		{
        			cancelRetentionNumTimeIntervalUnits = numUnits;
        			cancelRetentionTimeIntervalUnits = units;
        			cancelRetentionSpec = newSpec;
                	objectChanged();
        		}
        	}
    	}
    }
    public boolean isCancelRetentionForever() 
    {
    	return Literals.TIME_INTERVAL_UNIT_FOREVER.equals(cancelRetentionTimeIntervalUnits);
    }
    public EsfDateTime getCancelRetentionDateTimeFromNow()
    {
    	EsfDateTime retentionDate = new EsfDateTime();
    	retentionDate.addTimeInterval(cancelRetentionNumTimeIntervalUnits, cancelRetentionTimeIntervalUnits);
    	return retentionDate;
    }

    
    public String getTranType()
    {
    	return tranType;
    }
    public boolean doProductionResolve() 
    {
    	return TRAN_TYPE_PRODUCTION.equals(tranType) || TRAN_TYPE_TEST_PRODUCTION.equals(tranType);
    }
    public boolean doTestResolve() 
    {
    	return TRAN_TYPE_TEST.equals(tranType);
    }
    public boolean isProduction() 
    {
    	return TRAN_TYPE_PRODUCTION.equals(tranType);
    }
    public boolean isTest() 
    {
    	return TRAN_TYPE_TEST.equals(tranType) || TRAN_TYPE_TEST_PRODUCTION.equals(tranType);
    }
    
    public boolean isTemporary()
    {
    	return isTemporary;
    }
    
    public Record getRecord()
    {
    	return record;
    }
    void addUpdateTransactionStartedRequestData(HttpServletRequest request)
    {
    	record.addUpdate( TRAN_RECORD_START_TIMESTAMP_ESFNAME, new EsfDateTime(), false );
    	
    	EsfBoolean startedApiMode = new EsfBoolean((String)request.getAttribute(PageBean.ESF_IS_API_MODE_ATTRIBUTE));
    	record.addUpdate( TRAN_RECORD_STARTED_API_MODE_ESFNAME, startedApiMode, false );

    	Enumeration<String> paramNames = request.getParameterNames();
    	while( paramNames.hasMoreElements() ) 
    	{
    		String paramName = paramNames.nextElement();
    		// We don't save the PARAM_ESFLOGINPASSWORD if present
    		if ( StartTransaction.PARAM_ESFLOGINPASSWORD.equalsIgnoreCase(paramName) )
    			continue;
    		EsfName esfname = EsfName.createFromRegularString(paramName);
    		String[] values = request.getParameterValues(paramName);
    		if ( values.length == 1 )
    			record.addUpdate( esfname, new EsfString(values[0]), true ); // we let any original tran start data to be cloned
    		else
    		{
    			EsfString[] esfvalues = new EsfString[values.length];
    			for( int i=0; i < values.length; ++i )
    				esfvalues[i] = new EsfString(values[i]);
    			record.addUpdate( esfname, esfvalues, true ); // we let any original tran start data to be cloned
    		}
    	}
    	
    	// Save the first value of each header
    	java.util.Enumeration<String> headerNames = request.getHeaderNames(); 
    	while( headerNames.hasMoreElements() ) 
    	{
    		String name = headerNames.nextElement();
    		java.util.Enumeration<String> values = request.getHeaders(name);
	   
    		if( values.hasMoreElements() ) 
    		{
    			String value = values.nextElement(); // ignore any arrays of headers
    			record.addUpdate( EsfName.createFromRegularString("ESF_Request_Header_"+name), new EsfString(value), false );
    		}
    	}

    	// For now, we ignore headers, but we do store a few items from the request should they be needed
    	record.addUpdate( TRAN_RECORD_REQUEST_METHOD_ESFNAME, new EsfString(request.getMethod()), false );
    	String contentType = request.getContentType();
    	if ( contentType != null )
    		record.addUpdate( TRAN_RECORD_REQUEST_CONTENT_TYPE_ESFNAME, new EsfString(contentType), false );
    	record.addUpdate( TRAN_RECORD_REQUEST_REQUEST_URL_ESFNAME, new EsfString(request.getRequestURL().toString()), false );
    	record.addUpdate( TRAN_RECORD_REQUEST_QUERY_STRING_ESFNAME, new EsfString(request.getQueryString()), false );
    	record.addUpdate( TRAN_RECORD_REQUEST_REMOTE_ADDR_ESFNAME, new EsfString(request.getRemoteAddr()), false );
    	
    	objectChanged();
    }
    void addUpdateTransactionStartedCreatedBy(User createdBy)
    {
    	record.addUpdate( TRAN_RECORD_STARTED_BY_USER_ID_ESFNAME, createdBy.getId(), false );
    	record.addUpdate( TRAN_RECORD_STARTED_BY_USER_EMAIL_ESFNAME, new EsfString(createdBy.getEmail()), false );
    	record.addUpdate( TRAN_RECORD_STARTED_BY_USER_EMAIL_ADDRESS_ESFNAME, createdBy.getEmailAddress(), false );
    	record.addUpdate( TRAN_RECORD_STARTED_BY_USER_PERSONAL_NAME_ESFNAME, new EsfString(createdBy.getPersonalName()), false );
    	record.addUpdate( TRAN_RECORD_STARTED_BY_USER_FAMILY_NAME_ESFNAME, new EsfString(createdBy.getFamilyName()), false );
    	record.addUpdate( TRAN_RECORD_STARTED_BY_USER_EMPLOYEE_ID_ESFNAME, new EsfString(createdBy.getEmployeeId()), false );
    	record.addUpdate( TRAN_RECORD_STARTED_BY_USER_JOB_TITLE_ESFNAME, new EsfString(createdBy.getJobTitle()), false );
    	record.addUpdate( TRAN_RECORD_STARTED_BY_USER_LOCATION_ESFNAME, new EsfString(createdBy.getLocation()), false );
    	record.addUpdate( TRAN_RECORD_STARTED_BY_USER_DEPARTMENT_ESFNAME, new EsfString(createdBy.getDepartment()), false );
    	record.addUpdate( TRAN_RECORD_STARTED_BY_USER_PHONE_NUMBER_ESFNAME, new EsfString(createdBy.getPhoneNumber()), false );
    	objectChanged();
    }
    
    
    void addUpdateTransactionUpdateApiRequestData(User updatedBy, HttpServletRequest request)
    {
    	record.addUpdate( TRAN_RECORD_UPDATE_API_TIMESTAMP_ESFNAME, new EsfDateTime(), false );
    	record.addUpdate( TRAN_RECORD_UPDATE_API_USER_ID_ESFNAME, updatedBy.getId(), false );
    	record.addUpdate( TRAN_RECORD_UPDATE_API_USER_EMAIL_ESFNAME, new EsfString(updatedBy.getEmail()), false );
    	
    	Enumeration<String> paramNames = request.getParameterNames();
    	while( paramNames.hasMoreElements() ) 
    	{
    		String paramName = paramNames.nextElement();
    		// We don't save the PARAM_ESFLOGINPASSWORD if present
    		if ( UpdateTransaction.PARAM_ESFLOGINPASSWORD.equalsIgnoreCase(paramName) )
    			continue;
			EsfPathName pathname = new EsfPathName(TRAN_RECORD_UPDATE_API_PREFIX,EsfName.createFromRegularString(paramName));
    		String[] values = request.getParameterValues(paramName);
    		if ( values.length == 1 )
    			record.addUpdate( pathname, new EsfString(values[0]), true ); // we let any update API tran data to be cloned
    		else
    		{
    			EsfString[] esfvalues = new EsfString[values.length];
    			for( int i=0; i < values.length; ++i )
    				esfvalues[i] = new EsfString(values[i]);
    			record.addUpdate( pathname, esfvalues, true ); // we let any update API tran data to be cloned
    		}
    	}
    	
    	// Save the first value of each header
    	java.util.Enumeration<String> headerNames = request.getHeaderNames(); 
    	while( headerNames.hasMoreElements() ) 
    	{
    		String name = headerNames.nextElement();
    		java.util.Enumeration<String> values = request.getHeaders(name);
	   
    		if( values.hasMoreElements() ) 
    		{
    			String value = values.nextElement(); // ignore any arrays of headers
    			EsfPathName pathname = new EsfPathName(TRAN_RECORD_UPDATE_API_PREFIX,EsfName.createFromRegularString("ESF_Request_Header_"+name));
    			record.addUpdate( pathname, new EsfString(value), false );
    		}
    	}

    	// For now, we ignore headers, but we do store a few items from the request should they be needed
    	record.addUpdate( new EsfPathName(TRAN_RECORD_UPDATE_API_PREFIX,TRAN_RECORD_REQUEST_METHOD_ESFNAME), new EsfString(request.getMethod()), false );
    	String contentType = request.getContentType();
    	if ( contentType != null )
    		record.addUpdate( new EsfPathName(TRAN_RECORD_UPDATE_API_PREFIX,TRAN_RECORD_REQUEST_CONTENT_TYPE_ESFNAME), new EsfString(contentType), false );
    	record.addUpdate( new EsfPathName(TRAN_RECORD_UPDATE_API_PREFIX,TRAN_RECORD_REQUEST_REQUEST_URL_ESFNAME), new EsfString(request.getRequestURL().toString()), false );
    	record.addUpdate( new EsfPathName(TRAN_RECORD_UPDATE_API_PREFIX,TRAN_RECORD_REQUEST_QUERY_STRING_ESFNAME), new EsfString(request.getQueryString()), false );
    	record.addUpdate( new EsfPathName(TRAN_RECORD_UPDATE_API_PREFIX,TRAN_RECORD_REQUEST_REMOTE_ADDR_ESFNAME), new EsfString(request.getRemoteAddr()), false );
    	
    	objectChanged();
    }

    
    public synchronized List<TransactionFile> getAllTransactionFiles() 
    {
    	return new LinkedList<TransactionFile>(transactionFileList);
    }
    public synchronized TransactionFile getTransactionFileById(EsfUUID tranFileId) 
    {
    	for( TransactionFile tranFile : getAllTransactionFiles() )
    	{
    		if ( tranFile.getId().equals(tranFileId) )
    			return tranFile;
    	}
    	_logger.warn("getTransactionFileById() - Failed to find TransactionFile; tranFileId: " + tranFileId);
    	return null;
    }
    public synchronized List<TransactionFile> getAllTransactionFilesForDocumentField(EsfUUID tranDocId, EsfName fieldName) 
    {
    	LinkedList<TransactionFile> list = new LinkedList<TransactionFile>();
    	for( TransactionFile tranFile : getAllTransactionFiles() )
    	{
    		if ( tranFile.getTransactionDocumentId().equals(tranDocId) && tranFile.getFieldName().equals(fieldName) )
    			list.add(tranFile);
    	}
    	return list;
    }
    public synchronized List<TransactionFile> getAllTransactionFilesForDocument(EsfUUID tranDocId) 
    {
    	LinkedList<TransactionFile> list = new LinkedList<TransactionFile>();
    	for( TransactionFile tranFile : getAllTransactionFiles() )
    	{
    		if ( tranFile.getTransactionDocumentId().equals(tranDocId) )
    			list.add(tranFile);
    	}
    	return list;
    }
    public synchronized void addTransactionFile(TransactionFile tranFile) 
    {
    	transactionFileList.add(tranFile);
    	objectChanged();
    }
    public synchronized boolean removeTransactionFile(TransactionFile tranFile) 
    {
    	if ( transactionFileList.remove(tranFile) )
    	{
    		if ( transactionFilesToDeleteOnSave == null )
    			transactionFilesToDeleteOnSave = new LinkedList<TransactionFile>();
    		transactionFilesToDeleteOnSave.add(tranFile);
        	objectChanged();
        	return true;
    	}
    	return false;
    }
    
    Document getPackageDocument()
    {
    	return getPackageVersion().getPackageDocument();
    }
    boolean isPackageDocument(Document doc)
    {
    	return doc.equals(getPackageDocument());
    }
    boolean isPackageDocumentName(EsfName docName)
    {
    	return docName.equals(getPackageDocument().getEsfName());
    }
    
    public List<TransactionDocument> getAllTransactionDocuments()
    {
    	LinkedList<TransactionDocument> list = new LinkedList<TransactionDocument>();
    	for( EsfName docName : getPackageVersion().getDocumentNameList() ) 
    	{
        	TransactionDocument tranDoc = transactionDocumentMap.get(docName);
        	if ( tranDoc == null )
        		_logger.warn("getAllTransactionDocuments() - Failed to find transaction document with name: " + docName + "; transactionId: " + id);
        	else
        		list.add(tranDoc);
    	}
    	return list;
    }
    public int getNumTransactionDocumentsWithDocumentSnapshot()
    {
    	int numWithSnapshots = 0;
    	
    	for( EsfName docName : getPackageVersion().getDocumentNameList() ) 
    	{
        	TransactionDocument tranDoc = transactionDocumentMap.get(docName);
        	if ( tranDoc != null )
        	{
        		TransactionPartyDocument tpd = getLatestTransactionPartyDocumentWithSnapshot(tranDoc);
        		if ( tpd != null && ! tpd.isSnapshotDocumentSkipped() )
        			++numWithSnapshots;
        	}
    	}
    	
    	return numWithSnapshots;
    }
    
    public TransactionDocument getTransactionDocument(EsfName tranDocName)
    {
    	TransactionDocument tranDoc = transactionDocumentMap.get(tranDocName);
    	if ( tranDoc == null )
    	{
    		if ( ! isPackageDocumentName(tranDocName) )
    			_logger.warn("getTransactionDocument() - Failed to find transaction document with name: " + tranDocName + "; transactionId: " + id);
    	}
    	return tranDoc;
    }
    
    public TransactionDocument getTransactionDocument(EsfUUID tranDocId)
    {
        for( TransactionDocument tranDoc : transactionDocumentMap.values() )
        {
        	if ( tranDoc.getId().equals(tranDocId) )
        		return tranDoc;
        }

        _logger.warn("getTransactionDocument() - Failed to find transaction document with id: " + tranDocId + "; transactionId: " + id);
    	return null;
    }
    
    public TransactionDocument getTransactionDocumentByDocumentId(EsfUUID docId)
    {
        for( TransactionDocument tranDoc : transactionDocumentMap.values() )
        {
        	if ( tranDoc.getDocumentId().equals(docId) )
        		return tranDoc;
        }

        _logger.warn("getTransactionDocumentByDocumentId() - Failed to find transaction document with document id: " + docId + "; transactionId: " + id);
    	return null;
    }
    
    public TransactionDocument getFirstTransactionDocumentWithValueForField(EsfName fieldName)
    {
        for( TransactionDocument tranDoc : transactionDocumentMap.values() )
        {
        	EsfValue value = tranDoc.getRecord().getValueByName(fieldName);
        	if ( value != null )
        		return tranDoc;
        }

        _logger.warn("getFirstTransactionDocumentWithValueForField() - Failed to find transaction document with value for field: " + fieldName + "; transactionId: " + id);
    	return null;
    }
    
    public List<TransactionParty> getAllTransactionParties()
    {
    	LinkedList<TransactionParty> list = new LinkedList<TransactionParty>();
    	for( EsfName partyName : getPackageVersion().getPackageVersionPartyNameList() ) 
    	{
    		TransactionParty tranParty = transactionPartyMap.get(partyName);
        	if ( tranParty == null )
        		_logger.warn("getAllTransactionParties() - Failed to find transaction party with name: " + partyName + "; transactionId: " + id);
        	else
        		list.add(tranParty);
    	}
    	return list;
    }
    
    
    public List<TransactionParty> getAllTransactionPartiesForTransactionDocument(TransactionDocument tranDoc)
    {
    	LinkedList<TransactionParty> list = new LinkedList<TransactionParty>();
    	for( TransactionParty tranParty : transactionPartyMap.values() ) 
    	{
    		TransactionPartyDocument tranPartyDoc = tranParty.getTransactionPartyDocument(tranDoc.getId());
    		if ( tranPartyDoc != null )
    			list.add(tranParty);
    	}
    	return list;
    }

    public List<TransactionPartyDocument> getAllTransactionPartyDocumentsForTransactionDocument(TransactionDocument tranDoc)
    {
    	LinkedList<TransactionPartyDocument> list = new LinkedList<TransactionPartyDocument>();
    	for( TransactionParty tranParty : transactionPartyMap.values() ) 
    	{
    		TransactionPartyDocument tranPartyDoc = tranParty.getTransactionPartyDocument(tranDoc.getId());
    		if ( tranPartyDoc != null )
    			list.add(tranPartyDoc);
    	}
    	return list;
    }
    
    public TransactionParty getTransactionParty(EsfName partyName)
    {
    	TransactionParty tranParty = transactionPartyMap.get(partyName);
    	if ( tranParty == null )
    		_logger.warn("getTransactionParty() - Failed to find transaction party with name: " + partyName + "; transactionId: " + id);
    	return tranParty;
    }
    
    public TransactionParty getTransactionParty(EsfUUID tranPartyId)
    {
    	for( TransactionParty tranParty : transactionPartyMap.values() )
    	{
    		if ( tranParty.getId().equals(tranPartyId) )
    			return tranParty;
    	}
    	_logger.warn("getTransactionParty() - Failed to find transaction party with tranPartyId: " + tranPartyId + "; transactionId: " + id);
    	return null;
    }
    
    public TransactionParty getTransactionPartyByPackageParty(EsfUUID packagePartyId)
    {
    	for( TransactionParty tranParty : transactionPartyMap.values() )
    	{
    		if ( tranParty.getPackageVersionPartyTemplateId().equals(packagePartyId) )
    			return tranParty;
    	}
    	_logger.warn("getTransactionPartyByPackageParty() - Failed to find transaction party with packagePartyId: " + packagePartyId + "; transactionId: " + id);
    	return null;
    }
    
    /**
     * Gets all TransactionPartyDocument objects that match those specified in the document+party list that has a snapshot.  If no party
     * is specified, it returns the "latest" one found.  We basically scan each document in order, then scan the parties
     * on those documents, so the returned order has no bearing on the docPartyList order, and in fact, can be shorter
     * if the specification includes non-matching docs/parties.
     * @param docPartyList where the document id is the document id to match, and the party id (may be null if non-party specific)
     * is a package party id.
     * @return
     */
    public List<TransactionPartyDocument> getTransactionPartyDocumentsWithSnapshot(List<DocumentIdPartyId> docPartyList)
    {
    	LinkedList<TransactionPartyDocument> list = new LinkedList<TransactionPartyDocument>();
    	
    	PackageVersion pkgVersion = getPackageVersion();
    	for( EsfUUID docId : pkgVersion.getDocumentIdList() )
    	{
    		for( DocumentIdPartyId dp : docPartyList )
    		{
    			if ( dp.getDocumentId().equals(docId) )
    			{
    				// If no party id is specified, we keep the latest of this one
    				if ( ! dp.hasPartyId() )
    				{
    					TransactionDocument tranDoc = getTransactionDocumentByDocumentId(docId);
    					if ( tranDoc == null )
    						_logger.warn("getTransactionPartyDocumentsWithSnapshot() - Could not find transaction document for document id: " + docId + "; skipping...");
    					else
    					{
    						TransactionPartyDocument tpd = getLatestTransactionPartyDocumentWithSnapshot(tranDoc);
    						if ( tpd != null && ! list.contains(tpd) )
    							list.add(tpd);
    					}
    				}
    				else
    				{
    					TransactionDocument tranDoc = getTransactionDocumentByDocumentId(docId);
    					if ( tranDoc == null )
    						_logger.warn("getTransactionPartyDocumentsWithSnapshot() - Could not find transaction document for document id: " + docId + "; skipping...");
    					else
    					{
    						TransactionPartyDocument tpd = getLatestTransactionPartyDocumentWithSnapshot(tranDoc,dp.getPartyId());
    						if ( tpd != null && ! list.contains(tpd) )
    							list.add(tpd);
    					}
    				}
    			}
    		}
    	}
    	
    	return list;
    }
    
    public List<TransactionPartyDocument> getTransactionPartyDocumentsWithNonSkippedSnapshot(List<DocumentIdPartyId> docPartyList)
    {
    	List<TransactionPartyDocument> tranPartyDocumentList = getTransactionPartyDocumentsWithSnapshot(docPartyList);
		// Skip any documents that we didn't capture on purpose (such as auto-completing parties). 
		ListIterator<TransactionPartyDocument> tpdIterator = tranPartyDocumentList.listIterator();
		while( tpdIterator.hasNext() )
		{
			TransactionPartyDocument tpd = tpdIterator.next();
			if ( tpd.isSnapshotDocumentSkipped() )
				tpdIterator.remove();
		}
    	return tranPartyDocumentList;
    }
    
    public TransactionPartyDocument getLatestTransactionPartyDocumentWithSnapshot(TransactionDocument tranDoc)
    {
    	TransactionPartyDocument latest = null;
    	
    	for( EsfName partyName : getPackageVersion().getPackageVersionPartyNameList() ) 
    	{
    		TransactionParty tranParty = transactionPartyMap.get(partyName);
        	if ( tranParty == null )
        		_logger.warn("getLatestTransactionPartyDocumentWithSnapshot() - Failed to find transaction party with name: " + partyName + "; transactionId: " + id);
        	else
        	{
            	List<TransactionPartyDocument> transactionPartyDocuments = tranParty.getTransactionPartyDocuments();
            	for( TransactionPartyDocument tpd : transactionPartyDocuments ) 
            	{
            		if ( ! tpd.hasSnapshotXmlOrBlobId() )
            			continue;
            		if ( tpd.getTransactionDocumentId().equals(tranDoc.getId()) ) 
            		{
            			if ( latest == null )
            				latest = tpd;
            			else if ( latest.getLastUpdatedTimestamp().isBefore(tpd.getLastUpdatedTimestamp()) )
            				latest = tpd;
            		}
            	}
        	}
    	}
    	
    	return latest;
    }
    
    public TransactionPartyDocument getLatestTransactionPartyDocumentWithSnapshot(TransactionDocument tranDoc, EsfUUID partyId)
    {
    	TransactionPartyDocument latest = null;
    	
    	for( EsfName partyName : getPackageVersion().getPackageVersionPartyNameList() ) 
    	{
    		TransactionParty tranParty = transactionPartyMap.get(partyName);
        	if ( tranParty == null )
        		_logger.warn("getTransactionPartyDocumentWithSnapshot() - Failed to find transaction party with name: " + partyName + "; transactionId: " + id);
        	else if ( tranParty.getPackageVersionPartyTemplateId().equals(partyId) )
        	{
            	List<TransactionPartyDocument> transactionPartyDocuments = tranParty.getTransactionPartyDocuments();
            	for( TransactionPartyDocument tpd : transactionPartyDocuments ) 
            	{
            		if ( ! tpd.hasSnapshotXmlOrBlobId() )
            			continue;
            		if ( tpd.getTransactionDocumentId().equals(tranDoc.getId()) ) 
            		{
            			if ( latest == null )
            				latest = tpd;
            			else if ( latest.getLastUpdatedTimestamp().isBefore(tpd.getLastUpdatedTimestamp()) )
            				latest = tpd;
            		}
            	}
            	break;
        	}
    	}
    	
    	return latest;
    }
    
    public List<TransactionFile> getTransactionFiles(List<DocumentIdFieldName> docFileFieldList)
    {
    	LinkedList<TransactionFile> list = new LinkedList<TransactionFile>();
    	
    	for( TransactionFile tf : TransactionFile.Manager.getAllForTransaction(id) )
    	{
    		for( DocumentIdFieldName df : docFileFieldList )
    		{
    			TransactionDocument tranDoc = this.getTransactionDocumentByDocumentId(df.getDocumentId());
    			if ( tranDoc == null ) 
    				_logger.warn("getTransactionFiles() - Failed to find transaction document for document id: " + df.getDocumentId() + "; transactionId: " + id);
    			else 
    			{
        			if ( tranDoc.getId().equals(tf.getTransactionDocumentId()) && tf.getFieldName().equals(df.getFieldName()) ) 
        				list.add(tf);
    			}
    		}
    	}
    	
    	return list;
    }
    
    // Helper routine so we don't report missing fields for certain field types like radio buttons where the value is in the group
	private FieldTemplate getFieldTemplate(EsfName documentName, EsfName fieldName)
	{
		DocumentVersion docVer = getDocumentVersionByName(documentName);
		return docVer == null ? null : docVer.getFieldTemplate(fieldName);
	}
    
    public EsfValue getFieldValue(EsfName docName, EsfName fieldName, boolean fieldLimitedToDocument)
    {
    	if ( Literals.FIELD_SOURCE_TYPE_TRANSACTION.equals(docName.toString()) )
    	{
    		EsfValue value = getRecord().getValueByName(fieldName);
        	if ( value != null || fieldLimitedToDocument )
        		return value;
    	}
    	else
    	{
        	TransactionDocument tranDoc = getTransactionDocument(docName);
        	EsfValue value = (tranDoc == null) ? null : tranDoc.getRecord().getValueByName(fieldName);
        	if ( value != null || fieldLimitedToDocument )
        		return value;
    	}
    	// We didn't find it, so let's try our transaction record, then all documents
    	EsfValue value = getRecord().getValueByName(fieldName);
    	if ( value != null )
    		return value;
        for( TransactionDocument td : transactionDocumentMap.values() )
        {
        	value = td.getRecord().getValueByName(fieldName);
        	if ( value != null )
        		return value;
        }
        FieldTemplate ft = getFieldTemplate(docName,fieldName);
        if ( ft == null )
            _logger.debug("getFieldValue() - Failed to find field value (and no field template) for docName: " + docName + "; fieldName: " + fieldName + "; fieldLimitedToDocument: " + fieldLimitedToDocument);
        else if ( ! ft.isTypeRadioButton() )
            _logger.warn("getFieldValue() - Failed to find field value for docName: " + docName + "; fieldName: " + fieldName + "; fieldLimitedToDocument: " + fieldLimitedToDocument);
        return null;
    }
    public EsfValue[] getFieldValues(EsfName docName, EsfName fieldName, boolean fieldLimitedToDocument)
    {
    	if ( Literals.FIELD_SOURCE_TYPE_TRANSACTION.equals(docName.toString()) )
    	{
    		EsfValue[] values = getRecord().getValuesByName(fieldName);
        	if ( values != null || fieldLimitedToDocument )
        		return values;
    	}
    	else
    	{
        	TransactionDocument tranDoc = getTransactionDocument(docName);
        	EsfValue[] values = (tranDoc == null) ? null :  tranDoc.getRecord().getValuesByName(fieldName);
        	if ( values != null || fieldLimitedToDocument )
        		return values;
    	}
    	// We didn't find it, so let's try our transaction record, then all documents
    	EsfValue[] values = getRecord().getValuesByName(fieldName);
    	if ( values != null )
    		return values;
        for( TransactionDocument td : transactionDocumentMap.values() )
        {
        	values = td.getRecord().getValuesByName(fieldName);
        	if ( values != null )
        		return values;
        }
        _logger.warn("getFieldValues() - Failed to find field values for docName: " + docName + "; fieldName: " + fieldName + "; fieldLimitedToDocument: " + fieldLimitedToDocument);
        return null;
    }
    
    public EsfValue getFieldValue(EsfName docName, EsfPathName fieldPathName)
    {
    	if ( Literals.FIELD_SOURCE_TYPE_TRANSACTION.equals(docName.toString()) )
    		return getRecord().getValueByName(fieldPathName);
    	TransactionDocument tranDoc = getTransactionDocument(docName);
    	if ( tranDoc == null )
    	{
    		_logger.warn("getFieldValue() - Failed to find TransactionDocument; docName: " + docName + "; fieldPathName: " + fieldPathName);
    		return null;
    	}
    	EsfValue v = tranDoc.getRecord().getValueByName(fieldPathName);
    	if ( v == null )
    	{
        	if ( ! fieldPathName.hasEsfNameReservedPrefixInPathName() ) // don't want to report missing /ESF_* options we maintain about some fields 
        		_logger.warn("getFieldValue() - Failed to find field value; docName: " + docName + "; fieldPathName: " + fieldPathName);
    	}
    	return v;
    }
    public EsfValue getFieldValue(EsfName docName, EsfName fieldName)
    {
    	if ( Literals.FIELD_SOURCE_TYPE_TRANSACTION.equals(docName.toString()) )
    		return getRecord().getValueByName(fieldName);
    	TransactionDocument tranDoc = getTransactionDocument(docName);
    	if ( tranDoc == null )
    	{
    		_logger.warn("getFieldValue() - Failed to find TransactionDocument; docName: " + docName + "; fieldName: " + fieldName);
    		return null;
    	}
    	EsfValue v = tranDoc.getRecord().getValueByName(fieldName);
    	if ( v == null )
    	{
            FieldTemplate ft = getFieldTemplate(docName,fieldName);
            if ( ft == null )
                _logger.debug("getFieldValue() - Failed to find field value (and no field template); docName: " + docName + "; fieldName: " + fieldName,null);
            else if ( ! ft.isTypeRadioButton() )
                _logger.warn("getFieldValue() - Failed to find field value; docName: " + docName + "; fieldName: " + fieldName,null);
    	}
    	return v;
    }
    
    public EsfValue[] getFieldValues(EsfName docName, EsfPathName fieldPathName)
    {
    	if ( Literals.FIELD_SOURCE_TYPE_TRANSACTION.equals(docName.toString()) )
    		return getRecord().getValuesByName(fieldPathName);
    	TransactionDocument tranDoc = getTransactionDocument(docName);
    	if ( tranDoc == null )
    	{
    		_logger.warn("getFieldValues() - Failed to find TransactionDocument; docName: " + docName + "; fieldPathName: " + fieldPathName);
    		return null;
    	}
    	EsfValue[] v = tranDoc.getRecord().getValuesByName(fieldPathName);
    	if ( v == null )
    		_logger.warn("getFieldValues() - Failed to find field values; docName: " + docName + "; fieldPathName: " + fieldPathName);
    	return v;
    }
    public EsfValue[] getFieldValues(EsfName docName, EsfName fieldName)
    {
    	if ( Literals.FIELD_SOURCE_TYPE_TRANSACTION.equals(docName.toString()) )
    		return getRecord().getValuesByName(fieldName);
    	TransactionDocument tranDoc = getTransactionDocument(docName);
    	if ( tranDoc == null )
    	{
    		_logger.warn("getFieldValues() - Failed to find TransactionDocument; docName: " + docName + "; fieldName: " + fieldName);
    		return null;
    	}
    	EsfValue[] v = tranDoc.getRecord().getValuesByName(fieldName);
    	if ( v == null )
    		_logger.warn("getFieldValues() - Failed to find field values; docName: " + docName + "; fieldName: " + fieldName);
    	return v;
    }
    
    public void setFieldValue(EsfName docName, EsfName fieldName, EsfValue fieldValue, boolean canClone)
    {
    	if ( Literals.FIELD_SOURCE_TYPE_TRANSACTION.equals(docName.toString()) )
    		getRecord().addUpdate(fieldName,fieldValue,canClone);
    	else
    	{
        	TransactionDocument tranDoc = getTransactionDocument(docName);
        	if ( tranDoc == null )
        		_logger.warn("setFieldValue() - Failed to find TransactionDocument; docName: " + docName + "; fieldName: " + fieldName + "; fieldValue: " + fieldValue + "; canClone: " + canClone);
        	else 
        		tranDoc.getRecord().addUpdate(fieldName,fieldValue,canClone);
    	}
    }
    public void setFieldValue(EsfName docName, EsfName fieldName, EsfValue fieldValue)
    {
    	setFieldValue(docName,fieldName,fieldValue,true);
    }
    
    public void setFieldValue(EsfName docName, EsfPathName fieldPathName, EsfValue fieldValue, boolean canClone)
    {
    	if ( Literals.FIELD_SOURCE_TYPE_TRANSACTION.equals(docName.toString()) )
    		getRecord().addUpdate(fieldPathName,fieldValue,canClone);
    	else
    	{
        	TransactionDocument tranDoc = getTransactionDocument(docName);
        	if ( tranDoc == null )
        		_logger.warn("setFieldValue() - Failed to find TransactionDocument; docName: " + docName + "; fieldPathName: " + fieldPathName + "; fieldValue: " + fieldValue + "; canClone: " + canClone);
        	else 
        		tranDoc.getRecord().addUpdate(fieldPathName,fieldValue,canClone);
    	}
    }
    public void setFieldValue(EsfName docName, EsfPathName fieldPathName, EsfValue fieldValue)
    {
    	setFieldValue(docName,fieldPathName,fieldValue,true);
    }
    
    public void setFieldValues(EsfName docName, EsfName fieldName, EsfValue[] fieldValues, boolean canClone)
    {
    	if ( Literals.FIELD_SOURCE_TYPE_TRANSACTION.equals(docName.toString()) )
    		getRecord().addUpdate(fieldName,fieldValues,canClone);
    	else
    	{
        	TransactionDocument tranDoc = getTransactionDocument(docName);
        	if ( tranDoc == null )
        		_logger.warn("setFieldValues() - Failed to find TransactionDocument; docName: " + docName + "; fieldName: " + fieldName + "; fieldValues: " + fieldValues + "; canClone: " + canClone);
        	else 
        		tranDoc.getRecord().addUpdate(fieldName,fieldValues,canClone);
    	}
    }
    public void setFieldValues(EsfName docName, EsfName fieldName, EsfValue[] fieldValues)
    {
    	setFieldValues(docName,fieldName,fieldValues,true);
    }
    
    public Document getDocumentByName(EsfName documentName) 
    {
		PackageVersion packageVersion = getPackageVersion();
		for( EsfUUID docId : packageVersion.getDocumentIdList() )
		{
			Document document = Document.Manager.getById(docId);
			if ( document.getEsfName().equals(documentName) )
				return document;
		}
		_logger.warn("getDocumentByName() - Failed to find Document; documentName: " + documentName);
		return null;
    }
    public DocumentVersion getDocumentVersion(Document document)
    {
    	if ( document == null )
    	{
    		//_logger.debug("getDocumentVersion() - No document specified.",null); // 
    		_logger.debug("getDocumentVersion() - No document specified.");
    		return null;
    	}
    	DocumentVersion docVer;
    	TransactionDocument tranDoc = getTransactionDocumentByDocumentId(document.getId());
    	if ( tranDoc == null ) // We won't have a transaction document for the package document
        	docVer = doProductionResolve() ? document.getProductionDocumentVersion() : document.getTestDocumentVersion();
        else
        	docVer = DocumentVersion.Manager.getById(tranDoc.getDocumentVersionId());
    	if ( docVer == null ) 
    		_logger.warn("getDocumentVersion() - Failed to find DocumentVersion; document name: " + document.getEsfName() + "; document id: " + document.getId());
    	return docVer;
    }
    public DocumentVersion getDocumentVersionByName(EsfName documentName)
    {
    	Document document = getDocumentByName(documentName);
    	if ( document == null )
    	{
    		_logger.warn("getDocumentVersionByName() - No document found with name: " + documentName);
    		return null;
    	}
    	DocumentVersion docVer = getDocumentVersion(document);
    	if ( docVer == null )
    		_logger.warn("getDocumentVersionByName() - No document version found for document named: " + documentName);
    	return docVer;
    }
    
    public String getFieldSpecWithSubstitutions(User user, DocumentVersion currentDocumentVersion, String fieldSpecToExpand)
    {
    	return StringReplacement.replace(fieldSpecToExpand, user, this, currentDocumentVersion);
    }
    
    public String getUrlEncodedFieldSpecWithSubstitutions(User user, DocumentVersion currentDocumentVersion, String fieldSpecToExpand)
    {
    	return StringReplacement.replaceUrlEncoded(fieldSpecToExpand, user, this, currentDocumentVersion);
    }
    
    public List<Library> getAllDocumentLibraries()
    {
    	LinkedList<Library> allDocumentLibraries = new LinkedList<Library>();
    	
		for( TransactionDocument tranDoc : getAllTransactionDocuments() )
		{
			Document checkDoc = tranDoc.getDocument();
			if ( checkDoc != null )
			{
				Library checkDocLibrary = checkDoc.getLibrary();
				if ( checkDocLibrary != null && ! allDocumentLibraries.contains(checkDocLibrary) )
					allDocumentLibraries.add(checkDocLibrary);
			}
		}
			
		return allDocumentLibraries;
    }
    
	/**
	 * Finds the specified property name in the specified propertyset, if provided.
	 * @param propertySetName the name of the property set to look in; if null, all property sets are checked in alpha order
	 * @param propertyName the property name
	 * @param user if non-null, search the user's property sets first
	 * @param documentVersion if non-null, search the document version before the template library
	 * @return the EsfValue of the property (all properties initially are String types because the property GUI only supports them)
	 */
	public EsfValue getPropertyValue(EsfName propertySetName, EsfPathName propertyName, final User user, DocumentVersion documentVersion)
	{
		// As of 10/26/2015, we now look if user has a property set that overrides them all
		if ( user != null )
		{
			EsfValue value = user.getPropertyValue(propertySetName, propertyName, doProductionResolve(), isTest());
			if ( value != null )
				return value;
		}
		
		List<Library> searchedLibraries = new LinkedList<Library>();
		
		// First let's look in our brand library
		if ( hasBrandLibraryId() )
		{
			Library brandLibrary = getBrandLibrary();
			EsfValue value = brandLibrary.getPropertyValue(propertySetName, propertyName, doProductionResolve(), isTest());
			if ( value != null )
				return value;

			searchedLibraries.add(brandLibrary);
		}
		
		// Second, if we have a document version (and it's not our template library), otherwise we'll try the libraries the documents are in
		Library templateLibrary = Library.Manager.getTemplate();
		if ( documentVersion != null )
		{
			Library documentLibrary = documentVersion.getDocument().getLibrary();
			if ( ! templateLibrary.equals(documentLibrary) && ! searchedLibraries.contains(documentLibrary) )
			{
				EsfValue value = documentLibrary.getPropertyValue(propertySetName, propertyName, doProductionResolve(), isTest());
				if ( value != null )
					return value;

				searchedLibraries.add(documentLibrary);
			}
		}
		for( Library checkDocLibrary : getAllDocumentLibraries() )
		{
			if ( ! searchedLibraries.contains(checkDocLibrary) )
			{
				EsfValue value = checkDocLibrary.getPropertyValue(propertySetName, propertyName, doProductionResolve(), isTest());
				if ( value != null )
					return value;
				searchedLibraries.add(checkDocLibrary);
			}
		}

		// Finally, search the template library if it hasn't been searched yet...
		EsfValue value = ( searchedLibraries.contains(templateLibrary) ) ? null : templateLibrary.getPropertyValue(propertySetName, propertyName, doProductionResolve(), isTest());
		if ( value == null )
			_logger.warn("getPropertyValue() - No value found; propertySetName: " + propertySetName + "; propertyName: " + propertyName + "; documentVersion id: " + (documentVersion == null ? "null" : documentVersion.getId()));
		return value;
	}
	public EsfValue getPropertyValue(EsfName propertySetName, EsfPathName propertyName)
	{
		return getPropertyValue(propertySetName,propertyName,null,null);
	}
	public String getPropertyStringValue(EsfName propertySetName, EsfPathName propertyName)
	{
		EsfValue propValue = getPropertyValue(propertySetName,propertyName,null,null);
		if ( propValue == null )
			return null;
		return propValue.toPlainString();
	}
	
	
	
	public File getFile(EsfName fileName, EsfUUID fileId, DocumentVersion documentVersion)
	{
		// If we have a document version and it's defined inside the document, use it rather than any library brand.
		if ( documentVersion != null )
		{
			File file = File.Manager.getByName(documentVersion.getId(), fileName);
			if ( file != null )
				return file;
		}

		List<Library> searchedLibraries = new LinkedList<Library>();
		
		// First, let's first look through its brand library if present
		if ( hasBrandLibraryId() )
		{
			Library brandLibrary = getBrandLibrary();
			File file = File.Manager.getByName(brandLibrary.getId(), fileName);
			if ( file != null && file.isEnabled() )
			{
				if ( (doProductionResolve() && file.hasProductionVersion()) || ! doProductionResolve() )
					return file;
			}
			searchedLibraries.add(brandLibrary);
		}

		// Second, if we have a document version (and it's not our template library), otherwise we'll try the libraries the documents are in
		Library templateLibrary = Library.Manager.getTemplate();
		if ( documentVersion != null )
		{
			Library documentLibrary = documentVersion.getDocument().getLibrary();
			if ( ! templateLibrary.equals(documentLibrary) && ! searchedLibraries.contains(documentLibrary) )
			{
				File file = File.Manager.getByName(documentLibrary.getId(), fileName);
				if ( file != null && file.isEnabled() )
				{
					if ( (doProductionResolve() && file.hasProductionVersion()) || ! doProductionResolve() )
						return file;
				}
				searchedLibraries.add(documentLibrary);
			}
		}
		for( Library checkDocLibrary : getAllDocumentLibraries() )
		{
			if ( ! searchedLibraries.contains(checkDocLibrary) )
			{
				File file = File.Manager.getByName(checkDocLibrary.getId(), fileName);
				if ( file != null && file.isEnabled() )
				{
					if ( (doProductionResolve() && file.hasProductionVersion()) || ! doProductionResolve() )
						return file;
				}
				searchedLibraries.add(checkDocLibrary);
			}
		}
		
		// Lastly, check the template library if not yet done
		if ( ! searchedLibraries.contains(templateLibrary) )
		{
			File file = File.Manager.getByName(templateLibrary.getId(), fileName);
			if ( file != null && file.isEnabled() )
			{
				if ( (doProductionResolve() && file.hasProductionVersion()) || ! doProductionResolve() )
					return file;
			}
		}
		
		// Rats!  Okay, let's try using the fileId, if any, they gave us when it was originally programmed
		File file = (fileId == null) ? null : File.Manager.getById(fileId);
		if ( file == null )
			_logger.warn("getFile() - Failed to find File; fileName: " + fileName + "; fileId: " + fileId + "; documentVersion id: " + (documentVersion == null ? "null" : documentVersion.getId()));
		return file;
	}
	
	public FileVersion getFileVersion(EsfName fileName, DocumentVersion documentVersion)
	{
		File file = getFile(fileName,null,documentVersion);
		if ( file == null )
			return null;
		return doProductionResolve() ? file.getProductionFileVersion() : file.getTestFileVersion();
	}
	
	public Image getImage(EsfName imageName, EsfUUID imageId, DocumentVersion documentVersion)
	{
		// If we have a document version, check if the image is there since it overrides any brand library.
		if ( documentVersion != null )
		{
			Image image = Image.Manager.getByName(documentVersion.getId(), imageName);
			if ( image != null )
				return image;
		}
		
		List<Library> searchedLibraries = new LinkedList<Library>();
		
		// First, let's first look through its brand library if present
		if ( hasBrandLibraryId() )
		{
			Library brandLibrary = getBrandLibrary();
			Image image = Image.Manager.getByName(brandLibrary.getId(), imageName);
			if ( image != null && image.isEnabled() )
			{
				if ( (doProductionResolve() && image.hasProductionVersion()) || ! doProductionResolve() )
					return image;
			}
			searchedLibraries.add(brandLibrary);
		}

		// Second, if we have a document version (and it's not our template library), otherwise we'll try the libraries the documents are in
		Library templateLibrary = Library.Manager.getTemplate();
		if ( documentVersion != null )
		{
			Library documentLibrary = documentVersion.getDocument().getLibrary();
			if ( ! templateLibrary.equals(documentLibrary) && ! searchedLibraries.contains(documentLibrary) )
			{
				Image image = Image.Manager.getByName(documentLibrary.getId(), imageName);
				if ( image != null && image.isEnabled() )
				{
					if ( (doProductionResolve() && image.hasProductionVersion()) || ! doProductionResolve() )
						return image;
				}
				searchedLibraries.add(documentLibrary);
			}
		}
		for( Library checkDocLibrary : getAllDocumentLibraries() )
		{
			if ( ! searchedLibraries.contains(checkDocLibrary) )
			{
				Image image = Image.Manager.getByName(checkDocLibrary.getId(), imageName);
				if ( image != null && image.isEnabled() )
				{
					if ( (doProductionResolve() && image.hasProductionVersion()) || ! doProductionResolve() )
						return image;
				}
				searchedLibraries.add(checkDocLibrary);
			}
		}
		
		// Lastly, check the template library if not yet done
		if ( ! searchedLibraries.contains(templateLibrary) )
		{
			Image image = Image.Manager.getByName(templateLibrary.getId(), imageName);
			if ( image != null && image.isEnabled() )
			{
				if ( (doProductionResolve() && image.hasProductionVersion()) || ! doProductionResolve() )
					return image;
			}
		}
		
		// Rats!  Okay, let's try using the imageId, if any, they gave us when it was originally programmed
		Image image = (imageId == null) ? null : Image.Manager.getById(imageId);
		if ( image == null )
			_logger.warn("getImage() - Failed to find Image; imageName: " + imageName + "; imageId: " + imageId + "; documentVersion id: " + (documentVersion == null ? "null" : documentVersion.getId()));
		return image;
	}
	
	/**
	 * Finds the specified drop down name 
	 * @param dropdownName the drop down name
	 * @param documentVersion if non-null, search the document version before the template library
	 * @return the DropDown found or null
	 */
	public DropDown getDropDown(EsfName dropdownName, DocumentVersion documentVersion)
	{
		// If we have a document version, check if the dropdown is there and use instead of any brand library
		if ( documentVersion != null )
		{
			DropDown dropdown = DropDown.Manager.getByName(documentVersion.getId(), dropdownName);
			if ( dropdown != null )
				return dropdown;
		}
		
		List<Library> searchedLibraries = new LinkedList<Library>();
		
		// First let's look in our brand library
		if ( hasBrandLibraryId() )
		{
			Library brandLibrary = getBrandLibrary();
			DropDown dropdown = DropDown.Manager.getByName(brandLibrary.getId(), dropdownName);
			if ( dropdown != null && dropdown.isEnabled() )
			{
				if ( (doProductionResolve() && dropdown.hasProductionVersion()) || ! doProductionResolve() )
					return dropdown;
			}
	
			searchedLibraries.add(brandLibrary);
		}
		
		// Second, if we have a document version (and it's not our template library), otherwise we'll try the libraries the documents are in
		Library templateLibrary = Library.Manager.getTemplate();
		if ( documentVersion != null )
		{
			Library documentLibrary = documentVersion.getDocument().getLibrary();
			if ( ! templateLibrary.equals(documentLibrary) && ! searchedLibraries.contains(documentLibrary) )
			{
				DropDown dropdown = DropDown.Manager.getByName(documentLibrary.getId(), dropdownName);
				if ( dropdown != null )
					return dropdown;
				searchedLibraries.add(documentLibrary);
			}
		} 
		for( Library checkDocLibrary : getAllDocumentLibraries() )
		{
			if ( ! searchedLibraries.contains(checkDocLibrary) )
			{
				DropDown dropdown = DropDown.Manager.getByName(checkDocLibrary.getId(), dropdownName);
				if ( dropdown != null )
					return dropdown;
				searchedLibraries.add(checkDocLibrary);
			}
		}
	
		// Finally, search the template library if it hasn't been searched yet...
		if ( searchedLibraries.contains(templateLibrary) )
			return null;
		
		DropDown dropdown = DropDown.Manager.getByName(templateLibrary.getId(), dropdownName);
		if ( dropdown != null && dropdown.isEnabled() )
		{
			if ( (doProductionResolve() && dropdown.hasProductionVersion()) || ! doProductionResolve() )
				return dropdown;
		}
		_logger.warn("getDropDown() - Failed to find DropDown; dropdownName: " + dropdownName + "; documentVersion id: " + (documentVersion == null ? "null" : documentVersion.getId()));
		return null;
	}
	public DropDown getDropDown(EsfName dropdownName)
	{
		return getDropDown(dropdownName,null);
	}

	public DropDownVersion getDropDownVersion(EsfName dropdownName, DocumentVersion documentVersion)
	{
		DropDown dropdown = getDropDown(dropdownName,documentVersion);
		if ( dropdown == null )
			return null;
		DropDownVersion ddv = doProductionResolve() ? dropdown.getProductionDropDownVersion() : dropdown.getTestDropDownVersion();
		if ( ddv == null && documentVersion != null )
		{
			// If the dropdown is defined within the document, the test version is okay
			if ( dropdown.getContainerId().equals(documentVersion.getId()) )
				ddv = dropdown.getTestDropDownVersion();
		}
		return ddv;
	}
	public DropDownVersion getDropDownVersion(EsfName dropdownName)
	{
		return getDropDownVersion(dropdownName,null);
	}
	
	
	public EmailTemplate getEmailTemplate(EsfName emailName, Library documentLibrary)
	{
		List<Library> searchedLibraries = new LinkedList<Library>();
		
		// First, let's first look through its brand library if present
		if ( hasBrandLibraryId() )
		{
			Library brandLibrary = getBrandLibrary();
			EmailTemplate email = EmailTemplate.Manager.getByName(brandLibrary.getId(), emailName);
			if ( email != null && email.isEnabled() )
				return email;
			searchedLibraries.add(brandLibrary);
		}

		// Check the document's library if specified (and it's not our template library), otherwise we'll try the libraries the documents are in
		Library templateLibrary = Library.Manager.getTemplate();
		if ( documentLibrary != null && ! documentLibrary.equals(templateLibrary) )
		{
			if ( ! searchedLibraries.contains(documentLibrary) )
			{
				EmailTemplate email = EmailTemplate.Manager.getByName(documentLibrary.getId(), emailName);
				if ( email != null && email.isEnabled() )
					return email;
				searchedLibraries.add(documentLibrary);
			}
		} 
		for( Library checkDocLibrary : getAllDocumentLibraries() )
		{
			if ( ! searchedLibraries.contains(checkDocLibrary) )
			{
				EmailTemplate email = EmailTemplate.Manager.getByName(checkDocLibrary.getId(), emailName);
				if ( email != null && email.isEnabled() )
					return email;
				searchedLibraries.add(checkDocLibrary);
			}
		}
		
		// Lastly, check the template library if not yet done
		if ( ! searchedLibraries.contains(templateLibrary) )
		{
			EmailTemplate email = EmailTemplate.Manager.getByName(templateLibrary.getId(), emailName);
			if ( email != null && email.isEnabled() )
				return email;
		}
		
		_logger.warn("getEmailTemplate() - Failed to find EmailTemplate; emailName: " + emailName + "; documentLibrary pathname: " + (documentLibrary == null ? "null" : documentLibrary.getPathName()));
		return null;
	}
	

	public PartyTemplate getPartyTemplate(EsfName partyName, Library documentLibrary)
	{
		List<Library> searchedLibraries = new LinkedList<Library>();
		
		// First, let's first look through its brand library if present
		if ( hasBrandLibraryId() )
		{
			Library brandLibrary = getBrandLibrary();
			PartyTemplate party = PartyTemplate.Manager.getByName(brandLibrary.getId(),partyName);
			if ( party != null )
				return party;
			searchedLibraries.add(brandLibrary);
		}

		// Check the document's library if specified (and it's not our template library), otherwise we'll try the libraries the documents are in
		Library templateLibrary = Library.Manager.getTemplate();
		if ( documentLibrary != null && ! documentLibrary.equals(templateLibrary) )
		{
			if ( ! searchedLibraries.contains(documentLibrary) )
			{
				PartyTemplate party = PartyTemplate.Manager.getByName(documentLibrary.getId(),partyName);
				if ( party != null )
					return party;
				searchedLibraries.add(documentLibrary);
			}
		} 
		for( Library checkDocLibrary : getAllDocumentLibraries() )
		{
			if ( ! searchedLibraries.contains(checkDocLibrary) )
			{
				PartyTemplate party = PartyTemplate.Manager.getByName(checkDocLibrary.getId(),partyName);
				if ( party != null )
					return party;
				searchedLibraries.add(checkDocLibrary);
			}
		}
		
		// Lastly, check the template library if not yet done
		if ( ! searchedLibraries.contains(templateLibrary) )
		{
			PartyTemplate party = PartyTemplate.Manager.getByName(templateLibrary.getId(),partyName);
			if ( party != null )
				return party;
		}
		
		_logger.warn("getPartyTemplate() - Failed to find PartyTemplate; partyName: " + partyName + "; documentLibrary pathname: " + (documentLibrary == null ? "null" : documentLibrary.getPathName()));
		return null;
	}
	
	public TransactionParty getEsfReportsAccessTransactionParty()
	{
		return getTransactionParty(PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS);
	}

    /**
     * Non-standard routine for auto-creating the transaction party that goes with a newly created ESF_reports_access party in 
     * its package version.
     * @return the TransactionParty that mirrors the new package party
     */
    public TransactionParty _createViewOptionalEsfReportsAccessTransactionParty()
    {
    	TransactionParty tranParty = getEsfReportsAccessTransactionParty();
    	if ( tranParty != null )
    	{
    		_logger.warn("_createViewOnlyEsfReportsAccessTransactionParty() - Unexpectedly found existing ESF_reports_access transaction party for tranId: " + id);
    		return tranParty;
    	}
    	
    	PackageVersion pkgVersion = getPackageVersion();
    	if ( pkgVersion == null )
    	{
    		_logger.error("_createViewOnlyEsfReportsAccessTransactionParty() - Could not get package version for tranId: " + id);
    		return null;
    	}
    	
    	PackageVersionPartyTemplate pvpt = pkgVersion.getEsfReportsAccessPackageVersionPartyTemplate();
    	if ( pvpt == null )
    	{
    		pvpt = pkgVersion._createViewOptionalEsfReportsAccessPackageVersionPartyTemplate();
    		if ( pvpt == null )
    		{
        		_logger.error("_createViewOnlyEsfReportsAccessTransactionParty() - Could not create ESF_reports_access package party for tranId: " + id + 
        				"; packageVersionId: " + pkgVersion.getId());
    			return null;
    		}
    	}
    	
    	List<EsfUUID> docIdList = pkgVersion.getDocumentIdList();
    	
		tranParty = TransactionParty.Manager.createNew(this, pvpt);
		
		short documentNumber = 1;
    	for( EsfUUID docId : docIdList )
    	{
    		if ( pvpt.isPartyToDocument(docId) ) 
    		{
    			TransactionDocument tranDoc = getTransactionDocumentByDocumentId(docId);
    			TransactionPartyDocument tranPartyDoc = TransactionPartyDocument.Manager.createNew(tranParty, tranDoc, documentNumber++);
    			if ( pvpt.isViewOptional(docId) )
    				tranPartyDoc.setStatusViewOptional();
    			tranParty._addTransactionPartyDocumentDuringCreation(tranPartyDoc);
    		}
    	}
		
		transactionPartyMap.put(pvpt.getEsfName(), tranParty);
		
		tranParty.getCurrentAssignment().setStatusActivated();
		
		return save() ? getEsfReportsAccessTransactionParty() : null; 
    }
    
    
    public boolean setTimer(Connection con, String timerName, EsfDateTime expireTimestamp)
    {
    	try
    	{
        	TransactionTimer timer = TransactionTimer.Manager.getByNameForTransaction(con,id,timerName);
        	if ( timer == null )
        		timer = TransactionTimer.Manager.createNew(id, timerName, expireTimestamp);
        	else
        		timer.setExpireTimestamp(expireTimestamp);
        	return timer.save(con);
    	}
    	catch(SQLException e)
    	{
    		_logger.warn("setTimer() exception for transactionId: " + id + "; timerName: " + timerName + "; expireTimestamp: " + expireTimestamp,e);
    	}
    	return false;
    }
    
    public boolean deleteTimer(Connection con, String timerName)
    {
    	try
    	{
        	TransactionTimer timer = TransactionTimer.Manager.getByNameForTransaction(con,id,timerName);
        	if ( timer != null )
        		return timer.delete(con);
    	}
    	catch(SQLException e)
    	{
    		_logger.warn("deleteTimer() exception for transactionId: " + id + "; timerName: " + timerName,e);
    	}
    	return false;
    }
    
    public int deleteAllTimers(Connection con)
    {
    	try
    	{
        	return TransactionTimer.Manager.deleteAllForTransaction(con,id);
    	}
    	catch(SQLException e)
    	{
    		_logger.warn("deleteAllTimers() exception for transactionId: " + id,e);
    		return 0;
    	}
    }

	
	public void logActionBasic(Connection con, String message)
		throws SQLException
	{
		if ( isTemporary )
			_logger.info("logActionBasic(temp) tran id: "+ id + " - " + message);
		else
			Application.getInstance().getActivityLog().logTransactionActionBasic(con, id, message);
	}
	
	public void logActionDetail(Connection con, String message)
		throws SQLException
	{
		if ( isTemporary )
			_logger.info("logActionDetail(temp) tran id: "+ id + " - " + message);
		else
			Application.getInstance().getActivityLog().logTransactionActionDetail(con, id, message);
	}

	public void logGeneral(Connection con, String message)
		throws SQLException
	{
		if ( isTemporary )
			_logger.info("logGeneral(temp) tran id: "+ id + " - " + message);
		else
			Application.getInstance().getActivityLog().logTransactionGeneral(con, id, message);
	}
	public void logGeneral(String message)
	{
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
        	logGeneral(con, message);
        	con.commit();
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"logGeneral() on id: " + id + "; tranType: " + tranType + "; message: " + message);
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
	}

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof Transaction )
            return getId().equals(((Transaction)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(Transaction t)
    {
    	return getLastUpdatedTimestamp().compareTo(t.getLastUpdatedTimestamp());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
		// We never save temporary transactions (used for testing), but don't want any save code to be upset about it
		if ( isTemporary )
			return true;
		
    	_logger.debug("save(con,user) on id: " + id + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	lastUpdatedByUserId = (user == null) ? null : user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
        	if ( transactionFilesToDeleteOnSave != null )
        	{
            	for( TransactionFile tranFile : transactionFilesToDeleteOnSave )
            		tranFile.delete(con);
            	transactionFilesToDeleteOnSave.clear();
        	}
        	
        	for( TransactionFile tranFile : transactionFileList )
        		tranFile.save(con);
        	
        	// Save these fields before we save the transaction document data since we only save report fields if the data changed
        	// and after saving the data we reset the "data changed" attribute.
            saveReportFields(con,true); 
            
            for( TransactionDocument tranDoc : transactionDocumentMap.values() )
            	tranDoc.save(con);
            
            boolean hasActiveParty = false;
            boolean hasActivePartyWithEmailOrToDoPermission = false;
            for( TransactionParty tranParty : transactionPartyMap.values() )
            {
            	tranParty.save(con);
            	if ( tranParty.isActive() )
            	{
            		hasActiveParty = true;
            		if ( tranParty.getCurrentAssignment().hasEmailAddress() )
            			hasActivePartyWithEmailOrToDoPermission = true;
            		else if ( tranParty.hasTodoGroupId() )
            		{
            			Group toDoGroup = Group.Manager.getById(tranParty.getTodoGroupId());
            			if ( toDoGroup == null )
            			{
            				logGeneral(con, "Active package party '" + tranParty.getPackageVersionPartyTemplate().getEsfName() + "' has unknown To Do Group id: " + tranParty.getTodoGroupId());
            				_logger.error("save(con,user) - Active package party: " + tranParty.getPackageVersionPartyTemplate().getEsfName() + "; has unknown To Do Group id: " + tranParty.getTodoGroupId());
            			}
            			else if ( toDoGroup.isDisabled() )
            			{
            				logGeneral(con, "Active package party '" + tranParty.getPackageVersionPartyTemplate().getEsfName() + "' specifies a DISABLED To Do Group: " + toDoGroup.getPathName());
            				_logger.warn("save(con,user) - Active package party: " + tranParty.getPackageVersionPartyTemplate().getEsfName() + "; specifies DISABLED To Do Group: " + toDoGroup.getPathName() + "; group id: " + tranParty.getTodoGroupId());
            			}
            			else if ( toDoGroup.hasAnActiveMemberUser() )
            			{
            				hasActivePartyWithEmailOrToDoPermission = true;
            			}
            			else
            			{
            				logGeneral(con, "Active package party '" + tranParty.getPackageVersionPartyTemplate().getEsfName() + "' specifies To Do Group '" + toDoGroup.getPathName() + "' that has no enabled member users.");
            				_logger.warn("save(con,user) - Active package party: " + tranParty.getPackageVersionPartyTemplate().getEsfName() + "; has To Do Group with no enabled members: " + toDoGroup.getPathName() + "; group id: " + tranParty.getTodoGroupId());
            			}
            		}
            	}
            }

            // Check for stalled transactions or clear stalled if now has an active user or is now done.
            if ( isStalled() )
            {
            	if ( hasActivePartyWithEmailOrToDoPermission )
            	{
            		clearStalled();
            		logGeneral(con, "Stalled transaction has resumed with an active party with an email address or To Do permission.");
            	}
            	else if ( isEndState() )
            	{
            		clearStalled();
            		logGeneral(con, "Stalled transaction has reached the end state: " + getStatusLabel());
            	}
            }
            else 
            {
            	if ( ! isEndState() )
            	{
                	if ( ! hasActivePartyWithEmailOrToDoPermission )
                	{
                    	setStalled();
                    	if ( hasActiveParty )
                    		logGeneral(con, "Transaction has stalled because the active party does not specify an email address or valid To Do Group (enabled group with at least one enabled member user) even though the transaction has not yet ended. Current status: " + getStatusLabel());
                    	else
                    		logGeneral(con, "Transaction has stalled because it has no active parties even though the transaction has not yet ended. Current status: " + getStatusLabel());
                	}
            	}
            }
            
            // We clear the auto-cancel timestamp if the transaction has ended (completed or canceled).
            if ( isEndState() && hasCancelTimestamp() )
            {
            	if ( isCompleted() )
            		logGeneral(con, "Cleared the completed transaction's auto-cancel timestamp that was set to: " + getCancelTimestamp().toDateTimeMsecString());
            	else if ( isCanceled() )
            		logGeneral(con, "Cleared the canceled transaction's auto-cancel timestamp that was set to: " + getCancelTimestamp().toDateTimeMsecString());
            	clearCancelTimestamp();
            }
            
            record.save(con);
            
            if ( doInsert() )
            {
            	createdByUserId = (user == null) ? createdByUserId : user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
            	
            	stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_transaction (id,transaction_template_id,package_id,package_version_id,brand_library_id,tran_type," +
                	"created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,stall_timestamp,expire_timestamp,cancel_timestamp," + 
                	"status,status_text,cancel_retention_spec,record_id) " + 
                	"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(transactionTemplateId);
                stmt.set(packageId);
                stmt.set(packageVersionId);
                stmt.set(brandLibraryId);
                stmt.set(tranType);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(stallTimestamp);
                stmt.set(expireTimestamp);
                stmt.set(cancelTimestamp);
                stmt.set(status);
                stmt.set(statusText);
                stmt.set(cancelRetentionSpec);
                stmt.set(record.getId());
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id);
                    return false;
                }
                
                TransactionParty.Manager.setTransactionStatus(con, id, status);
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.add(this);
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new transaction; type: " + getTranType()); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new transaction; type: " + getTranType()); 
                }
                
                return true;
            }
            
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_transaction SET status=?,status_text=?,last_updated_timestamp=?,last_updated_by_user_id=?,stall_timestamp=?,expire_timestamp=?,cancel_timestamp=?,cancel_retention_spec=? WHERE id=?"
                						   );
                stmt.set(status);
                stmt.set(statusText);
            	stmt.set(lastUpdatedTimestamp);
            	stmt.set(lastUpdatedByUserId);
                stmt.set(stallTimestamp);
                stmt.set(expireTimestamp);
                stmt.set(cancelTimestamp);
                stmt.set(cancelRetentionSpec);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for transaction id: " + id + "; type: " + getTranType()); 
                }

                TransactionParty.Manager.setTransactionStatus(con, id, status); 
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	String tranTemplateName = getTransactionTemplate().getPathName().toString();
            	user.logConfigChange(con, "Updated transaction id: " + id + "; type: " + tranType + "; status: " + status + "; statusText: " + statusText + "; template: " + tranTemplateName); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated transaction id: " + id + "; type: " + tranType + "; status: " + status + "; template: " + tranTemplateName);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; tranType: " + tranType + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    public void saveReportFields(Connection con, boolean onlyIfDataChanged)
    	throws SQLException
    {
    	// Especially test transactions run the risk of a package version having been deleted since it was started. This should be blocked
    	// as of version 13.4.13 when we block deleting them if they are in use in a transaction.
    	// Regardless, if there's no package version, we cannot reload any of its fields so we just skip it.
    	PackageVersion pkgVersion = getPackageVersion();
    	if ( pkgVersion == null )
    	{
			_logger.error("saveReportFields(con) - Failed to find package version with id: " + getPackageVersionId() + "; linked to by transaction id: " + id + "; tran type: " + getTranType());
			return;
    	}
    	
    	for( PackageVersionReportField pvrf : pkgVersion.getPackageVersionReportFieldList() )
    	{    		
    		ReportFieldTemplate reportFieldTemplate = pvrf.getReportFieldTemplate();
    		if ( reportFieldTemplate == null )
    		{
    			_logger.error("saveReportFields(con) - Failed to find report field template with id: " + pvrf.getReportFieldTemplateId() + 
    					"; transaction document for document id: " + pvrf.getDocumentId() + "; for field: " + pvrf.getFieldName() + 
    					"; PackageVersionReportField id: " + pvrf.getId() + "; id: " + id + "; tran type: " + getTranType());
    			continue;
    		}
    		
    		TransactionDocument tranDoc = getTransactionDocumentByDocumentId(pvrf.getDocumentId());
    		if ( tranDoc == null )
    		{
    			_logger.error("saveReportFields(con) - Failed to find transaction document for document id: " + pvrf.getDocumentId() + "; for field: " + pvrf.getFieldName() + 
    					"; PackageVersionReportField id: " + pvrf.getId() + "; id: " + id + "; tran type: " + getTranType());
    			continue;
    		}
    		
    		// If the data has not changed, there's nothing we need to save now.
    		Record tranDocData = tranDoc.getRecord();
    		if ( onlyIfDataChanged && ! tranDocData.hasChanged() )
    			continue;
    		
    		EsfValue value = tranDocData.getValueByName(pvrf.getFieldName());
    		
    		// We can save it if we can map it...
    		if ( reportFieldTemplate.isFieldTypeStringOrRelated() )
    		{
    			String stringValue = null;
    			
    			if ( value != null && ! value.isNull() )
    			{
	        		if ( reportFieldTemplate.isFieldTypeString() )
	        		{
	        			stringValue = value.toPlainString();
	        			if ( EsfString.isBlank(stringValue) ) // we'll treat an empty string as a null value for the DB
	        				stringValue = null;
	        		}
	        		else if ( reportFieldTemplate.isFieldTypeNumericOnly() )
	        			stringValue = EsfString.getOnlyNumeric(value.toPlainString());
	        		else
	        			stringValue = EsfString.getOnlyAlphaNumeric(value.toPlainString());
    			}
    			saveTranReportFieldString(con,reportFieldTemplate.getId(),stringValue,doInsert());
    		}
    		else if ( reportFieldTemplate.isFieldTypeInteger() )
    		{
    			EsfInteger integerValue = null;
    			
    			if ( value != null && ! value.isNull() )
    			{
    				if ( value instanceof EsfInteger )
    					integerValue = (EsfInteger)value;
    				else if ( value instanceof EsfString )
    					integerValue = new EsfInteger( ((EsfString)value).getOnlyNumericString() );
    				else if ( value instanceof EsfDecimal )
    					integerValue = new EsfInteger( ((EsfDecimal)value).toPlainString(0) );
    				else
    					_logger.warn("saveReportFields(con) - Report field for document id: " + pvrf.getDocumentId() + "; for field: " + pvrf.getFieldName() + 
    								 "; PackageVersionReportField id: " + pvrf.getId() + "; is type integer, but value is unexpected type: " + value.getType() + 
    								 "; id: " + id + "; tran type: " + getTranType());
    			}
    			saveTranReportFieldLong(con,reportFieldTemplate.getId(),integerValue,doInsert());
    		}
    		else if ( reportFieldTemplate.isFieldTypeDecimal() )
    		{
    			EsfDecimal numericValue = null;
    			
    			if ( value != null && ! value.isNull() )
    			{
    				if ( value instanceof EsfDecimal )
    					numericValue = (EsfDecimal)value;
    				else if ( value instanceof EsfString )
    				{
    					String numericString = EsfDecimal.normalize( ((EsfString)value).toPlainString() );
    					numericValue = new EsfDecimal(numericString);
    				}
    				else if ( value instanceof EsfInteger )
    					numericValue = new EsfDecimal( ((EsfInteger)value).toPlainString() );
    				else
    					_logger.warn("saveReportFields(con) - Report field for document id: " + pvrf.getDocumentId() + "; for field: " + pvrf.getFieldName() + 
    								 "; PackageVersionReportField id: " + pvrf.getId() + "; is type decimal, but value is unexpected type: " + value.getType() + 
    								 "; id: " + id + "; tran type: " + getTranType());
    			}
    			saveTranReportFieldNumeric(con,reportFieldTemplate.getId(),numericValue,doInsert());
    		}
    		else if ( reportFieldTemplate.isFieldTypeDate() )
    		{
    			EsfDate dateValue = null;
    			
    			if ( value != null && ! value.isNull() )
    			{
    				if ( value instanceof EsfDate )
    					dateValue = (EsfDate)value;
    				else if ( value instanceof EsfString )
    					dateValue = EsfDate.CreateGuessFormat( ((EsfString)value).toPlainString() );
    				else if ( value instanceof EsfDateTime )
    					dateValue = new EsfDate( ((EsfDateTime)value).toDate() );
    				else
    					_logger.warn("saveReportFields(con) - Report field for document id: " + pvrf.getDocumentId() + "; for field: " + pvrf.getFieldName() + 
    								 "; PackageVersionReportField id: " + pvrf.getId() + "; is type decimal, but value is unexpected type: " + value.getType() + 
    								 "; id: " + id + "; tran type: " + getTranType());
    			}
    			saveTranReportFieldDate(con,reportFieldTemplate.getId(),dateValue,doInsert());
    		}
    		else if ( reportFieldTemplate.isFieldTypeFile() )
    		{
    			List<TransactionFile> tranFiles = getAllTransactionFilesForDocumentField(tranDoc.getId(), pvrf.getFieldName());
    			saveTranReportFieldTranFileId(con,reportFieldTemplate.getId(),tranFiles);
    		}
    		else
    		{
    			_logger.error("saveReportFields(con) - Unexpected report field type: " + reportFieldTemplate.getFieldType() + 
    					"; for document id: " + pvrf.getDocumentId() + 
    					"; for field: " + pvrf.getFieldName() + "; PackageVersionReportField id: " + pvrf.getId() + 
    					"; id: " + id + "; tran type: " + getTranType());
    		}
    	}
    }
    
    void saveTranReportFieldString(Connection con, EsfUUID reportFieldTemplateId, String value, boolean dbInsert) 
    	throws SQLException
    {
	    EsfPreparedStatement stmt = null;
	    
	    value = EsfString.ensureLength(value, Literals.REPORT_FIELD_STRING_MAX_LENGTH);

	    try
	    {
        	_logger.debug("saveTranReportFieldString(con) - tran id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value);
	        if ( dbInsert )
	        {
	            stmt = new EsfPreparedStatement(con, 
	            	"INSERT INTO esf_tran_report_field_string (transaction_id,report_field_template_id,field_value) VALUES (?,?,?)");
	            stmt.set(id);
	            stmt.set(reportFieldTemplateId);
	            stmt.set(value);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            	_logger.warn("saveTranReportFieldString(con) - Insert failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value);
	        }
	        else
	        {
	            // We assume we'll update it instead
	            stmt = new EsfPreparedStatement( con, 
	            		"UPDATE esf_tran_report_field_string SET field_value=? WHERE transaction_id=? AND report_field_template_id=?"
	            						   		);
	            stmt.set(value);
	            stmt.set(id);
	            stmt.set(reportFieldTemplateId);
	            int num = stmt.executeUpdate();
	            if ( num != 1 ) 
	            {
	            	_logger.warn("saveTranReportFieldString(con) - Update failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value + "; retry with INSERT");
	            	saveTranReportFieldString(con,reportFieldTemplateId,value,true);
	            }
	        }
	    }
	    catch( SQLException e )
	    {
        	_logger.sqlerr(e,"saveTranReportFieldString(con) - Update failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value + "; dbInsert: " + dbInsert);
	    	throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
    }
    
    void saveTranReportFieldLong(Connection con, EsfUUID reportFieldTemplateId, EsfInteger value, boolean dbInsert) 
		throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	
	    try
	    {
        	_logger.debug("saveTranReportFieldLong(con) - tran id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value);
	        if ( dbInsert )
	        {
	            stmt = new EsfPreparedStatement(con, 
	            	"INSERT INTO esf_tran_report_field_long (transaction_id,report_field_template_id,field_value) VALUES (?,?,?)");
	            stmt.set(id);
	            stmt.set(reportFieldTemplateId);
	            stmt.set(value);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            	_logger.warn("saveTranReportFieldLong(con) - Insert failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value);
	        }
	        else
	        {
	            // We assume we'll update it instead
	            stmt = new EsfPreparedStatement( con, 
	            		"UPDATE esf_tran_report_field_long SET field_value=? WHERE transaction_id=? AND report_field_template_id=?"
	            						   		);
	            stmt.set(value);
	            stmt.set(id);
	            stmt.set(reportFieldTemplateId);
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("saveTranReportFieldLong(con) - Update failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value + "; retry with INSERT");
	            	saveTranReportFieldLong(con,reportFieldTemplateId,value,true);
	            }
	        }
	    }
	    catch( SQLException e )
	    {
	    	_logger.sqlerr(e,"saveTranReportFieldLong(con) - Update failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value + "; dbInsert: " + dbInsert);
	    	throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

    void saveTranReportFieldNumeric(Connection con, EsfUUID reportFieldTemplateId, EsfDecimal value, boolean dbInsert) 
		throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	
	    try
	    {
	       	_logger.debug("saveTranReportFieldNumeric(con) - tran id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value);
	        if ( dbInsert )
	        {
	            stmt = new EsfPreparedStatement(con, 
	            	"INSERT INTO esf_tran_report_field_numeric (transaction_id,report_field_template_id,field_value) VALUES (?,?,?)");
	            stmt.set(id);
	            stmt.set(reportFieldTemplateId);
	            stmt.set(value);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            	_logger.warn("saveTranReportFieldNumeric(con) - Insert failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value);
	        }
	        else
	        {
	            // We assume we'll update it instead
	            stmt = new EsfPreparedStatement( con, 
	            		"UPDATE esf_tran_report_field_numeric SET field_value=? WHERE transaction_id=? AND report_field_template_id=?"
	            						   		);
	            stmt.set(value);
	            stmt.set(id);
	            stmt.set(reportFieldTemplateId);
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("saveTranReportFieldNumeric(con) - Update failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value + "; retry with INSERT");
	            	saveTranReportFieldNumeric(con,reportFieldTemplateId,value,true);
	            }
	        }
	    }
	    catch( SQLException e )
	    {
	    	_logger.sqlerr(e,"saveTranReportFieldNumeric(con) - Update failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value + "; dbInsert: " + dbInsert);
	    	throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

    void saveTranReportFieldDate(Connection con, EsfUUID reportFieldTemplateId, EsfDate value, boolean dbInsert) 
		throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	
	    try
	    {
	       	_logger.debug("saveTranReportFieldDate(con) - tran id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value);
	        if ( dbInsert )
	        {
	            stmt = new EsfPreparedStatement(con, 
	            	"INSERT INTO esf_tran_report_field_date (transaction_id,report_field_template_id,field_value) VALUES (?,?,?)");
	            stmt.set(id);
	            stmt.set(reportFieldTemplateId);
	            stmt.set(value);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            	_logger.warn("saveTranReportFieldDate(con) - Insert failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value);
	        }
	        else
	        {
	            // We assume we'll update it instead
	            stmt = new EsfPreparedStatement( con, 
	            		"UPDATE esf_tran_report_field_date SET field_value=? WHERE transaction_id=? AND report_field_template_id=?"
	            						   		);
	            stmt.set(value);
	            stmt.set(id);
	            stmt.set(reportFieldTemplateId);
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("saveTranReportFieldDate(con) - Update failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value + "; retry with INSERT");
	            	saveTranReportFieldDate(con,reportFieldTemplateId,value,true);
	            }
	        }
	    }
	    catch( SQLException e )
	    {
	    	_logger.sqlerr(e,"saveTranReportFieldDate(con) - Update failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; value: " + value + "; dbInsert: " + dbInsert);
	    	throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

    void saveTranReportFieldTranFileId(Connection con, EsfUUID reportFieldTemplateId, List<TransactionFile> tranFiles) 
		throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	
	    try
	    {
            stmt = new EsfPreparedStatement(con, 
        		"DELETE FROM esf_tran_report_field_tranfileid WHERE transaction_id=? AND report_field_template_id=?");
            stmt.set(id);
            stmt.set(reportFieldTemplateId);
            stmt.executeUpdate();
            
            for( TransactionFile tf : tranFiles )
            {
    	       	_logger.debug("saveTranReportFieldTranFileId(con) - tran id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; tranFileId: " + tf.getId());
                stmt = new EsfPreparedStatement(con, 
            		"INSERT INTO esf_tran_report_field_tranfileid (transaction_id,report_field_template_id,tran_file_id) VALUES (?,?,?)");
                stmt.set(id);
                stmt.set(reportFieldTemplateId);
                stmt.set(tf.getId());
            
                int num = stmt.executeUpdate();
                if ( num != 1 )
                	_logger.warn("saveTranReportFieldTranFileId(con) - Insert failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId + "; tran_file_id: " + tf.getId());
            }
	    }
	    catch( SQLException e )
	    {
	    	_logger.sqlerr(e,"saveTranReportFieldTranFileId(con) - Failed for id: " + id + "; reportFieldTemplateId: " + reportFieldTemplateId);
	    	throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
		// We never save or delete temporary transactions (used for testing), but don't want any delete code to be upset about it
		if ( isTemporary )
			return true;
    	
    	_logger.debug("delete(con) on id: " + id + "; tranType: " + tranType + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks 
        if ( ! checkReferential(con,errors) )
            return false;

        EsfPreparedStatement stmt = null;
        
        try
        {
        	// Delete all tran report fields regardless of type
        	deleteTranReportFields(con);
        	
        	// Delete all timers
        	deleteAllTimers(con);
        	
        	// Delete all of our attached files
        	for( TransactionFile tranFile : transactionFileList )
        		tranFile.delete(con);
        	
        	// Delete all of our documents
            for( TransactionDocument tranDoc : transactionDocumentMap.values() )
            	tranDoc.delete(con,errors);
            
        	// Delete all of our parties
            for( TransactionParty tranParty : transactionPartyMap.values() )
            	tranParty.delete(con,errors);
            
            // Delete the transaction
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_transaction WHERE id=?");
            stmt.set(id);
            int num = stmt.executeUpdate();
            if ( num == 0 )
            	_logger.warn("delete(con) - Failed to find esf_transaction database row to delete with id: " + id + "; tranType: " + tranType + "; doInsert(): " + doInsert());
            
            // Delete all outbound emails associated with this transaction
            OutboundEmailMessage.Manager.deleteAllForTransaction(con, id);
            
            // Delete all HTTP Sends associated with this transaction
            HttpSendRequest.Manager.deleteAllForTransaction(con, id);
            
            // Delete all transaction activity
            Application.getInstance().getActivityLog().deleteAllForTransaction(con, id);
            
            record.delete(con);
        	
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted transaction id: " + id + "; tranType " + getTranType() + "; status; " + getStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted transaction id: " + id + "; tranType " + getTranType() + "; status; " + getStatus());
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on id: " + id + "; tranType: " + tranType + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
    void deleteTranReportFields(Connection con) 
		throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	
	    try
	    {
            stmt = new EsfPreparedStatement( con, "DELETE FROM esf_tran_report_field_string WHERE transaction_id=?" );
            stmt.set(id);
            stmt.executeUpdate();
            stmt.close();
            
            stmt = new EsfPreparedStatement( con, "DELETE FROM esf_tran_report_field_long WHERE transaction_id=?" );
            stmt.set(id);
            stmt.executeUpdate();
            stmt.close();

            stmt = new EsfPreparedStatement( con, "DELETE FROM esf_tran_report_field_numeric WHERE transaction_id=?" );
            stmt.set(id);
            stmt.executeUpdate();
            stmt.close();

            stmt = new EsfPreparedStatement( con, "DELETE FROM esf_tran_report_field_date WHERE transaction_id=?" );
            stmt.set(id);
            stmt.executeUpdate();
	    }
	    catch( SQLException e )
	    {
	    	_logger.sqlerr(e,"deleteTranReportFields(con) - Detail failed for id: " + id);
	    	throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}
   
   
	public void updateApi(Connection con, final User updatedBy, HttpServletRequest request, TransactionDocument limitTranDoc, TransactionParty tranParty)
		throws EsfException, SQLException
	{
		if ( updatedBy == null )
			throw new EsfException("Update API requires authorized user");
		if ( request == null )
			throw new EsfException("Update API requires HTTP request");

		addUpdateTransactionUpdateApiRequestData(updatedBy, request);
	   	    	
		if ( limitTranDoc != null )
		{
			limitTranDoc.updateApi(con, this, tranParty);
		}
		else
		{
   	    	for( TransactionDocument tranDoc : getAllTransactionDocuments() )
   	    	{
   	    		tranDoc.updateApi(con, this, tranParty);
   	    	}
		}
	}


   	public static class Manager
   	{
   	    private static TransactionCache cache = new TransactionCache();
   	    
   		public static final Transaction getById(Connection con, EsfUUID id) 
			throws SQLException
		{
   			return getById(con,id,true);
		}
   		
   		protected static Transaction getById(Connection con, EsfUUID id, boolean checkCache) 
			throws SQLException
		{
   			if ( checkCache )
   			{
   	   	    	Transaction transaction = cache.getById(id);
   	   	    	if ( transaction != null )
   	   	    		return transaction;
   			}
   			
	        EsfPreparedStatement stmt = null;
	        
	        try
	        {
	        	stmt = new EsfPreparedStatement( con,
	        			"SELECT transaction_template_id,package_id,package_version_id,brand_library_id," +
	        			"created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,stall_timestamp," +
	        			"expire_timestamp,cancel_timestamp,cancel_retention_spec,tran_type,status,status_text,record_id " +
	        			"FROM esf_transaction WHERE id = ?"
	        									);
	        	stmt.set(id);
	        	
	        	EsfResultSet rs = stmt.executeQuery();
	        	if ( rs.next() )
	        	{
		            EsfUUID transactionTemplateId = rs.getEsfUUID();
		            EsfUUID packageId = rs.getEsfUUID();
		            EsfUUID packageVersionId = rs.getEsfUUID();
		            EsfUUID brandLibraryId = rs.getEsfUUID();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            EsfDateTime stallTimestamp = rs.getEsfDateTime();
		            EsfDateTime expireTimestamp = rs.getEsfDateTime();
		            EsfDateTime cancelTimestamp = rs.getEsfDateTime();
		            String cancelRetentionSpec = rs.getString();
		            String tranType = rs.getString();
		            String status = rs.getString();
		            String statusText = rs.getString();
		            EsfUUID recordId = rs.getEsfUUID();
		            
		            Record record = Record.Manager.getById(con, recordId);
		            if ( record == null )
		            {
		            	_logger.error("SERIOUS-ERROR Manager.getById() id: " + id + "; Could not load Record for transaction data"); 
		            	return null;
		            }
		            
		            List<TransactionFile> transactionFileList = TransactionFile.Manager.getAllForTransaction(con, id);
	
		        	Transaction transaction = new Transaction(id,transactionTemplateId,packageId,packageVersionId,brandLibraryId,tranType,
		        									createdTimestamp,createdByUserId,lastUpdatedTimestamp,lastUpdatedByUserId,
		        									stallTimestamp,expireTimestamp,cancelTimestamp,cancelRetentionSpec,status,statusText,record,transactionFileList);
		        	
		        	List<TransactionDocument> tranDocumentList = TransactionDocument.Manager.getAllByTransactionId(id);
		        	for( TransactionDocument tranDocument : tranDocumentList )
		        	{
		        		Document doc = tranDocument.getDocument();
		        		if ( doc != null )
		        			transaction.transactionDocumentMap.put(tranDocument.getDocument().getEsfName(), tranDocument);
		        		else
		        		{
		        			_logger.warn("SERIOUS-ERROR: Manager.getById() - id: " + id + "; checkCache: " + checkCache + "; failed to find document for tranDocument id: " +
		        					tranDocument.getId() + "; " + tranDocument.getDocumentVersionId());
		        			transaction.transactionDocumentMap.put(tranDocument.getDocumentId().toEsfName(), tranDocument);
		        		}
		        	}
		        	
		        	List<TransactionParty> tranPartyList = TransactionParty.Manager.getAllByTransactionId(id);
		        	for( TransactionParty tranParty : tranPartyList )
		        	{
		        		PackageVersionPartyTemplate packageVersionPartyTemplate = PackageVersionPartyTemplate.Manager.getById(tranParty.getPackageVersionPartyTemplateId());
		        		if ( packageVersionPartyTemplate == null )
		        			_logger.warn("Manager.getById() - id: " + id + "; checkCache: " + checkCache + "; could not find package party for tranParty id: " + tranParty.getId() + "; tranParty package party id: " + tranParty.getPackageVersionPartyTemplateId());
		        		else
		        			transaction.transactionPartyMap.put(packageVersionPartyTemplate.getEsfName(), tranParty);
		        	}
		        	
		        	transaction.setLoadedFromDb();
		            cache.add(transaction);
		            
	   	            _logger.debug("Manager.getById() - id: " + id + "; checkCache: " + checkCache);
	
	   	            return transaction;
	            }
	        }
	        finally
	        {
	            cleanupStatement(stmt);
	        }

	        _logger.debug("Manager.getById() - id: " + id + "; checkCache: " + checkCache + "; failed to find the transaction");
	        return null; 
		}
		
   	    /**
   	     * 
   	     * Retrieves a Transaction object based on the supplied id.
   	     * @param id the EsfUUID transaction id to retrieve
   	     * @return the Transaction object if found; else null
   	     */
   	    public static Transaction getById(final EsfUUID id)
   	    {
   	    	_logger.debug("Manager.getById() for id: " + id);
   	    	Transaction transaction = cache.getById(id);
   	    	if ( transaction != null )
   	    		return transaction;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	transaction = getById(con,id,false);
   	        	if ( transaction != null ) 
   	        	{
		            con.commit();
	   	        	return transaction;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   	    }
   	    
   		protected static int getCount(Connection con) 
			throws SQLException
		{
	        EsfPreparedStatement stmt = null;
	        
	        try
	        {
	        	stmt = new EsfPreparedStatement( con, "SELECT COUNT(*) from esf_transaction" );
	        	
	        	EsfResultSet rs = stmt.executeQuery();
	        	if ( rs.next() )
	        	{
		            return rs.getInt();
	            }
	        }
	        finally
	        {
	            cleanupStatement(stmt);
	        }
	
	        _logger.debug("Manager.getCount() - failed to get the count");
	        return -1; 
		}
	
   	    /**
   	     * 
   	     * Gets the number of transactions in the DB.
   	     */
   	    public static int getCount()
   	    {
   	    	_logger.debug("Manager.getCount()");
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	int count = getCount(con);
   	        	con.commit();
   	        	return count;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return -1; 
   	    }
   	    

   	    public static int deleteAllByTransactionTemplate(Connection con, EsfUUID tranTemplateId)
	    	throws SQLException
	    {
	    	_logger.debug("Manager.deleteAllByTransactionTemplate() - tranTemplateId: " + tranTemplateId);
	    	
	        ConnectionPool    pool = getConnectionPool();
	        Connection        listCon = pool.getConnection();
	        
	        EsfPreparedStatement listStmt = null;
	        
	        int numDeleted = 0;
	        
	        try
	        {
	        	listStmt = new EsfPreparedStatement( listCon, "SELECT id FROM esf_transaction WHERE transaction_template_id=?" );
	        	listStmt.set(tranTemplateId);
	        	
	        	EsfResultSet rs = listStmt.executeQuery();
	        	while ( rs.next() )
	        	{
		            EsfUUID tranId = rs.getEsfUUID();
		            Transaction tran = getById(con,tranId);
		            if ( tran != null )
		            {
			            tran.delete(con,null,null);
			            ++numDeleted;
		            }
	            }
	
	        	return numDeleted;
	        }
	        catch(SQLException e) 
	        {
	        	Application.getInstance().sqlerr(e,"Transaction.deleteAllByTransactionTemplate() - tranTemplateId: " + tranTemplateId);
	            pool.rollbackIgnoreException(listCon,e);
	            throw e;
	        }
	        finally
	        {
	        	cleanupPool(pool,listCon,listStmt);
	        }
	    }
   	    

   	    public static int resetExpireDatesRetention(Connection con, TransactionTemplate tranTemplate, boolean isProduction)
	    	throws SQLException
	    {
        	String dbIntervalSpec = isProduction ? tranTemplate.getProductionDatabaseDateIntervalSpec() : tranTemplate.getTestDatabaseDateIntervalSpec();

        	_logger.debug("Manager.resetExpireDatesRetention() - tranTemplateId: " + tranTemplate.getId() + "; isProduction: " + isProduction + "; dbIntervalSpec: " + dbIntervalSpec);
	    	
	        EsfPreparedStatement stmt = null;
	        
	        try
	        {
	        	String tranTypeSelector = isProduction ? "AND tran_type=?" : "AND tran_type<>?";
	        	if ( (isProduction && tranTemplate.isProductionRetentionForever()) || (!isProduction && tranTemplate.isTestRetentionForever()) )
	        		stmt = new EsfPreparedStatement( con, "UPDATE esf_transaction SET expire_timestamp=NULL WHERE transaction_template_id=? " + tranTypeSelector );
	        	else
	        		stmt = new EsfPreparedStatement( con, "UPDATE esf_transaction SET expire_timestamp = created_timestamp " + dbIntervalSpec + " WHERE transaction_template_id=? " + tranTypeSelector );
	        	stmt.set(tranTemplate.getId());
	        	stmt.set(Transaction.TRAN_TYPE_PRODUCTION);
	        	
	        	int num = stmt.executeUpdate();
	
	        	return num;
	        }
	        catch(SQLException e) 
	        {
	        	Application.getInstance().sqlerr(e,"Transaction.resetExpireDatesRetention() - tranTemplateId: " + tranTemplate.getId() + "; isProduction: " + isProduction + "; dbIntervalSpec: " + dbIntervalSpec);
	            throw e;
	        }
	        finally
	        {
	        	cleanupStatement(stmt);
	        }
	    }
	    

   	    public static int expireAll(Connection listCon)
   	    	throws SQLException
   	    {
   	    	_logger.debug("Manager.expireAll(listCon)");
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
	        EsfPreparedStatement listStmt = null;
	        
	        int numExpired = 0;
	        
	        try
	        {
	        	listStmt = new EsfPreparedStatement( listCon, "SELECT id FROM esf_transaction WHERE expire_timestamp < ?" );
	        	listStmt.set(new EsfDateTime());
	        	
	        	EsfResultSet rs = listStmt.executeQuery();
	        	while ( rs.next() )
	        	{
		            EsfUUID tranId = rs.getEsfUUID();
		            Transaction tran = getById(con,tranId);
		            if ( tran != null )
		            {
			            tran.delete(con,null,null);
			            ++numExpired;
		            }
	                // Let's commit any deletions after we do several just to be safe against big rollbacks
	                if ( (numExpired % 100) == 0 ) 
	                    con.commit();
	            }

	        	if ( (numExpired % 100) != 0 )
	                con.commit();
	        	
	        	return numExpired;
	        }
	        catch(SQLException e) 
	        {
	        	Application.getInstance().sqlerr(e,"Transaction.expireAll(listCon)");
	            pool.rollbackIgnoreException(con,e);
	            throw e;
	        }
	        finally
	        {
	        	cleanupStatement(listStmt);
	        	cleanupPool(pool,con,null);
	        }
   	    }

   	    /**
   	     * 
   	     * Finds all transactions that have expired and deletes them.
   	     * @return the number of transactions deleted
   	     */
   	    public static int expireAll()
   	    {
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	int numDeleted = expireAll(con);
   	        	con.commit();
   	        	return numDeleted;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	   	        return 0; 
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }
   	    }
   	    
   	    
   	    public static int cancelAll(Connection listCon)
	    	throws SQLException, EsfException
	    {
	    	_logger.debug("Manager.cancelAll(listCon)");
	    	
	        ConnectionPool    pool = getConnectionPool();
	        Connection        con  = pool.getConnection();
	        
	        EsfPreparedStatement listStmt = null;
	        
	        int numCanceled = 0;
	        
	        try
	        {
	        	listStmt = new EsfPreparedStatement( listCon, "SELECT id from esf_transaction WHERE cancel_timestamp < ? AND status = ?" );
	        	listStmt.set(new EsfDateTime());
	        	listStmt.set(TRAN_STATUS_IN_PROGRESS);
	        	
	        	EsfResultSet rs = listStmt.executeQuery();
	        	while ( rs.next() )
	        	{
		            EsfUUID tranId = rs.getEsfUUID();
		            Transaction tran = getById(con,tranId);
		            if ( tran != null && tran.isInProgress() && tran.hasCancelTimestamp() )
		            {
		            	tran.logGeneral(con, "Auto-canceling since In Progress and it reached the cancel date of " + tran.getCancelTimestamp().toLogTimeString());
		            	EsfString autoCancelReason = tran.getRecord().getStringByName( TRAN_RECORD_AUTO_CANCEL_REASON_ESFNAME );
		            	
	        			TransactionContext tranContext = new TransactionContext(null, tranId);
	        			TransactionEngine engine = new TransactionEngine(tranContext);
	        			engine.queueTransactionCanceledEvent( autoCancelReason == null || autoCancelReason.isBlank() ? "(Auto-cancel reason not specified)" : autoCancelReason.toString() );
	        			Errors errors = new Errors();
	        			engine.doWork(con, errors);

			            tran.save(con);
			            ++numCanceled;
		            }
	                // Let's commit any cancels after we do several just to be safe against big rollbacks
	                if ( (numCanceled % 100) == 0 ) 
	                    con.commit();
	            }
	
	        	if ( (numCanceled % 100) != 0 )
	                con.commit();
	        	
	        	return numCanceled;
	        }
	        catch(SQLException e) 
	        {
	        	Application.getInstance().sqlerr(e,"Manager.cancelAll(listCon)");
	            pool.rollbackIgnoreException(con,e);
	            throw e;
	        }
	        catch(EsfException e) 
	        {
	        	Application.getInstance().except(e,"Manager.cancelAll(listCon)");
	            pool.rollbackIgnoreException(con);
	            throw e;
	        }
	        finally
	        {
	        	cleanupStatement(listStmt);
	        	cleanupPool(pool,con,null);
	        }
	    }

	    /**
	     * 
	     * Finds all transactions that have reached their canceled date and cancels them.
	     * @return the number of transactions canceled
	     */
	    public static int cancelAll()
	    {
	        ConnectionPool    pool = getConnectionPool();
	        Connection        con  = pool.getConnection();
	        
	        try
	        {
	        	int numDeleted = cancelAll(con);
	        	con.commit();
	        	return numDeleted;
	        }
	        catch(SQLException e) 
	        {
	            pool.rollbackIgnoreException(con,e);
	   	        return 0; 
	        }
	        catch(EsfException e) 
	        {
	            pool.rollbackIgnoreException(con);
	   	        return 0; 
	        }
	        finally
	        {
	            cleanupPool(pool,con,null);
	        }
	    }
	    
	    
   	    // Common routine for setting up a new transactions, building the run-time version of documents and parties
   	    private static void setupNewTransaction(Transaction tran, TransactionTemplate transactionTemplate, User createdBy, HttpServletRequest request, Transaction cloneFromTransaction)
   	   	    	throws EsfException
   	    {
   	    	if ( cloneFromTransaction != null )
   	    	{
   	    		tran.getRecord().addUpdateClonableFields(cloneFromTransaction.getRecord());
   	    		tran.getRecord().addUpdate(RECORD_CLONED_FROM_TRANSACTION_ESFNAME,cloneFromTransaction.getId(),false);
   	    	}
   	    	
   	    	if ( request != null )
   	    		tran.addUpdateTransactionStartedRequestData(request);
   	    	
   	    	if ( createdBy != null )
   	    		tran.addUpdateTransactionStartedCreatedBy(createdBy);
   	    	
   	    	PackageVersion pkgVersion = tran.doProductionResolve() ? tran.getPackage().getProductionPackageVersion() : tran.getPackage().getTestPackageVersion();
   	    	if ( pkgVersion == null )
   	    		throw new EsfException("Cannot setup new transaction: PackageVersion could not be found. tran.doProductionResolve: " + tran.doProductionResolve() + "; package: " + tran.getPackage().getPathName() + "; packageId: " + tran.getPackageId() + "; tranId: " + tran.getId());
   	    	tran.packageVersionId = pkgVersion.getId();
   	    	
   	    	List<EsfUUID> docIdList = pkgVersion.getDocumentIdList();
   	    	for( EsfUUID docId : docIdList )
   	    	{
   	    		Document document = Document.Manager.getById(docId);
   	   	    	if ( document == null )
   	   	    		throw new EsfException("Cannot setup new transaction: Document could not be found. Package: " + tran.getPackage().getPathName() + "; packageId: " + tran.getPackageId() + "; documentId: " + docId + "; tranId: " + tran.getId());

   	   	    	DocumentVersion documentVersion = tran.doProductionResolve() ? document.getProductionDocumentVersion() : document.getTestDocumentVersion();
   	   	    	if ( documentVersion == null )
   	   	    		throw new EsfException("Cannot setup new transaction: DocumentVersion could not be found. tran.doProductionResolve: " + tran.doProductionResolve() + "; Package: " + tran.getPackage().getPathName() + "; packageId: " + tran.getPackageId() + "; document: " + document.getEsfName() + "; documentId: " + docId + "; tranId: " + tran.getId());

   	    		TransactionDocument tranDoc = TransactionDocument.Manager.createNew(createdBy, tran, document, documentVersion);
   	   	    	if ( tranDoc == null )
   	   	    		throw new EsfException("Cannot setup new transaction: TransactionDocument could not be created. tran.doProductionResolve: " + tran.doProductionResolve() + "; Package: " + tran.getPackage().getPathName() + "; packageId: " + tran.getPackageId() + "; document: " + document.getEsfName() + "; documentId: " + docId + "; tranId: " + tran.getId());

   	   	    	if ( cloneFromTransaction != null )
   	   	    	{
   	   	    		TransactionDocument cloneTranDoc = cloneFromTransaction.getTransactionDocumentByDocumentId(docId);
   	   	    		if ( cloneTranDoc != null )
   	   	    		{
   	   	    			tranDoc.getRecord().addUpdateClonableFields(cloneTranDoc.getRecord());
   	    	    		tranDoc.getRecord().addUpdate(RECORD_CLONED_FROM_TRANSACTION_ESFNAME,cloneFromTransaction.getId(),false);
   	    	    		tranDoc.getRecord().addUpdate(RECORD_CLONED_FROM_TRANSACTION_DOCUMENT_ESFNAME,cloneTranDoc.getId(),false);
   	   	    		}
   	   	    	}
   	    		tran.transactionDocumentMap.put(document.getEsfName(), tranDoc);
   	    	}
   	    	
   	    	List<PackageVersionPartyTemplate> packageVersionPartyTemplateList = pkgVersion.getPackageVersionPartyTemplateList();
   	    	for( PackageVersionPartyTemplate packageVersionPartyTemplate : packageVersionPartyTemplateList )
   	    	{
   	    		TransactionParty tranParty = TransactionParty.Manager.createNew(tran, packageVersionPartyTemplate);
   	    		
   	    		short documentNumber = 1;
   	   	    	for( EsfUUID docId : docIdList )
   	   	    	{
   	   	    		if ( packageVersionPartyTemplate.isPartyToDocument(docId) ) 
   	   	    		{
   	   	    			TransactionDocument tranDoc = tran.getTransactionDocumentByDocumentId(docId);
   	   	    			TransactionPartyDocument tranPartyDoc = TransactionPartyDocument.Manager.createNew(tranParty, tranDoc, documentNumber++);
   	   	    			if ( packageVersionPartyTemplate.isViewOptional(docId) )
   	   	    				tranPartyDoc.setStatusViewOptional();
   	   	    			tranParty._addTransactionPartyDocumentDuringCreation(tranPartyDoc);
   	   	    		}
   	   	    	}
   	    		
   	    		tran.transactionPartyMap.put(packageVersionPartyTemplate.getEsfName(), tranParty);
   	    	}
   	    	
   	    	// Now that we did basic initialization of our transaction documents and transaction parties, see if we have any initial file upload data to attach
   	    	TransactionParty firstTranParty = tran.getAllTransactionParties().get(0);
   	    	for( TransactionDocument tranDoc : tran.getAllTransactionDocuments() )
   	    	{
   	    		tranDoc.initializeFiles(tran, firstTranParty);
   	    	}
   	    	
   	    	if ( tran.isProduction() )
   	    	{
   	    		if ( transactionTemplate.isProductionRetentionForever() )
   	    			tran.setExpireTimestamp(null);
   	    		else
   	    			tran.setExpireTimestamp(transactionTemplate.getProductionRetentionDateTimeFromNow());
   	    	}
   	    	else
   	    	{
   	    		if ( transactionTemplate.isTestRetentionForever() )
   	    			tran.setExpireTimestamp(null);
   	    		else
   	    			tran.setExpireTimestamp(transactionTemplate.getTestRetentionDateTimeFromNow());
   	    	}
   	    }

   	    public static Transaction createProduction(TransactionTemplate transactionTemplate, User createdBy, HttpServletRequest request, Transaction cloneFromTransaction)
   	   	    	throws EsfException
   	    {
   	    	Transaction tran = new Transaction(transactionTemplate,TRAN_TYPE_PRODUCTION,createdBy);
   	    	setupNewTransaction(tran, transactionTemplate, createdBy, request, cloneFromTransaction);
   	    	cache.add(tran);
   	    	return tran;
   	    }

   	    public static Transaction createTest(TransactionTemplate transactionTemplate, User createdBy, HttpServletRequest request, Transaction cloneFromTransaction)
   	   	    	throws EsfException
   	    {
   	    	Transaction tran = new Transaction(transactionTemplate,TRAN_TYPE_TEST,createdBy);
   	    	setupNewTransaction(tran, transactionTemplate, createdBy, request, cloneFromTransaction);
   	    	cache.add(tran);
   	    	return tran;
   	    }
   	    
   	    public static Transaction createTestLikeProduction(TransactionTemplate transactionTemplate, User createdBy, HttpServletRequest request, Transaction cloneFromTransaction)
   	    	throws EsfException
   	    {
   	    	Transaction tran = new Transaction(transactionTemplate,TRAN_TYPE_TEST_PRODUCTION,createdBy);
   	    	setupNewTransaction(tran, transactionTemplate, createdBy, request, cloneFromTransaction);
   	    	cache.add(tran);
   	    	return tran;
   	    }


   	    public static Transaction createForTestDocument(TransactionTemplate transactionTemplate, User createdBy, HttpServletRequest request)
   	   	    	throws EsfException
   	    {
   	    	Transaction tran = new Transaction(transactionTemplate,TRAN_TYPE_TEST,createdBy);
   	    	tran.isTemporary = true;
   	    	setupNewTransaction(tran, transactionTemplate, createdBy, request, null);
   	    	cache.add(tran);
   	    	
   	    	return tran;
   	    }
   	    
   	    /**
   	     * Removes the transactions from the cache. Not a typical method, but we have a few bulk transaction processors that we want to 
   	     * remove them after we're done since we'd otherwise load too much into our cache.
   	     * @param transaction
   	     * @return boolean true if the transaction was removed; false if it wasn't in the cache.
   	     */
   	    public static boolean removeFromCache(Transaction transaction)
   	    {
   	    	return transaction == null ? false : cache.remove(transaction);
   	    }

   	    /**
   	     * Returns the total number of transactions objects in the cache
   	     * @return the total number of transactions in the cache
   	     */
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all transactions that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}
   		
   	} // Manager
   
}