// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfDecimal;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfHtml;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfMoney;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.data.Record;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.user.User;
import com.esignforms.open.util.Base64;

/**
* TransactionDocument holds information about a document version at runtime.
* 
* @author Yozons, Inc.
*/
public class TransactionDocument
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<TransactionDocument>, java.io.Serializable
{
	private static final long serialVersionUID = -3496159915353290968L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TransactionDocument.class);
	
	// Suffixes we use for HttpSendAction of files, but also for checking if we have param data that is a file
	public static final String FILENAME_PARAM_SUFFIX 		= "__fileName";
	public static final String FILEMIMETYPE_PARAM_SUFFIX 	= "__fileMimeType";

    protected final EsfUUID id;
    protected final EsfUUID transactionId; // The transaction we belong to
    protected final EsfUUID documentId;
    protected final EsfUUID documentVersionId;
    
    protected Record record;
    
    /**
     * This creates a TransactionDocument object from data retrieved from the DB.
     */
    protected TransactionDocument(EsfUUID id, EsfUUID transactionId, EsfUUID documentId, EsfUUID documentVersionId, Record record)
    {
        this.id = id;
    	this.transactionId = transactionId;
        this.documentId = documentId;
        this.documentVersionId = documentVersionId;
        this.record = record;
    }
    
    protected TransactionDocument(EsfUUID transactionId, Document document, DocumentVersion documentVersion, Record record)
    {
        this.id = new EsfUUID();
    	this.transactionId = transactionId;
        this.documentId = document.getId();
        this.documentVersionId = documentVersion.getId();
        this.record = record;
    }

   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getTransactionId()
    {
        return transactionId;
    }

    public final EsfUUID getDocumentId()
    {
        return documentId;
    }
    public Document getDocument()
    {
    	return Document.Manager.getById(documentId);
    }
    
    public final EsfUUID getDocumentVersionId()
    {
        return documentVersionId;
    }
    public DocumentVersion getDocumentVersion()
    {
    	return DocumentVersion.Manager.getById(documentVersionId);
    }
    
	public Record getRecord()
	{
		return record;
	}
	
	public enum INITIALIZE_RECORD { ALL_FIELDS, ONLY_FIELDS_WITH_NO_VALUE, ONLY_MISSING_FIELDS };
	public void initializeRecord(User user, Transaction transaction, Document document, DocumentVersion documentVersion, INITIALIZE_RECORD initRecord)
	{
		Map<EsfName,FieldTemplate> fieldMap = documentVersion.getFieldTemplateMap();
		for( EsfName fieldName : fieldMap.keySet() )
		{
			FieldTemplate fieldTemplate = fieldMap.get(fieldName);
			if ( fieldTemplate.isTypeCheckbox() || 
				 fieldTemplate.isTypeCreditCard() || 
				 fieldTemplate.isTypeGeneral() || 
				 fieldTemplate.isTypePhone() || 
				 fieldTemplate.isTypeRadioButtonGroup() || 
				 fieldTemplate.isTypeSsnEin() || 
				 fieldTemplate.isTypeTextarea() || 
				 fieldTemplate.isTypeZipCode()
			   ) 
			{
				EsfString value = record.getStringByName(fieldName);
				// On tran initialize, or when a party activates a document and the field has an initial value field spec (${}) and the current value is blank, try to set it using the initial value.
				// When a party retrieves a blank field, but the initial value is a literal string, we don't want to overwrite it because the literals are all set when the tran is started.
				if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS || 
					(initRecord == INITIALIZE_RECORD.ONLY_FIELDS_WITH_NO_VALUE && fieldTemplate.hasFieldSpecInitialValue() && (value == null || value.isBlank())) ||
					(initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && value == null)
				   )
				{
					if ( fieldTemplate.hasInitialValue() )
					{
						String initialValue = transaction.getFieldSpecWithSubstitutions(user, documentVersion, fieldTemplate.getInitialValue());
						// If they have enabled display empty values, and our initial value has a field spec that results in nothing
						if ( fieldTemplate.isDisplayEmptyValueEnabled() && fieldTemplate.hasFieldSpecInitialValue() && fieldTemplate.getInitialValue().equalsIgnoreCase(initialValue) )
							initialValue = null;
						value = new EsfString(initialValue);
					}
					else
					{
						EsfString initialPostValue = transaction.getRecord().getStringByName(fieldName);
						value = initialPostValue != null ? initialPostValue : new EsfString();
					}
					record.addUpdate(fieldName, value, fieldTemplate.isCloneAllowed());
				}
			}
			else if ( fieldTemplate.isTypeDate() )
			{
				EsfDate value = record.getDateByName(fieldName);
				// On tran initialize, or when a party activates a document and the field has an initial value field spec (${}) and the current value is blank, try to set it using the initial value.
				// When a party retrieves a blank field, but the initial value is a literal string, we don't want to overwrite it because the literals are all set when the tran is started.
				if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS || 
					(initRecord == INITIALIZE_RECORD.ONLY_FIELDS_WITH_NO_VALUE && fieldTemplate.hasFieldSpecInitialValue() && (value == null || value.isNull())) ||
					(initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && value == null)
				   )
				{
					if ( fieldTemplate.hasInitialValue() )
					{
						String spec = transaction.getFieldSpecWithSubstitutions(user, documentVersion, fieldTemplate.getInitialValue());
						// If they have enabled display empty values, and our initial value has a field spec that results in nothing
						if ( fieldTemplate.isDisplayEmptyValueEnabled() && fieldTemplate.hasFieldSpecInitialValue() && fieldTemplate.getInitialValue().equalsIgnoreCase(spec) )
							value = new EsfDate((java.util.Date)null);
						else
							value = EsfDate.CreateGuessFormat(spec); // should be in yyyy-mm-dd format, or at least dd-Mon-yyyy or mm/dd/yyyy format, or our special cases: now or +numDays or -numDays
					}
					else
					{
						EsfString initialPostValue = transaction.getRecord().getStringByName(fieldName);
						value = initialPostValue != null ? EsfDate.CreateGuessFormat(initialPostValue.toString()) : new EsfDate((java.util.Date)null);
					}
					record.addUpdate(fieldName, value, fieldTemplate.isCloneAllowed());
				}
			}
			else if ( fieldTemplate.isTypeDateTime() )
			{
				EsfDateTime value = record.getDateTimeByName(fieldName);
				// On tran initialize, or when a party activates a document and the field has an initial value field spec (${}) and the current value is blank, try to set it using the initial value.
				// When a party retrieves a blank field, but the initial value is a literal string, we don't want to overwrite it because the literals are all set when the tran is started.
				if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS || 
					(initRecord == INITIALIZE_RECORD.ONLY_FIELDS_WITH_NO_VALUE && fieldTemplate.hasFieldSpecInitialValue() && (value == null || value.isNull())) ||
					(initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && value == null)
				   )
				{
					if ( fieldTemplate.hasInitialValue() )
					{
						String spec = transaction.getFieldSpecWithSubstitutions(user, documentVersion, fieldTemplate.getInitialValue());
						// If they have enabled display empty values, and our initial value has a field spec that results in nothing
						if ( fieldTemplate.isDisplayEmptyValueEnabled() && fieldTemplate.hasFieldSpecInitialValue() && fieldTemplate.getInitialValue().equalsIgnoreCase(spec) )
							value = new EsfDateTime((java.util.Date)null);
						else
							value = EsfDateTime.CreateGuessFormat(spec); // should be in date format with HH:MM[:SS] [TZ] following it, or our special cases: now or +numDays or -numDays
					}
					else
					{
						EsfString initialPostValue = transaction.getRecord().getStringByName(fieldName);
						value = initialPostValue != null ? EsfDateTime.CreateGuessFormat(initialPostValue.toString()) : new EsfDateTime((java.util.Date)null);
					}
					record.addUpdate(fieldName, value, fieldTemplate.isCloneAllowed());
				}
			}
			else if ( fieldTemplate.isTypeDecimal() )
			{
				EsfValue value = record.getDecimalByName(fieldName);
				// On tran initialize, or when a party activates a document and the field has an initial value field spec (${}) and the current value is blank, try to set it using the initial value.
				// When a party retrieves a blank field, but the initial value is a literal string, we don't want to overwrite it because the literals are all set when the tran is started.
				if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS || 
					(initRecord == INITIALIZE_RECORD.ONLY_FIELDS_WITH_NO_VALUE && fieldTemplate.hasFieldSpecInitialValue() && (value == null || value.isNull())) ||
					(initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && value == null)
				   )
				{
					if ( fieldTemplate.hasInitialValue() )
					{
						String initialValue = transaction.getFieldSpecWithSubstitutions(user, documentVersion, fieldTemplate.getInitialValue());
						// If they have enabled display empty values, and our initial value has a field spec that results in nothing
						if ( fieldTemplate.isDisplayEmptyValueEnabled() && fieldTemplate.hasFieldSpecInitialValue() && fieldTemplate.getInitialValue().equalsIgnoreCase(initialValue) )
							value = new EsfDecimal((String)null);
						else
							value = new EsfDecimal(initialValue);
					}
					else
					{
						EsfString initialPostValue = transaction.getRecord().getStringByName(fieldName);
						value = initialPostValue != null ? new EsfDecimal(initialPostValue.toString()) : new EsfDecimal((String)null);
					}
					record.addUpdate(fieldName, value, fieldTemplate.isCloneAllowed());
				}
			}
			else if ( fieldTemplate.isTypeEmailAddress() )
			{
				EsfEmailAddress value = record.getEmailAddressByName(fieldName);
				// On tran initialize, or when a party activates a document and the field has an initial value field spec (${}) and the current value is blank, try to set it using the initial value.
				// When a party retrieves a blank field, but the initial value is a literal string, we don't want to overwrite it because the literals are all set when the tran is started.
				if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS || 
					(initRecord == INITIALIZE_RECORD.ONLY_FIELDS_WITH_NO_VALUE && fieldTemplate.hasFieldSpecInitialValue() && (value == null || value.isBlank())) ||
					(initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && value == null)
				   )
				{
					if ( fieldTemplate.hasInitialValue() )
					{
						String initialValue = transaction.getFieldSpecWithSubstitutions(user, documentVersion, fieldTemplate.getInitialValue());
						// If they have enabled display empty values, and our initial value has a field spec that results in nothing
						if ( fieldTemplate.isDisplayEmptyValueEnabled() && fieldTemplate.hasFieldSpecInitialValue() && fieldTemplate.getInitialValue().equalsIgnoreCase(initialValue) )
							value = new EsfEmailAddress();
						else
							value = new EsfEmailAddress(initialValue);
					}
					else
					{
						EsfString initialPostValue = transaction.getRecord().getStringByName(fieldName);
						value = initialPostValue != null ? new EsfEmailAddress(initialPostValue.toString()) : new EsfEmailAddress();
					}
					record.addUpdate(fieldName, value, fieldTemplate.isCloneAllowed());
				}
			}
			else if ( fieldTemplate.isTypeFile() )
			{
				EsfInteger value = record.getIntegerByName(fieldName);
				if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS || 
					(initRecord == INITIALIZE_RECORD.ONLY_FIELDS_WITH_NO_VALUE && (value == null || value.isNull())) ||
					(initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && value == null)
				   )
				{
					record.addUpdate(fieldName, new EsfInteger(0), fieldTemplate.isCloneAllowed()); // For files, we only track the number of files
				}
			}
			else if ( fieldTemplate.isTypeFileConfirmClick() )
			{
				EsfValue value = record.getValueByName(fieldName);
				if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS  || 
					(initRecord == INITIALIZE_RECORD.ONLY_FIELDS_WITH_NO_VALUE && (value == null || value.isNull())) ||
					(initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && value == null)
				   )
				{
					record.addUpdate(fieldName, new EsfName(fieldTemplate.getInputFormatSpec()), fieldTemplate.isCloneAllowed()); // 1/8/2014: Set the File name to use -- can be updated through custom logic set field value actions to be dynamic.
				}
			}
			else if ( fieldTemplate.isTypeInteger() )
			{
				EsfValue value = record.getIntegerByName(fieldName);
				// On tran initialize, or when a party activates a document and the field has an initial value field spec (${}) and the current value is blank, try to set it using the initial value.
				// When a party retrieves a blank field, but the initial value is a literal string, we don't want to overwrite it because the literals are all set when the tran is started.
				if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS || 
					(initRecord == INITIALIZE_RECORD.ONLY_FIELDS_WITH_NO_VALUE && fieldTemplate.hasFieldSpecInitialValue() && (value == null || value.isNull())) ||
					(initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && value == null)
				   )
				{
					if ( fieldTemplate.hasInitialValue() )
					{
						String initialValue = transaction.getFieldSpecWithSubstitutions(user, documentVersion, fieldTemplate.getInitialValue());
						// If they have enabled display empty values, and our initial value has a field spec that results in nothing
						if ( fieldTemplate.isDisplayEmptyValueEnabled() && fieldTemplate.hasFieldSpecInitialValue() && fieldTemplate.getInitialValue().equalsIgnoreCase(initialValue) )
							value = new EsfInteger((String)null);
						else
							value = new EsfInteger(initialValue);
					}
					else
					{
						EsfString initialPostValue = transaction.getRecord().getStringByName(fieldName);
						value = initialPostValue != null ? new EsfInteger(initialPostValue.toString()) : new EsfInteger((String)null);
					}
					record.addUpdate(fieldName, value, fieldTemplate.isCloneAllowed());
				}
			}
			else if ( fieldTemplate.isTypeMoney() )
			{
				EsfValue value = record.getMoneyByName(fieldName);
				// On tran initialize, or when a party activates a document and the field has an initial value field spec (${}) and the current value is blank, try to set it using the initial value.
				// When a party retrieves a blank field, but the initial value is a literal string, we don't want to overwrite it because the literals are all set when the tran is started.
				if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS || 
					(initRecord == INITIALIZE_RECORD.ONLY_FIELDS_WITH_NO_VALUE && fieldTemplate.hasFieldSpecInitialValue() && (value == null || value.isNull())) ||
					(initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && value == null)
				   )
				{
					if ( fieldTemplate.hasInitialValue() )
					{
						String initialValue = transaction.getFieldSpecWithSubstitutions(user, documentVersion, fieldTemplate.getInitialValue());
						// If they have enabled display empty values, and our initial value has a field spec that results in nothing
						if ( fieldTemplate.isDisplayEmptyValueEnabled() && fieldTemplate.hasFieldSpecInitialValue() && fieldTemplate.getInitialValue().equalsIgnoreCase(initialValue) )
							value = new EsfMoney((String)null);
						else
							value = new EsfMoney(initialValue);
					}
					else
					{
						EsfString initialPostValue = transaction.getRecord().getStringByName(fieldName);
						value = initialPostValue != null ? new EsfMoney(initialPostValue.toString()) : new EsfMoney((String)null);
					}
					record.addUpdate(fieldName, value, fieldTemplate.isCloneAllowed());
				}
			}
			else if ( fieldTemplate.isTypeRadioButton() )
			{
				// radio buttons are just labels in a radio button group -- the group carries the data
			}
			else if ( fieldTemplate.isTypeRichTextarea() )
			{
				EsfHtml value = record.getHtmlByName(fieldName);
				// On tran initialize, or when a party activates a document and the field has an initial value field spec (${}) and the current value is blank, try to set it using the initial value.
				// When a party retrieves a blank field, but the initial value is a literal string, we don't want to overwrite it because the literals are all set when the tran is started.
				if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS || 
					(initRecord == INITIALIZE_RECORD.ONLY_FIELDS_WITH_NO_VALUE && fieldTemplate.hasFieldSpecInitialValue() && (value == null || value.isBlank())) ||
					(initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && value == null)
				   )
				{
					if ( fieldTemplate.hasInitialValue() )
					{
						String initialValue = transaction.getFieldSpecWithSubstitutions(user, documentVersion, fieldTemplate.getInitialValue());
						// If they have enabled display empty values, and our initial value has a field spec that results in nothing
						if ( fieldTemplate.isDisplayEmptyValueEnabled() && fieldTemplate.hasFieldSpecInitialValue() && fieldTemplate.getInitialValue().equalsIgnoreCase(initialValue) )
							value = new EsfHtml();
						else
							value = new EsfHtml(initialValue);
					}
					else
					{
						EsfString initialPostValue = transaction.getRecord().getStringByName(fieldName);
						value = initialPostValue != null ? new EsfHtml(initialPostValue.toString()) : new EsfHtml();
					}
					record.addUpdate(fieldName, value, fieldTemplate.isCloneAllowed());
				}
			}
			else if ( fieldTemplate.isTypeSelection() ) 
			{
				DropDownVersion ddv = transaction.getDropDownVersion(new EsfName(fieldTemplate.getInputFormatSpec()),getDocumentVersion());
				if ( ddv == null )
				{
				}
				else if ( ddv.isAllowMultiSelection() )
				{
					EsfString[] values = record.getStringsByName(fieldName);
					boolean hasNonBlankValue = false;
					if ( values != null && values.length > 0 )
					{
						for( EsfString v : values )
						{
							if ( v != null && v.isNonBlank() )
							{
								hasNonBlankValue = true;
								break;
							}
						}
					}
					
					// On tran initialize, or when a party activates a document and the field has an initial value field spec (${}) and the current value is blank, try to set it using the initial value.
					// When a party retrieves a blank field, but the initial value is a literal string, we don't want to overwrite it because the literals are all set when the tran is started.
					if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS || 
						(initRecord == INITIALIZE_RECORD.ONLY_FIELDS_WITH_NO_VALUE && fieldTemplate.hasFieldSpecInitialValue() && ! hasNonBlankValue) ||
						(initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && values == null)
					   )
					{
						if ( fieldTemplate.hasInitialValue() )
						{
							String initialValue = transaction.getFieldSpecWithSubstitutions(user, documentVersion, fieldTemplate.getInitialValue());
							// If they have enabled display empty values, and our initial value has a field spec that results in nothing
							if ( fieldTemplate.isDisplayEmptyValueEnabled() && fieldTemplate.hasFieldSpecInitialValue() && fieldTemplate.getInitialValue().equalsIgnoreCase(initialValue) )
								initialValue = null;
							values = new EsfString[1];
							values[0] = new EsfString(initialValue);
						}
						else
						{
							EsfString[] initialPostValues = transaction.getRecord().getStringsByName(fieldName);
							values = initialPostValues != null ? initialPostValues : new EsfString[0];
						}
						record.addUpdate(fieldName, values, fieldTemplate.isCloneAllowed());
					}
				}
				else
				{
					EsfString value = record.getStringByName(fieldName);
					
					// On tran initialize, or when a party activates a document and the field has an initial value field spec (${}) and the current value is blank, try to set it using the initial value.
					// When a party retrieves a blank field, but the initial value is a literal string, we don't want to overwrite it because the literals are all set when the tran is started.
					if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS || 
						(initRecord == INITIALIZE_RECORD.ONLY_FIELDS_WITH_NO_VALUE && fieldTemplate.hasFieldSpecInitialValue() && (value==null || value.isBlank())) ||
						(initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && value == null)
					   )
					{
						if ( fieldTemplate.hasInitialValue() )
						{
							String initialValue = transaction.getFieldSpecWithSubstitutions(user, documentVersion, fieldTemplate.getInitialValue());
							// If they have enabled display empty values, and our initial value has a field spec that results in nothing
							if ( fieldTemplate.isDisplayEmptyValueEnabled() && fieldTemplate.hasFieldSpecInitialValue() && fieldTemplate.getInitialValue().equalsIgnoreCase(initialValue) )
								initialValue = null;
							value = new EsfString(initialValue);
						}
						else
						{
							EsfString initialPostValue = transaction.getRecord().getStringByName(fieldName);
							value = initialPostValue != null ? initialPostValue : new EsfString();
						}
						record.addUpdate(fieldName, value, fieldTemplate.isCloneAllowed());
					}	
				}
			}
			else if ( fieldTemplate.isTypeSignature() )
			{
				EsfString value = record.getStringByName(fieldName);
				if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS || (initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && value == null) )
				{
					record.addUpdate(fieldName, new EsfString(), fieldTemplate.isCloneAllowed());
				}
			}
			else if ( fieldTemplate.isTypeSignDate() )
			{
				EsfDate value = record.getDateByName(fieldName);
				if ( initRecord == INITIALIZE_RECORD.ALL_FIELDS || (initRecord == INITIALIZE_RECORD.ONLY_MISSING_FIELDS && value == null))
				{
					EsfDate nullDate = new EsfDate((EsfDate)null);
					record.addUpdate(fieldName, nullDate, fieldTemplate.isCloneAllowed());
				}
			}
			else
			{
				_logger.debug("initializeRecord() - Unexpected field to initialize: " + fieldName);
			}
		}
	}

	
	// We split this out because we cannot initialize files until our parties are setup, and our party setup needs the transaction documents to be setup!
	// So when both are done, we revisit to see if any of the initialization should include files.
	public void initializeFiles(Transaction transaction, TransactionParty firstTranParty)
	{
		Map<EsfName,FieldTemplate> fieldMap = getDocumentVersion().getFieldTemplateMap();
		for( EsfName fieldName : fieldMap.keySet() )
		{
			FieldTemplate fieldTemplate = fieldMap.get(fieldName);
			if ( fieldTemplate.isTypeFile() )
			{
				// Let's see if we have one or more file uploads set for this field name
				EsfString[] filesBase64 = transaction.getRecord().getStringsByName(fieldName);
				if ( filesBase64 != null && filesBase64.length > 0 )
				{
					EsfString[] fileNames = transaction.getRecord().getStringsByName(fieldName+FILENAME_PARAM_SUFFIX);
					EsfString[] fileMimeTypes = transaction.getRecord().getStringsByName(fieldName+FILEMIMETYPE_PARAM_SUFFIX);
					
					int limitNumFiles = Application.getInstance().stringToInt(fieldTemplate.getInputFormatSpec(), -1);
					
					for( int i=0; i < filesBase64.length; ++i )
					{
						byte[] fileBytes;

						EsfString fileBase64 = filesBase64[i];
						if ( fileBase64 != null && fileBase64.isNonBlank() )
						{
							fileBytes = Base64.decode(fileBase64.toPlainString());
							// If we don't have a file, skip this bad setup for initialization
							if ( fileBytes == null )
							{
								transaction.logGeneral("Initialize Files ERROR: Invalid Base-64 encoded file data for field: " + fieldName + "; param value length: " + fileBase64.getLength());
								continue;
							}
						}
						else
						{
							transaction.logGeneral("Initialize Files WARNING: No Base-64 encoded file data provided for field: " + fieldName);
							continue;
						}
						
						// If we're supposed to limit the number of files, ensure this isn't maxed out first
						if ( limitNumFiles > 0 )
						{
			    			List<TransactionFile> fileList = transaction.getAllTransactionFilesForDocumentField(getId(), fieldTemplate.getEsfName());
			    			if ( fileList.size() >= limitNumFiles )
			    			{
								transaction.logGeneral("Initialize Files ERROR: Too many files for field: " + fieldName);
			    				break; // can't do any more
			    			}
						}

						// Add this file
						String fileName = i < fileNames.length ? fileNames[i].toPlainString() : null;
						if ( EsfString.isBlank(fileName) )
							fileName = fieldName.toPlainString();
						
						String fileMimeType = i < fileMimeTypes.length ? fileMimeTypes[i].toPlainString() : null;
						if ( EsfString.isBlank(fileMimeType) )
							fileMimeType = Application.CONTENT_TYPE_BINARY;
						
		            	TransactionFile tranFile = TransactionFile.Manager.createNew(
		            			transaction.getId(), 
								getId(), 
								firstTranParty.getId(), 
								fieldTemplate.getEsfName(), 
								fileName, 
								fileMimeType, 
								fileBytes);			
		            	transaction.addTransactionFile(tranFile);
		            	
		            	// Let's say the first party has read this file
		            	String partyReadPathName = firstTranParty.getPackageVersionPartyTemplate().getEsfName() + EsfPathName.PATH_SEPARATOR + tranFile.getId().toNormalizedEsfNameString() + EsfPathName.PATH_SEPARATOR + "lastAccessTimestamp";
		            	EsfPathName fileAccessPathName = new EsfPathName(partyReadPathName);
						record.addUpdate(fileAccessPathName, new EsfDateTime(), false);
						
						List<TransactionFile> fileList = transaction.getAllTransactionFilesForDocumentField(getId(), fieldTemplate.getEsfName());
						EsfInteger value = new EsfInteger(fileList.size());
						record.addUpdate(fieldTemplate.getEsfName(), value, fieldTemplate.isCloneAllowed());
					}
				}
			}
		}
	}
	
	
	public void updateApi(Connection con, Transaction transaction, TransactionParty tranParty)
		throws SQLException
	{
		DocumentVersion documentVersion = getDocumentVersion();
		if ( documentVersion == null )
		{
			transaction.logActionBasic(con, "Update API: Could not find document version fields to update.");
			_logger.error("updateApi() - Could not find document version for transaction document id: " + getId() + "; transaction id: " + transaction.getId());
			return;
		}
		
		Map<EsfName,FieldTemplate> fieldMap = documentVersion.getFieldTemplateMap();
		for( EsfName fieldName : fieldMap.keySet() )
		{
			FieldTemplate fieldTemplate = fieldMap.get(fieldName);
			if ( fieldTemplate.isTypeCheckbox() || 
				 fieldTemplate.isTypeCreditCard() || 
				 fieldTemplate.isTypeGeneral() || 
				 fieldTemplate.isTypePhone() || 
				 fieldTemplate.isTypeRadioButtonGroup() || 
				 fieldTemplate.isTypeSsnEin() || 
				 fieldTemplate.isTypeTextarea() || 
				 fieldTemplate.isTypeZipCode()
			   ) 
			{
				EsfString updateApiPostValue = transaction.getRecord().getStringByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
				if ( updateApiPostValue != null )
				{
					EsfString value = record.getStringByName(fieldName);
					if ( ! updateApiPostValue.equals(value) )
					{
						transaction.logActionDetail(con, "Update API: set field: " + fieldName + "; from: " + value + "; to: " + updateApiPostValue);
						record.addUpdate(fieldName, updateApiPostValue, fieldTemplate.isCloneAllowed());
					}
				}
			}
			else if ( fieldTemplate.isTypeDate() )
			{
				EsfString updateApiPostValue = transaction.getRecord().getStringByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
				if ( updateApiPostValue != null )
				{
					EsfDate updateApiPostDateValue = EsfDate.CreateGuessFormat(updateApiPostValue.toString());
					EsfDate value = record.getDateByName(fieldName);
					if ( ! updateApiPostDateValue.equals(value) )
					{
						transaction.logActionDetail(con, "Update API: set field: " + fieldName + "; from: " + value + "; to: " + updateApiPostDateValue);
						record.addUpdate(fieldName, updateApiPostDateValue, fieldTemplate.isCloneAllowed());
					}
				}
			}
			else if ( fieldTemplate.isTypeDateTime() )
			{
				EsfString updateApiPostValue = transaction.getRecord().getStringByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
				if ( updateApiPostValue != null )
				{
					EsfDateTime updateApiPostDateTimeValue = EsfDateTime.CreateGuessFormat(updateApiPostValue.toString());
					EsfDateTime value = record.getDateTimeByName(fieldName);
					if ( ! updateApiPostDateTimeValue.equals(value) )
					{
						transaction.logActionDetail(con, "Update API: set field: " + fieldName + "; from: " + value + "; to: " + updateApiPostDateTimeValue);
						record.addUpdate(fieldName, updateApiPostDateTimeValue, fieldTemplate.isCloneAllowed());
					}
				}
			}
			else if ( fieldTemplate.isTypeDecimal() )
			{
				EsfString updateApiPostValue = transaction.getRecord().getStringByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
				if ( updateApiPostValue != null )
				{
					EsfDecimal updateApiPostDecimalValue = new EsfDecimal(updateApiPostValue.toString());
					EsfDecimal value = record.getDecimalByName(fieldName);
					if ( ! updateApiPostDecimalValue.equals(value) )
					{
						transaction.logActionDetail(con, "Update API: set field: " + fieldName + "; from: " + value + "; to: " + updateApiPostDecimalValue);
						record.addUpdate(fieldName, updateApiPostDecimalValue, fieldTemplate.isCloneAllowed());
					}
				}
			}
			else if ( fieldTemplate.isTypeEmailAddress() )
			{
				EsfString updateApiPostValue = transaction.getRecord().getStringByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
				if ( updateApiPostValue != null )
				{
					EsfEmailAddress updateApiPostEmailAddressValue = new EsfEmailAddress(updateApiPostValue.toString());
					EsfEmailAddress value = record.getEmailAddressByName(fieldName);
					if ( ! updateApiPostEmailAddressValue.equals(value) )
					{
						transaction.logActionDetail(con, "Update API: set field: " + fieldName + "; from: " + value + "; to: " + updateApiPostEmailAddressValue);
						record.addUpdate(fieldName, updateApiPostEmailAddressValue, fieldTemplate.isCloneAllowed());
					}
				}
			}
			else if ( fieldTemplate.isTypeFile() )
			{
				if ( tranParty == null )
				{
					transaction.logActionDetail(con,"Update API ERROR: Cannot attach file data for field: " + fieldName + "; no party was specified.");
					continue;
				}
				
				// Let's see if we have one or more file uploads set for this field name
				EsfString[] filesBase64 = transaction.getRecord().getStringsByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
				if ( filesBase64 != null && filesBase64.length > 0 )
				{
					EsfString[] fileNames = transaction.getRecord().getStringsByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,new EsfName(fieldName+FILENAME_PARAM_SUFFIX));
					EsfString[] fileMimeTypes = transaction.getRecord().getStringsByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,new EsfName(fieldName+FILEMIMETYPE_PARAM_SUFFIX));
					
					int limitNumFiles = Application.getInstance().stringToInt(fieldTemplate.getInputFormatSpec(), -1);
					
					for( int i=0; i < filesBase64.length; ++i )
					{
						byte[] fileBytes;

						EsfString fileBase64 = filesBase64[i];
						if ( fileBase64 != null && fileBase64.isNonBlank() )
						{
							fileBytes = Base64.decode(fileBase64.toPlainString());
							// If we don't have a file, skip this bad setup for initialization
							if ( fileBytes == null )
							{
								transaction.logActionDetail(con,"Update API ERROR: Invalid Base-64 encoded file data for field: " + fieldName + "; param value length: " + fileBase64.getLength());
								continue;
							}
						}
						else
						{
							transaction.logActionDetail(con,"Update API WARNING: No Base-64 encoded file data provided for field: " + fieldName);
							continue;
						}
						
						// If we're supposed to limit the number of files, ensure this isn't maxed out first
						if ( limitNumFiles > 0 )
						{
			    			List<TransactionFile> fileList = transaction.getAllTransactionFilesForDocumentField(getId(), fieldTemplate.getEsfName());
			    			if ( fileList.size() >= limitNumFiles )
			    			{
								transaction.logActionDetail(con,"Update API ERROR: Too many files for field: " + fieldName);
			    				break; // can't do any more
			    			}
						}

						// Add this file
						String fileName = i < fileNames.length ? fileNames[i].toPlainString() : null;
						if ( EsfString.isBlank(fileName) )
							fileName = fieldName.toPlainString();
						
						String fileMimeType = i < fileMimeTypes.length ? fileMimeTypes[i].toPlainString() : null;
						if ( EsfString.isBlank(fileMimeType) )
							fileMimeType = Application.CONTENT_TYPE_BINARY;
						
		            	TransactionFile tranFile = TransactionFile.Manager.createNew(
		            			transaction.getId(), 
								getId(), 
								tranParty.getId(), 
								fieldTemplate.getEsfName(), 
								fileName, 
								fileMimeType, 
								fileBytes);			
		            	transaction.addTransactionFile(tranFile);
		            	
		            	// Let's say the tran party has read this file
		            	String partyReadPathName = tranParty.getPackageVersionPartyTemplate().getEsfName() + EsfPathName.PATH_SEPARATOR + tranFile.getId().toNormalizedEsfNameString() + EsfPathName.PATH_SEPARATOR + "lastAccessTimestamp";
		            	EsfPathName fileAccessPathName = new EsfPathName(partyReadPathName);
						record.addUpdate(fileAccessPathName, new EsfDateTime(), false);
						
						List<TransactionFile> fileList = transaction.getAllTransactionFilesForDocumentField(getId(), fieldTemplate.getEsfName());
						EsfInteger value = new EsfInteger(fileList.size());
						record.addUpdate(fieldTemplate.getEsfName(), value, fieldTemplate.isCloneAllowed());

						transaction.logActionDetail(con,"Update API: Added file for field: " + fieldName + "; fileName: " + fileName);
					}
				}
			}
			else if ( fieldTemplate.isTypeFileConfirmClick() )
			{
				EsfString updateApiPostValue = transaction.getRecord().getStringByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
				if ( updateApiPostValue != null )
				{
					transaction.logActionDetail(con, "Update API WARNING: cannot set File Confirm Click field: " + fieldName);
				}
			}
			else if ( fieldTemplate.isTypeInteger() )
			{
				EsfString updateApiPostValue = transaction.getRecord().getStringByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
				if ( updateApiPostValue != null )
				{
					EsfInteger updateApiPostIntegerValue = new EsfInteger(updateApiPostValue.toString());
					EsfInteger value = record.getIntegerByName(fieldName);
					if ( ! updateApiPostIntegerValue.equals(value) )
					{
						transaction.logActionDetail(con, "Update API: set field: " + fieldName + "; from: " + value + "; to: " + updateApiPostIntegerValue);
						record.addUpdate(fieldName, updateApiPostIntegerValue, fieldTemplate.isCloneAllowed());
					}
				}
			}
			else if ( fieldTemplate.isTypeMoney() )
			{
				EsfString updateApiPostValue = transaction.getRecord().getStringByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
				if ( updateApiPostValue != null )
				{
					EsfMoney updateApiPostMoneyValue = new EsfMoney(updateApiPostValue.toString());
					EsfMoney value = record.getMoneyByName(fieldName);
					if ( ! updateApiPostMoneyValue.equals(value) )
					{
						transaction.logActionDetail(con, "Update API: set field: " + fieldName + "; from: " + value + "; to: " + updateApiPostMoneyValue);
						record.addUpdate(fieldName, updateApiPostMoneyValue, fieldTemplate.isCloneAllowed());
					}
				}
			}
			else if ( fieldTemplate.isTypeRadioButton() )
			{
				// radio buttons are just labels in a radio button group -- the group carries the data
			}
			else if ( fieldTemplate.isTypeRichTextarea() )
			{
				EsfString updateApiPostValue = transaction.getRecord().getStringByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
				if ( updateApiPostValue != null )
				{
					EsfHtml updateApiPostHtmlValue = new EsfHtml(updateApiPostValue.toString());
					EsfHtml value = record.getHtmlByName(fieldName);
					if ( ! updateApiPostHtmlValue.equals(value) )
					{
						transaction.logActionDetail(con, "Update API: set field: " + fieldName + "; from: " + value + "; to: " + updateApiPostHtmlValue);
						record.addUpdate(fieldName, updateApiPostHtmlValue, fieldTemplate.isCloneAllowed());
					}
				}
			}
			else if ( fieldTemplate.isTypeSelection() ) 
			{
				DropDownVersion ddv = transaction.getDropDownVersion(new EsfName(fieldTemplate.getInputFormatSpec()),getDocumentVersion());
				if ( ddv == null )
				{
				}
				else if ( ddv.isAllowMultiSelection() )
				{
					EsfString[] updateApiPostValue = transaction.getRecord().getStringsByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
					if ( updateApiPostValue != null )
					{
						EsfString[] values = record.getStringsByName(fieldName);
						transaction.logActionDetail(con, "Update API: set multi-select field: " + fieldName + "; from: " + values + "; to: " + updateApiPostValue);
						record.addUpdate(fieldName, updateApiPostValue, fieldTemplate.isCloneAllowed());
					}
				}
				else
				{
					EsfString updateApiPostValue = transaction.getRecord().getStringByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
					if ( updateApiPostValue != null )
					{
						EsfString value = record.getStringByName(fieldName);
						if ( ! updateApiPostValue.equals(value) )
						{
							transaction.logActionDetail(con, "Update API: set field: " + fieldName + "; from: " + value + "; to: " + updateApiPostValue);
							record.addUpdate(fieldName, updateApiPostValue, fieldTemplate.isCloneAllowed());
						}
					}
				}
			}
			else if ( fieldTemplate.isTypeSignature() )
			{
				EsfString updateApiPostValue = transaction.getRecord().getStringByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
				if ( updateApiPostValue != null )
				{
					transaction.logActionDetail(con, "Update API WARNING: cannot set Signature field: " + fieldName);
				}
			}
			else if ( fieldTemplate.isTypeSignDate() )
			{
				EsfString updateApiPostValue = transaction.getRecord().getStringByName(Transaction.TRAN_RECORD_UPDATE_API_PREFIX,fieldName);
				if ( updateApiPostValue != null )
				{
					transaction.logActionDetail(con, "Update API WARNING: cannot set Sign Date field: " + fieldName);
				}
			}
			else
			{
				_logger.debug("updateApi() - Unexpected field to update: " + fieldName);
			}
		}
	}



    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof TransactionDocument )
        {
        	TransactionDocument otherDocument = (TransactionDocument)o;
            return getId().equals(otherDocument.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(TransactionDocument o)
    {
    	return getId().compareTo(o.getId());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con)
        throws SQLException
    {
    	_logger.debug("save(con,user) on transactionId: " + transactionId + "; id: " + id + "; insert: " + doInsert());
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	record.save(con);

        	if ( doInsert() )
            {                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_transaction_document (id,transaction_id,library_document_id,library_document_version_id,record_id) VALUES (?,?,?,?,?)");
                stmt.set(id);
                stmt.set(transactionId);
                stmt.set(documentId);
                stmt.set(documentVersionId);
                stmt.set(record.getId());
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for transactionId: " + transactionId + "; id: " + id);
                    return false;
                }

                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                return true;
            }
            
            if ( hasChanged() )
            {
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on transactionId: " + transactionId + "; id: " + id + "; insert: " + doInsert());
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }

    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    
    
    synchronized boolean delete(final Connection con, final Errors errors)
        throws SQLException
    {
    	_logger.debug("delete(con) on transactionId: " + transactionId + "; id: " + id);
        
        clearLastSQLException();
    
        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of transaction document that was pending an INSERT transactionId: " + transactionId + "; id: " + id);
            objectDeleted();
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
        	record.delete(con);
        	
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_transaction_document WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            objectDeleted();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on transactionId: " + transactionId + "; id: " + id);
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete()
    {
        Errors nullErrors = null;
        return delete(nullErrors);
    }
        
   	public static class Manager
   	{
   		public static List<TransactionDocument> getAllByTransactionId(Connection con, EsfUUID transactionId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<TransactionDocument> list = new LinkedList<TransactionDocument>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id,library_document_id,library_document_version_id,record_id " +
   	        			               					"FROM esf_transaction_document WHERE transaction_id = ?" );
   	        	stmt.set(transactionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	EsfUUID docId = rs.getEsfUUID();
	            	EsfUUID docVerId = rs.getEsfUUID();
	            	EsfUUID recordId = rs.getEsfUUID();
	            	
	            	Record record = Record.Manager.getById(recordId);
	            	
	            	TransactionDocument tranDoc = new TransactionDocument(id,transactionId,docId,docVerId, record);
	            	tranDoc.setLoadedFromDb();
	            	list.add(tranDoc);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<TransactionDocument> getAllByTransactionId(EsfUUID transactionId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<TransactionDocument> docList = getAllByTransactionId(con,transactionId);
   	        	con.commit();
   	        	return docList;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   		public static TransactionDocument createNew(User user, Transaction transaction, Document document, DocumentVersion documentVersion)
   		{
   			Record record = new Record( document.getEsfName(), BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE );
   			TransactionDocument tranDoc = new TransactionDocument(transaction.getId(), document, documentVersion, record);
   			tranDoc.initializeRecord(user, transaction, document, documentVersion, INITIALIZE_RECORD.ALL_FIELDS); // initialize all fields, regardless if value set or not
   			return tranDoc;
   		}
   	    
   	} // Manager
   	
}