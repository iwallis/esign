// Copyright (C) 2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.condition;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.XmlUtil;


/**
* UpdateApiCondition is a Condition that is only true if any of the specified Update API was done
* with the matching event name.
* 
* @author Yozons, Inc.
*/
public class UpdateApiCondition 
	extends Condition
{
	private static final long serialVersionUID = 5337712958065725442L;

	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = -5772605435632620163L;

		public Spec(String eventName)
		{
			setEventName(eventName);
		}
		String eventName;
		public Spec duplicate() 
		{
			return new Spec(eventName);
		}
		public String getEventName() { return eventName; }
		public void setEventName(String v) { eventName = v==null ? "" : v.trim(); }
	}

	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates an UpdateApiCondition object
     */
	public UpdateApiCondition()
	{
	}
    
    public UpdateApiCondition(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
    	super(element,ns,_logger);
    	
		List<Element> specElementList = element.getChildren("Spec", ns);
		ListIterator<Element> iter = specElementList.listIterator();
		while (iter.hasNext()) 
		{
			Element specElement = iter.next();
			
			String eventName = specElement.getChildTextTrim("eventName", ns); 
			
			Spec spec = new Spec(eventName);
			specList.add(spec);
		}
    }

    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }

    @Override
    public UpdateApiCondition duplicate() 
    {
    	UpdateApiCondition condition = new UpdateApiCondition();
    	condition.isNegated = isNegated;
    	for( Spec spec : specList )
    		condition.specList.add( new Spec(spec.eventName) );
    	return condition;
    }

    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	// we have no party id mappings
    }

    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	// we have no document id mappings
    }

    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	// we have no document version page id mappings
    }

    @Override
    public boolean isAllowed(Connection con, TransactionContext context) throws SQLException, EsfException
    {
    	for( Spec spec : specList )
    	{
    		if ( context.isUpdateApiEvent(spec.eventName) )
    			return true;
    	}
    	
    	return super.isAllowed(con,context,false);
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 55 + super.getEstimatedLengthXml();
		for( Spec spec : specList )
			size += 40 + spec.eventName.length();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Condition type=\"UpdateApiCondition\">\n");
		super.appendXml(buf);
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <eventName>").append(XmlUtil.toEscapedXml(spec.eventName)).append("</eventName>\n");
			buf.append(" </Spec>\n");
		}
		buf.append("</Condition>\n");
		return buf;
	}
	
    @Override
    public String toString()
    {
    	StringBuilder buf = new StringBuilder(25 + (specList.size()*50));
    	buf.append(Application.getInstance().getServerMessages().getString("UpdateApiCondition.label")).append(' ');
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(", ");
    		buf.append(spec.eventName);
    	}
    	return super.toString(buf.toString());
    }

}