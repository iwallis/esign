// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.condition;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.runtime.workflow.TransactionContext;


/**
* AnyBlankCondition is a Condition that is true if any field is blank. If no values are set, it's considered any blank, too.
* 
* @author Yozons, Inc.
*/
public class AnyBlankCondition 
	extends Condition
{
	private static final long serialVersionUID = 8743624325241985695L;

	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = -6934023187744075853L;

		public Spec(EsfUUID documentId, EsfName field)
		{
			this.documentId = documentId == null ? new EsfUUID() : documentId;
			this.field = field == null ? new EsfName("") : field;
		}
		EsfUUID documentId;
		EsfName field;
		public Spec duplicate() 
		{
			return new Spec(documentId,field.duplicate());
		}
		public EsfUUID getDocumentId() { return documentId; }
		public void setDocumentId(EsfUUID v) { documentId = v; }
		public EsfName getField() { return field; }
		public void setField(EsfName v) { field = v; }
	}

	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates an AnyBlankCondition object
     */
	public AnyBlankCondition()
	{
	}
    
    public AnyBlankCondition(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
    	super(element,ns,_logger);
    	
		List<Element> specElementList = element.getChildren("Spec", ns);
		ListIterator<Element> iter = specElementList.listIterator();
		while (iter.hasNext()) 
		{
			Element specElement = iter.next();
			
			EsfUUID documentId = new EsfUUID(specElement.getChildTextTrim("documentId", ns)); 
			EsfName field = new EsfName(specElement.getChildTextTrim("field", ns)); 
			
			Spec spec = new Spec(documentId, field);
			specList.add(spec);
		}
    }

    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }

    @Override
    public AnyBlankCondition duplicate() 
    {
    	AnyBlankCondition condition = new AnyBlankCondition();
    	condition.isNegated = isNegated;
    	for( Spec spec : specList )
    		condition.specList.add( new Spec(spec.documentId, spec.field.duplicate()) );
    	return condition;
    }

    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	// we have no party id mappings
    }

    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	for( Spec spec : specList )
    	{
    		EsfUUID newDocId = documentIdMapping.get(spec.getDocumentId());
    		if ( newDocId != null )
    			spec.setDocumentId(newDocId);
    	}
    }

    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	// we have no document version page id mappings
    }

    @Override
    public boolean isAllowed(Connection con, TransactionContext context) throws SQLException, EsfException
    {
    	if ( specList.size() == 0 )
    		return super.isAllowed(con,context,true);
    	
    	for( Spec spec : specList )
    	{
    		Document document = context.getDocument(spec.getDocumentId());
    		if ( document == null )
    			context.transaction.logActionBasic(con, "ERROR: Condition '" + getEsfName() + "' wanted to check field '" + spec.getField() + "' in document id '" + spec.getDocumentId() +"' but no such document was found.");
    		else
    		{
    			FieldTemplate ft = context.getFieldTemplate(document, spec.getField());
    			// We don't really have a way to check if a file confirm click is blank or not by its value
    			// since it's party-specific.
    			if ( ft != null && ft.isTypeFileConfirmClick() )
    			{
					EsfPathName fileConfirmClickPathName = DocumentPageBean.createPartyFileConfirmClickLastAccessTimestampFieldPathName(context.pickupPartyName, ft.getEsfName());
					EsfDateTime accessDateTime = (EsfDateTime)context.transaction.getFieldValue(document.getEsfName(), fileConfirmClickPathName);
    				if ( accessDateTime == null || accessDateTime.isNull() )
    					return super.isAllowed(con, context, true);
    			}
    			else
    			{
	        		EsfValue currValue = context.transaction.getFieldValue(document.getEsfName(), spec.getField());
	        		if ( currValue == null || currValue.isNull() || EsfString.isBlank(currValue.toPlainString()) )
	        			return super.isAllowed(con,context,true);
    			}
    		}
    	}

    	return super.isAllowed(con,context,false);
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 51 + super.getEstimatedLengthXml();
		for( Spec spec : specList )
			size += 64 + spec.documentId.getEstimatedLengthXml() + spec.field.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Condition type=\"AnyBlankCondition\">\n");
		super.appendXml(buf);
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <documentId>").append(spec.documentId.toXml()).append("</documentId>\n");
			buf.append("  <field>").append(spec.field.toXml()).append("</field>\n");
			buf.append(" </Spec>\n");
		}
		buf.append("</Condition>\n");
		return buf;
	}

	
    @Override
    public String toString()
    {
    	StringBuilder buf = new StringBuilder(25 + (specList.size()*50));
    	buf.append(Application.getInstance().getServerMessages().getString("AnyBlankCondition.label")).append(' ');
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(", ");
    		buf.append(spec.field);
    	}
    	return super.toString(buf.toString());
    }
}