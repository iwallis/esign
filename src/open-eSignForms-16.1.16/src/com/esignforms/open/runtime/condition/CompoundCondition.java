// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.condition;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;


/**
* CompoundCondition is a Condition that contains subconditions (currently just AndCondition and OrCondition subclass this).
* 
* @author Yozons, Inc.
*/
public abstract class CompoundCondition 
	extends Condition
{
	private static final long serialVersionUID = 5422874896906185000L;

	LinkedList<Condition> conditions = new LinkedList<Condition>();
	
    /**
     * This creates a CompoundCondition object
     */
    public CompoundCondition()
    {
    	this(false, (List<Condition>)null);
    }
    public CompoundCondition(boolean negated, List<Condition> conditions)
    {
    	this.isNegated = negated;
    	if ( conditions != null )
    	{
    		for( Condition c : conditions )
    			this.conditions.add(c);
    	}
    }
    
    protected CompoundCondition(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
    	super(element,ns,_logger);
		List<Element> conditionElementList = element.getChildren("Condition", ns);
		ListIterator<Element> iter = conditionElementList.listIterator();
		while (iter.hasNext()) 
		{
			Element conditionElement = iter.next();
			Condition condition = Condition.createFromJdom(conditionElement, ns, _logger);
			if ( condition != null )
				conditions.add(condition);
			else
				_logger.warn("CompoundCondition() - Could not create the Condition type: " + conditionElement.getAttributeValue("type"));
		}
    }

    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	for( Condition c : conditions )
    		c.updatePackagePartyIds(partyIdMapping);
    	
    }
    
    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	for( Condition c : conditions )
    		c.updateDocumentIds(documentIdMapping);
    }


    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	for( Condition c : conditions )
    		c.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    }

    public synchronized List<Condition> getDuplicateConditions() 
    {
    	LinkedList<Condition> list = new LinkedList<Condition>();
    	for( Condition c : conditions )
    		list.add( c.duplicate() );
    	return list;
    }

    public boolean hasContainedCondition()
    {
    	return conditions.size() > 0;
    }

    public List<Condition> getContainedConditions()
    {
    	return conditions;
    }
    
    public synchronized void addCondition(Condition c)
    {
    	if ( conditions.add(c) )
    		setObjectChanged();
    }
    
    public synchronized void replaceCondition(Condition origCondition, Condition newCondition)
    {
    	ListIterator<Condition> iter = conditions.listIterator();
    	while ( iter.hasNext() )
    	{
    		Condition c = iter.next();
    		if ( c.equals(origCondition) )
    		{
    			iter.remove();
    			iter.add(newCondition);
    	    	setObjectChanged();
    			break;
    		}
    	}
    }
    
    public synchronized void removeCondition(Condition c)
    {
    	if ( conditions.remove(c) )
    		setObjectChanged();
    }
    
    public synchronized void removeAllConditions()
    {
    	if ( conditions.size() > 0 )
    	{
        	conditions.clear();
        	setObjectChanged();
    	}
    }
    
	public int getEstimatedLengthXml()
	{
		int size = super.getEstimatedLengthXml();
		for( Condition c : conditions )
			size += c.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		super.appendXml(buf);
		for( Condition c : conditions )
			c.appendXml(buf);
		return buf;
	}

	public String toDetailString()
	{
		if ( conditions.size() == 0 )
			return isNegated ? "NOT (no subconditions)" : "(no subconditions)";

		StringBuilder buf = new StringBuilder();
		buf.append('(');
		if ( conditions.size() == 1 )
		{
			Condition condition = conditions.getFirst();
    		if ( condition instanceof CompoundCondition )
    			buf.append(((CompoundCondition)condition).toDetailString());
    		else
    			buf.append(condition.toString());
		}
		else
		{
			boolean isFirst = true;
	    	for( Condition condition : conditions )
	    	{
	    		if ( isFirst )
	    			isFirst = false;
	    		else
	    			buf.append(' ').append(toString()).append(' ');
	    		if ( condition instanceof CompoundCondition )
	    			buf.append(((CompoundCondition)condition).toDetailString());
	    		else
	    			buf.append(condition.toString());
	    	}
		}
		buf.append(')');
		
    	return buf.toString();
	}
	
    public boolean isChanged()
    {
    	if ( super.isChanged() )
    		return true;
    	for( Condition c : conditions )
    	{
    		if ( c.isChanged() )
    			return true;
    	}
    	return false;
    }
    public void clearObjectChanged()
    {
    	super.clearObjectChanged();
    	for( Condition c : conditions )
    		c.clearObjectChanged();
    }

}