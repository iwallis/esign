// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.condition;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.workflow.TransactionContext;


/**
* Condition is the base class for all conditions placed on Actions and the like.
* 
* @author Yozons, Inc.
*/
public abstract class Condition implements java.io.Serializable
{
	private static final long serialVersionUID = 7955918708206265543L;

	boolean isChanged = false;
	protected boolean isNegated = false;
	
    /**
     * This creates a Condition object
     */
    public Condition()
    {
    }
    
    public Condition(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		try 
		{
			String elementName = element.getName();
			if (!"Condition".equals(elementName)) 
				throw new EsfException("The Condition tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!getClassSimpleName().equals(typeAttr)) 
				throw new EsfException("The Condition tag has type: " + typeAttr + "; expected " + getClassSimpleName() + ".");

			String negatedText = element.getChildText("negated", ns);
			isNegated = EsfBoolean.toBoolean(negatedText);
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }

    public static Condition createFromJdom(Element conditionElement, Namespace ns, com.esignforms.open.log.Logger _logger) 
    {
		if (conditionElement != null) 
		{
			String typeAttr = conditionElement.getAttributeValue("type");
			String className = "com.esignforms.open.runtime.condition."+typeAttr;
			try
			{
				// Try to instantiate a Condition object of that type
				Class<?> conditionClass = Class.forName(className);
				Constructor<?> conditionConstructor = conditionClass.getConstructor(new Class[]{Element.class,Namespace.class,com.esignforms.open.log.Logger.class});
				Condition condition = (Condition)conditionConstructor.newInstance(new Object[]{conditionElement,ns,_logger});
				return condition;
			}
			catch(ClassNotFoundException e)
			{
				_logger.warn("Condition.createFromJdom(): Could not find class: " + className,e);
			}
			catch(NoSuchMethodException e)
			{
				_logger.warn("Condition.createFromJdom(): Could not Condition(Element,Namespace,Logger) constructor for class: " + className,e);
			}
			catch(SecurityException e)
			{
				_logger.warn("Condition.createFromJdom(): Could not pass security for Condition(Element,Namespace,Logger) constructor for class: " + className,e);
			}
			catch(InvocationTargetException e)
			{
				_logger.warn("Condition.createFromJdom(): Could not invoke Condition(Element,Namespace,Logger) for class: " + className,e);
			}
			catch(IllegalAccessException e)
			{
				_logger.warn("Condition.createFromJdom(): Could not access Condition(Element,Namespace,Logger) for class: " + className,e);
			}
			catch(InstantiationException e)
			{
				_logger.warn("Condition.createFromJdom(): Could not instantiate Condition(Element,Namespace,Logger) for class: " + className,e);
			}
		} 
		return null;
    }

    public abstract Condition duplicate();
    public abstract void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping);
    public abstract void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping);
    public abstract void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping);
    
    final public boolean isNegated()
    {
    	return isNegated;
    }
    final public void setNegated(boolean v)
    {
    	if ( v != isNegated ) 
    	{
        	isNegated = v;
        	setObjectChanged();
    	}
    }
    final public boolean negate()
    {
    	isNegated = !isNegated;
       	setObjectChanged();
    	return isNegated;
    }
    
    public boolean isAllowed(Connection con, TransactionContext context) throws SQLException, EsfException
    {
    	return isAllowed(con,context,false);
    }
    // used to support basic NOT/negation in subclasses
    final protected boolean isAllowed(Connection con, TransactionContext context, boolean v) throws SQLException, EsfException
    {
    	return isNegated ? ! v : v;
    }
    
    final public boolean isNotAllowed(Connection con, TransactionContext context) throws SQLException, EsfException
    {
    	return ! isAllowed(con,context);
    }
    // used to support basic NOT/negation in subclasses
    final protected boolean isNotAllowed(Connection con, TransactionContext context, boolean v) throws SQLException, EsfException
    {
    	return ! isAllowed(con,context,v);
    }
    
    public String toString()
    {
    	return toString("ABSTRACT-CONDITION");
    }
    protected String toString(String v)
    {
    	return isNegated ? "NOT "+v : v;
    }
    
	public int getEstimatedLengthXml()
	{
		return 30;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("  <negated>").append(isNegated).append("</negated>\n");
		return buf;
	}
	
	public String toXml()
   	{
		isChanged = false; // reset after we convert to XML
   		StringBuilder buf = new StringBuilder(getEstimatedLengthXml());
   		return appendXml(buf).toString();
   	}
	public byte[] toXmlByteArray() {
		String xml = toXml();
		return EsfString.stringToBytes(xml);
	}
	
    public final EsfName getEsfName()
    {
    	return new EsfName(getClassSimpleName());
    }
    public final String getClassSimpleName()
    {
    	return getClass().getSimpleName();
    }
    
    public boolean isChanged()
    {
    	return isChanged;
    }
    public void setObjectChanged()
    {
    	isChanged = true;
    }
    public void clearObjectChanged()
    {
    	isChanged = false;
    }

}