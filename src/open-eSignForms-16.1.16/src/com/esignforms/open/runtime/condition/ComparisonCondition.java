// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.condition;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfDecimal;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfMoney;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.Document;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.XmlUtil;


/**
* ComparisonCondition is a Condition that is only true if the specified field matches a specified value according to the operator.
* 
* @author Yozons, Inc.
*/
public class ComparisonCondition 
	extends Condition
{
	private static final long serialVersionUID = -375658595615512916L;

	public static final String OPERATOR_EQUALS = "equals";
	public static final String OPERATOR_STARTS_WITH = "startsWith";
	public static final String OPERATOR_ENDS_WITH = "endsWith";
	public static final String OPERATOR_CONTAINS = "contains";
	public static final String OPERATOR_LESS_THAN = "lessThan";
	public static final String OPERATOR_LESS_THAN_EQUALS = "lessThanEquals";
	public static final String OPERATOR_GREATER_THAN = "greaterThan";
	public static final String OPERATOR_GREATER_THAN_EQUALS = "greaterThanEquals";
	public static final String OPERATOR_SAME_LENGTH = "sameLength";
	public static final String OPERATOR_SHORTER_THAN = "shorterThan";
	public static final String OPERATOR_SHORTER_THAN_OR_SAME = "shorterThanOrSame";
	public static final String OPERATOR_LONGER_THAN = "longerThan";
	public static final String OPERATOR_LONGER_THAN_OR_SAME = "longerThanOrSame";
	
	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = -5789310923464184798L;

		public Spec(EsfUUID documentId, EsfName field, String operator, String fieldValueSpec, EsfBoolean caseSensitive)
		{
			this.documentId = documentId == null ? new EsfUUID() : documentId;
			this.field = field == null ? new EsfName("") : field;
			this.operator = operator;
			this.fieldValueSpec = fieldValueSpec;
			this.caseSensitive = caseSensitive;
		}
		EsfUUID documentId;
		EsfName field;
		String operator;
		String fieldValueSpec;
		EsfBoolean caseSensitive;
		
		public Spec duplicate() 
		{
			return new Spec(documentId,field.duplicate(),operator,fieldValueSpec,caseSensitive.duplicate());
		}
		public EsfUUID getDocumentId() { return documentId; }
		public void setDocumentId(EsfUUID v) { documentId = v; }
		public EsfName getField() { return field; }
		public void setField(EsfName v) { field = v; }
		public String getOperator() { return operator; }
		public boolean isOperatorEquals() { return OPERATOR_EQUALS.equals(operator); }
		public boolean isOperatorStartsWith() { return OPERATOR_STARTS_WITH.equals(operator); }
		public boolean isOperatorEndsWith() { return OPERATOR_ENDS_WITH.equals(operator); }
		public boolean isOperatorContains() { return OPERATOR_CONTAINS.equals(operator); }
		public boolean isOperatorLessThan() { return OPERATOR_LESS_THAN.equals(operator); }
		public boolean isOperatorLessThanEquals() { return OPERATOR_LESS_THAN_EQUALS.equals(operator); }
		public boolean isOperatorGreaterThan() { return OPERATOR_GREATER_THAN.equals(operator); }
		public boolean isOperatorGreaterThanEquals() { return OPERATOR_GREATER_THAN_EQUALS.equals(operator); }
		public boolean isOperatorSameLength() { return OPERATOR_SAME_LENGTH.equals(operator); }
		public boolean isOperatorShorterThan() { return OPERATOR_SHORTER_THAN.equals(operator); }
		public boolean isOperatorShorterThanOrSame() { return OPERATOR_SHORTER_THAN_OR_SAME.equals(operator); }
		public boolean isOperatorLongerThan() { return OPERATOR_LONGER_THAN.equals(operator); }
		public boolean isOperatorLongerThanOrSame() { return OPERATOR_LONGER_THAN_OR_SAME.equals(operator); }
		public boolean isStringOnlyOperator() 
		{ 
			return isOperatorStartsWith() || isOperatorEndsWith() || isOperatorContains() || 
			       isOperatorSameLength() || isOperatorShorterThan() || isOperatorShorterThanOrSame() || isOperatorLongerThan() || isOperatorLongerThanOrSame();
		}
		public void setOperator(String v) { operator = v; }
		public String getFieldValueSpec() { return fieldValueSpec; }
		public void setFieldValueSpec(String v) { fieldValueSpec = v; /* don't trim */ }
		public boolean isCaseSensitive() { return caseSensitive.isTrue(); }
		public void setCaseSensitive(boolean v) { caseSensitive = new EsfBoolean(v); }
	}

	Spec spec = null;
	
    /**
     * This creates an ComparisonCondition object
     */
	public ComparisonCondition()
	{
	}
    
    public ComparisonCondition(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
    	super(element,ns,_logger);
    	
		Element specElement = element.getChild("Spec", ns);
		
		if ( specElement != null )
		{
			EsfUUID documentId = new EsfUUID(specElement.getChildTextTrim("documentId", ns)); 
			EsfName field = new EsfName(specElement.getChildTextTrim("field", ns)); 
			String operator = specElement.getChildTextTrim("operator", ns); 
			String fieldValueSpec = specElement.getChildTextTrim("fieldValueSpec", ns);
			EsfBoolean caseSensitive = new EsfBoolean(specElement.getChildTextTrim("caseSensitive", ns));
				
			spec = new Spec(documentId, field, operator, fieldValueSpec, caseSensitive);
		}
    }

    public Spec getSpec()
    {
    	return spec;
    }
    public void setSpec(Spec v)
    {
    	spec = v;
    	setObjectChanged();
    }

    @Override
    public ComparisonCondition duplicate() 
    {
    	ComparisonCondition condition = new ComparisonCondition();
    	condition.isNegated = isNegated;
    	if ( spec != null )
    		condition.spec = new Spec(spec.documentId, spec.field.duplicate(), spec.operator, spec.fieldValueSpec, spec.caseSensitive.duplicate());
    	return condition;
    }

    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	// we have no party id mappings
    }

    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	if ( spec != null )
    	{
    		EsfUUID newDocId = documentIdMapping.get(spec.getDocumentId());
    		if ( newDocId != null )
    			spec.setDocumentId(newDocId);
    	}
    }

    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	// we have no document version page id mappings
    }

    @Override
    public boolean isAllowed(Connection con, TransactionContext context) throws SQLException, EsfException
    {
		boolean conditionMatches = false;
		
    	if ( spec != null )
    	{
    		Document document = context.getDocument(spec.getDocumentId());
    		if ( document == null )
    			context.transaction.logActionBasic(con, "ERROR: Condition '" + getEsfName() + "' wanted to check field '" + spec.getField() + "' in document id '" + spec.getDocumentId() +"' but no such document was found.");
    		else
    		{
        		// 8/12/2014: The only known array type supported now is for a multi-select drop down, but no matter what, we've changed the base
    			// logic so that the condition only has to match ONE value if there's an ARRAY of values for it to be true.
    			EsfValue[] currValues = context.transaction.getFieldValues(document.getEsfName(), spec.getField());
        		if ( currValues == null )
        		{
        			context.transaction.logActionBasic(con, "ERROR: Condition '" + getEsfName() + "' wanted to check field '" + spec.getField() + "' in document id '" + spec.getDocumentId() +"' but no such value was found.");
        			return super.isAllowed(con,context,false);
        		}

        		String compareFieldValue = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion,spec.getFieldValueSpec());    		
        		if ( compareFieldValue == null )
        			compareFieldValue = "";
        		if ( ! spec.isCaseSensitive() )
        			compareFieldValue = compareFieldValue.toLowerCase();
        		
        		for( EsfValue currValue : currValues )
        		{
            		String currValueString = currValue.toPlainString();
            		if ( currValueString == null )
            			currValueString = "";
            		
            		if ( ! spec.isCaseSensitive() )
            			currValueString = currValueString.toLowerCase();
            		
            		if ( currValue instanceof EsfString || 
            			 currValue instanceof EsfEmailAddress || 
            			 currValue instanceof EsfName ||
            			 currValue instanceof EsfPathName ||
            			 currValue instanceof EsfUUID ||
            			 spec.isStringOnlyOperator() 
            		   )
            		{
                		if ( spec.isOperatorEquals() )
                			conditionMatches = currValueString.equals(compareFieldValue);
                		else if ( spec.isOperatorStartsWith() )
                			conditionMatches = currValueString.startsWith(compareFieldValue);
                		else if ( spec.isOperatorEndsWith() )
                			conditionMatches = currValueString.endsWith(compareFieldValue);
                		else if ( spec.isOperatorContains() )
                			conditionMatches = currValueString.contains(compareFieldValue);
                		else if ( spec.isOperatorLessThan() )
                			conditionMatches = currValueString.compareTo(compareFieldValue) < 0;
                		else if ( spec.isOperatorLessThanEquals() )
                			conditionMatches = currValueString.compareTo(compareFieldValue) <= 0;
                		else if ( spec.isOperatorGreaterThan() )
                			conditionMatches = currValueString.compareTo(compareFieldValue) > 0;
                		else if ( spec.isOperatorGreaterThanEquals() )
                			conditionMatches = currValueString.compareTo(compareFieldValue) >= 0;
               			else if ( spec.isOperatorSameLength() )
                			conditionMatches = currValueString.length() == compareFieldValue.length();
               			else if ( spec.isOperatorShorterThan() )
                			conditionMatches = currValueString.length() < compareFieldValue.length();
               			else if ( spec.isOperatorShorterThanOrSame() )
                			conditionMatches = currValueString.length() <= compareFieldValue.length();
               			else if ( spec.isOperatorLongerThan() )
                			conditionMatches = currValueString.length() > compareFieldValue.length();
               			else if ( spec.isOperatorLongerThanOrSame() )
                			conditionMatches = currValueString.length() >= compareFieldValue.length();
                   		else
                   			context.transaction.logActionBasic(con, "ERROR: Condition '" + getEsfName() + "' wanted to check field '" + spec.getField() + "' in document id '" + spec.getDocumentId() +"' but unexpected operator: " + spec.operator + "; for field's current value type: " + currValue.getType());
            		}
            		else if ( currValue instanceof EsfBoolean )
            		{
            			EsfBoolean currBoolean = (EsfBoolean)currValue;
            			EsfBoolean compareBoolean = new EsfBoolean(compareFieldValue);
            			
                		if ( spec.isOperatorEquals() )
                			conditionMatches = currBoolean.equals(compareBoolean);
                		else
                			context.transaction.logActionBasic(con, "ERROR: Condition '" + getEsfName() + "' wanted to check field '" + spec.getField() + "' in document id '" + spec.getDocumentId() +"' but unexpected operator: " + spec.operator + "; for field's current value type: " + currValue.getType());
            		}
            		else if ( currValue instanceof EsfInteger )
            		{
            			EsfInteger currInteger = (EsfInteger)currValue;
            			EsfInteger compareInteger = new EsfInteger(compareFieldValue);
            			
                		if ( spec.isOperatorEquals() )
                			conditionMatches = currInteger.equals(compareInteger);
                		else if ( spec.isOperatorLessThan() )
                			conditionMatches = currInteger.isLessThan(compareInteger);
                		else if ( spec.isOperatorLessThanEquals() )
                			conditionMatches = currInteger.isLessThanEqualTo(compareInteger);
                		else if ( spec.isOperatorGreaterThan() )
                			conditionMatches = currInteger.isGreaterThan(compareInteger);
                		else if ( spec.isOperatorGreaterThanEquals() )
                			conditionMatches = currInteger.isGreaterThanEqualTo(compareInteger);
                		else
                			context.transaction.logActionBasic(con, "ERROR: Condition '" + getEsfName() + "' wanted to check field '" + spec.getField() + "' in document id '" + spec.getDocumentId() +"' but unexpected operator: " + spec.operator + "; for field's current value type: " + currValue.getType());
            		}
            		else if ( currValue instanceof EsfMoney ) // do before EsfDecimal as we want to handle currency symbols if present
            		{
            			EsfMoney currMoney = (EsfMoney)currValue;
            			EsfMoney compareMoney = new EsfMoney(compareFieldValue);
            			
                		if ( spec.isOperatorEquals() )
                			conditionMatches = currMoney.equals(compareMoney);
                		else if ( spec.isOperatorLessThan() )
                			conditionMatches = currMoney.isLessThan(compareMoney);
                		else if ( spec.isOperatorLessThanEquals() )
                			conditionMatches = currMoney.isLessThanEqualTo(compareMoney);
                		else if ( spec.isOperatorGreaterThan() )
                			conditionMatches = currMoney.isGreaterThan(compareMoney);
                		else if ( spec.isOperatorGreaterThanEquals() )
                			conditionMatches = currMoney.isGreaterThanEqualTo(compareMoney);
                		else
                			context.transaction.logActionBasic(con, "ERROR: Condition '" + getEsfName() + "' wanted to check field '" + spec.getField() + "' in document id '" + spec.getDocumentId() +"' but unexpected operator: " + spec.operator + "; for field's current value type: " + currValue.getType());
            		}
            		else if ( currValue instanceof EsfDecimal )
            		{
            			EsfDecimal currDecimal = (EsfDecimal)currValue;
            			EsfDecimal compareDecimal = new EsfDecimal(compareFieldValue);
            			
                		if ( spec.isOperatorEquals() )
                			conditionMatches = currDecimal.equals(compareDecimal);
                		else if ( spec.isOperatorLessThan() )
                			conditionMatches = currDecimal.isLessThan(compareDecimal);
                		else if ( spec.isOperatorLessThanEquals() )
                			conditionMatches = currDecimal.isLessThanEqualTo(compareDecimal);
                		else if ( spec.isOperatorGreaterThan() )
                			conditionMatches = currDecimal.isGreaterThan(compareDecimal);
                		else if ( spec.isOperatorGreaterThanEquals() )
                			conditionMatches = currDecimal.isGreaterThanEqualTo(compareDecimal);
                		else
                			context.transaction.logActionBasic(con, "ERROR: Condition '" + getEsfName() + "' wanted to check field '" + spec.getField() + "' in document id '" + spec.getDocumentId() +"' but unexpected operator: " + spec.operator + "; for field's current value type: " + currValue.getType());
            		}
            		else if ( currValue instanceof EsfDate )
            		{
            			EsfDate currDate = (EsfDate)currValue;
            			EsfDate compareDate = EsfDate.CreateGuessFormat(compareFieldValue);
            			
                		if ( spec.isOperatorEquals() )
                			conditionMatches = currDate.equals(compareDate);
                		else if ( spec.isOperatorLessThan() )
                			conditionMatches = currDate.isBefore(compareDate);
                		else if ( spec.isOperatorLessThanEquals() )
                			conditionMatches = !currDate.isAfter(compareDate);
                		else if ( spec.isOperatorGreaterThan() )
                			conditionMatches = currDate.isAfter(compareDate);
                		else if ( spec.isOperatorGreaterThanEquals() )
                			conditionMatches = !currDate.isBefore(compareDate);
                		else
                			context.transaction.logActionBasic(con, "ERROR: Condition '" + getEsfName() + "' wanted to check field '" + spec.getField() + "' in document id '" + spec.getDocumentId() +"' but unexpected operator: " + spec.operator + "; for field's current value type: " + currValue.getType());
            		}
            		else if ( currValue instanceof EsfDateTime )
            		{
            			EsfDateTime currDateTime = (EsfDateTime)currValue;
            			EsfDateTime compareDateTime = EsfDateTime.CreateGuessFormat(compareFieldValue);
            			
                		if ( spec.isOperatorEquals() )
                			conditionMatches = currDateTime.equals(compareDateTime);
                		else if ( spec.isOperatorLessThan() )
                			conditionMatches = currDateTime.isBefore(compareDateTime);
                		else if ( spec.isOperatorLessThanEquals() )
                			conditionMatches = !currDateTime.isAfter(compareDateTime);
                		else if ( spec.isOperatorGreaterThan() )
                			conditionMatches = currDateTime.isAfter(compareDateTime);
                		else if ( spec.isOperatorGreaterThanEquals() )
                			conditionMatches = !currDateTime.isBefore(compareDateTime);
                		else
                			context.transaction.logActionBasic(con, "ERROR: Condition '" + getEsfName() + "' wanted to check field '" + spec.getField() + "' in document id '" + spec.getDocumentId() +"' but unexpected operator: " + spec.operator + "; for field's current value type: " + currValue.getType());
            		}
            		else
            		{
            			context.transaction.logActionBasic(con, "ERROR: Condition '" + getEsfName() + "' wanted to check field '" + spec.getField() + "' in document id '" + spec.getDocumentId() +"' but unexpected operator: " + spec.operator + "; for field's UNEXPECTED current value type: " + currValue.getType());
            		}
            		
            		if ( conditionMatches )
            			break;
        		} // end for loop of array values
    		} // document found
    	} // spec found
    	
		return super.isAllowed(con,context,conditionMatches);
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 93 + super.getEstimatedLengthXml();
		if ( spec != null )
			size += 123 + spec.documentId.getEstimatedLengthXml() + spec.field.getEstimatedLengthXml() + spec.operator.length() + spec.fieldValueSpec.length();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Condition type=\"ComparisonCondition\">\n");
		super.appendXml(buf);
		if ( spec != null )
		{
			buf.append(" <Spec>\n");
			buf.append("  <documentId>").append(spec.documentId.toXml()).append("</documentId>\n");
			buf.append("  <field>").append(spec.field.toXml()).append("</field>\n");
			buf.append("  <operator>").append(XmlUtil.toEscapedXml(spec.operator)).append("</operator>\n");
			buf.append("  <fieldValueSpec>").append(XmlUtil.toEscapedXml(spec.fieldValueSpec)).append("</fieldValueSpec>\n");
			buf.append("  <caseSensitive>").append(spec.caseSensitive.toXml()).append("</caseSensitive>\n");
			buf.append(" </Spec>\n");
		}
		buf.append("</Condition>\n");
		return buf;
	}
	
    @Override
    public String toString()
    {
    	int bufSize = 20;
    	if ( spec != null )
    		bufSize += spec.field.getLength() + spec.operator.length() + spec.fieldValueSpec.length();
    	StringBuilder buf = new StringBuilder(bufSize);
    	buf.append(Application.getInstance().getServerMessages().getString("ComparisonCondition.label"));
    	if ( spec != null )
    		buf.append(' ').append(spec.field).append(' ').append(spec.operator).append(' ').append(spec.fieldValueSpec);
    	return super.toString(buf.toString());
    }

}