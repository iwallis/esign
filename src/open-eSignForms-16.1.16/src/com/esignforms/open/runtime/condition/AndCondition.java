// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.condition;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.workflow.TransactionContext;


/**
* AndCondition is a CompoundCondition that is only true if all of its conditions is true.
* If there are no conditions, it's considered true.
* 
* @author Yozons, Inc.
*/
public class AndCondition 
	extends CompoundCondition
{
	private static final long serialVersionUID = -8525053771675956230L;

	/**
     * This creates a AndCondition object
     */
    public AndCondition()
    {
    	this(false, (List<Condition>)null);
    }
    public AndCondition(boolean negated, List<Condition> conditions)
    {
    	super(negated, conditions);
    }
    /*
     * Used to toggle an OR condition to an AND condition
     */
    public AndCondition(OrCondition orCondition)
    {
    	this(orCondition.isNegated, orCondition.conditions);
    }
    
    public AndCondition(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
    	super(element,ns,_logger);
    }

    public AndCondition duplicate() 
    {
    	AndCondition andCondition = new AndCondition( isNegated, super.getDuplicateConditions() );
    	return andCondition;
    }

    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	super.updatePackagePartyIds(partyIdMapping);
    	// we have no party id mappings
    }

    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	// we have no document id mappings
    }
    

    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }


    @Override
    public boolean isAllowed(Connection con, TransactionContext context) throws SQLException, EsfException
    {
    	if ( conditions.size() == 0 )
    		return super.isAllowed(con,context,true);
    	
    	for( Condition condition : conditions )
    	{
    		if ( ! condition.isAllowed(con,context) )
    			return super.isAllowed(con,context,false);
    	}
    	
    	return super.isAllowed(con,context,true);
    }

    @Override
    public String toString()
    {
    	return super.toString("AND");
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 46 + super.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Condition type=\"AndCondition\">\n");
		super.appendXml(buf);
		buf.append("</Condition>\n");
		return buf;
	}

}