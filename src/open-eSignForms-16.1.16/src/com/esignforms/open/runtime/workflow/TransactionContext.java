// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.workflow;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpSession;

import com.esignforms.open.Application;
import com.esignforms.open.config.CKEditorContext;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.prog.ButtonMessage;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentIdFieldName;
import com.esignforms.open.prog.DocumentStyleVersion;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DocumentVersionPage;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.prog.PackageVersionPartyTemplateDocumentParty;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.prog.PartyTemplateFieldTemplate;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyAssignment;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.runtime.action.Action;
import com.esignforms.open.servlet.EsfReportsAccessTransaction.EsfReportsAccessContext;
import com.esignforms.open.user.User;

/**
* TransactionContext keeps the current context for the transaction being processed.
* 
* @author Yozons, Inc.
*/
public class TransactionContext
	implements Comparable<TransactionContext>, java.io.Serializable
{
	private static final long serialVersionUID = 7751960684366040847L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TransactionContext.class);

	// Set at creation of the context when someone initiates access to a given transaction via pickup code
	public final String pickupCode;
	public final EsfName pickupPartyName;
	public final EsfUUID transactionId;
	public User user; // we may not know the user at context creation time if not logged in then, but login is required
    public boolean isApiMode = false; // Set for API-based requests
    public String ipOnlyAddress; // set to the user's IP address when it's known
    public String ipHost; // set to the user's IP address and host name when it's known
    public String userAgent; // set to the user's browser (user-agent)
    public String redirectToUrl; // set to an URL to redirect to when it makes sense
    
    // If set, this context is related to the ESF_reports_access
    EsfReportsAccessContext reportsAccessContext;
    
    // If set, this context was created in response to a transaction timer expiring
    // and won't have a user or other party-related/document-related information.
    String expiredTimerName;
    
    // If set, this context was created in response to a transaction update API call. If none provided, it will be "UpdateAPI".
    String updateApiEventName;
	
	// Runtime elements
	public Transaction transaction;
	public TransactionTemplate transactionTemplate;
	public TransactionParty transactionParty; // the transaction party/assignment (null if no known party)
	public List<TransactionPartyDocument> transactionPartyDocuments; // the party's status on all transaction docs party has access to (null if no known party)
	public String capturedHtml;
	
	// Runtime elements about the current context step (the runtime document and party)
	public TransactionPartyDocument currTransactionPartyDocument; // the party's status on the current transaction document
	public TransactionDocument currTransactionDocument; // the document working on now

	// Config elements (derived from transaction)
	public Package pkg;
	public PackageVersion pkgVersion;
	public PackageVersionPartyTemplate pkgVersionPartyTemplate; // the package party
	public List<Document> documents; // all documents in the package; the party may not have access to them all, but documents may retrieve data from other documents, even if the party can't access that doc itself
	public TreeMap<EsfUUID,EsfUUID> documentPartyIdToPackagePartyIdMapping; // maps all document party ids to the package party id (such as for converting document programming rules into package programming rules)
	public Document pkgDisclosureDocument; // the package+disclosure document
	public ButtonMessage pkgButtonMessage; // the button message
	
	// Config elements about the current context step (the configured document)
	public Document currDocument; // the document working on now
	public Library currLibrary; // the library the current document belongs to
	public DocumentVersion currDocumentVersion; // the version of the document working on now
	public DocumentVersionPage currDocumentVersionPage; // the page in the document version working on now (it is null when in page review of a multi-page document version)
	public List<DocumentVersionPage> currEditDocumentVersionPages; // the pages in the document version that can be filled in (edit mode)
	public DocumentVersionPage prevEditDocumentVersionPage; // calculated previous edit page, null if none
	public DocumentVersionPage nextEditDocumentVersionPage; // calculated next edit page, null if none
	public List<PartyTemplate> currDocumentVersionPartyList; // the document version parties -- the parties as known to the document version; could be empty if the package party is not mapped to a defined party in the document
	public List<EsfName> currDocumentVersionPartyNameList; // if the document version party is known (is defined), this is the document version party name as known in the document version, or is just giving view-only access
	public Map<EsfName,FieldTemplate> currDocumentVersionFieldTemplateMap; // all of the fields defined in the document version
	public Map<EsfName,PartyTemplateFieldTemplate> currDocumentVersionPartyFieldTemplateMap; // just those fields that party is given access to on a given page
	
	// Used to show navigation like Document 1 of 2, Page 3 of 4
	public boolean hasReviewOnlyPages; // when true, we have an extra "review only" page for a multi-page document
	public int documentNumber;
	public int numDocuments;
	public int pageNumber;
	public int numPages;
	
	// Used to expand ${document:page.number}. Previously we did this by using the dynamic page numbers, but this doesn't work 
	// for multi-page review mode since there's only one page (the "last page") but if their previous pages all use ${document:page.number}
	// they will all show only that page.
	public int documentPageNumber = 0; // This is set by the JSP as each page is output, it sets it to the current page number. So for multi-page review, it will actually change value as the JSP page is processed.
	
	private CKEditorContext ckeditorContext; // for the rich text field
	
	// Hack for APS_EIP pseudo action processing on document
	Action documentCompletedEventPseudoAction;
	public Action getDocumentCompletedEventPseudoAction()
	{
		return documentCompletedEventPseudoAction;
	}
	public boolean hasDocumentCompletedEventPseudoAction()
	{
		return documentCompletedEventPseudoAction != null;
	}
	public void setDocumentCompletedEventPseudoAction(Action v)
	{
		documentCompletedEventPseudoAction = v;
	}
    
	
	public TransactionContext(User user, String pickupCode, EsfName pickupPartyName, EsfUUID transactionId, String ipOnlyAddress, String ipHost, String userAgent)
	{
		this(user,null,pickupCode,pickupPartyName,transactionId,ipOnlyAddress,ipHost,userAgent);
	}
	
    // reportsAccessContext is non-null for access via reports
	public TransactionContext(User user, EsfReportsAccessContext reportsAccessContext, String pickupCode, EsfName pickupPartyName, EsfUUID transactionId, String ipOnlyAddress, String ipHost, String userAgent)
	{
		this.pickupCode = pickupCode;
		this.pickupPartyName = pickupPartyName;
		this.transactionId = transactionId;
		this.user = user;
		this.ipOnlyAddress = ipOnlyAddress;
		this.ipHost = ipHost;
		this.userAgent = userAgent;
		this.documentNumber = this.numDocuments = this.pageNumber = this.numPages = 0;
		this.hasReviewOnlyPages = false;
		this.reportsAccessContext = reportsAccessContext;

		
		// Load derived objects for this context if possible; when not a user/party operating there will be no party name
		refreshTransaction();
		if ( transaction != null )
		{
			// For non-party based transaction processing (like canceling a transaction via the reports), we'll have no pickup party
			if ( pickupPartyName != null )
			{
				if ( pkgVersionPartyTemplate.isLandingPageFirstDoc() )
					goToFirstDocument();
				else if ( pkgVersionPartyTemplate.isLandingPageFirstToDoOrFirstDoc() )
					goToFirstDocumentToDoOrFirstDoc();
				else if ( pkgVersionPartyTemplate.isLandingPageFirstToDoOrPackage() )
					goToFirstDocumentToDoOrPackage();
				else 
					goToPackageDocument();
			}
		}
	}
	
	/**
	 * This constructor is used for non-party access to a transaction context.
	 * @param user
	 * @param transactionId
	 */
	public TransactionContext(EsfUUID transactionId, String expiredTimerName)
	{
		this(null,null,null,null,transactionId,null,null,null);
		this.expiredTimerName = expiredTimerName;
	}
	
	/**
	 * This constructor is used for handling an Update API access to a transaction context.
	 * @param user the EsfUser who is authorized to use the Update API.
	 * @param transactionId the EsfUUID of the transaction updated
	 * @param updateApiEventName the String name of this event
	 */
	public TransactionContext(User user, EsfUUID transactionId, String updateApiEventName)
	{
		this(user,null,null,null,transactionId,null,null,null);
		this.updateApiEventName = updateApiEventName;
	}
	
	/**
	 * This constructor is used for non-party access to a transaction context.
	 * @param user
	 * @param transactionId
	 */
	public TransactionContext(User user, EsfUUID transactionId)
	{
		this(user,null,null,null,transactionId,null,null,null);
	}
	
	// Creates a "shallow" duplicate so we can modify it and such without changing a copy like it (that would also be in our cache)
	public TransactionContext(TransactionContext other)
	{
		pickupCode = other.pickupCode;
		pickupPartyName = other.pickupPartyName;
		transactionId = other.transactionId;
		user = other.user;
		isApiMode = other.isApiMode;
		ipOnlyAddress = other.ipOnlyAddress;
		ipHost = other.ipHost;
		userAgent = other.userAgent;
		redirectToUrl = other.redirectToUrl;
		
		reportsAccessContext = other.reportsAccessContext;

		transaction = other.transaction;
		transactionTemplate = other.transactionTemplate;
		transactionParty = other.transactionParty;
		transactionPartyDocuments = other.transactionPartyDocuments;
		capturedHtml = other.capturedHtml;
		
		currTransactionPartyDocument = other.currTransactionPartyDocument;
		currTransactionDocument = other.currTransactionDocument;

		pkg = other.pkg;
		pkgVersion = other.pkgVersion;
		pkgVersionPartyTemplate = other.pkgVersionPartyTemplate;
		pkgDisclosureDocument = other.pkgDisclosureDocument;
		pkgButtonMessage = other.pkgButtonMessage;
		documents = other.documents;
		documentPartyIdToPackagePartyIdMapping = other.documentPartyIdToPackagePartyIdMapping;
		
		currDocument = other.currDocument;
		currLibrary = other.currLibrary;
		currDocumentVersion = other.currDocumentVersion;
		currDocumentVersionPage = other.currDocumentVersionPage;
		currEditDocumentVersionPages = other.currEditDocumentVersionPages;
		prevEditDocumentVersionPage = other.prevEditDocumentVersionPage;
		nextEditDocumentVersionPage = other.nextEditDocumentVersionPage;
		currDocumentVersionPartyList = other.currDocumentVersionPartyList;
		currDocumentVersionPartyNameList = other.currDocumentVersionPartyNameList;
		currDocumentVersionFieldTemplateMap = other.currDocumentVersionFieldTemplateMap;
		currDocumentVersionPartyFieldTemplateMap = other.currDocumentVersionPartyFieldTemplateMap;
		
		hasReviewOnlyPages = other.hasReviewOnlyPages;
		documentNumber = other.documentNumber;
		numDocuments = other.numDocuments;
		pageNumber = other.pageNumber;
		numPages = other.numPages;
		
		ckeditorContext = other.ckeditorContext;
	}
	
	public void refreshTransaction()
	{
		transaction = Transaction.Manager.getById(transactionId);
		if ( transaction != null )
		{
			transactionTemplate = transaction.getTransactionTemplate();
			pkg = transaction.getPackage();
			pkgVersion = transaction.getPackageVersion();
			
			documents = new LinkedList<Document>();
			for ( TransactionDocument tranDoc : transaction.getAllTransactionDocuments() )
			{
				Document doc = Document.Manager.getById(tranDoc.getDocumentId());
				if ( doc == null )
					_logger.error("refreshTransaction() - Failed to find transaction document's documentId: " + tranDoc.getDocumentId() + "; transaction id: " + transaction.getId());
				else
					documents.add( doc );
			}
			
			documentPartyIdToPackagePartyIdMapping = new TreeMap<EsfUUID,EsfUUID>();
	    	for( PackageVersionPartyTemplate pvpt : pkgVersion.getPackageVersionPartyTemplateList() ) 
	    	{
	    		for( PackageVersionPartyTemplateDocumentParty pvptdp : pvpt.getPackageVersionPartyTemplateDocumentParties() )
	    		{
	    			if ( ! pvptdp.isDocumentPartyTemplateView() )
	    			{
	    				TransactionDocument tranDoc = transaction.getTransactionDocumentByDocumentId(pvptdp.getDocumentId());
	    				if ( tranDoc == null )
	    					_logger.error("refreshTransaction() - Failed to find the transaction document for documentId: " + pvptdp.getDocumentId() + "; for package version party template: " + pvptdp.getDocumentPartyTemplateName() + "; transaction id: " + transaction.getId());
	    				else
		    			{
		    				DocumentVersion docVer = DocumentVersion.Manager.getById(tranDoc.getDocumentVersionId());
		    				if ( docVer == null )
		    					_logger.error("refreshTransaction() - Failed to find transaction document's documentVersionId: " + tranDoc.getDocumentVersionId() + "; for package version party template: " + pvptdp.getDocumentPartyTemplateName() + "; transaction id: " + transaction.getId());
		    				else
		    				{
		    					for( PartyTemplate pt : docVer.getPartyTemplateList() )
		    					{
		    						if ( pt.getEsfName().equals(pvptdp.getDocumentPartyTemplateName()) )
		    						{
		    							documentPartyIdToPackagePartyIdMapping.put(pt.getId(), pvptdp.getPackageVersionPartyTemplateId());
		    							break;
		    						}
		    					}
		    				}
		    			}
	    			}
	    		}
	    	}
			
			// For non-party based transaction processing (like canceling a transaction via the reports), we'll have no pickup party
			if ( pickupPartyName != null )
			{
				transactionParty = transaction.getTransactionParty(pickupPartyName);
				if ( transactionParty == null )
					_logger.error("refreshTransaction() - Failed to find transaction party with pickupPartyName: " + pickupPartyName + "; transaction id: " + transaction.getId());
				else
				{
					transactionPartyDocuments = transactionParty.getNonSkippedTransactionPartyDocuments(); // we don't want any documents that have been skipped for this party
					numDocuments = transactionPartyDocuments.size();
				}
				
				// For reports access, we only let them see the documents that the report allows
				if ( isEsfReportsAccess() && reportsAccessContext.getReportTemplate().hasLimitedDocuments() && transactionPartyDocuments != null && transactionPartyDocuments.size() > 0 )
				{
					ListIterator<TransactionPartyDocument> iter = transactionPartyDocuments.listIterator();
					while( iter.hasNext() )
					{
						TransactionPartyDocument tpd = iter.next();
						TransactionDocument tranDoc = transaction.getTransactionDocument(tpd.getTransactionDocumentId());
						if ( tranDoc == null || ! reportsAccessContext.getReportTemplate().isDocumentAllowed(tranDoc.getDocumentId()) )
							iter.remove();
					}
				}
				
				pkgVersionPartyTemplate = pkgVersion.getPackageVersionPartyTemplateByName(pickupPartyName);
				if ( pkgVersionPartyTemplate == null )
					_logger.error("refreshTransaction() - Failed to find package version party template with pickupPartyName: " + pickupPartyName + "; transaction id: " + transaction.getId());
				else
				{
					pkgDisclosureDocument = pkgVersionPartyTemplate.hasOverridePackageDocument() ? pkgVersionPartyTemplate.getOverridePackageDocument() : pkgVersion.getPackageDocument();
					if ( pkgVersionPartyTemplate.hasOverridePackageDocument() && pkgDisclosureDocument == null ) 
					{
						_logger.error("refreshTransaction() - Failed to find package version party's override package+disclosure document with pickupPartyName: " + pickupPartyName + "; transaction id: " + transaction.getId() + "; package: " + pkg.getPathName());
						pkgDisclosureDocument = pkgVersion.getPackageDocument();
						if ( pkgDisclosureDocument == null )
						{
							_logger.error("refreshTransaction() - Also failed to find package version package+disclosure document with pickupPartyName: " + pickupPartyName + "; transaction id: " + transaction.getId() + "; package: " + pkg.getPathName());
							pkgDisclosureDocument = Document.Manager.getStandardPackageDocument();
						}
					}

					pkgButtonMessage = pkgVersionPartyTemplate.hasOverrideButtonMessage() ? pkgVersionPartyTemplate.getOverrideButtonMessage() : pkgVersion.getButtonMessage();
					if ( pkgVersionPartyTemplate.hasOverrideButtonMessage() && pkgButtonMessage == null ) 
					{
						_logger.error("refreshTransaction() - Failed to find package version party's override button message with pickupPartyName: " + pickupPartyName + "; transaction id: " + transaction.getId() + "; package: " + pkg.getPathName());
						pkgButtonMessage = pkgVersion.getButtonMessage();
						if ( pkgButtonMessage == null )
						{
							_logger.error("refreshTransaction() - Also failed to find package version button message with pickupPartyName: " + pickupPartyName + "; transaction id: " + transaction.getId() + "; package: " + pkg.getPathName());
							pkgButtonMessage = ButtonMessage.Manager.getDefault();
						}
					}
				}
				
				
				// If we already show cached values for current stuff, let's refresh those, too...
				// Unlike setupForCurrDocument(), we try to set any existing values to the potentially new object references discovered through the transaction.
				// ******* Want to keep refreshTransaction() and setupForCurrDocument() in sync. ***********
				if ( currTransactionPartyDocument != null )
				{
					boolean found = false;
					for( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
					{
						if ( tranPartyDoc.equals(currTransactionPartyDocument) )
						{
							currTransactionPartyDocument = tranPartyDoc;
							found = true;
							break;
						}
					}
					
					if ( ! found )
					{
						_logger.error("refreshTransaction() - Failed to refresh currTransactionPartyDocument; transaction id: " + transaction.getId() + 
								"; currTransactionPartyDocument.getTransactionDocumentId(): " + currTransactionPartyDocument.getTransactionDocumentId() +
								"; currTransactionPartyDocument.getTransactionPartyId(): " + currTransactionPartyDocument.getTransactionPartyId());
					}
					else
					{
						currTransactionDocument = transaction.getTransactionDocument(currTransactionPartyDocument.getTransactionDocumentId());
						if ( currTransactionDocument == null )
							_logger.error("refreshTransaction() - Failed to refresh currTransactionDocument using currTransactionPartyDocument.getTransactionDocumentId(): " + currTransactionPartyDocument.getTransactionDocumentId() + "; transaction id: " + transaction.getId());
						else
						{
							currDocument = getDocument(currTransactionDocument.getDocumentId());
							if ( currDocument == null )
								_logger.error("refreshTransaction() - Failed to refresh currDocument using currTransactionDocument.getDocumentId(): " + currTransactionDocument.getDocumentId() + "; transaction id: " + transaction.getId());
						}
					}
				}
				
				if ( currDocument != null )
				{
					currLibrary = currDocument.getLibrary();
					if ( currTransactionDocument == null && ! isPackageDocument() )
						_logger.error("refreshTransaction() - SHOULD NOT HAPPEN: No currTransactionDocument and not the package document; document id: " + currDocument.getId() + "; transaction id: " + transaction.getId());						
					if ( isPackageDocument() )  // package document does not have a transaction document
						currDocumentVersion = transaction.doProductionResolve() ? currDocument.getProductionDocumentVersion() : currDocument.getTestDocumentVersion();
					else
						currDocumentVersion = DocumentVersion.Manager.getById(currTransactionDocument.getDocumentVersionId());
				}

				if ( currDocumentVersion != null )
				{
					currEditDocumentVersionPages = currDocumentVersion.getEditPages();
					
					if ( currDocumentVersionPage != null )
					{
						boolean found = false;
						for( DocumentVersionPage p : currEditDocumentVersionPages )
						{
							if ( p.equals(currDocumentVersionPage) )
							{
								currDocumentVersionPage = p;
								found = true;
								break;
							}
						}
						if ( ! found )
							_logger.error("refreshTransaction() - Failed to refresh currDocumentVersionPage id: " + currDocumentVersionPage.getId() + "; transaction id: " + transaction.getId());
					}
					
					if ( prevEditDocumentVersionPage != null )
					{
						boolean found = false;
						for( DocumentVersionPage p : currEditDocumentVersionPages )
						{
							if ( p.equals(prevEditDocumentVersionPage) )
							{
								prevEditDocumentVersionPage = p;
								found = true;
								break;
							}
						}
						if ( ! found )
							_logger.error("refreshTransaction() - Failed to refresh prevEditDocumentVersionPage id: " + prevEditDocumentVersionPage.getId() + "; transaction id: " + transaction.getId());
					}
					
					if ( nextEditDocumentVersionPage != null )
					{
						boolean found = false;
						for( DocumentVersionPage p : currEditDocumentVersionPages )
						{
							if ( p.equals(nextEditDocumentVersionPage) )
							{
								nextEditDocumentVersionPage = p;
								found = true;
								break;
							}
						}
						if ( ! found )
							_logger.error("refreshTransaction() - Failed to refresh nextEditDocumentVersionPage id: " + nextEditDocumentVersionPage.getId() + "; transaction id: " + transaction.getId());
					}
					
					currDocumentVersionPartyList = pkgVersion.getDocumentVersionPartyTemplates(currDocumentVersion,pkgVersionPartyTemplate);
					currDocumentVersionPartyNameList = new LinkedList<EsfName>();
					for( PartyTemplate pt : currDocumentVersionPartyList )
						currDocumentVersionPartyNameList.add(pt.getEsfName());
					currDocumentVersionFieldTemplateMap = currDocumentVersion.getFieldTemplateMap();
					currDocumentVersionPartyFieldTemplateMap = createDocumentVersionPartyFieldTemplateMap();
					
					hasReviewOnlyPages = currDocumentVersion.hasReviewOnlyPages();
					numPages = currEditDocumentVersionPages.size();
				} // end if currDocumentVersion != null
			} // end if pickupPartyName != null
			else
			{
				transactionParty = null;
				transactionPartyDocuments = null;
			}
		} // end if transaction != null
		else
		{
			transactionTemplate = null;
			transactionParty = null;
			transactionPartyDocuments = null;
		}
	}
	
	public boolean isEsfReportsAccess() 
	{
		return reportsAccessContext != null;
	}
	public boolean isEsfReportsAccessForView() 
	{
		return isEsfReportsAccess() && reportsAccessContext.isForView();
	}
	public boolean isEsfReportsAccessForUpdate() 
	{
		return isEsfReportsAccess() && reportsAccessContext.isForUpdate();
	}
	
	public boolean hasTimerExpired() 
	{
		return EsfString.isNonBlank(expiredTimerName);
	}
	public boolean isTimerExpired(String timerName)
	{
		if ( hasTimerExpired() )
			return expiredTimerName.equalsIgnoreCase(timerName);
		return false;
	}
	public String getExpiredTimerName()
	{
		return expiredTimerName;
	}
	public void setTimerExpiredWhenTransactionCanceled()
	{
		expiredTimerName += "-Canceled";
	}
	public void setTimerExpiredWhenTransactionSuspended()
	{
		expiredTimerName += "-Suspended";
	}
	
	public boolean hasUpdateApiEventName() 
	{
		return EsfString.isNonBlank(updateApiEventName);
	}
	public boolean isUpdateApiEvent(String eventName)
	{
		if ( hasUpdateApiEventName() )
			return updateApiEventName.equalsIgnoreCase(eventName);
		return false;
	}
	public String getUpdateApiEventName()
	{
		return updateApiEventName;
	}
	public void setUpdateApiWhenTransactionCanceled()
	{
		updateApiEventName += "-Canceled";
	}
	public void setUpdateApiWhenTransactionSuspended()
	{
		updateApiEventName += "-Suspended";
	}
	
	public void setupForCurrDocument()
	{
		// Unlike refreshTransaction(), this routine sets up a fresh loading of a document (like we start on page 1, have no next/prev pages, etc.
		// ******* Want to keep refreshTransaction() and setupForCurrDocument() in sync. ***********

		// We (should) always have a current document set since we need something to show when a party picks up a transaction
		currLibrary = currDocument.getLibrary();
		if ( currTransactionDocument == null && ! isPackageDocument() )
			_logger.error("setupForCurrDocument() - SHOULD NOT HAPPEN: No currTransactionDocument and not the package document; document id: " + currDocument.getId() + "; transaction id: " + transaction.getId());						
		if ( isPackageDocument() ) // package document does not have a transaction document
			currDocumentVersion = transaction.doProductionResolve() ? currDocument.getProductionDocumentVersion() : currDocument.getTestDocumentVersion();
		else
			currDocumentVersion = DocumentVersion.Manager.getById(currTransactionDocument.getDocumentVersionId());
		
		if ( currDocumentVersion == null ) // configuration error 
		{
			Application.getInstance().err("Failed to find a document version for transaction id: " + transaction.getId() + "; transactionTemplateId: " + transaction.getTransactionTemplateId() +
					"; productionResolve: " + transaction.doProductionResolve() + "; currDocument id: " + currDocument.getId() + "; currDocument: " + currDocument.getEsfName());
			currLibrary = null;
			currDocumentVersionPage = prevEditDocumentVersionPage = nextEditDocumentVersionPage = null;
			currEditDocumentVersionPages = new LinkedList<DocumentVersionPage>(); // none
			currDocumentVersionPartyList = null;
			currDocumentVersionPartyNameList = null;
			currDocumentVersionFieldTemplateMap = null;
			currDocumentVersionPartyFieldTemplateMap = null;
			documentNumber = numDocuments = pageNumber = numPages = 0;
			hasReviewOnlyPages = false;
			return;
		}
		currEditDocumentVersionPages = currDocumentVersion.getEditPages();
		currDocumentVersionPage = currEditDocumentVersionPages.get(0);
		prevEditDocumentVersionPage = null;
		nextEditDocumentVersionPage = currEditDocumentVersionPages.size() > 1 ? currEditDocumentVersionPages.get(1) : null;
		currDocumentVersionPartyList = pkgVersion.getDocumentVersionPartyTemplates(currDocumentVersion,pkgVersionPartyTemplate);
		currDocumentVersionPartyNameList = new LinkedList<EsfName>();
		for( PartyTemplate pt : currDocumentVersionPartyList )
			currDocumentVersionPartyNameList.add(pt.getEsfName());
		currDocumentVersionFieldTemplateMap = currDocumentVersion.getFieldTemplateMap();
		currDocumentVersionPartyFieldTemplateMap = createDocumentVersionPartyFieldTemplateMap();
		
		hasReviewOnlyPages = currDocumentVersion.hasReviewOnlyPages();
		numPages = currEditDocumentVersionPages.size();
		
		if ( isPackageDocument() )
		{
			currTransactionPartyDocument = null;
			currTransactionDocument = null;
		}
		else
		{
			documentNumber = 0;
			for( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
			{
				++documentNumber;
				currTransactionDocument = transaction.getTransactionDocument(tranPartyDoc.getTransactionDocumentId());
				if ( currTransactionDocument.getDocumentId().equals(currDocument.getId()) )
				{
					currTransactionPartyDocument = tranPartyDoc;
					break;
				}
			}

			// we fix up the num pages for multipage documents in DocumentPageBean as the extra review page is only available for multi-page docs that are not in view only mode
			if ( isMultiPageViewOnlyNonParty() ) // this view only non-party only sees the final review page
			{
				numPages = 1;
				goToMultiPageReviewPage();
			}
		}
	}
	
	public void goToPageNumber(int gotoPageNumber) // the pageNumber starts with 1 while our list index starts with 0
	{
		if ( currEditDocumentVersionPages != null )
		{
			int numEditPages = currEditDocumentVersionPages.size();
			int lastPageIndex = numEditPages - 1;
			int pageIndex = 0;

			// Let's scan our edit pages for the matching page number
			for( int i=0; i < numEditPages; ++i )
			{
				DocumentVersionPage p = currEditDocumentVersionPages.get(i);
				if ( p != null && p.getPageNumber() == gotoPageNumber )
				{
					pageIndex = i;
					break;
				}
			}
			
			currDocumentVersionPage = currEditDocumentVersionPages.get(pageIndex);
			prevEditDocumentVersionPage = pageIndex > 0 ? currEditDocumentVersionPages.get(pageIndex-1) : null;
			nextEditDocumentVersionPage = pageIndex < lastPageIndex ? currEditDocumentVersionPages.get(pageIndex+1) : null;
			currDocumentVersionPartyFieldTemplateMap = createDocumentVersionPartyFieldTemplateMap(); // get the fields for this new page
			pageNumber = pageIndex+1;
		}
	}
	
	// This routine basically finds the literal page mentioned so that if it's a review-only page, we'll
	// go to the multi-page review page otherwise we'll use our normal page jump routine.
	public void goToPageNumberForTesting(int gotoPageNumber) // the pageNumber starts with 1 while our list index starts with 0
	{
		// Get the literal page number regardless of whether it's edit/review. But if it's review only
		// then we'll show the review mode.
		if ( currDocumentVersion != null )
		{
			List<DocumentVersionPage> allPages = currDocumentVersion.getPages();
			int numPages = allPages.size();
			int pageIndex = (gotoPageNumber < 1 || gotoPageNumber > numPages) ? 0 : gotoPageNumber-1;
			numPages = currDocumentVersion.getNumEditPages();
			currDocumentVersionPage = allPages.get(pageIndex);
			gotoPageNumber = pageIndex + 1;
			if ( currDocumentVersionPage.isReviewOnly() )
				goToMultiPageReviewPage();
			else
			{ 
				// Calculate the "edit page number" where our found page lands (there could be review-only pages defined ahead of this page)
				gotoPageNumber = 1;
				for( DocumentVersionPage checkPage : allPages )
				{
					if ( checkPage.equals(currDocumentVersionPage) )
					{
						goToPageNumber(gotoPageNumber);
						break;
					}
					else if ( checkPage.isEdit() )
						++gotoPageNumber;
				}
			}
		}
	}
	
	public boolean hasNextEditPage()
	{
		return nextEditDocumentVersionPage != null;
	}
	public void goToNextEditPage()
	{
		if ( hasNextEditPage() )
			goToPageNumber(nextEditDocumentVersionPage.getPageNumber());
	}

	public boolean hasPreviousEditPage()
	{
		return prevEditDocumentVersionPage != null;
	}
	public void goToPreviousEditPage()
	{
		if ( hasPreviousEditPage() )
			goToPageNumber(prevEditDocumentVersionPage.getPageNumber());
	}
	
	// This is a pseudo page for multi-page document versions
	public void goToMultiPageReviewPage()
	{
		nextEditDocumentVersionPage = prevEditDocumentVersionPage = currDocumentVersionPage = null;
		pageNumber = numPages;
	}
	public boolean isMultiPageReviewPage()
	{
		return currDocumentVersionPage == null && currDocumentVersion != null && currDocumentVersion.isMultiPage();
	}
	public boolean isRegularPage()
	{
		return currDocumentVersionPage != null;
	}
	
	public boolean isMultiPageDocument()
	{
		return currDocumentVersion != null && currDocumentVersion.isMultiPage();
	}

	public boolean hasRedirectToUrl()
	{
		return EsfString.isNonBlank(redirectToUrl);
	}
	public String getAndClearRedirectToUrl()
	{
		String url = redirectToUrl;
		redirectToUrl = null;
		return url;
	}
	public void setRedirectToUrl(String url)
	{
		redirectToUrl = url;
	}
	
	public void goToPackageDocument()
	{
		currDocument = pkgDisclosureDocument;
		setupForCurrDocument();
	}
	
	public void goToFirstDocument()
	{
		if ( transactionPartyDocuments == null || transactionPartyDocuments.size() == 0 )
			goToPackageDocument();
		else
		{
			currTransactionPartyDocument = transactionPartyDocuments.get(0);
			currTransactionDocument = transaction.getTransactionDocument(currTransactionPartyDocument.getTransactionDocumentId()); // holds our data about this document
			currDocument = getDocument(currTransactionDocument.getDocumentId());
			pageNumber = 1;
			setupForCurrDocument();
		}
	}
	
	public void goToFirstDocumentToDoOrFirstDoc()
	{
		if ( transactionParty.isCompleted() )
			goToFirstDocument();
		else
		{
			for ( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
			{
				if ( tranPartyDoc.isWorkNeeded() || isEsfReportsAccessForUpdate() )
				{
					currTransactionPartyDocument = tranPartyDoc;
					currTransactionDocument = transaction.getTransactionDocument(currTransactionPartyDocument.getTransactionDocumentId()); // holds our data about this document
					currDocument = getDocument(currTransactionDocument.getDocumentId());
					pageNumber = 1;
					setupForCurrDocument();
					return;
				}
			}
			goToFirstDocument();
		}
	}
	
	public void goToFirstDocumentToDoOrPackage()
	{
		if ( transactionParty.isCompleted() )
			goToPackageDocument();
		else
			goToFirstDocumentToDoOrFirstDoc();
	}
	
	public void goToNextDocumentToDo()
	{
		if ( transactionParty.isCompleted() || currTransactionPartyDocument == null )
			goToFirstDocument();
		else
		{
			for ( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
			{
				if( (tranPartyDoc.isWorkNeeded() || isEsfReportsAccessForUpdate()) && tranPartyDoc.getDocumentNumber() > currTransactionPartyDocument.getDocumentNumber() )
				{
					currTransactionPartyDocument = tranPartyDoc;
					currTransactionDocument = transaction.getTransactionDocument(currTransactionPartyDocument.getTransactionDocumentId()); // holds our data about this document
					currDocument = getDocument(currTransactionDocument.getDocumentId());
					pageNumber = 1;
					setupForCurrDocument();
					break;
				}
			}
		}
	}
	public boolean hasNextDocumentToDo()
	{
		if ( transaction.isEndState() || transaction.isSuspended() || transactionParty.isCompleted() || currTransactionPartyDocument == null )
			return false;
		for ( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
		{
			if( tranPartyDoc.isWorkNeeded() )
				return true;
		}
		return false;
	}
	public boolean hasAnyDocumentToDo()
	{
		if ( transaction.isEndState() || transaction.isSuspended() )
			return false;
		for ( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
		{
			if( tranPartyDoc.isWorkNeeded() || isEsfReportsAccessForUpdate() )
				return true;
		}
		return false;
	}
	
	
	public void goToNextDocument()
	{
		if ( currTransactionPartyDocument == null )
			goToFirstDocument();
		else
		{
			for ( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
			{
				// If for any reason we find a previous document that needs work, we'll return to that document now, otherwise
				// we'll continue until we at a document beyond where we are now.
				if( (transactionParty.isActive() && tranPartyDoc.isWorkNeeded()) || isEsfReportsAccessForUpdate() || tranPartyDoc.getDocumentNumber() > currTransactionPartyDocument.getDocumentNumber() )
				{
					currTransactionPartyDocument = tranPartyDoc;
					currTransactionDocument = transaction.getTransactionDocument(currTransactionPartyDocument.getTransactionDocumentId()); // holds our data about this document
					currDocument = getDocument(currTransactionDocument.getDocumentId());
					pageNumber = 1;
					setupForCurrDocument();
					break;
				}
			}
		}
	}
	public boolean hasNextDocument()
	{
		return currTransactionPartyDocument != null && currTransactionPartyDocument.getDocumentNumber() < transactionPartyDocuments.size();
	}
	public boolean hasNextCompletedDocument()
	{
		if ( hasNextDocument() )
		{
			for ( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
			{
				if( tranPartyDoc.getDocumentNumber() > currTransactionPartyDocument.getDocumentNumber() )
				{
					if ( tranPartyDoc.isCompleted() )
						return true;
				}
			}
		}
		return false;
	}

	public void goToPreviousDocument()
	{
		if ( currTransactionPartyDocument == null || currTransactionPartyDocument.getDocumentNumber() <= 2 )
			goToFirstDocument();
		else
		{
			TransactionPartyDocument prevTranPartyDoc = null;
			for ( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
			{
				if( tranPartyDoc.getDocumentNumber() < currTransactionPartyDocument.getDocumentNumber() )
					prevTranPartyDoc = tranPartyDoc;
			}
			if ( prevTranPartyDoc == null )
				goToFirstDocument();
			else
			{
				currTransactionPartyDocument = prevTranPartyDoc;
				currTransactionDocument = transaction.getTransactionDocument(currTransactionPartyDocument.getTransactionDocumentId()); // holds our data about this document
				currDocument = getDocument(currTransactionDocument.getDocumentId());
				pageNumber = 1;
				setupForCurrDocument();
			}
		}
	}
	public boolean hasPreviousDocument()
	{
		return currTransactionPartyDocument != null && currTransactionPartyDocument.getDocumentNumber() > 1;
	}
	public boolean hasPreviousCompletedDocument()
	{
		if ( hasPreviousDocument() )
		{
			TransactionPartyDocument prevTranPartyDoc = null;
			for ( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
			{
				if( tranPartyDoc.getDocumentNumber() < currTransactionPartyDocument.getDocumentNumber() )
					prevTranPartyDoc = tranPartyDoc;
			}
			if ( prevTranPartyDoc != null && prevTranPartyDoc.isCompleted() )
				return true;
		}
		return false;
	}
	
	public void goToDocument(int documentNumber)
	{
		if ( documentNumber < 1 )
			documentNumber = 1;

		boolean found = false;
		for ( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
		{
			if( tranPartyDoc.getDocumentNumber() == documentNumber )
			{
				currTransactionPartyDocument = tranPartyDoc;
				currTransactionDocument = transaction.getTransactionDocument(currTransactionPartyDocument.getTransactionDocumentId()); // holds our data about this document
				currDocument = getDocument(currTransactionDocument.getDocumentId());
				pageNumber = 1;
				setupForCurrDocument();
				found = true;
				break;
			}
		}
		
		if ( ! found )
			goToFirstDocument();
	}
	
	public void goToDocumentForTransactionDocumentAndParty(EsfUUID tranDocId, EsfUUID tranPartyId)
	{
		for ( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
		{
			if( tranPartyDoc.getTransactionDocumentId().equals(tranDocId) && tranPartyDoc.getTransactionPartyId().equals(tranPartyId) )
			{
				currTransactionPartyDocument = tranPartyDoc;
				currTransactionDocument = transaction.getTransactionDocument(currTransactionPartyDocument.getTransactionDocumentId()); // holds our data about this document
				currDocument = getDocument(currTransactionDocument.getDocumentId());
				pageNumber = 1;
				setupForCurrDocument();
				return;
			}
		}
		
		// Not supposed to happen, but we'll land somewhere if we don't know where
		goToPackageDocument();
	}

	
	// This is an error checking condition only
	public boolean hasNoTransactionPartyDocuments()
	{
		return transactionPartyDocuments == null || transactionPartyDocuments.size() == 0;
	}
	
	public TransactionParty getNextTransactionParty()
	{
		// If there's a configured next party that is not already done, return that one.
		EsfUUID nextTransactionPartyId = transactionParty.getNextTransactionPartyId();
		if ( nextTransactionPartyId != null && ! nextTransactionPartyId.isNull() )
		{
			TransactionParty nextParty = transaction.getTransactionParty(nextTransactionPartyId);
			if ( nextParty != null && ! nextParty.isEndState() && nextParty.hasNonSkippedTransactionPartyDocuments() )
				return nextParty;
		}
		
		// Find the first party that is not yet done.
		for( PackageVersionPartyTemplate pvpt : pkgVersion.getPackageVersionPartyTemplateList() )
		{
			TransactionParty tranParty = transaction.getTransactionParty(pvpt.getEsfName());
			if ( tranParty != null && ! tranParty.isEndState() && ! tranParty.isReports() && tranParty.hasNonSkippedTransactionPartyDocuments() ) // don't return a party that is already completed
				return tranParty;
		}
		
		// No next party could be found, so we must be done.
		return null;
	}
	
	public final boolean hasTransaction() { return transaction != null; }
	public final boolean hasNoTransaction() { return transaction == null; }
	public final boolean doProductionResolve() { return hasTransaction() && transaction.doProductionResolve(); }
	public final boolean doTestResolve() { return hasTransaction() && transaction.doTestResolve(); }
	public final boolean isProductionTransaction() { return hasTransaction() && transaction.isProduction(); }
	public final boolean isTestTransaction() { return hasTransaction() && transaction.isTest(); }
	
	public final boolean hasTransactionParty() { return transactionParty != null; }
	public final boolean hasNoTransactionParty() { return transactionParty == null; }

	public final boolean isPackageDocument()
	{
		return currDocument != null && pkgVersion != null && currDocument.equals(pkgDisclosureDocument);
	}
	
	public boolean hasUser() { return user != null; }
	public void setUser(User u) 
	{
		if ( u != null )
			user = u;
	}
	
	public Document getDocument(EsfUUID docId)
	{
		if ( documents == null )
			return null;
		
		for( Document doc : documents )
		{
			if ( doc.getId().equals(docId) )
				return doc;
		}
		return null;
	}
	
	public Document getDocument(EsfName docName)
	{
		if ( documents == null )
			return null;
		
		for( Document doc : documents )
		{
			if ( doc.getEsfName().equals(docName) )
				return doc;
		}
		return null;
	}
	
	public synchronized Map<EsfUUID,EsfUUID> getDocumentPartyIdToPackagePartyIdMapping()
	{
		if ( documentPartyIdToPackagePartyIdMapping == null )
		{
			documentPartyIdToPackagePartyIdMapping = new TreeMap<EsfUUID,EsfUUID>();
	    	for( PackageVersionPartyTemplate pvpt : pkgVersion.getPackageVersionPartyTemplateList() ) 
	    	{
	    		for( PackageVersionPartyTemplateDocumentParty pvptdp : pvpt.getPackageVersionPartyTemplateDocumentParties() )
	    		{
	    			if ( ! pvptdp.isDocumentPartyTemplateView() )
	    			{
		    			Document doc = getDocument(pvptdp.getDocumentId());
		    			if ( doc != null )
		    			{
		    				TransactionDocument tranDoc = transaction.getTransactionDocumentByDocumentId(pvptdp.getDocumentId());
		    				if ( tranDoc == null )
		    					_logger.error("getDocumentPartyIdToPackagePartyIdMapping() - Failed to find the transaction document for documentId:" + pvptdp.getDocumentId() + "; for package version party template: " + pvptdp.getDocumentPartyTemplateName() + "; transaction id: " + transaction.getId());
		    				else
		    				{
			    				DocumentVersion docVer = DocumentVersion.Manager.getById(tranDoc.getDocumentVersionId());
			    				if ( docVer == null )
			    					_logger.error("getDocumentPartyIdToPackagePartyIdMapping() - Failed to find transaction document's documentVersionId:" + tranDoc.getDocumentVersionId() + "; for package version party template: " + pvptdp.getDocumentPartyTemplateName() + "; transaction id: " + transaction.getId());
			    				else
			    				{
			    					for( PartyTemplate pt : docVer.getPartyTemplateList() )
			    					{
			    						if ( pt.getEsfName().equals(pvptdp.getDocumentPartyTemplateName()) )
			    						{
			    							documentPartyIdToPackagePartyIdMapping.put(pt.getId(), pvptdp.getPackageVersionPartyTemplateId());
			    							break;
			    						}
			    					}
			    				}
		    				}
		    			}
	    			}
	    		}
	    	}
		}

		return new TreeMap<EsfUUID,EsfUUID>(documentPartyIdToPackagePartyIdMapping);
	}
	
	public Document getDocumentByTransactionDocumentId(EsfUUID tranDocId)
	{
		if ( ! hasTransaction() )
			return null;
		TransactionDocument tranDoc = transaction.getTransactionDocument(tranDocId);
		return getDocument( tranDoc.getDocumentId() );
	}
	
	public FieldTemplate getFieldTemplate(Document document, EsfName fieldName)
	{
		if ( document == null )
			return null;
		TransactionDocument tranDoc = transaction.getTransactionDocumentByDocumentId(document.getId());
		if ( tranDoc == null )
			_logger.error("getFieldTemplate() - Failed to find the transaction document for documentId:" + document.getId() + "; for fieldName: " + fieldName + "; transaction id: " + transaction.getId());
		else
		{
			DocumentVersion docVer = DocumentVersion.Manager.getById(tranDoc.getDocumentVersionId());
			if ( docVer == null )
				_logger.error("getFieldTemplate() - Failed to find transaction document's documentVersionId:" + tranDoc.getDocumentVersionId() + "; for fieldName: " + fieldName + "; transaction id: " + transaction.getId());
			else
				return docVer.getFieldTemplate(fieldName);
		}
		
		return null;
	}
	
	public FieldTemplate getFieldTemplate(EsfUUID documentId, EsfName fieldName)
	{
		Document document = getDocument(documentId);
		return getFieldTemplate(document,fieldName);
	}
	
	public FieldTemplate getFieldTemplate(EsfName documentName, EsfName fieldName)
	{
		Document document = getDocument(documentName);
		return getFieldTemplate(document,fieldName);
	}
	
	/**
	 * Gets the document id/name and field name that goes with a simple one-field spec like ${field} or ${doc.field}
	 * @param oneFieldSpec the String simple one-field spec like ${field} or ${doc.field}
	 * @return the DocumentIdFieldName that represents the document and field of the field spec, or null if it cannot
	 *  	be determined or has more than one field spec.
	 */
	public DocumentIdFieldName getDocumentIdFieldNameFromSpec(String oneFieldSpec)
	{
		if ( EsfString.isBlank(oneFieldSpec) || EsfString.countCharInString(oneFieldSpec, '.') > 1 )
			return null;
		oneFieldSpec = oneFieldSpec.trim();
		if ( ! oneFieldSpec.startsWith("${") )
			return null;
		oneFieldSpec = oneFieldSpec.substring(2);
		if ( ! oneFieldSpec.endsWith("}") )
			return null;
		oneFieldSpec = oneFieldSpec.substring(0,oneFieldSpec.length()-1);
		EsfName documentName = null;
		EsfName fieldName = null;
		if ( oneFieldSpec.contains(".") )
		{
			String[] parts = oneFieldSpec.split("\\.");
			documentName = new EsfName(parts[0]);
			fieldName = new EsfName(parts[1]);
		}
		else
		{
			documentName = currDocument.getEsfName();
			fieldName = new EsfName(oneFieldSpec);
		}
		
		Document document = getDocument(documentName);
		if ( document == null )
			return null;
		
		return new DocumentIdFieldName(document.getId(),fieldName,documentName);
	}
	
	public boolean isDocumentParty(EsfName docVerPartyName)
	{
		return currDocumentVersionPartyNameList.contains(docVerPartyName);
	}
	
	public boolean isFieldEditable(EsfName fieldName)
	{
		return currDocumentVersionPartyFieldTemplateMap.containsKey(fieldName);
	}
	
	public boolean isPackageParty(EsfName pkgVerPartyName)
	{
		return pkgVerPartyName != null && pkgVerPartyName.isValid() && hasTransaction() && transactionParty != null && transactionParty.equals(transaction.getTransactionParty(pkgVerPartyName));
	}
	
	/**
	 * Determines if the current party is a view-only party who is not even defined as a party in the document.
	 * @return true if the party has no mapping to a document party
	 */
	public boolean isViewOnlyNonParty()
	{
		return currDocumentVersionPartyList == null  ||  currDocumentVersionPartyList.size() == 0;
	}
	public boolean isMultiPageViewOnlyNonParty() // used to send such parties to the multipage review page as they don't see any other pages
	{
		return isMultiPageDocument() && isViewOnlyNonParty();
	}
	
	public boolean isViewOnlyParty()
	{
		// If it's a multi-page document and this is the review page, we check against all fields for this party regardless of page
		if ( isMultiPageReviewPage() )
			return isPartyViewOnlyOnAllPages();
		return canPartyUpdateNoFields();
	}
	public boolean canPartyUpdateNoFields()
	{
		return currDocumentVersionPartyFieldTemplateMap == null || currDocumentVersionPartyFieldTemplateMap.size() == 0;
	}
	public boolean canPartyUpdateFields()
	{
		return currDocumentVersionPartyFieldTemplateMap != null && currDocumentVersionPartyFieldTemplateMap.size() > 0;
	}
	
	public String getCurrentDocumentRequestDispatcherUrl()
	{
		if ( hasNoTransaction() )
			return null;
		
		if ( isMultiPageReviewPage() )
		{
			return "/" + Application.getInstance().getLibraryGeneratedJSPDocumentPath() + 
			   "/" + currDocument.getId() + 
			   "/" + currDocumentVersion.getId() + 
			   "/" + currDocumentVersion.getId() +
			   ".jsp";
		}
		
		return "/" + Application.getInstance().getLibraryGeneratedJSPDocumentPath() + 
			   "/" + currDocument.getId() + 
			   "/" + currDocumentVersion.getId() + 
			   "/" + currDocumentVersionPage.getId() +
			   ".jsp";
	}
	
	public String getDocumentStyleIncludeUrl()
	{
		if ( hasNoTransaction() || currDocumentVersion == null )
			return null;
		
		DocumentStyleVersion documentStyleVersion = doProductionResolve() ?
				currDocumentVersion.getDocumentStyle().getProductionDocumentStyleVersion() :
				currDocumentVersion.getDocumentStyle().getTestDocumentStyleVersion();	    											
	
		return documentStyleVersion.getCssFileName();
	}
	public String getDocumentStyleClass()
	{
		if ( hasNoTransaction() || currDocumentVersion == null )
			return "";
		
		DocumentStyleVersion documentStyleVersion = doProductionResolve() ?
				currDocumentVersion.getDocumentStyle().getProductionDocumentStyleVersion() :
				currDocumentVersion.getDocumentStyle().getTestDocumentStyleVersion();	    											
	
		return documentStyleVersion.getId().toNormalizedEsfNameString();
	}
	
	private synchronized Map<EsfName,PartyTemplateFieldTemplate> createDocumentVersionPartyFieldTemplateMap()
	{
		TreeMap<EsfName,PartyTemplateFieldTemplate> map = new TreeMap<EsfName,PartyTemplateFieldTemplate>();
		if ( isViewOnlyNonParty())
			return map;
		
		for( EsfName fieldName : currDocumentVersionFieldTemplateMap.keySet() )
		{
			FieldTemplate fieldTemplate = currDocumentVersionFieldTemplateMap.get(fieldName);
			
			if ( isRegularPage() && ! currDocumentVersionPage.hasFieldTemplateIdOnPage(fieldTemplate.getId()) )
				continue;
			
			if ( isEsfReportsAccessForView() )
				continue;
			
			if ( isEsfReportsAccessForUpdate() && (fieldTemplate.isTypeSignature() || fieldTemplate.isTypeSignDate()) )
				continue;
			
			// Check if the field level block has been put in...if so, no party can edit this field
			EsfValue blockFurtherPartyEditsField = transaction.getFieldValue(currDocument.getEsfName(), new EsfPathName(fieldName,FieldTemplate.BLOCK_FURTHER_PARTY_EDITS_PATH_SUFFIX));
			if ( blockFurtherPartyEditsField != null )
				continue;
			
			for( PartyTemplate pt : currDocumentVersionPartyList )
			{
				for( PartyTemplateFieldTemplate partyTemplateFieldTemplate : pt.getPartyTemplateFieldTemplates() )
				{
					if ( fieldTemplate.getId().equals(partyTemplateFieldTemplate.getFieldTemplateId()) )
						map.put(fieldTemplate.getEsfName(),partyTemplateFieldTemplate);
				}
			}
			
		}
		return map;
	}
	
	public void blockFurtherPartyEdits(EsfName documentName, EsfName fieldName)
	{
		EsfPathName blockFurtherPartyEditsFieldPathName = new EsfPathName(fieldName,FieldTemplate.BLOCK_FURTHER_PARTY_EDITS_PATH_SUFFIX);
		transaction.setFieldValue(documentName, blockFurtherPartyEditsFieldPathName, new EsfDateTime(), false); // don't allow this pseudo-field to be cloned
		// If we are blocking a field in our current document, let's block now if the party can edit it
		if ( currDocument != null && currDocument.getEsfName().equals(documentName) )
		{
			if ( currDocumentVersionPartyFieldTemplateMap != null )
				currDocumentVersionPartyFieldTemplateMap.remove(fieldName);
		}
	}
	
	// For File fields only
	public void changeFileViewDownloadOptionalForParty(EsfName documentName, EsfName fieldName, EsfUUID partyId)
	{
		EsfPathName viewDownloadOptionalFileFieldPathName = new EsfPathName(fieldName,partyId.toEsfName(),FieldTemplate.VIEW_DOWNLOAD_OPTIONAL_PATH_SUFFIX);
		transaction.setFieldValue(documentName, viewDownloadOptionalFileFieldPathName, new EsfDateTime(), false); // don't allow this pseudo-field to be cloned
	}
	
	// For File fields only
	public void changeFileViewDownloadProhibitedForParty(EsfName documentName, EsfName fieldName, EsfUUID partyId)
	{
		EsfPathName viewDownloadProhibitedFileFieldPathName = new EsfPathName(fieldName,partyId.toEsfName(),FieldTemplate.VIEW_DOWNLOAD_PROHIBITED_PATH_SUFFIX);
		transaction.setFieldValue(documentName, viewDownloadProhibitedFileFieldPathName, new EsfDateTime(), false); // don't allow this pseudo-field to be cloned
		// If we are making prohibited a file field in our current document and we are the target party, let's block now if the party can edit it
		if ( currDocument != null && currDocument.getEsfName().equals(documentName) && pkgVersionPartyTemplate != null && pkgVersionPartyTemplate.getId().equals(partyId) )
		{
			if ( currDocumentVersionPartyFieldTemplateMap != null )
				currDocumentVersionPartyFieldTemplateMap.remove(fieldName);
		}
	}
	
	public boolean isPartyViewOnlyOnAllPages()
	{
		if ( isViewOnlyNonParty() )
			return true;
		
		for( EsfName fieldName : currDocumentVersionFieldTemplateMap.keySet() )
		{
			FieldTemplate fieldTemplate = currDocumentVersionFieldTemplateMap.get(fieldName);
			
			for( PartyTemplate pt : currDocumentVersionPartyList )
			{
				for( PartyTemplateFieldTemplate partyTemplateFieldTemplate : pt.getPartyTemplateFieldTemplates() )
				{
					if ( fieldTemplate.getId().equals(partyTemplateFieldTemplate.getFieldTemplateId()) )
						return false;
				}
			}
			
		}
		return true;
	}
	
	public boolean canPartyInputDate()
	{
		// We'll scan just those field that the party can edit, but check the field templates directly
		for( EsfName fieldName : currDocumentVersionPartyFieldTemplateMap.keySet() )
		{
			FieldTemplate fieldTemplate = currDocumentVersionFieldTemplateMap.get(fieldName);
			if ( fieldTemplate.isTypeDate() || fieldTemplate.isTypeDateTime() )
				return true;
		}
		return false;
	}
	
	public boolean canPartyInputRichTextarea()
	{
		// We'll scan just those field that the party can edit, but check the field templates directly
		for( EsfName fieldName : currDocumentVersionPartyFieldTemplateMap.keySet() )
		{
			FieldTemplate fieldTemplate = currDocumentVersionFieldTemplateMap.get(fieldName);
			if ( fieldTemplate.isTypeRichTextarea() )
				return true;
		}
		return false;
	}
	
	public CKEditorContext getCKEditorContext(HttpSession session)
	{
		if ( ckeditorContext == null )
		{
			ckeditorContext = new CKEditorContext();
			session.setAttribute("CKEditorContext-"+ckeditorContext.getIdInSession(),ckeditorContext);
		}
		if ( currLibrary != null )
			ckeditorContext.setLibrary(currLibrary);
		if ( currDocumentVersion != null )
			ckeditorContext.setDocumentVersion(currDocumentVersion);
		return ckeditorContext;
	}
    public void releaseCKEditorContext(HttpSession session)
    {
    	if ( ckeditorContext != null )
    		session.removeAttribute("CKEditorContext-"+ckeditorContext.getIdInSession());
    	ckeditorContext = null;
    }
	
	public List<FieldTemplate> getPartyFileConfirmClickFieldTemplates()
	{
		LinkedList<FieldTemplate> list = null;
		
		// We'll scan all fields in the document for confirm link types
		for( EsfName fieldName : currDocumentVersionFieldTemplateMap.keySet() )
		{
			FieldTemplate fieldTemplate = currDocumentVersionFieldTemplateMap.get(fieldName);
			if ( fieldTemplate.isTypeFileConfirmClick() )
			{
				if ( list == null )
					list = new LinkedList<FieldTemplate>();
				list.add(fieldTemplate);
			}
		}
		return list;
	}
	
	public List<FieldTemplate> getPartyFileFieldTemplates()
	{
		LinkedList<FieldTemplate> list = null;
		
		// We'll scan all fields in the document for file types
		for( EsfName fieldName : currDocumentVersionFieldTemplateMap.keySet() )
		{
			FieldTemplate fieldTemplate = currDocumentVersionFieldTemplateMap.get(fieldName);
			if ( fieldTemplate.isTypeFile() )
			{
				if ( list == null )
					list = new LinkedList<FieldTemplate>();
				list.add(fieldTemplate);
			}
		}
		return list;
	}
	
	public List<FieldTemplate> getPartyUploadFileFieldTemplates()
	{
		LinkedList<FieldTemplate> list = null;
		
		// We'll scan just those field that the party can edit, but check the field templates directly
		for( EsfName fieldName : currDocumentVersionPartyFieldTemplateMap.keySet() )
		{
			FieldTemplate fieldTemplate = currDocumentVersionFieldTemplateMap.get(fieldName);
			if ( fieldTemplate.isTypeFile() )
			{
				if ( list == null )
					list = new LinkedList<FieldTemplate>();
				list.add(fieldTemplate);
			}
		}
		return list;
	}
	
	public boolean canPartyUploadFile()
	{
		// We'll scan just those field that the party can edit, but check the field templates directly
		for( EsfName fieldName : currDocumentVersionPartyFieldTemplateMap.keySet() )
		{
			FieldTemplate fieldTemplate = currDocumentVersionFieldTemplateMap.get(fieldName);
			if ( fieldTemplate.isTypeFile() )
				return true;
		}
		return false;
	}
	
	public boolean canPartySignCurrDocument()
	{
		// If it's a multi-page document and this is the review page, we check against all signature fields for this party regardless of page
		if ( isMultiPageReviewPage() )
		{
			for( EsfName fieldName : currDocumentVersionFieldTemplateMap.keySet() )
			{
				FieldTemplate fieldTemplate = currDocumentVersionFieldTemplateMap.get(fieldName);
				if ( fieldTemplate.isTypeSignature() )
				{
					for( PartyTemplate pt : currDocumentVersionPartyList )
					{
						for( PartyTemplateFieldTemplate partyTemplateFieldTemplate : pt.getPartyTemplateFieldTemplates() )
						{
							if ( fieldTemplate.getId().equals(partyTemplateFieldTemplate.getFieldTemplateId()) )
								return true;
						}
					}	
				}
			}
		}
		else
		{
			// We'll scan just those field that the party can edit, but check the field templates directly
			for( EsfName fieldName : currDocumentVersionPartyFieldTemplateMap.keySet() )
			{
				FieldTemplate fieldTemplate = currDocumentVersionFieldTemplateMap.get(fieldName);
				if ( fieldTemplate.isTypeSignature() )
					return true;
			}
		}

		return false;
	}
	
	public boolean hasPartySignedCurrDocument()
	{
		// If it's a multi-page document and this is the review page, we check against all signature fields for this party regardless of page
		if ( isMultiPageReviewPage() )
		{
			for( EsfName fieldName : currDocumentVersionFieldTemplateMap.keySet() )
			{
				FieldTemplate fieldTemplate = currDocumentVersionFieldTemplateMap.get(fieldName);
				if ( fieldTemplate.isTypeSignature() )
				{
					for( PartyTemplate pt : currDocumentVersionPartyList )
					{
						for( PartyTemplateFieldTemplate partyTemplateFieldTemplate : pt.getPartyTemplateFieldTemplates() )
						{
							if ( fieldTemplate.getId().equals(partyTemplateFieldTemplate.getFieldTemplateId()) )
							{
								EsfString signValue = currTransactionDocument.getRecord().getStringByName(fieldTemplate.getEsfName());
								if ( signValue != null && signValue.isNonBlank() )
									return true;
							}
						}
					}	
				}
			}
		}
		else
		{
			// We'll scan just those field that the party can edit, but check the field templates directly
			for( EsfName fieldName : currDocumentVersionPartyFieldTemplateMap.keySet() )
			{
				FieldTemplate fieldTemplate = currDocumentVersionFieldTemplateMap.get(fieldName);
				if ( fieldTemplate.isTypeSignature() )
				{
					EsfString signValue = currTransactionDocument.getRecord().getStringByName(fieldTemplate.getEsfName());
					if ( signValue != null && signValue.isNonBlank() )
						return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean hasDocumentBeenSigned()
	{
		List<TransactionParty> allTranParties = transaction.getAllTransactionParties();
		for( TransactionParty tranParty : allTranParties )
		{
			List<TransactionPartyDocument> tranPartyDocList = tranParty.getTransactionPartyDocuments();
			for( TransactionPartyDocument tranPartyDoc : tranPartyDocList )
			{
				// If this is for our doc, check if the party signed it
				if ( tranPartyDoc.getTransactionDocumentId().equals(this.currTransactionDocument.getId()) )
				{
					if ( tranPartyDoc.isEsigned() )
						return true;
				}
			}
		}
		return false;
	}
	
	public boolean hasAnyDocumentBeenSigned()
	{
		List<TransactionParty> allTranParties = transaction.getAllTransactionParties();
		for( TransactionParty tranParty : allTranParties )
		{
			List<TransactionPartyDocument> tranPartyDocList = tranParty.getTransactionPartyDocuments();
			for( TransactionPartyDocument tranPartyDoc : tranPartyDocList )
			{
				if ( tranPartyDoc.isEsigned() )
					return true;
			}
		}
		return false;
	}
	
	public List<FieldTemplate> getMaskInDataSnapshotFieldTemplates()
	{
		LinkedList<FieldTemplate> list = null;
		
		// We'll scan all field in the document for file types
		for( EsfName fieldName : currDocumentVersionFieldTemplateMap.keySet() )
		{
			FieldTemplate fieldTemplate = currDocumentVersionFieldTemplateMap.get(fieldName);
			if ( fieldTemplate.isMaskInDataSnapshot() )
			{
				if ( list == null )
					list = new LinkedList<FieldTemplate>();
				list.add(fieldTemplate);
			}
		}
		return list;
	}
	
	public static class SignedPartyInfo
	{
		public EsfName packagePartyName;
		public EsfUUID packagePartyId;
		public String emailAddress;
		public String userInfo;
		public String displayName;
		public String signIpHost;
		public EsfDateTime signTimestamp;
	}
	
	public List<SignedPartyInfo> getAllSignedParties()
	{
		List<SignedPartyInfo> signedParties = new LinkedList<SignedPartyInfo>();
		
		if ( currTransactionDocument == null ) // package page typically
			return signedParties;
		
		List<TransactionParty> allTranParties = transaction.getAllTransactionParties();
		for( TransactionParty tranParty : allTranParties )
		{
			TransactionPartyAssignment tranPartyAssignment = tranParty.getCurrentAssignment();
			List<TransactionPartyDocument> tranPartyDocList = tranParty.getTransactionPartyDocuments();
			for( TransactionPartyDocument tranPartyDoc : tranPartyDocList )
			{
				// If this is for our doc, check if the party signed it
				if ( tranPartyDoc.getTransactionDocumentId().equals(currTransactionDocument.getId()) )
				{
					if ( tranPartyDoc.isEsigned() )
					{
						PackageVersionPartyTemplate packageParty = tranParty.getPackageVersionPartyTemplate();
						if ( packageParty == null && pkgVersionPartyTemplate.getId().equals(tranParty.getPackageVersionPartyTemplateId()) ) // happens only when doing TestDocument mode
							packageParty = pkgVersionPartyTemplate;
						SignedPartyInfo info = new SignedPartyInfo();
						if ( packageParty != null ) // normally not null except when testing a document
						{
							info.packagePartyName = packageParty.getEsfName();
							info.packagePartyId = packageParty.getId();
							info.displayName = packageParty.getDisplayName();
						}
						else
						{
							info.packagePartyName = new EsfName("TestDocumentParty");
							info.packagePartyId = new EsfUUID("1-1-1-1-1");
							info.displayName = "Test Document Party";
						}
						info.signIpHost = tranPartyDoc.getEsignIpHostAddr();
						info.signTimestamp = tranPartyDoc.getEsignTimestamp();
						info.emailAddress = tranPartyAssignment.hasEmailAddress() ? tranPartyAssignment.getEmailAddress() : "(not specified)";
						info.userInfo = tranPartyAssignment.hasUserId() ? (tranPartyAssignment.getUser().getFullDisplayName() + " (" + tranPartyAssignment.getUserId() + ")") : null;
						signedParties.add(info);
					}
				}
			}
		}
		return signedParties;
	}
	
	public final boolean isApiMode() 
	{
		return isApiMode;
	}
	public final void setApiMode(boolean isApiMode)
	{
		this.isApiMode = isApiMode;
	}
	
	public final String getIpOnlyAddress()
	{
		return hasIpOnlyAddress() ? ipOnlyAddress : "(no IP)";
	}
	public final boolean hasIpOnlyAddress()
	{
		return EsfString.isNonBlank(ipOnlyAddress);
	}
	public void setIpOnlyAddress(String ipOnlyAddress)
	{
		this.ipOnlyAddress = ipOnlyAddress;
	}

	public final String getIpHost()
	{
		return hasIpHost() ? ipHost : "(no IP)";
	}
	public final boolean hasIpHost()
	{
		return EsfString.isNonBlank(ipHost);
	}
	public void setIpHost(String ipHost)
	{
		this.ipHost = ipHost;
	}

	public final String getUserAgent()
	{
		return hasUserAgent() ? userAgent : "(no user-agent)";
	}
	public final boolean hasUserAgent()
	{
		return EsfString.isNonBlank(userAgent);
	}
	public void setUserAgent(String userAgent)
	{
		this.userAgent = userAgent;
	}

	public synchronized void reset()
	{
		// We don't touch any of the final fields that are set once and can't be reset for a transaction context (since it points to the transaction and party who picked it up)
		
		currTransactionPartyDocument = null;
		currTransactionDocument = null;

		pkg = null;
		pkgVersion = null;
		pkgVersionPartyTemplate = null;
		pkgDisclosureDocument = null;
		pkgButtonMessage = null;
		documents = null;
		currDocument = null;
		currLibrary = null;
		currDocumentVersion = null;
		currDocumentVersionPage = prevEditDocumentVersionPage = nextEditDocumentVersionPage = null;
		currDocumentVersionPartyList = null;
		currDocumentVersionPartyNameList = null;
		currDocumentVersionFieldTemplateMap = null;
		currDocumentVersionPartyFieldTemplateMap = null;
	}
	
	
	@Override
	public int compareTo(TransactionContext o) {
		return pickupCode.compareTo(o.pickupCode);
	}
}

