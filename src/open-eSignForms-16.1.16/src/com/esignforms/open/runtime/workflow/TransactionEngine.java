// Copyright (C) 2011-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.workflow;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.runtime.action.Action;
import com.esignforms.open.runtime.event.Event;
import com.esignforms.open.runtime.event.PartyActivatedEvent;
import com.esignforms.open.runtime.event.PartyAutoPostDocumentEvent;
import com.esignforms.open.runtime.event.PartyCompletedDocumentEvent;
import com.esignforms.open.runtime.event.PartyCompletedEvent;
import com.esignforms.open.runtime.event.PartyCompletedPageEvent;
import com.esignforms.open.runtime.event.PartyCreatedEvent;
import com.esignforms.open.runtime.event.PartyRetrievedDocumentEvent;
import com.esignforms.open.runtime.event.PartyReviewDocumentEvent;
import com.esignforms.open.runtime.event.PartySavedDocumentEvent;
import com.esignforms.open.runtime.event.PartyTransferredEvent;
import com.esignforms.open.runtime.event.PartyViewCompletedDocumentEvent;
import com.esignforms.open.runtime.event.TimerExpiredEvent;
import com.esignforms.open.runtime.event.TransactionCanceledEvent;
import com.esignforms.open.runtime.event.TransactionCompletedEvent;
import com.esignforms.open.runtime.event.TransactionResumedEvent;
import com.esignforms.open.runtime.event.TransactionStartedEvent;
import com.esignforms.open.runtime.event.TransactionSuspendedEvent;
import com.esignforms.open.runtime.event.TransactionUpdateApiEvent;
import com.esignforms.open.runtime.event.ViewOptionalPartyViewedDocumentEvent;

/**
* TransactionEngine handles events, actions and conditions flowing for a given Transaction.  Each Transaction
* that is "running" has a TransactionEngine that moves the process flow along.  Important state about
* a transaction is recorded in the transaction. The engine keeps its own state minimal to just processing
* work for a given "request".
* 
* @author Yozons, Inc.
*/
public class TransactionEngine
{
	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TransactionEngine.class);

	LinkedList<Event> eventQueue = new LinkedList<Event>();
	
    protected TransactionContext context;
    protected DocumentPageBean docPage; // only present when the engine is running for a give document page, generally needed for handling error messages, redirects, etc.
    
    /**
     * This creates a TransactionEngine object for a given TransactionContext
     */
    public TransactionEngine(TransactionContext context)
    {
        this.context = context;
        this.docPage = null;
        
    }
    public TransactionEngine(TransactionContext context, DocumentPageBean docPage)
    {
        this.context = context;
        this.docPage = docPage;
    }
    
    public void doWork(Connection con, Errors errors)
    	throws SQLException, EsfException
    {
    	Event event;
    	
    	while( (event = dequeueEvent()) != null )
    	{
        	try
    		{
        		boolean allowCustomLogicActions = event.doBuiltInAction(con, context, errors);
        		context.transaction._setChangedForInternalUseOnly();
        		if ( ! allowCustomLogicActions )
        		{
            		context.transaction.logGeneral(con, "WARNING: Built-in processing of Event '" + event.toString() + "' requested custom logic actions be skipped.");
        			continue;
        		}
    		}
    		catch( EsfException e )
    		{
        		context.transaction.logGeneral(con, "Event: " + event.toString() + "; built-in exception: " + e.getMessage());
        		throw e;
    		}
    		catch( SQLException e )
    		{
        		context.transaction.logGeneral(con, "Event: " + event.toString() + "; built-in SQL exception: " + e.getMessage());
        		throw e;
    		}
    		
    		
    		for( Action action : event.getActions(context) )
    		{
    			try
    			{
    				if ( action.passesCondition(con,context) )
    				{
    					action.doAction(con,context,errors);
    					context.transaction._setChangedForInternalUseOnly();
    				}
    			}
        		catch( EsfException e )
        		{
            		context.transaction.logGeneral(con, "Event: " + event.toString() + "; Action: " + action.toString() + "; exception: " + e.getMessage());
            		throw e;
        		}
        		catch( SQLException e )
        		{
            		context.transaction.logGeneral(con, "Event: " + event.toString() + "; Action: " + action.toString() + "; SQL exception: " + e.getMessage());
            		throw e;
        		}
    		}
    		
    		// Handle our pseudo-event-actions here. For now, this is just a hack for doing APS_EIP on the 
    		// document completed event and we hope this doesn't need to expand for more such hacks in the future.
    		// The idea is that APS_EIP will become a full-fledged customizable Action via Custom Logic if it becomes generally useful.
    		if ( event instanceof PartyCompletedDocumentEvent && context.hasDocumentCompletedEventPseudoAction() )
    		{
    			Action pseudoAction = context.getDocumentCompletedEventPseudoAction();
    			try
    			{
    				if ( pseudoAction.passesCondition(con,context) )
    				{
    					pseudoAction.doAction(con,context,errors);
    					context.transaction._setChangedForInternalUseOnly();
    				}
    			}
        		catch( EsfException e )
        		{
            		context.transaction.logGeneral(con, "Event: " + event.toString() + "; Pseudo-Action: " + pseudoAction.toString() + "; exception: " + e.getMessage());
            		throw e;
        		}
        		catch( SQLException e )
        		{
            		context.transaction.logGeneral(con, "Event: " + event.toString() + "; Pseudo-Action: " + pseudoAction.toString() + "; SQL exception: " + e.getMessage());
            		throw e;
        		}
    		}
    	}
    }
    
    public void doWork()
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
        	Errors errors = new Errors();
        	doWork(con,errors);
        	context.transaction.save(con); // save the transaction in case it was changed by any of the actions for this event
            con.commit();
        }
        catch(EsfException e) 
        {
        	_logger.error("doWork()",e);
            pool.rollbackIgnoreException(con);
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"doWork()");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }
    
    public void queueEvent(Event newEvent) 
    {
    	synchronized(eventQueue) 
    	{
    		eventQueue.add(newEvent);
    	}
    }
    
    protected Event dequeueEvent() 
    {
    	Event nextEvent;
    	
    	synchronized(eventQueue) 
    	{
    		nextEvent = eventQueue.size() > 0 ? eventQueue.removeFirst() : null;
    	}
    	
    	return nextEvent;
    }
    
    public void queueTransactionStartedEvent()
    {
    	TransactionStartedEvent event = new TransactionStartedEvent();
    	queueEvent(event);
    }

    public void queuePartyCreatedEvent(EsfName partyName)
    {
    	PartyCreatedEvent event = new PartyCreatedEvent(partyName);
    	queueEvent(event);
    }

    public void queuePartyActivatedEvent(EsfName partyName)
    {
    	PartyActivatedEvent event = new PartyActivatedEvent(partyName);
    	queueEvent(event);
    }

    public void queuePartyTransferredEvent(EsfName partyName, String toEmail)
    {
    	PartyTransferredEvent event = new PartyTransferredEvent(partyName,toEmail);
    	queueEvent(event);
    }

    // This is when the view optional party first views a document
    public void queueViewOptionalPartyViewedDocumentEvent(EsfName partyName, EsfName documentName, TransactionPartyDocument tranPartyDocument)
    {
    	ViewOptionalPartyViewedDocumentEvent event = new ViewOptionalPartyViewedDocumentEvent(partyName, documentName, tranPartyDocument);
    	queueEvent(event);
    }

    // This is when the party first retrieves a document
    public void queuePartyRetrievedDocumentEvent(EsfName partyName, EsfName documentName, TransactionPartyDocument tranPartyDocument)
    {
    	PartyRetrievedDocumentEvent event = new PartyRetrievedDocumentEvent(partyName, documentName, tranPartyDocument);
    	queueEvent(event);
    }

    // This is when the party submits a page on a multi-page document (must have passed general validation for that page)
    public void queuePartyCompletedPageEvent(EsfName partyName, EsfName documentName, TransactionPartyDocument tranPartyDocument, EsfName pageName)
    {
    	PartyCompletedPageEvent event = new PartyCompletedPageEvent(partyName, documentName, tranPartyDocument, pageName);
    	queueEvent(event);
    }

    // This is when the party reviews a document (must have passed general validation)
    public void queuePartyReviewDocumentEvent(EsfName partyName, EsfName documentName, TransactionPartyDocument tranPartyDocument)
    {
    	PartyReviewDocumentEvent event = new PartyReviewDocumentEvent(partyName, documentName, tranPartyDocument);
    	queueEvent(event);
    }

    // This is when the party saves a document (must have passed general validation when updating via reports)
    public void queuePartySavedDocumentEvent(EsfName partyName, EsfName documentName, TransactionPartyDocument tranPartyDocument)
    {
    	PartySavedDocumentEvent event = new PartySavedDocumentEvent(partyName, documentName, tranPartyDocument);
    	queueEvent(event);
    }

    // This is when the party clicks dropdown or the like that triggers an autopost on a document
    public void queuePartyAutoPostDocumentEvent(EsfName partyName, EsfName documentName, String autoPostFieldId, EsfName autoPostFieldName)
    {
    	PartyAutoPostDocumentEvent event = new PartyAutoPostDocumentEvent(partyName, documentName, autoPostFieldId, autoPostFieldName);
    	queueEvent(event);
    }

    public void queuePartyCompletedDocumentEvent(EsfName partyName, EsfName documentName, TransactionDocument tranDocument, TransactionPartyDocument tranPartyDocument, String capturedHtml)
    {
    	PartyCompletedDocumentEvent event = new PartyCompletedDocumentEvent(partyName, documentName, tranDocument, tranPartyDocument, capturedHtml);
    	queueEvent(event);
    }

    // This is when the party views an already completed document
    public void queuePartyViewCompletedDocumentEvent(EsfName partyName, EsfName documentName, TransactionPartyDocument tranPartyDocument)
    {
    	PartyViewCompletedDocumentEvent event = new PartyViewCompletedDocumentEvent(partyName, documentName, tranPartyDocument);
    	queueEvent(event);
    }

    public void queuePartyCompletedEvent(EsfName partyName, TransactionParty tranParty)
    {
    	PartyCompletedEvent event = new PartyCompletedEvent(partyName, tranParty);
    	queueEvent(event);
    }

    public void queueTransactionCompletedEvent(EsfName completingPartyName)
    {
    	TransactionCompletedEvent event = new TransactionCompletedEvent(completingPartyName);
    	queueEvent(event);
    }

    public void queueTransactionCanceledEvent(String reason)
    {
    	TransactionCanceledEvent event = new TransactionCanceledEvent(reason);
    	queueEvent(event);
    }
    
    public void queueTransactionSuspendedEvent(String reason)
    {
    	TransactionSuspendedEvent event = new TransactionSuspendedEvent(reason);
    	queueEvent(event);
    }
    
    public void queueTransactionResumedEvent(String reason)
    {
    	TransactionResumedEvent event = new TransactionResumedEvent(reason);
    	queueEvent(event);
    }
    
    public void queueTransactionUpdateApiEvent(String eventName)
    {
    	TransactionUpdateApiEvent event = new TransactionUpdateApiEvent(eventName);
    	queueEvent(event);
    }
    
    public void queueTimerExpiredEvent(String timerName)
    {
    	TimerExpiredEvent event = new TimerExpiredEvent(timerName);
    	queueEvent(event);
    }

}