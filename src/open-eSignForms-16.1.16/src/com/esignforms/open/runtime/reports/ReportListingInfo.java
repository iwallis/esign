// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.reports;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.prog.ReportFieldValue;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.ReportTemplateReportField;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.user.User;

/**
 * Represents a single report listing row, made up of the selected report fields.
 *
 * @author Yozons, Inc.
 */
public class ReportListingInfo
	extends com.esignforms.open.db.DatabaseObject 
	implements java.lang.Comparable<ReportListingInfo>
{
	private static final long serialVersionUID = 4063557367770388384L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ReportListingInfo.class);

	private EsfUUID id;
    private String status;
    private String statusText;
	private EsfPathName templatePathName;
	private EsfPathName packagePathName;
	private EsfDateTime lastUpdatedTimestamp;
	private EsfDateTime createdTimestamp;
	private EsfDateTime cancelTimestamp;
	private EsfDateTime expireTimestamp;
	private EsfDateTime stallTimestamp;
	private List<ReportTemplateReportField> reportFieldValueList;
	
	public ReportListingInfo(EsfUUID id,String status,String statusText,EsfPathName templatePathName,EsfPathName packagePathName,
								  EsfDateTime lastUpdatedTimestamp,EsfDateTime createdTimestamp,EsfDateTime cancelTimestamp,EsfDateTime expireTimestamp,EsfDateTime stallTimestamp,
								  List<ReportTemplateReportField> reportFieldValueList)
    {
		this.id = id;
		this.status = status;
		this.statusText = statusText;
		this.templatePathName = templatePathName;
		this.packagePathName = packagePathName;
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
		this.createdTimestamp = createdTimestamp;
		this.cancelTimestamp = cancelTimestamp;
		this.expireTimestamp = expireTimestamp;
		this.stallTimestamp = stallTimestamp;
		this.reportFieldValueList = reportFieldValueList;
    }
    
	public EsfUUID getId()
	{
		return id;
	}
	
	public List<ReportTemplateReportField> getReportFieldValueList()
	{
		return reportFieldValueList;
	}

	public String getStatus()
	{
		return status;
	}
	
	public String getStatusText()
	{
		return statusText;
	}
	
	public EsfPathName getTemplatePathName()
	{
		return templatePathName;
	}
	
	public EsfPathName getPackagePathName()
	{
		return packagePathName;
	}
	
	public EsfDateTime getLastUpdatedTimestamp()
	{
		return lastUpdatedTimestamp;
	}
	
	public EsfDateTime getCreatedTimestamp()
	{
		return createdTimestamp;
	}
	
	public EsfDateTime getCancelTimestamp()
	{
		return cancelTimestamp;
	}
	
	public EsfDateTime getExpireTimestamp()
	{
		return expireTimestamp;
	}

	public EsfDateTime getStallTimestamp()
	{
		return stallTimestamp;
	}
	
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof ReportListingInfo )
        {
        	ReportListingInfo other = (ReportListingInfo)o;
            return getId().equals(other.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getLastUpdatedTimestamp().hashCode() + getId().hashCode();
    }
    
    @Override
    public int compareTo(ReportListingInfo o)
    {
    	int diff = getLastUpdatedTimestamp().compareTo(o.getLastUpdatedTimestamp());
    	if ( diff != 0 )
    		return diff;
    	return getId().compareTo(o.getId());
    }

	
	/******************* DB routines */
    public static class Manager
    {
        private static void appendBuiltInSearchKeySubselect(StringBuilder sqlBuff, List<ReportTemplateReportField> builtInSearchFields)
        {
            if ( builtInSearchFields == null || builtInSearchFields.size() == 0 )
                return;
            
            for ( ReportTemplateReportField rtrf : builtInSearchFields )
            {
            	EsfValue searchValue = rtrf.getFieldValue();
            	ReportFieldTemplate reportFieldTemplate = rtrf.getReportFieldTemplate();
            	if ( ! reportFieldTemplate.isFieldTypeBuiltIn() ) {
            		_logger.warn("Manager.appendBuiltInSearchKeySubselect() - Unexpectedly passed in non-built-in field: " + reportFieldTemplate.getFieldName() + 
            					 "; reportFieldTemplate id: " + reportFieldTemplate.getId() + "; ignoring");
            		continue;
            	}
            	
            	if ( reportFieldTemplate.getFieldName().equals("esf_status_text") )
            	{
                	String searchField = searchValue.toPlainString();
                	boolean negated = searchField.startsWith("!");
                	if ( negated )
                		searchField = searchField.substring(1).trim();
            		if ( searchField.startsWith("=") )
            		{
            			if ( negated )
            				sqlBuff.append("AND lower(TRAN.status_text) <> ? ");
            			else
            				sqlBuff.append("AND lower(TRAN.status_text) = ? ");
            		}
            		else
            		{
            			if ( negated )
                			sqlBuff.append("AND lower(TRAN.status_text) NOT LIKE ? ");
            			else
            				sqlBuff.append("AND lower(TRAN.status_text) LIKE ? ");
            		}
                }
            }
        }
        
        private static void appendBuiltInSearchKeyWhereParams(EsfPreparedStatement stmt, List<ReportTemplateReportField> builtInSearchFields)
	        throws SQLException
	    {
	        if ( builtInSearchFields == null || builtInSearchFields.size() == 0 )
	            return;
	
	        for ( ReportTemplateReportField rtrf : builtInSearchFields )
	        {
	        	EsfValue searchValue = rtrf.getFieldValue();
	        	ReportFieldTemplate reportFieldTemplate = rtrf.getReportFieldTemplate();
	        	if ( ! reportFieldTemplate.isFieldTypeBuiltIn() ) {
	        		_logger.warn("Manager.appendBuiltInSearchKeyWhereParams() - Unexpectedly passed in non-built-in field: " + reportFieldTemplate.getFieldName() + 
	        					 "; reportFieldTemplate id: " + reportFieldTemplate.getId() + "; ignoring");
	        		continue;
	        	}
	        	
            	if ( reportFieldTemplate.getFieldName().equals("esf_status_text") )
            	{
	            	String searchField = searchValue.toPlainString();
                	boolean negated = searchField.startsWith("!");
                	if ( negated )
                		searchField = searchField.substring(1).trim();
	        		if ( searchField.startsWith("=") )
	                	stmt.set(searchField.substring(1).trim().toLowerCase());
	        		else if ( searchField.startsWith("^") )
	    				stmt.set(escapeLIKE(searchField.substring(1).trim().toLowerCase())+"%");
	        		else
	        			stmt.set("%"+escapeLIKE(searchField.toLowerCase())+"%");
	            }
	        }
	    }

        
        private static void appendUserSearchKeySubselect(StringBuilder sqlBuff, List<ReportTemplateReportField> userSearchFields)
        {
            if ( userSearchFields == null || userSearchFields.size() == 0 )
                return;
            
            boolean needsIntersect = false;
            boolean hasSubselect   = false;  // Tells if we've put the subselect in or not.
            
            for ( ReportTemplateReportField rtrf : userSearchFields )
            {
            	ReportFieldTemplate reportFieldTemplate = rtrf.getReportFieldTemplate();
            	if ( reportFieldTemplate.isFieldTypeBuiltIn() ) {
            		_logger.warn("Manager.appendUserSearchKeySubselect() - Unexpectedly passed in built-in field: " + reportFieldTemplate.getFieldName() + 
            					 "; reportFieldTemplate id: " + reportFieldTemplate.getId() + "; ignoring");
            		continue;
            	}
            	
                if ( ! hasSubselect ) // We only want to put this in if we'll do a subselect
                {
                    sqlBuff.append("AND TRAN.id IN ( ");
                    hasSubselect = true;
                }

                if ( needsIntersect )
                    sqlBuff.append("INTERSECT ");
                else
                    needsIntersect = true; // we'll put in the INTERSECT on the next one if we find one

                if ( reportFieldTemplate.isFieldTypeStringOrRelated() )
                {
                	EsfValue searchValue = rtrf.getFieldValue();
                	String searchField = searchValue.toPlainString();
                	boolean negated = searchField.startsWith("!");
                	if ( negated )
                		searchField = searchField.substring(1).trim();
            		if ( searchField.startsWith("=") )
            		{
            			String matchingText = searchField.substring(1).trim();
            			if ( EsfString.isBlank(matchingText) )
            			{
            				if ( negated )
                				sqlBuff.append("SELECT DISTINCT transaction_id FROM esf_tran_report_field_string WHERE report_field_template_id=? AND field_value IS NOT NULL ");
            				else
                				sqlBuff.append("SELECT DISTINCT transaction_id FROM esf_tran_report_field_string WHERE report_field_template_id=? AND field_value IS NULL ");
            			}
            			else
            			{
            				if ( negated )
            					sqlBuff.append("SELECT DISTINCT transaction_id FROM esf_tran_report_field_string WHERE report_field_template_id=? AND lower(field_value)<>? ");  // don't include NULL values, even if "not equal"
            				else
            					sqlBuff.append("SELECT DISTINCT transaction_id FROM esf_tran_report_field_string WHERE report_field_template_id=? AND lower(field_value)=? ");
            			}
            		}
            		else
            		{
            			if ( negated )
            				sqlBuff.append("SELECT DISTINCT transaction_id FROM esf_tran_report_field_string WHERE report_field_template_id=? AND lower(field_value) NOT LIKE ? ");
            			else
            				sqlBuff.append("SELECT DISTINCT transaction_id FROM esf_tran_report_field_string WHERE report_field_template_id=? AND lower(field_value) LIKE ? ");
            		}
                }
                else if ( reportFieldTemplate.isFieldTypeDate() )
            	{
                	EsfValue[] searchDateValues = rtrf.getFieldValues();
                	EsfDate searchValueFromDate = (EsfDate)searchDateValues[0];
                	EsfDate searchValueToDate = (EsfDate)searchDateValues[1];
                	sqlBuff.append("SELECT DISTINCT transaction_id FROM esf_tran_report_field_date WHERE report_field_template_id=? AND ");
                	if ( ! searchValueFromDate.isNull() )
                	{
                		sqlBuff.append("field_value >= ? ");
                		if ( ! searchValueToDate.isNull() )
                			sqlBuff.append("AND ");
                	}
                	if ( ! searchValueToDate.isNull() )
                	{
                		sqlBuff.append("field_value <= ? ");
                	}
            	}
            }
            
            if ( hasSubselect )
                sqlBuff.append(") ");
        }

        
        private static void appendUserSearchKeyWhereParams(EsfPreparedStatement stmt, List<ReportTemplateReportField> userSearchFields)
	        throws SQLException
	    {
            if ( userSearchFields == null || userSearchFields.size() == 0 )
                return;
	
	        for ( ReportTemplateReportField rtrf : userSearchFields )
	        {
            	ReportFieldTemplate reportFieldTemplate = rtrf.getReportFieldTemplate();
            	if ( reportFieldTemplate.isFieldTypeBuiltIn() ) {
            		_logger.warn("Manager.appendUserSearchKeyWhereParams() - Unexpectedly passed in built-in field: " + reportFieldTemplate.getFieldName() + 
            					 "; reportFieldTemplate id: " + reportFieldTemplate.getId() + "; ignoring");
            		continue;
            	}
            	
	            stmt.set(reportFieldTemplate.getId());
	            
	            if ( reportFieldTemplate.isFieldTypeStringOrRelated() )
	            {
	            	EsfValue searchValue = rtrf.getFieldValue();
                	String searchField = searchValue.toPlainString();
                	boolean negated = searchField.startsWith("!");
                	if ( negated )
                		searchField = searchField.substring(1).trim();
	        		if ( searchField.startsWith("=") )
	        		{
            			String searchText = searchField.substring(1).trim();
            			if ( EsfString.isNonBlank(searchText) ) // if blank, we'll do an IS NULL or IS NOT NULL (no statement param value needed)
            				stmt.set(searchText.toLowerCase());
	        		}
	        		else if ( searchField.startsWith("^") )
	    				stmt.set(escapeLIKE(searchField.substring(1).trim().toLowerCase())+"%");
	        		else
	        			stmt.set("%"+escapeLIKE(searchField.toLowerCase())+"%");
	            }
	            else if ( reportFieldTemplate.isFieldTypeDate() )
	            {
                	EsfValue[] searchDateValues = rtrf.getFieldValues();
                	EsfDate searchValueFromDate = (EsfDate)searchDateValues[0];
                	EsfDate searchValueToDate = (EsfDate)searchDateValues[1];
                	if ( ! searchValueFromDate.isNull() )
                		stmt.set(searchValueFromDate);
                	if ( ! searchValueToDate.isNull() )
                		stmt.set(searchValueToDate);
	            }
	        }
	    }

    	
    	
    	@SuppressWarnings("deprecation")
		public static List<ReportListingInfo> getMatching(Connection con, ReportTemplate reportTemplate,
    														EsfUUID tranId, String dateType, EsfDateTime fromDateTime, EsfDateTime toDateTime, 
    														List<EsfUUID> transactionTemplateIdList, List<EsfUUID> startedByUserIdList, 
    														List<EsfUUID> partyToUserIdList, String partyEmail,
    														boolean showProductionOnly, boolean showInProgress, boolean showCompleted, boolean showCanceled, boolean showSuspended, boolean showStalledOnly,
    														List<ReportTemplateReportField> builtInSearchFields, List<ReportTemplateReportField> userSearchFields, int maxTransactions)
			throws java.sql.SQLException
		{
			EsfPreparedStatement stmt = null;
			
			if ( tranId != null && tranId.isNull() )
				tranId = null;
			
			if ( startedByUserIdList != null && startedByUserIdList.size() == 0 )
				startedByUserIdList = null;
			
			List<String> partyToEmailList = null;
			if ( partyToUserIdList != null && partyToUserIdList.size() == 0 )
				partyToUserIdList = null;
			
			// If they select all possible transaction statuses, then we don't need to select on tran status
			boolean ignoreTranStatus = showInProgress && showCompleted && showCanceled && showSuspended;

			StringBuilder sqlBuf = new StringBuilder(1500);
			sqlBuf.append("SELECT DISTINCT TRAN.id,TRAN.status,TRAN.status_text,TEMPLATE.path_name,PKG.path_name,TRAN.last_updated_timestamp,TRAN.created_timestamp,TRAN.cancel_timestamp,TRAN.expire_timestamp,TRAN.stall_timestamp,TRAN.created_by_user_id,TRAN.last_updated_by_user_id ");
			sqlBuf.append("FROM esf_transaction TRAN,esf_transaction_template TEMPLATE,esf_package PKG ");
			
			if ( EsfString.isNonBlank(partyEmail) || partyToUserIdList != null )
			{
				sqlBuf.append(",esf_transaction_party PARTY,esf_transaction_party_assignment ASSIGN ");
			}
			
			String tranDateField;
			if ( Literals.DATE_STARTED.equals(dateType) )
				tranDateField = "TRAN.created_timestamp";
			else if ( Literals.DATE_CANCELS.equals(dateType) )
				tranDateField = "TRAN.cancel_timestamp";
			else if ( Literals.DATE_EXPIRES.equals(dateType) )
				tranDateField = "TRAN.expire_timestamp";
			else
				tranDateField = "TRAN.last_updated_timestamp";
			
			if ( tranId == null )
			{
				boolean needsWhere = true;
				boolean needsAnd = false;
				
				if ( fromDateTime != null )
				{
					if ( needsWhere ) 
					{
						sqlBuf.append("WHERE ");
						needsWhere = false;
					}
					if ( needsAnd )
						sqlBuf.append("AND ");
					sqlBuf.append(tranDateField).append(">=? ");
					needsAnd = true;
				}
				if ( toDateTime != null )
				{
					if ( needsWhere ) 
					{
						sqlBuf.append("WHERE ");
						needsWhere = false;
					}
					if ( needsAnd )
						sqlBuf.append("AND ");
					sqlBuf.append(tranDateField).append("<=? ");
					needsAnd = true;
				}

				if ( needsWhere ) 
				{
					sqlBuf.append("WHERE ");
					needsWhere = false;
				}
				if ( needsAnd )
					sqlBuf.append("AND ");
				
				sqlBuf.append("TRAN.transaction_template_id IN (");
				boolean needsComma = false;
				for( int i=0; i < transactionTemplateIdList.size(); ++i )
				{
					if ( needsComma )
						sqlBuf.append(',');
					else
						needsComma = true;
					sqlBuf.append('?');
				}
				sqlBuf.append(") ");
				
				if ( showProductionOnly )
					sqlBuf.append("AND TRAN.tran_type=? ");
				else
					sqlBuf.append("AND TRAN.tran_type<>? ");
				
				// If we don't ignore tran status, build the select by status
				if ( ! ignoreTranStatus )
				{
					sqlBuf.append("AND TRAN.status IN (");
					needsComma = false;
					if ( showInProgress )
					{
						if ( needsComma )
							sqlBuf.append(',');
						else
							needsComma = true;
						sqlBuf.append('?');
					}
					if ( showCompleted )
					{
						if ( needsComma )
							sqlBuf.append(',');
						else
							needsComma = true;
						sqlBuf.append('?');
					}
					if ( showCanceled )
					{
						if ( needsComma )
							sqlBuf.append(',');
						else
							needsComma = true;
						sqlBuf.append('?');
					}
					if ( showSuspended )
					{
						if ( needsComma )
							sqlBuf.append(',');
						else
							needsComma = true;
						sqlBuf.append('?');
					}
					sqlBuf.append(") ");					
				}
				
				if ( showStalledOnly )
					sqlBuf.append("AND TRAN.stall_timestamp IS NOT NULL ");
				
				// Set up the started by options
				if ( startedByUserIdList != null )
				{
					boolean needsCreatedByUserIsNull = false;
					boolean needsCreatedByInList = false;
					for ( EsfUUID startedByUserId : startedByUserIdList )
					{
						if ( startedByUserId == null || startedByUserId.isNull() )
							needsCreatedByUserIsNull = true;
						else
							needsCreatedByInList = true;
					}
					if ( needsCreatedByInList || needsCreatedByUserIsNull )
					{
						sqlBuf.append("AND ");
						if ( needsCreatedByInList && needsCreatedByUserIsNull ) // if we need both, we'll need to group them
							sqlBuf.append("( ");
						
						if ( needsCreatedByInList )
						{
							needsComma = false;
							sqlBuf.append("TRAN.created_by_user_id IN (");
							for ( EsfUUID startedByUserId : startedByUserIdList )
							{
								if ( startedByUserId != null && ! startedByUserId.isNull() )
								{
									if ( needsComma )
										sqlBuf.append(',');
									else
										needsComma = true;
									sqlBuf.append('?');
								}
							}
							sqlBuf.append(") ");
							
							if ( needsCreatedByUserIsNull )
								sqlBuf.append("OR ");
						}
						
						if ( needsCreatedByUserIsNull )
							sqlBuf.append("TRAN.created_by_user_id IS NULL ");

						if ( needsCreatedByInList && needsCreatedByUserIsNull )
							sqlBuf.append(") ");
					}
				}
				
				// Set up the party to options
				if ( partyToUserIdList != null )
				{
					partyToEmailList = new LinkedList<String>();
					
					sqlBuf.append("AND ( ");

					needsComma = false;
					sqlBuf.append("ASSIGN.user_id IN (");
					for ( EsfUUID partyToUserId : partyToUserIdList )
					{
						if ( partyToUserId != null && ! partyToUserId.isNull() )
						{
							User u = User.Manager.getById(partyToUserId);
							if ( u != null && ! partyToEmailList.contains(u.getEmail()) )
								partyToEmailList.add(u.getEmail());
							
							if ( needsComma )
								sqlBuf.append(',');
							else
								needsComma = true;
							sqlBuf.append('?');
						}
					}
					sqlBuf.append(") "); // ends ASSIGN.user_id IN
					
					if ( partyToEmailList.size() > 0 )
					{
						needsComma = false;
						sqlBuf.append("OR ASSIGN.email_address IN (");
						for ( @SuppressWarnings("unused") String partyToEmail : partyToEmailList )
						{
							if ( needsComma )
								sqlBuf.append(',');
							else
								needsComma = true;
							sqlBuf.append('?');
						}
						sqlBuf.append(") "); // ends ASSIGN.email_address IN
					}
					sqlBuf.append(") "); // ends AND
				}
				
				if ( EsfString.isNonBlank(partyEmail) )
				{
					boolean negated = partyEmail.startsWith("!");
					if ( negated )
						partyEmail = partyEmail.substring(1).trim(); // this also removes the negation from the partyEmail param so it's already not present further below
					if ( partyEmail.startsWith("=") )
					{
						if ( negated )
							sqlBuf.append("AND ASSIGN.email_address<>? "); // don't include NULL values, even if "not equal"
						else
							sqlBuf.append("AND ASSIGN.email_address=? ");
					}
					else 
					{
						if ( negated )
							sqlBuf.append("AND ASSIGN.email_address NOT LIKE ? ");
						else
							sqlBuf.append("AND ASSIGN.email_address LIKE ? ");
					}
				}

				appendBuiltInSearchKeySubselect(sqlBuf,builtInSearchFields);
			}
			else
			{
				sqlBuf.append("WHERE TRAN.id=? ");
			}
			
			// Join our tables by their ids
			sqlBuf.append("AND TRAN.transaction_template_id=TEMPLATE.id AND TRAN.package_id=PKG.id ");
			
			// Link in our party tables if not doing a tran id search and they have specified a party email or partyTo user ids
			if ( tranId == null && (EsfString.isNonBlank(partyEmail) || partyToUserIdList != null) )
				sqlBuf.append("AND TRAN.id=PARTY.transaction_id AND PARTY.id=ASSIGN.transaction_party_id ");
			
			// Only include user defined searches if not doing a tran-id search
			if ( tranId == null )
				appendUserSearchKeySubselect(sqlBuf,userSearchFields);
		    
			sqlBuf.append("ORDER BY ").append(tranDateField).append(" DESC");
			
			if ( maxTransactions > 0 )
				sqlBuf.append(" LIMIT ").append(maxTransactions);
			
			LinkedList<ReportListingInfo> list = new LinkedList<ReportListingInfo>();
			
		    try 
		    {
		    	stmt = new EsfPreparedStatement( con, sqlBuf.toString() );
		    	
		    	if ( tranId == null )
		    	{
					if ( fromDateTime != null )
						stmt.set(fromDateTime);
					if ( toDateTime != null )
						stmt.set(toDateTime);

					for( EsfUUID transactionTemplateId : transactionTemplateIdList )
						stmt.set(transactionTemplateId);
					
					stmt.set(Transaction.TRAN_TYPE_PRODUCTION);
					
					if ( ! ignoreTranStatus )
					{
						if ( showInProgress )
							stmt.set(Transaction.TRAN_STATUS_IN_PROGRESS);
				        
						if ( showCompleted )
							stmt.set(Transaction.TRAN_STATUS_COMPLETED);
				        
						if ( showCanceled )
							stmt.set(Transaction.TRAN_STATUS_CANCELED);
				        
						if ( showSuspended )
							stmt.set(Transaction.TRAN_STATUS_SUSPENDED);
					}

					if ( startedByUserIdList != null )
					{
						for ( EsfUUID startedByUserId : startedByUserIdList )
						{
							if ( startedByUserId != null && ! startedByUserId.isNull() )
								stmt.set(startedByUserId);
						}
					}
			        
					if ( partyToUserIdList != null )
					{
						for ( EsfUUID partyToUserId : partyToUserIdList )
						{
							if ( partyToUserId != null && ! partyToUserId.isNull() )
								stmt.set(partyToUserId);
						}
						for ( String partyToEmail : partyToEmailList )
						{
							stmt.set(partyToEmail.toLowerCase());
						}
					}
			        
					if ( EsfString.isNonBlank(partyEmail) ) // any not "!" prefix has already been removed above
					{
		        		if ( partyEmail.startsWith("=") )
		                	stmt.set(partyEmail.substring(1).toLowerCase());
		        		else if ( partyEmail.startsWith("^") )
		    				stmt.set(escapeLIKE(partyEmail.substring(1).toLowerCase())+"%");
		        		else
		        			stmt.set("%"+escapeLIKE(partyEmail.toLowerCase())+"%");
					}
					
					appendBuiltInSearchKeyWhereParams(stmt,builtInSearchFields);
					appendUserSearchKeyWhereParams(stmt,userSearchFields);
		    	}
		    	else
		    	{
		    		stmt.set(tranId);
		    	}
		    	
				_logger.debug("Manager.getMatching(con) - " + stmt.toString());
				EsfResultSet rs = stmt.executeQuery();
				while( rs.next() )
				{
					EsfUUID id = rs.getEsfUUID();
					String status = rs.getString();
					String statusText = rs.getString();
					EsfPathName templatePathName = rs.getEsfPathName();
					EsfPathName packagePathName = rs.getEsfPathName();
					EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
					EsfDateTime createdTimestamp = rs.getEsfDateTime();
					EsfDateTime cancelTimestamp = rs.getEsfDateTime();
					EsfDateTime expireTimestamp = rs.getEsfDateTime();
					EsfDateTime stallTimestamp = rs.getEsfDateTime();
					EsfUUID createdByUserId = rs.getEsfUUID();
					EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
					
					List<ReportTemplateReportField> reportFieldValueList = ReportFieldValue.Manager.getCustom(id,reportTemplate.getReportTemplateReportFieldList());
					
					for( ReportTemplateReportField rtrf : reportFieldValueList )
					{
						if ( ! rtrf.hasFieldValueOrValues() )
						{
							ReportFieldTemplate reportFieldTemplate = rtrf.getReportFieldTemplate();
							if ( reportFieldTemplate.isFieldTypeBuiltIn() )
							{
								if ( reportFieldTemplate.getFieldName().equals("esf_transaction_id") )
									rtrf.setFieldValue(id);
								else if ( reportFieldTemplate.getFieldName().equals("esf_transaction_template_name") )
									rtrf.setFieldValue(templatePathName);
								else if ( reportFieldTemplate.getFieldName().equals("esf_package_name") )
									rtrf.setFieldValue(packagePathName);
								else if ( reportFieldTemplate.getFieldName().equals("esf_status") )
									rtrf.setFieldValue(new EsfString(status));
								else if ( reportFieldTemplate.getFieldName().equals("esf_status_text") )
									rtrf.setFieldValue(new EsfString(statusText));
								else if ( reportFieldTemplate.getFieldName().equals("esf_start_timestamp") )
									rtrf.setFieldValue(createdTimestamp);
								else if ( reportFieldTemplate.getFieldName().equals("esf_last_updated_timestamp") )
									rtrf.setFieldValue(lastUpdatedTimestamp);
								else if ( reportFieldTemplate.getFieldName().equals("esf_created_by_user") )
									rtrf.setFieldValue(createdByUserId);
								else if ( reportFieldTemplate.getFieldName().equals("esf_last_updated_by_user") )
									rtrf.setFieldValue(lastUpdatedByUserId);
								else if ( reportFieldTemplate.getFieldName().equals("esf_expire_timestamp") )
									rtrf.setFieldValue(expireTimestamp);
								else if ( reportFieldTemplate.getFieldName().equals("esf_stall_timestamp") )
									rtrf.setFieldValue(stallTimestamp);
								else if ( reportFieldTemplate.getFieldName().equals("esf_cancel_timestamp") )
									rtrf.setFieldValue(cancelTimestamp);
								else if ( reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_LATEST_SNAPSHOTS) )
									rtrf.setFieldValue(null);
								else if ( reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_SELECTED_SNAPSHOTS) )
									rtrf.setFieldValue(null);
								else if ( reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_SELECTED_SNAPSHOTS) )
									rtrf.setFieldValue(null);
								else if ( reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_HTML_SELECTED_SNAPSHOT) )
									rtrf.setFieldValue(null);
								else if ( reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_RESUME_TRANSACTION_BUTTON) )
									rtrf.setFieldValue(null);
								else if ( reportFieldTemplate.getFieldName().equals("esf_literal") )
									rtrf.setFieldValue(new EsfString(rtrf.getOutputFormatSpec()));
								else 
									rtrf.setFieldValue(new EsfString("??"+reportFieldTemplate.getFieldName()+"??"));
							}
						}
					}
					
					ReportListingInfo reportListing = new ReportListingInfo(id,status,statusText,templatePathName,packagePathName,lastUpdatedTimestamp,createdTimestamp,cancelTimestamp,expireTimestamp,stallTimestamp,reportFieldValueList);
					list.add(reportListing);
				}
				
				return list;
			}
		    catch( java.sql.SQLException e )
		    {
		        _logger.sqlerr(e,"Manager.getMatching(" + fromDateTime + "," + toDateTime + ")");
		        throw e;
		    }
		    finally
		    {
		    	cleanupStatement(stmt);
		    }
		}
	
		/**
		 * Retrieves matching transactions for a custom report.
		 * Either startedByUserIdList or partyToUserIdList must be non-null.
		 * @param fromDateTime the EsfDateTime of the earliest/oldest last updated date/time to retrieve.  If null, no lower date range is set (from earliest date possible)
		 * @param toDateTime the EsfDateTime of the latest/newest last updated date/time to retrieve.  If null, no upper date range is set (through current time)
		 * @return
		 */
    	public static List<ReportListingInfo> getMatching(ReportTemplate reportTemplate, EsfUUID tranId, String dateType, EsfDateTime fromDateTime, EsfDateTime toDateTime,
    														List<EsfUUID> transactionTemplateIdList, List<EsfUUID> startedByUserIdList, 
    														List<EsfUUID> partyToUserIdList, String partyEmail,
    														boolean showProductionOnly, boolean showInProgress, boolean showCompleted, boolean showCanceled, boolean showSuspended, boolean showStalledOnly,
    														List<ReportTemplateReportField> builtInSearchFields, List<ReportTemplateReportField> userSearchFields, int maxTransactions)
		{
		    ConnectionPool    pool = getConnectionPool();
		    Connection        con  = pool.getConnection();
		    
		    try 
		    {
		    	List<ReportListingInfo> list = getMatching(con, reportTemplate, tranId, dateType, fromDateTime, toDateTime, 
		    									transactionTemplateIdList, startedByUserIdList, partyToUserIdList, partyEmail, 
		    									showProductionOnly, showInProgress, showCompleted, showCanceled, showSuspended, showStalledOnly, 
		    									builtInSearchFields, userSearchFields, maxTransactions);
		        con.commit();
		        return list;
		    } 
		    catch(java.sql.SQLException e) 
		    {
		        pool.rollbackIgnoreException(con,e);
		        return null;
		    }
		    finally
		    {
		    	cleanupPool(pool,con,null);
		    }
		}
		
    } // end Manager

}