// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.reports;

import java.sql.Connection;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.runtime.Transaction;

/**
 * Represents a single transaction listing.
 *
 * @author Yozons, Inc.
 */
public class TransactionListingInfo
	extends com.esignforms.open.db.DatabaseObject 
	implements java.lang.Comparable<TransactionListingInfo>
{
	private static final long serialVersionUID = 3503452796733503545L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TransactionListingInfo.class);

	private EsfUUID id;
    private String status;
    private String statusText;
	private EsfPathName templatePathName;
	private EsfPathName packagePathName;
	private EsfDateTime lastUpdatedTimestamp;
	private EsfDateTime createdTimestamp;
	private EsfDateTime cancelTimestamp;
	private EsfDateTime expireTimestamp;
	private EsfDateTime stallTimestamp;
	
	public TransactionListingInfo(EsfUUID id,String status,String statusText,EsfPathName templatePathName,EsfPathName packagePathName,
								  EsfDateTime lastUpdatedTimestamp,EsfDateTime createdTimestamp,EsfDateTime cancelTimestamp,EsfDateTime expireTimestamp,EsfDateTime stallTimestamp)
    {
		this.id = id;
		this.status = status;
		this.statusText = statusText;
		this.templatePathName = templatePathName;
		this.packagePathName = packagePathName;
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
		this.createdTimestamp = createdTimestamp;
		this.cancelTimestamp = cancelTimestamp;
		this.expireTimestamp = expireTimestamp;
		this.stallTimestamp = stallTimestamp;
    }
    
	public EsfUUID getId()
	{
		return id;
	}
	
	public String getStatus()
	{
		return status;
	}
	
	public String getStatusText()
	{
		return statusText;
	}
	
	public EsfPathName getTemplatePathName()
	{
		return templatePathName;
	}
	
	public EsfPathName getPackagePathName()
	{
		return packagePathName;
	}
	
	public EsfDateTime getLastUpdatedTimestamp()
	{
		return lastUpdatedTimestamp;
	}
	
	public EsfDateTime getCreatedTimestamp()
	{
		return createdTimestamp;
	}
	
	public EsfDateTime getCancelTimestamp()
	{
		return cancelTimestamp;
	}
	
	public EsfDateTime getExpireTimestamp()
	{
		return expireTimestamp;
	}

	public EsfDateTime getStallTimestamp()
	{
		return stallTimestamp;
	}
	
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof TransactionListingInfo )
        {
        	TransactionListingInfo other = (TransactionListingInfo)o;
            return getId().equals(other.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getLastUpdatedTimestamp().hashCode() + getId().hashCode();
    }
    
    @Override
    public int compareTo(TransactionListingInfo o)
    {
    	int diff = getLastUpdatedTimestamp().compareTo(o.getLastUpdatedTimestamp());
    	if ( diff != 0 )
    		return diff;
    	return getId().compareTo(o.getId());
    }

	
	/******************* DB routines */
    public static class Manager
    {
    	public static List<TransactionListingInfo> getMatching(Connection con, EsfUUID tranId, String partyPickupCode, 
    														   EsfDateTime fromDateTime, EsfDateTime toDateTime, List<EsfUUID> transactionTemplateIdList, EsfUUID limitToPackageVersionId, String partyEmail,
    														   boolean showProductionOnly, boolean showInProgress, boolean showCompleted, boolean showCanceled, boolean showSuspended, boolean showStalledOnly,
    														   int maxTransactions)
			throws java.sql.SQLException
		{
			EsfPreparedStatement stmt = null;
			
			if ( tranId != null && tranId.isNull() )
				tranId = null;
			
			if ( limitToPackageVersionId != null && limitToPackageVersionId.isNull() )
				limitToPackageVersionId = null;
			
			if ( tranId != null || EsfString.isBlank(partyPickupCode) ) // tran id search overrides party pickup code search
				partyPickupCode = null;
			
			// If they select all possible transaction statuses, then we don't need to select on tran status
			boolean ignoreTranStatus = showInProgress && showCompleted && showCanceled && showSuspended;

			StringBuilder sqlBuf = new StringBuilder(1000);
			sqlBuf.append("SELECT TRAN.id,TRAN.status,TRAN.status_text,TEMPLATE.path_name,PKG.path_name,TRAN.last_updated_timestamp,TRAN.created_timestamp,TRAN.cancel_timestamp,TRAN.expire_timestamp,TRAN.stall_timestamp ");
			sqlBuf.append("FROM esf_transaction TRAN,esf_transaction_template TEMPLATE,esf_package PKG ");
			
			if ( tranId == null && EsfString.areAnyNonBlank(partyEmail,partyPickupCode) )
			{
				sqlBuf.append(",esf_transaction_party PARTY,esf_transaction_party_assignment ASSIGN ");
			}
			
			if ( tranId != null )
			{
				sqlBuf.append("WHERE TRAN.id=? ");
			}
			else if ( partyPickupCode != null )
			{
				sqlBuf.append("WHERE ASSIGN.pickup_code=? ");
			}
			else
			{
				boolean needsWhere = true;
				boolean needsAnd = false;
				
				if ( limitToPackageVersionId != null )
				{
					sqlBuf.append("WHERE TRAN.package_version_id=? ");
					needsWhere = false;
					needsAnd = true;
				}
				
				if ( fromDateTime != null )
				{
					if ( needsWhere ) 
					{
						sqlBuf.append("WHERE ");
						needsWhere = false;
					}
					if ( needsAnd )
						sqlBuf.append("AND ");
					sqlBuf.append("TRAN.last_updated_timestamp>=? ");
					needsAnd = true;
				}
				if ( toDateTime != null )
				{
					if ( needsWhere ) 
					{
						sqlBuf.append("WHERE ");
						needsWhere = false;
					}
					if ( needsAnd )
						sqlBuf.append("AND ");
					sqlBuf.append("TRAN.last_updated_timestamp<=? ");
					needsAnd = true;
				}

				if ( needsWhere ) 
				{
					sqlBuf.append("WHERE ");
					needsWhere = false;
				}
				if ( needsAnd )
					sqlBuf.append("AND ");
				
				sqlBuf.append("TRAN.transaction_template_id IN (");
				boolean needsComma = false;
				for( int i=0; i < transactionTemplateIdList.size(); ++i )
				{
					if ( needsComma )
						sqlBuf.append(',');
					else
						needsComma = true;
					sqlBuf.append('?');
				}
				sqlBuf.append(") ");
				
				if ( showProductionOnly )
					sqlBuf.append("AND TRAN.tran_type=? ");
				else
					sqlBuf.append("AND TRAN.tran_type<>? ");
				
				// If we don't ignore tran status, build the select by status
				if ( ! ignoreTranStatus )
				{
					sqlBuf.append("AND TRAN.status IN (");
					needsComma = false;
					if ( showInProgress )
					{
						if ( needsComma )
							sqlBuf.append(',');
						else
							needsComma = true;
						sqlBuf.append('?');
					}
					if ( showCompleted )
					{
						if ( needsComma )
							sqlBuf.append(',');
						else
							needsComma = true;
						sqlBuf.append('?');
					}
					if ( showCanceled )
					{
						if ( needsComma )
							sqlBuf.append(',');
						else
							needsComma = true;
						sqlBuf.append('?');
					}
					if ( showSuspended )
					{
						if ( needsComma )
							sqlBuf.append(',');
						else
							needsComma = true;
						sqlBuf.append('?');
					}
					sqlBuf.append(") ");
				}				
				
				if ( showStalledOnly )
					sqlBuf.append("AND TRAN.stall_timestamp IS NOT NULL ");
				
				if ( EsfString.isNonBlank(partyEmail) )
				{
					if ( partyEmail.startsWith("=") )
						sqlBuf.append("AND ASSIGN.email_address=? ");
					else 
						sqlBuf.append("AND ASSIGN.email_address LIKE ? ");
				}
			}
			
			// Join our tables by their ids
			sqlBuf.append("AND TRAN.transaction_template_id=TEMPLATE.id AND TRAN.package_id=PKG.id ");
		    
			// Link in our party tables if not doing a tran id search and they have specified a party email or party pickup code
			if ( tranId == null && EsfString.areAnyNonBlank(partyEmail,partyPickupCode) )
				sqlBuf.append("AND TRAN.id=PARTY.transaction_id AND PARTY.id=ASSIGN.transaction_party_id ");
			
			sqlBuf.append("ORDER BY TRAN.last_updated_timestamp DESC");
			
			if ( maxTransactions > 0 )
				sqlBuf.append(" LIMIT ").append(maxTransactions);
			
			LinkedList<TransactionListingInfo> list = new LinkedList<TransactionListingInfo>();
			
		    try 
		    {
		    	stmt = new EsfPreparedStatement( con, sqlBuf.toString() );
		    	
		    	if ( tranId != null )
		    	{
		    		stmt.set(tranId);
		    	}
		    	else if ( partyPickupCode != null )
		    	{
		    		stmt.set(partyPickupCode);
		    	}
		    	else
		    	{
		    		if ( limitToPackageVersionId != null )
		    			stmt.set(limitToPackageVersionId);
					if ( fromDateTime != null )
						stmt.set(fromDateTime);
					if ( toDateTime != null )
						stmt.set(toDateTime);

					for( EsfUUID transactionTemplateId : transactionTemplateIdList )
						stmt.set(transactionTemplateId);
					
					stmt.set(Transaction.TRAN_TYPE_PRODUCTION);
					
					if ( ! ignoreTranStatus )
					{
						if ( showInProgress )
							stmt.set(Transaction.TRAN_STATUS_IN_PROGRESS);
				        
						if ( showCompleted )
							stmt.set(Transaction.TRAN_STATUS_COMPLETED);
				        
						if ( showCanceled )
							stmt.set(Transaction.TRAN_STATUS_CANCELED);
				        
						if ( showSuspended )
							stmt.set(Transaction.TRAN_STATUS_SUSPENDED);
					}
					
					if ( EsfString.isNonBlank(partyEmail) )
					{
		        		if ( partyEmail.startsWith("=") )
		                	stmt.set(partyEmail.substring(1).toLowerCase());
		        		else if ( partyEmail.startsWith("^") )
		    				stmt.set(escapeLIKE(partyEmail.substring(1).toLowerCase())+"%");
		        		else
		        			stmt.set("%"+escapeLIKE(partyEmail.toLowerCase())+"%");
					}
		    	}
		        
				_logger.debug("Manager.getMatching(con) - " + stmt.toString());
				EsfResultSet rs = stmt.executeQuery();
				while( rs.next() )
				{
					EsfUUID id = rs.getEsfUUID();
					String status = rs.getString();
					String statusText = rs.getString();
					EsfPathName templatePathName = rs.getEsfPathName();
					EsfPathName packagePathName = rs.getEsfPathName();
					EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
					EsfDateTime createdTimestamp = rs.getEsfDateTime();
					EsfDateTime cancelTimestamp = rs.getEsfDateTime();
					EsfDateTime expireTimestamp = rs.getEsfDateTime();
					EsfDateTime stallTimestamp = rs.getEsfDateTime();
					TransactionListingInfo tranListing = new TransactionListingInfo(id,status,statusText,templatePathName,packagePathName,lastUpdatedTimestamp,createdTimestamp,cancelTimestamp,expireTimestamp,stallTimestamp);
					list.add(tranListing);
				}
				
				return list;
			}
		    catch( java.sql.SQLException e )
		    {
		        _logger.sqlerr(e,"Manager.getMatching(" + fromDateTime + "," + toDateTime + ")");
		        throw e;
		    }
		    finally
		    {
		    	cleanupStatement(stmt);
		    }
		}
	
		/**
		 * Retrieves matching transactions.
		 * @param fromDateTime the EsfDateTime of the earliest/oldest last updated date/time to retrieve.  If null, no lower date range is set (from earliest date possible)
		 * @param toDateTime the EsfDateTime of the latest/newest last updated date/time to retrieve.  If null, no upper date range is set (through current time)
		 * @return
		 */
    	public static List<TransactionListingInfo> getMatching(EsfUUID tranId, String partyPickupCode,
    														   EsfDateTime fromDateTime, EsfDateTime toDateTime, List<EsfUUID> transactionTemplateIdList, EsfUUID limitToPackageVersionId, String partyEmail,
				   											   boolean showProductionOnly, boolean showInProgress, boolean showCompleted, boolean showCanceled, boolean showSuspended, boolean showStalledOnly, int maxTransactions)
		{
		    ConnectionPool    pool = getConnectionPool();
		    Connection        con  = pool.getConnection();
		    
		    try 
		    {
		    	List<TransactionListingInfo> list = getMatching(con, tranId, partyPickupCode, 
		    												    fromDateTime, toDateTime, transactionTemplateIdList, limitToPackageVersionId, partyEmail, 
		    												    showProductionOnly, showInProgress, showCompleted, showCanceled, showSuspended, showStalledOnly, maxTransactions);
		        con.commit();
		        return list;
		    } 
		    catch(java.sql.SQLException e) 
		    {
		        pool.rollbackIgnoreException(con,e);
		        return null;
		    }
		    finally
		    {
		    	cleanupPool(pool,con,null);
		    }
		}
    	
		/**
		 * This reloads the tran data (for report fields) for all transactions listed.
		 */
    	public static int reloadTransactionData(List<TransactionListingInfo> tranInfoList)
		{
		    ConnectionPool    pool = getConnectionPool();
		    Connection        con  = pool.getConnection();
		    
		    int numReloaded = 0;
		    
		    try 
		    {
		    	for( TransactionListingInfo info : tranInfoList )
		    	{
		    		Transaction tran = Transaction.Manager.getById(con, info.getId());
		    		if ( tran != null )
		    		{
		    			tran.saveReportFields(con,false); // reload even if the data hasn't changed in the tran object itself
		    			Transaction.Manager.removeFromCache(tran);
		    			++numReloaded;
		    		}
		    		else
		    		{
		    			_logger.warn("Manager.reloadTransactionData() - specified transaction id: " + info.getId() + ", but the transaction was not found."); 
		    		}
		    		
		    		// Let's commit reloads after we several just to be safe against big rollbacks
		    		if ( (numReloaded % 100) == 0 )
		    			con.commit();
		    	}
		    	
	        	if ( (numReloaded % 100) != 0 )
	                con.commit();
	        	
	        	return numReloaded;
		    } 
		    catch(java.sql.SQLException e) 
		    {
		    	_logger.error("Manager.reloadTransactionData()",e);
		        pool.rollbackIgnoreException(con,e);
		        return 0;
		    }
		    finally
		    {
		    	cleanupPool(pool,con,null);
		    }
		}
		
    } // end Manager

}