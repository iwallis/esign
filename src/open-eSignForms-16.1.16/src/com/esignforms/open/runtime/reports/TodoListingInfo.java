// Copyright (C) 2011-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.reports;

import java.sql.Connection;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionReportField;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.prog.ReportFieldValue;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionParty;

/**
 * Represents a single To Do listing.
 *
 * @author Yozons, Inc.
 */
public class TodoListingInfo
	extends com.esignforms.open.db.DatabaseObject 
	implements java.lang.Comparable<TodoListingInfo>
{
	private static final long serialVersionUID = -793918416243463311L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TodoListingInfo.class);

    private EsfUUID transactionId;
    private String statusText;
    private String pickupCode;
    private EsfUUID todoGroupId;
    private EsfUUID userId;
    private EsfUUID packageVersionId;
	private EsfPathName templatePathName;
	private String templateDisplayName;
	private EsfDateTime lastUpdatedTimestamp;
	private EsfName partyName;
	private String partyDisplayName;
	private List<ReportFieldValue> reportFieldValueList;
	
	public TodoListingInfo(EsfUUID transactionId,String statusText,String pickupCode, EsfUUID todoGroupId, EsfUUID userId, EsfUUID packageVersionId, EsfPathName templatePathName, String templateDisplayName,
			EsfDateTime lastUpdatedTimestamp, EsfName partyName, String partyDisplayName, List<ReportFieldValue> reportFieldValueList )
    {
		this.transactionId = transactionId;
		this.statusText = statusText;
		this.pickupCode = pickupCode;
		this.todoGroupId = todoGroupId;
		this.userId = userId;
		this.packageVersionId = packageVersionId;
		this.templatePathName = templatePathName;
		this.templateDisplayName = templateDisplayName;
		this.lastUpdatedTimestamp = lastUpdatedTimestamp;
		this.partyName = partyName;
		this.partyDisplayName = partyDisplayName;
		this.reportFieldValueList = reportFieldValueList;
    }
    
	public EsfUUID getTransactionId()
	{
		return transactionId;
	}
	
	public String getStatusText()
	{
		return statusText;
	}
	
	public String getPickupCode()
	{
		return pickupCode;
	}
	
	public EsfUUID getToDoGroupId()
	{
		return todoGroupId;
	}
	public boolean hasToDoGroupId()
	{
		return todoGroupId != null;
	}
	
	public EsfUUID getUserId()
	{
		return userId;
	}
	public boolean hasUserId()
	{
		return userId != null;
	}
	
	public EsfUUID getPackageVersionId()
	{
		return packageVersionId;
	}
	
	public String getPackagePathNameVersionWithLabel()
	{
		PackageVersion pkgVersion = PackageVersion.Manager.getById(packageVersionId);
		if ( pkgVersion == null )
			return "? [?]";
		return pkgVersion.getPackagePathNameVersionWithLabel();
	}
	
	public EsfPathName getTemplatePathName()
	{
		return templatePathName;
	}
	
	public String getTemplateDisplayName()
	{
		return templateDisplayName;
	}
	
	public EsfDateTime getLastUpdatedTimestamp()
	{
		return lastUpdatedTimestamp;
	}
	
	public EsfName getPartyName()
	{
		return partyName;
	}
	
	public String getPartyDisplayName()
	{
		return partyDisplayName;
	}
	
	public List<ReportFieldValue> getReportFieldValueList()
	{
		return reportFieldValueList;
	}
	
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof TodoListingInfo )
        {
        	TodoListingInfo other = (TodoListingInfo)o;
            return getTemplatePathName().equals(other.getTemplatePathName()) &&
            	    getPartyName().equals(other.getPartyName()) &&
            	    getLastUpdatedTimestamp().equals(other.getLastUpdatedTimestamp()) &&
            	    getPickupCode().equals(other.getPickupCode());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getTemplatePathName().hashCode() + getPartyName().hashCode() + getLastUpdatedTimestamp().hashCode() + getPickupCode().hashCode();
    }
    
    @Override
    public int compareTo(TodoListingInfo o)
    {
    	int diff = getTemplatePathName().compareTo(o.getTemplatePathName());
    	if ( diff != 0 )
    		return diff;
    	
    	diff = getPartyName().compareTo(o.getPartyName());
    	if ( diff != 0 )
    		return diff;
    		
    	diff = getLastUpdatedTimestamp().compareTo(o.getLastUpdatedTimestamp());
    	if ( diff != 0 )
    		return diff;
    	return getPickupCode().compareTo(o.getPickupCode());
    }

	
	/******************* DB routines */
    public static class Manager
    {
    	public static List<TodoListingInfo> getMatching(Connection con, boolean showProductionOnly, EsfUUID forUserId, String forEmailAddress)
			throws java.sql.SQLException
		{
			EsfPreparedStatement stmt = null;
			
			// We break on package versions since they have To Do report fields that may change between package versions, regardless of transaction template type.
			StringBuilder sqlBuf = new StringBuilder(1000);
			sqlBuf.append("SELECT T.package_version_id, T.id, TT.path_name, TT.display_name, TP.transaction_party_assignment_pickup_code, TP.todo_group_id, TPA.user_id, T.last_updated_timestamp, T.status_text, PVPT.esfname, PVPT.display_name ");
			sqlBuf.append("FROM esf_transaction_party TP, esf_transaction_party_assignment TPA, esf_transaction T, esf_transaction_template TT, esf_package_version_party_template PVPT, esf_package_version PV ");
			sqlBuf.append("WHERE TP.transaction_status=? AND TP.status=? AND ");
			sqlBuf.append("(TPA.user_id=? OR TPA.email_address=? OR ");
					sqlBuf.append("(TPA.user_id IS NULL AND TP.todo_group_id IN (SELECT group_id FROM esf_group_user WHERE esf_group_user.user_id=?) )");
			sqlBuf.append(") ");
			
			if ( showProductionOnly )
				sqlBuf.append("AND T.tran_type=? ");
			else
				sqlBuf.append("AND T.tran_type<>? ");
			
			// Join our tables by their ids
			sqlBuf.append("AND TP.transaction_party_assignment_pickup_code = TPA.pickup_code AND TP.package_version_party_template_id = PVPT.id AND TP.transaction_id = T.id AND T.transaction_template_id = TT.id AND T.package_version_id = PV.id ");
		    
			sqlBuf.append("ORDER BY lower(TT.path_name) ASC, PV.version DESC, lower(PVPT.esfname) ASC, T.last_updated_timestamp ASC");
			
			LinkedList<TodoListingInfo> list = new LinkedList<TodoListingInfo>();
			
		    try 
		    {
		    	stmt = new EsfPreparedStatement( con, sqlBuf.toString() );
				stmt.set(Transaction.TRAN_STATUS_IN_PROGRESS);
				stmt.set(TransactionParty.STATUS_ACTIVE);
				stmt.set(forUserId);
				stmt.set(forEmailAddress.toLowerCase());
				stmt.set(forUserId);
				stmt.set(Transaction.TRAN_TYPE_PRODUCTION);
		        
				EsfResultSet rs = stmt.executeQuery();
				
				EsfUUID previousPackageVersionId = new EsfUUID(); // won't match anything
				List<ReportFieldTemplate> reportFieldTemplateList = null; // We load this for each package version we come across
				
				while( rs.next() )
				{
					EsfUUID packageVersionId = rs.getEsfUUID();
					EsfUUID transactionId = rs.getEsfUUID(); // we need to find associated To Do report fields
					EsfPathName templatePathName = rs.getEsfPathName();
					String templateDisplayName = rs.getString();
					String pickupCode = rs.getString();
					EsfUUID todoGroupId = rs.getEsfUUID();
					EsfUUID userId = rs.getEsfUUID();
					EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
					String statusText = rs.getString();
					EsfName partyName = rs.getEsfName();
					String partyDisplayName = rs.getString();
					
					// If we change package versions, get the To Do fields for this package
					if ( ! previousPackageVersionId.equals(packageVersionId) )
					{
						List<EsfUUID> reportFieldTemplateIdList = PackageVersionReportField.Manager.getToDoReportFieldTemplateIdList(packageVersionId);
						reportFieldTemplateList = ReportFieldTemplate.Manager.getByIdList(reportFieldTemplateIdList);
						previousPackageVersionId = packageVersionId;
					}

					List<ReportFieldValue> reportFieldValueList = ReportFieldValue.Manager.get(transactionId,reportFieldTemplateList);

					TodoListingInfo todoListing = new TodoListingInfo(transactionId,statusText,pickupCode,todoGroupId,userId,packageVersionId,templatePathName,templateDisplayName,lastUpdatedTimestamp,partyName,partyDisplayName,reportFieldValueList);
					list.add(todoListing);
				}
				
				return list;
			}
		    catch( java.sql.SQLException e )
		    {
		        _logger.sqlerr(e,"Manager.getMatching(" + forUserId + "," + forEmailAddress + ")");
		        throw e;
		    }
		    finally
		    {
		    	cleanupStatement(stmt);
		    }
		}
	
		/**
		 * Retrieves transactions awaiting the specified user for To Do.
		 * @return
		 */
    	public static List<TodoListingInfo> getMatching(boolean showProductionOnly, EsfUUID forUserId, String forEmailAddress)
		{
		    ConnectionPool    pool = getConnectionPool();
		    Connection        con  = pool.getConnection();
		    
		    try 
		    {
		    	List<TodoListingInfo> list = getMatching(con, showProductionOnly, forUserId, forEmailAddress);
		        con.commit();
		        return list;
		    } 
		    catch(java.sql.SQLException e) 
		    {
		        pool.rollbackIgnoreException(con,e);
		        return null;
		    }
		    finally
		    {
		    	cleanupPool(pool,con,null);
		    }
		}
		
    } // end Manager

}