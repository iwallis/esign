// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime;

import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;
import org.jdom2.output.XMLOutputter;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.admin.SignatureKey;
import com.esignforms.open.config.Literals;
import com.esignforms.open.crypto.XmlDigitalSignature;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.db.BlobDb.CompressOption;
import com.esignforms.open.db.BlobDb.EncryptOption;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.XmlUtil;

/**
* TransactionPartyDocument holds information about a specific transaction party's access to a document.
* 
* @author Yozons, Inc.
*/
public class TransactionPartyDocument
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<TransactionPartyDocument>, java.io.Serializable
{
	private static final long serialVersionUID = -7833134809578742123L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TransactionPartyDocument.class);
    
    public static final String STATUS_NOT_YET_RETRIEVED = "U"; // Not yet retrieved (undefined/assigned to party)
    public static final String STATUS_VIEW_OPTIONAL = "V"; // View optional (an end state) -- NOT YET RETRIEVED but also COMPLETED
    public static final String STATUS_VIEW_OPTIONAL_VIEWED = "v"; // View optional was viewed (an end state) also COMPLETED
    public static final String STATUS_RETRIEVED = "R"; // read or retrieved
    public static final String STATUS_FIXED_REQUESTED = "F"; // a completed document that has been returned to the party to be fixed/corrected (no longer completed)
    public static final String STATUS_COMPLETED = "C"; // retrieved and then completed
    public static final String STATUS_REJECTED = "X"; // retrieved and then rejected (an end state)
    public static final String STATUS_SKIPPED = "S"; // skipped means that the document is no longer accessible to the pre-defined party

    protected final EsfUUID transactionPartyId; // The transaction party working the document
    protected final EsfUUID transactionDocumentId; // The transaction document
    protected final short documentNumber; // Order this document is shown to the specified party
    protected String status;
    protected EsfDateTime lastUpdatedTimestamp;
    
    private boolean snapshotXmlChanged;
    protected String snapshotDocumentSkipped; // N or Y
    protected EsfUUID snapshotBlobId;
    protected String snapshotXml;
    protected EsfDateTime snapshotTimestamp;
    protected String documentFileName; // if not null, use this as the file name when showing the document in the snapshot
    
    protected String esignIpHostAddr;
    protected EsfDateTime esignTimestamp;
    
    /**
     * This creates a TransactionPartyDocument object from data retrieved from the DB.
     */
    protected TransactionPartyDocument(EsfUUID transactionPartyId, EsfUUID transactionDocumentId, short documentNumber, String status, EsfDateTime lastUpdatedTimestamp, 
    								   String snapshotDocumentSkipped, EsfUUID snapshotBlobId, String snapshotXml, EsfDateTime snapshotTimestamp, String documentFileName, 
    								   String esignIpHostAddr, EsfDateTime esignTimestamp)
    {
        this.transactionPartyId = transactionPartyId;
    	this.transactionDocumentId = transactionDocumentId;
    	this.documentNumber = documentNumber;
        this.status = status;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.snapshotXmlChanged = false;
        this.snapshotDocumentSkipped = snapshotDocumentSkipped;
        this.snapshotBlobId = snapshotBlobId;
        this.snapshotXml = snapshotXml;
        this.snapshotTimestamp = snapshotTimestamp;
        this.documentFileName = documentFileName;
        this.esignIpHostAddr = esignIpHostAddr;
        this.esignTimestamp = esignTimestamp;
    }
    
    protected TransactionPartyDocument(EsfUUID transactionPartyId, EsfUUID transactionDocumentId, short documentNumber)
    {
        this.transactionPartyId = transactionPartyId;
    	this.transactionDocumentId = transactionDocumentId;
    	this.documentNumber = documentNumber;
        this.status = STATUS_NOT_YET_RETRIEVED;
        this.lastUpdatedTimestamp = new EsfDateTime();
        this.snapshotXmlChanged = false;
        this.snapshotDocumentSkipped = Literals.N;
        this.snapshotXml = null;
        this.snapshotTimestamp = null;
        this.documentFileName = null;
        this.esignIpHostAddr = null;
        this.esignTimestamp = null;
    }

   
    public final EsfUUID getTransactionPartyId()
    {
        return transactionPartyId;
    }
    
    public final EsfUUID getTransactionDocumentId()
    {
        return transactionDocumentId;
    }
    
    public final short getDocumentNumber()
    {
    	return documentNumber;
    }
    
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String v)
    {
    	if ( ! status.equals(v) )
    	{
        	status = v;
        	lastUpdatedTimestamp = new EsfDateTime();
        	objectChanged();
    	}
    }
    public boolean isNetYetRetrieved()
    {
    	return STATUS_NOT_YET_RETRIEVED.equals(status);
    }
    public void setStatusNotYetRetrieved()
    {
    	setStatus(STATUS_NOT_YET_RETRIEVED);
    }
    public boolean isViewOptional()
    {
    	return STATUS_VIEW_OPTIONAL.equals(status);
    }
    public void setStatusViewOptional()
    {
    	setStatus(STATUS_VIEW_OPTIONAL);
    }
    public boolean isViewOptionalViewed()
    {
    	return STATUS_VIEW_OPTIONAL_VIEWED.equals(status);
    }
    public void setStatusViewOptionalViewed()
    {
    	setStatus(STATUS_VIEW_OPTIONAL_VIEWED);
    }
    public boolean isRetrieved()
    {
    	return STATUS_RETRIEVED.equals(status);
    }
    public void setStatusRetrieved()
    {
    	setStatus(STATUS_RETRIEVED);
    }
    public boolean isFixedRequested()
    {
    	return STATUS_FIXED_REQUESTED.equals(status);
    }
    public boolean isWorkNeeded()
    {
    	return isNetYetRetrieved() || isRetrieved() || isFixedRequested();
    }
    
    public boolean isSkipped()
    {
    	return STATUS_SKIPPED.equals(status);
    }
    public void setStatusSkipped()
    {
    	setStatus(STATUS_SKIPPED);
    }
    
    public boolean isRejected()
    {
    	return STATUS_REJECTED.equals(status);
    }
    public void setStatusRejected()
    {
    	setStatus(STATUS_REJECTED);
    }
    public boolean isCompleted()
    {
    	return STATUS_COMPLETED.equals(status);
    }
    public void setStatusCompleted()
    {
    	setStatus(STATUS_COMPLETED);
    }
    public boolean isEndState()
    {
    	return isRejected() || isCompleted() || isViewOptional() || isViewOptionalViewed();
    }
    
    public String getStatusLabel()
    {
    	if ( isCompleted() )
    		return Application.getInstance().getServerMessages().getString("transaction.partyDocument.status.completed");
    	if ( isNetYetRetrieved() )
    		return Application.getInstance().getServerMessages().getString("transaction.partyDocument.status.netYetRetrieved");
    	if ( isViewOptional() )
    		return Application.getInstance().getServerMessages().getString("transaction.partyDocument.status.viewOptional");
    	if ( isViewOptionalViewed() )
    		return Application.getInstance().getServerMessages().getString("transaction.partyDocument.status.viewOptionalViewed");
    	if ( isRetrieved() )
    		return Application.getInstance().getServerMessages().getString("transaction.partyDocument.status.retrieved");
    	if ( isRejected() )
    		return Application.getInstance().getServerMessages().getString("transaction.partyDocument.status.rejected");
    	if ( isFixedRequested() )
    		return Application.getInstance().getServerMessages().getString("transaction.partyDocument.status.fixRequested");
    	if ( isSkipped() )
    		return Application.getInstance().getServerMessages().getString("transaction.partyDocument.status.skipped");
    	return "??"+status+"??";
    	
    }


    public final EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }
    public final void updateLastUpdatedTimestamp()
    {
    	lastUpdatedTimestamp = new EsfDateTime();
    	objectChanged();
    }

    public String getSnapshotDocumentSkipped()
    {
    	return snapshotDocumentSkipped;
    }
    public boolean isSnapshotDocumentSkipped()
    {
    	return Literals.Y.equals(snapshotDocumentSkipped);
    }
    public void setSnapshotDocumentSkipped(String v)
    {
    	snapshotDocumentSkipped =  Literals.Y.equalsIgnoreCase(v) ? Literals.Y : Literals.N;
    	objectChanged();
    }
    
    public String getSnapshotXml()
    {
    	return snapshotXml;
    }
    public boolean hasSnapshotXml()
    {
    	return EsfString.isNonBlank(snapshotXml);
    }
    public boolean hasSnapshotBlobId()
    {
    	return snapshotBlobId != null && ! snapshotBlobId.isNull();
    }
    public boolean hasSnapshotXmlOrBlobId()
    {
    	return hasSnapshotXml() || hasSnapshotBlobId();
    }
    public void createSnapshotXml(TransactionContext context, String documentHtml, String dataXml)
    	throws EsfException
    {
    	StringBuilder exceptionText = null;
    	if ( EsfString.areAnyNonBlank(documentHtml,dataXml) )
    	{
        	String signedDocumentXml = null;
        	String signedDataXml = null;
        	
        	String signFileName = EsfString.isBlank(documentFileName) ? "document.html" : documentFileName;
        	
        	EsfDateTime timestamp = new EsfDateTime();
        	
    		String namespaceTimestampAttributes = "xmlns=\"" + XmlUtil.getXmlNamespace2011() + "\" timestamp=\"" + timestamp.toXmlWithTz() + "\"";
        	
        	SignatureKey signatureKey = Application.getInstance().getSignatureKey();

        	if ( EsfString.isNonBlank(documentHtml) )
        	{
        		try
        		{
        			String originalHtml = "<snapshot type=\"document\" " + namespaceTimestampAttributes + "><![CDATA[" + documentHtml + "]]></snapshot>";
        	    	XmlDigitalSignature xmlDigitalSignature = new XmlDigitalSignature();
        	    	if ( context == null )
        	    		signedDocumentXml = xmlDigitalSignature.sign(originalHtml, signFileName, Application.CONTENT_TYPE_HTML, signatureKey);
        	    	else
        	    		signedDocumentXml = xmlDigitalSignature.sign(originalHtml, signFileName, Application.CONTENT_TYPE_HTML, signatureKey, context.hasIpHost() ? context.getIpHost() : null, context.hasUserAgent() ? context.getUserAgent() : null);
        		}
        		catch( EsfException e )
        		{
        	        _logger.warn("createSnapshotXml() - Failed to sign documentHtml; transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; documentNumber: " + documentNumber, e);
        	        if ( exceptionText == null )
        	        	exceptionText = new StringBuilder(200);
        	        exceptionText.append("XML Digital Signature failed on the document HTML. Exception: " + e.getMessage());
        		}
        	}
        	
        	if ( EsfString.isNonBlank(dataXml) )
        	{
        		try
        		{
        			if ( signFileName.toLowerCase().endsWith(".html") )
        				signFileName = signFileName.substring(0,signFileName.length()-5) + ".xml";
        			else if ( signFileName.toLowerCase().endsWith(".htm") )
        				signFileName = signFileName.substring(0,signFileName.length()-4) + ".xml";
        			
        			String originalXml = "<snapshot type=\"data\" " + namespaceTimestampAttributes + ">" + dataXml + "</snapshot>";
        	    	XmlDigitalSignature xmlDigitalSignature = new XmlDigitalSignature();
        	    	if ( context == null )
        	    		signedDataXml = xmlDigitalSignature.sign(originalXml,  signFileName, Application.CONTENT_TYPE_XML, signatureKey);
        	    	else
        	    		signedDataXml = xmlDigitalSignature.sign(originalXml, signFileName, Application.CONTENT_TYPE_XML, signatureKey, context.hasIpHost() ? context.getIpHost() : null, context.hasUserAgent() ? context.getUserAgent() : null);
        		}
        		catch( EsfException e )
        		{
        	        _logger.warn("createSnapshotXml() - Failed to sign dataXml; transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; documentNumber: " + documentNumber, e);
        	        if ( exceptionText == null )
        	        	exceptionText = new StringBuilder(200);
        	        else
        	        	exceptionText.append(' ');
        	        exceptionText.append("XML Digital Signature failed on the data XML. Exception: " + e.getMessage());
        		}
        	}
       	
        	if ( signedDocumentXml != null || signedDataXml != null )
        	{
        		int estimatedSize = 100;
        		if ( signedDocumentXml != null )
        			estimatedSize += signedDocumentXml.length();
        		if ( signedDataXml != null )
        			estimatedSize += signedDataXml.length();
        		StringBuilder snapshotsXmlBuf = new StringBuilder(estimatedSize);
        		snapshotsXmlBuf.append("<snapshots ").append(namespaceTimestampAttributes).append(">");
        		if ( signedDocumentXml != null )
        			snapshotsXmlBuf.append('\n').append(signedDocumentXml);
        		if ( signedDataXml != null )
        			snapshotsXmlBuf.append('\n').append(signedDataXml);
        		snapshotsXmlBuf.append("\n</snapshots>");
        		snapshotXml = snapshotsXmlBuf.toString();
        		snapshotTimestamp = timestamp;
        		snapshotXmlChanged = true;
        		objectChanged();
        	}
    	}
    	else
    	{
    		exceptionText = new StringBuilder("No document HTML or data XML was provided to be digitally signed.");
    	}
    	if ( exceptionText != null )
    		throw new EsfException(exceptionText.toString());
    }
    
    public EsfDateTime getSnapshotTimestamp()
    {
    	return snapshotTimestamp;
    }

    public String getSnapshotData()
    {
    	return getSnapshotContents("data");
    }
    
    public String getSnapshotDocument()
    {
    	return isSnapshotDocumentSkipped() ? "" : getSnapshotContents("document");
    }
    
    protected String getSnapshotContents(String targetSnapshotType)
    {
    	if ( ! hasSnapshotXml() || EsfString.isBlank(targetSnapshotType) )
    		return "";
    	
		StringReader sr = null;

		SAXBuilder saxBuilder = new SAXBuilder(XMLReaders.NONVALIDATING);
		saxBuilder.setIgnoringElementContentWhitespace(true);
		
		Namespace ns = Namespace.getNamespace(XmlUtil.getXmlNamespace2011());

		try {
			
			sr = new StringReader(getSnapshotXml());

			Document doc = saxBuilder.build(sr);
			Element rootElement = doc.getRootElement();
			
			String rootName = rootElement.getName();
			if ( ! "snapshots".equals(rootName) ) 
			{
				_logger.error("getSnapshotContents(" + targetSnapshotType + ") - root is not 'snapshots' for transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; in XML string: " + getSnapshotXml());
			}
			else
			{
				List<Element> snapshotElements = rootElement.getChildren("snapshot", ns);
				if ( snapshotElements == null || snapshotElements.size() < 1 )
				{
					_logger.error("getSnapshotContents(" + targetSnapshotType + ") - No 'snapshot' elements for transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; in XML string: " + getSnapshotXml());
				}
				else
				{
					ListIterator<Element> snapshotIter = snapshotElements.listIterator();
					while( snapshotIter.hasNext() )
					{
						Element snapshot = snapshotIter.next();
						String snapshotType = snapshot.getAttributeValue("type");
						if ( targetSnapshotType.equals(snapshotType) )
						{
							if ( "data".equals(targetSnapshotType ) ) {
								XMLOutputter outputter = new XMLOutputter();
								/* This code seems to work, which is good, but we don't really want to reverify the digital signature on every load
								*/
								try {
									XmlDigitalSignature dsig = new XmlDigitalSignature();
									String snapshotString = outputter.outputString(snapshot);
									dsig.verifyAcceptEmbeddedKeysIfKeyIdNotFound(snapshotString);
								} catch(EsfException e) { 
									_logger.error("getSnapshotContents(" + targetSnapshotType + ") - BAD signature verify on transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId,e);
								}
								
								Element esfRecord = snapshot.getChild("esfRecord",Namespace.getNamespace(XmlUtil.getXmlNamespace2009()));
								return outputter.outputString(esfRecord);
							} else {
								try {
									XMLOutputter outputter = new XMLOutputter();
									XmlDigitalSignature dsig = new XmlDigitalSignature();
									String snapshotString = outputter.outputString(snapshot);
									dsig.verifyAcceptEmbeddedKeysIfKeyIdNotFound(snapshotString);
								} catch(EsfException e) { 
									_logger.error("getSnapshotContents(" + targetSnapshotType + ") - BAD signature verify on transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId,e);
								}
								return snapshot.getText();
							}
						}
					}
					_logger.warn("getSnapshotContents() - No 'snapshot' elements of type: '" + targetSnapshotType + 
							"'; for transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; in XML string: " + getSnapshotXml());
					
					return "";
				}
			}
		} catch (java.io.IOException e) {
			_logger.error("getSnapshotContents() - could not read data from the XML string: " + getSnapshotXml(),e);
		} catch (JDOMException e) {
			_logger.error("getSnapshotContents() - could not XML parse data from the XML string: " + getSnapshotXml(),e);
		} finally {
			if (sr != null)
				try {
					sr.close();
				} catch (Exception e) {
					//ignore close exception
				}
		}
    	
		return "";
	}
    
    public String getDigitallySignedSnapshotData()
    {
    	return getDigitallySignedSnapshot("data");
    }
    
    public String getDigitallySignedSnapshotDocument()
    {
    	return isSnapshotDocumentSkipped() ? "" : getDigitallySignedSnapshot("document");
    }
    
    protected String getDigitallySignedSnapshot(String targetSnapshotType)
    {
    	if ( ! hasSnapshotXml() || EsfString.isBlank(targetSnapshotType) )
    		return "";
    	
		StringReader sr = null;

		SAXBuilder saxBuilder = new SAXBuilder(XMLReaders.NONVALIDATING);
		saxBuilder.setIgnoringElementContentWhitespace(true);
		
		Namespace ns = Namespace.getNamespace(XmlUtil.getXmlNamespace2011());

		try {
			
			sr = new StringReader(getSnapshotXml());

			Document doc = saxBuilder.build(sr);
			Element rootElement = doc.getRootElement();
			
			String rootName = rootElement.getName();
			if ( ! "snapshots".equals(rootName) ) 
			{
				_logger.error("getDigitallySignedSnapshot(" + targetSnapshotType + ") - root is not 'snapshots' for transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; in XML string: " + getSnapshotXml());
			}
			else
			{
				List<Element> snapshotElements = rootElement.getChildren("snapshot", ns);
				if ( snapshotElements == null || snapshotElements.size() < 1 )
				{
					_logger.error("getDigitallySignedSnapshot(" + targetSnapshotType + ") - No 'snapshot' elements for transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; in XML string: " + getSnapshotXml());
				}
				else
				{
					ListIterator<Element> snapshotIter = snapshotElements.listIterator();
					while( snapshotIter.hasNext() )
					{
						Element snapshot = snapshotIter.next();
						String snapshotType = snapshot.getAttributeValue("type");
						if ( targetSnapshotType.equals(snapshotType) )
						{
							if ( "data".equals(targetSnapshotType ) ) {
								XMLOutputter outputter = new XMLOutputter();
								/* This code seems to work, which is good, but we don't really want to reverify the digital signature on every load
								*/
								try {
									XmlDigitalSignature dsig = new XmlDigitalSignature();
									String snapshotString = outputter.outputString(snapshot);
									dsig.verifyAcceptEmbeddedKeysIfKeyIdNotFound(snapshotString);
									return snapshotString;
								} catch(EsfException e) { 
									_logger.error("getDigitallySignedSnapshot(" + targetSnapshotType + ") - BAD signature verify on transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId,e);
									return "";
								}
							} else {
								XMLOutputter outputter = new XMLOutputter();
								try {
									XmlDigitalSignature dsig = new XmlDigitalSignature();
									String snapshotString = outputter.outputString(snapshot);
									dsig.verifyAcceptEmbeddedKeysIfKeyIdNotFound(snapshotString);
									return snapshotString;
								} catch(EsfException e) { 
									_logger.error("snapshot(" + targetSnapshotType + ") - BAD signature verify on transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId,e);
									return "";
								}
							}
						}
					}
					_logger.warn("getDigitallySignedSnapshot() - No 'snapshot' elements of type: '" + targetSnapshotType + 
							"'; for transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; in XML string: " + getSnapshotXml());
					
					return "";
				}
			}
		} catch (java.io.IOException e) {
			_logger.error("getDigitallySignedSnapshot() - could not read data from the XML string: " + getSnapshotXml(),e);
		} catch (JDOMException e) {
			_logger.error("getDigitallySignedSnapshot() - could not XML parse data from the XML string: " + getSnapshotXml(),e);
		} finally {
			if (sr != null)
				try {
					sr.close();
				} catch (Exception e) {
					//ignore close exception
				}
		}
    	
		return "";
	}
    
    public String getDocumentFileName()
    {
    	return documentFileName;
    }
    public boolean hasDocumentFileName()
    {
    	return EsfString.isNonBlank(documentFileName);
    }
    public void setDocumentFileName(String v)
    {
    	if ( v == null )
    		documentFileName = v;
    	else if ( v.length() > Literals.FILE_NAME_MAX_LENGTH )
    		documentFileName = v.substring(0,Literals.FILE_NAME_MAX_LENGTH).trim();
        else
        	documentFileName = v.trim();
        objectChanged();
    }
    
    public String getEsignIpHostAddr()
    {
    	return esignIpHostAddr;
    }
    public void setEsignIpHostAddr(String v)
    {
    	if ( v == null )
    		esignIpHostAddr = v;
    	else if ( v.length() > Literals.IP_HOST_ADDRESS_LENGTH )
    		esignIpHostAddr = v.substring(0,Literals.IP_HOST_ADDRESS_LENGTH).trim();
        else
        	esignIpHostAddr = v.trim();
        objectChanged();
    }
    
    public EsfDateTime getEsignTimestamp()
    {
    	return esignTimestamp;
    }
    public void setEsignTimestamp(EsfDateTime v)
    {
    	esignTimestamp = v;
    	objectChanged();
    }
    public boolean isEsigned()
    {
    	return esignTimestamp != null;
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof TransactionPartyDocument )
        {
        	TransactionPartyDocument other = (TransactionPartyDocument)o;
            return getTransactionPartyId().equals(other.getTransactionPartyId())  &&  getTransactionDocumentId().equals(other.getTransactionDocumentId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getTransactionPartyId().hashCode() + getTransactionDocumentId().hashCode();
    }
    
    @Override
    public int compareTo(TransactionPartyDocument o)
    {
    	int diff = getTransactionPartyId().compareTo(o.getTransactionPartyId());
    	if ( diff != 0 )
    		return diff;
    	return getDocumentNumber() - o.getDocumentNumber();
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con)
        throws SQLException
    {
    	_logger.debug("save(con,user) on transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; status: " + status + "; insert: " + doInsert());
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( hasSnapshotBlobId() )
        	{
            	if ( snapshotXmlChanged )
            	{
    				byte[] xmlBlob = EsfString.stringToBytes(snapshotXml);
    				getBlobDb().update(con, snapshotBlobId, xmlBlob, CompressOption.ENABLE, EncryptOption.ENABLE);
                    snapshotXmlChanged = false;
            	}        		
        	}
        	else
        	{
            	if ( snapshotXmlChanged )
            	{
            		snapshotBlobId = new EsfUUID();
    				byte[] xmlBlob = EsfString.stringToBytes(snapshotXml);
    				getBlobDb().insert(con, snapshotBlobId, xmlBlob, CompressOption.ENABLE, EncryptOption.ENABLE);
                    snapshotXmlChanged = false;
            	}
        	}
        	
        	if ( doInsert() )
            {                      	
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_transaction_party_document " +
                	"(transaction_party_id,transaction_document_id,document_number,status,last_updated_timestamp,snapshot_document_skipped,snapshot_blob_id,snapshot_timestamp,document_file_name,esign_ip_host_addr,esign_timestamp) " +
                	"VALUES (?,?,?,?,?,?,?,?,?,?,?)");
                stmt.set(transactionPartyId);
                stmt.set(transactionDocumentId);
                stmt.set(documentNumber);
                stmt.set(status);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(snapshotDocumentSkipped);
                stmt.set(snapshotBlobId);
                stmt.set(snapshotTimestamp);
                stmt.set(documentFileName);
                stmt.set(esignIpHostAddr);
                stmt.set(esignTimestamp);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; status: " + status);
                    return false;
                }

                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                return true;
            }
            
        	if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_transaction_party_document SET status=?,last_updated_timestamp=?,snapshot_document_skipped=?,snapshot_blob_id=?,snapshot_timestamp=?,document_file_name=?,esign_ip_host_addr=?,esign_timestamp=? " +
                		"WHERE transaction_party_id=? AND transaction_document_id=?"
                						   );
                stmt.set(status);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(snapshotDocumentSkipped);
                stmt.set(snapshotBlobId);
                stmt.set(snapshotTimestamp);
                stmt.set(documentFileName);
                stmt.set(esignIpHostAddr);
                stmt.set(esignTimestamp);
                stmt.set(transactionPartyId);
                stmt.set(transactionDocumentId);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; status: " + status); 
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; status: " + status + "; insert: " + doInsert());
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }

    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    
    
    synchronized boolean delete(final Connection con, final Errors errors)
        throws SQLException
    {
    	_logger.debug("delete(con) on transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; status: " + status);
        
        clearLastSQLException();
    
        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of transaction party document that was pending an INSERT transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; status: " + status);
            objectDeleted();
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_transaction_party_document WHERE transaction_party_id_code=? AND transaction_document_id=?");
            stmt.set(transactionPartyId);
            stmt.set(transactionDocumentId);
            stmt.executeUpdate();
            
            if ( snapshotBlobId != null && ! snapshotBlobId.isNull() )
            	getBlobDb().delete(con, snapshotBlobId);
            
            objectDeleted();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on transactionPartyId: " + transactionPartyId + "; transactionDocumentId: " + transactionDocumentId + "; status: " + status);
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete()
    {
        Errors nullErrors = null;
        return delete(nullErrors);
    }
        
   	public static class Manager
   	{
   		static void DEBUG_VERIFY(String snapshotsXml)
   		{
   			_logger.debug("DEBUG_VERIFY() called -- should not be done once testing is done.");
   			StringReader sr = null;

   			SAXBuilder saxBuilder = new SAXBuilder(XMLReaders.NONVALIDATING);
   			saxBuilder.setIgnoringElementContentWhitespace(true);

   			Application app = Application.getInstance();
   			
   			try {
   				Namespace ns = Namespace.getNamespace(XmlUtil.getXmlNamespace2011());
   				
   				sr = new StringReader(snapshotsXml);

   				Document doc = saxBuilder.build(sr);
   				Element rootElement = doc.getRootElement();

   				String rootName = rootElement.getName();
   				if (!rootName.equals("snapshots")) {
   					app.err("DEBUG_VERIFY(): Root element is not snapshots.  Found instead: " + rootName + "; in XML: "
   							+ snapshotsXml);
   					return;
   				}
   				String timestamp = rootElement.getAttributeValue("timestamp");
   				EsfDateTime dateTime = EsfDateTime.CreateFromXmlDateTimeWithTz(timestamp);
   				app.debug("DEBUG_VERIFY() - snapshots timestamp: " + dateTime.toLongTimeString());
   				
   				List<Element> snapshotList = rootElement.getChildren("snapshot", ns);
   				if ( snapshotList == null )
   				{
   					app.err("DEBUG_VERIFY(): No snapshot elements");
   					return;
   				}
   				XmlDigitalSignature xmlDigitalSignature = new XmlDigitalSignature();
   				ListIterator<Element> iter = snapshotList.listIterator();
   				while (iter.hasNext()) {
   					Element snapshotElement = iter.next();
   					String type = snapshotElement.getAttributeValue("type");
   	   				timestamp = snapshotElement.getAttributeValue("timestamp");
   	   				dateTime = EsfDateTime.CreateFromXmlDateTimeWithTz(timestamp);
   	   				app.debug("DEBUG_VERIFY() - snapshot type: " + type + "; timestamp: " + dateTime.toLongTimeString());
   	   				XMLOutputter snapshotOutputter = new XMLOutputter();
   	   				StringWriter writer = new StringWriter(snapshotElement.getText().length()+100);
   	   				snapshotOutputter.output(snapshotElement, writer);
   	   				String signedSnapshotXml = writer.toString();
   	   				try
   	   				{
   	   					xmlDigitalSignature.verifyAcceptEmbeddedKeysIfKeyIdNotFound(signedSnapshotXml);
   	   	   				app.debug("DEBUG_VERIFY() - xml digital signature verification OKAY");
   	   				}
   	   				catch(EsfException e) {
   	   					app.except(e,"DEBUG_VERIFY() - xml digital signature verification FAILED");
   	   				}
   				}
   				
   			} catch (java.io.IOException e) {
   				app.except(e, "DEBUG_VERIFY() - could not read data from the XML string: " + snapshotsXml);
   			} catch (JDOMException e) {
   				app.except(e, "DEBUG_VERIFY() - could not XML parse data from the XML string: " + snapshotsXml);
   			} finally {
   				if (sr != null)
   					try {
   						sr.close();
   					} catch (Exception e) {
   					}
   			}
   		}
   		
   		private static String getSnapshotXml(EsfUUID blobId)
   	    {
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
				byte[] xmlBlob;
				xmlBlob = Application.getInstance().getBlobDb().select(con, blobId);
				if (xmlBlob == null) 
				{
					_logger.error("Manager.getSnapshotXml() blobId: " + blobId + "; found no XML blob data");
					con.rollback();
				}
				else
				{
					con.commit();
					return EsfString.bytesToString(xmlBlob);
				}
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   	    }

   		public static List<TransactionPartyDocument> getAllByTransactionPartyId(Connection con, EsfUUID transactionPartyId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<TransactionPartyDocument> list = new LinkedList<TransactionPartyDocument>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, 
   	        			"SELECT transaction_document_id, document_number, status, last_updated_timestamp, snapshot_document_skipped, snapshot_blob_id, snapshot_timestamp, document_file_name, esign_ip_host_addr, esign_timestamp " +
   	        			"FROM esf_transaction_party_document WHERE transaction_party_id=? ORDER BY document_number" );
   	        	stmt.set(transactionPartyId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID transactionDocumentId = rs.getEsfUUID();
	            	short docNumber = rs.getShort();
	            	String status = rs.getString();
	            	EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
	            	String snapshotDocumentSkipped = rs.getString();
	            	EsfUUID snapshotBlobId = rs.getEsfUUID();
	            	EsfDateTime snapshotTimestamp = rs.getEsfDateTime();
	            	String documentFileName = rs.getString();
	            	String esignIpHostAddr = rs.getString();
	            	EsfDateTime esignTimestamp = rs.getEsfDateTime();
	            	
	            	String snapshotXml = snapshotBlobId == null ? null : getSnapshotXml(snapshotBlobId);
	            	
	            	// TEST ONLY CODE to ensure we can revalidate the digitally signed data retrieved from the DB
	            	//if ( snapshotBlobId != null )
	            	//	DEBUG_VERIFY(snapshotXml);
	            	
	            	TransactionPartyDocument tranPartyDoc = new TransactionPartyDocument(transactionPartyId,transactionDocumentId,docNumber,status,lastUpdatedTimestamp,
	            			snapshotDocumentSkipped,snapshotBlobId,snapshotXml,snapshotTimestamp,documentFileName,esignIpHostAddr,esignTimestamp);
	            	tranPartyDoc.setLoadedFromDb();
	            	list.add(tranPartyDoc);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<TransactionPartyDocument> getAllByTransactionPartyId(EsfUUID transactionPartyId)
   	    {
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<TransactionPartyDocument> partyDocs = getAllByTransactionPartyId(con,transactionPartyId);
   	        	con.commit();
   	        	return partyDocs;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   	    }

   		
   		public static int deleteAllByTransactionPartyId(Connection con, EsfUUID transactionPartyId)
			throws SQLException
		{
	        EsfPreparedStatement stmt = null;
	        int numDeleted = 0;
	        
	        try
	        {
	        	stmt = new EsfPreparedStatement( con, "DELETE FROM esf_blob WHERE id IN (SELECT snapshot_blob_id FROM esf_transaction_party_document WHERE transaction_party_id=? AND snapshot_blob_id IS NOT NULL)" );
	        	stmt.set(transactionPartyId);
	        	stmt.executeUpdate();
	        	stmt.close();

	        	stmt = new EsfPreparedStatement( con, "DELETE FROM esf_transaction_party_document WHERE transaction_party_id=?" );
	        	stmt.set(transactionPartyId);
	        	numDeleted = stmt.executeUpdate();
	        }
	        finally
	        {
	            cleanupStatement(stmt);
	        }

	        return numDeleted; 
		}
		
   		public static TransactionPartyDocument createNew(TransactionParty tranParty, TransactionDocument tranDoc, short documentNumber)
   		{
   			TransactionPartyDocument tpd = new TransactionPartyDocument(tranParty.getId(), tranDoc.getId(), documentNumber);
   			return tpd;
   		}
   	    
   	} // Manager
   	
}