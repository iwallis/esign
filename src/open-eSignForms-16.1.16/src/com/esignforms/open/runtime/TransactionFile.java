// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;


/**
* TransactionFile holds the definition of a file that's been uploaded into a transaction.
* 
* @author Yozons, Inc.
*/
public class TransactionFile
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<TransactionFile>, java.io.Serializable
{
	private static final long serialVersionUID = 2096197194807920186L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TransactionFile.class);
    
    protected final EsfUUID id;
    protected final EsfUUID transactionId;
    protected final EsfUUID transactionDocumentId;
    protected final EsfUUID transactionPartyId;
    protected final EsfDateTime uploadTimestamp;
    protected EsfName fieldName;
    protected int fileSize;
    protected String fileName;
    protected String fileMimeType;
    protected EsfUUID fileDataBlobId;
    
    protected byte[] fileData; // only loaded when needed
   
    /**
     * This creates a TransactionFile object from data retrieved from the DB.
     */
    protected TransactionFile(EsfUUID id, EsfUUID transactionId, EsfUUID transactionDocumentId, EsfUUID transactionPartyId,
    		EsfDateTime uploadTimestamp, EsfName fieldName,
    		int fileSize, String fileName, String fileMimeType, EsfUUID fileDataBlobId)
    {
        this.id = id;
        this.transactionId = transactionId;
        this.transactionDocumentId = transactionDocumentId;
        this.transactionPartyId = transactionPartyId;
        this.uploadTimestamp = uploadTimestamp;
        this.fieldName = fieldName;
        this.fileSize = fileSize;
        this.fileName = fileName;
        this.fileMimeType = fileMimeType;
        this.fileDataBlobId = fileDataBlobId;
        this.fileData = null;
    }
    
    protected TransactionFile(EsfUUID transactionId, EsfUUID transactionDocumentId, EsfUUID transactionPartyId, 
    		EsfName fieldName, String fileName, String fileMimeType, byte[] fileData)
    {
        this.id = new EsfUUID();
        this.transactionId = transactionId;
        this.transactionDocumentId = transactionDocumentId;
        this.transactionPartyId = transactionPartyId;
        this.uploadTimestamp = new EsfDateTime();
        this.fieldName = fieldName;
        this.fileSize = fileData.length;
        this.fileName = fileName;
        this.fileMimeType = fileMimeType;
        this.fileDataBlobId = new EsfUUID();
        this.fileData = fileData;
    }
    
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getTransactionId()
    {
        return transactionId;
    }
    
    public final EsfUUID getTransactionDocumentId()
    {
        return transactionDocumentId;
    }
    
    public final EsfUUID getTransactionPartyId()
    {
        return transactionPartyId;
    }
    
	public EsfDateTime getUploadTimestamp()
	{
		return uploadTimestamp;
	}

	public EsfName getFieldName()
    {
    	return fieldName;
    }
    
	public int getFileSize()
	{
		return fileSize;
	}
    
    public String getFileName()
    {
    	return fileName;
    }

    public String getFileMimeType()
    {
    	return fileMimeType;
    }
    public boolean isContentTypeImage()
    {
        return Application.getInstance().isContentTypeImage(fileMimeType);
    }
    public boolean isContentTypeBrowserSafe()
    {
        return Application.getInstance().isContentTypeBrowserSafe(fileMimeType);
    }
    public boolean isContentTypePDF()
    {
    	return Application.CONTENT_TYPE_PDF.equals(fileMimeType);
    }
    
    public EsfUUID getFileDataBlobId()
    {
        return fileDataBlobId;
    }

    public byte[] getFileData()
    {
        return fileData;
    }
    public boolean isFileDataLoaded()
    {
    	return fileData != null;
    }
    
    public byte[] getFileDataFromDatabase(Connection con)
    	throws SQLException
    {
    	return Application.getInstance().getBlobDb().select(con, fileDataBlobId);
    }
	public byte[] getFileDataFromDatabase()
	{
		if ( isFileDataLoaded() )
			return fileData;
		
	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        byte[] data = getFileDataFromDatabase(con);
	        con.commit();
	        return data;
	    }
	    catch(SQLException e) 
	    {
	        pool.rollbackIgnoreException(con,e);
	        return null;
	    }
	    finally
	    {
	    	Application.getInstance().cleanupPool(pool,con,null);
	    }
	}
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof TransactionFile )
        {
        	TransactionFile other = (TransactionFile)o;
            return getId().equals(other.getId());
        }
        return false;
    }
    
    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(TransactionFile o)
    {
    	return getId().compareTo(o.getId());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con)
        throws SQLException
    {
    	_logger.debug("save(con) on id: " + id + "; transactionId: " + transactionId + "; transactionDocumentId: " + transactionDocumentId + "; transactionPartyId: " + transactionPartyId + "; fieldName: " + fieldName + "; fileName: " + fileName + "; insert: " + doInsert());
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
            if ( doInsert() )
            {
				getBlobDb().insert(con, fileDataBlobId, fileData, BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE);
				
				stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_transaction_file (id,transaction_id,transaction_document_id,transaction_party_id,upload_timestamp,field_name,file_name,file_mime_type,file_blob_id) VALUES (?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(transactionId);
                stmt.set(transactionDocumentId);
                stmt.set(transactionPartyId);
                stmt.set(uploadTimestamp);
                stmt.set(fieldName);
                stmt.set(fileName);
                stmt.set(fileMimeType);
                stmt.set(fileDataBlobId);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; transactionId: " + transactionId + "; transactionDocumentId: " + transactionDocumentId + "; transactionPartyId: " + transactionPartyId + "; fieldName: " + fieldName + "; fileName: " + fileName);
                    return false;
                }
                
                // Let's clear out our byte buffer so as not to consume memory holding the file any longer
                fileData = null;
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                
                return true;
            }
            
            // There should be no way to update a file -- just insert and delete
            
            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; transactionId: " + transactionId + "; transactionDocumentId: " + transactionDocumentId + "; transactionPartyId: " + transactionPartyId + "; fieldName: " + fieldName + "; fileName: " + fileName);
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }

    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    
    
    public synchronized boolean delete(final Connection con)
	    throws SQLException
	{
		_logger.debug("delete(con) on id: " + id + "; transactionId: " + transactionId + "; transactionDocumentId: " + transactionDocumentId + "; transactionPartyId: " + transactionPartyId + "; fieldName: " + fieldName + "; fileName: " + fileName);
	    
	    clearLastSQLException();
	
	    if ( doInsert() )
	    {
	    	_logger.warn("delete(con) - Ignored delete of file that was pending an INSERT id: " + id + "; transactionId: " + transactionId + "; transactionDocumentId: " + transactionDocumentId + "; transactionPartyId: " + transactionPartyId + "; fieldName: " + fieldName + "; fileName: " + fileName);
	        objectDeleted();
	        return true;
	    }
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        // Delete the file data itself
	    	getBlobDb().delete(con,fileDataBlobId);
	        
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_transaction_file WHERE id=?");
	        stmt.set(id);
	        stmt.executeUpdate();
	        
	        objectDeleted();
	
	        return true;
	    }
	    catch(SQLException e)
	    {
	    	_logger.sqlerr(e,"delete(con) on id: " + id + "; transactionId: " + transactionId + "; transactionDocumentId: " + transactionDocumentId + "; transactionPartyId: " + transactionPartyId + "; fieldName: " + fieldName + "; fileName: " + fileName);
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}

	public boolean delete()
	{
	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        boolean result = delete(con);
	        con.commit();
	        return result;
	    }
	    catch(SQLException e) 
	    {
	        pool.rollbackIgnoreException(con,e);
	        return false;
	    }
	    finally
	    {
	    	Application.getInstance().cleanupPool(pool,con,null);
	    }
	}

   	public static class Manager
   	{
   		public static List<TransactionFile> getAllForTransaction(Connection con, EsfUUID transactionId) throws SQLException
   		{
   			EsfPreparedStatement stmt = null;

   			LinkedList<TransactionFile> list = new LinkedList<TransactionFile>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT esf_transaction_file.id,transaction_document_id,transaction_party_id,upload_timestamp,field_name,file_name,file_mime_type,file_blob_id,esf_blob.orig_size " +
   	        			"FROM esf_transaction_file,esf_blob WHERE transaction_id = ? AND file_blob_id=esf_blob.id"
   	        									);
   	        	stmt.set(transactionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	EsfUUID transactionDocumentId = rs.getEsfUUID();
	            	EsfUUID transactionPartyId = rs.getEsfUUID();
		            EsfDateTime uploadTimestamp = rs.getEsfDateTime();
		            EsfName fieldName = rs.getEsfName();
		            String fileName = rs.getString();
		            String fileMimeType = rs.getString();
		            EsfUUID fileDataBlobId = rs.getEsfUUID();
		            int fileSize = rs.getInt();
		            
		            // We don't load the file itself unless requested specifically (for a download)
		            
		            TransactionFile tranFile = new TransactionFile(id,transactionId,transactionDocumentId,transactionPartyId,uploadTimestamp,fieldName,fileSize,fileName,fileMimeType,fileDataBlobId);
		            tranFile.setLoadedFromDb();

		            list.add(tranFile);
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAllForTransaction() - transactionId: " + transactionId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list;
  		}
   		
   		public static List<TransactionFile> getAllForTransaction(EsfUUID transactionId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<TransactionFile> list = getAllForTransaction(con,transactionId);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   		public static TransactionFile createNew(EsfUUID transactionId, EsfUUID transactionDocumentId, EsfUUID transactionPartyId, 
   												EsfName fieldName, String fileName, String fileMimeType, byte[] fileData)
   		{
   			return new TransactionFile(transactionId, transactionDocumentId, transactionPartyId, fieldName, fileName, fileMimeType, fileData);
   		}
   	    
   	} // Manager
   	
}