// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.event;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyAssignment;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* PartyTransferredEvent represents the transfer of a party to a new email address (or if no email is set, just leaves it unspecified).
* 
* @author Yozons, Inc.
*/
public class PartyTransferredEvent extends Event
{
	private static final long serialVersionUID = 997197395138819194L;

	EsfName tranPartyName;
	String toEmail;
	
    /**
     * This creates an PartyTransferredEvent object
     */
    public PartyTransferredEvent(EsfName partyName, String toEmail)
    {
    	super();
    	tranPartyName = partyName;
    	this.toEmail = toEmail;
    }

    @Override
    public boolean doBuiltInAction(Connection con, TransactionContext context, Errors errors)
    	throws SQLException, EsfException
    {
    	TransactionParty tranParty = context.transaction.getTransactionParty(tranPartyName);
    	if ( tranParty == null )
    	{
			context.transaction.logActionDetail(con, "ERROR: Transferring party " + tranPartyName + " could not be found.");
			return false;
    	}
    	if ( context.isEsfReportsAccess() )
    	{
			context.transaction.logActionDetail(con, "ERROR: Transferring built-in party '" + tranPartyName + "' is not allowed.");
			return false;
    	}

        TransactionPartyAssignment tranPartyAssignment = tranParty.getCurrentAssignment();
        String origEmail = tranPartyAssignment.hasEmailAddress() ? tranPartyAssignment.getEmailAddress() : "(no original email address)";
        String toEmailLabel = EsfString.isBlank(toEmail) ? "(new email address unspecified)" : toEmail;
		String userName = context.hasUser() ? context.user.getFullDisplayName() : "(not logged in)";
		if ( tranParty.transferParty(con, toEmail) )
		{
			context.transaction.logGeneral(con,"User " + userName + " transferred transaction party: " + tranPartyName + "; from email: " + origEmail + "; to email: " + toEmailLabel);
			if ( context.hasUser() )
				context.user.logConfigChange(con,"Transferred transaction id: " + context.transaction.getId() + "; transaction party: " + tranPartyName + "; from email: " + origEmail + "; to email: " + toEmailLabel);
		}
		else
		{
			context.transaction.logGeneral(con,"WARNING: User " + userName + " attempted to transfer transaction party: " + tranPartyName + "; from email: " + origEmail + "; to email: " + toEmailLabel + "; but the request was not allowed.");
		}
		return true;
    }
    
    
    // Used so we can determine the package party that goes with this tranPartyName activation
    public EsfUUID getPackageVersionPartyTemplateId(TransactionContext context)
    {
    	TransactionParty tranParty = context.transaction.getTransactionParty(tranPartyName);
    	if ( tranParty == null )
    		return null;
    	return tranParty.getPackageVersionPartyTemplateId();
    }
}