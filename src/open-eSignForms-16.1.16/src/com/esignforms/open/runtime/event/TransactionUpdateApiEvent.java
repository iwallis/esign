// Copyright (C) 2015-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.event;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* TransactionUpdateApiEvent represents a transaction being updated via the API.
* 
* @author Yozons, Inc.
*/
public class TransactionUpdateApiEvent extends Event
{
	private static final long serialVersionUID = 7001648824582995106L;

	String eventName;
	
	/**
     * This creates a TransactionUpdateApiEvent object
     */
    public TransactionUpdateApiEvent(String eventName)
    {
    	super();
    	this.eventName = eventName;
    }
    

    @Override
    public boolean doBuiltInAction(Connection con, TransactionContext context, Errors errors)
    	throws SQLException, EsfException
    {
    	String prodOrTest = context.isProductionTransaction() ? "Production" : "Test";
    	String currStatus = context.transaction.getStatusLabel();
    	String prevStatusText = context.transaction.getStatusText();
    	
    	context.transaction.setStatusText(Application.getInstance().getServerMessages().getString("transaction.status.updateApi",prevStatusText));

    	if ( context.transaction.isCanceled() )
    	{
    		context.setUpdateApiWhenTransactionCanceled();
        	context.transaction.logGeneral(con, "WARNING: Canceled " + prodOrTest + " transaction updated by API user: " + context.user.getFullDisplayName() + "; event name: " + context.getUpdateApiEventName() + "; curr status: " + currStatus + "; prev status text: " + prevStatusText);
    	}
    	else if ( context.transaction.isSuspended() )
    	{
    		context.setUpdateApiWhenTransactionSuspended();
        	context.transaction.logGeneral(con, "WARNING: Suspended " + prodOrTest + " transaction updated by API user: " + context.user.getFullDisplayName() + "; event name: " + context.getUpdateApiEventName() + "; curr status: " + currStatus + "; prev status text: " + prevStatusText);
    	}
    	else
    	{
        	context.transaction.logGeneral(con, prodOrTest + " transaction updated by API user: " + context.user.getFullDisplayName() + "; event name: " + context.getUpdateApiEventName() + "; curr status: " + currStatus + "; prev status text: " + prevStatusText);    		
    	}
    	context.user.logConfigChange(con, prodOrTest + " transaction updated by API; event name: " + context.getUpdateApiEventName() + "; curr status: " + currStatus + "; prev status text: " + prevStatusText);
    	
    	// This event is special in that we allow rules to fire for the Transaction Updated API Event that match any of the currently active parties.
    	for ( TransactionParty tranParty : context.transaction.getAllTransactionParties() )
    	{
    		if ( tranParty.isActive() )
    			addAdditionalMatchingPackageVersionPartyTemplateId( tranParty.getPackageVersionPartyTemplateId() );
    	}
    	
    	return true;
    }
}