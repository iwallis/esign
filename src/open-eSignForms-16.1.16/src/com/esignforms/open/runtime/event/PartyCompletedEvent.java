// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.event;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyAssignment;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* PartyCompletedEvent represents a transaction party completing all of his documents.
* 
* @author Yozons, Inc.
*/
public class PartyCompletedEvent extends Event
{
	private static final long serialVersionUID = -8917377095249162615L;

	EsfName tranPartyName;

	TransactionParty tranParty;
	
    /**
     * This creates an PartyCompletedEvent object
     */
    public PartyCompletedEvent(EsfName partyName, TransactionParty tranParty)
    {
    	super();
    	tranPartyName = partyName;
    	this.tranParty = tranParty;
    }

    @Override
    public boolean doBuiltInAction(Connection con, TransactionContext context, Errors errors)
    	throws SQLException, EsfException
    {
    	if ( ! tranParty.isReports() )
    	{
        	tranParty.setStatusCompleted();
        	context.transaction.setStatusText(tranPartyName.toString() + " completed");
    	}
    	
    	TransactionPartyAssignment tranPartyAssignment = tranParty.getCurrentAssignment();
    	if ( tranPartyAssignment == null )
    	{
			context.transaction.logActionDetail(con, "WARNING: Completing party '" + tranPartyName + "' has no current assignment for pickup code: " + tranParty.getTransactionPartyAssignmentPickupCode());
			return false;
    	}

    	if ( ! tranParty.isReports() )
    		tranPartyAssignment.setStatusCompleted();
    	// If when the party completes we don't have an email address for the party, but we do have a package party email spec, 
    	// let's see if we can expand it now that we're done. This occurs for a party where the package party spec is not valid until docs are filled out.
    	if ( ! tranPartyAssignment.hasEmailAddress() )
    	{
    		PackageVersionPartyTemplate packageVersionPartyTemplate = tranParty.getPackageVersionPartyTemplate();
        	if ( packageVersionPartyTemplate != null && packageVersionPartyTemplate.hasEmailFieldExpression() )
        	{
            	String emailFieldExpression = packageVersionPartyTemplate.getEmailFieldExpression();
        		String email = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, emailFieldExpression).trim();
        		EsfEmailAddress emailAddress = new EsfEmailAddress(email); // Handles full email address spec, though for a party, we only track by the actual routing email address portion
        		if ( emailAddress.isValid() )
                	tranPartyAssignment.setEmailAddress(emailAddress.getEmailAddress());
        		else
        			context.transaction.logActionDetail(con, "ERROR: Completed party '" + tranPartyName + "' had no email specified and it still has an invalid email specification: '" + emailFieldExpression + "'; expanded to: " + email); 
        	}
    	}
    	
        context.transaction.logGeneral(con, "Party '" + tranPartyName + "' has completed all documents. IP address: " + context.getIpHost() + "; browser: " + context.getUserAgent());
        return true;
    }
}