// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.event;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.prog.PackageVersionPartyTemplateDocumentParty;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyAssignment;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* PartyCompletedDocumentEvent represents a transaction party completing the given document.
* 
* @author Yozons, Inc.
*/
public class PartyCompletedDocumentEvent extends Event
{
	private static final long serialVersionUID = -4419843778644111923L;

	EsfName tranPartyName;
	EsfName tranDocumentName;
	TransactionDocument tranDocument;
	TransactionPartyDocument tranPartyDocument;
	String capturedHtml;
	
    /**
     * This creates an PartyCompletedDocumentEvent object
     */
    public PartyCompletedDocumentEvent(EsfName partyName, EsfName documentName, TransactionDocument tranDocument, TransactionPartyDocument tranPartyDocument, String capturedHtml)
    {
    	super();
    	tranPartyName = partyName;
    	tranDocumentName = documentName;
    	this.tranDocument = tranDocument;
    	this.tranPartyDocument = tranPartyDocument;
    	this.capturedHtml = capturedHtml;
    }

    @Override
    public boolean doBuiltInAction(Connection con, TransactionContext context, Errors errors)
    	throws SQLException, EsfException
    {
    	if ( ! context.isEsfReportsAccess() && ! PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS.equals(tranPartyName) )
    		context.transaction.setStatusText(tranPartyName.toString() + " (" + tranDocumentName + ")");
    	boolean snapshotDocument = false;
    	boolean snapshotData = false;
    	
    	List<PackageVersionPartyTemplateDocumentParty> mappings = context.pkgVersionPartyTemplate.getPackageVersionPartyTemplateDocumentPartiesByDocumentId(tranDocument.getDocumentId());
    	for( PackageVersionPartyTemplateDocumentParty mapping : mappings )
    	{
    		if ( mapping.isSnapshotDocumentStatusEnabled() )
    			snapshotDocument = true;
    		if ( mapping.isSnapshotDataStatusEnabled() )
    			snapshotData = true;
    	}
    	
    	// If a snapshot is requested, let's do it
    	if ( snapshotDocument || snapshotData )
    	{
    		String dataXml = null;
    		if ( snapshotData )
    		{
    			List<FieldTemplate> maskFields = context.getMaskInDataSnapshotFieldTemplates();
    			if ( maskFields == null )
    				dataXml = tranDocument.getRecord().toXml();
    			else
    			{
    				LinkedList<EsfName> maskNames = new LinkedList<EsfName>();
    				for( FieldTemplate ft : maskFields )
    					maskNames.add(ft.getEsfName());
    				dataXml = tranDocument.getRecord().toXml(false,maskNames);
    			}
    		}

    		try
    		{
				String fileName = null;
				if ( context.currDocument.hasFileNameSpec() )
				{
					fileName = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, context.currDocument.getFileNameSpec()).trim();
					if ( EsfString.isBlank(fileName) )
						fileName = null;
					else 
					{
						if ( ! fileName.toLowerCase().endsWith(".html") && ! fileName.toLowerCase().endsWith(".htm") )
							fileName += ".html";
						
						fileName = Application.getInstance().sanitizeFileName(fileName);
					}
				}
    			
    			tranPartyDocument.setDocumentFileName(fileName);
    			tranPartyDocument.createSnapshotXml( context, (snapshotDocument ? capturedHtml : null), dataXml );
    			
    	    	if ( snapshotDocument && EsfString.isBlank(capturedHtml) )
    	    	{
    	    		if ( context.isApiMode() )
    	    		{
    	    			context.transaction.logActionBasic(con, "API Party '" + tranPartyName + "' has no HTML snapshot for the document '" + tranDocumentName + "' because it was auto-processed.");
    	    			tranPartyDocument.setSnapshotDocumentSkipped(Literals.Y);
    	    		}
    	    		else
    	    			context.transaction.logActionBasic(con, "SERIOUS-ERROR: Party '" + tranPartyName + "' is missing the HTML snapshot for the document " + tranDocumentName);   		
    	    	}
    	    	if ( snapshotData && EsfString.isBlank(dataXml) )
    	    	{
    				context.transaction.logActionBasic(con, "SERIOUS-ERROR: Party '" + tranPartyName + "' is missing the XML data snapshot for the document " + tranDocumentName);   		
    	    	}
    			
    			context.transaction.logActionDetail(con, "Party '" + tranPartyName + "' snapshot(s) created for the document '" + tranDocumentName + "': " + (snapshotDocument && EsfString.isNonBlank(capturedHtml)) + "; for the data: " + (snapshotData && EsfString.isNonBlank(dataXml)) + "; fileName: " + (fileName==null?"(n/a)":fileName));    	        	
    		}
    		catch( EsfException e )
    		{
    			context.transaction.logActionBasic(con, "SERIOUS-ERROR: Party '" + tranPartyName + "' snapshot(s) failed to be created for the document " + tranDocumentName + ": " + (snapshotDocument && EsfString.isNonBlank(capturedHtml)) + "; for the data: " + (snapshotData && EsfString.isNonBlank(dataXml)));    	        	
    			context.transaction.logActionBasic(con, "SERIOUS-ERROR: Snapshot exception: " + e.getMessage());    	        	
    		}
    	}
    	
    	if ( ! context.isEsfReportsAccess() )
    		tranPartyDocument.setStatusCompleted();

    	String actionType = tranPartyDocument.isEsigned() ? "completed and e-signed" : "completed";
    	
    	if ( context.isApiMode() )
            context.transaction.logGeneral(con, "API Party '" + tranPartyName + "' has " + actionType + " the document '" + tranDocumentName + "'. IP address: " +
            		context.getIpHost() + "; browser: " + context.getUserAgent());
    	else
    		context.transaction.logGeneral(con, "Party '" + tranPartyName + "' has " + actionType + " the document '" + tranDocumentName + "'. IP address: " +
    				context.getIpHost() + "; browser: " + context.getUserAgent());
        
    	// If when the party completes a document we don't have an email address for the party, but we do have a package party email spec, 
    	// let's see if we can expand it now that we're done. This occurs for a party where the package party spec is not valid until docs are filled out.
        TransactionParty tranParty = context.transaction.getTransactionParty(tranPartyName);
        if ( tranParty != null )
        {
        	TransactionPartyAssignment tranPartyAssignment = tranParty.getCurrentAssignment();
        	if ( ! tranPartyAssignment.hasEmailAddress() )
        	{
        		PackageVersionPartyTemplate packageVersionPartyTemplate = tranParty.getPackageVersionPartyTemplate();
            	if ( packageVersionPartyTemplate != null && packageVersionPartyTemplate.hasEmailFieldExpression() )
            	{
                	String emailFieldExpression = packageVersionPartyTemplate.getEmailFieldExpression();
            		String email = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, emailFieldExpression).trim();
            		EsfEmailAddress emailAddress = new EsfEmailAddress(email); // Handles full email address spec, though for a party, we only track by the actual routing email address portion
            		if ( emailAddress.isValid() )
            			tranPartyAssignment.setEmailAddress(emailAddress.getEmailAddress());
            		else
            			context.transaction.logActionDetail(con, "ERROR: Party '" + tranPartyName + "' completed document '" + tranDocumentName + "' and had no email specified, and it still has an invalid email specification: '" + emailFieldExpression + "'; expanded to: " + email); 
            	}
        	}
        }
        return true;
    }
}