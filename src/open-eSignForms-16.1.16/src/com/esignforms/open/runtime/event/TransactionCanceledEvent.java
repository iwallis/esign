// Copyright (C) 2012-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.event;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* TransactionCanceledEvent represents a transaction being canceled.
* 
* @author Yozons, Inc.
*/
public class TransactionCanceledEvent extends Event
{
	private static final long serialVersionUID = -5609187401174707018L;

	String reason;
	
    /**
     * This creates a TransactionCompletedEvent object
     */
    public TransactionCanceledEvent(String reason)
    {
    	super();
    	this.reason = reason;
    }

    @Override
    public boolean doBuiltInAction(Connection con, TransactionContext context, Errors errors)
    	throws SQLException, EsfException
    {
    	String prodOrTest = context.isProductionTransaction() ? "Production" : "Test";
    	String prevStatus = context.transaction.getStatusLabel();

    	if ( context.transaction.isCanceled() )
    	{
    		context.transaction.logGeneral(con, prodOrTest + " transaction is already canceled. Ignoring request to cancel it.");
    		return false;
    	}
    	if ( context.transaction.isCompleted() )
    	{
    		context.transaction.logGeneral(con, prodOrTest + " transaction is already completed. Ignoring request to cancel it.");
    		return false;
    	}
    	
    	context.transaction.setStatusCanceled();
    	context.transaction.setStatusText(Application.getInstance().getServerMessages().getString("transaction.status.canceled"));
    	context.transaction.getRecord().addUpdate( Transaction.TRAN_RECORD_CANCELED_TIMESTAMP_ESFNAME, new EsfDateTime() );
    	if ( EsfString.isNonBlank(reason) ) 
    	{
        	context.transaction.getRecord().addUpdate( Transaction.TRAN_RECORD_CANCELED_REASON_ESFNAME, new EsfString(reason) );
    		context.transaction.logGeneral(con, prodOrTest + " transaction has been canceled; previous status: " + prevStatus + ". Reason: " + reason);
    	} 
    	else 
    	{
    		context.transaction.logGeneral(con, prodOrTest + " transaction has been canceled; previous status: " + prevStatus);
    	}
    		
    	if ( context.transaction.hasCancelRetentionSpec() )
    	{
    		EsfDateTime origExpireDateTime = context.transaction.getExpireTimestamp();
    		EsfDateTime expireDateTime = context.transaction.isCancelRetentionForever() ? null : context.transaction.getCancelRetentionDateTimeFromNow();
			context.transaction.setExpireTimestamp(expireDateTime);
    		if ( expireDateTime != null ) 
    		{
        		if ( origExpireDateTime == null || expireDateTime.isBefore(origExpireDateTime) )
        			context.transaction.logActionDetail(con, "Setting new EARLIER expiration date on canceled transaction to: " + 
        					expireDateTime.toLogString() + "; previously expiring: " + (origExpireDateTime == null ? "Forever" : origExpireDateTime.toLogString()));
        		else
        			context.transaction.logActionDetail(con, "Setting new LATER expiration date on canceled transaction to: " + 
        					expireDateTime.toLogString() + "; previously expiring: " + (origExpireDateTime == null ? "Forever" : origExpireDateTime.toLogString()));
    		}
    		else
    		{
    			context.transaction.logActionDetail(con, "Setting new KEEP FOREVER expiration date on canceled transaction; previously expiring: " + (origExpireDateTime == null ? "Forever" : origExpireDateTime.toLogString()));
    		}
    	}
    	
    	// This event is special in that we allow rules to fire for the Transaction Canceled Event that match any of the currently active parties.
    	for ( TransactionParty tranParty : context.transaction.getAllTransactionParties() )
    	{
    		if ( tranParty.isActive() )
    			addAdditionalMatchingPackageVersionPartyTemplateId( tranParty.getPackageVersionPartyTemplateId() );
    	}
    	
    	return true;
    }
}