// Copyright (C) 2011-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.event;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.DocumentProgramming;
import com.esignforms.open.prog.DocumentProgrammingRule;
import com.esignforms.open.prog.PackageProgramming;
import com.esignforms.open.prog.PackageProgrammingRule;
import com.esignforms.open.runtime.action.Action;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* Event is the base class of all events used by the transaction engine.
* 
* @author Yozons, Inc.
*/
public abstract class Event 
	implements java.io.Serializable
{
	private static final long serialVersionUID = 1414603577866121071L;

	LinkedList<Action> actionList = new LinkedList<Action>();
	
	// Special list of PackageVersionPartyTemplate IDs that can be used to match package programming rules
	LinkedList<EsfUUID> additionalMatchingPackageVersionPartyTemplateIds = null;
	
    /**
     * This creates an Event object
     */
    public Event()
    {
    }
    
    public List<Action> getActions(TransactionContext context)
    {
    	if ( context.hasTransaction() )
    	{
        	if ( context.currDocumentVersion != null && context.currDocumentVersion.hasDocumentProgramming() )
        	{
        		Map<EsfUUID,EsfUUID> partyIdMapping = null;
        		
        		DocumentProgramming prog = context.currDocumentVersion.getDocumentProgramming();
        		for( DocumentProgrammingRule rule : prog.getRules() )
        		{
        			if ( rule.isForEvent(getEsfName()) && 
        				 (context.currDocumentVersionPage == null || rule.isForPage(context.currDocumentVersionPage.getId()) ) &&
        				 rule.isForParty(context.currDocumentVersionPartyList)
        			   )
        			{
        				for( Action action : rule.getActions() )
        				{
        					if ( partyIdMapping == null )
        						partyIdMapping = context.getDocumentPartyIdToPackagePartyIdMapping();

        					Action myAction = action.duplicate();
        					if ( partyIdMapping != null )
        						myAction.updatePackagePartyIds(partyIdMapping);
            				actionList.add(myAction);
        				}
        			}
        		}
        	}

        	if ( context.pkgVersion != null && context.pkgVersion.hasPackageProgramming() )
        	{
        		PackageProgramming prog = context.pkgVersion.getPackageProgramming();
        		for( PackageProgrammingRule rule : prog.getRules() )
        		{
        			EsfName eventName = getEsfName();
        			if ( rule.isForEvent(eventName) )
        			{
        				// These rules are special in that they fire when the event's party matches the rule's party
        				if ( eventName.equals("PartyCreatedEvent") )
        				{
        					PartyCreatedEvent pcEvent = (PartyCreatedEvent)this;
        					EsfUUID packagePartyId = pcEvent.getPackageVersionPartyTemplateId(context);
                			if ( rule.isForParty(packagePartyId) )
                			{
                				actionList.addAll(rule.getActions());
                			}
        				}
        				else if ( eventName.equals("PartyActivatedEvent") )
        				{
        					PartyActivatedEvent paEvent = (PartyActivatedEvent)this;
        					EsfUUID packagePartyId = paEvent.getPackageVersionPartyTemplateId(context);
                			if ( rule.isForParty(packagePartyId) )
                			{
                				actionList.addAll(rule.getActions());
                			}
        				}
        				else if ( eventName.equals("PartyTransferredEvent") )
        				{
        					PartyTransferredEvent ptEvent = (PartyTransferredEvent)this;
        					EsfUUID packagePartyId = ptEvent.getPackageVersionPartyTemplateId(context);
                			if ( rule.isForParty(packagePartyId) )
                			{
                				actionList.addAll(rule.getActions());
                			}
        				}
        				else
        				{
        					// This is for the standard events that should be done based on where we are in the process
                			if ( rule.isForDocument(context.currDocument == null ? null : context.currDocument.getId()) )
                   			{
                  				 // package currently doesn't do any page-level filtering like document programming has
                    			if ( rule.isForParty(context.pkgVersionPartyTemplate == null ? null : context.pkgVersionPartyTemplate.getId()) )
                    			{
                    				actionList.addAll(rule.getActions());
                    			}
                    			else if ( hasAdditionalMatchingPackageVersionPartyTemplateIds() )
                    			{
                    				for( EsfUUID pvptId : getAdditionalMatchingPackageVersionPartyTemplateIds() )
                    				{
                    					if ( rule.isForParty(pvptId) )
                    					{
                            				actionList.addAll(rule.getActions());
                    						break;
                    					}
                    				}
                    			}
                   			}
        				}
        			}
        			
        		}
        	}
    	}
    	return actionList;
    }
    
    // Returns true to allow subsequent user-programmed actions to take place; or false to indicate no further actions should be done.
    public abstract boolean doBuiltInAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException;
    
    public final EsfName getEsfName()
    {
    	return new EsfName(getClass().getSimpleName());
    }
    
    public synchronized void addAdditionalMatchingPackageVersionPartyTemplateId(EsfUUID pvptId)
    {
    	if ( additionalMatchingPackageVersionPartyTemplateIds == null )
    		additionalMatchingPackageVersionPartyTemplateIds = new LinkedList<EsfUUID>();
    	additionalMatchingPackageVersionPartyTemplateIds.add(pvptId);
    }
    synchronized List<EsfUUID> getAdditionalMatchingPackageVersionPartyTemplateIds()
    {
    	return additionalMatchingPackageVersionPartyTemplateIds == null ? new LinkedList<EsfUUID>() : additionalMatchingPackageVersionPartyTemplateIds;
    }
    synchronized boolean hasAdditionalMatchingPackageVersionPartyTemplateIds()
    {
    	return additionalMatchingPackageVersionPartyTemplateIds != null && additionalMatchingPackageVersionPartyTemplateIds.size() > 0;
    }
}