// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.event;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyAssignment;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* PartyCreatedEvent represents a new transaction party being created (first time initialization).
* 
* @author Yozons, Inc.
*/
public class PartyCreatedEvent extends Event
{
	private static final long serialVersionUID = -5471989845433513368L;

	EsfName tranPartyName;
	
    /**
     * This creates an PartyCreatedEvent object
     */
    public PartyCreatedEvent(EsfName partyName)
    {
    	super();
    	tranPartyName = partyName;
    }
    
    @Override
    public boolean doBuiltInAction(Connection con, TransactionContext context, Errors errors)
    	throws SQLException
    {
    	if ( context.transaction.isEndState() )
    	{
			context.transaction.logActionDetail(con, "WARNING: Create party " + tranPartyName + " ignored since transaction is in end status: '" + context.transaction.getStatusLabel() + "'."); 
    		return false;
    	}
    	
    	TransactionParty tranParty = context.transaction.getTransactionParty(tranPartyName);
    	if ( tranParty == null )
    	{
			context.transaction.logActionDetail(con, "WARNING: Create party " + tranPartyName + " ignored since it could not be found."); 
    		return false;
    	}
    	if ( ! tranParty.isUndefined() )
    	{
			context.transaction.logActionDetail(con, "WARNING: Create party " + tranPartyName + " ignored since it has already been done. Current status: " + tranParty.getStatusLabel()); 
    		return false;
    	}
    	
        // Do initial email setup
    	PackageVersionPartyTemplate packageVersionPartyTemplate = tranParty.getPackageVersionPartyTemplate();
    	String emailFieldExpression = packageVersionPartyTemplate.getEmailFieldExpression();
    	if ( EsfString.isNonBlank(emailFieldExpression)  )
    	{
    		String email = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, emailFieldExpression).trim();
			if ( EsfString.isBlank(email) )
				context.transaction.logActionDetail(con, "WARNING: Created party '" + tranPartyName + "' has email specification: '" + emailFieldExpression + "'; that expands to a blank/empty value."); 
			else 
			{
	    		EsfEmailAddress emailAddress = new EsfEmailAddress(email); // Handles full email address spec, though for a party, we only track by the actual routing email address portion
	    		if ( emailAddress.isValid() )
	    		{
	            	TransactionPartyAssignment tranPartyAssignment = tranParty.getCurrentAssignment();
	            	tranPartyAssignment.setEmailAddress(emailAddress.getEmailAddress());
	    		}
	    		else
	    		{
	    			context.transaction.logActionDetail(con, "WARNING: Created party '" + tranPartyName + "' has invalid email specification: '" + emailFieldExpression + "'; expanded to: " + email); 
	    		}
			}
    	}
    	// Let's set up the ToDo group permission if requested and so configured
    	if ( packageVersionPartyTemplate.hasLibraryPartyTemplateTodoEsfName() )
    	{
    		EsfName todoPartyName = packageVersionPartyTemplate.getLibraryPartyTemplateTodoEsfName();
    		PartyTemplate todoParty = context.transaction.getPartyTemplate(todoPartyName, context.currLibrary);
    		if ( todoParty == null )
    		{
    			context.transaction.logActionDetail(con, "ERROR: Created party " + tranPartyName + " has To Do Party specification: '" + todoPartyName + "' that could not be found."); 
    		}
    		else if ( context.transaction.isProduction() && ! todoParty.hasTodoGroupId() )
    		{
    			context.transaction.logActionDetail(con, "WARNING: Created party " + tranPartyName + " has To Do Party specification: '" + todoPartyName + "' but it has no Production To Do Group set."); 
    		}
    		else if ( context.transaction.isTest() && ! todoParty.hasTestTodoGroupId() )
    		{
    			context.transaction.logActionDetail(con, "WARNING: Created party " + tranPartyName + " has To Do Party specification: '" + todoPartyName + "' but it has no Test To Do Group set."); 
    		}
    		else
    		{
    			tranParty.setTodoGroupId( context.transaction.isProduction() ? todoParty.getTodoGroupId() : todoParty.getTestTodoGroupId() );
    		}
    	}
    	
    	tranParty.setStatusCreated();
    	
    	context.transaction.logActionDetail(con, "Party " + tranPartyName + " has been created.");
    	return true;
    }
    
    
    // Used so we can determine the package party that goes with this tranPartyName activation
    public EsfUUID getPackageVersionPartyTemplateId(TransactionContext context)
    {
    	TransactionParty tranParty = context.transaction.getTransactionParty(tranPartyName);
    	if ( tranParty == null )
    		return null;
    	return tranParty.getPackageVersionPartyTemplateId();
    }
}