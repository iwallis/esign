// Copyright (C) 2011-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.event;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* TransactionStartedEvent represents a new transaction has been started.
* 
* @author Yozons, Inc.
*/
public class TransactionStartedEvent extends Event
{
	private static final long serialVersionUID = 5025884647158008366L;

	/**
     * This creates an TransactionStartedEvent object
     */
    public TransactionStartedEvent()
    {
    	super();
    }

    @Override
    public boolean doBuiltInAction(Connection con, TransactionContext context, Errors errors)
    	throws SQLException, EsfException
    {
    	context.transaction.setStatusInProgress();
    	context.transaction.setStatusText("Started");
    	String prodOrTest = context.isProductionTransaction() ? "Production" : "Test";
    	context.transaction.logGeneral(con, prodOrTest + " transaction has started. IP address: " + context.getIpHost() + "; browser: " + context.getUserAgent());

    	// This event is special in that we allow rules to fire for the Transaction Started Event that match any of the currently active parties.
    	for ( TransactionParty tranParty : context.transaction.getAllTransactionParties() )
    	{
    		if ( tranParty.isActive() )
    			addAdditionalMatchingPackageVersionPartyTemplateId( tranParty.getPackageVersionPartyTemplateId() );
    	}
    	
    	return true;
    }
}