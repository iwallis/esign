// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.event;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* PartyViewCompletedDocumentEvent represents a transaction party viewing the given document after it's already been completed by the party.
* 
* @author Yozons, Inc.
*/
public class PartyViewCompletedDocumentEvent extends Event
{
	private static final long serialVersionUID = 7835053360503755520L;

	EsfName tranPartyName;
	EsfName tranDocumentName;
	TransactionPartyDocument tranPartyDocument;
	
    /**
     * This creates an PartyViewCompletedDocumentEvent object
     */
    public PartyViewCompletedDocumentEvent(EsfName partyName, EsfName documentName, TransactionPartyDocument tranPartyDocument)
    {
    	super();
    	tranPartyName = partyName;
    	tranDocumentName = documentName;
    	this.tranPartyDocument = tranPartyDocument;
    }

    @Override
    public boolean doBuiltInAction(Connection con, TransactionContext context, Errors errors)
    	throws SQLException, EsfException
    {
    	// Nothing special. We already log that the party is viewing the document in the PickupTransaction servlet.
    	return true;
    }
}