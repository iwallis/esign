// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.event;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* TransactionCompletedEvent represents a transaction completing because all parties are done.
* 
* @author Yozons, Inc.
*/
public class TransactionCompletedEvent extends Event
{
	private static final long serialVersionUID = -878871804192823886L;

	EsfName tranPartyName;
	
    /**
     * This creates an TransactionCompletedEvent object
     */
    public TransactionCompletedEvent(EsfName completingPartyName)
    {
    	super();
    	tranPartyName = (completingPartyName != null && completingPartyName.isValid()) ? completingPartyName : null;
    }

    @Override
    public boolean doBuiltInAction(Connection con, TransactionContext context, Errors errors)
    	throws SQLException, EsfException
    {
    	context.transaction.clearCancelTimestamp();
    	context.transaction.setStatusCompleted();
    	context.transaction.setStatusText(Application.getInstance().getServerMessages().getString("transaction.status.completed"));
    	context.transaction.getRecord().addUpdate( new EsfName("ESF_Completed_Timestamp"), new EsfDateTime() );
    	String prodOrTest = context.isProductionTransaction() ? "Production" : "Test";
    	if ( tranPartyName != null ) {
        	context.transaction.getRecord().addUpdate( new EsfName("ESF_Completed_Party"), tranPartyName );
    		context.transaction.logGeneral(con, prodOrTest + " transaction has completed. Completed by party: " + tranPartyName);
    	} else {
    		context.transaction.logGeneral(con, prodOrTest + " transaction has completed.");
    	}
    	return true;
    }
}