// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.event;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyAssignment;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* PartyActivatedEvent represents a new transaction party being activated for access to the transaction.
* 
* @author Yozons, Inc.
*/
public class PartyActivatedEvent extends Event
{
	private static final long serialVersionUID = 1967145693440186906L;

	EsfName tranPartyName;
	
    /**
     * This creates an PartyActivatedEvent object
     */
    public PartyActivatedEvent(EsfName partyName)
    {
    	super();
    	tranPartyName = partyName;
    }

    @Override
    public boolean doBuiltInAction(Connection con, TransactionContext context, Errors errors)
    	throws SQLException, EsfException
    {
    	if ( context.transaction.isEndState() && ! context.isEsfReportsAccess() )
    	{
			context.transaction.logActionDetail(con, "WARNING: Activate party " + tranPartyName + " ignored since transaction is in end status: '" + context.transaction.getStatusLabel() + "'."); 
    		return false;
    	}
    	
    	TransactionParty tranParty = context.transaction.getTransactionParty(tranPartyName);
    	if ( tranParty == null )
    	{
			context.transaction.logActionDetail(con, "WARNING: Activate party " + tranPartyName + " ignored since it could not be found."); 
    		return false;
    	}
    	
    	if ( ! tranParty.isReports() ) // report party stays in reports status
    	{
    		context.transaction.setStatusText(tranPartyName.toString());
    		tranParty.setStatusActive();
    	}
        TransactionPartyAssignment tranPartyAssignment = tranParty.getCurrentAssignment();
        tranPartyAssignment.setStatusActivated();
        
        tranParty.notifyParty(con, context.user, context.currDocumentVersion, context.currLibrary);
        
    	if ( tranPartyAssignment.hasEmailAddress() )
    		context.transaction.logActionBasic(con, "Party " + tranPartyName + " has been activated for email: " + tranPartyAssignment.getEmailAddress());
    	else
    		context.transaction.logActionBasic(con, "Party " + tranPartyName + " has been activated without an email address.");
    	return true;
    }
    
    // Used so we can determine the package party that goes with this tranPartyName activation
    public EsfUUID getPackageVersionPartyTemplateId(TransactionContext context)
    {
    	TransactionParty tranParty = context.transaction.getTransactionParty(tranPartyName);
    	if ( tranParty == null )
    		return null;
    	return tranParty.getPackageVersionPartyTemplateId();
    }
}