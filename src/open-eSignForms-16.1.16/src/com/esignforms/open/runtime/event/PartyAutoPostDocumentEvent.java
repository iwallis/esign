// Copyright (C) 2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.event;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* PartyAutoPostDocumentEvent represents a transaction party changing a dropdown or the like that triggers an auto post on
* the given document, remaining in EDIT mode.
* It is generally used to perform calculations for auto post (similar to a save, except no data validations take place).
* 
* @author Yozons, Inc.
*/
public class PartyAutoPostDocumentEvent extends Event
{
	private static final long serialVersionUID = -1253787252256636245L;

	EsfName tranPartyName;
	EsfName tranDocumentName;
	String autoPostFieldId; 
	EsfName autoPostFieldName;
	
    /**
     * This creates an PartyAutoPostDocumentEvent object
     */
    public PartyAutoPostDocumentEvent(EsfName partyName, EsfName documentName, String autoPostFieldId, EsfName autoPostFieldName)
    {
    	super();
    	this.tranPartyName = partyName;
    	this.tranDocumentName = documentName;
    	this.autoPostFieldId = autoPostFieldId;
    	this.autoPostFieldName = autoPostFieldName;
    }

    @Override
    public boolean doBuiltInAction(Connection con, TransactionContext context, Errors errors)
    	throws SQLException, EsfException
    {
    	if ( ! context.isEsfReportsAccess() && ! PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS.equals(tranPartyName) )
    		context.transaction.setStatusText(tranPartyName.toString() + " autopost saved (" + tranDocumentName + ")");
		context.transaction.logActionDetail(con, "Party " + tranPartyName + " triggered Auto-Post in document: " + tranDocumentName + "; field: " + (autoPostFieldName == null ? "(n/a)" : autoPostFieldName) + "; fieldId: " + autoPostFieldId + "; IP address: " + context.getIpHost() + "; browser: " + context.getUserAgent());
        return true;
    }
}