// Copyright (C) 2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.integration.apspayroll.APS_EIP;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* Action is the base class of all actions used by the transaction engine.
* APS_EIP_PseudoAction submits XML structure from the APS_EIP document in the package to APS EIP.
* 
* @author Yozons, Inc.
*/
public class APS_EIP_PseudoAction extends Action
{
	private static final long serialVersionUID = -7027564406330132284L;

	APS_EIP apsEIP;

    /**
     * This creates an APS_EIP_PseudoAction object
     */
    public APS_EIP_PseudoAction(APS_EIP apsEIP)
    {
    	this.apsEIP = apsEIP;
    }
    
    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	super.updatePackagePartyIds(partyIdMapping);
    	// we have no party id mappings
    }
    
    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	// we have no document id mappings
    }
    
    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }

    
    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	apsEIP.sendXML(con,context,errors);
    }
    
    @Override
    public String specsToString()
    {
    	return "pseudo APS_EIP action";
    }

	@Override
	public Action duplicate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getEstimatedLengthXml() {
		// TODO Auto-generated method stub
		return 0;
	}
    
  
}