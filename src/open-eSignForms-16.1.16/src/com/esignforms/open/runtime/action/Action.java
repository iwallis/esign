// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.condition.CompoundCondition;
import com.esignforms.open.runtime.condition.Condition;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* Action is the base class of all actions used by the transaction engine, with an optional Condition. 
* It includes various to/from XML for use by the PackageProgrammingRule system.
* 
* @author Yozons, Inc.
*/
public abstract class Action implements java.io.Serializable
{
	private static final long serialVersionUID = -2597669822189957000L;

	boolean isChanged = false;
	
	Condition condition;
	
    /**
     * This creates an Action object
     */
    public Action()
    {
    }

    public Action(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		Element conditionElement = element.getChild("Condition", ns);
		if ( conditionElement != null )
			condition = Condition.createFromJdom(conditionElement,ns,_logger);
    }
    
    public static Action createFromJdom(Element actionElement, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		if (actionElement != null) 
		{
			String typeAttr = actionElement.getAttributeValue("type");
			String className = "com.esignforms.open.runtime.action."+typeAttr;
			try
			{
				// Try to instantiate an Action object of that type
				Class<?> actionClass = Class.forName(className);
				Constructor<?> actionConstructor = actionClass.getConstructor(new Class[]{Element.class,Namespace.class,com.esignforms.open.log.Logger.class});
				Action action = (Action)actionConstructor.newInstance(new Object[]{actionElement,ns,_logger});
				return action;
			}
			catch(ClassNotFoundException e)
			{
				_logger.warn("Action.createFromJdom(): Could not find class: " + className,e);
			}
			catch(NoSuchMethodException e)
			{
				_logger.warn("Action.createFromJdom(): Could not Action(Element,Namespace,Logger) constructor for class: " + className,e);
			}
			catch(SecurityException e)
			{
				_logger.warn("Action.createFromJdom(): Could not pass security for Action(Element,Namespace,Logger) constructor for class: " + className,e);
			}
			catch(InvocationTargetException e)
			{
				_logger.warn("Action.createFromJdom(): Could not invoke Action(Element,Namespace,Logger) for class: " + className,e);
			}
			catch(IllegalAccessException e)
			{
				_logger.warn("Action.createFromJdom(): Could not access Action(Element,Namespace,Logger) for class: " + className,e);
			}
			catch(InstantiationException e)
			{
				_logger.warn("Action.createFromJdom(): Could not instantiate Action(Element,Namespace,Logger) for class: " + className,e);
			}
		} 
		return null;
    }

    public Condition getCondition()
    {
    	return condition;
    }
    public boolean hasCondition()
    {
    	// If our top level condition is a compound condition with no subconditions, assume it has no conditions.
    	if ( condition != null )
    	{
    		if ( condition instanceof CompoundCondition )
    		{
    			CompoundCondition cc = (CompoundCondition)condition;
    			if ( ! cc.hasContainedCondition() )
    				return false;
    		}
        	return true;
    	}
    	return false;
    }
    public void setCondition(Condition c)
    {
    	condition = c;
    	setObjectChanged();
    }
    public boolean passesCondition(Connection con, TransactionContext context) throws SQLException, EsfException
    {
    	if ( hasCondition() )
    	{
    		boolean passes = condition.isAllowed(con, context);
    		if ( ! passes )
    		{
    			String details = condition instanceof CompoundCondition ? ((CompoundCondition)condition).toDetailString() : condition.toString();
    			context.transaction.logActionDetail(con,"Action '" + getEsfName() + "' skipped because it did not pass its Condition: " + details);
    		}
    		return passes;
    	}
    	return true;
    }
    
    public abstract Action duplicate();
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	if ( hasCondition() )
    		condition.updatePackagePartyIds(partyIdMapping);    	
    }
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	if ( hasCondition() )
    		condition.updateDocumentIds(documentIdMapping);    	
    }
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	if ( hasCondition() )
    		condition.updateDocumentVersionPageIds(documentVersionPageIdMapping);    	
    }

    public abstract void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException;
    
    public abstract String specsToString();

    public abstract int getEstimatedLengthXml();

	public StringBuilder appendXml(StringBuilder buf)
	{
		if ( hasCondition() )
			condition.appendXml(buf);
		return buf;
	}
	
	public String toXml()
   	{
		isChanged = false; // reset after we convert to XML
   		StringBuilder buf = new StringBuilder(getEstimatedLengthXml());
   		return appendXml(buf).toString();
   	}
	public byte[] toXmlByteArray() {
		String xml = toXml();
		return EsfString.stringToBytes(xml);
	}
	
    public final EsfName getEsfName()
    {
    	return new EsfName(getClass().getSimpleName());
    }
    
    public boolean isChanged()
    {
    	return isChanged;
    }
    public void setObjectChanged()
    {
    	isChanged = true;
    }
    public void clearObjectChanged()
    {
    	isChanged = false;
    }
}