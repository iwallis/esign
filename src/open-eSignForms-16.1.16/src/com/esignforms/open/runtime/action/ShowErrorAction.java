// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.XmlUtil;

/**
* Action is the base class of all actions used by the transaction engine.
* ShowErrorAction is used to set the report an error back to the user, typically from a data validation issue.
* 
* @author Yozons, Inc.
*/
public class ShowErrorAction extends Action
{
	private static final long serialVersionUID = 4679994968366280938L;

	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = -1274161562259639031L;

		public Spec(int o, String message, boolean htmlMessage, String type, List<String> highlightFields)
		{
			order = o;
			this.message = message;
			this.htmlMessage = htmlMessage;
			this.type = type;
			this.highlightFields = highlightFields;
		}
		int order;
		String message;
		boolean htmlMessage;
		String type;
		List<String> highlightFields;
		
		public Spec duplicate() 
		{
			return new Spec(order,message,htmlMessage,type,new LinkedList<String>(highlightFields));
		}
		public int getOrder() { return order; }
		public void setOrder(int v) { order = v; }
		public String getMessage() { return message; }
		public void setMessage(String v) { message = v == null ? "" : v.trim(); }
		public boolean isHtmlMessage() { return htmlMessage; }
		public void setHtmlMessage(boolean v) { htmlMessage = v; }
		public String getType() { return type; }
		public void setType(String v) { type = v == null ? "" : v.trim(); }
		public List<String> getHighlightFieldList() { return highlightFields; }
		public String[] getHighlightFieldArray() 
		{ 
			String[] fa = new String[highlightFields.size()];
			highlightFields.toArray(fa);
			return fa; 
		}
		public void setHighlightFieldList(List<String> v) { highlightFields = v; }
		public Set<String> getHighlightFieldSet() { return new HashSet<String>(highlightFields); }
		public void setHighlightFieldSet(Set<String> v) 
		{ 
			highlightFields.clear();
			for( String s : v )
				highlightFields.add(s.trim());
		}
	}
	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates a ShowErrorAction object
     */
    public ShowErrorAction()
    {
    }
    
    public ShowErrorAction(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		super(element,ns,_logger);
		try 
		{
			String elementName = element.getName();
			if (!"Action".equals(elementName)) 
				throw new EsfException("The Action tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!"ShowErrorAction".equals(typeAttr)) 
				throw new EsfException("The Action tag has type: " + typeAttr + "; expected ShowErrorAction.");

			List<Element> specElementList = element.getChildren("Spec", ns);
			ListIterator<Element> iter = specElementList.listIterator();
			int order = 1;
			while (iter.hasNext()) 
			{
				Element specElement = iter.next();
				
				String message = specElement.getChildTextTrim("message", ns);
				boolean htmlMessage = EsfBoolean.toBoolean(specElement.getChildTextTrim("htmlMessage", ns));
				String type = specElement.getChildTextTrim("type", ns);
				List<Element> highlightFieldElements = specElement.getChildren("highlightField", ns);
				LinkedList<String> highlightFieldList = new LinkedList<String>();
				for( Element highlightFieldElement : highlightFieldElements )
					highlightFieldList.add(highlightFieldElement.getTextTrim());
				
				Spec spec = new Spec(order++, message, htmlMessage, type, highlightFieldList);
				specList.add(spec);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }
    
    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }
    
    public ShowErrorAction duplicate()
    {
    	ShowErrorAction action = new ShowErrorAction();
    	for( Spec spec : specList )
    		action.specList.add( spec.duplicate() );
    	if ( hasCondition() )
    		action.condition = condition.duplicate();
    	return action;
    }
    
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	super.updatePackagePartyIds(partyIdMapping);
    	// we have no party id mappings
    }

    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	// we have no document id mappings
    }
    
    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }


    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	if ( specList.size() == 0 )
    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' had no errors specified to be shown.");
    	else
    	{
			for( Spec spec : specList ) 
        	{
        		String message = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion,spec.getMessage());
        		if ( EsfString.isNonBlank(message) ) 
        		{
        			if ( Errors.TYPE_SUCCESS.equals(spec.type) )
        			{
        				if ( spec.isHtmlMessage() )
        					errors.addHtmlSuccess(message, spec.getHighlightFieldArray());
        				else
        					errors.addSuccess(message, spec.getHighlightFieldArray());
        			}
        			else if ( Errors.TYPE_INFO.equals(spec.type) )
        			{
        				if ( spec.isHtmlMessage() )
        					errors.addHtmlInfo(message, spec.getHighlightFieldArray());
        				else
        					errors.addInfo(message, spec.getHighlightFieldArray());
        			}
        			else if ( Errors.TYPE_WARNING.equals(spec.type) )
        			{
        				if ( spec.isHtmlMessage() )
        					errors.addHtmlWarning(message, spec.getHighlightFieldArray());
        				else
        					errors.addWarning(message, spec.getHighlightFieldArray());
        			}
        			else
        			{
        				if ( spec.isHtmlMessage() )
        					errors.addHtmlError(message, spec.getHighlightFieldArray());
        				else
        					errors.addError(message, spec.getHighlightFieldArray());
        			}
        		}
        	}
    	}
    }
    
    @Override
    public String specsToString()
    {
    	StringBuilder buf = new StringBuilder(specList.size() * 80);
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(",");
    		buf.append(spec.message);
     	}
    	return buf.toString();
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 40;
		for( Spec spec : specList )
			size += 85 + spec.message.length() + (spec.highlightFields.size() * Literals.ESFNAME_MAX_LENGTH);
		if ( hasCondition() )
			size += condition.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Action type=\"ShowErrorAction\">\n");
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <message>").append(XmlUtil.toEscapedXml(spec.message)).append("</message>\n");
			buf.append("  <htmlMessage>").append(spec.isHtmlMessage()).append("</htmlMessage>\n");
			buf.append("  <type>").append(XmlUtil.toEscapedXml(spec.type)).append("</type>\n");
			for( String highlightField : spec.highlightFields )
				buf.append("  <highlightField>").append(XmlUtil.toEscapedXml(highlightField)).append("</highlightField>\n");
			buf.append(" </Spec>\n");
		}
		super.appendXml(buf);
		buf.append("</Action>\n");
		return buf;
	}
   
}