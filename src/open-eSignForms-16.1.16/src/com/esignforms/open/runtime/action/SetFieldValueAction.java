// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfDecimal;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfHtml;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfMoney;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentIdFieldName;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionDocument.INITIALIZE_RECORD;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.XmlUtil;

/**
* Action is the base class of all actions used by the transaction engine.
* SetFieldValueAction is used to set the specified field in the specified document to the specified value. 
* After setting the value, it can optionally preclude further field updates even if a subsequent party is specified as having edit permission.
* 
* @author Yozons, Inc.
*/
public class SetFieldValueAction extends Action
{
	private static final long serialVersionUID = -6339719269812170982L;

	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = -513300932444641295L;

		public Spec(int o, EsfUUID targetDocumentId, EsfName targetField, String v, boolean blockFurtherPartyEdits, String[] transforms, int substringOffset, int substringLength)
		{
			order = o;
			this.targetDocumentId = targetDocumentId;
			this.targetField = targetField;
			this.value = v;
			this.blockFurtherPartyEdits = blockFurtherPartyEdits;
			this.transforms = transforms;
			this.substringOffset = substringOffset;
			this.substringLength = substringLength;
		}
		int order;
		EsfUUID targetDocumentId;
		EsfName targetField;
		String value;
		boolean blockFurtherPartyEdits;
		String[] transforms;
		int substringOffset; // must be set >=0 for any substring to take place
		int substringLength; // if substringLength<1 or would go beyond the end of the original string (after its offset) we consider this to be "the rest of the string starting from the offset"
		public Spec duplicate() 
		{
			String[] newTransforms = null;
			if ( hasTransforms() )
			{
				newTransforms = new String[transforms.length];
				System.arraycopy(transforms, 0, newTransforms, 0, transforms.length);
			}
			return new Spec(order,targetDocumentId,targetField.duplicate(),value,blockFurtherPartyEdits,newTransforms,substringOffset,substringLength);
		}
		public int getOrder() { return order; }
		public void setOrder(int v) { order = v; }
		public EsfUUID getTargetDocumentId() { return targetDocumentId; }
		public void setTargetDocumentId(EsfUUID v) { targetDocumentId = v; }
		public EsfName getTargetField() { return targetField; }
		public void setTargetField(EsfName v) { targetField = v; }
		public String getValue() { return value; }
		public void setValue(String v) { value = v; /* don't trim so user can set to a single space if necessary */ }
		public boolean isBlockFurtherPartyEdits() { return blockFurtherPartyEdits; }
		public void setBlockFurtherPartyEdits(boolean v) { blockFurtherPartyEdits = v; }
		public String[] getTransforms() { return transforms; }
		public void setTransforms(String[] v) { transforms = v; };
		public boolean hasTransforms() { return transforms != null && transforms.length > 0; }
		public Set<String> getTransformSet() // for Vaadin ListSelect that operates on a Set
		{ 
			HashSet<String> set = new HashSet<String>();
			if ( transforms != null )
			{
				for(String transform : transforms )
					set.add(transform);
			}
			return set;
		}
		public void setTransformSet(Set<String> v) // for Vaadin ListSelect that operates on a Set
		{
			if ( v == null )
				transforms = null;
			else
			{
				transforms = new String[v.size()];
				v.toArray(transforms);
			}
		}
		public int getSubstringOffset() { return substringOffset; }
		public void setSubstringOffset(int v) { substringOffset = v; }
		public boolean hasSubstringOffset() { return substringOffset >= 0; }
		public String getSubstringOffsetString() { return substringOffset < 0 ? "" : Integer.toString(substringOffset); } // Vaadin string versions
		public void setSubstringOffsetString(String v) { if ( EsfString.isBlank(v) ) setSubstringOffset(-1); else setSubstringOffset(Application.getInstance().stringToInt(v, -1)); }
		public int getSubstringLength() { return substringLength; }
		public void setSubstringLength(int v) { substringLength = v; }
		public String getSubstringLengthString() { return substringLength < 1 ? "" : Integer.toString(substringLength); } // Vaadin string versions
		public void setSubstringLengthString(String v) { if ( EsfString.isBlank(v) ) setSubstringLength(-1); else setSubstringLength(Application.getInstance().stringToInt(v, -1)); }
	}
	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates an SetFieldValueAction object
     */
    public SetFieldValueAction()
    {
    }
    
    public SetFieldValueAction(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		super(element,ns,_logger);
		try 
		{
			String elementName = element.getName();
			if (!"Action".equals(elementName)) 
				throw new EsfException("The Action tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!"SetFieldValueAction".equals(typeAttr)) 
				throw new EsfException("The Action tag has type: " + typeAttr + "; expected SetFieldValueAction.");
			
			Application app = Application.getInstance();

			List<Element> specElementList = element.getChildren("Spec", ns);
			ListIterator<Element> iter = specElementList.listIterator();
			int order = 1;
			while (iter.hasNext()) 
			{
				Element specElement = iter.next();
				
				EsfUUID targetDocumentId = new EsfUUID(specElement.getChildTextTrim("targetDocumentId", ns)); 
				EsfName targetField = new EsfName(specElement.getChildTextTrim("targetField", ns)); 
				String value = specElement.getChildTextTrim("value", ns);
				boolean blockFurtherPartyEdits = EsfBoolean.toBoolean(specElement.getChildTextTrim("blockFurtherPartyEdits", ns));
				
				List<Element> transformElements = specElement.getChildren("transform", ns);
				String[] transforms = transformElements == null ? null : new String[transformElements.size()];
				Iterator<Element> transformElementsIter = transformElements.iterator();
				for(int i=0; i < transformElements.size(); ++i) 
				{
					Element transformElement = transformElementsIter.next();
					transforms[i] = transformElement.getText();
				}
				
				int substringOffset = app.stringToInt(specElement.getChildTextTrim("substringOffset", ns),-1);
				int substringLength = app.stringToInt(specElement.getChildTextTrim("substringLength", ns),-1);
				
				Spec spec = new Spec(order++, targetDocumentId, targetField, value, blockFurtherPartyEdits,transforms,substringOffset,substringLength);
				specList.add(spec);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }
    
    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }
    
    public SetFieldValueAction duplicate()
    {
    	SetFieldValueAction action = new SetFieldValueAction();
    	for( Spec spec : specList )
    		action.specList.add( spec.duplicate() );
    	if ( hasCondition() )
    		action.condition = condition.duplicate();
    	return action;
    }
    
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	super.updatePackagePartyIds(partyIdMapping);
    	// we have no party id mappings
    }

    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	for( Spec spec : specList )
    	{
    		EsfUUID newDocId = documentIdMapping.get(spec.getTargetDocumentId());
    		if ( newDocId != null )
    			spec.setTargetDocumentId(newDocId);
    	}
    }

    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }

    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	if ( specList.size() == 0 )
    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' had no fields specified to be set.");
    	else
    	{
        	for( Spec spec : specList ) 
        	{
        		Document document = context.getDocument(spec.getTargetDocumentId());
        		if ( document == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document id '" + spec.getTargetDocumentId() +"' but no such document was found.");
        		else
        		{
        			FieldTemplate ft = context.getFieldTemplate(document.getEsfName(), spec.getTargetField());
        			if ( ft == null )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' but no such field was found.");
        			else
        			{
        				// We have special code for multi-selection dropdowns that have array values
        				if ( ft.isTypeSelection() )
        				{
        					DocumentVersion docVer = context.transaction.getDocumentVersion(document);
        					DropDownVersion ddv = context.transaction.getDropDownVersion(new EsfName(ft.getInputFormatSpec()),docVer);
        					if ( ddv == null )
        					{
                    			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set dropdown field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' but its associated drop down: " + ft.getInputFormatSpec() + " was not found.");
        					}
        					else if ( ddv.isAllowMultiSelection() )
        					{
                        		EsfValue[] currValues = context.transaction.getFieldValues(document.getEsfName(), spec.getTargetField());
                        		if ( currValues == null || ! (currValues instanceof EsfString[]) )
                        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set multiselect dropdown field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' but no such field values were found.");
                        		else
                        		{
                        			DocumentIdFieldName docIdFieldName = context.getDocumentIdFieldNameFromSpec(spec.getValue());
                        			if ( docIdFieldName == null )
                            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set multiselect dropdown field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to unresolvable field spec: " + spec.getValue());
                        			else
                        			{
                        				EsfValue[] newValues = context.transaction.getFieldValues(docIdFieldName.getDocumentName(), docIdFieldName.getFieldName());
                            			if ( newValues == null || ! (currValues instanceof EsfString[]) ) 
                            				newValues = new EsfString[0];
                            			context.transaction.setFieldValues(document.getEsfName(), spec.getTargetField(), newValues);
                            			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set multiselect dropdown field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to values: " + Arrays.toString(newValues) + "; priorValue: " + Arrays.toString(currValues));            				
                        			}
                        		}
                        		continue;
        					}
        					
        					// drop through for regular field assignment code
        					
        				} // end multi-selection dropdown special code
        				
        				// Regular field assignment code
                		EsfValue currValue = context.transaction.getFieldValue(document.getEsfName(), spec.getTargetField());
                		// If there's no value to set on our first look, let's try to re-initialize the record to create any missing fields 
                		// Generally these are fields that were added into a document after the transaction has already been started.
                		if ( currValue == null ) 
                		{
                			context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' but it was not found; trying to create missing/newly-added fields.");
                			TransactionDocument retryTranDoc = context.transaction.getTransactionDocument(document.getEsfName());
            				DocumentVersion retryDocVer = context.transaction.getDocumentVersion(document);
                			if ( retryTranDoc != null && retryDocVer != null )
                			{
                				retryTranDoc.initializeRecord(context.user, context.transaction, document, retryDocVer, INITIALIZE_RECORD.ONLY_MISSING_FIELDS);
                				currValue = context.transaction.getFieldValue(document.getEsfName(), spec.getTargetField());
                			}
                		}
                		if ( currValue == null ) // if nothing after two tries, it's an error that really shouldn't happen as we already know we have the field template
                			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' but no such field value was found.");
                		else
                		{
                			String newValue = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, spec.getValue());
                			if ( spec.hasSubstringOffset() )
                				newValue = doSubstring(newValue,spec.getSubstringOffset(),spec.getSubstringLength());
                			if ( spec.hasTransforms() )
                				newValue = doTransforms(newValue,spec.transforms);
                			if ( currValue instanceof EsfHtml ) // we check this before EsfString since EsfHtml is a subclass of EsfString
                			{
                        		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), new EsfHtml(newValue));
                    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set html field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value: " + newValue + "; priorValue: " + currValue.toString());            				
                			}
                			else if ( currValue instanceof EsfString ) // put at top since most likely field to set
                			{
                        		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), new EsfString(newValue));
                    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set string field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value: " + newValue + "; priorValue: " + currValue.toString());            				
                			}
                			else if ( currValue instanceof EsfBoolean )
                			{
                        		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), new EsfBoolean(newValue));
                    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set boolean field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value: " + newValue + "; priorValue: " + currValue.toString());            				
                			}
                			else if ( currValue instanceof EsfDate )
                			{
                				if ( EsfString.isBlank(newValue) )
                				{
                    				EsfDate newDateValue = new EsfDate((EsfDate)null);
                            		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), newDateValue);
                        			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set date field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to NULL value; priorValue: " + currValue.toString());            				
                				}
                				else
                				{
                    				EsfDate newDateValue = EsfDate.CreateGuessFormat(newValue);
                            		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), newDateValue);
                        			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set date field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value: " + newDateValue + "; priorValue: " + currValue.toString());            				
                				}
                			}
                			else if ( currValue instanceof EsfDateTime )
                			{
                				if ( EsfString.isBlank(newValue) )
                				{
                       				EsfDateTime newDateTimeValue = new EsfDateTime((EsfDateTime)null);
                            		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), newDateTimeValue);
                        			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set date+time field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to NULL value; priorValue: " + currValue.toString());            				
                				}
                				else
                				{
                       				EsfDateTime newDateTimeValue = EsfDateTime.CreateGuessFormat(newValue);
                            		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), newDateTimeValue);
                        			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set date+time field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value: " + newDateTimeValue + "; priorValue: " + currValue.toString());            				
                				}
                			}
                    		else if ( currValue instanceof EsfMoney ) // check money subclass before decimal superclass
                			{
                    			if ( EsfString.isBlank(newValue) )
                    			{
                            		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), new EsfMoney((EsfMoney)null));
                        			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set money field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to NULL value: " + newValue + "; priorValue: " + currValue.toString());            				
                    			}
                    			else
                    			{
                            		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), new EsfMoney(newValue));
                        			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set money field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value: " + newValue + "; priorValue: " + currValue.toString());            				
                    			}
                			}
                			else if ( currValue instanceof EsfDecimal )
                			{
                				if ( EsfString.isBlank(newValue) )
                				{
                            		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), new EsfDecimal((EsfDecimal)null));
                        			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set decimal field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to NULL value: " + newValue + "; priorValue: " + currValue.toString());            				
                				}
                				else
                				{
                            		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), new EsfDecimal(newValue));
                        			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set decimal field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value: " + newValue + "; priorValue: " + currValue.toString());            				
                				}
                			}
                			else if ( currValue instanceof EsfEmailAddress )
                			{
                        		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), new EsfEmailAddress(newValue));
                    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set email field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value: " + newValue + "; priorValue: " + currValue.toString());            				
                			}
                			else if ( currValue instanceof EsfInteger )
                			{
                				if ( EsfString.isBlank(newValue) )
                				{
                            		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), new EsfInteger((EsfInteger)null));
                        			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set integer field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to NULL value: " + newValue + "; priorValue: " + currValue.toString());            				
                				}
                				else
                				{
                            		context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), new EsfInteger(newValue));
                        			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set integer field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value: " + newValue + "; priorValue: " + currValue.toString());            				
                				}
                			}
                			else if ( currValue instanceof EsfName )
                			{
                				EsfName esfname = new EsfName(newValue);
                				if ( esfname.isValid() )
                				{
                					context.transaction.setFieldValue(document.getEsfName(), spec.getTargetField(), esfname);
                					context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set EsfName field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value: " + newValue + "; priorValue: " + currValue.toString());            				
                				}
                				else
                					context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' tried to set EsfName field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value " + newValue + "; New value is not a valid EsfName value.");            				
                			}
                    		else if ( currValue instanceof EsfPathName )
                    			context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' tried to set EsfPathName field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value " + newValue + "; Cannot change such types.");            				
                			else if ( currValue instanceof EsfUUID )
                    			context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' tried to set EsfUUID field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value " + newValue + "; Cannot change such types.");            				
                			else
                    			context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' tried to set unexpected type of field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to value " + newValue + "; currValue field type: " + currValue.getType());            				
                			
                			if ( spec.isBlockFurtherPartyEdits() ) 
                			{
                				context.blockFurtherPartyEdits(document.getEsfName(), spec.getTargetField());
                    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to block further party edits.");            				
                			}
                		} // has value
        			} // has FieldTemplate
        		} // has document
        	} // end for each Spec
    	}
    }

    String doSubstring(String value, int offset, int length)
    {
    	if ( value == null || offset < 0 )
    		return value;
    	if ( offset >= value.length() ) // if our offset points beyond our string's length, just return an empty string
    		return "";
    	if ( length < 1 || length > (value.length()-offset) ) // if our length is not set or is longer than our string can go from the offset, just return from the offset to the end of the string
    		return value.substring(offset);
    	return value.substring(offset, offset+length); // do a regular substring
    }
    
    String doTransforms(String value, String[] transforms)
    {
    	if ( value == null || transforms == null )
    		return value;
    	
    	for( String transform : transforms )
    	{
    		if ( "toUpper".equals(transform) )
    			value = value.toUpperCase();
    		else if ( "toLower".equals(transform) )
    			value = value.toLowerCase();
    		else if ( "capFirst".equals(transform) )
    			value = EsfString.capitalizeFirstLetter(value);
    		else if ( "capFirstToLowerOthers".equals(transform) )
    			value = EsfString.capitalizeFirstLetterLowercaseOthers(value);
    		else if ( "compressWhitespace".equals(transform) )
    			value = EsfString.compressWhitespace(value);
    		else if ( "stripWhitespace".equals(transform) )
    			value = EsfString.stripWhitespace(value);
    		else if ( "trimWhitespace".equals(transform) )
    			value = value.trim();
    		else if ( "numericOnly".equals(transform) )
    			value = EsfString.getOnlyNumericWhitespace(value);
    		else if ( "alphaOnly".equals(transform) )
    			value = EsfString.getOnlyAlphaWhitespace(value);
    		else if ( "alphaNumericOnly".equals(transform) )
    			value = EsfString.getOnlyAlphaNumericWhitespace(value);
    		else if ( "firstCharOnly".equals(transform) )
    			value = value.length() < 2 ? value : value.substring(0,1);
    	}
    	
    	return value;
    }
    
    @Override
    public String specsToString()
    {
    	StringBuilder buf = new StringBuilder(specList.size() * 80);
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(',');
    		buf.append(spec.targetField).append('=').append(spec.value);
    		if ( spec.isBlockFurtherPartyEdits() )
    			buf.append(" (noEdit)");
    		if ( spec.hasTransforms() || spec.hasSubstringOffset() )
    		{
    			buf.append(" {");
    			if ( spec.hasTransforms() )
    			{
        			for( int i=0; i < spec.transforms.length; ++i )
        			{
        				if ( i > 0 )
        					buf.append(',');
        				buf.append(spec.transforms[i]);
        			}
    			}
    			if ( spec.hasSubstringOffset() )
    			{
    				if ( spec.hasTransforms() )
        				buf.append(',');
    				if ( spec.substringLength < 1 )
    					buf.append("substring ").append(spec.substringOffset);
    				else
    					buf.append("substring ").append(spec.substringOffset).append(':').append(spec.substringLength);
    			}
    			buf.append('}');
    		}
     	}
    	return buf.toString();
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 50;
		for( Spec spec : specList )
		{
			size += 235 + spec.targetDocumentId.getEstimatedLengthXml() + spec.targetField.getEstimatedLengthXml() + spec.value.length() + 5 /*"false" for blockFurtherPartyEdits boolean*/ + 8 /*for 2 substring numbers*/;
			if ( spec.hasTransforms() )
			{
				for( String transform : spec.transforms )
					size += 26 + transform.length();
			}
		}
		if ( hasCondition() )
			size += condition.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Action type=\"SetFieldValueAction\">\n");
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <targetDocumentId>").append(spec.targetDocumentId.toXml()).append("</targetDocumentId>\n");
			buf.append("  <targetField>").append(spec.targetField.toXml()).append("</targetField>\n");
			buf.append("  <value>").append(XmlUtil.toEscapedXml(spec.value)).append("</value>\n");
			buf.append("  <blockFurtherPartyEdits>").append(spec.blockFurtherPartyEdits).append("</blockFurtherPartyEdits>\n");
			if ( spec.hasTransforms() )
			{
				for( String transform : spec.transforms )
					buf.append("  <transform>").append(XmlUtil.toEscapedXml(transform)).append("</transform>\n");
			}
			buf.append("  <substringOffset>").append(spec.substringOffset).append("</substringOffset>\n");
			buf.append("  <substringLength>").append(spec.substringLength).append("</substringLength>\n");
			buf.append(" </Spec>\n");
		}
		super.appendXml(buf);
		buf.append("</Action>\n");
		return buf;
	}
   
}