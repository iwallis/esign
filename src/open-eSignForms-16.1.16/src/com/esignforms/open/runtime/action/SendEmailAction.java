// Copyright (C) 2011-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.email.OutboundEmailMessage;
import com.esignforms.open.email.OutboundEmailMessageAttachment;
import com.esignforms.open.email.RecipientList;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.integration.wkhtmltopdf.HtmlToPdf;
import com.esignforms.open.prog.DocumentIdFieldName;
import com.esignforms.open.prog.DocumentIdPartyId;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.EmailTemplateVersion;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionFile;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyAssignment;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.User;
import com.esignforms.open.util.StringReplacement;
import com.esignforms.open.util.XmlUtil;

/**
* Action is the base class of all actions used by the transaction engine.
* SendEmailAction is used to send an email.
* 
* @author Yozons, Inc.
*/
public class SendEmailAction extends Action
{
	private static final long serialVersionUID = -287350269339696369L;

	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = -8549083846288360173L;

		public Spec(int o, EsfName emailTemplateName, String emailAddressFieldExpression, EsfUUID partyId, List<DocumentIdPartyId> documentIdPartyIdList, EsfBoolean attachAsPdf, String pdfFileNameSpec, List<DocumentIdFieldName> fileFieldList)
		{
			order = o;
			this.emailTemplateName = emailTemplateName;
			this.setEmailAddressFieldExpression(emailAddressFieldExpression);
			this.partyId = partyId; 
			this.documentIdPartyIdList = documentIdPartyIdList;
			this.attachAsPdf = attachAsPdf;
			this.setPdfFileNameSpec(pdfFileNameSpec);
			this.fileFieldList = fileFieldList;
		}
		
		int order;
		EsfName emailTemplateName;
		String emailAddressFieldExpression;
		EsfUUID partyId;
		List<DocumentIdPartyId> documentIdPartyIdList;
		List<DocumentIdFieldName> fileFieldList;
		EsfBoolean attachAsPdf;
		String pdfFileNameSpec;
		
		public Spec duplicate() 
		{
			List<DocumentIdPartyId> clonedDocumentIdPartyIdList = new LinkedList<DocumentIdPartyId>(documentIdPartyIdList);
			List<DocumentIdFieldName> clonedFileFieldList = new LinkedList<DocumentIdFieldName>(fileFieldList);
			return new Spec(order,emailTemplateName.duplicate(),emailAddressFieldExpression,partyId,
					        clonedDocumentIdPartyIdList,new EsfBoolean(attachAsPdf),pdfFileNameSpec,clonedFileFieldList);
		}
		
		public int getOrder() { return order; }
		public void setOrder(int v) { order = v; }
		
		public EsfName getEmailTemplateName() { return emailTemplateName; }
		public void setEmailTemplateName(EsfName v) { emailTemplateName = v; }
		public String getEmailAddressFieldExpression() { return emailAddressFieldExpression; }
		public void setEmailAddressFieldExpression(String v) { emailAddressFieldExpression = v == null ? "" : v.trim(); }
		
		public EsfUUID getPartyId() { return partyId; }
		public boolean hasPartyId() { return partyId != null && ! partyId.isNull(); }
		public void setPartyId(EsfUUID v) { partyId = v; }
		
		public List<DocumentIdPartyId> getDocumentIdList() { return documentIdPartyIdList; }
		public void setDocumentIdList(List<DocumentIdPartyId> v) { documentIdPartyIdList = v; }
		public Set<DocumentIdPartyId> getDocumentIdSet() { return new HashSet<DocumentIdPartyId>(documentIdPartyIdList); }
		public void setDocumentIdSet(Set<DocumentIdPartyId> v) 
		{ 
			documentIdPartyIdList.clear();
			for( DocumentIdPartyId dp : v )
				documentIdPartyIdList.add(dp);
		}

		public boolean isAttachAsPdf() { return attachAsPdf.isTrue(); }
		public void setAttachAsPdf(boolean v) { attachAsPdf = new EsfBoolean(v); } 
		
		public String getPdfFileNameSpec() { return pdfFileNameSpec; }
		public boolean hasPdfFileNameSpec() { return EsfString.isNonBlank(pdfFileNameSpec); }
		public void setPdfFileNameSpec(String v) { pdfFileNameSpec = v == null ? "" : v.trim(); }

		public boolean hasFileFields() { return fileFieldList != null && fileFieldList.size() > 0; }
		public List<DocumentIdFieldName> getFileFieldList() { return fileFieldList; }
		public void setFileFieldList(List<DocumentIdFieldName> v) { fileFieldList = v; }
		public Set<DocumentIdFieldName> getFileFieldSet() { return new HashSet<DocumentIdFieldName>(fileFieldList); }
		public void setFileFieldSet(Set<DocumentIdFieldName> v) { 
			fileFieldList.clear();
			for( DocumentIdFieldName f : v )
				fileFieldList.add(f);
		}

	}
	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates an SendEmailAction object
     */
    public SendEmailAction()
    {
    }
    
    public SendEmailAction(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		super(element,ns,_logger);
		try 
		{
			String elementName = element.getName();
			if (!"Action".equals(elementName)) 
				throw new EsfException("The Action tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!"SendEmailAction".equals(typeAttr)) 
				throw new EsfException("The Action tag has type: " + typeAttr + "; expected SendEmailAction.");

			List<Element> specElementList = element.getChildren("Spec", ns);
			ListIterator<Element> iter = specElementList.listIterator();
			int order = 1;
			while (iter.hasNext()) 
			{
				Element specElement = iter.next();
				
				EsfName emailTemplateName = new EsfName(specElement.getChildTextTrim("emailTemplateName", ns)); 
				EsfUUID partyId = new EsfUUID(specElement.getChildTextTrim("partyId", ns));
				
				List<DocumentIdPartyId> documentIdPartyIdList = new LinkedList<DocumentIdPartyId>();
				
				List<Element> documentIdElementList = specElement.getChildren("documentId", ns);
				List<Element> tranPartyIdElementList = specElement.getChildren("tranPartyId", ns);
				ListIterator<Element> documentIdIter = documentIdElementList.listIterator();
				ListIterator<Element> tranPartyIdIter = tranPartyIdElementList.listIterator();
				while( documentIdIter.hasNext() && tranPartyIdIter.hasNext() ) 
				{
					Element documentIdElement = documentIdIter.next();
					Element tranPartyIdElement = tranPartyIdIter.next();
					
					DocumentIdPartyId dp = new DocumentIdPartyId(new EsfUUID(documentIdElement.getTextTrim()), new EsfUUID(tranPartyIdElement.getTextTrim()));
					documentIdPartyIdList.add(dp);
				}
				
				EsfBoolean attachAsPdf = new EsfBoolean(specElement.getChildTextTrim("attachAsPdf", ns));
				
				List<DocumentIdFieldName> fileFieldList = new LinkedList<DocumentIdFieldName>();

				List<Element> fileFieldDocumentIdElementList = specElement.getChildren("fileFieldDocumentId",ns);
				List<Element> fileFieldNameElementList = specElement.getChildren("fileFieldName", ns);
				ListIterator<Element> fileFieldDocumentIdIter = fileFieldDocumentIdElementList.listIterator();
				ListIterator<Element> fileFieldNameIter = fileFieldNameElementList.listIterator();
				while( fileFieldDocumentIdIter.hasNext() && fileFieldNameIter.hasNext() ) 
				{
					Element fileFieldDocumentIdElement = fileFieldDocumentIdIter.next();
					Element fileFieldNameElement = fileFieldNameIter.next();
					DocumentIdFieldName df = new DocumentIdFieldName(new EsfUUID(fileFieldDocumentIdElement.getTextTrim()), new EsfName(fileFieldNameElement.getTextTrim()));
					fileFieldList.add( df );
				}

				Spec spec = new Spec(order++, emailTemplateName, specElement.getChildTextTrim("emailAddressFieldExpression", ns), 
							partyId, documentIdPartyIdList, attachAsPdf, specElement.getChildTextTrim("pdfFileNameSpec", ns), fileFieldList);
				specList.add(spec);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }
    
    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }
    
    public SendEmailAction duplicate()
    {
    	SendEmailAction action = new SendEmailAction();
    	for( Spec spec : specList )
    		action.specList.add( spec.duplicate() );
    	if ( hasCondition() )
    		action.condition = condition.duplicate();
    	return action;
    }
    
    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	super.updatePackagePartyIds(partyIdMapping);
    	for( Spec spec : specList )
    	{
    		if ( spec.hasPartyId() )
    			spec.setPartyId( partyIdMapping.get(spec.getPartyId()) );
    	}
    }
    
    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	// We have no document ids (only transaction document ids)
    }

    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }

    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	if ( specList.size() == 0 )
    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' had no email templates specified to be send emails.");
    	else
    	{
        	for( Spec spec : specList ) 
        	{
        		EsfUUID targetPartyId = spec.hasPartyId() ? spec.getPartyId() : null;
        		if ( targetPartyId == null && context.pkgVersionPartyTemplate != null ) 
        			targetPartyId = context.pkgVersionPartyTemplate.getId();
        		
        		// The email doesn't really have to be to a party at all, but if we create a LINK in the email, we'll use either the defined party or the current transaction party
        		// to build one.
        		TransactionParty tranParty = context.transaction.getTransactionPartyByPackageParty(targetPartyId);
    			TransactionPartyAssignment tranPartyAssignment = tranParty == null ? null : tranParty.getCurrentAssignment();
        		
        		EmailTemplate emailTemplate = context.transaction.getEmailTemplate(spec.getEmailTemplateName(), context.currLibrary);
        		EmailTemplateVersion emailTemplateVersion = null;
        		if ( emailTemplate == null )
        		{
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' specified email template '" + spec.getEmailTemplateName() + "' but no such email template was found. Email to: " + spec.getEmailAddressFieldExpression());
        		}
        		else
        		{
        			emailTemplateVersion = context.transaction.doProductionResolve() ? emailTemplate.getProductionEmailTemplateVersion() : emailTemplate.getTestEmailTemplateVersion();
        			if ( emailTemplateVersion == null )
        			{
        	  			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' specified email template '" + spec.getEmailTemplateName() + 
        	  					"', but could not find a " + (context.transaction.doProductionResolve()?"PRODUCTION":"TEST") + " version. Email to: " + spec.getEmailAddressFieldExpression());
        			}
        		}
        		
        		// 9/9/2014 - SendEmailAction supports multiple email addresses
        		RecipientList emailAddresses = null;
        		String emailFieldExpression = spec.getEmailAddressFieldExpression();
        		if ( EsfString.isNonBlank(emailFieldExpression) && ! "${EMAIL}".equalsIgnoreCase(emailFieldExpression)  )
        		{
        			String emails = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, emailFieldExpression).trim();
        			if ( EsfString.isBlank(emails) )
        				context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' has email specification: '" + emailFieldExpression + "'; that expands to a blank/empty value."); 
        			else 
        			{
            			emailAddresses = new RecipientList(emails); // Handles full email address spec for one or more email addresses
            			if ( emailAddresses.hasInvalid() )
            				context.transaction.logActionDetail(con, "ERROR: Action '" + getEsfName() + "' has invalid email specification: '" + emailFieldExpression + "'; expanded to: '" + emails + "'"); 
        			}
        		}
        		else
        		{
        			// If the party has an email assigned, we'll try that one.
        			if ( tranPartyAssignment != null && tranPartyAssignment.hasEmailAddress() )
        			{
        				emailAddresses = new RecipientList(tranPartyAssignment.getEmailAddress());
        				context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' has no email specification, so using the party's configured email: " + tranPartyAssignment.getEmailAddress()); 
        			}
        			else if ( tranParty != null )
        			{
        				// Our last chance is to see if the related package party has an email spec we can try
        				PackageVersionPartyTemplate pvpt = tranParty.getPackageVersionPartyTemplate();
        				if ( pvpt.hasEmailFieldExpression() )
        				{
        					emailFieldExpression = pvpt.getEmailFieldExpression();
        					String email = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, emailFieldExpression).trim();
                			if ( EsfString.isBlank(email) )
                				context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' has no email specification, and the package party email specification '" + emailFieldExpression + "' expands to a blank/empty value."); 
                			else 
                			{
            					emailAddresses = new RecipientList(email); // Handles full email address spec
                    			if ( emailAddresses.hasInvalid() )
                    				context.transaction.logActionDetail(con, "ERROR: Action '" + getEsfName() + "' has no email specification, and the package party has an invalid email specification: '" + emailFieldExpression + "'; expanded to: '" + email + "'"); 
                			}
        				}
        				else if ( tranParty.hasTodoGroupId() && tranParty.getPackageVersionPartyTemplate().isNotifyAllTodo() ) // if we send an email to a party belonging to a To Do group and notify all is set, we'll do that then.
        				{
        	    			Group todoGroup = Group.Manager.getById(tranParty.getTodoGroupId());
        					if ( todoGroup == null )
        					{
                				context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' attempted to use To Do group id: " + tranParty.getTodoGroupId() + "; for party: " + tranParty.getPackageVersionPartyTemplate().getEsfName() + "; but that group was not found. No email was sent."); 
        					}
        					else if ( todoGroup.isDisabled() )
        					{
                				context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' attempted to use To Do group id: " + tranParty.getTodoGroupId() + "; for party: " + tranParty.getPackageVersionPartyTemplate().getEsfName() + "; but that group is disabled. No email was sent."); 
        					}
        					else
        					{
        	    				// TODO: Do we need a "context user" for a TransactionTemplate that specifies more explicitly which user should be used to resolve
        	    				// this scheme for determining which users belong to a ToDo group and are visible (we don't want to leak "all member users")?
        	    				User groupContextUser = User.Manager.getById(context.transaction.getTransactionTemplate().getLastUpdatedByUserId());

        						Collection<User> todoUserList = todoGroup.getMemberUsers(groupContextUser);
        						if ( todoUserList == null || todoUserList.size() < 1 )
        						{
                    				context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' attempted to use To Do group: " + todoGroup.getPathName() + 
                    						"; for party: " + tranParty.getPackageVersionPartyTemplate().getEsfName() + "; but no member users were found." + (groupContextUser==null?" (No TransactionTemplate last updated user.)":" Per TransactionTemplate last updated user: "+groupContextUser.getFullDisplayName())); 
        						}
        						else
        						{
                					emailAddresses = new RecipientList(); 
        							for( User emailUser : todoUserList )
        							{
        								emailAddresses.add(emailUser.getEmailAddress());
        							}
        						}        						
        					}
        				}
            			else
            			{
            				context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' has no email specification and the package party also has no email specification or To Do group with a notify all set. No email was sent."); 
            			}
        			}
        			else
        				context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' has no email specification it could find. No email was sent."); 
        		}
        		
        		// If we have both one or more email addresses and a template, let's send it out
        		if ( emailAddresses != null && emailAddresses.hasEmailAddresses() && ! emailAddresses.hasInvalid() && emailTemplateVersion != null )
        		{
        			Application app = Application.getInstance();
        			LinkedList<StringReplacement.FromToSpec> list = new LinkedList<StringReplacement.FromToSpec>();
        			list.add( new StringReplacement.FromToSpec("${CONTEXTPATH}",app.getExternalContextPath()) );
        			if ( tranPartyAssignment != null )
        			{
            			list.add( new StringReplacement.FromToSpec("${PICKUPCODE}",tranPartyAssignment.getPickupCode()) );
            			list.add( new StringReplacement.FromToSpec("${LINK}",app.getExternalContextPath() + "/P/" + tranPartyAssignment.getPickupCode()) );
        			}
        			else
        			{
            			list.add( new StringReplacement.FromToSpec("${PICKUPCODE}","") );
            			list.add( new StringReplacement.FromToSpec("${LINK}",app.getExternalContextPath()) );
        			}
        			list.add( new StringReplacement.FromToSpec("${EMAIL}",emailAddresses.getEmailText()) );
        			
        			OutboundEmailMessage outboundEmail = OutboundEmailMessage.Manager.createNew(context.transaction, context.currDocumentVersion, tranParty, context.user==null?null:context.user.getId(), emailTemplateVersion, list);

        			// Get the list of any attachments to include, converting to PDF if necessary...
        			List<TransactionPartyDocument> tranPartyDocumentList = context.transaction.getTransactionPartyDocumentsWithSnapshot(spec.documentIdPartyIdList);
        			// Skip any documents that we didn't capture on purpose (such as auto-completing parties). 
        			ListIterator<TransactionPartyDocument> tpdIterator = tranPartyDocumentList.listIterator();
        			while( tpdIterator.hasNext() )
        			{
        				TransactionPartyDocument tpd = tpdIterator.next();
        				if ( tpd.isSnapshotDocumentSkipped() )
        					tpdIterator.remove();
        			}
        			
        			if ( tranPartyDocumentList.size() > 0 )
        			{
        				
        				// If it's a PDF, we basically submit all HTML agreements to a PDF converter and attach that single PDF.
        				if ( spec.isAttachAsPdf() )
        				{
        					// If they gave us a file name spec, try to use it, else use our default name. Ensure it ends with .pdf.
        					String pdfFileName = null;
        					if ( spec.hasPdfFileNameSpec() )
        						pdfFileName = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, spec.getPdfFileNameSpec()).trim();
        					if ( EsfString.isBlank(pdfFileName) )
        						pdfFileName = context.transaction.getPackageDownloadFileName(context.user, context.currDocumentVersion);
        					if ( ! pdfFileName.toLowerCase().endsWith(".pdf") )
        						pdfFileName += ".pdf";
        					
        					pdfFileName = Application.getInstance().sanitizeFileName(pdfFileName);
        					
        					boolean useLandscape = false;
        					String[] htmlFiles = new String[tranPartyDocumentList.size()];
        					int i = 0;
            				for( TransactionPartyDocument tpd : tranPartyDocumentList )
            				{
            					TransactionDocument td = context.transaction.getTransactionDocument(tpd.getTransactionDocumentId());
            					if ( td.getDocumentVersion().isLandscape() )
            						useLandscape = true;
            					htmlFiles[i++] = tpd.getSnapshotDocument();
            				}
            				HtmlToPdf htmlToPdf = new HtmlToPdf();
            				byte[] pdf = useLandscape ? htmlToPdf.generateLandscapeSignedPdf(htmlFiles) : htmlToPdf.generateSignedPdf(htmlFiles);
            				if ( pdf != null )
            				{
            					OutboundEmailMessageAttachment att = OutboundEmailMessageAttachment.Manager.createNew(pdfFileName, Application.CONTENT_TYPE_PDF, pdf);
                				outboundEmail.addAttachment(att);
            				}
            				else
            				{
                				context.transaction.logActionDetail(con, "ERROR: Action '" + getEsfName() + "' has PDF attachment requested but the PDF generation failed sending to: '" + emailAddresses.getEmailText() + "'; using PDF file name: " + pdfFileName); 
            				}
        				}
        				else
        				{
            				for( TransactionPartyDocument tpd : tranPartyDocumentList )
            				{
            					TransactionDocument tranDoc = context.transaction.getTransactionDocument(tpd.getTransactionDocumentId());
            					String html = tpd.getSnapshotDocument();
            					
            					String htmlFileName;
            					if ( tpd.hasDocumentFileName() )
            						htmlFileName = tpd.getDocumentFileName();
            					else 
            						htmlFileName = app.getServerMessages().getString("SendEmailAction.attachHTML.defaultFileName",tranDoc.getDocument().getEsfName().toString());
            					
            					OutboundEmailMessageAttachment att = OutboundEmailMessageAttachment.Manager.createNew(
            							htmlFileName, Application.CONTENT_TYPE_HTML, EsfString.stringToBytes(html));
            					outboundEmail.addAttachment(att);
            				}
        				}
        			}
        			
        			// Get the list of uploaded files, if any should be attached
        			if ( spec.hasFileFields() ) 
        			{
        				List<TransactionFile> tranFileList = context.transaction.getTransactionFiles(spec.fileFieldList);
        				for( TransactionFile tf : tranFileList )
        				{
        					byte[] fileData = tf.getFileDataFromDatabase();
        					if ( fileData == null )
                				context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' could not load uploaded file: " + tf.getFileName()); 
        					else
        					{
            					OutboundEmailMessageAttachment att = OutboundEmailMessageAttachment.Manager.createNew(tf.getFileName(), tf.getFileMimeType(), fileData);
            					outboundEmail.addAttachment(att);
        					}
        				}
        			}
        			
        			if ( outboundEmail.save(con) )
        	        {
    	    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' has sent email template: '" + spec.getEmailTemplateName() + "' to: '" + outboundEmail.getEmailTo() + "'; subject: " + outboundEmail.getEmailSubject());    	        	
        	        }
        	        else
        	        {
    	    			context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' failed to create outbound email for email template: '" + spec.getEmailTemplateName() + "' to: '" + outboundEmail.getEmailTo() + "'; subject: " + outboundEmail.getEmailSubject()); 
        	        }
        		}
        	}
    	}
    }
    
    @Override
    public String specsToString()
    {
    	StringBuilder buf = new StringBuilder(specList.size() * 80);
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(",");
    		buf.append(spec.emailTemplateName);
     	}
    	return buf.toString();
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 50;
		for( Spec spec : specList )
		{
			size += 174 + spec.emailTemplateName.getEstimatedLengthXml();
			if ( spec.emailAddressFieldExpression != null )
				size += spec.emailAddressFieldExpression.length();
			if ( spec.hasPartyId() )
				size += spec.getPartyId().getEstimatedLengthXml();
			size += 2 * spec.documentIdPartyIdList.size() * (EsfUUID.DISPLAY_LENGTH + 30); // 2 times for two EsfUUIDs
			size += 35; // attachAsPdf element
			if ( spec.hasPdfFileNameSpec() )
				size += spec.getPdfFileNameSpec().length();
			if ( spec.hasFileFields() )
				size += spec.getFileFieldList().size() * (Literals.ESFNAME_MAX_LENGTH + 34 + EsfUUID.DISPLAY_LENGTH + 48);
		}
		if ( hasCondition() )
			size += condition.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Action type=\"SendEmailAction\">\n");
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <emailTemplateName>").append(spec.emailTemplateName.toXml()).append("</emailTemplateName>\n");
			buf.append("  <emailAddressFieldExpression>").append(XmlUtil.toEscapedXml(spec.emailAddressFieldExpression)).append("</emailAddressFieldExpression>\n");
			buf.append("  <partyId>").append(spec.hasPartyId() ? spec.partyId.toXml() : "").append("</partyId>\n");
			for( DocumentIdPartyId dp : spec.documentIdPartyIdList ) {
				buf.append("  <documentId>").append(dp.documentId.toXml()).append("</documentId>\n");
				buf.append("  <tranPartyId>").append(dp.partyId == null || dp.partyId.isNull() ? "" : dp.partyId.toXml()).append("</tranPartyId>\n");
			}
			buf.append("  <attachAsPdf>").append(spec.attachAsPdf.toXml()).append("</attachAsPdf>\n");
			buf.append("  <pdfFileNameSpec>").append(XmlUtil.toEscapedXml(spec.pdfFileNameSpec)).append("</pdfFileNameSpec>\n");
			for( DocumentIdFieldName df : spec.fileFieldList ) {
				buf.append("  <fileFieldDocumentId>").append(df.getDocumentId().toXml()).append("</fileFieldDocumentId>\n");
				buf.append("  <fileFieldName>").append(df.getFieldName().toXml()).append("</fileFieldName>\n");
			}
			buf.append(" </Spec>\n");
		}
		super.appendXml(buf);
		buf.append("</Action>\n");
		return buf;
	}
   
}