// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.runtime.workflow.TransactionEngine;
import com.esignforms.open.util.XmlUtil;

/**
* Action is the base class of all actions used by the transaction engine.
* ChangeStatusAction is used to change the status of a transaction or party.
* It can 
*   cancel a transaction (now or in the future)
*   clear a scheduled auto-cancel
*   suspend a transaction
*   resume a suspended transaction
*   skip a document (remove all parties from it)
*   skip a party (so no longer is party of transaction)
*   activate a party (to make it activate ahead of it's standard process flow; typically only for "final step parties" who can be concurrent)
* 
* @author Yozons, Inc.
*/
public class ChangeStatusAction extends Action
{
	private static final long serialVersionUID = 98137730232125952L;

	public static final String CANCEL_TRANSACTION_TYPE = "cancelTransaction";
	public static final String CLEAR_CANCEL_TRANSACTION_TYPE = "clearCancelTransaction";
	public static final String SUSPEND_TRANSACTION_TYPE = "suspendTransaction";
	public static final String RESUME_TRANSACTION_TYPE = "resumeTransaction";
	public static final String SKIP_DOCUMENT_TYPE = "skipDocument";
	public static final String SKIP_DOCUMENT_PARTY_TYPE = "skipDocumentParty";
	public static final String SKIP_PARTY_TYPE = "skipParty";
	public static final String ACTIVATE_PARTY_TYPE = "activateParty";
	
	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = 2812607057536198775L;

		public Spec(int o, String statusChangeType, String reasonSpec, List<EsfUUID> documentIdList, List<EsfUUID> partyIdList, String cancelNumUnits, String cancelUnit, 
						boolean cancelRetentionChange, String cancelRetentionNumUnits, String cancelRetentionUnit)
		{
			order = o;
			this.statusChangeType = statusChangeType;
			this.reasonSpec = reasonSpec;
			this.documentIdList = documentIdList;
			this.partyIdList = partyIdList;
			setCancelNumUnits(cancelNumUnits);
			this.cancelUnit = cancelUnit;
			this.cancelRetentionChange = cancelRetentionChange;
			setCancelRetentionNumUnits(cancelRetentionNumUnits);
			this.cancelRetentionUnit = cancelRetentionUnit;

		}
		int order;
		String statusChangeType;
		String reasonSpec; // may contain field specs that we'll expand first
		
		String cancelNumUnits; // specified only for CANCEL_TRANSACTION_TYPE
		String cancelUnit; // specified only for CANCEL_TRANSACTION_TYPE
		boolean cancelRetentionChange; // specified only for CANCEL_TRANSACTION_TYPE
		String cancelRetentionNumUnits; // specified only for CANCEL_TRANSACTION_TYPE
		String cancelRetentionUnit; // specified only for CANCEL_TRANSACTION_TYPE
		
		List<EsfUUID> documentIdList; // Only if the statusChangeType is SKIP_DOCUMENT_TYPE or SKIP_DOCUMENT_PARTY_TYPE
		List<EsfUUID> partyIdList; // Only if the statusChangeType is SKIP_PARTY_TYPE or ACTIVATE_PARTY_TYPE, or SKIP_DOCUMENT_PARTY_TYPE
		
		public Spec duplicate() 
		{
			return new Spec(order,statusChangeType,reasonSpec,
							isSkipDocumentType() || isSkipDocumentPartyType() ? new LinkedList<EsfUUID>(documentIdList) : null,
							isSkipPartyType() || isSkipDocumentPartyType() || isActivatePartyType() ? new LinkedList<EsfUUID>(partyIdList) : null,
							cancelNumUnits,cancelUnit,cancelRetentionChange,cancelRetentionNumUnits,cancelRetentionUnit);
		}
		public int getOrder() { return order; }
		public void setOrder(int v) { order = v; }
		public String getStatusChangeType() { return statusChangeType; }
		public boolean isCancelTransactionType() { return CANCEL_TRANSACTION_TYPE.equals(statusChangeType); }
		public boolean isClearCancelTransactionType() { return CLEAR_CANCEL_TRANSACTION_TYPE.equals(statusChangeType); }
		public boolean isSuspendTransactionType() { return SUSPEND_TRANSACTION_TYPE.equals(statusChangeType); }
		public boolean isResumeTransactionType() { return RESUME_TRANSACTION_TYPE.equals(statusChangeType); }
		public boolean isSkipDocumentType() { return SKIP_DOCUMENT_TYPE.equals(statusChangeType); }
		public boolean isSkipDocumentPartyType() { return SKIP_DOCUMENT_PARTY_TYPE.equals(statusChangeType); }
		public boolean isSkipPartyType() { return SKIP_PARTY_TYPE.equals(statusChangeType); }
		public boolean isActivatePartyType() { return ACTIVATE_PARTY_TYPE.equals(statusChangeType); }
		public void setStatusChangeType(String v) { statusChangeType = v == null ? "" : v.trim(); }
		public String getReasonSpec() { return reasonSpec; }
		public void setReasonSpec(String v) { reasonSpec = v == null ? "" : v.trim(); }
		public String getCancelNumUnits() { return cancelNumUnits; }
		public void setCancelNumUnits(String v) { cancelNumUnits = EsfString.isBlank(v) ? "1" : v.trim(); }
		public String getCancelUnit() { return cancelUnit; }
		public boolean isCancelNow() { return Literals.TIME_INTERVAL_UNIT_NOW.equals(cancelUnit); }
		public void setCancelUnit(String v) { cancelUnit = v == null ? "" : v.trim(); }
		public boolean isCancelRetentionChange() { return cancelRetentionChange; }
		public void setCancelRetentionChange(boolean v) { cancelRetentionChange = v; }
		public String getCancelRetentionNumUnits() { return cancelRetentionNumUnits; }
		public void setCancelRetentionNumUnits(String v) { cancelRetentionNumUnits = EsfString.isBlank(v) ? "1" : v.trim(); }
		public String getCancelRetentionUnit() { return cancelRetentionUnit; } 
	    public boolean isCancelRetentionForever() { return Literals.TIME_INTERVAL_UNIT_FOREVER.equals(cancelRetentionUnit); }
	    public EsfDateTime getCancelRetentionDateTimeFromNow()
	    {
	    	EsfDateTime retentionDate = new EsfDateTime();
	    	retentionDate.addTimeInterval(Application.getInstance().stringToInt(cancelRetentionNumUnits,0), cancelRetentionUnit);
	    	return retentionDate;
	    }
		public void setCancelRetentionUnit(String v) { cancelRetentionUnit = EsfString.isBlank(v) ? "year" : v.trim(); } // hack to ensure we always have a valid value even if not used
		
		// Only if the statusChangeType is SKIP_DOCUMENT_TYPE or SKIP_DOCUMENT_PARTY_TYPE
		public List<EsfUUID> getDocumentIdList() { return documentIdList; }
		public void setDocumentIdList(List<EsfUUID> v) { documentIdList = v; }
		public Set<EsfUUID> getDocumentIdSet() { return documentIdList == null ? new HashSet<EsfUUID>() : new HashSet<EsfUUID>(documentIdList); }
		public void setDocumentIdSet(Set<EsfUUID> v)
		{
			if ( documentIdList == null )
				documentIdList = new LinkedList<EsfUUID>();
			else
				documentIdList.clear();
			for( EsfUUID id : v )
				documentIdList.add(id);
		}

		// Only if the statusChangeType is SKIP_PARTY_TYPE or ACTIVATE_PARTY_TYPE, or SKIP_DOCUMENT_PARTY_TYPE
		public List<EsfUUID> getPartyIdList() { return partyIdList; }
		public void setPartyIdList(List<EsfUUID> v) { partyIdList = v; }
		public Set<EsfUUID> getPartyIdSet() { return partyIdList == null ? new HashSet<EsfUUID>() : new HashSet<EsfUUID>(partyIdList); }
		public void setPartyIdSet(Set<EsfUUID> v)
		{
			if ( partyIdList == null )
				partyIdList = new LinkedList<EsfUUID>();
			else
				partyIdList.clear();
			for( EsfUUID id : v )
				partyIdList.add(id);
		}
	}
	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates an ChangeStatusAction object
     */
    public ChangeStatusAction()
    {
    }
    
    public ChangeStatusAction(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		super(element,ns,_logger);
		try 
		{
			String elementName = element.getName();
			if (!"Action".equals(elementName)) 
				throw new EsfException("The Action tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!"ChangeStatusAction".equals(typeAttr)) 
				throw new EsfException("The Action tag has type: " + typeAttr + "; expected ChangeStatusAction.");

			List<Element> specElementList = element.getChildren("Spec", ns);
			ListIterator<Element> iter = specElementList.listIterator();
			int order = 1;
			while (iter.hasNext()) 
			{
				Element specElement = iter.next();
				
				String statusChangeType = specElement.getChildTextTrim("statusChangeType", ns);
				String reasonSpec = specElement.getChildTextTrim("reasonSpec", ns);
				
				String cancelNumUnits = null;
				String cancelUnit = null;
				boolean cancelRetentionChange = false;
				String cancelRetentionNumUnits = null;
				String cancelRetentionUnit = null;
				LinkedList<EsfUUID> documentIdList = null;
				LinkedList<EsfUUID> partyIdList = null;
				if ( CANCEL_TRANSACTION_TYPE.equals(statusChangeType) )
				{
					cancelNumUnits = specElement.getChildTextTrim("cancelNumUnits", ns);
					if ( EsfString.isBlank(cancelNumUnits) )
						cancelNumUnits = "1";

					cancelUnit = specElement.getChildTextTrim("cancelUnit", ns);
					if ( EsfString.isBlank(cancelUnit) )
						cancelUnit = Literals.TIME_INTERVAL_UNIT_NOW;
					
					cancelRetentionChange = EsfBoolean.toBoolean( specElement.getChildTextTrim("cancelRetentionChange", ns) );
					
					cancelRetentionNumUnits = specElement.getChildTextTrim("cancelRetentionNumUnits", ns);
					if ( EsfString.isBlank(cancelRetentionNumUnits) )
						cancelRetentionNumUnits = "1";

					cancelRetentionUnit = specElement.getChildTextTrim("cancelRetentionUnit", ns);
					if ( EsfString.isBlank(cancelRetentionUnit) )
						cancelRetentionUnit = Literals.TIME_INTERVAL_UNIT_NOW;
				}
				/*
				else if ( SUSPEND_TRANSACTION_TYPE.equals(statusChangeType) )
				{
				}
				else if ( RESUME_TRANSACTION_TYPE.equals(statusChangeType) )
				{
				} 
				*/
				else if ( SKIP_DOCUMENT_TYPE.equals(statusChangeType) )
				{
					documentIdList = new LinkedList<EsfUUID>();
					
					List<Element> skipDocumentIdElementList = specElement.getChildren("skipDocumentId", ns); // bad name, but keep for XML
					ListIterator<Element> idIter = skipDocumentIdElementList.listIterator();
					while( idIter.hasNext() )
					{
						Element skipDocumentIdElement = idIter.next();
						EsfUUID skipDocumentId = new EsfUUID( skipDocumentIdElement.getTextTrim() );
						documentIdList.add(skipDocumentId);
					}
				}
				else if ( SKIP_DOCUMENT_PARTY_TYPE.equals(statusChangeType) )
				{
					documentIdList = new LinkedList<EsfUUID>();
					
					List<Element> skipDocumentIdElementList = specElement.getChildren("skipDocumentId", ns); // bad name, but keep for XML
					ListIterator<Element> idIter = skipDocumentIdElementList.listIterator();
					while( idIter.hasNext() )
					{
						Element skipDocumentIdElement = idIter.next();
						EsfUUID skipDocumentId = new EsfUUID( skipDocumentIdElement.getTextTrim() );
						documentIdList.add(skipDocumentId);
					}

					partyIdList = new LinkedList<EsfUUID>();
					
					List<Element> skipPartyIdElementList = specElement.getChildren("skipPartyId", ns); // bad name, but keep for XML
					idIter = skipPartyIdElementList.listIterator();
					while( idIter.hasNext() )
					{
						Element skipPartyIdElement = idIter.next();
						EsfUUID skipPartyId = new EsfUUID( skipPartyIdElement.getTextTrim() );
						partyIdList.add(skipPartyId);
					}
				}
				else if ( SKIP_PARTY_TYPE.equals(statusChangeType) )
				{
					partyIdList = new LinkedList<EsfUUID>();
					
					List<Element> skipPartyIdElementList = specElement.getChildren("skipPartyId", ns); // bad name, but keep for XML
					ListIterator<Element> idIter = skipPartyIdElementList.listIterator();
					while( idIter.hasNext() )
					{
						Element skipPartyIdElement = idIter.next();
						EsfUUID skipPartyId = new EsfUUID( skipPartyIdElement.getTextTrim() );
						partyIdList.add(skipPartyId);
					}
				}
				else if ( ACTIVATE_PARTY_TYPE.equals(statusChangeType) )
				{
					partyIdList = new LinkedList<EsfUUID>();
					
					List<Element> partyIdElementList = specElement.getChildren("partyId", ns);
					ListIterator<Element> idIter = partyIdElementList.listIterator();
					while( idIter.hasNext() )
					{
						Element partyIdElement = idIter.next();
						EsfUUID partyId = new EsfUUID( partyIdElement.getTextTrim() );
						partyIdList.add(partyId);
					}
				}
				
				Spec spec = new Spec(order++, statusChangeType, reasonSpec, documentIdList, partyIdList, cancelNumUnits, cancelUnit, cancelRetentionChange, cancelRetentionNumUnits, cancelRetentionUnit);
				specList.add(spec);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }
    
    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }
    
    public ChangeStatusAction duplicate()
    {
    	ChangeStatusAction action = new ChangeStatusAction();
    	for( Spec spec : specList )
    		action.specList.add( spec.duplicate() );
    	if ( hasCondition() )
    		action.condition = condition.duplicate();
    	return action;
    }
    
    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {	
    	super.updatePackagePartyIds(partyIdMapping);
    	for( Spec spec : specList )
    	{
    		if ( spec.isSkipPartyType() || spec.isActivatePartyType() || spec.isSkipDocumentPartyType() )
    		{
    	    	LinkedList<EsfUUID> newList = new LinkedList<EsfUUID>();

    	    	for( EsfUUID partyId : spec.getPartyIdList() )
    	    	{
    	    		EsfUUID newPartyId = partyIdMapping.get(partyId);
    	    		newList.add( newPartyId == null ? partyId : newPartyId); // keep the original if we don't have a mapping
    	    	}
    			spec.setPartyIdList( newList );
    		}
    	}
    }

    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	for( Spec spec : specList )
    	{
    		if ( spec.isSkipDocumentType() || spec.isSkipDocumentPartyType() )
    		{
    	    	LinkedList<EsfUUID> newList = new LinkedList<EsfUUID>();

    	    	for( EsfUUID docId : spec.getDocumentIdList() )
    	    	{
    	    		EsfUUID newDocId = documentIdMapping.get(docId);
    	    		newList.add( newDocId == null ? docId : newDocId); // keep the original if we don't have a mapping
    	    	}
    			spec.setDocumentIdList( newList );
    		}
    	}
    }
    
    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }

    
    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	if ( specList.size() == 0 )
    	{
    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' has no status change specifications.");
    	}
    	else
    	{
        	for( Spec spec : specList ) 
        	{
        		String reasonText = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion,spec.getReasonSpec());
        		if ( spec.isCancelTransactionType() )
        		{
        			// If the cancel retention changes, do that update first in case it's canceled right away and the new retention should be in place
        			if ( spec.isCancelRetentionChange() )
        			{
        				context.transaction.setCancelRetentionSpec(spec.getCancelRetentionNumUnits() + " " + spec.getCancelRetentionUnit());
            			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' updating cancel retention spec transaction to: " + context.transaction.getCancelRetentionSpec() + "; reason: " + reasonText);
        			}

        			if ( context.transaction.isCanceled() )
        			{
            			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' tried to cancel transaction that is already canceled, ignoring. Cancel reason: " + reasonText);
        			}
        			else if ( context.transaction.isCompleted() )
        			{
            			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' tried to cancel transaction that is already completed, ignoring. Cancel reason: " + reasonText);
        			}
        			else
        			{
        				// If the transaction isn't already canceled or completed...
            			if ( spec.isCancelNow() )
            			{
                			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' canceling transaction, reason: " + reasonText);
                			TransactionEngine engine = new TransactionEngine(context);
                			engine.queueTransactionCanceledEvent(reasonText);
                			engine.doWork(con,errors);
            			}
            			else
            			{
            				context.transaction.getRecord().addUpdate( Transaction.TRAN_RECORD_AUTO_CANCEL_REASON_ESFNAME, new EsfString(reasonText) );
            		    	EsfDateTime autoCancelDate = new EsfDateTime();
            		    	EsfInteger numUnits = new EsfInteger(spec.getCancelNumUnits());
            		    	autoCancelDate.addTimeInterval( numUnits.isNull() ? 1 : numUnits.toInt(), spec.getCancelUnit());
            		    	context.transaction.setCancelTimestamp(autoCancelDate);
                			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' SETTING auto-cancel transaction to: " + autoCancelDate.toLogString() + "; reason: " + reasonText);
            			}
        			}
        		}
        		else if ( spec.isClearCancelTransactionType() )
        		{
        			String origCancelTimestamp = context.transaction.hasCancelTimestamp() ? context.transaction.getCancelTimestamp().toLogString() : "(none set)";
        			context.transaction.clearCancelTimestamp();
        			context.transaction.setCancelRetentionSpec(null);
    				context.transaction.getRecord().remove( Transaction.TRAN_RECORD_AUTO_CANCEL_REASON_ESFNAME );
        			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' CLEARING auto-cancel transaction previously set to: " + origCancelTimestamp + "; reason: " + reasonText);            				
        		}
        		else if ( spec.isSuspendTransactionType() )
        		{
        			if ( context.transaction.isCanceled() )
        			{
            			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' tried to suspend the transaction that is already canceled, ignoring. Suspend reason: " + reasonText);
        			}
        			else if ( context.transaction.isCompleted() )
        			{
            			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' tried to suspend the transaction that is already completed, ignoring. Suspend reason: " + reasonText);
        			}
        			else if ( context.transaction.isSuspended() )
        			{
            			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' tried to suspend the transaction that is already suspended, ignoring. Suspend reason: " + reasonText);
        			}
        			else
        			{
            			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' suspending transaction, reason: " + reasonText);
            			TransactionEngine engine = new TransactionEngine(context);
            			engine.queueTransactionSuspendedEvent(reasonText);
            			engine.doWork(con,errors);
        			}
        		}
        		else if ( spec.isResumeTransactionType() )
        		{
        			if ( context.transaction.isCanceled() )
        			{
            			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' tried to resume the transaction that is already canceled, ignoring. Resume reason: " + reasonText);
        			}
        			else if ( context.transaction.isCompleted() )
        			{
            			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' tried to resume the transaction that is already completed, ignoring. Resume reason: " + reasonText);
        			}
        			else if ( context.transaction.isInProgress() )
        			{
            			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' tried to resume the transaction that is already in progress, ignoring. Resume reason: " + reasonText);
        			}
        			else
        			{
            			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' resuming transaction, reason: " + reasonText);
            			TransactionEngine engine = new TransactionEngine(context);
            			engine.queueTransactionResumedEvent(reasonText);
            			engine.doWork(con,errors);
        			}
        		}
        		else if ( spec.isSkipDocumentType() )
        		{
        			if ( spec.getDocumentIdList().size() == 0 )
        			{
        	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Document but no documents are specified.");
        			}
        			else
        			{
        				for( EsfUUID skipDocumentId : spec.getDocumentIdList() )
        				{
        					TransactionDocument tranDoc = context.transaction.getTransactionDocumentByDocumentId(skipDocumentId); 
        					if ( tranDoc != null )
        					{
            					EsfName documentName = tranDoc.getDocument().getEsfName();
            					List<TransactionParty> tranParties = context.transaction.getAllTransactionParties();
            					for( TransactionParty tranParty : tranParties )
            					{
            						TransactionPartyDocument tranPartyDoc = tranParty.getTransactionPartyDocument(tranDoc.getId());
            						if ( tranPartyDoc != null ) // if null, the party doesn't work on this document
            						{
                            			EsfName partyName = tranParty.getPackageVersionPartyTemplate().getEsfName();
                   						if ( tranPartyDoc.isNetYetRetrieved() || tranPartyDoc.isViewOptional() )
                						{
                            	    		context.transaction.logActionBasic(con, "Action '" + getEsfName() + "' Skipped Document '" + documentName + "' for transaction party '" + partyName + "', reason: " + reasonText);
                							tranPartyDoc.setStatusSkipped();
                							// if this is our current party, let's update our tran documents list
                							if ( tranParty.equals(context.transactionParty) )
                							{
                            					context.transactionPartyDocuments.clear();
                							    context.transactionPartyDocuments.addAll(tranParty.getNonSkippedTransactionPartyDocuments()); // we don't want any documents that have been skipped for this party
                							}
                						}
                						else if ( tranPartyDoc.isSkipped() )
                            	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Document '" + documentName + "' but transaction party '" + partyName + "' has already been skipped.");
                						else 
                            	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Document '" + documentName + "' but transaction party '" + partyName + "' has already accessed it with status: " + tranPartyDoc.getStatus());
            						}
             					}
        					}
        					else
        					{
                	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Document but no document found with id: " + skipDocumentId);
        					}
        				} // end for
        			}
        		}
        		else if ( spec.isSkipDocumentPartyType() )
        		{
        			if ( spec.getDocumentIdList().size() == 0 )
        			{
        	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Document Party but no documents are specified.");
        			}
        			else if ( spec.getPartyIdList().size() == 0 )
        			{
        	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Document Party but no parties are specified.");
        			}
        			else
        			{
        				for( EsfUUID skipDocumentId : spec.getDocumentIdList() )
        				{
        					TransactionDocument tranDoc = context.transaction.getTransactionDocumentByDocumentId(skipDocumentId); 
        					if ( tranDoc != null )
        					{
            					EsfName documentName = tranDoc.getDocument().getEsfName();
            					List<TransactionParty> tranParties = context.transaction.getAllTransactionParties();
            					for( TransactionParty tranParty : tranParties )
            					{
            						TransactionPartyDocument tranPartyDoc = tranParty.getTransactionPartyDocument(tranDoc.getId());
            						if ( tranPartyDoc == null ) // if null, the party doesn't work on this document, ignore this tran party
            							continue;
            						
            						// Determine if the tranParty is in my skip parties list
            						boolean skippedPartyFound = false;
                    				for( EsfUUID skipPartyId : spec.getPartyIdList() )
                    				{
                    					TransactionParty checkSkipTranParty = context.transaction.getTransactionPartyByPackageParty(skipPartyId);
                    					if ( checkSkipTranParty == null )
                    					{
                            	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Document Package Party ID '" + skipPartyId + "' on document '" + documentName + "' but the corresponding transaction party was not found.");
                    					}
                    					else if ( tranParty.equals(checkSkipTranParty) )
                    					{
                    						skippedPartyFound = true;
                    						break;
                    					}
                    				}            						
            						// If this tranParty is not in our skip list, ignore this tran party.
                    				if ( ! skippedPartyFound )
                    					continue;
                    				
                        			EsfName partyName = tranParty.getPackageVersionPartyTemplate().getEsfName();

                        			if ( tranParty.isReports() )
                        			{
                        	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Document Party on document '" + documentName + "' for transaction party '" + partyName + "' but that is the built-in reports access party. Not skipping.");
                    	    			continue;
                        			}
            						
               						if ( tranPartyDoc.isNetYetRetrieved() || tranPartyDoc.isViewOptional() || tranPartyDoc.isViewOptionalViewed() )
            						{
               							if ( tranPartyDoc.isViewOptionalViewed() )
               								context.transaction.logActionBasic(con, "WARNING Action '" + getEsfName() + "' Skipped Document Party on document '" + documentName + "' for transaction party '" + partyName + "' even though has already viewed the document, reason: " + reasonText);
               							else
               								context.transaction.logActionBasic(con, "Action '" + getEsfName() + "' Skipped Document Party on document '" + documentName + "' for transaction party '" + partyName + "', reason: " + reasonText);
               							tranPartyDoc.setStatusSkipped();
            							// if this is our current party, let's update our tran documents list
            							if ( tranParty.equals(context.transactionParty) )
            							{
                        					context.transactionPartyDocuments.clear();
            							    context.transactionPartyDocuments.addAll(tranParty.getNonSkippedTransactionPartyDocuments()); // we don't want any documents that have been skipped for this party
            							}
            						}
            						else if ( tranPartyDoc.isSkipped() )
                        	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Document Party on document '" + documentName + "' but transaction party '" + partyName + "' has already been skipped on this document. Skip ignored.");
            						else 
                        	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Document Party on document '" + documentName + "' but transaction party '" + partyName + "' has already accessed it with status: " + tranPartyDoc.getStatus() + ". Skip ignored.");
             					} // end for all tran parties
        					}
        					else
        					{
                	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Document Party but no document found with id: " + skipDocumentId);
        					}
        				} // end for skip documents list
        			}
        		}
        		else if ( spec.isSkipPartyType() )
        		{
        			if ( spec.getPartyIdList().size() == 0 )
        			{
        	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Party but no parties are specified.");
        			}
        			else
        			{
        				for( EsfUUID skipPartyId : spec.getPartyIdList() )
        				{
        					TransactionParty tranParty = context.transaction.getTransactionPartyByPackageParty(skipPartyId);
        					if ( tranParty == null )
        					{
                	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Package Party ID '" + skipPartyId + "' but the corresponding transaction party was not found.");
        					}
        					else
        					{
                    			EsfName partyName = tranParty.getPackageVersionPartyTemplate().getEsfName();
                    			if ( tranParty.isReports() )
                    	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Party '" + partyName + "' but that is the built-in reports access party. Not skipping.");
                    			else if ( tranParty.isCompleted() )
                    	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Party '" + partyName + "' but that party is already completed. Not skipping.");
            					else if ( tranParty.isSkipped() )
                    	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Party '" + partyName + "' but that party is already skipped. Skip ignored.");
            					else
            					{
                					if ( tranParty.isActive() ) 
                					{
                        	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Skip Party '" + partyName + "' on the already active party. Skipping...");
                					}
                    	    		context.transaction.logActionBasic(con, "Action '" + getEsfName() + "' skipping party '" + partyName + "', reason: " + reasonText);
            						tranParty.setStatusSkipped();
            						tranParty.getCurrentAssignment().setStatusSkipped();
            					}
        					}
        				} // end for
        			}
        		}
        		else if ( spec.isActivatePartyType() )
        		{
        			if ( spec.getPartyIdList().size() == 0 )
        			{
        	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Activate Party but no parties are specified.");
        			}
        			else
        			{
        				for( EsfUUID activatePartyId : spec.getPartyIdList() )
        				{
        					TransactionParty tranParty = context.transaction.getTransactionPartyByPackageParty(activatePartyId);
        					if ( tranParty == null )
        					{
                	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Activate Package Party ID '" + activatePartyId + "' but the corresponding transaction party was not found.");
        					}
        					else
        					{
                       			EsfName partyName = tranParty.getPackageVersionPartyTemplate().getEsfName();
                    			if ( tranParty.isReports() )
                    	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Activate Party but '" + partyName + "' is the built-in reports access party. Not activated.");
                    			else if ( tranParty.isSkipped() )
                    	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Activate Party but party '" + partyName + "' is skipped. Not activated.");
            					else if ( tranParty.isActive() )
                    	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Activate Party but party '" + partyName + "'is already active. Not activated.");
            					else
            					{
                					if ( tranParty.isCompleted() ) 
                					{
                        	    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' requested Activate Party but party '" + partyName + "' is already completed. Re-activating...");
                					}
                    	    		context.transaction.logActionBasic(con, "Action '" + getEsfName() + "' activating party '" + partyName + "', reason: " + reasonText);
                        			TransactionEngine engine = new TransactionEngine(context);
                        			if ( tranParty.isUndefined() )
                        			{
                        				engine.queuePartyCreatedEvent(partyName);
                        			}
                        			engine.queuePartyActivatedEvent(partyName);
                        			engine.doWork(con,errors);
            					}
        					}
         				} // end for
        			}
        		}
        		else
        		{
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' specifies an unexpected type: '" + spec.getStatusChangeType() + "'.");            				
        		}
        	}
    	}
    }
    
    @Override
    public String specsToString()
    {
    	StringBuilder buf = new StringBuilder(specList.size() * 80);
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(",");
    		buf.append(spec.statusChangeType);
     	}
    	return buf.toString();
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 50;
		for( Spec spec : specList )
		{
			size += 85 + spec.statusChangeType.length() + spec.reasonSpec.length();
			if ( spec.isCancelTransactionType() )
				size += 200;
			if ( spec.isSkipDocumentType() || spec.isSkipDocumentPartyType() )
				size += (40 + EsfUUID.DISPLAY_LENGTH) * spec.getDocumentIdList().size();
			if ( spec.isSkipPartyType() || spec.isSkipDocumentPartyType() )
				size += (30 + EsfUUID.DISPLAY_LENGTH) * spec.getPartyIdList().size();
			if ( spec.isActivatePartyType() )
				size += (25 + EsfUUID.DISPLAY_LENGTH) * spec.getPartyIdList().size();
		}
		
		if ( hasCondition() )
			size += condition.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Action type=\"ChangeStatusAction\">\n");
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <statusChangeType>").append(XmlUtil.toEscapedXml(spec.statusChangeType)).append("</statusChangeType>\n");
			buf.append("  <reasonSpec>").append(XmlUtil.toEscapedXml(spec.reasonSpec)).append("</reasonSpec>\n");
			if ( spec.isCancelTransactionType() )
			{
				buf.append("  <cancelNumUnits>").append(XmlUtil.toEscapedXml(spec.cancelNumUnits)).append("</cancelNumUnits>\n");
				buf.append("  <cancelUnit>").append(XmlUtil.toEscapedXml(spec.cancelUnit)).append("</cancelUnit>\n");
				buf.append("  <cancelRetentionChange>").append(spec.cancelRetentionChange).append("</cancelRetentionChange>\n");
				buf.append("  <cancelRetentionNumUnits>").append(XmlUtil.toEscapedXml(spec.cancelRetentionNumUnits)).append("</cancelRetentionNumUnits>\n");
				buf.append("  <cancelRetentionUnit>").append(XmlUtil.toEscapedXml(spec.cancelRetentionUnit)).append("</cancelRetentionUnit>\n");
			}
			if ( spec.isSkipDocumentType() || spec.isSkipDocumentPartyType() )
			{
				for ( EsfUUID skipDocumentId : spec.getDocumentIdList() )
					buf.append("  <skipDocumentId>").append(skipDocumentId.toXml()).append("</skipDocumentId>\n"); // bad name but keep for XML
			}
			if ( spec.isSkipPartyType() || spec.isSkipDocumentPartyType() )
			{
				for ( EsfUUID skipPartyId : spec.getPartyIdList() )
					buf.append("  <skipPartyId>").append(skipPartyId.toXml()).append("</skipPartyId>\n"); // bad name but keep for XML
			}
			if ( spec.isActivatePartyType() )
			{
				for ( EsfUUID partyId : spec.getPartyIdList() )
					buf.append("  <partyId>").append(partyId.toXml()).append("</partyId>\n");
			}
			buf.append(" </Spec>\n");
		}
		super.appendXml(buf);
		buf.append("</Action>\n");
		return buf;
	}
   
}