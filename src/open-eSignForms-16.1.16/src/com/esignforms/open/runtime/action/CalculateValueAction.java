// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDecimal;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfMoney;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionDocument.INITIALIZE_RECORD;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.XmlUtil;

/**
* Action is the base class of all actions used by the transaction engine.
* CalculateValueAction is used to set the specified field in the specified document to the specified calculated value.
* 
* @author Yozons, Inc.
*/
public class CalculateValueAction extends Action
{
	private static final long serialVersionUID = -9070547128428011957L;

	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = 6638911077050161514L;

		public static String OPERATOR_ADD = "+";
		public static String OPERATOR_SUBTRACT = "-";
		public static String OPERATOR_MULTIPLY = "*";
		public static String OPERATOR_DIVIDE = "/";
		
		public Spec(int o, EsfUUID targetDocumentId, EsfName targetField, 
				EsfUUID leftSideDocumentId, EsfName leftSideField, EsfUUID rightSideDocumentId, EsfName rightSideField, String operator)
		{
			order = o;
			this.targetDocumentId = targetDocumentId;
			this.targetField = targetField;
			this.leftSideDocumentId = leftSideDocumentId;
			this.leftSideField = leftSideField;
			this.rightSideDocumentId = rightSideDocumentId;
			this.rightSideField = rightSideField;
			this.operator = operator;
		}
		int order;
		EsfUUID targetDocumentId;
		EsfName targetField;
		EsfUUID leftSideDocumentId;
		EsfName leftSideField;
		EsfUUID rightSideDocumentId;
		EsfName rightSideField;
		String operator;
		public Spec duplicate() 
		{
			return new Spec(order,targetDocumentId,targetField.duplicate(),
					        leftSideDocumentId,leftSideField.duplicate(),rightSideDocumentId,rightSideField.duplicate(),operator);
		}
		public int getOrder() { return order; }
		public void setOrder(int v) { order = v; }
		
		public EsfUUID getTargetDocumentId() { return targetDocumentId; }
		public void setTargetDocumentId(EsfUUID v) { targetDocumentId = v; }
		public EsfName getTargetField() { return targetField; }
		public void setTargetField(EsfName v) { targetField = v; }
		
		public EsfUUID getLeftSideDocumentId() { return leftSideDocumentId; }
		public void setLeftSideDocumentId(EsfUUID v) { leftSideDocumentId = v; }
		public EsfName getLeftSideField() { return leftSideField; }
		public void setLeftSideField(EsfName v) { leftSideField = v; }
		
		public EsfUUID getRightSideDocumentId() { return rightSideDocumentId; }
		public void setRightSideDocumentId(EsfUUID v) { rightSideDocumentId = v; }
		public EsfName getRightSideField() { return rightSideField; }
		public void setRightSideField(EsfName v) { rightSideField = v; }
		
		public String getOperator() { return operator; }
		public boolean isOperatorAdd() { return OPERATOR_ADD.equals(operator); }
		public boolean isOperatorSubtract() { return OPERATOR_SUBTRACT.equals(operator); }
		public boolean isOperatorMultiply() { return OPERATOR_MULTIPLY.equals(operator); }
		public boolean isOperatorDivide() { return OPERATOR_DIVIDE.equals(operator); }
		public void setOperator(String v) { operator = v.trim(); }
	}
	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates an CalculateValueAction object
     */
    public CalculateValueAction()
    {
    }
    
    public CalculateValueAction(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		super(element,ns,_logger);
		try 
		{
			String elementName = element.getName();
			if (!"Action".equals(elementName)) 
				throw new EsfException("The Action tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!"CalculateValueAction".equals(typeAttr)) 
				throw new EsfException("The Action tag has type: " + typeAttr + "; expected CalculateValueAction.");

			List<Element> specElementList = element.getChildren("Spec", ns);
			ListIterator<Element> specIter = specElementList.listIterator();
			int order = 1;
			while( specIter.hasNext() ) 
			{
				Element specElement = specIter.next();
				
				EsfUUID targetDocumentId = new EsfUUID(specElement.getChildTextTrim("targetDocumentId", ns)); 
				EsfName targetField = new EsfName(specElement.getChildTextTrim("targetField", ns)); 
				
				EsfUUID leftSideDocumentId = new EsfUUID(specElement.getChildTextTrim("leftSideDocumentId", ns)); 
				EsfName leftSideField = new EsfName(specElement.getChildTextTrim("leftSideField", ns)); 
				
				EsfUUID rightSideDocumentId = new EsfUUID(specElement.getChildTextTrim("rightSideDocumentId", ns)); 
				EsfName rightSideField = new EsfName(specElement.getChildTextTrim("rightSideField", ns)); 
				
				Spec spec = new Spec(order++, targetDocumentId, targetField, 
						leftSideDocumentId, leftSideField, rightSideDocumentId, rightSideField, specElement.getChildTextTrim("operator", ns));
				specList.add(spec);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }
    
    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }
    
    public CalculateValueAction duplicate()
    {
    	CalculateValueAction action = new CalculateValueAction();
    	for( Spec spec : specList )
    		action.specList.add( spec.duplicate() );
    	if ( hasCondition() )
    		action.condition = condition.duplicate();
    	return action;
    }
    
    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	super.updatePackagePartyIds(partyIdMapping);
    	// we have no party id mappings
    }
    
    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	for( Spec spec : specList )
    	{
    		EsfUUID newDocId = documentIdMapping.get(spec.getTargetDocumentId());
    		if ( newDocId != null )
    			spec.setTargetDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getLeftSideDocumentId());
    		if ( newDocId != null )
    			spec.setLeftSideDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getRightSideDocumentId());
    		if ( newDocId != null )
    			spec.setRightSideDocumentId(newDocId);
    	}
    }
    
    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }

    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	if ( specList.size() == 0 )
    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' had no fields specified to be set.");
    	else
    	{
        	for( Spec spec : specList ) 
        	{
        		Document targetDocument = context.getDocument(spec.getTargetDocumentId());
        		Document leftSideDocument = context.getDocument(spec.getLeftSideDocumentId());
        		Document rightSideDocument = context.getDocument(spec.getRightSideDocumentId());
        		if ( targetDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document id '" + spec.getTargetDocumentId() + "' but no such document was found.");
        		else if ( leftSideDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document id '" + spec.getTargetDocumentId() + "' with left side document id '" + spec.getLeftSideDocumentId() + "' but no such left side document was found.");
        		else if ( rightSideDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document id '" + spec.getTargetDocumentId() + "' with right side document id '" + spec.getRightSideDocumentId() + "' but no such right side document was found.");
        		else
        		{
            		EsfValue targetValue = context.transaction.getFieldValue(targetDocument.getEsfName(), spec.getTargetField());
            		EsfValue leftSideValue = context.transaction.getFieldValue(leftSideDocument.getEsfName(), spec.getLeftSideField());
            		EsfValue rightSideValue = context.transaction.getFieldValue(rightSideDocument.getEsfName(), spec.getRightSideField());

            		// If there's no value to set on our first look, let's try to re-initialize the record to create any missing fields 
            		// Generally these are fields that were added into a document after the transaction has already been started.
            		if ( targetValue == null ) 
            		{
            			context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() +"' but it was not found; trying to create missing/newly-added fields.");
            			TransactionDocument retryTranDoc = context.transaction.getTransactionDocument(targetDocument.getEsfName());
        				DocumentVersion retryDocVer = context.transaction.getDocumentVersion(targetDocument);
            			if ( retryTranDoc != null && retryDocVer != null )
            			{
            				retryTranDoc.initializeRecord(context.user, context.transaction, targetDocument, retryDocVer, INITIALIZE_RECORD.ONLY_MISSING_FIELDS);
            				targetValue = context.transaction.getFieldValue(targetDocument.getEsfName(), spec.getTargetField());
            			}
            		}
            		if ( targetValue == null )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' but no such target field was found.");
            		else if ( leftSideValue == null )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' with left side field '" + spec.getLeftSideField() + "' in document '" + leftSideDocument.getEsfName() + "' but no such left side field was found.");
            		else if ( rightSideValue == null )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' with right side field '" + spec.getRightSideField() + "' in document '" + rightSideDocument.getEsfName() + "' but no such right side field was found.");
            		else
            		{
            			if ( targetValue instanceof EsfInteger )
            			{
            				if ( leftSideValue instanceof EsfInteger )
            				{
            					EsfInteger lhInteger = new EsfInteger( (EsfInteger)leftSideValue );
            					if ( spec.isOperatorAdd() )
            						lhInteger.add(rightSideValue);
            					else if ( spec.isOperatorSubtract() )
            						lhInteger.subtract(rightSideValue);
            					else if ( spec.isOperatorMultiply() )
            						lhInteger.multiply(rightSideValue);
            					else if ( spec.isOperatorDivide() )
            						lhInteger.divide(rightSideValue);
                        		context.transaction.setFieldValue(targetDocument.getEsfName(), spec.getTargetField(), lhInteger);
                    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set integer field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() +"' to value: " + lhInteger + "; priorValue: " + targetValue.toString());            					
            				}
            				else if ( leftSideValue instanceof EsfDecimal )
            				{
            					EsfDecimal lhDecimal = new EsfDecimal( (EsfDecimal)leftSideValue );
            					if ( spec.isOperatorAdd() )
            						lhDecimal.add(rightSideValue);
            					else if ( spec.isOperatorSubtract() )
            						lhDecimal.subtract(rightSideValue);
            					else if ( spec.isOperatorMultiply() )
            						lhDecimal.multiply(rightSideValue);
            					else if ( spec.isOperatorDivide() )
            						lhDecimal.divide(rightSideValue);
            					EsfInteger targetInteger = lhDecimal.toEsfInteger();
                        		context.transaction.setFieldValue(targetDocument.getEsfName(), spec.getTargetField(), targetInteger);
                    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set integer field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() +"' to value: " + targetInteger + "; priorValue: " + targetValue.toString());            					
            				}
            				else
            				{
                    			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set integer field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' but left field is type '" + leftSideValue.getType() + "' instead of Integer, Decimal or Money.");            					
            				}
            			}
            			else if ( targetValue instanceof EsfMoney )
            			{
            				if ( leftSideValue instanceof EsfInteger )
            				{
            					EsfDecimal lhDecimal = ((EsfInteger)leftSideValue).toEsfDecimal();
            					if ( spec.isOperatorAdd() )
            						lhDecimal.add(rightSideValue);
            					else if ( spec.isOperatorSubtract() )
            						lhDecimal.subtract(rightSideValue);
            					else if ( spec.isOperatorMultiply() )
            						lhDecimal.multiply(rightSideValue);
            					else if ( spec.isOperatorDivide() )
            						lhDecimal.divide(rightSideValue);
            					EsfMoney targetMoney = new EsfMoney( lhDecimal, ((EsfMoney)targetValue).getCurrency() );
                        		context.transaction.setFieldValue(targetDocument.getEsfName(), spec.getTargetField(), targetMoney);
                    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set money field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() +"' to value: " + targetMoney + "; priorValue: " + targetValue.toString());            					
            				}
            				else if ( leftSideValue instanceof EsfDecimal )
            				{
            					EsfDecimal lhDecimal = new EsfDecimal( (EsfDecimal)leftSideValue );
            					if ( spec.isOperatorAdd() )
            						lhDecimal.add(rightSideValue);
            					else if ( spec.isOperatorSubtract() )
            						lhDecimal.subtract(rightSideValue);
            					else if ( spec.isOperatorMultiply() )
            						lhDecimal.multiply(rightSideValue);
            					else if ( spec.isOperatorDivide() )
            						lhDecimal.divide(rightSideValue);
            					EsfMoney targetMoney = new EsfMoney( lhDecimal, ((EsfMoney)targetValue).getCurrency() );
                        		context.transaction.setFieldValue(targetDocument.getEsfName(), spec.getTargetField(), targetMoney);
                    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set money field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() +"' to value: " + targetMoney + "; priorValue: " + targetValue.toString());            					
            				}
            				else
            				{
                    			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set " + targetValue.getType() + " field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' but left field is type '" + leftSideValue.getType() + "' instead of Integer, Decimal or Money.");            					
            				}
            			}
            			else if ( targetValue instanceof EsfDecimal )
            			{
            				if ( leftSideValue instanceof EsfInteger )
            				{
            					EsfDecimal lhDecimal = ((EsfInteger)leftSideValue).toEsfDecimal();
            					if ( spec.isOperatorAdd() )
            						lhDecimal.add(rightSideValue);
            					else if ( spec.isOperatorSubtract() )
            						lhDecimal.subtract(rightSideValue);
            					else if ( spec.isOperatorMultiply() )
            						lhDecimal.multiply(rightSideValue);
            					else if ( spec.isOperatorDivide() )
            						lhDecimal.divide(rightSideValue);
                        		context.transaction.setFieldValue(targetDocument.getEsfName(), spec.getTargetField(), lhDecimal);
                    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set decimal field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() +"' to value: " + lhDecimal + "; priorValue: " + targetValue.toString());            					
            				}
            				else if ( leftSideValue instanceof EsfDecimal )
            				{
            					EsfDecimal lhDecimal = new EsfDecimal( (EsfDecimal)leftSideValue );
            					if ( spec.isOperatorAdd() )
            						lhDecimal.add(rightSideValue);
            					else if ( spec.isOperatorSubtract() )
            						lhDecimal.subtract(rightSideValue);
            					else if ( spec.isOperatorMultiply() )
            						lhDecimal.multiply(rightSideValue);
            					else if ( spec.isOperatorDivide() )
            						lhDecimal.divide(rightSideValue);
                        		context.transaction.setFieldValue(targetDocument.getEsfName(), spec.getTargetField(), lhDecimal);
                    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set decimal field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() +"' to value: " + lhDecimal + "; priorValue: " + targetValue.toString());            					
            				}
            				else
            				{
                    			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set " + targetValue.getType() + " field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' but left field is type '" + leftSideValue.getType() + "' instead of Integer, Decimal or Money.");            					
            				}
            			}
            			else
            			{
                			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' but target field is type '" + targetValue.getType() + "' instead of Integer, Decimal or Money.");
            			}
            		}
        		}
        	}
    	}
    }
    
    @Override
    public String specsToString()
    {
    	StringBuilder buf = new StringBuilder(specList.size() * 80);
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(",");
    		buf.append(spec.targetField).append("=").append(spec.leftSideField).append(spec.operator).append(spec.rightSideField);
     	}
    	return buf.toString();
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 50;
		for( Spec spec : specList )
			size += 180 + spec.targetDocumentId.getEstimatedLengthXml() + spec.targetField.getEstimatedLengthXml() + 
					spec.leftSideDocumentId.getEstimatedLengthXml() + spec.leftSideField.getEstimatedLengthXml() + 
					spec.rightSideDocumentId.getEstimatedLengthXml() + spec.rightSideField.getEstimatedLengthXml() + 
					spec.operator.length();
		if ( hasCondition() )
			size += condition.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Action type=\"CalculateValueAction\">\n");
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <targetDocumentId>").append(spec.targetDocumentId.toXml()).append("</targetDocumentId>\n");
			buf.append("  <targetField>").append(spec.targetField.toXml()).append("</targetField>\n");
			buf.append("  <leftSideDocumentId>").append(spec.leftSideDocumentId.toXml()).append("</leftSideDocumentId>\n");
			buf.append("  <leftSideField>").append(spec.leftSideField.toXml()).append("</leftSideField>\n");
			buf.append("  <rightSideDocumentId>").append(spec.rightSideDocumentId.toXml()).append("</rightSideDocumentId>\n");
			buf.append("  <rightSideField>").append(spec.rightSideField.toXml()).append("</rightSideField>\n");
			buf.append("  <operator>").append(XmlUtil.toEscapedXml(spec.operator)).append("</operator>\n");
			buf.append(" </Spec>\n");
		}
		super.appendXml(buf);
		buf.append("</Action>\n");
		return buf;
	}
   
}