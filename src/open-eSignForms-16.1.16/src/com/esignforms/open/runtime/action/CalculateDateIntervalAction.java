// Copyright (C) 2015-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionDocument.INITIALIZE_RECORD;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.XmlUtil;

/**
* Action is the base class of all actions used by the transaction engine.
* CalculateDateInveralAction is used to set the a number field to the number of days/years that have passed
* between two dates.
* 
* @author Yozons, Inc.
*/
public class CalculateDateIntervalAction extends Action
{
	private static final long serialVersionUID = 4827729552228159352L;

	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = 2791142764215210617L;

		public Spec(int o, EsfUUID targetDocumentId, EsfName targetField, String interval,
				EsfUUID fromDocumentId, EsfName fromField, boolean useFromFieldSpec, String fromFieldSpec, 
				EsfUUID toDocumentId, EsfName toField, boolean useToFieldSpec, String toFieldSpec)
		{
			order = o;
			this.targetDocumentId = targetDocumentId;
			this.targetField = targetField;
			this.interval = interval;
			
			this.fromDocumentId = fromDocumentId;
			this.fromField = fromField;
			this.useFromFieldSpec = useFromFieldSpec;
			this.fromFieldSpec = fromFieldSpec;
			
			this.toDocumentId = toDocumentId;
			this.toField = toField;
			this.useToFieldSpec = useToFieldSpec;
			this.toFieldSpec = toFieldSpec;
		}
		int order;
		EsfUUID targetDocumentId;
		EsfName targetField;
		String interval;
		
		EsfUUID fromDocumentId; // optional (required if useFromFieldSpec==false)
		EsfName fromField; // optional (required if useFromFieldSpec==false)
		boolean useFromFieldSpec;
		String fromFieldSpec; // optional (required if useFromFieldSpec==true)
		
		EsfUUID toDocumentId; // optional (required if useToFieldSpec==false)
		EsfName toField; // optional (required if useToFieldSpec==false)
		boolean useToFieldSpec;
		String toFieldSpec; //optional (required if useToFieldSpec==true)
		
		public Spec duplicate() 
		{
			return new Spec(order,targetDocumentId,targetField.duplicate(),interval,
					        fromDocumentId,fromField!=null ? fromField.duplicate() : null,useFromFieldSpec,fromFieldSpec,
					        toDocumentId,toField!=null ? toField.duplicate() : null,useToFieldSpec,toFieldSpec);
		}
		public int getOrder() { return order; }
		public void setOrder(int v) { order = v; }
		
		public EsfUUID getTargetDocumentId() { return targetDocumentId; }
		public void setTargetDocumentId(EsfUUID v) { targetDocumentId = v; }
		public EsfName getTargetField() { return targetField; }
		public void setTargetField(EsfName v) { targetField = v; }
		
		public String getInterval() { return interval; }
		public void setInterval(String v) { interval = v; if ( ! isIntervalDays() && ! isIntervalYears() ) interval = "year"; }
		public boolean isIntervalDays() { return "day".equals(interval); }
		public boolean isIntervalYears() { return "year".equals(interval); }
		
		public EsfUUID getFromDocumentId() { return fromDocumentId; }
		public void setFromDocumentId(EsfUUID v) { fromDocumentId = v; }
		public EsfName getFromField() { return fromField; }
		public void setFromField(EsfName v) { fromField = v; }
		public boolean isUseFromFieldSpec() { return useFromFieldSpec; }
		public void setUseFromFieldSpec(boolean v) { useFromFieldSpec = v; }
		public String getFromFieldSpec() { return fromFieldSpec; }
		public void setFromFieldSpec(String v) { fromFieldSpec = v; }
		public String getFromFieldDisplay() {
			if ( isUseFromFieldSpec() )
				return getFromFieldSpec();
			Document d = Document.Manager.getById(fromDocumentId);
			return d == null ? "?." + getFromField() : d.getEsfName() + "." + getFromField();
		}
		
		public EsfUUID getToDocumentId() { return toDocumentId; }
		public void setToDocumentId(EsfUUID v) { toDocumentId = v; }
		public EsfName getToField() { return toField; }
		public void setToField(EsfName v) { toField = v; }
		public boolean isUseToFieldSpec() { return useToFieldSpec; }
		public void setUseToFieldSpec(boolean v) { useToFieldSpec = v; }
		public String getToFieldSpec() { return toFieldSpec; }
		public void setToFieldSpec(String v) { toFieldSpec = v; }
		public String getToFieldDisplay() {
			if ( isUseToFieldSpec() )
				return getToFieldSpec();
			Document d = Document.Manager.getById(toDocumentId);
			return d == null ? "?." + getToField() : d.getEsfName() + "." + getToField();
		}
	}
	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates an CalculateDateIntervalAction object
     */
    public CalculateDateIntervalAction()
    {
    }
    
    public CalculateDateIntervalAction(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		super(element,ns,_logger);
		try 
		{
			String elementName = element.getName();
			if (!"Action".equals(elementName)) 
				throw new EsfException("The Action tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!"CalculateDateIntervalAction".equals(typeAttr)) 
				throw new EsfException("The Action tag has type: " + typeAttr + "; expected CalculateDateIntervalAction.");

			List<Element> specElementList = element.getChildren("Spec", ns);
			ListIterator<Element> specIter = specElementList.listIterator();
			int order = 1;
			while( specIter.hasNext() ) 
			{
				Element specElement = specIter.next();
				
				EsfUUID targetDocumentId = new EsfUUID(specElement.getChildTextTrim("targetDocumentId", ns)); 
				EsfName targetField = new EsfName(specElement.getChildTextTrim("targetField", ns)); 
				
				String interval = specElement.getChildTextTrim("interval", ns);
				
				EsfUUID fromDocumentId = new EsfUUID(specElement.getChildTextTrim("fromDocumentId", ns)); 
				EsfName fromField = new EsfName(specElement.getChildTextTrim("fromField", ns));
				boolean useFromFieldSpec = EsfBoolean.toBoolean(specElement.getChildTextTrim("useFromFieldSpec", ns));
				String fromFieldSpec = specElement.getChildTextTrim("fromFieldSpec", ns);
				
				EsfUUID toDocumentId = new EsfUUID(specElement.getChildTextTrim("toDocumentId", ns)); 
				EsfName toField = new EsfName(specElement.getChildTextTrim("toField", ns)); 
				boolean useToFieldSpec = EsfBoolean.toBoolean(specElement.getChildTextTrim("useToFieldSpec", ns));
				String toFieldSpec = specElement.getChildTextTrim("toFieldSpec", ns);

				Spec spec = new Spec(order++, targetDocumentId, targetField, interval,
									fromDocumentId, fromField, useFromFieldSpec, fromFieldSpec,
									toDocumentId, toField, useToFieldSpec, toFieldSpec);
				specList.add(spec);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }
    
    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }
    
    public CalculateDateIntervalAction duplicate()
    {
    	CalculateDateIntervalAction action = new CalculateDateIntervalAction();
    	for( Spec spec : specList )
    		action.specList.add( spec.duplicate() );
    	if ( hasCondition() )
    		action.condition = condition.duplicate();
    	return action;
    }
    
    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	super.updatePackagePartyIds(partyIdMapping);
    	// we have no party id mappings
    }
    
    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	for( Spec spec : specList )
    	{
    		EsfUUID newDocId = documentIdMapping.get(spec.getTargetDocumentId());
    		if ( newDocId != null )
    			spec.setTargetDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getFromDocumentId());
    		if ( newDocId != null )
    			spec.setFromDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getToDocumentId());
    		if ( newDocId != null )
    			spec.setToDocumentId(newDocId);
    	}
    }
    
    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }

    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	if ( specList.size() == 0 )
    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' had no fields specified to be set.");
    	else
    	{
        	for( Spec spec : specList ) 
        	{
        		Document targetDocument = context.getDocument(spec.getTargetDocumentId());
        		Document fromDocument = spec.isUseFromFieldSpec() ? null : context.getDocument(spec.getFromDocumentId());
        		Document toDocument = spec.isUseToFieldSpec() ? null : context.getDocument(spec.getToDocumentId());
        		if ( targetDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document id '" + spec.getTargetDocumentId() + "' but no such document was found.");
        		else if ( ! spec.isUseFromFieldSpec() && fromDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document id '" + spec.getTargetDocumentId() + "' with from document id '" + spec.getFromDocumentId() + "' but no such from document was found.");
        		else if ( ! spec.isUseToFieldSpec() && toDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document id '" + spec.getTargetDocumentId() + "' with to document id '" + spec.getToDocumentId() + "' but no such to document was found.");
        		else
        		{
            		EsfValue targetValue = context.transaction.getFieldValue(targetDocument.getEsfName(), spec.getTargetField());
            		EsfValue fromValue; 
            		String expandedFromValueFieldSpec = null;
            		EsfValue toValue; 
            		String expandedToValueFieldSpec = null;
            		
            		if ( spec.isUseFromFieldSpec() )
            		{
            			expandedFromValueFieldSpec = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, spec.fromFieldSpec);
						fromValue = EsfDate.CreateGuessFormat(expandedFromValueFieldSpec); // should be in yyyy-mm-dd format, or at least dd-Mon-yyyy or mm/dd/yyyy format, or our special cases: now or +numDays or -numDays
            		}
            		else
            			fromValue = context.transaction.getFieldValue(fromDocument.getEsfName(), spec.getFromField());
            		
            		if ( spec.isUseToFieldSpec() )
            		{
            			expandedToValueFieldSpec = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, spec.toFieldSpec);
						toValue = EsfDate.CreateGuessFormat(expandedToValueFieldSpec); // should be in yyyy-mm-dd format, or at least dd-Mon-yyyy or mm/dd/yyyy format, or our special cases: now or +numDays or -numDays
            		}
            		else
            			toValue = context.transaction.getFieldValue(toDocument.getEsfName(), spec.getToField());

            		// If there's no value to set on our first look, let's try to re-initialize the record to create any missing fields 
            		// Generally these are fields that were added into a document after the transaction has already been started.
            		if ( targetValue == null ) 
            		{
            			context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() +"' but it was not found; trying to create missing/newly-added fields.");
            			TransactionDocument retryTranDoc = context.transaction.getTransactionDocument(targetDocument.getEsfName());
        				DocumentVersion retryDocVer = context.transaction.getDocumentVersion(targetDocument);
            			if ( retryTranDoc != null && retryDocVer != null )
            			{
            				retryTranDoc.initializeRecord(context.user, context.transaction, targetDocument, retryDocVer, INITIALIZE_RECORD.ONLY_MISSING_FIELDS);
            				targetValue = context.transaction.getFieldValue(targetDocument.getEsfName(), spec.getTargetField());
            			}
            		}
            		if ( targetValue == null )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' but no such target field was found.");
          			else if ( ! (targetValue instanceof EsfInteger) )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' but to field is type '" + targetValue.getType() + "' rather than an Integer.");            					
            		else if ( fromValue == null || fromValue.isNull() )
            		{
            			if ( spec.isUseFromFieldSpec() )
                			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' with from date field spec '" + spec.getFromFieldSpec() + "' but it could not create a date after expanded to: " + expandedFromValueFieldSpec);
            			else
            				context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' with from date field '" + spec.getFromField() + "' in document '" + fromDocument.getEsfName() + "' but no such from date field value was found.");
            		}
            		else if ( toValue == null || toValue.isNull() )
            		{
            			if ( spec.isUseToFieldSpec() )
                			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' with to date field spec '" + spec.getToFieldSpec() + "' but it could not create a date after expanded to: " + expandedToValueFieldSpec);
            			else
            				context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' with to date field '" + spec.getToField() + "' in document '" + toDocument.getEsfName() + "' but no such to date field value was found.");
            		}
            		else
            		{
            			if ( fromValue instanceof EsfDateTime )
            				fromValue = new EsfDate((EsfDateTime)fromValue);
            			if ( toValue instanceof EsfDateTime )
            				toValue = new EsfDate((EsfDateTime)toValue);
            			
            			if ( ! (fromValue instanceof EsfDate) )
                			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set integer field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' but from field value is type '" + fromValue.getType() + "' rqther than a Date or DateTime.");            					
            			else if ( ! (toValue instanceof EsfDate) )
                			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set integer field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' but to field value is type '" + toValue.getType() + "' rather than a Date or DateTime.");            					
            			else
            			{
            				EsfDate fromDateValue = (EsfDate)fromValue;
            				EsfDate toDateValue = (EsfDate)toValue;
            				
                			int interval = spec.isIntervalDays() ? fromDateValue.daysUntil(toDateValue) : fromDateValue.getYearsUntil(toDateValue);
                    		context.transaction.setFieldValue(targetDocument.getEsfName(), spec.getTargetField(), new EsfInteger(interval));
                			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set Integer field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() +"' to date interval value: " + interval + "; priorValue: " + targetValue.toString());
            			}
            		}            			
        		}
        	}
    	}
    }
    
    @Override
    public String specsToString()
    {
    	StringBuilder buf = new StringBuilder(specList.size() * 80);
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(",");
    		String fromField = spec.useFromFieldSpec ? spec.fromFieldSpec : spec.fromField.toString();
    		String toField = spec.useToFieldSpec ? spec.toFieldSpec : spec.toField.toString();
    		buf.append(spec.targetField).append("=INTERVAL(").append(fromField).append(",").append(toField).append(",").append(spec.interval).append(")");
     	}
    	return buf.toString();
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 60;
		for( Spec spec : specList )
		{
			size += 350 + spec.targetDocumentId.getEstimatedLengthXml() + spec.targetField.getEstimatedLengthXml() + spec.interval.length();
			if ( spec.fromDocumentId != null )
				size += spec.fromDocumentId.getEstimatedLengthXml();
			if ( spec.fromField != null )
				size += spec.fromField.getEstimatedLengthXml();
			if ( spec.fromFieldSpec != null )
				size += spec.fromFieldSpec.length();
			if ( spec.toDocumentId != null )
				size += spec.toDocumentId.getEstimatedLengthXml();
			if ( spec.toField != null )
				size += spec.toField.getEstimatedLengthXml();
			if ( spec.toFieldSpec != null )
				size += spec.toFieldSpec.length();
		}
		if ( hasCondition() )
			size += condition.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Action type=\"CalculateDateIntervalAction\">\n");
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <targetDocumentId>").append(spec.targetDocumentId.toXml()).append("</targetDocumentId>\n");
			buf.append("  <targetField>").append(spec.targetField.toXml()).append("</targetField>\n");
			buf.append("  <interval>").append(XmlUtil.toEscapedXml(spec.interval)).append("</interval>\n");
			buf.append("  <fromDocumentId>").append(spec.fromDocumentId != null ? spec.fromDocumentId.toXml() : "").append("</fromDocumentId>\n");
			buf.append("  <fromField>").append(spec.fromField != null ? spec.fromField.toXml() : "").append("</fromField>\n");
			buf.append("  <useFromFieldSpec>").append(spec.useFromFieldSpec).append("</useFromFieldSpec>\n");
			buf.append("  <fromFieldSpec>").append(spec.fromFieldSpec != null ? spec.fromFieldSpec : "").append("</fromFieldSpec>\n");
			buf.append("  <toDocumentId>").append(spec.toDocumentId != null ? spec.toDocumentId.toXml() : "").append("</toDocumentId>\n");
			buf.append("  <toField>").append(spec.toField != null ? spec.toField.toXml() : "").append("</toField>\n");
			buf.append("  <useToFieldSpec>").append(spec.useToFieldSpec).append("</useToFieldSpec>\n");
			buf.append("  <toFieldSpec>").append(spec.toFieldSpec != null ? spec.toFieldSpec : "").append("</toFieldSpec>\n");
			buf.append(" </Spec>\n");
		}
		super.appendXml(buf);
		buf.append("</Action>\n");
		return buf;
	}
   
}