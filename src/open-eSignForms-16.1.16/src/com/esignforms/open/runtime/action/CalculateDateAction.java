// Copyright (C) 2014-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionDocument.INITIALIZE_RECORD;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.XmlUtil;

/**
* Action is the base class of all actions used by the transaction engine.
* CalculateDateAction is used to set the specified date field in the specified document to the specified other date value plus/minus an offset
* with various units (day,weekday,month,year for Date and DateTime, plus minute and hour for DateTime).
* 
* @author Yozons, Inc.
*/
public class CalculateDateAction extends Action
{
	private static final long serialVersionUID = 1487768231887025276L;

	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = -4484296959567352592L;

		public Spec(int o, EsfUUID targetDocumentId, EsfName targetField, 
				EsfUUID leftSideDocumentId, EsfName leftSideField, String numOffsetUnits, String offsetUnit)
		{
			order = o;
			this.targetDocumentId = targetDocumentId;
			this.targetField = targetField;
			this.leftSideDocumentId = leftSideDocumentId;
			this.leftSideField = leftSideField;
			this.numOffsetUnits = numOffsetUnits;
			this.offsetUnit = offsetUnit;
		}
		int order;
		EsfUUID targetDocumentId;
		EsfName targetField;
		EsfUUID leftSideDocumentId;
		EsfName leftSideField;
		String numOffsetUnits; // actually an integer value
		String offsetUnit;
		public Spec duplicate() 
		{
			return new Spec(order,targetDocumentId,targetField.duplicate(),
					        leftSideDocumentId,leftSideField.duplicate(),numOffsetUnits,offsetUnit);
		}
		public int getOrder() { return order; }
		public void setOrder(int v) { order = v; }
		
		public EsfUUID getTargetDocumentId() { return targetDocumentId; }
		public void setTargetDocumentId(EsfUUID v) { targetDocumentId = v; }
		public EsfName getTargetField() { return targetField; }
		public void setTargetField(EsfName v) { targetField = v; }
		
		public EsfUUID getLeftSideDocumentId() { return leftSideDocumentId; }
		public void setLeftSideDocumentId(EsfUUID v) { leftSideDocumentId = v; }
		public EsfName getLeftSideField() { return leftSideField; }
		public void setLeftSideField(EsfName v) { leftSideField = v; }
		
		public int numOffsetUnitsAsInt() { return Application.getInstance().stringToInt(numOffsetUnits,0); } // not using javabeans 'get' format on purpose since numOffsetUnits is the bean attribute
		public String getNumOffsetUnits() { return numOffsetUnits; }
		public boolean hasNumOffsetUnits() { return numOffsetUnitsAsInt() != 0; }
		public void setNumOffsetUnits(String v) { numOffsetUnits = EsfString.isBlank(v) ? "1" : v; }
		
		public String getOffsetUnit() { return offsetUnit; }
		public void setOffsetUnit(String v) { offsetUnit = v.trim(); }
	}
	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates an CalculateDateAction object
     */
    public CalculateDateAction()
    {
    }
    
    public CalculateDateAction(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		super(element,ns,_logger);
		try 
		{
			String elementName = element.getName();
			if (!"Action".equals(elementName)) 
				throw new EsfException("The Action tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!"CalculateDateAction".equals(typeAttr)) 
				throw new EsfException("The Action tag has type: " + typeAttr + "; expected CalculateDateAction.");

			List<Element> specElementList = element.getChildren("Spec", ns);
			ListIterator<Element> specIter = specElementList.listIterator();
			int order = 1;
			while( specIter.hasNext() ) 
			{
				Element specElement = specIter.next();
				
				EsfUUID targetDocumentId = new EsfUUID(specElement.getChildTextTrim("targetDocumentId", ns)); 
				EsfName targetField = new EsfName(specElement.getChildTextTrim("targetField", ns)); 
				
				EsfUUID leftSideDocumentId = new EsfUUID(specElement.getChildTextTrim("leftSideDocumentId", ns)); 
				EsfName leftSideField = new EsfName(specElement.getChildTextTrim("leftSideField", ns)); 
				
				Spec spec = new Spec(order++, targetDocumentId, targetField, 
						leftSideDocumentId, leftSideField, specElement.getChildTextTrim("numOffsetUnits", ns), specElement.getChildTextTrim("offsetUnit", ns));
				specList.add(spec);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }
    
    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }
    
    public CalculateDateAction duplicate()
    {
    	CalculateDateAction action = new CalculateDateAction();
    	for( Spec spec : specList )
    		action.specList.add( spec.duplicate() );
    	if ( hasCondition() )
    		action.condition = condition.duplicate();
    	return action;
    }
    
    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	super.updatePackagePartyIds(partyIdMapping);
    	// we have no party id mappings
    }
    
    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	for( Spec spec : specList )
    	{
    		EsfUUID newDocId = documentIdMapping.get(spec.getTargetDocumentId());
    		if ( newDocId != null )
    			spec.setTargetDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getLeftSideDocumentId());
    		if ( newDocId != null )
    			spec.setLeftSideDocumentId(newDocId);
    	}
    }
    
    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }

    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	if ( specList.size() == 0 )
    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' had no fields specified to be set.");
    	else
    	{
        	for( Spec spec : specList ) 
        	{
        		Document targetDocument = context.getDocument(spec.getTargetDocumentId());
        		Document leftSideDocument = context.getDocument(spec.getLeftSideDocumentId());
        		if ( targetDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document id '" + spec.getTargetDocumentId() + "' but no such document was found.");
        		else if ( leftSideDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document id '" + spec.getTargetDocumentId() + "' with left side document id '" + spec.getLeftSideDocumentId() + "' but no such left side document was found.");
        		else
        		{
            		EsfValue targetValue = context.transaction.getFieldValue(targetDocument.getEsfName(), spec.getTargetField());
            		EsfValue leftSideValue = context.transaction.getFieldValue(leftSideDocument.getEsfName(), spec.getLeftSideField());

            		// If there's no value to set on our first look, let's try to re-initialize the record to create any missing fields 
            		// Generally these are fields that were added into a document after the transaction has already been started.
            		if ( targetValue == null ) 
            		{
            			context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() +"' but it was not found; trying to create missing/newly-added fields.");
            			TransactionDocument retryTranDoc = context.transaction.getTransactionDocument(targetDocument.getEsfName());
        				DocumentVersion retryDocVer = context.transaction.getDocumentVersion(targetDocument);
            			if ( retryTranDoc != null && retryDocVer != null )
            			{
            				retryTranDoc.initializeRecord(context.user, context.transaction, targetDocument, retryDocVer, INITIALIZE_RECORD.ONLY_MISSING_FIELDS);
            				targetValue = context.transaction.getFieldValue(targetDocument.getEsfName(), spec.getTargetField());
            			}
            		}
            		if ( targetValue == null )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' but no such target field was found.");
            		else if ( leftSideValue == null )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' with left side field '" + spec.getLeftSideField() + "' in document '" + leftSideDocument.getEsfName() + "' but no such left side field was found.");
            		else if ( targetValue instanceof EsfDate )
            		{
        				EsfDate lhDate = null;
            			if ( leftSideValue instanceof EsfDate )
            			{
            				EsfDate leftSideDate = (EsfDate)leftSideValue;
            				if ( ! leftSideDate.isNull() )
            				{
                				lhDate = leftSideDate.duplicate();
                				int numOffsetUnits = spec.numOffsetUnitsAsInt();
                				if ( numOffsetUnits != 0 )
                					lhDate.addDateInterval(numOffsetUnits,spec.offsetUnit);
            				}
            			}
            			else if ( leftSideValue instanceof EsfDateTime )
            			{
            				EsfDateTime leftSideDateTime = (EsfDateTime)leftSideValue;
            				if ( ! leftSideDateTime.isNull() )
            				{
                				lhDate = new EsfDate( leftSideDateTime.toDate() );
                				int numOffsetUnits = spec.numOffsetUnitsAsInt();
                				if ( numOffsetUnits != 0 )
                					lhDate.addDateInterval(numOffsetUnits,spec.offsetUnit);
            				}
            			}
            			else
            			{
                			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set Date field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' but left field is type '" + leftSideValue.getType() + "' instead of Date or DateTime.");            					
            			}
            			if ( lhDate != null )
            			{
                    		context.transaction.setFieldValue(targetDocument.getEsfName(), spec.getTargetField(), lhDate);
                			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set Date field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() +"' to value: " + lhDate + "; priorValue: " + targetValue.toString());
            			}
            		}
            		else if ( targetValue instanceof EsfDateTime )
            		{
        				EsfDateTime lhDateTime = null;
            			if ( leftSideValue instanceof EsfDate )
            			{
            				EsfDate leftSideDate = (EsfDate)leftSideValue;
            				if ( ! leftSideDate.isNull() )
            				{
                				lhDateTime = new EsfDateTime( leftSideDate.toDate() );
                				int numOffsetUnits = spec.numOffsetUnitsAsInt();
                				if ( numOffsetUnits != 0 )
                					lhDateTime.addTimeInterval(numOffsetUnits,spec.offsetUnit);
            				}
            			}
            			else if ( leftSideValue instanceof EsfDateTime )
            			{
            				EsfDateTime leftSideDateTime = (EsfDateTime)leftSideValue;
            				if ( ! leftSideDateTime.isNull() )
            				{
                				lhDateTime = leftSideDateTime.duplicate();
                				int numOffsetUnits = spec.numOffsetUnitsAsInt();
                				if ( numOffsetUnits != 0 )
                					lhDateTime.addTimeInterval(numOffsetUnits,spec.offsetUnit);
            				}
            			}
            			else
            			{
                			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set DateTime field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' but left field is type '" + leftSideValue.getType() + "' instead of Date or DateTime.");            					
            			}
            			if ( lhDateTime != null )
            			{
                    		context.transaction.setFieldValue(targetDocument.getEsfName(), spec.getTargetField(), lhDateTime);
                			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set DateTime field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() +"' to value: " + lhDateTime + "; priorValue: " + targetValue.toString());
            			}
            		}
            		else
            		{
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to set field '" + spec.getTargetField() + "' in document '" + targetDocument.getEsfName() + "' but target field is type '" + targetValue.getType() + "' instead of Date or DateTime.");
            		}
        		}
        	}
    	}
    }
    
    @Override
    public String specsToString()
    {
    	StringBuilder buf = new StringBuilder(specList.size() * 80);
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(",");
    		buf.append(spec.targetField).append("=").append(spec.leftSideField).append(" ").append(spec.numOffsetUnits).append(" ").append(spec.offsetUnit);
     	}
    	return buf.toString();
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 50;
		for( Spec spec : specList )
			size += 150 + spec.targetDocumentId.getEstimatedLengthXml() + spec.targetField.getEstimatedLengthXml() + 
					spec.leftSideDocumentId.getEstimatedLengthXml() + spec.leftSideField.getEstimatedLengthXml() + 
					spec.numOffsetUnits.length() + spec.offsetUnit.length();
		if ( hasCondition() )
			size += condition.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Action type=\"CalculateDateAction\">\n");
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <targetDocumentId>").append(spec.targetDocumentId.toXml()).append("</targetDocumentId>\n");
			buf.append("  <targetField>").append(spec.targetField.toXml()).append("</targetField>\n");
			buf.append("  <leftSideDocumentId>").append(spec.leftSideDocumentId.toXml()).append("</leftSideDocumentId>\n");
			buf.append("  <leftSideField>").append(spec.leftSideField.toXml()).append("</leftSideField>\n");
			buf.append("  <numOffsetUnits>").append(XmlUtil.toEscapedXml(spec.numOffsetUnits)).append("</numOffsetUnits>\n");
			buf.append("  <offsetUnit>").append(XmlUtil.toEscapedXml(spec.offsetUnit)).append("</offsetUnit>\n");
			buf.append(" </Spec>\n");
		}
		super.appendXml(buf);
		buf.append("</Action>\n");
		return buf;
	}
   
}