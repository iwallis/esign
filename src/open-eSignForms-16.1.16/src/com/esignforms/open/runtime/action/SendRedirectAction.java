// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.XmlUtil;

/**
* Action is the base class of all actions used by the transaction engine.
* SendRedirectAction is used to set the specified URL to redirect to.
* It only supports a single spec since we can only redirect to one URL.
* 
* @author Yozons, Inc.
*/
public class SendRedirectAction extends Action
{
	private static final long serialVersionUID = 5276420338135856312L;

	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = 7264424313955493468L;

		public Spec(String v)
		{
			this.urlSpec = EsfString.ensureNotNull(v);
		}
		String urlSpec;
		public Spec duplicate() 
		{
			return new Spec(urlSpec);
		}
		public String getUrlSpec() { return urlSpec; }
		public void setUrlSpec(String v) { urlSpec = v.trim(); }
	}
	Spec spec = null;
	
    /**
     * This creates an SendRedirectAction object
     */
    public SendRedirectAction()
    {
    }
    
    public SendRedirectAction(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		super(element,ns,_logger);
		try 
		{
			String elementName = element.getName();
			if (!"Action".equals(elementName)) 
				throw new EsfException("The Action tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!"SendRedirectAction".equals(typeAttr)) 
				throw new EsfException("The Action tag has type: " + typeAttr + "; expected SendRedirectAction.");

			List<Element> specElementList = element.getChildren("Spec", ns);
			ListIterator<Element> iter = specElementList.listIterator();
			if (iter.hasNext()) 
			{
				Element specElement = iter.next();
				
				Spec spec = new Spec(specElement.getChildTextTrim("urlSpec", ns));
				this.spec = spec;
			}
			else
				throw new EsfException("The Action tag has type: " + typeAttr + "; is missing a required Spec.");
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }
    
    public Spec getSpec()
    {
    	return spec;
    }
    public void setSpec(Spec v)
    {
    	spec = v;
    	setObjectChanged();
    }
    
    public SendRedirectAction duplicate()
    {
    	SendRedirectAction action = new SendRedirectAction();
    	action.spec = spec.duplicate();
    	if ( hasCondition() )
    		action.condition = condition.duplicate();
    	return action;
    }
    
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	super.updatePackagePartyIds(partyIdMapping);
    	// we have no party id mappings
    }

    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	// we have no document id mappings
    }

    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }

    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	if ( spec == null )
    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' had no URL specification to redirect to.");
    	else
    	{
			String url = context.transaction.getUrlEncodedFieldSpecWithSubstitutions(context.user, context.currDocumentVersion,spec.getUrlSpec());
			context.transaction.logActionBasic(con, "Action '" + getEsfName() + "' redirecting to URL: " + url);
			context.redirectToUrl = url;
		}
    }
    
    @Override
    public String specsToString()
    {
    	return spec.urlSpec == null ? "(no URL set)" : spec.urlSpec;
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 50;
		size += 40 + spec.urlSpec.length();
		if ( hasCondition() )
			size += condition.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Action type=\"SendRedirectAction\">\n");
		buf.append(" <Spec>\n");
		buf.append("  <urlSpec>").append(XmlUtil.toEscapedXml(spec.urlSpec)).append("</urlSpec>\n");
		buf.append(" </Spec>\n");
		super.appendXml(buf);
		buf.append("</Action>\n");
		return buf;
	}
   
}