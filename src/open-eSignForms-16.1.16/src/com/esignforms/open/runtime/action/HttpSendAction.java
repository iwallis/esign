// Copyright (C) 2013-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.StringTokenizer;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.integration.httpsend.HttpSendRequest;
import com.esignforms.open.integration.wkhtmltopdf.HtmlToPdf;
import com.esignforms.open.jsp.libdocsgen.taglib.FieldLabel;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentIdPartyId;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionFile;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.Base64;
import com.esignforms.open.util.ServletUtil;
import com.esignforms.open.util.XmlUtil;

/**
* Action is the base class of all actions used by the transaction engine.
* HttpSendAction is used to send an HTTP(S) GET/POST.
* 
* @author Yozons, Inc.
*/
public class HttpSendAction extends Action
{
	private static final long serialVersionUID = 6443620615269041781L;

	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = -8703149857681812502L;

		int order;
		String requestMethod;
		String url;
		int maxAttempts;
		int retryDelayMinutes;
		String reasonSpec;
		String limitSuccessfulHttpStatusCodes; // when set, it's a comma-separated list of http status codes that must be returned to be successful (if blank, not status code check is done)
		String responseMatchType;
		String responseMatchValue;
		List<FieldSpec> fieldSpecs;
		
		public Spec(int o, String requestMethod, String url, int maxAttempts, int retryDelayMinutes, 
				    String reasonSpec, String limitSuccessfulHttpStatusCodes, String responseMatchType, String responseMatchValue,
				    List<FieldSpec> fieldSpecs)
		{
			order = o;
			this.requestMethod = requestMethod;
			this.url = url; 
			this.maxAttempts = maxAttempts;
			this.retryDelayMinutes = retryDelayMinutes;
			this.reasonSpec = reasonSpec;
			this.limitSuccessfulHttpStatusCodes = limitSuccessfulHttpStatusCodes;
			setResponseMatchType(responseMatchType);
			this.responseMatchValue = responseMatchValue;
			this.fieldSpecs = fieldSpecs == null ? new LinkedList<FieldSpec>() : fieldSpecs;
		}
		
		public Spec duplicate() 
		{
			List<FieldSpec> duplicatedFieldSpecs = new LinkedList<FieldSpec>();
			
			for( FieldSpec fs : fieldSpecs )
				duplicatedFieldSpecs.add( fs.duplicate() );
			
			return new Spec(order,requestMethod,url,maxAttempts,retryDelayMinutes,reasonSpec,
							limitSuccessfulHttpStatusCodes,responseMatchType,responseMatchValue,
							duplicatedFieldSpecs);
		}
		
		public int getOrder() { return order; }
		public void setOrder(int v) { order = v; }
		
		public String getRequestMethod() { return requestMethod; }
		public void setRequestMethod(String v) { requestMethod = v; }

		public String getUrl() { return url; }
		public void setUrl(String v) { url = v; }
		
		public int getMaxAttempts() { return maxAttempts; }
		public void setMaxAttempts(int v) { maxAttempts = v < 1 ? 1 : v; }

		public int getRetryDelayMinutes() { return retryDelayMinutes; }
		public void setRetryDelayMinutes(int v) { retryDelayMinutes = v < 1 ? 1 : v; }

		public String getReasonSpec() { return reasonSpec; }
		public void setReasonSpec(String v) { reasonSpec = v; }

		public String getLimitSuccessfulHttpStatusCodes() { return limitSuccessfulHttpStatusCodes; }
		public boolean hasLimitSucccessfulHttpStatusCodes() { return EsfString.isNonBlank(limitSuccessfulHttpStatusCodes); }
		public void setLimitSuccessfulHttpStatusCodes(String v) { 
			if ( EsfString.isBlank(v) )
				limitSuccessfulHttpStatusCodes = "";
			else {
				StringBuilder buf = new StringBuilder(v.length());
				StringTokenizer st = new StringTokenizer(v,",;/| "); // we'll normalize any of these separators to just a semicolon
				while( st.hasMoreTokens() ) {
					String c = st.nextToken();
					int code = Application.getInstance().stringToInt(c, 0);
					if (  buf.length() > 0 )
						buf.append(";");
					buf.append(code);
				}
				limitSuccessfulHttpStatusCodes = buf.toString();
			}
		}
		public int[] getLimitSuccessfulHttpStatusCodesArray() {
			if ( ! hasLimitSucccessfulHttpStatusCodes() )
				return new int[0];
			String[] a = limitSuccessfulHttpStatusCodes.split(";");
			int[] array = new int[a.length];
			for( int i=0; i < a.length; ++i )
				array[i] = Application.getInstance().stringToInt(a[i], 0);
			return array;
		}
		
		public String getResponseMatchType() { return responseMatchType; }
		public void setResponseMatchType(String v) { responseMatchType = EsfString.isBlank(v) ? HttpSendRequest.RESPONSE_MATCH_TYPE_ANY : v; }

		public String getResponseMatchValue() { return responseMatchValue; }
		public void setResponseMatchValue(String v) { responseMatchValue = v; }
		
		public List<FieldSpec> getFieldSpecs() { return fieldSpecs; }
		public void setFieldSpecs(List<FieldSpec> v) { fieldSpecs = v == null ? new LinkedList<FieldSpec>() : v; }
		
		public static class FieldSpec implements java.io.Serializable
		{
			private static final long serialVersionUID = 5292533558353361202L;

			public static final String COMPLETED_DOCUMENT_TYPE_HTML = "HTML";
			public static final String COMPLETED_DOCUMENT_TYPE_PDF = "PDF";
			public static final String COMPLETED_DOCUMENT_TYPE_XML_DATA = "XMLData";
			public static final String COMPLETED_DOCUMENT_TYPE_XML_SNAPSHOTS = "XMLSnapshots";

			public static final String TYPE_PARTY_LINK = "partyLink";
			public static final String TYPE_COMPLETED_DOCUMENT = "completedDocument";
			public static final String TYPE_DOCUMENT_FIELD = "docField";
			public static final String TYPE_LITERAL = "literal";
			
			int order;
			String type;
			String paramName;
			String outputFormat;

			// For TYPE_COMPLETED_DOCUMENT
			EsfUUID[] completedDocumentIds;
			String completedDocumentType; // the type of data represented by this completed document

			// Used by TYPE_COMPLETED_DOCUMENT as well as TYPE_PARTY_LINK
			EsfUUID[] completedDocumentPartyIds; // null entry means "latest" party
			String[] completedDocumentPartyNames; // for display purposes only

			// For TYPE_DOCUMENT_FIELD
			EsfUUID documentId;
			EsfName documentFieldName;
			
			// For TYPE_LITERAL
			String literal;
			
			public FieldSpec(int order, String type, String paramName, String outputFormat, // all
					         EsfUUID documentId, EsfName documentFieldName, // TYPE_DOCUMENT_FIELD
					         EsfUUID[] completedDocumentIds, EsfUUID[] completedDocumentPartyIds, String[] completedDocumentPartyNames, String completedDocumentType, // TYPE_COMPLETED_DOCUMENT
					         String literal) // TYPE_LITERAL
			{
				this.order = order;
				this.type = type;
				this.paramName = paramName;
				this.outputFormat = outputFormat;
				
				this.documentId = documentId;
				this.documentFieldName = documentFieldName;
				
				this.completedDocumentIds = completedDocumentIds;
				this.completedDocumentPartyIds = completedDocumentPartyIds;
				this.completedDocumentPartyNames = completedDocumentPartyNames;
				this.completedDocumentType = completedDocumentType;
				
				this.literal = literal;
			}
			
			public int getOrder() { return order; }
			public void setOrder(int v) { order = v; }
			
			public String getType() { return type; }
			public boolean isTypePartyLink() { return TYPE_PARTY_LINK.equals(type); }
			public boolean isTypeCompletedDocument() { return TYPE_COMPLETED_DOCUMENT.equals(type); }
			public boolean isTypeDocumentField() { return TYPE_DOCUMENT_FIELD.equals(type); }
			public boolean isTypeLiteral() { return TYPE_LITERAL.equals(type); }
			public void setType(String v) { type = v == null ? TYPE_LITERAL : v; }
			
			public String getParamName() { return paramName; }
			public void setParamName(String v) { paramName = v == null ? "" : v; }
			
			public String getOutputFormat() { return outputFormat; }
			public boolean hasOutputFormat() { return EsfString.isNonBlank(outputFormat); }
			public void setOutputFormat(String v) { outputFormat = v == null ? "" : v; };
			
			public EsfUUID getDocumentId() { return documentId; }
			public void setDocumentId(EsfUUID v) { documentId = v; }
			public EsfName getDocumentName() { 
				if ( documentId == null )
					return null;
				
				Document document = Document.Manager.getById(documentId);
				if ( document == null )
					return documentId.toEsfName();
				
				return document.getEsfName();
			}
			public EsfName getDocumentFieldName() { return documentFieldName; }
			public void setDocumentFieldName(EsfName v) { documentFieldName = v; }

			public EsfUUID[] getCompletedDocumentIds() { return completedDocumentIds; }
			public void setCompletedDocumentIds(EsfUUID[] v) { completedDocumentIds = v; }
			public EsfUUID[] getCompletedDocumentPartyIds() { return completedDocumentPartyIds; }
			public void setCompletedDocumentPartyIds(EsfUUID[] v) { completedDocumentPartyIds = v; }
			public String[] getCompletedDocumentPartyNames() { return completedDocumentPartyNames; }
			public void setCompletedDocumentPartyNames(String[] v) { completedDocumentPartyNames = v; }
			public String getCompletedDocumentType() { return completedDocumentType; }
			public boolean isCompletedDocumentTypeHtml() { return COMPLETED_DOCUMENT_TYPE_HTML.equals(completedDocumentType); }
			public boolean isCompletedDocumentTypePdf() { return COMPLETED_DOCUMENT_TYPE_PDF.equals(completedDocumentType); }
			public boolean isCompletedDocumentTypeXmlData() { return COMPLETED_DOCUMENT_TYPE_XML_DATA.equals(completedDocumentType); }
			public boolean isCompletedDocumentTypeXmlSnapshots() { return COMPLETED_DOCUMENT_TYPE_XML_SNAPSHOTS.equals(completedDocumentType); }
			public void setCompletedDocumentType(String v) { completedDocumentType = v; }
			
			public EsfUUID[] getPickupPartyIds() { return completedDocumentPartyIds; }
			public void setPickupPartyIds(EsfUUID[] v) { completedDocumentPartyIds = v; }
			public String[] getPickupPartyNames() { return completedDocumentPartyNames; }
			public void setPickupPartyNames(String[] v) { completedDocumentPartyNames = v; }
			public boolean isMatchingPickupPartyId(EsfUUID pid) 
			{ 
				if ( completedDocumentPartyIds == null ) 
					return false;
				for( int i=0; i < completedDocumentPartyIds.length; ++i )
				{
					if ( completedDocumentPartyIds[i].equals(pid) )
						return true;
				}
				return false;
			}

			public String getLiteral() { return literal; }
			public void setLiteral(String v) { literal = v == null ? "" : v; }
			
			public String getFieldSourceDisplay()
			{
				if ( isTypeLiteral() )
					return literal;
				
				if ( isTypeCompletedDocument() ) 
				{
					if ( completedDocumentIds == null || completedDocumentIds.length < 1 )
						return "???";
					
					StringBuilder buf = new StringBuilder(completedDocumentIds.length*100);
					buf.append(completedDocumentType).append(": ");
					for( int i=0; i < completedDocumentIds.length; ++i ) 
					{
						if ( i > 0 )
							buf.append(", ");
						Document document = Document.Manager.getById(completedDocumentIds[i]);
						buf.append(document.getEsfName()).append("(").append(completedDocumentPartyNames[i]).append(")");
					}
					return buf.toString();
				}
				
				if ( isTypePartyLink() ) 
				{
					if ( completedDocumentPartyIds == null || completedDocumentPartyIds.length < 1 )
						return "???";
					
					StringBuilder buf = new StringBuilder(completedDocumentPartyNames.length*Literals.ESFNAME_MAX_LENGTH);
					buf.append(Application.getInstance().getServerMessages().getString("party.pickup_link.label")).append(": ");
					for( int i=0; i < completedDocumentPartyNames.length; ++i ) 
					{
						if ( i > 0 )
							buf.append(", ");
						buf.append(completedDocumentPartyNames[i]);
					}
					return buf.toString();
				}
					
				// Must be document field type
				Document document = Document.Manager.getById(documentId);
				return (document == null ? "???" : document.getEsfName()) + "." + documentFieldName;
			}
			
			public FieldSpec duplicate() 
			{
				EsfUUID[] dupCompletedDocumentIds = null;
				EsfUUID[] dupCompletedDocumentPartyIds = null;
				String[] dupCompletedDocumentPartyNames = null;
				
				if ( isTypeCompletedDocument() ) 
				{
					if ( completedDocumentIds != null && completedDocumentIds.length > 0 ) 
					{
						dupCompletedDocumentIds = new EsfUUID[completedDocumentIds.length];
						dupCompletedDocumentPartyIds = new EsfUUID[completedDocumentPartyIds.length];
						dupCompletedDocumentPartyNames = new String[completedDocumentPartyNames.length];
						for( int i=0; i < completedDocumentIds.length; ++i ) 
						{
							dupCompletedDocumentIds[i] = completedDocumentIds[i].duplicate();
							if ( completedDocumentPartyIds[i] == null )
								dupCompletedDocumentPartyIds[i] = null;
							else
								dupCompletedDocumentPartyIds[i] = completedDocumentPartyIds[i].duplicate();
							dupCompletedDocumentPartyNames[i] = completedDocumentPartyNames[i];
						}
					}
				} 
				else if ( isTypePartyLink() ) 
				{
					if ( completedDocumentPartyIds != null && completedDocumentPartyIds.length > 0 ) 
					{
						dupCompletedDocumentPartyIds = new EsfUUID[completedDocumentPartyIds.length];
						dupCompletedDocumentPartyNames = new String[completedDocumentPartyNames.length];
						for( int i=0; i < dupCompletedDocumentPartyIds.length; ++i ) 
						{
							dupCompletedDocumentPartyIds[i] = completedDocumentPartyIds[i].duplicate();
							dupCompletedDocumentPartyNames[i] = completedDocumentPartyNames[i];
						}
					}
				}

				return new FieldSpec(order, type, paramName, outputFormat,
									 documentId, documentFieldName == null ? null : documentFieldName.duplicate(),
									 dupCompletedDocumentIds, dupCompletedDocumentPartyIds, dupCompletedDocumentPartyNames, completedDocumentType,
									 literal);
			}
			
			public int getEstimatedLengthXml()
			{
				int size = 130 + type.length() + paramName.length() + outputFormat.length();
				if ( isTypeDocumentField() ) 
				{
					size += documentId.getEstimatedLengthXml() + documentFieldName.getEstimatedLengthXml();
				} 
				else if ( isTypeCompletedDocument() ) 
				{
					size += 70 + completedDocumentType.length();
					for( int i=0; i < completedDocumentIds.length; ++i ) 
					{
						size += 170 + completedDocumentIds[i].getEstimatedLengthXml() + completedDocumentPartyNames[i].length();
						if ( completedDocumentPartyIds[i] != null ) 
						{
							size += completedDocumentPartyIds[i].getEstimatedLengthXml();
						}
					}
				} 
				else if ( isTypePartyLink() ) {
					for( int i=0; i < completedDocumentPartyIds.length; ++i ) 
					{
						size += 64 + completedDocumentPartyIds[i].getEstimatedLengthXml() + completedDocumentPartyNames[i].length();
					}
				} 
				else if ( isTypeLiteral() ) 
				{
					size += 25 + literal.length();
				}
				return size;
			}

			public StringBuilder appendXml(StringBuilder buf)
			{
				buf.append("  <FieldSpec>\n");
				buf.append("    <type>").append(XmlUtil.toEscapedXml(type)).append("</type>\n");
				buf.append("    <paramName>").append(XmlUtil.toEscapedXml(paramName)).append("</paramName>\n");
				buf.append("    <outputFormat>").append(XmlUtil.toEscapedXml(outputFormat)).append("</outputFormat>\n");
				if ( isTypeDocumentField() )
				{
					buf.append("    <documentId>").append(documentId.toXml()).append("</documentId>\n");
					buf.append("    <documentFieldName>").append(documentFieldName.toXml()).append("</documentFieldName>\n");
				} 
				else if ( isTypeCompletedDocument() )
				{
					for( int i=0; i < completedDocumentIds.length; ++i ) 
					{
						buf.append("    <completedDocumentId>").append(completedDocumentIds[i].toXml()).append("</completedDocumentId>\n");
						buf.append("    <completedDocumentPartyId>").append(completedDocumentPartyIds[i]==null?"":completedDocumentPartyIds[i].toXml()).append("</completedDocumentPartyId>\n");
						buf.append("    <completedDocumentPartyName>").append(XmlUtil.toEscapedXml(completedDocumentPartyNames[i])).append("</completedDocumentPartyName>\n");
					}
					buf.append("    <completedDocumentType>").append(XmlUtil.toEscapedXml(completedDocumentType)).append("</completedDocumentType>\n");
				} 
				else if ( isTypePartyLink() )
				{
					for( int i=0; i < completedDocumentPartyIds.length; ++i ) 
					{
						buf.append("    <linkPartyId>").append(completedDocumentPartyIds[i].toXml()).append("</linkPartyId>\n");
						buf.append("    <linkPartyName>").append(XmlUtil.toEscapedXml(completedDocumentPartyNames[i])).append("</linkPartyName>\n");
					}
				} 
				else if ( isTypeLiteral() )
				{
					buf.append("    <literal>").append(XmlUtil.toEscapedXml(literal)).append("</literal>\n");
				}
				buf.append("  </FieldSpec>\n");
				return buf;
			}
		}
	}
	
	
	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates an HttpSendAction object
     */
    public HttpSendAction()
    {
    }
    
    public HttpSendAction(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		super(element,ns,_logger);
		try 
		{
			String elementName = element.getName();
			if (!"Action".equals(elementName)) 
				throw new EsfException("The Action tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!"HttpSendAction".equals(typeAttr)) 
				throw new EsfException("The Action tag has type: " + typeAttr + "; expected HttpSendAction.");

			List<Element> specElementList = element.getChildren("Spec", ns);
			ListIterator<Element> iter = specElementList.listIterator();
			int order = 1;
			while (iter.hasNext()) 
			{
				Element specElement = iter.next();
				
				String requestMethod = specElement.getChildTextTrim("requestMethod", ns);
				String url = specElement.getChildTextTrim("url", ns);
				int maxAttempts = Application.getInstance().stringToInt(specElement.getChildTextTrim("maxAttempts", ns),1);
				int retryDelayMinutes = Application.getInstance().stringToInt(specElement.getChildTextTrim("retryDelayMinutes", ns),1);
				String reasonSpec = specElement.getChildTextTrim("reasonSpec", ns);
				String limitSuccessfulHttpStatusCodes = specElement.getChildTextTrim("limitSuccessfulHttpStatusCodes", ns);
				String responseMatchType = specElement.getChildTextTrim("responseMatchType", ns);
				String responseMatchValue = specElement.getChildTextTrim("responseMatchValue", ns);
				
				List<Spec.FieldSpec> fieldSpecList = new LinkedList<Spec.FieldSpec>();
				
				List<Element> fieldSpecElementList = specElement.getChildren("FieldSpec", ns);
				ListIterator<Element> fieldSpecListIter = fieldSpecElementList.listIterator();
				int fieldSpecOrder = 1;
				while( fieldSpecListIter.hasNext() )
				{
					Element fieldSpecElement = fieldSpecListIter.next();
					
					String type = fieldSpecElement.getChildTextTrim("type", ns);
					String paramName = fieldSpecElement.getChildTextTrim("paramName", ns);
					String outputFormat = fieldSpecElement.getChildTextTrim("outputFormat", ns);
					
					EsfUUID documentId = null;
					EsfName documentFieldName = null;
					
					EsfUUID[] completedDocumentIds = null;
					EsfUUID[] completedDocumentPartyIds = null;
					String[] completedDocumentPartyNames = null;
					String completedDocumentType = null;
					
					String literal = null;
					
					if ( Spec.FieldSpec.TYPE_DOCUMENT_FIELD.equals(type) )
					{
						documentId =  new EsfUUID(fieldSpecElement.getChildTextTrim("documentId", ns));
						documentFieldName =  new EsfName(fieldSpecElement.getChildTextTrim("documentFieldName", ns));
					}
					else if ( Spec.FieldSpec.TYPE_COMPLETED_DOCUMENT.equals(type) )
					{
						List<Element> completedDocumentIdElements = fieldSpecElement.getChildren("completedDocumentId", ns);
						List<Element> completedDocumentPartyIdElements = fieldSpecElement.getChildren("completedDocumentPartyId", ns);
						List<Element> completedDocumentPartyNameElements = fieldSpecElement.getChildren("completedDocumentPartyName", ns);
						
						completedDocumentIds = new EsfUUID[completedDocumentIdElements.size()];
						completedDocumentPartyIds = new EsfUUID[completedDocumentIdElements.size()];
						completedDocumentPartyNames = new String[completedDocumentIdElements.size()];
						
						ListIterator<Element> completedDocumentIdElementsIter = completedDocumentIdElements.listIterator();
						ListIterator<Element> completedDocumentPartyIdElementsIter = completedDocumentPartyIdElements.listIterator();
						ListIterator<Element> completedDocumentPartyNameElementsIter = completedDocumentPartyNameElements.listIterator();
						
						int i = 0;
						while( completedDocumentIdElementsIter.hasNext() && completedDocumentPartyIdElementsIter.hasNext() && completedDocumentPartyNameElementsIter.hasNext() ) {
							Element completedDocumentIdElement = completedDocumentIdElementsIter.next();
							Element completedDocumentPartyIdElement = completedDocumentPartyIdElementsIter.next();
							Element completedDocumentPartyNameElement = completedDocumentPartyNameElementsIter.next();
							completedDocumentIds[i] = new EsfUUID(completedDocumentIdElement.getTextTrim());
							completedDocumentPartyIds[i] = new EsfUUID(completedDocumentPartyIdElement.getTextTrim());
							completedDocumentPartyNames[i] = completedDocumentPartyNameElement.getTextTrim();
							++i;
						}
						completedDocumentType = fieldSpecElement.getChildTextTrim("completedDocumentType", ns);
					}
					else if ( Spec.FieldSpec.TYPE_PARTY_LINK.equals(type) )
					{
						List<Element> completedDocumentPartyIdElements = fieldSpecElement.getChildren("linkPartyId", ns);
						List<Element> completedDocumentPartyNameElements = fieldSpecElement.getChildren("linkPartyName", ns);
						
						completedDocumentPartyIds = new EsfUUID[completedDocumentPartyIdElements.size()];
						completedDocumentPartyNames = new String[completedDocumentPartyIdElements.size()];
						
						ListIterator<Element> completedDocumentPartyIdElementsIter = completedDocumentPartyIdElements.listIterator();
						ListIterator<Element> completedDocumentPartyNameElementsIter = completedDocumentPartyNameElements.listIterator();
						
						int i = 0;
						while( completedDocumentPartyIdElementsIter.hasNext() && completedDocumentPartyNameElementsIter.hasNext() ) {
							Element completedDocumentPartyIdElement = completedDocumentPartyIdElementsIter.next();
							Element completedDocumentPartyNameElement = completedDocumentPartyNameElementsIter.next();
							completedDocumentPartyIds[i] = new EsfUUID(completedDocumentPartyIdElement.getTextTrim());
							completedDocumentPartyNames[i] = completedDocumentPartyNameElement.getTextTrim();
							++i;
						}
					}
					else if ( Spec.FieldSpec.TYPE_LITERAL.equals(type) )
					{
						literal = fieldSpecElement.getChildTextTrim("literal", ns);
					}
					
					fieldSpecList.add( new Spec.FieldSpec(fieldSpecOrder++, type, paramName, outputFormat,
							                              documentId, documentFieldName,
							                              completedDocumentIds, completedDocumentPartyIds, completedDocumentPartyNames, completedDocumentType,
							                              literal) );
				}
				
				Spec spec = new Spec(order++, requestMethod, url, maxAttempts, retryDelayMinutes, reasonSpec, limitSuccessfulHttpStatusCodes, responseMatchType, responseMatchValue, fieldSpecList);
				specList.add(spec);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }
    
    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }
    
    public HttpSendAction duplicate()
    {
    	HttpSendAction action = new HttpSendAction();
    	for( Spec spec : specList )
    		action.specList.add( spec.duplicate() );
    	if ( hasCondition() )
    		action.condition = condition.duplicate();
    	return action;
    }
    
    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	super.updatePackagePartyIds(partyIdMapping);
    	for( Spec spec : specList )
    	{
        	for( Spec.FieldSpec fieldSpec : spec.fieldSpecs )
        	{
        		if ( fieldSpec.completedDocumentPartyIds != null ) {
            		for( int i=0; i < fieldSpec.completedDocumentPartyIds.length; ++i) {
            			fieldSpec.completedDocumentPartyIds[i] = partyIdMapping.get(fieldSpec.completedDocumentPartyIds[i]);
            		}
        		}
        	}
    	}
    }
    
    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	for( Spec spec : specList )
    	{
        	for( Spec.FieldSpec fieldSpec : spec.fieldSpecs )
        	{
        		if ( fieldSpec.documentId != null )
        			fieldSpec.documentId = documentIdMapping.get(fieldSpec.documentId);
        	}
    	}
    }

    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }

    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	if ( specList.size() == 0 )
    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' had nothing specified to HTTP(S) GET/POST.");
    	else
    	{
        	for( Spec spec : specList ) 
        	{
        		String url = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, spec.getUrl());
        		
        		HttpSendRequest httpSendRequest = HttpSendRequest.Manager.createNew(context.transaction);
        		httpSendRequest.setMaxAttempts(spec.getMaxAttempts());
        		httpSendRequest.setRetryDelayMinutes(spec.getRetryDelayMinutes());
        		httpSendRequest.setUrl(url);
        		httpSendRequest.setRequestMethod(spec.getRequestMethod());
        		String reason = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, spec.getReasonSpec());
        		httpSendRequest.setReason(reason);
        		if ( spec.hasLimitSucccessfulHttpStatusCodes() )
        			httpSendRequest.setLimitSuccessfulHttpStatusCodes(spec.getLimitSuccessfulHttpStatusCodesArray());
        		httpSendRequest.setResponseMatchType(spec.getResponseMatchType());
        		httpSendRequest.setResponseMatchValue(spec.getResponseMatchValue());
        		
        		StringBuilder paramsBuf = new StringBuilder(256 * spec.fieldSpecs.size());
        		for( Spec.FieldSpec fieldSpec : spec.fieldSpecs )
        		{
    				if ( paramsBuf.length() > 0 )
    					paramsBuf.append('&');

    				if ( fieldSpec.isTypeLiteral() )
        			{
        				String literal = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, fieldSpec.getLiteral());
        				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()) );
        				paramsBuf.append('=');
        				paramsBuf.append( ServletUtil.urlEncode(literal) );
        			}
    				else if ( fieldSpec.isTypeDocumentField() )
    				{
        				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()) );
        				paramsBuf.append('=');

        				FieldTemplate ft = context.getFieldTemplate(fieldSpec.getDocumentId(), fieldSpec.getDocumentFieldName());
        				if ( ft == null )
        				{
				    		context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' included document field spec for field: " + fieldSpec.getDocumentFieldName() +
				    				"; in document id: " + fieldSpec.getDocumentId() + "; but no such field was found.");
        				}
        				else
        				{
        					if ( ft.isTypeFile() ) 
        					{
        						TransactionDocument tranDoc = context.transaction.getTransactionDocumentByDocumentId(fieldSpec.getDocumentId());
        						if ( tranDoc == null )
        						{
        				    		context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' included document field spec for File field: " + fieldSpec.getDocumentFieldName() +
        				    				"; in document id: " + fieldSpec.getDocumentId() + "; but no such transaction document was found.");
        						}
        						else
        						{
            						List<TransactionFile> tranFileList = context.transaction.getAllTransactionFilesForDocumentField(tranDoc.getId(), fieldSpec.getDocumentFieldName());
            						// If there are no files, we'll just send over the field name with no value
            						if ( tranFileList != null && tranFileList.size() > 0 )
            						{
            							boolean needsAnotherParamName = false;
            							for( TransactionFile tranFile : tranFileList )
            							{
            								if ( needsAnotherParamName )
            								{
            									paramsBuf.append('&');
            			        				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()) );
            			        				paramsBuf.append('=');
            								}
            								else
            									needsAnotherParamName = true;
            								byte[] tranFileBytes = tranFile.getFileDataFromDatabase(con);
            								if ( tranFileBytes != null && tranFileBytes.length > 0 )
            									paramsBuf.append( ServletUtil.urlEncode(Base64.encodeToString(tranFileBytes)) ); 
        									paramsBuf.append('&');
        			        				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()+TransactionDocument.FILENAME_PARAM_SUFFIX) );
        			        				paramsBuf.append('=');
        			        				paramsBuf.append(ServletUtil.urlEncode(tranFile.getFileName()));
        									paramsBuf.append('&');
        			        				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()+TransactionDocument.FILEMIMETYPE_PARAM_SUFFIX) );
        			        				paramsBuf.append('=');
        			        				paramsBuf.append(ServletUtil.urlEncode(tranFile.getFileMimeType()));
            							}
            						}
        						}
        					}
        					else if ( ft.isTypeSelection() )
        					{
        						TransactionDocument tranDoc = context.transaction.getTransactionDocumentByDocumentId(fieldSpec.getDocumentId());
        						if ( tranDoc == null )
        						{
        				    		context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' included document field spec for Selection field: " + fieldSpec.getDocumentFieldName() +
        				    				"; in document id: " + fieldSpec.getDocumentId() + "; but no such transaction document was found.");
        						}
        						else
        						{
            						DropDownVersion ddv = context.transaction.getDropDownVersion(new EsfName(ft.getInputFormatSpec()),tranDoc.getDocumentVersion());
            						if ( ddv == null )
            						{
            				    		context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' included document field spec for Selection field: " + fieldSpec.getDocumentFieldName() +
            				    				"; in document id: " + fieldSpec.getDocumentId() + "; for DropDown: " + ft.getInputFormatSpec() + "; but no such dropdown was found.");
            						}
            						else if ( ddv.isAllowMultiSelection() )
            						{
                    					EsfValue[] fieldValues = context.transaction.getFieldValues(fieldSpec.getDocumentName(), fieldSpec.getDocumentFieldName());
                    					if ( fieldValues != null )
                    					{
                        					boolean needsAnotherParamName = false;
                        					for( EsfValue fieldValue : fieldValues )
                        					{
                								if ( needsAnotherParamName )
                								{
                									paramsBuf.append('&');
                			        				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()) );
                			        				paramsBuf.append('=');
                								}
                								else
                									needsAnotherParamName = true;
                								paramsBuf.append( ServletUtil.urlEncode(fieldValue.toPlainString()) );
                        					}
                         				}
            						}
            						else
            						{
                    					EsfValue fieldValue = context.transaction.getFieldValue(fieldSpec.getDocumentName(), fieldSpec.getDocumentFieldName());
                    					if ( fieldSpec.hasOutputFormat() ) // if we have a spec, let's update our FieldTemplate's output spec to use this one instead
                    						ft.setOutputFormatSpec(fieldSpec.getOutputFormat());
                    					String stringValue = FieldLabel.getFormattedFieldValue(context.user, context.transaction, context.currDocumentVersion, ft, fieldValue);
                    					paramsBuf.append( ServletUtil.urlEncode(stringValue) );
            						}
        						}
        					}
        					else
        					{
            					EsfValue fieldValue = context.transaction.getFieldValue(fieldSpec.getDocumentName(), fieldSpec.getDocumentFieldName());
            					if ( fieldSpec.hasOutputFormat() ) // if we have a spec, let's update our FieldTemplate's output spec to use this one instead
            						ft.setOutputFormatSpec(fieldSpec.getOutputFormat());
            					String stringValue = FieldLabel.getFormattedFieldValue(context.user, context.transaction, context.currDocumentVersion, ft, fieldValue);
            					paramsBuf.append( ServletUtil.urlEncode(stringValue) );
        					}
        				}
    				}
    				else if ( fieldSpec.isTypeCompletedDocument() ) 
    				{
    					List<DocumentIdPartyId> documentIdPartyIdList = createCompletedDocumentIdPartyId(fieldSpec);
            			List<TransactionPartyDocument> tranPartyDocumentList = context.transaction.getTransactionPartyDocumentsWithSnapshot(documentIdPartyIdList);

            			// For document retrievals, we skip any documents that we didn't capture on purpose (such as auto-completing parties). For data, there's no need to check.
            			if ( fieldSpec.isCompletedDocumentTypeHtml() || fieldSpec.isCompletedDocumentTypePdf() )
            			{
                			ListIterator<TransactionPartyDocument> tpdIterator = tranPartyDocumentList.listIterator();
                			while( tpdIterator.hasNext() )
                			{
                				TransactionPartyDocument tpd = tpdIterator.next();
                				if ( tpd.isSnapshotDocumentSkipped() )
                					tpdIterator.remove();
                			}
            			}
            			
            			// If we can't find any documents, just output the param name with no value
            			if ( tranPartyDocumentList.size() == 0 )
            			{
            				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()) );
            				paramsBuf.append('=');
            				continue;
            			}

    					if ( fieldSpec.isCompletedDocumentTypeHtml() ) 
    					{
    						boolean needsAmp = false;
            				for( TransactionPartyDocument tpd : tranPartyDocumentList )
            				{
            					if ( needsAmp )
            						paramsBuf.append('&');
            					else
            						needsAmp = true;
            					byte[] htmlBytes = EsfString.stringToBytes(tpd.getSnapshotDocument());
                  				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()) );
                				paramsBuf.append('=');
                				if ( htmlBytes != null )
                					paramsBuf.append( ServletUtil.urlEncode(Base64.encodeToString(htmlBytes)) );
								paramsBuf.append('&');
		        				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()+TransactionDocument.FILEMIMETYPE_PARAM_SUFFIX) );
		        				paramsBuf.append('=');
		        				paramsBuf.append(ServletUtil.urlEncode(Application.CONTENT_TYPE_HTML));
            				}
    					} 
    					else if ( fieldSpec.isCompletedDocumentTypePdf() ) 
    					{
        					boolean useLandscape = false;
        					String[] htmlFiles = new String[tranPartyDocumentList.size()];
        					int i = 0;
            				for( TransactionPartyDocument tpd : tranPartyDocumentList )
            				{
            					TransactionDocument td = context.transaction.getTransactionDocument(tpd.getTransactionDocumentId());
            					if ( td.getDocumentVersion().isLandscape() )
            						useLandscape = true;
            					htmlFiles[i++] = tpd.getSnapshotDocument();
            				}
            				
            				HtmlToPdf htmlToPdf = new HtmlToPdf();
            				byte[] pdf = useLandscape ? htmlToPdf.generateLandscapeSignedPdf(htmlFiles) : htmlToPdf.generateSignedPdf(htmlFiles);
               				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()) );
            				paramsBuf.append('=');
            				if ( pdf != null )
            					paramsBuf.append( ServletUtil.urlEncode(Base64.encodeToString(pdf)) ); 
							paramsBuf.append('&');
	        				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()+TransactionDocument.FILEMIMETYPE_PARAM_SUFFIX) );
	        				paramsBuf.append('=');
	        				paramsBuf.append(ServletUtil.urlEncode(Application.CONTENT_TYPE_PDF));
    					} 
    					else if ( fieldSpec.isCompletedDocumentTypeXmlData() ) 
    					{
    						boolean needsAmp = false;
            				for( TransactionPartyDocument tpd : tranPartyDocumentList )
            				{
            					if ( needsAmp )
            						paramsBuf.append('&');
            					else
            						needsAmp = true;
            					
            					byte[] xmlDataBytes = EsfString.stringToBytes(tpd.getSnapshotData());
                  				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()) );
                				paramsBuf.append('=');
                				if ( xmlDataBytes != null )
                					paramsBuf.append( ServletUtil.urlEncode(Base64.encodeToString(xmlDataBytes)) ); 
								paramsBuf.append('&');
		        				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()+TransactionDocument.FILEMIMETYPE_PARAM_SUFFIX) );
		        				paramsBuf.append('=');
		        				paramsBuf.append(ServletUtil.urlEncode(Application.CONTENT_TYPE_XML));
            				}
    					}
    					else if ( fieldSpec.isCompletedDocumentTypeXmlSnapshots() ) 
    					{
    						boolean needsAmp = false;
            				for( TransactionPartyDocument tpd : tranPartyDocumentList )
            				{
            					if ( needsAmp )
            						paramsBuf.append('&');
            					else
            						needsAmp = true;
            					byte[] xmlBytes = EsfString.stringToBytes(tpd.getSnapshotXml());
                  				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()) );
                				paramsBuf.append('=');
                				if ( xmlBytes != null )
                					paramsBuf.append( ServletUtil.urlEncode(Base64.encodeToString(xmlBytes)) ); 
								paramsBuf.append('&');
		        				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()+TransactionDocument.FILEMIMETYPE_PARAM_SUFFIX) );
		        				paramsBuf.append('=');
		        				paramsBuf.append(ServletUtil.urlEncode(Application.CONTENT_TYPE_XML));
            				}
    					}
    				}
            		else if ( fieldSpec.isTypePartyLink() )
            		{
            			Application app = Application.getInstance();
            			
        				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()) );
        				paramsBuf.append('=');

        				List<TransactionParty> tranParties = context.transaction.getAllTransactionParties();
        				
						boolean needsParam = false;
						for( TransactionParty tranParty : tranParties )
						{
							if ( fieldSpec.isMatchingPickupPartyId(tranParty.getPackageVersionPartyTemplateId()) )
							{
	           					if ( needsParam )
	           					{
	        						paramsBuf.append('&');
	    	        				paramsBuf.append( ServletUtil.urlEncode(fieldSpec.getParamName()) );
	    	        				paramsBuf.append('=');
	           					}
	        					else
	        						needsParam = true;

	           					paramsBuf.append( ServletUtil.urlEncode(app.getExternalContextPath() + "/P/" + tranParty.getTransactionPartyAssignmentPickupCode()) );
							}
						}
            		}
    				
        		} // end for each Spec.FieldSpec
        		
        		// Save the params we've created
        		httpSendRequest.setParams(paramsBuf.toString());
        		
        		if ( httpSendRequest.save(con) ) 
        		{
	    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' has queued HTTP Send id: " + httpSendRequest.getId() + "; method '" + spec.getRequestMethod() + "' to: '" + spec.getUrl() + "'; reason: " + reason);    	        	
    	        }
    	        else
    	        {
    				context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' failed to queued HTTP Send: " + httpSendRequest.getId() + "; method '" + spec.getRequestMethod() + "' to: '" + spec.getUrl() + "'; reason: " + reason);    	        	
    	        }
        	}
    	}
    }
    
    List<DocumentIdPartyId> createCompletedDocumentIdPartyId(Spec.FieldSpec fieldSpec) 
    {
    	LinkedList<DocumentIdPartyId> list = new LinkedList<DocumentIdPartyId>();
    	for( int i=0; i < fieldSpec.getCompletedDocumentIds().length; ++i )
    	{
    		DocumentIdPartyId dp = new DocumentIdPartyId(fieldSpec.getCompletedDocumentIds()[i], null, fieldSpec.getCompletedDocumentPartyIds()[i], null);
    		list.add(dp);
    	}
    	return list;
    }
    
    @Override
    public String specsToString()
    {
    	StringBuilder buf = new StringBuilder(specList.size() * 80);
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(",");
    		buf.append(spec.getRequestMethod() + ": " + spec.getReasonSpec());
     	}
    	return buf.toString();
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 40;
		for( Spec spec : specList )
		{
			size += 320 + spec.requestMethod.length() + spec.url.length() + 4 + 4 + spec.reasonSpec.length() + spec.responseMatchType.length();
			if ( spec.limitSuccessfulHttpStatusCodes != null )
				size += spec.limitSuccessfulHttpStatusCodes.length();
			if ( spec.responseMatchValue != null )
				size += spec.responseMatchValue.length();
			for( Spec.FieldSpec fieldSpec : spec.fieldSpecs )
				size += fieldSpec.getEstimatedLengthXml();
		}
		if ( hasCondition() )
			size += condition.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Action type=\"HttpSendAction\">\n");
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <requestMethod>").append(XmlUtil.toEscapedXml(spec.requestMethod)).append("</requestMethod>\n");
			buf.append("  <url>").append(XmlUtil.toEscapedXml(spec.url)).append("</url>\n");
			buf.append("  <maxAttempts>").append(spec.maxAttempts).append("</maxAttempts>\n");
			buf.append("  <retryDelayMinutes>").append(spec.retryDelayMinutes).append("</retryDelayMinutes>\n");
			buf.append("  <reasonSpec>").append(XmlUtil.toEscapedXml(spec.reasonSpec)).append("</reasonSpec>\n");
			buf.append("  <limitSuccessfulHttpStatusCodes>").append(XmlUtil.toEscapedXml(spec.limitSuccessfulHttpStatusCodes)).append("</limitSuccessfulHttpStatusCodes>\n");
			buf.append("  <responseMatchType>").append(XmlUtil.toEscapedXml(spec.responseMatchType)).append("</responseMatchType>\n");
			buf.append("  <responseMatchValue>").append(XmlUtil.toEscapedXml(spec.responseMatchValue)).append("</responseMatchValue>\n");
			for( Spec.FieldSpec fieldSpec : spec.fieldSpecs )
				fieldSpec.appendXml(buf);
			buf.append(" </Spec>\n");
		}
		super.appendXml(buf);
		buf.append("</Action>\n");
		return buf;
	}
   
}