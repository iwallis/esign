// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfMoney;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.data.Record;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.integration.paypal.NvpDirectPayment;
import com.esignforms.open.prog.Document;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.CreditCardValidator;
import com.esignforms.open.util.XmlUtil;

/**
* Action is the base class of all actions used by the transaction engine.
* PayPalDoDirectPaymentSaleAction submits credit card information for a new sale using the 
* PayPal DoDirectPayment "Sale" method.
* 
* After a payment attempt is made, a set of PAYPAL_* fields are added to the document record and can be used to test for success
* as needed.
* 
* @author Yozons, Inc.
*/
public class PayPalDoDirectPaymentSaleAction extends Action
{
	private static final long serialVersionUID = 9107456110101879611L;

	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = 1148874632157965393L;

		public Spec(int o, 
					String customerNameFieldSpec, String descriptionFieldSpec, String emailFieldSpec, String invoiceNumberFieldSpec, // optional
					EsfUUID ccBillingFirstNameDocumentId, EsfName ccBillingFirstNameField, 
					EsfUUID ccBillingLastNameDocumentId, EsfName ccBillingLastNameField, 
					EsfUUID ccBillingAddressDocumentId, EsfName ccBillingAddressField, 
					EsfUUID ccBillingAddress2DocumentId, EsfName ccBillingAddress2Field, // optional
					EsfUUID ccBillingCityDocumentId, EsfName ccBillingCityField, 
					EsfUUID ccBillingStateDocumentId, EsfName ccBillingStateField,
					EsfUUID ccBillingZipDocumentId, EsfName ccBillingZipField, 
					EsfUUID ccBillingCountryCodeDocumentId, EsfName ccBillingCountryCodeField, // optional
					EsfUUID ccTypeDocumentId, EsfName ccTypeField,  // optional
					EsfUUID ccAccountNumberDocumentId, EsfName ccAccountNumberField,
					EsfUUID ccExpirationDocumentId, EsfName ccExpirationField, 
					EsfUUID ccSecurityIdDocumentId, EsfName ccSecurityIdField,  // optional
					EsfUUID amountDocumentId, EsfName amountField,
					String onSuccessMessageSpec, String onFailureMessageSpec,  // optional
					boolean maskAccountNumberOnSuccess, boolean maskSecurityIdOnSuccess)
		{
			order = o;
			this.customerNameFieldSpec = EsfString.ensureNotNull(customerNameFieldSpec);
			this.descriptionFieldSpec = EsfString.ensureNotNull(descriptionFieldSpec);
			this.emailFieldSpec = EsfString.ensureNotNull(emailFieldSpec);
			this.invoiceNumberFieldSpec = EsfString.ensureNotNull(invoiceNumberFieldSpec);
			
			this.ccBillingFirstNameDocumentId = EsfUUID.ensureNotNull(ccBillingFirstNameDocumentId);
			this.ccBillingFirstNameField = EsfName.ensureNotNull(ccBillingFirstNameField);
			
			this.ccBillingLastNameDocumentId = EsfUUID.ensureNotNull(ccBillingLastNameDocumentId);
			this.ccBillingLastNameField = EsfName.ensureNotNull(ccBillingLastNameField);
			
			this.ccBillingAddressDocumentId = EsfUUID.ensureNotNull(ccBillingAddressDocumentId);
			this.ccBillingAddressField = EsfName.ensureNotNull(ccBillingAddressField);
			
			this.ccBillingAddress2DocumentId = EsfUUID.ensureNotNull(ccBillingAddress2DocumentId);
			this.ccBillingAddress2Field = ccBillingAddress2Field; // may be null (optional)
			
			this.ccBillingCityDocumentId = EsfUUID.ensureNotNull(ccBillingCityDocumentId);
			this.ccBillingCityField = EsfName.ensureNotNull(ccBillingCityField);
			
			this.ccBillingStateDocumentId = EsfUUID.ensureNotNull(ccBillingStateDocumentId);
			this.ccBillingStateField = EsfName.ensureNotNull(ccBillingStateField);
			
			this.ccBillingZipDocumentId = EsfUUID.ensureNotNull(ccBillingZipDocumentId);
			this.ccBillingZipField = EsfName.ensureNotNull(ccBillingZipField);
			
			this.ccBillingCountryCodeDocumentId = EsfUUID.ensureNotNull(ccBillingCountryCodeDocumentId);
			this.ccBillingCountryCodeField = ccBillingCountryCodeField; // may be null (optional)
			
			this.ccTypeDocumentId = EsfUUID.ensureNotNull(ccTypeDocumentId);
			this.ccTypeField = ccTypeField; // may be null (optional)
			
			this.ccAccountNumberDocumentId = EsfUUID.ensureNotNull(ccAccountNumberDocumentId);
			this.ccAccountNumberField = EsfName.ensureNotNull(ccAccountNumberField);
			
			this.ccExpirationDocumentId = EsfUUID.ensureNotNull(ccExpirationDocumentId);
			this.ccExpirationField = EsfName.ensureNotNull(ccExpirationField);
			
			this.ccSecurityIdDocumentId = EsfUUID.ensureNotNull(ccSecurityIdDocumentId);
			this.ccSecurityIdField = ccSecurityIdField; // may be null (optional)
			
			this.amountDocumentId = EsfUUID.ensureNotNull(amountDocumentId);
			this.amountField = EsfName.ensureNotNull(amountField);
			
			this.onSuccessMessageSpec = EsfString.ensureNotNull(onSuccessMessageSpec);
			this.onFailureMessageSpec = EsfString.ensureNotNull(onFailureMessageSpec);
			this.maskAccountNumberOnSuccess = maskAccountNumberOnSuccess;
			this.maskSecurityIdOnSuccess = maskSecurityIdOnSuccess;

		}
		
		int order;
		String customerNameFieldSpec; // optional
		String descriptionFieldSpec; // optional
		String emailFieldSpec; // optional
		String invoiceNumberFieldSpec; // optional

		EsfUUID ccBillingFirstNameDocumentId;
		EsfName ccBillingFirstNameField;
		
		EsfUUID ccBillingLastNameDocumentId;
		EsfName ccBillingLastNameField;
		
		EsfUUID ccBillingAddressDocumentId;
		EsfName ccBillingAddressField;
		EsfUUID ccBillingAddress2DocumentId;    // optional
		EsfName ccBillingAddress2Field;
		
		EsfUUID ccBillingCityDocumentId;
		EsfName ccBillingCityField;

		EsfUUID ccBillingStateDocumentId;
		EsfName ccBillingStateField;

		EsfUUID ccBillingZipDocumentId;
		EsfName ccBillingZipField;

		EsfUUID ccBillingCountryCodeDocumentId; // optional
		EsfName ccBillingCountryCodeField;

		EsfUUID ccTypeDocumentId; // optional
		EsfName ccTypeField;

		EsfUUID ccAccountNumberDocumentId;
		EsfName ccAccountNumberField;

		EsfUUID ccExpirationDocumentId;
		EsfName ccExpirationField;

		EsfUUID ccSecurityIdDocumentId; // optional CVV
		EsfName ccSecurityIdField;

		EsfUUID amountDocumentId; 
		EsfName amountField;
		
		String onSuccessMessageSpec;
		String onFailureMessageSpec;
		boolean maskAccountNumberOnSuccess;
		boolean maskSecurityIdOnSuccess;

		public Spec duplicate() 
		{
			return new Spec(order,
					customerNameFieldSpec,descriptionFieldSpec,emailFieldSpec,invoiceNumberFieldSpec,
					ccBillingFirstNameDocumentId, ccBillingFirstNameField.duplicate(),
					ccBillingLastNameDocumentId, ccBillingLastNameField.duplicate(),
					ccBillingAddressDocumentId, ccBillingAddressField.duplicate(),
					ccBillingAddress2DocumentId, ccBillingAddress2Field==null ? null : ccBillingAddress2Field.duplicate(),
					ccBillingCityDocumentId, ccBillingCityField.duplicate(),
					ccBillingStateDocumentId, ccBillingStateField.duplicate(),
					ccBillingZipDocumentId, ccBillingZipField.duplicate(),
					ccBillingCountryCodeDocumentId, ccBillingCountryCodeField==null ? null : ccBillingCountryCodeField.duplicate(),
					ccTypeDocumentId, ccTypeField==null ? null : ccTypeField.duplicate(),
					ccAccountNumberDocumentId, ccAccountNumberField.duplicate(),
					ccExpirationDocumentId, ccExpirationField.duplicate(),
					ccSecurityIdDocumentId, ccSecurityIdField==null ? null : ccSecurityIdField.duplicate(),
					amountDocumentId, amountField.duplicate(), 
					onSuccessMessageSpec, onFailureMessageSpec, 
					maskAccountNumberOnSuccess, maskSecurityIdOnSuccess);
		}
		public int getOrder() { return order; }
		public void setOrder(int v) { order = v; }
		
		public String getCustomerNameFieldSpec() { return customerNameFieldSpec; }
		public void setCustomerNameFieldSpec(String v) { customerNameFieldSpec = EsfString.ensureNotNull(v); }
		public String getDescriptionFieldSpec() { return descriptionFieldSpec; }
		public void setDescriptionFieldSpec(String v) { descriptionFieldSpec = EsfString.ensureNotNull(v); }
		public String getEmailFieldSpec() { return emailFieldSpec; }
		public void setEmailFieldSpec(String v) { emailFieldSpec = EsfString.ensureNotNull(v); }
		public String getInvoiceNumberFieldSpec() { return invoiceNumberFieldSpec; }
		public void setInvoiceNumberFieldSpec(String v) { invoiceNumberFieldSpec = EsfString.ensureNotNull(v); }

		public EsfUUID getCcBillingFirstNameDocumentId() { return ccBillingFirstNameDocumentId; }
		public void setCcBillingFirstNameDocumentId(EsfUUID v) { ccBillingFirstNameDocumentId = v; }
		public EsfName getCcBillingFirstNameField() { return ccBillingFirstNameField; }
		public void setCcBillingFirstNameField(EsfName v) { ccBillingFirstNameField = v; }

		public EsfUUID getCcBillingLastNameDocumentId() { return ccBillingLastNameDocumentId; }
		public void setCcBillingLastNameDocumentId(EsfUUID v) { ccBillingLastNameDocumentId = v; }
		public EsfName getCcBillingLastNameField() { return ccBillingLastNameField; }
		public void setCcBillingLastNameField(EsfName v) { ccBillingLastNameField = v; }

		public EsfUUID getCcBillingAddressDocumentId() { return ccBillingAddressDocumentId; }
		public void setCcBillingAddressDocumentId(EsfUUID v) { ccBillingAddressDocumentId = v; }
		public EsfName getCcBillingAddressField() { return ccBillingAddressField; }
		public void setCcBillingAddressField(EsfName v) { ccBillingAddressField = v; }

		public EsfUUID getCcBillingAddress2DocumentId() { return ccBillingAddress2DocumentId; }
		public void setCcBillingAddress2DocumentId(EsfUUID v) { ccBillingAddress2DocumentId = EsfUUID.ensureNotNull(v); }
		public EsfName getCcBillingAddress2Field() { return ccBillingAddress2Field; }
		public void setCcBillingAddress2Field(EsfName v) { ccBillingAddress2Field = v; }
		public boolean hasCcBillingAddress2Specified() 
		{ 
			return ! ccBillingAddress2DocumentId.isNull() && ccBillingAddress2Field != null && ccBillingAddress2Field.isValid();
		}

		public EsfUUID getCcBillingCityDocumentId() { return ccBillingCityDocumentId; }
		public void setCcBillingCityDocumentId(EsfUUID v) { ccBillingCityDocumentId = v; }
		public EsfName getCcBillingCityField() { return ccBillingCityField; }
		public void setCcBillingCityField(EsfName v) { ccBillingCityField = v; }

		public EsfUUID getCcBillingStateDocumentId() { return ccBillingStateDocumentId; }
		public void setCcBillingStateDocumentId(EsfUUID v) { ccBillingStateDocumentId = v; }
		public EsfName getCcBillingStateField() { return ccBillingStateField; }
		public void setCcBillingStateField(EsfName v) { ccBillingStateField = v; }

		public EsfUUID getCcBillingZipDocumentId() { return ccBillingZipDocumentId; }
		public void setCcBillingZipDocumentId(EsfUUID v) { ccBillingZipDocumentId = v; }
		public EsfName getCcBillingZipField() { return ccBillingZipField; }
		public void setCcBillingZipField(EsfName v) { ccBillingZipField = v; }

		public EsfUUID getCcBillingCountryCodeDocumentId() { return ccBillingCountryCodeDocumentId; }
		public void setCcBillingCountryCodeDocumentId(EsfUUID v) { ccBillingCountryCodeDocumentId = EsfUUID.ensureNotNull(v); }
		public EsfName getCcBillingCountryCodeField() { return ccBillingCountryCodeField; }
		public void setCcBillingCountryCodeField(EsfName v) { ccBillingCountryCodeField = v; }
		public boolean hasCcBillingCountryCodeSpecified() 
		{ 
			return ! ccBillingCountryCodeDocumentId.isNull() && ccBillingCountryCodeField != null && ccBillingCountryCodeField.isValid();
		}

		public EsfUUID getCcTypeDocumentId() { return ccTypeDocumentId; }
		public void setCcTypeDocumentId(EsfUUID v) { ccTypeDocumentId = EsfUUID.ensureNotNull(v); }
		public EsfName getCcTypeField() { return ccTypeField; }
		public void setCcTypeField(EsfName v) { ccTypeField = v; }
		public boolean hasCcTypeSpecified() 
		{ 
			return ! ccTypeDocumentId.isNull() && ccTypeField != null && ccTypeField.isValid();
		}

		public EsfUUID getCcAccountNumberDocumentId() { return ccAccountNumberDocumentId; }
		public void setCcAccountNumberDocumentId(EsfUUID v) { ccAccountNumberDocumentId = v; }
		public EsfName getCcAccountNumberField() { return ccAccountNumberField; }
		public void setCcAccountNumberField(EsfName v) { ccAccountNumberField = v; }

		public EsfUUID getCcExpirationDocumentId() { return ccExpirationDocumentId; }
		public void setCcExpirationDocumentId(EsfUUID v) { ccExpirationDocumentId = v; }
		public EsfName getCcExpirationField() { return ccExpirationField; }
		public void setCcExpirationField(EsfName v) { ccExpirationField = v; }

		public EsfUUID getCcSecurityIdDocumentId() { return ccSecurityIdDocumentId; }
		public void setCcSecurityIdDocumentId(EsfUUID v) { ccSecurityIdDocumentId = EsfUUID.ensureNotNull(v); }
		public EsfName getCcSecurityIdField() { return ccSecurityIdField; }
		public void setCcSecurityIdField(EsfName v) { ccSecurityIdField = v; }
		public boolean hasCcSecurityIdSpecified() 
		{ 
			return ! ccSecurityIdDocumentId.isNull() && ccSecurityIdField != null && ccSecurityIdField.isValid();
		}

		public EsfUUID getAmountDocumentId() { return amountDocumentId; }
		public void setAmountDocumentId(EsfUUID v) { amountDocumentId = v; }
		public EsfName getAmountField() { return amountField; }
		public void setAmountField(EsfName v) { amountField = v; }
		
		public String getOnSuccessMessageSpec() { return onSuccessMessageSpec; }
		public void setOnSuccessMessageSpec(String v) { onSuccessMessageSpec = EsfString.ensureNotNull(v); }

		public String getOnFailureMessageSpec() { return onFailureMessageSpec; }
		public void setOnFailureMessageSpec(String v) { onFailureMessageSpec = EsfString.ensureNotNull(v); }

		public boolean isMaskAccountNumberOnSuccess() { return maskAccountNumberOnSuccess; }
		public void setMaskAccountNumberOnSuccess(boolean v) { maskAccountNumberOnSuccess = v; }

		public boolean isMaskSecurityIdOnSuccess() { return maskSecurityIdOnSuccess; }
		public void setMaskSecurityIdOnSuccess(boolean v) { maskSecurityIdOnSuccess = v; }
	}
	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates an PayPalDoDirectPaymentSaleAction object
     */
    public PayPalDoDirectPaymentSaleAction()
    {
    }
    
    public PayPalDoDirectPaymentSaleAction(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		super(element,ns,_logger);
		try 
		{
			String elementName = element.getName();
			if (!"Action".equals(elementName)) 
				throw new EsfException("The Action tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!"PayPalDoDirectPaymentSaleAction".equals(typeAttr)) 
				throw new EsfException("The Action tag has type: " + typeAttr + "; expected PayPalDoDirectPaymentSaleAction.");

			List<Element> specElementList = element.getChildren("Spec", ns);
			ListIterator<Element> specIter = specElementList.listIterator();
			int order = 1;
			while( specIter.hasNext() ) 
			{
				Element specElement = specIter.next();
				
				EsfUUID ccBillingFirstNameDocumentId = new EsfUUID(specElement.getChildTextTrim("ccBillingFirstNameDocumentId", ns)); 
				EsfName ccBillingFirstNameField = new EsfName(specElement.getChildTextTrim("ccBillingFirstNameField", ns)); 
				
				EsfUUID ccBillingLastNameDocumentId = new EsfUUID(specElement.getChildTextTrim("ccBillingLastNameDocumentId", ns)); 
				EsfName ccBillingLastNameField = new EsfName(specElement.getChildTextTrim("ccBillingLastNameField", ns)); 
				
				EsfUUID ccBillingAddressDocumentId = new EsfUUID(specElement.getChildTextTrim("ccBillingAddressDocumentId", ns)); 
				EsfName ccBillingAddressField = new EsfName(specElement.getChildTextTrim("ccBillingAddressField", ns)); 

				EsfUUID ccBillingAddress2DocumentId = new EsfUUID(specElement.getChildTextTrim("ccBillingAddress2DocumentId", ns)); 
				EsfName ccBillingAddress2Field = new EsfName(specElement.getChildTextTrim("ccBillingAddress2Field", ns)); 
				if ( ! ccBillingAddress2Field.isValid() )
					ccBillingAddress2Field = null;
				
				EsfUUID ccBillingCityDocumentId = new EsfUUID(specElement.getChildTextTrim("ccBillingCityDocumentId", ns)); 
				EsfName ccBillingCityField = new EsfName(specElement.getChildTextTrim("ccBillingCityField", ns)); 
				
				EsfUUID ccBillingStateDocumentId = new EsfUUID(specElement.getChildTextTrim("ccBillingStateDocumentId", ns)); 
				EsfName ccBillingStateField = new EsfName(specElement.getChildTextTrim("ccBillingStateField", ns)); 
				
				EsfUUID ccBillingZipDocumentId = new EsfUUID(specElement.getChildTextTrim("ccBillingZipDocumentId", ns));
				EsfName ccBillingZipField = new EsfName(specElement.getChildTextTrim("ccBillingZipField", ns));
				
				EsfUUID ccBillingCountryCodeDocumentId = new EsfUUID(specElement.getChildTextTrim("ccBillingCountryCodeDocumentId", ns)); 
				EsfName ccBillingCountryCodeField = new EsfName(specElement.getChildTextTrim("ccBillingCountryCodeField", ns)); 
				if ( ! ccBillingCountryCodeField.isValid() )
					ccBillingCountryCodeField = null;
				
				EsfUUID ccTypeDocumentId = new EsfUUID(specElement.getChildTextTrim("ccTypeDocumentId", ns)); 
				EsfName ccTypeField = new EsfName(specElement.getChildTextTrim("ccTypeField", ns)); 
				if ( ! ccTypeField.isValid() )
					ccTypeField = null;
				
				EsfUUID ccAccountNumberDocumentId = new EsfUUID(specElement.getChildTextTrim("ccAccountNumberDocumentId", ns)); 
				EsfName ccAccountNumberField = new EsfName(specElement.getChildTextTrim("ccAccountNumberField", ns)); 
				
				EsfUUID ccExpirationDocumentId = new EsfUUID(specElement.getChildTextTrim("ccExpirationDocumentId", ns)); 
				EsfName ccExpirationField = new EsfName(specElement.getChildTextTrim("ccExpirationField", ns)); 
				
				EsfUUID ccSecurityIdDocumentId = new EsfUUID(specElement.getChildTextTrim("ccSecurityIdDocumentId", ns)); 
				EsfName ccSecurityIdField = new EsfName(specElement.getChildTextTrim("ccSecurityIdField", ns)); 
				if ( ! ccSecurityIdField.isValid() )
					ccSecurityIdField = null;
				
				EsfUUID amountDocumentId = new EsfUUID(specElement.getChildTextTrim("amountDocumentId", ns)); 
				EsfName amountField = new EsfName(specElement.getChildTextTrim("amountField", ns)); 
				
				boolean maskAccountNumberOnSuccess = EsfBoolean.toBoolean(specElement.getChildTextTrim("maskAccountNumberOnSuccess", ns));
				boolean maskSecurityIdOnSuccess = EsfBoolean.toBoolean(specElement.getChildTextTrim("maskSecurityIdOnSuccess", ns));	
				
				Spec spec = new Spec(order++, 
									specElement.getChildTextTrim("customerNameFieldSpec", ns),
									specElement.getChildTextTrim("descriptionFieldSpec", ns),
									specElement.getChildTextTrim("emailFieldSpec", ns),
									specElement.getChildTextTrim("invoiceNumberFieldSpec", ns),
									
									ccBillingFirstNameDocumentId, ccBillingFirstNameField,
									ccBillingLastNameDocumentId, ccBillingLastNameField,
									ccBillingAddressDocumentId, ccBillingAddressField,
									ccBillingAddress2DocumentId, ccBillingAddress2Field,
									ccBillingCityDocumentId, ccBillingCityField,
									ccBillingStateDocumentId, ccBillingStateField,
									ccBillingZipDocumentId, ccBillingZipField,
									ccBillingCountryCodeDocumentId, ccBillingCountryCodeField,
									ccTypeDocumentId, ccTypeField,
									ccAccountNumberDocumentId, ccAccountNumberField,
									ccExpirationDocumentId, ccExpirationField,
									ccSecurityIdDocumentId, ccSecurityIdField,
									amountDocumentId, amountField, 
									specElement.getChildTextTrim("onSuccessMessageSpec", ns),
									specElement.getChildTextTrim("onFailureMessageSpec", ns),
									maskAccountNumberOnSuccess, maskSecurityIdOnSuccess
									);
				specList.add(spec);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }
    
    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }
    
    public PayPalDoDirectPaymentSaleAction duplicate()
    {
    	PayPalDoDirectPaymentSaleAction action = new PayPalDoDirectPaymentSaleAction();
    	for( Spec spec : specList )
    		action.specList.add( spec.duplicate() );
    	if ( hasCondition() )
    		action.condition = condition.duplicate();
    	return action;
    }
    
    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	super.updatePackagePartyIds(partyIdMapping);
    	// we have no party id mappings
    }
    
    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	for( Spec spec : specList )
    	{
    		EsfUUID newDocId = documentIdMapping.get(spec.getCcBillingFirstNameDocumentId());
    		if ( newDocId != null )
    			spec.setCcBillingFirstNameDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getCcBillingLastNameDocumentId());
    		if ( newDocId != null )
    			spec.setCcBillingLastNameDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getCcBillingAddressDocumentId());
    		if ( newDocId != null )
    			spec.setCcBillingAddressDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getCcBillingAddress2DocumentId());
    		if ( newDocId != null )
    			spec.setCcBillingAddress2DocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getCcBillingCityDocumentId());
    		if ( newDocId != null )
    			spec.setCcBillingCityDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getCcBillingStateDocumentId());
    		if ( newDocId != null )
    			spec.setCcBillingStateDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getCcBillingZipDocumentId());
    		if ( newDocId != null )
    			spec.setCcBillingZipDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getCcBillingCountryCodeDocumentId());
    		if ( newDocId != null )
    			spec.setCcBillingCountryCodeDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getCcTypeDocumentId());
    		if ( newDocId != null )
    			spec.setCcTypeDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getCcAccountNumberDocumentId());
    		if ( newDocId != null )
    			spec.setCcAccountNumberDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getCcExpirationDocumentId());
    		if ( newDocId != null )
    			spec.setCcExpirationDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getCcSecurityIdDocumentId());
    		if ( newDocId != null )
    			spec.setCcSecurityIdDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getAmountDocumentId());
    		if ( newDocId != null )
    			spec.setAmountDocumentId(newDocId);
    	}
    }
    
    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }

    
    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	if ( specList.size() == 0 )
    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' had no payments configured to be done.");
    	else
    	{
    		int specNumber = 1;
        	for( Spec spec : specList ) 
        	{
        		Document ccBillingFirstNameDocument = context.getDocument(spec.getCcBillingFirstNameDocumentId());
        		Document ccBillingLastNameDocument = context.getDocument(spec.getCcBillingLastNameDocumentId());
        		Document ccBillingAddressDocument = context.getDocument(spec.getCcBillingAddressDocumentId());
        		Document ccBillingAddress2Document = spec.hasCcBillingAddress2Specified() ? context.getDocument(spec.getCcBillingAddress2DocumentId()) : null; // optional
        		Document ccBillingCityDocument = context.getDocument(spec.getCcBillingCityDocumentId());
        		Document ccBillingStateDocument = context.getDocument(spec.getCcBillingStateDocumentId());
        		Document ccBillingZipDocument = context.getDocument(spec.getCcBillingZipDocumentId());
        		Document ccBillingCountryCodeDocument = spec.hasCcBillingCountryCodeSpecified() ? context.getDocument(spec.getCcBillingCountryCodeDocumentId()) : null; // optional
        		Document ccTypeDocument = spec.hasCcTypeSpecified() ? context.getDocument(spec.getCcTypeDocumentId()) : null; // optional
        		Document ccAccountNumberDocument = context.getDocument(spec.getCcAccountNumberDocumentId());
        		Document ccExpirationDocument = context.getDocument(spec.getCcExpirationDocumentId());
        		Document ccSecurityIdDocument = spec.hasCcSecurityIdSpecified() ? context.getDocument(spec.getCcSecurityIdDocumentId()) : null; // optional
        		Document amountDocument = context.getDocument(spec.getAmountDocumentId());
        		
        		if ( ccBillingFirstNameDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to use cc billing first name field '" + spec.getCcBillingFirstNameField() + "' in document id '" + spec.getCcBillingFirstNameDocumentId() + "' but no such document was found.");
        		else if ( ccBillingLastNameDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to use cc billing last name field '" + spec.getCcBillingLastNameField() + "' in document id '" + spec.getCcBillingLastNameDocumentId() + "' but no such document was found.");
        		else if ( ccBillingAddressDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to use cc billing address field '" + spec.getCcBillingAddressField() + "' in document id '" + spec.getCcBillingAddressDocumentId() + "' but no such document was found.");
        		else if ( ccBillingCityDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to use cc billing city field '" + spec.getCcBillingCityField() + "' in document id '" + spec.getCcBillingCityDocumentId() + "' but no such document was found.");
        		else if ( ccBillingStateDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to use cc billing state field '" + spec.getCcBillingStateField() + "' in document id '" + spec.getCcBillingStateDocumentId() + "' but no such document was found.");
        		else if ( ccBillingZipDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to use cc billing zip field '" + spec.getCcBillingZipField() + "' in document id '" + spec.getCcBillingZipDocumentId() + "' but no such document was found.");
        		else if ( ccAccountNumberDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to use cc account field '" + spec.getCcAccountNumberField() + "' in document id '" + spec.getCcAccountNumberDocumentId() + "' but no such document was found.");
        		else if ( ccExpirationDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to use cc expiration field '" + spec.getCcExpirationField() + "' in document id '" + spec.getCcExpirationDocumentId() + "' but no such document was found.");
        		else if ( amountDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to use amount field '" + spec.getAmountField() + "' in document id '" + spec.getAmountDocumentId() + "' but no such document was found.");
        		else
        		{
            		String customerName = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, spec.customerNameFieldSpec); // optional
            		String description = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, spec.descriptionFieldSpec); // optional
            		String email = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, spec.emailFieldSpec); // optional
            		String invoiceNumber = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, spec.invoiceNumberFieldSpec); // optional
            		
            		if ( EsfString.isNonBlank(email) && ! EsfEmailAddress.isValidEmail(email) )
            		{
            			context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' email specification '" + spec.emailFieldSpec + "' resulted in an invalid email: " + email + ", so not used.");
                		email = "";
            		}
            		
            		EsfValue ccBillingFirstNameValue = context.transaction.getFieldValue(ccBillingFirstNameDocument.getEsfName(), spec.getCcBillingFirstNameField());
            		EsfValue ccBillingLastNameValue = context.transaction.getFieldValue(ccBillingLastNameDocument.getEsfName(), spec.getCcBillingLastNameField());
            		EsfValue ccBillingAddressValue = context.transaction.getFieldValue(ccBillingAddressDocument.getEsfName(), spec.getCcBillingAddressField());
            		EsfValue ccBillingAddress2Value = spec.hasCcBillingAddress2Specified() ? context.transaction.getFieldValue(ccBillingAddress2Document.getEsfName(), spec.getCcBillingAddress2Field()) : new EsfString("");
            		EsfValue ccBillingCityValue = context.transaction.getFieldValue(ccBillingCityDocument.getEsfName(), spec.getCcBillingCityField());
            		EsfValue ccBillingStateValue = context.transaction.getFieldValue(ccBillingStateDocument.getEsfName(), spec.getCcBillingStateField());
            		EsfValue ccBillingZipValue = context.transaction.getFieldValue(ccBillingZipDocument.getEsfName(), spec.getCcBillingZipField());
            		EsfValue ccBillingCountryCodeValue = spec.hasCcBillingCountryCodeSpecified() ? context.transaction.getFieldValue(ccBillingCountryCodeDocument.getEsfName(), spec.getCcBillingCountryCodeField()) : new EsfString("");
            		EsfValue ccTypeValue = spec.hasCcTypeSpecified() ? context.transaction.getFieldValue(ccTypeDocument.getEsfName(), spec.getCcTypeField()) : new EsfString("");
            		EsfValue ccAccountNumberValue = context.transaction.getFieldValue(ccAccountNumberDocument.getEsfName(), spec.getCcAccountNumberField());
            		EsfValue ccExpirationValue = context.transaction.getFieldValue(ccExpirationDocument.getEsfName(), spec.getCcExpirationField());
            		EsfValue ccSecurityIdValue = spec.hasCcSecurityIdSpecified() ? context.transaction.getFieldValue(ccSecurityIdDocument.getEsfName(), spec.getCcSecurityIdField()) : new EsfString("");
            		EsfValue amountValue = context.transaction.getFieldValue(amountDocument.getEsfName(), spec.getAmountField());

            		String ccBillingFirstName = ccBillingFirstNameValue == null || ccBillingFirstNameValue.isNull() ? "" : ccBillingFirstNameValue.toPlainString();
            		String ccBillingLastName = ccBillingLastNameValue == null || ccBillingLastNameValue.isNull() ? "" : ccBillingLastNameValue.toPlainString();
            		String ccBillingAddress = ccBillingAddressValue == null || ccBillingAddressValue.isNull() ? "" : ccBillingAddressValue.toPlainString();
            		String ccBillingAddress2 = ccBillingAddress2Value == null || ccBillingAddress2Value.isNull() ? "" : ccBillingAddress2Value.toPlainString();
            		String ccBillingCity = ccBillingCityValue == null || ccBillingCityValue.isNull() ? "" : ccBillingCityValue.toPlainString();
            		String ccBillingState = ccBillingStateValue == null || ccBillingStateValue.isNull() ? "" : ccBillingStateValue.toPlainString();
            		String ccBillingZip = ccBillingZipValue == null || ccBillingZipValue.isNull() ? "" : ccBillingZipValue.toPlainString();
            		String ccBillingCountryCode = ccBillingCountryCodeValue == null || ccBillingCountryCodeValue.isNull() ? "" : ccBillingCountryCodeValue.toPlainString();
            		String ccType = ccTypeValue == null || ccTypeValue.isNull() ? "" : ccTypeValue.toPlainString();
            		String ccAccountNumber = ccAccountNumberValue == null || ccAccountNumberValue.isNull() ? "" : ccAccountNumberValue.toPlainString();
            		EsfDate ccExpiration = ccExpirationValue == null || ccExpirationValue.isNull() || ! (ccExpirationValue instanceof EsfDate) ? null : (EsfDate)ccExpirationValue;
            		String ccSecurityId = ccSecurityIdValue == null || ccSecurityIdValue.isNull() ? "" : ccSecurityIdValue.toPlainString();
            		EsfMoney amount = amountValue == null || amountValue.isNull() || ! (amountValue instanceof EsfMoney) ? null : (EsfMoney)amountValue;
            		
             		if ( EsfString.isBlank(ccBillingFirstName) ) 
            		{
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' missing the required first name field.");
            			errors.addError("The credit card charge #" + specNumber + " could not take place because the required billing first name was missing.",spec.getCcBillingFirstNameField().toString());
            		} 
            		else if ( EsfString.isBlank(ccBillingLastName) ) 
            		{
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' missing the required last name field.");
            			errors.addError("The credit card charge #" + specNumber + " could not take place because the required billing last name was missing.",spec.getCcBillingLastNameField().toString());
            		} 
            		else if ( EsfString.isBlank(ccBillingAddress) ) 
            		{
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' missing the required billing address field.");
            			errors.addError("The credit card charge #" + specNumber + " could not take place because the required billing address was missing.",spec.getCcBillingAddressField().toString());
            		} 
            		else if ( EsfString.isBlank(ccBillingCity) ) 
            		{
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' missing the required billing city field.");
            			errors.addError("The credit card charge #" + specNumber + " could not take place because the required billing city was missing.",spec.getCcBillingCityField().toString());
            		} 
            		else if ( EsfString.isBlank(ccBillingState) ) 
            		{
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' missing the required billing state field.");
            			errors.addError("The credit card charge #" + specNumber + " could not take place because the required billing state/province was missing.",spec.getCcBillingStateField().toString());
            		} 
            		else if ( EsfString.isBlank(ccBillingZip) ) 
            		{
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' missing the required billing zip field.");
            			errors.addError("The credit card charge #" + specNumber + " could not take place because the required zip/postal code was missing.",spec.getCcBillingZipField().toString());
            		} 
            		else if ( EsfString.isBlank(ccAccountNumber) ) 
            		{
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' missing the required credit card number field.");
            			errors.addError("The credit card charge #" + specNumber + " could not take place because the required credit card number was missing.",spec.getCcAccountNumberField().toString());
            		} 
            		else if ( ccExpiration == null || ccExpiration.isNull() ) 
            		{
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' missing the required credit card expiration field.");
            			errors.addError("The credit card charge #" + specNumber + " could not take place because the required credit card expiration date was missing.",spec.getCcExpirationField().toString());
            		} 
            		else if ( amount == null || amount.isNull() ) 
            		{
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' missing the required amount to charge field.");
            			errors.addError("The credit card charge #" + specNumber + " could not take place because the required amount to charge was missing.",spec.getAmountField().toString());
            		} 
            		else if ( amount.isNegative() || amount.isZero() ) 
            		{
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' the amount to charge field is zero or negative: " + amount.toString());
            			errors.addError("The credit card charge #" + specNumber + " could not take place because the amount to charge is zero or negative.",spec.getAmountField().toString());
            		} 
            		else
            		{
            			NvpDirectPayment paypal = new NvpDirectPayment(con, context, errors);
            			try
            			{
            				paypal.doCreditCardPayments(customerName, description, email, invoiceNumber, 
            						ccType, ccBillingFirstName, ccBillingLastName, ccBillingAddress, ccBillingAddress2, 
            						ccBillingCity, ccBillingState, ccBillingZip, ccBillingCountryCode, 
            						ccAccountNumber, ccExpiration, ccSecurityId, amount, context.getIpOnlyAddress());
                			context.transaction.logActionBasic(con, "PAYPAL SUCCESS: Action '" + getEsfName() + "' PayPal Transaction ID #" + paypal.getTransactionId() + " for " + paypal.getAmountCharged() +
                					". AVS: " + paypal.getAvsCode() + " (" + paypal.getAvsCodeMeaning() + "). CVV2: " + paypal.getCvv2Match() + " (" + paypal.getCvv2MatchMeaning() + ").");
                			Record docRecord = context.currTransactionDocument.getRecord();
                			addPayPalResponseToRecord(docRecord,paypal,null);
                			
                			if ( EsfString.isNonBlank(spec.onSuccessMessageSpec) )
                			{
                				String msg = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, spec.onSuccessMessageSpec);
                				errors.addSuccess(msg);
                			}
                			
                			if ( spec.maskAccountNumberOnSuccess )
                			{
                				if ( ccAccountNumberValue instanceof EsfString )
                				{
	                				String masked = Application.getInstance().maskCreditCard(CreditCardValidator.toPrettyString(ccAccountNumberValue.toString()));
	                        		context.transaction.setFieldValue(ccAccountNumberDocument.getEsfName(), spec.getCcAccountNumberField(), new EsfString(masked));
	                    			context.transaction.logActionDetail(con, "PAYPAL SUCCESS: Action '" + getEsfName() + "' saved the masked the credit card number field: " + ccAccountNumberDocument.getEsfName() + "." + spec.getCcAccountNumberField() + "; to value: " + masked);
                				}
                    			else if ( ccAccountNumberValue instanceof EsfInteger )
                				{
                            		context.transaction.setFieldValue(ccAccountNumberDocument.getEsfName(), spec.getCcAccountNumberField(), new EsfInteger(0));
                           			context.transaction.logActionDetail(con, "PAYPAL SUCCESS: Action '" + getEsfName() + "' saved the zeroed out credit card number field: " + ccAccountNumberDocument.getEsfName() + "." + spec.getCcAccountNumberField());
                				}
                				else
                				{
                           			context.transaction.logActionDetail(con, "WARNING on PAYPAL SUCCESS: Action '" + getEsfName() + "' could not mask credit card security id field: " + ccAccountNumberDocument.getEsfName() + "." + spec.getCcAccountNumberField() + "; because it's not a String or Integer type field.");
                				}
                			}
                			
                			if ( spec.maskSecurityIdOnSuccess )
                			{
                				if ( spec.hasCcSecurityIdSpecified() )
                				{
                    				if ( ccSecurityIdValue instanceof EsfString )
                    				{
                        				String masked = ccSecurityIdValue.toPlainString();
                        				masked = Application.getInstance().mask(masked);
                                		context.transaction.setFieldValue(ccSecurityIdDocument.getEsfName(), spec.getCcSecurityIdField(), new EsfString(masked));
                            			context.transaction.logActionDetail(con, "PAYPAL SUCCESS: Action '" + getEsfName() + "' saved the masked the credit card security id field: " + ccSecurityIdDocument.getEsfName() + "." + spec.getCcSecurityIdField() + "; to value: " + masked);
                    				}
                    				else if ( ccSecurityIdValue instanceof EsfInteger )
                    				{
                                		context.transaction.setFieldValue(ccSecurityIdDocument.getEsfName(), spec.getCcSecurityIdField(), new EsfInteger(0));
                               			context.transaction.logActionDetail(con, "PAYPAL SUCCESS: Action '" + getEsfName() + "' saved the zeroed out credit card security id field: " + ccSecurityIdDocument.getEsfName() + "." + spec.getCcSecurityIdField());
                    				}
                    				else
                    				{
                               			context.transaction.logActionDetail(con, "WARNING on PAYPAL SUCCESS: Action '" + getEsfName() + "' could not mask credit card security id field: " + ccSecurityIdDocument.getEsfName() + "." + spec.getCcSecurityIdField() + "; because it's not a String or Integer type field.");
                    				}
                				}
                				else
                				{
                           			context.transaction.logActionDetail(con, "WARNING on PAYPAL SUCCESS: Action '" + getEsfName() + "' could not mask credit card security id as requested because the field was not specified.");
                				}
                			}
            			}
            			catch( EsfException e )
            			{
                			context.transaction.logActionBasic(con, "PAYPAL FAILURE: Action '" + getEsfName() + "' failed: PayPal amount " + paypal.getAmountCharged() + 
                					". AVS=" + paypal.getAvsCode() + " (" + paypal.getAvsCodeMeaning() + "). CVV2: " + paypal.getCvv2Match() + " (" + paypal.getCvv2MatchMeaning() + ")." + 
                					" Error: " + e.getMessage());
                			Record docRecord = context.currTransactionDocument.getRecord();
                			addPayPalResponseToRecord(docRecord,paypal,e);
                			
                			if ( EsfString.isNonBlank(spec.onFailureMessageSpec) )
                			{
                				String msg = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, spec.onFailureMessageSpec);
                				errors.addError(msg);
                			}
                			else
                				errors.addError(e.getMessage()); // we need some error to tell the system it failed.
            			}
            		}
            		
            		++specNumber;
        		}
        	}
    	}
    }
    
    void addPayPalResponseToRecord(Record record, NvpDirectPayment paypal, EsfException e)
    {
    	record.addUpdate( new EsfName("PAYPAL_ACK"), new EsfString(paypal.getAck()) );
    	record.addUpdate( new EsfName("PAYPAL_TRANSACTIONID"), new EsfString(paypal.getTransactionId()) );
    	record.addUpdate( new EsfName("PAYPAL_AMT"), new EsfString(paypal.getAmountCharged()) );
    	record.addUpdate( new EsfName("PAYPAL_AVSCODE"), new EsfString(paypal.getAvsCode()) );
    	record.addUpdate( new EsfName("PAYPAL_AVSCODE_MEANING"), new EsfString(paypal.getAvsCodeMeaning()) );
    	record.addUpdate( new EsfName("PAYPAL_CVV2MATCH"), new EsfString(paypal.getCvv2Match()) );
    	record.addUpdate( new EsfName("PAYPAL_CVV2MATCH_MEANING"), new EsfString(paypal.getCvv2MatchMeaning()) );
    	record.addUpdate( new EsfName("PAYPAL_EXCEPTION"), new EsfString(e==null ? (String)null : e.getMessage()) );
    	
    }
    
    @Override
    public String specsToString()
    {
    	StringBuilder buf = new StringBuilder(specList.size() * 80);
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(",");
    		buf.append(spec.amountField).append(" on ").append(spec.ccAccountNumberField);
     	}
    	return buf.toString();
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 60;
		for( Spec spec : specList )
		{
			size += (34*50) /* 34 entries each roughly 50 chars of XML tags */ + 
			    spec.customerNameFieldSpec.length() + spec.descriptionFieldSpec.length() + spec.emailFieldSpec.length() + spec.invoiceNumberFieldSpec.length() +
				spec.ccBillingFirstNameDocumentId.getEstimatedLengthXml() + spec.ccBillingFirstNameField.getEstimatedLengthXml() +
				spec.ccBillingLastNameDocumentId.getEstimatedLengthXml() + spec.ccBillingLastNameField.getEstimatedLengthXml() +
				spec.ccBillingAddressDocumentId.getEstimatedLengthXml() + spec.ccBillingAddressField.getEstimatedLengthXml() +
				spec.ccBillingCityDocumentId.getEstimatedLengthXml() + spec.ccBillingCityField.getEstimatedLengthXml() +
				spec.ccBillingStateDocumentId.getEstimatedLengthXml() + spec.ccBillingStateField.getEstimatedLengthXml() +
				spec.ccBillingZipDocumentId.getEstimatedLengthXml() + spec.ccBillingZipField.getEstimatedLengthXml() +
				spec.ccAccountNumberDocumentId.getEstimatedLengthXml() + spec.ccAccountNumberField.getEstimatedLengthXml() +
				spec.ccExpirationDocumentId.getEstimatedLengthXml() + spec.ccExpirationField.getEstimatedLengthXml() +
				spec.amountDocumentId.getEstimatedLengthXml() + spec.amountField.getEstimatedLengthXml() +
				spec.onSuccessMessageSpec.length() + spec.onFailureMessageSpec.length() + 10;
				size += spec.ccBillingAddress2DocumentId.getEstimatedLengthXml() + (spec.ccBillingAddress2Field==null ? 0 : spec.ccBillingAddress2Field.getEstimatedLengthXml());
				size += spec.ccBillingCountryCodeDocumentId.getEstimatedLengthXml() + (spec.ccBillingCountryCodeField==null ? 0 : spec.ccBillingCountryCodeField.getEstimatedLengthXml());
				size += spec.ccTypeDocumentId.getEstimatedLengthXml() + (spec.ccTypeField==null ? 0 : spec.ccTypeField.getEstimatedLengthXml());
				size += spec.ccSecurityIdDocumentId.getEstimatedLengthXml() + (spec.ccSecurityIdField==null ? 0 : spec.ccSecurityIdField.getEstimatedLengthXml());
		}
		if ( hasCondition() )
			size += condition.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Action type=\"PayPalDoDirectPaymentSaleAction\">\n");
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <customerNameFieldSpec>").append(XmlUtil.toEscapedXml(spec.customerNameFieldSpec)).append("</customerNameFieldSpec>\n");
			buf.append("  <descriptionFieldSpec>").append(XmlUtil.toEscapedXml(spec.descriptionFieldSpec)).append("</descriptionFieldSpec>\n");
			buf.append("  <emailFieldSpec>").append(XmlUtil.toEscapedXml(spec.emailFieldSpec)).append("</emailFieldSpec>\n");
			buf.append("  <invoiceNumberFieldSpec>").append(XmlUtil.toEscapedXml(spec.invoiceNumberFieldSpec)).append("</invoiceNumberFieldSpec>\n");
			buf.append("  <ccBillingFirstNameDocumentId>").append(spec.ccBillingFirstNameDocumentId.toXml()).append("</ccBillingFirstNameDocumentId>\n");
			buf.append("  <ccBillingFirstNameField>").append(spec.ccBillingFirstNameField.toXml()).append("</ccBillingFirstNameField>\n");
			buf.append("  <ccBillingLastNameDocumentId>").append(spec.ccBillingLastNameDocumentId.toXml()).append("</ccBillingLastNameDocumentId>\n");
			buf.append("  <ccBillingLastNameField>").append(spec.ccBillingLastNameField.toXml()).append("</ccBillingLastNameField>\n");
			buf.append("  <ccBillingAddressDocumentId>").append(spec.ccBillingAddressDocumentId.toXml()).append("</ccBillingAddressDocumentId>\n");
			buf.append("  <ccBillingAddressField>").append(spec.ccBillingAddressField.toXml()).append("</ccBillingAddressField>\n");
			buf.append("  <ccBillingAddress2DocumentId>").append(spec.ccBillingAddress2DocumentId.toXml()).append("</ccBillingAddress2DocumentId>\n");
			buf.append("  <ccBillingAddress2Field>").append(spec.ccBillingAddress2Field != null ? spec.ccBillingAddress2Field.toXml() : "").append("</ccBillingAddress2Field>\n");
			buf.append("  <ccBillingCityDocumentId>").append(spec.ccBillingCityDocumentId.toXml()).append("</ccBillingCityDocumentId>\n");
			buf.append("  <ccBillingCityField>").append(spec.ccBillingCityField.toXml()).append("</ccBillingCityField>\n");
			buf.append("  <ccBillingStateDocumentId>").append(spec.ccBillingStateDocumentId.toXml()).append("</ccBillingStateDocumentId>\n");
			buf.append("  <ccBillingStateField>").append(spec.ccBillingStateField.toXml()).append("</ccBillingStateField>\n");
			buf.append("  <ccBillingZipDocumentId>").append(spec.ccBillingZipDocumentId.toXml()).append("</ccBillingZipDocumentId>\n");
			buf.append("  <ccBillingZipField>").append(spec.ccBillingZipField.toXml()).append("</ccBillingZipField>\n");
			buf.append("  <ccBillingCountryCodeDocumentId>").append(spec.ccBillingCountryCodeDocumentId.toXml()).append("</ccBillingCountryCodeDocumentId>\n");
			buf.append("  <ccBillingCountryCodeField>").append(spec.ccBillingCountryCodeField != null ? spec.ccBillingCountryCodeField.toXml() : "").append("</ccBillingCountryCodeField>\n");
			buf.append("  <ccTypeDocumentId>").append(spec.ccTypeDocumentId.toXml()).append("</ccTypeDocumentId>\n");
			buf.append("  <ccTypeField>").append(spec.ccTypeField != null ? spec.ccTypeField.toXml() : "").append("</ccTypeField>\n");
			buf.append("  <ccAccountNumberDocumentId>").append(spec.ccAccountNumberDocumentId.toXml()).append("</ccAccountNumberDocumentId>\n");
			buf.append("  <ccAccountNumberField>").append(spec.ccAccountNumberField.toXml()).append("</ccAccountNumberField>\n");
			buf.append("  <ccExpirationDocumentId>").append(spec.ccExpirationDocumentId.toXml()).append("</ccExpirationDocumentId>\n");
			buf.append("  <ccExpirationField>").append(spec.ccExpirationField.toXml()).append("</ccExpirationField>\n");
			buf.append("  <ccSecurityIdDocumentId>").append(spec.ccSecurityIdDocumentId.toXml()).append("</ccSecurityIdDocumentId>\n");
			buf.append("  <ccSecurityIdField>").append(spec.ccSecurityIdField != null ? spec.ccSecurityIdField.toXml() : "").append("</ccSecurityIdField>\n");
			buf.append("  <amountDocumentId>").append(spec.amountDocumentId.toXml()).append("</amountDocumentId>\n");
			buf.append("  <amountField>").append(spec.amountField.toXml()).append("</amountField>\n");
			buf.append("  <onSuccessMessageSpec>").append(XmlUtil.toEscapedXml(spec.onSuccessMessageSpec)).append("</onSuccessMessageSpec>\n");
			buf.append("  <onFailureMessageSpec>").append(XmlUtil.toEscapedXml(spec.onFailureMessageSpec)).append("</onFailureMessageSpec>\n");
			buf.append("  <maskAccountNumberOnSuccess>").append(spec.maskAccountNumberOnSuccess).append("</maskAccountNumberOnSuccess>\n");
			buf.append("  <maskSecurityIdOnSuccess>").append(spec.maskSecurityIdOnSuccess).append("</maskSecurityIdOnSuccess>\n");
			buf.append(" </Spec>\n");
		}
		super.appendXml(buf);
		buf.append("</Action>\n");
		return buf;
	}
   
}