// Copyright (C) 2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.TransactionTimer;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.XmlUtil;

/**
* Action is the base class of all actions used by the transaction engine.
* TimerAction is used to set or cancel a transaction timer or cancel all timers for a transaction.
* 
* @author Yozons, Inc.
*/
public class TimerAction extends Action
{
	private static final long serialVersionUID = -2919895458813833696L;

	public static final String CANCEL_TIMER_TYPE = "cancelTimer";
	public static final String CANCEL_ALL_TIMERS_TYPE = "cancelAllTimers";
	public static final String SET_TIMER_TYPE = "setTimer";
	
	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = -6219993022911268484L;

		public Spec(int o, String timerType, String timerName, String reasonSpec, String expireTimestampSpec )
		{
			order = o;
			setTimerType(timerType);
			setTimerName(timerName);
			setReasonSpec(reasonSpec);
			setExpireTimestampSpec(expireTimestampSpec);
		}
		int order;
		String timerType;
		String timerName;
		String reasonSpec; // may contain field specs that we'll expand first
		
		String expireTimestampSpec; // specified only for SET_TIMER_TYPE
		
		public Spec duplicate() 
		{
			return new Spec(order,timerType,timerName,reasonSpec,
					        isSetTimerType() ? expireTimestampSpec : "");
		}
		public int getOrder() { return order; }
		public void setOrder(int v) { order = v; }
		public String getTimerType() { return timerType; }
		public boolean isCancelTimerType() { return CANCEL_TIMER_TYPE.equals(timerType); }
		public boolean isCancelAllTimersType() { return CANCEL_ALL_TIMERS_TYPE.equals(timerType); }
		public boolean isSetTimerType() { return SET_TIMER_TYPE.equals(timerType); }
		public void setTimerType(String v) { timerType = v == null ? "" : v.trim(); }
		public String getTimerName() { return timerName; }
		public void setTimerName(String v) { timerName = EsfString.isBlank(v) ? TransactionTimer.DEFAULT_NAME : v.trim(); }
		public String getReasonSpec() { return reasonSpec; }
		public void setReasonSpec(String v) { reasonSpec = v == null ? "" : v.trim(); }
		
		// Only for SET_TIMER_TYPE
		public String getExpireTimestampSpec() { return expireTimestampSpec; }
		public void setExpireTimestampSpec(String v) { expireTimestampSpec = v == null ? "" : v.trim(); }
	}
	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates an TimerAction object
     */
    public TimerAction()
    {
    }
    
    public TimerAction(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		super(element,ns,_logger);
		try 
		{
			String elementName = element.getName();
			if (!"Action".equals(elementName)) 
				throw new EsfException("The Action tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!"TimerAction".equals(typeAttr)) 
				throw new EsfException("The Action tag has type: " + typeAttr + "; expected TimerAction.");

			List<Element> specElementList = element.getChildren("Spec", ns);
			ListIterator<Element> iter = specElementList.listIterator();
			int order = 1;
			while (iter.hasNext()) 
			{
				Element specElement = iter.next();
				
				String timerType = specElement.getChildTextTrim("timerType", ns);
				String timerName = specElement.getChildTextTrim("timerName", ns);
				String reasonSpec = specElement.getChildTextTrim("reasonSpec", ns);
				String expireTimestampSpec = specElement.getChildTextTrim("expireTimestampSpec", ns);

				Spec spec = new Spec(order++, timerType, timerName, reasonSpec, expireTimestampSpec);
				specList.add(spec);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }
    
    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }
    
    public TimerAction duplicate()
    {
    	TimerAction action = new TimerAction();
    	for( Spec spec : specList )
    		action.specList.add( spec.duplicate() );
    	if ( hasCondition() )
    		action.condition = condition.duplicate();
    	return action;
    }
    
    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {	
    	super.updatePackagePartyIds(partyIdMapping);
    	// we have no package party id mappings
    }

    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	// we have no document id mappings
    }    
    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }

    
    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	if ( specList.size() == 0 )
    	{
    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' has no timer specifications.");
    	}
    	else
    	{
        	for( Spec spec : specList ) 
        	{
        		String reasonText = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion,spec.getReasonSpec());
        		if ( spec.isCancelAllTimersType() )
        		{
           			int numDeleted = context.transaction.deleteAllTimers(con);
           			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' canceled all (" + numDeleted + ") timers previously set; reason: " + reasonText);            				
        		}
        		else if ( spec.isCancelTimerType() )
        		{
           			boolean deleted = context.transaction.deleteTimer(con,spec.getTimerName());
           			if ( deleted )
           				context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' canceled timer '" + spec.getTimerName() + "'; reason: " + reasonText);            				
           			else
           				context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' failed to cancel named timer '" + spec.getTimerName() + "'; reason: " + reasonText);            				
        		}
        		else if ( spec.isSetTimerType() )
        		{
            		String timerSpec = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, spec.getExpireTimestampSpec());
        			EsfDateTime expireTimestamp = EsfDateTime.CreateGuessFormat(timerSpec); // should be in date format with HH:MM[:SS] [TZ] following it, or our special cases: now or +numDays or -numDays;
           			boolean timerSet = context.transaction.setTimer(con,spec.getTimerName(),expireTimestamp);
           			if ( timerSet )
           				context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set timer '" + spec.getTimerName() + "'; using timer spec: " + spec.getExpireTimestampSpec() + "; calculated expireTimestamp: " + expireTimestamp + "; reason: " + reasonText);            				
           			else
           				context.transaction.logActionDetail(con, "ERROR: Action '" + getEsfName() + "' failed to set named timer '" + spec.getTimerName() + "'; using timer spec: " + spec.getExpireTimestampSpec() + "; calculated expireTimestamp: " + expireTimestamp + "; reason: " + reasonText);            				
        		}
        		else
        		{
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' specifies an unexpected type: '" + spec.getTimerType() + "'.");            				
        		}
        	}
    	}
    }
    
    @Override
    public String specsToString()
    {
    	StringBuilder buf = new StringBuilder(specList.size() * 80);
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(",");
    		if ( spec.isCancelAllTimersType() )
        		buf.append(spec.timerType);
    		else
    			buf.append(spec.timerType).append(" (").append(spec.timerName).append(")");
     	}
    	return buf.toString();
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 40;
		for( Spec spec : specList )
		{
			size += 142 + spec.timerType.length() + spec.timerName.length() + spec.reasonSpec.length() + spec.expireTimestampSpec.length();
		}
		
		if ( hasCondition() )
			size += condition.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Action type=\"TimerAction\">\n");
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <timerType>").append(XmlUtil.toEscapedXml(spec.timerType)).append("</timerType>\n");
			buf.append("  <timerName>").append(XmlUtil.toEscapedXml(spec.timerName)).append("</timerName>\n");
			buf.append("  <reasonSpec>").append(XmlUtil.toEscapedXml(spec.reasonSpec)).append("</reasonSpec>\n");
			buf.append("  <expireTimestampSpec>").append(XmlUtil.toEscapedXml(spec.expireTimestampSpec)).append("</expireTimestampSpec>\n");
			buf.append(" </Spec>\n");
		}
		super.appendXml(buf);
		buf.append("</Action>\n");
		return buf;
	}
   
}