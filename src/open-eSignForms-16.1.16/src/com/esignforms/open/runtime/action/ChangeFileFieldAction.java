// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.XmlUtil;

/**
* Action is the base class of all actions used by the transaction engine.
* ChangeFileFieldAction is used to change the processing logic for uploaded files for specific parties.
* 
* @author Yozons, Inc.
*/
public class ChangeFileFieldAction extends Action
{
	private static final long serialVersionUID = 8187258349264028986L;

	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = -663594288960169829L;

		public static final String CHANGE_TYPE_VIEW_DOWNLOAD_OPTIONAL   = "viewOptional";
		public static final String CHANGE_TYPE_VIEW_DOWNLOAD_PROHIBITED = "viewProhibited";
		
		public Spec(int o, EsfUUID targetDocumentId, EsfName targetField, EsfUUID targetPartyId, String changeType)
		{
			order = o;
			this.targetDocumentId = targetDocumentId;
			this.targetField = targetField;
			this.targetPartyId = targetPartyId;
			this.changeType = changeType;
		}
		int order;
		EsfUUID targetDocumentId;
		EsfName targetField;
		EsfUUID targetPartyId;
		String changeType;
		public Spec duplicate() 
		{
			return new Spec(order,targetDocumentId,targetField.duplicate(),targetPartyId,changeType);
		}
		public int getOrder() { return order; }
		public void setOrder(int v) { order = v; }
		public EsfUUID getTargetDocumentId() { return targetDocumentId; }
		public void setTargetDocumentId(EsfUUID v) { targetDocumentId = v; }
		public EsfName getTargetField() { return targetField; }
		public void setTargetField(EsfName v) { targetField = v; }
		public EsfUUID getTargetPartyId() { return targetPartyId; }
		public void setTargetPartyId(EsfUUID v) { targetPartyId = v; }
		public String getChangeType() { return changeType; }
		public boolean isChangeTypeViewDownloadOptional()   { return CHANGE_TYPE_VIEW_DOWNLOAD_OPTIONAL.equals(changeType); }
		public boolean isChangeTypeViewDownloadProhibited() { return CHANGE_TYPE_VIEW_DOWNLOAD_PROHIBITED.equals(changeType); }
		public void setChangeType(String v) { changeType = v; }
	}
	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates an ChangeFileFieldAction object
     */
    public ChangeFileFieldAction()
    {
    }
    
    public ChangeFileFieldAction(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		super(element,ns,_logger);
		try 
		{
			String elementName = element.getName();
			if (!"Action".equals(elementName)) 
				throw new EsfException("The Action tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!"ChangeFileFieldAction".equals(typeAttr)) 
				throw new EsfException("The Action tag has type: " + typeAttr + "; expected ChangeFileFieldAction.");
			
			List<Element> specElementList = element.getChildren("Spec", ns);
			ListIterator<Element> iter = specElementList.listIterator();
			int order = 1;
			while (iter.hasNext()) 
			{
				Element specElement = iter.next();
				
				EsfUUID targetDocumentId = new EsfUUID(specElement.getChildTextTrim("targetDocumentId", ns)); 
				EsfName targetField = new EsfName(specElement.getChildTextTrim("targetField", ns)); 
				EsfUUID targetPartyId = new EsfUUID(specElement.getChildTextTrim("targetPartyId", ns)); 
				String changeType = specElement.getChildTextTrim("changeType", ns); 
				
				Spec spec = new Spec(order++, targetDocumentId, targetField, targetPartyId, changeType);
				specList.add(spec);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }
    
    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }
    
    public ChangeFileFieldAction duplicate()
    {
    	ChangeFileFieldAction action = new ChangeFileFieldAction();
    	for( Spec spec : specList )
    	{
    		action.specList.add( spec.duplicate() );
    	}
    	if ( hasCondition() )
    		action.condition = condition.duplicate();
    	return action;
    }
    
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	super.updatePackagePartyIds(partyIdMapping);
    	for( Spec spec : specList )
    	{
    		EsfUUID partyId = partyIdMapping.get(spec.getTargetPartyId());
    		if ( partyId != null )
    			spec.setTargetPartyId(partyId);
    	}
    }

    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	for( Spec spec : specList )
    	{
    		EsfUUID newDocId = documentIdMapping.get(spec.getTargetDocumentId());
    		if ( newDocId != null )
    			spec.setTargetDocumentId(newDocId);
    	}
    }

    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }

    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	if ( specList.size() == 0 )
    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' had no fields specified to be set.");
    	else
    	{
        	for( Spec spec : specList ) 
        	{
        		Document document = context.getDocument(spec.getTargetDocumentId());
        		if ( document == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to change File field '" + spec.getTargetField() + "' in document id '" + spec.getTargetDocumentId() + "' but no such document was found.");
        		else
        		{
        			PackageVersionPartyTemplate pvpt = context.pkgVersion.getPackageVersionPartyTemplate(spec.getTargetPartyId());
        			if ( pvpt == null )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to change File field '" + spec.getTargetField() + "' in document id '" + spec.getTargetDocumentId() + "' for party id '" + spec.getTargetPartyId() + "' but no such party was found.");
        			else
        			{
        				FieldTemplate ft = context.getFieldTemplate(spec.getTargetDocumentId(), spec.getTargetField());
        				if ( ft == null || ! ft.isTypeFile() )
        				{
                			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to change File field '" + spec.getTargetField() + "' in document '" + document.getEsfName() + "' for party '" + pvpt.getEsfName() + "' but no such File field was found.");
        				}
        				else
        				{
                			if ( spec.isChangeTypeViewDownloadOptional() )
                			{
                				context.changeFileViewDownloadOptionalForParty(document.getEsfName(), spec.getTargetField(), spec.getTargetPartyId());
                    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' changed File field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to make view/download optional for party " + pvpt.getEsfName());            				
                			}
                			else if ( spec.isChangeTypeViewDownloadProhibited() ) 
                			{
                				context.changeFileViewDownloadProhibitedForParty(document.getEsfName(), spec.getTargetField(), spec.getTargetPartyId());
                    			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' changed File field '" + spec.getTargetField() + "' in document '" + document.getEsfName() +"' to make view/download prohibited for party " + pvpt.getEsfName());            				
                			}
                			else
                			{
                    			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' wanted to change File field '" + spec.getTargetField() + "' in document '" + document.getEsfName() + "' for party '" + pvpt.getEsfName() + "' but the change type was not known: " + spec.getChangeType());
                			}
        				}
        			}
        		}
        	}
    	}
    }

    @Override
    public String specsToString()
    {
    	StringBuilder buf = new StringBuilder(specList.size() * 80);
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(',');
    		Document document = Document.Manager.getById(spec.getTargetDocumentId());
    		String documentName = document == null ? "?" : document.getEsfName().toString();
    		buf.append(documentName).append('.').append(spec.targetField).append("=>").append(spec.changeType);
     	}
    	return buf.toString();
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 50;
		for( Spec spec : specList )
		{
			size += 50 + spec.targetDocumentId.getEstimatedLengthXml() + spec.targetField.getEstimatedLengthXml() + spec.targetPartyId.getEstimatedLengthXml() + spec.changeType.length();
		}
		if ( hasCondition() )
			size += condition.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Action type=\"ChangeFileFieldAction\">\n");
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <targetDocumentId>").append(spec.targetDocumentId.toXml()).append("</targetDocumentId>\n");
			buf.append("  <targetField>").append(spec.targetField.toXml()).append("</targetField>\n");
			buf.append("  <targetPartyId>").append(spec.targetPartyId.toXml()).append("</targetPartyId>\n");
			buf.append("  <changeType>").append(XmlUtil.toEscapedXml(spec.changeType)).append("</changeType>\n");
			buf.append(" </Spec>\n");
		}
		super.appendXml(buf);
		buf.append("</Action>\n");
		return buf;
	}
   
}