// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime.action;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDecimal;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfMoney;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionDocument.INITIALIZE_RECORD;
import com.esignforms.open.runtime.workflow.TransactionContext;

/**
* Action is the base class of all actions used by the transaction engine.
* CalculateMonthlyPaymentAction is a financial calculation that determines the monthly payment based on the amount financed, APR and number of payments (monthly).
* 
* @author Yozons, Inc.
*/
public class CalculateMonthlyPaymentAction extends Action
{
	private static final long serialVersionUID = -5909536662622694853L;

	public static class Spec implements java.io.Serializable
	{
		private static final long serialVersionUID = 3900865204866734548L;

		public Spec(int o, EsfUUID targetMonthlyPaymentDocumentId, EsfName targetMonthlyPaymentField,
				EsfUUID amountFinancedDocumentId, EsfName amountFinancedField,
				EsfUUID annualInterestRateDocumentId, EsfName annualInterestRateField,
				EsfUUID numberMonthlyPaymentsDocumentId, EsfName numberMonthlyPaymentsField)
		{
			order = o;
			this.targetMonthlyPaymentDocumentId = targetMonthlyPaymentDocumentId;
			this.targetMonthlyPaymentField = targetMonthlyPaymentField;
			this.amountFinancedDocumentId = amountFinancedDocumentId;
			this.amountFinancedField = amountFinancedField;
			this.annualInterestRateDocumentId = annualInterestRateDocumentId;
			this.annualInterestRateField = annualInterestRateField;
			this.numberMonthlyPaymentsDocumentId = numberMonthlyPaymentsDocumentId;
			this.numberMonthlyPaymentsField = numberMonthlyPaymentsField;
		}
		int order;
		EsfUUID targetMonthlyPaymentDocumentId;
		EsfName targetMonthlyPaymentField;
		EsfUUID amountFinancedDocumentId;
		EsfName amountFinancedField;
		EsfUUID annualInterestRateDocumentId;
		EsfName annualInterestRateField;
		EsfUUID numberMonthlyPaymentsDocumentId;
		EsfName numberMonthlyPaymentsField;

		public Spec duplicate() 
		{
			return new Spec(order,targetMonthlyPaymentDocumentId,targetMonthlyPaymentField.duplicate(),
					amountFinancedDocumentId,amountFinancedField.duplicate(),
					annualInterestRateDocumentId,annualInterestRateField.duplicate(),
					numberMonthlyPaymentsDocumentId,numberMonthlyPaymentsField.duplicate());
		}
		public int getOrder() { return order; }
		public void setOrder(int v) { order = v; }
		
		public EsfUUID getTargetMonthlyPaymentDocumentId() { return targetMonthlyPaymentDocumentId; }
		public void setTargetMonthlyPaymentDocumentId(EsfUUID v) { targetMonthlyPaymentDocumentId = v; }
		public EsfName getTargetMonthlyPaymentField() { return targetMonthlyPaymentField; }
		public void setTargetMonthlyPaymentField(EsfName v) { targetMonthlyPaymentField = v; }
		
		public EsfUUID getAmountFinancedDocumentId() { return amountFinancedDocumentId; }
		public void setAmountFinancedDocumentId(EsfUUID v) { amountFinancedDocumentId = v; }
		public EsfName getAmountFinancedField() { return amountFinancedField; }
		public void setAmountFinancedField(EsfName v) { amountFinancedField = v; }
		
		public EsfUUID getAnnualInterestRateDocumentId() { return annualInterestRateDocumentId; }
		public void setAnnualInterestRateDocumentId(EsfUUID v) { annualInterestRateDocumentId = v; }
		public EsfName getAnnualInterestRateField() { return annualInterestRateField; }
		public void setAnnualInterestRateField(EsfName v) { annualInterestRateField = v; }
		
		public EsfUUID getNumberMonthlyPaymentsDocumentId() { return numberMonthlyPaymentsDocumentId; }
		public void setNumberMonthlyPaymentsDocumentId(EsfUUID v) { numberMonthlyPaymentsDocumentId = v; }
		public EsfName getNumberMonthlyPaymentsField() { return numberMonthlyPaymentsField; }
		public void setNumberMonthlyPaymentsField(EsfName v) { numberMonthlyPaymentsField = v; }
	}
	LinkedList<Spec> specList = new LinkedList<Spec>();
	
    /**
     * This creates an CalculateMonthlyPaymentAction object
     */
    public CalculateMonthlyPaymentAction()
    {
    }
    
    public CalculateMonthlyPaymentAction(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException
    {
		super(element,ns,_logger);
		try 
		{
			String elementName = element.getName();
			if (!"Action".equals(elementName)) 
				throw new EsfException("The Action tag is missing.");
			
			String typeAttr = element.getAttributeValue("type");
			if(!"CalculateMonthlyPaymentAction".equals(typeAttr)) 
				throw new EsfException("The Action tag has type: " + typeAttr + "; expected CalculateMonthlyPaymentAction.");

			List<Element> specElementList = element.getChildren("Spec", ns);
			ListIterator<Element> specIter = specElementList.listIterator();
			int order = 1;
			while( specIter.hasNext() ) 
			{
				Element specElement = specIter.next();
				
				EsfUUID targetMonthlyPaymentDocumentId = new EsfUUID(specElement.getChildTextTrim("targetMonthlyPaymentDocumentId", ns)); 
				EsfName targetMonthlyPaymentField = new EsfName(specElement.getChildTextTrim("targetMonthlyPaymentField", ns)); 
				
				EsfUUID amountFinancedDocumentId = new EsfUUID(specElement.getChildTextTrim("amountFinancedDocumentId", ns)); 
				EsfName amountFinancedField = new EsfName(specElement.getChildTextTrim("amountFinancedField", ns)); 
				
				EsfUUID annualInterestRateDocumentId = new EsfUUID(specElement.getChildTextTrim("annualInterestRateDocumentId", ns)); 
				EsfName annualInterestRateField = new EsfName(specElement.getChildTextTrim("annualInterestRateField", ns)); 
				
				EsfUUID numberMonthlyPaymentsDocumentId = new EsfUUID(specElement.getChildTextTrim("numberMonthlyPaymentsDocumentId", ns)); 
				EsfName numberMonthlyPaymentsField = new EsfName(specElement.getChildTextTrim("numberMonthlyPaymentsField", ns)); 
				
				Spec spec = new Spec(order++, targetMonthlyPaymentDocumentId, targetMonthlyPaymentField, 
						amountFinancedDocumentId, amountFinancedField, 
						annualInterestRateDocumentId, annualInterestRateField, 
						numberMonthlyPaymentsDocumentId, numberMonthlyPaymentsField);
				specList.add(spec);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
    }
    
    public List<Spec> getSpecList()
    {
    	return specList;
    }
    public void setSpecList(List<Spec> v)
    {
    	specList = new LinkedList<Spec>(v);
    	setObjectChanged();
    }
    
    public CalculateMonthlyPaymentAction duplicate()
    {
    	CalculateMonthlyPaymentAction action = new CalculateMonthlyPaymentAction();
    	for( Spec spec : specList )
    		action.specList.add( spec.duplicate() );
    	if ( hasCondition() )
    		action.condition = condition.duplicate();
    	return action;
    }
    
    @Override
    public void updatePackagePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	super.updatePackagePartyIds(partyIdMapping);
    	// we have no party id mappings
    }
    
    @Override
    public void updateDocumentIds(Map<EsfUUID,EsfUUID> documentIdMapping)
    {
    	super.updateDocumentIds(documentIdMapping);
    	for( Spec spec : specList )
    	{
    		EsfUUID newDocId = documentIdMapping.get(spec.getTargetMonthlyPaymentDocumentId());
    		if ( newDocId != null )
    			spec.setTargetMonthlyPaymentDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getAmountFinancedDocumentId());
    		if ( newDocId != null )
    			spec.setAmountFinancedDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getAnnualInterestRateDocumentId());
    		if ( newDocId != null )
    			spec.setAnnualInterestRateDocumentId(newDocId);
    		newDocId = documentIdMapping.get(spec.getNumberMonthlyPaymentsDocumentId());
    		if ( newDocId != null )
    			spec.setNumberMonthlyPaymentsDocumentId(newDocId);
    	}
    }
    
    @Override
    public void updateDocumentVersionPageIds(Map<EsfUUID,EsfUUID> documentVersionPageIdMapping)
    {
    	super.updateDocumentVersionPageIds(documentVersionPageIdMapping);
    	// we have no document version page id mappings
    }

    
    @Override
    public void doAction(Connection con, TransactionContext context, Errors errors) throws SQLException, EsfException
    {
    	if ( specList.size() == 0 )
    		context.transaction.logActionBasic(con, "WARNING: Action '" + getEsfName() + "' had no fields specified to be set by the calculation.");
    	else
    	{
        	for( Spec spec : specList ) 
        	{
        		Document targetMonthlyPaymentDocument = context.getDocument(spec.getTargetMonthlyPaymentDocumentId());
        		Document amountFinancedDocument = context.getDocument(spec.getAmountFinancedDocumentId());
        		Document annualInterestRateDocument = context.getDocument(spec.getAnnualInterestRateDocumentId());
        		Document numberMonthlyPaymentsDocument = context.getDocument(spec.getNumberMonthlyPaymentsDocumentId());
        		if ( targetMonthlyPaymentDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set calculated field '" + spec.getTargetMonthlyPaymentField() + "' in document id '" + spec.getTargetMonthlyPaymentDocumentId() + "' but no such document was found.");
        		else if ( amountFinancedDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetMonthlyPaymentField() + "' in document id '" + spec.getTargetMonthlyPaymentDocumentId() + "' with amount financed document id '" + spec.getAmountFinancedDocumentId() + "' but no such amount financed document was found.");
        		else if ( annualInterestRateDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetMonthlyPaymentField() + "' in document id '" + spec.getTargetMonthlyPaymentDocumentId() + "' with annual interest rate document id '" + spec.getAnnualInterestRateDocumentId() + "' but no such amount financed document was found.");
        		else if ( numberMonthlyPaymentsDocument == null )
        			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetMonthlyPaymentField() + "' in document id '" + spec.getTargetMonthlyPaymentDocumentId() + "' with number monthly payments document id '" + spec.getNumberMonthlyPaymentsDocumentId() + "' but no such amount financed document was found.");
        		else
        		{
            		EsfValue targetMonthlyPaymentValue = context.transaction.getFieldValue(targetMonthlyPaymentDocument.getEsfName(), spec.getTargetMonthlyPaymentField());
            		EsfValue amountFinancedValue = context.transaction.getFieldValue(amountFinancedDocument.getEsfName(), spec.getAmountFinancedField());
            		EsfValue annualInterestRateValue = context.transaction.getFieldValue(annualInterestRateDocument.getEsfName(), spec.getAnnualInterestRateField());
            		EsfValue numberMonthlyPaymentsValue = context.transaction.getFieldValue(numberMonthlyPaymentsDocument.getEsfName(), spec.getNumberMonthlyPaymentsField());

            		// If there's no value to set on our first look, let's try to re-initialize the record to create any missing fields 
            		// Generally these are fields that were added into a document after the transaction has already been started.
            		if ( targetMonthlyPaymentValue == null ) 
            		{
            			context.transaction.logActionDetail(con, "WARNING: Action '" + getEsfName() + "' tried to set field '" + spec.getTargetMonthlyPaymentField() + "' in document '" + targetMonthlyPaymentDocument.getEsfName() +"' but it was not found; trying to create missing/newly-added fields.");
            			TransactionDocument retryTranDoc = context.transaction.getTransactionDocument(targetMonthlyPaymentDocument.getEsfName());
        				DocumentVersion retryDocVer = context.transaction.getDocumentVersion(targetMonthlyPaymentDocument);
            			if ( retryTranDoc != null && retryDocVer != null )
            			{
            				retryTranDoc.initializeRecord(context.user, context.transaction, targetMonthlyPaymentDocument, retryDocVer, INITIALIZE_RECORD.ONLY_MISSING_FIELDS);
            				targetMonthlyPaymentValue = context.transaction.getFieldValue(targetMonthlyPaymentDocument.getEsfName(), spec.getTargetMonthlyPaymentField());
            			}
            		}
            		if ( targetMonthlyPaymentValue == null )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set calculated monthly payment field '" + spec.getTargetMonthlyPaymentField() + "' in document '" + targetMonthlyPaymentDocument.getEsfName() + "' but no such field was found.");
            		else if ( ! (targetMonthlyPaymentValue instanceof EsfMoney) )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set calculated monthly payment field '" + spec.getTargetMonthlyPaymentField() + "' in document '" + targetMonthlyPaymentDocument.getEsfName() + "' but field is not money. Unexpected value type: " + targetMonthlyPaymentValue.getType());
            		else if ( amountFinancedValue == null )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set calculated monthly payment field '" + spec.getTargetMonthlyPaymentField() + "' in document '" + targetMonthlyPaymentDocument.getEsfName() + "' using amount financed field '" + spec.getAmountFinancedField() + "' in document '" + amountFinancedDocument.getEsfName() + "' but no such amount financed field was found.");
            		else if ( ! (amountFinancedValue instanceof EsfMoney) )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set calculated monthly payment field '" + spec.getTargetMonthlyPaymentField() + "' in document '" + targetMonthlyPaymentDocument.getEsfName() + "' using amount financed field '" + spec.getAmountFinancedField() + "' in document '" + amountFinancedDocument.getEsfName() + "' but is not money. Unexpected value type: " + amountFinancedValue.getType());
            		else if ( annualInterestRateValue == null )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set calculated monthly payment field '" + spec.getTargetMonthlyPaymentField() + "' in document '" + targetMonthlyPaymentDocument.getEsfName() + "' using annual interest rate field '" + spec.getAnnualInterestRateField() + "' in document '" + annualInterestRateDocument.getEsfName() + "' but no such annual interest rate field was found.");
            		else if ( ! (annualInterestRateValue instanceof EsfDecimal) )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set calculated monthly payment field '" + spec.getTargetMonthlyPaymentField() + "' in document '" + targetMonthlyPaymentDocument.getEsfName() + "' using annual interest rate field '" + spec.getAnnualInterestRateField() + "' in document '" + annualInterestRateDocument.getEsfName() + "' but is not decimal. Unexpected value type: " + annualInterestRateValue.getType());
            		else if ( numberMonthlyPaymentsValue == null )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set calculated monthly payment field '" + spec.getTargetMonthlyPaymentField() + "' in document '" + targetMonthlyPaymentDocument.getEsfName() + "' using number monthly payments field '" + spec.getNumberMonthlyPaymentsField() + "' in document '" + numberMonthlyPaymentsDocument.getEsfName() + "' but no such number monthly payments field was found.");
            		else if ( ! (numberMonthlyPaymentsValue instanceof EsfInteger) )
            			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set calculated monthly payment field '" + spec.getTargetMonthlyPaymentField() + "' in document '" + targetMonthlyPaymentDocument.getEsfName() + "' using number monthly payments field '" + spec.getNumberMonthlyPaymentsField() + "' in document '" + numberMonthlyPaymentsDocument.getEsfName() + "' but is not integer. Unexpected value type: " + numberMonthlyPaymentsValue.getType());
            		else
            		{
            			EsfMoney monthlyPayment = null;
            			
            			EsfMoney amountFinanced = new EsfMoney((EsfMoney)amountFinancedValue);
            			EsfDecimal apr = new EsfDecimal((EsfDecimal)annualInterestRateValue);
            			EsfInteger numberOfMonthlyPayments = new EsfInteger((EsfInteger)numberMonthlyPaymentsValue);
            			
            			if ( amountFinanced.isNull() || amountFinanced.isNegative() )
                			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set calculated monthly payment field '" + spec.getTargetMonthlyPaymentField() + "' in document '" + targetMonthlyPaymentDocument.getEsfName() + "' using amount financed field '" + spec.getAmountFinancedField() + "' in document '" + amountFinancedDocument.getEsfName() + "' but amount financed field had unexpected value: " + amountFinancedValue);
            			else if ( apr.isNull() || apr.isNegative() )
                			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set calculated monthly payment field '" + spec.getTargetMonthlyPaymentField() + "' in document '" + targetMonthlyPaymentDocument.getEsfName() + "' using annual interest rate field '" + spec.getAnnualInterestRateField() + "' in document '" + annualInterestRateDocument.getEsfName() + "' but annual interest rate field had unexpected value: " + annualInterestRateValue);
            			else if ( numberOfMonthlyPayments.isNull() || numberOfMonthlyPayments.isNegative() || numberOfMonthlyPayments.isZero() )
                			context.transaction.logActionBasic(con, "ERROR: Action '" + getEsfName() + "' tried to set calculated monthly payment field '" + spec.getTargetMonthlyPaymentField() + "' in document '" + targetMonthlyPaymentDocument.getEsfName() + "' using number monthly payments field '" + spec.getNumberMonthlyPaymentsField() + "' in document '" + numberMonthlyPaymentsDocument.getEsfName() + "' but number monthly payments field had unexpected value: " + numberMonthlyPaymentsValue);
            			else
            			{
            				double monthlyInterestRate = apr.toBigDecimal().doubleValue() / 12.0;
            				
            				if ( monthlyInterestRate == 0 )
            				{
            					monthlyPayment = new EsfMoney(amountFinanced);
            					monthlyPayment.divide(numberOfMonthlyPayments);
            				}
            				else
            				{
                				int numberOfMonthlyPaymentsInt = numberOfMonthlyPayments.toInt();
                				
            					double pow = 1;
            					for( int j=0; j < numberOfMonthlyPaymentsInt; ++j )
            					{
            						pow = pow * (1 + monthlyInterestRate);
            					}
            					
            					double powMinus1 = pow - 1;
            					
            					monthlyPayment = new EsfMoney(amountFinanced);
            					monthlyPayment.multiply(pow);
            					monthlyPayment.multiply(monthlyInterestRate);
            					monthlyPayment.divide(powMinus1);
            				}
            				
        					EsfValue newValue = new EsfMoney(monthlyPayment);
                    		context.transaction.setFieldValue(targetMonthlyPaymentDocument.getEsfName(), spec.getTargetMonthlyPaymentField(), newValue);
                			context.transaction.logActionDetail(con, "Action '" + getEsfName() + "' set money field '" + spec.getTargetMonthlyPaymentField() + "' in document '" + targetMonthlyPaymentDocument.getEsfName() +"' to value: " + newValue + "; priorValue: " + targetMonthlyPaymentValue);            					
            			}
            		}
        		}
        	}
    	}
    }
    
    @Override
    public String specsToString()
    {
    	StringBuilder buf = new StringBuilder(specList.size() * 80);
    	boolean isFirstField = true;
    	for( Spec spec : specList )
    	{
    		if ( isFirstField )
    			isFirstField = false;
    		else
    			buf.append(",");
    		buf.append(spec.targetMonthlyPaymentField).append("=(").append(spec.amountFinancedField).append(",").append(spec.annualInterestRateField).append(",").append(spec.numberMonthlyPaymentsField).append(")");
     	}
    	return buf.toString();
    }
    
	public int getEstimatedLengthXml()
	{
		int size = 60;
		for( Spec spec : specList )
			size += 100 + spec.targetMonthlyPaymentDocumentId.getEstimatedLengthXml() + spec.targetMonthlyPaymentField.getEstimatedLengthXml() + 
					spec.amountFinancedDocumentId.getEstimatedLengthXml() + spec.amountFinancedField.getEstimatedLengthXml() + 
					spec.annualInterestRateDocumentId.getEstimatedLengthXml() + spec.annualInterestRateField.getEstimatedLengthXml() + 
					spec.numberMonthlyPaymentsDocumentId.getEstimatedLengthXml() + spec.numberMonthlyPaymentsField.getEstimatedLengthXml();
		if ( hasCondition() )
			size += condition.getEstimatedLengthXml();
		return size;
	}

	public StringBuilder appendXml(StringBuilder buf)
	{
		buf.append("<Action type=\"CalculateMonthlyPaymentAction\">\n");
		for( Spec spec : specList )
		{
			buf.append(" <Spec>\n");
			buf.append("  <targetMonthlyPaymentDocumentId>").append(spec.targetMonthlyPaymentDocumentId.toXml()).append("</targetMonthlyPaymentDocumentId>\n");
			buf.append("  <targetMonthlyPaymentField>").append(spec.targetMonthlyPaymentField.toXml()).append("</targetMonthlyPaymentField>\n");
			buf.append("  <amountFinancedDocumentId>").append(spec.amountFinancedDocumentId.toXml()).append("</amountFinancedDocumentId>\n");
			buf.append("  <amountFinancedField>").append(spec.amountFinancedField.toXml()).append("</amountFinancedField>\n");
			buf.append("  <annualInterestRateDocumentId>").append(spec.annualInterestRateDocumentId.toXml()).append("</annualInterestRateDocumentId>\n");
			buf.append("  <annualInterestRateField>").append(spec.annualInterestRateField.toXml()).append("</annualInterestRateField>\n");
			buf.append("  <numberMonthlyPaymentsDocumentId>").append(spec.numberMonthlyPaymentsDocumentId.toXml()).append("</numberMonthlyPaymentsDocumentId>\n");
			buf.append("  <numberMonthlyPaymentsField>").append(spec.numberMonthlyPaymentsField.toXml()).append("</numberMonthlyPaymentsField>\n");
			buf.append(" </Spec>\n");
		}
		super.appendXml(buf);
		buf.append("</Action>\n");
		return buf;
	}
   
}