// Copyright (C) 2011-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.runtime;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.email.OutboundEmailMessage;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.EmailTemplateVersion;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.User;
import com.esignforms.open.util.StringReplacement;

/**
* TransactionParty holds information about a package party at runtime. In general, a runtime party may be switched over time,
* but this holds the current transaction party's information.
* 
* @author Yozons, Inc.
*/
public class TransactionParty
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<TransactionParty>, java.io.Serializable
{
	private static final long serialVersionUID = 3949476760816087808L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TransactionParty.class);
    
    public static final String STATUS_UNDEFINED = "U"; // not yet activated
    public static final String STATUS_CREATED = "c";
    public static final String STATUS_SKIPPED = "S";
    public static final String STATUS_ACTIVE = "A";
    public static final String STATUS_COMPLETED = "C";
    public static final String STATUS_REPORTS = "r"; // this is the status for the ESF_reports_access party that never ends and is not a party to the workflow.

    protected final EsfUUID id;
    protected final EsfUUID transactionId; // The transaction we belong to
    protected final EsfUUID packageVersionPartyTemplateId;
    protected EsfUUID prevTransactionPartyId; // points to the previous party
    protected EsfUUID nextTransactionPartyId; // points to the next party
    protected String transactionPartyAssignmentPickupCode; // when set, points to the pickup code for the current assignment of this party
    protected String status;
    protected EsfUUID todoGroupId; // when set, members of this group may play the role of this party
    protected EsfName libraryEmailTemplateEsfName; // when set, this is the current email template being used for this party

    protected List<TransactionPartyDocument> transactionPartyDocuments; 
    
    protected TransactionPartyAssignment currentAssignment; // when set, we'll also save this when our party is saved
    
    protected EsfDateTime[] saveRenotifyDateTimes; // when set, we save these as the renotify date/times for this party
    
    /**
     * This creates a TransactionParty object from data retrieved from the DB.
     */
    protected TransactionParty(EsfUUID id, EsfUUID transactionId, EsfUUID packageVersionPartyTemplateId, EsfUUID prevTransactionPartyId, EsfUUID nextTransactionPartyId, 
    		String transactionPartyAssignmentPickupCode, String status, EsfUUID todoGroupId, EsfName libraryEmailTemplateEsfName, List<TransactionPartyDocument> transactionPartyDocuments)
    {
        this.id = id;
    	this.transactionId = transactionId;
        this.packageVersionPartyTemplateId = packageVersionPartyTemplateId;
        this.prevTransactionPartyId = prevTransactionPartyId;
        this.nextTransactionPartyId = nextTransactionPartyId;
        this.transactionPartyAssignmentPickupCode = transactionPartyAssignmentPickupCode;
        this.status = status;
        this.todoGroupId = todoGroupId;
        this.libraryEmailTemplateEsfName = libraryEmailTemplateEsfName;
        this.transactionPartyDocuments = transactionPartyDocuments;
    }
    
    protected TransactionParty(EsfUUID transactionId, PackageVersionPartyTemplate packageVersionParty)
    {
        this.id = new EsfUUID();
    	this.transactionId = transactionId;
        this.packageVersionPartyTemplateId = packageVersionParty.getId();
        this.prevTransactionPartyId = null;
        this.nextTransactionPartyId = null;
        this.transactionPartyAssignmentPickupCode = null;
        this.status = STATUS_UNDEFINED;
        this.libraryEmailTemplateEsfName = null;
        this.todoGroupId = null;
        
        this.transactionPartyDocuments = new LinkedList<TransactionPartyDocument>(); // when creating new, these are added once the party itself is created
    }

   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getTransactionId()
    {
        return transactionId;
    }
    public Transaction getTransaction()
    {
    	return Transaction.Manager.getById(transactionId);
    }

    public final EsfUUID getPackageVersionPartyTemplateId()
    {
        return packageVersionPartyTemplateId;
    }
    public PackageVersionPartyTemplate getPackageVersionPartyTemplate()
    {
    	return PackageVersionPartyTemplate.Manager.getById(packageVersionPartyTemplateId);
    }
    
    public EsfUUID getPrevTransactionPartyId()
    {
        return prevTransactionPartyId;
    }
    public void setPrevTransactionPartyId(EsfUUID transactionPartyId)
    {
    	prevTransactionPartyId = transactionPartyId;
    	objectChanged();
    }
    public boolean hasPrevTransactionParty()
    {
    	return prevTransactionPartyId != null;
    }
    
    public EsfUUID getNextTransactionPartyId()
    {
        return nextTransactionPartyId;
    }
    public void setNextTransactionPartyId(EsfUUID transactionPartyId)
    {
    	nextTransactionPartyId = transactionPartyId;
    	objectChanged();
    }
    public boolean hasNextTransactionParty()
    {
    	return nextTransactionPartyId != null;
    }
    
    public String getTransactionPartyAssignmentPickupCode()
    {
    	return transactionPartyAssignmentPickupCode;
    }
    public void setTransactionPartyAssignmentPickupCode(String v)
    {
    	transactionPartyAssignmentPickupCode = v;
    	objectChanged();
    }
    
    public synchronized TransactionPartyAssignment getCurrentAssignment()
    {
    	if ( currentAssignment == null )
    		currentAssignment = TransactionPartyAssignment.Manager.getByPickupCode(transactionPartyAssignmentPickupCode);
    	return currentAssignment;
    }
    public synchronized void setCurrentAssignment(TransactionPartyAssignment v)
    {
    	currentAssignment = v;
    	setTransactionPartyAssignmentPickupCode(currentAssignment.getPickupCode()); // does objectChanged() for us too
    }
    
    public List<TransactionPartyDocument> getTransactionPartyDocuments()
    {
    	return transactionPartyDocuments;
    }
    public List<TransactionPartyDocument> getNonSkippedTransactionPartyDocuments()
    {
    	LinkedList<TransactionPartyDocument> list = new LinkedList<TransactionPartyDocument>(transactionPartyDocuments);
    	ListIterator<TransactionPartyDocument> iter = list.listIterator();
    	while( iter.hasNext() )
    	{
    		TransactionPartyDocument tranPartyDoc = iter.next();
    		if ( tranPartyDoc.isSkipped() )
    			iter.remove();
    	}
    	return list;
    }
    public boolean hasNonSkippedTransactionPartyDocuments()
    {
    	for( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
    	{
    		if ( ! tranPartyDoc.isSkipped() )
    			return true;
    	}
    	return false;
    }
    public TransactionPartyDocument getTransactionPartyDocument(EsfUUID tranDocId)
    {
    	for( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
    	{
    		if ( tranPartyDoc.getTransactionDocumentId().equals(tranDocId) )
    			return tranPartyDoc;
    	}
    	return null;
    }
    public void _addTransactionPartyDocumentDuringCreation(TransactionPartyDocument partyDoc)
    {
    	transactionPartyDocuments.add(partyDoc);
    }
    
    public String getStatus()
    {
        return status;
    }
    public void setStatus(String v)
    {
    	status = v;
    	objectChanged();
    }
    public boolean isUndefined()
    {
    	return STATUS_UNDEFINED.equals(status);
    }
    public boolean isCreated()
    {
    	return STATUS_CREATED.equals(status);
    }
    public void setStatusCreated()
    {
    	setStatus(STATUS_CREATED);
    }
    public boolean isSkipped()
    {
    	return STATUS_SKIPPED.equals(status);
    }
    public void setStatusSkipped()
    {
    	setStatus(STATUS_SKIPPED);
    }
    public boolean isActive()
    {
    	return STATUS_ACTIVE.equals(status);
    }
    public void setStatusActive()
    {
    	setStatus(STATUS_ACTIVE);
    }
    public boolean isCompleted()
    {
    	return STATUS_COMPLETED.equals(status);
    }
    public void setStatusCompleted()
    {
    	setStatus(STATUS_COMPLETED);
    }
    public boolean isReports()
    {
    	return STATUS_REPORTS.equals(status);
    }
    private void _setStatusReports()
    {
    	setStatus(STATUS_REPORTS);
    }
    public boolean isEndState()
    {
    	return isSkipped() || isCompleted();
    }
    
    public String getStatusLabel()
    {
    	if ( isCompleted() )
    		return Application.getInstance().getServerMessages().getString("transaction.party.status.completed");
    	if ( isActive() )
    		return Application.getInstance().getServerMessages().getString("transaction.party.status.active");
    	if ( isSkipped() )
    		return Application.getInstance().getServerMessages().getString("transaction.party.status.skipped");
    	if ( isCreated() )
    		return Application.getInstance().getServerMessages().getString("transaction.party.status.created");
    	if ( isReports() )
    		return Application.getInstance().getServerMessages().getString("transaction.party.status.reports");
    	if ( isUndefined() )
    		return Application.getInstance().getServerMessages().getString("transaction.party.status.undefined");
    	return "??"+status+"??";
    	
    }

    public EsfUUID getTodoGroupId()
    {
    	return todoGroupId;
    }
    public boolean hasTodoGroupId()
    {
    	return todoGroupId != null;
    }
    public void setTodoGroupId(EsfUUID v)
    {
    	todoGroupId = ( v == null || v.isNull() ) ? null : v;
    	objectChanged();
    }
    
    public EsfName getLibraryEmailTemplateEsfName()
    {
    	return libraryEmailTemplateEsfName;
    }
    public boolean hasLibraryEmailTemplateEsfName()
    {
    	return libraryEmailTemplateEsfName != null;
    }
    public void setLibraryEmailTemplateEsfName(EsfName v)
    {
    	if ( v != null && v.isValid() )
    	{
    		libraryEmailTemplateEsfName = v;
    		objectChanged();
    	}
    }
    
    public void setupRenotificationDates()
    {
    	saveRenotifyDateTimes = getPackageVersionPartyTemplate().getRenotificationDateTimes();
    }
    
    public void renotifyParty(Connection con)
		throws SQLException
	{
    	doNotifyParty(con,true,(User)null,(DocumentVersion)null,(Library)null);
	}
    
	public void notifyParty(Connection con, User user, DocumentVersion documentVersion, Library searchLibrary)
		throws SQLException
	{
		doNotifyParty(con,false,user,documentVersion,searchLibrary);
	}
	
	void doNotifyParty(Connection con, boolean isRenotify, User user, DocumentVersion documentVersion, Library searchLibrary)
		throws SQLException
	{
		if ( isReports() )
			return;
		
    	String partyNotificationType = isRenotify ? "Renotify" : "Notify";
    	
        // If the party has an email configured, let's try to send an email notification
    	PackageVersionPartyTemplate packageVersionPartyTemplate = getPackageVersionPartyTemplate();
    	EsfName tranPartyName = packageVersionPartyTemplate.getEsfName();
    	Transaction transaction = getTransaction();
    	if ( transaction == null )
    	{
    		_logger.error("notifyParty() type: " + partyNotificationType + "; could not find transaction for id: " + getTransactionId());
    		return;
    	}
    	
    	TransactionPartyAssignment tranPartyAssignment = getCurrentAssignment();
        
        if ( ! hasTodoGroupId() && packageVersionPartyTemplate.hasLibraryPartyTemplateTodoEsfName() )
        {
    		EsfName todoPartyName = packageVersionPartyTemplate.getLibraryPartyTemplateTodoEsfName();
    		PartyTemplate todoParty = transaction.getPartyTemplate(todoPartyName, searchLibrary);
    		if ( todoParty == null )
    		{
    			transaction.logActionDetail(con, "ERROR: " + partyNotificationType + " party " + tranPartyName + " has To Do Party specification: '" + todoPartyName + "' that could not be found."); 
    		}
    		else if ( transaction.isProduction() && ! todoParty.hasTodoGroupId() )
    		{
    			transaction.logActionDetail(con, "WARNING: " + partyNotificationType + " party " + tranPartyName + " has To Do Party specification: '" + todoPartyName + "' but it has no Production To Do Group set."); 
    		}
    		else if ( transaction.isTest() && ! todoParty.hasTestTodoGroupId() )
    		{
    			transaction.logActionDetail(con, "WARNING: " + partyNotificationType + " party " + tranPartyName + " has To Do Party specification: '" + todoPartyName + "' but it has no Test To Do Group set."); 
    		}
    		else
    		{
    			setTodoGroupId( transaction.isProduction() ? todoParty.getTodoGroupId() : todoParty.getTestTodoGroupId() );
    		}
        }
        
    	if ( packageVersionPartyTemplate.hasLibraryEmailTemplateEsfName() )
    	{
    		EmailTemplate emailTemplate = transaction.getEmailTemplate(packageVersionPartyTemplate.getLibraryEmailTemplateEsfName(), searchLibrary);
    		EmailTemplateVersion emailTemplateVersion = null;
    		if ( emailTemplate == null )
    		{
       			_logger.warn("notifyParty() - Skipping transaction party: " + packageVersionPartyTemplate.getEsfName() + 
  						 "; transaction party id: " + getId() + "; transaction id: " + transactionId +
	     			     "; Could not find email template: " + getLibraryEmailTemplateEsfName());
    			transaction.logActionDetail(con, "ERROR: " + partyNotificationType + " party " + tranPartyName + " specifies email template: '" + packageVersionPartyTemplate.getLibraryEmailTemplateEsfName() + "', but it could not be found."); 
    		}
    		else
    		{
    			emailTemplateVersion = transaction.doProductionResolve() ? emailTemplate.getProductionEmailTemplateVersion() : emailTemplate.getTestEmailTemplateVersion();
    			if ( emailTemplateVersion == null )
    			{
    				_logger.warn("notifyParty() - Skipping transaction party: " + tranPartyName + 
    						"; transaction party id: " + getId() + "; transaction id: " + transactionId +
    						"; Could not find email template: " + getLibraryEmailTemplateEsfName() + "; version type: " + (transaction.doProductionResolve()?"PRODUCTION":"TEST"));
        			transaction.logActionDetail(con, "ERROR: " + partyNotificationType + " party " + tranPartyName + " specifies email template: '" + packageVersionPartyTemplate.getLibraryEmailTemplateEsfName() + 
        					"', but could not find a " + (transaction.doProductionResolve()?"PRODUCTION":"TEST") + " version.");
    			}
    			else if ( ! isRenotify ) // if an original notify, setup our renotify dates and remember the configured email template to be used
    			{
    		        setupRenotificationDates();
    		        setLibraryEmailTemplateEsfName(emailTemplate.getEsfName());
    			}
    		}
    		
    		// If the party doesn't have an email set yet but has a user id, let's set the email to the user's
    		if ( ! tranPartyAssignment.hasEmailAddress() && tranPartyAssignment.hasUserId() )
    		{
				User partyUser = User.Manager.getById(tranPartyAssignment.getUserId());
				if ( partyUser != null )
					tranPartyAssignment.setEmailAddress(partyUser.getEmail());
    			else
    			{
          			_logger.warn("notifyParty() - Transaction party: " + packageVersionPartyTemplate.getEsfName() + 
      						 "; transaction party id: " + getId() + "; transaction id: " + transactionId +
		     			     "; Assigned to unknow user id: " + tranPartyAssignment.getUserId() + ". Removing user id from party.");
        			transaction.logActionDetail(con, "WARNING: " + partyNotificationType + " party " + packageVersionPartyTemplate.getEsfName() + " specifies unknown user id: " + tranPartyAssignment.getUserId() + 
        					". Removing user id from party.");
        			tranPartyAssignment.setUserId(null);
    			}
    		}
    		
    		if ( ! tranPartyAssignment.hasEmailAddress() )
    		{
    			String emailFieldExpression = packageVersionPartyTemplate.getEmailFieldExpression();
    	    	if ( EsfString.isNonBlank(emailFieldExpression)  )
    	    	{
    	    		String email = transaction.getFieldSpecWithSubstitutions(user, documentVersion, emailFieldExpression).trim();
        			if ( EsfString.isBlank(email) )
        				transaction.logActionDetail(con, "WARNING: " + partyNotificationType + " party '" + tranPartyName + "' has email specification: '" + emailFieldExpression + "'; that expands to a blank/empty value."); 
        			else
        			{
        	    		EsfEmailAddress emailAddress = new EsfEmailAddress(email); // Handles full email address spec, though for a party, we only track by the actual routing email address portion
        	    		if ( emailAddress.isValid() )
        	    		{
        	            	tranPartyAssignment.setEmailAddress(emailAddress.getEmailAddress());
        	    		}
        	    		else
        	    		{
        	    			transaction.logActionDetail(con, "ERROR: " + partyNotificationType + " party '" + tranPartyName + "' has invalid email specification: '" + emailFieldExpression + "'; expanded to: " + email); 
        	    		}
        			}
    	    	}
    		}
    		
    		// If we have both an email address and template, let's send it out
    		if ( tranPartyAssignment.hasEmailAddress() && emailTemplateVersion != null )
    		{
    			Application app = Application.getInstance();
    			LinkedList<StringReplacement.FromToSpec> list = new LinkedList<StringReplacement.FromToSpec>();
    			list.add( new StringReplacement.FromToSpec("${CONTEXTPATH}",app.getExternalContextPath()) );
    			list.add( new StringReplacement.FromToSpec("${PICKUPCODE}",tranPartyAssignment.getPickupCode()) );
    			list.add( new StringReplacement.FromToSpec("${LINK}",app.getExternalContextPath() + "/P/" + tranPartyAssignment.getPickupCode()) );
    			list.add( new StringReplacement.FromToSpec("${EMAIL}",tranPartyAssignment.getEmailAddress()) );
    			OutboundEmailMessage outboundEmail = OutboundEmailMessage.Manager.createNew(transaction, documentVersion, this, user==null?null:user.getId(), emailTemplateVersion, list);
    	        if ( outboundEmail.save(con) )
    	        {
	    			transaction.logActionDetail(con, partyNotificationType + " party " + tranPartyName + " email to: '" + outboundEmail.getEmailTo() + "'; subject: " + outboundEmail.getEmailSubject());    	        	
    	        }
    	        else
    	        {
	    			transaction.logActionBasic(con, "WARNING: " + partyNotificationType + " party " + tranPartyName + ", but failed to queue the outbound email notification to: " + tranPartyAssignment.getEmailAddress()); 
    	        }
    		}
    		
    		// If we have a ToDo Group id and it's not already assigned to a user, and we're asked to notify all of them and we have the email template to use, 
    		// send it out to all of them
    		if ( hasTodoGroupId() && ! tranPartyAssignment.hasUserId() && packageVersionPartyTemplate.isNotifyAllTodo() && emailTemplateVersion != null )
    		{
    			Group todoGroup = Group.Manager.getById(getTodoGroupId());
    			if ( todoGroup == null )
    			{
	    			transaction.logActionDetail(con, partyNotificationType + " party " + tranPartyName + " specified notify all for an unknown 'To Do' Group Id: " + getTodoGroupId() + ". The To Do group id has been removed.");
	    			setTodoGroupId(null);
    			}
    			else if ( todoGroup.isDisabled() )
    			{
	    			transaction.logActionDetail(con, partyNotificationType + " party " + tranPartyName + " specified notify all for a disabled 'To Do' Group Id: " + getTodoGroupId() + ". The To Do group id has been removed.");
	    			setTodoGroupId(null);
    			}
    			else
    			{
    				// TODO: Do we need a "context user" for a TransactionTemplate that specifies more explicitly which user should be used to resolve
    				// this scheme for determining which users belong to a ToDo group and are visible (we don't want to leak "all member users")?
    				User groupContextUser = User.Manager.getById(transaction.getTransactionTemplate().getLastUpdatedByUserId());

					Collection<User> todoUserList = todoGroup.getMemberUsers(groupContextUser);
					if ( todoUserList == null || todoUserList.size() < 1 )
					{
		    			transaction.logActionBasic(con, "WARNING: " + partyNotificationType + " party " + tranPartyName + " specified notify all for 'To Do' Group Id: " + 
		    						getTodoGroupId() + ", but no member users were found." + (groupContextUser==null?" (No TransactionTemplate last updated user.)":" Per TransactionTemplate last updated user: "+groupContextUser.getFullDisplayName()));
					}
					else
					{
						for( User emailUser : todoUserList )
						{
							// Let's not send to this To Do party if we also sent them one above because the email was set
							if ( tranPartyAssignment.hasEmailAddress() && tranPartyAssignment.getEmailAddress().equalsIgnoreCase(emailUser.getEmail()) )
								continue;
							
			    			Application app = Application.getInstance();
			    			
			    			LinkedList<StringReplacement.FromToSpec> list = new LinkedList<StringReplacement.FromToSpec>();
			    			list.add( new StringReplacement.FromToSpec("${CONTEXTPATH}",app.getExternalContextPath()) );
			    			list.add( new StringReplacement.FromToSpec("${PICKUPCODE}",tranPartyAssignment.getPickupCode()) );
			    			list.add( new StringReplacement.FromToSpec("${LINK}",app.getExternalContextPath() + "/P/" + tranPartyAssignment.getPickupCode()) );
			    			list.add( new StringReplacement.FromToSpec("${EMAIL}",emailUser.getEmail()) );
			    			OutboundEmailMessage outboundEmail = OutboundEmailMessage.Manager.createNew(transaction, documentVersion, this, emailUser.getId(), emailTemplateVersion, list);
			    	        if ( outboundEmail.save(con) )
			    	        {
				    			transaction.logActionDetail(con, partyNotificationType + " party " + tranPartyName + " To Do email to: '" + outboundEmail.getEmailTo() + "'; subject: " + outboundEmail.getEmailSubject());    	        	
			    	        }
			    	        else
			    	        {
				    			transaction.logActionBasic(con, "WARNING: " + partyNotificationType + " party " + tranPartyName + ", but failed to queue the outbound email notification to To Do user: " + emailUser.getEmail()); 
			    	        }
						}
					}
    			}
    		}
    	}
    	
	}

    public boolean transferParty(Connection con, String toNewEmail)
		throws SQLException
	{
    	boolean transferDone = true;
    	
		TransactionPartyAssignment tranPartyAssignment = getCurrentAssignment();
		if ( tranPartyAssignment.isUndefined() )
			tranPartyAssignment.setEmailAddress(toNewEmail);
		else if ( tranPartyAssignment.isActivated() || tranPartyAssignment.isRetrieved() )
		{
			tranPartyAssignment.setStatusTransferred(); // transfer away from this one
			tranPartyAssignment.save(con);
			tranPartyAssignment = TransactionPartyAssignment.Manager.createNew(this, toNewEmail);
			tranPartyAssignment.setStatusActivated();
			setCurrentAssignment(tranPartyAssignment);
			
			// If our prior party assignment has retrieved any documents, let's mark them as not retrieved now since we transferred
			for ( TransactionPartyDocument tpd : getTransactionPartyDocuments() ) 
			{
				if ( tpd.isRetrieved() )
					tpd.setStatusNotYetRetrieved();
			}
		}
		else
			transferDone = false;
		
		objectChanged();
		
		return transferDone;
	}

    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof TransactionParty )
        {
        	TransactionParty other = (TransactionParty)o;
            return getId().equals(other.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(TransactionParty o)
    {
    	return getId().compareTo(o.getId());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con)
        throws SQLException
    {
    	_logger.debug("save(con,user) on transactionId: " + transactionId + "; id: " + id + "; insert: " + doInsert());
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
            if ( currentAssignment != null )
            	currentAssignment.save(con);
            
            for (TransactionPartyDocument tranPartyDoc : transactionPartyDocuments )
            	tranPartyDoc.save(con);

        	if ( doInsert() )
            {                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_transaction_party " +
                	"(id,transaction_id,package_version_party_template_id,prev_transaction_party_id,next_transaction_party_id,transaction_party_assignment_pickup_code,status,todo_group_id,library_email_template_name) " +
                	"VALUES (?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(transactionId);
                stmt.set(packageVersionPartyTemplateId);
                stmt.set(prevTransactionPartyId);
                stmt.set(nextTransactionPartyId);
                stmt.set(transactionPartyAssignmentPickupCode);
                stmt.set(status);
                stmt.set(todoGroupId);
                stmt.set(libraryEmailTemplateEsfName);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for transactionId: " + transactionId + "; id: " + id);
                    return false;
                }
                
                saveAutoRenotificationDates(con);
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                return true;
            }
            
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_transaction_party SET prev_transaction_party_id=?,next_transaction_party_id=?,transaction_party_assignment_pickup_code=?,status=?,todo_group_id=?,library_email_template_name=? WHERE id=?"
                						   );
                stmt.set(prevTransactionPartyId);
                stmt.set(nextTransactionPartyId);
                stmt.set(transactionPartyAssignmentPickupCode);
                stmt.set(status);
                stmt.set(todoGroupId);
                stmt.set(libraryEmailTemplateEsfName);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for transaction id: " + id + "; status: " + status); 
                }
            }

            if ( isEndState() )
            	deleteAutoRenotificationDates(con);
            else
            	saveAutoRenotificationDates(con);

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on transactionId: " + transactionId + "; id: " + id + "; insert: " + doInsert());
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	
     public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    
    
     synchronized boolean saveAutoRenotificationDates(final Connection con)
	    throws SQLException
	{
    	 if ( saveRenotifyDateTimes == null || saveRenotifyDateTimes.length < 1 || isEndState() )
    		 return true;
    	 
    	 // First, let's delete any we already have since we're saving new ones now.
    	 deleteAutoRenotificationDates(con);
    	 
    	 clearLastSQLException();
    	 
		_logger.debug("saveAutoRenotificationDates(con) on transactionId: " + transactionId + "; id: " + id);
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	    	stmt = new EsfPreparedStatement(con,"INSERT INTO esf_transaction_party_renotify (notify_timestamp,transaction_party_id) VALUES (?,?)");
	    	for( EsfDateTime d : saveRenotifyDateTimes )
	    	{
	    		if ( d == null )
	    			continue;
	    		stmt.set(d);
	    		stmt.set(2, id);
	    		stmt.executeUpdate();
	    	}
            
            saveRenotifyDateTimes = null;

            return true;
	    }
	    catch(SQLException e)
	    {
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}


     synchronized boolean deleteAutoRenotificationDates(final Connection con)
	    throws SQLException
	{
		_logger.debug("deleteAutoRenotificationDates(con) on transactionId: " + transactionId + "; id: " + id);
		
		clearLastSQLException();
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_transaction_party_renotify WHERE transaction_party_id=?");
	        stmt.set(id);
	        stmt.executeUpdate();
	        return true;
	    }
	    catch(SQLException e)
	    {
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}


    synchronized boolean delete(final Connection con, final Errors errors)
        throws SQLException
    {
    	_logger.debug("delete(con) on transactionId: " + transactionId + "; id: " + id);
        
        clearLastSQLException();
    
        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of transaction party that was pending an INSERT transactionId: " + transactionId + "; id: " + id);
            objectDeleted();
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
            TransactionPartyAssignment.Manager.deleteAllByTransactionPartyId(con,id);
            TransactionPartyDocument.Manager.deleteAllByTransactionPartyId(con,id);
            
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_transaction_party WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            objectDeleted();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on transactionId: " + transactionId + "; id: " + id);
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete()
    {
        Errors nullErrors = null;
        return delete(nullErrors);
    }
        
   	public static class Manager
   	{
   		public static List<TransactionParty> getAllByTransactionId(Connection con, EsfUUID transactionId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<TransactionParty> list = new LinkedList<TransactionParty>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id,package_version_party_template_id,prev_transaction_party_id,next_transaction_party_id,transaction_party_assignment_pickup_code,status,todo_group_id,library_email_template_name " +
   	        			               					"FROM esf_transaction_party WHERE transaction_id = ?" );
   	        	stmt.set(transactionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	EsfUUID packageVersionPartyTemplateId = rs.getEsfUUID();
	            	EsfUUID prevTransactionPartyId = rs.getEsfUUID();
	            	EsfUUID nextTransactionPartyId = rs.getEsfUUID();
	            	String transactionPartyAssignmentPickupCode = rs.getString();
	            	String status = rs.getString();
	            	EsfUUID todoGroupId = rs.getEsfUUID();
	            	EsfName libraryEmailTemplateEsfName = rs.getEsfName();
	            	
	            	List<TransactionPartyDocument> transactionPartyDocuments = TransactionPartyDocument.Manager.getAllByTransactionPartyId(con, id);
	            	
	            	TransactionParty tranParty = new TransactionParty(id, transactionId, packageVersionPartyTemplateId, prevTransactionPartyId, nextTransactionPartyId,
	            										transactionPartyAssignmentPickupCode, status, todoGroupId, libraryEmailTemplateEsfName, transactionPartyDocuments);
	            	tranParty.setLoadedFromDb();
	            	list.add(tranParty);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   		public static List<TransactionParty> getAllByTransactionId(EsfUUID transactionId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<TransactionParty> partyList = getAllByTransactionId(con,transactionId);
   	        	con.commit();
   	        	return partyList;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   		public static TransactionParty getByPickupCode(Connection con, String pickupCode)
			throws SQLException
		{
	        EsfPreparedStatement stmt = null;
	        
	        try
	        {
	        	stmt = new EsfPreparedStatement( con, "SELECT id,transaction_id,package_version_party_template_id,prev_transaction_party_id,next_transaction_party_id,status,todo_group_id,library_email_template_name " +
	        			               					"FROM esf_transaction_party WHERE transaction_party_assignment_pickup_code = ?" );
	        	stmt.set(pickupCode);
	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	EsfUUID transactionId = rs.getEsfUUID();
	            	EsfUUID packageVersionPartyTemplateId = rs.getEsfUUID();
	            	EsfUUID prevTransactionPartyId = rs.getEsfUUID();
	            	EsfUUID nextTransactionPartyId = rs.getEsfUUID();
	            	String status = rs.getString();
	            	EsfUUID todoGroupId = rs.getEsfUUID();
	            	EsfName libraryEmailTemplateEsfName = rs.getEsfName();
	            	
	            	List<TransactionPartyDocument> transactionPartyDocuments = TransactionPartyDocument.Manager.getAllByTransactionPartyId(con, id);
	            	
	            	TransactionParty tranParty = new TransactionParty(id, transactionId, packageVersionPartyTemplateId, prevTransactionPartyId, nextTransactionPartyId,
	            														pickupCode, status, todoGroupId, libraryEmailTemplateEsfName, transactionPartyDocuments);
	            	tranParty.setLoadedFromDb();
	            	return tranParty;
	            }
	        }
	        finally
	        {
	            cleanupStatement(stmt);
	        }

            return null;
		}

   		public static TransactionParty getByPickupCode(String pickupCode)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	TransactionParty tranParty = getByPickupCode(con,pickupCode);
   	        	if ( tranParty != null ) 
   	        	{
		            con.commit();
	   	        	return tranParty;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
		
   		public static void setTransactionStatus(Connection con, EsfUUID transactionId, String transactionStatus)
			throws SQLException
		{
	        EsfPreparedStatement stmt = null;
	        
	        try
	        {
	        	stmt = new EsfPreparedStatement( con, "UPDATE esf_transaction_party SET transaction_status=? WHERE transaction_id = ?" );
	        	stmt.set(transactionStatus);
	        	stmt.set(transactionId);
	        	stmt.executeUpdate();
            }
	        catch( SQLException e )
	        {
	        	_logger.sqlerr(e, "setTransactionStatus() transactionId: " + transactionId + "; transactionStatus: " + transactionStatus);
	        	throw e;
	        }
	        finally
	        {
	            cleanupStatement(stmt);
	        }
		}

   		public static TransactionParty createNew(Transaction transaction, PackageVersionPartyTemplate packageVersionPartyTemplate)
   		{
   			TransactionParty tranParty = new TransactionParty(transaction.getId(), packageVersionPartyTemplate);
   			if ( packageVersionPartyTemplate.getEsfName().equals(PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS) )
   				tranParty._setStatusReports();
   			tranParty.setCurrentAssignment(TransactionPartyAssignment.Manager.createNew(tranParty, null));
   			return tranParty;
   		}
   		
   	} // Manager
   	
}