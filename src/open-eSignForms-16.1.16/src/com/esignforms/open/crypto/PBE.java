// Copyright (C) 2009-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.crypto;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import com.esignforms.open.Application;


/**
 * PbeUtil is a utility class wrapping up the password-based encryption routines.
 * Note that the encrypt and decrypt use the same technique of converting the password
 * into a secure hash to generate the 8 byte salt.
 * @author Yozons, Inc.
 */
public final class PBE
{
    private SecretKeyFactory	keyFactory;
    private String              cipherAlgorithm;
    private String              cipherProvider;
    private int					numIterations;
    private int					saltSize;
    private Application			app;
    
    public PBE(Application app, String keyAlgorithm, String keyProvider, String cipherAlgorithm, String cipherProvider, int numIterations, int saltSize)
    {
        try
        {
        	this.app			= app;
        	this.numIterations	= numIterations;
        	this.saltSize		= saltSize;
        	this.cipherAlgorithm= cipherAlgorithm;
        	this.cipherProvider	= cipherProvider;
            keyFactory 			= SecretKeyFactory.getInstance(keyAlgorithm,keyProvider);
        }
        catch(java.security.NoSuchAlgorithmException e)
        {
            app.except(e,"PBE: Failed to find SecretKeyFactory algorithm: " + keyAlgorithm);
        }
        catch(java.security.NoSuchProviderException e)
        {
            app.except(e,"PBE: Failed to find SecretKeyFactory provider: " + keyProvider);
        }
    }
    
    // This method is used to create a PBE with specific iteration and salt size to be compatible with PBE-encrypted data that perhaps used a different value
    public static PBE createCompatiblePBE(PBE likePBE, int numIterations, int saltSize)
    {
    	return new PBE(likePBE.app,likePBE.keyFactory.getAlgorithm(),likePBE.keyFactory.getProvider().getName(),likePBE.cipherAlgorithm,likePBE.cipherProvider,numIterations,saltSize);
    }
    
    public int getSaltSize()
    {
    	return saltSize;
    }
    
    public int getNumIterations()
    {
    	return numIterations;
    }
    
    public Cipher getCipher()
    {
        try
        {
            Cipher cipher = Cipher.getInstance(cipherAlgorithm, cipherProvider);
            return cipher;
        }
        catch(java.security.NoSuchAlgorithmException e)
        {
            app.except(e,"PBE: Failed to find Cipher algorithm: " + cipherAlgorithm);
            return null;
        }
        catch(java.security.NoSuchProviderException e)
        {
            app.except(e,"PBE: Failed to find Cipher provider: " + cipherProvider);
            return null;
        }
        catch( Exception e )
        {
            app.except(e,"PBE.getCipher() Failed to get cipher");
            return null;
        }
    }
    
    public byte[] encrypt(String password, byte[] data)
    {
        return encrypt(password,data,0,data.length);
    }
    
    public byte[] encrypt(String password, byte[] data, int length)
    {
        return encrypt(password,data,0,length);
    }
    
    public byte[] encrypt(String password, byte[] data, int offset, int length)
    {
    	PBEKeySpec keySpec = null;
        try
        {
            keySpec = new PBEKeySpec(password.toCharArray());
            SecretKey key = keyFactory.generateSecret(keySpec);
            
            byte[] digest = app.getSecureHash().getDigest( password );
            byte[] salt = new byte[saltSize]; 
            System.arraycopy(digest,0,salt,0,salt.length);
            PBEParameterSpec paramSpec = new PBEParameterSpec(salt,numIterations);
            
            Cipher cipher = getCipher();
            cipher.init(Cipher.ENCRYPT_MODE,key,paramSpec);
            
            return cipher.doFinal(data,offset,length);
        }
        catch(Exception e)
        {
            app.except(e,"PBE.encrypt()");
            return null;
        }
        finally
        {
        	if ( keySpec != null )
        		keySpec.clearPassword();
        }
    }
    
    
    
    public byte[] decrypt(String password, byte[] data)
    {
        return decrypt(password,data,0,data.length);
    }
    
    public byte[] decrypt(String password, byte[] data, int length)
    {
        return decrypt(password,data,0,length);
    }
    
    public byte[] decrypt(String password, byte[] data, int offset, int length)
    {
    	PBEKeySpec keySpec = null;
        try
        {
            keySpec = new PBEKeySpec(password.toCharArray());
            SecretKey key = keyFactory.generateSecret(keySpec);
            
            byte[] digest = app.getSecureHash().getDigest( password );
            byte[] salt = new byte[saltSize]; 
            System.arraycopy(digest,0,salt,0,salt.length);
            PBEParameterSpec paramSpec = new PBEParameterSpec(salt,numIterations);
            
            Cipher cipher = getCipher();
            cipher.init(Cipher.DECRYPT_MODE,key,paramSpec);
            
            return cipher.doFinal(data,offset,length);
        }
        catch(Exception e)
        {
            app.except(e,"PBE.decrypt()");
            return null;
        }
        finally
        {
        	if ( keySpec != null )
        		keySpec.clearPassword();
        }
    }
    
    public static void main(String[] args)
        throws Exception
    {
        Application app = Application.getInstallationInstance("/");
        //PBE pbe = app.getPBE();
        
        PBE pbe = new PBE(app, "PBEWithSHA256And256BitAES-CBC-BC", "BC", "PBEWithSHA256And256BitAES-CBC-BC", "BC", 50000, 16);
        
        System.out.println("Generating key");
        
        String ss = "abc";
        String ms = "shorties";
        String ls = "shorties are 4 wimps";
        
        long startTime = System.currentTimeMillis();
        byte[] ess = pbe.encrypt("password1",ss.getBytes("UTF-8"));
        System.out.println("ss = '" + ss + "' and password encrypted length=" + ess.length);
        byte[] ems = pbe.encrypt("password2",ms.getBytes("UTF-8"));
        System.out.println("ms = '" + ms + "' and password encrypted length=" + ems.length);
        byte[] els = pbe.encrypt("password3",ls.getBytes("UTF-8"));
        System.out.println("ls = '" + ls + "' and password encrypted length=" + els.length);
        
        ess = pbe.decrypt("password1",ess);
        System.out.println("decrypted ss = '" + new String(ess,"UTF-8") + "'");
        ems = pbe.decrypt("password2",ems);
        System.out.println("decrypted ms = '" + new String(ems,"UTF-8") + "'");
        els = pbe.decrypt("password3",els);
        if ( els == null )
        	System.out.println("Failed to decrypt with bad password");
        else
        	System.out.println("decrypted ls = '" + new String(els,"UTF-8") + "'");

        long endTime = System.currentTimeMillis();
        
        System.out.println("Done in " + (endTime-startTime) + " msecs.");
        
        System.exit(0);
    }
    
}