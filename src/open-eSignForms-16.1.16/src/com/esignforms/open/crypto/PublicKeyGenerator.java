// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.crypto;

import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier;
import org.bouncycastle.asn1.x509.X509Extension;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.util.Base64;


/**
 * PublicKeyGenerator is used to create 4096-bit RSA public and private key pairs for our XML digital signatures.
 * Can also do basic SHA512+RSA digital signatures over byte arrays, though we don't really use this as we're using XML Digital Signatures.
 * @author Yozons, Inc.
 */
public final class PublicKeyGenerator
{
	// Based on looking at the size when encoded as a base64 string for storage in the DB
	public static final int PUBLIC_KEY_ENCODED_STRING_ESTIMATED_LENGTH = 732;  // For 4096 bits
	public static final int PRIVATE_KEY_ENCODED_STRING_ESTIMATED_LENGTH = 3182; // it's also encrypted
	public static final String SIGNATURE_ALGORITHM = "SHA512WithRSA"; // only used in testing PDF signing (appears we need a real cert)
	public static final String CERT_SIGNATURE_ALGORITHM = "SHA512WithRSA";
    public static final int CERT_SERIAL_NUMBER_BITS = 64;

	
    private String algorithm;
    private String provider;
	private int keySize;

	private KeyPairGenerator keyPairGenerator;
    private KeyFactory keyFactory;
    
    public PublicKeyGenerator(String algorithm, String provider, int keySize, java.security.SecureRandom prng)
    {
    	this.algorithm = algorithm;
    	this.provider = provider;
    	this.keySize = keySize;
        
        try
        {
        	keyPairGenerator = KeyPairGenerator.getInstance(algorithm,provider);
            Application.getInstance().out("PublicKeyGenerator provider = " + keyPairGenerator.getProvider() + "; keysize: " + keySize);
            keyPairGenerator.initialize(keySize,prng);
        }
        catch(java.security.NoSuchAlgorithmException e)
        {
            Application.getInstance().err("PublicKeyGenerator: Failed to find algorithm: " + algorithm + "; exception: " + e.getMessage());
        }
        catch(java.security.NoSuchProviderException e)
        {
            Application.getInstance().err("PublicKeyGenerator: Failed to find provider: " + provider + "; exception: " + e.getMessage());
        }
    }
    
    /**
     * Creates an public key generator using the common SecureRandom generator.
     */
    public PublicKeyGenerator(String algorithm, String provider, int keySize)
    {
        this(algorithm,provider,keySize,Application.getInstance().getSecureRandom().getPRNG());
    }
    
    public String getAlgorithm()
    {
    	return algorithm;
    }
    public String getProvider()
    {
    	return provider;
    }
    public int getKeySize()
    {
    	return keySize;
    }
 
    public synchronized KeyPair generateKeyPair()
    {
    	Application.getInstance().debug("PublicKeyGenerator.generateKeyPair() Started...");
    	KeyPair keyPair = keyPairGenerator.genKeyPair();
    	Application.getInstance().debug("PublicKeyGenerator.generateKeyPair() Completed.");
    	return keyPair;
    }
    
    public synchronized KeyFactory getKeyFactory()
    {
        try
        {
            if ( keyFactory == null )
                keyFactory = KeyFactory.getInstance(algorithm,provider);
            return keyFactory;
        }
        catch( Exception e )
        {
            Application.getInstance().err("PublicKeyGenerator.getKeyFactory() Failed: " + e.getMessage());
            return null;
        }
    }
    
    
    public PublicKey getPublicFromX509Encoded( byte[] x509EncodedPublicKey )
    {
        try
        {
            X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(x509EncodedPublicKey);
            return getKeyFactory().generatePublic(pubKeySpec);
        }
        catch( Exception e )
        {
            Application.getInstance().err("PublicKeyGenerator.getPublicFromX509Encoded() Failed to decode: " + e.getMessage());
            return null;
        }
    }

        
    public PrivateKey getPrivateFromPkcs8Encoded( byte[] pkcs8EncodedPrivateKey )
    {
        try
        {
            PKCS8EncodedKeySpec privKeySpec = new java.security.spec.PKCS8EncodedKeySpec(pkcs8EncodedPrivateKey);
            return getKeyFactory().generatePrivate(privKeySpec);
        }
        catch( Exception e )
        {
            Application.getInstance().err("PublicKeyGenerator.getPrivateFromPkcs8Encoded() Failed to decode: " + e.getMessage());
            return null;
        }
    }
       
        
    public KeyPair getKeyPairFromEncoded( byte[] x509EncodedPublicKey, byte[] pkcs8EncodedPrivateKey )
    {
        try
        {
            X509EncodedKeySpec  pubKeySpec  = new X509EncodedKeySpec(x509EncodedPublicKey);
            PKCS8EncodedKeySpec privKeySpec = new java.security.spec.PKCS8EncodedKeySpec(pkcs8EncodedPrivateKey);
            KeyFactory          keyFactory  = getKeyFactory();
            return new KeyPair( keyFactory.generatePublic(pubKeySpec), keyFactory.generatePrivate(privKeySpec) );
        }
        catch( Exception e )
        {
            Application.getInstance().err("PublicKeyGenerator.getKeyPairFromEncoded() Failed to decode: " + e.getMessage());
            return null;
        }
    }
   
    public byte[] sign(PrivateKey key, byte[] data)
    {
        return sign(key,data,0,data.length);
    }
    
    public byte[] sign(PrivateKey key, byte[] data, int length)
    {
        return sign(key,data,0,length);
    }

    public byte[] sign(PrivateKey key, byte[] data, int offset, int length)
    {
        try
        {
        	Signature sig = Signature.getInstance(SIGNATURE_ALGORITHM,getProvider()); 
            sig.initSign(key);
            sig.update(data,offset,length);
            return sig.sign();
        }
        catch( Exception e )
        {
            Application.getInstance().except(e,"PublicKeyGenerator.sign() Failed to sign");
            return null;
        }
    }
    
    /** 
     * @param dname the X.509 Distinguished Name
     * @param keyPair the KeyPair that is the target of the certificate as well as the signer
     * @param validFrom the datetime when the cert becomes valid. If null, it'll use the current time minus 1 hour.
     * @param numDaysValid number of days from the 'validFrom' datetime the Certificate is valid
     * @see #createSelfSignedCertificate(String, KeyPair, EsfDateTime, EsfDateTime)
     */ 
    public X509Certificate createSelfSignedCertificate(String dname, KeyPair keyPair, EsfDateTime validFrom, int numDaysValid)
    	throws CertificateException, java.io.IOException
    {
    	if ( validFrom == null )
    	{
    		validFrom = new EsfDateTime();
    		validFrom.addHours(-1);
    	}
    	
    	EsfDateTime validTo = new EsfDateTime(validFrom);
    	validTo.addDays(numDaysValid);
    	return createSelfSignedCertificate(dname,keyPair,validFrom,validTo);
    }
    
    /**
     * @param dname the X.509 Distinguished Name
     * @param keyPair the KeyPair that is the target of the certificate as well as the signer
     * @param validFrom the datetime when the cert becomes valid. If null, it'll use the current time minus 1 hour.
     * @param validTo the datetime when teh cert no longer is valid. If null, it'll be 10 years from the 'valid from'.
     * @return
     * @throws CertificateException
     * @throws java.io.IOException
     */
    public X509Certificate createSelfSignedCertificate(String dname, KeyPair keyPair, EsfDateTime validFrom, EsfDateTime validTo)
     	throws CertificateException, java.io.IOException
    {
    	if ( validFrom == null )
    	{
    		validFrom = new EsfDateTime();
    		validFrom.addHours(-1);
    	}
    	if ( validTo == null )
    	{
    		validTo = new EsfDateTime(validFrom);
    		validTo.addYears(10);
    	}
    	
    	X500Name owner = new X500Name(dname);
    	BigInteger serialNumber = new BigInteger(CERT_SERIAL_NUMBER_BITS, Application.getInstance().getSecureRandom().getPRNG());
    	JcaX509v3CertificateBuilder v3CertGen = new JcaX509v3CertificateBuilder(owner, serialNumber, validFrom.toDate(), validTo.toDate(), owner, keyPair.getPublic());

    	AuthorityKeyIdentifier authorityKeyId = new AuthorityKeyIdentifier(keyPair.getPublic().getEncoded());
    	SubjectKeyIdentifier subjectKeyId = new SubjectKeyIdentifier(keyPair.getPublic().getEncoded());
    	KeyUsage keyUsage = new KeyUsage(KeyUsage.digitalSignature|KeyUsage.nonRepudiation);
    	
        v3CertGen.addExtension(X509Extension.subjectKeyIdentifier, false, subjectKeyId);
        v3CertGen.addExtension(X509Extension.authorityKeyIdentifier, false, authorityKeyId);
        v3CertGen.addExtension(X509Extension.keyUsage, true, keyUsage);

        try 
        {
        	X509CertificateHolder certHolder = v3CertGen.build(new JcaContentSignerBuilder(CERT_SIGNATURE_ALGORITHM).setProvider(getProvider()).build(keyPair.getPrivate()));
			return new JcaX509CertificateConverter().setProvider(getProvider()).getCertificate(certHolder);
		} 
        catch (OperatorCreationException e) 
		{
			Application.getInstance().except(e,"X509Certificate.createSelfSignedCertificate()");
			return null;
		}
    }   
    
    public String toBase64EncodeX509Certificate(X509Certificate cert)
    {
    	try
    	{
        	return Base64.encodeToString( cert.getEncoded() );
    	}
    	catch( java.security.cert.CertificateEncodingException e )
    	{
            Application.getInstance().except(e,"PublicKeyGenerator.getBase64EncodeX509Certificate()");
    		return null;
    	}
    }
    
    public X509Certificate toX509CertificateFromBase64Encoded(String base64EncodeCert)
    {
    	try
    	{
    		byte[] encodedCert = Base64.decode(base64EncodeCert);
    		if ( encodedCert == null )
    		{
    	 		Application.getInstance().warning("PublicKeyGenerator.getX509CertificateFromBase64Encoded() - Failed to Base64 decode.");
        		return null;
    		}
    		
    		CertificateFactory factory = CertificateFactory.getInstance("X.509");
    		if ( factory == null )
    		{
    	 		Application.getInstance().warning("PublicKeyGenerator.getX509CertificateFromBase64Encoded() - Failed to get an X.509 certificate factory.");
        		return null;
    		}
    		
    		ByteArrayInputStream bais = new ByteArrayInputStream(encodedCert); 		
    		Certificate cert = (X509Certificate)factory.generateCertificate(bais);
    		if ( cert instanceof X509Certificate )
    			return (X509Certificate)cert;
    		
    		Application.getInstance().warning("PublicKeyGenerator.getX509CertificateFromBase64Encoded() certficate is not X.509 as expected. Type is: " +
    				cert.getType());
    		return null;
    	}
    	catch( java.security.cert.CertificateException e )
    	{
            Application.getInstance().except(e,"PublicKeyGenerator.getX509CertificateFromBase64Encoded()");
    		return null;
    	}
    }
}