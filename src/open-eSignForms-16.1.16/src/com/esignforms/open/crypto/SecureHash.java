// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2009 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.crypto;

import java.security.MessageDigest;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.util.Base64;


/**
 * SecureHash is a simple wrapper around MessageDigest, typically for SHA-256 when it was created.
 * This is not thread-safe so a new instance should be created for use whenever digesting, which is
 * the default behavior when retrieving one from the Application.
 * @author Yozons, Inc.
 */
public final class SecureHash
{
    private MessageDigest digester;
	private Application   app;
    
    public SecureHash(Application app,String algorithm, String provider)
    {
        try
        {
        	this.app = app;
        	digester = MessageDigest.getInstance(algorithm,provider);
            app.info("SecureHash: Created SecureHash with algorithm: " + digester.getAlgorithm() + "; provider: " + digester.getProvider());
       }
        catch(java.security.NoSuchAlgorithmException e)
        {
            app.except(e,"SecureHash: Failed to find algorithm");
        }
        catch(java.security.NoSuchProviderException e)
        {
            app.except(e,"SecureHash: Failed to find provider");
        }
    }
    
    public String getBase64Digest(String data)
    {
        byte[] hash = getDigest(data);
        return Base64.encodeToString(hash);
    }
    public String getBase64Digest(byte[] data)
    {
        byte[] hash = getDigest(data);
        return Base64.encodeToString(hash);
    }
    public String getBase64Digest(byte[] data, int length)
    {
        byte[] hash = getDigest(data,length);
        return Base64.encodeToString(hash);
    }
    public String getBase64Digest(byte[] data, int offset, int length)
    {
        byte[] hash = getDigest(data,offset,length);
        return Base64.encodeToString(hash);
    }
    
    
    /**
     * Gets a digest of a String in UTF-8 encoding.  
     * @param data the String data to be digested
     * @return the byte array of the digested String
     */
    public byte[] getDigest(String data)
    {
		byte[] b = EsfString.stringToBytes(data);
		return getDigest(b,0,b.length);
    }
    
    public byte[] getDigest(byte[] data)
    {
        return getDigest(data,0,data.length);
    }
    
    public byte[] getDigest(byte[] data, int length)
    {
        return getDigest(data,0,length);
    }
    
    public synchronized byte[] getDigest(byte[] data, int offset, int length)
    {
        try
        {
        	digester.update(data,offset,length);
            return digester.digest();
        }
        catch(Exception e)
        {
            app.except(e,"SecureHash.getDigest()");
            return null;
        }
    }
    
    public static final void main(String[] args)
    {
    	Application app = Application.getInstallationInstance("/");
    	SecureHash hash = new SecureHash(app,"SHA-256","BC");
    	
    	String value = "test";
    	System.out.println("Value: " + value);
    	System.out.println("Hashed Base64 Value: " + hash.getBase64Digest(value));
    	System.out.print("Hash bytes: ");
    	byte[] hashedValue = hash.getDigest(value);
    	for( byte b : hashedValue )
    	{
    		System.out.print(b);
    		System.out.print(" ");
    	}
    	System.out.println();
    	
    	System.out.println("Hashed Base64 Value: " + hash.getBase64Digest(value));
    	System.out.println("Hashed Base64 Value: " + hash.getBase64Digest(value));
    	System.exit(0);
    }  
    
}