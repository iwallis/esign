// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2009 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.crypto;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfString;

/**
 * SecretKeyGenerator is the general class secret generation.
 * @author Yozons, Inc.
 */
public class SecretKeyGenerator
{
    private KeyGenerator keyGen           = null;
    private String       keyAlgorithm     = null;
    private String       cipherAlgorithm  = null;
    private String       cipherProvider   = null;
    private boolean      isEcbMode        = false;
    private int          blockSizeInBytes = 8;
    private byte[]       staticPseudoIv   = null;
    
    public SecretKeyGenerator( String keyAlgorithm,   String keyProvider,   int keySize,
                               String cipherAlgorithm,String cipherProvider,SecureRandom prng
                             )
    {
        try
        {
            this.keyAlgorithm    = keyAlgorithm;
            this.cipherAlgorithm = cipherAlgorithm;
            this.cipherProvider  = cipherProvider;
            keyGen = KeyGenerator.getInstance(keyAlgorithm,keyProvider);
            Application.getInstance().debug("SecretKeyGenerator: " + keyAlgorithm + " KeyGenerator provider = " + keyGen.getProvider() + " with keysize=" + keySize);
            keyGen.init(keySize,prng);
            isEcbMode = ( cipherAlgorithm.indexOf("/ECB/") > 0 );
        }
        catch(java.security.NoSuchAlgorithmException e)
        {
            Application.getInstance().err("SecretKeyGenerator: Failed to find algorithm: '" + keyAlgorithm + "' - " + e.getMessage());
        }
        catch(java.security.NoSuchProviderException e)
        {
            Application.getInstance().err("SecretKeyGenerator: Failed to find provider: '" + keyProvider + "' - " + e.getMessage());
        }
    }
    
    public void setBlockSizeInBytes(int size)
    {
        blockSizeInBytes = size;
    }
    
    public synchronized SecretKey generateKey()
    {
        return keyGen.generateKey();
    }
    
    public synchronized SecretKey getFromRawEncoded(byte[] raw)
    {
        try
        {
            return new SecretKeySpec(raw,keyAlgorithm);
        }
        catch( Exception e )
        {
            Application.getInstance().err("SecretKeyGenerator.getFromRawEncoded() Failed to decode: " + keyAlgorithm + " - " + e.getMessage());
            return null;
        }
    }
    
    public boolean isEcbMode()
    {
        return isEcbMode;
    }
    
    public Cipher getCipher()
    {
        try
        {
            Cipher cipher = Cipher.getInstance(cipherAlgorithm,cipherProvider);
            return cipher;
        }
        catch( Exception e )
        {
            Application.getInstance().err("SecretKeyGenerator.getCipher() Failed to get cipher: " + cipherAlgorithm + " - " + e.getMessage());
            return null;
        }
    }
    
    public byte[] encrypt(SecretKey key, byte[] data)
    {
        return encrypt(key,data,0,data.length);
    }
    
    public byte[] encrypt(SecretKey key, byte[] data, int length)
    {
        return encrypt(key,data,0,length);
    }
    
    public byte[] encrypt(SecretKey key, byte[] data, int offset, int length)
    {
        if( ! isEcbMode )  
        {
            return encryptIv(key,createStaticPseudoIv(),data,offset,length);
        }
        
        try
        {
            Cipher cipher = getCipher();
            cipher.init(Cipher.ENCRYPT_MODE,key);
            return cipher.doFinal(data,offset,length);
        }
        catch( Exception e )
        {
            Application.getInstance().err("SecretKeyGenerator.encrypt(" + cipherAlgorithm + ") Failed to encrypt: " + e.getMessage());
            return null;
        }
    }
    
    public byte[] encryptIv(SecretKey key, long id, byte[] data)
    {
        return encryptIv(key,getInitializationVector(id),data,0,data.length);
    }
    
    public byte[] encryptIv(SecretKey key, long id, byte[] data, int length)
    {
        return encryptIv(key,getInitializationVector(id),data,0,length);
    }
    
    public byte[] encryptIv(SecretKey key, long id, byte[] data, int offset, int length)
    {
        return encryptIv(key,getInitializationVector(id),data,offset,length);
    }
    
    public byte[] encryptIv(SecretKey key, String id, byte[] data)
    {
        return encryptIv(key,getInitializationVector(id),data,0,data.length);
    }
    
    public byte[] encryptIv(SecretKey key, String id, byte[] data, int length)
    {
        return encryptIv(key,getInitializationVector(id),data,0,length);
    }
    
    public byte[] encryptIv(SecretKey key, String id, byte[] data, int offset, int length)
    {
        return encryptIv(key,getInitializationVector(id),data,offset,length);
    }
    
    public byte[] encryptIv(SecretKey key, byte[] iv, byte[] data, int offset, int length)
    {
        if ( iv.length < blockSizeInBytes )
            iv = fixupShortIv(iv);
            
        try
        {
            Cipher cipher = getCipher();
            IvParameterSpec ivSpec = new IvParameterSpec( iv );
            cipher.init(Cipher.ENCRYPT_MODE,key,ivSpec);
            return cipher.doFinal(data,offset,length);
        }
        catch( Exception e )
        {
            Application.getInstance().err("SecretKeyGenerator.encryptIv(" + cipherAlgorithm + ") Failed to encrypt: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * This is a lame "fix" for CBC-mode algorithms when they are called with no IV to use.
     */
    private synchronized byte[] createStaticPseudoIv()
    {
        if ( staticPseudoIv == null )
        {
            staticPseudoIv = new byte[blockSizeInBytes];
            for( int i=0; i<staticPseudoIv.length; ++i )
                staticPseudoIv[i] = (byte)(127-i);
        }
        return staticPseudoIv;
    }
        
    /**
     * If we use a 'long' as our IV, we will be okay
     * for algorithms that use 8-byte blocks, but some like AES and Rijndael have 16 byte blocks, so we need
     * to pad it out as appropriate.
     */
    private byte[] fixupShortIv(byte[] shortIv)
    {
        byte[] iv = new byte[blockSizeInBytes];
        int ivPos = 0;
        int bytesRemaining = iv.length;
        while( bytesRemaining > 0 )
        {
            int len = Math.min(shortIv.length, bytesRemaining);
            System.arraycopy(shortIv, 0, iv, ivPos, len);
            ivPos          += len;
            bytesRemaining -= len;
        }
        return iv;
    }
    
    private byte[] getInitializationVector(long v)
    {
        if ( blockSizeInBytes <= 8 )
            return Application.getInstance().longToByteArray(v);
        if ( blockSizeInBytes <= 16 )        
            return longToInitializationVector16(v);
        byte[] iv = longToInitializationVector16(v);
        return fixupShortIv(iv);
    }
    
    private byte[] getInitializationVector(String v)
    {
        byte[] vBytes = EsfString.stringToBytes(v);
        return fixupShortIv(vBytes);
    }
    
    public byte[] longToInitializationVector16(long v)
    {
        // a long is 8 bytes long, but this is a perversion routine
        // used to create an InitializationVector for AES and Rijndael crypto
        // which has a 16 byte blocksize.
        byte[] b = new byte[16]; 
        b[0]  = (byte)(v >> 56);
        b[1]  = (byte)(v >> 48);
        b[2]  = (byte)(v >> 40);
        b[3]  = (byte)(v >> 32);
        b[4]  = (byte)(v >> 24);
        b[5]  = (byte)(v >> 16);
        b[6]  = (byte)(v >> 8);
        b[7]  = (byte)v;
        b[8]  = b[0];
        b[9]  = b[1];
        b[10] = b[2];
        b[11] = b[3];
        b[12] = b[4];
        b[13] = b[5];
        b[14] = b[6];
        b[15] = b[7];
        
        return b;
    }
    
    
    
    public byte[] decrypt(SecretKey key, byte[] data)
    {
        return decrypt(key,data,0,data.length);
    }
    
    public byte[] decrypt(SecretKey key, byte[] data, int length)
    {
        return decrypt(key,data,0,length);
    }
    
    public byte[] decrypt(SecretKey key, byte[] data, int offset, int length)
    {
        if( ! isEcbMode )  
        {
            return decryptIv(key,createStaticPseudoIv(),data,offset,length);
        }
        
        try
        {
            Cipher cipher = getCipher();
            cipher.init(Cipher.DECRYPT_MODE,key);
            return cipher.doFinal(data,offset,length);
        }
        catch( Exception e )
        {
            Application.getInstance().err("SecretKeyGenerator.decrypt(" + cipherAlgorithm + ") Failed to decrypt: " + e.getMessage());
            return null;
        }
    }
    
    public byte[] decryptIv(SecretKey key, long id, byte[] data)
    {
        return decryptIv(key,getInitializationVector(id),data,0,data.length);
    }
    
    public byte[] decryptIv(SecretKey key, long id, byte[] data, int length)
    {
        return decryptIv(key,getInitializationVector(id),data,0,length);
    }
    
    public byte[] decryptIv(SecretKey key, long id, byte[] data, int offset, int length)
    {
        return decryptIv(key,getInitializationVector(id),data,offset,length);
    }
    
    public byte[] decryptIv(SecretKey key, String id, byte[] data)
    {
        return decryptIv(key,getInitializationVector(id),data,0,data.length);
    }
    
    public byte[] decryptIv(SecretKey key, String id, byte[] data, int length)
    {
        return decryptIv(key,getInitializationVector(id),data,0,length);
    }
    
    public byte[] decryptIv(SecretKey key, String id, byte[] data, int offset, int length)
    {
        return decryptIv(key,getInitializationVector(id),data,offset,length);
    }
    
    public byte[] decryptIv(SecretKey key, byte[] iv, byte[] data, int offset, int length)
    {
        if ( iv.length < blockSizeInBytes )
            iv = fixupShortIv(iv);
            
        try
        {
            Cipher cipher = getCipher();
            IvParameterSpec ivSpec = new IvParameterSpec( iv );
            cipher.init(Cipher.DECRYPT_MODE,key,ivSpec);
            return cipher.doFinal(data,offset,length);
        }
        catch( Exception e )
        {
            Application.getInstance().err("SecretKeyGenerator.decryptIv(" + cipherAlgorithm + ") Failed to decrypt: " + e.getMessage());
            return null;
        }
    }
    
}