// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.crypto;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfString;

/**
 * SecureRandom is a convenience class to ensure that everybody uses the same java.security.SecureRandom object
 * since the PRNG sometimes takes a bit of time to initialize so we want to only do it once at startup.
 * 
 * @author Yozons, Inc.
 */
public final class SecureRandom
{
    private java.security.SecureRandom prng = null;
    
    public SecureRandom(String algorithm, String provider)
    {
    	Application app = Application.getInstance();
        try
        {
        	// If either is not provided, we'll just use the standard for the JDK, which is generally the best it has to offer.
        	if ( EsfString.isBlank(algorithm) || EsfString.isBlank(provider) )
        	{
        		app.debug("SecureRandom: DEBUG starting random with default algorithm and provider");
                prng = new java.security.SecureRandom();
        	}
        	else
        	{
        		app.debug("SecureRandom: DEBUG starting random with algorithm: " + algorithm + "; provider: " + provider);
        		prng = java.security.SecureRandom.getInstance(algorithm, provider);
        	}
            byte[] randSink = new byte[331]; // 331 is prime, but otherwise not really special
            prng.nextBytes(randSink); // force the PRNG's seeding itself and eat initial 331 random bytes 
            app.info("SecureRandom: Created SecureRandom with algorithm: " + prng.getAlgorithm() + "; provider: " + prng.getProvider());
        }
        catch(java.security.NoSuchAlgorithmException e)
        {
            app.except(e,"SecureRandom: Failed to find " + algorithm + " algorithm");
        }
        catch(java.security.NoSuchProviderException e) 
        {
            app.except(e,"SecureRandom: Failed to find " + provider + " PRNG provider");
        }
    }
    
    public void setSeed(String s)
    {
		setSeed(); // calls to setSeed(String) should occur at random times
    	if ( s != null && s.length() > 0 )
    		setSeed(s.hashCode());
    }

    public void setSeed(long i)
    {
    	prng.setSeed(i);
    }

    public void setSeed()
    {
        setSeed(System.currentTimeMillis());
    }
    
    public java.security.SecureRandom getPRNG()
    {
    	return prng;
    }
}