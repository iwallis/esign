// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2009 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.crypto;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.util.Base64;

/**
 * STANDESDecryption does STAN (Student Authentication Network) DES encryption/decryption per
 * Pearson Government Solutions, Student Authentication Network API Version 1.17.
 * @author Yozons, Inc.
 */
public final class STANDESDecryption
    extends com.esignforms.open.crypto.SecretKeyGenerator
{
    public static final int KEYSIZE = 64;
    
    protected IvParameterSpec ivSpec = null;
    protected SecretKey       key    = null;
    
    /**
     * Creates an STANDES key generator using the 64 bits by default with the common SecureRandom generator.
     */
    public STANDESDecryption(String encryptionKeyBase64, String ivBase64)
    {
        super( "DES", "BC", KEYSIZE, "DES/CBC/PKCS5Padding", "BC", Application.getInstance().getSecureRandom().getPRNG() ); 
        setBlockSizeInBytes(8);
        
        byte[] encryptionKeyBytes = Base64.decode(encryptionKeyBase64);
        if ( encryptionKeyBytes == null || encryptionKeyBytes.length < 1 )
            Application.getInstance().err("STANDESDecryption() Could not Base64 decode encryption key: " + encryptionKeyBase64);
        else
        {
            key = getFromRawEncoded(encryptionKeyBytes);
            if ( key == null )
                Application.getInstance().err("STANDESDecryption() Could not create encryption key from decoded: " + encryptionKeyBase64);
            else
            {
                byte[] iv = Base64.decode(ivBase64);
                if ( iv == null || iv.length < 1 )
                    Application.getInstance().err("STANDESDecryption() Could not Base64 decode IV: " + ivBase64);
                ivSpec = new IvParameterSpec( iv );
                if ( ivSpec == null )
                    Application.getInstance().err("STANDESDecryption() Could not IV Spect from decoded IV: " + ivBase64);
            }
        }
    }
    
    public String decrypt(String dataBase64)
    {
        byte[] data = Base64.decode(dataBase64);
        if ( data == null || data.length < 1 )
        {
            Application.getInstance().err("STANDESDecryption.decrypt() Could not Base64 decode: " + dataBase64);
            return null;
        }
        
        try
        {
            Cipher cipher = getCipher();
            cipher.init(Cipher.DECRYPT_MODE,key,ivSpec);
            byte[] decrypted = cipher.doFinal(data,0,data.length);
            if ( decrypted == null )
            {
                Application.getInstance().err("STANDESDecryption.decrypt() Failed to decrypt: " + dataBase64);
                return null;
            }
            return EsfString.bytesToString(decrypted);
        }
        catch( Exception e )
        {
            Application.getInstance().err("STANDESDecryption.decrypt() Failed to decrypt: " + e.getMessage());
            return null;
        }
    }
}