// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.crypto;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.security.Key;
import java.security.KeyException;
import java.security.PublicKey;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.KeySelector;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignatureProperties;
import javax.xml.crypto.dsig.SignatureProperty;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLObject;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyName;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.esignforms.open.Application;
import com.esignforms.open.Version;
import com.esignforms.open.admin.SignatureKey;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.util.XmlUtil;


/**
 * XmlDigitalSignature provides our interface to XML Digital Signatures.
 * 
 * Much of the code understanding came from Oracle's technotes: 
 * http://download.oracle.com/javase/6/docs/technotes/guides/security/xmldsig/XMLDigitalSignature.html
 * 
 * @author Yozons, Inc.
 */
public final class XmlDigitalSignature
{
	// These literals are supported as of Java 7, but are not defined in the API yet...
	private static final String SignatureMethod_RSA_SHA512 = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512";
	private static final String CanonicalizationMethod_INCLUSIVE_C14N11_WITH_COMMENTS = "http://www.w3.org/2006/12/xml-c14n11#WithComments";
	
    private XMLSignatureFactory xmlSignatureFactory;
    
    public XmlDigitalSignature()
    {
    	xmlSignatureFactory = XMLSignatureFactory.getInstance("DOM");
    }
    

    public String sign(String xmlToSign, String description, String contentType, SignatureKey signatureKey)
		throws EsfException
	{
    	return sign(xmlToSign,description,contentType,signatureKey,null,null);
	}
    
    public String sign(String xmlToSign, String description, String contentType, SignatureKey signatureKey, String signerIpHost, String signerUserAgent)
    	throws EsfException
    {
    	if ( EsfString.isBlank(xmlToSign) )
    		throw new EsfException("Missing XML string to be signed.");
    	if ( signatureKey == null )
    		throw new EsfException("Missing the signature key needed to sign.");
    	
    	try
    	{
    		// Our KeyInfo section will have a KeyName that is our signature key's id, as well as the public key itself,
    		// and our self-sign X.509 certificate.
        	KeyInfoFactory keyInfoFactory = xmlSignatureFactory.getKeyInfoFactory();
        	List<XMLStructure> keyInfoList = new LinkedList<XMLStructure>();
        	KeyName keyName = keyInfoFactory.newKeyName(signatureKey.getId().toString());
        	keyInfoList.add(keyName);
        	KeyValue keyValue = keyInfoFactory.newKeyValue(signatureKey.getPublicKey());
        	keyInfoList.add(keyValue);
        	X509Data keyX409Data = keyInfoFactory.newX509Data(Collections.singletonList(signatureKey.getX509Certificate()));
        	keyInfoList.add(keyX409Data);
        	KeyInfo keyInfo = keyInfoFactory.newKeyInfo(keyInfoList,"KeyInfo_ID");
        	
        	// Instantiate the document to be signed
        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        	dbf.setNamespaceAware(true);
        	Document document = dbf.newDocumentBuilder().parse(new BufferedInputStream(new ByteArrayInputStream(EsfString.stringToBytes(xmlToSign))));

        	// Create a DOMSignContext and specify the RSA PrivateKey and
        	// location of the resulting XMLSignature's parent element.
        	DOMSignContext domSignContext = new DOMSignContext(signatureKey.getPrivateKey(), document.getDocumentElement());
        	
        	// Add in the EU Directive extras: http://uri.etsi.org/01903/v1.4.1/ts_101903v010401p.pdf
        	String signingTime = (new EsfDateTime()).toXmlWithTz();
        	Element qualifyingPropertiesElement = document.createElement("QualifyingProperties");
        	qualifyingPropertiesElement.setAttribute("Id","QualifyingProperties_ID");
        	qualifyingPropertiesElement.setIdAttribute("Id",true); // mark our id as the "Id" attribute
        	Element signedPropertiesElement = document.createElement("SignedProperties");
        	Element signedSignaturePropertiesElement = document.createElement("SignedSignatureProperties");
        	Element signingTimeElement = document.createElement("SigningTime");
        	signingTimeElement.setTextContent(signingTime);

        	Element signedDataObjectPropertiesElement = document.createElement("SignedDataObjectProperties");
        	Element dataObjectFormatElement = document.createElement("DataObjectFormat");
        	dataObjectFormatElement.setAttribute("ObjectReference", "#Payload_Reference_ID");
        	Element dataObjectDescriptionElement = document.createElement("Description");
        	dataObjectDescriptionElement.setTextContent(XmlUtil.toEscapedXml(description));
        	dataObjectFormatElement.appendChild(dataObjectDescriptionElement);
        	Element dataObjectMimeTypeElement = document.createElement("MimeType");
        	dataObjectMimeTypeElement.setTextContent(XmlUtil.toEscapedXml(contentType));
        	dataObjectFormatElement.appendChild(dataObjectMimeTypeElement);
        	
        	qualifyingPropertiesElement.appendChild(signedPropertiesElement);
        	signedPropertiesElement.appendChild(signedSignaturePropertiesElement);
        	signedPropertiesElement.appendChild(signedDataObjectPropertiesElement);
        	signedSignaturePropertiesElement.appendChild(signingTimeElement);
        	signedDataObjectPropertiesElement.appendChild(dataObjectFormatElement);
        	
        	DOMStructure qualifyingPropertiesObject = new DOMStructure(qualifyingPropertiesElement);
        	XMLObject qualifyingPropertiesXMLObject = xmlSignatureFactory.newXMLObject(Collections.singletonList(qualifyingPropertiesObject), null, null, null);
        	
        	// Our signature includes the deployment id, and the current timestamp
        	Element esignformsElement = document.createElement("OpenESignForms_XmlDigitalSignatureSeal");
        	esignformsElement.setAttribute("Version", XmlUtil.toEscapedXml(Version.getVersionString()));
        	esignformsElement.setAttribute("Timestamp", signingTime);
        	esignformsElement.setAttribute("DeploymentId", Application.getInstance().getDeployId().toXml());
        	esignformsElement.setAttribute("DeploymentHostName", XmlUtil.toEscapedXml(Application.getInstance().getMyHostName()));
        	esignformsElement.setAttribute("DeploymentHostAddress", XmlUtil.toEscapedXml(Application.getInstance().getMyHostAddress()));
        	if ( EsfString.isNonBlank(signerIpHost) )
            	esignformsElement.setAttribute("SignerAddress", XmlUtil.toEscapedXml(signerIpHost));
        	if ( EsfString.isNonBlank(signerUserAgent) )
            	esignformsElement.setAttribute("SignerAgent", XmlUtil.toEscapedXml(signerUserAgent));
        	
        	DOMStructure signatureObject = new DOMStructure(esignformsElement);
        	
        	SignatureProperty signatureProperty = xmlSignatureFactory.newSignatureProperty(Collections.singletonList(signatureObject), "#OpenESignForms_Seal", "OpenESignForms_Seal_ID");
        	SignatureProperties signatureProperties = xmlSignatureFactory.newSignatureProperties(Collections.singletonList(signatureProperty), null); 
        	XMLObject signaturePropertiesXMLObject = xmlSignatureFactory.newXMLObject(Collections.singletonList(signatureProperties),null, null, null);
        	
        	// Sign the main payload
        	Reference referencePayload = xmlSignatureFactory.newReference("", 
					xmlSignatureFactory.newDigestMethod(DigestMethod.SHA512, null),
					Collections.singletonList( xmlSignatureFactory.newTransform(Transform.ENVELOPED, (TransformParameterSpec)null) ),
					null, 
					"Payload_Reference_ID"
					);
        	// Sign the KeyInfo per the EU spec
        	Reference referenceKeyInfo = xmlSignatureFactory.newReference("#KeyInfo_ID", 
					xmlSignatureFactory.newDigestMethod(DigestMethod.SHA512, null));
        	// Sign the EU qualifying properties
        	Reference referenceQualifyingProperties = xmlSignatureFactory.newReference("#QualifyingProperties_ID", 
					xmlSignatureFactory.newDigestMethod(DigestMethod.SHA512, null));
        	// Sign our seal
        	Reference referenceSignatureProperties = xmlSignatureFactory.newReference("#OpenESignForms_Seal_ID", 
					xmlSignatureFactory.newDigestMethod(DigestMethod.SHA512, null));

        	List<Reference> referenceList = new LinkedList<Reference>();
        	referenceList.add(referencePayload);
        	referenceList.add(referenceKeyInfo);
        	referenceList.add(referenceQualifyingProperties);
        	referenceList.add(referenceSignatureProperties);
        	
        	SignedInfo signedInfo = xmlSignatureFactory.newSignedInfo( xmlSignatureFactory.newCanonicalizationMethod( CanonicalizationMethod_INCLUSIVE_C14N11_WITH_COMMENTS/*CanonicalizationMethod.INCLUSIVE_WITH_COMMENTS*/, (C14NMethodParameterSpec)null ),
						xmlSignatureFactory.newSignatureMethod(SignatureMethod_RSA_SHA512, null),
						referenceList
						);

        	// Create the XMLSignature, but don't sign it yet.
        	List<XMLObject> signingXMLObjects = new LinkedList<XMLObject>();
        	signingXMLObjects.add(qualifyingPropertiesXMLObject);
           	signingXMLObjects.add(signaturePropertiesXMLObject);
           	XMLSignature xmlSignature = xmlSignatureFactory.newXMLSignature(signedInfo, keyInfo, signingXMLObjects, "OpenESignForms_Seal", null);

        	// Marshal, generate, and sign the enveloped signature.
        	xmlSignature.sign(domSignContext);

        	Writer writer = new StringWriter(xmlToSign.length()+8000); // add in overhead for Signature element 
        	TransformerFactory tf = TransformerFactory.newInstance();
        	Transformer transformer = tf.newTransformer();
        	// we don't want <?xml version="1.0" encoding="UTF-8" standalone="no"?> at the top since we embed our signed XML in other XML structures (it's not a standalone document at all)
        	transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes"); 
        	transformer.transform(new DOMSource(document), new StreamResult(writer));

        	return writer.toString();
    	}
    	catch( Exception e )
    	{
    		Application.getInstance().except(e, "XmlDigitalSignature.sign() SERIOUS-ERROR");
    		throw new EsfException(e.getMessage());
    	}
    }
    
    public void verify(String signedXml, SignatureKey signatureKey)
    	throws EsfException
    {
    	if ( EsfString.isBlank(signedXml) || signatureKey == null )
    		throw new EsfException("Missing required parameter signedXml and/or signatureKey");

    	try
    	{
        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        	dbf.setNamespaceAware(true);
        	Document document = dbf.newDocumentBuilder().parse(new BufferedInputStream(new ByteArrayInputStream(EsfString.stringToBytes(signedXml))));

        	NodeList qualifyingPropertiesNodeList = document.getElementsByTagName("QualifyingProperties");
        	if ( qualifyingPropertiesNodeList != null ) 
        	{
        		for( int i=0; i < qualifyingPropertiesNodeList.getLength(); ++i )
        		{
        			Element qualifyingPropertiesElement = (Element)qualifyingPropertiesNodeList.item(i);
        			if ( qualifyingPropertiesElement != null )
        				qualifyingPropertiesElement.setIdAttribute("Id",true); // mark our id as the "Id" attribute 
        		}
        	}

        	NodeList nl = document.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
        	if (nl.getLength() == 0) 
        	{
        		Application.getInstance().err("XmlDigitalSignature.verify(SignatureKey) SERIOUS-ERROR no Signature element");
        		throw new EsfException("The signedXML has no Signature element");
        	}

        	// The embedded key must match our signatureKey's values or the validation will fail.
        	DOMValidateContext valContext = new DOMValidateContext(new VerifyingKeySelector(signatureKey), nl.item(0));

        	XMLSignature signature = xmlSignatureFactory.unmarshalXMLSignature(valContext);
        	if ( ! signature.validate(valContext) ) 
        	{
        	    boolean isSignatureValueValid = signature.getSignatureValue().validate(valContext);
        		Application.getInstance().err("XmlDigitalSignature.verify(SignatureKey) SERIOUS-ERROR Failed core validation; SignatureValue validated: " + isSignatureValueValid);
        		StringBuilder exceptionText = new StringBuilder();
        		exceptionText.append("Invalid signature. SignatureValue ").append(isSignatureValueValid ? "validated" : "INVALID");
    	        Iterator<?> i = signature.getSignedInfo().getReferences().iterator();
    	        for (int j=0; i.hasNext(); j++) 
    	        {
    	            boolean refValid = ((Reference)i.next()).validate(valContext);
            		Application.getInstance().err(" -> Reference["+j+"] validated: " + refValid);
            		exceptionText.append("; ").append(getReferenceName(j)).append(refValid ? " validated" : " INVALID");
    	        }
        	    throw new EsfException(exceptionText.toString());
        	}
    	}
    	catch( EsfException e )
    	{
    		throw e;
    	}
    	catch( Exception e )
    	{
    		Application.getInstance().except(e, "XmlDigitalSignature.verify(SignatureKey) SERIOUS-ERROR");
    		throw new EsfException("Invalid signature: " + e.getMessage());
    	}
    	
    }
    
    private static String getReferenceName(int i)
    {
    	if ( i == 0 )
    		return "XML data payload";
    	if ( i == 1 )
    		return "OpenESignForms Seal";
    	return "Unexpected Reference[" + i + "]";
    }
    
    public void verify(String signedXml, PublicKey publicKey)
		throws EsfException
    {
    	if ( EsfString.isBlank(signedXml) || publicKey == null )
    		throw new EsfException("Missing required parameter signedXml and/or publicKey");

    	try
    	{
        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        	dbf.setNamespaceAware(true);
        	Document document = dbf.newDocumentBuilder().parse(new BufferedInputStream(new ByteArrayInputStream(EsfString.stringToBytes(signedXml))));

        	NodeList qualifyingPropertiesNodeList = document.getElementsByTagName("QualifyingProperties");
        	if ( qualifyingPropertiesNodeList != null ) 
        	{
        		for( int i=0; i < qualifyingPropertiesNodeList.getLength(); ++i )
        		{
        			Element qualifyingPropertiesElement = (Element)qualifyingPropertiesNodeList.item(i);
        			if ( qualifyingPropertiesElement != null )
        				qualifyingPropertiesElement.setIdAttribute("Id",true); // mark our id as the "Id" attribute 
        		}
        	}

        	NodeList nl = document.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
        	if (nl.getLength() == 0) 
        	{
        		Application.getInstance().err("XmlDigitalSignature.verify(PublicKey) SERIOUS-ERROR no Signature element");
        		throw new EsfException("The signedXML has no Signature element");
        	}

        	DOMValidateContext valContext = new DOMValidateContext(publicKey, nl.item(0));
        	//DOMValidateContext valContext = new DOMValidateContext(KeySelector.singletonKeySelector(publicKey), nl.item(0));

        	XMLSignature signature = xmlSignatureFactory.unmarshalXMLSignature(valContext);
        	if ( ! signature.validate(valContext) ) {
        	    boolean isSignatureValueValid = signature.getSignatureValue().validate(valContext);
        		Application.getInstance().err("XmlDigitalSignature.verify(PublicKey) SERIOUS-ERROR Failed core validation; SignatureValue validated; " + isSignatureValueValid);
        		StringBuilder exceptionText = new StringBuilder();
        		exceptionText.append("Invalid signature. SignatureValue ").append(isSignatureValueValid ? "validated" : "INVALID");
    	        Iterator<?> i = signature.getSignedInfo().getReferences().iterator();
    	        for (int j=0; i.hasNext(); j++) 
    	        {
    	            boolean refValid = ((Reference)i.next()).validate(valContext);
            		Application.getInstance().err(" -> Reference["+j+"] validated: " + refValid);
            		exceptionText.append("; ").append(getReferenceName(j)).append(refValid ? " validated" : " INVALID");
    	        }
        	    throw new EsfException(exceptionText.toString());
        	}
    	}
    	catch( Exception e )
    	{
    		Application.getInstance().except(e, "XmlDigitalSignature.verify(PublicKey) SERIOUS-ERROR");
    		throw new EsfException("Invalid signature: " + e.getMessage());
    	}
    	
    }
    
    public void verifyAcceptEmbeddedKeysIfKeyIdNotFound(String signedXml)
		throws EsfException
    {
    	if ( EsfString.isBlank(signedXml) )
    		throw new EsfException("Missing required parameter signedXml");

    	try
    	{
        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        	dbf.setNamespaceAware(true);
        	Document document = dbf.newDocumentBuilder().parse((new BufferedInputStream(new ByteArrayInputStream(EsfString.stringToBytes(signedXml)))));
        	
        	NodeList qualifyingPropertiesNodeList = document.getElementsByTagName("QualifyingProperties");
        	if ( qualifyingPropertiesNodeList != null ) 
        	{
        		for( int i=0; i < qualifyingPropertiesNodeList.getLength(); ++i )
        		{
        			Element qualifyingPropertiesElement = (Element)qualifyingPropertiesNodeList.item(i);
        			if ( qualifyingPropertiesElement != null )
        				qualifyingPropertiesElement.setIdAttribute("Id",true); // mark our id as the "Id" attribute 
        		}
        	}

        	NodeList nl = document.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
        	if (nl.getLength() == 0) 
        	{
        		Application.getInstance().err("XmlDigitalSignature.verifyAcceptEmbeddedKeysIfKeyIdNotFound() SERIOUS-ERROR no Signature element");
        		throw new EsfException("The signedXML has no Signature element");
        	}

        	// The embedded key must match our signatureKey's values or the validation will fail.
        	DOMValidateContext valContext = new DOMValidateContext(new AcceptEmbeddedKeySelector(), nl.item(0));

        	XMLSignature signature = xmlSignatureFactory.unmarshalXMLSignature(valContext);
        	if ( ! signature.validate(valContext) ) 
        	{
        	    boolean isSignatureValueValid = signature.getSignatureValue().validate(valContext);
        		Application.getInstance().err("XmlDigitalSignature.verifyAcceptEmbeddedKeysIfKeyIdNotFound() SERIOUS-ERROR Failed core validation; SignatureValue validated; " + isSignatureValueValid);
        		StringBuilder exceptionText = new StringBuilder();
        		exceptionText.append("Invalid signature. SignatureValue ").append(isSignatureValueValid ? "validated" : "INVALID");
    	        Iterator<?> i = signature.getSignedInfo().getReferences().iterator();
    	        for (int j=0; i.hasNext(); j++) 
    	        {
    	            boolean refValid = ((Reference)i.next()).validate(valContext);
            		Application.getInstance().err(" -> Reference["+j+"] validated: " + refValid);
            		exceptionText.append("; ").append(getReferenceName(j)).append(refValid ? " validated" : " INVALID");
    	        }
        	    throw new EsfException(exceptionText.toString());
        	}
    	}
    	catch( Exception e )
    	{
    		Application.getInstance().except(e, "XmlDigitalSignature.verifyAcceptEmbeddedKeysIfKeyIdNotFound() SERIOUS-ERROR");
    		throw new EsfException("Invalid signature: " + e.getMessage());
    	}
    }
    
	protected static class VerifyingKeySelector extends KeySelector 
	{
		SignatureKey signatureKey; 
		
		public VerifyingKeySelector(SignatureKey signatureKey)
		{
			this.signatureKey = signatureKey;
		}
		public VerifyingKeySelector()
		{
			this.signatureKey = null;
		}
		
		@Override
		public KeySelectorResult select(KeyInfo keyInfo, Purpose purpose, AlgorithmMethod method, XMLCryptoContext context)
				throws KeySelectorException 
		{
			if (keyInfo == null) 
				throw new KeySelectorException("NULL KeyInfo");

		    SignatureMethod sm = (SignatureMethod) method;
		    List<?> list = keyInfo.getContent();

    		EsfUUID keyId = null;
    		PublicKey publicKey = null;
		    for (int i = 0; i < list.size(); i++) 
		    {
		    	XMLStructure xmlStructure = (XMLStructure)list.get(i);
		    	if (xmlStructure instanceof KeyName)
		    	{
		    		String name = ((KeyName)xmlStructure).getName();
		    		keyId = new EsfUUID(name);
		    	}
		    	else if (xmlStructure instanceof KeyValue) 
		    	{
		    		try 
		    		{
		    			publicKey = ((KeyValue)xmlStructure).getPublicKey();
		    		} 
		    		catch( KeyException ke ) 
		    		{
		    			throw new KeySelectorException(ke);
		    		}
		    	}
		    }
		    
		    if ( keyId == null || keyId.isNull() || publicKey == null )
		    	throw new KeySelectorException("XML Signature element needs KeyName and KeyValue to verify");
		    
		    // If we don't have a signatureKey set, let's see if we can find it by the id
		    if ( signatureKey == null )
			    signatureKey = SignatureKey.Manager.getById(keyId);
		    
		    if ( signatureKey == null )
			    throw new KeySelectorException("KeyName and KeyValue cannot be verified without a SignatureKey");
		    
		    if ( keyId.equals(signatureKey.getId()) && 
		    	 publicKey.equals(signatureKey.getPublicKey()) && 
		    	 algEquals(sm.getAlgorithm(), publicKey.getAlgorithm()) 
		    	)
    			return new SimpleKeySelectorResult(publicKey);
		    	
		    throw new KeySelectorException("KeyName and KeyValue are not correct and do not match the expected SignatureKey");
		}
	} 
	
	protected static class AcceptEmbeddedKeySelector extends KeySelector 
	{
		public AcceptEmbeddedKeySelector() {}
		
		@Override
		public KeySelectorResult select(KeyInfo keyInfo, Purpose purpose, AlgorithmMethod method, XMLCryptoContext context)
				throws KeySelectorException 
		{
			if (keyInfo == null) 
				throw new KeySelectorException("NULL KeyInfo");

		    SignatureMethod sm = (SignatureMethod) method;
		    List<?> list = keyInfo.getContent();

    		EsfUUID keyId = null;
    		PublicKey publicKey = null;
		    for (int i = 0; i < list.size(); i++) 
		    {
		    	XMLStructure xmlStructure = (XMLStructure)list.get(i);
		    	if (xmlStructure instanceof KeyName)
		    	{
		    		String name = ((KeyName)xmlStructure).getName();
		    		keyId = new EsfUUID(name);
		    	}
		    	else if (xmlStructure instanceof KeyValue) 
		    	{
		    		try 
		    		{
		    			publicKey = ((KeyValue)xmlStructure).getPublicKey();
		    		} 
		    		catch( KeyException ke ) 
		    		{
		    			throw new KeySelectorException(ke);
		    		}
		    	}
		    }
		    
		    if ( keyId == null || keyId.isNull() || publicKey == null )
		    	throw new KeySelectorException("XML Signature element needs KeyName and KeyValue as these are always present when valid, even if not trusted");
		    
		    // If we can find the key that uses the keyid specified, we'll ensure the public key is valid, but if not found,
		    // we'll implicitly trust it for this method's purpose.
		    SignatureKey signatureKey = SignatureKey.Manager.getById(keyId);
		    if ( signatureKey == null )
		    {
			    if ( algEquals(sm.getAlgorithm(), publicKey.getAlgorithm()) )
	    			return new SimpleKeySelectorResult(publicKey);
		    }
		    else
		    {
			    if ( publicKey.equals(signatureKey.getPublicKey()) && algEquals(sm.getAlgorithm(), publicKey.getAlgorithm()) )
			    	return new SimpleKeySelectorResult(publicKey);
		    	
		    }
		    throw new KeySelectorException("KeyName and KeyValue are not correct");
		}
		

	} 
	
	public static class SimpleKeySelectorResult implements KeySelectorResult
	{
		PublicKey publicKey;
		SimpleKeySelectorResult(PublicKey publicKey)
		{
			this.publicKey = publicKey;
		}
		
		public Key getKey()
		{
			return publicKey;
		}
	}

	static boolean algEquals(String algURI, String algName) 
	{
	    return (algName.equalsIgnoreCase("DSA") && algURI.equalsIgnoreCase(SignatureMethod.DSA_SHA1)) ||
	           (algName.equalsIgnoreCase("RSA") && (algURI.equalsIgnoreCase(SignatureMethod_RSA_SHA512) || algURI.equalsIgnoreCase(SignatureMethod.RSA_SHA1)));
	}
    
}