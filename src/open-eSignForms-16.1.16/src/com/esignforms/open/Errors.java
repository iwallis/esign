// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open;

import java.util.ArrayList;

import com.esignforms.open.ErrorEntry;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.util.HtmlUtil;

/**
 * This class holds info/warning/error messages.  
 * @author Yozons, Inc.
 */
public class Errors
	implements java.io.Serializable
{
	private static final long serialVersionUID = -705011014679469490L;

	public final static String TYPE_ERROR 	= "error";		// "red" sad
	public final static String TYPE_WARNING = "warning";	// "yellow" concerned
	public final static String TYPE_SUCCESS	= "success";	// "green" happy
	public final static String TYPE_INFO 	= "info";		// "black" or "blue" neutral
	
	public final static boolean MSG_HTML 	= true;			// For our params to ErrorEntry
	public final static boolean MSG_TEXT 	= false;
	
    private ArrayList<ErrorEntry> errorEntryList;
    
    public Errors()
    {
    	errorEntryList = new ArrayList<ErrorEntry>();
    }
    
    public void clear()
    {
    	errorEntryList.clear();
    }
    
    public void add(Errors errors)
    {
    	errorEntryList.addAll(errors.getErrorEntries());
    }
    
    public final Errors addHtmlError(String msgHtml)
    {
    	if ( EsfString.isNonBlank(msgHtml) )
    	{
            ErrorEntry entry = new ErrorEntry(msgHtml,TYPE_ERROR,MSG_HTML,(String[])null);
            errorEntryList.add(entry);
    	}
        return this;
    }
    
    public final Errors addHtmlError(String msgHtml, String... fields)
    {
    	return addErrorEntry(msgHtml,TYPE_ERROR,MSG_HTML,fields);
    }
    
    public final Errors addHtmlWarning(String msgHtml, String... fields)
    {
    	return addErrorEntry(msgHtml,TYPE_WARNING,MSG_HTML,fields);
    }
    
    public final Errors addHtmlSuccess(String msgHtml, String... fields)
    {
    	return addErrorEntry(msgHtml,TYPE_SUCCESS,MSG_HTML,fields);
    }
    
    public final Errors addHtmlInfo(String msgHtml, String... fields)
    {
    	return addErrorEntry(msgHtml,TYPE_INFO,MSG_HTML,fields);
    }
    
    public final Errors addError(String msg)
    {
    	if ( EsfString.isNonBlank(msg) )
    	{
            ErrorEntry entry = new ErrorEntry(msg,TYPE_ERROR,MSG_TEXT,(String[])null);
            errorEntryList.add(entry);
    	}
        return this;
    }
    
    public final Errors addError(String msg, String... fields)
    {
    	return addErrorEntry(msg,TYPE_ERROR,MSG_TEXT,fields);
    }
    
    public final Errors addWarning(String msg, String... fields)
    {
    	return addErrorEntry(msg,TYPE_WARNING,MSG_TEXT,fields);
    }
    
    public final Errors addSuccess(String msg, String... fields)
    {
    	return addErrorEntry(msg,TYPE_SUCCESS,MSG_TEXT,fields);
    }
    
    public final Errors addInfo(String msg, String... fields)
    {
    	return addErrorEntry(msg,TYPE_INFO,MSG_TEXT,fields);
    }
    
    private Errors addErrorEntry(String msgHtml, String type, boolean isHtml, String... fields)
    {
    	if ( EsfString.isNonBlank(msgHtml) )
    	{
    		ErrorEntry entry = new ErrorEntry(msgHtml,type,isHtml,fields);
    		errorEntryList.add(entry);
    	}
        return this;
    }
    
    public boolean hasError()
    {
    	for( ErrorEntry entry : errorEntryList )
    	{
    		if ( entry.isError() )
    			return true;
    	}
    	return false;
    }
    
    public boolean hasWarning()
    {
    	for( ErrorEntry entry : errorEntryList )
    	{
    		if ( entry.isWarning() )
    			return true;
    	}
    	return false;
    }
    
    public boolean hasErrorOrWarning()
    {
    	for( ErrorEntry entry : errorEntryList )
    	{
    		if ( entry.isErrorOrWarning() )
    			return true;
    	}
    	return false;
    }
    
    public boolean hasSuccess()
    {
    	for( ErrorEntry entry : errorEntryList )
    	{
    		if ( entry.isSuccess() )
    			return true;
    	}
    	return false;
    }
    
    public boolean hasInfo()
    {
    	for( ErrorEntry entry : errorEntryList )
    	{
    		if ( entry.isInfo() )
    			return true;
    	}
    	return false;
    }
    
    public boolean hasSuccessOrInfo()
    {
    	return hasSuccess() || hasInfo();
    }
    
    public boolean isErrorField(String checkField)
    {
    	for ( ErrorEntry e : errorEntryList )
    	{
    		if ( e.isErrorField(checkField) )
    			return true;
    	}
    	
    	return false;
    }

    public boolean isErrorOrWarningField(String checkField)
    {
    	for ( ErrorEntry e : errorEntryList )
    	{
    		if ( e.isErrorOrWarningField(checkField) )
    			return true;
    	}
    	
    	return false;
    }
    
    public int getNumErrorEntries() { return errorEntryList.size(); }
    public ArrayList<ErrorEntry> getErrorEntries() 
    { 
    	return errorEntryList;
    }
    
    public String toString()
    {
    	if ( errorEntryList.size() == 0 )
    		return "";
    	StringBuilder buf = new StringBuilder(errorEntryList.size()*80);
    	for ( ErrorEntry e : errorEntryList )
    	{
    		if ( buf.length() > 0 )
    			buf.append('\n');
    		buf.append(e.getMessage());
    	}
    	return buf.toString();
    }
    
    public String toHtml()
    {
    	if ( errorEntryList.size() == 0 )
    		return "";
		StringBuilder html = new StringBuilder();
		
		String image;
		if ( hasErrorOrWarning() )
			image = Application.getInstance().getImageIconForErrorEsfName().toString();
		else
			image = Application.getInstance().getImageIconForInfoEsfName().toString();
		
		html.append("<table cellpadding=\"0\" cellspacing=\"0\" class=\"errorEntryArea\"><tr>");
		html.append("<td><img src=\"").append(Application.getInstance().getContextPath()).append("/images/").append(image).append("/\" alt=\"user message icon\" />");
		html.append("</td>");
		
		html.append("<td class=\"w100\">");
		html.append("<ul class=\"errorEntryArea\">");
		for( ErrorEntry e : errorEntryList )
		{
			html.append("<li class=\"").append(e.getTypeCSS()).append("\">");
			if ( e.isHtml() )
				html.append(e.getMessage());
			else
				html.append(HtmlUtil.toDisplayHtml(e.getMessage()));
			html.append("</li>");
		}
		html.append("</ul>");
		html.append("</td>");
		
		html.append("</tr></table>");
    	return html.toString();
    }
}