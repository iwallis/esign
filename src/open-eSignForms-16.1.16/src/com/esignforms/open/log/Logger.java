// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.log;

/**
 * Logging framework (now based on Apache Log4J) for Open.eSignForms.com custom web applications.
 *
 * @author Yozons, Inc.
 */
public class Logger 
{
    private org.apache.log4j.Logger logger = null;
     
    protected Logger(String name)
    {
        logger = org.apache.log4j.Logger.getLogger(name);
    }
    
    public Logger()
    {
        this("OpenESignForms");
    }
    
    public Logger(Class<?> c)
    {
        this(c.getName());
    }
    
    final public boolean isDebugEnabled()
    {
        return logger.isDebugEnabled();
    }
    
    final public void setDebugOn()
    {
        logger.setLevel(org.apache.log4j.Level.DEBUG);
    }
    
    final public void setDebugOff()
    {
        logger.setLevel(org.apache.log4j.Level.INFO);
    }
    
    final public void error(String msg, Throwable t)
    {
        logger.error(msg,t == null ? new Throwable("Logger.error null throwable for stacktrace") : t);
    }
    final public void error(String msg)
    {
        logger.error(msg);
    }
    
    final public void warn(String msg)
    {
        logger.warn(msg);
    }
    final public void warn(String msg, Throwable t)
    {
        logger.warn(msg,t == null ? new Throwable("Logger.warn null throwable for stacktrace") : t);
    }
    
    final public void info(String msg)
    {
        logger.info(msg);
    }
    final public void info(String msg, Throwable t)
    {
        logger.info(msg,t == null ? new Throwable("Logger.info null throwable for stacktrace") : t);
    }
    
    final public void debug(String msg)
    {
        logger.debug(msg);
    }
    final public void debug(String msg, Throwable t)
    {
        logger.debug(msg,t == null ? new Throwable("Logger.debug null throwable for stacktrace") : t);
    }
    
    final public void fatal(String msg)
    {
        error(msg);
        System.exit(1);
    }
    final public void fatal(String msg, Throwable t)
    {
        error(msg,t == null ? new Throwable("Logger.fatal null throwable for stacktrace") : t);
        System.exit(1);
    }
    
    /**
     * Report an SQL error message
     * @param msg the String message to report
     * @param e the SQLException
     */
    final public void sqlerr( String msg, java.sql.SQLException e )
    {
        logger.error("SQLException: " + msg + ":");
        while (e != null) 
        {
            logger.error("  Message:   " + e.getMessage(),e);
            logger.error("  SQLState:  " + e.getSQLState());
            logger.error("  ErrorCode: " + e.getErrorCode());
            e = e.getNextException();
        }
    }
    final public void sqlerr( java.sql.SQLException e, String msg )
    {
        sqlerr(msg,e);
    }    
}