// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import com.esignforms.open.Application;
import com.esignforms.open.config.Literals;
import com.esignforms.open.config.MessageFormatFile;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.log.Logger;
import com.esignforms.open.util.DateUtil;

/**
 * Logging code for the esf_system_activity_log, esf_user_activity_log... tables
 *
 * @author Yozons, Inc.
 */
public class ActivityLog 
{
	// The esf_system_activity_log reserves the absolute value of log types 0 through 99.
    public static final short LOG_TYPE_SYSTEM_VERSION_CHANGE_PERMANENT	= -1;
    public static final short LOG_TYPE_SYSTEM_START_STOP				= 2; 
    public static final short LOG_TYPE_SYSTEM_USER_ACTIVITY				= 3; 
    public static final short LOG_TYPE_SYSTEM_CONFIG_CHANGE				= 4; 
    
	// The esf_user_activity_log reserves the absolute value of log types 100 through 199.
    public static final short LOG_TYPE_USER_LOGIN_LOGOFF				= 101;
    public static final short LOG_TYPE_USER_SECURITY					= 102; 
    public static final short LOG_TYPE_USER_CONFIG_CHANGE				= 103; 
    
	// The esf_transaction_activity_log reserves the absolute value of log types 200 through 299, though only positive values are used.
    public static final short LOG_TYPE_TRANSACTION_GENERAL				= 201;
    public static final short LOG_TYPE_TRANSACTION_ACTION_BASIC_TRACE	= 202; 
    public static final short LOG_TYPE_TRANSACTION_ACTION_DETAIL_TRACE	= 203; 
    
    public static String getLogTypeDescription(short type)
    {
    	switch( type )
    	{
    	case LOG_TYPE_SYSTEM_VERSION_CHANGE_PERMANENT:	return "System version change (" + type + ")";
    	case LOG_TYPE_SYSTEM_START_STOP:				return "System start/stop (" + type + ")";
    	case LOG_TYPE_SYSTEM_USER_ACTIVITY:				return "System user activity (" + type + ")";
    	case LOG_TYPE_SYSTEM_CONFIG_CHANGE:				return "System config change (" + type + ")";

    	case LOG_TYPE_USER_LOGIN_LOGOFF:				return "User login/logoff (" + type + ")";
    	case LOG_TYPE_USER_SECURITY:					return "User security (" + type + ")";
    	case LOG_TYPE_USER_CONFIG_CHANGE:				return "User config change (" + type + ")";
    	
    	case LOG_TYPE_TRANSACTION_GENERAL:				return "Transaction general (" + type + ")";
    	case LOG_TYPE_TRANSACTION_ACTION_BASIC_TRACE:	return "Transaction action (" + type + ")";
    	case LOG_TYPE_TRANSACTION_ACTION_DETAIL_TRACE:	return "Transaction action detail (" + type + ")";
    	
    	default: return "Unexpected type (" + type + ")";
    	}
    }
    
    public static String getLogTypeShortDescription(short type)
    {
    	MessageFormatFile mff = Application.getInstance().getServerMessages();
    	switch( type )
    	{
    	case LOG_TYPE_SYSTEM_VERSION_CHANGE_PERMANENT:	return mff.getString("ActivityLog.type.short.system.versionChange");
    	case LOG_TYPE_SYSTEM_START_STOP:				return mff.getString("ActivityLog.type.short.system.startStop");
    	case LOG_TYPE_SYSTEM_USER_ACTIVITY:				return mff.getString("ActivityLog.type.short.system.userActivity");
    	case LOG_TYPE_SYSTEM_CONFIG_CHANGE:				return mff.getString("ActivityLog.type.short.system.configChange");

    	case LOG_TYPE_USER_LOGIN_LOGOFF:				return mff.getString("ActivityLog.type.short.user.loginLogoff");
    	case LOG_TYPE_USER_SECURITY:					return mff.getString("ActivityLog.type.short.user.security");
    	case LOG_TYPE_USER_CONFIG_CHANGE:				return mff.getString("ActivityLog.type.short.user.configChange");
    	
    	case LOG_TYPE_TRANSACTION_GENERAL:				return mff.getString("ActivityLog.type.short.transaction.general");
    	case LOG_TYPE_TRANSACTION_ACTION_BASIC_TRACE:	return mff.getString("ActivityLog.type.short.transaction.basic");
    	case LOG_TYPE_TRANSACTION_ACTION_DETAIL_TRACE:	return mff.getString("ActivityLog.type.short.transaction.detail");
    	
    	default: return "?"+type+"?";
    	}
    }
    
    protected static ActivityLog _log  = null;
    
    protected Application       app;
    protected Logger            logger;
    
    
    
    // Only static methods are used so it's never instantiated
    public ActivityLog(Application app)
    {
        this.app = app;
        logger   = new Logger(ActivityLog.class);
    }
    
    public static ActivityLog getInstance()
    {
        return _log;
    }

    public static ActivityLog getInitialInstance(Application app)
    {
        _log = new ActivityLog(app);
        return _log;
    }
    
    /***************************** BEGIN SYSTEM ACTIVITY LOG ROUTINES *******************************/
    
    /**
     * Logs a record to the system activity log under the Connection object's transaction control.
     * @param logType the short type of the log record with positive numbers being log records that can 
     *  be aged off, while negative numbers indicate permanent records that shouldn't be deleted
     * @param logText the String text to log to the database
     * @exception java.sql.SQLException thrown if there's a DB error
     */
    protected void logSystem(Connection con, short logType, String logText)
        throws java.sql.SQLException
    {
        logSystem(con,logType,DateUtil.getCurrentSqlTimestamp(),logText);
    }
    
    
    protected void logSystem(Connection con, short logType, Timestamp when, String logText)
        throws java.sql.SQLException
    {
        if ( EsfString.isBlank(logText) )
            return;
        
        PreparedStatement stmt = null;
        
        if ( logText.length() > Literals.LOG_MAX_LENGTH )
            logText = logText.substring(0,Literals.LOG_MAX_LENGTH);
        
        logger.info("System: "+logText);

        try 
        {
            String sql = "INSERT INTO esf_system_activity_log (log_timestamp, log_type, log_text) VALUES(?,?,?)";
            
            stmt = con.prepareStatement(sql);
            stmt.setTimestamp(1,when);
            stmt.setInt(2,logType);
            stmt.setString(3,logText);
            
            int num = stmt.executeUpdate();
            if ( num != 1 )
            {
                logger.error("logSystem(con) - Failed to record: type: " + logType + "; text: " + logText);
            }
        }
        catch( java.sql.SQLException e )
        {
            logger.sqlerr("logSystem(con) - Failed to record: type: " + logType + "; text: " + logText,e);
            throw e;
        }
        finally
        {
            app.cleanupStatement(stmt);
        }
    }

    /**
     * Logs a record to the system activity log under its own transaction control.
     * @param logType the short type of the log record with positive numbers being log records that can 
     *  be aged off, while negative numbers indicate permanent records that shouldn't be deleted
     * @param logText the String text to log to the database
     */
    protected void logSystem(short logType, String logText)
    {
        ConnectionPool    pool = app.getConnectionPool();
        Connection        con  = pool.getConnection();
        
        try 
        {
            logSystem(con,logType,logText);
            con.commit();
        } 
        catch(java.sql.SQLException e) 
        {
            logger.sqlerr(e,"logSystem(" + logType + "," + logText + ")");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            app.cleanupPool(pool, con, null);
        }
    }
    
    public void logSystemVersionChange(Connection con, String message)
    	throws java.sql.SQLException
    {
        logSystem( con, LOG_TYPE_SYSTEM_VERSION_CHANGE_PERMANENT, message);
    }
    public void logSystemVersionChange(String message)
    {
        logSystem( LOG_TYPE_SYSTEM_VERSION_CHANGE_PERMANENT, message);
    }
    
    public void logSystemStartStop(Connection con, String message)
        throws java.sql.SQLException
    {
        logSystem( con, LOG_TYPE_SYSTEM_START_STOP, message);
    }
    public void logSystemStartStop(String message)
	{
	    logSystem( LOG_TYPE_SYSTEM_START_STOP, message);
	}
    
    public void logSystemUserActivity(Connection con, String message)
	    throws java.sql.SQLException
	{
	    logSystem( con, LOG_TYPE_SYSTEM_USER_ACTIVITY, message);
	}
	public void logSystemUserActivity(String message)
	{
	    logSystem( LOG_TYPE_SYSTEM_USER_ACTIVITY, message);
	}

    public void logSystemConfigChange(Connection con, String message)
	    throws java.sql.SQLException
	{
	    logSystem( con, LOG_TYPE_SYSTEM_CONFIG_CHANGE, message);
	}
	public void logSystemConfigChange(String message)
	{
	    logSystem( LOG_TYPE_SYSTEM_CONFIG_CHANGE, message);
	}

	
    protected int cleanupSystem(Connection con,EsfDateTime olderThan,short logType)
	    throws SQLException
	{
	    PreparedStatement stmt = null;
	    
	    try 
	    {
	        // Remove all non-permanent rows where they were logged before the date specified
	        Timestamp olderTimestamp = olderThan.toSqlTimestamp();
	        String sql = "DELETE FROM esf_system_activity_log WHERE log_type=? AND log_timestamp<?";
	        stmt = con.prepareStatement(sql);
	        stmt.setInt(1,logType);
	        stmt.setTimestamp(2,olderTimestamp);
	
	        int num = stmt.executeUpdate();
	        logger.info("cleanupSystem() - Removed " + num + 
	                     " " + getLogTypeDescription(logType) + " log entries before " + olderThan + "." );
	        return num;
	    } 
	    catch( java.sql.SQLException e )
	    {
	        logger.sqlerr("cleanupSystem()",e);
	        throw e;
	    }
	    finally
	    {
	        app.cleanupStatement(stmt);
	    }
	}

	public int cleanupSystemStartStop(Connection con,EsfDateTime olderThan)
		throws SQLException
	{
		return cleanupSystem(con,olderThan,LOG_TYPE_SYSTEM_START_STOP);
	}
	public int cleanupSystemUserActivity(Connection con,EsfDateTime olderThan)
		throws SQLException
	{
		return cleanupSystem(con,olderThan,LOG_TYPE_SYSTEM_USER_ACTIVITY);
	}
	public int cleanupSystemConfigChange(Connection con,EsfDateTime olderThan)
		throws SQLException
	{
		return cleanupSystem(con,olderThan,LOG_TYPE_SYSTEM_CONFIG_CHANGE);
	}
    /***************************** END OF SYSTEM ACTIVITY LOG ROUTINES *******************************/
    
    /***************************** BEGIN USER ACTIVITY LOG ROUTINES *******************************/
    
    /**
     * Logs a record to the user activity log under the Connection object's transaction control.
     * @param userId the EsfUUID of the user
     * @param logType the short type of the log record with positive numbers being log records that can 
     *  be aged off, while negative numbers indicate permanent records that shouldn't be deleted (unless the user record is deleted)
     * @param logText the String text to log to the database
     * @exception java.sql.SQLException thrown if there's a DB error
     */
    protected void logUser(Connection con, EsfUUID userId, short logType, String logText)
        throws java.sql.SQLException
    {
        logUser(con,userId,logType,DateUtil.getCurrentSqlTimestamp(),logText);
    }
    
    
    protected void logUser(Connection con, EsfUUID userId, short logType, Timestamp when, String logText)
        throws java.sql.SQLException
    {
        if ( EsfString.isBlank(logText) )
            return;
        
        PreparedStatement stmt = null;
        
        if ( logText.length() > Literals.LOG_MAX_LENGTH )
            logText = logText.substring(0,Literals.LOG_MAX_LENGTH);
        
        logger.info("User: "+ userId + " - " + logText);

        try 
        {
            String sql = "INSERT INTO esf_user_activity_log (user_id, log_timestamp, log_type, log_text) VALUES(?,?,?,?)";
            
            stmt = con.prepareStatement(sql);
            stmt.setObject(1,userId.getUUID(),Types.OTHER);
            stmt.setTimestamp(2,when);
            stmt.setInt(3,logType);
            stmt.setString(4,logText);
            
            int num = stmt.executeUpdate();
            if ( num != 1 )
            {
                logger.error("logUser(con) - Failed to record: userId: " + userId + "; type: " + logType + "; text: " + logText);
            }
        }
        catch( java.sql.SQLException e )
        {
            logger.sqlerr("logUser(con) - Failed to record: userId: " + userId + "; type: " + logType + "; text: " + logText,e);
            throw e;
        }
        finally
        {
            app.cleanupStatement(stmt);
        }
    }

    /**
     * Logs a record to the user activity log under its own transaction control.
     * @param userId the EsfUUID of the user
     * @param logType the short type of the log record with positive numbers being log records that can 
     *  be aged off, while negative numbers indicate permanent records that shouldn't be deleted (unless the user record is deleted)
     * @param logText the String text to log to the database
     */
    protected void logUser(EsfUUID userId, short logType, String logText)
    {
        ConnectionPool    pool = app.getConnectionPool();
        Connection        con  = pool.getConnection();
        
        try 
        {
            logUser(con,userId,logType,logText);
            con.commit();
        } 
        catch(java.sql.SQLException e) 
        {
            logger.sqlerr(e,"logUser(" + userId + "," + logType + "," + logText + ")");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            app.cleanupPool(pool, con, null);
        }
    }
    
    public void logUserLoginLogoff(Connection con, EsfUUID userId, String message)
    	throws java.sql.SQLException
    {
        logUser( con, userId, LOG_TYPE_USER_LOGIN_LOGOFF, message);
    }
    public void logUserLoginLogoff(EsfUUID userId, String message)
    {
        logUser( userId, LOG_TYPE_USER_LOGIN_LOGOFF, message);
    }

    public void logUserSecurity(Connection con, EsfUUID userId, String message)
		throws java.sql.SQLException
	{
	    logUser( con, userId, LOG_TYPE_USER_SECURITY, message);
	}
	public void logUserSecurity(EsfUUID userId, String message)
	{
	    logUser( userId, LOG_TYPE_USER_SECURITY, message);
	}

    public void logUserConfigChange(Connection con, EsfUUID userId, String message)
		throws java.sql.SQLException
	{
	    logUser( con, userId, LOG_TYPE_USER_CONFIG_CHANGE, message);
	}
	public void logUserConfigChange(EsfUUID userId, String message)
	{
	    logUser( userId, LOG_TYPE_USER_CONFIG_CHANGE, message);
	}

    public int deleteAllForUser(Connection con, EsfUUID userId)
        throws SQLException
    {
        PreparedStatement stmt = null;
        
        try 
        {
            String sql = "DELETE FROM esf_user_activity_log WHERE user_id=?";
            stmt = con.prepareStatement(sql);
            stmt.setObject(1,userId.getUUID(),Types.OTHER);
            int num = stmt.executeUpdate();
            logger.info("deleteAllForUser() - Removed " + num + 
                         " log entries with userId: " + userId );
            return num;
        } 
        catch( java.sql.SQLException e )
        {
            logger.sqlerr("deleteAllForUser() userId: " + userId,e);
            throw e;
        }
        finally
        {
            app.cleanupStatement(stmt);
        }
    }
    
    protected int cleanupUser(Connection con,EsfDateTime olderThan,short logType)
	    throws SQLException
	{
	    PreparedStatement stmt = null;
	    
	    try 
	    {
	        // Remove all non-permanent rows where they were logged before the date specified
	        Timestamp olderTimestamp = olderThan.toSqlTimestamp();
	        String sql = "DELETE FROM esf_user_activity_log WHERE log_type=? AND log_timestamp<?";
	        stmt = con.prepareStatement(sql);
	        stmt.setInt(1,logType);
	        stmt.setTimestamp(2,olderTimestamp);
	
	        int num = stmt.executeUpdate();
	        logger.info("cleanupUser() - Removed " + num + 
	                     " " + getLogTypeDescription(logType) + " log entries before " + olderThan + "." );
	        return num;
	    } 
	    catch( java.sql.SQLException e )
	    {
	        logger.sqlerr("cleanupUser()",e);
	        throw e;
	    }
	    finally
	    {
	        app.cleanupStatement(stmt);
	    }
	}
	
	public int cleanupUserLoginLogoff(Connection con,EsfDateTime olderThan)
		throws SQLException
	{
		return cleanupUser(con,olderThan,LOG_TYPE_USER_LOGIN_LOGOFF);
	}
	public int cleanupUserSecurity(Connection con,EsfDateTime olderThan)
		throws SQLException
	{
		return cleanupUser(con,olderThan,LOG_TYPE_USER_SECURITY);
	}
	public int cleanupUserConfigChange(Connection con,EsfDateTime olderThan)
		throws SQLException
	{
		return cleanupUser(con,olderThan,LOG_TYPE_USER_CONFIG_CHANGE);
	}
	
	
    /***************************** END OF USER ACTIVITY LOG ROUTINES *******************************/
	
    /***************************** BEGIN TRANSACTION ACTIVITY LOG ROUTINES *******************************/
    
    /**
     * Logs a record to the transaction activity log under the Connection object's transaction control.
     * @param tranId the EsfUUID of the transaction
     * @param logType the short type of the log record with positive numbers being log records that can 
     *  be aged off, while negative numbers indicate permanent records that shouldn't be deleted (unless the user record is deleted)
     * @param logText the String text to log to the database
     * @exception java.sql.SQLException thrown if there's a DB error
     */
    protected void logTransaction(Connection con, EsfUUID tranId, short logType, String logText)
        throws java.sql.SQLException
    {
        logTransaction(con,tranId,logType,DateUtil.getCurrentSqlTimestamp(),logText);
    }
    
    
    protected void logTransaction(Connection con, EsfUUID tranId, short logType, Timestamp when, String logText)
        throws java.sql.SQLException
    {
        if ( EsfString.isBlank(logText) )
            return;
        
        PreparedStatement stmt = null;
        
        if ( logText.length() > Literals.LOG_MAX_LENGTH )
            logText = logText.substring(0,Literals.LOG_MAX_LENGTH);
        
        logger.info("tranId: "+ tranId + " - " + logText);

        try 
        {
            String sql = "INSERT INTO esf_transaction_activity_log (transaction_id, log_timestamp, log_type, log_text) VALUES(?,?,?,?)";
            
            stmt = con.prepareStatement(sql);
            stmt.setObject(1,tranId.getUUID(),Types.OTHER);
            stmt.setTimestamp(2,when);
            stmt.setInt(3,logType);
            stmt.setString(4,logText);
            
            int num = stmt.executeUpdate();
            if ( num != 1 )
            {
                logger.error("logUser(con) - Failed to record: tranId: " + tranId + "; type: " + logType + "; text: " + logText);
            }
        }
        catch( java.sql.SQLException e )
        {
            logger.sqlerr("logUser(con) - Failed to record: tranId: " + tranId + "; type: " + logType + "; text: " + logText,e);
            throw e;
        }
        finally
        {
            app.cleanupStatement(stmt);
        }
    }

    /**
     * Logs a record to the transaction activity log under its own transaction control.
     * @param tranId the EsfUUID of the transaction
     * @param logType the short type of the log record with positive numbers being log records that can 
     *  be aged off, while negative numbers indicate permanent records that shouldn't be deleted (unless the user record is deleted)
     * @param logText the String text to log to the database
     */
    protected void logTransaction(EsfUUID tranId, short logType, String logText)
    {
        ConnectionPool    pool = app.getConnectionPool();
        Connection        con  = pool.getConnection();
        
        try 
        {
            logUser(con,tranId,logType,logText);
            con.commit();
        } 
        catch(java.sql.SQLException e) 
        {
            logger.sqlerr(e,"logTransaction(" + tranId + "," + logType + "," + logText + ")");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            app.cleanupPool(pool, con, null);
        }
    }
    
    public void logTransactionGeneral(Connection con, EsfUUID tranId, String message)
		throws java.sql.SQLException
	{
	    logTransaction( con, tranId, LOG_TYPE_TRANSACTION_GENERAL, message);
	}
	public void logTransactionGeneral(EsfUUID tranId, String message)
	{
		logTransaction( tranId, LOG_TYPE_TRANSACTION_GENERAL, message);
	}

    public void logTransactionActionBasic(Connection con, EsfUUID tranId, String message)
    	throws java.sql.SQLException
    {
        logTransaction( con, tranId, LOG_TYPE_TRANSACTION_ACTION_BASIC_TRACE, message);
    }
    public void logTransactionActionBasic(EsfUUID tranId, String message)
    {
    	logTransaction( tranId, LOG_TYPE_TRANSACTION_ACTION_BASIC_TRACE, message);
    }

    public void logTransactionActionDetail(Connection con, EsfUUID tranId, String message)
		throws java.sql.SQLException
	{
	    logTransaction( con, tranId, LOG_TYPE_TRANSACTION_ACTION_DETAIL_TRACE, message);
	}
	public void logTransactionActionDetail(EsfUUID tranId, String message)
	{
		logTransaction( tranId, LOG_TYPE_TRANSACTION_ACTION_DETAIL_TRACE, message);
	}

    public int deleteAllForTransaction(Connection con, EsfUUID tranId)
        throws SQLException
    {
        PreparedStatement stmt = null;
        
        try 
        {
            String sql = "DELETE FROM esf_transaction_activity_log WHERE transaction_id=?";
            stmt = con.prepareStatement(sql);
            stmt.setObject(1,tranId.getUUID(),Types.OTHER);
            int num = stmt.executeUpdate();
            logger.info("deleteAllForTransaction() - Removed " + num + 
                         " log entries with tranId: " + tranId );
            return num;
        } 
        catch( java.sql.SQLException e )
        {
            logger.sqlerr("deleteAllForTransaction() tranId: " + tranId,e);
            throw e;
        }
        finally
        {
            app.cleanupStatement(stmt);
        }
    }
    
    /***************************** END OF TRANSACTION ACTIVITY LOG ROUTINES *******************************/

    
}