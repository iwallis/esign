// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.log;

import java.sql.Connection;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.db.DatabaseObject;

/**
 * Represents a single log record retrieved from the system activity log.
 *
 * @author Yozons, Inc.
 */
public class SystemActivityLogInfo
	extends com.esignforms.open.db.DatabaseObject 
	implements java.lang.Comparable<SystemActivityLogInfo>
{
	private static final long serialVersionUID = -6435866566556063713L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(SystemActivityLogInfo.class);

	private EsfDateTime logTimestamp;
	private short logType;
	private String logText;
	
	public SystemActivityLogInfo(EsfDateTime logTimestamp, short logType, String logText)
    {
		this.logTimestamp = logTimestamp;
		this.logType = logType;
		this.logText = logText;
    }
    
	public EsfDateTime getTimestamp()
	{
		return logTimestamp;
	}
	
	public short getType()
	{
		return logType;
	}
	
    public boolean isPermanent()
    {
        return logType < 0;
    }
	
	public String getText()
	{
		return logText;
	}
	
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof SystemActivityLogInfo )
        {
        	SystemActivityLogInfo other = (SystemActivityLogInfo)o;
            return getTimestamp().equals(other.getTimestamp()) && getType() == other.getType() && getText().equals(other.getText());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getTimestamp().hashCode() + getType() + getText().hashCode();
    }
    
    @Override
    public int compareTo(SystemActivityLogInfo o)
    {
    	int diff = getTimestamp().compareTo(o.getTimestamp());
    	if ( diff != 0 )
    		return diff;
    	diff = getType() - o.getType();
    	if ( diff != 0 )
    		return diff;
    	return getText().compareTo(o.getText());
    }

	
	/******************* DB routines */
    public static class Manager
    {
    	public static List<SystemActivityLogInfo> getMatching(Connection con, EsfDateTime fromDateTime, EsfDateTime toDateTime, List<Short> limitLogTypes, String limitLogTextContains)
			throws java.sql.SQLException
		{
			EsfPreparedStatement stmt = null;
			
			StringBuilder sqlBuf = new StringBuilder(1000);
			sqlBuf.append("SELECT log_timestamp, log_type, log_text FROM esf_system_activity_log ");
			
			boolean needsWhere = true;
			boolean needsAnd = false;
			if ( fromDateTime != null )
			{
				if ( needsWhere ) 
				{
					sqlBuf.append("WHERE ");
					needsWhere = false;
				}
				if ( needsAnd )
					sqlBuf.append("AND ");
				sqlBuf.append("log_timestamp>=? ");
				needsAnd = true;
			}
			if ( toDateTime != null )
			{
				if ( needsWhere ) 
				{
					sqlBuf.append("WHERE ");
					needsWhere = false;
				}
				if ( needsAnd )
					sqlBuf.append("AND ");
				sqlBuf.append("log_timestamp<=? ");
				needsAnd = true;
			}
			if ( limitLogTypes != null && limitLogTypes.size() > 0 )
			{
				if ( needsWhere ) 
				{
					sqlBuf.append("WHERE ");
					needsWhere = false;
				}
				if ( needsAnd )
					sqlBuf.append("AND ");
				
				sqlBuf.append("(");
				boolean needsOr = false;
				for( int i=0; i < limitLogTypes.size(); ++i )
				{
					if ( needsOr )
						sqlBuf.append("OR ");
					else
						needsOr = true;
					sqlBuf.append("abs(log_type)=? ");
				}
				sqlBuf.append(") ");
				needsAnd = true;
			}
			if ( EsfString.isNonBlank(limitLogTextContains) )
			{
				if ( needsWhere ) 
				{
					sqlBuf.append("WHERE ");
					needsWhere = false;
				}
				if ( needsAnd )
					sqlBuf.append("AND ");
				sqlBuf.append("lower(log_text) LIKE ? ");
				needsAnd = true;
			}
		    
			sqlBuf.append("ORDER BY log_timestamp DESC LIMIT 20000"); // TODO Make limit something controllable by the user
			
			LinkedList<SystemActivityLogInfo> list = new LinkedList<SystemActivityLogInfo>();
			
		    try 
		    {
		    	stmt = new EsfPreparedStatement( con, sqlBuf.toString() );
		    	
				if ( fromDateTime != null )
					stmt.set(fromDateTime);
				if ( toDateTime != null )
					stmt.set(toDateTime);
				if ( limitLogTypes != null && limitLogTypes.size() > 0 )
				{
					for( short logType : limitLogTypes )
						stmt.set(logType);
				}
				if ( EsfString.isNonBlank(limitLogTextContains) )
					stmt.set("%"+DatabaseObject.escapeLIKE(limitLogTextContains.toLowerCase())+"%");
		        
				EsfResultSet rs = stmt.executeQuery();
				while( rs.next() )
				{
					EsfDateTime timestamp = rs.getEsfDateTime();
					short type = rs.getShort();
					String text = rs.getString();
					SystemActivityLogInfo logInfo = new SystemActivityLogInfo(timestamp,type,text);
					list.add(logInfo);
				}
				
				return list;
			}
		    catch( java.sql.SQLException e )
		    {
		        _logger.sqlerr(e,"Manager.getMatching(" + fromDateTime + "," + toDateTime + ")");
		        throw e;
		    }
		    finally
		    {
		    	cleanupStatement(stmt);
		    }
		}
	
		/**
		 * Retrieves matching system activity log records.
		 * @param fromDateTime the EsfDateTime of the earliest/oldest date/time to retrieve.  If null, no lower date range is set (from earliest date possible)
		 * @param toDateTime the EsfDateTime of the latest/newest date/time to retrieve.  If null, no upper date range is set (through current time)
		 * @param limitLogTypes a list of log types to include; if null or empty, all log types are retrieved
		 * @param limitLogTextContains a contains limit string on the log text; if null or blank, no restriction on the log record contents is done
		 * @return
		 */
		public static List<SystemActivityLogInfo> getMatching(EsfDateTime fromDateTime, EsfDateTime toDateTime, List<Short> limitLogTypes, String limitLogTextContains)
		{
		    ConnectionPool    pool = getConnectionPool();
		    Connection        con  = pool.getConnection();
		    
		    try 
		    {
		    	List<SystemActivityLogInfo> list = getMatching(con, fromDateTime, toDateTime, limitLogTypes, limitLogTextContains);
		        con.commit();
		        return list;
		    } 
		    catch(java.sql.SQLException e) 
		    {
		        pool.rollbackIgnoreException(con,e);
		        return null;
		    }
		    finally
		    {
		    	cleanupPool(pool,con,null);
		    }
		}
		
    } // end Manager

}