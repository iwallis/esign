// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.log;

import java.sql.Connection;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.db.DatabaseObject;

/**
 * Represents a single log record retrieved from the user activity log.
 *
 * @author Yozons, Inc.
 */
public class UserActivityLogInfo
	extends com.esignforms.open.db.DatabaseObject 
	implements java.lang.Comparable<UserActivityLogInfo>
{
 	private static final long serialVersionUID = 8665005093599685832L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(UserActivityLogInfo.class);

    private EsfUUID userId;
	private EsfDateTime logTimestamp;
	private short logType;
	private String logText;
	
	public UserActivityLogInfo(EsfUUID userId, EsfDateTime logTimestamp, short logType, String logText)
    {
		this.userId = userId;
		this.logTimestamp = logTimestamp;
		this.logType = logType;
		this.logText = logText;
    }
    
	public EsfUUID getUserId()
	{
		return userId;
	}
	
	public EsfDateTime getTimestamp()
	{
		return logTimestamp;
	}
	
	public short getType()
	{
		return logType;
	}
	
    public boolean isPermanent()
    {
        return logType < 0;
    }
	
	public String getText()
	{
		return logText;
	}
	
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof UserActivityLogInfo )
        {
        	UserActivityLogInfo other = (UserActivityLogInfo)o;
            return getUserId().equals(other.getUserId()) && getTimestamp().equals(other.getTimestamp()) && getType() == other.getType() && getText().equals(other.getText());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getUserId().hashCode() + getTimestamp().hashCode() + getType() + getText().hashCode();
    }
    
    @Override
    public int compareTo(UserActivityLogInfo o)
    {
    	int diff = getUserId().compareTo(o.getUserId());
    	if ( diff != 0 )
    		return diff;
    	diff = getTimestamp().compareTo(o.getTimestamp());
    	if ( diff != 0 )
    		return diff;
    	diff = getType() - o.getType();
    	if ( diff != 0 )
    		return diff;
    	return getText().compareTo(o.getText());
    }

	
	/******************* DB routines */
    public static class Manager
    {
    	public static List<UserActivityLogInfo> getMatching(Connection con, EsfUUID userId, EsfDateTime fromDateTime, EsfDateTime toDateTime, List<Short> limitLogTypes, String limitLogTextContains)
			throws java.sql.SQLException
		{
			EsfPreparedStatement stmt = null;
			
			StringBuilder sqlBuf = new StringBuilder(1000);
			sqlBuf.append("SELECT user_id, log_timestamp, log_type, log_text FROM esf_user_activity_log ");
			
			boolean needsWhere = true;
			boolean needsAnd = false;
			if ( userId != null && ! userId.isNull() )
			{
				if ( needsWhere ) 
				{
					sqlBuf.append("WHERE ");
					needsWhere = false;
				}
				if ( needsAnd )
					sqlBuf.append("AND ");
				sqlBuf.append("user_id=? ");
				needsAnd = true;
			}
			if ( fromDateTime != null )
			{
				if ( needsWhere ) 
				{
					sqlBuf.append("WHERE ");
					needsWhere = false;
				}
				if ( needsAnd )
					sqlBuf.append("AND ");
				sqlBuf.append("log_timestamp>=? ");
				needsAnd = true;
			}
			if ( toDateTime != null )
			{
				if ( needsWhere ) 
				{
					sqlBuf.append("WHERE ");
					needsWhere = false;
				}
				if ( needsAnd )
					sqlBuf.append("AND ");
				sqlBuf.append("log_timestamp<=? ");
				needsAnd = true;
			}
			if ( limitLogTypes != null && limitLogTypes.size() > 0 )
			{
				if ( needsWhere ) 
				{
					sqlBuf.append("WHERE ");
					needsWhere = false;
				}
				if ( needsAnd )
					sqlBuf.append("AND ");
				
				sqlBuf.append("(");
				boolean needsOr = false;
				for( int i=0; i < limitLogTypes.size(); ++i )
				{
					if ( needsOr )
						sqlBuf.append("OR ");
					else
						needsOr = true;
					sqlBuf.append("abs(log_type)=? ");
				}
				sqlBuf.append(") ");
				needsAnd = true;
			}
			if ( EsfString.isNonBlank(limitLogTextContains) )
			{
				if ( needsWhere ) 
				{
					sqlBuf.append("WHERE ");
					needsWhere = false;
				}
				if ( needsAnd )
					sqlBuf.append("AND ");
				sqlBuf.append("lower(log_text) LIKE ? ");
				needsAnd = true;
			}
		    
			sqlBuf.append("ORDER BY log_timestamp DESC LIMIT 20000"); // TODO Make limit something controllable by the user
			
			LinkedList<UserActivityLogInfo> list = new LinkedList<UserActivityLogInfo>();
			
		    try 
		    {
		    	stmt = new EsfPreparedStatement( con, sqlBuf.toString() );
		    	
				if ( userId != null && ! userId.isNull() )
					stmt.set(userId);
				if ( fromDateTime != null )
					stmt.set(fromDateTime);
				if ( toDateTime != null )
					stmt.set(toDateTime);
				if ( limitLogTypes != null && limitLogTypes.size() > 0 )
				{
					for( short logType : limitLogTypes )
						stmt.set(logType);
				}
				if ( EsfString.isNonBlank(limitLogTextContains) )
					stmt.set("%"+DatabaseObject.escapeLIKE(limitLogTextContains.toLowerCase())+"%");
		        
				EsfResultSet rs = stmt.executeQuery();
				while( rs.next() )
				{
					EsfUUID id = rs.getEsfUUID();
					EsfDateTime timestamp = rs.getEsfDateTime();
					short type = rs.getShort();
					String text = rs.getString();
					UserActivityLogInfo logInfo = new UserActivityLogInfo(id,timestamp,type,text);
					list.add(logInfo);
				}
				
				return list;
			}
		    catch( java.sql.SQLException e )
		    {
		        _logger.sqlerr(e,"Manager.getMatching(" + userId + "," + fromDateTime + "," + toDateTime + ")");
		        throw e;
		    }
		    finally
		    {
		    	cleanupStatement(stmt);
		    }
		}
	
		/**
		 * Retrieves matching user activity log records.
		 * @param userId the EsfUUID of the user to retrieve.  If null, does not limit by user id.
		 * @param fromDateTime the EsfDateTime of the earliest/oldest date/time to retrieve.  If null, no lower date range is set (from earliest date possible)
		 * @param toDateTime the EsfDateTime of the latest/newest date/time to retrieve.  If null, no upper date range is set (through current time)
		 * @param limitLogTypes a list of log types to include; if null or empty, all log types are retrieved
		 * @param limitLogTextContains a contains limit string on the log text; if null or blank, no restriction on the log record contents is done
		 * @return
		 */
		public static List<UserActivityLogInfo> getMatching(EsfUUID userId, EsfDateTime fromDateTime, EsfDateTime toDateTime, List<Short> limitLogTypes, String limitLogTextContains)
		{
		    ConnectionPool    pool = getConnectionPool();
		    Connection        con  = pool.getConnection();
		    
		    try 
		    {
		    	List<UserActivityLogInfo> list = getMatching(con, userId, fromDateTime, toDateTime, limitLogTypes, limitLogTextContains);
		        con.commit();
		        return list;
		    } 
		    catch(java.sql.SQLException e) 
		    {
		        pool.rollbackIgnoreException(con,e);
		        return null;
		    }
		    finally
		    {
		    	cleanupPool(pool,con,null);
		    }
		}
		
    } // end Manager

}