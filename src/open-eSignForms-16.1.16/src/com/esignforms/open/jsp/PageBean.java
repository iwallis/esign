// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp;

import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import javax.servlet.http.*;

import com.esignforms.open.Application;
import com.esignforms.open.admin.SessionTracker;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.Errors;
import com.esignforms.open.util.HtmlUtil;
import com.esignforms.open.util.XmlUtil;
import com.esignforms.open.log.Logger;
import com.esignforms.open.user.User;
import com.esignforms.open.user.UserLoginInfo;
import com.esignforms.open.util.FileUploader;
import com.esignforms.open.util.FileUploader.UploadToBufferInfo;
import com.esignforms.open.util.ServletUtil;

/**
 * The PageBean base class is used to encapsulate the various classes and methods commonly used
 * when developing web page beans for Open ESF.  These utility routines should make such pages
 * easier to develop and more consistent.
 * 
 * @author Yozons, Inc.
 */
public class PageBean
{
	public static final String CHARSET_UTF_8              	= EsfString.CHARSET_UTF_8;
    public static final String CONTENT_TYPE_CHARSET_UTF_8 	= "; charset=" + EsfString.CHARSET_UTF_8;
    
    public static final String URL_PARAM_TO_URL					= "toURL";
    public static final String URL_PARAM_CANCEL_URL				= "cancelURL";
    
    public static final String ESF_IS_API_MODE_ATTRIBUTE		= "ESF_IS_API_MODE";
    
    protected Logger           logger        = null;

    /**
     * The Application object when direct access is needed. Best to avoid direct use.
     */
    public Application         app           = Application.getInstance();

    /**
     * The context path for this web application
     */
    public String              contextPath   = app.getContextPath();
    
    /**
	 * The HttpSession object when direct access is needed. Best to avoid direct use.
	 */
	public HttpSession         session  = null;
	public boolean             doEndSession = false;
	/**
	 * The HttpServletRequest object when direct access is needed. Best to avoid direct use.
	 */
	public HttpServletRequest  request  = null;
    /**
     * The FileUploader object when direct access is needed. Best to avoid direct use.
     */
    public FileUploader         fileUploader      = null;
    public UploadToBufferInfo[] fileUploadBuffers = null;
	/**
	 * The HttpServletResponse object when direct access is needed. Best to avoid direct use.
	 */
	public HttpServletResponse response = null;
	/**
	 * Errors for this page
	 */
	public Errors errors = null;

	public String  paramToURL					= null; // Set when toURL is passed in
    public String  paramCancelURL				= null; // Set when cancelURL is passed in
    public boolean isTestMode					= false; // Set when TESTMODE is passed in
    public boolean isApiMode					= false; // Set for API-based requests

    /**
     *  This is the page state when the user clicks buttons on the page.  
     *  "edit" means that the page is editable, accepts data input
     *  "review" means that the page is now in review mode, before being signed
     *  "submitted" means that the page has been submitted (form sent), completed and/or signed by the party
     *  "viewonly" means that the page should only be displayed in view only mode, like review, but with no actions that can take place
     *  "error" means that the page is in an error state and should not display its contents.
     *  "printEdit" means that the page is to be displayed for printing only, but it has some edit fields that can be input first
     *  "printReview" means that the page is to be printed for hand signing after printing, faxing, etc.
     */
    public enum PageState { EDIT, REVIEW, SUBMITTED, VIEW_ONLY, ERROR, PRINT_EDIT, PRINT_REVIEW };
    public PageState pageState = PageState.EDIT;
    public final boolean isPageEdit()          { return PageState.EDIT.equals(pageState); }
    public final boolean isPageReview()        { return PageState.REVIEW.equals(pageState); }
    public final boolean isPageSubmitted()     { return PageState.SUBMITTED.equals(pageState); }
    public final boolean isPageViewOnly()      { return PageState.VIEW_ONLY.equals(pageState); }
    public final boolean isPagePrintEdit()     { return PageState.PRINT_EDIT.equals(pageState); }
    public final boolean isPagePrintReview()   { return PageState.PRINT_REVIEW.equals(pageState); }
    public final boolean isPagePrint()         { return isPagePrintEdit() || isPagePrintReview(); }
    public final boolean isPageError()         { return PageState.ERROR.equals(pageState); }
    
    public final void setPageEdit()            { pageState = PageState.EDIT; }
    public final void setPageReview()          { pageState = PageState.REVIEW; }
    public final void setPageSubmitted()       { pageState = PageState.SUBMITTED; }
    public final void setPageViewOnly()        { pageState = PageState.VIEW_ONLY; }
    public final void setPagePrintEdit()       { pageState = PageState.PRINT_EDIT; }
    public final void setPagePrintReview()     { pageState = PageState.PRINT_REVIEW; }
    public final void setPageError()           { pageState = PageState.ERROR; }

    protected User       loggedInUser     = null;
    protected String     ipAddr           = null;
	
	/**
	 * Constructs the PageBean instance.
	 */
	public PageBean()
	{
	}


    /**
     * Initializes the PageBean instance.
     * @param session the HttpSession for the requested the page; may be null if the session won't be used
     * @param request the HttpServletRequest for the requested page
     * @param response the HttpServletResponse to be returned
     */
    public void init(HttpSession session, HttpServletRequest request, HttpServletResponse response)
    {
        this.session  = session;
        this.request  = request;
        this.response = response;
        
        try
        {
            if ( request.getCharacterEncoding() == null )
            	request.setCharacterEncoding(CHARSET_UTF_8);
            response.setCharacterEncoding(CHARSET_UTF_8);
        }
        catch( UnsupportedEncodingException e ) 
        {
        	app.warning("PageBean.init() - Failed to set request/response to UTF-8 encoding.");
        }
        
        this.errors   = new Errors();
        contextPath   = request.getContextPath();
        
        if ( org.apache.commons.fileupload.servlet.ServletFileUpload.isMultipartContent(request) )
            fileUploader = new FileUploader(request);
        
        if ( hasFileUploader() )
            fileUploadBuffers  = fileUploader.uploadToBufferUncompressed(errors);
        
        paramToURL		= getParam(URL_PARAM_TO_URL);
        paramCancelURL	= getParam(URL_PARAM_CANCEL_URL);
    }
    
    protected Logger getLogger()
    {
        if ( logger == null )
            logger = new Logger(this.getClass());
        return logger;
    }

    public enum PageProcessing { CONTINUE, DONE };
    
    public PageProcessing doGet()
		throws java.io.IOException
    {
        throw new java.io.IOException("Base class PageBean's doGet() was called.");
    }

    public PageProcessing doPost()
		throws java.io.IOException
    {
    	throw new java.io.IOException("Base class PageBean's doPost() was called.");
    }
    
    public boolean isUrlForMyApplication(String url)
    {
    	if ( isBlank(url) )
    		return false;
    	return url.startsWith(".") || url.startsWith("/") || url.startsWith(getExternalUrlContextPath());
    }

    public String ensureUrlIsForMyApplication(String url)
    {
    	if ( ! isBlank(url) && ! isUrlForMyApplication(url) )
    	{
    		errors.addError("WARNING! It appears you may be a victim of a fraudulent link trying to trick you.");
    		errors.addError("Note: The link specified would redirect you to the following unsupported page: " + url);
    		errors.addError("We recommend that you not continue.");
    		return getExternalUrlContextPath()+"/";
    	}
    	return url;
    }

    String addHostNameToIPAddress(String ipAddress)
    {
		try
		{
			java.net.InetAddress iaddr = java.net.InetAddress.getByName(ipAddress);
			String host = iaddr.getHostName();
			if ( ! host.equals(ipAddress) )
				ipAddress = host + " (" + ipAddress + ")";
		}
		catch( java.net.UnknownHostException e )
		{
			// ignore exceptions
		}
   	
		return ipAddress;
    }
    
    
	/**
	 * Returns the IP address and optional host name (if it can be determined through reverse DNS) of the request.
	 * @return the String IP address or "hostname (ip address)" if the hostname can be determined.
	 */
	public final String getIP()
	{
		if ( ipAddr != null )
			return ipAddr;
			
		ipAddr = addHostNameToIPAddress( request.getRemoteAddr() );
		if ( session !=  null )
			session.setAttribute(SessionTracker.IPADDR_SESSION_ATTRIBUTE_NAME,ipAddr);
		
		return ipAddr;
	}
    
	/**
	 * Returns only the IP address of the request.
	 * @return the String IP address.
	 */
	public final String getIPOnly()
	{
		return request.getRemoteAddr(); 
	}
	
	
	static final String _IPv4_255 = "(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
	static final Pattern validIPv4Pattern = Pattern.compile("^(?:" + _IPv4_255 + "\\.){3}" + _IPv4_255 + "$");
	public static boolean isIPv4Valid(String ip) 
	{
		return validIPv4Pattern.matcher(ip).matches();
	}
	
	public static String longToIpV4(long longIp) 
	{
		int octet3 = (int) ((longIp >> 24) % 256);
		int octet2 = (int) ((longIp >> 16) % 256);
		int octet1 = (int) ((longIp >> 8) % 256);
		int octet0 = (int) ((longIp) % 256);
		return octet3 + "." + octet2 + "." + octet1 + "." + octet0;
	}
	public static long ipV4ToLong(String ip) 
	{
		String[] octets = ip.split("\\.");
		return (Long.parseLong(octets[0]) << 24) + (Integer.parseInt(octets[1]) << 16) +
		       (Integer.parseInt(octets[2]) << 8) + Integer.parseInt(octets[3]);
	}	
	final static long ipV4ToLong_10_0_0_0 = ipV4ToLong("10.0.0.0");
	final static long ipV4ToLong_10_255_255_255 = ipV4ToLong("10.255.255.255");
	final static long ipV4ToLong_172_16_0_0 = ipV4ToLong("172.16.0.0");
	final static long ipV4ToLong_172_31_255_255 = ipV4ToLong("172.31.255.255");
	final static long ipV4ToLong_192_168_0_0 = ipV4ToLong("192.168.0.0");
	final static long ipV4ToLong_192_168_255_255 = ipV4ToLong("192.168.255.255");
	public static boolean isIPv4Private(String ip) 
	{
		long longIp = ipV4ToLong(ip);
		return (longIp >= ipV4ToLong_10_0_0_0    && longIp <= ipV4ToLong_10_255_255_255)   ||
  	           (longIp >= ipV4ToLong_172_16_0_0  && longIp <= ipV4ToLong_172_31_255_255)   ||
		       (longIp >= ipV4ToLong_192_168_0_0 && longIp <= ipV4ToLong_192_168_255_255);
	}
	  
	public String[] getIPAddresses()
	{
		LinkedList<String> ipAddrList = new LinkedList<String>();

		String ip;
	    boolean found = false;

	    if ( (ip = request.getHeader("x-forwarded-for")) != null ) 
	    {
	    	debug("getIPAddresses() - found x-forwarded-for header: " + ip);
	    	StringTokenizer strtok = new StringTokenizer(ip, ",");

	    	while( strtok.hasMoreTokens() ) 
	    	{
	    		ip = strtok.nextToken().trim();
	
		        if ( isIPv4Valid(ip) ) 
		        {
			        if ( isIPv4Private(ip)) 
			        {
				    	debug("getIPAddresses() - found x-forwarded-for header with private IP addr: " + ip);
				    	ipAddrList.add( "x-forwarded-for " + ip );
			        }
			        else
			        {
				    	debug("getIPAddresses() - found x-forwarded-for header with public IP addr: " + ip);
				    	ipAddrList.add( "x-forwarded-for " + addHostNameToIPAddress(ip) );
			        	found = true;
		        	}
		        }
	    	}
	    }
	    
	    if ( (ip = request.getHeader("Proxy-Client-IP")) != null ) 
	    {
	    	debug("getIPAddresses() - found Proxy-Client-IP header: " + ip);
	    	ipAddrList.add( "Proxy-Client-IP " + addHostNameToIPAddress(ip) );
	    	found = true;
	    }    
	    
	    if ( (ip = request.getHeader("WL-Proxy-Client-IP")) != null ) 
	    {
	    	debug("getIPAddresses() - found WL-Proxy-Client-IP header: " + ip);
	    	ipAddrList.add( "WL-Proxy-Client-IP " + addHostNameToIPAddress(ip) );
	    	found = true;
	    }    

	    if ( ! found ) 
	    {
	    	ip = addHostNameToIPAddress(request.getRemoteAddr());
	    	debug("getIPAddresses() - using standard getRemoteAddr(): " + ip);
	    	ipAddrList.add( ip );
	    }

	    String[] ipAddrs = new String[ipAddrList.size()];
	    ipAddrList.toArray(ipAddrs);
	    return ipAddrs;
	}
	
	/**
	 * Note that this can return null if no user-agent header is specified.
	 * @see #getUserAgent() for one that returns 'No-user-agent-header-provided' when no such user-agent header is provided.
	 * @return null if no user-agent header is found, else the value specified
	 */
	public final String getUserAgentHeader()
	{
		return request.getHeader("user-agent");
	}
    
	/**
	 * A non-null-pointer version that most should use unless they want to actually know if there's 
	 * no user-agent header set and want a null pointer provided.
	 * @see #getUserAgentHeader() to get exactly what request.getHeader("user-agent") returns
	 * @return the String user-agent header if provided, else the string 'No-user-agent-header-provided'
	 */
    public final String getUserAgent()
    {
    	String agent = getUserAgentHeader();
    	return ( agent == null ) ? "No-user-agent-header-provided" : agent;
    }
    public final boolean isUserAgentIE()
    {
        return getUserAgent().indexOf("MSIE ") > 0;
    }
    public final boolean isUserAgentIE5()
    {
        return getUserAgent().indexOf("MSIE 5.") > 0;
    }
    public final boolean isUserAgentIE6()
    {
        return getUserAgent().indexOf("MSIE 6.") > 0;
    }
    public final boolean isUserAgentIE7()
    {
        return getUserAgent().indexOf("MSIE 7.") > 0;
    }
    public final boolean isUserAgentIE8()
    {
        return getUserAgent().indexOf("MSIE 8.") > 0;
    }
    public final boolean isUserAgentIE5_6_or_7() // 10/23/2013 - added to support detecting older IE that doesn't support data URIs for embedded images
    {
    	return isUserAgentIE7() || isUserAgentIE6() || isUserAgentIE5();
    }
    public final boolean isUserAgentIE5_6_7_or_8() // 10/21/2013 - added to support detecting older IE that doesn't support data URIs for embedded images
    {
    	return isUserAgentIE8() || isUserAgentIE7() || isUserAgentIE6() || isUserAgentIE5();
    }
    public final boolean isUserAgentGecko()
    {
        return getUserAgent().indexOf("Gecko") > 0;
    }
    
	/**
	 * Determines the the request was an HTTP GET or not.
	 * @return true if the request was a GET
	 */
	public final boolean isGet()
	{
		return request.getMethod().equals("GET");
	}
	
	/**
	 * Determines the the request was an HTTP POST or not.
	 * @return true if the request was a POST
	 */
	public final boolean isPost()
	{
		return request.getMethod().equals("POST");
	}
    
    public final String getContextPath()
    {
        return contextPath;
    }
	
	/**
	 * Will send a redirect to another URL as the response.  The URL will be encoded for redirect before being sent.
	 * @param toUrl the String URL to redirect to
	 * @throws java.io.IOException if there's an I/O error redirecting
	 */
	public final void sendRedirect(String toUrl)
		throws java.io.IOException
	{
		response.sendRedirect(response.encodeRedirectURL(toUrl));
	}
	
	public final boolean isApiMode() 
	{
		return isApiMode;
	}
	public final void setApiMode(boolean isApiMode)
	{
		this.isApiMode = isApiMode;
		if ( isApiMode )
			request.setAttribute(ESF_IS_API_MODE_ATTRIBUTE,"true");
		else
			request.removeAttribute(ESF_IS_API_MODE_ATTRIBUTE);
	}
	
	/**
	 * Gets an object from the request object by name.
	 * @param name the String name of the request attribute previously stored
	 * @return the Object associated with the name; null if no such object
	 */
	public final Object getRequestAttribute(String name)
	{
        if ( request == null )
            return null;
		return request.getAttribute(name);
	}
	
	/**
	 * Gets a String object from the request object by name.
	 * @param name the String name of the request attribute previously stored
	 * @return the String object associated with the name; null if no such object or the object was not a String
	 */
	public final String getRequestStringAttribute(String name)
	{
		Object ov = request.getAttribute(name);
		if ( ov != null && ov instanceof String )
			return (String)ov;
		return null;
	}
	
	/**
	 * Sets an object in the request with a given name.
	 * @param name the String name of the object to store
	 * @param val the Object to be stored with that name
	 */
	public final void setRequestAttribute(String name,Object val)
	{
        if ( request != null )
            request.setAttribute(name,val);
	}
	
	/**
	 * Gets an object from the session object by name.
	 * @param name the String name of the session attribute previously stored
	 * @return the Object associated with the name; null if no such object
	 */
	public final Object getSessionAttribute(String name)
	{
		if ( session == null )
			return null;
        try
        {
            return session.getAttribute(name);
        }
        catch( IllegalStateException e )
        {
            return null;
        }
	}
	
	/**
	 * Sets an object in the session with a given name.  Useful for storing data that should
	 * be remembered over a given login session, such as caching names, addresses, etc. that
	 * can then be auto-populated in other forms to avoid redundant data entry.
	 * @param name the String name of the object to store
	 * @param val the Object to be stored with that name
	 */
	public final void setSessionAttribute(String name,Object val)
	{
		if ( session != null )
        {
            try
            {
                session.setAttribute(name,val);
            }
            catch( IllegalStateException e ) {}
        }
	}
	
	/**
	 * Removes a given object from the session by name.
	 * @param name the String name of the session object to be removed
	 */
	public final void removeSessionAttribute(String name)
	{
		if ( session != null )
        {
            try
            {
                session.removeAttribute(name);
            }
            catch( IllegalStateException e ) {}
        }
	}
	
    /**
     * Ends the current session.
     */
    public final void endSession()
    {
        if ( session != null )
        {
            try
            {
                session.invalidate();
                session = null;
            }
            catch( IllegalStateException e ) {}
        }
    }
    
    public final void endSessionIfRequested()
    {
    	if ( doEndSession )
    	{
    		endSession();
    		doEndSession = false;
    	}
    }
    
	/**
	 * Gets a session attribute in which the object is assumed to be a String.
	 * It is often used to retrieve cached names, addresses, etc.  In particular,
	 * it's used when doing a getRequestAttribute() and using the session attribute
	 * as the default so that previously entered data (cached) can be the default.
	 * @param name the String name of the session attribute to retrieve
	 * @param def the String default value to return if that object doesn't exist
	 * @return the String object associated with the name
	 */
	public final String getSessionStringAttribute(String name, String def)
	{
		if ( session == null )
			return null;
		return ServletUtil.getSessionStringParam(session,name,def);
	}
	
	/**
	 * Gets a session attribute in which the object is assumed to be a String.
	 * @param name the String name of the session attribute to retrieve
	 * @return the String object associated with the name, or null if no such named object exists.
	 */
	public final String getSessionStringAttribute(String name)
	{
		if ( session == null )
			return null;
		return ServletUtil.getSessionStringParam(session,name,null);
	}
    
	
	public int getSessionTimeoutMinutes()
	{
		if ( session == null )
			return 0;
		return session.getMaxInactiveInterval() / 60; // convert seconds to minutes
	}
    
    /**
     * Gets an object from the application object by name.
     * @param name the String name of the application attribute previously stored
     * @return the Object associated with the name; null if no such object
     */
    public final Object getApplicationAttribute(String name)
    {
        if ( session == null )
            return null;
        try
        {
            return session.getServletContext().getAttribute(name);
        }
        catch( IllegalStateException e )
        {
            return null;
        }
    }
    
    /**
     * Sets an object in the application with a given name. 
     * @param name the String name of the object to store
     * @param val the Object to be stored with that name
     */
    public final void setApplicationAttribute(String name,Object val)
    {
        if ( session != null )
        {
            try
            {
                session.getServletContext().setAttribute(name,val);
            }
            catch( IllegalStateException e ) {}
        }
    }
    
    /**
     * Removes a given object from the application by name.
     * @param name the String name of the application object to be removed
     */
    public final void removeApplicationAttribute(String name)
    {
        if ( session != null )
        {
            try
            {
                session.getServletContext().removeAttribute(name);
            }
            catch( IllegalStateException e ) {}
        }
    }
    
    /**
     * Gets an application attribute in which the object is assumed to be a String.
     * It is often used to retrieve cached names, addresses, etc.  
     * @param name the String name of the application attribute to retrieve
     * @param def the String default value to return if that object doesn't exist
     * @return the String object associated with the name
     */
    public final String getApplicationStringAttribute(String name, String def)
    {
        Object o = getApplicationAttribute(name);
        if ( o == null )
            return def;
        if ( o instanceof String )
            return (String)o;
        return def;
    }
    
    /**
     * Gets an application attribute in which the object is assumed to be a String.
     * @param name the String name of the application attribute to retrieve
     * @return the String object associated with the name, or null if no such named object exists.
     */
    public final String getApplicationStringAttribute(String name)
    {
        Object o = getApplicationAttribute(name);
        if ( o == null )
            return null;
        if ( o instanceof String )
            return (String)o;
        return null;
    }
    
    
    /*
     * Determines if valid esignforms user and password using ESF password management, and logs the esignforms user in the session.
     * If the user was already logged in, this effectively replaces that with the new login attempt.
     * The 'errors' object is updated for invalid logins when the login is activated for the user
     * @return the User object of the logged in user when the email and password match for an active user who is now logged in.
     */
    public final User loginUser(String email, String password)
    {
    	if ( ! app.isSystemReady() )
    	{
    		app.warning("User email: " + email + " attempted to login before the system was ready. isApiMode: " + isApiMode);
    		errors.addError(app.getServerMessages().getString("password.warn.notbooted"));
    		return null;
    	}
    	
        app.getSessionTracker().updateEmail(email,session); // if not a logged in user, updates the email attempted for the session lists

        if ( isUserLoggedIn() )
        {
            String msg = "User "+loggedInUser.getEmail()+" forced logged out by new call to loginUser with email '" + email + 
            			 "' from IP address: " + getIP();
            loggedInUser.logLoginLogoff(msg);
            loggedInUser.logoff();
            loggedInUser = null;
        }
        
        User user = User.Manager.getByEmail(email);
        if ( user != null )
        {
            try
            {
            	UserLoginInfo loginInfo = app.getPasswordManager().login(user,password,getExternalUrlContextPath(),getIP());
                loggedInUser = user;
                loggedInUser.setLoggedInNow(loginInfo, getIP());
                app.getSessionTracker().setUser(user,session);
                
                if ( ! isApiMode )
                {
                    String msg = "User "+loggedInUser.getEmail()+" logged in from IP address "+getIP()+" with browser type: " + getUserAgent();
                    loggedInUser.logLoginLogoff(msg);
                    app.getActivityLog().logSystemUserActivity(msg);
                    
                    String userAgent = getUserAgent();
                    loggedInUser.setIPHONE(userAgent.contains("iPhone") || userAgent.contains("iPod"));
                    loggedInUser.setIPAD(userAgent.contains("iPad"));
                }
            }
            catch( Exception e )
            {
                String msg = "Invalid login for email: " + email + " from IP address: " + getIP() + " with browser type: " + getUserAgent() + "; exception: " + e.getMessage();
                user.logLoginLogoff(msg);
                app.getActivityLog().logSystemUserActivity(msg);
                errors.addError(app.getServerMessages().getString("password.error.invalidLogin.userNotification"));
                errors.addError(e.getMessage());
            }
        }
        else
        {
            String msg = "Invalid login for unknown email " + email + " from IP address " + getIP() + " with browser type: " + getUserAgent();
            app.getActivityLog().logSystemUserActivity(msg);
            errors.addError(app.getServerMessages().getString("password.error.invalidLogin"));
        }

        return loggedInUser;
    }
    
    public boolean checkLoggedIn()
        throws java.io.IOException
    {
        // Let's see if we have the user in the session like they should be...
        if ( ! isUserLoggedIn() )
        {
            sendRedirect(request.getContextPath()+"/"+app.getLoginPage()+"?"+URL_PARAM_TO_URL+"="+getMyEncodedUrl());
            return false;
        }
        return true;
    }
	
    public boolean isUserLoggedIn()
    {
        // Let's see if we have the user in the session like they should be...
        loggedInUser = (User)getSessionAttribute(SessionTracker.USER_SESSION_ATTRIBUTE_NAME);
        return loggedInUser != null;
    }

    /**
     * Gets the currently logged in user
     * @return the User who is logged in, or null if not logged in
     */
    public final User getLoggedInUser()
    {
        return loggedInUser;
    }

    /*
     * Logs out the user if the user is logged in an the auth code matches.
     * The user session ends as well.
     * @return the true if the user is logged off, false if not logged in to begin with or the code doesn't match.
     */
    public final boolean logoffUser()
    {
        if ( ! isUserLoggedIn() )
        {
        	app.warning("Request to logoff user who is not logged in; IP address: " + getIP() + "; isApiMode: " + isApiMode);
        	errors.addError(app.getServerMessages().getString("security.error.notLoggedIn"));
        	return false;
        }
        
        if ( ! isApiMode )
        {
            String msg = "User " + loggedInUser.getFullDisplayName() + " logoff from IP: " + getIP();
        	loggedInUser.logLoginLogoff(msg);
        	app.getActivityLog().logSystemUserActivity(msg);
        }
    	loggedInUser.logoff();
        endSession();
        
        return true;
    }  
	
    
	/**************************  Various utility routines wrappers for Version to make programming simpler ************************/

	/**
	 * Returns the HTML 4.0 doctype
	 * @return the String HTML 4.0 DOCTYPE
	 * @see #getDocTypeXhtml1()
	 */
	public final String getDocTypeHtml4()
	{
		return 	HtmlUtil.getDocTypeHtml4();
	}

	/**
	 * Returns the XHTML 1.0 doctype.
	 * @return the String XHTML 1.0 DOCTYPE
	 */
	public final String getDocTypeXhtml1()
	{
		return 	HtmlUtil.getDocTypeXhtml1();
	}

	
	/**************************  Various utility routines wrappers for ServletUtil to make programming simpler ************************/

    /**
     * Determines if this is a regular POST or if it's a multi-part file upload POST.
     * @return true if this page uses the File Upload mechanism for sending data rather than a standard POST.
     */
    public boolean hasFileUploader()
    {
	    return fileUploader != null;
    }
    
    /**
	 * Retrieves a given parameter from a request or null if the parameter is not present.
     * @param name the String name of the GET/POST param.
     * @return the String value that goes with the param 'name'
	 */
	public final String getParam(String name)
	{
		return ( fileUploader == null ) ? ServletUtil.getParam(request,name,null) : fileUploader.getParam(name,null);
	}
	
	/**
	 * Retrieves a given parameter from a request, and if none found, returns the default.
	 */
	public final String getParam(String name,String def)
	{
		return ( fileUploader == null ) ? ServletUtil.getParam(request,name,def) : fileUploader.getParam(name,def);
	}
	
	/**
	 * Returns true if a checkbox with the given name was checked on submit.
	 * @param name the String name of the input field
	 * @return true if the field name was present (it was checked)
	 */
	public final boolean getCheckbox(String name)
	{
		return ( fileUploader == null ) ? ServletUtil.getCheckbox(request,name) : fileUploader.getCheckbox(name);
	}
	
	/**
	 * Retrieves a given 'int' parameter from a request, and if none found, returns the default.
	 */
	public final int getIntParam(String name,int def)
	{
		return ( fileUploader == null ) ? ServletUtil.getIntParam(request,name,def) : fileUploader.getIntParam(name,def);
	}
	
	/**
	 * Retrieves a given 'long' parameter from a request, and if none found, returns the default.
	 */
	public final long getLongParam(String name,long def)
	{
		return ( fileUploader == null ) ? ServletUtil.getLongParam(request,name,def) : fileUploader.getLongParam(name,def);
	}
	
    /**
     * Retrieves a string parameter and removes and scripts that may be inside.
     * @param name the String name of the parameter to retrieve
     * @return the String value associated with the parameter name with any scripts removed
     */
    public final String getScriptFreeParam(String name,String def)
    {
        return ( fileUploader == null ) ? ServletUtil.getScriptFreeParam(request,name,def) : fileUploader.getParam(name,def);
    }
    
	/**
	 * Retrieves multiple values for a given parameter name, such as when using a checkbox
	 * across a list of items and you want to receive all checked items.
	 * @param name the String name of the parameter to retrieve
	 * @return the String array of values associated with the parameter name
	 */
	public final String[] getMultiParam(String name)
	{
		return ( fileUploader == null ) ? ServletUtil.getMultiParam(request,name) : fileUploader.getMultiParam(name);
	}
	
	/**
	 * Returns the local URL of the current request.  This should be
	 * used to get the URL that was used to retrieve the page, but isn't needed
	 * for a user session.
	 * @return the String URL of the current request.
	 */
	public final String getMyUrl()
	{
		return ServletUtil.getLocalRequestUrl(request);
	}
	
	public final String getMyExternalUrl()
	{
		return ServletUtil.getExternalRequestUrl(request);
	}
	
	/**
	 * Returns the local URL of the current request, encoded with any session cookie info.
	 * This is basically a combo of getMyUrl() and encodeSession()
	 * @return the String URL of the current request, encoded with any session info.
	 */
	public final String getMySessionUrl()
	{
		return encodeSession(getMyUrl());
	}
	
	/**
	 * Like getMySessionUrl(), but it's also URL encoded so that it can be safely passed as a single parameter in another URL.
	 * Mostly used for embedding the current page's URL as a parameter for another page (like 'toURL').
	 * @return String URL encoded for use in other URL
	 */
	public final String getMyEncodedUrl()
	{
		return encode(getMyUrl());
	}
	
	/**
	 * Gets the current session-encoded URL from the request and encodes it so it can
	 * be embedded in other URLs without being interpretted, such as when passing one URL
	 * in another URL.
	 * @return the current session request URL, encoded.
	 */
	public final String getMyEncodedSessionUrl()
	{
		return encode(getMySessionUrl());
	}
	
	/**
	 * Used to return the complete external name that gets to the "context" for the web application.
	 * @return the String "base context path" of the requested page, such as "https://ssd2.yozons.com/ssd"
	 * on the hosted ASP, but could be more complicated like: "https://esign.vanpool.com:4432/ssd"
	 * Mostly used to create the BASEHREF since a signed HTML page that uses relative path names needs to keep
	 * the full complete path to retrieve those objects later (when not viewed while actually on the web site
	 * and the external URL is assumed).
	 */
	public final String getExternalUrlContextPath()
	{
		return ServletUtil.getExternalUrlContextPath(request);
	}
	public final String getExternalUrlWithoutContextPath()
	{
		return ServletUtil.getExternalUrlWithoutContextPath(request);
	}
    
	/**
	 * Encodes a given URL to include any session ids required to keep the user
	 * within an active session with the web application.
	 * @param url the URL to ensure has session info
	 * @return the encoded URL when needed (i.e. the session is not using cookies)
	 */
	public final String encodeSession(String url)
	{
		return response.encodeURL(url);
	}
	
	/**
	 * Encodes the given string to be used in an URL parameter.
	 * 
	 * @param s the data to be encoded
	 * @return the URL encoded value
	 */
	public final String encode(String s)
	{
		return ServletUtil.urlEncode(s);
	}
	public final String decode(String s)
	{
		return ServletUtil.urlDecode(s);
	}
    
 	/**************************  Various utility routines wrappers for HtmlEscaper to make programming simpler ***********************/
	/**
	 * Converts the given string into HTML safe codes, mapping double spaces to "&nbsp; ", newlines to &lt;br&gt;, 
	 * '&' to &amp;amp; etc.
	 * @param s the String text to convert to HTML-safe markup.
	 * @return the HTML-safe version of 's'
	 */
	public final String toHtml(String s)
	{
		return HtmlUtil.toDisplayHtml(s);
	}
	
	/**
	 * Like toHtml() except that if the parameter is empty, an "&nbsp;" is emitted so that there's always something output.
	 * @param s the String text to convert to HTML-safe markup.
	 * @return the HTML-safe version of 's', and if 's' is an empty string, then "&nbsp;"
	 */
	public final String toHtmlNbsp(String s)
	{
		if ( isBlank(s) )
			return "&nbsp;";
		return HtmlUtil.toDisplayHtml(s);
	}
	
	/**
	 * Like toHtml() above, but doesn't do the newlines and double space conversions, just the entity mappings for
	 * quotes, ampersands, less than, greater than, etc.  This is used mostly when displaying a string in a textarea.
	 */
	public final String toTextAreaHtml(String s)
	{
		return HtmlUtil.toEscapedHtml(s);
	}
	
	/**
	 * Like the above, but converts the string to be an XML safe string.
	 */
	public final String toXml(String s)
	{
		return XmlUtil.toEscapedXml(s);
	}
	
	/**************************  Various utility routines wrappers for Application to make programming simpler ***********************/
	
	/**
	 * Ensures that the given string is no longer thann maxLength characters.  If it is, it's truncated.  If not, the original string
	 * is returned.
	 * @param v the String to check
	 * @param maxLength the int maximum number of characters allowed in 'v'
	 * @return v if v is null or less than maxLength characters, or a new string that is the first maxLength characters of v.
	 */
	public final String ensureLength(String v, int maxLength)
	{
		if ( v == null || v.length() <= maxLength )
			return v;
		return v.substring(0,maxLength);
	}
	
	public final int getLength(String v)
	{
		return ( v == null ) ? 0 : v.length();
	}

    /**
     * Remove extra whitespace after a given whitespace character is found.  That is, only allow
     * one whitespace character to exist before a non-whitespace character appears, such as removing
     * double spaces within a string.  Therefore, a value like "Doe,   John" would become "Doe, John".
     * @param v the String to squeeze extra spaces out of
     * @return the String with only one whitespace character in a row.  Note that if the String doesn't need to be
     * changed, the original string 'v' is returned unchanged.
     */
    public final String compressWhitespace(String v)
    {
        return EsfString.compressWhitespace(v);
    }
    
    
	/**
	 * Returns true if the given string is blank (null, empty string, only containing whitespace).
	 */
	public final boolean isBlank(String s)
	{
		return EsfString.isBlank(s);
	}
	public final boolean isNonBlank(String s)
	{
		return EsfString.isNonBlank(s);
	}
	
	/**
	 * Returns true if the given string[] is blank (null, zero length, empty strings or only containing whitespace).
	 */
	public final boolean isBlank(String[] s)
	{
		return EsfString.isBlank(s);
	}
	public final boolean isNonBlank(String[] s)
	{
		return EsfString.isNonBlank(s);
	}
	
    /**
     * Checks if all String variables are blank
     * @return true if all of the variables passed in are null or blank
     */
    public final boolean areAllBlank(String... v)
    {
    	return EsfString.areAllBlank(v);
    }
    public final boolean areAllNonBlank(String... v)
    {
    	return EsfString.areAllNonBlank(v);
    }
    
    /**
     * Checks if any String variables are blank
     * @return true if any of the variables passed in are null or blank
     */
    public final boolean areAnyBlank(String... v)
    {
    	return EsfString.areAnyBlank(v);
    }
    public final boolean areAnyNonBlank(String... v)
    {
    	return EsfString.areAnyNonBlank(v);
    }
    
    //  All just renamings of existing routines to make coding cleaner, sometimes.
    /**
     * Checks if the field specified has no data.
     * @see #isBlank(String)
     * @return true if the field is blank
     */
    public final boolean hasNoData(String v)
    {
    	return EsfString.isBlank(v);
    }
    /**
     * Checks if the field specified has data.
     * @see #isBlank(String)
     * @return true if the field is not blank
     */
    public final boolean hasData(String v)
    {
    	return ! EsfString.isBlank(v);
    }
    /**
     * Checks if none of the fields specified have data.
     * @see #areAllBlank(String...)
     * @return true if all fields are blank
     */
    public final boolean noneHaveData(String...v)
    {
    	return EsfString.areAllBlank(v);
    }
    /**
     * Checks if all of the fields specified have data.
     * @see #areAllNonBlank(String...)
     * @return true if all fields are non-blank
     */
    public final boolean allHaveData(String...v)
    {
    	return EsfString.areAllNonBlank(v);
    }
    /**
     * Checks if some of the fields specified have no data.
     * @see #areAnyBlank(String...)
     * @return true if any of the fields are blank
     */
    public final boolean someHaveNoData(String...v)
    {
    	return EsfString.areAnyBlank(v);
    }
    /**
     * Checks if at least some of the fields specified have data.
     * @see #areAnyNonBlank(String...)
     * @return true if any of the fields are non-blank
     */
    public final boolean someHaveData(String...v)
    {
    	return EsfString.areAnyNonBlank(v);
    }
    /**
     * Checks if some, but not all, of the fields have data
     * @see Application#areSomeButNotAllBlank(String...)
     * @return true if any, but not all, of the fields are non-blank
     */
    public final boolean someButNotAllHaveData(String...v)
    {
    	return EsfString.areSomeButNotAllBlank(v);
    }

    
    /**
	 * Returns true if the email address provided is properly formatted. It not, the 'errors' is updated
	 * with an appropriate error message.
	 */
	public final boolean isValidEmail(String email)
	{
		return EsfEmailAddress.isValidEmail(email);
	}
	
	/**
	 * Returns true if the zip code is either 5 or 9 digits long.
	 */
	public final boolean isValidZip(String zip)
	{
		return app.isValidZip(zip);
	}
	
	/**
	 * Returns true if the phone number is at least 10 digits long.
	 */
	public final boolean isValidPhone(String phone)
	{
		return app.isValidPhone(phone);
	}

    /**
     * Splits a multi-line 'v' into separate lines (CR+LF or LF separated)
     * @param v the multi-line content to split (such as a textarea would provide, or property value)
     * @return a String array of lines found in 'v'
     */
    public final String[] splitLines(String v)
    {
    	return EsfString.splitLines(v);
    }

    
	/**
	 * Converts a String number into a short, and if there's a format error in the string, returns the default value.
	 */
	public final short stringToShort(String num,short def)
	{
		return app.stringToShort(num,def);
	}
	
	/**
	 * Converts a String number into an int, and if there's a format error in the string, returns the default value.
	 */
	public final int stringToInt(String num,int def)
	{
		return app.stringToInt(num,def);
	}
	
	/**
	 * Converts a String number into a long, and if there's a format error in the string, returns the default value instead.
	 */
	public final long stringToLong(String num,long def)
	{
		return app.stringToLong(num,def);
	}
	
    /**
     * Takes a credit card number and returns a string of the same length, but with the 
     * top numbers masked to 'X' and showing only the last 4 digits.
     * @param creditCardNumber the String credit card number to mask
     * @return the String masked credit card number
     */
    public String maskCreditCard(String creditCardNumber)
    {
        return app.maskCreditCard(creditCardNumber);
    }
    
    
    /**
	 * Converts a boolean into either a "Y" or "N" string.
	 */
	public final String boolToString( boolean b ) 
	{
		return new EsfBoolean(b).toYN();
	}

	/**
	 * Converts a String into a boolean.
	 * @param b the String to be tested
	 * @return true if the String b is any of the following regardless of case: Y YES TRUE T 1
	 */
	public final boolean stringToBool( String b ) 
	{
		return EsfBoolean.toBoolean(b);
	}

	/**
	 * Simply a routine with a name more meaningful to programs that is identical to stringToBool().
	 * @see #stringToBool
	 */
	public final boolean isYesTrue( String b ) 
	{
		return stringToBool(b);
	}

	/**
	 * If a phone is 7 digits, returns in like 822-4465.  If it's 10 digits, returns it like 425.822.4465.
	 * Otherwise, it just returns the phone number as it was provided.
	 */
	public final String makePrettyPhone(String phone)
	{
		return app.makePrettyPhone(phone);
	}
	
	/**
	 * Converts the long into a pretty number with comma separators, like 1,234,567
	 */
	public final String makePrettyNumber(long num)
	{
		return (new EsfInteger(num)).toString();
	}
	
	/**
	 * Returns only the numbers in a given string.
	 */
	public final String getOnlyNumeric(String s)
	{
		return EsfString.getOnlyNumeric(s);
	}
	
	/**
	 * Puts the SSN into a pretty 555-12-1234 format.
	 */
	public String makePrettySsn(String ssn)
	{
		return app.makePrettySsn(ssn);
	}

	public String makePrettyZip(String zipDigits)
	{
		return app.makePrettyZip(zipDigits);
	}
	
	/**************************  Various utility routines wrappers for Application to make programming simpler ***********************/

    public final String getPageTitlePrefix()
    {
        return app.getPageTitlePrefix();
    }
    
	/**
	 * Used to display a debug message in the yozons.log
	 */
	public final void debug(String msg)
	{
        getLogger().debug(ServletUtil.getLocalRequestUrl(request) + ": " + msg);
	}
	
	/**
	 * Used to display an informational message in the yozons.log
	 */
	public final void info(String msg)
	{
        getLogger().info(ServletUtil.getLocalRequestUrl(request) + ": " + msg);
	}
	
	/**
	 * Used to display a warning message in the yozons.log
	 */
	public final void warning(String msg)
	{
        getLogger().warn(ServletUtil.getLocalRequestUrl(request) + ": " + msg);
	}
	
	/**
	 * Used to display an error message in the yozons.log
	 */
	public final void err(String msg)
	{
        getLogger().error(ServletUtil.getLocalRequestUrl(request) + ": " + msg);
	}
	
    public final void sqlerr( java.sql.SQLException e, String msg )
    {
        getLogger().error("SQLException: " + msg + ":");
        while (e != null) 
        {
            getLogger().error("  Message:   " + e.getMessage(),e);
            getLogger().error("  SQLState:  " + e.getSQLState());
            getLogger().error("  ErrorCode: " + e.getErrorCode());
            e = e.getNextException();
        }
    }
    
	/**
	 * Used to display an error message in the yozons.log along with the exception text and stack trace.
	 */
	public final void except(Exception e,String msg)
	{
        getLogger().error(ServletUtil.getLocalRequestUrl(request) + ": " + msg,e);
	}
	
	public final void debugexcept(Exception e,String msg)
	{
		 getLogger().debug(ServletUtil.getLocalRequestUrl(request) + ": " + msg,e);
	}
	
    /**************************  Various utility routines for cookies to make programming simpler ************************/

    public String getCookie(String name)
    {
        javax.servlet.http.Cookie[] cookies = request.getCookies();
        if ( cookies != null )
        {
            for( javax.servlet.http.Cookie cookie : cookies )
            {
                if ( name.equals(cookie.getName()) )
                	return decode(cookie.getValue());
            }
        }
        
        return null;
    }
    
    public String getCookie(String name, String defaultValue)
    {
        String value = getCookie(name);
        return ( value == null ) ? defaultValue : value;
    }
    
    public void saveCookie(String name, String value)
    {
        javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie(name, encode(value));
        cookie.setMaxAge(60*60*24*90); // 90 days in seconds
       	cookie.setSecure(request.isSecure());
       	cookie.setHttpOnly(true);
        response.addCookie(cookie);
    }    

    public void saveSessionOnlyCookie(String name, String value)
    {
        javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie(name, encode(value));
        cookie.setMaxAge(-1); // session cookie
       	cookie.setSecure(request.isSecure());
       	cookie.setHttpOnly(true);
        response.addCookie(cookie);
    }    

    public void removeCookie(String name)
    {
        javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie(name, null);
        cookie.setMaxAge(0); // remove the cookie
       	cookie.setSecure(request.isSecure());
        response.addCookie(cookie);
    }
    
}