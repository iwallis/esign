// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen.taglib;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.util.HtmlUtil;

/**
 * This class is used to display a property, optionally from a specified propertyset.
 * 
 * @author Yozons, Inc.
 */
public class PropertyOut 
    extends javax.servlet.jsp.tagext.SimpleTagSupport
{
    private String name; // required
    private String propertySet; // required, but can have no value
    private String fieldSpec; // required 
    private boolean toHtml = true; // optional

	public void setName(String str)
	{
		name = str;
	}

	public void setPropertySet(String str)
	{
		propertySet = str;
	}

	public void setFieldSpec(String str)
	{
		fieldSpec = str;
	}

    public void setToHtml(String str)
    {
        toHtml = EsfBoolean.toBoolean(str);
    }
	public void setToHtml(boolean v)
	{
		toHtml = v;
	}

    public void doTag() 
        throws java.io.IOException
    {
    	JspWriter out = getJspContext().getOut();
    	
    	// The generated page should have initialized its DocumentPageBean
        DocumentPageBean docPage = (DocumentPageBean)getJspContext().getAttribute(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId,PageContext.REQUEST_SCOPE);
        if ( docPage == null )
        {
        	Application.getInstance().err("PropertyOut tag could not find DocumentPageBean with id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId + " in the request scope.");
            out.write(fieldSpec);
            return;
        }
        if ( docPage.context.hasNoTransaction() )
        {
        	Application.getInstance().err("PropertyOut tag called without a transaction context");
            out.write(fieldSpec);
            return;
        }
        
        EsfName propertySetName = EsfString.isBlank(propertySet) ? null : new EsfName(propertySet);
        String value = docPage.getExpandedPropertyValue( propertySetName, new EsfPathName(name) );
    	if ( value != null )
    		out.write( toHtml ? HtmlUtil.toDisplayHtml(value) : value );
    	else
    		out.write(fieldSpec);
    }
}
