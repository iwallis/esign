// Copyright (C) 2010-2011 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen;

/**
 * Utility class to hold the list of date selector objects on a JSP page.

 * @author Yozons, Inc.
 */
public class DateSelectorCalendar
{
    public String inputFieldId;
    public String buttonId;
    public String format;
    public String minYear = "1900";
    public String maxYear = "2200";
    
    public static final String LIST_ATTRIBUTE = "OpenEsfDateSelectorList";
    
    // Formats supported by the popup calendar
    public static final String FORMAT_MM_DD_YYYY  = "%m/%d/%Y";   // 01/31/2008
    public static final String TIP_MM_DD_YYYY     = "mm/dd/yyyy"; 

    public static final String FORMAT_MM_YYYY  = "%m/%Y";   // 01/2008 like with credit card expirations
    public static final String TIP_MM_YYYY     = "mm/yyyy (any day of the month is ok)"; 

    public static final String FORMAT_DD_MM_YYYY  = "%d/%m/%Y";   // 31/01/2008
    public static final String TIP_DD_MM_YYYY     = "dd/mm/yyyy"; 

    public static final String FORMAT_YYYY_MM_DD  = "%Y-%m-%d";   // 2008-01-31
    public static final String TIP_YYYY_MM_DD     = "yyyy-mm-dd"; 

    public static final String ISO_FORMAT_DD_MM_YYYY  = "%d.%m.%Y";   // 31.01.2008
    public static final String ISO_TIP_DD_MM_YYYY     = "dd.mm.yyyy"; 

    public static final String FORMAT_DD_Mon_YYYY = "%d-%b-%Y";   // 01-Dec-2008
    public static final String TIP_DD_Mon_YYYY    = "dd-Mon-yyyy"; 
    
    public static String[] SELECTION_FORMATS = { FORMAT_MM_DD_YYYY, FORMAT_DD_MM_YYYY, ISO_FORMAT_DD_MM_YYYY, FORMAT_DD_Mon_YYYY, FORMAT_YYYY_MM_DD, FORMAT_MM_YYYY };
    public static String[] SELECTION_FORMAT_CAPTIONS = { TIP_MM_DD_YYYY, TIP_DD_MM_YYYY, ISO_TIP_DD_MM_YYYY, TIP_DD_Mon_YYYY, TIP_YYYY_MM_DD, TIP_MM_YYYY };
    public static String getCaptionForFormat(String format)
    {
    	for( int i=0; i < SELECTION_FORMATS.length; ++i )
    	{
    		if ( SELECTION_FORMATS[i].equals(format) )
    			return SELECTION_FORMAT_CAPTIONS[i];
    	}
    	return "??" + format + "??";
    }
}