// Copyright (C) 2010-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.*;
import javax.servlet.jsp.JspContext;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.Version;
import com.esignforms.open.admin.SessionTracker;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfDecimal;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfHtml;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfMoney;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.exception.SendRedirectException;
import com.esignforms.open.integration.wkhtmltopdf.HtmlToPdf;
import com.esignforms.open.jsp.libdocsgen.taglib.FieldLabel;
import com.esignforms.open.prog.ButtonMessage;
import com.esignforms.open.prog.ButtonMessageVersion;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.File;
import com.esignforms.open.prog.FileVersion;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.LabelTemplate;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.prog.PartyTemplateFieldTemplate;
import com.esignforms.open.prog.PropertySet;
import com.esignforms.open.prog.PropertySetVersion;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionFile;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyAssignment;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.runtime.workflow.TransactionEngine;
import com.esignforms.open.servlet.EsfReportsAccessTransaction;
import com.esignforms.open.servlet.EsfReportsAccessTransaction.EsfReportsAccessContext;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.User;
import com.esignforms.open.util.CreditCardValidator;
import com.esignforms.open.util.HtmlUtil;
import com.esignforms.open.util.ServletUtil;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

/**
 * The DocumentPageBean base class is used to encapsulate the various classes and methods used
 * by generated document pages for Open ESF.  
 * 
 * @author Yozons, Inc.
 */
public class DocumentPageBean extends com.esignforms.open.jsp.PageBean implements EsfDocumentPageInterface
{
	// Buttons on the package page
	public static final String PACKAGE_CONTINUE_BUTTON_PREFIX = "PackageContinueButton_";
	public static final String PACKAGE_VIEW_COMPLETED_DOCUMENT_BUTTON_PREFIX = "PackageViewCompletedDocumentButton_";
	public static final String PACKAGE_EDIT_DOCUMENT_BUTTON_PREFIX = "PackageEditDocumentButton_";
	public static final String PACKAGE_DELETE_TRAN_BUTTON_PREFIX = "PackageDeleteTranButton_";
	public static final String PACKAGE_NOT_COMPLETING_BUTTON_PREFIX = "PackageNotCompletingButton_";
	public static final String PACKAGE_DOWNLOAD_MY_DOCS_AS_PDF_BUTTON_PREFIX = "PackageDownloadMyDocsAsPdfButton_";

	// Buttons on a document page being worked on
	public static final String DOCUMENT_NEXT_PAGE_BUTTON_PREFIX = "DocumentNextPageButton_"; // only used on multipage docs
	public static final String DOCUMENT_PREVIOUS_PAGE_BUTTON_PREFIX = "DocumentPreviousPageButton_"; // only used on multipage docs
	public static final String DOCUMENT_REVIEW_BUTTON_PREFIX = "DocumentReviewButton_";
	public static final String DOCUMENT_SAVE_BUTTON_PREFIX = "DocumentSaveButton_";
	public static final String DOCUMENT_EDIT_BUTTON_PREFIX = "DocumentEditButton_";
	public static final String DOCUMENT_COMPLETE_BUTTON_PREFIX = "DocumentCompleteButton_";
	public static final String DOCUMENT_VIEW_PACKAGE_BUTTON_PREFIX = "DocumentViewPackageButton_"; // also on view only docs
	public static final String DOCUMENT_CONFIRM_LINK_BUTTON_PREFIX = "DocumentConfirmLinkButton_"; // we use PREFIX+fieldname for confirm click link "button"

	public static final String DOCUMENT_UPLOAD_BUTTON_PREFIX = "DocumentUploadButton_"; // we use PREFIX+fieldname+DOCID for file upload buttons
	public static final String DOCUMENT_DELETE_UPLOAD_BUTTON_PREFIX = "DocumentDeleteUploadButton_"; // we use PREFIX+fieldname+DOCID for delete file upload buttons
	public static final String DOCUMENT_DOWNLOAD_BUTTON_PREFIX = "DocumentDownloadButton_"; // we use PREFIX+fieldname+DOCID for download file link "button"
	
	// Buttons on a document page being viewed only
	public static final String DOCUMENT_NEXT_DOCUMENT_BUTTON_PREFIX = "DocumentNextDocumentButton_";
	public static final String DOCUMENT_PREVIOUS_DOCUMENT_BUTTON_PREFIX = "DocumentPreviousDocumentButton_";
	
	// Hidden field on a document page used for auto-posting forms to set which field triggered the post
	public static final String HIDDEN_AUTO_POST_FIELD_ID = "ESF_AUTO_POST_FIELD_ID";

	// This param is generally only provided when we are picking up an API transaction to get it to review mode.
	public static final String ESFAPI_PAGE_REVIEW_SESSION_PICKUPCODE_KEY_PARAM_NAME = "esfapi_page_review_session_pickupcode_key";

	public TransactionContext context; // Our transaction context
	public StringBuilder capturedHtml; // This is the HTML page as captured on rendering
	public String documentAndPageNumbersInfo; // the document/page number information
	
	public String jumpToHashtag; // When set, our page will attempt to jump to this hashtag value

	// Use getButtonMessageVersion() rather than access this directly
	private ButtonMessageVersion buttonMessageVersion; 

	/**
	 * Constructs the DocumentPageBean instance.
	 */
	public DocumentPageBean()
	{
		super();
	}

	
	// ******************  Implements the interface: EsfDocumentPageInterface which we publish as the recommended interface to
	// ******************  custom Java programming in a document.
	
	public final boolean isEditMode()
	{
		return isPageEdit();
	}
	
	public final boolean isReviewMode()
	{
		return isPageReview();
	}
	
	public final boolean isViewMode()
	{
		return isPageViewOnly();
	}
	
	public final boolean isViewOnlyParty()
	{
		return context.hasTransaction() && context.isViewOnlyParty();
	}
	
	public final boolean isViewOnlyNonParty()
	{
		return context.hasTransaction() && context.isViewOnlyNonParty();
	}
	
	public final boolean isParty(String docPartyName)
	{
		return isParty( new EsfName(docPartyName) );
	}
	
	public final boolean isPartyCompleted()
	{
		return context.hasTransaction() && context.hasTransactionParty() && context.transactionParty.isCompleted();
	}
	
	public final boolean isPartyLoggedIn()
	{
		return context.hasTransaction() && context.hasUser() && isUserLoggedIn();
	}
	
	public final boolean isPackageParty(String pkgPartyName)
	{
		if ( context.hasNoTransaction() || EsfString.isBlank(pkgPartyName) )
			return false;
		return context.isPackageParty( new EsfName(pkgPartyName) );
	}
	
	public final boolean isTransactionCompleted()
	{
		return context.hasTransaction() && context.transaction.isCompleted();
	}
	
	public final boolean isTransactionDeleted()
	{
		return context.hasNoTransaction() || ( context.hasTransaction() && context.transaction.isDeleted() );
	}
	
	public final String getFieldSpecValue(String fieldSpec)
	{
		return getFieldSpecWithSubstitutions(fieldSpec);
	}
	public final String getFieldSpecValueOrBlankIfNotFound(String fieldSpec)
	{
		String v = getFieldSpecWithSubstitutions(fieldSpec);
		return v.equals(fieldSpec) ? "" : v;
	}
	
	public final boolean isFieldSpecBlank(String fieldSpec)
	{
		return isBlank(getFieldSpecValueOrBlankIfNotFound(fieldSpec));
	}
	public final boolean isFieldSpecNonBlank(String fieldSpec)
	{
		return isNonBlank(getFieldSpecValueOrBlankIfNotFound(fieldSpec));
	}
	
	// public boolean isBlank(String v) is defined already in PageBean
	// public boolean isNonBlank(String v) is defined already in PageBean
	
	// public final int stringToInt(String num,int def) is already defined in PageBean
	public final int stringToInt(String v) 
	{
		return stringToInt(v,0);
	}

	// public final boolean stringToBool( String b ) is already defined in PageBean
	
	// ******************  END of Implements the interface: EsfDocumentPageInterface
	

	public boolean hasJumpToHashtag() { return EsfString.isNonBlank(jumpToHashtag); }
	public String getJumpToHashtag() { return jumpToHashtag; }
	public void setJumpToHashtag(String v) { jumpToHashtag = v; }
	public String getDocumentAndPageNumbersInfo() { return documentAndPageNumbersInfo; }
	
	
	/**
	 * Processes a request for a transaction context that we've already determined.
	 * @return true if the request was processed fine and the page should continue to render; false if the page should not render or we're being redirected away.
	 * @throws java.io.IOException
	 */
	public PageProcessing processRequest()
		throws java.io.IOException
	{
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        TransactionEngine engine = new TransactionEngine(context);

        try
		{
            // If the party isn't active, let's put in view only state
			if ( context.transactionParty.isActive() )
			{
				// If the party is active, but all documents have been completed, something has gone "wrong" and so we'll complete the party now
				if ( ! context.hasAnyDocumentToDo() && ! context.transaction.isSuspended() )
					completeCurrentParty(con,engine);
			}
			else if ( context.isEsfReportsAccessForUpdate() )
				setPageEdit();
			else
				setPageViewOnly();
			
			// Special notice for canceled/suspended transaction (canceled only available via reports access)
	        if ( context.transaction.isCanceled() )
	        	errors.addWarning(app.getServerMessages().getString("DocumentPageBean.warn.transactionIsCanceled"));
	        else if ( context.transaction.isSuspended() )
	        {
	        	if ( context.isEsfReportsAccess() )
	        		errors.addInfo(app.getServerMessages().getString("DocumentPageBean.warn.transactionIsSuspended.EsfReportsAccess"));
	        	else
	        		errors.addWarning(app.getServerMessages().getString("DocumentPageBean.warn.transactionIsSuspended"));
	        }

			// Let's see if we can figure out if a button action has been specified
			String buttonSuffix = context.currDocument.getId().toNormalizedEsfNameString();

			boolean requestedActionHandled = false;

            // We do special processing for the package document
            if ( context.isPackageDocument() )
            {
    			if ( getParam(PACKAGE_CONTINUE_BUTTON_PREFIX+buttonSuffix) != null )
    			{
    				processPackageButtonContinue(con,engine);
    				requestedActionHandled = true;
    			}
    			else if ( getParam(PACKAGE_DELETE_TRAN_BUTTON_PREFIX+buttonSuffix) != null )
    			{
    				processPackageButtonDeleteTran(con,engine);
    				requestedActionHandled = true;
    			}
    			else if ( getParam(PACKAGE_NOT_COMPLETING_BUTTON_PREFIX+buttonSuffix) != null )
    			{
    				processPackageButtonNotCompleting(con,engine);
    				requestedActionHandled = true;
    			}
    			else if ( getParam(PACKAGE_DOWNLOAD_MY_DOCS_AS_PDF_BUTTON_PREFIX+buttonSuffix) != null )
    			{
    				processPackageButtonDownloadMyDocsAsPdf(con,engine);
    				if ( ! errors.hasErrorOrWarning() )
    				{
    	            	engine.doWork(con,errors);
    	            	context.transaction.save(con,context.user);
    	            	con.commit();
        				return PageProcessing.DONE; // No more processing to do since we handled the PDF download already
    				}
    				else
    	            	context.transaction.logGeneral(con, "Package download as PDF doWork error: " + errors.toString());
    				requestedActionHandled = true;
    			}
    			else if ( isPost() ) // some other button pressed
    			{
    				// if we have multiple documents in the package for this party
    				if ( context.transactionPartyDocuments.size() > 1 || context.isEsfReportsAccessForView() )
    				{
        				// See if it's any of our view completed document buttons were pressed
        				for( TransactionPartyDocument tpd : context.transactionPartyDocuments )
        				{
        					if ( getParam(PACKAGE_VIEW_COMPLETED_DOCUMENT_BUTTON_PREFIX+buttonSuffix+"_"+tpd.getDocumentNumber()) != null ) // setup in TransactionOut tag
        					{
        						processPackageButtonViewDocument(con, engine, tpd.getDocumentNumber());
        						requestedActionHandled = true;
        						break;
        					}
        				}
    				}
    				if ( ! requestedActionHandled && context.isEsfReportsAccessForUpdate() )
    				{
        				// See if it's any of our edit document buttons were pressed
        				for( TransactionPartyDocument tpd : context.transactionPartyDocuments )
        				{
        					if ( getParam(PACKAGE_EDIT_DOCUMENT_BUTTON_PREFIX+buttonSuffix+"_"+tpd.getDocumentNumber()) != null ) // setup in TransactionOut tag
        					{
        						processPackageButtonEditDocument(con, engine, tpd.getDocumentNumber());
        						requestedActionHandled = true;
        						break;
        					}
        				}
    				}
    			}
    			else if ( isGet() )
    			{
    				requestedActionHandled = true;
    			}
            }
            else // not the package document
            {
            	// If this document doesn't need any work, ensure is in view only mode
    			if ( ! context.currTransactionPartyDocument.isWorkNeeded() && ! context.isEsfReportsAccessForUpdate() )
    				setPageViewOnly();
    			else if ( context.isEsfReportsAccessForView() || (! context.isEsfReportsAccessForUpdate() && context.transaction.isSuspended()) )
    				setPageViewOnly();
    			else
    			{
    		        // See if we're in API mode and doing the page review mode processing compared to a normal processing, we just go to review and no more
    		        String apiPageReviewPickupCodeKey = getParam(ESFAPI_PAGE_REVIEW_SESSION_PICKUPCODE_KEY_PARAM_NAME);
    		        if ( EsfString.isNonBlank(apiPageReviewPickupCodeKey) )
    		        {
    		        	String verifyPickupCode = getSessionStringAttribute(apiPageReviewPickupCodeKey);
    		        	if ( context.pickupCode.equals(verifyPickupCode) )
    		        	{
    		        		setPageReview();
    		        		return PageProcessing.CONTINUE;
    		        	}
    		        }
    			}

    			// If the is the first time pickup for the party, we'll fire some events to let them know this happened as well as update our status to show we've retrieved it now
    			if ( context.currTransactionPartyDocument.isNetYetRetrieved() && ! context.transaction.isSuspended() )
    			{
    				engine.queuePartyRetrievedDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);
    				engine.doWork(con,errors); // do this now since it may change document data or the like
    				if ( errors.hasErrorOrWarning() )
    	            	context.transaction.logGeneral(con, "Party retrieved document doWork error: " + errors.toString());
    			}
    			
    			if ( context.currTransactionPartyDocument.isViewOptional() && ! context.transaction.isSuspended() )
    			{
    				engine.queueViewOptionalPartyViewedDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);
    				engine.doWork(con,errors); // do this now since it may change document data or the like
    				if ( errors.hasErrorOrWarning() )
    	            	context.transaction.logGeneral(con, "View Optional Party viewed document doWork error: " + errors.toString());
    			}

    			if ( getParam(DOCUMENT_REVIEW_BUTTON_PREFIX+buttonSuffix) != null )
    			{
    				processButtonReview(con,engine);
    				requestedActionHandled = true;
    			}
    			else if ( getParam(DOCUMENT_SAVE_BUTTON_PREFIX+buttonSuffix) != null )
    			{
    				processButtonSave(con,engine);
    				requestedActionHandled = true;
    			}
    			else if ( getParam(DOCUMENT_NEXT_PAGE_BUTTON_PREFIX+buttonSuffix) != null )
    			{
    				processButtonNextPage(con,engine);
    				requestedActionHandled = true;
    			} 
    			else if ( getParam(DOCUMENT_PREVIOUS_PAGE_BUTTON_PREFIX+buttonSuffix) != null )
    			{
    				processButtonPreviousPage(con,engine);
    				requestedActionHandled = true;
    			} 
    			else if ( getParam(DOCUMENT_COMPLETE_BUTTON_PREFIX+buttonSuffix) != null )
    			{
    				processButtonComplete(con,engine);
    				requestedActionHandled = true;
    			} 
    			else if ( getParam(DOCUMENT_EDIT_BUTTON_PREFIX+buttonSuffix) != null )
    			{
    				processButtonEdit(con,engine);
    				requestedActionHandled = true;
    			}
    			else if ( getParam(DOCUMENT_VIEW_PACKAGE_BUTTON_PREFIX+buttonSuffix) != null )
    			{
    				processButtonViewPackage(con,engine);
    				requestedActionHandled = true;
    			}
    			else if ( getParam(DOCUMENT_NEXT_DOCUMENT_BUTTON_PREFIX+buttonSuffix) != null )
    			{
    				processButtonNextDocument(con,engine);
    				requestedActionHandled = true;
    			}
    			else if ( getParam(DOCUMENT_PREVIOUS_DOCUMENT_BUTTON_PREFIX+buttonSuffix) != null )
    			{
    				processButtonPreviousDocument(con,engine);
    				requestedActionHandled = true;
    			}
    			else if ( isPost() && EsfString.isNonBlank(getParam(HIDDEN_AUTO_POST_FIELD_ID)) ) // this field is always present, but is only non-blank when it has a value set
    			{
    				processAutoPost(con,engine,getParam(HIDDEN_AUTO_POST_FIELD_ID));
    				requestedActionHandled = true;
    			}
    			else // not our regular buttons, so let's see if it's one of our special buttons
    			{
    				if ( isPageEdit() ) // Only check for uploads if in page edit mode
    				{
            			List<FieldTemplate> uploadFileFieldTemplates = context.getPartyUploadFileFieldTemplates();
            			if ( uploadFileFieldTemplates != null )
            			{
            				for( FieldTemplate fieldTemplate : uploadFileFieldTemplates )
            				{
            					// Check for an upload
            					String buttonName = DocumentPageBean.DOCUMENT_UPLOAD_BUTTON_PREFIX + fieldTemplate.getEsfName().toLowerCase() + buttonSuffix;
            					if ( getParam(buttonName) != null )
            					{
            						if ( ! requestedActionHandled && isPageEdit() )
            							loadFieldsPartyCanEdit(); // keep any data they may have entered elsewhere
            	    				processButtonFileUpload(con,engine,fieldTemplate);
            	    				requestedActionHandled = true;
            	    				break; // can only be one of these buttons
            					}
            					else
            					{
            						// Check for a remove upload
            						List<TransactionFile> fileList = context.transaction.getAllTransactionFilesForDocumentField(context.currTransactionDocument.getId(), fieldTemplate.getEsfName());
            						for( TransactionFile tranFile : fileList )
            						{
                						buttonName = DocumentPageBean.DOCUMENT_DELETE_UPLOAD_BUTTON_PREFIX + fieldTemplate.getEsfName().toLowerCase() + tranFile.getId().toNormalizedEsfNameString();
                						if ( getParam(buttonName) != null )
                						{
                							context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Delete File Upload button; IP: " + getIP());

                				            if ( ! hasJumpToHashtag() )
                				            	setJumpToHashtag(fieldTemplate.getId().toNormalizedEsfNameString());
                    						if ( ! requestedActionHandled && isPageEdit() )
                    							loadFieldsPartyCanEdit(); // keep any data they may have entered elsewhere
                							if ( ! context.transaction.removeTransactionFile(tranFile) )
                								errors.addWarning(app.getServerMessages().getString("DocumentPageBean.warn.deleteUploadedFile.notFound",tranFile.getFileName()), fieldTemplate.getEsfName().toLowerCase());
                							else
                							{
                								EsfPathName fileAccessPathName = createPartyFileLastAccessTimestampFieldPathName(tranFile.getId());
                								context.currTransactionDocument.getRecord().remove( fileAccessPathName ); // remove
                				                context.transaction.logActionBasic(con, "Party " + context.pickupPartyName + " removed uploaded file " + tranFile.getFileName() + " from document " + context.currDocument.getEsfName());
                				                
                				    			setFieldValue(fieldTemplate.getEsfName(), new EsfInteger(fileList.size()-1), fieldTemplate.isCloneAllowed());
                							}
                							requestedActionHandled = true;
                							break;
                						}
            						}
            					}
            				} // end particular file field 
            			} // end has file fields to edit/upload/remove 
    				} // end check if page edit for uploads
        			
        			// If it wasn't one of those buttons, let's check if they clicked on any File field links
        			if ( ! requestedActionHandled )
        			{
            			List<FieldTemplate> fileFieldTemplates = context.getPartyFileFieldTemplates();
            			if ( fileFieldTemplates != null )
            			{
            				for( FieldTemplate fieldTemplate : fileFieldTemplates )
            				{
            					EsfValue blockViewDownload = context.transaction.getFieldValue(context.currDocument.getEsfName(), new EsfPathName(fieldTemplate.getEsfName(),context.pkgVersionPartyTemplate.getId().toEsfName(),FieldTemplate.VIEW_DOWNLOAD_PROHIBITED_PATH_SUFFIX));
            					if ( blockViewDownload != null )
            					{
    				                context.transaction.logActionBasic(con, "Party " + context.pickupPartyName + " attempted to view/downloaded files for field " + fieldTemplate.getEsfName() + " from document " + context.currDocument.getEsfName() + ", but such access has been blocked. IP: " + getIP() + "; browser: " + getUserAgent());
            					}
            					else
            					{
                					List<TransactionFile> fileList = context.transaction.getAllTransactionFilesForDocumentField(context.currTransactionDocument.getId(), fieldTemplate.getEsfName());
            						for( TransactionFile tranFile : fileList )
            						{
                						String buttonName = DocumentPageBean.DOCUMENT_DOWNLOAD_BUTTON_PREFIX + fieldTemplate.getEsfName().toLowerCase() + tranFile.getId().toNormalizedEsfNameString();
                						if ( getParam(buttonName) != null )
                						{
                							context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked File Downlaod button; IP: " + getIP());

                							requestedActionHandled = true;
                							
                							byte[] fileData = tranFile.getFileDataFromDatabase();
                							if ( fileData == null )
                								errors.addError(app.getServerMessages().getString("DocumentPageBean.warn.downloadUploadedFile.notFound",tranFile.getFileName()), fieldTemplate.getEsfName().toLowerCase());
                							else
                							{
                								// Record in our variable the date/time it was downloaded by this party
                								EsfPathName fileAccessPathName = createPartyFileLastAccessTimestampFieldPathName(tranFile.getId());
                								setFieldValue(fileAccessPathName, new EsfDateTime(), false);
                				                context.transaction.logActionBasic(con, "Party " + context.pickupPartyName + " downloaded file " + tranFile.getFileName() + " from document " + context.currDocument.getEsfName() + ". IP: " + getIP() + "; browser: " + getUserAgent());
                								
                    			            	engine.doWork(con,errors);
                    		    				if ( errors.hasErrorOrWarning() )
                    		    	            	context.transaction.logGeneral(con, "Party download file doWork error: " + errors.toString());
                    			            	context.transaction.save(con,context.user);
                    			            	con.commit();
                    			            	
                    			                if ( tranFile.isContentTypeBrowserSafe() || tranFile.isContentTypePDF() )
                    			                {
                        			                response.setContentType(tranFile.getFileMimeType());
                        			            	String disposition = "inline; filename=\""+ tranFile.getFileName() + "\"";
                        			                response.setHeader("Content-Disposition", disposition );
                    			                }
                    			                else
                    			                {
                        			                response.setContentType(Application.CONTENT_TYPE_BINARY);
                        			            	String disposition = "attachment; filename=\""+ tranFile.getFileName() + "\"";
                        			                response.setHeader("Content-Disposition", disposition );
                    			                }
                    			                response.setContentLength(fileData.length);
                    			                ServletOutputStream sos = response.getOutputStream();
                    			                sos.write(fileData);
                    			                sos.close();
                    			            	return PageProcessing.DONE; // there's no page to show since we handled the download instead
                							}
                						}
            						} // end particular file stored in the field
            					} // end blocked access or not
            				} // end particular file field 
            			} // end has file fields that could be downloaded
        			} // end check if request was handled yet
        			
        			// Next let's check if they clicked on any confirm click links
        			if ( ! requestedActionHandled )
        			{
            			List<FieldTemplate> fileConfirmClickFieldTemplates = context.getPartyFileConfirmClickFieldTemplates();
            			if ( fileConfirmClickFieldTemplates != null )
            			{
            				for( FieldTemplate fieldTemplate : fileConfirmClickFieldTemplates )
            				{
            					String buttonName = DocumentPageBean.DOCUMENT_CONFIRM_LINK_BUTTON_PREFIX + fieldTemplate.getEsfName().toLowerCase() + buttonSuffix;
            					if ( getParam(buttonName) != null )
            					{
            						context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Confirm Link button; IP: " + getIP());

            						requestedActionHandled = true;
        							
            			    		// If the value is set to an EsfName, we use that name for the file. Otherwise we'll default to how it was originally configured.
            						EsfValue fieldValue = getFieldValue(fieldTemplate.getEsfName());
            						EsfName fileName = (fieldValue instanceof EsfName) ? (EsfName)fieldValue : new EsfName(fieldTemplate.getInputFormatSpec());
            						FileVersion fileVersion = context.transaction.getFileVersion(fileName, context.currDocumentVersion);
            						if ( fileVersion == null )
        				                context.transaction.logActionBasic(con, "ERROR: Party " + context.pickupPartyName + " tried file confirm clicked on field " + fieldTemplate.getEsfName() + " in document " + context.currDocument.getEsfName() + ", but did not find a File named " + fileName + ". IP: " + getIP() + "; browser: " + getUserAgent());
            						else
            						{
	    								// Record in our variable the date/time it was clicked by this party
	    								EsfPathName fileConfirmClickPathName = createPartyFileConfirmClickLastAccessTimestampFieldPathName(fieldTemplate.getEsfName());
	    								setFieldValue(fileConfirmClickPathName, new EsfDateTime(), false);
	    				                context.transaction.logActionBasic(con, "Party " + context.pickupPartyName + " confirm clicked file link " + fieldTemplate.getEsfName() + " from document " + context.currDocument.getEsfName() + ". IP: " + getIP() + "; browser: " + getUserAgent());
	    			            		sendRedirect(app.getContextPath()+"/files/"+fileName.toHtml()+"/"+encode(fileVersion.getFileFileName())+"?fvid="+fileVersion.getId().toHtml());
	    			            		return PageProcessing.DONE;
            						}
    							}
            				} // end particular file confirm click field 
            			} // end has file confirm click link fields
        			} // end check if request was handled yet
        			
        			// If we've still not done anything (no buttons pressed, and this is a GET request, let's put out some messages about this document).
                	if ( ! requestedActionHandled && isGet() )
                	{
            			if ( (! context.currTransactionPartyDocument.isWorkNeeded() && ! context.isEsfReportsAccessForUpdate()) || context.isEsfReportsAccessForView() )
            			{
            				setPageViewOnly();
            				if ( context.currTransactionPartyDocument.isCompleted() )
            				{
            					if ( ! context.transaction.isSuspended() )
            					{
                    				engine.queuePartyViewCompletedDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);
                					errors.addInfo(getButtonMessageVersion().getDocumentAlreadyCompletedMessage(context));
            					}
            				}
            				else if ( context.currTransactionPartyDocument.isRejected() )
            					errors.addWarning(app.getServerMessages().getString("DocumentPageBean.warn.document.alreadyRejected.cannotComplete"));
            				
            				// Non-party view only people on a multi-page document only get to see the review page
            				if ( context.isMultiPageViewOnlyNonParty() )
            					context.goToMultiPageReviewPage();
            			}
            			else if ( context.isViewOnlyParty() )
            			{
            				if ( context.isMultiPageDocument() && ! context.isMultiPageReviewPage() )
            				{
            					if ( context.isPartyViewOnlyOnAllPages() )
            						errors.addInfo(getButtonMessageVersion().getViewOnlyDocumentFYIMessage(context));
            					if ( context.hasNextEditPage() )
            						errors.addInfo(getButtonMessageVersion().getDocumentReviewViewOnlyButtonContinueNextPageMessage(context));
            					else
            						errors.addInfo(getButtonMessageVersion().getDocumentReviewViewOnlyButtonCompleteMessage(context));
            					errors.addInfo(getButtonMessageVersion().getDocumentButtonGoToPackageMessage(context));
            				}
            				else if ( ! context.transaction.isSuspended() )
            				{
	            				engine.queuePartyReviewDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);
	            				engine.doWork(con,errors); // do this now since it may change document data or the like
    		    				if ( errors.hasErrorOrWarning() )
    		    	            	context.transaction.logGeneral(con, "View-only party review document doWork error: " + errors.toString());
            					errors.addInfo(getButtonMessageVersion().getViewOnlyDocumentFYIMessage(context));
	            				errors.addInfo(getButtonMessageVersion().getDocumentReviewViewOnlyButtonCompleteMessage(context));
            					errors.addInfo(getButtonMessageVersion().getDocumentButtonGoToPackageMessage(context));
            				}
            				setPageReview();
            			}
            			else
            			{
            				if ( context.isMultiPageReviewPage() )
            				{
	            				errors.addInfo(getButtonMessageVersion().getReviewDocumentNotSignerMessage(context));
	            				errors.addInfo(getButtonMessageVersion().getDocumentReviewButtonCompleteNotSignerMessage(context));
        						errors.addInfo(getButtonMessageVersion().getDocumentReviewButtonReturnToEditMessage(context));
        						context.currTransactionPartyDocument.setEsignIpHostAddr(null);
        						context.currTransactionPartyDocument.setEsignTimestamp(null);
            					setPageReview();
            				}
            				else
            				{
            					if ( ! context.isEsfReportsAccess() ) // skip messages for document access via reports
            					{
                					errors.addInfo(getButtonMessageVersion().getCompleteDocumentEditsMessage(context));
                					if ( context.isMultiPageDocument() && context.hasNextEditPage() )
                						; // no need to prompt the user about the next page button
                					else
                						errors.addInfo(getButtonMessageVersion().getDocumentEditButtonContinueToReviewMessage(context));
                					errors.addInfo(getButtonMessageVersion().getDocumentButtonGoToPackageMessage(context));
            					}
                				setPageEdit();
            				}
            			}
            			
            			requestedActionHandled = true;
                	}
                	
    			} // end check for special button/link requests since no standard buttons pressed
    			
            } // end if regular document (not package)
            
            if ( requestedActionHandled )
            {
            	engine.doWork(con,errors);
				if ( errors.hasErrorOrWarning() )
	            	context.transaction.logGeneral(con, "Final process request doWork error: " + errors.toString());
            	if ( ! context.transaction.isDeleted() ) // in case we deleted it from the package page
            		context.transaction.save(con,context.user);
            	
            	con.commit();

            	if ( context.hasRedirectToUrl() )
            	{
            		String redirectUrl = context.getAndClearRedirectToUrl();
            		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' redirected to URL: " + redirectUrl + "; IP: " + getIP());
            		sendRedirect(redirectUrl);
            		return PageProcessing.DONE;
            	}
            }
            else
            {
            	con.rollback();
				errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.unexpectedRequest"));
				errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.unexpectedRequest.buttonInfo"));
				errors.addWarning(app.getServerMessages().getString("DocumentPageBean.error.document.unexpectedRequest.packageListShown"));
				context.currDocument = context.pkgDisclosureDocument;
				context.setupForCurrDocument();
            }
		}
        catch(SQLException e) 
        {
        	sqlerr(e,"processRequest()");
            pool.rollbackIgnoreException(con,e);
        }
		catch( SendRedirectException e )
		{
			if ( e.isError() )
			{
				err("processRequest() - Rolled back DB changes because of redirect exception to URL: " + e.getUrl());
				pool.rollbackIgnoreException(con); // Not yet sure why we'd redirect at all, so let's watch this and see how it plays out when we're running
			}
			else
			{
				try 
				{ 
					engine.doWork(con,errors); 
    				if ( errors.hasErrorOrWarning() )
    	            	context.transaction.logGeneral(con, "SendRedirect doWork error: " + errors.toString());
				} 
				catch( Exception e2 ) 
				{ 
					except(e2,"processRequest() - failed engine.doWork() when requested SendRedirectException to URL: " + e.getUrl());
				}
				try 
				{ 
					debug("processRequest() - Committing DB changes along with SendRedirectException to URL: " + e.getUrl());
					con.commit(); 
				} 
				catch( SQLException e2 ) 
				{ 
					except(e2,"processRequest() - failed DB commit before SendRedirectException to URL: " + e.getUrl());
				}
			}
			sendRedirect(e.getUrl());
			return PageProcessing.DONE;
		}
        catch(EsfException e) 
        {
        	except(e,"processRequest()");
            pool.rollbackIgnoreException(con);
        }
		finally
		{
        	Application.getInstance().cleanupPool(pool,con,null);
        	documentAndPageNumbersInfo = getDocumentAndPageNumbers();
		}

		return PageProcessing.CONTINUE;
	}
	
	public PageProcessing processRequest(HttpSession session, HttpServletRequest request, HttpServletResponse response)
		throws java.io.IOException, SendRedirectException
	{
		super.init(session, request, response);
		return processRequest();
	}
	
	// ******************  Package document routines  ***********************
	void processPackageButtonContinue(Connection con, TransactionEngine engine)
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Continue button on the package; IP: " + getIP());
		
		// We want to go to the first document that still needs to be worked, and if none can be found, we'll just go to the first document.
		if ( context.transactionParty.isActive() || context.isEsfReportsAccessForUpdate() )
		{
			context.goToFirstDocumentToDoOrFirstDoc();
			
			// This will only be null if there's bad configuration, such as having the package document be the same as a document inside the package
			if ( context.currTransactionPartyDocument == null )
			{
				errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.unexpectedRequest"));
				errors.addWarning(app.getServerMessages().getString("DocumentPageBean.error.document.unexpectedRequest.packageListShown"));
				context.transaction.logGeneral("Package CONTINUE button pressed, but no valid first document is defined. Check that package+disclosure is valid and not one of the package's documents.");
				context.goToPackageDocument();
				return;
			}
			
            // If the is the first time pickup for the party, we'll fire some events to let them know this happened as well as update our status to show we've retrieved it now
			if ( context.currTransactionPartyDocument.isNetYetRetrieved() && ! context.transaction.isSuspended() )
			{
				engine.queuePartyRetrievedDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);
				engine.doWork(con,errors); // do this now since it may change document data or the like
				if ( errors.hasErrorOrWarning() )
	            	context.transaction.logGeneral(con, "Package continue to party retrieved document doWork error: " + errors.toString());
			}
			
			if ( context.currTransactionPartyDocument.isViewOptional() && ! context.transaction.isSuspended() )
			{
				engine.queueViewOptionalPartyViewedDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);
				engine.doWork(con,errors); // do this now since it may change document data or the like
				if ( errors.hasErrorOrWarning() )
	            	context.transaction.logGeneral(con, "Package continue to view optional party viewed document doWork error: " + errors.toString());
			}
			
			if ( context.isViewOnlyParty() )
			{
				if ( context.isMultiPageDocument() && ! context.isMultiPageReviewPage() )
				{
					if ( context.isPartyViewOnlyOnAllPages() )
						errors.addInfo(getButtonMessageVersion().getViewOnlyDocumentFYIMessage(context));
					if ( context.hasNextEditPage() )
						errors.addInfo(getButtonMessageVersion().getDocumentReviewViewOnlyButtonContinueNextPageMessage(context));
					else
						errors.addInfo(getButtonMessageVersion().getDocumentReviewViewOnlyButtonCompleteMessage(context));

					errors.addInfo(getButtonMessageVersion().getDocumentButtonGoToPackageMessage(context));
				}
				else if ( ! context.transaction.isSuspended() )
				{
					engine.queuePartyReviewDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);
					engine.doWork(con,errors); // do this now since it may change document data or the like
    				if ( errors.hasErrorOrWarning() )
    	            	context.transaction.logGeneral(con, "Package continue View-only party review document doWork error: " + errors.toString());
					errors.addInfo(getButtonMessageVersion().getViewOnlyDocumentFYIMessage(context));
    				errors.addInfo(getButtonMessageVersion().getDocumentReviewViewOnlyButtonCompleteMessage(context));
					errors.addInfo(getButtonMessageVersion().getDocumentButtonGoToPackageMessage(context));
				}
				setPageReview();
			}
			else if ( ! context.transaction.isSuspended() )
			{
				errors.addInfo(getButtonMessageVersion().getCompleteDocumentEditsMessage(context));
				if ( context.isMultiPageDocument() && context.hasNextEditPage() )
					; // no need to prompt the user about the next page button
				else
					errors.addInfo(getButtonMessageVersion().getDocumentEditButtonContinueToReviewMessage(context));
				errors.addInfo(getButtonMessageVersion().getDocumentButtonGoToPackageMessage(context));
				setPageEdit();
			}
		}
		else
		{
			context.goToFirstDocument();
			
			// This will only be null if there's bad configuration, such as having the package document be the same as a document inside the package
			if ( context.currTransactionPartyDocument == null )
			{
				errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.unexpectedRequest"));
				errors.addWarning(app.getServerMessages().getString("DocumentPageBean.error.document.unexpectedRequest.packageListShown"));
				context.transaction.logGeneral("Package CONTINUE button pressed, but no valid first document is defined. Check that package+disclosure is valid and not one of the package's documents.");
				context.goToPackageDocument();
				return;
			}
			
			// Non-party view only people on a multi-page document only get to see the review page
			if ( context.isMultiPageViewOnlyNonParty() )
				context.goToMultiPageReviewPage();
			setPageViewOnly();
			if ( context.currTransactionPartyDocument.isCompleted() )
			{
				if ( ! context.transaction.isSuspended() )
					engine.queuePartyViewCompletedDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);
				errors.addInfo(getButtonMessageVersion().getDocumentAlreadyCompletedMessage(context));
			}
			else if ( context.currTransactionPartyDocument.isRejected() )
				errors.addWarning(app.getServerMessages().getString("DocumentPageBean.warn.document.alreadyRejected.cannotComplete"));
		}
	}
	
	
	void processPackageButtonDeleteTran(Connection con, TransactionEngine engine)
		throws java.io.IOException, EsfException, SQLException
	{
		if ( context.transaction.isSuspended() )
		{
			setPageViewOnly();
			return;
		}
		
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Delete Transaction button on the package; IP: " + getIP());

		// If active and no signatures applied, delete the tran after recording it.
		if ( ! context.transactionParty.isActive() )
		{
			setPageViewOnly();
			if ( context.currTransactionPartyDocument.isCompleted() )
				errors.addInfo(app.getServerMessages().getString("DocumentPageBean.info.package.alreadyCompleted.cannotDelete"));
			else if ( context.currTransactionPartyDocument.isRejected() )
				errors.addWarning(app.getServerMessages().getString("DocumentPageBean.info.package.alreadyRejected.cannotDelete"));
		}
		else if ( context.hasAnyDocumentBeenSigned() )
		{
			setPageViewOnly();
			errors.addInfo(app.getServerMessages().getString("DocumentPageBean.info.package.alreadySigned.cannotDelete"));
		}
		else
		{
			setPageViewOnly();
			
			if ( context.transaction.delete(con, errors, context.user) )
			{
				errors.addSuccess(getButtonMessageVersion().getDeletedTransactionMessage(context));
				// If we don't have a user, log it to the system log.
				if ( context.user == null )
					app.getActivityLog().logSystemConfigChange(con, "Party '" + context.pickupPartyName + "' deleted transaction id: " + context.transaction + "; tranType " + context.transaction.getTranType() + "; status; " + context.transaction.getStatus());
			}
		}
	}


	void processPackageButtonNotCompleting(Connection con, TransactionEngine engine)
		throws java.io.IOException, EsfException, SQLException
	{
		if ( context.transaction.isSuspended() )
		{
			setPageViewOnly();
			return;
		}
		
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Not-Completing button on the package; IP: " + getIP());

		// If active and has a not completing URL on this party, redirect to the requested URL
		if ( ! context.transactionParty.isActive() )
		{
			setPageViewOnly();
			if ( context.currTransactionPartyDocument.isCompleted() )
				errors.addInfo(app.getServerMessages().getString("DocumentPageBean.info.package.alreadyCompleted.cannotNotComplete"));
			else if ( context.currTransactionPartyDocument.isRejected() )
				errors.addWarning(app.getServerMessages().getString("DocumentPageBean.info.package.alreadyRejected.cannotNotComplete"));
		}
		else if ( ! context.pkgVersionPartyTemplate.hasNotCompletedUrlFieldExpression() )
		{
			setPageViewOnly();
			errors.addInfo(app.getServerMessages().getString("DocumentPageBean.error.document.unexpectedRequest"));
		}
		else
		{
			String url = getUrlEncodedFieldSpecWithSubstitutions(context.pkgVersionPartyTemplate.getNotCompletedUrlFieldExpression());
			context.transaction.logGeneral(con, "Redirecting package party '" + context.pkgVersionPartyTemplate.getEsfName() + "' to its defined NOT completed URL: " + url);
			context.setRedirectToUrl(url);
		}
	}

	void processPackageButtonDownloadMyDocsAsPdf(Connection con, TransactionEngine engine)
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Download My Documents as PDF button on the package; IP: " + getIP());

		// If not completed we shouldn't get this link pressed
		if ( ! context.transactionParty.isCompleted() || context.hasNoTransactionPartyDocuments() )
		{
			setPageViewOnly();
			errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.unexpectedRequest"));
		}
		else
		{
			String pdfFileName = context.transaction.getPackageDownloadFileName(context.user,context.currDocumentVersion);
			
			boolean useLandscape = false;
			List<String> htmlFileList = new LinkedList<String>();
			for( TransactionPartyDocument tpd : context.transactionPartyDocuments )
			{
				TransactionDocument tranDoc = context.transaction.getTransactionDocument(tpd.getTransactionDocumentId());
				
				TransactionPartyDocument latestTpd = context.transaction.getLatestTransactionPartyDocumentWithSnapshot(tranDoc);
				if ( latestTpd != null && ! latestTpd.isSnapshotDocumentSkipped() )
				{
					htmlFileList.add( latestTpd.getSnapshotDocument() );
					
					DocumentVersion docVer = DocumentVersion.Manager.getById(tranDoc.getDocumentVersionId());
					if ( docVer.isLandscape() )
						useLandscape = true;
				}
			}
			
			String[] htmlFileArray = new String[htmlFileList.size()];
			htmlFileList.toArray(htmlFileArray);
			
			HtmlToPdf htmlToPdf = new HtmlToPdf();
        	final byte[] data = useLandscape ? htmlToPdf.generateLandscapeSignedPdf(htmlFileArray) : htmlToPdf.generateSignedPdf(htmlFileArray);
        	if ( data == null )
        	{
    			setPageViewOnly();
    			errors.addError(app.getServerMessages().getString("DocumentPageBean.error.package.downloadMyDocsAsPdf.conversionError"));
    			context.transaction.logGeneral(con, "Package party '" + context.pkgVersionPartyTemplate.getEsfName() + "' failed to downloaded its documents as PDF.");
        	}
        	else
        	{
    			context.transaction.logGeneral(con, "Package party '" + context.pkgVersionPartyTemplate.getEsfName() + "' downloaded its documents as PDF.");
    			response.setContentType(Application.CONTENT_TYPE_PDF);
    			response.setContentLength(data.length);
                String disposition = "attachment;filename=\""+ pdfFileName + "\"";
                response.setHeader("Content-Disposition", disposition );
                ServletOutputStream sos = response.getOutputStream();
                sos.write(data);
                sos.close();
        	}
		}
	}
	
	void processPackageButtonViewDocument(Connection con, TransactionEngine engine, int documentNumber)
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked View Document #" + documentNumber + " button on the package; IP: " + getIP());

		context.goToDocument(documentNumber);
		setPageViewOnly();
		if ( context.currTransactionPartyDocument.isCompleted() )
			errors.addInfo(getButtonMessageVersion().getDocumentAlreadyCompletedMessage(context));
		else if ( context.currTransactionPartyDocument.isRejected() )
			errors.addWarning(app.getServerMessages().getString("DocumentPageBean.warn.document.alreadyRejected.cannotComplete"));
		// Non-party view only people on a multi-page document only get to see the review page
		if ( context.isMultiPageViewOnlyNonParty() || (context.isMultiPageDocument() && context.isEsfReportsAccessForView()) )
			context.goToMultiPageReviewPage();
	}

	void processPackageButtonEditDocument(Connection con, TransactionEngine engine, int documentNumber)
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Edit Document #" + documentNumber + " button on the package; IP: " + getIP());

		context.goToDocument(documentNumber);
		if ( ! context.isEsfReportsAccessForUpdate() && context.transaction.isSuspended() )
			setPageViewOnly();
		else
			setPageEdit();
		if ( context.currTransactionPartyDocument.isCompleted() )
			errors.addInfo(getButtonMessageVersion().getDocumentAlreadyCompletedMessage(context));
		else if ( context.currTransactionPartyDocument.isRejected() )
			errors.addWarning(app.getServerMessages().getString("DocumentPageBean.warn.document.alreadyRejected.cannotComplete"));
	}

	String getDocumentAndPageNumbers()
	{
		if ( context.transaction.isDeleted() )
			return app.getServerMessages().getString("DocumentPageBean.DocumentAndPageNumbers.tranDeleted.info");
		
		if ( context.isPackageDocument() )
			return app.getServerMessages().getString("DocumentPageBean.DocumentAndPageNumbers.package.info");

		String mode;
		if ( isPageEdit() )
			mode = app.getServerMessages().getString("DocumentPageBean.DocumentAndPageNumbers.document.info.mode.editing");
		else if ( isPageReview() )
			mode = app.getServerMessages().getString("DocumentPageBean.DocumentAndPageNumbers.document.info.mode.reviewing");
		else
			mode = app.getServerMessages().getString("DocumentPageBean.DocumentAndPageNumbers.document.info.mode.viewing");
		
		if ( context.isMultiPageDocument() && context.hasReviewOnlyPages && ! isPageViewOnly() ) // this type of document has an extra review only page if the user is not in view only mode
		{
			int adjustedNumPages = context.numPages + 1;
			int adjustedPageNumber = context.isMultiPageReviewPage() ? context.pageNumber + 1 : context.pageNumber;
			return app.getServerMessages().getString("DocumentPageBean.DocumentAndPageNumbers.document.info",
					mode,context.documentNumber,context.numDocuments,adjustedPageNumber,adjustedNumPages);
		}

		return app.getServerMessages().getString("DocumentPageBean.DocumentAndPageNumbers.document.info",
				mode,context.documentNumber,context.numDocuments,context.pageNumber,context.numPages);
	}

	// ******************  Document routines  ***********************
	void processButtonViewPackage(Connection con, TransactionEngine engine)
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked View Package button; IP: " + getIP());

		context.goToPackageDocument();
	}
	
	void processButtonNextDocument(Connection con, TransactionEngine engine)
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Next Document button; IP: " + getIP());

		if ( ! context.hasNextDocument() )
		{
			errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.unexpectedRequest.buttonInfo"));
			errors.addWarning(app.getServerMessages().getString("DocumentPageBean.error.document.unexpectedRequest.packageListShown"));
			context.goToPackageDocument();
		}
		else
		{
			context.goToNextDocument();
			setPageViewOnly();
			if ( context.currTransactionPartyDocument.isCompleted() )
				errors.addInfo(getButtonMessageVersion().getDocumentAlreadyCompletedMessage(context));
			else if ( context.currTransactionPartyDocument.isRejected() )
				errors.addWarning(app.getServerMessages().getString("DocumentPageBean.warn.document.alreadyRejected.cannotComplete"));
			// Non-party view only people on a multi-page document only get to see the review page
			if ( context.isMultiPageViewOnlyNonParty() )
				context.goToMultiPageReviewPage();
		}
	}

	void processButtonPreviousDocument(Connection con, TransactionEngine engine)
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Previous Document button; IP: " + getIP());

		if ( ! context.hasPreviousDocument() )
		{
			errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.unexpectedRequest.buttonInfo"));
			errors.addWarning(app.getServerMessages().getString("DocumentPageBean.error.document.unexpectedRequest.packageListShown"));
			context.goToPackageDocument();
		}
		else
		{
			context.goToPreviousDocument();
			setPageViewOnly();
			if ( context.currTransactionPartyDocument.isCompleted() )
				errors.addInfo(getButtonMessageVersion().getDocumentAlreadyCompletedMessage(context));
			else if ( context.currTransactionPartyDocument.isRejected() )
				errors.addWarning(app.getServerMessages().getString("DocumentPageBean.warn.document.alreadyRejected.cannotComplete"));
			// Non-party view only people on a multi-page document only get to see the review page
			if ( context.isMultiPageViewOnlyNonParty() )
				context.goToMultiPageReviewPage();
		}
	}

	// Return true if page is okay and should be rendered; false if redirected away
	void processButtonEdit(Connection con, TransactionEngine engine)
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Edit button; IP: " + getIP());

		if ( ! context.isEsfReportsAccessForUpdate() && context.transaction.isSuspended() )
			setPageViewOnly();
		else if ( (context.transactionParty.isActive() && context.currTransactionPartyDocument.isWorkNeeded()) || context.isEsfReportsAccessForUpdate() )
		{
			setPageEdit();
			if ( context.currTransactionPartyDocument.isEsigned() )
			{
				context.currTransactionPartyDocument.setEsignIpHostAddr(null); // remove any signature info we stored if returns to edit mode
				context.currTransactionPartyDocument.setEsignTimestamp(null);
			}
			if ( context.currDocumentVersion.isMultiPage() ) // on return from review mode for multi-page docs, we go back to the first page.
				context.goToPageNumber(0);
		}
		else if ( context.currTransactionPartyDocument.isCompleted() )
		{
			errors.addWarning(getButtonMessageVersion().getDocumentAlreadyCompletedCannotEditMessage(context));
			setPageViewOnly();
		}
		else if ( context.currTransactionPartyDocument.isRejected() )
		{
			errors.addWarning(app.getServerMessages().getString("DocumentPageBean.info.document.edit.alreadyRejected"));
			setPageViewOnly();
		}
	}
	
	// We use this name to store the timestamp when this party accessed this file confirm click
	public static EsfPathName createPartyFileConfirmClickLastAccessTimestampFieldPathName(EsfName partyName, EsfName fileConfirmClickFieldTemplateName)
	{
		if ( partyName == null )
			partyName = new EsfName();
		if ( fileConfirmClickFieldTemplateName == null )
			fileConfirmClickFieldTemplateName = new EsfName();
		String dataName = partyName + EsfPathName.PATH_SEPARATOR + fileConfirmClickFieldTemplateName + EsfPathName.PATH_SEPARATOR + "lastAccessTimestamp";
		return new EsfPathName(dataName);
	}
	public EsfPathName createPartyFileConfirmClickLastAccessTimestampFieldPathName(EsfName fileConfirmClickFieldTemplateName)
	{
		return createPartyFileConfirmClickLastAccessTimestampFieldPathName(context.pickupPartyName, fileConfirmClickFieldTemplateName);
	}
	
	// We use this name to store the timestamp when this party accessed this file, be it the upload or download
	public EsfPathName createPartyFileLastAccessTimestampFieldPathName(EsfUUID tranFileId)
	{
		String dataName = context.pickupPartyName + EsfPathName.PATH_SEPARATOR + tranFileId.toNormalizedEsfNameString() + EsfPathName.PATH_SEPARATOR + "lastAccessTimestamp";
		return new EsfPathName(dataName);
	}
	
	void processPotentialFileUploads(Connection con, TransactionEngine engine) // this processes any file uploads that may be present when certain non-UPLOAD buttons are pressed
		throws java.io.IOException, EsfException, SQLException
	{
		List<FieldTemplate> uploadFileFieldTemplates = context.getPartyUploadFileFieldTemplates();
		if ( uploadFileFieldTemplates != null )
		{
			for( FieldTemplate fieldTemplate : uploadFileFieldTemplates )
				processPotentialFileUpload(con,engine,fieldTemplate);
		}
	}	
	
	
	void processPotentialFileUpload(Connection con, TransactionEngine engine, FieldTemplate fieldTemplate) // this processes any file uploads that may be present when certain non-UPLOAD buttons are pressed
		throws java.io.IOException, EsfException, SQLException
	{
		if ( (context.transactionParty.isActive() && context.currTransactionPartyDocument.isWorkNeeded()) || context.isEsfReportsAccessForUpdate() )
		{
            String paramName = fieldTemplate.getEsfName().toLowerCase();
			
            int limitNumFiles = app.stringToInt(fieldTemplate.getInputFormatSpec(), -1);

            boolean onlyAllowImages = fieldTemplate.getOutputFormatSpec().startsWith("imageOnly");

			if ( fileUploadBuffers != null && fileUploadBuffers.length > 0 )
			{
			    for( com.esignforms.open.util.FileUploader.UploadToBufferInfo uploadedFile : fileUploadBuffers )
			    {
			    	if ( paramName.equals(uploadedFile.paramName) )
			    	{
			            if ( limitNumFiles > 0 )
			            {
			    			List<TransactionFile> fileList = context.transaction.getAllTransactionFilesForDocumentField(context.currTransactionDocument.getId(), fieldTemplate.getEsfName());
			    			if ( fileList.size() >= limitNumFiles )
			    			{
			    				errors.addWarning(app.getServerMessages().getString("DocumentPageBean.warn.uploadFile.tooManyFilesUploaded",limitNumFiles),paramName);
			    				continue;
			    			}
			            }

			            if ( isNonBlank(uploadedFile.fileName) )
				        {
				            if ( onlyAllowImages && ! app.isContentTypeImage(uploadedFile.contentType) )
				            {
				                errors.addError(app.getServerMessages().getString("DocumentPageBean.warn.uploadFile.imageOnlyAllowed",uploadedFile.fileName,uploadedFile.contentType),paramName);
				            }
				            else if ( uploadedFile.uncompressedSize > 0 )
				            {
				            	EsfInteger uploadSize = new EsfInteger(uploadedFile.uncompressedSize);
				            	
				            	TransactionFile tranFile = TransactionFile.Manager.createNew(context.transactionId, 
				            																context.currTransactionDocument.getId(), 
				            																context.transactionParty.getId(), 
				            																fieldTemplate.getEsfName(), 
				            																uploadedFile.fileName, 
				            																uploadedFile.contentType, 
				            																uploadedFile.fileData);			
				            	
				            	context.transaction.addTransactionFile(tranFile);
				                errors.addSuccess(app.getServerMessages().getString("DocumentPageBean.info.uploadFile.success",uploadSize.toString(),uploadedFile.fileName));
								EsfPathName fileAccessPathName = createPartyFileLastAccessTimestampFieldPathName(tranFile.getId());
								setFieldValue(fileAccessPathName, new EsfDateTime(), false);
				                context.transaction.logActionBasic(con, "Party " + context.pickupPartyName + " uploaded file " + uploadedFile.fileName + " to document " + context.currDocument.getEsfName());
				    			
				                List<TransactionFile> fileList = context.transaction.getAllTransactionFilesForDocumentField(context.currTransactionDocument.getId(), fieldTemplate.getEsfName());
				    			setFieldValue(fieldTemplate.getEsfName(), new EsfInteger(fileList.size()), fieldTemplate.isCloneAllowed());
				            }
				        }
			    	}
			    }
			}
		}
	}
	
	void processButtonFileUpload(Connection con, TransactionEngine engine, FieldTemplate fieldTemplate)
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked File Upload button; IP: " + getIP());

		if ( (context.transactionParty.isActive() && context.currTransactionPartyDocument.isWorkNeeded()) || context.isEsfReportsAccessForUpdate() )
		{
			setPageEdit(); // The upload button doesn't change our page state
			
            if ( ! hasJumpToHashtag() )
            	setJumpToHashtag(fieldTemplate.getId().toNormalizedEsfNameString());
            
            String paramName = fieldTemplate.getEsfName().toLowerCase();
			
            int limitNumFiles = app.stringToInt(fieldTemplate.getInputFormatSpec(), -1);
            
            boolean onlyAllowImages = fieldTemplate.getOutputFormatSpec().startsWith("imageOnly");

			if ( fileUploadBuffers == null || fileUploadBuffers.length < 1 )
			{
				errors.addError(app.getServerMessages().getString("DocumentPageBean.warn.uploadFile.noFilesSelected"),paramName);
			}
			else
			{
				int numFilesUploaded = 0;
			    for( com.esignforms.open.util.FileUploader.UploadToBufferInfo uploadedFile : fileUploadBuffers )
			    {
			    	if ( paramName.equals(uploadedFile.paramName) )
			    	{
			            if ( limitNumFiles > 0 )
			            {
			    			List<TransactionFile> fileList = context.transaction.getAllTransactionFilesForDocumentField(context.currTransactionDocument.getId(), fieldTemplate.getEsfName());
			    			if ( fileList.size() >= limitNumFiles )
			    			{
			    				errors.addWarning(app.getServerMessages().getString("DocumentPageBean.warn.uploadFile.tooManyFilesUploaded",limitNumFiles),paramName);
			    				continue;
			    			}
			            }

			            if ( isNonBlank(uploadedFile.fileName) )
				        {
				            if ( uploadedFile.uncompressedSize == 0 )
				            {
				                errors.addError(app.getServerMessages().getString("DocumentPageBean.warn.uploadFile.emptyFile",uploadedFile.fileName),paramName);
				            }
				            else if ( onlyAllowImages && ! app.isContentTypeImage(uploadedFile.contentType) )
				            {
				                errors.addError(app.getServerMessages().getString("DocumentPageBean.warn.uploadFile.imageOnlyAllowed",uploadedFile.fileName,uploadedFile.contentType),paramName);
				            }
				            else
				            {
				            	EsfInteger uploadSize = new EsfInteger(uploadedFile.uncompressedSize);
				            	
				            	TransactionFile tranFile = TransactionFile.Manager.createNew(context.transactionId, 
				            																context.currTransactionDocument.getId(), 
				            																context.transactionParty.getId(), 
				            																fieldTemplate.getEsfName(), 
				            																uploadedFile.fileName, 
				            																uploadedFile.contentType, 
				            																uploadedFile.fileData);			
				            	
				            	context.transaction.addTransactionFile(tranFile);
				                errors.addSuccess(app.getServerMessages().getString("DocumentPageBean.info.uploadFile.success",uploadSize.toString(),uploadedFile.fileName));
								EsfPathName fileAccessPathName = createPartyFileLastAccessTimestampFieldPathName(tranFile.getId());
								setFieldValue(fileAccessPathName, new EsfDateTime(), false);
				                context.transaction.logActionBasic(con, "Party " + context.pickupPartyName + " uploaded file " + uploadedFile.fileName + " to document " + context.currDocument.getEsfName());
				                ++numFilesUploaded;
				                
				    			List<TransactionFile> fileList = context.transaction.getAllTransactionFilesForDocumentField(context.currTransactionDocument.getId(), fieldTemplate.getEsfName());
				    			setFieldValue(fieldTemplate.getEsfName(), new EsfInteger(fileList.size()), fieldTemplate.isCloneAllowed());
				            }
				        }
			    	}
			    	else
			    	{
			    		// We can silently ignore an entry with no file name not associated with our button (occurs when there are more than 1
			    		// upload button on a page).
			    		if ( EsfString.isNonBlank(uploadedFile.fileName) )
			    			errors.addError(app.getServerMessages().getString("DocumentPageBean.warn.uploadFile.unexpectedFile",uploadedFile.fileName),paramName);
			    	}
			    }
				if ( numFilesUploaded == 0 )
				{
					errors.addError(app.getServerMessages().getString("DocumentPageBean.warn.uploadFile.noFilesUploaded"),paramName);
				}
			}
		}
		else if ( context.currTransactionPartyDocument.isCompleted() )
		{
			errors.addWarning(app.getServerMessages().getString("DocumentPageBean.warn.uploadFile.alreadyCompleted"));
			setPageViewOnly();
		}
		else if ( context.currTransactionPartyDocument.isRejected() )
		{
			errors.addWarning(app.getServerMessages().getString("DocumentPageBean.warn.uploadFile.alreadyRejected"));
			setPageViewOnly();
		}
	}
	
	void processButtonSave(Connection con, TransactionEngine engine)
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Save button; IP: " + getIP());

		if ( ! context.isEsfReportsAccessForUpdate() && context.transaction.isSuspended() )
			setPageViewOnly();
		else if ( (context.transactionParty.isActive() && context.currTransactionPartyDocument.isWorkNeeded() && ! context.isApiMode) || context.isEsfReportsAccessForUpdate() )
		{
			loadFieldsPartyCanEdit();
			processPotentialFileUploads(con,engine);
			
			if ( context.isEsfReportsAccessForUpdate() )
			{
				errors.addSuccess(app.getServerMessages().getString("DocumentPageBean.info.document.save.reportsAccess.success"));
				if ( validatePartyInput() ) {
					engine.queuePartySavedDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);
					engine.doWork(con,errors); // do this now since it may change document data or the like
					if ( errors.hasErrorOrWarning() ) { // only go to next page if no errors/warnings
						context.transaction.logGeneral(con, "Party '" + context.pickupPartyName + "' on document '" + 
								context.currDocument.getEsfName() + "' save button doWork error: " + errors.toString());
					}
				}
			}
			else
			{
				errors.addInfo(app.getServerMessages().getString("DocumentPageBean.info.document.save.success"));
				validatePartyInputButIgnoreErrors();
				// No validation takes place for regular parties on save
				engine.queuePartySavedDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);
				engine.doWork(con,errors); // do this now since it may change document data or the like
				if ( errors.hasErrorOrWarning() ) {
					context.transaction.logGeneral(con, "Party '" + context.pickupPartyName + "' on document '" + 
							context.currDocument.getEsfName() + "' save button doWork error: " + errors.toString());
				}
			}

			// Update the last updated time for our party on this document
			context.currTransactionPartyDocument.updateLastUpdatedTimestamp();
			
			setPageEdit();
		}
		else if ( context.currTransactionPartyDocument.isCompleted() )
		{
			errors.addWarning(getButtonMessageVersion().getDocumentAlreadyCompletedCannotEditMessage(context));
			setPageViewOnly();
		}
		else if ( context.currTransactionPartyDocument.isRejected() )
		{
			errors.addWarning(app.getServerMessages().getString("DocumentPageBean.info.document.edit.alreadyRejected"));
			setPageViewOnly();
		}
	}

	void processButtonNextPage(Connection con, TransactionEngine engine) // should only be called for multi-page docs
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Next Page button; IP: " + getIP());

		if ( (context.transactionParty.isActive() && context.currTransactionPartyDocument.isWorkNeeded()) || context.isEsfReportsAccessForUpdate() )
		{
			if ( ! context.isEsfReportsAccessForUpdate() && context.transaction.isSuspended() )
				setPageViewOnly();
			else
			{
				if ( ! context.isApiMode ) // only load fields if we're not in API mode 
				{
					loadFieldsPartyCanEdit();
					processPotentialFileUploads(con,engine);
				}
				
				if ( validatePartyInput() ) 
				{
					if ( ! context.transaction.isSuspended() )
					{
						engine.queuePartyCompletedPageEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument, context.currDocumentVersionPage.getEsfName());
						engine.doWork(con,errors); // do this now since it may change document data or the like
					}
					if ( ! errors.hasErrorOrWarning() ) // only go to next page if no errors/warnings
						context.goToNextEditPage();
					else
						context.transaction.logGeneral(con, "Next page party completed page doWork error: " + errors.toString());

				}
				setPageEdit();
			}
		}
		else if ( context.currTransactionPartyDocument.isCompleted() )
		{
			// Non-party view only people on a multi-page document only get to see the review page
			if ( context.isMultiPageViewOnlyNonParty() )
				context.goToMultiPageReviewPage();
			else
				context.goToNextEditPage();
			setPageViewOnly();
		}
		else if ( context.currTransactionPartyDocument.isRejected() )
		{
			// Non-party view only people on a multi-page document only get to see the review page
			if ( context.isMultiPageViewOnlyNonParty() )
				context.goToMultiPageReviewPage();
			else
				context.goToNextEditPage();
			setPageViewOnly();
		}
	}

	void processButtonPreviousPage(Connection con, TransactionEngine engine) // should only be called for multi-page docs
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Previous Page button; IP: " + getIP());

		if ( (context.transactionParty.isActive() && context.currTransactionPartyDocument.isWorkNeeded()) || context.isEsfReportsAccessForUpdate() )
		{
			if ( ! context.isEsfReportsAccessForUpdate() && context.transaction.isSuspended() )
				setPageViewOnly();
			else
			{
				if ( ! context.isApiMode ) // only load fields if we're not in API mode 
				{
					loadFieldsPartyCanEdit();
					processPotentialFileUploads(con,engine);
				}
				setPageEdit();
			}
			// no validation though
			context.goToPreviousEditPage();
		}
		else if ( context.currTransactionPartyDocument.isCompleted() )
		{
			// Non-party view only people on a multi-page document only get to see the review page
			if ( context.isMultiPageViewOnlyNonParty() )
				context.goToMultiPageReviewPage();
			else
				context.goToPreviousEditPage();
			setPageViewOnly();
		}
		else if ( context.currTransactionPartyDocument.isRejected() )
		{
			// Non-party view only people on a multi-page document only get to see the review page
			if ( context.isMultiPageViewOnlyNonParty() )
				context.goToMultiPageReviewPage();
			else
				context.goToPreviousEditPage();
			setPageViewOnly();
		}
	}
	
	// We try to see if we can map this field id to the field template that it belongs to
	private FieldTemplate resolveAutoPostFieldTemplateId(String autoPostFieldId)
	{
		EsfUUID id = EsfUUID.createFromNormalizedEsfNameString(autoPostFieldId);
		if ( id == null || id.isNull() )
			return null;
		
		for( EsfName fieldName : context.currDocumentVersionPartyFieldTemplateMap.keySet() )
		{
			FieldTemplate fieldTemplate = context.currDocumentVersionFieldTemplateMap.get(fieldName);
			if ( fieldTemplate.getId().equals(id) )
				return fieldTemplate;
		}
		
		return null;
	}

	void processAutoPost(Connection con, TransactionEngine engine, String autoPostFieldId)
		throws java.io.IOException, EsfException, SQLException
	{
		if ( ! context.isEsfReportsAccessForUpdate() && context.transaction.isSuspended() )
			setPageViewOnly();
		else if ( context.transactionParty.isActive() && context.currTransactionPartyDocument.isWorkNeeded() )
		{
			if ( ! context.isApiMode ) // only load fields if we're not in API mode 
			{
				FieldTemplate ft = resolveAutoPostFieldTemplateId(autoPostFieldId);
				loadFieldsPartyCanEdit();
				processPotentialFileUploads(con,engine);
				engine.queuePartyAutoPostDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), autoPostFieldId, ft == null ? null : ft.getEsfName());
				engine.doWork(con,errors); // do this now since it may change document data or the like
			}
			setPageEdit();
		}
	}

	void processButtonReview(Connection con, TransactionEngine engine)
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Review button; IP: " + getIP());

		if ( context.transaction.isSuspended() )
			setPageViewOnly();
		else if ( context.transactionParty.isActive() && context.currTransactionPartyDocument.isWorkNeeded() )
		{
			if ( ! context.isApiMode ) // only load fields if we're not in API mode 
			{
				loadFieldsPartyCanEdit();
				processPotentialFileUploads(con,engine);
			}
			
			if ( ! validatePartyInput() )
				setPageEdit();
			else
			{
				if ( context.isRegularPage() )
					engine.queuePartyCompletedPageEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument, context.currDocumentVersionPage.getEsfName());
				engine.queuePartyReviewDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);
				engine.doWork(con,errors); // do this now since it may change document data or the like
				if ( errors.hasErrorOrWarning() )  // remain on page if errors/warnings (don't go to review mode)
				{
					context.transaction.logGeneral(con, "Review document doWork error: " + errors.toString());
					setPageEdit();
				}
				else
				{
					// If need be, put us into multi-page review so we can check e-signing correctly across all previous pages.
					if ( context.currDocumentVersion.isMultiPage() )
						context.goToMultiPageReviewPage();
					if ( context.hasPartySignedCurrDocument() )
					{
						errors.addInfo(getButtonMessageVersion().getReviewDocumentSignerMessage(context));
						errors.addInfo(getButtonMessageVersion().getDocumentReviewButtonCompleteSignerMessage(context));
						errors.addInfo(getButtonMessageVersion().getDocumentReviewButtonReturnToEditMessage(context));
						context.currTransactionPartyDocument.setEsignIpHostAddr(getIP());
						context.currTransactionPartyDocument.setEsignTimestamp( new EsfDateTime() );
					}
					else if ( context.isViewOnlyParty() )
					{
    					errors.addInfo(getButtonMessageVersion().getViewOnlyDocumentFYIMessage(context));
        				errors.addInfo(getButtonMessageVersion().getDocumentReviewViewOnlyButtonCompleteMessage(context));
    					errors.addInfo(getButtonMessageVersion().getDocumentButtonGoToPackageMessage(context));
						context.currTransactionPartyDocument.setEsignIpHostAddr(null);
						context.currTransactionPartyDocument.setEsignTimestamp(null);
					}
					else
					{
        				errors.addInfo(getButtonMessageVersion().getReviewDocumentNotSignerMessage(context));
        				errors.addInfo(getButtonMessageVersion().getDocumentReviewButtonCompleteNotSignerMessage(context));
						errors.addInfo(getButtonMessageVersion().getDocumentReviewButtonReturnToEditMessage(context));
						context.currTransactionPartyDocument.setEsignIpHostAddr(null);
						context.currTransactionPartyDocument.setEsignTimestamp(null);
					}
					setPageReview();
				}
			}
		}
		else if ( context.currTransactionPartyDocument.isCompleted() )
		{
			errors.addWarning(getButtonMessageVersion().getDocumentAlreadyCompletedCannotEditMessage(context));
			setPageViewOnly();
			if ( context.currDocumentVersion.isMultiPage() )
				context.goToMultiPageReviewPage();
		}
		else if ( context.currTransactionPartyDocument.isRejected() )
		{
			errors.addWarning(app.getServerMessages().getString("DocumentPageBean.info.document.edit.alreadyRejected"));
			setPageViewOnly();
			if ( context.currDocumentVersion.isMultiPage() )
				context.goToMultiPageReviewPage();
		}
	}
	
	void processButtonComplete(Connection con, TransactionEngine engine)
		throws java.io.IOException, EsfException, SQLException
	{
		context.transaction.logActionDetail(con, "Party '" + context.pickupPartyName + "' clicked Complete button; IP: " + getIP());

		if ( context.transaction.isSuspended() )
			setPageViewOnly();
		else if ( context.transactionParty.isActive() && context.currTransactionPartyDocument.isWorkNeeded() )
		{
			// Don't let this complete if we don't have the capture HTML to digitally sign
			if ( ! isApiMode() && EsfString.isBlank(context.capturedHtml) )
			{
				errors.addError(app.getServerMessages().getString("DocumentPageBean.warn.document.missingCapturedHtml"));
				setPageReview();
				// Non-party view only people on a multi-page document only get to see the review page
				if ( context.isMultiPageViewOnlyNonParty() )
					context.goToMultiPageReviewPage();
				return;
			}
			
			if ( ! validatePartyInput() )
			{
				setPageReview();
				// Non-party view only people on a multi-page document only get to see the review page
				if ( context.isMultiPageViewOnlyNonParty() )
					context.goToMultiPageReviewPage();
				return;
			}
			
			engine.queuePartyCompletedDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionDocument, context.currTransactionPartyDocument, context.capturedHtml);
			engine.doWork(con,errors); // do this now so the context is updated that this document is already done
			if ( errors.hasErrorOrWarning() )  // only complete the document if no errors/warnings
			{
				setPageReview(); // something went wrong, send back to review mode and uncomplete the party on this document
				context.currTransactionPartyDocument.setStatusRetrieved();
		        context.transaction.logGeneral(con, "WARNING Party " + context.pickupPartyName + " is back on the document " + context.currDocument.getEsfName() + " because there was an error/warning completing it.");
		        context.transaction.logGeneral(con, "Party complete document doWork Error: " + errors.toString());
			}
			else
			{
				// If there is another document to work on, we go to that one now...
				if ( context.hasNextDocumentToDo() )
				{
					context.goToNextDocumentToDo();
	    			if ( context.currTransactionPartyDocument.isNetYetRetrieved() )
	    				engine.queuePartyRetrievedDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);

	    			if ( context.isViewOnlyParty() )
	    			{
    					errors.addInfo(getButtonMessageVersion().getViewOnlyDocumentFYIMessage(context));
        				errors.addInfo(getButtonMessageVersion().getDocumentReviewViewOnlyButtonCompleteMessage(context));
    					errors.addInfo(getButtonMessageVersion().getDocumentButtonGoToPackageMessage(context));
	    				setPageReview();
	    			}
	    			else
	    			{
    					errors.addInfo(getButtonMessageVersion().getCompleteDocumentEditsMessage(context));
    					if ( context.isMultiPageDocument() && context.hasNextEditPage() )
    						; // no need to prompt the user about the next page button
    					else
    						errors.addInfo(getButtonMessageVersion().getDocumentEditButtonContinueToReviewMessage(context));
    					errors.addInfo(getButtonMessageVersion().getDocumentButtonGoToPackageMessage(context));
	    				setPageEdit();
	    			}
				}
				else
				{
					completeCurrentParty(con,engine);
				}
			}
		}
		else if ( context.currTransactionPartyDocument.isCompleted() )
		{
			engine.queuePartyViewCompletedDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);
			errors.addWarning(getButtonMessageVersion().getDocumentAlreadyCompletedCannotEditMessage(context));
			setPageViewOnly();
			// Non-party view only people on a multi-page document only get to see the review page
			if ( context.isMultiPageViewOnlyNonParty() )
				context.goToMultiPageReviewPage();
		}
		else if ( context.currTransactionPartyDocument.isRejected() )
		{
			errors.addWarning(app.getServerMessages().getString("DocumentPageBean.info.document.edit.alreadyRejected"));
			setPageViewOnly();
			// Non-party view only people on a multi-page document only get to see the review page
			if ( context.isMultiPageViewOnlyNonParty() )
				context.goToMultiPageReviewPage();
		}
	}
	
	void completeCurrentParty(Connection con, TransactionEngine engine)
			throws java.io.IOException, EsfException, SQLException
	{
		if ( context.transaction.isSuspended() )
		{
			context.goToPackageDocument();		
			return;
		}
		
		engine.queuePartyCompletedEvent(context.pickupPartyName, context.transactionParty);
		engine.doWork(con,errors); // do this now so the context is updated that this party is already done
		if ( errors.hasErrorOrWarning() )
        	context.transaction.logGeneral(con, "Party completed event doWork error: " + errors.toString());
		// We don't block here even if there are errors since we don't block on party completed, only party completed document
		errors.addSuccess(getButtonMessageVersion().getCompletedAllDocumentsMessage(context));
		
		activateNextPartyOrCompleteTheTran(con,engine); // we'll activate next while on "last doc" so we have a default doc for any field substitutions

		if ( context.pkgVersionPartyTemplate.hasCompletedUrlFieldExpression() )
		{
			String url = getUrlEncodedFieldSpecWithSubstitutions(context.pkgVersionPartyTemplate.getCompletedUrlFieldExpression());
			context.transaction.logGeneral(con, "Redirecting package party '" + context.pkgVersionPartyTemplate.getEsfName() + "' to its defined completed URL: " + url);
			context.setRedirectToUrl(url);
		}
		else
			context.goToPackageDocument();		
	}
	
	// Utility routine to determine who the next party is, activate them, and if no more, complete the tran.
	void activateNextPartyOrCompleteTheTran(Connection con, TransactionEngine engine)
		throws java.io.IOException, EsfException, SQLException
	{
		TransactionParty tranParty = context.getNextTransactionParty();
		if ( tranParty == null ) 
		{
			engine.queueTransactionCompletedEvent(context.pickupPartyName);
			engine.doWork(con,errors); // do this now so the context is updated that this tran is already done
			if ( errors.hasErrorOrWarning() )
            	context.transaction.logGeneral(con, "Transaction completed doWork error: " + errors.toString());
			return;
		}
		
		// If we find an already active party, we won't do anything more.
		if ( tranParty.isActive() )
			return;
		
		PackageVersionPartyTemplate packageVersionPartyTemplate = tranParty.getPackageVersionPartyTemplate();
		if ( packageVersionPartyTemplate != null ) // generally not null, but will be when testing a document (not in a transaction)
		{
			if ( tranParty.isUndefined() )
				engine.queuePartyCreatedEvent(packageVersionPartyTemplate.getEsfName());
	        engine.queuePartyActivatedEvent(packageVersionPartyTemplate.getEsfName());
	        engine.doWork(con,errors);
			if ( errors.hasErrorOrWarning() )
            	context.transaction.logGeneral(con, "Activate next package party '" + packageVersionPartyTemplate.getEsfName() + "' - Party created/activated event doWork error: " + errors.toString());
		}
	}
	
	void loadFieldsPartyCanEdit()
	{
		// For all fields that the party can update, let's load them if they are on this page
		for( EsfName fieldName : context.currDocumentVersionPartyFieldTemplateMap.keySet() )
		{
			//PartyTemplateFieldTemplate partyFieldTemplate = context.currDocumentVersionPartyFieldTemplateMap.get(fieldName); -- currently, no special attributes yet, so no need anything
			FieldTemplate fieldTemplate = context.currDocumentVersionFieldTemplateMap.get(fieldName);
			
			// Get the appropriate field or field array based on the field type
			String fieldNameString = fieldTemplate.getEsfName().toPlainString();
			String paramName = fieldNameString.toLowerCase(); // actual param field is always lowercase
			
			if ( fieldTemplate.isTypeGeneral() ||
				 fieldTemplate.isTypeCreditCard() ||
				 fieldTemplate.isTypeDate() ||
				 fieldTemplate.isTypeEmailAddress() ||
				 fieldTemplate.isTypeInteger() ||
				 fieldTemplate.isTypeMoney() ||
				 fieldTemplate.isTypePhone() ||
				 fieldTemplate.isTypeRichTextarea() ||
				 fieldTemplate.isTypeSignature() ||
				 fieldTemplate.isTypeSsnEin() ||
				 fieldTemplate.isTypeTextarea() ||
				 fieldTemplate.isTypeZipCode()
			   )
			{
				String inputValue = getParam(paramName);
				if ( inputValue == null )
				{
					warning("loadFieldsPartyCanEdit() - Unexpectedly missing input field: " + fieldNameString + "; type: " + fieldTemplate.getType() + 
							"; tranId: " + context.transactionId + "; packageId: " + context.pkg.getId() + "; packageParty: " + context.pkgVersionPartyTemplate.getEsfName() + 
							"; docId: " + context.currDocument.getId() + "; docVerId" + context.currDocumentVersion.getId() + "; docParties: " + context.currDocumentVersionPartyNameList);
					inputValue = "";
				}
				EsfString value = new EsfString(inputValue);
				setFieldValue(fieldTemplate.getEsfName(), value, fieldTemplate.isCloneAllowed());
			}
			else if ( fieldTemplate.isTypeDateTime() ) 
			{
				String dateInputValue = getParam(paramName);
				String prefix = fieldTemplate.getId().toNormalizedEsfNameString();
				String hourInputValue = getParam(prefix+"_HH");
				String minuteInputValue = getParam(prefix+"_MM");
				String secondInputValue = getParam(prefix+"_SS");
				String timezoneInputValue = getParam(prefix+"_TZ");
				if ( dateInputValue == null )
				{
					warning("loadFieldsPartyCanEdit() - Unexpectedly missing input field: " + fieldNameString + "; type: " + fieldTemplate.getType() + 
							"; tranId: " + context.transactionId + "; packageId: " + context.pkg.getId() + "; packageParty: " + context.pkgVersionPartyTemplate.getEsfName() + 
							"; docId: " + context.currDocument.getId() + "; docVerId" + context.currDocumentVersion.getId() + "; docParties: " + context.currDocumentVersionPartyNameList);
					dateInputValue = "";
				}

				EsfString dateTimeValue = new EsfString(dateInputValue + " " + hourInputValue + ":" + minuteInputValue + ":" + secondInputValue + " " + timezoneInputValue);
				setFieldValue(fieldTemplate.getEsfName(), dateTimeValue, fieldTemplate.isCloneAllowed());
			}
			else if ( fieldTemplate.isTypeDecimal() ) 
			{
				String inputValue = getParam(paramName);
				if ( inputValue == null )
				{
					warning("loadFieldsPartyCanEdit() - Unexpectedly missing input field: " + fieldNameString + "; type: " + fieldTemplate.getType() + 
							"; tranId: " + context.transactionId + "; packageId: " + context.pkg.getId() + "; packageParty: " + context.pkgVersionPartyTemplate.getEsfName() + 
							"; docId: " + context.currDocument.getId() + "; docVerId" + context.currDocumentVersion.getId() + "; docParties: " + context.currDocumentVersionPartyNameList);
					inputValue = "";
				}
				
				// For decimals that use percent output format, if a value is provided but there's no %, we'll append one as that's a typical way to enter a percent: 10 instead of 10%
				if ( fieldTemplate.isDecimalPercentOutputFormat() && EsfString.isNonBlank(inputValue) && ! inputValue.contains("%") )
					inputValue += "%";
				
				EsfString value = new EsfString(inputValue);
				setFieldValue(fieldTemplate.getEsfName(), value, fieldTemplate.isCloneAllowed());
			}
			else if ( fieldTemplate.isTypeCheckbox() ||	fieldTemplate.isTypeRadioButtonGroup() ) 
			{
				String inputValue = getParam(paramName,""); // If a checkbox or radio button is not chosen, it will NOT send a param value in the post, so we'll default to empty value then
				EsfString value = new EsfString(inputValue);
				setFieldValue(fieldTemplate.getEsfName(), value, fieldTemplate.isCloneAllowed());
			}
			else if ( fieldTemplate.isTypeFile() ) // doesn't actually have field to be retrieved via getParam(), but saving # files uploaded to this field
			{
				List<TransactionFile> fileList = context.transaction.getAllTransactionFilesForDocumentField(context.currTransactionDocument.getId(), fieldTemplate.getEsfName());
				EsfInteger value = new EsfInteger(fileList.size());
				setFieldValue(fieldTemplate.getEsfName(), value, fieldTemplate.isCloneAllowed());
			}
			else if ( fieldTemplate.isTypeRadioButton() ) // data is handled by the radio button group
			{
			}
			else if ( fieldTemplate.isTypeSelection() )
			{
				DocumentVersion docVer = context.currDocumentVersion == null ? context.transaction.getDocumentVersion(context.currDocument) : context.currDocumentVersion;
				DropDownVersion ddv = context.transaction.getDropDownVersion(new EsfName(fieldTemplate.getInputFormatSpec()),docVer);
				if ( ddv == null )
				{
					warning("loadFieldsPartyCanEdit() - Unexpectedly missing selection DropDownVersion for input field: " + fieldNameString + 
							"; tranId: " + context.transactionId + "; packageId: " + context.pkg.getId() + "; packageParty: " + context.pkgVersionPartyTemplate.getEsfName() + 
							"; docId: " + context.currDocument.getId() + "; docVerId" + context.currDocumentVersion.getId() + "; docParties: " + context.currDocumentVersionPartyNameList +
							"; DropDownVersion: " + fieldTemplate.getInputFormatSpec());
				}
				else
				{
					if ( ddv.isAllowMultiSelection() )
					{
						String[] inputValues = getMultiParam(paramName);
						if ( inputValues == null )
						{
							warning("loadFieldsPartyCanEdit() - Unexpectedly missing multi-selection input field: " + fieldNameString + 
									"; tranId: " + context.transactionId + "; packageId: " + context.pkg.getId() + "; packageParty: " + context.pkgVersionPartyTemplate.getEsfName() + 
									"; docId: " + context.currDocument.getId() + "; docVerId" + context.currDocumentVersion.getId() + "; docParties: " + context.currDocumentVersionPartyNameList);
							inputValues = new String[0];
						}
						EsfString[] values = new EsfString[inputValues.length];
						for( int i=0; i < inputValues.length; ++i )
						{
							values[i] = new EsfString(inputValues[i]);
						}
						setFieldValues(fieldTemplate.getEsfName(), values, fieldTemplate.isCloneAllowed());
					}
					else
					{
						String inputValue = getParam(paramName);
						if ( inputValue == null )
						{
							warning("loadFieldsPartyCanEdit() - Unexpectedly missing single-selection input field: " + fieldNameString + 
									"; tranId: " + context.transactionId + "; packageId: " + context.pkg.getId() + "; packageParty: " + context.pkgVersionPartyTemplate.getEsfName() + 
									"; docId: " + context.currDocument.getId() + "; docVerId" + context.currDocumentVersion.getId() + "; docParties: " + context.currDocumentVersionPartyNameList);
							inputValue = "";
						}
						EsfString value = new EsfString(inputValue);
						setFieldValue(fieldTemplate.getEsfName(), value, fieldTemplate.isCloneAllowed());
					}
				}
			}
			else if ( fieldTemplate.isTypeSignDate() ) // doesn't actually have field to be retrieved via getParam()
			{
				// Let's see if we have a Signature field with the same name (minus the Date suffix) that has been provided and if so, set the date
				EsfName signatureFieldName = new EsfName( fieldTemplate.getEsfName().toString().substring(0, fieldTemplate.getEsfName().getLength()-4));
				FieldTemplate signatureFieldTemplate = context.currDocumentVersionFieldTemplateMap.get(signatureFieldName);
				if ( signatureFieldTemplate != null && signatureFieldTemplate.isTypeSignature() )
				{
					EsfString signatureInputValue = (EsfString)getFieldValue(signatureFieldName);
					if ( EsfString.isBlank(signatureInputValue) )
						setFieldValue(fieldTemplate.getEsfName(), new EsfDate((EsfDate)null), false);
					else
						setFieldValue(fieldTemplate.getEsfName(), new EsfDate(), false);
				}
			}
		}
	}
	
	// Finds all fields in the document that are radio buttons for the specified group name
	List<FieldTemplate> getRadioButtonsForGroup(EsfName radioButtonGroupName)
	{
		LinkedList<FieldTemplate> list = new LinkedList<FieldTemplate>();
		
		for( EsfName fieldName : context.currDocumentVersionFieldTemplateMap.keySet() )
		{
			FieldTemplate fieldTemplate = context.currDocumentVersionFieldTemplateMap.get(fieldName);
			if ( fieldTemplate.isTypeRadioButton() )
			{
				EsfName groupName = new EsfName(fieldTemplate.getOutputFormatSpec());
				if ( radioButtonGroupName.equals(groupName) )
					list.add(fieldTemplate);
			}
		}
		
		return list;
	}
	
	synchronized void validatePartyInputButIgnoreErrors()
	{
		Errors realErrors = errors; // save the real errors object
		errors = new Errors(); // create a temporary errors object to use for error reporting
		validatePartyInput(); // ignore results of validation
		errors = realErrors; // restore the real errors object, ignoring any set in our temporary one during validation
	}
	
	boolean validatePartyInput()
	{
		// For all fields that the party can update, let's validate them
		for( EsfName fieldName : context.currDocumentVersionPartyFieldTemplateMap.keySet() )
		{
			//PartyTemplateFieldTemplate partyFieldTemplate = context.currDocumentVersionPartyFieldTemplateMap.get(fieldName); -- currently, no special attributes yet, so no need anything
			FieldTemplate fieldTemplate = context.currDocumentVersionFieldTemplateMap.get(fieldName);
			
			// Get the appropriate field or field array based on the field type
			String fieldNameString = fieldTemplate.getEsfName().toPlainString();
			String paramName = fieldNameString.toLowerCase(); // actual param field is always lowercase
			
			if ( fieldTemplate.isTypeGeneral() )
			{
				EsfString inputValue = (EsfString)getFieldValue(fieldTemplate.getEsfName());
				if ( isRequired(fieldTemplate) && inputValue.isBlank() )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.general.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				if ( fieldTemplate.hasInputFormatSpec() && inputValue.getLength() > 0 ) 
				{
					String stringValue = ( inputValue == null ) ? "" : inputValue.toString();
					if ( ! stringValue.matches(fieldTemplate.getInputFormatSpec()) ) 
					{
						String tip = fieldTemplate.hasTooltip() ? fieldTemplate.getTooltip() : "";
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.general.badInputFormat",fieldTemplate.getLabelTemplate().getLabel(),tip), paramName);
					}
				}
				if ( fieldTemplate.getMinLength() > inputValue.getLength() && inputValue.getLength() > 0 ) // min length is only checked if something is specified -- use 'required' for if blank is not allowed
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.general.tooShort",fieldTemplate.getLabelTemplate().getLabel(),fieldTemplate.getMinLength(),inputValue.getLength()), paramName);
				if ( fieldTemplate.getMaxLength() < inputValue.getLength() )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.general.tooLong",fieldTemplate.getLabelTemplate().getLabel(),fieldTemplate.getMaxLength(),inputValue.getLength()), paramName);
			}
			else if ( fieldTemplate.isTypeCheckbox() )
			{
				EsfString inputValue = (EsfString)getFieldValue(fieldTemplate.getEsfName());
				if ( isRequired(fieldTemplate) && inputValue.isBlank() )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.checkbox.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
			}
			else if ( fieldTemplate.isTypeCreditCard() )
			{
				EsfString inputValue = (EsfString)getFieldValue(fieldTemplate.getEsfName());
				
				if ( isRequired(fieldTemplate) && inputValue.isBlank() )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.creditcard.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				else if ( inputValue.isNonBlank() )
				{
					if ( ! CreditCardValidator.isValid(inputValue.toString()) )
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.creditcard.invalid",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				}
			}
			else if ( fieldTemplate.isTypeDate() )
			{
				EsfValue value = getFieldValue(fieldTemplate.getEsfName());
				if ( value instanceof EsfDate )
					continue; // if we have the right type, it's already been validated
				
				EsfDate dateValue = null;
				EsfString inputValue = (EsfString)value;
				
				if ( inputValue != null && inputValue.isNonBlank() )
				{
					if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_MM_DD_YYYY) )
						dateValue = EsfDate.CreateFromMDY(inputValue.toPlainString());
					else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_DD_Mon_YYYY) )
						dateValue = EsfDate.CreateFromDDMonYYYY(inputValue.toPlainString());
					else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_DD_MM_YYYY) )
						dateValue = EsfDate.CreateFromDMY(inputValue.toPlainString());
					else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_YYYY_MM_DD) )
						dateValue = EsfDate.CreateFromYMD(inputValue.toPlainString());
					else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.ISO_FORMAT_DD_MM_YYYY) )
						dateValue = EsfDate.CreateFromIsoDMY(inputValue.toPlainString());
					else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_MM_YYYY) )
						dateValue = EsfDate.CreateFromMMYYYY(inputValue.toPlainString());
				}

				if ( isRequired(fieldTemplate) && (dateValue==null || dateValue.isNull()) )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.date.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				else if ( dateValue != null && ! dateValue.isNull() && isPageEdit() )
					setFieldValue(fieldTemplate.getEsfName(), dateValue, fieldTemplate.isCloneAllowed()); // replace with the valid value
			}
			else if ( fieldTemplate.isTypeDateTime() )
			{
				EsfValue value = getFieldValue(fieldTemplate.getEsfName());
				if ( value instanceof EsfDateTime )
					continue; // if we have the right type, it's already been validated
				
				EsfDateTime dateTimeValue = null;
				EsfString dateTimeInputValue = (EsfString)value;
				
				if ( dateTimeInputValue != null && dateTimeInputValue.isNonBlank() )
				{
					String[] dateTimeTzString = dateTimeInputValue.toPlainString().split(" ");
					String calValue = dateTimeTzString.length > 0 ? dateTimeTzString[0] : "";
					EsfDate dateValue = null;
					
					String[] timeString = dateTimeTzString.length > 1 ? dateTimeTzString[1].split(":") : new String[0];
					String hourValue   = timeString.length > 0 ? timeString[0] : "";
					String minuteValue = timeString.length > 1 ? timeString[1] : "";
					String secondValue = timeString.length > 2 ? timeString[2] : "";
					
					String timezoneValue = dateTimeTzString.length > 2 ? dateTimeTzString[2] : "";
					
					// If they've given us of the date or time fields (ignoring the timezone for now), we will attempt to validate the input.
					if ( EsfString.areAnyNonBlank(calValue,hourValue,minuteValue,secondValue) )
					{
						if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_MM_DD_YYYY) )
							dateValue = EsfDate.CreateFromMDY(calValue);
						else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_DD_Mon_YYYY) )
							dateValue = EsfDate.CreateFromDDMonYYYY(calValue);
						else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_DD_MM_YYYY) )
							dateValue = EsfDate.CreateFromDMY(calValue);
						else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_YYYY_MM_DD) )
							dateValue = EsfDate.CreateFromYMD(calValue);
						else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.ISO_FORMAT_DD_MM_YYYY) )
							dateValue = EsfDate.CreateFromIsoDMY(calValue);
						else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_MM_YYYY) )
							dateValue = EsfDate.CreateFromMMYYYY(calValue);
						
						if ( ! fieldTemplate.getOutputFormatSpec().contains("s") && EsfString.isBlank(secondValue) )
							secondValue = "00"; // for this purpose only -- validation -- if they don't care about seconds, we won't treat it as empty, but as 0 seconds so it passes validation
						
						if ( dateValue == null || dateValue.isNull() || areAnyBlank(hourValue,minuteValue,secondValue,timezoneValue) )
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.datetime.invalid",fieldTemplate.getLabelTemplate().getLabel()), paramName, paramName+"_HH", paramName+"_MM", paramName+"_SS", paramName+"_TZ");
						else
						{
							dateTimeValue = EsfDateTime.CreateFromYYYYMMDDHHMMSSTTT( dateValue.toYMDString() + " " + hourValue + ":" + minuteValue + ":" + secondValue + " " + timezoneValue );
							if ( dateTimeValue == null || dateTimeValue.isNull() )
								errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.datetime.invalid",fieldTemplate.getLabelTemplate().getLabel()), paramName, paramName+"_HH", paramName+"_MM", paramName+"_SS", paramName+"_TZ");
						}
					}
				}

				if ( isRequired(fieldTemplate) && (dateTimeValue==null || dateTimeValue.isNull()) )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.datetime.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				else if ( dateTimeValue != null && isPageEdit() )
					setFieldValue(fieldTemplate.getEsfName(), dateTimeValue, fieldTemplate.isCloneAllowed()); // replace with the valid value
			}
			else if ( fieldTemplate.isTypeDecimal() )
			{
				EsfValue value = getFieldValue(fieldTemplate.getEsfName());
				if ( value instanceof EsfDecimal )
					continue; // if we have the right type, it's already been validated

				EsfString inputValue = (EsfString)value;
				
				EsfDecimal decimalValue; // if not null, then we'll use keep that value
				
				if ( isRequired(fieldTemplate) && (inputValue == null || inputValue.isBlank()) ) 
				{
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.decimal.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					decimalValue = null;
				}
				else if ( ! isRequired(fieldTemplate) && inputValue.isBlank() )
				{
					// okay
					decimalValue = new EsfDecimal((String)null);
				}
				else
				{
					decimalValue = new EsfDecimal(inputValue.toPlainString());
					if ( decimalValue.isNull() )
					{
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.decimal.invalid",fieldTemplate.getLabelTemplate().getLabel()), paramName);
						decimalValue = null;
					}
					else
					{
						EsfInteger minIntegerValue = new EsfInteger(fieldTemplate.getMinLength()); // our lengths are actually min/max integer value
						if ( minIntegerValue.isNull() )
							minIntegerValue = new EsfInteger(Long.MIN_VALUE);
						EsfInteger maxIntegerValue = new EsfInteger(fieldTemplate.getMaxLength());
						if ( maxIntegerValue.isNull() )
							maxIntegerValue = new EsfInteger(Long.MAX_VALUE);
						
						if ( decimalValue.isLessThan( new EsfDecimal(minIntegerValue.toPlainString()) ) )
						{
							String formattedMin = minIntegerValue.format(fieldTemplate.getOutputFormatSpec());
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.decimal.tooSmall",fieldTemplate.getLabelTemplate().getLabel(),formattedMin), paramName);
							decimalValue = null;
						}
						else if ( decimalValue.isGreaterThan( new EsfDecimal(maxIntegerValue.toPlainString()) ) )
						{
							String formattedMax = maxIntegerValue.format(fieldTemplate.getOutputFormatSpec());
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.decimal.tooBig",fieldTemplate.getLabelTemplate().getLabel(),formattedMax), paramName);
							decimalValue = null;
						}
					}
				}

				if ( decimalValue != null && isPageEdit() )
					setFieldValue(fieldTemplate.getEsfName(), decimalValue, fieldTemplate.isCloneAllowed());
			}
			else if ( fieldTemplate.isTypeEmailAddress() )
			{
				EsfValue value = getFieldValue(fieldTemplate.getEsfName());
				if ( value instanceof EsfEmailAddress )
					continue; // if we have the right type, it's already been validated

				EsfEmailAddress emailValue = new EsfEmailAddress(value.toPlainString());
				if ( isRequired(fieldTemplate) && emailValue.isBlank() )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.email.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				else if ( emailValue.isNonBlank() && ! emailValue.isValid() )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.email.invalid",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				else if ( isPageEdit() )
					setFieldValue(fieldTemplate.getEsfName(), emailValue, fieldTemplate.isCloneAllowed()); // replace with the valid value
			}
			else if ( fieldTemplate.isTypeFile() && ! context.isEsfReportsAccess() ) // for files, we don't require the reports access party to ever read
			{
				if ( isRequired(fieldTemplate) )
				{
					List<TransactionFile> fileList = context.transaction.getAllTransactionFilesForDocumentField(context.currTransactionDocument.getId(), fieldTemplate.getEsfName());
					if ( fileList == null || fileList.size() == 0 )
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.file.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				}
			}
			else if ( fieldTemplate.isTypeFileConfirmClick() )
			{
				if ( isRequired(fieldTemplate) && ! context.isEsfReportsAccess() ) // for files, we don't require the reports access party to ever read
				{
					EsfPathName fileConfirmClickPathName = createPartyFileConfirmClickLastAccessTimestampFieldPathName(fieldTemplate.getEsfName());
					EsfDateTime accessDateTime = (EsfDateTime)getFieldValue(fileConfirmClickPathName);
					if ( accessDateTime == null )
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.fileconfirmclick.notRead",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				}
			}
			else if ( fieldTemplate.isTypeInteger() )
			{
				EsfValue value = getFieldValue(fieldTemplate.getEsfName());
				if ( value instanceof EsfInteger )
					continue; // if we have the right type, it's already been validated

				EsfString inputValue = (EsfString)value;
				
				EsfInteger integerValue; // if not null, then we'll use to keep that value
				
				if ( isRequired(fieldTemplate) && (inputValue == null || inputValue.isBlank()) ) 
				{
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.integer.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					integerValue = null;
				}
				else if ( ! isRequired(fieldTemplate) && inputValue.isBlank() )
				{
					// okay
					integerValue = new EsfInteger((String)null);
				}
				else
				{
					integerValue = new EsfInteger(inputValue.toPlainString());
					if ( integerValue.isNull() )
					{
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.integer.invalid",fieldTemplate.getLabelTemplate().getLabel()), paramName);
						integerValue = null;
					}
					else
					{
						EsfInteger minIntegerValue = new EsfInteger(fieldTemplate.getMinLength()); // our lengths are actually min/max integer value
						if ( minIntegerValue.isNull() )
							minIntegerValue = new EsfInteger(Long.MIN_VALUE);
						EsfInteger maxIntegerValue = new EsfInteger(fieldTemplate.getMaxLength());
						if ( maxIntegerValue.isNull() )
							maxIntegerValue = new EsfInteger(Long.MAX_VALUE);
						
						if ( integerValue.isLessThan(minIntegerValue) )
						{
							String formattedMin = minIntegerValue.format(fieldTemplate.getOutputFormatSpec());
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.integer.tooSmall",fieldTemplate.getLabelTemplate().getLabel(),formattedMin), paramName);
							integerValue = null;
						}
						else if ( integerValue.isGreaterThan(maxIntegerValue) )
						{
							String formattedMax = maxIntegerValue.format(fieldTemplate.getOutputFormatSpec());
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.integer.tooBig",fieldTemplate.getLabelTemplate().getLabel(),formattedMax), paramName);
							integerValue = null;
						}
					}
				}

				if ( integerValue != null && isPageEdit() )
					setFieldValue(fieldTemplate.getEsfName(), integerValue, fieldTemplate.isCloneAllowed());
			}
			else if ( fieldTemplate.isTypeMoney() )
			{
				EsfValue value = getFieldValue(fieldTemplate.getEsfName());
				if ( value instanceof EsfMoney )
					continue; // if we have the right type, it's already been validated

				EsfString inputValue = (EsfString)value;
				
				EsfMoney moneyValue; // if not null, then we'll use keep that value
				
				if ( isRequired(fieldTemplate) && (inputValue == null || inputValue.isBlank()) ) 
				{
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.money.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					moneyValue = null;
				}
				else if ( ! isRequired(fieldTemplate) && inputValue.isBlank() )
				{
					// okay
					moneyValue = new EsfMoney((String)null);
				}
				else
				{
					moneyValue = new EsfMoney(inputValue.toPlainString());
					if ( moneyValue.isNull() )
					{
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.money.invalid",fieldTemplate.getLabelTemplate().getLabel()), paramName);
						moneyValue = null;
					}
					else
					{
						EsfInteger minIntegerValue = new EsfInteger(fieldTemplate.getMinLength()); // our lengths are actually min/max integer value
						if ( minIntegerValue.isNull() )
							minIntegerValue = new EsfInteger(Long.MIN_VALUE);
						EsfInteger maxIntegerValue = new EsfInteger(fieldTemplate.getMaxLength());
						if ( maxIntegerValue.isNull() )
							maxIntegerValue = new EsfInteger(Long.MAX_VALUE);
						
						if ( moneyValue.isLessThan( new EsfMoney(minIntegerValue.toPlainString()) ) )
						{
							String formattedMin = minIntegerValue.format(fieldTemplate.getOutputFormatSpec());
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.money.tooSmall",fieldTemplate.getLabelTemplate().getLabel(),formattedMin), paramName);
							moneyValue = null;
						}
						else if ( moneyValue.isGreaterThan( new EsfMoney(maxIntegerValue.toPlainString()) ) )
						{
							String formattedMax = maxIntegerValue.format(fieldTemplate.getOutputFormatSpec());
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.money.tooBig",fieldTemplate.getLabelTemplate().getLabel(),formattedMax), paramName);
							moneyValue = null;
						}
					}
				}

				if ( moneyValue != null && isPageEdit() )
					setFieldValue(fieldTemplate.getEsfName(), moneyValue, fieldTemplate.isCloneAllowed());
			}
			else if ( fieldTemplate.isTypePhone() )
			{
				EsfString inputValue = (EsfString)getFieldValue(fieldTemplate.getEsfName());
				
				if ( isRequired(fieldTemplate) && inputValue.isBlank() )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.phone.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				else if ( inputValue.isNonBlank() )
				{
					String countryCode = fieldTemplate.hasInputFormatSpec() ? fieldTemplate.getInputFormatSpec() : app.getDefaultCountryCode();
					PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
					try 
					{
						PhoneNumber pn = phoneUtil.parse(inputValue.toPlainString(), countryCode);
						if ( ! phoneUtil.isValidNumber(pn) )
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.phone.invalid",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					} 
					catch (NumberParseException e) 
					{
						debug("validatePartyInput() - PhoneNumberUtil.parse() NumberParseException was thrown for phone: " + inputValue.toPlainString() + "; countryCode: " + countryCode + "; exception: " + e.getMessage());
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.phone.invalid",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					}
				}
			}
			else if ( fieldTemplate.isTypeRadioButton() )
			{
				// validations are on the radio button group as it carries the data value
			}
			else if ( fieldTemplate.isTypeRadioButtonGroup() )
			{
				EsfString inputValue = (EsfString)getFieldValue(fieldTemplate.getEsfName());
				if ( isRequired(fieldTemplate) && inputValue.isBlank() )
				{
					StringBuilder radioLabels = new StringBuilder(100);
					List<FieldTemplate> radios = getRadioButtonsForGroup(fieldTemplate.getEsfName());
					String[] errorFields = new String[radios.size()];
					int i = 0;
					for( FieldTemplate radio : radios )
					{
						errorFields[i++] = radio.getEsfName().toPlainString();
						if ( radioLabels.length() > 0 )
							radioLabels.append(", ");
						radioLabels.append(radio.getLabelTemplate().getLabel());
					}
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.radiobuttongroup.required",radioLabels.toString()), errorFields);
				}
			}
			else if ( fieldTemplate.isTypeRichTextarea() )
			{
				EsfValue value = getFieldValue(fieldTemplate.getEsfName());
				if ( value instanceof EsfHtml )
					continue; // if we have the right type, it's already been validated
				
				EsfString inputValue = (EsfString)value;
				EsfHtml htmlValue; // if not null, then we'll use to keep that value
				
				if ( isRequired(fieldTemplate) && (inputValue == null || inputValue.isBlank()) )
				{
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.richtextarea.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					htmlValue = null;
				}
				else if ( ! isRequired(fieldTemplate) && inputValue.isBlank() )
				{
					// okay
					htmlValue = new EsfHtml((String)null);
				}
				else
				{
					htmlValue = new EsfHtml(inputValue);
					if ( fieldTemplate.getMinLength() > inputValue.getLength() ) 
					{
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.richtextarea.tooShort",fieldTemplate.getLabelTemplate().getLabel(),fieldTemplate.getMinLength(),inputValue.getLength()), paramName);
						htmlValue = null;
					}
					if ( fieldTemplate.getMaxLength() < inputValue.getLength() )
					{
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.richtextarea.tooLong",fieldTemplate.getLabelTemplate().getLabel(),fieldTemplate.getMaxLength(),inputValue.getLength()), paramName);
						htmlValue = null;
					}
				}
				
				if ( htmlValue != null && isPageEdit() )
					setFieldValue(fieldTemplate.getEsfName(), htmlValue, fieldTemplate.isCloneAllowed());
			}
			else if ( fieldTemplate.isTypeSelection() )
			{
				EsfString[] inputValues = (EsfString[])getFieldValues(fieldTemplate.getEsfName());
				if ( inputValues == null || inputValues.length == 0 )
				{
					if ( isRequired(fieldTemplate) )
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.selection.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				}
				else
				{
					if ( inputValues.length == 1 ) 
					{
						if ( isRequired(fieldTemplate) && inputValues[0].isBlank() )
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.selection.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					}
				}
			}
			else if ( fieldTemplate.isTypeSignature() )
			{
				EsfString inputValue = (EsfString)getFieldValue(fieldTemplate.getEsfName());
				
				String mustMatch;
				if ( fieldTemplate.hasInputFormatSpec() )
					mustMatch = context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, fieldTemplate.getInputFormatSpec()).trim();
				else
					mustMatch = null;
				
				if ( EsfString.isNonBlank(mustMatch) && ! mustMatch.equalsIgnoreCase(inputValue.toString()) )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.signature.noMatch",mustMatch,fieldTemplate.getLabelTemplate().getLabel()), paramName);
				else if ( isRequired(fieldTemplate) && inputValue.isBlank() )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.signature.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				if ( fieldTemplate.getMinLength() > inputValue.getLength() && inputValue.getLength() > 0 ) // min length is only checked if something is specified -- use 'required' for if blank is not allowed
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.signature.tooShort",fieldTemplate.getLabelTemplate().getLabel(),fieldTemplate.getMinLength(),inputValue.getLength()), paramName);
				if ( fieldTemplate.getMaxLength() < inputValue.getLength() )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.signature.tooLong",fieldTemplate.getLabelTemplate().getLabel(),fieldTemplate.getMaxLength(),inputValue.getLength()), paramName);

				if ( isPageEdit() && ! errors.hasError() ) // for signature fields
				{
					setFieldValue( new EsfName(fieldNameString+"_Timestamp"), new EsfDateTime(), false);
					setFieldValue( new EsfName(fieldNameString+"_IPOnly"), new EsfString(getIPOnly()), false);
					setFieldValue( new EsfName(fieldNameString+"_IP"), new EsfString(getIP()), false);
				}
			}
			else if ( fieldTemplate.isTypeSignDate() )
			{
				// nothing to validate
			}
			else if ( fieldTemplate.isTypeSsnEin() )
			{
				// We only know if this is an EIN versus an SSN if the output format spec includes the EIN description
				if ( fieldTemplate.hasOutputFormatSpec() && fieldTemplate.getOutputFormatSpec().endsWith("EIN") )
				{
					EsfString inputValue = (EsfString)getFieldValue(fieldTemplate.getEsfName());
					
					if ( isRequired(fieldTemplate) && inputValue.isBlank() )
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.ein.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					else if ( inputValue.isNonBlank() )
					{
						String valueNumbers = inputValue.getOnlyNumericString();
						if ( valueNumbers.length() != 9 ) 
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.ein.wrongsize",fieldTemplate.getLabelTemplate().getLabel(),valueNumbers.length()), paramName);
						else if ( valueNumbers.startsWith("00") || valueNumbers.endsWith("0000000") )
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.ein.zerosegments",fieldTemplate.getLabelTemplate().getLabel()), paramName);
						else if ( ! FieldTemplate.isValidEINPrefix(valueNumbers) )
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.ein.invalidprefix",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					}
				}
				else
				{
					EsfString inputValue = (EsfString)getFieldValue(fieldTemplate.getEsfName());
					
					if ( isRequired(fieldTemplate) && inputValue.isBlank() )
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.ssn.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					else if ( inputValue.isNonBlank() )
					{
						String valueNumbers = inputValue.getOnlyNumericString();
						if ( valueNumbers.length() != 9 ) 
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.ssn.wrongsize",fieldTemplate.getLabelTemplate().getLabel(),valueNumbers.length()), paramName);
						else if ( valueNumbers.startsWith("000") || valueNumbers.endsWith("0000") || valueNumbers.substring(3, 5).equals("00") )
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.ssn.zerosegments",fieldTemplate.getLabelTemplate().getLabel()), paramName);
						else if ( valueNumbers.startsWith("666") )
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.ssn.no666",fieldTemplate.getLabelTemplate().getLabel()), paramName);
						// 3/6/2013: We now allow the SSN to start with a 9 since that would be an ITIN, which some may have instead of a traditional SSN.
						// An ITIN, or Individual Taxpayer Identification Number, is a tax processing number only available for certain nonresident and resident aliens, their spouses, 
						// and dependents who cannot get a Social Security Number (SSN). It is a 9-digit number, beginning with the number "9", formatted like an SSN (NNN-NN-NNNN).
						//else if ( valueNumbers.startsWith("9") )
						//	errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.ssn.no9",fieldTemplate.getLabelTemplate().getLabel()), paramName);
						else if ( valueNumbers.equals("078051120") )
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.ssn.invalidated",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					}
				}
			}
			else if ( fieldTemplate.isTypeTextarea() )
			{
				EsfString inputValue = (EsfString)getFieldValue(fieldTemplate.getEsfName());
				
				if ( isRequired(fieldTemplate) && inputValue.isBlank() )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.textarea.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				if ( fieldTemplate.getMinLength() > inputValue.getLength() && inputValue.getLength() > 0 ) // min length is only checked if something is specified -- use 'required' for if blank is not allowed
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.textarea.tooShort",fieldTemplate.getLabelTemplate().getLabel(),fieldTemplate.getMinLength(),inputValue.getLength()), paramName);
				if ( fieldTemplate.getMaxLength() < inputValue.getLength() )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.textarea.tooLong",fieldTemplate.getLabelTemplate().getLabel(),fieldTemplate.getMaxLength(),inputValue.getLength()), paramName);
			}
			else if ( fieldTemplate.isTypeZipCode() )
			{
				EsfString inputValue = (EsfString)getFieldValue(fieldTemplate.getEsfName());
				
				if ( isRequired(fieldTemplate) && inputValue.isBlank() )
					errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.zipcode.required",fieldTemplate.getLabelTemplate().getLabel()), paramName);
				else if ( inputValue.isNonBlank() )
				{
					String zipNumbers = inputValue.getOnlyNumericString();
					if ( fieldTemplate.getInputFormatSpec().equals("5") && zipNumbers.length() != 5 ) 
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.zipcode.not5digits",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					else if ( fieldTemplate.getInputFormatSpec().equals("9") && zipNumbers.length() != 9 ) 
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.zipcode.not9digits",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					else if ( fieldTemplate.getInputFormatSpec().equals("") && zipNumbers.length() != 5 && zipNumbers.length() != 9 ) 
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.zipcode.not5or9digits",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					else if ( ! app.isValidZip(inputValue.toPlainString()) )
					{
						if ( fieldTemplate.getInputFormatSpec().equals("5") )
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.zipcode.invalid.5digits",fieldTemplate.getLabelTemplate().getLabel()), paramName);
						else if ( fieldTemplate.getInputFormatSpec().equals("9") )
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.zipcode.invalid.9digits",fieldTemplate.getLabelTemplate().getLabel()), paramName);
						else 
							errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.zipcode.invalid.5or9digits",fieldTemplate.getLabelTemplate().getLabel()), paramName);
					}
					else if ( isPageEdit() )
						setFieldValue(fieldTemplate.getEsfName(), new EsfString( app.makePrettyZip(inputValue.toPlainString())), fieldTemplate.isCloneAllowed()); // update with format
				}
			}
		}
		
		if ( ! context.isEsfReportsAccess() ) 
		{
			// All files should have been accessed before they continue, unless the optional flag has been set for this party and File field
			for( TransactionFile tranFile : context.transaction.getAllTransactionFilesForDocument(context.currTransactionDocument.getId()) )
			{
				FieldTemplate fieldTemplate = context.currDocumentVersionFieldTemplateMap.get(tranFile.getFieldName());
				if ( fieldTemplate == null )
				{
					warning("validatePartyInput() - Could not find field template with tran file field name: " + tranFile.getFieldName());
					continue;
				}
				
				if ( context.isRegularPage() && ! context.currDocumentVersionPage.hasFieldTemplateIdOnPage(fieldTemplate.getId()) )
					continue;
				
				EsfValue optionalViewDownloadFiles = context.transaction.getFieldValue(
						context.currDocument.getEsfName(), 
						new EsfPathName(tranFile.getFieldName(), context.pkgVersionPartyTemplate.getId().toEsfName(), FieldTemplate.VIEW_DOWNLOAD_OPTIONAL_PATH_SUFFIX));
				
				EsfValue blockViewDownloadFiles = context.transaction.getFieldValue(
						context.currDocument.getEsfName(), 
						new EsfPathName(tranFile.getFieldName(), context.pkgVersionPartyTemplate.getId().toEsfName(), FieldTemplate.VIEW_DOWNLOAD_PROHIBITED_PATH_SUFFIX));
				
				if ( optionalViewDownloadFiles == null && blockViewDownloadFiles == null )
				{
					EsfPathName fileAccessPathName = createPartyFileLastAccessTimestampFieldPathName(tranFile.getId());
					EsfDateTime accessDateTime = (EsfDateTime)getFieldValue(fileAccessPathName);
					if ( accessDateTime == null )
						errors.addError(app.getServerMessages().getString("DocumentPageBean.error.document.validation.file.notRead",tranFile.getFileName()), tranFile.getFieldName().toPlainString());
				}
			}
		}
		
		return ! errors.hasError();
	}

	public boolean isInputMode(FieldTemplate fieldTemplate)
	{
		FieldTemplate checkFieldTemplate = fieldTemplate.isTypeRadioButton() ? context.currDocumentVersionFieldTemplateMap.get( new EsfName(fieldTemplate.getOutputFormatSpec()) ) : fieldTemplate;
		return checkFieldTemplate != null && isPageEdit() && context.currDocumentVersionPartyFieldTemplateMap.get(checkFieldTemplate.getEsfName()) != null;
	}
	public boolean isRequired(FieldTemplate fieldTemplate)
	{
		FieldTemplate checkFieldTemplate = fieldTemplate.isTypeRadioButton() ? context.currDocumentVersionFieldTemplateMap.get( new EsfName(fieldTemplate.getOutputFormatSpec()) ) : fieldTemplate;
		if ( checkFieldTemplate == null )
			return false;
		PartyTemplateFieldTemplate ptft = context.currDocumentVersionPartyFieldTemplateMap.get(checkFieldTemplate.getEsfName());
		return checkFieldTemplate.isRequired() && ptft != null && ptft.isKeepRequired();
	}
	public boolean isOptionalStyleRequired(FieldTemplate fieldTemplate)
	{
		FieldTemplate checkFieldTemplate = fieldTemplate.isTypeRadioButton() ? context.currDocumentVersionFieldTemplateMap.get( new EsfName(fieldTemplate.getOutputFormatSpec()) ) : fieldTemplate;
		return checkFieldTemplate != null && checkFieldTemplate.isOptionalStyleRequired();
	}
	
	public File getFile(EsfName fileName, EsfUUID fileId)
	{
		if ( context.hasTransaction() )
			return context.transaction.getFile(fileName,fileId,context.currDocumentVersion);
		
		// With no transaction, it'll have to be in the template
		Library templateLibrary = Library.Manager.getTemplate();
		File file = File.Manager.getByName(templateLibrary.getId(), fileName);
		if ( file != null )
			return file;
		
		// Rats!  Okay, let's try using the fileId, if any
		file = (fileId == null) ? null : File.Manager.getById(fileId);
		if ( file == null )
			warning("getFile() - Failed to find File with name: " + fileName + "; fileId: " + fileId);
		return file;
	}
	
	public FileVersion getFileVersion(EsfName fileName)
	{
		if ( context.hasTransaction() )
			return context.transaction.getFileVersion(fileName,context.currDocumentVersion);
		
		// With no transaction, it'll have to be in the template
		Library templateLibrary = Library.Manager.getTemplate();
		File file = File.Manager.getByName(templateLibrary.getId(), fileName);
		FileVersion fileVersion = file == null ? null : file.getTestFileVersion();
		if ( fileVersion == null )
			warning("getFileVersion() - Failed to find FileVersion with name: " + fileName);
		return fileVersion;
	}
	
	public Image getImage(EsfName imageName, EsfUUID imageId)
	{
		if ( context.hasTransaction() )
			return context.transaction.getImage(imageName,imageId,context.currDocumentVersion);
		
		// With no transaction, it'll have to be in the template
		Library templateLibrary = Library.Manager.getTemplate();
		Image image = Image.Manager.getByName(templateLibrary.getId(), imageName);
		if ( image != null )
			return image;
		
		// Rats!  Okay, let's try using the imageId, if any
		image = (imageId == null) ? null : Image.Manager.getById(imageId);
		if ( image == null )
			warning("getImage() - Failed to find Image with name: " + imageName + "; imageId: " + imageId);
		return image;
	}
	
	public EmailTemplate getEmailTemplate(EsfName emailName)
	{
		if ( context.hasTransaction() )
			return context.transaction.getEmailTemplate(emailName,context.currLibrary);
		
		// With no transaction, it'll have to be in the template
		Library templateLibrary = Library.Manager.getTemplate();
		EmailTemplate email = EmailTemplate.Manager.getByName(templateLibrary.getId(), emailName);
		if ( email != null )
			return email;
		
		warning("getEmailTemplate() - Failed to find EmailTemplate with name: " + emailName);
		return null;
	}
	
	public boolean isParty(EsfName docPartyName)
	{
		if ( context.hasNoTransaction() || docPartyName == null || ! docPartyName.isValid() )
			return false;
		return context.isDocumentParty(docPartyName);
	}
	
	public EsfValue getFieldValue(EsfName fieldName)
	{
		return getFieldValue(context.currDocument.getEsfName(),fieldName);
	}
	public EsfValue getFieldValue(String fieldName) // utility method for passing a string name
	{
		return getFieldValue(new EsfName(fieldName));
	}
	public EsfValue getFieldValue(EsfPathName fieldPathName)
	{
		return getFieldValue(context.currDocument.getEsfName(),fieldPathName);
	}
	public EsfValue[] getFieldValues(EsfName fieldName)
	{
		return getFieldValues(context.currDocument.getEsfName(),fieldName);
	}
	public void setFieldValue(EsfName fieldName, EsfValue value, boolean canClone)
	{
		context.currTransactionDocument.getRecord().addUpdate( fieldName, value, canClone );
	}
	public void setFieldValue(String fieldName, EsfValue value, boolean canClone) // utility method for passing a string name
	{
		setFieldValue( new EsfName(fieldName), value, canClone);
	}
	public void setFieldValue(String fieldName, String value, boolean canClone) // utility method for passing a string name and value
	{
		setFieldValue( new EsfName(fieldName), new EsfString(value), canClone);
	}
	public void setFieldValue(EsfPathName fieldPathName, EsfValue value, boolean canClone)
	{
		context.currTransactionDocument.getRecord().addUpdate( fieldPathName, value, canClone );
	}
	public void setFieldValues(EsfName fieldName, EsfValue[] values, boolean canClone)
	{
		context.currTransactionDocument.getRecord().addUpdate( fieldName, values, canClone );
	}
	public void setFieldValues(EsfPathName fieldPathName, EsfValue[] values, boolean canClone)
	{
		context.currTransactionDocument.getRecord().addUpdate( fieldPathName, values, canClone );
	}
	
	public EsfValue getFieldValue(EsfName documentSource, EsfPathName fieldPathName)
	{
		EsfName docName = ( Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(documentSource.toPlainString()) ) ? context.currDocument.getEsfName() : documentSource;
		EsfValue value = context.transaction.getFieldValue(docName, fieldPathName);
		return value;
	}
	public EsfValue getFieldValue(EsfName documentSource, EsfName fieldName)
	{
		EsfName docName = ( Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(documentSource.toPlainString()) ) ? context.currDocument.getEsfName() : documentSource;
		EsfValue value = context.transaction.getFieldValue(docName, fieldName);
		return value;
	}
	public EsfValue getFieldValue(EsfName documentSource, EsfName fieldName, String fieldSpec)
	{
		EsfName docName = ( Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(documentSource.toPlainString()) ) ? context.currDocument.getEsfName() : documentSource;
		EsfValue value = context.transaction.getFieldValue(docName, fieldName);
		return ( value == null ) ? new EsfString(fieldSpec) : value;
	}
	public EsfValue[] getFieldValues(EsfName documentSource, EsfName fieldName)
	{
		EsfName docName = ( Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(documentSource.toPlainString()) ) ? context.currDocument.getEsfName() : documentSource;
		EsfValue[] values = context.transaction.getFieldValues(docName, fieldName);
		return values;
	}
	public EsfValue[] getFieldValues(EsfName documentSource, EsfName fieldName, String fieldSpec)
	{
		EsfName docName = ( Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(documentSource.toPlainString()) ) ? context.currDocument.getEsfName() : documentSource;
		EsfValue[] values = context.transaction.getFieldValues(docName, fieldName);
		if ( values == null )
		{
			values = new EsfString[1];
			values[0] = new EsfString(fieldSpec);
		}
		return values;
	}
	public String getFieldSpecWithSubstitutions(String fieldSpecToExpand)
	{
		return context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, fieldSpecToExpand);
	}
	public String getUrlEncodedFieldSpecWithSubstitutions(String fieldSpecToExpand)
	{
		return context.transaction.getUrlEncodedFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, fieldSpecToExpand);
	}
	
	public FieldTemplate getFieldTemplate(EsfName documentSource, EsfName fieldName, String fieldSpec)
	{
		DocumentVersion docVer = findDocumentVersion(documentSource);
		if ( docVer != null )
		{
			//PartyTemplate partyTemplate = docVer.getPartyTemplate(context.pickupPartyName);
			return docVer.getFieldTemplate(fieldName);
		}
		
		return null;
	}
	
	public final static int INDEX_FIELD_TEMPLATE_LABEL = 0;
	public final static int INDEX_FIELD_TEMPLATE_CLASS = 1;
	public String[] getFieldTemplateLabelAndClass(EsfName documentSource, EsfName fieldName, String fieldSpec)
	{
		String[] labelAndClass = new String[2];
		
		labelAndClass[INDEX_FIELD_TEMPLATE_LABEL] = fieldSpec;
		labelAndClass[INDEX_FIELD_TEMPLATE_CLASS] = "normal-error";
		
		DocumentVersion docVer = findDocumentVersion(documentSource);
		if ( docVer != null )
		{
			FieldTemplate fieldTemplate = docVer.getFieldTemplate(fieldName);
			if ( fieldTemplate != null ) {
				LabelTemplate labelTemplate = fieldTemplate.getLabelTemplate();
				if ( labelTemplate.isSuppressNonInput() && ! isInputMode(fieldTemplate) )
				{
					labelAndClass[INDEX_FIELD_TEMPLATE_LABEL] = "";
					labelAndClass[INDEX_FIELD_TEMPLATE_CLASS] = labelTemplate.getCss(errors.isErrorOrWarningField(fieldName.toPlainString()));
				}
				else
				{
					labelAndClass[INDEX_FIELD_TEMPLATE_LABEL] = labelTemplate.getLabelAndSeparatorForHtml();
					labelAndClass[INDEX_FIELD_TEMPLATE_CLASS] = labelTemplate.getCss(errors.isErrorOrWarningField(fieldName.toPlainString()));
				}
			}
		}
		
		return labelAndClass;
	}
	
	protected DocumentVersion findDocumentVersion(EsfName documentName)
	{
		if ( Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(documentName.toPlainString()) )
			return context.currDocumentVersion;
		return context.transaction.getDocumentVersion( context.getDocument(documentName) );
	}
	
	/**
	 * Gets the most appropriate ButtonMessageVersion using this algorithm based on the button message set configured on the transaction's package and retrieve it's name.
	 * 
	 * We check first in the transaction's branding library, then the package's specifically defined one, then finally the template library's default set if nothing is found (odd).
	 * 
	 * @return the ButtonMessageVersion most appropriate based on the current setup
	 */
	public ButtonMessageVersion getButtonMessageVersion()
	{
		if ( buttonMessageVersion != null )
			return buttonMessageVersion;
		
		if ( context.hasTransaction() )
		{
			EsfUUID configuredButtonMessageId = context.pkgButtonMessage == null ? context.transaction.getPackageVersion().getButtonMessageId() : context.pkgButtonMessage.getId(); // may be from package version or override in package party
			ButtonMessage configuredButtonMessage = ButtonMessage.Manager.getById(configuredButtonMessageId);
			if ( configuredButtonMessage == null )
			{
				err("getButtonMessageVersion() - Transaction id: " + context.transactionId + "; package version id: " + context.transaction.getPackageVersionId() + 
						"; package version party: " + context.pkgVersionPartyTemplate.getEsfName() +
						"; specifies button message id: " + configuredButtonMessageId + "; but no such button message was found.");
				context.transaction.logGeneral("ERROR: Could not find the button message set configured in the package/pkgParty with id: " + configuredButtonMessageId);
			}
			else
			{
				// Let's see if we can find this button message in the transaction's branding library
				if ( context.transaction.hasBrandLibraryId() ) 
				{
					ButtonMessage targetButtonMessage = ButtonMessage.Manager.getByName(context.transaction.getBrandLibrary(), configuredButtonMessage.getEsfName());
					if ( targetButtonMessage != null )
					{
						buttonMessageVersion = context.transaction.doProductionResolve() ? targetButtonMessage.getProductionButtonMessageVersion() : targetButtonMessage.getTestButtonMessageVersion();
						if ( buttonMessageVersion != null )
							return buttonMessageVersion;
						context.transaction.logGeneral("WARNING: The transaction's branding library has a button message set named: " + configuredButtonMessage.getEsfName() + "; but there's no production version.");
					}
				}
				
				// Let's use the one configured in the package/pkgParty then
				buttonMessageVersion = context.transaction.doProductionResolve() ? configuredButtonMessage.getProductionButtonMessageVersion() : configuredButtonMessage.getTestButtonMessageVersion();
				if ( buttonMessageVersion != null )
					return buttonMessageVersion;
				context.transaction.logGeneral("WARNING: The transaction's package's button message set: " + configuredButtonMessage.getEsfName() + "; has no production version. Using the system default.");
			}
		}

		// Just return the system default production-level button message set, and if any reason it's not there, return the test version.
		ButtonMessage buttonMessage = ButtonMessage.Manager.getDefault();
		return buttonMessage.hasProductionVersion() ? buttonMessage.getProductionButtonMessageVersion() : buttonMessage.getTestButtonMessageVersion();
	}
	
	public String getExpandedPropertyValue(EsfName propertySetName, EsfPathName propertyName)
	{
		EsfValue value = getPropertyValue(propertySetName, propertyName);
		if ( value == null )
			return null;
		
		if ( value instanceof EsfString && context.hasTransaction() )
			return context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, value.toString());
		
		return value.toString();
	}
	
	/**
	 * Try to find the named propertyset.
	 * First check the transaction's branding library.
	 * Second check the current document's library.
	 * Third check the template library.
	 * @param propertySetName the EsfName propertyset to be found
	 * @return the PropertySetVersion found, or null if not found
	 */
	public PropertySetVersion getPropertySetVersionByName(EsfName propertySetName)
	{
		List<EsfUUID> searchedLibraryIds = new LinkedList<EsfUUID>();
		
		if ( context.hasUser() )
		{
			PropertySet propertySet = PropertySet.Manager.getByName(context.user.getId(), propertySetName);
			if ( propertySet != null )
				return context.transaction.doProductionResolve() ? propertySet.getProductionPropertySetVersion() : propertySet.getTestPropertySetVersion();
		}
		
		if ( context.hasTransaction() )
		{
			if ( context.transaction.hasBrandLibraryId() )
			{
				PropertySet propertySet = PropertySet.Manager.getByName(context.transaction.getBrandLibraryId(), propertySetName);
				if ( propertySet != null )
					return context.transaction.doProductionResolve() ? propertySet.getProductionPropertySetVersion() : propertySet.getTestPropertySetVersion();
				searchedLibraryIds.add(context.transaction.getBrandLibraryId());
			}
			
			if ( context.currDocumentVersion != null && ! searchedLibraryIds.contains(context.currDocument.getLibraryId()) )
			{
				PropertySet propertySet = PropertySet.Manager.getByName(context.currDocument.getLibraryId(), propertySetName);
				if ( propertySet != null )
					return context.transaction.doProductionResolve() ? propertySet.getProductionPropertySetVersion() : propertySet.getTestPropertySetVersion();
				searchedLibraryIds.add(context.currDocument.getLibraryId());
			}
		}
		
		Library templateLibrary = Library.Manager.getTemplate();
		if ( ! searchedLibraryIds.contains(templateLibrary.getId()) )
		{
			boolean doProductionResolve = context.hasTransaction() ? context.transaction.doProductionResolve() : false;
			PropertySet propertySet = PropertySet.Manager.getByName(templateLibrary.getId(), propertySetName);
			if ( propertySet != null )
				return doProductionResolve ? propertySet.getProductionPropertySetVersion() : propertySet.getTestPropertySetVersion();
		}
		
		warning("getPropertySetVersionByName() - Failed to find PropertySet; propertySetName: " + propertySetName);
		return null;
	}
	
	/**
	 * Finds the specified property name in the specified propertyset, if provided.
	 * @param propertySetName the name of the property set to look in; if null, all property sets are checked in alpha order
	 * @param propertyName the property name
	 * @return the EsfValue of the property (all properties initially are String types because the property GUI only supports them)
	 */
	public EsfValue getPropertyValue(EsfName propertySetName, EsfPathName propertyName)
	{
		// If we have a transaction, use it to find the property.
		if ( context.hasTransaction() )
			return context.transaction.getPropertyValue(propertySetName, propertyName, context.user, context.currDocumentVersion);

		if ( context.hasUser() )
		{
			EsfValue value = context.user.getPropertyValue(propertySetName, propertyName, false, false);
			if ( value != null )
				return value;
		}
		
		// Otherwise, if we have a document version, check if the property is there
		List<Library> searchedLibraries = new LinkedList<Library>();
		if ( context.currLibrary != null )
		{
			
			EsfValue value = context.currLibrary.getPropertyValue(propertySetName, propertyName, false, false);
			if ( value != null )
				return value;
			searchedLibraries.add(context.currLibrary);
		}
		
		// Lastly, check the template library, and we assume Test allowed when there's no transaction at all
		Library templateLibrary = Library.Manager.getTemplate();
		return searchedLibraries.contains(templateLibrary) ? null : templateLibrary.getPropertyValue(propertySetName, propertyName, false, false);
	}
	
	public void appendCapturedHtml(String v)
	{
		if ( isPageReview() )
		{
			if ( capturedHtml == null )
			{
				int initialBufferSize = Math.max(v.length(), 32 * Literals.KB); // Minimum of 32KB
				capturedHtml = new StringBuilder(initialBufferSize);
				capturedHtml.append(Version.getDocTypeXhtml10Transitional()).append('\n');
			}
			capturedHtml.append(v);
			context.capturedHtml = getCapturedHtml();
		}
	}
	
	public String getCapturedHtml()
	{
		if ( capturedHtml == null )
			return "";
		
    	// Now we strip all areas between <!-- ESF_BEGIN_REMOVE --> and <!-- ESF_END_REMOVE -->
    	StringBuilder buf = new StringBuilder(capturedHtml.length());
    	int beginIndex = 0;
    	int endIndex = capturedHtml.indexOf(Literals.JSP_BEGIN_REMOVE_COMMENT); // find the next ESF_BEGIN_REMOVE comment
    	while( true )
    	{
    		buf.append(capturedHtml.substring(beginIndex, endIndex)); // get's everything up to the ESF_BEGIN_REMOVE comment
    		beginIndex = endIndex + Literals.JSP_BEGIN_REMOVE_COMMENT.length(); // skips the ESF_BEGIN_REMOVE comment itself
    		endIndex = capturedHtml.indexOf(Literals.JSP_END_REMOVE_COMMENT, beginIndex); // find where the ESF_END_REMOVE is
    		//buf.append(capturedHtml.substring(beginIndex, endIndex)); -- Don't do this as this would append the contents between those tags. 
    		beginIndex = endIndex + Literals.JSP_END_REMOVE_COMMENT.length(); // skips the ESF_END_REMOVE comment itself
    		endIndex = capturedHtml.indexOf(Literals.JSP_BEGIN_REMOVE_COMMENT, beginIndex); // find the next ESF_BEGIN_REMOVE comment
    		if ( endIndex < 0 )
    		{
    			buf.append(capturedHtml.substring(beginIndex)); // if not more, just append everything that remains
    			break;
    		}
    	}
    	
    	return buf.toString();
	}
	
	/**
	 * API routine for processing a given party automatically. 
	 * @param pickupCode the String pickupCode for the party to process
	 * @return true if the party is processed until completed; false otherwise (errors object should contain details)
	 * @throws java.io.IOException
	 */
	public boolean processPartyAutomatically(String pickupCode)
		throws java.io.IOException
	{
    	// Let's pick up the transaction using the initiators pickup code.
    	if ( ! pickupTransaction(pickupCode) || context.transaction.isSuspended() )
    		return false;
    	
		context.setApiMode(true);
		
    	// For the case where the party is already completed
    	if ( context.transactionParty.isCompleted() )
    		return true;

	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	
	    try
		{
	    	boolean partyProcessed = true;
	    	
	        TransactionEngine engine = new TransactionEngine(context);
	        context.transaction.logGeneral(con, "Processing party " + context.pickupPartyName + " automatically; API IP: " + getIP());
	        
	        while( ! context.transactionParty.isEndState() )
	        {
		    	errors.clear(); // For the API, we want to clear any prior messages from previous documents since we do stop if we get an error
		    	
	        	// Any state other than active means something is not right
	        	if ( ! context.transactionParty.isActive() )
				{
	    	        context.transaction.logGeneral(con, "Automatic processing party " + context.pickupPartyName + " is not active. Current status: " + context.transactionParty.getStatus() + "; API IP: " + getIP());
					partyProcessed = false;
					break;
				}
	        	
	        	// If we're on the package document, let's process like a Continue button
	        	if ( context.isPackageDocument() )
	        	{
	        		processPackageButtonContinue(con,engine);
    				if ( errors.hasError() )
    				{
    	            	context.transaction.logGeneral(con, "Automatic processing party " + context.pickupPartyName + " failed on the package+disclosure page: " + errors.toString());
    					partyProcessed = false;
    	            	break;
    				}
	        	}
	        	else
	        	{
	    			if ( context.currTransactionPartyDocument.isNetYetRetrieved() )
	    			{
	    				engine.queuePartyRetrievedDocumentEvent(context.pickupPartyName, context.currDocument.getEsfName(), context.currTransactionPartyDocument);
	    				engine.doWork(con,errors); // do this now since it may change document data or the like
	    				if ( errors.hasError() )
	    	            	context.transaction.logGeneral(con, "Automatic processing party " + context.pickupPartyName + " retrieved document doWork error: " + errors.toString());
	    			}
	    			
	    			// Ignore ViewOptionalPartyViewed if for some reason set up for APIs as there's no need to change status
	        		
	    			// If this document needs work, let's review and complete
	    			if ( context.currTransactionPartyDocument.isWorkNeeded() )
	    			{
	    				processButtonReview(con,engine);
	    				if ( ! isPageReview() ) // If we don't end up with page review, we assume it failed somehow
	    				{
	    	    	        context.transaction.logGeneral(con, "Automatic processing party " + context.pickupPartyName + " failed REVIEW on document: " + context.currDocument.getEsfName() + "; API IP: " + getIP());
	    	    	        context.transaction.logActionBasic(con, "API Review Error: " + errors.toString());
	    					partyProcessed = false;
	    					break;
	    				}
	    				// Let's do a pseudo pickup so we can have the captureHtml done
	    				getApiReviewPageForHtmlSnapshot(pickupCode);
	    				
	    				processButtonComplete(con,engine);
	    				if ( errors.hasError() )
	    				{
	    	    	        context.transaction.logGeneral(con, "Automatic processing party " + context.pickupPartyName + " failed COMPLETE on document: " + context.currDocument.getEsfName() + "; API IP: " + getIP());
	    	    	        context.transaction.logActionBasic(con, "API Complete Error: " + errors.toString());
	    					partyProcessed = false;
	    					break;
	    				}
	    			}
	        	} // if a regular document
	        	
	        } // while the tran party is not completed
	        
	        // Process any queued up work that may have occurred
        	engine.doWork(con,errors);
			if ( errors.hasError() )
            	context.transaction.logGeneral(con, "Process party automatically processed: " + partyProcessed + "; final doWork error: " + errors.toString());
        	context.transaction.save(con,context.user);
	        con.commit();
	        return partyProcessed;
		}
        catch(SQLException e) 
        {
        	sqlerr(e,"processPartyAutomatically()");
            pool.rollbackIgnoreException(con,e);
            return false;
        }
		catch( SendRedirectException e )
		{
			if ( e.isError() )
			{
				err("processPartyAutomatically() - Rolled back DB changes because of redirect exception to URL: " + e.getUrl());
				pool.rollbackIgnoreException(con); // Not yet sure why we'd redirect at all, so let's watch this and see how it plays out when we're running
			}
			else
			{
				debug("processPartyAutomatically() - Committing DB changes along with redirect exception to URL: " + e.getUrl());
				try { con.commit(); } catch( Exception e2 ) {}
			}
			// of course we don't actually do any redirects since this is automated running of the document
			return false;
		}
        catch(EsfException e) 
        {
        	except(e,"processPartyAutomatically()");
            pool.rollbackIgnoreException(con);
            return false;
        }
		finally
		{
        	Application.getInstance().cleanupPool(pool,con,null);
		}
	}
	
	void getApiReviewPageForHtmlSnapshot(String pickupCode)
    {
		String sessionPickupCodeKey = app.getRandomKey().getLoginAuthorizationString();
		
    	String pickupUrl = getExternalUrlContextPath() + "/P/" + pickupCode + ";jsessionid=" + session.getId();
    	pickupUrl = ServletUtil.appendUrlParam(pickupUrl, ESFAPI_PAGE_REVIEW_SESSION_PICKUPCODE_KEY_PARAM_NAME, sessionPickupCodeKey);

    	java.net.HttpURLConnection con = null;
        java.io.BufferedReader     br  = null;
        
        try
        {
        	setSessionAttribute(sessionPickupCodeKey, pickupCode);
        	
            URL url = new URL(pickupUrl);
            
            // Create the connection and set the connection properties
            con = (java.net.HttpURLConnection)url.openConnection();
            con.setRequestMethod("GET");
            con.setAllowUserInteraction(false);  // this is program, so no popups
            con.setDoInput(true);      // we will read from the URL
            con.setDoOutput(false);     // we will not write to the URL
            con.setIfModifiedSince(0); // always get
            con.setUseCaches(false);   // don't use cache
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Accept", "*/*"); 
            con.connect();
            
            java.io.InputStream is = con.getInputStream();
            if ( is == null )
            {
            	err("getApiReviewPageForHtmlSnapshot() - Could not retrieve review page using URL: " + pickupUrl);
                return;
            }
                
            br = new java.io.BufferedReader( new java.io.InputStreamReader(con.getInputStream()));
            while ( br.readLine() != null )
            {
            	//ignore the response
            }
        }
        catch( java.io.IOException e )
        {
        	except(e,"getApiReviewPageForHtmlSnapshot() - Could not retrieve page using URL: " + pickupUrl);
        }
        finally
        {
        	removeSessionAttribute(sessionPickupCodeKey); // Remove the session hook to our pickup code to put in API review mode
            if ( br != null )
                try { br.close(); } catch( Exception e2 ) {}
            if ( con != null )
                con.disconnect();
        }
    }

	
	//************************  Code for formatting HTML to display for fields **********************************
	
	public String buildFieldLabelHtml(JspContext jspContext, EsfName docName, EsfName fieldName, String fieldSpec, boolean isOverlayField)
	{
		FieldTemplate fieldTemplate = getFieldTemplate(docName,fieldName,fieldSpec);
		
        if ( fieldTemplate == null )
        	return "<span class=\"viewFieldData-error\">"+fieldSpec+"</span>"; // no field found

        if ( fieldTemplate.isTypeRadioButtonGroup() )
        	return ""; // the group has no visible input field, only its radio buttons do
 
    	LabelTemplate labelTemplate = fieldTemplate.getLabelTemplate();

    	EsfValue fieldValue = getFieldValue(docName,fieldName,fieldSpec);
        
        String fieldId = fieldTemplate.getId().toNormalizedEsfNameString();
        String[] labelAndClass = getFieldTemplateLabelAndClass(docName,fieldName,fieldSpec);
        
        boolean isInputMode = isInputMode(fieldTemplate);
        
        // For stacked, we put the width on the labelFieldAreaStacked DIV, otherwise on the fieldArea DIV of the labelFieldAreaSameLine DIV.
        String labelFieldAreaStyle, fieldAreaStyle;
    	String regularOrStackedCssSuffix = labelTemplate.isPositionStacked() ? "Stacked" : "";
        if ( labelTemplate.isPositionLeft() || labelTemplate.isPositionRight() ) {
        	String fieldStyle = "style=\"" + ((isInputMode) ? fieldTemplate.getWidthStyle() : fieldTemplate.getWidthStyleOut()) + "\"";
        	labelFieldAreaStyle = fieldTemplate.isWidthUnitPercent() ? fieldStyle : "";
        	fieldAreaStyle = fieldTemplate.isWidthUnitPercent() ? "" : fieldStyle;
        } else { 
        	labelFieldAreaStyle = "style=\"" + ((isInputMode) ? fieldTemplate.getWidthStyle() : fieldTemplate.getWidthStyleOut()) + "\"";
        	fieldAreaStyle = "";
        }
        
        String labelAreaClass;
        if ( labelTemplate.isPositionTopRight() || labelTemplate.isPositionBottomRight() )
        	labelAreaClass = "labelAreaRight"+regularOrStackedCssSuffix;
        else if ( labelTemplate.isPositionTopCenter() || labelTemplate.isPositionBottomCenter() )
        	labelAreaClass = "labelAreaCenter"+regularOrStackedCssSuffix;
        else
        	labelAreaClass = "labelArea"+regularOrStackedCssSuffix;

        String fieldAreaClass = "bottom fieldArea";
        if ( fieldTemplate.isAlignCenter() )
        	fieldAreaClass += "Center" + regularOrStackedCssSuffix;
        else if ( fieldTemplate.isAlignRight() )
        	fieldAreaClass += "Right" + regularOrStackedCssSuffix;
        else if ( fieldTemplate.isAlignLeft() )
        	fieldAreaClass += "Left" + regularOrStackedCssSuffix;
        
        // For auto width fields, we use auto sized field areas too
        if ( fieldTemplate.isWidthUnitAuto() )
        	fieldAreaClass += "Auto";

        String fieldBorderCssClass = fieldTemplate.getBorderCssClass();
        String fieldHtml;
        String labelHtml;
		String titleAttr = fieldTemplate.hasTooltip() ? " title=\""+HtmlUtil.toEscapedHtml(fieldTemplate.getTooltip())+"\"" : "";

        if ( isInputMode )
        {
    		String placeholderAttr = fieldTemplate.hasInputPrompt() ? " placeholder=\""+HtmlUtil.toDisplayHtml(fieldTemplate.getInputPrompt())+"\"" : "";

    		String autoCompleteAttr = fieldTemplate.isAutoCompleteNotAllowed() || 
										fieldTemplate.isTypeCheckbox() || 
										fieldTemplate.isTypeRadioButton() ? " autocomplete=\"off\"" : "";

    		String inputTypeAttr = fieldTemplate.isMaskInput() ? " type=\"password\"" : " type=\"text\"";

    		String inputClass;
        	if ( errors.isErrorOrWarningField(fieldName.toPlainString()) )
        		inputClass = "error";
        	else
        		inputClass = isRequired(fieldTemplate) || isOptionalStyleRequired(fieldTemplate) ? "required" : "optional";
        	
        	if ( fieldTemplate.isTypeGeneral() )
        	{
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\" maxlength=\""+fieldTemplate.getMaxLength()+"\""+titleAttr+placeholderAttr+autoCompleteAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeCheckbox() )
        	{
        		String checkValue = fieldTemplate.hasInputFormatSpec() ? fieldTemplate.getInputFormatSpec() : "Y";
    			String checkedAttribute = checkValue.equals(fieldValue.toPlainString()) ? " checked=\"checked\"" : "";
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\" type=\"checkbox\" name=\""+fieldName.toLowerCase()+"\" value=\""+HtmlUtil.toDisplayHtml(checkValue)+"\" class=\""+inputClass+"\""+titleAttr+autoCompleteAttr+checkedAttribute+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeCreditCard() )
        	{
        		String sizeAttr = fieldTemplate.isWidthUnitAuto() ? " size=\"19\"" : ""; // 16 digits plus 3 hyphens
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\" maxlength=\"19\""+titleAttr+placeholderAttr+autoCompleteAttr+sizeAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeDate() )
        	{
        		String dateInput = FieldLabel.createDateHtml(jspContext,this,fieldTemplate,fieldId,fieldName,inputClass,titleAttr,placeholderAttr,autoCompleteAttr,fieldValue);
        		fieldHtml = "<span class=\"nowrap "+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+titleAttr+">"+dateInput+"</span>";
        	}
        	else if ( fieldTemplate.isTypeDateTime() )
        	{
        		String dateTimeInput = FieldLabel.createDateTimeHtml(jspContext,this,fieldTemplate,fieldId,fieldName,inputClass,titleAttr,placeholderAttr,autoCompleteAttr,null,fieldValue);
        		fieldHtml = "<span class=\"nowrap "+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+titleAttr+">"+dateTimeInput+"</span>";
        	}
        	else if ( fieldTemplate.isTypeDecimal() )
        	{
        		String fieldValueHtml;
        		if ( fieldValue instanceof EsfDecimal ) 
        			fieldValueHtml = ((EsfDecimal)fieldValue).formatToHtml(fieldTemplate.getOutputFormatSpec());
        		else
        			fieldValueHtml = fieldValue.toHtml();
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValueHtml+"\" class=\""+inputClass+"\" maxlength=\"20\""+titleAttr+placeholderAttr+autoCompleteAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeEmailAddress() )
        	{
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\" maxlength=\"200\""+titleAttr+placeholderAttr+autoCompleteAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeFile() )
        	{
        		if ( context.currTransactionDocument != null && context.currDocument != null )
        		{
            		List<TransactionFile> fileList = context.transaction.getAllTransactionFilesForDocumentField(context.currTransactionDocument.getId(), fieldTemplate.getEsfName());
            		String removableFileList = FieldLabel.createRemovableFileList(this,fieldId,fieldName,fieldTemplate,fileList);
            		
            		String buttonSuffix = context.currDocument.getId().toNormalizedEsfNameString();

            		// Don't fight trying to style the size...browsers just do as they please with file uploads (Chrome even puts the select button inside the field and doesn't display the file path like IE and FF do)
            		// We also include an anchor tag so that after an upload, we can jump to this location.
            		int limitNumFiles = app.stringToInt(fieldTemplate.getInputFormatSpec(), -1);
            		if ( limitNumFiles > 0 && fileList.size() >= limitNumFiles )
            		{
                		fieldHtml = "<a name=\""+fieldId+"\"></a><span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+">" + 
    					removableFileList + 
    		            "</span>";
            		}
            		else
            		{
                		fieldHtml = "<a name=\""+fieldId+"\"></a><span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+">" + 
    					removableFileList + 
    		            "<input id=\""+fieldId+"\" type=\"file\" name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\""+titleAttr+autoCompleteAttr+"/> " +
    		            "<input id=\""+fieldId+"Upload\" type=\"submit\" class=\"upload\" name=\""+DocumentPageBean.DOCUMENT_UPLOAD_BUTTON_PREFIX+fieldName.toLowerCase()+buttonSuffix+"\" value=\"Upload\"" + titleAttr + " onclick=\"esf.checkConfirmOnPageUnload(false)\" />" +
    		            "</span>";
            		}
        		}
        		else
        		{
        			fieldHtml = "";
        		}
        	}
        	else if ( fieldTemplate.isTypeFileConfirmClick() )
        	{
        		// If the value is set to an EsfName, we use that name for the file. Otherwise we'll default to how it was originally configured.
        		EsfName fileName = (fieldValue instanceof EsfName) ? (EsfName)fieldValue : new EsfName(fieldTemplate.getInputFormatSpec());
        		String fileInfoLink = FieldLabel.createFileConfirmClickHtml(this,fieldId,fieldName,fileName,fieldTemplate,isInputMode);        		
         		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+">" + fileInfoLink + "</span>";
        	}
        	else if ( fieldTemplate.isTypeInteger() )
        	{
        		String fieldValueHtml;
        		if ( fieldValue instanceof EsfInteger )
        			fieldValueHtml = ((EsfInteger)fieldValue).formatToHtml(fieldTemplate.getOutputFormatSpec());
        		else
        			fieldValueHtml = fieldValue.toHtml();
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValueHtml+"\" class=\""+inputClass+"\" maxlength=\"20\""+titleAttr+placeholderAttr+autoCompleteAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeMoney() )
        	{
        		String fieldValueHtml;
        		if ( fieldValue instanceof EsfMoney )
        			fieldValueHtml = ((EsfMoney)fieldValue).formatToHtml(fieldTemplate.getOutputFormatSpec());
        		else
        			fieldValueHtml = fieldValue.toHtml();
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValueHtml+"\" class=\""+inputClass+"\" maxlength=\"20\""+titleAttr+placeholderAttr+autoCompleteAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypePhone() )
        	{
        		String sizeAttr = "";
        		if ( fieldTemplate.isWidthUnitAuto() ) {
        			if ( fieldTemplate.getOutputFormatSpec().equals("standard") )
        				sizeAttr = " size=\"16\""; // 1 (425) 555-0100
        			else
        				sizeAttr = " size=\"14\""; // 1-425-555-0100 (or separator can be dot or space)
        		}
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\" maxlength=\"30\""+titleAttr+placeholderAttr+autoCompleteAttr+sizeAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeRadioButton() )
        	{
        		// Output format is the name of the radio button group and field we actually set
        		String radioButtonName = fieldTemplate.getOutputFormatSpec();
        		EsfValue radioFieldValue = getFieldValue(docName,new EsfName(radioButtonName),fieldSpec);
        		// Input format, if specified, is the value of this radio button, else it's its name
        		String checkValue = EsfString.isBlank(fieldTemplate.getInputFormatSpec()) ? fieldTemplate.getEsfName().toPlainString() : fieldTemplate.getInputFormatSpec();
    			String checkedAttribute = checkValue.equals(radioFieldValue.toPlainString()) ? " checked=\"checked\"" : "";
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\" type=\"radio\" name=\""+radioButtonName.toLowerCase()+"\" value=\""+HtmlUtil.toDisplayHtml(checkValue)+"\" class=\""+inputClass+"\""+titleAttr+autoCompleteAttr+checkedAttribute+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeRichTextarea() )
        	{
        		String textareaInput = FieldLabel.createRichTextareaHtml(jspContext,this,fieldTemplate,fieldId,fieldName,inputClass,titleAttr,autoCompleteAttr,fieldValue);
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+titleAttr+">"+textareaInput+"</span>";
        	}
        	else if ( fieldTemplate.isTypeSelection() )
        	{
        		String selectHtml;
        		DropDownVersion ddv;
        		// If the drop down has extra options, let's see if we can find the drop down using the name specified. If not, we'll just stick with the hard-configured one.
        		if ( fieldTemplate.hasExtraOptions() )
        		{
        			String ddName = getFieldSpecWithSubstitutions(fieldTemplate.getExtraOptions());
        			ddv = context.transaction.getDropDownVersion(new EsfName(ddName),context.currDocumentVersion);
            		if ( ddv == null )
            		{
    					warning("buildFieldLabelHtml() - Could not find dynamic dropdown box for field: " + fieldTemplate.getEsfName() + 
    							"; Dynamic DropDown name spec: " + fieldTemplate.getExtraOptions() + "; expanded to drop down name: " + ddName +
    							"; tranId: " + context.transactionId + "; packageId: " + context.pkg.getId() + "; packageParty: " + context.pkgVersionPartyTemplate.getEsfName() + 
    							"; docId: " + context.currDocument.getId() + "; docParties: " + context.currDocumentVersionPartyNameList);
            			ddv = context.transaction.getDropDownVersion(new EsfName(fieldTemplate.getInputFormatSpec()),context.currDocumentVersion);
            		}
        		}
        		else
        			ddv = context.transaction.getDropDownVersion(new EsfName(fieldTemplate.getInputFormatSpec()),context.currDocumentVersion);
        		if ( ddv == null )
        		{
					warning("buildFieldLabelHtml() - Unexpectedly missing dropdown box for field: " + fieldTemplate.getEsfName() + 
							"; DropDown: " + fieldTemplate.getInputFormatSpec() +
							"; tranId: " + context.transactionId + "; packageId: " + context.pkg.getId() + "; packageParty: " + context.pkgVersionPartyTemplate.getEsfName() + 
							"; docId: " + context.currDocument.getId() + "; docParties: " + context.currDocumentVersionPartyNameList);
					selectHtml = "<input id=\""+fieldId+"\" type=\"text\" name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\""+titleAttr+autoCompleteAttr+"/>";
        		}
        		else
        		{
        			if ( ddv.isAllowMultiSelection() )
        			{
        				EsfValue[] fieldValues = getFieldValues(docName,fieldName,fieldSpec);
        				selectHtml = FieldLabel.createMultiSelectHtml(ddv,fieldId,fieldName,inputClass,titleAttr,autoCompleteAttr,fieldTemplate.isAutoPostAllowed(),fieldValues);
        			}
        			else
        				selectHtml = FieldLabel.createSelectHtml(ddv,fieldId,fieldName,inputClass,titleAttr,autoCompleteAttr,fieldTemplate.isAutoPostAllowed(),fieldValue);
        		}
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+">" + selectHtml + "</span>";
        	}
        	else if ( fieldTemplate.isTypeSignature() )
        	{
        		fieldHtml = "<span class=\"signature "+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\" type=\"text\" name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\" maxlength=\""+fieldTemplate.getMaxLength()+"\""+titleAttr+placeholderAttr+autoCompleteAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeSignDate() )
        	{
        		fieldHtml = "<span class=\"signature "+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+titleAttr+">&nbsp;</span>";
        	}
        	else if ( fieldTemplate.isTypeSsnEin() )
        	{
        		String sizeAttr = fieldTemplate.isWidthUnitAuto() ? " size=\"11\"" : "";
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\" maxlength=\"11\""+titleAttr+placeholderAttr+autoCompleteAttr+sizeAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeTextarea() )
        	{
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><textarea id=\""+fieldId+"\" name=\""+fieldName.toLowerCase()+"\" class=\""+inputClass+"\" maxlength=\""+fieldTemplate.getMaxLength()+"\""+titleAttr+placeholderAttr+autoCompleteAttr+" rows=\""+fieldTemplate.getInputFormatSpec()+"\">"+fieldValue.toHtml()+"</textarea></span>";
        	}
        	else if ( fieldTemplate.isTypeZipCode() )
        	{
        		String sizeAttr = "";
        		if ( fieldTemplate.isWidthUnitAuto() ) {
        			if ( fieldTemplate.getInputFormatSpec().equals("5") )
        				sizeAttr = " size=\"5\"";
        			else
        				sizeAttr = " size=\"10\"";
        		}
        		String maxlengthAttr;
    			if ( fieldTemplate.getInputFormatSpec().equals("5") )
    				maxlengthAttr = " maxlength=\"5\"";
    			else
    				maxlengthAttr = " maxlength=\"10\"";
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\""+titleAttr+placeholderAttr+autoCompleteAttr+sizeAttr+maxlengthAttr+"/></span>";
        	}
        	else
        	{
        		fieldHtml = fieldValue.toHtml();
        		if ( EsfString.isBlank(fieldHtml) )
        			fieldHtml = "&nbsp;";
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldHtml+"\" class=\""+inputClass+"\""+titleAttr+placeholderAttr+autoCompleteAttr+" /></span>";
        	}
        	
        	labelHtml = "<span class=\""+labelAreaClass+"\"><label for=\""+fieldId+"\" class=\""+labelAndClass[DocumentPageBean.INDEX_FIELD_TEMPLATE_CLASS]+"\">"+labelAndClass[DocumentPageBean.INDEX_FIELD_TEMPLATE_LABEL]+"</label></span>";
        }
        else // display mode
        {
        	fieldHtml = FieldLabel.formatDisplayField(this, docName, fieldName, fieldSpec, docName.toString(), fieldId, fieldTemplate, isOverlayField, fieldValue);
        	
    		if ( fieldTemplate.isTypeSignature() )
    		{
    			fieldAreaClass += " signature";
    		}
    		else if ( fieldTemplate.isTypeSignDate() )
    		{
    			fieldAreaClass += " signature";
    		} 
    		
        	fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+titleAttr+">" + fieldHtml + "</span>";
        	labelHtml = "<span class=\""+labelAreaClass+"\"><label class=\""+labelAndClass[DocumentPageBean.INDEX_FIELD_TEMPLATE_CLASS]+"\">"+labelAndClass[DocumentPageBean.INDEX_FIELD_TEMPLATE_LABEL]+"</label></span>";
        }
        
        String labelFieldAreaClass = "labelFieldArea";
    	if ( labelTemplate.isPositionBottom() )
    		labelFieldAreaClass += " top";
    	else
    		labelFieldAreaClass += " bottom";
    	
    	StringBuilder htmlBuf = new StringBuilder(100+fieldHtml.length()+labelHtml.length());
    	htmlBuf.append("<span class=\"").append(labelFieldAreaClass).append("\" ").append(labelFieldAreaStyle).append(">");
        
        if ( labelTemplate.isPositionNoAutoShow() || (labelTemplate.isSuppressNonInput() && ! isInputMode ) )
    	{
        	htmlBuf.append(fieldHtml);
    	}
    	else if ( labelTemplate.isPositionRight() || labelTemplate.isPositionBottomLeft() || labelTemplate.isPositionBottomRight() || labelTemplate.isPositionBottomCenter() )
    	{
    		htmlBuf.append(fieldHtml).append(labelHtml);
    	}
    	else
    	{            
    		htmlBuf.append(labelHtml).append(fieldHtml);
    	}
        htmlBuf.append("</span>");
        
        return htmlBuf.toString();
	}
	
	
	public String buildFieldHtml(JspContext jspContext, EsfName docName, EsfName fieldName, String fieldSpec, boolean isOverlayField)
	{
        FieldTemplate fieldTemplate = getFieldTemplate(docName,fieldName,fieldSpec);
    	
        if ( fieldTemplate == null )
        	return "<span class=\"viewFieldData-error\">"+fieldSpec+"</span>"; // no field found

        if ( fieldTemplate.isTypeRadioButtonGroup() )
        	return ""; // the group has no visible input field, only its radio buttons do


    	EsfValue fieldValue = getFieldValue(docName,fieldName,fieldSpec);
        
        String fieldId = fieldTemplate.getId().toNormalizedEsfNameString();
        
        boolean isInputMode = isInputMode(fieldTemplate);
        
        String fieldAreaStyle = "style=\"" + ((isInputMode) ? fieldTemplate.getWidthStyle() : fieldTemplate.getWidthStyleOut()) + "\"";
        
        String fieldAreaClass = "fieldArea";
        if ( fieldTemplate.isAlignCenter() )
        	fieldAreaClass += "Center";
        else if ( fieldTemplate.isAlignRight() )
        	fieldAreaClass += "Right";
        else if ( fieldTemplate.isAlignLeft() )
        	fieldAreaClass += "Left";
        
        // For auto width fields, we use auto sized field areas too
        if ( fieldTemplate.isWidthUnitAuto() )
        	fieldAreaClass += "Auto";

        String fieldHtml;
        String fieldBorderCssClass = fieldTemplate.getBorderCssClass();
		String titleAttr = fieldTemplate.hasTooltip() ? " title=\""+HtmlUtil.toDisplayHtml(fieldTemplate.getTooltip())+"\"" : "";

		if ( isInputMode )
        {
    		String placeholderAttr = fieldTemplate.hasInputPrompt() ? " placeholder=\""+HtmlUtil.toDisplayHtml(fieldTemplate.getInputPrompt())+"\"" : "";

    		String autoCompleteAttr = fieldTemplate.isAutoCompleteNotAllowed() || 
										fieldTemplate.isTypeCheckbox() || 
										fieldTemplate.isTypeRadioButton() ? " autocomplete=\"off\"" : "";

    		String inputTypeAttr = fieldTemplate.isMaskInput() ? " type=\"password\"" : " type=\"text\"";

    		String inputClass;
        	if ( errors.isErrorOrWarningField(fieldName.toPlainString()) )
        		inputClass = "error";
        	else
        		inputClass = isRequired(fieldTemplate) || isOptionalStyleRequired(fieldTemplate) ? "required" : "optional";
        	
        	if ( fieldTemplate.isTypeGeneral() )
        	{
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\" maxlength=\""+fieldTemplate.getMaxLength()+"\""+titleAttr+placeholderAttr+autoCompleteAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeCheckbox() )
        	{
        		String checkValue = fieldTemplate.hasInputFormatSpec() ? fieldTemplate.getInputFormatSpec() : "Y";
    			String checkedAttribute = checkValue.equals(fieldValue.toPlainString()) ? " checked=\"checked\"" : "";
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\" type=\"checkbox\" name=\""+fieldName.toLowerCase()+"\" value=\""+HtmlUtil.toDisplayHtml(checkValue)+"\" class=\""+inputClass+"\""+titleAttr+autoCompleteAttr+checkedAttribute+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeCreditCard() )
        	{
        		String sizeAttr = fieldTemplate.isWidthUnitAuto() ? " size=\"19\"" : ""; // 16 digits plus 3 hyphens
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\" maxlength=\"19\""+titleAttr+placeholderAttr+autoCompleteAttr+sizeAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeDate() )
        	{
        		String dateInput = FieldLabel.createDateHtml(jspContext,this,fieldTemplate,fieldId,fieldName,inputClass,titleAttr,placeholderAttr,autoCompleteAttr,fieldValue);
        		fieldHtml = "<span class=\"nowrap "+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+titleAttr+">"+dateInput+"</span>";
        	}
        	else if ( fieldTemplate.isTypeDateTime() ) // just a General field as of 6/11/2012
        	{
        		String dateTimeInput = FieldLabel.createDateTimeHtml(jspContext,this,fieldTemplate,fieldId,fieldName,inputClass,titleAttr,placeholderAttr,autoCompleteAttr,null,fieldValue);
        		fieldHtml = "<span class=\"nowrap "+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+titleAttr+">"+dateTimeInput+"</span>";
        	}
        	else if ( fieldTemplate.isTypeDecimal() )
        	{
        		String fieldValueHtml;
        		if ( fieldValue instanceof EsfDecimal ) 
        			fieldValueHtml = ((EsfDecimal)fieldValue).formatToHtml(fieldTemplate.getOutputFormatSpec());
        		else
        			fieldValueHtml = fieldValue.toHtml();
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValueHtml+"\" class=\""+inputClass+"\" maxlength=\"20\""+titleAttr+placeholderAttr+autoCompleteAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeEmailAddress() )
        	{
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\" maxlength=\"200\""+titleAttr+placeholderAttr+autoCompleteAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeFile() )
        	{
        		if ( context.currTransactionDocument != null && context.currDocument != null )
        		{
            		List<TransactionFile> fileList = context.transaction.getAllTransactionFilesForDocumentField(context.currTransactionDocument.getId(), fieldTemplate.getEsfName());

            		String removableFileList = FieldLabel.createRemovableFileList(this,fieldId,fieldName,fieldTemplate,fileList);
            		
            		String buttonSuffix = context.currDocument.getId().toNormalizedEsfNameString();

            		// Don't fight trying to style the size...browsers just do as they please with file uploads (Chrome even puts the select button inside the field and doesn't display the file path like IE and FF do)
            		// We also include an anchor tag so that after an upload, we can jump to this location.
            		// Input format spec can contain a number that will limit the number of file uploads we do
            		int limitNumFiles = app.stringToInt(fieldTemplate.getInputFormatSpec(), -1);
            		if ( limitNumFiles > 0 && fileList.size() >= limitNumFiles )
            		{
                		fieldHtml = "<a name=\""+fieldId+"\"></a><span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+">" + 
    					removableFileList + 
    		            "</span>";
            		}
            		else
            		{
                		fieldHtml = "<a name=\""+fieldId+"\"></a><span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+">" + 
    					removableFileList + 
    		            "<input id=\""+fieldId+"\" type=\"file\" name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\""+titleAttr+autoCompleteAttr+"/> " +
    		            "<input id=\""+fieldId+"Upload\" type=\"submit\" class=\"upload\" name=\""+DocumentPageBean.DOCUMENT_UPLOAD_BUTTON_PREFIX+fieldName.toLowerCase()+buttonSuffix+"\" value=\"Upload\"" + titleAttr + " onclick=\"esf.checkConfirmOnPageUnload(false)\" />" +
    		            "</span>";
            		}
        		}
        		else
        		{
        			fieldHtml = "";
        		}
        	}
        	else if ( fieldTemplate.isTypeFileConfirmClick() )
        	{
        		// If the value is set to an EsfName, we use that name for the file. Otherwise we'll default to how it was originally configured.
        		EsfName fileName = (fieldValue instanceof EsfName) ? (EsfName)fieldValue : new EsfName(fieldTemplate.getInputFormatSpec());
        		String fileInfoLink = FieldLabel.createFileConfirmClickHtml(this,fieldId,fieldName,fileName,fieldTemplate,isInputMode);
         		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+">" + fileInfoLink + "</span>";
        	}
        	else if ( fieldTemplate.isTypeInteger() )
        	{
        		String fieldValueHtml;
        		if ( fieldValue instanceof EsfInteger )
        			fieldValueHtml = ((EsfInteger)fieldValue).formatToHtml(fieldTemplate.getOutputFormatSpec());
        		else
        			fieldValueHtml = fieldValue.toHtml();
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValueHtml+"\" class=\""+inputClass+"\" maxlength=\"20\""+titleAttr+placeholderAttr+autoCompleteAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeMoney() )
        	{
        		String fieldValueHtml;
        		if ( fieldValue instanceof EsfMoney )
        			fieldValueHtml = ((EsfMoney)fieldValue).formatToHtml(fieldTemplate.getOutputFormatSpec());
        		else
        			fieldValueHtml = fieldValue.toHtml();
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValueHtml+"\" class=\""+inputClass+"\" maxlength=\"20\""+titleAttr+placeholderAttr+autoCompleteAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypePhone() )
        	{
        		String sizeAttr = "";
        		if ( fieldTemplate.isWidthUnitAuto() ) {
        			if ( fieldTemplate.getOutputFormatSpec().equals("standard") )
        				sizeAttr = " size=\"16\""; // 1 (425) 555-0100
        			else
        				sizeAttr = " size=\"14\""; // 1-425-555-0100
        		}
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\" maxlength=\"30\""+titleAttr+placeholderAttr+autoCompleteAttr+sizeAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeRadioButton() )
        	{
        		// Output format is the name of the radio button group and field we actually set
        		String radioButtonGroupName = fieldTemplate.getOutputFormatSpec();
        		EsfValue radioFieldValue = getFieldValue(docName,new EsfName(radioButtonGroupName),fieldSpec);
        		// Input format, if specified, is the value of this radio button, else it's its name
        		String checkValue = EsfString.isBlank(fieldTemplate.getInputFormatSpec()) ? fieldTemplate.getEsfName().toPlainString() : fieldTemplate.getInputFormatSpec();
    			String checkedAttribute = checkValue.equals(radioFieldValue.toPlainString()) ? " checked=\"checked\"" : "";
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\" type=\"radio\" name=\""+radioButtonGroupName.toLowerCase()+"\" value=\""+HtmlUtil.toDisplayHtml(checkValue)+"\" class=\""+inputClass+"\""+titleAttr+autoCompleteAttr+checkedAttribute+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeRichTextarea() )
        	{
        		String textareaInput = FieldLabel.createRichTextareaHtml(jspContext,this,fieldTemplate,fieldId,fieldName,inputClass,titleAttr,autoCompleteAttr,fieldValue);
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+titleAttr+">"+textareaInput+"</span>";
        	}
        	else if ( fieldTemplate.isTypeSelection() )
        	{
        		String selectHtml;
        		DropDownVersion ddv;
        		// If the drop down has extra options, let's see if we can find the drop down using the name specified. If not, we'll just stick with the hard-configured one.
        		if ( fieldTemplate.hasExtraOptions() )
        		{
        			String ddName = getFieldSpecWithSubstitutions(fieldTemplate.getExtraOptions());
        			ddv = context.transaction.getDropDownVersion(new EsfName(ddName),context.currDocumentVersion);
            		if ( ddv == null )
            		{
            			warning("buildFieldHtml() - Could not find dynamic dropdown box for field: " + fieldTemplate.getEsfName() + 
    							"; Dynamic DropDown name spec: " + fieldTemplate.getExtraOptions() + "; expanded to drop down name: " + ddName +
    							"; tranId: " + context.transactionId + "; packageId: " + context.pkg.getId() + "; packageParty: " + context.pkgVersionPartyTemplate.getEsfName() + 
    							"; docId: " + context.currDocument.getId() + "; docParties: " + context.currDocumentVersionPartyNameList);
            			ddv = context.transaction.getDropDownVersion(new EsfName(fieldTemplate.getInputFormatSpec()),context.currDocumentVersion);
            		}
        		}
        		else
        			ddv = context.transaction.getDropDownVersion(new EsfName(fieldTemplate.getInputFormatSpec()),context.currDocumentVersion);
        		if ( ddv == null )
        		{
        			warning("buildFieldHtml() - Unexpectedly missing dropdown box for field: " + fieldTemplate.getEsfName() + 
							"; DropDown: " + fieldTemplate.getInputFormatSpec() +
							"; tranId: " + context.transactionId + "; packageId: " + context.pkg.getId() + "; packageParty: " + context.pkgVersionPartyTemplate.getEsfName() + 
							"; docId: " + context.currDocument.getId() + "; docParties: " + context.currDocumentVersionPartyNameList);
					selectHtml = "<input id=\""+fieldId+"\" type=\"text\" name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\""+titleAttr+autoCompleteAttr+"/>";
        		}
        		else
        		{
        			if ( ddv.isAllowMultiSelection() )
        			{
        				EsfValue[] fieldValues = getFieldValues(docName,fieldName,fieldSpec);
        				selectHtml = FieldLabel.createMultiSelectHtml(ddv,fieldId,fieldName,inputClass,titleAttr,autoCompleteAttr,fieldTemplate.isAutoPostAllowed(),fieldValues);
        			}
        			else
        				selectHtml = FieldLabel.createSelectHtml(ddv,fieldId,fieldName,inputClass,titleAttr,autoCompleteAttr,fieldTemplate.isAutoPostAllowed(),fieldValue);
        		}
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+">" + selectHtml + "</span>";
        	}
        	else if ( fieldTemplate.isTypeSignature() )
        	{
        		fieldHtml = "<span class=\"signature "+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\" type=\"text\" name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\" maxlength=\""+fieldTemplate.getMaxLength()+"\""+titleAttr+placeholderAttr+autoCompleteAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeSignDate() )
        	{
        		fieldHtml = "<span class=\"signature "+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+titleAttr+">&nbsp;</span>";
        	}
        	else if ( fieldTemplate.isTypeSsnEin() )
        	{
        		String sizeAttr = fieldTemplate.isWidthUnitAuto() ? " size=\"11\"" : "";
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\" maxlength=\"11\""+titleAttr+placeholderAttr+autoCompleteAttr+sizeAttr+"/></span>";
        	}
        	else if ( fieldTemplate.isTypeTextarea() )
        	{
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><textarea id=\""+fieldId+"\" name=\""+fieldName.toLowerCase()+"\" class=\""+inputClass+"\" maxlength=\""+fieldTemplate.getMaxLength()+"\""+titleAttr+placeholderAttr+autoCompleteAttr+" rows=\""+fieldTemplate.getInputFormatSpec()+"\">"+fieldValue.toHtml()+"</textarea></span>";
        	}
        	else if ( fieldTemplate.isTypeZipCode() )
        	{
        		String sizeAttr = "";
        		if ( fieldTemplate.isWidthUnitAuto() ) {
        			if ( fieldTemplate.getInputFormatSpec().equals("5") )
        				sizeAttr = " size=\"5\"";
        			else
        				sizeAttr = " size=\"10\"";
        		}
        		String maxlengthAttr;
    			if ( fieldTemplate.getInputFormatSpec().equals("5") )
    				maxlengthAttr = " maxlength=\"5\"";
    			else
    				maxlengthAttr = " maxlength=\"10\"";
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldValue.toHtml()+"\" class=\""+inputClass+"\""+titleAttr+placeholderAttr+autoCompleteAttr+sizeAttr+maxlengthAttr+"/></span>";
        	}
        	else
        	{
        		fieldHtml = fieldValue.toHtml();
        		if ( EsfString.isBlank(fieldHtml) )
        			fieldHtml = "&nbsp;";
        		fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+"><input id=\""+fieldId+"\"" + inputTypeAttr + " name=\""+fieldName.toLowerCase()+"\" value=\""+fieldHtml+"\" class=\""+inputClass+"\""+titleAttr+placeholderAttr+autoCompleteAttr+" /></span>";
        	}
        }
        else // display mode
        {
        	fieldHtml = FieldLabel.formatDisplayField(this, docName, fieldName, fieldSpec, docName.toString(), fieldId, fieldTemplate, isOverlayField, fieldValue);
	
    		if ( fieldTemplate.isTypeSignature() )
    		{
    			fieldAreaClass += " signature";
    		}
    		else if ( fieldTemplate.isTypeSignDate() )
    		{
    			fieldAreaClass += " signature";
    		}
    		
        	fieldHtml = "<span class=\""+fieldAreaClass+" "+fieldBorderCssClass+"\" "+fieldAreaStyle+titleAttr+">" + fieldHtml + "</span>";
        }

		return fieldHtml;
	}
	
	// Creates the HTML that goes with a ${out:XXX} type field -- display just the value without input possible
	public String buildFieldOutHtml(JspContext jspContext, EsfName docName, EsfName fieldName, String fieldSpec, boolean isOverlayField)
	{
        FieldTemplate fieldTemplate = getFieldTemplate(docName,fieldName,fieldSpec);
        
        if ( fieldTemplate == null )
        	return "<span class=\"viewFieldData-error\">"+fieldSpec+"</span>"; // no field found
 
        EsfValue fieldValue = getFieldValue(docName,fieldName,fieldSpec);
        	
        String fieldId = fieldTemplate.getId().toNormalizedEsfNameString();

        String fieldHtml = FieldLabel.formatDisplayField(this, docName, fieldName, fieldSpec, docName.toString(), fieldId, fieldTemplate, isOverlayField, fieldValue);
        	
        return fieldHtml;
	}
	
	//************************  Code for setting up the transaction context **********************************
	
	public boolean pickupTransaction(String pickupCode)
			throws IOException
	{
		return pickupTransaction(pickupCode,true);
	}
	
	public boolean pickupTransaction(String pickupCode, boolean enforceLoginRequirement)
		throws IOException
	{
        if ( ! app.getRandomKey().isCorrectPickupCodeLength(pickupCode) )
        {
        	warning("The pickup code in the path info of the current link is not the expected length: " + pickupCode);
        	response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Please check your link. The pickup code specified to identify your request is missing or not valid.");
        	return false;
        }
        
        context = DocumentPageBean.TransactionPickupManager.getTransactionContext(session, pickupCode);
        
        // See if we're in API mode and doing the page review mode pickup compared to a normal pickup, we don't do any more processing
        String apiPageReviewPickupCodeKey = getParam(ESFAPI_PAGE_REVIEW_SESSION_PICKUPCODE_KEY_PARAM_NAME);
        if ( EsfString.isNonBlank(apiPageReviewPickupCodeKey) && context != null )
        {
        	String verifyPickupCode = getSessionStringAttribute(apiPageReviewPickupCodeKey);
        	if ( pickupCode.equals(verifyPickupCode) )
        		return true;
        }
        
        // Let's see if we can find out what transaction this is for by looking directly for the party assignment, since
        // in production, this could have changed with a transfer party.  But if it can't be found, we don't block
        // as we have further code that can try to resolve the transaction party (not the assignment)...
    	TransactionPartyAssignment tpa = TransactionPartyAssignment.Manager.getByPickupCode(pickupCode);
    	if ( tpa == null )
    	{
    		if ( context == null || context.hasNoTransaction() ) 
    		{
    			warning("Failed to find a transaction party assignment for the pickupCode: " + pickupCode);
            	response.sendError(HttpServletResponse.SC_NOT_FOUND, "The transaction associated with your pickup code cannot be found.");
                return false;
    		}
    		// we'll try more below...
    	}
    	else
    	{
        	if ( tpa.isTransferred() )
        	{
        		warning("TransactionPartyAssignment for pickupCode: " + pickupCode + " found, but has been transferred.");
            	response.sendError(HttpServletResponse.SC_FORBIDDEN, "The transaction associated with your pickup code has been re-assigned and is no longer available.");
                return false;
        	}
        	
        	if ( tpa.isRejected() )
        	{
        		warning("TransactionPartyAssignment for pickupCode: " + pickupCode + " found, but was rejected.");
            	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You previously rejected the transaction associated with your pickup code so it is no longer available.");
                return false;
        	}
        	
        	if ( tpa.isUndefined() )
        	{
            	err("TransactionPartyAssignment for pickupCode: " + pickupCode + " found, but the status is 'undefined' and so should not be used yet.");
            	response.sendError(HttpServletResponse.SC_FORBIDDEN, "The transaction associated with your pickup code is not yet available for your access.");
                return false;
        	}
     	}
    	
        if ( context == null || context.hasNoTransaction() )
        {
        	TransactionParty tranParty = TransactionParty.Manager.getByPickupCode(pickupCode);
        	if ( tranParty == null )
        	{
	        	err("Failed to find a transaction party -- but did find the party assignment -- for the pickupCode: " + pickupCode);
	        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "The transaction associated with your pickup code cannot be found.");
	            return false;
        	}

        	PackageVersionPartyTemplate packageVersionPartyTemplate = PackageVersionPartyTemplate.Manager.getById(tranParty.getPackageVersionPartyTemplateId());
        	if ( packageVersionPartyTemplate == null )
        	{
	        	err("Failed to find the package version party template for the pickupCode: " + pickupCode + 
	        			"; tranParty.getPackageVersionPartyTemplateId(): " + tranParty.getPackageVersionPartyTemplateId() + 
	        			"; tranParty status: " + tranParty.getStatus() + "; tranParty status label: " + tranParty.getStatusLabel());
	        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "The transaction associated with your pickup code cannot be found.");
	            return false;
        	}
        	
        	context = DocumentPageBean.TransactionPickupManager.registerPickupCode(getLoggedInUser(), session, null, pickupCode, packageVersionPartyTemplate.getEsfName(), tranParty.getTransactionId(), getIPOnly(), getIP(), getUserAgent());
        }
        
        if ( context.hasNoTransaction() )
        {
        	err("Failed to find a transaction with the id: " + context.transactionId + "; pickupCode: " + pickupCode);
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "The transaction associated with your pickup code no longer exists.");
            return false;
        }
        
        if ( context.pkgVersionPartyTemplate == null )
        {
        	err("Could not determine the package/document party for transaction id: " + context.transactionId + "; pickupPartyName" + context.pickupPartyName);
        	DocumentPageBean.TransactionPickupManager.resetTransactionContext(session, context);
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Unfortunately, we were not able to determine your role in this transaction, so access cannot be granted.");
            return false;
        }
        
        // If login is required, enforce that now. 
        if ( context.pkgVersionPartyTemplate.isRequireLogin() )
        {
        	// The check redirected to the login page with the toURL setup to return once the login succeeds.
        	if ( enforceLoginRequirement && ! checkLoggedIn() )
        		return false;
        }
        else
        {
        	app.getSessionTracker().updateEmail(context.transactionParty.getCurrentAssignment().getEmailAddress(), session);
        	session.setAttribute(SessionTracker.PARTY_NAME_SESSION_ATTRIBUTE_NAME,context.transaction.getTransactionTemplate().getPathName().toString() + " (" + context.pkgVersionPartyTemplate.getEsfName().toString() + ")");
        }
        
    	// If we don't know the user yet and we're logged in, set it now.
    	if ( ! context.hasUser() && isUserLoggedIn() )
    		context.setUser(getLoggedInUser());

        context.setIpOnlyAddress(getIPOnly()); // we'll update in case it's changed
        
    	if ( context.transaction.isProduction() && ! app.allowProductionTransactions() )
        {
        	err("Production transactions are not allowed on this deployment.");
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Production transactions are not currently allowed on this deployment.");
            return false;
        }
        
        if ( context.transaction.isTest() && ! app.allowTestTransactions() )
        {
        	warning("Test transactions are not allowed on this deployment.");
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Test transactions are not currently allowed on this deployment.");
            return false;
        }
        
        if ( context.transaction.isCanceled() )
        {
        	warning("Canceled transaction cannot be retrieved for processing.");
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Sorry, the requested transaction has been canceled is no longer accessible.");
            return false;
        }
        
        if ( context.hasUser() )
        {
        	if ( ! isUserLoggedIn() )
        	{
        		warning("Transaction context is for user: " + context.user.getFullDisplayName() + "; but user is no longer logged in for transaction id: " + context.transactionId + "; pickupPartyName" + context.pickupPartyName);
            	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You must remain logged in while you work on this transaction.");
                return false;
        	} 
        	if ( ! context.user.equals(getLoggedInUser()) )
        	{
            	warning("Transaction context is for user: " + context.user.getFullDisplayName() + "; but user is currently logged in as: " + getLoggedInUser().getFullDisplayName() + "; for transaction id: " + context.transactionId + "; pickupPartyName" + context.pickupPartyName);
            	response.sendError(HttpServletResponse.SC_FORBIDDEN, "You must remain logged in to the same account while you work on this transaction.");
                return false;
        	}
        }
        
        // If active or completed (or via our reports access), all is okay to continue...
        if ( ! context.transactionParty.isActive() && ! context.transactionParty.isCompleted() && ! context.transactionParty.isReports() )
        {
           	err("Non-active and non-completed party attempted to pickup the transaction. Transaction party status: " + context.transactionParty.getStatus() + "; transactionPartyId" + context.transactionParty.getId());
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Sorry, the requested transaction is not expecting your access at this time.");
            return false;
        }
        
        TransactionPartyAssignment assignment = context.transactionParty.getCurrentAssignment();
        
        // If it's locked to a user that's not the current user, don't let them in
        if ( enforceLoginRequirement && assignment.hasUserId() && (! context.hasUser() || ! assignment.getUserId().equals(context.user.getId())) )
        {
        	if ( context.hasUser() )
        	{
        		warning("User " + context.user.getFullDisplayName() + " wanted to process transaction: " + context.transactionId + "; as transactionPartyId: " + 
        				context.transactionParty.getId() + "; but it's already assigned to another user id: " + assignment.getUserId());
            	if ( context.transactionParty.isActive() )
            		response.sendError(HttpServletResponse.SC_FORBIDDEN, "Sorry, the requested transaction is currently being processed by another user.");
            	else
            		response.sendError(HttpServletResponse.SC_FORBIDDEN, "Sorry, the requested transaction was processed by another user.");
        	}
        	else
        	{
        		warning("Pickup party wanted to process transaction: " + context.transactionId + "; as transactionPartyId: " + 
        				context.transactionParty.getId() + "; but it's already assigned to user id: " + assignment.getUserId());
            	if ( context.transactionParty.isActive() )
            		response.sendError(HttpServletResponse.SC_FORBIDDEN, "Sorry, the requested transaction is currently being processed by another user. If you were logged in when you started it, please login again before accessing it.");
            	else
            		response.sendError(HttpServletResponse.SC_FORBIDDEN, "Sorry, the requested transaction was processed by another user. If you were logged in when you completed it, please login again before accessing it.");
        	}
            return false;
        }
        
        // When login is required and we know who the user is, let's make sure that user is allowed to play this
        // role if a To Do group has been set up so that users outside of that To Do group cannot process it even
        // with the pickup code.
        if ( enforceLoginRequirement && context.pkgVersionPartyTemplate.isRequireLogin() ) 
        {
        	boolean isToDoGroupAllowed = true;
        	if ( context.transactionParty.hasTodoGroupId() ) 
        	{
        		Group todoGroup = Group.Manager.getById(context.transactionParty.getTodoGroupId());
        		if ( todoGroup == null )
        		{
        			warning("Missing To Do Group for todoGroupId: " + context.transactionParty.getTodoGroupId() + 
        					"; package party: " + context.pkgVersionPartyTemplate.getEsfName() + "; transaction id: " +
        					context.transactionId);
            		context.transaction.logGeneral("ERROR: User " + context.user.getFullDisplayName() + " tried to access transaction as package party: " +
            				context.pkgVersionPartyTemplate.getEsfName() + "; but missing specified To Do group id: " + context.transactionParty.getTodoGroupId());
            		response.sendError(HttpServletResponse.SC_FORBIDDEN, "Sorry, the requested transaction specifies a missing To Do group. Access is denied.");
        			return false;
        		}
        		if ( todoGroup.isDisabled() )
        		{
        			warning("Disabled To Do Group for todoGroupId: " + context.transactionParty.getTodoGroupId() + 
        					"; package party: " + context.pkgVersionPartyTemplate.getEsfName() + "; transaction id: " +
        					context.transactionId);
            		context.transaction.logGeneral("WARNING: User " + context.user.getFullDisplayName() + " tried to access transaction as package party: " +
            				context.pkgVersionPartyTemplate.getEsfName() + "; but To Do group " + todoGroup.getPathName() + " is disabled.");
            		response.sendError(HttpServletResponse.SC_FORBIDDEN, "Sorry, the requested transaction specifies a disabled To Do group. Access is denied.");
        			return false;
        		}
        		isToDoGroupAllowed = context.user.isMemberOfGroup(todoGroup);
        	}

        	// If not in the group and he's not the currently assigned party, reject it
        	if ( ! isToDoGroupAllowed && ! context.user.getEmail().equalsIgnoreCase(assignment.getEmailAddress()) )
        	{
        		context.user.logSecurity("Tried to access transaction id: " + context.transactionId + "; as package party: " +
        				context.pkgVersionPartyTemplate.getEsfName() + "; but is not an authorized To Do user.");
        		context.transaction.logGeneral("WARNING: User " + context.user.getFullDisplayName() + " tried to access transaction as package party: " +
        				context.pkgVersionPartyTemplate.getEsfName() + "; but is not an authorized To Do user.");
        		response.sendError(HttpServletResponse.SC_FORBIDDEN, "Sorry, the requested transaction specifies a To Do group that you are not a member of.");
    			return false;
        	}
        }
        
        if ( context.transaction.isInProgress() || context.isEsfReportsAccess() )
        {
            // If the transaction party is marked as activated, we'll mark it as retrieved now
            if ( assignment.isActivated() )
            {
            	assignment.setStatusRetrieved();
            	if ( context.hasUser() )
            	{
            		assignment.setUserId(context.user.getId());
            		assignment.setEmailAddress(context.user.getEmail());
            	}
            }
            else if ( assignment.isRetrieved() ) // set the user on retrieval if we don't already know it
            {
            	if ( ! assignment.hasUserId() && context.hasUser() )
            	{
            		assignment.setUserId(context.user.getId());
            		assignment.setEmailAddress(context.user.getEmail());
            	}
            }
            assignment.setLastAccessedTimestampToNow(); // record this access no matter what
        }
        
        return true;
	}
	
	public boolean pickupTransaction(EsfReportsAccessTransaction.EsfReportsAccessContext reportsAccessContext)
		throws IOException
	{
        boolean needsPartyCreatedEvent = false;
        
        context = DocumentPageBean.TransactionPickupManager.getTransactionContext(session, reportsAccessContext.getPickupCode());

        if ( context == null || context.hasNoTransaction() )
        {
        	// We ensure the transaction has the ESF_reports_access party defined 
        	Transaction tran = Transaction.Manager.getById(reportsAccessContext.getTransactionId());
        	if ( tran == null )
        	{
	        	err("Failed to find the ESF_reports_access transaction by id: " + reportsAccessContext.getTransactionId());
	        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "The transaction associated with your report access request cannot be found. If not recently deleted, please try again.");
	            return false;
        	}
        	
        	TransactionParty tranParty = tran.getEsfReportsAccessTransactionParty();
        	if ( tranParty == null )
        	{
        		tranParty = tran._createViewOptionalEsfReportsAccessTransactionParty();
            	Transaction.Manager.removeFromCache(tran); // refresh our transaction so it will also have the new reports party defined
            	tran = Transaction.Manager.getById(reportsAccessContext.getTransactionId());
            	if ( tran == null )
            	{
    	        	err("Failed to reload the transaction with ESF_reports_access created; transactionId: " + reportsAccessContext.getTransactionId());
    	        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "The transaction associated with your report access request cannot be setup.");
    	            return false;
            	}
            	tranParty = tran.getEsfReportsAccessTransactionParty();
            	if ( tranParty == null )
            	{
    	        	err("Failed to setup ESF_reports_access party for transactionId: " + reportsAccessContext.getTransactionId());
    	        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "The transaction party associated with your report access request cannot be setup.");
    	            return false;
            	}
            	needsPartyCreatedEvent = true;
        	}
        	
         	PackageVersionPartyTemplate packageVersionPartyTemplate = PackageVersionPartyTemplate.Manager.getById(tranParty.getPackageVersionPartyTemplateId());
        	if ( packageVersionPartyTemplate == null )
        	{
	        	err("Failed to find the ESF_reports_access package version party template for tranParty.getPackageVersionPartyTemplateId(): " + tranParty.getPackageVersionPartyTemplateId() + 
	        			"; tranParty status: " + tranParty.getStatus() + "; tranParty status label: " + tranParty.getStatusLabel());
	        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "The transaction associated with your report access cannot be found.");
	            return false;
        	}
        	
        	context = DocumentPageBean.TransactionPickupManager.registerPickupCode(getLoggedInUser(), session, reportsAccessContext, reportsAccessContext.getPickupCode(), packageVersionPartyTemplate.getEsfName(), tranParty.getTransactionId(), getIPOnly(), getIP(), getUserAgent());
        }
        
        if ( context.hasNoTransaction() )
        {
        	err("Failed to find a ESF_reports_access transaction with the id: " + context.transactionId + "; pickupCode: " + context.pickupCode);
        	response.sendError(HttpServletResponse.SC_NOT_FOUND, "The transaction associated with your reports access no longer exists.");
            return false;
        }
        
        if ( context.pkgVersionPartyTemplate == null )
        {
        	err("Could not determine the ESF_reports_access package/document party for transaction id: " + context.transactionId + "; pickupPartyName" + context.pickupPartyName);
        	DocumentPageBean.TransactionPickupManager.resetTransactionContext(session, context);
        	response.sendError(HttpServletResponse.SC_FORBIDDEN, "Unfortunately, we were not able to determine your expected ESF_reports_access role in this transaction, so access cannot be granted.");
            return false;
        }
        
    	// If we don't know the user yet and we're logged in, set it now.
    	if ( ! context.hasUser() && isUserLoggedIn() )
    		context.setUser(getLoggedInUser());

        context.setIpOnlyAddress(getIPOnly()); // we'll update in case it's changed
        
		TransactionEngine engine = null;

		if ( needsPartyCreatedEvent )
		{
			if ( engine == null )
				engine = new TransactionEngine(context);
	    	engine.queuePartyCreatedEvent(PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS);
	    	engine.doWork();
		}

    	TransactionPartyAssignment assignment = context.transactionParty.getCurrentAssignment();
        
        if ( context.hasUser() )
        {
    		assignment.setUserId(context.user.getId());
    		assignment.setEmailAddress(context.user.getEmail());
        }
        
        // If the transaction party is marked as unspecified, we'll mark it as activated now
        if ( assignment.isUndefined() )
        {
        	if ( engine == null )
        		engine = new TransactionEngine(context);
        	engine.queuePartyActivatedEvent(context.pickupPartyName);
        	engine.doWork();
        }
        
        // If the transaction party is marked as activated, we'll mark it as retrieved now
        if ( assignment.isActivated() )
        	assignment.setStatusRetrieved();
        assignment.setLastAccessedTimestampToNow(); // record this access no matter what

        return true;
	}
		
	public static class TransactionPickupManager
    {
        private static final String SESSION_ATTRIBUTE_TRANSACTION_CONTEXT_CACHE = "ESF-DocumentPageBean-Transaction-Context-Cache";

        public synchronized static TransactionContext registerPickupCode(User user, HttpSession session, String pickupCode, EsfName pickupPartyName, EsfUUID tranId, String ipOnlyAddress, String ipHost, String userAgent)
    	{
        	return registerPickupCode(user,session,null,pickupCode,pickupPartyName,tranId,ipOnlyAddress,ipHost,userAgent);
    	}
        
        // reportsAccessContext is non-null for access via reports
        public synchronized static TransactionContext registerPickupCode(User user, HttpSession session, EsfReportsAccessContext reportsAccessContext, String pickupCode, EsfName pickupPartyName, EsfUUID tranId, String ipOnlyAddress, String ipHost, String userAgent)
    	{
    		TreeMap<String,TransactionContext> cache = getTransactionContextCache(session);
    		TransactionContext ctx = new TransactionContext(user,reportsAccessContext,pickupCode,pickupPartyName,tranId,ipOnlyAddress,ipHost,userAgent);
    		cache.put(pickupCode, ctx);
    		return ctx;
    	}
		
        public synchronized static void resetTransactionContext(HttpSession session, TransactionContext ctx)
    	{
    		TreeMap<String,TransactionContext> cache = getTransactionContextCache(session);
    		cache.remove(ctx.pickupCode);
    		ctx.reset();
    	}
		
		public synchronized static TransactionContext getTransactionContext(HttpSession session, String pickupCode)
		{
    		TreeMap<String,TransactionContext> cache = getTransactionContextCache(session);
    		TransactionContext context = cache.get(pickupCode);
            if ( context != null && context.hasTransaction() ) // we do this to ensure any context cache of the transaction matches our latest version
            	context.refreshTransaction();
            return context;
		}
    	
    	@SuppressWarnings("unchecked")
		private static TreeMap<String,TransactionContext> getTransactionContextCache(HttpSession session)
    	{
    		TreeMap<String,TransactionContext> cache = (TreeMap<String,TransactionContext>)session.getAttribute(SESSION_ATTRIBUTE_TRANSACTION_CONTEXT_CACHE);
    		if ( cache == null )
    		{
    			cache = new TreeMap<String,TransactionContext>();
    			session.setAttribute(SESSION_ATTRIBUTE_TRANSACTION_CONTEXT_CACHE, cache);
    		}
    		return cache;
    	}
    }

}