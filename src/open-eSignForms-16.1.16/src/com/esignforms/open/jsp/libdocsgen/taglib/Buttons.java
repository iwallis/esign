// Copyright (C) 2010-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen.taglib;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.esignforms.open.Application;
import com.esignforms.open.config.Literals;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.util.HtmlUtil;

/**
 * This class is used to display the buttons for navigating the form.
 * 
 * @author Yozons, Inc.
 */
public class Buttons 
    extends javax.servlet.jsp.tagext.SimpleTagSupport
{
    public void doTag() 
        throws java.io.IOException
    {
    	JspWriter out = getJspContext().getOut();

    	// The generated page should have initialized its DocumentPageBean
        DocumentPageBean docPage = (DocumentPageBean)getJspContext().getAttribute(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId,PageContext.REQUEST_SCOPE);
        if ( docPage == null )
        {
        	Application.getInstance().err("Buttons tag could not find DocumentPageBean with id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId + " in the request scope.");
            return;
        }
        
        // Do not do any buttons if the transaction is not known or has been deleted
        if ( docPage.context.transaction == null || docPage.context.transaction.isDeleted() )
        	return;
        
        // If canceled, allow reports access to SAVE, but don't do any other buttons
        if ( docPage.context.transaction.isCanceled() )
        {
        	if ( ! docPage.context.isEsfReportsAccess() )
        		return;
        }
        
        String buttonSuffix = docPage.context.currDocument.getId().toNormalizedEsfNameString();
        
        out.write(Literals.JSP_BEGIN_REMOVE_COMMENT);
        
        if ( docPage.context.isPackageDocument() )
        {
	    	String buttonLabel = docPage.context.transactionParty.isEndState() ? docPage.getButtonMessageVersion().getPackageButtonContinueDone(docPage.context) : docPage.getButtonMessageVersion().getPackageButtonContinueToDo(docPage.context);
	    	String buttonTitle = docPage.context.transactionParty.isEndState() ? docPage.getButtonMessageVersion().getPackageButtonContinueDoneTitle(docPage.context) : docPage.getButtonMessageVersion().getPackageButtonContinueToDoTitle(docPage.context);

	    	out.write("<p class=\"buttons\">");
           	
	    	if ( ! docPage.context.isEsfReportsAccess() )
	    	{
	           	out.write("<input type=\"submit\" class=\"preferredFlow\" name=\"");
	        	out.write(DocumentPageBean.PACKAGE_CONTINUE_BUTTON_PREFIX);
	        	out.write(buttonSuffix);
	        	out.write("\" value=\"");
	        	out.write(HtmlUtil.toEscapedHtml(buttonLabel));
	        	out.write("\" title=\"");
	        	out.write(HtmlUtil.toEscapedHtml(buttonTitle));
	        	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
	    	}
        	
        	// If the party is active, is allowed to delete the transaction, and no signatures have been applied, show a delete button, too.
        	if ( ! docPage.context.transaction.isSuspended() && docPage.context.transactionParty.isActive() && docPage.context.pkgVersionPartyTemplate.isAllowDeleteTranIfUnsigned() && ! docPage.context.hasAnyDocumentBeenSigned() )
        	{
    	    	String deleteButtonLabel = docPage.getButtonMessageVersion().getPackageButtonDelete(docPage.context);
    	    	String deleteButtonTitle = docPage.getButtonMessageVersion().getPackageButtonDeleteTitle(docPage.context);

    	    	out.write("<input type=\"submit\" class=\"caution\" name=\"");
            	out.write(DocumentPageBean.PACKAGE_DELETE_TRAN_BUTTON_PREFIX);
            	out.write(buttonSuffix);
            	out.write("\" value=\"");
            	out.write(HtmlUtil.toEscapedHtml(deleteButtonLabel));
            	out.write("\" title=\"");
            	out.write(HtmlUtil.toEscapedHtml(deleteButtonTitle));
            	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
        	}
        	
        	if ( ! docPage.context.transaction.isSuspended() && docPage.context.transactionParty.isActive() && docPage.context.pkgVersionPartyTemplate.hasNotCompletedUrlFieldExpression() )
        	{
    	    	String notCompletedButtonLabel = docPage.getButtonMessageVersion().getPackageButtonNotCompleting(docPage.context);
    	    	String notCompletedButtonTitle = docPage.getButtonMessageVersion().getPackageButtonNotCompletingTitle(docPage.context);

    	    	out.write("<input type=\"submit\" name=\"");
            	out.write(DocumentPageBean.PACKAGE_NOT_COMPLETING_BUTTON_PREFIX);
            	out.write(buttonSuffix);
            	out.write("\" value=\"");
            	out.write(HtmlUtil.toEscapedHtml(notCompletedButtonLabel));
            	out.write("\" title=\"");
            	out.write(HtmlUtil.toEscapedHtml(notCompletedButtonTitle));
            	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
        	}
        	
        	if ( docPage.context.transactionParty.isCompleted() && docPage.context.transactionPartyDocuments != null && docPage.context.transactionPartyDocuments.size() > 0 )
        	{
    	    	String downloadButtonLabel = docPage.getButtonMessageVersion().getPackageButtonDownloadMyDocsAsPdf(docPage.context);
    	    	String downloadButtonTitle = docPage.getButtonMessageVersion().getPackageButtonDownloadMyDocsAsPdfTitle(docPage.context);

    	    	out.write("<input type=\"submit\" name=\"");
            	out.write(DocumentPageBean.PACKAGE_DOWNLOAD_MY_DOCS_AS_PDF_BUTTON_PREFIX);
            	out.write(buttonSuffix);
            	out.write("\" value=\"");
            	out.write(HtmlUtil.toEscapedHtml(downloadButtonLabel));
            	out.write("\" title=\"");
            	out.write(HtmlUtil.toEscapedHtml(downloadButtonTitle));
            	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false,6000)\" />"); // hide temporarily since no new web page will be displayed as a result (to reshow the buttons)
        	}
        	
        	out.write("</p>");
        }
        else
        {
            if ( docPage.isPageEdit() )
            {
            	out.write("<p class=\"buttons\">");
            	
            	if ( docPage.context.hasNextEditPage() )
            	{
        	    	String buttonLabel = docPage.getButtonMessageVersion().getDocumentEditButtonContinueNextPage(docPage.context);
        	    	String buttonTitle = docPage.getButtonMessageVersion().getDocumentEditButtonContinueNextPageTitle(docPage.context);

                	out.write("<input type=\"submit\" class=\"preferredFlow\" name=\"");
                	out.write(DocumentPageBean.DOCUMENT_NEXT_PAGE_BUTTON_PREFIX);
                	out.write(buttonSuffix);
                	out.write("\" value=\"");
                	out.write(HtmlUtil.toEscapedHtml(buttonLabel));
                	out.write("\" title=\"");
                	out.write(HtmlUtil.toEscapedHtml(buttonTitle));
                	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
            	}
            	else
            	{
            		// When editing a page from the reports, we show only a SAVE button
            		if ( docPage.context.isEsfReportsAccessForUpdate() )
            		{
            	    	String buttonLabel = docPage.getButtonMessageVersion().getDocumentEditButtonSave(docPage.context);
            	    	String buttonTitle = docPage.getButtonMessageVersion().getDocumentEditButtonSaveTitle(docPage.context);

                    	out.write("<input type=\"submit\" class=\"preferredFlow\" name=\"");
                    	out.write(DocumentPageBean.DOCUMENT_SAVE_BUTTON_PREFIX);
                    	out.write(buttonSuffix);
                    	out.write("\" value=\"");
                    	out.write(HtmlUtil.toEscapedHtml(buttonLabel));
                    	out.write("\" title=\"");
                    	out.write(HtmlUtil.toEscapedHtml(buttonTitle));
                    	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
            		}
            		else if ( ! docPage.context.transaction.isSuspended() )
            		{
            	    	String buttonLabel = docPage.getButtonMessageVersion().getDocumentEditButtonContinueToReview(docPage.context);
            	    	String buttonTitle = docPage.getButtonMessageVersion().getDocumentEditButtonContinueToReviewTitle(docPage.context);

                    	out.write("<input type=\"submit\" class=\"preferredFlow\" name=\"");
                    	out.write(DocumentPageBean.DOCUMENT_REVIEW_BUTTON_PREFIX);
                    	out.write(buttonSuffix);
                    	out.write("\" value=\"");
                    	out.write(HtmlUtil.toEscapedHtml(buttonLabel));
                    	out.write("\" title=\"");
                    	out.write(HtmlUtil.toEscapedHtml(buttonTitle));
                    	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
            		}
            	}
            	
            	if ( docPage.context.hasPreviousEditPage() )
            	{
        	    	String buttonLabel = docPage.getButtonMessageVersion().getDocumentEditButtonPreviousPage(docPage.context);
        	    	String buttonTitle = docPage.getButtonMessageVersion().getDocumentEditButtonPreviousPageTitle(docPage.context);

                	out.write("<input type=\"submit\" name=\"");
                	out.write(DocumentPageBean.DOCUMENT_PREVIOUS_PAGE_BUTTON_PREFIX);
                	out.write(buttonSuffix);
                	out.write("\" value=\"");
                	out.write(HtmlUtil.toEscapedHtml(buttonLabel));
                	out.write("\" title=\"");
                	out.write(HtmlUtil.toEscapedHtml(buttonTitle));
                	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
            	}
            	
    	    	String returnToPackageButtonLabel = docPage.getButtonMessageVersion().getDocumentButtonGoToPackage(docPage.context);
    	    	String returnToPackageButtonTitle = docPage.getButtonMessageVersion().getDocumentButtonGoToPackageTitle(docPage.context);

            	out.write("<input type=\"submit\" name=\"");
            	out.write(DocumentPageBean.DOCUMENT_VIEW_PACKAGE_BUTTON_PREFIX);
            	out.write(buttonSuffix);
            	out.write("\" value=\"");
            	out.write(HtmlUtil.toEscapedHtml(returnToPackageButtonLabel));
            	out.write("\" title=\"");
            	out.write(HtmlUtil.toEscapedHtml(returnToPackageButtonTitle));
            	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");

            	out.write("</p>");
            	
            	if ( ! docPage.context.isEsfReportsAccess() && ! docPage.context.transaction.isSuspended() )
            	{
                	out.write("<p class=\"extraButtons\">");
                	
        	    	String saveButtonLabel = docPage.getButtonMessageVersion().getDocumentEditButtonSave(docPage.context);
        	    	String saveButtonTitle = docPage.getButtonMessageVersion().getDocumentEditButtonSaveTitle(docPage.context);

                	out.write("<input type=\"submit\" name=\"");
                	out.write(DocumentPageBean.DOCUMENT_SAVE_BUTTON_PREFIX);
                	out.write(buttonSuffix);
                	out.write("\" value=\"");
                	out.write(HtmlUtil.toEscapedHtml(saveButtonLabel));
                	out.write("\" title=\"");
                	out.write(HtmlUtil.toEscapedHtml(saveButtonTitle));
                	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");

                	out.write("</p>");
            	}
            }
            else if ( docPage.isPageReview() )
            {
            	out.write("<p class=\"buttons\">");
            	
            	if ( docPage.context.isViewOnlyParty() )
            	{
                	if ( docPage.context.hasNextEditPage() )
                	{
            	    	String buttonLabel = docPage.getButtonMessageVersion().getDocumentReviewViewOnlyButtonContinueNextPage(docPage.context);
            	    	String buttonTitle = docPage.getButtonMessageVersion().getDocumentReviewViewOnlyButtonContinueNextPageTitle(docPage.context);

                    	out.write("<input type=\"submit\" class=\"preferredFlow\" name=\"");
                    	out.write(DocumentPageBean.DOCUMENT_NEXT_PAGE_BUTTON_PREFIX);
                    	out.write(buttonSuffix);
                    	out.write("\" value=\"");
                    	out.write(HtmlUtil.toEscapedHtml(buttonLabel));
                    	out.write("\" title=\"");
                    	out.write(HtmlUtil.toEscapedHtml(buttonTitle));
                    	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
                	}
                	else if ( ! docPage.context.transaction.isSuspended() )
                	{
            	    	String buttonLabel = docPage.getButtonMessageVersion().getDocumentReviewViewOnlyButtonComplete(docPage.context);
            	    	String buttonTitle = docPage.getButtonMessageVersion().getDocumentReviewViewOnlyButtonCompleteTitle(docPage.context);

                    	out.write("<input type=\"submit\" class=\"preferredFlow\" name=\"");
                    	out.write(DocumentPageBean.DOCUMENT_COMPLETE_BUTTON_PREFIX);
                    	out.write(buttonSuffix);
                    	out.write("\" value=\"");
                    	out.write(HtmlUtil.toEscapedHtml(buttonLabel));
                    	out.write("\" title=\"");
                    	out.write(HtmlUtil.toEscapedHtml(buttonTitle));
                    	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
                	}
                	
                	if ( docPage.context.hasPreviousEditPage() )
                	{
            	    	String buttonLabel = docPage.getButtonMessageVersion().getDocumentReviewViewOnlyButtonPreviousPage(docPage.context);
            	    	String buttonTitle = docPage.getButtonMessageVersion().getDocumentReviewViewOnlyButtonPreviousPageTitle(docPage.context);

                    	out.write("<input type=\"submit\" name=\"");
                    	out.write(DocumentPageBean.DOCUMENT_PREVIOUS_PAGE_BUTTON_PREFIX);
                    	out.write(buttonSuffix);
                    	out.write("\" value=\"");
                    	out.write(HtmlUtil.toEscapedHtml(buttonLabel));
                    	out.write("\" title=\"");
                    	out.write(HtmlUtil.toEscapedHtml(buttonTitle));
                    	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
                	}
            		
        	    	String returnToPackageButtonLabel = docPage.getButtonMessageVersion().getDocumentButtonGoToPackage(docPage.context);
        	    	String returnToPackageButtonTitle = docPage.getButtonMessageVersion().getDocumentButtonGoToPackageTitle(docPage.context);

                	out.write("<input type=\"submit\" name=\"");
                	out.write(DocumentPageBean.DOCUMENT_VIEW_PACKAGE_BUTTON_PREFIX);
                	out.write(buttonSuffix);
                	out.write("\" value=\"");
                	out.write(HtmlUtil.toEscapedHtml(returnToPackageButtonLabel));
                	out.write("\" title=\"");
                	out.write(HtmlUtil.toEscapedHtml(returnToPackageButtonTitle));
                	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
            	}
            	else
            	{
        	    	if ( ! docPage.context.transaction.isSuspended() )
        	    	{
            	    	String completeButtonLabel = docPage.context.canPartySignCurrDocument() ? docPage.getButtonMessageVersion().getDocumentReviewButtonCompleteSigner(docPage.context) : docPage.getButtonMessageVersion().getDocumentReviewButtonCompleteNotSigner(docPage.context);
            	    	String completeButtonTitle = docPage.context.canPartySignCurrDocument() ? docPage.getButtonMessageVersion().getDocumentReviewButtonCompleteSignerTitle(docPage.context) : docPage.getButtonMessageVersion().getDocumentReviewButtonCompleteNotSignerTitle(docPage.context);

                    	out.write("<input type=\"submit\" class=\"preferredFlow\" name=\"");
                    	out.write(DocumentPageBean.DOCUMENT_COMPLETE_BUTTON_PREFIX);
                    	out.write(buttonSuffix);
                    	out.write("\" value=\"");
                    	out.write(HtmlUtil.toEscapedHtml(completeButtonLabel));
                    	out.write("\" title=\"");
                    	out.write(HtmlUtil.toEscapedHtml(completeButtonTitle));
                    	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
        	    	}
                	
        	    	String returnToEditButtonLabel = docPage.getButtonMessageVersion().getDocumentReviewButtonReturnToEdit(docPage.context);
        	    	String returnToEditButtonTitle = docPage.getButtonMessageVersion().getDocumentReviewButtonReturnToEditTitle(docPage.context);

                	out.write("<input type=\"submit\" name=\"");
                	out.write(DocumentPageBean.DOCUMENT_EDIT_BUTTON_PREFIX);
                	out.write(buttonSuffix);
                	out.write("\" value=\"");
                	out.write(HtmlUtil.toEscapedHtml(returnToEditButtonLabel));
                	out.write("\" title=\"");
                	out.write(HtmlUtil.toEscapedHtml(returnToEditButtonTitle));
                	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
            	}
            	
            	out.write("</p>");
            }
            else if ( docPage.isPageViewOnly() )
            {
            	out.write("<p class=\"buttons\">");
            	
            	if ( docPage.context.hasNextEditPage() )
            	{
        	    	String buttonLabel = docPage.getButtonMessageVersion().getDocumentViewOnlyButtonNextPage(docPage.context);
        	    	String buttonTitle = docPage.getButtonMessageVersion().getDocumentViewOnlyButtonNextPageTitle(docPage.context);

                	out.write("<input type=\"submit\" class=\"preferredFlow\" name=\"");
                	out.write(DocumentPageBean.DOCUMENT_NEXT_PAGE_BUTTON_PREFIX);
                	out.write(buttonSuffix);
                	out.write("\" value=\"");
                	out.write(HtmlUtil.toEscapedHtml(buttonLabel));
                	out.write("\" title=\"");
                	out.write(HtmlUtil.toEscapedHtml(buttonTitle));
                	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
            	}
            	else if ( docPage.context.hasNextCompletedDocument() )
            	{
        	    	String buttonLabel = docPage.getButtonMessageVersion().getDocumentViewOnlyButtonNextDocument(docPage.context);
        	    	String buttonTitle = docPage.getButtonMessageVersion().getDocumentViewOnlyButtonNextDocumentTitle(docPage.context);

                	out.write("<input type=\"submit\" class=\"preferredFlow\" name=\"");
                	out.write(DocumentPageBean.DOCUMENT_NEXT_DOCUMENT_BUTTON_PREFIX);
                	out.write(buttonSuffix);
                	out.write("\" value=\"");
                	out.write(HtmlUtil.toEscapedHtml(buttonLabel));
                	out.write("\" title=\"");
                	out.write(HtmlUtil.toEscapedHtml(buttonTitle));
                	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
            	}

            	if ( docPage.context.hasPreviousEditPage() )
            	{
        	    	String buttonLabel = docPage.getButtonMessageVersion().getDocumentViewOnlyButtonPreviousPage(docPage.context);
        	    	String buttonTitle = docPage.getButtonMessageVersion().getDocumentViewOnlyButtonPreviousPageTitle(docPage.context);

                	out.write("<input type=\"submit\" name=\"");
                	out.write(DocumentPageBean.DOCUMENT_PREVIOUS_PAGE_BUTTON_PREFIX);
                	out.write(buttonSuffix);
                	out.write("\" value=\"");
                	out.write(HtmlUtil.toEscapedHtml(buttonLabel));
                	out.write("\" title=\"");
                	out.write(HtmlUtil.toEscapedHtml(buttonTitle));
                	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
            	}
            	else if ( docPage.context.hasPreviousCompletedDocument() )
            	{
        	    	String buttonLabel = docPage.getButtonMessageVersion().getDocumentViewOnlyButtonPreviousDocument(docPage.context);
        	    	String buttonTitle = docPage.getButtonMessageVersion().getDocumentViewOnlyButtonPreviousDocumentTitle(docPage.context);

                	out.write("<input type=\"submit\" name=\"");
                	out.write(DocumentPageBean.DOCUMENT_PREVIOUS_DOCUMENT_BUTTON_PREFIX);
                	out.write(buttonSuffix);
                	out.write("\" value=\"");
                	out.write(HtmlUtil.toEscapedHtml(buttonLabel));
                	out.write("\" title=\"");
                	out.write(HtmlUtil.toEscapedHtml(buttonTitle));
                	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
            	}

    	    	String returnToPackageButtonLabel = docPage.getButtonMessageVersion().getDocumentButtonGoToPackage(docPage.context);
    	    	String returnToPackageButtonTitle = docPage.getButtonMessageVersion().getDocumentButtonGoToPackageTitle(docPage.context);

            	out.write("<input type=\"submit\" name=\"");
            	out.write(DocumentPageBean.DOCUMENT_VIEW_PACKAGE_BUTTON_PREFIX);
            	out.write(buttonSuffix);
            	out.write("\" value=\"");
            	out.write(HtmlUtil.toEscapedHtml(returnToPackageButtonLabel));
            	out.write("\" title=\"");
            	out.write(HtmlUtil.toEscapedHtml(returnToPackageButtonTitle));
            	out.write("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");

            	out.write("</p>");
            }
            out.write("<div class=\"documentAndPageNumbersInfo\">");
            out.write(HtmlUtil.toDisplayHtml(docPage.getDocumentAndPageNumbersInfo()));
            out.write("</div>\n");
        }
        out.write(Literals.JSP_END_REMOVE_COMMENT);
    }
}
