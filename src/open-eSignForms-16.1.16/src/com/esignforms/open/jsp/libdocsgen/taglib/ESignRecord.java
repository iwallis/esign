// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen.taglib;

import java.util.List;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.esignforms.open.Application;
import com.esignforms.open.config.MessageFormatFile;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.HtmlUtil;

/**
 * This class is used to display the electronic signature process record, if any, on review.
 * 
 * @author Yozons, Inc.
 */
public class ESignRecord 
    extends javax.servlet.jsp.tagext.SimpleTagSupport
{

    public void doTag() 
        throws java.io.IOException
    {
    	JspWriter out = getJspContext().getOut();

    	// The generated page should have initialized its DocumentPageBean
        DocumentPageBean docPage = (DocumentPageBean)getJspContext().getAttribute(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId,PageContext.REQUEST_SCOPE);
        if ( docPage == null )
        {
        	Application.getInstance().err("ESignRecord tag could not find DocumentPageBean with id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId + " in the request scope.");
            return;
        }
        
        if ( ! docPage.isPageReview() && ! docPage.isPageViewOnly() )
        	return;
        
        if ( docPage.context.isMultiPageDocument() && ! docPage.context.isMultiPageReviewPage() )
        	return;
        
        // Don't show if it's set to not show it
        if ( docPage.context.currDocumentVersion.isEsignProcessRecordLocationNone() )
        	return;
        
        List<TransactionContext.SignedPartyInfo> signedPartyList = docPage.context.getAllSignedParties();
        if ( signedPartyList.size() == 0 )
        	return;
        
        MessageFormatFile mff = Application.getInstance().getServerMessages();
        
        if ( docPage.context.currDocumentVersion.isEsignProcessRecordLocationFinalOwnPage() )
        	out.write("<div class=\"newPage\">&nbsp;</div>");
        else
        	out.write("<div>&nbsp;</div>");
        
        out.write("<div class=\"signatureBlock signatureMetadata\">");
        
        if ( docPage.context.isProductionTransaction() )
        {
            out.write(mff.getString("ESignRecord.headerHtml"));
            out.write(mff.getString("ESignRecord.noticeHtml"));
        }
        else
        {
            out.write(mff.getString("ESignRecord.headerHtml.testTran"));
            out.write(mff.getString("ESignRecord.noticeHtml.testTran"));
        }
        
        out.write("<p><span title=\"");
        	out.write(mff.getString("ESignRecord.yuid.spanTitle"));
        	out.write("\">YUID: <b>");
        	out.write(docPage.context.transactionId.toHtml());
        	out.write("-");
        	out.write(docPage.context.currDocumentVersion.getId().toHtml());
        out.write("</b></span></p>");
        
        for( TransactionContext.SignedPartyInfo signedParty : signedPartyList )
        {
            out.write("<p>");
            
            out.write(mff.getString("ESignRecord.signingParty.htmlLabel"));
            out.write(" ");
            out.write(HtmlUtil.toDisplayHtml(signedParty.displayName));
            
            out.write("<br />");
            out.write(mff.getString("ESignRecord.signingPartyId.htmlLabel"));
            out.write(" ");
            out.write(signedParty.packagePartyId.toHtml());
        	
            out.write("<br />");
            out.write(mff.getString("ESignRecord.ipAddress.htmlLabel"));
            out.write(" ");
            out.write(HtmlUtil.toDisplayHtml(signedParty.signIpHost));
        	
            out.write("<br />");
            out.write(mff.getString("ESignRecord.timestamp.htmlLabel"));
            out.write(" ");
            out.write(HtmlUtil.toDisplayHtml(signedParty.signTimestamp.toLogString()));
           	out.write(" (");
           	out.write(HtmlUtil.toDisplayHtml(signedParty.signTimestamp.toDateTimeMsecZuluString()));
           	out.write(")");
        	
            if ( EsfString.isNonBlank(signedParty.emailAddress) )
            {
        		out.write("<br />");
   				out.write(mff.getString("ESignRecord.email.htmlLabel"));
   				out.write(" ");
       			out.write(HtmlUtil.toDisplayHtml(signedParty.emailAddress));
            }
        	
            if ( EsfString.isNonBlank(signedParty.userInfo) )
        	{
               	out.write("<br />");
				out.write(mff.getString("ESignRecord.user.htmlLabel"));
				out.write(" ");
           		out.write(HtmlUtil.toDisplayHtml(signedParty.userInfo));
        	}
            
            out.write("</p>");
        }
        
        out.write("</div>");
        
    }
}
