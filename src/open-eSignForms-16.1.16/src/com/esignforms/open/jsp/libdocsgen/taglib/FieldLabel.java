// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen.taglib;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import org.vaadin.openesignforms.ckeditor.CKEditorConfig;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfDecimal;
import com.esignforms.open.data.EsfHtml;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfMoney;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.jsp.libdocsgen.DateSelectorCalendar;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.DropDownVersionOption;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.FileVersion;
import com.esignforms.open.prog.Library;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionFile;
import com.esignforms.open.user.User;
import com.esignforms.open.util.CreditCardValidator;
import com.esignforms.open.util.DateUtil;
import com.esignforms.open.util.HtmlUtil;
import com.esignforms.open.util.ServletUtil;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

/**
 * This class is used to display on optional label and the field, in either input or display mode depending on the party and page mode.
 * It is called by the standard ${fieldname} syntax (or the uncommon, verbose ${fieldlabel:fieldname}.
 * 
 * @author Yozons, Inc.
 */
public class FieldLabel
    extends javax.servlet.jsp.tagext.SimpleTagSupport
{
    private String name;
    private String source;
    private String fieldSpec; // we display this if the 'name' cannot be determined

	public void setName(String str)
	{
		name = str;
	}

	public void setSource(String str)
	{
		source = str;
	}

	public void setFieldSpec(String str)
	{
		fieldSpec = str;
	}

	public void doTag() 
        throws java.io.IOException
    {
		Application app = Application.getInstance();

    	JspWriter out = getJspContext().getOut();
    	
    	// The generated page should have initialized its DocumentPageBean
        DocumentPageBean docPage = (DocumentPageBean)getJspContext().getAttribute(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId,PageContext.REQUEST_SCOPE);
        if ( docPage == null )
        {
            app.err("FieldLabel tag could not find DocumentPageBean with id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId + " in the request scope.");
            if ( EsfString.isNonBlank(fieldSpec) ) {
            	out.write("<span class=\"viewFieldData-error\">");
            	out.write(fieldSpec);
            	out.write("</span>");            	
            }
            return;
        }
        
        EsfName docName = new EsfName(source);
        EsfName fieldName = new EsfName(name);
        
        out.write(docPage.buildFieldLabelHtml(getJspContext(), docName, fieldName, fieldSpec, false));
     }
	
	// transaction and documentVersion may be null if not known
	public static String getFormattedFieldValue(User user, Transaction transaction, DocumentVersion documentVersion, FieldTemplate fieldTemplate, EsfValue fieldValue)
	{
		Application app = Application.getInstance();

		if ( fieldValue == null )
			fieldValue = new EsfString();
		
    	String formattedFieldValue = fieldValue.toString();
		if ( EsfString.isBlank(formattedFieldValue) )
			formattedFieldValue = "";

		if ( fieldTemplate.isTypeCreditCard() )
		{
			if ( fieldValue instanceof EsfString && ! fieldValue.isNull() )
				formattedFieldValue = formatCreditCard(fieldValue.toPlainString(),fieldTemplate);
			else
				formattedFieldValue = "";
		}
		else if ( fieldTemplate.isTypeDate() )
		{
			if ( fieldValue instanceof EsfDate && ! fieldValue.isNull() )
			{
    			String dateFormatSpec = fieldTemplate.hasOutputFormatSpec() ? fieldTemplate.getOutputFormatSpec() : app.getDefaultDateFormat();
    			formattedFieldValue = ((EsfDate)fieldValue).format(dateFormatSpec);
			}
			else // date fields that aren't dates are invalid and shouldn't be shown
				formattedFieldValue = "";
		}
		else if ( fieldTemplate.isTypeDateTime() )
		{
			if ( fieldValue instanceof EsfDateTime && ! fieldValue.isNull() )
			{
    			String dateTimeFormatSpec = fieldTemplate.hasOutputFormatSpec() ? fieldTemplate.getOutputFormatSpec() : app.getDefaultDateTimeFormat();
    			formattedFieldValue = ((EsfDateTime)fieldValue).format(dateTimeFormatSpec);
			}
			else // date-time fields that aren't datetimes are invalid and shouldn't be shown
				formattedFieldValue = "";
		}
		else if ( fieldTemplate.isTypeDecimal() )
		{
			if ( fieldValue instanceof EsfDecimal && ! fieldValue.isNull() )
			{
				EsfDecimal decimalValue = (EsfDecimal)fieldValue;
				formattedFieldValue = decimalValue.format(fieldTemplate.getOutputFormatSpec());
			}
		}
		else if ( fieldTemplate.isTypeFile() )
		{
			if ( transaction != null && documentVersion != null )
			{
				TransactionDocument tranDoc = transaction.getTransactionDocumentByDocumentId(documentVersion.getDocumentId());
				if ( tranDoc != null )
				{
					List<TransactionFile> fileList = transaction.getAllTransactionFilesForDocumentField(tranDoc.getId(), fieldTemplate.getEsfName());
					if ( fileList == null || fileList.size() == 0 )
						formattedFieldValue = "";
					else
					{
						StringBuilder buf = new StringBuilder(fileList.size() * 128);
						for( TransactionFile tranFile : fileList )
						{
							if ( buf.length() > 0 )
								buf.append(", ");
							buf.append(tranFile.getFileName());
						}
						formattedFieldValue = buf.toString();
					}
				}
			}
		}
    	else if ( fieldTemplate.isTypeFileConfirmClick() )
    	{
    		// If the value is set to an EsfName, we use that name for the file. Otherwise we'll default to how it was originally configured.
    		EsfName fileName = (fieldValue instanceof EsfName) ? (EsfName)fieldValue : new EsfName(fieldTemplate.getInputFormatSpec());
    		FileVersion fileVersion = transaction.getFileVersion(fileName,documentVersion);
    		if ( fileVersion == null )
    			formattedFieldValue = "";
    		else
    			formattedFieldValue = fileVersion.getFileFileName();
    	}
		else if ( fieldTemplate.isTypeGeneral() )
		{
			if ( ! formattedFieldValue.equals("") ) // no output formatting needed if just empty
				formattedFieldValue = formatGeneral(fieldValue.toPlainString(),fieldTemplate);     
		}
		else if ( fieldTemplate.isTypeInteger() )
		{
			if ( fieldValue instanceof EsfInteger && ! fieldValue.isNull() )
			{
				EsfInteger integerValue = (EsfInteger)fieldValue;
				formattedFieldValue = integerValue.format(fieldTemplate.getOutputFormatSpec());
				if ( "ordinal".equals(fieldTemplate.getExtraOptions()) )
					formattedFieldValue = app.addOrdinalSuffixToNum(formattedFieldValue);
			}
		}
		else if ( fieldTemplate.isTypeMoney() )
		{
			if ( fieldValue instanceof EsfMoney && ! fieldValue.isNull() )
			{
				EsfMoney moneyValue = (EsfMoney)fieldValue;
				formattedFieldValue = moneyValue.format(fieldTemplate.getOutputFormatSpec());
			}
		}
		else if ( fieldTemplate.isTypePhone() )
		{
			if ( fieldValue instanceof EsfString && ! fieldValue.isNull() )
				formattedFieldValue = formatPhone(fieldValue.toPlainString(),fieldTemplate);
		}
    	else if ( fieldTemplate.isTypeRichTextarea() )
    	{
    		if ( fieldValue.isNull() )
    			formattedFieldValue = "";
    	}
		else if ( fieldTemplate.isTypeSelection() )
		{
			if ( transaction != null && documentVersion != null )
			{
	    		DropDownVersion ddv;
	    		// If the drop down has extra options, let's see if we can find the drop down using the name specified. If not, we'll just stick with the hard-configured one.
	    		if ( fieldTemplate.hasExtraOptions() )
	    		{
	    			String ddName = transaction.getFieldSpecWithSubstitutions(user,documentVersion,fieldTemplate.getExtraOptions());
	    			ddv = transaction.getDropDownVersion(new EsfName(ddName),documentVersion);
	        		if ( ddv == null )
	        		{
						app.warning("FieldLabel.getFormattedFieldValue() - Could not find dynamic dropdown box for field: " + fieldTemplate.getEsfName() + 
								"; Dynamic DropDown name spec: " + fieldTemplate.getExtraOptions() + "; expanded to drop down name: " + ddName +
								"; tranId: " + transaction.getId() + 
								"; docVersionId: " + documentVersion.getId());
	        			ddv = transaction.getDropDownVersion(new EsfName(fieldTemplate.getInputFormatSpec()),documentVersion);
	        		}
	    		}
	    		else
	    			ddv = transaction.getDropDownVersion(new EsfName(fieldTemplate.getInputFormatSpec()),documentVersion);
	    		if ( ddv == null )
	    		{
	    		}
	    		else if ( ddv.isAllowMultiSelection() )
	    		{
	    			EsfValue[] fieldValues = transaction.getFieldValues(documentVersion.getDocument().getEsfName(),fieldTemplate.getEsfName());
	    			if ( fieldValues != null && fieldValues.length > 0 )
	    			{
		    			StringBuilder buf = new StringBuilder(fieldValues.length * 100);
		    			for( EsfValue value : fieldValues )
		    			{
		    				if ( buf.length() > 0 )
		    					buf.append(", ");
		    				if ( "option".equals(fieldTemplate.getOutputFormatSpec()) )
		    				{
		    					String ddVal = ddv.getOptionByValue(value.toString());
			    				if ( EsfString.isBlank(ddVal) )
			    					ddVal = "";
		    					buf.append(ddVal);
		    				}
		    				else
		    					buf.append(value.toString());
		    			}
		    			formattedFieldValue = buf.toString();
	    			}
	    			else
	    				formattedFieldValue = "";
	    		} 
	    		else
	    		{
	    			if ( "option".equals(fieldTemplate.getOutputFormatSpec()) )
	    			{
	    				formattedFieldValue = ddv.getOptionByValue(fieldValue.toString());
	    				if ( EsfString.isBlank(formattedFieldValue) )
	    					formattedFieldValue = "";
	    			}
	    		}
			}
		}
		else if ( fieldTemplate.isTypeSsnEin() )
		{
			if ( fieldValue instanceof EsfString && ! fieldValue.isNull() )
			{
				formattedFieldValue = formatSsnEin(fieldValue.toPlainString(),fieldTemplate);
			}
		}
		
		if ( EsfString.isBlank(formattedFieldValue) && fieldTemplate.isDisplayEmptyValueEnabled() )
		{
			if ( transaction != null && documentVersion != null )
			{
				formattedFieldValue = transaction.getFieldSpecWithSubstitutions(user,documentVersion,fieldTemplate.getDisplayEmptyValue());
				if ( EsfString.isBlank(formattedFieldValue) )
					formattedFieldValue = "";
			}
		}
		
		return formattedFieldValue;
	}
	
	public static String formatDisplayField(DocumentPageBean docPage, EsfName docName, EsfName fieldName, String fieldSpec, String source, String fieldId, 
											FieldTemplate fieldTemplate, boolean isOverlayField, EsfValue fieldValue)
	{
		Application app = Application.getInstance();

    	String fieldHtml = fieldValue.toHtml();
		if ( EsfString.isBlank(fieldHtml) )
			fieldHtml = "&nbsp;";
		
		if ( fieldTemplate.isTypeCheckbox() )
		{
			String checkedImage, uncheckedImage;
			if ( isOverlayField )
			{
				checkedImage   = "<img src=\"./static/esf/check.png\" alt=\"Checked\" title=\"Checked\"/>";
				uncheckedImage = "&nbsp;&nbsp;";
			}
			else
			{
				checkedImage   = "<img src=\"./static/esf/check.png\" style=\"padding-bottom: 2px;\" alt=\"Checked\" title=\"Checked\"/>";
				uncheckedImage = "<img src=\"./static/esf/uncheck.png\" style=\"padding-bottom: 2px;\" alt=\"NOT checked\" title=\"NOT checked\"/>";
			}
    		if ( fieldTemplate.hasInputFormatSpec() )
    			fieldHtml = fieldTemplate.getInputFormatSpec().equals(fieldValue.toPlainString()) ? checkedImage : uncheckedImage;
    		else
    			fieldHtml = "Y".equals(fieldValue.toPlainString()) ? checkedImage : uncheckedImage;
		}
		else if ( fieldTemplate.isTypeCreditCard() )
		{
			if ( fieldValue instanceof EsfString && ! fieldValue.isNull() )
			{
				fieldHtml = formatCreditCardToHtml(fieldValue.toPlainString(),fieldTemplate);
			}
			else if ( docPage.getFieldValue(docName,fieldName) == null )
				fieldHtml = "&nbsp;";
		}
		else if ( fieldTemplate.isTypeDate() )
		{
			if ( fieldValue instanceof EsfDate && ! fieldValue.isNull() )
			{
    			String dateFormatSpec = fieldTemplate.hasOutputFormatSpec() ? fieldTemplate.getOutputFormatSpec() : app.getDefaultDateFormat();
				fieldHtml = ((EsfDate)fieldValue).formatToHtml(dateFormatSpec);
			}
			else // if missing or not valid, don't display any date
				fieldHtml = "&nbsp;";
		}
		else if ( fieldTemplate.isTypeDateTime() )
		{
			if ( fieldValue instanceof EsfDateTime && ! fieldValue.isNull() )
			{
				EsfDateTime dt = (EsfDateTime)fieldValue;
    			String dateTimeFormatSpec = fieldTemplate.hasOutputFormatSpec() ? fieldTemplate.getOutputFormatSpec() : app.getDefaultDateTimeFormat();
				fieldHtml = docPage.isUserLoggedIn() ? dt.formatToHtml(dateTimeFormatSpec) : dt.formatToHtml(dateTimeFormatSpec);
			}
			else // if missing or not valid, don't display any datetime
				fieldHtml = "&nbsp;";
		}
		else if ( fieldTemplate.isTypeDecimal() )
		{
			if ( fieldValue instanceof EsfDecimal && ! fieldValue.isNull() )
			{
				EsfDecimal decimalValue = (EsfDecimal)fieldValue;
				fieldHtml = decimalValue.formatToHtml(fieldTemplate.getOutputFormatSpec());
			}
			else if ( docPage.getFieldValue(docName,fieldName) == null )
				fieldHtml = "&nbsp;";
		}
		else if ( fieldTemplate.isTypeFile() )
		{
			// The tran can be deleted if the package+disclosure document has been misconfigured to include fields like this.
			if ( ! docPage.context.transaction.isDeleted() )
			{
				EsfValue blockViewDownload = docPage.context.transaction.getFieldValue(docPage.context.currDocument.getEsfName(), new EsfPathName(fieldName,docPage.context.pkgVersionPartyTemplate.getId().toEsfName(),FieldTemplate.VIEW_DOWNLOAD_PROHIBITED_PATH_SUFFIX));
				if ( blockViewDownload == null )
				{
					fieldHtml = createViewFileList(docPage,fieldId,fieldName,fieldTemplate);
					if ( EsfString.isBlank(fieldHtml) )
						fieldHtml = "&nbsp;";
				}
				else
					fieldHtml = "&nbsp;";
			}
		}
		else if ( fieldTemplate.isTypeFileConfirmClick() )
		{
			// The tran can be deleted if the package+disclosure document has been misconfigured to include fields like this.
			if ( ! docPage.context.transaction.isDeleted() )
			{
	    		// If the value is set to an EsfName, we use that name for the file. Otherwise we'll default to how it was originally configured.
	    		EsfName fileName = (fieldValue instanceof EsfName) ? (EsfName)fieldValue : new EsfName(fieldTemplate.getInputFormatSpec());
				fieldHtml = createFileConfirmClickHtml(docPage,fieldId,fieldName,fileName,fieldTemplate,false);
				if ( EsfString.isBlank(fieldHtml) )
					fieldHtml = "&nbsp;";
			}
		}
		else if ( fieldTemplate.isTypeGeneral() )
		{
			if ( ! fieldHtml.equals("&nbsp;") ) // no output formatting needed if just empty
				fieldHtml = formatGeneralToHtml(fieldValue.toPlainString(),fieldTemplate);     
		}
		else if ( fieldTemplate.isTypeInteger() )
		{
			if ( fieldValue instanceof EsfInteger && ! fieldValue.isNull() )
			{
				EsfInteger integerValue = (EsfInteger)fieldValue;
				fieldHtml = integerValue.format(fieldTemplate.getOutputFormatSpec());
				if ( "ordinal".equals(fieldTemplate.getExtraOptions()) )
					fieldHtml = app.addOrdinalSuffixToNum(fieldHtml);
				fieldHtml = HtmlUtil.toEscapedHtml(fieldHtml);
			}
			else if ( docPage.getFieldValue(docName,fieldName) == null )
				fieldHtml = "&nbsp;";
		}
		else if ( fieldTemplate.isTypeMoney() )
		{
			if ( fieldValue instanceof EsfMoney && ! fieldValue.isNull() )
			{
				EsfMoney moneyValue = (EsfMoney)fieldValue;
				fieldHtml = moneyValue.formatToHtml(fieldTemplate.getOutputFormatSpec());
			}
			else if ( docPage.getFieldValue(docName,fieldName) == null )
				fieldHtml = "&nbsp;";
		}
		else if ( fieldTemplate.isTypePhone() )
		{
			if ( fieldValue instanceof EsfString && ! fieldValue.isNull() )
			{
				fieldHtml = formatPhoneToHtml(fieldValue.toPlainString(),fieldTemplate);
			}
			else if ( docPage.getFieldValue(docName,fieldName) == null )
				fieldHtml = "&nbsp;";
		}
		else if ( fieldTemplate.isTypeRadioButton() )
		{
			String radioButtonName = fieldTemplate.getOutputFormatSpec();
			EsfValue radioFieldValue = docPage.getFieldValue(docName,new EsfName(radioButtonName),fieldSpec);
			String checkValue = EsfString.isBlank(fieldTemplate.getInputFormatSpec()) ? fieldTemplate.getEsfName().toPlainString() : fieldTemplate.getInputFormatSpec();
			String selectedImage, unselectedImage;
			if ( isOverlayField )
			{
				selectedImage   = "<img src=\"./static/esf/radioSelect.png\" alt=\"Selected\" title=\"Selected\"/>";
				unselectedImage = "&nbsp;&nbsp;";
			}
			else
			{
				selectedImage   = "<img src=\"./static/esf/radioSelect.png\" style=\"padding-bottom: 2px;\" alt=\"Selected\" title=\"Selected\"/>";
				unselectedImage = "<img src=\"./static/esf/radioUnselect.png\" style=\"padding-bottom: 2px;\" alt=\"NOT selected\" title=\"NOT selected\"/>";
			}
			fieldHtml = checkValue.equals(radioFieldValue.toPlainString()) ? selectedImage : unselectedImage;
		}
    	else if ( fieldTemplate.isTypeRichTextarea() )
    	{
    		if ( fieldValue.isNull() )
    			fieldHtml = "";
    		else
    		{
    			if ( fieldValue instanceof EsfHtml ) // we do this because EsfHtml is also EsfString, so we test it first
    				fieldHtml = fieldValue.toHtml();
    			else if ( fieldValue instanceof EsfString )
    				fieldHtml = ((EsfString)fieldValue).toDisplayHtml();
    			else
    				fieldHtml = fieldValue.toHtml();
    		}
    	}
		else if ( fieldTemplate.isTypeSelection() )
		{
    		DropDownVersion ddv;
    		// If the drop down has extra options, let's see if we can find the drop down using the name specified. If not, we'll just stick with the hard-configured one.
    		if ( fieldTemplate.hasExtraOptions() )
    		{
    			String ddName = docPage.context.transaction.getFieldSpecWithSubstitutions(docPage.getLoggedInUser(),docPage.context.currDocumentVersion,fieldTemplate.getExtraOptions());
    			ddv = docPage.context.transaction.getDropDownVersion(new EsfName(ddName),docPage.context.currDocumentVersion);
        		if ( ddv == null )
        		{
					docPage.warning("FieldLabel.formatDisplayField() - Could not find dynamic dropdown box for field: " + fieldTemplate.getEsfName() + 
							"; Dynamic DropDown name spec: " + fieldTemplate.getExtraOptions() + "; expanded to drop down name: " + ddName +
							"; tranId: " + docPage.context.transactionId + "; packageId: " + docPage.context.pkg.getId() + "; packageParty: " + docPage.context.pkgVersionPartyTemplate.getEsfName() + 
							"; docId: " + docPage.context.currDocument.getId() + "; docParties: " + docPage.context.currDocumentVersionPartyNameList);
        			ddv = docPage.context.transaction.getDropDownVersion(new EsfName(fieldTemplate.getInputFormatSpec()),docPage.context.currDocumentVersion);
        		}
    		}
    		else
    			ddv = docPage.context.transaction.getDropDownVersion(new EsfName(fieldTemplate.getInputFormatSpec()),docPage.context.currDocumentVersion);
    		if ( ddv == null )
    		{
				app.warning("FieldLabel.formatDisplayField() - Unexpectedly missing dropdown box for field: " + fieldTemplate.getEsfName() + 
						"; DropDown: " + fieldTemplate.getInputFormatSpec() +
						"; tranId: " + docPage.context.transactionId + "; packageId: " + docPage.context.pkg.getId() + "; packageParty: " + docPage.context.pkgVersionPartyTemplate.getEsfName() + 
						"; docId: " + docPage.context.currDocument.getId() + "; docParties: " + docPage.context.currDocumentVersionPartyNameList);
    		}
    		else if ( ddv.isAllowMultiSelection() )
    		{
    			StringBuilder buf = new StringBuilder(1024);
    			EsfValue[] fieldValues = docPage.getFieldValues(docName,fieldName,fieldSpec);
    			for( EsfValue value : fieldValues )
    			{
    				if ( buf.length() > 0 )
    					buf.append(", ");
    				if ( "option".equals(fieldTemplate.getOutputFormatSpec()) )
    					buf.append(ddv.getOptionByValue(value.toString()));
    				else
    					buf.append(value.toString());
    			}
    			fieldHtml = HtmlUtil.toDisplayHtml(buf.toString());
    		} 
    		else
    		{
    			if ( fieldTemplate.getOutputFormatSpec().equals("option") )
    			{
    				fieldHtml = HtmlUtil.toDisplayHtml(ddv.getOptionByValue(fieldValue.toString()));
    			}
    		}
		}
		else if ( fieldTemplate.isTypeSignature() )
		{
		}
		else if ( fieldTemplate.isTypeSignDate() )
		{
			// Only show the date if the related signature field has data inside it
			String fieldNameString = fieldTemplate.getEsfName().toPlainString();
			boolean showSignDate = true;
			if ( fieldNameString.toLowerCase().endsWith("date") ) 
			{
    			fieldNameString = fieldNameString.substring(0,fieldNameString.length()-4); // remove the Date suffix
				EsfName sourceName = new EsfName(source);
				EsfName signatureFieldName = new EsfName(fieldNameString);
				
    			FieldTemplate checkSignatureField = docPage.getFieldTemplate(sourceName, signatureFieldName, null );
    			if ( checkSignatureField != null && checkSignatureField.isTypeSignature() )
    			{
    				// Let's only show it if that field has some data in it
    				EsfString signatureValue = (EsfString)docPage.getFieldValue(sourceName, signatureFieldName);
    				showSignDate = signatureValue != null && signatureValue.isNonBlank();
    			}
			}
			if ( fieldValue instanceof EsfDate && showSignDate )
			{
    			String dateFormatSpec = fieldTemplate.hasOutputFormatSpec() ? fieldTemplate.getOutputFormatSpec() : app.getDefaultDateFormat();
				fieldHtml = ((EsfDate)fieldValue).formatToHtml(dateFormatSpec);
			}
			else if ( docPage.getFieldValue(docName,fieldName) == null )
				fieldHtml = "&nbsp;";
		} 
		else if ( fieldTemplate.isTypeSsnEin() )
		{
			if ( fieldValue instanceof EsfString && ! fieldValue.isNull() )
			{
				fieldHtml = formatSsnEinToHtml(fieldValue.toPlainString(),fieldTemplate);
			}
			else if ( docPage.getFieldValue(docName,fieldName) == null )
				fieldHtml = "&nbsp;";
		}
    	else if ( fieldTemplate.isTypeTextarea() )
    	{
    		fieldHtml = fieldValue instanceof EsfString ? ((EsfString)fieldValue).toDisplayHtml() : fieldValue.toHtml();
    	}
		
		if ( EsfHtml.isBlankOrNbsp(fieldHtml) && fieldTemplate.isDisplayEmptyValueEnabled() )
		{
			fieldHtml = docPage.getFieldSpecWithSubstitutions(fieldTemplate.getDisplayEmptyValue());
			if ( fieldHtml == null )
				fieldHtml = "";
		}
		
		return fieldHtml;
	}

	public static String createSelectHtml(DropDownVersion ddv,String fieldId,EsfName fieldName,String inputClass,String titleAttr,String autoCompleteAttr,boolean isAutoPost,EsfValue value)
	{
		StringBuilder buf = new StringBuilder(1024);
		buf.append("<select id=\"");
		buf.append(fieldId);
		buf.append("\" name=\"");
		buf.append(fieldName.toLowerCase());
		buf.append("\" class=\"");
		buf.append(inputClass);
		buf.append("\"");
		buf.append(titleAttr);
		buf.append(autoCompleteAttr);
		if ( isAutoPost )
			buf.append(" onchange=\"esf.autoPost(this)\"");
		buf.append(">");
		
		for( DropDownVersionOption option : ddv.getOptions() )
		{
			buf.append("<option value=\"");
			buf.append(HtmlUtil.toDisplayHtml(option.getValue()));
			buf.append("\"");
			if ( option.getValue().equals(value.toString()) )
				buf.append(" selected=\"selected\"");
			buf.append(">");
			buf.append(HtmlUtil.toDisplayHtml(option.getOption()));
			buf.append("</option>");
		}
		
		buf.append("</select>");
		return buf.toString();
	}

	
	public static String createMultiSelectHtml(DropDownVersion ddv,String fieldId,EsfName fieldName,String inputClass,String titleAttr,String autoCompleteAttr,boolean isAutoPost,EsfValue[] values)
	{
		StringBuilder buf = new StringBuilder(1024);
		buf.append("<select multiple=\"multiple\" id=\"");
		buf.append(fieldId);
		buf.append("\" name=\"");
		buf.append(fieldName.toLowerCase());
		buf.append("\" class=\"");
		buf.append(inputClass);
		buf.append("\"");
		buf.append(titleAttr);
		buf.append(autoCompleteAttr);
		if ( isAutoPost )
			buf.append(" onchange=\"esf.autoPost(this)\"");
		buf.append(">");
		
		for( DropDownVersionOption option : ddv.getOptions() )
		{
			buf.append("<option value=\"");
			buf.append(HtmlUtil.toDisplayHtml(option.getValue()));
			buf.append("\"");
			boolean isSelected = false;
			for( EsfValue value : values )
			{
				if ( option.getValue().equals(value.toString()) )
				{
					isSelected = true;
					break;
				}
			}
			if ( isSelected )
				buf.append(" selected=\"selected\"");
			buf.append(">");
			buf.append(HtmlUtil.toDisplayHtml(option.getOption()));
			buf.append("</option>");
		}
		
		buf.append("</select>");
		return buf.toString();
	}
	
	
	public static String createDateHtml(JspContext jspContext,DocumentPageBean docPage,FieldTemplate fieldTemplate,String fieldId,EsfName fieldName,String inputClass,
										String titleAttr,String placeholderAttr,String autoCompleteAttr,EsfValue value)
	{
		String imageButtonId = fieldId + "Button";
		
		String calValue = "";
		
		if ( value != null && ! value.isNull() )
		{
			if ( value instanceof EsfString )
				calValue = value.toString();
			else if ( value instanceof EsfDate ) // normal
			{
				EsfDate dateValue = (EsfDate)value;
				if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_MM_DD_YYYY) )
					calValue = dateValue.toMDYString();
				else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_DD_Mon_YYYY) )
					calValue = dateValue.toDDMonYYYYString();
				else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_DD_MM_YYYY) )
					calValue = dateValue.toDDMMYYYYString();
				else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_YYYY_MM_DD) )
					calValue = dateValue.toYMDString();
				else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.ISO_FORMAT_DD_MM_YYYY) )
					calValue = dateValue.toIsoDDMMYYYYString();
				else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_MM_YYYY) )
					calValue = dateValue.toMMYYYYString();
			}
		}
		
		StringBuilder buf = new StringBuilder(1024);
		buf.append("<input type=\"text\" id=\"");
		buf.append(fieldId);
		buf.append("\" name=\"");
		buf.append(fieldName.toLowerCase());
		buf.append("\" value=\"");
		buf.append(calValue);
		buf.append("\" class=\"");
		buf.append(inputClass);
		buf.append("\" maxlength=\"");
		buf.append(fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_DD_Mon_YYYY) ? "11" : "10");
		buf.append("\"");
		
		if ( fieldTemplate.isWidthUnitAuto() )
			buf.append(" size=\"12\"");
		
		buf.append(titleAttr);
		buf.append(placeholderAttr);
		buf.append(autoCompleteAttr);
		buf.append("/>");
		
		buf.append("<img src=\"");
		buf.append(docPage.getContextPath());
		buf.append("/static/calendar/calendar20x14.gif\" width=\"20\" height=\"14\" id=\"");
		buf.append(imageButtonId);
		buf.append("\" style=\"margin-left: -20px; cursor: pointer; border: none;\" title=\"Date selector - "); // we move the image back into the input field
		//buf.append("\" style=\"cursor: pointer; border: 1px solid #105AA5;\" title=\"Date selector - ");
		buf.append(DateSelectorCalendar.getCaptionForFormat(fieldTemplate.getInputFormatSpec()));
		buf.append("\" alt=\"Date selector\" ");
		buf.append("onmouseover=\"this.style.background='#105AA5';\" onmouseout=\"this.style.background=''\" />");
        
        DateSelectorCalendar cal = new DateSelectorCalendar();
        cal.inputFieldId = fieldId;
        cal.buttonId = imageButtonId;
        cal.format = fieldTemplate.getInputFormatSpec();
        
        Object obj = jspContext.getAttribute(DateSelectorCalendar.LIST_ATTRIBUTE,javax.servlet.jsp.PageContext.REQUEST_SCOPE);
        if ( obj == null )
        	Application.getInstance().err("FieldLabel.createDateHtml() missing request attribute: " + DateSelectorCalendar.LIST_ATTRIBUTE);
        else
        {
            @SuppressWarnings("unchecked")
			ArrayList<DateSelectorCalendar> list = (ArrayList<DateSelectorCalendar>)obj;
            list.add(cal);
        }

        return buf.toString();
	}
	
	
	public static String createDateTimeHtml(JspContext jspContext,DocumentPageBean docPage,FieldTemplate fieldTemplate,String fieldId,EsfName fieldName,String inputClass,
			String titleAttr,String placeholderAttr,String autoCompleteAttr,String positionAttrs,EsfValue value)
	{
		String hourName = fieldId + "_HH";
		String minuteName = fieldId + "_MM";
		String secondName = fieldId + "_SS";
		String timezoneName = fieldId + "_TZ";
		
		String imageButtonId = fieldId + "Button";
		
		String calValue = "";
		String hourValue = "";
		String minuteValue = "";
		String secondValue = "";
		String timezoneValue = docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getTimezone().toString() : com.esignforms.open.util.DateUtil.getDefaultTimeZone().toString();
		
		if ( value != null && ! value.isNull() )
		{
			if ( value instanceof EsfString ) 
			{
				String[] dateTimeTzString = value.toPlainString().split(" ");
				calValue = dateTimeTzString.length > 0 ? dateTimeTzString[0] : "";
				
				String[] timeString = dateTimeTzString.length > 1 ? dateTimeTzString[1].split(":") : new String[0];
				hourValue 	= timeString.length > 0 ? timeString[0] : "";
				minuteValue = timeString.length > 1 ? timeString[1] : "";
				secondValue = timeString.length > 2 ? timeString[2] : "";
				
				timezoneValue = dateTimeTzString.length > 2 ? dateTimeTzString[2] : timezoneValue;
			}
			else if ( value instanceof EsfDateTime ) // normal
			{
				EsfDateTime dateTimeValue = (EsfDateTime)value;
				EsfDate dateValue = new EsfDate(dateTimeValue);
				if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_MM_DD_YYYY) )
					calValue = dateValue.toMDYString();
				else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_DD_Mon_YYYY) )
					calValue = dateValue.toDDMonYYYYString();
				else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_DD_MM_YYYY) )
					calValue = dateValue.toDDMMYYYYString();
				else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_YYYY_MM_DD) )
					calValue = dateValue.toYMDString();
				else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.ISO_FORMAT_DD_MM_YYYY) )
					calValue = dateValue.toIsoDDMMYYYYString();
				else if ( fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_MM_YYYY) )
					calValue = dateValue.toMMYYYYString();
				
				String timeString = dateTimeValue.to24HourTimeZoneString();
				hourValue = timeString.substring(0, 2);
				minuteValue = timeString.substring(3, 5);
				secondValue = timeString.substring(6, 8);
				timezoneValue = DateUtil.fixupTimeZone(timeString.substring(9));
			}
		}
		
		StringBuilder buf = new StringBuilder(5600);
		buf.append("<input type=\"text\" id=\"");
		buf.append(fieldId);
		buf.append("\" name=\"");
		buf.append(fieldName.toLowerCase());
		buf.append("\" value=\"");
		buf.append(calValue);
		buf.append("\" style=\"width: auto;\" class=\"");
		buf.append(inputClass);
		buf.append("\" maxlength=\"");
		buf.append(fieldTemplate.getInputFormatSpec().equals(DateSelectorCalendar.FORMAT_DD_Mon_YYYY) ? "11" : "10");
		buf.append("\" size=\"12\""); // Unlike a date, we'll keep our input sizes compact no matter what they specified because 100% wide on a date input plus 4 selects doesn't work.
		
		buf.append(titleAttr);
		buf.append(placeholderAttr);
		buf.append(autoCompleteAttr);
		if ( positionAttrs != null )
			buf.append(positionAttrs);
		buf.append("/>");
		
		buf.append("<img src=\"");
		buf.append(docPage.getContextPath());
		buf.append("/static/calendar/calendar20x14.gif\" width=\"20\" height=\"14\" id=\"");
		buf.append(imageButtonId);
		buf.append("\" style=\"margin-left: -20px; cursor: pointer; border: none;\" title=\"Date selector - "); // we move the image back into the input field
		buf.append(DateSelectorCalendar.getCaptionForFormat(fieldTemplate.getInputFormatSpec()));
		buf.append("\" alt=\"Date selector\" ");
		buf.append("onmouseover=\"this.style.background='#105AA5';\" onmouseout=\"this.style.background=''\" />");
		
		// Add in select box for hours
		buf.append(" <select name=\"");
		buf.append(hourName);
		buf.append("\" style=\"width: auto;\" class=\"");
		buf.append(inputClass);
		buf.append("\">");
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "HH", "") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "00 (AM)", "00") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "01 (AM)", "01") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "02 (AM)", "02") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "03 (AM)", "03") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "04 (AM)", "04") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "05 (AM)", "05") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "06 (AM)", "06") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "07 (AM)", "07") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "08 (AM)", "08") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "09 (AM)", "09") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "10 (AM)", "10") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "11 (AM)", "11") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "12 (Noon)", "12") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "13 (1 PM)", "13") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "14 (2 PM)", "14") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "15 (3 PM)", "15") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "16 (4 PM)", "16") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "17 (5 PM)", "17") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "18 (6 PM)", "18") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "19 (7 PM)", "19") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "20 (8 PM)", "20") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "21 (9 PM)", "21") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "22 (10 PM)", "22") );
		buf.append( ServletUtil.makeOptionOptionValue(hourValue, "23 (11 PM)", "23") );
		buf.append("</select>");
		
		// Add in select box for minutes
		buf.append(":<select name=\"");
		buf.append(minuteName);
		buf.append("\" style=\"width: auto;\" class=\"");
		buf.append(inputClass);
		buf.append("\">");
		buf.append( ServletUtil.makeOptionOptionValue(minuteValue, "MM", "") );
		for( int i=0; i < 60; ++i )
		{
			String mm = docPage.app.make2digits(i);
			buf.append( ServletUtil.makeOptionOptionValue(minuteValue, mm, mm) );
		}
		buf.append("</select>");
		
		if ( fieldTemplate.getOutputFormatSpec().contains("s") )
		{
			// Add in select box for seconds
			buf.append(":<select name=\"");
			buf.append(secondName);
			buf.append("\" style=\"width: auto;\" class=\"");
			buf.append(inputClass);
			buf.append("\">");
			buf.append( ServletUtil.makeOptionOptionValue(secondValue, "SS", "") );
			for( int i=0; i < 60; ++i )
			{
				String ss = docPage.app.make2digits(i);
				buf.append( ServletUtil.makeOptionOptionValue(secondValue, ss, ss) );
			}
			buf.append("</select>");
		}
		else
			buf.append("<input type=\"hidden\" name=\"").append(secondName).append("\" value=\"").append(secondValue).append("\" />");
		
		if ( fieldTemplate.getOutputFormatSpec().contains("z") )
		{
			// Add in select box for the timezone
			DropDown tzDropDown = DropDown.Manager.getByName(Library.Manager.getTemplate().getId(), docPage.app.getDropDownTimeZoneEsfName());
			DropDownVersion tzDropDownVersion = tzDropDown.getProductionDropDownVersion();

			buf.append(" <select name=\"");
			buf.append(timezoneName);
			buf.append("\" style=\"width: auto;\" class=\"");
			buf.append(inputClass);
			buf.append("\">");
			for( DropDownVersionOption option : tzDropDownVersion.getOptions() )
			{
				buf.append( ServletUtil.makeOptionOptionValue(timezoneValue, option.getOption(), option.getValue()) );
			}
			buf.append("</select>");
		}
		else
			buf.append("<input type=\"hidden\" name=\"").append(timezoneName).append("\" value=\"").append(timezoneValue).append("\" />");
		
		DateSelectorCalendar cal = new DateSelectorCalendar();
		cal.inputFieldId = fieldId;
		cal.buttonId = imageButtonId;
		cal.format = fieldTemplate.getInputFormatSpec();
		
		Object obj = jspContext.getAttribute(DateSelectorCalendar.LIST_ATTRIBUTE,javax.servlet.jsp.PageContext.REQUEST_SCOPE);
		if ( obj == null )
			Application.getInstance().err("FieldLabel.createDateHtml() missing request attribute: " + DateSelectorCalendar.LIST_ATTRIBUTE);
		else
		{
			@SuppressWarnings("unchecked")
			ArrayList<DateSelectorCalendar> list = (ArrayList<DateSelectorCalendar>)obj;
			list.add(cal);
		}
		
		return buf.toString();
	}


	public static String createRichTextareaHtml(JspContext jspContext,DocumentPageBean docPage,FieldTemplate fieldTemplate,String fieldId,EsfName fieldName,String inputClass,
											String titleAttr,String autoCompleteAttr,EsfValue value)
	{
		EsfHtml htmlValue;
		
		if ( value != null && ! value.isNull() )
		{
			if ( value instanceof EsfString )
				htmlValue = new EsfHtml(value.toString());
			else if ( value instanceof EsfHtml ) // normal
				htmlValue = (EsfHtml)value;
			else 
				htmlValue = new EsfHtml("");
		}
		else 
			htmlValue = new EsfHtml("");
		
		EsfInteger numRows = new EsfInteger(fieldTemplate.getInputFormatSpec());
		if ( numRows.isNull() || numRows.isLessThan(5) )
			numRows.setValue(5);
		numRows.setValue( (long)(numRows.toLong() * 1.5) + 1 );
		
		StringBuilder buf = new StringBuilder(1024);
		buf.append("<textarea id=\"");
		buf.append(fieldId);
		buf.append("\" name=\"");
		buf.append(fieldName.toLowerCase());
		buf.append("\" class=\"");
		buf.append(inputClass);
		buf.append("\"");
		buf.append(titleAttr);
		buf.append(autoCompleteAttr);
		buf.append(">");
		buf.append(HtmlUtil.toEscapedHtml(htmlValue.toString()));
		buf.append("</textarea>");
		
    	CKEditorConfig config = new CKEditorConfig();
    	config.setupForOpenESignForms(docPage.getContextPath(),docPage.context.getCKEditorContext(docPage.session).getIdInSession(),null);
    	config.setHeight(numRows+"em");
    	
		// Activate CKEditor on this field
		buf.append("<script type=\"text/javascript\">");
		buf.append("CKEDITOR.replace('");
		buf.append(fieldId);
		buf.append("', ");
		buf.append(config.getInPageConfig());
		buf.append(" );");
		buf.append("</script>");
		
        return buf.toString();
	}

	
	public static String formatCreditCard(String value, FieldTemplate fieldTemplate)
	{
		if ( EsfString.isBlank(value) )
			return "";
		String valueNumbers = EsfString.getOnlyNumeric(value);
		if ( valueNumbers.length() < 4 )
			return value;
		if ( "normal".equals(fieldTemplate.getOutputFormatSpec()) )
			return CreditCardValidator.toPrettyString(value);
		if ( "masked".equals(fieldTemplate.getOutputFormatSpec()) )
			return Application.getInstance().maskCreditCard( valueNumbers.length() == 4 ? value : CreditCardValidator.toPrettyString(value) );
		if ( "last4".equals(fieldTemplate.getOutputFormatSpec()) )
			return valueNumbers.substring(valueNumbers.length()-4);
		return valueNumbers; // digits
	}
	
	public static String formatCreditCardToHtml(String value, FieldTemplate fieldTemplate)
	{
		value = formatCreditCard(value,fieldTemplate);
		return EsfString.isBlank(value) ? "&nbsp;" : HtmlUtil.toEscapedHtml(value);
	}
	
	
	public static String formatGeneral(String value, FieldTemplate fieldTemplate)
	{
		if ( EsfString.isBlank(value) )
			return "";
		if ( "fullmask".equals(fieldTemplate.getOutputFormatSpec()) )
			return Application.getInstance().mask(value);
		if ( "left4mask".equals(fieldTemplate.getOutputFormatSpec()) )
			return Application.getInstance().maskLeft(value);
		if ( "right4mask".equals(fieldTemplate.getOutputFormatSpec()) )
			return Application.getInstance().maskRight(value);
		return value; 
	}

	public static String formatGeneralToHtml(String value, FieldTemplate fieldTemplate)
	{
		value = formatGeneral(value,fieldTemplate);
		return EsfString.isBlank(value) ? "&nbsp;" : HtmlUtil.toEscapedHtml(value);
	}
	

	public static String formatPhone(String value, FieldTemplate fieldTemplate)
	{
		if ( EsfString.isBlank(value) )
			return "";
		
		PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
		try 
		{
			String countryCode = fieldTemplate.hasInputFormatSpec() ? fieldTemplate.getInputFormatSpec() : Application.getInstance().getDefaultCountryCode();
			PhoneNumber pn = phoneUtil.parse(value,countryCode);
			if ( ! phoneUtil.isValidNumber(pn) )
				return value;
			
			if ( "standard".equals(fieldTemplate.getOutputFormatSpec()) )
				return phoneUtil.format(pn, PhoneNumberFormat.NATIONAL);
			if ( "international".equals(fieldTemplate.getOutputFormatSpec()) )
				return phoneUtil.format(pn, PhoneNumberFormat.INTERNATIONAL);
			if ( "e164".equals(fieldTemplate.getOutputFormatSpec()) )
				return phoneUtil.format(pn, PhoneNumberFormat.E164);
			if ( "rfc3966".equals(fieldTemplate.getOutputFormatSpec()) )
				return phoneUtil.format(pn, PhoneNumberFormat.RFC3966);
			if ( "digits".equals(fieldTemplate.getOutputFormatSpec()) )
				return EsfString.getOnlyNumeric(phoneUtil.format(pn, PhoneNumberFormat.NATIONAL));
			if ( "input".equals(fieldTemplate.getOutputFormatSpec()) )
				return value;

			// When in doubt, we'll return the national look
			return phoneUtil.format(pn, PhoneNumberFormat.NATIONAL);
		} 
		catch (NumberParseException e) 
		{
			Application.getInstance().except(e,"formatPhone() - PhoneNumberUtil.parse() NumberParseException was thrown");
			return value;
		}
	}
	
	public static String formatPhoneToHtml(String value, FieldTemplate fieldTemplate)
	{
		value = formatPhone(value,fieldTemplate);
		return EsfString.isBlank(value) ? "&nbsp;" : HtmlUtil.toEscapedHtml(value);
	}
	
	public static String formatSsnEin(String value, FieldTemplate fieldTemplate)
	{
		if ( EsfString.isBlank(value) )
			return "";
		String valueNumbers = EsfString.getOnlyNumeric(value);
		if ( valueNumbers.length() != 9 )
			return value;
		if ( "normal".equals(fieldTemplate.getOutputFormatSpec()) )
			return valueNumbers.substring(0,3) + "-" + valueNumbers.substring(3,5) + "-" + valueNumbers.substring(5);
		if ( "normalEIN".equals(fieldTemplate.getOutputFormatSpec()) )
			return valueNumbers.substring(0,2) + "-" + valueNumbers.substring(2);
		if ( "masked".equals(fieldTemplate.getOutputFormatSpec()) )
			return "XXX-XX-" + valueNumbers.substring(5);
		if ( "maskedEIN".equals(fieldTemplate.getOutputFormatSpec()) )
			return "XX-XXX" + valueNumbers.substring(5);
		if ( "last4".equals(fieldTemplate.getOutputFormatSpec()) )
			return valueNumbers.substring(5);
		return valueNumbers; // digits
	}

	public static String formatSsnEinToHtml(String value, FieldTemplate fieldTemplate)
	{
		value = formatSsnEin(value,fieldTemplate);
		return EsfString.isBlank(value) ? "&nbsp;" : HtmlUtil.toEscapedHtml(value);
	}

	public static String createRemovableFileList(DocumentPageBean docPage,String fieldId, EsfName fieldName, FieldTemplate fieldTemplate, List<TransactionFile> fileList)
	{
		if ( fileList == null || fileList.size() == 0 )
			return "";

		StringBuilder buf = new StringBuilder(fileList.size() * 256);
		buf.append("<span style=\"display: table;\">");
		
		String rowBackground = "bgGreenbarDark";
		
		for( TransactionFile tranFile : fileList )
		{
			rowBackground = "bgGreenbarLight".equals(rowBackground) ? "bgGreenbarDark" : "bgGreenbarLight";
			
			String pseudoButtonName = DocumentPageBean.DOCUMENT_DOWNLOAD_BUTTON_PREFIX + fieldName.toLowerCase()+ tranFile.getId().toNormalizedEsfNameString();
			String fileUrl = docPage.getMySessionUrl();
			fileUrl = ServletUtil.appendUrlParam(fileUrl, pseudoButtonName, "");
			
			// Show images inline
			if ( tranFile.isContentTypeImage() && fieldTemplate.hasOutputFormatSpec() && fieldTemplate.getOutputFormatSpec().startsWith("image") )
			{
				String imgClass;
				if ( "image200".equals(fieldTemplate.getOutputFormatSpec()) || "imageOnly200".equals(fieldTemplate.getOutputFormatSpec()) )
					imgClass = "fileUploadInlineImage200";
				else if ( "image600".equals(fieldTemplate.getOutputFormatSpec()) || "imageOnly600".equals(fieldTemplate.getOutputFormatSpec()) )
					imgClass = "fileUploadInlineImage600";
				else if ( "image800".equals(fieldTemplate.getOutputFormatSpec()) || "imageOnly800".equals(fieldTemplate.getOutputFormatSpec()) )
					imgClass = "fileUploadInlineImage800";
				else 
					imgClass = "fileUploadInlineImage400";

				buf.append("<span style=\"display: table-row;\" class=\"").append(rowBackground).append("\">");
				
				buf.append("<span style=\"display: table-cell; padding: 5px;\">");
				buf.append("<img class=\"").append(imgClass).append("\" src=\"");
				buf.append(fileUrl);
				buf.append("\" />");
				buf.append("</span>");
				
				buf.append("<span style=\"display: table-cell;\">&nbsp;</span>");
				buf.append("<span style=\"display: table-cell;\">&nbsp;</span>");
				buf.append("<span style=\"display: table-cell;\">&nbsp;</span>");

				buf.append("</span>");
			}

			buf.append("<span style=\"display: table-row;\" class=\"").append(rowBackground).append("\">");
			
			String userAgent = docPage.getUserAgent().toLowerCase();
			String target;
			if ( tranFile.isContentTypeBrowserSafe() || tranFile.isContentTypePDF() )
				target = "_blank";
			else
				target = userAgent.indexOf("safari") != -1 || userAgent.indexOf(" chrome/") != -1 ? "_top" : "_blank";
			
			String clearPageUnload = " onclick=\"esf.checkLinkConfirmOnPageUnload(false,this)\"";
			
			buf.append("<span style=\"display: table-cell; padding: 5px;\">");

			buf.append("<a href=\"");
			buf.append(fileUrl);
			buf.append("\" target=\"");
			buf.append(target);
			buf.append("\" title=\"").append(docPage.app.getServerMessages().getString("FieldLabel.file.link.title")).append("\"");
			buf.append(clearPageUnload);
			buf.append(">");
			buf.append(HtmlUtil.toEscapedHtml(tranFile.getFileName()));
			buf.append("</a>");
			buf.append("</span>");
			
			buf.append("<span style=\"display: table-cell; text-align: right; padding: 5px;\">");
			buf.append(EsfInteger.byteSizeInUnits(tranFile.getFileSize()));
			buf.append("</span>");
			
			buf.append("<span style=\"display: table-cell; text-align: center; padding: 5px;\">");
			buf.append("<input id=\"");
			buf.append(fieldId);
			buf.append("RemoveUpload");
			buf.append("\" type=\"submit\" class=\"small\" name=\"");
			buf.append(DocumentPageBean.DOCUMENT_DELETE_UPLOAD_BUTTON_PREFIX);
			buf.append(fieldName.toLowerCase());
			buf.append(tranFile.getId().toNormalizedEsfNameString());
			buf.append("\" value=\"").append(docPage.app.getServerMessages().getString("FieldLabel.file.button.removeUploaded.label")).append("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
			buf.append("</span>");
			
			EsfPathName fileAccessPathName = docPage.createPartyFileLastAccessTimestampFieldPathName(tranFile.getId());
			EsfDateTime accessDateTime = (EsfDateTime)docPage.getFieldValue(fileAccessPathName);
			
			buf.append("<span style=\"display: table-cell; padding-left: 10px;\">");
			if ( accessDateTime == null )
			{
				buf.append("<strong><em style=\"background-color: yellow;\">").append(docPage.app.getServerMessages().getString("FieldLabel.file.notRead.label")).append("</em></strong>");
			}
			else
			{
				String dateTime = docPage.isUserLoggedIn() ? accessDateTime.toString(docPage.getLoggedInUser()) : accessDateTime.toString();
				buf.append(docPage.app.getServerMessages().getString("FieldLabel.file.lastAccessed.label",dateTime));
			}
			buf.append("</span>");

			buf.append("</span>");
		}
		
		buf.append("</span>");		
		return buf.toString();
	}

	public static String createViewFileList(DocumentPageBean docPage, String fieldId, EsfName fieldName, FieldTemplate fieldTemplate)
	{
		List<TransactionFile> fileList = docPage.context.transaction.getAllTransactionFilesForDocumentField(docPage.context.currTransactionDocument.getId(), fieldTemplate.getEsfName());
		if ( fileList == null || fileList.size() == 0 )
			return "";

		boolean isFirstRow = true;
		StringBuilder buf = new StringBuilder(fileList.size() * 128);
		buf.append("<span style=\"display: table;\">");
		
		String rowBackground = "bgGreenbarDark";
		
		for( TransactionFile tranFile : fileList )
		{
			rowBackground = "bgGreenbarLight".equals(rowBackground) ? "bgGreenbarDark" : "bgGreenbarLight";

			String fileUrl = docPage.getExternalUrlContextPath() + 
					"/filedownload/" +
					docPage.encode(tranFile.getFieldName().toPlainString()) +
					"/" +
					docPage.encode(tranFile.getFileName()) +
					"?tfid=" +
					docPage.encode(tranFile.getId().toPlainString()) +
					"&puc=" +
					docPage.encode(docPage.context.transactionParty.getTransactionPartyAssignmentPickupCode()) +
					"&tdid=" +
					docPage.encode(docPage.context.currTransactionDocument.getId().toPlainString());
	
			String cellPaddingTop;
			if ( isFirstRow )
			{
				cellPaddingTop = "";
				isFirstRow = false;
			}
			else
				cellPaddingTop = " padding-top: 10px;";
			
			// Show images inline if so requested
			if ( tranFile.isContentTypeImage() && fieldTemplate.hasOutputFormatSpec() && fieldTemplate.getOutputFormatSpec().startsWith("image") )
			{
				String imgClass;
				if ( "image200".equals(fieldTemplate.getOutputFormatSpec()) || "imageOnly200".equals(fieldTemplate.getOutputFormatSpec()) )
					imgClass = "fileUploadInlineImage200";
				else if ( "image600".equals(fieldTemplate.getOutputFormatSpec()) || "imageOnly600".equals(fieldTemplate.getOutputFormatSpec()) )
					imgClass = "fileUploadInlineImage600";
				else if ( "image800".equals(fieldTemplate.getOutputFormatSpec()) || "imageOnly800".equals(fieldTemplate.getOutputFormatSpec()) )
					imgClass = "fileUploadInlineImage800";
				else 
					imgClass = "fileUploadInlineImage400";
				
				buf.append("<span style=\"display: table-row;\" class=\"").append(rowBackground).append("\">");
				
				buf.append("<span style=\"display: table-cell;" + cellPaddingTop + "\">");
				buf.append("<img class=\"").append(imgClass).append("\" src=\"");
				buf.append(fileUrl);
				buf.append("\" />");
				buf.append("</span>");

				buf.append("<span style=\"display: table-cell;\">&nbsp;</span>");
				buf.append("<span style=\"display: table-cell;\">&nbsp;</span>");

				buf.append("</span>");
			}
			
			buf.append("<span style=\"display: table-row;\" class=\"").append(rowBackground).append("\">");

			buf.append("<span style=\"display: table-cell;" + cellPaddingTop + "\">");
			
			String userAgent = docPage.getUserAgent().toLowerCase();
			String target;
			if ( tranFile.isContentTypeBrowserSafe() || tranFile.isContentTypePDF() )
				target = "_blank";
			else
				target = userAgent.indexOf("safari") != -1 || userAgent.indexOf(" chrome/") != -1 ? "_top" : "_blank";
			
			String clearPageUnload = " onclick=\"esf.checkLinkConfirmOnPageUnload(false,this)\"";
			
			buf.append("<a href=\"");
			buf.append(fileUrl);
			buf.append("\" target=\"");
			buf.append(target);
			buf.append("\" title=\"").append(docPage.app.getServerMessages().getString("FieldLabel.file.link.title")).append("\"");
			buf.append(clearPageUnload);
			buf.append(">");
			buf.append(HtmlUtil.toEscapedHtml(tranFile.getFileName()));
			buf.append("</a>");

			buf.append("</span>");
			
			buf.append("<span style=\"display: table-cell; text-align: right; padding-left: 10px;" + cellPaddingTop + "\">");
			buf.append(EsfInteger.byteSizeInUnits(tranFile.getFileSize()));
			buf.append("</span>");
			
			EsfPathName fileAccessPathName = docPage.createPartyFileLastAccessTimestampFieldPathName(tranFile.getId());
			EsfDateTime accessDateTime = (EsfDateTime)docPage.getFieldValue(fileAccessPathName);
			
			buf.append("<span style=\"display: table-cell; padding-left: 10px;" + cellPaddingTop + "\">");
			if ( accessDateTime == null )
			{
				buf.append("<strong><em style=\"background-color: yellow;\">").append(docPage.app.getServerMessages().getString("FieldLabel.file.notRead.label")).append("</em></strong>");
			}
			else
			{
				String dateTime = docPage.isUserLoggedIn() ? accessDateTime.toString(docPage.getLoggedInUser()) : accessDateTime.toString();
				buf.append(docPage.app.getServerMessages().getString("FieldLabel.file.lastAccessed.label",dateTime));
			}
			buf.append("</span>");
			
			buf.append("</span>");
		}
		
		buf.append("</span>");		
		return buf.toString();
	}	

	
	public static String createFileConfirmClickHtml(DocumentPageBean docPage, String fieldId, EsfName fieldName, EsfName fileName, FieldTemplate fieldTemplate, boolean isInputMode)
	{
		FileVersion fileVersion = docPage.getFileVersion(fileName);
		if ( fileVersion == null )
			return "";
		
		StringBuilder buf = new StringBuilder(256);
		buf.append("<span style=\"display: table;\">");
		
		buf.append("<span style=\"display: table-row;\">");

		buf.append("<span style=\"display: table-cell;\">");
		
		String title = fieldTemplate.hasTooltip() ? fieldTemplate.getTooltip() : docPage.app.getServerMessages().getString("FieldLabel.fileconfirmclick.link.default.title");
			
		String userAgent = docPage.getUserAgent().toLowerCase();
		String target;
		if ( fileVersion.isContentTypeBrowserSafe() || fileVersion.isContentTypePDF() )
			target = "_blank";
		else
			target = userAgent.indexOf("safari") != -1 || userAgent.indexOf(" chrome/") != -1 ? "_top" : "_blank";
		
		String clearPageUnload = " onclick=\"esf.checkLinkConfirmOnPageUnload(false,this)\"";
		
		if ( isInputMode )
		{
			String buttonSuffix = docPage.context.currDocument.getId().toNormalizedEsfNameString();
			String pseudoButtonName = DocumentPageBean.DOCUMENT_CONFIRM_LINK_BUTTON_PREFIX + fieldName.toLowerCase() + buttonSuffix;
			
			buf.append("<a href=\"");
			String url = docPage.getMySessionUrl();
			url = ServletUtil.appendUrlParam(url, pseudoButtonName, "");
			buf.append(url);
			buf.append("\" target=\"");
			buf.append(target);
			buf.append("\" title=\"").append(docPage.encode(title)).append("\"");
			buf.append(clearPageUnload);
			buf.append(">");
			buf.append(HtmlUtil.toEscapedHtml(fileVersion.getFile().getDisplayOrEsfName()));
			buf.append("</a>");
		}
		else // in display mode, we just give a link that will work now and into the future
		{
			buf.append("<a href=\"");
			buf.append(docPage.getExternalUrlContextPath());
			buf.append("/fileconfirmclick/");
			buf.append(docPage.encode(fileName.toPlainString()));
			buf.append("/");
			buf.append(docPage.encode(fileVersion.getFileFileName()));
			buf.append("?fvid=");
			buf.append(docPage.encode(fileVersion.getId().toPlainString()));
			buf.append("&puc=");
			buf.append(docPage.encode(docPage.context.pickupCode));
			buf.append("&tdid=");
			buf.append(docPage.encode(docPage.context.currTransactionDocument.getId().toPlainString()));
			buf.append("&fieldName=");
			buf.append(docPage.encode(fieldName.toPlainString()));
			buf.append("\" target=\"");
			buf.append(target);
			buf.append("\" title=\"").append(title).append("\"");
			buf.append(clearPageUnload);
			buf.append(">");
			buf.append(HtmlUtil.toEscapedHtml(fileVersion.getFile().getDisplayOrEsfName()));
			buf.append("</a>");
		}

		buf.append("</span>");
			
		buf.append("<span style=\"display: table-cell; text-align: right; padding-left: 10px;\">");
		buf.append(EsfInteger.byteSizeInUnits(fileVersion.getFileSize()));
		buf.append("</span>");
			
		EsfPathName fileConfirmClickPathName = docPage.createPartyFileConfirmClickLastAccessTimestampFieldPathName(fieldTemplate.getEsfName());
		EsfDateTime accessDateTime = (EsfDateTime)docPage.getFieldValue(fileConfirmClickPathName);
		
		buf.append("<span style=\"display: table-cell; padding-left: 10px;\">");
		if ( accessDateTime == null )
		{
			if ( isInputMode && (docPage.isRequired(fieldTemplate) || docPage.isOptionalStyleRequired(fieldTemplate)) )
				buf.append("<strong><em style=\"background-color: yellow;\">").append(docPage.app.getServerMessages().getString("FieldLabel.fileconfirmclick.notRead.label")).append("</em></strong>");
		}
		else
		{
			String dateTime = docPage.isUserLoggedIn() ? accessDateTime.toString(docPage.getLoggedInUser()) : accessDateTime.toString();
			buf.append(docPage.app.getServerMessages().getString("FieldLabel.fileconfirmclick.lastAccessed.label",dateTime));
		}
		buf.append("</span>");
		
		buf.append("</span>");
		
		buf.append("</span>");		
		return buf.toString();
	}

}
