// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen.taglib;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.Document;
import com.esignforms.open.util.HtmlUtil;

/**
 * This class is used to display the value retrieved from the document.
 * 
 * @author Yozons, Inc.
 */
public class DocumentOut 
    extends javax.servlet.jsp.tagext.SimpleTagSupport
{
    private String name;
    private String fieldSpec; // we display this if the 'name' cannot be determined

	public void setName(String str)
	{
		name = str;
	}
	
	public void setFieldSpec(String str)
	{
		fieldSpec = str;
	}

    public void doTag() 
        throws java.io.IOException
    {
    	JspWriter out = getJspContext().getOut();
    	
    	// The generated page should have initialized its DocumentPageBean
        DocumentPageBean docPage = (DocumentPageBean)getJspContext().getAttribute(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId,PageContext.REQUEST_SCOPE);
        if ( docPage == null )
        {
        	Application.getInstance().err("DocumentOut tag could not find DocumentPageBean with id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId + " in the request scope.");
            if ( EsfString.isNonBlank(fieldSpec) )
            	out.write(fieldSpec);
            return;
        }
        
    	String value = null;

    	Document document = docPage.context.currDocument;
    	if ( document != null )
    	{
    		// document info
        	if ( "esfname".equalsIgnoreCase(name) )
        		value = document.getEsfName().toHtml();
        	else if ( "displayname".equalsIgnoreCase(name) )
        		value = HtmlUtil.toEscapedHtml(document.getDisplayName());
        	else if ( "id".equalsIgnoreCase(name) )
        		value = document.getId().toHtml();
        	else if ( "version".equalsIgnoreCase(name) )
        		value = docPage.makePrettyNumber(docPage.context.currDocumentVersion.getVersion());
        	else if ( "version.id".equalsIgnoreCase(name) )
        		value = docPage.context.currDocumentVersion.getId().toHtml();
        	// page info
        	else if ( "page.esfname".equalsIgnoreCase(name) )
        		value = docPage.context.currDocumentVersionPage.getEsfName().toHtml();
        	else if ( "page.id".equalsIgnoreCase(name) )
        		value = docPage.context.currDocumentVersionPage.getId().toHtml();
        	else if ( "page.number".equalsIgnoreCase(name) )
        		value = docPage.makePrettyNumber(docPage.context.documentPageNumber);
        	// party info
        	else if ( "party.esfname".equalsIgnoreCase(name) )
        		value = docPage.context.pickupPartyName.toHtml();
        	else if ( "party.displayname".equalsIgnoreCase(name) )
        		value = HtmlUtil.toEscapedHtml(docPage.context.pkgVersionPartyTemplate.getDisplayName());
        	// package info
        	else if ( "package.pathname".equalsIgnoreCase(name) )
        		value = docPage.context.pkg.getPathName().toHtml();
           	else if ( "package.version".equalsIgnoreCase(name) )
        		value = docPage.makePrettyNumber(docPage.context.pkgVersion.getVersion());
        	else if ( "package.id".equalsIgnoreCase(name) )
        		value = docPage.context.pkg.getId().toHtml();
        	else if ( "package.version.id".equalsIgnoreCase(name) )
        		value = docPage.context.pkgVersion.getId().toHtml();
        	// Unknown, so echo the field spec back as unresolved
        	else
        		value = fieldSpec;
    	}
    	else
    		value = fieldSpec;
    	
    	out.write(value);
    }
}
