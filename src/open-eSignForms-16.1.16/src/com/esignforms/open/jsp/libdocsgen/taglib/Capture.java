// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;

import com.esignforms.open.Application;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;

/**
 * This class allows us to capture the HTML generated between the tags.
 *
 * @author Yozons, Inc.
 */
public class Capture 
	extends javax.servlet.jsp.tagext.BodyTagSupport
{
	private static final long serialVersionUID = 3822850295874780190L;

	@Override
    public int doStartTag() throws JspException
    {
	    return EVAL_BODY_BUFFERED;
    }

 	@Override
	public int doEndTag() throws JspException
	{
		try 
		{
	        Object pageObject = pageContext.getAttribute(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId,PageContext.REQUEST_SCOPE);
	        if ( pageObject == null )
	        {
	        	Application.getInstance().err("Capture tag could not find DocumentPageBean with attribute id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId + " in the request scope.");
	        	throw new JspException("Missing DocumentPageBean class specified by attribute id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId);
	        }
	        if ( ! (pageObject instanceof DocumentPageBean) )
	        {
	        	Application.getInstance().err("Capture tag found object with attribute id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId + " in the request scope that is not a DocumentPageBean; type: " + pageObject.getClass().getName());
	        	throw new JspException("Class specified by attribute id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId + " is not a DocumentPageBean object.");
	        }
	        DocumentPageBean docPage = (DocumentPageBean)pageObject;

	        String capture = getBodyContent().getString();
			
			docPage.appendCapturedHtml(capture);
			pageContext.getOut().write(capture);
		} 
		catch(Exception e) 
		{
			throw new JspException(e.getMessage(),e);
		}

		return EVAL_PAGE;
	}
}
