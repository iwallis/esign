// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen;

/**
 * EsfDocumentPageInterface defines the supported interface for custom java code used in a document.
 * DocumentPageBean implements this interface, and thus a programmer who wants, can actually do more
 * as this is not a fool proof solution, just a document valid interface so that if the programmer is
 * using these methods, we'll support their effort, otherwise they are "advanced" enough to do as they please
 * with their custom Java code!
 * 
 * @author Yozons, Inc.
 */
public interface EsfDocumentPageInterface 
{
	/**
	 * @return true if the page is currently in EDIT mode, meaning the party is authorized to fill out fields on the form, the 
	 * stage before going into REVIEW mode.
	 */
    public boolean isEditMode();

	/**
	 * @return true if the page is currently in REVIEW mode, meaning the party is authorized to fill out fields on the form while in EDIT
	 * mode but now is reviewing the page before submitting it.
	 */
    public boolean isReviewMode();
    
	/**
	 * @return true if the page is currently in VIEW-ONLY mode, meaning the party is not authorized to fill out any fields, or is
	 * returning after completing the document so no further changes can be made.
	 */
    public boolean isViewMode();

	/**
	 * @return true if the current party can only view the document and has no fields that can be updated/filled out on any
	 * page in the document.
	 */
	public boolean isViewOnlyParty();
	
	/**
	 * @return true if the current party is a view-only party that is not even defined as a party to the document. 
	 * This party cannot make any changes and is generally defined in the package but is not mapped to a specific document party.
	 */
	public boolean isViewOnlyNonParty();
	
	/**
	 * @param docPartyName the String document party name to check (should be one of the document party names defined for the document)
	 * @return true if the current party is the specified document party
	 */
	public boolean isParty(String docPartyName);
	
	/**
	 * @return true if the current party is completed (all documents done)
	 */
	public boolean isPartyCompleted();
	
	/**
	 * @return true if the current party is a logged in user
	 */
	public boolean isPartyLoggedIn();
	
	/**
	 * @return true if the transaction is completed (all parties done)
	 */
	public boolean isTransactionCompleted();
	
	/**
	 * @return true if the transaction is deleted (the current party has deleted the transaction)
	 */
	public boolean isTransactionDeleted();
	
	/**
	 * @param packagePartyName the String package party name to check. This is mostly useful for the package+disclosure document that
	 * does not have document parties defined at all.
	 * @return true if the current party is the specified package party
	 */
	public boolean isPackageParty(String packagePartyName);
	
	/**
	 * @param fieldSpec the String field specification, such as ${firstName} to get the field firstName from the document
	 * @return the String value associated with the field spec. If the field spec is invalid (no such field can be found)
	 * the field spec is returned without any values replacing it.
	 */
	public String getFieldSpecValue(String fieldSpec);

	/**
	 * @param fieldSpec the String field specification, such as ${firstName} to get the field firstName from the document
	 * @return the String value associated with the field spec. If the field spec is not transformed at all, it returns an empty string.
	 */
	public String getFieldSpecValueOrBlankIfNotFound(String fieldSpec);

	/**
	 * @param v the String to check if it's blank (null, empty or just spaces)
	 * @return true if 'v' is blank
	 */
	public boolean isBlank(String v);
	/**
	 * @param fieldSpec the String field specification, such as ${firstName} to get the field firstName from the document
	 * @return true if the resulting field spec is blank (or has no other value we can transform to).  This is
	 * essentially isBlank(getFieldSpecValueOrBlankIfNotFound(fieldSpec))
	 */
	public boolean isFieldSpecBlank(String fieldSpec);

	/**
	 * @param v the String to check if it's non-blank (not null, not empty and not just spaces)
	 * @return true if 'v' is non-blank
	 */
	public boolean isNonBlank(String v);
	/**
	 * @param fieldSpec the String field specification, such as ${firstName} to get the field firstName from the document
	 * @return true if the resulting field spec is nonblank.  This is essentially isNonBlank(getFieldSpecValueOrBlankIfNotFound(fieldSpec))
	 */
	public boolean isFieldSpecNonBlank(String fieldSpec);
	
	/**
	 * Converts an integer in a String to an int value. If the 'v' is not a valid integer, it will return the default value.
	 * @param v the String containing the integer to convert
	 * @param defaultValue the default integer to return if 'v' cannot be converted
	 * @return the integer value of the string 'v', or if 'v' is not a valid integer, the default value is returned.
	 */
	public int stringToInt(String v, int defaultValue);
	/**
	 * Converts an integer in a String to an int value. If the 'v' is not a valid integer, it will return 0.
	 * @param v the String containing the integer to convert
	 * @return the integer value of the string 'v', or if 'v' is not a valid integer, 0 is returned.
	 */
	public int stringToInt(String v);
	
	/**
	 * Converts a string into a boolean.
	 * @param b the String to convert to a boolean
	 * @return true if the case insensitive string 'b' is one of: true, y, yes, t or 1
	 */
	public boolean stringToBool( String b );
}
