// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen.taglib;

import java.util.ArrayList;
import java.util.ListIterator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.esignforms.open.Application;
import com.esignforms.open.Version;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.jsp.libdocsgen.DateSelectorCalendar;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.DocumentVersionPage;
import com.esignforms.open.util.HtmlUtil;

/**
 * This class is used to wrap a JSP in HTML tags.
 * 
 * @author Yozons, Inc.
 */
public class Html 
extends javax.servlet.jsp.tagext.BodyTagSupport
{
	private static final long serialVersionUID = -4022737694749725140L;

	public static final String DocumentPageBeanAttributeId = "docPage";
	public static final String EsfDocumentPageInterfaceVariableName = "esf";
    
    private String title = "";
    
    public final void setTitle(String str)
	{
    	title = str;
	}

 	@Override
    public int doStartTag() throws JspException
    {
 		Application app = Application.getInstance();
 		
		try 
		{
	        Object pageObject = pageContext.getAttribute(DocumentPageBeanAttributeId,PageContext.REQUEST_SCOPE);
	        if ( pageObject == null )
	        {
	        	app.err("Html tag could not find DocumentPageBean with attribute id: " + DocumentPageBeanAttributeId + " in the request scope.");
	        	throw new JspException("Missing DocumentPageBean class specified by attribute id: " + DocumentPageBeanAttributeId);
	        }
	        if ( ! (pageObject instanceof DocumentPageBean) )
	        {
	        	app.err("Html tag found object with attribute id: " + DocumentPageBeanAttributeId + " in the request scope that is not a DocumentPageBean; type: " + pageObject.getClass().getName());
	        	throw new JspException("Class specified by attribute id: " + DocumentPageBeanAttributeId + " is not a DocumentPageBean object.");
	        }
	        DocumentPageBean docPage = (DocumentPageBean)pageObject;
	        
	    	JspWriter out = pageContext.getOut();
	    	
	    	// Write HTML tags
	    	out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n");
	    	out.write("<head>\n");
	    	out.write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n");
	    	
	    	String pageTitle = EsfString.isNonBlank(title) ? title : ( docPage.getPageTitlePrefix() + " - " + docPage.context.currDocument.getDisplayName() );
	    	out.write("<title>");
	    	out.write(HtmlUtil.toEscapedHtml(pageTitle));
	    	out.write("</title>\n");

	    	out.write("<meta name=\"robots\" content=\"noindex, nofollow\" />\n");
	    	out.write("<meta name=\"Description\" content=\"Confidential document\" />\n");
	    	out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" />\n");
	    	
	    	out.write("<meta name=\"Generator\" content=\"Yozons Open eSignForms document generator version ");
	    	out.write(Version.getVersionString());
	    	out.write("\" />\n");
	    	
	    	out.write("<meta name=\"DeploymentId\" content=\"");
	    	out.write(app.getDeployId().toHtml());
	    	out.write("\" />\n");

	    	out.write("<meta name=\"TransactionId\" content=\"");
	    	out.write(docPage.context.transaction.getId().toHtml());
	    	out.write("\" />\n");

	    	out.write("<meta name=\"TransactionTemplateId\" content=\"");
	    	out.write(docPage.context.transaction.getTransactionTemplateId().toHtml());
	    	out.write("\" />\n");
	    	
	    	out.write("<meta name=\"TransactionMode\" content=\"");
	    	out.write(docPage.context.transaction.isProduction() ? "Production" : "Test");
	    	out.write("\" />\n");
	    	
	    	out.write("<meta name=\"PackageId\" content=\"");
	    	out.write(docPage.context.pkg.getId().toHtml());
	    	out.write("\" />\n");
	    	
	    	out.write("<meta name=\"PackageVersionId\" content=\"");
	    	out.write(docPage.context.pkgVersion.getId().toHtml());
	    	out.write("\" />\n");
	    	
	    	out.write("<meta name=\"PackageVersionPartyName\" content=\"");
	    	out.write(docPage.context.pkgVersionPartyTemplate.getEsfName().toHtml());
	    	out.write("\" />\n");
	    	
	    	out.write("<meta name=\"DocumentId\" content=\"");
	    	out.write(docPage.context.currDocument.getId().toHtml());
	    	out.write("\" />\n");

	    	out.write("<meta name=\"DocumentName\" content=\"");
	    	out.write(HtmlUtil.toEscapedHtml(docPage.context.currDocument.getDisplayName()));
	    	out.write("\" />\n");

	    	out.write("<meta name=\"DocumentVersionId\" content=\"");
	    	out.write(docPage.context.currDocumentVersion.getId().toHtml());
	    	out.write("\" />\n");
	    	
	    	// Can be null if a multi-page document and we're in review mode since there is no document version page to represent
	    	// the combined pages.
	    	if ( docPage.context.currDocumentVersionPage != null )
	    	{
		    	out.write("<meta name=\"DocumentVersionPageId\" content=\"");
		    	out.write(docPage.context.currDocumentVersionPage.getId().toHtml());
		    	out.write("\" />\n");
	    	}
	    	else
	    	{
	    		for( DocumentVersionPage page : docPage.context.currDocumentVersion.getReviewPages() )
	    		{
			    	out.write("<meta name=\"MultiPageDocumentVersionPageId\" content=\"");
			    	out.write(page.getId().toHtml());
			    	out.write("\" />\n");
	    		}
	    	}
	    		
	    	for( EsfName partyName : docPage.context.currDocumentVersionPartyNameList )
	    	{
		    	out.write("<meta name=\"DocumentVersionPartyName\" content=\"");
		    	out.write(partyName.toHtml());
		    	out.write("\" />\n");
	    	}

	    	out.write("<meta name=\"DocumentPageMode\" content=\"");
	    	out.write(docPage.pageState.toString());
	    	out.write("\" />\n");

	    	out.write("<meta name=\"HtmlRenderTimestamp\" content=\"");
	    	out.write(HtmlUtil.toEscapedHtml((new EsfDateTime()).toDateTimeMsecString()));
	    	out.write("\" />\n");

	    	out.write("<meta name=\"RequestIpAddr\" content=\"");
	    	out.write(HtmlUtil.toEscapedHtml(docPage.getIP()));
	    	out.write("\" />\n");

	    	String[] ipAddresses = docPage.getIPAddresses();
	    	if ( ipAddresses != null && ipAddresses.length > 1 )
	    	{
	    		for( String ip : ipAddresses )
	    		{
	    	    	out.write("<meta name=\"IpAddresses\" content=\"");
	    	    	out.write(HtmlUtil.toEscapedHtml(ip));
	    	    	out.write("\" />\n");
	    		}
	    	}
	    	
	    	out.write("<meta name=\"RequestUserAgent\" content=\"");
	    	out.write(HtmlUtil.toEscapedHtml(docPage.getUserAgent()));
	    	out.write("\" />\n");

	    	out.write("<base href=\"");
	    	out.write(docPage.getExternalUrlContextPath());
	    	out.write("/\" />\n");
	    	
	    	out.write("<style type=\"text/css\">\n/* platform esf.css */\n");
	    	out.write(app.getServletResourceAsString("/static/esf/esf.css"));
	    	out.write(app.getServletResourceAsString(docPage.context.getDocumentStyleIncludeUrl()));
	    	//pageContext.include("/static/esf/esf.css"); -- For some reason, this pageContext.include() is not reliable, so we just read the files directly and write them out now
	    	//pageContext.include(docPage.context.getDocumentStyleIncludeUrl());
	    	out.write("</style>\n");
	    	
	    	out.write(Literals.JSP_BEGIN_REMOVE_COMMENT);
	    	out.write("<script type=\"text/javascript\">\n");
	    	out.write(app.getServletResourceAsString("/static/esf/esf.js"));
	    	out.write("\n");
	    	//pageContext.include("/static/esf/esf.js");
	    	
	    	if ( docPage.hasJumpToHashtag() )
	    		out.write("window.location.hash = '#" + docPage.getJumpToHashtag() + "';\n");
	    	
	    	out.write("document.onkeypress = esf.blockEnterKeyViaInputFields;\n");
	    	
	    	// Temporary transactions are for testing only during development of a document
	    	if ( ! docPage.context.transaction.isEndState() && 
	    		 ! docPage.context.transaction.isSuspended() &&
	    		 ! docPage.context.transaction.isDeleted() && 
	    		 ! docPage.context.isEsfReportsAccess() && 
	    		 ! docPage.context.transaction.isTemporary() 
	    	   )
	    	{
	    		if ( docPage.context.isPackageDocument() )
	    		{
	    			if ( ! docPage.context.transactionParty.isCompleted() )
	    			{
				    	out.write("window.onbeforeunload = esf.confirmOnPageUnload;\n");
				    	String unloadMessage = docPage.app.getServerMessages().getString("Html.onbeforeunload.packageNotCompleted.message");
		    			out.write("esf.confirmOnPageUnloadMessage = '");
		    			out.write(unloadMessage);
		    			out.write("'\n");
	    			}
	    		}
	    		else
	    		{
		    		if ( docPage.isPageEdit() || docPage.isPageReview() ) 
		    		{
				    	out.write("window.onbeforeunload = esf.confirmOnPageUnload;\n");
				    	String unloadMessage = docPage.isPageEdit() ? docPage.app.getServerMessages().getString("Html.onbeforeunload.pageEdit.message") : docPage.app.getServerMessages().getString("Html.onbeforeunload.pageReview.message");
		    			out.write("esf.confirmOnPageUnloadMessage = '");
		    			out.write(unloadMessage);
		    			out.write("'\n");
			    	}
	    		}
	    	}
	    	
	    	out.write("</script>\n");
	    	out.write(Literals.JSP_END_REMOVE_COMMENT);

	    	// Add the calendar popup widget if we're in edit mode
	    	if ( docPage.isPageEdit() && docPage.context.canPartyInputDate() )
	    	{
	            out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"");
	            out.write(app.getContextPath());
	            out.write("/static/calendar/skins/aqua/theme.css\" title=\"Aqua\" />");
	            
	            out.write("<script type=\"text/javascript\" src=\"");
	            out.write(app.getContextPath());
	            out.write("/static/calendar/calendar.js\"></script>");
	            
	            out.write("<script type=\"text/javascript\" src=\"");
	            out.write(app.getContextPath());
	            out.write("/static/calendar/lang/calendar-en.js\"></script>");

	            out.write("<script type=\"text/javascript\" src=\"");
	            out.write(app.getContextPath());
	            out.write("/static/calendar/calendar-setup.js\"></script>\n");
	            
	            // Since this initializes the page, we'll put a list of calendars that the page will configure
	            ArrayList<DateSelectorCalendar> list = new ArrayList<DateSelectorCalendar>(10);
	            pageContext.setAttribute(DateSelectorCalendar.LIST_ATTRIBUTE,list,javax.servlet.jsp.PageContext.REQUEST_SCOPE);
	    	}
	    	
	    	if ( docPage.isPageEdit() && docPage.context.canPartyInputRichTextarea() )
	    	{
	    		out.write("<script type=\"text/javascript\" src=\"");
	    		out.write(app.getContextPath());
	    		out.write("/VAADIN/widgetsets/com.esignforms.open.vaadin.widgetset.Open_esignformsvaadinWidgetset/ckeditor/ckeditor.js\"></script>\n");
	    	}
	    	
	    	out.write("</head>\n");
	    	
	    	// Write BODY tags
	    	out.write("<body class=\"esf\">\n");
	    	out.write("<div id=\"");
    		out.write(docPage.context.currDocumentVersion.getId().toNormalizedEsfNameString());
	    	out.write("\" class=\"");
	    	out.write(docPage.context.getDocumentStyleClass());
	    	out.write(" pagediv\">\n");
	    	
	    	if ( docPage.context.transaction.isTest() )
	    		out.write("<div style=\"background-color: pink; text-align: center; width: 100%;\">* * * &nbsp; T E S T &nbsp; T R A N S A C T I O N &nbsp; * * *</div>\n");
	    	
            if ( ! docPage.context.isPackageDocument() )
            {
	            out.write(Literals.JSP_BEGIN_REMOVE_COMMENT);
	            out.write("<div class=\"documentAndPageNumbersInfo\">");
	            out.write(HtmlUtil.toDisplayHtml(docPage.getDocumentAndPageNumbersInfo()));
	            out.write("</div>");
	            out.write(Literals.JSP_END_REMOVE_COMMENT);
            }
		}
		catch(Exception e) 
		{
			throw new JspException(e.getMessage(),e);
		}

		return EVAL_BODY_INCLUDE;
    }
    
	/**
	 * When the tag ends, we display the end body and html tags.
	 */
 	@Override
	public int doEndTag() throws JspException
	{
		try 
		{
	        Object pageObject = pageContext.getAttribute(DocumentPageBeanAttributeId,PageContext.REQUEST_SCOPE);
	        if ( pageObject == null )
	        {
	        	Application.getInstance().err("Html end tag could not find DocumentPageBean with attribute id: " + DocumentPageBeanAttributeId + " in the request scope.");
	        	throw new JspException("Missing DocumentPageBean class specified by attribute id: " + DocumentPageBeanAttributeId);
	        }
	        if ( ! (pageObject instanceof DocumentPageBean) )
	        {
	        	Application.getInstance().err("Html end tag found object with attribute id: " + DocumentPageBeanAttributeId + " in the request scope that is not a DocumentPageBean; type: " + pageObject.getClass().getName());
	        	throw new JspException("Class specified by attribute id: " + DocumentPageBeanAttributeId + " is not a DocumentPageBean object.");
	        }
	        DocumentPageBean docPage = (DocumentPageBean)pageObject;
	        
	    	JspWriter out = pageContext.getOut();
	    	
	    	if ( docPage.isPageEdit() && docPage.context.canPartyInputDate() )
	    	{
	            Object obj = pageContext.getAttribute(DateSelectorCalendar.LIST_ATTRIBUTE,javax.servlet.jsp.PageContext.REQUEST_SCOPE);
	            if ( obj != null )
	            {
		            @SuppressWarnings("unchecked")
					ArrayList<DateSelectorCalendar> list = (ArrayList<DateSelectorCalendar>)obj;
		            
		            if ( list.size() > 0 )
		            {
		                out.write("<script type=\"text/javascript\"> ");
		                
		                ListIterator<DateSelectorCalendar> iter = list.listIterator();
		                while( iter.hasNext() )
		                {
		                    DateSelectorCalendar cal = iter.next();
		                    
		                    out.write("{ var dateSelectorYearRange = new Array(2); ");
		                    out.write("dateSelectorYearRange[0] = "); out.write(cal.minYear); out.write("; ");
		                    out.write("dateSelectorYearRange[1] = "); out.write(cal.maxYear); out.write("; ");
		                    
		                    out.write("Calendar.setup({ ");
		                    out.write("inputField : \""); out.write(cal.inputFieldId); out.write("\", ");
		                    out.write("ifFormat : \""); out.write(cal.format); out.write("\", ");
		                    out.write("daFormat : \""); out.write(cal.format); out.write("\", ");
		                    out.write("button : \""); out.write(cal.buttonId); out.write("\", ");
		                    out.write("singleClick : true, ");
		                    out.write("weekNumbers : false, ");
		                    out.write("range : dateSelectorYearRange, ");
		                    out.write("showTime : false, ");
		                    out.write("cache : true, ");
		                    out.write("showOthers : true ");
		                    out.write("}); } ");
		                }
		                
		                out.write("</script>\n");

		                pageContext.removeAttribute(DateSelectorCalendar.LIST_ATTRIBUTE,javax.servlet.jsp.PageContext.REQUEST_SCOPE);
		            }
	            }
	    	}	
	    	
	    	// We currently have no need for anything to end CKEditor, but this is a placeholder should it be needed later.
	    	//if ( docPage.isPageEdit() && docPage.context.canPartyInputRichTextarea() )
	    	//{
	    	//}

	    	String footerHtml = docPage.context.isPackageDocument() ? docPage.getButtonMessageVersion().getPackageFooterHtml(docPage.context) : docPage.getButtonMessageVersion().getDocumentFooterHtml(docPage.context);
	    	if ( EsfString.isNonBlank(footerHtml) )
	    	{
	    		String link;
	    		if ( docPage.context.transaction.isDeleted() )
	    			link = docPage.app.getServerMessages().getString("Html.footer.link.transactionDeleted");
	    		else if ( docPage.context.isEsfReportsAccess() )
	    			link = docPage.app.getServerMessages().getString("Html.footer.link.ESF_reports_access");
	    		else
	    			link = HtmlUtil.toEscapedHtml(docPage.getMyExternalUrl());
	    		footerHtml = footerHtml.replace("${LINK}", link);
	    		out.write(footerHtml);
	    	}
	        
	        out.write("</div>\n"); // end pagediv    
	        out.write("</body>\n");
	        out.write("</html>\n");
		}
		catch(Exception e) 
		{
			throw new JspException(e.getMessage(),e);
		}

		return EVAL_PAGE;
	}
	
 	@Override
 	public void release()
 	{
 		title = "";
 	}
}
