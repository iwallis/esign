// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen.taglib;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.esignforms.open.Application;
import com.esignforms.open.config.Literals;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.util.HtmlUtil;

/**
 * This class is used to display a common form tag.
 * 
 * @author Yozons, Inc.
 */
public class Form 
extends javax.servlet.jsp.tagext.BodyTagSupport
{
	private static final long serialVersionUID = 480781521130801942L;

	private static final String DEFAULT_METHOD = "post";
	private static final String DEFAULT_NAME = "aform";
	
	private String name = DEFAULT_NAME;
	private String method = DEFAULT_METHOD;
	private String formAction = null;
    
    public final void setName(String str)
	{
    	name = str;
	}

 	@Override
    public int doStartTag() throws JspException
    {
		try 
		{
	    	JspWriter out = pageContext.getOut();
	    	
	    	// The generated page should have initialized its DocumentPageBean
	        DocumentPageBean docPage = (DocumentPageBean)pageContext.getAttribute(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId,PageContext.REQUEST_SCOPE);
	        if ( docPage == null )
	        {
	        	Application.getInstance().err("Form tag could not find DocumentPageBean with id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId + " in the request scope.");
	        	throw new JspException("Missing DocumentPageBean class specified by attribute id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId);
	        }
	        
	        String encType = docPage.context.canPartyUploadFile() ? "multipart/form-data" : "application/x-www-form-urlencoded";
	        
	    	formAction = docPage.getMySessionUrl();
	    	
	        out.write(Literals.JSP_BEGIN_REMOVE_COMMENT);
	        out.write("<form name=\"");
	        out.write(name);
	        out.write("\" method=\"");
	        out.write(method);
	        out.write("\" action=\"");
	        out.write(HtmlUtil.toDisplayHtml(formAction));
	        out.write("\" enctype=\"");
	        out.write(encType); 
            out.write("\">\n"); // we move the hide all submit fields to individual buttons since our download buttons shouldn't cause other buttons to disappear
            out.write("<input type=\"hidden\" name=\"");
            out.write(DocumentPageBean.HIDDEN_AUTO_POST_FIELD_ID);
            out.write("\" value=\"\" />\n");
            out.write(Literals.JSP_END_REMOVE_COMMENT);
		}
		catch(Exception e) 
		{
			throw new JspException(e.getMessage(),e);
		}

		return EVAL_BODY_INCLUDE;
    }
    
	/**
	 * When the tag ends, we display the end form tag.
	 */
 	@Override
    public int doEndTag() throws JspException
    {
		try 
		{
	    	JspWriter out = pageContext.getOut();

	        out.write(Literals.JSP_BEGIN_REMOVE_COMMENT);
	        out.write("</form>\n");
	        out.write(Literals.JSP_END_REMOVE_COMMENT);
		}
		catch(Exception e) 
		{
			throw new JspException(e.getMessage(),e);
		}

		return EVAL_PAGE;
	}
	
 	@Override
 	public void release()
 	{
 	    name = DEFAULT_NAME;
 		method = DEFAULT_METHOD;
 		formAction = null;
 	}
}
