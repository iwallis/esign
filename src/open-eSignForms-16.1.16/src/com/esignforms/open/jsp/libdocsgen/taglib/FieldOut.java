// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen.taglib;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;

/**
 * This class is used to display just the field, without a label, in display mode (no EDIT).
 * It is called by the standard ${out:fieldname} syntax.
 * 
 * @author Yozons, Inc.
 */
public class FieldOut
    extends javax.servlet.jsp.tagext.SimpleTagSupport
{
    private String name;
    private String source;
    private String fieldSpec; // we display this if the 'name' cannot be determined

	public void setName(String str)
	{
		name = str;
	}

	public void setSource(String str)
	{
		source = str;
	}

	public void setFieldSpec(String str)
	{
		fieldSpec = str;
	}

	public void doTag() 
        throws java.io.IOException
    {
    	JspWriter out = getJspContext().getOut();
    	
    	// The generated page should have initialized its DocumentPageBean
        DocumentPageBean docPage = (DocumentPageBean)getJspContext().getAttribute(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId,PageContext.REQUEST_SCOPE);
        if ( docPage == null )
        {
            Application.getInstance().err("FieldOut tag could not find DocumentPageBean with id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId + " in the request scope.");
            if ( EsfString.isNonBlank(fieldSpec) ) {
            	out.write("<span class=\"viewFieldData-error\">");
            	out.write(fieldSpec);
            	out.write("</span>");            	
            }
            return;
        }
        
        EsfName docName = new EsfName(source);
        EsfName fieldName = new EsfName(name);
        
        out.write(docPage.buildFieldOutHtml(getJspContext(), docName, fieldName, fieldSpec, false));
    }
}
