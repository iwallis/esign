// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen.taglib;

import javax.servlet.jsp.JspWriter;

import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.util.HtmlUtil;

/**
 * This class is used to display the value specified as HTML, unless turned off, and optionally an &nbsp; if the value is blank.
 * 
 * @author Yozons, Inc.
 */
public class Out 
    extends javax.servlet.jsp.tagext.SimpleTagSupport
{
    private String value = "";
    private boolean toHtml = true;
    private String displayEmptyValue = "&nbsp;"; 
	private String displayClass = null;

	public void setToHtml(String str)
	{
		toHtml = EsfBoolean.toBoolean(str);
	}
	public void setToHtml(boolean v)
	{
		toHtml = v;
	}

	public void setValue(String str)
	{
		value = str;
	}

	public void setDisplayEmptyValue(String str)
	{
		displayEmptyValue = str;
	}
	
	public void setDisplayClass(String str)
	{
		displayClass = str;
	}
	
    public void doTag() 
        throws java.io.IOException
    {
    	JspWriter out = getJspContext().getOut();
    	   	
    	if ( EsfString.isBlank(value) )
    	{
    		if ( EsfString.isNonBlank(displayEmptyValue) )
    		{
    	    	if ( EsfString.isNonBlank(displayClass) )
    	    	{
					out.write("<span class=\"");
					out.write(displayClass);
					out.write("\">");
    	    	}
    			out.write(displayEmptyValue);
    	    	if ( EsfString.isNonBlank(displayClass) )
					out.write("</span>");
    		}
    	}
    	else
    	{
    		String htmlValue = ( toHtml ) ? HtmlUtil.toDisplayHtml(value) : value;

	    	if ( EsfString.isNonBlank(displayClass) )
	    	{
				out.write("<span class=\"");
				out.write(displayClass);
				out.write("\">");
	    	}
	    	out.write(htmlValue);
	    	if ( EsfString.isNonBlank(displayClass) )
				out.write("</span>");
    	}
    }
}
