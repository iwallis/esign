// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen.taglib;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.File;
import com.esignforms.open.prog.FileVersion;

/**
 * This class is used to display a link to a pre-defined file.
 * 
 * @author Yozons, Inc.
 */
public class FileOut 
    extends javax.servlet.jsp.tagext.SimpleTagSupport
{
    private String name; // required
    private String fieldSpec; // required 
    private String fileId; // optional 
	private String tagExtras; // optional
	private String linkText; // optional

	public void setName(String str)
	{
		name = str;
	}

	public void setFieldSpec(String str)
	{
		fieldSpec = str;
	}

	public void setFileId(String str)
	{
		fileId = str;
	}
	
	public void setTagExtras(String str)
	{
		tagExtras = str;
	}
	
	public void setLinkText(String str)
	{
		linkText = str;
	}
	
    public void doTag() 
        throws java.io.IOException
    {
    	JspWriter out = getJspContext().getOut();
    	
    	// The generated page should have initialized its DocumentPageBean
        DocumentPageBean docPage = (DocumentPageBean)getJspContext().getAttribute(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId,PageContext.REQUEST_SCOPE);
        if ( docPage == null )
        {
        	Application.getInstance().err("FileOut tag could not find DocumentPageBean with id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId + " in the request scope.");
            out.write(fieldSpec);
            return;
        }
        if ( docPage.context.hasNoTransaction() )
        {
        	Application.getInstance().err("FileOut tag called without a transaction context");
            out.write(fieldSpec);
            return;
        }
        
        EsfUUID fileUUID = EsfString.isNonBlank(fileId) ? new EsfUUID(fileId) : null;
        File file = docPage.getFile( new EsfName(name), fileUUID );
    	if ( file != null )
    	{
    		FileVersion fileVersion = ( docPage.context.doProductionResolve() ) ? file.getProductionFileVersion() : file.getTestFileVersion();
    		if ( fileVersion == null )
    		{
    			// If the file is defined within the document, the test version is okay
    			if ( file.getContainerId().equals(docPage.context.currDocumentVersion.getId()) )
    				fileVersion = file.getTestFileVersion();
        		if ( fileVersion == null )
        		{
        			Application.getInstance().err("FileOut tag called but couldn't find the file version for file id: " +  file.getId() + "; doProductionResolve: " + docPage.context.doProductionResolve());
        			out.write(fieldSpec);
        			return;
        		}
    		}
    		
    		if ( EsfString.isBlank(linkText) )
    			linkText = docPage.toHtml(file.getDisplayOrEsfName());

    		out.write("<a href=\"");
    		out.write(docPage.getExternalUrlContextPath());
    		out.write("/files/");
    		out.write(docPage.encode(name));
    		out.write("/");
    		out.write(docPage.encode(fileVersion.getFileFileName()));
    		out.write("?fvid=");
    		out.write(docPage.encode(fileVersion.getId().toPlainString()));
    		out.write("\" ");
    		if ( docPage.isNonBlank(tagExtras) )
    			out.write(tagExtras);
    		out.write(" onclick=\"esf.checkLinkConfirmOnPageUnload(false,this)\"");
    		out.write(">");
    		out.write(linkText);
    		out.write("</a>");
    	}
    	else
    		out.write(fieldSpec);
    }
}
