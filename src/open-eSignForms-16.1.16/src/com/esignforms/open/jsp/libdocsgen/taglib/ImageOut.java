// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen.taglib;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.prog.ImageVersionOverlayField;

/**
 * This class is used to display an image.
 * 11/2/2013 - Added support for overlay images and outputting overlay fields along with the image
 * 
 * @author Yozons, Inc.
 */
public class ImageOut 
    extends javax.servlet.jsp.tagext.SimpleTagSupport
{
    private String name; // required
    private String fieldSpec; // required 
    private String imageId; // optional 
	private String tagExtras; // optional

	public void setName(String str)
	{
		name = str;
	}

	public void setFieldSpec(String str)
	{
		fieldSpec = str;
	}

	public void setImageId(String str)
	{
		imageId = str;
	}
	
	public void setTagExtras(String str)
	{
		tagExtras = str;
	}
	
    public void doTag() 
        throws java.io.IOException
    {
    	JspWriter out = getJspContext().getOut();
    	
    	// The generated page should have initialized its DocumentPageBean
        DocumentPageBean docPage = (DocumentPageBean)getJspContext().getAttribute(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId,PageContext.REQUEST_SCOPE);
        if ( docPage == null )
        {
        	Application.getInstance().err("ImageOut tag could not find DocumentPageBean with id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId + " in the request scope.");
            out.write(fieldSpec);
            return;
        }
        if ( docPage.context.hasNoTransaction() )
        {
        	Application.getInstance().err("ImageOut tag called without a transaction context");
            out.write(fieldSpec);
            return;
        }
        
        EsfUUID imageUUID = EsfString.isNonBlank(imageId) ? new EsfUUID(imageId) : null;
        Image image = docPage.getImage( new EsfName(name), imageUUID );
    	if ( image != null )
    	{
    		ImageVersion imageVersion = ( docPage.context.doProductionResolve() ) ? image.getProductionImageVersion() : image.getTestImageVersion();
    		if ( imageVersion == null )
    		{
    			// If the image is defined within the document, the test version is okay
    			if ( image.getContainerId().equals(docPage.context.currDocumentVersion.getId()) )
    	    		imageVersion = image.getTestImageVersion();
        		if ( imageVersion == null )
        		{
        			Application.getInstance().err("ImageOut tag called but couldn't find the image version for image id: " +  image.getId() + "; doProductionResolve: " + docPage.context.doProductionResolve());
        			out.write(fieldSpec);
        			return;
        		}
    		}
    		
    		if ( imageVersion.hasOverlayFields() )
    			out.write("<div class=\"imageOverlay\">");
    		
    		// If the image wants to be embedded with a data URI and we're not on IE 5-7 (or IE8 if the data URI is more than 32k), we'll honor that request.
    		boolean useDataUri = imageVersion.isUseDataUri();
    		String dataUri = null;
    		if ( useDataUri )
    		{
    			dataUri = imageVersion.getDataUri();
    			if ( docPage.isUserAgentIE8() )
    				useDataUri = dataUri.length() < 32000;
    			else if ( docPage.isUserAgentIE5_6_or_7() )
    				useDataUri = false;
    		}
    		
    		if ( useDataUri )
    		{
        		out.write("<img src=\"");
        		out.write(dataUri);
        		out.write("\" ");
        		if ( docPage.isNonBlank(tagExtras) )
        			out.write(tagExtras);
        		out.write("/>");
    		}
    		else
    		{
        		out.write("<img src=\"");
        		out.write(docPage.getContextPath());
        		out.write("/images/");
        		out.write(docPage.encode(name));
        		out.write("/");
        		out.write(docPage.encode(imageVersion.getImageFileName()));
        		out.write("?ivid=");
        		out.write(docPage.encode(imageVersion.getId().toPlainString()));
        		out.write("\" ");
        		if ( docPage.isNonBlank(tagExtras) )
        			out.write(tagExtras);
        		out.write("/>");
    		}
    		
    		if ( imageVersion.hasOverlayFields() )
    		{
    	        EsfName docName = docPage.context.currDocument.getEsfName();

    			for( ImageVersionOverlayField ivof : imageVersion.getOverlayFields() )
    			{
    				FieldTemplate fieldTemplate = docPage.context.currDocumentVersion.getFieldTemplate(ivof.getFieldTemplateId());
    				if ( fieldTemplate != null )
    				{
    		        	if ( fieldTemplate.isTypeRadioButtonGroup() )
    		        		continue;

     	    	        EsfName fieldName = fieldTemplate.getEsfName();
    	    	        
    		    		String positionStyle = fieldTemplate.isTypeRadioButton() || fieldTemplate.isTypeCheckbox() 
    		    				? (" style=\"left: "+ivof.getPositionLeft()+"px; top: "+ivof.getPositionTop()+"px; "+ivof.getBackgroundColor()+"\"") 
    		    				: (" style=\"left: "+ivof.getPositionLeft()+"px; top: "+ivof.getPositionTop()+"px; width: "+ivof.getPositionWidth()+"px; height: "+ivof.getPositionHeight()+"px; "+ivof.getBackgroundColor()+"\"");
    		    				
    		    	    String html;
    		    	    
    		    	    if ( ivof.isDisplayModeOut() )
    		    			html = docPage.buildFieldOutHtml(getJspContext(),docName, fieldName, fieldSpec, true);
    		    	    else if ( ivof.isDisplayModeFieldLabel() )
    		    			html = docPage.buildFieldLabelHtml(getJspContext(),docName, fieldName, fieldSpec, true);
    		    	    else
    		    			html = docPage.buildFieldHtml(getJspContext(),docName, fieldName, fieldSpec, true);
    		    	    
    		    		out.write("<span class=\"imageOverlayFieldArea\"");
    		    		out.write(positionStyle); 
    		    		out.write(">");
    		    		out.write(html);
    		            out.write("</span>");
    				}
    			}
    			
    			out.write("</div>"); // end starting overlay DIV before the IMG tag
    		}
    	}
    	else
    		out.write(fieldSpec);
    }
}
