// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.jsp.libdocsgen.taglib;

import java.util.HashMap;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.data.SubscriptedEsfName;
import com.esignforms.open.jsp.libdocsgen.DocumentPageBean;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.util.HtmlUtil;

/**
 * This class is used to display the value retrieved from the transaction, and optionally an &nbsp; if the value is blank.
 * 
 * @author Yozons, Inc.
 */
public class TransactionOut 
    extends javax.servlet.jsp.tagext.SimpleTagSupport
{
    private String name; 
    private String fieldSpec; // we display this if the 'name' cannot be determined

	public void setName(String str)
	{
		name = str;
	}

	public void setFieldSpec(String str)
	{
		fieldSpec = str;
	}

    public void doTag() 
        throws java.io.IOException
    {
    	JspWriter out = getJspContext().getOut();
    	
    	// The generated page should have initialized its DocumentPageBean
        DocumentPageBean docPage = (DocumentPageBean)getJspContext().getAttribute(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId,PageContext.REQUEST_SCOPE);
        if ( docPage == null )
        {
        	Application.getInstance().err("TransactionOut tag could not find DocumentPageBean with id: " + com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId + " in the request scope.");
            if ( EsfString.isNonBlank(fieldSpec) )
            	out.write(fieldSpec);
            return;
        }
        
    	String value = null;

    	if ( docPage.context.transaction != null )
    	{
        	if ( "id".equalsIgnoreCase(name) ) {
        		value = docPage.context.transaction.getId().toHtml();
        	} else if ( "displayname".equalsIgnoreCase(name) ) {
        		value = HtmlUtil.toEscapedHtml(docPage.context.transactionTemplate.getDisplayName());
        	} else if ( "pathname".equalsIgnoreCase(name) ) {
        		value = docPage.context.transactionTemplate.getPathName().toHtml();
        	} else if ( "documents.todo".equalsIgnoreCase(name) ) {
        		value = createToDoListHtml(docPage);
        	} else if ( "user.id".equalsIgnoreCase(name) ) {
        		value = docPage.isUserLoggedIn() ? docPage.getLoggedInUser().getId().toHtml() : fieldSpec;
        	} else if ( "user.email".equalsIgnoreCase(name) ) {
           		value = docPage.isUserLoggedIn() ? HtmlUtil.toEscapedHtml(docPage.getLoggedInUser().getEmail()) : fieldSpec;
        	} else if ( "user.email_address".equalsIgnoreCase(name) ) {
            	value = docPage.isUserLoggedIn() ? HtmlUtil.toEscapedHtml(docPage.getLoggedInUser().getEmailAddress().toString()) : fieldSpec;
        	} else if ( "user.personal_name".equalsIgnoreCase(name) ) {
           		value = docPage.isUserLoggedIn() ? HtmlUtil.toEscapedHtml(docPage.getLoggedInUser().getPersonalName()) : fieldSpec;
        	} else if ( "user.family_name".equalsIgnoreCase(name) ) {
           		value = docPage.isUserLoggedIn() ? HtmlUtil.toEscapedHtml(docPage.getLoggedInUser().getFamilyName()) : fieldSpec;
        	} else if ( "user.employee_id".equalsIgnoreCase(name) ) {
           		value = docPage.isUserLoggedIn() && docPage.getLoggedInUser().hasEmployeeId() ? HtmlUtil.toEscapedHtml(docPage.getLoggedInUser().getEmployeeId()) : fieldSpec;
           	} else if ( "user.job_title".equalsIgnoreCase(name) ) {
           		value = docPage.isUserLoggedIn() && docPage.getLoggedInUser().hasJobTitle() ? HtmlUtil.toEscapedHtml(docPage.getLoggedInUser().getJobTitle()) : fieldSpec;
           	} else if ( "user.location".equalsIgnoreCase(name) ) {
           		value = docPage.isUserLoggedIn() && docPage.getLoggedInUser().hasLocation() ? HtmlUtil.toEscapedHtml(docPage.getLoggedInUser().getLocation()) : fieldSpec;
           	} else if ( "user.department".equalsIgnoreCase(name) ) {
           		value = docPage.isUserLoggedIn() && docPage.getLoggedInUser().hasDepartment() ? HtmlUtil.toEscapedHtml(docPage.getLoggedInUser().getDepartment()) : fieldSpec;
           	} else if ( "user.phone_number".equalsIgnoreCase(name) ) {
           		value = docPage.isUserLoggedIn() && docPage.getLoggedInUser().hasPhoneNumber() ? HtmlUtil.toEscapedHtml(docPage.getLoggedInUser().getPhoneNumber()) : fieldSpec;
           	} else {
        		SubscriptedEsfName subscriptedEsfName = SubscriptedEsfName.createSubscriptedEsfName(name);
            	if ( subscriptedEsfName != null ) 
            	{
            		EsfValue v = docPage.context.transaction.getRecord().getValueByName( subscriptedEsfName );
            		if ( v != null )
            			value = v.toHtml();
            		else
            			value = fieldSpec;
            	}
            	else
            		value = fieldSpec;
        	}
    	}
    	else
    		value = fieldSpec;
    	
    	out.write(value);
    }
    
    
    private String createToDoListHtml(DocumentPageBean docPage)
    {
    	StringBuilder html = new StringBuilder( (docPage.context.documents.size()+1) * 1024);
    	html.append("<table cellspacing=\"0\" cellpadding=\"7\">");
    	
    	// We keep this map so we don't fetch document images over and over for multiple documents of the same status
    	HashMap<EsfName,String> imageMap = new HashMap<EsfName,String>();
    	
    	String viewButtonLabel = null;
    	String viewButtonLabelTitle = null;
    	String viewButtonSuffix = null;
    	String editButtonLabel = null;
    	String editButtonLabelTitle = null;
    	String editButtonSuffix = null;
    	boolean hasMultipleDocuments = docPage.context.transactionPartyDocuments.size() > 1;
    	if ( hasMultipleDocuments || docPage.context.isEsfReportsAccess() )
    	{
    		viewButtonLabel = docPage.getButtonMessageVersion().getPackageButtonViewCompletedDocument(docPage.context);
    		viewButtonLabelTitle  = docPage.getButtonMessageVersion().getPackageButtonViewCompletedDocumentTitle(docPage.context);
    		viewButtonSuffix = docPage.context.currDocument.getId().toNormalizedEsfNameString();
    	}
    	if ( docPage.context.isEsfReportsAccessForUpdate() )
    	{
    		editButtonLabel = docPage.getButtonMessageVersion().getPackageButtonEditDocument(docPage.context);
    		editButtonLabelTitle  = docPage.getButtonMessageVersion().getPackageButtonEditDocumentTitle(docPage.context);
    		editButtonSuffix = docPage.context.currDocument.getId().toNormalizedEsfNameString();
    	}
    	
    	boolean foundDocumentThatNeedsWork = false;
    	int documentNumber = 0;
    	for( TransactionPartyDocument tranPartyDoc : docPage.context.transactionPartyDocuments )
    	{
    		Document document = docPage.context.getDocumentByTransactionDocumentId( tranPartyDoc.getTransactionDocumentId() );
    		if ( document == null )
    		{
            	Application.getInstance().err("TransactionOut tag createToDoListHtml(): Failed to find document #" + (documentNumber+1) + "; tranDocId: " + tranPartyDoc.getTransactionDocumentId() + "; tranId: " + docPage.context.transactionId);  			
    			continue;
    		}
    		++documentNumber;
    		html.append("<tr>");
    		html.append("<td align=\"right\">").append(documentNumber).append(".</td>");
    		html.append("<td>").append(HtmlUtil.toEscapedHtml(document.getDisplayName())).append("</td>");
    		
    		boolean isViewDocument = false;
    		
    		String imagePath, imageTitle;
    		if ( docPage.context.pkgVersionPartyTemplate.isView(document.getId()) || docPage.context.isEsfReportsAccessForView() || 
    				((tranPartyDoc.isViewOptional() || tranPartyDoc.isViewOptionalViewed()) && !docPage.context.isEsfReportsAccessForUpdate()) )
    		{
    			imagePath = createImagePath(imageMap,docPage,docPage.app.getImagePackageDocumentViewOnlyEsfName());
    			imageTitle = docPage.app.getServerMessages().getString("TransactionOut.documents.todo.image.title.viewOnly");
    			isViewDocument = true;
    		}
    		else if ( docPage.context.isEsfReportsAccessForUpdate() )
    		{
    			imagePath = createImagePath(imageMap,docPage,docPage.app.getImagePackageDocumentToDoEsfName()); // reports updater can update
    			imageTitle = docPage.app.getServerMessages().getString("TransactionOut.documents.todo.image.title.workNeeded");
    		}
    		else if ( tranPartyDoc.isFixedRequested() )
    		{
    			imagePath = createImagePath(imageMap,docPage,docPage.app.getImagePackageDocumentFixRequestedEsfName()); // can edit, but is in error so needs fixing
    			imageTitle = docPage.app.getServerMessages().getString("TransactionOut.documents.todo.image.title.fixRequested");
    			foundDocumentThatNeedsWork = true;
    		}
    		else if ( tranPartyDoc.isNetYetRetrieved() || tranPartyDoc.isRetrieved() )
    		{
    			imagePath = createImagePath(imageMap,docPage,docPage.app.getImagePackageDocumentToDoEsfName()); // waiting to be completed
    			imageTitle = docPage.app.getServerMessages().getString("TransactionOut.documents.todo.image.title.workNeeded");
    			foundDocumentThatNeedsWork = true;
    		}
    		else if ( tranPartyDoc.isCompleted() )
    		{
    			imagePath = createImagePath(imageMap,docPage,docPage.app.getImagePackageDocumentCompletedEsfName()); // all done
    			imageTitle = docPage.app.getServerMessages().getString("TransactionOut.documents.todo.image.title.completed");
    		}
    		else if ( tranPartyDoc.isRejected() )
    		{
    			imagePath = createImagePath(imageMap,docPage,docPage.app.getImagePackageDocumentRejectedEsfName());
    			imageTitle = docPage.app.getServerMessages().getString("TransactionOut.documents.todo.image.title.rejected");
    		}
    		else
    		{
    			docPage.err("TransactionOut.createToDoListHtml() - unexpected tran party doc status: " + tranPartyDoc.getStatus() + 
    					"; TransactionDocumentId: " + tranPartyDoc.getTransactionDocumentId() + "; TransactionPartyId: " + tranPartyDoc.getTransactionPartyId()); 
    			imagePath = createImagePath(imageMap,docPage,docPage.app.getImagePackageDocumentViewOnlyEsfName());
    			imageTitle = docPage.app.getServerMessages().getString("TransactionOut.documents.todo.image.title.viewOnly");
    		}
    		
    		html.append("<td><img src=\"").append(docPage.getContextPath()).append(imagePath).append("\" alt=\"document status icon\" title=\"").append(imageTitle).append("\"/>");
    		html.append("</td>");
    		if ( hasMultipleDocuments || docPage.context.isEsfReportsAccess() ) // when there's more than one document, we add a view button to all completed documents, and to all viewonly/viewoptional before we find one that needs work to be done
    		{
        		html.append("<td>");
    			if ( tranPartyDoc.isCompleted() || (! foundDocumentThatNeedsWork && isViewDocument) )
    			{
            		html.append("<input type=\"submit\" class=\"small\" name=\"");
            		html.append(DocumentPageBean.PACKAGE_VIEW_COMPLETED_DOCUMENT_BUTTON_PREFIX).append(viewButtonSuffix).append("_").append(tranPartyDoc.getDocumentNumber());
            		html.append("\" value=\"");
            		html.append(HtmlUtil.toEscapedHtml(viewButtonLabel));
            		html.append("\" title=\"");
            		html.append(HtmlUtil.toEscapedHtml(viewButtonLabelTitle));
            		html.append("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
    			}
    			else if ( docPage.context.isEsfReportsAccessForUpdate() )
    			{
            		html.append("<input type=\"submit\" class=\"small\" name=\"");
            		html.append(DocumentPageBean.PACKAGE_EDIT_DOCUMENT_BUTTON_PREFIX).append(editButtonSuffix).append("_").append(tranPartyDoc.getDocumentNumber());
            		html.append("\" value=\"");
            		html.append(HtmlUtil.toEscapedHtml(editButtonLabel));
            		html.append("\" title=\"");
            		html.append(HtmlUtil.toEscapedHtml(editButtonLabelTitle));
            		html.append("\" onclick=\"esf.checkConfirmOnPageUnload(false)\" />");
    			}
    			else
    			{
    				html.append("&nbsp;");
    			}
        		html.append("</td>");
    		}
    		html.append("</tr>");
    	}
    	html.append("</table>");
    	return html.toString();
    }
    
    private String createImagePath(HashMap<EsfName,String> imageMap, DocumentPageBean docPage, EsfName imageName)
    {
    	String imagePath = imageMap.get(imageName);
    	if ( imagePath != null )
    		return imagePath;
    	
    	Image image = docPage.getImage(imageName, null);
    	if ( image == null )
    		return "/images/" + imageName + "/"; // this will not resolve to an image since we couldn't find it
    	
    	ImageVersion imageVersion = image.getTestImageVersion();
    	
		imagePath = "/images/" + image.getEsfName() + "/" + imageVersion.getImageFileName() + "?ivid=" + imageVersion.getId().toHtml(); 
		imageMap.put(imageName, imagePath);
		return imagePath;
    }
}
