// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import com.esignforms.open.config.Literals;

/**
 * Defines an ESF Path Name field, which is basically an array of EsfName fields that are concatenated
 * into a path with specified separator.
 * 
 * The path cannot be null.  If it is null, the path will be set to NULL_ESFPATHNAME_IS_INVALID, and if
 * is otherwise not valid it will be set to ESFPATHNAME_IS_INVALID.
 *
 * @author Yozons, Inc.
 */
public class EsfPathName
	implements EsfValue, Comparable<EsfPathName>
{
	private static final long serialVersionUID = 4674662828518358502L;

	public static final String PATH_SEPARATOR	= "/";
    
    public static final String ESF_RESERVED_PATH_PREFIX = "ESF" + PATH_SEPARATOR;
    public static final String ESF_RESERVED_GROUP_PATH_PREFIX = ESF_RESERVED_PATH_PREFIX + "Group" + PATH_SEPARATOR;
    public static final String ESF_RESERVED_LIBRARY_PATH_PREFIX = ESF_RESERVED_PATH_PREFIX + "Library" + PATH_SEPARATOR;
    public static final String ESF_RESERVED_PACKAGE_PATH_PREFIX = ESF_RESERVED_PATH_PREFIX + "Package" + PATH_SEPARATOR;
    public static final String ESF_RESERVED_REPORT_TEMPLATE_PATH_PREFIX = ESF_RESERVED_PATH_PREFIX + "ReportTemplate" + PATH_SEPARATOR;
    public static final String ESF_RESERVED_TRANSACTION_TEMPLATE_PATH_PREFIX = ESF_RESERVED_PATH_PREFIX + "TransactionTemplate" + PATH_SEPARATOR;
    public static final String ESF_RESERVED_UI_PATH_PREFIX = ESF_RESERVED_PATH_PREFIX + "UI" + PATH_SEPARATOR;
    
    public static final String ESFNAME_RESERVED_PREFIX_IN_PATHNAME = PATH_SEPARATOR + EsfName.ESF_RESERVED_PREFIX;

    public static final String TYPE 			= "esfpathname";
    
    public static final String NULL_ESFPATHNAME_IS_INVALID	= "invalid_missing_pathname";
    public static final String ESFPATHNAME_IS_INVALID 		= "invalid_pathname_specified";

	private String value;
	private EsfName[] names;
	
	public EsfPathName()
	{
		setInvalidNullPathName();
	}
	
	public EsfPathName(String v)
	{
		setPathName(v);
	}
	
	public EsfPathName(EsfString v)
	{
		this( v == null || v.isNull() ? null : v.toPlainString() );
	}
	
	public EsfPathName(EsfName... v)
	{
		setPathName(v);
	}
	
	
	public static boolean isReservedPathPrefix(String v)
	{
		return v != null && v.toLowerCase().startsWith(ESF_RESERVED_PATH_PREFIX.toLowerCase());
	}
	
	public static boolean isEsfNameReservedPrefixInPathName(String v)
	{
		return v != null && v.toLowerCase().contains(ESFNAME_RESERVED_PREFIX_IN_PATHNAME.toLowerCase());
	}
	
	public static boolean isValidEsfPathName(String v)
	{
		if ( EsfString.isBlank(v) || v.endsWith(PATH_SEPARATOR) )
			return false;
		
		String[] names = v.split(PATH_SEPARATOR);
    	for( String n : names )
    	{
    		if ( ! EsfName.isValidEsfName(n) )
    			return false;
    	}
		
		return true;
	}
	
	public static boolean isValidEsfPathName(EsfName...esfnames)
	{
		if ( esfnames == null || esfnames.length == 0 )
			return false;
		
    	for( EsfName n : esfnames )
    	{
    		if ( n == null || ! n.isValid() )
    			return false;
    	}
		
		return true;
	}

	
	private void setInvalidNullPathName()
	{
		value = NULL_ESFPATHNAME_IS_INVALID;
		names = new EsfName[1];
		names[0] = new EsfName(NULL_ESFPATHNAME_IS_INVALID);
	}
	
	private void setInvalidPathName()
	{
		value = ESFPATHNAME_IS_INVALID;
		names = new EsfName[1];
		names[0] = new EsfName(ESFPATHNAME_IS_INVALID);
	}
	
	private void setPathName(String v)
	{
		if ( EsfString.isBlank(v) )
			setInvalidNullPathName();
		else
		{
			String[] vSplit = v.split(PATH_SEPARATOR);
			value = v;
	    	names = new EsfName[vSplit.length];
	    	int i = 0;
	    	for( String n : vSplit )
	    	{
	    		names[i] = new EsfName(n);
	    		if ( ! names[i].isValid() )
	    		{
	    			setInvalidPathName();
	    			break;
	    		}
	    		++i;
	    	}
		}
	}
	private void setPathName(EsfName... v)
	{
		if ( v == null || v.length == 0 )
			setInvalidNullPathName();
		else
		{
			value = namesToPathName(v);
			names = v;
			for( EsfName n : v )
			{
				if ( n == null || n.isNull() )
				{
					setInvalidNullPathName();
					break;
				}
				else if ( ! n.isValid() )
				{
					setInvalidPathName();
					break;
				}
			}
		}
	}
	
	
	public boolean hasReservedPathPrefix()
	{
		return isReservedPathPrefix(value);
	}
	
	public boolean hasEsfNameReservedPrefixInPathName()
	{
		return isEsfNameReservedPrefixInPathName(value);
	}

	
	/**
	 * Converts an array of EsfName in a Stringified path name, which is each component separated by a path separator '/' character.
	 * @param esfnames the EsfName array converted to a pathname
	 * @return the path name represented by the array of names
	 */
    private String namesToPathName(final EsfName... esfnames)
    {
     	StringBuilder pathName = new StringBuilder(esfnames.length * Literals.ESFNAME_MAX_LENGTH);
    	for( EsfName n : esfnames )
    	{
    		if ( pathName.length() > 0 )
    			pathName.append(PATH_SEPARATOR);
    		pathName.append(n.toPlainString());
    	}
        return pathName.toString();
    }

	/**
	 * This will return a new instance using 'fromXml' as the source.  The 'fromXml'
	 * is stored in the name-value pairs using the corresponding toXml() method.
	 * @param fromXml the String that was stored in the XML for name-value pairs.
	 * @return a new value object.
	 * @see #toXml()
	 */
	public static EsfPathName createFromToXml(String fromXml)
	{
		return new EsfPathName( fromXml );
	}
	
	/**
	 * This will return a string that represents this value's "simple type" rather than the class name, suitable for
	 * storing with the name-value data in XML.
	 * @return a String literal "esfpathname"
	 */
	@Override
	public String getType()
	{
		return TYPE;
	}

	/**
	 * This will return a true if the underlying value is null
	 * @return a boolean true if the underlying value is null
	 */
	@Override
	public boolean isNull()
	{
		return value == null || value.equals(NULL_ESFPATHNAME_IS_INVALID);
	}

	/**
	 * This will return a duplicate of the value so that it has the same value, but is a distinct object.
	 * @return a new EsfValue that has the value as this object.
	 */
	@Override
	public EsfPathName duplicate()
	{
		return new EsfPathName( value );
	}
	
	/**
	 * Returns a one element array containing this value.
	 */
	@Override
	public EsfPathName[] toArray()
	{
		EsfPathName[] a = new EsfPathName[1];
		a[0] = this;
		return a;
	}
	
	/**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.
	 * @return the value as pretty String
	 */
	@Override
    public final String toString()
    {
    	return toPlainString();
    }
    
	/**
	 * Return the value as a "plain" string, with no adornments, such as for storing in a DB
	 * @return the value as plain String; 
	 */
    @Override
    public final String toPlainString()
    {
    	return value;
    }

    /**
     * Return the value as a String suitable for HTML 
     * @return the value encoded as HTML
     */
    public final String toHtml()
    {
    	return toPlainString(); // already HTML safe
    }

    /**
     * Return the value as a String suitable for XML 
     * @return the value encoded as XML
     */
    @Override
    public final String toXml()
    {
    	return toPlainString(); // already XML safe
    }
    public final long getEstimatedLengthXml()
    {
    	return getLength();
    }
    

	/**
	 * Sets a new path name if 'v' is valid.  Otherwise it does not change its existing value.
	 * @param v the new String path name to use
	 */
	public void setEsfPathName(String v)
	{
		if ( isValidEsfPathName(v) )
			setPathName(v);
	}
	
	public void setEsfPathName(EsfName... v)
	{
		if ( isValidEsfPathName(v) )
			setPathName(v);
	}
	
	public EsfName[] getNames()
	{
		return names;
	}

	/**
	 * Determines if the EsfPathName is valid.  It will be unless it's one of the invalid strings we set
	 * it to.
	 * @return true if the name is not null or ESFPATHNAME_IS_INVALID.
	 */
	public boolean isValid()
	{
		return ! isNull() && ! value.equals(ESFPATHNAME_IS_INVALID);
	}
	
	@Override
	public boolean equals(Object other)
	{
		if ( other instanceof EsfPathName )
			return value.equalsIgnoreCase(((EsfPathName)other).value);
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return value.toLowerCase().hashCode();
	}
	
	@Override
	public int compareTo(EsfPathName other)
	{
		return value.compareToIgnoreCase(other.value);
	}
	
	public String toLowerCase()
	{
		return value.toLowerCase();
	}
	
	public int getLength()
	{
		return isValid() ? value.length() : 0;
	}
	
    public static EsfPathName ensureNotNull(EsfPathName v) 
    {
    	return v == null ? new EsfPathName() : v;
    }
  
}