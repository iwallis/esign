// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.esignforms.open.Application;
import com.esignforms.open.util.DateUtil;
import com.esignforms.open.util.HtmlUtil;

/**
 * Defines a Date field (yyyy-mm-dd generically speaking), with the internal Date's time component normalized to 00:00:00.000 UTC/GMT/Zulu.
 *
 * @author Yozons, Inc.
 */
public class EsfDate
	implements EsfValue, java.lang.Comparable<EsfDate>
{
	private static final long serialVersionUID = -8118846636286667936L;

	public static final String TYPE = "date";

    // dd-Mon-YYYY finder
    public static final java.util.regex.Pattern DDMonYYYYPattern = java.util.regex.Pattern.compile("([0-3]?\\d)[-/. ]?([a-zA-Z]{3})[-/. ]?(\\d{2,4})");
    // mm/dd/YYYY finder
    public static final java.util.regex.Pattern MDYPattern = java.util.regex.Pattern.compile("([0-1]?\\d)[-/. ]?([0-3]?\\d)[-/. ]?(\\d{2,4})");
    // mm/[YY]YY finder for credit card expiration dates
    public static final java.util.regex.Pattern MMYYYYPattern = java.util.regex.Pattern.compile("([0-1]?\\d)[-/. ]?(\\d{2,4})");
    // yyyy-mm-dd finder
    public static final java.util.regex.Pattern YMDPattern = java.util.regex.Pattern.compile("(\\d{4})[-/.]?([0-1]?\\d)[-/.]?([0-3]?\\d)");
    // dd/mm/YYYY finder
    public static final java.util.regex.Pattern DMYPattern = java.util.regex.Pattern.compile("([0-3]?\\d)[-/. ]?([0-1]?\\d)[-/. ]?(\\d{2,4})");

    
	private Date 	value;
	private Locale  locale   = Application.getInstance().getDefaultLocale();
	
	public EsfDate()
	{
		value = DateUtil.normalizeTimeToNoonUTC(new Date());
	}
	
	public EsfDate(TimeZone normalizeTz)
	{
		this(new EsfDateTime(),normalizeTz);
	}
	
	public EsfDate(EsfDate v)
	{
		value 	= v == null ? null : v.value;
		locale 	= v == null ? null : v.locale;
	}
	
	public EsfDate(Date v)
	{
		value = v == null ? null : DateUtil.normalizeTimeToNoonUTC( v );
	}
	
	public EsfDate(java.sql.Date v)
	{
		value = v == null ? null : DateUtil.normalizeTimeToNoonUTC( v );
	}
	
	public EsfDate(EsfDateTime v)
	{
		value = v == null ? null : DateUtil.normalizeTimeToNoonUTC( v.toDate() );
	}
	
	public EsfDate(EsfDateTime v, TimeZone normalizeTz)
	{
		value = v == null ? null : DateUtil.normalizeTimeToNoon( v.toDate(), v.getTimeZone(), normalizeTz );
	}
	
	/**
	 * This will return a new instance using 'fromXml' as the source.  The 'fromXml'
	 * is stored in the name-value pairs using the corresponding toXml() method.
	 * @param fromXml the String that was stored in the XML for name-value pairs.
	 * @return a new value object.
	 * @see #toXml()
	 */
	public static EsfDate createFromToXml(String fromXml)
	{
		return CreateFromXmlDate( fromXml );
	}
	
	/**
	 * Creates an EsfDate from a date of unspecified format.  It first tries DD-Mon-YYYY, then YYYY-MM-DD, then MM/DD/YYYY
	 * @param unknownDateString the String date of unknown format
	 * @return the EsfDate based on guessing the format; null if none known
	 */
    public static EsfDate CreateGuessFormat( String unknownDateString )
    {
    	if ( EsfString.isBlank(unknownDateString) )
    		return new EsfDate((java.util.Date)null);
    	
    	// Handle a few special case values we support before assuming it's just a date string
		if ( "now".equalsIgnoreCase(unknownDateString) )
			return new EsfDate();
		
		if ( unknownDateString.startsWith("+") || unknownDateString.startsWith("-") )
		{
			// This should be a +numDays or -numDays spec
			String numDaysString = ( unknownDateString.charAt(0) == '+' ) ? unknownDateString.substring(1) : unknownDateString; // remove the leading + for 'stringToInt'
			int numDays = Application.getInstance().stringToInt(numDaysString, 0);
			EsfDate d = new EsfDate();
			d.addDays(numDays);
			return d;
		}

    	// Let's start with the most distinct DD-Mon-YYYY format first
    	String DMYFixup = DateUtil.fixupDDMonYYYYDate(unknownDateString);
        java.util.regex.Matcher m = DDMonYYYYPattern.matcher(DMYFixup);
    	if ( m != null && m.matches() )
    		return CreateFromDDMonYYYY(DMYFixup);

    	// Let's next try with the YMD standard
    	String YMDFixup = DateUtil.fixupYMDDate(unknownDateString);
    	m = YMDPattern.matcher(YMDFixup);
    	if ( m != null && m.matches() )
    		return CreateFromYMD(YMDFixup);
    	
    	// Maybe it's a just a mm/yyyy format?
    	String MMYYYYFixup = DateUtil.fixupMMYYYYDate(unknownDateString);
    	m = MMYYYYPattern.matcher(MMYYYYFixup);
    	if ( m != null && m.matches() )
    		return CreateFromMMYYYY(MMYYYYFixup);
    	
    	// Better be a US MDY date...
    	return CreateFromMDY(unknownDateString);
    }
    
    /**
     * Convert an XML date in yyyy-mm-dd standard form to an EsfDate.
     * @param xmlDate the XML date in yyyy-mm-dd format
     * @return the EsfDate that represents the specified date.
     * @see #CreateEsfDateFromYMD(String)
     */
    public static EsfDate CreateFromXmlDate( String xmlDate )
    {
    	return CreateFromYMD(xmlDate);
    }
    
    /**
     * Create an EsfDate object from an dd-Mon-yyyy date.
     * @param ddmonyyyy the d[d]-Mon-yy[yy] (or d[d]Monyy[yy]) date to convert to an EsfDate 
     * @return the EsfDate object for that date
     */
    public static EsfDate CreateFromDDMonYYYY( String dmy )
    {
    	if ( EsfString.isBlank(dmy) )
    		return new EsfDate((java.util.Date)null);

    	dmy = DateUtil.fixupDDMonYYYYDate(dmy);
    	
    	Date d;
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( "dd-MMM-yyyy" );
        try
        {
            d = fmt.parse(dmy);
        }
        catch( java.text.ParseException e )
        {
            try
            {
            	fmt = new java.text.SimpleDateFormat( "ddMMMyyyy" );
                d = fmt.parse(dmy);
            }
            catch( java.text.ParseException e2 )
            {
            	return new EsfDate((java.util.Date)null);
            }
        }
        return new EsfDate(d);
    }
        
    /**
     * Create an EsfDate object from an yyyy-mm-dd date.
     * @param ymd the yy[yy]-m[m]-d[d] (or yy[yy]/m[m]/d[d]) date to convert to an EsfDate 
     * @return the EsfDate object for that date; null if invalid
     */
    public static EsfDate CreateFromYMD( String ymd )
    {
    	if ( EsfString.isBlank(ymd) )
    		return new EsfDate((java.util.Date)null);
    	
    	ymd = DateUtil.fixupYMDDate(ymd);
    	
    	Date d;
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( "yyyy-MM-dd" );
        try
        {
            d = fmt.parse(ymd);
        }
        catch( java.text.ParseException e )
        {
            try
            {
            	fmt = new java.text.SimpleDateFormat( "yyyy/MM/dd" );
                d = fmt.parse(ymd);
            }
            catch( java.text.ParseException e2 )
            {
            	return new EsfDate((java.util.Date)null);
            }
        }
        return new EsfDate(d);
    }
    
    /**
     * Create an EsfDate object from an mm/dd/yyyy date.
     * @param mdy the m[m]/d[d]/yy[yy] (or m[m]-d[d]-yy[yy]) date to convert to an EsfDate 
     * @return the EsfDate object for that date; null if invalid
     */
    public static EsfDate CreateFromMDY( String mdy )
    {
    	if ( EsfString.isBlank(mdy) )
    		return new EsfDate((java.util.Date)null);

    	mdy = DateUtil.fixupMDYDate(mdy);
    	
    	Date d;
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( "MM/dd/yyyy" );
        try
        {
            d = fmt.parse(mdy);
        }
        catch( java.text.ParseException e )
        {
            try
            {
            	fmt = new java.text.SimpleDateFormat( "MM-dd-yyyy" );
                d = fmt.parse(mdy);
            }
            catch( java.text.ParseException e2 )
            {
            	return new EsfDate((java.util.Date)null);
            }
        }
        return new EsfDate(d);
    }
    
    /**
     * Create an EsfDate object from an mm/yyyy date.
     * @param mmyyyy the m[m]/yy[yy] (or m[m]-yy[yy]) date to convert to an EsfDate; the day will be the last day of the month specified
     * @return the EsfDate object for that date; null if invalid
     */
    public static EsfDate CreateFromMMYYYY( String mmyyyy )
    {
    	if ( EsfString.isBlank(mmyyyy) )
    		return new EsfDate((java.util.Date)null);

    	return CreateFromMDY( DateUtil.fixupMMYYYYToMDYDate(mmyyyy) );
    }
    
    /**
     * Create an EsfDate object from an dd/mm/yyyy date.
     * @param dmy the d[d]/m[m]/yy[yy] (or d[d]-m[m]-yy[yy]) date to convert to an EsfDate 
     * @return the EsfDate object for that date; null if invalid
     */
    public static EsfDate CreateFromDMY( String dmy )
    {
    	if ( EsfString.isBlank(dmy) )
    		return new EsfDate((java.util.Date)null);

    	dmy = DateUtil.fixupDMYDate(dmy);
    	
    	Date d;
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( "dd/MM/yyyy" );
        try
        {
            d = fmt.parse(dmy);
        }
        catch( java.text.ParseException e )
        {
            try
            {
            	fmt = new java.text.SimpleDateFormat( "dd-MM-yyyy" );
                d = fmt.parse(dmy);
            }
            catch( java.text.ParseException e2 )
            {
            	return new EsfDate((java.util.Date)null);
            }
        }
        return new EsfDate(d);
    }
    
    /**
     * Create an EsfDate object from an ISO standard dd.mm.yyyy date.
     * @param dmy the d[d].m[m].yy[yy] date to convert to an EsfDate 
     * @return the EsfDate object for that date; null if invalid
     */
    public static EsfDate CreateFromIsoDMY( String dmy )
    {
    	if ( EsfString.isBlank(dmy) )
    		return new EsfDate((java.util.Date)null);

    	dmy = DateUtil.fixupIsoDMYDate(dmy);
    	
    	Date d;
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( "dd.MM.yyyy" );
        try
        {
            d = fmt.parse(dmy);
        }
        catch( java.text.ParseException e )
        {
        	return new EsfDate((java.util.Date)null);
        }
        return new EsfDate(d);
    }
    
	/**
	 * This will return a string that represents this value's "simple type" rather than the class name, suitable for
	 * storing with the name-value data in XML.
	 * @return a String literal "date"
	 */
    @Override
	public String getType()
	{
		return TYPE;
	}

	/**
	 * This will return a true if the underlying value is null
	 * @return a boolean true if the underlying value is null
	 */
    @Override
	public boolean isNull()
	{
		return value == null;
	}

	/**
	 * This will return a duplicate of the value so that it has the same value, but is a distinct object.
	 * @return a new EsfValue that has the value as this object.
	 */
    @Override
	public EsfDate duplicate()
	{
		EsfDate d = value == null ? new EsfDate((EsfDate)null) : new EsfDate( toDate() );
		d.setLocale(getLocale());
		return d;
	}
	
	/**
	 * Returns a one element array containing this value.
	 */
	@Override
	public EsfDate[] toArray()
	{
		EsfDate[] a = new EsfDate[1];
		a[0] = this;
		return a;
	}
	
	/**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.
	 * @return the value as pretty String
	 */
    @Override
    public String toString()
    {
    	if ( value == null ) return "";
    	SimpleDateFormat fmt = new SimpleDateFormat(Application.getInstance().getDefaultDateFormat());
    	fmt.setTimeZone(DateUtil.getUtcTimeZone());
    	return fmt.format(value);
    }

    public String toLogString()
    {
    	if ( value == null ) return "";
    	SimpleDateFormat fmt = new SimpleDateFormat(Application.getInstance().getDefaultLogDateFormat());
    	fmt.setTimeZone(DateUtil.getUtcTimeZone());
    	return fmt.format(value);
    }

    /**
	 * Return the date in yyyymmdd format.
	 * @return the value as plain yyyymmdd String
	 */
    @Override
    public String toPlainString()
    {
    	if ( value == null ) return null;
    	SimpleDateFormat plainDateFmt = new SimpleDateFormat( "yyyyMMdd" );
    	plainDateFmt.setTimeZone(DateUtil.getUtcTimeZone());
    	return plainDateFmt.format(value);
    }
    
    public String format(String format)
    {
    	if ( value == null ) return "";
    	SimpleDateFormat fmt;
    	try
    	{
    		fmt = new SimpleDateFormat(format);
    	}
    	catch( Exception e ) 
    	{
    		Application app = Application.getInstance();
    		app.except(e, "EsfDate.format() - Invalid format specified: " + format);
    		fmt = new SimpleDateFormat(app.getDefaultDateFormat());
    	}
    	fmt.setTimeZone(DateUtil.getUtcTimeZone());
    	
    	String formattedDate = fmt.format(value);
    	
    	// If the date format contains the literal string ORD, we'll attempt to convert the number in front to an ordinal value
    	int ordinalStartIndex = formattedDate.indexOf("ORD");
    	if ( ordinalStartIndex > 0 )
    	{
    		// Let's find the number just before the ORD literal
    		int indexOfFirstDigit = -1; // assume not found
    		for( int i = (ordinalStartIndex-1); i >= 0; --i )
    		{
    			if ( Character.isDigit(formattedDate.charAt(i)) )
    				indexOfFirstDigit = i;
    			else
    				break;
    		}
    		
    		// If we found digits, let's make it ordinal then
    		if ( indexOfFirstDigit >= 0 )
    		{
    			String numString = formattedDate.substring(indexOfFirstDigit, ordinalStartIndex);
    			String ordinalString =  Application.getInstance().addOrdinalSuffixToNum(numString);
    			formattedDate = formattedDate.replace(numString+"ORD", ordinalString);
    		}
    	}
    	
    	return formattedDate;
    }
    
    public String formatToHtml(String format)
    {
    	return HtmlUtil.toEscapedHtml(format(format));
    }
    
    /**
     * Return the value as a String suitable for HTML 
     * @return the value encoded as HTML
     */
    @Override
    public String toHtml()
    {
    	return toXml();
    }
    
    /**
     * Return the value as a String suitable for XML 
     * @return the value encoded as XML
     */
    @Override
    public String toXml()
    {
    	if ( value == null ) return "";
    	java.text.SimpleDateFormat xmlDateFmt = new java.text.SimpleDateFormat( "yyyy-MM-dd" );
    	xmlDateFmt.setTimeZone(DateUtil.getUtcTimeZone());
    	return xmlDateFmt.format(value);
    }
    @Override
    public final long getEstimatedLengthXml()
    {
    	return 10;
    }
    
    public long getEstimatedLengthHtml()
    {
    	return getEstimatedLengthXml();
    }
    
    /**
     * Gets the locale for this date. 
     * @return the locale of this object
     */
    public Locale getLocale()
    {
    	return locale;
    }
    
    /**
     * Sets the locale for this date. 
     * @param l the locale to use; if null, the default locale is used.
     */
    public void setLocale(Locale l)
    {
    	locale = l == null ? Application.getInstance().getDefaultLocale() : l;
    }
    
	/**
	 * Returns the current date as a long.
	 * @return the long that represents the date.
	 */
    public final long toNumber()
    {
        return value == null ? 0 : value.getTime();
    }

    /**
     * Converts the date to an SQL Date object.
     * @return the SQL date
     */
    public final java.sql.Date toSqlDate()
    {
        return value == null ? null : new java.sql.Date( toNumber() );
    }
    
    /**
     * Converts this date into a Java date object.
     * @return the Java date
     */
    public final Date toDate()
    {
    	return value == null ? null : new Date( toNumber() );
    }
    
    /**
     * Convert the date to US standard MM/DD/YYYY format.
     * @return the date as MM/DD/YYYY format
     */
    public String toMDYString()
    {
    	if ( value == null ) return "";
    	DateFormat fmt = new java.text.SimpleDateFormat( "MM/dd/yyyy" );
    	fmt.setTimeZone(DateUtil.getUtcTimeZone());
		return fmt.format( value );    	
    }

    /**
     * Convert the date to credit card expiration standard MM/YYYY format.
     * @return the date as MM/YYYY format
     */
    public String toMMYYYYString()
    {
    	if ( value == null ) return "";
    	DateFormat fmt = new java.text.SimpleDateFormat( "MM/yyyy" );
    	fmt.setTimeZone(DateUtil.getUtcTimeZone());
		return fmt.format( value );    	
    }

    /**
     * Convert the date to standard yyyy-mm-dd format.
     * @return the date as yyyy-mm-dd format
     */
    public String toYMDString()
    {
    	if ( value == null ) return "";
		DateFormat fmt = new java.text.SimpleDateFormat( "yyyy-MM-dd" );
    	fmt.setTimeZone(DateUtil.getUtcTimeZone());
		return fmt.format( value );    	
    }

    /**
     * Convert the date to universal dd-MON-yyyy format.
     * @return the date as dd-MON-yyyy format
     */
    public String toDDMonYYYYString()
    {
    	if ( value == null ) return "";
		DateFormat fmt = new java.text.SimpleDateFormat( "dd-MMM-yyyy" );
    	fmt.setTimeZone(DateUtil.getUtcTimeZone());
		return fmt.format( value );    	
    }
    
    /**
     * Convert the date to euro dd-mm-yyyy format.
     * @return the date as dd-mm-yyyy format
     */
    public String toDDMMYYYYString()
    {
    	if ( value == null ) return "";
		DateFormat fmt = new java.text.SimpleDateFormat( "dd-MM-yyyy" );
    	fmt.setTimeZone(DateUtil.getUtcTimeZone());
		return fmt.format( value );    	
    }
    
    /**
     * Convert the date to ISO dd.mm.yyyy format.
     * @return the date as dd.mm.yyyy format
     */
    public String toIsoDDMMYYYYString()
    {
    	if ( value == null ) return "";
		DateFormat fmt = new java.text.SimpleDateFormat( "dd.MM.yyyy" );
    	fmt.setTimeZone(DateUtil.getUtcTimeZone());
		return fmt.format( value );    	
    }
    
    /**
     * Formats the date in the 'long' wordy way, such as January 4, 2010, based on the locale.
     * @return the long date format
     */
    public String toLongString()
    {
    	if ( value == null ) return "";
    	DateFormat fmt = DateFormat.getDateInstance(DateFormat.LONG,locale);
    	fmt.setTimeZone(DateUtil.getUtcTimeZone());
    	return fmt.format(value);
    }

    /**
     * Returns the year, month and day as numbers in the array in this order.
     * @return an integer array with [0]=year, [1]=month, [2]=day
     */
    public final int[] getYMDNumbers()
    {
    	return DateUtil.getYMDNumbers(value);
    }

    /**
     * Returns the 4-digit year, 2-digit month and 2-digit day as Strings in the array in this order.
     * @return a String array with [0]=YYYY, [1]=MM, [2]=DD
     */
    public final String[] getYMDStrings()
    {
    	return DateUtil.getYMDStrings(value);
    }

    /**
     * Returns the 4-digit year, 2-digit month, 2-digit day (ordinal, 1st, 2nd, 3rd...), and month name as Strings in the array in this order.
     * @return a String array with [0]=YYYY, [1]=MM, [2]=DDst (ordinal), [3]=monthname
     */
    public final String[] getYMDOrdinalStrings()
    {
    	Application app = Application.getInstance();
    	
        String[] values = new String[4];
        
        Calendar cal = Calendar.getInstance(DateUtil.getUtcTimeZone());
        cal.setTime(value);
        
        values[0] = Integer.toString(cal.get(Calendar.YEAR));
        values[1] = app.make2digits(cal.get(Calendar.MONTH)+1);
        values[2] = app.addOrdinalSuffixToNum(cal.get(Calendar.DAY_OF_MONTH));
        values[3] = getMonthName();
        
        return values;
    }

    /**
     * Returns the the 4-digit year as String
     * @return a String year.
     */
    public String getYearString()
    {
    	if ( value == null ) return "";
		DateFormat fmt = new java.text.SimpleDateFormat( "yyyy" );
		fmt.setTimeZone(DateUtil.getUtcTimeZone());
		return fmt.format( value );    	
    }

    /**
     * Returns the the full name of the month.
     * @return a String name of the month based on the locale.
     */
    public String getMonthName()
    {
    	if ( value == null ) return "";
		DateFormat fmt = new java.text.SimpleDateFormat( "MMMM", locale );
		fmt.setTimeZone(DateUtil.getUtcTimeZone());
		return fmt.format( value );    	
    }

    /**
     * Returns the the name of the month.
     * @return a String abbreviation of the month based on the locale.
     */
    public String getMonthAbbrev()
    {
    	if ( value == null ) return "";
		DateFormat fmt = new java.text.SimpleDateFormat( "MMM", locale );
		fmt.setTimeZone(DateUtil.getUtcTimeZone());
		return fmt.format( value );    	
    }
    
    public final boolean isBefore( EsfDate other )
    {
    	if ( value == null )
    	{
    		if ( other != null && other.value != null )
    			return true;
    		return false;
    	}
    	if ( other == null || other.value == null ) 
    		return false;
    	return value.before(other.value);
    }

    public final boolean isAfter( EsfDate other )
    {
    	if ( value == null )
    		return false;
    	if ( other == null || other.value == null ) 
    		return true;
    	return value.after(other.value);
    }

    public int compareTo(EsfDate o)
    {
    	if ( value == null )
    		return o != null && o.value != null ? -1 : 0;
    	if ( o == null || o.value == null ) return 1;
    	return value.compareTo(o.value);
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o instanceof EsfDate )
        {
        	EsfDate other = (EsfDate)o;
        	if ( value == null && other.value == null ) 
        		return true;
            return value.equals(other.value);
        }
        return false;
    }
	@Override
	public int hashCode()
	{
		return value == null ? 0 : value.hashCode();
	}

    /**
     * Add/subtract the specified number of days from the current date.
     * @param numDays the number of days to add or subtract
     * @return this object (for simple chaining)
     */
    public EsfDate addDays(int numDays)
    {
    	value = DateUtil.dateDaysFromDate(numDays, value);
    	return this;
    }

    /**
     * Returns true if the date is a weekday.
     * @return true if this is a weekday.
     */
    public boolean isWeekDay()
    {
    	return DateUtil.isWeekDay(value);
    }
    
    /**
     * Adds the specified number of week days (currently has no support for holidays)
     * @param numWeekDays the number of weekdays to add/subtract
     * @return this object (for simple chaining)
     */
    public EsfDate addWeekDays( int numWeekDays )
    {
    	int addDay   = (numWeekDays < 0) ? -1 : 1;
    	int numTimes = (numWeekDays < 0) ? numWeekDays*-1 : numWeekDays;
    	
    	while( numTimes-- > 0 )
    	{
    		do // get the next/previous weekday
    		{
    			addDays(addDay);
    		} 
    		while( ! isWeekDay() );
        }
    	return this;
    }

    /**
     * Add/subtract the specified number of years from the current date.
     * @param numYears the number of years to add or subtract
     * @return this object (for simple chaining)
     */
    public EsfDate addYears( int numYears )
    {
    	value = DateUtil.dateYearsFromDate(numYears, value);
    	return this;
    }

    public EsfDate addDateInterval(int numUnits, String intervalUnit)
    {
    	value = DateUtil.normalizeTimeToNoonUTC( DateUtil.dateTimeIntervalFromDate(numUnits, intervalUnit, value) );
    	return this;
    }
        
    
    public EsfDate nextDateForDayOfMonth(int dayOfMonth)
    {
    	value = DateUtil.nextDateForDayOfMonth(dayOfMonth, value, DateUtil.getDefaultTimeZone());
    	return this;
    }

    public EsfDate nextDateForDayOfMonthUnlessAlreadyIs(int dayOfMonth)
    {
    	value = DateUtil.nextDateForDayOfMonthUnlessAlreadyIs(dayOfMonth, value, DateUtil.getDefaultTimeZone());
    	return this;
    }

    /**
     * Calculates the number of 24 hour days from this date to the otherDate.
     * @param otherDate the other date
     * @return the int number of days until otherDate is reached.
     */
    public int daysUntil(EsfDate otherDate)
    {
        return (int)(( otherDate.toNumber() - toNumber() ) / (1000L*60L*60L*24L)); // number of msecs per day
    }
    
    /**
     * Gets the age in years of this date, such a date of birth.  It does not take into consideration time of day.
     * @return the int number of years between this date and the current date.
     */
    public int getAgeInYears()
    {
    	return (new EsfDate()).getYearsSince( this );
    }
    
    /** 
     * Gets the number of years since another date, presumably in the past.
     * @param otherDate the other date
     * @return the int number of years since the otherDate. If it's more than a year in the future, this will be negative.
     */
    public int getYearsSince(EsfDate otherDate)
    {
    	return DateUtil.yearsBetween(otherDate.value, value);
    }
    
    /**
     * Gets the number of years until another date, presumably in the future.
     * @param otherDate the other date
     * @return the int number of years until the otherDate.  If it's more than a year in the past, this will be negative.
     */
    public int getYearsUntil(EsfDate otherDate)
    {
    	return DateUtil.yearsBetween(value, otherDate.value);
    }
    
    public static EsfDate ensureNotNull(EsfDate v) 
    {
    	return v == null ? new EsfDate() : v;
    }

}