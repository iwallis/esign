// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

/**
 * Interface provided by all data value objects.
 *
 * @author Yozons, Inc.
 */
public interface EsfValue extends java.io.Serializable
{
	/**
	 * This will return a string that represents this value's "simple type" rather than the class name, suitable for
	 * storing with the name-value data in XML.
	 * @return a String type
	 */
	public String getType();
	
	/**
	 * This will return a true if the underlying value is null
	 * @return a boolean true if the underlying value is null
	 */
	public boolean isNull();

	/**
	 * This will return a duplicate of the value so that it has the same value, but is a distinct object.
	 * @return a new EsfValue that has the value as this object.
	 */
	public EsfValue duplicate();
	
	/**
	 * This will return the value as an array of its type.
	 * @return the EsfValue array
	 */
	public EsfValue[] toArray();
	
	/**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.
	 * @return the value as pretty String
	 */
    public String toString();
    
	/**
	 * Return the value as a "plain" string, with no adornments, such as for storing in a DB
	 * @return the value as plain String
	 */
    public String toPlainString();
    
    /**
     * Return the value as a String suitable for HTML. 
     * @return the value encoded for HTML
     */
    public String toHtml();

    /**
     * Return the value as a String suitable for XML.  This is also the format used to store values in the database
     * so it will be passed into the static createFromToXml routine in order to recreate the value of implementing classes.
     * @return the value encoded as XML
     */
    public String toXml();
    public long getEstimatedLengthXml();
    
}