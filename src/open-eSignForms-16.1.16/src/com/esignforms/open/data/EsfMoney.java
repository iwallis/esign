// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Currency;

import com.esignforms.open.Application;

/**
 * Defines a Money field based on a Decimal field.  
 *
 * @author Yozons, Inc.
 */
public class EsfMoney
	extends EsfDecimal
{
	private static final long serialVersionUID = -3629567774532783536L;

	public static final String TYPE 	= "money";
	
    static String[] expectedCurrencyCodes = { "USD", "CAD", "AUD", "GBP", "EUR", "JPY" };
    static char[] expectedCurrencySymbols = { '$',   '$',   '$',   '�',   '�',   '�'   };
    
	private Currency				currency = Currency.getInstance(locale);
	
	/**
	 * Create a money with value $0.00.
	 */
	public EsfMoney()
	{
		super("$0.00");
	}
	
	/**
	 * Create a money with the specified string value base 10.  It should only be digits and a leading + or - with optional leading currency spec
	 * @param v the String to set the value to; if null or invalid, our money will be null
	 */
	public EsfMoney(String v)
	{
		try
		{
			Object[] n = normalize(v,currency);
			if ( n != null )
			{
				currency = (Currency)n[NORMALIZED_CURRENCY];
				value = new BigDecimal( (String)n[NORMALIZED_VALUE] );
			}
			else
				value = null;
		}
		catch( NumberFormatException e )
		{
			value = null;
		}
	}
	
	/**
	 * Create a money from another money
	 * @param v the EsfMoney to use
	 */
	public EsfMoney(EsfMoney v)
	{
		super( (EsfDecimal)v );
		if ( v != null )
			currency = v.currency;
	}
	
	/**
	 * Create a money from a decimal and a currency
	 * @param v the EsfDecimal to use
	 * @param c the Currency for the money
	 */
	public EsfMoney(EsfDecimal v, Currency c)
	{
		super( v );
		currency = c;
	}
	
	/**
	 * This will return a new instance using 'fromXml' as the source.  The 'fromXml'
	 * is stored in the name-value pairs using the corresponding toXml() method.
	 * @param fromXml the String that was stored in the XML for name-value pairs.
	 * @return a new value object.
	 * @see #toXml()
	 */
	public static EsfMoney createFromToXml(String fromXml)
	{
		return new EsfMoney( fromXml );
	}
	
	/**
	 * This will return a string that represents this value's "simple type" rather than the class name, suitable for
	 * storing with the name-value data in XML.
	 * @return a String type "money"
	 */
	@Override
	public String getType()
	{
		return TYPE;
	}

	/**
	 * This will return a duplicate of the value so that it has the same value, but is a distinct object.
	 * @return a new EsfDecimal that has the value as this object.
	 */
	@Override
	public EsfMoney duplicate()
	{
		return new EsfMoney( isNull() ? null : value.toPlainString() );
	}
	
	/**
	 * Returns a one element array containing this value.
	 */
	@Override
	public EsfMoney[] toArray()
	{
		EsfMoney[] a = new EsfMoney[1];
		a[0] = this;
		return a;
	}
	
	@Override
	public boolean equals(Object other)
	{
		if ( other instanceof EsfMoney )
			return ((EsfMoney)other).value.equals(value) && currency.equals(((EsfMoney)other).currency);
		else if ( other instanceof EsfDecimal )
			return super.equals(other);
		return false;
	}

	
	/**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.
	 * @return the value as pretty String; returns "" if null
	 */
	@Override
    public String toString()
    {
    	if ( value == null )
    		return "";
    	
    	String fractionFormat;
    	if ( value.scale() > 0 )
    	{
    		StringBuilder buf = new StringBuilder(value.scale()+1);
    		buf.append(".0");
    		for( int i=1; i < value.scale(); ++i)
    		{
    			buf.append( i == 1 ? "0" : "#");
    		}
    		fractionFormat = buf.toString();
    	}
    	else
    		fractionFormat = "";
    		
    	DecimalFormat format = new DecimalFormat(getCurrencyFormat()+"###,###,###,###,##0"+fractionFormat,symbols);
    	format.setCurrency(currency);
    	return format.format(value);
    }
	
    public String toString(int numFractional)
    {
    	if ( value == null )
    		return "";
    	
    	String fractionFormat;
    	if ( numFractional > 0 )
    	{
    		StringBuilder buf = new StringBuilder(numFractional+1);
    		buf.append(".");
    		for( int i=0; i < numFractional; ++i)
    			buf.append("0");
    		fractionFormat = buf.toString();
    	}
    	else
    		fractionFormat = "";
    		
    	DecimalFormat format = new DecimalFormat(getCurrencyFormat()+"###,###,###,###,##0"+fractionFormat,symbols);
    	format.setCurrency(currency);
    	return format.format(value);
    }

    /**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.
	 * @return the value as pretty String; returns "" if null
	 */
    public final String toStringNoCurrency()
    {
    	if ( value == null )
    		return "";
    	
    	String fractionFormat;
    	if ( value.scale() > 0 )
    	{
    		StringBuilder buf = new StringBuilder(value.scale()+1);
    		buf.append(".0");
    		for( int i=1; i < value.scale(); ++i)
    		{
    			buf.append( i == 1 ? "0" : "#");
    		}
    		fractionFormat = buf.toString();
    	}
    	else
    		fractionFormat = "";
    		
    	DecimalFormat format = new DecimalFormat("###,###,###,###,##0"+fractionFormat,symbols);
    	return format.format(value);
    }
	
    public final String toStringNoCurrency(int numFractional)
    {
    	if ( value == null )
    		return "";
    	
    	String fractionFormat;
    	if ( numFractional > 0 )
    	{
    		StringBuilder buf = new StringBuilder(numFractional+1);
    		buf.append(".");
    		for( int i=0; i < numFractional; ++i)
    			buf.append("0");
    		fractionFormat = buf.toString();
    	}
    	else
    		fractionFormat = "";
    		
    	DecimalFormat format = new DecimalFormat("###,###,###,###,##0"+fractionFormat,symbols);
    	return format.format(value);
    }

    /**
	 * Change the value of our money using the specified string
	 * @param v the EsfString with an money string literal inside; if the string value is invalid, our object becomes null
	 */
	public void setValue(EsfString v)
	{
		setValue( v == null || v.isNull() ? null : v.toPlainString() );
	}
	
	/**
	 * Change the value of our money using the specified string
	 * @param v the EsfString with an money string literal inside; if the string value is invalid, our object becomes null
	 */
	public void setValue(String v)
	{
		try
		{
			Object[] n = normalize(v,currency);
			if ( n != null )
			{
				currency = (Currency)n[NORMALIZED_CURRENCY];
				value = new BigDecimal( (String)n[NORMALIZED_VALUE] );
			}
			else
				value = null;
		}
		catch( NumberFormatException e )
		{
			value = null;
		}
	}
	
	public String getCurrencyCode()
	{
		return currency.getCurrencyCode();
	}
	public Currency getCurrency()
	{
		return currency;
	}
	public void setCurrency(Currency v)
	{
		currency = v;
	}
	private String getCurrencyFormat()
	{
		for( int i=0; i < expectedCurrencyCodes.length; ++i )
		{
			if ( currency.getCurrencyCode().equals(expectedCurrencyCodes[i]) )
				return "'"+expectedCurrencySymbols[i]+"'";
		}
		return "�� "; // use international format for those we don't know
	}
	

	
    /**
     * Normalize the String decimal. If it returns null, it implies the decimal is not formatted in a way we can normalize it.
     * @param v
     * @return the an array in which the first element is the normalized string format and the second element is the currency type.
     */
    public static int NORMALIZED_VALUE = 0;
    public static int NORMALIZED_CURRENCY = 1;

    public static Object[] normalize(String v, Currency currency) 
    {
    	if ( v == null )
    		return null;

    	Object[] ret = new Object[2];
    	ret[NORMALIZED_VALUE] = v;
    	ret[NORMALIZED_CURRENCY] = currency;

    	if ( EsfString.isBlank(v) )
    	{
    		ret[NORMALIZED_VALUE] = "0";
    		return ret;
    	}
    	
    	v = v.trim();
    	
    	boolean hasCurrencyCode = false;
    	for( String currencyCode : expectedCurrencyCodes )
    	{
        	int currencyIndex = v.indexOf(currencyCode);
        	if ( currencyIndex >= 0 )
        	{
        		ret[NORMALIZED_CURRENCY] = Currency.getInstance(currencyCode);
        		v = v.substring(0,currencyIndex) + v.substring(currencyIndex+3);
        		hasCurrencyCode = true;
        		break;
        	}
    	}
    	
    	boolean hasCurrencySymbol = false;
    	boolean hasPositiveNegativeSign = false;
    	boolean hasDigit = false;
    	boolean hasDecimal = false;
    	
        StringBuilder normal = new StringBuilder(v.length());
        for( int i=0; i < v.length(); ++i )
        {
            char c = v.charAt(i);
            
            boolean charProcessed = false;
            for( int j = 0; j < expectedCurrencySymbols.length; ++j )
            {
            	if ( c == expectedCurrencySymbols[j] )
            	{
                	if ( hasDigit || hasDecimal )// can only have one and must precede any digits and decimal point
                		return null;
                	if ( hasCurrencySymbol ) // only should have one currency symbol
                		return null;
                	// swallow it
                	hasCurrencySymbol = true;
                	if ( ! hasCurrencyCode )
                		ret[NORMALIZED_CURRENCY] = Currency.getInstance(expectedCurrencyCodes[j]);
                	charProcessed = true;
                	break;
            	}
            }
            if ( charProcessed )
            	continue;
            
            if ( c == '+' )
            {
            	if ( hasPositiveNegativeSign || hasDigit || hasDecimal ) // can only have one and must precede any digits and decimal point
            		return null;
            	hasPositiveNegativeSign = true;
            	// swallow a leading positive sign
            }
            else if ( c == '-' )
            {
            	if ( hasPositiveNegativeSign || hasDigit || hasDecimal )// can only have one and must precede any digits and decimal point
            		return null;
            	hasPositiveNegativeSign = true;
            	normal.append(c);
            }
            else if ( c == ',' )
            {
            	if ( ! hasDigit ) // allow a comma, but only if we've seen at least one digit
            		return null;
            	if ( hasDecimal ) // if we have a decimal point, no more commas
            		return null;
            	// swallow it
            }
            else if ( c == ' ' )
            {
            	if ( hasDigit || hasDecimal )
            		return null;
            	// swallow it
            }
            else if ( c == '.' )
            {
            	if ( hasDecimal ) // can only have one decimal point
            		return null;
            	hasDecimal = true;
            	normal.append(c);
            }
            else if ( Character.isDigit(c) )
            {
            	hasDigit = true;
            	normal.append(c);
            }
            else
            	return null;
        }

        ret[NORMALIZED_VALUE] = normal.toString();
        return ret;
    }
    
    public static EsfMoney ensureNotNull(EsfMoney v) 
    {
    	return v == null ? new EsfMoney() : v;
    }

    public static void main(String[] args)
    {
    	/*Application app = */ Application.getInstallationInstance("/");
    	
    	EsfMoney zero = new EsfMoney("0");
    	EsfMoney zeroPointZero = new EsfMoney("0.00");
    	
    	if ( zero.equals(zeroPointZero) )
    		System.out.println("0 equals 0.00");
    	else
    		System.out.println("0 NOT equals 0.00");
    	
    	EsfMoney v1 = new EsfMoney("�123.45");
    	System.out.println("v1: " + v1.toString() + " ; " + v1.toPlainString());
    	
    	EsfMoney v2 = new EsfMoney("�2,123.456");
    	System.out.println("v2: " + v2.toString() + " ; " + v2.toPlainString());
    	
    	EsfMoney v3 = new EsfMoney("$33,123.4567");
    	System.out.println("v3: " + v3.toString() + " ; " + v3.toPlainString());
    	
    	EsfMoney v4 = new EsfMoney("GBP 444,123.456789");
    	System.out.println("v4: " + v4.toString() + " ; " + v4.toPlainString());
    	
    	EsfMoney v5 = new EsfMoney("AUD 5,555,123.");
    	System.out.println("v5: " + v5.toString() + " ; " + v5.toPlainString());
    	
    	EsfMoney v6 = new EsfMoney("USD.1234");
    	System.out.println("v6: " + v6.toString() + " ; " + v6.toPlainString());
    	    	
    	System.exit(0);
    }
}