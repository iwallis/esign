// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import com.esignforms.open.config.Literals;

/**
 * Defines a Boolean field.
 *
 * @author Yozons, Inc.
 */
public class EsfBoolean
	implements EsfValue
{
	private static final long serialVersionUID = -1238158715193442276L;

	public static final String TYPE = "boolean";

	private boolean value;
	
	/**
	 * Create a false boolean.
	 */
	public EsfBoolean()
	{
		value = false;
	}
	
	/**
	 * Create a boolean with the specified value.
	 * @param v the String to set the value to; may be null
	 */
	public EsfBoolean(String v)
	{
		value = toBoolean(v);
	}
	
	/**
	 * Create a boolean from another boolean
	 * @param v the EsfBoolean to use
	 */
	public EsfBoolean(boolean v)
	{
		value = v;
	}
	
	/**
	 * Create a boolean from a Boolean
	 * @param v the Boolean to use
	 */
	public EsfBoolean(Boolean v)
	{
		value = v == null ? false : v.booleanValue();
	}
	/**
	 * Create a boolean from another boolean
	 * @param v the EsfBoolean to use
	 */
	public EsfBoolean(EsfBoolean v)
	{
		value = v.value;
	}
	
	/**
	 * This will return a new instance using 'fromXml' as the source.  The 'fromXml'
	 * is stored in the name-value pairs using the corresponding toXml() method.
	 * @param fromXml the String that was stored in the XML for name-value pairs.
	 * @return a new value object.
	 * @see #toXml()
	 */
	public static EsfBoolean createFromToXml(String fromXml)
	{
		return new EsfBoolean( fromXml );
	}
	
	/**
	 * This will return a string that represents this value's "simple type" rather than the class name, suitable for
	 * storing with the name-value data in XML.
	 * @return a String literal "boolean"
	 */
	@Override
	public String getType()
	{
		return TYPE;
	}

	/**
	 * This will return a true if the underlying value is null
	 * @return always returns false
	 */
	@Override
	public final boolean isNull()
	{
		return false;
	}

	/**
	 * This will return a duplicate of the value so that it has the same value, but is a distinct object.
	 * @return a new EsfBoolean that has the value as this object.
	 */
	@Override
	public EsfBoolean duplicate()
	{
		return new EsfBoolean( value );
	}
	
	/**
	 * Returns a one element array containing this value.
	 */
	@Override
	public EsfBoolean[] toArray()
	{
		EsfBoolean[] a = new EsfBoolean[1];
		a[0] = duplicate();
		return a;
	}
	
	@Override
	public boolean equals(Object other)
	{
		if ( other instanceof EsfBoolean )
			return value == ((EsfBoolean)other).value;
		return false;
	}
	@Override
	public int hashCode()
	{
		return value ? 1 : 0;
	}

	
	/**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.
	 * @return the value as pretty String
	 */
	@Override
    public final String toString()
    {
    	return toPlainString();
    }
    
	/**
	 * Return the value as a "plain" string, with no adornments, such as for storing in a DB
	 * @return the value as plain String; 
	 */
	@Override
    public final String toPlainString()
    {
    	return value ? "true" : "false";
    }
    
    /**
     * Return the value as a String suitable for HTML 
     * @return the value encoded as HTML
     */
	@Override
    public String toHtml()
    {
    	return toPlainString();
    }

    /**
     * Return the value as a String suitable for XML 
     * @return the value encoded as XML
     */
	@Override
    public String toXml()
    {
    	return toPlainString();
    }
	@Override
    public long getEstimatedLengthXml()
    {
    	return 5;
    }
    

	
	/**
	 * Change the value of our boolean using the specified string
	 * @param v the EsfString with a boolean string literal inside
	 */
	public void setValue(EsfString v)
	{
		value = toBoolean(v.toPlainString());
	}
	
	/**
	 * Change the value of our boolean to the specified boolean
	 * @param v the boolean to use
	 */
	public void setValue(EsfBoolean v)
	{
		value = v.value;
	}
	
	public void setValue(boolean v)
	{
		value = v;
	}
	
	/**
	 * This will return a true if the underlying value is blank (null, empty or only whitespace)
	 * @return a boolean true if the underlying value is blank
	 */
	public final boolean isTrue()
	{
		return value;
	}
	public final boolean isFalse()
	{
		return ! value;
	}
	
	public final String toYesNo()
	{
		return value ? "Yes" : "No";
	}

	public final String toYN()
	{
		return value ? Literals.Y : Literals.N;
	}

    /**
     * Returns true if the string provided is one of: true, y, yes, t or 1
     * @param b the string to check (not case sensitive)
     * @return true if the string provided is one of: true, y, yes, t or 1
     */
    public static final boolean toBoolean( String b ) 
    {
        return ( "true".equalsIgnoreCase(b) || "y".equalsIgnoreCase(b) || "yes".equalsIgnoreCase(b) || "t".equalsIgnoreCase(b) || "1".equals(b) );
    }
    public static final boolean toBoolean( char b ) 
    {
        return ( 't' == b || 'T' == b || 'y' == b || 'Y' == b || '1' == b );
    }

    public static EsfBoolean ensureNotNull(EsfBoolean v) 
    {
    	return v == null ? new EsfBoolean() : v;
    }

}