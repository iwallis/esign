// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.esignforms.open.Application;
import com.esignforms.open.data.NameValue;

/**
 * This class holds a set of NameValue objects as a HashMap.  Thus it's not ordered at all. 
 * The names of the name-values will be unique and are based on NameValue path names.
 * 
 * @author Yozons, Inc.
 */
public class NameValueMap
    implements java.io.Serializable
{
	private static final long serialVersionUID = -3531231616534162346L;

	protected HashMap<String,NameValue> map;
    
	/**
	 * Create a new empty name value map.
	 */
    public NameValueMap()
    {
    	map = new HashMap<String,NameValue>();
    }
    
	/**
	 * Create a new empty name value map with the specified initial capacity.
	 * @param initialCapacity the initial capacity of the name value map.
	 */
    public NameValueMap(int initialCapacity)
    {
    	if ( initialCapacity < 16 )
    		initialCapacity = 16;
    	map = new HashMap<String,NameValue>(initialCapacity);
    }
    
    /* 
     * Creates a new name value map with the same contents as the 'other' map.
     * @param other the NameValueMap to copy from
     */
    public NameValueMap(NameValueMap other)
    {
    	this( other.getSize() + 16 );
    	synchronized(other)
    	{
            for( Map.Entry<String,NameValue> nve : other.map.entrySet() )
                addUpdate(nve.getValue());
    	}
    }
    
    /**
     * This routines clears (empties) all name-values in the set.
     */
    public synchronized void clear()
    {
    	map.clear();
    }
    
    /**
     * Returns a sorted array of NameValue objects.
     * @return the NameValue array, sorted by pathnames.
     */
    public NameValue[] getSorted()
    {
        NameValue[] a = toArray();
        Arrays.sort(a);
        return a;
    }
    
    /**
     * This will duplicate a new NameValueMap like this map for all elements marked as allowing it to be cloned.
     * The new elements are fully duplicate name-value objects so a change to the original will not change 
     * the other.
     * @return the NameValueMap that is a copy of the current map's fields that allow cloning
     */
    public synchronized NameValueMap duplicate()
    {
    	NameValueMap dupMap = new NameValueMap( map.size() + 16 );
        for( Map.Entry<String,NameValue> nve : map.entrySet() )
        {
        	NameValue nv = nve.getValue();
        	if ( nv.canClone() )
        	{
        		NameValue dupNV = nv.duplicate();
        		dupMap.addUpdate(dupNV);
        	}
        }
        return dupMap;
    }
    
    public synchronized void addUpdateClonableFields(NameValueMap otherMap)
    {
        for( Map.Entry<String,NameValue> nve : otherMap.map.entrySet() )
        {
        	NameValue nv = nve.getValue();
        	if ( nv.canClone() )
        	{
        		NameValue dupNV = nv.duplicate();
        		addUpdate(dupNV);
        	}
        }
    }
    

    /**
     * Adds or updates the NameValue. If the same 'path name' (case insensitive) already exists, it replaces that one,
     * otherwise it just adds it.
     */
    public synchronized void addUpdate(NameValue nv)
    {
    	if ( nv.isValid() )
    		map.put(nv.getPathName().toLowerCase(),nv);
    }
    
    /**
     * Adds or updates the the EsfName and related EsfValue.  This is just a convenience function.
     * @param name the EsfName to addupdate
     * @param value the EsfValue to addupdate
     * @see NameValue(EsfName,EsfValue)
     * @see NameValueMap#addUpdate(NameValue)
     */
    public synchronized void addUpdate(EsfName name, EsfValue value)
    {
    	NameValue nv = new NameValue(name,value);
    	addUpdate(nv);
    }
    
    /**
     * Only adds the NameValue if the 'path name' (case insensitive) is not already added.
     * Returns true only if added.
     */
    public synchronized boolean addUnique(NameValue nv)
    {
    	if ( nv.isValid() )
    		return false;
    	if ( map.containsKey(nv.getPathName().toLowerCase()) )
    		return false;
    	addUpdate(nv);
        return true;
    }
    
    /**
     * Only adds the EsfName and EsfValue if the 'path name' is not already added.  This is just a convenience function.
     * Returns true only if added.
     * @see NameValue(EsfName,EsfValue)
     * @see NameValueMap#addUnique(NameValue)
     */
    public synchronized boolean addUnique(EsfName name, EsfValue value)
    {
    	NameValue nv = new NameValue(name,value);
    	return addUnique(nv);
    }
    public synchronized boolean addUnique(EsfName[] names, EsfValue value)
    {
    	NameValue nv = new NameValue(names,value);
    	return addUnique(nv);
    }
    
    /**
     * Only adds the EsfName and EsfValue if the 'path name' is not already added.  This is just a convenience function.
     * Returns true only if added.
     * @see NameValue(EsfName,EsfValue)
     * @see NameValueMap#addUnique(NameValue)
     */
    public synchronized boolean addUnique(String pathName, EsfValue value)
    {
    	NameValue nv = new NameValue(new EsfPathName(pathName),value);
    	return addUnique(nv);
    }
    
    /**
     * Removes the specified element from the map using the name value's path name (doesn't care if the value parts match).
     * @param nv the NameValue to remove using it's path name
     */
    public synchronized void remove(NameValue nv)
    {
    	map.remove(nv.getPathName().toLowerCase());
    }
    
    /**
     * Removes the specified element from the map matching the EsfName
     * @param nv the NameValue to remove using it's path name
     */
    public synchronized void remove(EsfName name)
    {
    	EsfPathName pathName = new EsfPathName(name);
    	map.remove(pathName.toLowerCase());
    }
    public synchronized void remove(EsfName[] names)
    {
    	EsfPathName pathName = new EsfPathName(names);
    	map.remove(pathName.toLowerCase());
    }
    public synchronized void remove(EsfPathName pathName)
    {
    	map.remove(pathName.toLowerCase());
    }
    
    public synchronized void remove(String pathName)
    {
    	map.remove(pathName.toLowerCase());
    }
    
    public synchronized int removeAllWithPathPrefix(String pathPrefix)
    {
    	int numRemoved = 0;
    	
    	Set<String> pathNameSet = map.keySet();
    	pathPrefix = pathPrefix.toLowerCase();
    	for( String pathName : pathNameSet )
    	{
    		if ( pathName.startsWith(pathPrefix) )
    		{
    			map.remove(pathName);
    			++numRemoved;
    		}
    	}
    	
    	return numRemoved;
    }
    public synchronized int removeAllWithPathPrefix(EsfName[] prefixNames)
    {
    	EsfPathName pathName = new EsfPathName(prefixNames);
    	return removeAllWithPathPrefix(pathName.toPlainString());
    }
    public synchronized int removeAllWithPathPrefix(EsfPathName prefixPathName)
    {
    	return removeAllWithPathPrefix(prefixPathName.toPlainString());
    }
    
    /**
     * Gets the number of NameValue objects in the map.
     * @return the int number of NameValue objects in the map.
     */
    public int getSize()
    {
        return map.size();
    }
    
    /**
     * Gets the NameValue objects in the map in an unordered array.
     * @return the NameValue array
     */
    public synchronized NameValue[] toArray()
    {
    	java.util.Collection<NameValue> values = map.values();
        NameValue[] nvArray = new NameValue[values.size()];
        values.toArray(nvArray);
        return nvArray;
    }
    
    /**
     * Gets the NameValue objects in the map in an unordered array and
     * clears the entries in the map.  This is just a convenience function.
     * @return the NameValue array
     * @see #toArray()
     * @see #clear()
     */
    public synchronized NameValue[] getAndClear()
    {
        NameValue[] nv = toArray();
        clear();
        return nv;
    }
    
    /**
     * Returns the value found that matches the specified EsfName
     * @return the NameValue value that is associated with specified 'name'; null if not found
     */
	public synchronized NameValue getNameValue(EsfName name)
	{
		EsfPathName pathName = new EsfPathName(name);
		return map.get(pathName.toLowerCase());
	}
	public synchronized NameValue getNameValue(EsfName[] names)
	{
		EsfPathName pathName = new EsfPathName(names);
		return map.get(pathName.toLowerCase());
	}
	public synchronized NameValue getNameValue(EsfPathName pathName)
	{
		return map.get(pathName.toLowerCase());
	}
	public synchronized NameValue getNameValue(String pathName)
	{
		return map.get(pathName.toLowerCase());
	}
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder(getXmlEstimatedSize());
        return appendXml(buf).toString();
    }
    
    public String toXmlSorted()
    {
        StringBuilder buf = new StringBuilder(getXmlEstimatedSize());
        return appendXmlSorted(buf).toString();
    }
    
    /**
     * Append the name values into the XML buffer.  
     * The <nameValues> element attribute 'count' tells us now many <nameValue> elements will be found with in it.
     * The <nameValue> element has an optional attribute 'clone' that if true means we can clone it, while false means we will not. 
     * It the 'clone' attribute is missing, it is assumed to be true.
     * The <name> element is always treated as an array of at least size 1.  Multiple names imply a path name.
     * The <nameValue> element has an optional attribute 'array' that if set means we have an array of <value> elements under it.  If
     * the 'array' attribute is missing, it means it is a simple value (not an array).
     * The <value> element has an optional element 'type' that specifies the value's simple type.  If the 'type' attribute is missing,
     * it is assumed to be a "string".  The value also has an optional 'null' attribute, that if true, means the value is null.  If
     * the 'null' attribute is missing, it means it is not null.  If the <value> element itself is missing, we assume a null string value.
     * There is an optional <comment> element that if present means a comment has been specified with this name-value.
     * 
     * An example of the simple XML format shown below.
     * <code>
     *   <nameValues count="5">
     *     <nameValue>
     *       <name>SimpleString</name>  <!-- path name is /simplestring -->
     *       <value>this is a string</value>
     *       <comment>This is a comment about this simple string.</comment>
     *     </nameValue>
     *     <nameValue clone="false">
     *       <name>SimpleNullString</name>
     *       <value null="true"/>
     *     </nameValue>
     *     <nameValue>
     *       <name>Simple</name> <!-- path name is /simple/date -->
     *       <name>Date</name>
     *       <value type="date">2009-10-13</value>
     *     </nameValue>
     *     <nameValue type="datetime">
     *       <name>SimpleDateTime</name>
     *       <value>2009-10-13T23:50:00Z</value>
     *     </nameValue>
     *     <nameValue array="3">
     *       <name>ArrayOfStrings</name>
     *       <value>string 1</value>
     *       <value null="true"/>
     *       <value>string 3</value>
     *     </nameValue>
     *     <nameValue array="3" type="date">
     *       <name>ArrayOfDates</name>
     *       <value>2009-10-01</value>
     *       <value null="true"/>
     *       <value>2009-10-02</value>
     *     </nameValue>
     *   </nameValues>
     * </code>
     * @param buf the StringBuilder buffer to use
     * @return the StringBuilder buf so it can be chained
     */
    public StringBuilder appendXml(StringBuilder buf)
    {
    	return appendXml(buf,null);
    }    
    public StringBuilder appendXml(StringBuilder buf, List<EsfName> maskNames)
    {
        if ( buf == null )
            buf = new StringBuilder(getXmlEstimatedSize());
        else
            buf.ensureCapacity(buf.length()+getXmlEstimatedSize());
        
        int num = getSize();
        buf.append("<nameValues count=\"" + num + "\">\n");

        for( NameValue nv : map.values() )
        	appendNameValue(nv, buf, maskNames);

        buf.append("</nameValues>\n");
        return buf;
    }
    
    public StringBuilder appendXmlSorted(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder(getXmlEstimatedSize());
        else
            buf.ensureCapacity(buf.length()+getXmlEstimatedSize());
        
        int num = getSize();
        buf.append("<nameValues count=\"" + num + "\">\n");

        for( NameValue nv : getSorted() )
        	appendNameValue(nv, buf);
        
        buf.append("</nameValues>\n");
        return buf;
    }
    
    private StringBuilder appendNameValue(NameValue nv, StringBuilder buf)
    {
    	return appendNameValue(nv,buf,null);
    }
    
    private StringBuilder appendNameValue(NameValue nv, StringBuilder buf, List<EsfName> maskNames)
    {
    	buf.append(" <nameValue");
    	if ( ! nv.canClone() ) // by default, we assume can clone
    		buf.append(" clone=\"false\"");
    	
        if ( nv.isArray() ) 
        {
            EsfValue[] values = nv.getValues(); // there must always be values, even if it's zero length array
            buf.append(" array=\"").append(values.length).append("\"");
            
            String typeAttrValue;
            
            if ( values.length == 0 ) 
            {
            	try
            	{
            		// For a zero-length array, we suffer having to get the array's component's type (the actual EsfValue subclass)
            		// and its static TYPE string to determine the type that goes with the class since we don't have an instance.
            		Field typeField = values.getClass().getComponentType().getField("TYPE");
            		typeAttrValue = (String)typeField.get(values.getClass().getComponentType());
            	}
            	catch( Exception e )
            	{
            		typeAttrValue = EsfString.TYPE;  // Got me what this is!
            	}
            }
            else
            	typeAttrValue = values[0].getType(); // This is easy since we just get the type of the first object
            
            if ( ! typeAttrValue.equals(EsfString.TYPE) ) // by default, we assume string type
            	buf.append(" type=\"").append(typeAttrValue).append("\"");
            
            buf.append(">\n");
            
    		EsfName[] names = nv.getNames();
            for( EsfName eachName : names )
            	buf.append("  <name>").append(eachName.toXml()).append("</name>\n");
            
            for( EsfValue v : values )
            {
                if ( v == null || v.isNull() )
                    buf.append("  <value null=\"true\" />\n");
                else
                {
                	String valueXml = v.toXml();
                	if ( maskNames != null && names.length == 1 && maskNames.contains(names[0]) )
                		valueXml = Application.getInstance().mask(valueXml);
                    buf.append("  <value>").append(valueXml).append("</value>\n");
                }
            }
        }
        else
        {
            EsfValue value = nv.getValue();

            String typeAttrValue = value.getType();
            if ( ! typeAttrValue.equals(EsfString.TYPE) )
            	buf.append(" type=\"").append(typeAttrValue).append("\"");
            
            buf.append(">\n");

            EsfName[] names = nv.getNames();
            for( EsfName eachName : names )
            	buf.append("  <name>").append(eachName.toXml()).append("</name>\n");
            
            if ( value.isNull() )
                buf.append("  <value null=\"true\"/>\n");
            else
            {
            	String valueXml = nv.getValue().toXml();
            	if ( maskNames != null && names.length == 1 && maskNames.contains(names[0]) )
            		valueXml = Application.getInstance().mask(valueXml);
                buf.append("  <value>").append(valueXml).append("</value>\n");
            }
        }
        if ( nv.hasComment() )
        {
        	buf.append("  <comment>").append(nv.getComment().toXml()).append("</comment>\n");
        }
        buf.append(" </nameValue>\n");
    	return buf;
    }
    
    public int getXmlEstimatedSize()
    {
        int size = 50; // basic XML overhead for <nameValues> element.
        
        for( Map.Entry<String,NameValue> nve : map.entrySet() )
        {
        	NameValue nv = nve.getValue();

        	for( EsfName eachName : nv.getNames() )
            	size += eachName.getEstimatedLengthXml();

        	if ( nv.isArray() )
        	{
        		for( EsfValue v : nv.getValues() ) 
        		{
        			if ( v != null )
        				size += v.getEstimatedLengthXml();
        		}
        	}
        	else
        	{
        		if ( nv.getValue() != null )
        			size += nv.getValue().getEstimatedLengthXml();
        	}
        		
        	if ( nv.hasComment() )
        		size += nv.getComment().getEstimatedLengthXml();
        	size += 80; // basic XML overhead for <nameValue> element
        }
        return size;
    }
}