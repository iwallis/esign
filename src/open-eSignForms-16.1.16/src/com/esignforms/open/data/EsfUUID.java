// Copyright (C) 2009-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import java.util.UUID;

import com.esignforms.open.util.RandomKey;

/**
 * Holds a standards-compliant 16-byte (128-bit) random UUID.  It can never be null unless you 
 * are creating your own from a poorly constructed string or UUID.
 *
 * @author Yozons, Inc.
 */
public class EsfUUID
	implements EsfValue, Comparable<EsfUUID>
{
	private static final long serialVersionUID = 2713335752084571832L;

	public static final String TYPE = "uuid";
	
	private UUID value;
	
	public EsfUUID()
	{
		do
		{
			value = UUID.randomUUID();
		}
		while( RandomKey.ContainsUndesirable(value.toString()) );
	}
	
	public EsfUUID(String v)
	{
		try
		{
			value = EsfString.isBlank(v) || "null".equalsIgnoreCase(v) ? null : UUID.fromString(v);
		}
		catch( IllegalArgumentException e )
		{
			value = null;
		}
	}
	
	public EsfUUID(UUID v)
	{
		value = v;
	}
	
	public EsfUUID(EsfUUID v)
	{
		value = v.value;
	}
	
	public UUID getUUID()
	{
		return value;
	}
	
	/**
	 * This will return a string that represents this value's "simple type" rather than the class name, suitable for
	 * storing with the name-value data in XML.
	 * @return a String value of "uuid"
	 */
	@Override
	public String getType()
	{
		return TYPE;
	}

	/**
	 * This will return a true if the underlying value is null
	 * @return a boolean true if the underlying value is null
	 */
	@Override
	public boolean isNull()
	{
		return value == null;
	}

	/**
	 * This will return a duplicate of the value so that it has the same value, but is a distinct object.
	 * @return a new EsfValue that has the value as this object.
	 */
	@Override
	public EsfUUID duplicate()
	{
		return new EsfUUID( value );
	}
	
	/**
	 * Returns a one element array containing this value.
	 */
	@Override
	public EsfUUID[] toArray()
	{
		EsfUUID[] a = new EsfUUID[1];
		a[0] = this;
		return a;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if ( o instanceof EsfUUID )
		{
			EsfUUID other = (EsfUUID)o;
			if ( value == null ) 
				return other.value == null;
			return value.equals(other.value);
		}
		return false;
	}
	@Override
	public int hashCode()
	{
		return value == null ? 0 : value.hashCode();
	}

	@Override
	public int compareTo(EsfUUID other)
	{
		if ( value == null ) 
			return other == null || other.value == null ? 0 : -1;
		if ( other == null )
			return value == null ? 0 : 1;
		return value.compareTo(other.value);
	}
	
	/**
	 * We want to use our IDs often in pathnames, so we need this method to normalize our UUID into
	 * a string that is legal for path name components.  The length will be fine, but it may need
	 * to start with a letter (we always use 'X' in this case), and hyphens (-) must be turned into underscores (_).
	 * @return
	 */
	public String toNormalizedEsfNameString()
	{
		String uuid = toPlainString();
		StringBuilder esfNameUUID = new StringBuilder(uuid.length()+1);
		if ( ! Character.isLetter(uuid.charAt(0)) )
			esfNameUUID.append('X');
		
		for( char c : uuid.toCharArray() )
		{
			if ( c == '-' )
				esfNameUUID.append('_');
			else
				esfNameUUID.append(c);
		}
		return esfNameUUID.toString();
	}
	public EsfName toEsfName()
	{
		return new EsfName(toNormalizedEsfNameString());
	}
	
	public static EsfUUID createFromNormalizedEsfName(EsfName v)
	{
		return createFromNormalizedEsfNameString( v == null || v.isNull() ? null : v.toPlainString());
	}
	public static EsfUUID createFromNormalizedEsfNameString(String v)
	{
		v = v == null ? "" : v;
		v = v.replace('_', '-');
		EsfUUID id = new EsfUUID(v);
		if ( ! id.isNull() )
			return id;
		
		if ( v.startsWith("X") )
			v = v.substring(1);
		return new EsfUUID(v);
	}
	
	/**
	 * This will return a new instance using 'fromXml' as the source.  The 'fromXml'
	 * is stored in the name-value pairs using the corresponding toXml() method.
	 * @param fromXml the String that was stored in the XML for name-value pairs.
	 * @return a new value object.
	 * @see #toXml()
	 */
	public static EsfUUID createFromToXml(String fromXml)
	{
		return new EsfUUID( fromXml );
	}
	
	/**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.
	 * @return the value as pretty String
	 */
	@Override
    public final String toString()
    {
    	return toPlainString();
    }
    
	/**
	 * Return the value as a "plain" string, with no adornments, such as for storing in a DB
	 * @return the value as plain String; 
	 */
	@Override
    public final String toPlainString()
    {
    	return value == null ? null : value.toString();
    }
    
    /**
     * Return the value as a String suitable for HTML 
     * @return the value encoded as HTML
     */
    public final String toHtml()
    {
    	return toPlainString(); // UUID is already HTML safe
    }
    
    /**
     * Return the value as a String suitable for XML 
     * @return the value encoded as XML
     */
    public final String toXml()
    {
    	return toPlainString(); // UUID is already XML safe
    }
    public static final long DISPLAY_LENGTH = 36;
    public final long getEstimatedLengthXml()
    {
    	return DISPLAY_LENGTH; // i.e. cb07fa8e-b538-4d19-b099-35da467b752e
    }
    
    public static EsfUUID ensureNotNull(EsfUUID v) 
    {
    	return v == null ? new EsfUUID() : v;
    }
}