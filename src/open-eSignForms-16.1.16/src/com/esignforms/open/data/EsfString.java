// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;

import com.esignforms.open.Application;
import com.esignforms.open.util.HtmlUtil;
import com.esignforms.open.util.XmlUtil;
/**
 * Defines a String field.
 *
 * @author Yozons, Inc.
 */
public class EsfString
	implements EsfValue, Comparable<EsfString>
{
	private static final long serialVersionUID = 9218407850157579232L;

	public static final String CHARSET_UTF_8	= "UTF-8";
    public static final String TYPE 			= "string";

	protected String value;
	
	/**
	 * Create a null string.
	 */
	public EsfString()
	{
		value = null;
	}
	
	/**
	 * Create a string from another string
	 * @param v the EsfString to use
	 */
	public EsfString(EsfString v)
	{
		value = v.value;
	}
	
	/**
	 * Create a string with the specified value.
	 * @param v the String to set the value to; may be null
	 */
	public EsfString(String v)
	{
		value = v;
	}
	
	/**
	 * Create a string from a UTF-8 byte array
	 * @param v the UTF-8 byte array
	 */
	public EsfString(byte[] v)
	{
		value = bytesToString(v);
	}
	
	/**
	 * This will return a string that represents this value's "simple type" rather than the class name, suitable for
	 * storing with the name-value data in XML.
	 * @return the String "string"
	 */
	@Override
	public String getType()
	{
		return TYPE;
	}

	/**
	 * This will return a true if the underlying value is null
	 * @return a boolean true if the underlying value is null
	 */
	@Override
	public final boolean isNull()
	{
		return value == null;
	}


	
	/**
	 * Change the value of our string to the specified string
	 * @param v the EsfString to use
	 */
	public void setValue(EsfString v)
	{
		value = v.value;
	}
	
	/**
	 * Change the value of our string to the specified string
	 * @param v the string to use
	 */
	public void setValue(String v)
	{
		value = v;
	}
	
	/**
	 * Change the value of our string to the specified UTF-8 byte array
	 * @param v the UTF-8 byte array
	 */
	public void setValue(byte[] v)
	{
		value = bytesToString(v);
	}
	
	/**
	 * This will return a true if the underlying value is blank (null, empty or only whitespace)
	 * @return a boolean true if the underlying value is blank
	 */
	public final boolean isBlank()
	{
		return isBlank(value);
	}

	/**
	 * This will return a true if the underlying value is not blank (non-null, non-empty has has characters other than whitespace)
	 * @return a boolean true if the underlying value is not blank
	 */
	public final boolean isNonBlank()
	{
		return isNonBlank(value);
	}
	
	/**
	 * Gets only the numeric characters from the current string
	 * @return the new EsfString that's only the numeric digits from this string
	 */
	public final EsfString getOnlyNumeric()
	{
		return new EsfString(getOnlyNumeric(value));
	}
	public final String getOnlyNumericString()
	{
		return getOnlyNumeric(value);
	}

	/**
	 * This will return a duplicate of the value so that it has the same value, but is a distinct object.
	 * @return a new EsfValue that has the value as this object.
	 */
	public EsfString duplicate()
	{
		return new EsfString( value );
	}
	
	/**
	 * Returns a one element array containing this value.
	 */
	@Override
	public EsfString[] toArray()
	{
		EsfString[] a = new EsfString[1];
		a[0] = this;
		return a;
	}
	
	public boolean equals(String other)
	{
		if ( value == null )
			return other == null;
		return value.equalsIgnoreCase(((String)other));
	}
	
	@Override
	public boolean equals(Object other)
	{
		if ( value == null )
			return other == null;
		if ( other instanceof EsfString )
			return value.equals(((EsfString)other).value);
		return false;
	}
	@Override
	public int hashCode()
	{
		return value == null ? 0 : value.hashCode();
	}

	
	@Override
	public int compareTo(EsfString other)
	{
		return value.compareTo(other.value);
	}
	
	/**
	 * This will return a new instance using 'fromXml' as the source.  The 'fromXml'
	 * is stored in the name-value pairs using the corresponding toXml() method.
	 * @param fromXml the String that was stored in the XML for name-value pairs.
	 * @return a new value object.
	 * @see #toXml()
	 */
	public static EsfString createFromToXml(String fromXml)
	{
		return new EsfString( fromXml );
	}
	
	/**
	 * Return the string as a UTF-8 byte array.
	 * @return
	 */
	public byte[] toBytes()
	{
		return value == null ? null : stringToBytes(value);
	}
	
	/**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.  Returns "" if the value is null.
	 * @return the value as pretty String
	 */
	@Override
    public String toString()
    {
    	return value == null ? "" : value;
    }

	/**
	 * Return the value as a "plain" string, with no adornments, such as for storing in a DB
	 * @return the value as plain String; 
	 */
	@Override
    public final String toPlainString()
    {
    	return value;
    }
	
	public final int getLength()
	{
		return value == null ? 0 : value.length();
	}
	
	public EsfString trim()
	{
		return ( value == null ) ? new EsfString() : new EsfString(value.trim());
	}
	
	public boolean contains(EsfString otherString)
	{
		if ( value == null || otherString == null || otherString.isNull() )
			return false;
		return value.contains(otherString.value);
	}


    
    /**
     * Return the value as a String suitable for XML 
     * @return the value encoded as XML
     */
    public String toXml()
    {
    	return XmlUtil.toEscapedXml(value);
    }
    public long getEstimatedLengthXml()
    {
    	return value == null ? 0 : value.length();
    }
    
    /**
     * Appends the value as a String suitable for XML into the specified buffer
     * @param buf the StringBuilder object to use
     * @return the StringBuilder buffer used
     */
    public StringBuilder appendXml(StringBuilder buf)
    {
    	return XmlUtil.appendEscapedXml(value,buf);
    }
    
    /**
     * Return the value as a String suitable for HTML 
     * @return the value encoded as HTML
     */
    public String toHtml()
    {
    	return HtmlUtil.toEscapedHtml(value);
    }
    
    /**
     * Return the value as a String suitable for pretty display of HTML, such as handling multiple spaces as &nbsp; and converting
     * embedded CR+LF as <br/> tags.
     * @return the value encoded as pretty HTML
     */
    public String toDisplayHtml()
    {
    	return HtmlUtil.toDisplayHtml(value);
    }
    
    public static final byte[] stringToBytes(String v)
    {
        if ( v == null )
            return null;
        try
        {
            return v.getBytes(CHARSET_UTF_8);
        }
        catch( java.io.UnsupportedEncodingException e )
        {
            Application.getInstance().except(e,"EsfString.stringToBytes() - Could not get UTF-8 encoding of String");
            return v.getBytes();
        }
    }
    
    public static final String bytesToString(byte[] v)
    {
        if ( v == null )
            return null;
        try
        {
            return new String(v,CHARSET_UTF_8);
        }
        catch( java.io.UnsupportedEncodingException e )
        {
            Application.getInstance().except(e,"EsfString.bytesToString() - Could not use UTF-8 encoded bytes to create String");
            return new String(v);
        }
    }
    
    /**
     * This routines converts a Java String into a Javascript-safe string.
     * @return the Javascript-safe version of the string
     */
	public static String toJavascript(String s)
	{
		if ( isBlank(s) )
			return "";
		StringBuilder jbuf = new StringBuilder(s.length() * 2);
		for( char c : s.toCharArray() )
		{
			if ( c == '\n' )  // convert newlines into \n string
				jbuf.append("\\n");
			else if ( c == '\r') // convert carriage returns into \r string
				jbuf.append("\\r");
			else
			{
				if ( c == '\'' || c == '"' || c == '/' ) // if we find a quote or forward slash, escape it
					jbuf.append('\\');
				jbuf.append(c);
			}
		}
		return jbuf.toString();
	}
	
    /**
     * Determines if a string contains whitespace characters or not.
     * @param s the String to check
     * @return true if it contains at least one whitespace, else false
     */
    public static boolean containsWhitespace(String s)
    {
        if ( s == null )
            return false;
        for( int i = 0; i < s.length(); ++i )
        {
            if ( Character.isWhitespace(s.charAt(i)) )
                return true;
        }
        return false;
    }
    
    /**
     * Determines if a string contains any of a set of characters, returning the position of the first such character found
     * or -1 if not found.
     * @param s the String to scan
     * @param c the char array to find in 's'
     * @return the index position in 's' where one of the chars in 'c' was first found
     */
    public static int indexOfAnyChar(String s, char... c)
    {
        if ( s == null || c == null || c.length == 0 )
            return -1;
        for( int i = 0; i < s.length(); ++i )
        {
        	char stringChar = s.charAt(i);
        	for( char testChar : c )
        	{
        		if ( stringChar == testChar )
        			return i;
        	}
        }
        return -1;
    }
    
    /**
     * Splits a multi-line 'v' into separate lines (CR+LF or LF separated)
     * @param v the multi-line content to split (such as a textarea would provide, or property value)
     * @return a String array of lines found in 'v'
     */
    public static String[] splitLines(String v)
    {
    	if ( v == null )
    		return new String[0];
    	return v.split("\r?\n");
    }

    /**
     * Counts the number of occurrences of a given character in a string.
     * @param s the String to scan
     * @param c the char to find in 's'
     * @return the number of times 'c' is found in 's'
     */
    public static int countCharInString(String s, char c)
    {
        int num=0;
        int at = s.indexOf(c);
        while( at >= 0 )
        {
            ++num;
            at = s.indexOf(c,++at);
        }
        return num;
    }
    
    public static int estimateNumberOfTextareaRows(String s)
    {
        int numRows=0;
        int numChars=0;
        
        for( int i=0; i < s.length(); ++i )
        {
        	char c = s.charAt(i);
        	if ( c == '\n' )
        	{
        		numChars = 0;
        		++numRows;
        	}
        	else
        	{
        		++numChars;
        		if ( numChars > 80 )
        		{
               		numChars = 0;
        			++numRows;
        		}
        	}
        }
        
        return numRows;
    }
    
    /**
     * Checks if a string is empty or spaces only.
     * @return true if the string is null, empty or made up only of whitespace.
     */
    public static boolean isBlank(String v)
    {
        if ( v == null )
            return true;
        for( int i=0; i < v.length(); ++i )
        {
            if ( ! Character.isWhitespace(v.charAt(i)) )
                return false;
        }
        return true;
    }
    public static final boolean isBlank(EsfString v)
    {
    	return v == null || v.isBlank();
    }
    public static final boolean isNonBlank(String v)
    {
    	return ! isBlank(v);
    }
    public static final boolean isNonBlank(EsfString v)
    {
    	return v != null && v.isNonBlank();
    }
    
    /**
     * Checks if a string array is null, empty or contains entries with spaces only.
     * @return true if the string is null, empty or made up only of whitespace.
     */
    public static boolean isBlank(String[] v)
    {
        if ( v == null )
            return true;
        for( int i=0; i < v.length; ++i )
        {
            if ( ! isBlank(v[i]) )
                return false;
        }
        return true;
    }
    public static final boolean isNonBlank(String[] v)
    {
    	return ! isBlank(v);
    }
    
    /**
     * Checks if all String variables are blank
     * @return true if all of the variables passed in are null or blank
     */
    public static boolean areAllBlank(String... v)
    {
        if ( v == null )
            return true;
        for( String s : v )
        {
            if ( ! isBlank(s) )
            	return false;
        }
        return true;
    }
    public static final boolean areAllNonBlank(String... v)
    {
    	return ! areAnyBlank(v);
    }
    
    /**
     * Checks if any String variables are blank
     * @return true if any of the variables passed in are null or blank
     */
    public static boolean areAnyBlank(String... v)
    {
        if ( v == null )
            return true;
        for( String s : v )
        {
            if ( isBlank(s) )
            	return true;
        }
        return false;
    }
    public static final boolean areAnyNonBlank(String... v)
    {
    	return ! areAllBlank(v);
    }
    
    /**
     * Checks if some, but not all, String variables are blank.
     * @return true if some, but not all, of the variables passed in are null or blank
     */
    public static boolean areSomeButNotAllBlank(String... v)
    {
        if ( v == null )
            return false;
        boolean hasBlank 	= false;
        boolean hasNonBlank = false;
        for( String s : v )
        {
            if ( isBlank(s) )
            {
            	hasBlank = true;
            	if ( hasNonBlank )
            		return true;
            }
            else
            {
            	hasNonBlank = true;
            	if ( hasBlank )
            		return true;
            }
        }
        return false;
    }
    public static final boolean areSomeButNotAllNonBlank(String... v)
    {
    	return areSomeButNotAllBlank(v);
    }

    /**
     * Checks if a string is all capitals or not.
     * @return true if the string is all caps (and is not empty).
     */
    public static boolean isAllCaps(String v)
    {
        if ( v == null || v.length() == 0 )
            return false;
        for( int i=0; i < v.length(); ++i )
        {
            if ( ! Character.isUpperCase(v.charAt(i)) )
                return false;
        }
        return true;
    }
    
    
    /**
     * Takes a "long" string and inserts a newline after every chunkSize characters
     */
    public static String breakLongStringEvenly(String longString, int chunkSize)
    {
    	if ( longString == null || longString.length() < chunkSize )
    		return longString;
    	
    	if ( chunkSize < 1 )
    		chunkSize = 80;

    	int bufLength = longString.length();
        bufLength += (bufLength/chunkSize) + 1; // add some space for the \n we'll add
        
        StringBuilder buf = new StringBuilder(bufLength);
        int begin = 0; 
        int end   = Math.min(longString.length(),chunkSize); 
        while( begin < longString.length() ) 
        {
            buf.append( longString.substring(begin,end) ).append('\n');
            begin = end; 
            end  += chunkSize; 
            if ( end > longString.length() ) 
                end = longString.length(); 
        }
        return buf.toString();
    }

    /**
     * Takes a "long" string and inserts a newline after every maxLineLength characters is found
     * without it's own newline being found
     */
    public static String breakLongStringLines(String longString, int maxLineLength)
    {
    	if ( longString == null || longString.length() < maxLineLength )
    		return longString;
    	
    	if ( maxLineLength < 1 )
    		maxLineLength = 80;
    	
        int bufLength = longString.length();
        bufLength += (bufLength/maxLineLength) + 1; // add some space for the \n we'll add
        
        StringBuilder buf = new StringBuilder(bufLength);
        int currLineLength = 0;
        for( int i=0; i < longString.length(); ++i )
        {
        	char c = longString.charAt(i);
        	
        	if ( c == '\n')
        	{
        		buf.append(c);
        		currLineLength = 0;
        		continue;
        	}
        	
        	if ( currLineLength == maxLineLength )
        	{
        		buf.append('\n');
        		currLineLength = 0;
        	}
        	
        	if ( currLineLength < maxLineLength )
        	{
        		buf.append(c);
        		++currLineLength;
        	}
        }
        return buf.toString();
    }

    
    /**
     * Truncate a string and append ellipses
     */
    public static String truncateString(String str, int len)
    {
        if ( str == null || str.length() <= len )
            return str;
        
        StringBuilder pretty = new StringBuilder(len);
        pretty.append(str.substring(0,len-3)).append("...");
        return pretty.toString();
    }
    public static String truncateTrimmedString(String str, int len)
    {
    	return str == null ? null : truncateString(str.trim(),len);
    }
    
	public static String ensureLength(String v, int maxLength)
	{
		if ( v == null || v.length() <= maxLength )
			return v;
		return v.substring(0,maxLength);
	}

	public static String ensureTrimmedLength(String v, int maxLength)
	{
		return v == null ? null : ensureLength(v,maxLength).trim();
	}

	public static int getLength(String v)
	{
		return ( v == null ) ? 0 : v.length();
	}

   
    /**
     * Remove extra whitespace after a given whitespace character is found.  That is, only allow
     * one whitespace character to exist before a non-whitespace character appears, such as removing
     * double spaces within a string.  Therefore, a value like "Doe,   John" would become "Doe, John".
     * @param v the String to squeeze extra spaces out of
     * @return the String with only one whitespace character in a row.  Note that if the String doesn't need to be
     * changed, the original string 'v' is returned unchanged.
     */
    public static String compressWhitespace(String v)
    {
        if ( v == null || v.length() == 0 )
            return v;
        
        boolean whitespaceRemoved = false;
        boolean inWhitespace = false;
        StringBuilder cv = new StringBuilder(v.length());
        
        for( int i=0; i < v.length(); ++i )
        {
            char c = v.charAt(i);
            if ( Character.isWhitespace(c) )
            {
                if ( inWhitespace )
                    whitespaceRemoved = true;
                else
                {
                    cv.append(c);
                    inWhitespace = true;
                }
            }
            else
            {
                cv.append(c);
                inWhitespace = false;
            }
        }
        
        if ( whitespaceRemoved )
            return cv.toString();
        
        return v;
    }
    
    
    public static String stripISOControl(String v)
    {
        if ( v == null || v.length() == 0 )
            return v;
        
        boolean controlRemoved = false;
        StringBuilder cv = new StringBuilder(v.length());
        
        for( int i=0; i < v.length(); ++i )
        {
        	char c = v.charAt(i);
            if ( Character.isISOControl(c) )
            	controlRemoved = true;
            else
                cv.append(c);
        }
        
        if ( controlRemoved )
            return cv.toString();
        
        return v;
    }
    
    
    public static String stripWhitespace(String v)
    {
        if ( v == null || v.length() == 0 )
            return v;
        
        boolean whitespaceRemoved = false;
        StringBuilder cv = new StringBuilder(v.length());
        
        for( int i=0; i < v.length(); ++i )
        {
            char c = v.charAt(i);
            if ( Character.isWhitespace(c) )
            	whitespaceRemoved = true;
            else
                cv.append(c);
        }
        
        if ( whitespaceRemoved )
            return cv.toString();
        
        return v;
    }
    
    
    /**
     * Remove extra whitespace and then converts any remaining whitespaces to an underbar (_).
     * Therefore, a value like "Doe   John" would become "Doe_John".
     * @param v the String to convert
     * @return the converted String Note that if the String doesn't need to be
     * changed, the original string 'v' is returned unchanged.
     */
    public static String spacesToUnderbar(String v)
    {
        if ( v == null || v.length() == 0 )
            return v;
        
        v = compressWhitespace(v);
        
        boolean underbarAdded = false;
        StringBuilder cv = new StringBuilder(v.length());
        
        for( int i=0; i < v.length(); ++i )
        {
            char c = v.charAt(i);
            if ( Character.isWhitespace(c) )
            {
            	cv.append('_');
            	underbarAdded = true;
            }
            else
                cv.append(c);
        }
        
        if ( underbarAdded )
            return cv.toString();
        
        return v;
    }
    
    /**
     * Capitalize just the first letter
     * @param s the String to be capitalized
     * @return the String with the first letter capitalized
     */
    public static String capitalizeFirstLetter(String s)
    {
    	if ( isBlank(s) )
    		return s;
    	if ( s.length() == 1 )
    		return s.toUpperCase();
    	return Character.toUpperCase(s.charAt(0)) + s.substring(1);
    }

    /**
     * This routine will capitalize the first letter of each word, and put the others to lowercase.
     * @param s the String to be adjusted
     * @return the String with the first letter of each word capitalized and the others lowercase
     */
	public static String capitalizeFirstLetterLowercaseOthers(String s)
	{
		if ( isBlank(s) )
			return s;
		
		StringBuilder buf = new StringBuilder(s.length());
		boolean isFirstCharOfWord = true;
		for( int i=0; i < s.length(); ++i )
		{
			char c = s.charAt(i);
			if ( Character.isLetter(c) )
			{
				if ( isFirstCharOfWord )
				{
					buf.append(Character.toUpperCase(c));
					isFirstCharOfWord = false;
				}
				else
					buf.append(Character.toLowerCase(c));
			}
			else
			{
				isFirstCharOfWord = true;
				buf.append(c);
			}
		}
		return buf.toString();
	}
	
    /**
     * Returns only the numbers in the string (removing letters, whitespace, punctuation, etc.)
     * @param s the String to be checked
     * @return the String with only the numbers remaining
     */
    public static String getOnlyNumeric(String s)
    {
        if ( s == null )
            return "";
        StringBuilder numeric = new StringBuilder(s.length());
        for( int i=0; i < s.length(); ++i )
        {
            char c = s.charAt(i);
            if ( Character.isDigit(c) )
                numeric.append(c);
        }
        return numeric.toString();
    }

    /**
     * Returns only the numbers in the string (removing letters, punctuation, etc.)
     * @param s the String to be checked
     * @return the String with only the numbers and whitespace remaining
     */
    public static String getOnlyNumericWhitespace(String s)
    {
        if ( s == null )
            return "";
        StringBuilder numeric = new StringBuilder(s.length());
        for( int i=0; i < s.length(); ++i )
        {
            char c = s.charAt(i);
            if ( Character.isDigit(c) || Character.isWhitespace(c) )
                numeric.append(c);
        }
        return numeric.toString();
    }

    /**
     * Returns only the letters and numbers in the string (removing whitespace, punctuation, etc.)
     * @param s the String to be checked
     * @return the String with only the letters and numbers remaining
     */
    public static String getOnlyAlphaNumeric(String s)
    {
        StringBuilder outString = new StringBuilder(s.length());
        for( int i=0; i < s.length(); ++i )
        {
            char c = s.charAt(i);
            if ( Character.isWhitespace(c) )
                continue;
            if ( Character.isLetterOrDigit(c) )
                outString.append(c);
        }
        return outString.toString();
    }

    /**
     * Returns only the letters and numbers in the string (removing punctuation, etc.)
     * @param s the String to be checked
     * @return the String with only the letters, whitespace and numbers remaining
     */
    public static String getOnlyAlphaNumericWhitespace(String s)
    {
        StringBuilder outString = new StringBuilder(s.length());
        for( int i=0; i < s.length(); ++i )
        {
            char c = s.charAt(i);
            if ( Character.isLetterOrDigit(c) )
                outString.append(c);
        }
        return outString.toString();
    }

    /**
     * Returns only the letters in the string (removing whitespace, numbers, punctuation, etc.)
     * @param s the String to be checked
     * @return the String with only the letters remaining
     */
    public static String getOnlyAlpha(String s)
    {
        StringBuilder outString = new StringBuilder(s.length());
        for( int i=0; i < s.length(); ++i )
        {
            char c = s.charAt(i);
            if ( Character.isWhitespace(c) )
                continue;
            if ( Character.isLetter(c) )
                outString.append(c);
        }
        return outString.toString();
    }
    
    /**
     * Returns only the letters in the string (removing numbers, punctuation, etc.)
     * @param s the String to be checked
     * @return the String with only the letters and whitespace remaining
     */
    public static String getOnlyAlphaWhitespace(String s)
    {
        StringBuilder outString = new StringBuilder(s.length());
        for( int i=0; i < s.length(); ++i )
        {
            char c = s.charAt(i);
            if ( Character.isLetter(c) )
                outString.append(c);
        }
        return outString.toString();
    }
    
    public static EsfString ensureNotNull(EsfString v) 
    {
    	return v == null ? new EsfString() : v;
    }
    public static String ensureNotNull(String v) 
    {
    	return v == null ? "" : v;
    }

    
    /**
     * Used to sanitize a byte array to String to ensure it's only valid for the specified charset. If we get any errors, we'll revert to UTF-8 if it's not already that.
     */
    public static String sanitizeByCharset(byte[] bytesToBeAString, String charset)
    {
    	if ( bytesToBeAString == null )
    		return null;
    	if ( bytesToBeAString.length == 0 )
    		return "";
    	
    	if ( "cp932".equalsIgnoreCase(charset) )
    		charset = "windows-31j"; // official name for this charset
    	
    	try
    	{
        	CharsetDecoder charsetDecoder = Charset.forName(charset).newDecoder();
        	charsetDecoder.onMalformedInput(CodingErrorAction.REPLACE);
        	charsetDecoder.onUnmappableCharacter(CodingErrorAction.REPLACE);
        	charsetDecoder.replaceWith(" ");
        	
        	ByteBuffer byteBuffer = ByteBuffer.wrap(bytesToBeAString);
        	CharBuffer decodedCharBuffer = charsetDecoder.decode(byteBuffer);
        	return decodedCharBuffer.toString();
        	/* Simple String method is easy, but it doesn't do simple space replacement for bad input data like we want
        	String decodedString = decodedCharBuffer.toString();
        	String otherDecoded = new String(bytesToBeAString,charset);
        	if ( ! decodedString.equals(otherDecoded) )
        	{
        		SHOW_BIG_STRING("decodedString",decodedString);
        		SHOW_BIG_STRING("otherDecoded",otherDecoded);
        	}
        	return decodedString;
        	*/
    	}
    	catch( Exception e )
    	{
    		Application.getInstance().except(e,"EsfString.sanitizeByCharset(byte[]) charset: " + charset);
    		return CHARSET_UTF_8.equals(charset) ? "" : sanitizeByCharset(bytesToBeAString,CHARSET_UTF_8);
    	}
    }

    public static void SHOW_BIG_STRING(String prefix, String v)
    {
    	if ( v == null )
    		v = "(String v was null)";
    	if ( v.length() < 256 ) 
    		Application.getInstance().debug("SHOW_BIG_STRING: " + prefix + "; length: " + v.length() + "; v: " + v + "||EOD||");
    	else
    		Application.getInstance().debug("SHOW_BIG_STRING: " + prefix + "; length: " + v.length() + "; v initial: " + v.substring(0,128) + "||EOD||" + "; v ending: " + v.substring(v.length()-128,v.length()) + "||EOD||");
    }
}