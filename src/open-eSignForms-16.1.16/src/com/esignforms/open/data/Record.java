// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;
import org.jdom2.JDOMException;

import com.esignforms.open.Application;
import com.esignforms.open.data.NameValue;
import com.esignforms.open.data.NameValueMap;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.util.XmlUtil;

/**
 * This class represents a data record that is stored in the database as a blob,
 * encrypted or not as specified. Because it's XML text when stored, we
 * recommend you it be compressed, and encrypted if the data is sensitive. When
 * a "nameValue" is marked with the attribute clone="false", copying a data
 * object will not copy that element.
 * 
 * The format for the XML is (see NameValueMap for more details on the
 * <nameValues> element):
 * 
 * <pre>
 * <code>
 *     <esfRecord xmlns="http://open.esignforms.com/XMLSchema/2009">
 *     <id>record_unique_id</id>
 *     <typename id="type_unique_id">my_record_type</typename>
 *     <esfname>my_record_name</esfname>
 *     <encrypt>true</encrypt>
 *     <compress>true</compress>
 *     <nameValues count="2"> 
 *      <nameValue>
 * 	  <name>pathName</name>
 * 	  <value>some value</value>
 *      </nameValue>
 *      <nameValue clone="false">
 * 	  <name>some_other_name</name>
 * 	  <value>some other value</value>
 * 	  <comment>This one has a comment</comment>
 *      </nameValue>
 * 	</nameValues>
 *     </esfRecord>
 * </code>
 * </pre>
 * 
 * @see com.esignforms.open.data.NameValueMap#toXml()
 * 
 * @author Yozons, Inc.
 */
public class Record 
	extends com.esignforms.open.db.DatabaseObject
	implements java.io.Serializable
{
	private static final long serialVersionUID = -6462414031620021883L;

	protected EsfUUID id;
	protected EsfUUID typeId; /*
							   * may be null if not associated with a pre-defined
							   * type
							   */
	protected EsfName typeName; /*
								 * may be null if not associated with a
								 * pre-defined type
								 */
	protected EsfName esfname;
	protected BlobDb.EncryptOption encryptOption;
	protected BlobDb.CompressOption compressOption;
	protected NameValueMap map = new NameValueMap();
	protected boolean readOnly = false; /*
										 * Not persisted as we have no need for
										 * read-only records
										 */

	/**
	 * Creates a record without any type specifications so it can hold any
	 * name-values.
	 * 
	 * @param esfname
	 *            the String name of this record
	 * @param compressOption
	 *            the BlobDb.CompressOption to use when the record is stored
	 * @param encryptOption
	 *            the BlobDb.EncryptOption to use when the record is stored
	 * @see BlobDb
	 */
	public Record(EsfName esfname, BlobDb.CompressOption compressOption, BlobDb.EncryptOption encryptOption) {
		this(esfname, compressOption, encryptOption, null, null);
	}

	/**
	 * Creates a record of the specified type
	 * 
	 * @param esfname
	 *            the String name of this record
	 * @param compressOption
	 *            the BlobDb.CompressOption to use when the record is stored
	 * @param encryptOption
	 *            the BlobDb.EncryptOption to use when the record is stored
	 * @param typeId
	 *            the EsfUUID of the record type; null means untyped (can hold
	 *            any name-values)
	 * @param typeName
	 *            the EsfName of the record type; null means untyped (can hold
	 *            any name-values)
	 * @see BlobDb
	 */
	public Record(EsfName esfname, BlobDb.CompressOption compressOption, BlobDb.EncryptOption encryptOption, EsfUUID typeId,
			EsfName typeName) {
		id = getEsfUUID();
		this.typeId = typeId;
		this.typeName = typeName;
		this.esfname = esfname;
		this.compressOption = compressOption;
		this.encryptOption = encryptOption;
	}

	/**
	 * Creates a record like an existing record, but with the new name.
	 * 
	 * @param esfname
	 *            the name of this record
	 * @param other
	 *            the Record that is being cloned
	 */
	public Record(EsfName esfname, Record other) {
		id = getEsfUUID();
		typeId = other.typeId;
		typeName = other.typeName;
		this.esfname = esfname;
		compressOption = other.compressOption;
		encryptOption = other.encryptOption;
		map = other.map.duplicate();
	}
	public Record(Record other) {
		this(other.getEsfName(),other);
	}

	/**
	 * This constructor is used when loading existing data from the blob stored
	 * in the database.
	 * 
	 * @param dataBlob
	 *            the byte array of data blob; it is not encrypted
	 * @throws EsfException
	 *             thrown if the data cannot be processed from the XML
	 */
	public Record(byte[] dataBlob) throws EsfException {
		String xml = EsfString.bytesToString(dataBlob);
		loadXml(xml);
	}

	/**
	 * This constructor is used when loading existing data from XML.
	 * 
	 * @param dataXml
	 *            the String XML.
	 * @throws EsfException
	 *             thrown if the data cannot be processed from the XML
	 */
	public Record(String dataXml) throws EsfException {
		loadXml(dataXml);
	}

	/**
	 * This constructor is used when loading existing data from a JDOM tree.
	 * 
	 * @param esfRecordElement
	 *            the JDOM element that represents the esfRecordElement root
	 * @throws EsfException
	 *             thrown if the data cannot be processed from the JDOM
	 */
	public Record(Element esfRecordElement) throws EsfException {
		loadXmlFromJdom(esfRecordElement, "fromExistingJDOM");
	}
	public Record(EsfUUID newId, Element esfRecordElement) throws EsfException {
		loadXmlFromJdom(esfRecordElement, "fromExistingJDOM");
		id = newId;
	}

	// Utility routine for the constructors to load the XML data from the string
	// representation into this object
	protected void loadXml(String dataXml) throws EsfException {
		StringReader sr = null;

		SAXBuilder saxBuilder = new SAXBuilder(XMLReaders.NONVALIDATING);
		saxBuilder.setIgnoringElementContentWhitespace(true);

		try {
			sr = new StringReader(dataXml);

			Document doc = saxBuilder.build(sr);
			Element rootElement = doc.getRootElement();

			loadXmlFromJdom(rootElement, dataXml);
		} catch (EsfException e) {
			throw e;
		} catch (java.io.IOException e) {
			Application.getInstance().except(e, "Record.loadXml() - could not read data from the XML string: " + dataXml);
			throw new EsfException("Could not load the record.");
		} catch (JDOMException e) {
			Application.getInstance().except(e, "Record.loadXml() - could not XML parse data from the XML string: " + dataXml);
			throw new EsfException("Could not XML parse and load the record.");
		} finally {
			if (sr != null)
				try {
					sr.close();
				} catch (Exception e) {
				}
		}
	}

	// Utility routine for the constructors to load the XML data from the string
	// representation into this object
	protected void loadXmlFromJdom(Element rootElement, String dataXml) throws EsfException {
		Namespace ns = Namespace.getNamespace(XmlUtil.getXmlNamespace2009());

		try {
			String rootName = rootElement.getName();
			if (!rootName.equals("esfRecord")) {
				Application.getInstance().err("Record.loadXmlFromJdom(): Root element is not esfRecord.  Found instead: " + rootName + "; in XML: "
						+ dataXml);
				throw new EsfException("The esfRecord root tag is missing.");
			}

			id = new EsfUUID(rootElement.getChildText("id", ns));
			if (id.isNull()) {
				Application.getInstance().err("Record.loadXmlFromJdom(): required id element is missing in XML: " + dataXml);
				throw new EsfException("The esfRecord.id element is missing.");
			}

			Element typeNameElement = rootElement.getChild("typename", ns);
			if (typeNameElement == null) {
				typeId = null;
				typeName = null;
			} else {
				typeName = new EsfName(typeNameElement.getText());
				typeId = new EsfUUID(typeNameElement.getAttributeValue("id"));
				if (typeId.isNull()) {
					Application.getInstance().err("Record.loadXmlFromJdom(): required id attribute in typename element is missing in XML: " + dataXml);
					throw new EsfException("The esfRecord.typename id attribute is missing.");
				}
			}

			esfname = new EsfName(rootElement.getChildText("esfname", ns));
			if (!esfname.isValid()) {
				Application.getInstance().err("Record.loadXmlFromJdom(): required esfname element is missing in XML: " + dataXml);
				throw new EsfException("The esfRecord.esfname element is missing.");
			}

			encryptOption = EsfBoolean.toBoolean(rootElement.getChildText("encrypt", ns)) ? BlobDb.EncryptOption.ENABLE
					: BlobDb.EncryptOption.DISABLE;
			compressOption = EsfBoolean.toBoolean(rootElement.getChildText("compress", ns)) ? BlobDb.CompressOption.ENABLE
					: BlobDb.CompressOption.DISABLE;

			Element nameValuesElement = rootElement.getChild("nameValues", ns);
			if (nameValuesElement == null) {
				Application.getInstance().err("Record.loadXmlFromJdom(): No nameValues element was found in XML: " + dataXml);
				throw new EsfException("The esfRecord nameValues element is missing.");
			}

			List<Element> pairs = nameValuesElement.getChildren("nameValue", ns);
			if (pairs != null) {
				ListIterator<Element> iter = pairs.listIterator();
				while (iter.hasNext()) {
					Element nv = iter.next();
					String clone = nv.getAttributeValue("clone");
					if (EsfString.isBlank(clone))
						clone = "true";

					Attribute arrayAttribute = nv.getAttribute("array");

					Attribute typeAttribute = nv.getAttribute("type");
					String type = (typeAttribute == null) ? EsfString.TYPE : typeAttribute.getValue();

					String comment = nv.getChildText("comment", ns);

					List<Element> names = nv.getChildren("name", ns);
					if ( names == null || names.size() < 1 ) {
						Application.getInstance().err("Record.loadXmlFromJdom(): required nameValue.name element(s) missing in XML: " + dataXml);
						throw new EsfException("At least one esfRecord.nameValues.nameValue.name element is missing.");
					}
					EsfName[] nameArray = new EsfName[names.size()];
					ListIterator<Element> namesIter = names.listIterator();
					int nameIndex = 0;
					while( namesIter.hasNext() ) {
						Element n = namesIter.next();
						nameArray[nameIndex++] = new EsfName(n.getTextTrim());
					}
					EsfPathName pathName = new EsfPathName(nameArray);
					
					// If we have the array attribute, we'll process an array of values
					if (arrayAttribute != null) {
						NameValue foundNV;

						LinkedList<String> values = new LinkedList<String>();

						List<Element> valuesList = nv.getChildren("value", ns);
						if (valuesList != null) {
							ListIterator<Element> iterValues = valuesList.listIterator();
							while (iterValues.hasNext()) {
								Element valueElement = iterValues.next();
								boolean valueIsNull = EsfBoolean.toBoolean(valueElement.getAttributeValue("null"));
								if (valueIsNull)
									values.add(null);
								else
									values.add(valueElement.getText());
							}
							if (EsfString.TYPE.equals(type)) {
								EsfString[] va = new EsfString[values.size()];
								int i = 0;
								for (String s : values)
									va[i++] = EsfString.createFromToXml(s);
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfHtml.TYPE.equals(type)) {
								EsfHtml[] va = new EsfHtml[values.size()];
								int i = 0;
								for (String s : values)
									va[i++] = EsfHtml.createFromToXml(s);
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfInteger.TYPE.equals(type)) {
								EsfInteger[] va = new EsfInteger[values.size()];
								int i = 0;
								for (String s : values)
									va[i++] = EsfInteger.createFromToXml(s);
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfDecimal.TYPE.equals(type)) {
								EsfDecimal[] va = new EsfDecimal[values.size()];
								int i = 0;
								for (String s : values)
									va[i++] = EsfDecimal.createFromToXml(s);
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfMoney.TYPE.equals(type)) {
								EsfMoney[] va = new EsfMoney[values.size()];
								int i = 0;
								for (String s : values)
									va[i++] = EsfMoney.createFromToXml(s);
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfBoolean.TYPE.equals(type)) {
								EsfBoolean[] va = new EsfBoolean[values.size()];
								int i = 0;
								for (String s : values)
									va[i++] = EsfBoolean.createFromToXml(s);
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfDate.TYPE.equals(type)) {
								EsfDate[] va = new EsfDate[values.size()];
								int i = 0;
								for (String s : values)
									va[i++] = EsfDate.createFromToXml(s);
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfDateTime.TYPE.equals(type)) {
								EsfDateTime[] va = new EsfDateTime[values.size()];
								int i = 0;
								for (String s : values)
									va[i++] = EsfDateTime.createFromToXml(s);
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfEmailAddress.TYPE.equals(type)) {
								EsfEmailAddress[] va = new EsfEmailAddress[values.size()];
								int i = 0;
								for (String s : values)
									va[i++] = EsfEmailAddress.createFromToXml(s);
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfName.TYPE.equals(type)) {
								EsfName[] va = new EsfName[values.size()];
								int i = 0;
								for (String s : values)
									va[i++] = EsfName.createFromToXml(s);
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfPathName.TYPE.equals(type)) {
								EsfPathName[] va = new EsfPathName[values.size()];
								int i = 0;
								for (String s : values)
									va[i++] = EsfPathName.createFromToXml(s);
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfUUID.TYPE.equals(type)) {
								EsfUUID[] va = new EsfUUID[values.size()];
								int i = 0;
								for (String s : values)
									va[i++] = EsfUUID.createFromToXml(s);
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else {
								Application.getInstance().err("Record.loadXmlFromJdom(): unexpected nameValue with array type attribute: " + type
										+ "; for pathName: " + pathName + "; in XML: " + dataXml);
								throw new EsfException(
										"The esfRecord.nameValues.nameValue array element's type attribute is unexpected: " + type
												+ "; for pathName: " + pathName);
							}
						} else { /* it's a zero-length array of values */	
							if (EsfString.TYPE.equals(type)) {
								EsfString[] va = new EsfString[0];
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfHtml.TYPE.equals(type)) {
								EsfHtml[] va = new EsfHtml[0];
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfInteger.TYPE.equals(type)) {
								EsfInteger[] va = new EsfInteger[0];
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfDecimal.TYPE.equals(type)) {
								EsfDecimal[] va = new EsfDecimal[0];
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfMoney.TYPE.equals(type)) {
								EsfMoney[] va = new EsfMoney[0];
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfBoolean.TYPE.equals(type)) {
								EsfBoolean[] va = new EsfBoolean[0];
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfDate.TYPE.equals(type)) {
								EsfDate[] va = new EsfDate[0];
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfDateTime.TYPE.equals(type)) {
								EsfDateTime[] va = new EsfDateTime[0];
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfEmailAddress.TYPE.equals(type)) {
								EsfEmailAddress[] va = new EsfEmailAddress[0];
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfName.TYPE.equals(type)) {
								EsfName[] va = new EsfName[0];
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfPathName.TYPE.equals(type)) {
								EsfPathName[] va = new EsfPathName[0];
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else if (EsfUUID.TYPE.equals(type)) {
								EsfUUID[] va = new EsfUUID[0];
								foundNV = new NameValue(pathName, va, EsfBoolean.toBoolean(clone));
								if (EsfString.isNonBlank(comment))
									foundNV.setComment(comment);
							} else {
								Application.getInstance().err("Record.loadXmlFromJdom(): unexpected nameValue with zero length array type attribute: "
										+ type + "; for pathName: " + pathName + "; in XML: " + dataXml);
								throw new EsfException(
										"The esfRecord.nameValues.nameValue zero length array element's type attribute is unexpected: "
												+ type + "; for pathName: " + pathName);
							}
						}

						// Put the array NV in our map
						addUpdate(foundNV);
					} else  {
						// We have a simple value
						String value = null;
						Element valueElement = nv.getChild("value", ns);
						if (valueElement != null) {
							boolean valueIsNull = EsfBoolean.toBoolean(valueElement.getAttributeValue("null"));
							if (!valueIsNull)
								value = valueElement.getText();
						}

						NameValue foundNV;
						if (EsfString.TYPE.equals(type)) {
							EsfString v = EsfString.createFromToXml(value);
							foundNV = new NameValue(pathName, v, EsfBoolean.toBoolean(clone));
							if (EsfString.isNonBlank(comment))
								foundNV.setComment(comment);
						} else if (EsfHtml.TYPE.equals(type)) {
							EsfHtml v = EsfHtml.createFromToXml(value);
							foundNV = new NameValue(pathName, v, EsfBoolean.toBoolean(clone));
							if (EsfString.isNonBlank(comment))
								foundNV.setComment(comment);
						} else if (EsfInteger.TYPE.equals(type)) {
							EsfInteger v = EsfInteger.createFromToXml(value);
							foundNV = new NameValue(pathName, v, EsfBoolean.toBoolean(clone));
							if (EsfString.isNonBlank(comment))
								foundNV.setComment(comment);
						} else if (EsfDecimal.TYPE.equals(type)) {
							EsfDecimal v = EsfDecimal.createFromToXml(value);
							foundNV = new NameValue(pathName, v, EsfBoolean.toBoolean(clone));
							if (EsfString.isNonBlank(comment))
								foundNV.setComment(comment);
						} else if (EsfMoney.TYPE.equals(type)) {
							EsfMoney v = EsfMoney.createFromToXml(value);
							foundNV = new NameValue(pathName, v, EsfBoolean.toBoolean(clone));
							if (EsfString.isNonBlank(comment))
								foundNV.setComment(comment);
						} else if (EsfBoolean.TYPE.equals(type)) {
							EsfBoolean v = EsfBoolean.createFromToXml(value);
							foundNV = new NameValue(pathName, v, EsfBoolean.toBoolean(clone));
							if (EsfString.isNonBlank(comment))
								foundNV.setComment(comment);
						} else if (EsfDate.TYPE.equals(type)) {
							EsfDate v = EsfDate.createFromToXml(value);
							foundNV = new NameValue(pathName, v, EsfBoolean.toBoolean(clone));
							if (EsfString.isNonBlank(comment))
								foundNV.setComment(comment);
						} else if (EsfDateTime.TYPE.equals(type)) {
							EsfDateTime v = EsfDateTime.createFromToXml(value);
							foundNV = new NameValue(pathName, v, EsfBoolean.toBoolean(clone));
							if (EsfString.isNonBlank(comment))
								foundNV.setComment(comment);
						} else if (EsfEmailAddress.TYPE.equals(type)) {
							EsfEmailAddress v = EsfEmailAddress.createFromToXml(value);
							foundNV = new NameValue(pathName, v, EsfBoolean.toBoolean(clone));
							if (EsfString.isNonBlank(comment))
								foundNV.setComment(comment);
						} else if (EsfName.TYPE.equals(type)) {
							EsfName v = EsfName.createFromToXml(value);
							foundNV = new NameValue(pathName, v, EsfBoolean.toBoolean(clone));
							if (EsfString.isNonBlank(comment))
								foundNV.setComment(comment);
						} else if (EsfPathName.TYPE.equals(type)) {
							EsfPathName v = EsfPathName.createFromToXml(value);
							foundNV = new NameValue(pathName, v, EsfBoolean.toBoolean(clone));
							if (EsfString.isNonBlank(comment))
								foundNV.setComment(comment);
						} else if (EsfUUID.TYPE.equals(type)) {
							EsfUUID v = EsfUUID.createFromToXml(value);
							foundNV = new NameValue(pathName, v, EsfBoolean.toBoolean(clone));
							if (EsfString.isNonBlank(comment))
								foundNV.setComment(comment);
						} else {
							Application.getInstance().err("Record.loadXmlFromJdom(): unexpected nameValue element type attribute: " + type
									+ "; for pathName: " + pathName + "; in XML: " + dataXml);
							throw new EsfException("The esfRecord.nameValues.nameValue element's type attribute is unexpected: "
									+ type + "; for pathName: " + pathName);
						}

						// Put it in our map
						addUpdate(foundNV);
					}
				}
			} else {
				Application.getInstance().warning("Record.loadXmlFromJdom(): No nameValues.nameValue elements were found in XML: " + dataXml);
			}
		} catch (EsfException e) {
			throw e;
		}
	}

	/**
	 * Returns the id of this record.
	 * 
	 * @return the EsfUUID of this record
	 */
	public EsfUUID getId() {
		return id;
	}

	/**
	 * Returns the name of this record.
	 * 
	 * @return the EsfName of this record
	 */
	public EsfName getEsfName() {
		return esfname;
	}

	/**
	 * Returns the encrypt flag options.
	 * 
	 * @return true if this record is encrypted; else false
	 */
	public boolean isDoEncrypt() {
		return encryptOption == BlobDb.EncryptOption.ENABLE;
	}

	/**
	 * Returns the compress flag options.
	 * 
	 * @return returns true if this record is compressed; else false
	 */
	public boolean isDoCompress() {
		return compressOption == BlobDb.CompressOption.ENABLE;
	}

	/**
	 * Returns the id of this record type.
	 * 
	 * @return the EsfUUID of this record type; null if it's not associated with
	 *         a record type
	 */
	public EsfUUID getTypeId() {
		return typeId;
	}

	/**
	 * Returns the name of this record type.
	 * 
	 * @return the EsfName of this record type; null if it's not associated with
	 *         a record type
	 */
	public EsfName getTypeName() {
		return typeName;
	}

	/**
	 * Determines if this Record is read-only. This is not persisted and only
	 * affects this object instance.
	 * 
	 * @return the boolean true if it's marked read-only
	 */
	public boolean isReadOnly() {
		return readOnly;
	}

	/**
	 * Mark this record as read-only. This is not persisted, but is just a state
	 * for this particular Record instance.
	 */
	public void markReadOnly() {
		readOnly = true;
	}

	/**
	 * Adds the given name-value to the map, and if it's already there, it
	 * overwrites it. Each 'name' must be unique, so if you add the same name,
	 * it will overwrite the previous value.
	 * 
	 * @param nv
	 *            the NameValue to store
	 * @param the
	 *            Record object (for chaining); null if 'nv' is not valid
	 */
	public Record addUpdate(NameValue nv) {
		if (readOnly)
			return null;

		if (nv == null || ! nv.getPathName().isValid())
			return null;

		map.addUpdate(nv);
		objectChanged();
		return this;
	}

	/**
	 * Convenience method to add a name and a value.
	 * 
	 * @param name
	 *            the EsfName to add
	 * @param value
	 *            the EsfValue to add
	 * @param the
	 *            Record object (for chaining); null if 'nv' is not valid
	 * @see #addUpdate(NameValue)
	 */
	public final Record addUpdate(EsfName name, EsfValue value) {
		return (name == null || ! name.isValid() || value == null) ? null : addUpdate(new NameValue(name, value));
	}
	public final Record addUpdate(EsfPathName pathName, EsfValue value) {
		return (pathName == null || ! pathName.isValid() || value == null) ? null : addUpdate(new NameValue(pathName, value));
	}
	public final Record addUpdate(String pathName, EsfValue value) {
		return (pathName == null || ! EsfPathName.isValidEsfPathName(pathName) || value == null) ? null : addUpdate(new NameValue(new EsfPathName(pathName), value));
	}

	public final Record addUpdate(EsfName name, EsfValue[] values) {
		return (name == null || ! name.isValid() || values == null) ? null : addUpdate(new NameValue(name, values));
	}
	public final Record addUpdate(EsfPathName pathName, EsfValue[] values) {
		return (pathName == null || ! pathName.isValid() || values == null) ? null : addUpdate(new NameValue(pathName, values));
	}
	public final Record addUpdate(String pathName, EsfValue[] values) {
		return (pathName == null || ! EsfPathName.isValidEsfPathName(pathName) || values == null) ? null : addUpdate(new NameValue(new EsfPathName(pathName), values));
	}

	public final Record addUpdate(EsfName name, EsfValue value, boolean canClone) {
		return (name == null || ! name.isValid() || value == null) ? null : addUpdate(new NameValue(name, value, canClone));
	}
	public final Record addUpdate(EsfPathName pathName, EsfValue value, boolean canClone) {
		return (pathName == null || ! pathName.isValid() || value == null) ? null : addUpdate(new NameValue(pathName, value, canClone));
	}
	public final Record addUpdate(String pathName, EsfValue value, boolean canClone) {
		return (pathName == null || ! EsfPathName.isValidEsfPathName(pathName) || value == null) ? null : addUpdate(new NameValue(new EsfPathName(pathName), value, canClone));
	}

	public final Record addUpdate(EsfName name, EsfValue[] values, boolean canClone) {
		return (name == null || ! name.isValid() || values == null) ? null : addUpdate(new NameValue(name, values, canClone));
	}
	public final Record addUpdate(EsfPathName pathName, EsfValue[] values, boolean canClone) {
		return (pathName == null || ! pathName.isValid() || values == null) ? null : addUpdate(new NameValue(pathName, values, canClone));
	}
	public final Record addUpdate(String pathName, EsfValue[] values, boolean canClone) {
		return (pathName == null || ! EsfPathName.isValidEsfPathName(pathName) || values == null) ? null : addUpdate(new NameValue(new EsfPathName(pathName), values, canClone));
	}
	
	public final Record addUpdateClonableFields(Record otherRecord) {
		map.addUpdateClonableFields(otherRecord.map);
		return this;
	}

	/**
	 * Retrieves a name-value from our map by it's name.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the NameValue that matches; null if not found
	 */
	public final NameValue getByName(EsfName... esfname) {
		return map.getNameValue(esfname);
	}
	public final NameValue getByName(EsfPathName pathName) {
		return map.getNameValue(pathName);
	}
	public final NameValue getByName(String pathName) {
		return getByName( new EsfPathName(pathName) );
	}

	/**
	 * Retrieves an EsfBoolean value associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfBoolean that matches; null if not found
	 */
	public final EsfBoolean getBooleanByName(EsfName... esfname) {
		EsfValue v = getValueByName(esfname);
		return (v instanceof EsfBoolean) ? (EsfBoolean) v : null;
	}
	public final EsfBoolean getBooleanByName(EsfPathName pathName) {
		EsfValue v = getValueByName(pathName);
		return (v instanceof EsfBoolean) ? (EsfBoolean) v : null;
	}
	public final EsfBoolean getBooleanByName(String pathName) {
		return getBooleanByName(new EsfPathName(pathName));
	}

	/**
	 * Retrieves an EsfDate value associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfDate that matches; null if not found
	 */
	public final EsfDate getDateByName(EsfName... esfname) {
		EsfValue v = getValueByName(esfname);
		return (v instanceof EsfDate) ? (EsfDate) v : null;
	}
	public final EsfDate getDateByName(EsfPathName pathName) {
		EsfValue v = getValueByName(pathName);
		return (v instanceof EsfDate) ? (EsfDate) v : null;
	}
	public final EsfDate getDateByName(String pathName) {
		return getDateByName(new EsfPathName(pathName));
	}

	/**
	 * Retrieves an EsfDateTime value associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfDateTime that matches; null if not found
	 */
	public final EsfDateTime getDateTimeByName(EsfName... esfname) {
		EsfValue v = getValueByName(esfname);
		return (v instanceof EsfDateTime) ? (EsfDateTime) v : null;
	}
	public final EsfDateTime getDateTimeByName(EsfPathName pathName) {
		EsfValue v = getValueByName(pathName);
		return (v instanceof EsfDateTime) ? (EsfDateTime) v : null;
	}
	public final EsfDateTime getDateTimeByName(String pathName) {
		return getDateTimeByName(new EsfPathName(pathName));
	}

	/**
	 * Retrieves an EsfEmailAddress value associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfEmailAddress that matches; null if not found
	 */
	public final EsfEmailAddress getEmailAddressByName(EsfName... esfname) {
		EsfValue v = getValueByName(esfname);
		return (v instanceof EsfEmailAddress) ? (EsfEmailAddress)v : null;
	}
	public final EsfEmailAddress getEmailAddressByName(EsfPathName pathName) {
		EsfValue v = getValueByName(pathName);
		return (v instanceof EsfEmailAddress) ? (EsfEmailAddress)v : null;
	}
	public final EsfEmailAddress getEmailAddressByName(String pathName) {
		return getEmailAddressByName(new EsfPathName(pathName));
	}

	/**
	 * Retrieves an EsfHtml value associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfHtml that matches; null if not found
	 */
	public final EsfHtml getHtmlByName(EsfName... esfname) {
		EsfValue v = getValueByName(esfname);
		return (v instanceof EsfHtml) ? (EsfHtml)v : null;
	}
	public final EsfHtml getHtmlByName(EsfPathName pathName) {
		EsfValue v = getValueByName(pathName);
		return (v instanceof EsfHtml) ? (EsfHtml)v : null;
	}
	public final EsfHtml getHtmlByName(String pathName) {
		return getHtmlByName(new EsfPathName(pathName));
	}

	/**
	 * Retrieves an EsfInteger value associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfInteger that matches; null if not found
	 */
	public final EsfInteger getIntegerByName(EsfName... esfname) {
		EsfValue v = getValueByName(esfname);
		return (v instanceof EsfInteger) ? (EsfInteger) v : null;
	}
	public final EsfInteger getIntegerByName(EsfPathName pathName) {
		EsfValue v = getValueByName(pathName);
		return (v instanceof EsfInteger) ? (EsfInteger) v : null;
	}
	public final EsfInteger getIntegerByName(String pathName) {
		return getIntegerByName(new EsfPathName(pathName));
	}
	public final EsfInteger[] getIntegersByName(EsfPathName pathName) {
		EsfValue[] v = getValuesByName(pathName);
		if ( v == null )
			return null;
		if ( v.length == 0 )
			return new EsfInteger[0];
		if ( ! (v[0] instanceof EsfInteger) )
			return null;
		EsfInteger[] intArray = new EsfInteger[v.length];
		for( int i=0; i < v.length; ++i )
			intArray[i] = (EsfInteger)v[i];
		return intArray;
	}


	/**
	 * Retrieves an EsfDecimal value associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfDecimal that matches; null if not found
	 */
	public final EsfDecimal getDecimalByName(EsfName... esfname) {
		EsfValue v = getValueByName(esfname);
		return (v instanceof EsfDecimal) ? (EsfDecimal) v : null;
	}
	public final EsfDecimal getDecimalByName(EsfPathName pathName) {
		EsfValue v = getValueByName(pathName);
		return (v instanceof EsfDecimal) ? (EsfDecimal) v : null;
	}
	public final EsfDecimal getDecimalByName(String pathName) {
		return getDecimalByName(new EsfPathName(pathName));
	}

	/**
	 * Retrieves an EsfMoney value associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfMoney that matches; null if not found
	 */
	public final EsfMoney getMoneyByName(EsfName... esfname) {
		EsfValue v = getValueByName(esfname);
		return (v instanceof EsfMoney) ? (EsfMoney) v : null;
	}
	public final EsfMoney getMoneyByName(EsfPathName pathName) {
		EsfValue v = getValueByName(pathName);
		return (v instanceof EsfMoney) ? (EsfMoney) v : null;
	}
	public final EsfMoney getMoneyByName(String pathName) {
		return getMoneyByName(new EsfPathName(pathName));
	}

	/**
	 * Retrieves an EsfName value associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfName that matches; null if not found
	 */
	public final EsfName getNameByName(EsfName... esfname) {
		EsfValue v = getValueByName(esfname);
		return (v instanceof EsfName) ? (EsfName) v : null;
	}
	public final EsfName getNameByName(EsfPathName pathName) {
		EsfValue v = getValueByName(pathName);
		return (v instanceof EsfName) ? (EsfName) v : null;
	}
	public final EsfName getNameByName(String pathName) {
		return getNameByName(new EsfPathName(pathName));
	}

	/**
	 * Retrieves the EsfName values (array) associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfName array that matches; null if not found
	 */
	public final EsfName[] getNamesByName(EsfName... esfname) {
		EsfValue[] v = getValuesByName(esfname);
		return (v instanceof EsfName[]) ? (EsfName[]) v : null;
	}
	public final EsfName[] getNamesByName(EsfPathName pathName) {
		EsfValue[] v = getValuesByName(pathName);
		return (v instanceof EsfName[]) ? (EsfName[]) v : null;
	}
	public final EsfName[] getNamesByName(String pathName) {
		return getNamesByName(new EsfPathName(pathName));
	}


	/**
	 * Retrieves an EsfPathName value associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfPathName that matches; null if not found
	 */
	public final EsfPathName getPathNameByName(EsfName... esfname) {
		EsfValue v = getValueByName(esfname);
		return (v instanceof EsfPathName) ? (EsfPathName) v : null;
	}
	public final EsfPathName getPathNameByName(EsfPathName pathName) {
		EsfValue v = getValueByName(pathName);
		return (v instanceof EsfPathName) ? (EsfPathName) v : null;
	}
	public final EsfPathName getPathNameByName(String pathName) {
		return getPathNameByName(new EsfPathName(pathName));
	}

	/**
	 * Retrieves the EsfPathName values (array) associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfName array that matches; null if not found
	 */
	public final EsfPathName[] getPathNamesByName(EsfName... esfname) {
		EsfValue[] v = getValuesByName(esfname);
		return (v instanceof EsfPathName[]) ? (EsfPathName[]) v : null;
	}
	public final EsfPathName[] getPathNamesByName(EsfPathName pathName) {
		EsfValue[] v = getValuesByName(pathName);
		return (v instanceof EsfPathName[]) ? (EsfPathName[]) v : null;
	}
	public final EsfPathName[] getPathNamesByName(String pathName) {
		return getPathNamesByName(new EsfPathName(pathName));
	}

	
	/**
	 * Retrieves an EsfString value associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfString that matches; null if not found
	 */
	public final EsfString getStringByName(EsfName... esfname) {
		EsfValue v = getValueByName(esfname);
		return (v instanceof EsfString) ? (EsfString) v : null;
	}
	public final EsfString getStringByName(EsfPathName pathName) {
		EsfValue v = getValueByName(pathName);
		return (v instanceof EsfString) ? (EsfString) v : null;
	}
	public final EsfString getStringByName(String pathname) {
		return getStringByName(new EsfPathName(pathname));
	}

	/**
	 * Retrieves the EsfString values (array) associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfString array that matches; null if not found
	 */
	public final EsfString[] getStringsByName(EsfName... esfname) {
		EsfValue[] v = getValuesByName(esfname);
		return (v instanceof EsfString[]) ? (EsfString[]) v : null;
	}
	public final EsfString[] getStringsByName(EsfPathName pathName) {
		EsfValue[] v = getValuesByName(pathName);
		return (v instanceof EsfString[]) ? (EsfString[]) v : null;
	}
	public final EsfString[] getStringsByName(String pathName) {
		return getStringsByName(new EsfPathName(pathName));
	}

	/**
	 * Retrieves an EsfUUID value associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfUUID that matches; null if not found
	 */
	public final EsfUUID getUUIDByName(EsfName... esfname) {
		EsfValue v = getValueByName(esfname);
		return (v != null && v instanceof EsfUUID) ? (EsfUUID) v : null;
	}
	public final EsfUUID getUUIDByName(EsfPathName pathName) {
		EsfValue v = getValueByName(pathName);
		return (v != null && v instanceof EsfUUID) ? (EsfUUID) v : null;
	}
	public final EsfUUID getUUIDByName(String pathName) {
		return getUUIDByName(new EsfPathName(pathName));
	}

	/**
	 * Retrieves EsfUUID values associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfUUID array that matches; null if not found
	 */
	public final EsfUUID[] getUUIDsByName(EsfName... esfname) {
		EsfValue[] v = getValuesByName(esfname);
		return (v != null && v instanceof EsfUUID[]) ? (EsfUUID[]) v : null;
	}
	public final EsfUUID[] getUUIDsByName(EsfPathName pathName) {
		EsfValue[] v = getValuesByName(pathName);
		return (v != null && v instanceof EsfUUID[]) ? (EsfUUID[]) v : null;
	}
	public final EsfUUID[] getUUIDsByName(String pathName) {
		return getUUIDsByName(new EsfPathName(pathName));
	}

	/**
	 * Retrieves a generic EsfValue value associated with the specified esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfValue that matches; null if not found
	 */
	public final EsfValue getValueByName(EsfName... esfname) {
		NameValue nv = map.getNameValue(esfname);
		if (nv == null)
			return null;
		return nv.getValue();
	}
	public final EsfValue getValueByName(EsfPathName pathName) {
		NameValue nv = map.getNameValue(pathName);
		if (nv == null)
			return null;
		return nv.getValue();
	}
	public final EsfValue getValueByName(String pathName) {
		return getValueByName(new EsfPathName(pathName));
	}
	
	/**
	 * Array retrievable version for a simple EsfName (not a path)
	 * @param esfname the SubscriptedEsfname with or without a subscript
	 * @return the EsfValue that matches; of subscripted, the particular element so long as the subscript is within range; else null.
	 */
	public final EsfValue getValueByName(SubscriptedEsfName esfname) {
		if ( esfname.hasSubscript() ) {
			EsfValue[] va = getValuesByName(esfname);
			int nameSub = esfname.getSubscript();
			return ( va != null && va.length > nameSub ) ? va[nameSub] : null;
		} 

		return getValueByName( (EsfName)esfname );
	}


	/**
	 * Retrieves generic array of EsfValue values associated with the specified
	 * esfname.
	 * 
	 * @param esfname
	 *            the EsfName to retrieve
	 * @return the EsfValue array that matches; null if not found
	 */
	public final EsfValue[] getValuesByName(EsfName... esfname) {
		NameValue nv = map.getNameValue(esfname);
		if (nv == null)
			return null;
		return nv.getValues();
	}
	public final EsfValue[] getValuesByName(EsfPathName pathName) {
		NameValue nv = map.getNameValue(pathName);
		if (nv == null)
			return null;
		return nv.getValues();
	}
	public final EsfValue[] getValuesByName(String pathName) {
		return getValuesByName(new EsfPathName(pathName));
	}

	/*
	 * Gets the number of name-values in the record.
	 * 
	 * @return the int number of name-value fields in this record.
	 */
	public int getSize() {
		return map.getSize();
	}
	
	public NameValue[] getNameValues()
	{
		return map.toArray();
	}
	
	public NameValue[] getSortedNameValues()
	{
		return map.getSorted();
	}
	
    public void remove(EsfName name)
    {
		if (! readOnly)
		{
			map.remove(name);
			objectChanged();
		}
    }
    public void remove(EsfName[] names)
    {
		if (! readOnly)
		{
			map.remove(names);
			objectChanged();
		}
    }
    public void remove(EsfPathName pathName)
    {
		if (! readOnly)
		{
			map.remove(pathName);
			objectChanged();
		}
    }
    public void remove(String pathName)
    {
		if (! readOnly)
		{
			map.remove(pathName);
			objectChanged();
		}
    }
    
    public int removeAllWithPathPrefix(EsfName[] prefixNames)
    {
    	if ( readOnly )
    		return 0;
    	int i = map.removeAllWithPathPrefix(prefixNames);
		objectChanged();
    	return i;
    }
    public int removeAllWithPathPrefix(EsfPathName pathPrefix)
    {
    	if ( readOnly )
    		return 0;
    	int i = map.removeAllWithPathPrefix(pathPrefix);
		objectChanged();
    	return i;
    }
    public int removeAllWithPathPrefix(String pathPrefix)
    {
    	if ( readOnly )
    		return 0;
    	int i = map.removeAllWithPathPrefix(pathPrefix);
		objectChanged();
    	return i;
    }

    public void removeAll()
    {
		if (! readOnly)
		{
			map.clear();
			objectChanged();
		}
    }

	public StringBuilder appendXml(StringBuilder buf) {
		return appendXml(buf,null);
	}
	
	public StringBuilder appendXml(StringBuilder buf, List<EsfName> maskNames) {
		buf.ensureCapacity(buf.length() + getEstimatedLengthXml());
		buf.append("<esfRecord xmlns=\"").append(XmlUtil.getXmlNamespace2009()).append("\">\n");
		buf.append(" <id>").append(id).append("</id>\n");
		if (typeName != null) {
			buf.append(" <typename");
			if (typeId != null)
				buf.append(" id=\"").append(typeId).append("\">");
			else
				buf.append(">");
			buf.append(typeName).append("</typename>");
		}
		buf.append(" <esfname>").append(esfname).append("</esfname>\n");
		buf.append(" <encrypt>").append(isDoEncrypt()).append("</encrypt>\n");
		buf.append(" <compress>").append(isDoCompress()).append("</compress>\n");
		map.appendXml(buf,maskNames);
		buf.append("</esfRecord>\n");
		return buf;
	}

	
	public int getEstimatedLengthXml() {
		int size = 220 + map.getXmlEstimatedSize();
		return size;
	}

	public String toXml(boolean addXmlDeclaration) {
		return toXml(addXmlDeclaration,null);
	}

	public String toXml() {
		return toXml(false,null);
	}

	public String toXml(boolean addXmlDeclaration, List<EsfName> maskNames) {
		StringBuilder buf = new StringBuilder(getEstimatedLengthXml());
		if ( addXmlDeclaration ) {
			buf.append(XmlUtil.XML_DECLARATION_UTF_8);
		}
		return appendXml(buf,maskNames).toString();
	}

	public byte[] toXmlByteArray(boolean addXmlDeclaration) {
		String xml = toXml(addXmlDeclaration);
		return EsfString.stringToBytes(xml);
	}

	public byte[] toXmlByteArray() {
		return toXmlByteArray(false);
	}

	/*
	 * =========================== BEGIN DATABASE RELATED ROUTINES =============================
	 */

	public boolean save(Connection con) throws SQLException {
		try {
			if (readOnly)
				return false;

			if (doInsert()) {
				byte[] xmlBlob = toXmlByteArray();

				getBlobDb().insert(con, id, xmlBlob, compressOption, encryptOption);

				// Now we mark this object as if it were loaded fresh from the
				// database
				setLoadedFromDb();
				return true;
			}

			// This must be an update request. But if it hasn't changed, we can
			// just treat this as a null operation

			if (!hasChanged())
				return true;

			byte[] xmlBlob = toXmlByteArray();

			getBlobDb().update(con, id, xmlBlob, compressOption, encryptOption);

			// Now we mark this object as if it were loaded fresh from the
			// database
			setLoadedFromDb();
			return true;
		} catch (SQLException e) {
			setLastSQLException(e);
			throw e;
		}
	}

	public boolean save() {
		Application.getInstance().debug("Record.save() doInsert: " + doInsert() + "; esfname: " + esfname + "; id: " + id + "; readonly: " + readOnly);

		if (readOnly)
			return false;

		ConnectionPool pool = getConnectionPool();
		Connection con = pool.getConnection();
		try {
			boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
			return result;
		} catch (SQLException e) {
			Application.getInstance().sqlerr(e, "Record.save() doInsert: " + doInsert() + "; esfname: " + esfname + "; id: " + id);
			pool.rollbackIgnoreException(con, e);
			return false;
		} finally {
			cleanupPool(pool, con, null);
		}
	}

	public boolean delete(Connection con) throws SQLException {
		if (readOnly)
			return false;

		try {
			getBlobDb().delete(con, id);
			objectDeleted();
			return true;
		} catch (SQLException e) {
			setLastSQLException(e);
			throw e;
		}
	}

	public boolean delete() {
		Application.getInstance().debug("Record.delete() - esfname: " + esfname + "; id: " + id + "; readonly: " + readOnly);

		if (readOnly)
			return false;

		ConnectionPool pool = getConnectionPool();
		Connection con = pool.getConnection();
		try {
			boolean result = delete(con);
			con.commit();
			return result;
		} catch (SQLException e) {
			Application.getInstance().sqlerr(e, "Record.delete() - esfname: " + esfname + "; id: " + id);
			pool.rollbackIgnoreException(con, e);
			return false;
		} finally {
			cleanupPool(pool, con, null);
		}
	}

	public static class Manager	{
		
		public static Record getById(Connection con, EsfUUID id) throws SQLException {
			try {
				byte[] xmlBlob;
				xmlBlob = Application.getInstance().getBlobDb().select(con, id);
				if (xmlBlob == null) {
					Application.getInstance().err("Record.Manager.getById(con) id: " + id + "; found no XML blob data");
					return null;
				}

				Record rec = new Record(xmlBlob);

				if (!id.equals(rec.getId())) {
					Application.getInstance().err("Record.Manager.getById(con) id: " + id + "; load XML with embedded id: " + rec.getId());
					return null;
				}

				// Now we mark this object as if it were loaded fresh from the
				// database
				rec.setLoadedFromDb();

				return rec;
			} catch (EsfException e) {
				Application.getInstance().except(e, "Record.Manager.getById(con) id: " + id + "; could not load XML data");
				return null;
			}
		}

		public static Record getById(EsfUUID id) {
			Application.getInstance().debug("Record.Manager.getById() id: " + id);

			ConnectionPool pool = getConnectionPool();
			Connection con = pool.getConnection();
			try {
				Record d = getById(con, id);
				con.commit();
				return d;
			} catch (SQLException e) {
				Application.getInstance().sqlerr(e, "Record.Manager.getById() id: " + id);
				pool.rollbackIgnoreException(con, e);
				return null;
			} finally {
				cleanupPool(pool, con, null);
			}
		}
	}
}