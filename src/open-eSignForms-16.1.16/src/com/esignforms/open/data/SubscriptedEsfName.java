// Copyright (C) 2011 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import java.util.regex.Matcher;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;

/**
* A SubscriptedEsfName is a EsfName with an optional subscript.
* 
* @author Yozons, Inc.
*/
public class SubscriptedEsfName
	extends EsfName
{
 	private static final long serialVersionUID = -4186835131004074193L;

	// Finds the word (EsfName) made up of A-Z, a-z, 0-9 or '_' with an optional subscript, such as Sample_Name or Sample_Name[3].
    private static java.util.regex.Pattern SubscriptedEsfNamePattern = java.util.regex.Pattern.compile("([A-Za-z][A-Za-z0-9_]*)(\\[([0-9]+)\\])?");

	private Integer subscript;
	
	private SubscriptedEsfName(String esfName, Integer subscript)
	{
		super(esfName);
		this.subscript = subscript;
	}
	
	public int getSubscript()
	{
		return subscript == null ? -1 : subscript;
	}
	public boolean hasSubscript()
	{
		return subscript != null;
	}
	
	/**
	 * Returns the SubscriptedEsfName with optional subscript if the name to check string is a valid EsfName, with an optional subscript.
	 * 
	 * @param esfNameSpecToCheck the String that is an EsfName with an optional subscript, such as Sample_Name or Sample_Name[2]
	 * @return returns null if the esfNameSpecToCheck is not a valid esfName with an optional subscript
	 */
    public static SubscriptedEsfName createSubscriptedEsfName(String esfNameSpecToCheck)
    {
        Matcher m = SubscriptedEsfNamePattern.matcher(esfNameSpecToCheck);
        if ( m == null )
            return null;
        
        if ( ! m.find() ) 
        	return null;

        //String matchedText = m.group(0);
        String name = m.group(1);
        //String matchedSubscript = m.group(2);
        String subscript = m.group(3);
            
        if ( ! EsfName.isValidEsfName(name) )
        	return null;
            
        Integer i = EsfString.isBlank(subscript) ? null : new Integer(subscript);
            
        return new SubscriptedEsfName( name, i );
    }
    
}