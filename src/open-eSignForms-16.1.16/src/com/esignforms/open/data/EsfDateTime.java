// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import com.esignforms.open.Application;
import com.esignforms.open.user.User;
import com.esignforms.open.util.DateUtil;
import com.esignforms.open.util.HtmlUtil;

/**
 * Defines a Date-time field (yyyy-mm-dd hh:mm:ss.ccc generically speaking).
 *
 * @author Yozons, Inc.
 */
public class EsfDateTime
	implements EsfValue, java.lang.Comparable<EsfDateTime>
{
	private static final long serialVersionUID = 7918921733602727036L;

	public static final String TYPE = "datetime";

    private Date     value;
	private TimeZone timeZone = DateUtil.getDefaultTimeZone();
	private Locale   locale   = Application.getInstance().getDefaultLocale();
	
	public EsfDateTime()
	{
		value = new Date();
	}
	
	public EsfDateTime(EsfDateTime v)
	{
		value		= v == null ? null : v.value;
		timeZone	= v == null ? null : v.timeZone;
		locale		= v == null ? null : v.locale;
	}
	
	public EsfDateTime(Date v)
	{
		value = v == null ? null : v;
	}
	
    public EsfDateTime( Timestamp t )
    {
		value = t == null ? null : new Date( t.getTime() );
    }
    
    public EsfDateTime( long t )
    {
		value = new Date( t );
    }
    
	/**
	 * This will return a new instance using 'fromXml' as the source.  The 'fromXml'
	 * is stored in the name-value pairs using the corresponding toXml() method.
	 * @param fromXml the String that was stored in the XML for name-value pairs.
	 * @return a new value object.
	 * @see #toXml()
	 */
	public static EsfDateTime createFromToXml(String fromXml)
	{
		return CreateFromXmlDateTime( fromXml );
	}
	
    /**
     * Convert an XML datetime in yyyy-mm-ddTHH:MM:SSZ standard UTC form to an EsfDateTime.
     * @param xmlDateTime the XML datetime in yyyy-mm-ddTHH:MM:SSZ format
     * @return the EsfDateTime that represents the specified datetime; null if it's not a standard ZULU XML datetime
     */
    public static EsfDateTime CreateFromXmlDateTime( String xmlDateTime )
    {
    	try
    	{
    		if ( EsfString.isBlank(xmlDateTime) )
    			return new EsfDateTime((Date)null);
        	java.text.SimpleDateFormat xmlDateTimeFmt = new java.text.SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss'Z'" );
        	xmlDateTimeFmt.setTimeZone(DateUtil.getUtcTimeZone());
        	Date d = xmlDateTimeFmt.parse(xmlDateTime);
        	EsfDateTime dt = new EsfDateTime(d);
        	dt.setTimeZone(DateUtil.getUtcTimeZone());
        	return dt;
    	}
		catch( java.text.ParseException e )
		{
			return new EsfDateTime((Date)null);
		}
    }
    
    /**
     * Convert an XML datetime in yyyy-mm-ddTHH:MM:SSZ, where Z = '+' or "-" with 4 digits HH:MM offset from GMT, to an EsfDateTime.
 	 * YYYY-MM-DDTHH:MM:SS-hh:mm if found is reworked to YYYY-MM-DDTHH:MM:SS-hhmm (remove the final colon in the TZ spec)
 	 * @param xmlDateTimeWithTz the XML datetime in yyyy-mm-ddTHH:MM:SSZ, where Z = '+' or "-" with 4 digits HH:MM offset from GMT
     * @return the EsfDateTime that represents the specified datetime; null if it's not valid
     */
    public static EsfDateTime CreateFromXmlDateTimeWithTz( String xmlDateTimeWithTz )
    {
    	try
    	{
        	// XML TZ is like -08:00, but Java Z format wants -0800
			if ( xmlDateTimeWithTz.length() == 25 && (xmlDateTimeWithTz.charAt(19) == '+' || xmlDateTimeWithTz.charAt(19) == '-') && xmlDateTimeWithTz.charAt(22) == ':' )
				xmlDateTimeWithTz = xmlDateTimeWithTz.substring(0,22) + xmlDateTimeWithTz.substring(23);
        	java.text.SimpleDateFormat xmlDateTimeFmt = new java.text.SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssZ" ); 
        	Date d = xmlDateTimeFmt.parse(xmlDateTimeWithTz);
        	return new EsfDateTime(d);
    	}
		catch( java.text.ParseException e )
		{
			return new EsfDateTime((Date)null);
		}
    }
    
    /**
     * Convert date/time in yyyy-mm-dd HH:mm:ss.SSS zzz format to an EsfDateTime.
     * @param dtTZ date/time in yyyy-mm-dd HH:mm:ss.SSS zzz format
     * @return the EsfDateTime that represents the specified datetime; null invalid
     */
    public static EsfDateTime CreateFromYYYYMMDDHHMMSSMMMTTT( String dtTZ )
    {
    	try
    	{
        	java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.SSS z" );
        	Date d = fmt.parse(dtTZ);
        	
        	String[] dtParts = dtTZ.split(" ");
        	TimeZone tz = dtParts.length==3 ? TimeZone.getTimeZone(DateUtil.fixupTimeZone(dtParts[2])) : DateUtil.getDefaultTimeZone();
        	
        	EsfDateTime dt = new EsfDateTime(d);
        	dt.setTimeZone(tz);
        	if ( tz.inDaylightTime(d) ) // hack to fix since it seems these routines auto-change to standard time when you give a timezone like PST8PDT.
        		dt.addHours(-1);
        	return dt;
    	}
		catch( java.text.ParseException e )
		{
			return new EsfDateTime((Date)null);
		}
    }
    
    /**
     * Convert date/time in yyyy-mm-dd HH:mm:ss zzz format to an EsfDateTime.
     * @param dtTZ date/time in yyyy-mm-dd HH:mm:ss zzz format
     * @return the EsfDateTime that represents the specified datetime; null invalid
     */
    public static EsfDateTime CreateFromYYYYMMDDHHMMSSTTT( String dtTZ )
    {
    	try
    	{
        	java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( "yyyy-MM-dd HH:mm:ss z" );
        	Date d = fmt.parse(dtTZ);
        	
        	String[] dtParts = dtTZ.split(" ");
        	TimeZone tz = dtParts.length==3 ? TimeZone.getTimeZone(DateUtil.fixupTimeZone(dtParts[2])) : DateUtil.getDefaultTimeZone();
        	
        	EsfDateTime dt = new EsfDateTime(d);
        	dt.setTimeZone(tz);
        	if ( tz.inDaylightTime(d) ) // hack to fix since it seems these routines auto-change to standard time when you give a timezone like PST8PDT.
        		dt.addHours(-1);
        	return dt;
    	}
		catch( java.text.ParseException e )
		{
			return new EsfDateTime((Date)null);
		}
    }
    
    /**
     * Convert date/time in yyyy-mm-dd HH:mm:ss format to an EsfDateTime for the current timezone.
     * @param dt date/time in yyyy-mm-dd HH:mm:ss format
     * @return the EsfDateTime that represents the specified datetime; null invalid
     */
    public static EsfDateTime CreateFromYYYYMMDDHHMMSS( String dt )
    {
    	try
    	{
        	java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
        	fmt.setTimeZone(DateUtil.getDefaultTimeZone());
        	Date d = fmt.parse(dt);
        	return new EsfDateTime(d);
    	}
		catch( java.text.ParseException e )
		{
			return new EsfDateTime((Date)null);
		}
    }
    

    /**
     * Convert date/time in dd-MMM-yyyy h:mm a z (or dd-MMM-yyyy HH:mm z) format to an EsfDateTime for the specified timezone.
     * This format is the default on installation of OpenESF.
     * @param dt date/time in dd-MMM-yyyy h:mm a z (or dd-MMM-yyyy HH:mm z) format
     * @return the EsfDateTime that represents the specified datetime; null invalid
     */
    public static EsfDateTime CreateFromDDMMMYYYYHMMAZ( String dt )
    {
    	try
    	{
        	java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( "dd-MMM-yyyy h:mm a z" );
        	Date d = fmt.parse(dt);
        	return new EsfDateTime(d);
    	}
		catch( java.text.ParseException e )
		{
	    	try
	    	{
	    		java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( "dd-MMM-yyyy HH:mm z" );
	        	Date d = fmt.parse(dt);
	        	return new EsfDateTime(d);
	    	}
			catch( java.text.ParseException e2 )
			{
				return new EsfDateTime((Date)null);
			}
		}
    }
    

	/**
	 * Creates an EsfDateTime from a date-time of unspecified format.  It first tries DD-Mon-YYYY HH:MM[:SS] Z, then YYYY-MM-DD HH:MM[:SS] Z, then MM/DD/YYYY HH:MM[:SS] Z.
	 * If there are only two parts separated by a space, we assume the local timezone. If only one part, we assume it's just a date with no time component.
	 * @param unknownDateString the String date-time of unknown format
	 * @return the EsfDateTime based on guessing the format; null if none known
	 */
    public static EsfDateTime CreateGuessFormat( String unknownDateTimeString )
    {
    	if ( EsfString.isBlank(unknownDateTimeString) )
    		return new EsfDateTime((java.util.Date)null);
    	
    	// Handle a few special case values we support before assuming it's just a date string
		if ( "now".equalsIgnoreCase(unknownDateTimeString) )
			return new EsfDateTime();
		
		if ( unknownDateTimeString.startsWith("+") || unknownDateTimeString.startsWith("-") )
		{
			// This should be a +numDays or -numDays spec
			String numDaysString = ( unknownDateTimeString.charAt(0) == '+' ) ? unknownDateTimeString.substring(1) : unknownDateTimeString; // remove the leading + for 'stringToInt'
			int numDays = Application.getInstance().stringToInt(numDaysString, 0);
			EsfDateTime dt = new EsfDateTime();
			dt.addDays(numDays);
			return dt;
		}

    	// Let's try a few of our built in formats
    	EsfDateTime dt = CreateFromXmlDateTime(unknownDateTimeString);
    	if ( ! dt.isNull() )
    		return dt;
    	dt = CreateFromXmlDateTimeWithTz(unknownDateTimeString);
    	if ( ! dt.isNull() )
    		return dt;
    	dt = CreateFromDDMMMYYYYHMMAZ(unknownDateTimeString);
    	if ( ! dt.isNull() )
    		return dt;
    	dt = CreateFromYYYYMMDDHHMMSSMMMTTT(unknownDateTimeString);
    	if ( ! dt.isNull() )
    		return dt;
    	dt = CreateFromYYYYMMDDHHMMSSTTT(unknownDateTimeString);
    	if ( ! dt.isNull() )
    		return dt;
    	dt = CreateFromYYYYMMDDHHMMSS(unknownDateTimeString);
    	if ( ! dt.isNull() )
    		return dt;
    	
    	String[] dateTimeParts = unknownDateTimeString.split(" ");
    	if ( dateTimeParts.length == 1 )
    	{
    		EsfDate dateOnly = EsfDate.CreateGuessFormat(unknownDateTimeString);
    		return dateOnly.isNull() ? new EsfDateTime() : new EsfDateTime( dateOnly.toDate() );
    	}
    	
    	String timeZoneString = dateTimeParts.length == 2 ? null : dateTimeParts[2];
    	TimeZone timeZone = EsfString.isBlank(timeZoneString) ? DateUtil.getDefaultTimeZone() : TimeZone.getTimeZone(DateUtil.fixupTimeZone(timeZoneString));

    	EsfDate date = EsfDate.CreateGuessFormat(dateTimeParts[0]);
    	if ( date.isNull() )
    		return new EsfDateTime((java.util.Date)null);
    	
    	String[] timeParts = dateTimeParts[1].split(":");
    	if ( timeParts.length >= 1 && timeParts.length <= 3 )
    	{
    		Application app = Application.getInstance();
    		int hh = app.stringToInt(timeParts[0], 0);
    		int mm = timeParts.length > 1 ? app.stringToInt(timeParts[1], 0) : 0;
    		int ss = timeParts.length > 2 ? app.stringToInt(timeParts[2], 0) : 0;
    		
            Calendar cal = Calendar.getInstance();
            cal.setTime(date.toDate());
            cal.set( Calendar.HOUR_OF_DAY, hh );
            cal.set( Calendar.MINUTE, mm );
            cal.set( Calendar.SECOND, ss );
            cal.set( Calendar.MILLISECOND, 0 );
            cal.setTimeZone(timeZone);
            
            dt = new EsfDateTime( cal.getTime() );
            dt.setTimeZone(timeZone);
            return dt;
    	}
    	
        dt = new EsfDateTime( date.toDate() );
        dt.setTimeZone(timeZone);
        return dt;
    }
    

	/**
	 * This will return a string that represents this value's "simple type" rather than the class name, suitable for
	 * storing with the name-value data in XML.
	 * @return a String literal "datetime"
	 */
    @Override
	public String getType()
	{
		return TYPE;
	}

	/**
	 * This will return a true if the underlying value is null
	 * @return a boolean true if the underlying value is null
	 */
    @Override
	public boolean isNull()
	{
		return value == null;
	}

	/**
	 * This will return a duplicate of the value so that it has the same value, but is a distinct object.
	 * @return a new EsfValue that has the value as this object.
	 */
    @Override
	public EsfDateTime duplicate()
	{
		EsfDateTime d = value == null ? new EsfDateTime((EsfDateTime)null) : new EsfDateTime( toDate() );
		d.setLocale(getLocale());
		return d;
	}
	
	/**
	 * Returns a one element array containing this value.
	 */
	@Override
	public EsfDateTime[] toArray()
	{
		EsfDateTime[] a = new EsfDateTime[1];
		a[0] = this;
		return a;
	}
	
	/**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.
	 * @return the value as pretty String
	 */
    @Override
    public String toString()
    {
    	if ( value == null ) return "";
    	SimpleDateFormat fmt = new SimpleDateFormat(Application.getInstance().getDefaultDateTimeFormat());
    	fmt.setTimeZone(timeZone);
    	return fmt.format(value);
    }
    public String toString(User user)
    {
    	if ( value == null ) return "";
    	if ( user == null || user.getTimezoneTz() == null )
    		return toString();
    	SimpleDateFormat fmt = new SimpleDateFormat(Application.getInstance().getDefaultDateTimeFormat());
    	fmt.setTimeZone(user.getTimezoneTz());
    	return fmt.format(value);
    }
    
    public String toLogString()
    {
    	if ( value == null ) return "";
    	SimpleDateFormat fmt = new SimpleDateFormat(Application.getInstance().getDefaultLogDateTimeFormat());
    	fmt.setTimeZone(timeZone);
    	return fmt.format(value);
    }
    public String toLogString(User user)
    {
    	if ( value == null ) return "";
    	if ( user == null || user.getTimezoneTz() == null )
    		return toLogString();
    	SimpleDateFormat fmt = new SimpleDateFormat(Application.getInstance().getDefaultLogDateTimeFormat());
    	fmt.setTimeZone(user.getTimezoneTz());
    	return fmt.format(value);
    }
    
    public String toLogTimeString()
    {
    	if ( value == null ) return "";
    	SimpleDateFormat fmt = new SimpleDateFormat(Application.getInstance().getDefaultLogTimeFormat());
    	fmt.setTimeZone(timeZone);
    	return fmt.format(value);
    }
    public String toLogTimeString(User user)
    {
    	if ( value == null ) return "";
    	if ( user == null || user.getTimezoneTz() == null )
    		return toLogTimeString();
    	SimpleDateFormat fmt = new SimpleDateFormat(Application.getInstance().getDefaultLogTimeFormat());
    	fmt.setTimeZone(user.getTimezoneTz());
    	return fmt.format(value);
    }

    /**
	 * Return the date/time as yyyymmddhhmmss format for the timezone.
	 * @return the value as plain String
	 */
    @Override
    public String toPlainString()
    {
    	if ( value == null ) return "";
    	java.text.SimpleDateFormat plainDateFmt = new java.text.SimpleDateFormat( "yyyyMMddHHmmss" );
    	plainDateFmt.setTimeZone(timeZone);
    	return plainDateFmt.format(value);
    }
        
    /**
     * Return the value as a String suitable for HTML.  This is the same value as toString().
     * @return the value encoded as HTML
     */
    @Override
    public String toHtml()
    {
    	return HtmlUtil.toEscapedHtml(toString());
    }
    
    /**
     * Return the value as a String suitable for XML in yyyy-MM-ddTHH:mm:ssZ (Zulu or GMT).
     * @return the date/time value encoded as XML
     */
    @Override
    public String toXml()
    {
    	if ( value == null ) return "";
    	java.text.SimpleDateFormat xmlDateTimeFmt = new java.text.SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss'Z'" );
    	xmlDateTimeFmt.setTimeZone(DateUtil.getUtcTimeZone());
    	return xmlDateTimeFmt.format(value);
    }
    @Override
    public final long getEstimatedLengthXml()
    {
    	return 20;
    }
    
  
    /**
     * Gets the timezone for this datetime. 
     * @return the timezone of this object
     */
    public TimeZone getTimeZone()
    {
    	return timeZone;
    }

    /**
     * Sets the TimeZone for this date/time. For XML and Json and such, this has no effect as they use GMT.
     * @param tz the TimeZone to use; if null, the default TimeZone is used.
     */
    public void setTimeZone(TimeZone tz)
    {
    	timeZone = tz == null ? DateUtil.getDefaultTimeZone() : tz;
    }
    
    /**
     * Gets the locale for this datetime. 
     * @return the locale of this object
     */
    public Locale getLocale()
    {
    	return locale;
    }

    /**
     * Sets the locale for this date/time. 
     * @param l the locale to use; if null, the default locale is used.
     */
    public void setLocale(Locale l)
    {
    	locale = l == null ? Application.getInstance().getDefaultLocale() : l;
    }
    
	/**
	 * Returns the current date as a long.
	 * @return the long that represents the date.
	 */
    public final long toNumber()
    {
        return value == null ? 0 : value.getTime();
    }
    
    /**
     * Converts this date/time into a Java date object.
     * @return the Java date
     */
    public final Date toDate()
    {
    	return new Date( toNumber() );
    }
    
    /**
     * Converts the date/time to an SQL Timestamp object.
     * @return the SQL timestamp
     */
    public final java.sql.Timestamp toSqlTimestamp()
    {
        return new java.sql.Timestamp( toNumber() );
    }

    public final boolean isBefore( EsfDateTime other )
    {
    	if ( value == null )
    	{
    		if ( other != null && other.value != null )
    			return true;
    		return false;
    	}
    	if ( other == null || other.value == null ) 
    		return false;
    	return value.before(other.value);
    }

    public final boolean isAfter( EsfDateTime other )
    {
    	if ( value == null )
    		return false;
    	if ( other == null || other.value == null ) 
    		return true;
    	return value.after(other.value);
    }
    
    public int compareTo(EsfDateTime o)
    {
    	if ( value == null )
    		return o != null && o.value != null ? -1 : 0;
    	if ( o == null || o.value == null ) return 1;
    	return value.compareTo(o.value);
    }

	@Override
    public boolean equals(Object o)
    {
        if ( o instanceof EsfDateTime ) 
        {
        	EsfDateTime other = (EsfDateTime)o;
        	if ( value == null && other.value == null ) 
        		return true;
            return value.equals(other.value);
        }
        return false;
    }
    
	@Override
	public int hashCode()
	{
		return value == null ? 0 : value.hashCode();
	}

	/**
     * Used to set the current date/time so that the time is 00:00:00.000 UTC/GMT/Zulu.
     */
    public void normalizeTimeToZeroUTC()
    {
    	value = DateUtil.normalizeTimeToZeroUTC(value);
    }
    
    /**
     * Used to set the current date/time so that the time is 00:00:00.000 in the current timezone.
     */
    public void normalizeTimeToZero()
    {
    	value = DateUtil.normalizeTimeToZero(value,timeZone);
    }
    public void normalizeTimeToZero(TimeZone tz)
    {
    	value = DateUtil.normalizeTimeToZero(value,timeZone,tz);
    }
    
	/**
     * Used to set the current date/time so that the time is 23:59:59.999 UTC/GMT/Zulu.
     */
    public void normalizeTimeToEndOfDayUTC()
    {
    	value = DateUtil.normalizeTimeToEndOfDayUTC(value);
    }
    
    /**
     * Used to set the current date/time so that the time is 23:59:59.999 in the current timezone.
     */
    public void normalizeTimeToEndOfDay()
    {
    	value = DateUtil.normalizeTimeToEndOfDay(value,timeZone);
    }
    public void normalizeTimeToEndOfDay(TimeZone tz)
    {
    	value = DateUtil.normalizeTimeToEndOfDay(value,timeZone,tz);
    }
    
    public String format(TimeZone tz, String format)
    {
    	if ( value == null ) return "";
    	SimpleDateFormat fmt;
    	try
    	{
    		fmt = new SimpleDateFormat(format);
    	}
    	catch( Exception e ) 
    	{
    		Application app = Application.getInstance();
    		app.except(e, "EsfDateTime.format() - Invalid format specified: " + format);
    		fmt = new SimpleDateFormat(app.getDefaultDateTimeFormat());
    	}
    	fmt.setTimeZone(tz);
    	return fmt.format(value);
    }
    public String format(String format)
    {
    	return format(timeZone,format);
    }
    public String format(User user, String format)
    {
    	return format(user.getTimezoneTz(),format);
    }
    
    public String formatToHtml(String format)
    {
    	return HtmlUtil.toEscapedHtml(format(format));
    }
    public String formatToHtml(TimeZone tz, String format)
    {
    	return HtmlUtil.toEscapedHtml(format(tz,format));
    }
    public String formatToHtml(User user, String format)
    {
    	return HtmlUtil.toEscapedHtml(format(user, format));
    }

    /**
     * Formats the date/time for the current timezone as yyyy-MM-dd HH:mm:ss.SSS zzz
     * @return the date/time down to the millsecond with the TZ
     */
    public String toDateTimeMsecString()
    {
    	if ( value == null ) return "";
    	java.text.SimpleDateFormat msecFmt = new java.text.SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.SSS zzz" );
    	msecFmt.setTimeZone( timeZone ); 
        return msecFmt.format( value );
    }

    /**
     * Formats the date/time for the current timezone as yyyy-MM-dd HH:mm:ss.SSS'Z' for the GMT/Zulu timezone.
     * @return the date/time down to the millsecond with the TZ set to GMT/Zulu
     */
    public String toDateTimeMsecZuluString()
    {
    	if ( value == null ) return "";
    	java.text.SimpleDateFormat msecFmt = new java.text.SimpleDateFormat( "yyyy-MM-dd HH:mm:ss.SSS'Z'" );
    	msecFmt.setTimeZone( DateUtil.getUtcTimeZone() ); 
        return msecFmt.format( value );	
    }
    
    /**
     * Formats the date/time in the 'long' wordy way, such as January 4, 2010 3:30:42pm PST, based on the locale.
     * @return the long date/time format
     */
    public String toLongString()
    {
    	if ( value == null ) return "";
    	DateFormat fmt = DateFormat.getDateTimeInstance(DateFormat.LONG,DateFormat.FULL,locale);
    	fmt.setTimeZone( timeZone ); 
    	return fmt.format(value);
    }
    
    /**
     * Formats the date/time in the 'long time' wordy way, such as 3:30:42pm PST, based on the locale.
     * @return the long time format
     */
    public String toLongTimeString()
    {
    	if ( value == null ) return "";
    	DateFormat fmt = DateFormat.getTimeInstance(DateFormat.FULL,locale);
    	fmt.setTimeZone( timeZone ); 
    	return fmt.format(value);
    }
       
    /**
     * Formats the date/time in the 24 hour clock HH:mm:ss zzz format with the current timezone
     * @return the 24-hour time with TZ
     */
    public String to24HourTimeZoneString()
    {
    	if ( value == null ) return "";
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( "HH:mm:ss zzz" );
        fmt.setTimeZone(timeZone);
    	return fmt.format(value);
    }
    public String to24HourTimeZoneString(User user)
    {
    	if ( value == null ) return "";
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( "HH:mm:ss zzz" );
        fmt.setTimeZone(user.getTimezoneTz());
    	return fmt.format(value);
    }
    
    /**
     * Formats the date/time in the 24 hour clock HH:mm:ss with no TZ
     * @return the 24-hour time without a TZ
     */
    public String to24HourString()
    {
    	if ( value == null ) return "";
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( "HH:mm:ss" );
        fmt.setTimeZone(timeZone);
    	return fmt.format(value);
    }
    
    public EsfDateTime addTimeInterval(int numUnits, String intervalUnit)
    {
    	value = DateUtil.dateTimeIntervalFromDate(numUnits, intervalUnit, value);
    	return this;
    }
        
    /**
     * Add/subtract the specified number of milliseconds from the current date/time.
     * @param numMsecs the number of milliseconds to add or subtract
     * @return this object (for simple chaining)
     */
    public EsfDateTime addMsecs(int numMsecs)
    {
    	if ( value != null )
    	{
    		long mseconds = value.getTime() + numMsecs;
    		value.setTime(mseconds);
    	}
    	return this;
    }

    /**
     * Add/subtract the specified number of minutes from the current date/time.
     * @param numMinutes the number of minutes to add or subtract
     * @return this object (for simple chaining)
     */
    public EsfDateTime addMinutes(int numMinutes)
    {
    	value = DateUtil.dateMinutesFromDate(numMinutes, value);
    	return this;
    }

    /**
     * Add/subtract the specified number of minutes from the current date/time.
     * @param numMinutes the number of minutes to add or subtract
     * @return this object (for simple chaining)
     */
    public EsfDateTime addHours(int numMinutes)
    {
    	value = DateUtil.dateHoursFromDate(numMinutes, value);
    	return this;
    }

    /**
     * Add/subtract the specified number of days from the current date/time.
     * @param numDays the number of days to add or subtract
     * @return this object (for simple chaining)
     */
    public EsfDateTime addDays(int numDays)
    {
    	value = DateUtil.dateDaysFromDate(numDays, value);
    	return this;
    }

    /**
     * Returns true if the date is a weekday.
     * @return true if this is a weekday.
     */
    public boolean isWeekDay()
    {
    	return DateUtil.isWeekDay(value);
    }
    
    /**
     * Adds the specified number of week days (currently has no support for holidays)
     * @param numWeekDays the number of weekdays to add/subtract
     * @return this object (for simple chaining)
     */
    public EsfDateTime addWeekDays( int numWeekDays )
    {
    	int addDay   = (numWeekDays < 0) ? -1 : 1;
    	int numTimes = (numWeekDays < 0) ? numWeekDays*-1 : numWeekDays;
    	
    	while( numTimes-- > 0 )
    	{
    		do // get the next/previous weekday
    		{
    			addDays(addDay);
    		} 
    		while( ! isWeekDay() );
        }
    	return this;
    }

    /**
     * Add/subtract the specified number of years from the current datetime.
     * @param numYears the number of years to add or subtract
     * @return this object (for simple chaining)
     */
    public EsfDateTime addYears( int numYears )
    {
    	value = DateUtil.dateYearsFromDate(numYears, value);
    	return this;
    }
    
    /**
     * Calculates the next date/time for the specified hour and minute.  The seconds/milliseconds will be set to zero.
     * @param hour the hour number 0-23
     * @param minute the minute of that hour, 0-59
     * @return this object (for simple chaining)
     */
    public EsfDateTime nextDateForHourMinute(int hour, int minute)
    {
    	value = DateUtil.nextDateForHourMinute(hour, minute, value, timeZone);
    	return this;
    }
    
    public EsfDateTime nextDateForDayOfMonth(int dayOfMonth)
    {
    	value = DateUtil.nextDateForDayOfMonth(dayOfMonth, value, timeZone);
    	return this;
    }

    public EsfDateTime nextDateForDayOfMonthUnlessAlreadyIs(int dayOfMonth)
    {
    	value = DateUtil.nextDateForDayOfMonthUnlessAlreadyIs(dayOfMonth, value, timeZone);
    	return this;
    }

    /**
     * Return the value as a String suitable for XML in yyyy-MM-ddTHH:mm:ssZ using the current timezone 
     * where Z = '+' or "-" with 4 digits HH:MM offset from GMT
     * @return the date/time value encoded as XML with a local timezone offset
     */
    public final String toXmlWithTz()
    {
    	return toXmlWithTz(timeZone);
    }
    public final String toXmlWithTz(User user)
    {
    	return toXmlWithTz(user.getTimezoneTz());
    }
    public String toXmlWithTz(TimeZone timeZone)
    {
    	if ( value == null ) return "";
    	// XML TZ is like -08:00, but Java Z format wants -0800
    	java.text.SimpleDateFormat xmlDateTimeWithTzFmt = new java.text.SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssZ" ); 
    	xmlDateTimeWithTzFmt.setTimeZone(timeZone);
		// If we have a 4 digit TZ, make it HH:MM format rather than HHMM format Java gives us
		String xml = xmlDateTimeWithTzFmt.format( value );
		xml = xml.substring(0,22) + ":" + xml.substring(22);
		return xml;
    }
    
    public static EsfDateTime ensureNotNull(EsfDateTime v) 
    {
    	return v == null ? new EsfDateTime() : v;
    }

}