// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import com.esignforms.open.Application;
import com.esignforms.open.config.Literals;
import com.esignforms.open.util.HtmlUtil;

/**
 * Defines a Long integer field.  For general needs, figured no need to support Short or Integer at this point as
 * we have no real numeric efficiency with EsfValue objects and most computer are 64-bit these days anyway.
 *
 * @author Yozons, Inc.
 */
public class EsfInteger
	implements EsfValue, Comparable<EsfInteger>
{
	private static final long serialVersionUID = -97203382452822438L;

	public static final String TYPE 	= "integer";
    
	private Long 					value;
	private Locale  				locale	= Application.getInstance().getDefaultLocale();
	private DecimalFormatSymbols	symbols = new DecimalFormatSymbols(locale);
	
	/**
	 * Create an integer with value 0.
	 */
	public EsfInteger()
	{
		value = new Long(0L);
	}
	
	/**
	 * Create an integer with the specified string value base 10.  It should only be digits and a leading + or -
	 * @param v the String to set the value to; if null or invalid, our integer will be null
	 */
	public EsfInteger(String v)
	{
		try
		{
			value = v == null ? null : Long.parseLong( normalize(v) );
		}
		catch( NumberFormatException e )
		{
			value = null;
		}
	}
	
	/**
	 * Create an integer from another integer
	 * @param v the EsfInteger to use
	 */
	public EsfInteger(EsfInteger v)
	{
		value = v == null ? null : v.value;
		if ( v != null )
		{
			locale	= v.locale;
			symbols	= v.symbols;
		}
	}
	
	/**
	 * Create an integer from a long
	 * @param v the EsfInteger to use
	 */
	public EsfInteger(long v)
	{
		value = new Long(v);
	}
	
	/**
	 * This will return a new instance using 'fromXml' as the source.  The 'fromXml'
	 * is stored in the name-value pairs using the corresponding toXml() method.
	 * @param fromXml the String that was stored in the XML for name-value pairs.
	 * @return a new value object.
	 * @see #toXml()
	 */
	public static EsfInteger createFromToXml(String fromXml)
	{
		return new EsfInteger( fromXml );
	}
	
	/**
	 * This will return a string that represents this value's "simple type" rather than the class name, suitable for
	 * storing with the name-value data in XML.
	 * @return a String type "integer"
	 */
	@Override
	public String getType()
	{
		return TYPE;
	}

	/**
	 * This will return a true if the underlying value is null
	 * @return always returns false
	 */
	@Override
	public final boolean isNull()
	{
		return value == null;
	}

	/**
	 * This will return a duplicate of the value so that it has the same value, but is a distinct object.
	 * @return a new EsfInteger that has the value as this object.
	 */
	@Override
	public EsfInteger duplicate()
	{
		return value == null ? new EsfInteger((String)null) : new EsfInteger(value.longValue());
	}
	
	/**
	 * Returns a one element array containing this value.
	 */
	@Override
	public EsfInteger[] toArray()
	{
		EsfInteger[] a = new EsfInteger[1];
		a[0] = this;
		return a;
	}
	
	@Override
	public boolean equals(Object other)
	{
		if ( other instanceof EsfInteger )
			return value.equals(((EsfInteger)other).value);
		return false;
	}
	@Override
	public int hashCode()
	{
		return value == null ? 0 : value.hashCode();
	}

	
    /**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.
	 * @return the value as pretty String; returns "" if null
	 */
	@Override
    public final String toString()
    {
    	if ( value == null ) return "";
    	DecimalFormat format = new DecimalFormat("###,###,###,###,##0",symbols);
    	return format.format(value);
    }
    
	/**
	 * Return the value as a "plain" string, with no adornments, such as for storing in a DB
	 * @return the value as plain String; returns an empty string if null
	 */
	@Override
    public final String toPlainString()
    {
    	return value == null ? "" : value.toString();
    }
   
    /**
     * Return the value as a String suitable for HTML 
     * @return the value encoded as HTML
     */
	@Override
    public String toHtml()
    {
    	return toString();
    }

    /**
     * Return the value as a String suitable for XML 
     * @return the value encoded as XML
     */
	@Override
    public String toXml()
    {
    	return toPlainString();
    }
    public long getEstimatedLengthXml()
    {
    	return 10;
    }
    

    /**
     * Gets the locale for this integer. 
     * @return the locale of this object
     */
    public Locale getLocale()
    {
    	return locale;
    }

    /**
     * Sets the locale for this integer. 
     * @param l the locale to use; if null, the default locale is used.
     */
    public void setLocale(Locale l)
    {
    	locale 	= l == null ? Application.getInstance().getDefaultLocale() : l;
    	symbols = new DecimalFormatSymbols(locale);
    }
    
	/**
	 * Change the value of our integer using the specified string
	 * @param v the EsfString with an integer string literal inside; if the string value is invalid, our object becomes null
	 */
	public void setValue(EsfString v)
	{
		try
		{
			value = (v == null || v.isNull() ) ? null : Long.parseLong(normalize(v.toPlainString()));
		}
		catch( NumberFormatException e )
		{
			value = null;
		}
	}
	
	/**
	 * Change the value of our integer using the specified string
	 * @param v the EsfString with an integer string literal inside; if the string value is invalid, our object becomes null
	 */
	public void setValue(String v)
	{
		try
		{
			value = (v == null) ? null : Long.parseLong(normalize(v));
		}
		catch( NumberFormatException e )
		{
			value = null;
		}
	}
	
	/**
	 * Change the value of our integer using the specified long
	 * @param v the long value to set our number to
	 */
	public void setValue(long v)
	{
		value = new Long(v);
	}
	
	public void add(EsfValue v)
	{
		if ( v == null || v.isNull() )
			return;
		if ( isNull() )
			setValue(0);
		if ( v instanceof EsfInteger )
			value += ((EsfInteger)v).toLong();
		else if ( v instanceof EsfDecimal )
			value += ((EsfDecimal)v).toEsfInteger().toLong();
	}
	
	public void subtract(EsfValue v)
	{
		if ( v == null || v.isNull() )
			return;
		if ( isNull() )
			setValue(0);
		if ( v instanceof EsfInteger )
			value -= ((EsfInteger)v).toLong();
		else if ( v instanceof EsfDecimal )
			value -= ((EsfDecimal)v).toEsfInteger().toLong();
	}
	
	public void multiply(EsfValue v)
	{
		if ( v == null || v.isNull() )
			return;
		if ( isNull() )
			setValue(0);
		if ( v instanceof EsfInteger )
			value *= ((EsfInteger)v).toLong();
		else if ( v instanceof EsfDecimal )
			value *= ((EsfDecimal)v).toEsfInteger().toLong();
	}
	
	public void divide(EsfValue v)
	{
		if ( v == null || v.isNull() )
			return;
		try
		{
			if ( isNull() )
				setValue(0);
			if ( v instanceof EsfInteger )
				value /= ((EsfInteger)v).toLong();
			else if ( v instanceof EsfDecimal )
				value /= ((EsfDecimal)v).toEsfInteger().toLong();
		}
		catch( ArithmeticException e ) {}
	}
	

	/**
	 * This will return a true if the underlying value is zero
	 * @return a boolean true if the underlying value is zero
	 */
	public final boolean isZero()
	{
		return value != null && value.longValue() == 0;
	}
	public final boolean isPositive()
	{
		return value != null && value.longValue() > 0;
	}
	public final boolean isNegative()
	{
		return value != null && value.longValue() < 0;
	}
	public final boolean equals(EsfInteger v)
	{
		return ((value == null || value == 0) && (v == null || v.value == null || v.value == 0)) || 
		        value != null && v != null && v.value != null && value.equals(v.toLong());
	}
	public final boolean equals(long v)
	{
		return value != null && value.longValue() == v;
	}
	public final int compareTo(EsfInteger v)
	{
		if ( value == null || value == 0 )
			return (v == null || v.value == null || v.value == 0) ? 0 : -1;
		if ( v == null || v.value == null ) 
			return 1;
		return value.compareTo(v.value);
	}
	public final boolean isLessThan(EsfInteger v)
	{
		if ( value == null )
			return (v == null || v.value == null || v.value == 0) ? false : true;
		if ( v == null || v.value == null )
			return false;
		return value.longValue() < v.value.longValue();
	}
	public final boolean isLessThan(long v)
	{
		return value == null || value.longValue() < v;
	}
	public final boolean isLessThanEqualTo(EsfInteger v)
	{
		if ( value == null )
			return (v == null || v.value == null || v.value == 0) ? true : false;
		if ( v == null || v.value == null )
			return value == 0 ? true : false;
		return value.longValue() <= v.value.longValue();
	}
	public final boolean isLessThanEqualTo(long v)
	{
		return value == null || value.longValue() <= v;
	}
	public final boolean isGreaterThan(EsfInteger v)
	{
		if ( value == null )
			return false;
		if ( v == null || v.value == null )
			return value == 0 ? false : true;
		return value.longValue() > v.value.longValue();
	}
	public final boolean isGreaterThan(long v)
	{
		return value != null && value.longValue() > v;
	}
	public final boolean isGreaterThanEqualTo(EsfInteger v)
	{
		if ( value == null )
			return (v == null || v.value == null || v.value == 0) ? true : false;
		if ( v == null || v.value == null )
			return true;
		return value.longValue() >= v.value.longValue();
	}
	public final boolean isGreaterThanEqualTo(long v)
	{
		return value != null && value.longValue() >= v;
	}
	
	/**
	 * Display this integer as bytesize units (k,m,g) for kilobytes, megabytes or gigabytes.
	 * @return the String version of this number as bytesize units. We show nothing less than kilobytes.  Returns 0 if value is null.
	 */
	public String toByteSizeUnits()
	{
		if ( value == null )
			return byteSizeInUnits(0,symbols);
		return byteSizeInUnits(value.longValue(),symbols);
	}
	
	/**
	 * Returns as a long value
	 * @return the long value that represents this object; returns 0 if null
	 */
	public long toLong()
	{
		if ( value == null )
			return 0;
		return value.longValue();
	}
	public int toInt()
	{
		if ( value == null )
			return 0;
		return value.intValue();
	}
	public short toShort()
	{
		if ( value == null )
			return 0;
		return value.shortValue();
	}
	
	public BigDecimal toBigDecimal()
	{
		if ( value == null )
			return new BigDecimal(0);
		return new BigDecimal(value.longValue());
	}
	
	public EsfDecimal toEsfDecimal()
	{
		if ( value == null )
			return new EsfDecimal();
		return new EsfDecimal(toBigDecimal());
	}
	
	/**
	 * Returns the integer formated using the NumberFormat specified.
	 * @param decimalFormat the String with a DecimalFormat specifier.  The standard symbols object is used. If null or blank, toString() is returned.
	 * @return the String formatted number.
	 */
    public final String format(String decimalFormat)
    {
    	if ( value == null )
    		return "";
    	
    	if ( EsfString.isBlank(decimalFormat) )
    		return toString();
    		
    	DecimalFormat format = new DecimalFormat(decimalFormat,symbols);
    	return format.format(value);
    }

    public final String formatToHtml(String decimalFormat)
    {
    	if ( value == null )
    		return "";
    	
    	if ( EsfString.isBlank(decimalFormat) )
    		return toHtml();
    		
    	DecimalFormat format = new DecimalFormat(decimalFormat,symbols);
    	return HtmlUtil.toEscapedHtml(format.format(value));
    }

    // These two methods are the traditional SI decimal units for disks and such: 1KB=1000 bytes; 1MB=1000*1000 bytes; 1GB=1000*1000*1000 bytes
    public static synchronized String byteSizeInUnits(long size)
    {
    	return byteSizeInUnits(size, new DecimalFormatSymbols(Application.getInstance().getDefaultLocale()) );
    }
    
    public static String byteSizeInUnits(long size,DecimalFormatSymbols symbols)
    {
        if ( size <= 0 )
            return "0KB";

        if ( size <= Literals.KB )
            return "1KB";
        
        if ( size < Literals.MB )
        {
        	DecimalFormat kilobyteFormat = new DecimalFormat("#,##0.#KB",symbols);
            double newSize = (double)size / Literals.KB;
            return kilobyteFormat.format(newSize);
        }
        if ( size < Literals.GB )
        {
        	DecimalFormat megabyteFormat	= new DecimalFormat("#,##0.#MB",symbols);
            double newSize = (double)size / Literals.MB;
            return megabyteFormat.format(newSize);
        }

        DecimalFormat gigabyteFormat	= new DecimalFormat("###,###,##0.#GB",symbols);
        double newSize = (double)size / Literals.GB;
        return gigabyteFormat.format(newSize);
    }
    
    // These two methods are IEC binary units 1KiB=1024 bytes; 1MiB=1024*1024 bytes; 1GiB=1024*1024*1024...
    public static synchronized String byteSizeInBinaryUnits(long size)
    {
    	return byteSizeInBinaryUnits(size, new DecimalFormatSymbols(Application.getInstance().getDefaultLocale()) );
    }
    
    public static String byteSizeInBinaryUnits(long size,DecimalFormatSymbols symbols)
    {
        if ( size <= 0 )
            return "0KiB";

        if ( size <= Literals.KiB )
            return "1KiB";
        
        if ( size < Literals.MiB )
        {
        	DecimalFormat kibibyteFormat = new DecimalFormat("#,##0.#KiB",symbols);
            double newSize = (double)size / Literals.KiB;
            return kibibyteFormat.format(newSize);
        }
        if ( size < Literals.GiB )
        {
        	DecimalFormat mebibyteFormat	= new DecimalFormat("#,##0.#MiB",symbols);
            double newSize = (double)size / Literals.MiB;
            return mebibyteFormat.format(newSize);
        }

        DecimalFormat gibibyteFormat	= new DecimalFormat("###,###,##0.#GiB",symbols);
        double newSize = (double)size / Literals.GiB;
        return gibibyteFormat.format(newSize);
    }
    
    /**
     * Normalize the String integer. If it returns null, it implies the integer is not formatted in a way we can normalize it.
     * @param v
     * @return the normalized integer or null if invalid; blank is considered to be 0.
     */
    public static String normalize(String v) 
    {
    	if ( v == null )
    		return null;

    	if ( EsfString.isBlank(v) )
    		return "0";
    	
    	v = v.trim();
    	
    	boolean hasPositiveNegativeSign = false;
    	boolean hasDigit = false;
    	
        StringBuilder normal = new StringBuilder(v.length());
        for( int i=0; i < v.length(); ++i )
        {
            char c = v.charAt(i);
            
            if ( c == '+' )
            {
            	if ( hasPositiveNegativeSign || hasDigit ) // can only have one and must preceed any digits
            		return null;
            	hasPositiveNegativeSign = true;
            	// swallow a leading positive sign
            }
            else if ( c == '-' )
            {
            	if ( hasPositiveNegativeSign || hasDigit )// can only have one and must preceed any digits
            		return null;
            	hasPositiveNegativeSign = true;
            	normal.append(c);
            }
            else if ( c == ',' )
            {
            	if ( ! hasDigit ) // allow a comma, but only if we've seen at least one digit
            		return null;
            	// swallow it
            }
            else if ( c == ' ' )
            {
            	if ( hasDigit )
            		return null;
            	// swallow it
            }
            else if ( Character.isDigit(c) )
            {
            	hasDigit = true;
            	normal.append(c);
            }
            else
            	return null;
        }

        return normal.toString();
    }
    
    public static EsfInteger ensureNotNull(EsfInteger v) 
    {
    	return v == null ? new EsfInteger() : v;
    }

}