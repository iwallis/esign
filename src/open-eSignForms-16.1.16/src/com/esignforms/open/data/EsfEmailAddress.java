// Copyright (C) 2009-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import javax.mail.internet.InternetAddress;

import com.esignforms.open.Application;
import com.esignforms.open.config.MessageFormatFile;
import com.esignforms.open.Errors;
import com.esignforms.open.util.HtmlUtil;
import com.esignforms.open.util.XmlUtil;


/**
 * Defines an email address field.  Can be composed of both a display name and a traditional email address.
 * So it works with names like:
 * 	support@somehost.com
 * 	"Technical Support" <support@somehost.com>
 *
 * @author Yozons, Inc.
 */
public class EsfEmailAddress
	implements EsfValue, Comparable<EsfEmailAddress>
{
	private static final long serialVersionUID = 9056029988416812842L;

	public static final String TYPE 			= "emailaddress";
    public static final int    EMAIL_ADDR_MIN	= 6;   // i.e.   a@x.ca  (we don't support intranet only email addresses: a@x)
    public static final int    EMAIL_ADDR_MAX	= 100;

	private String displayName;
	private String emailAddress;
	
	/**
	 * Create a null email address, which is not valid.
	 */
	public EsfEmailAddress()
	{
		displayName = emailAddress = null;
	}
	
	/**
	 * Create an email address with the specified string.
	 * @param emailAddress the String email address with or without a display name to set the value to
	 */
	public EsfEmailAddress(String emailAddress)
	{
    	if ( emailAddress != null && emailAddress.indexOf("<") >= 0 )
    		parseEmail(emailAddress);
    	else
    	{
            this.displayName  = null;
            this.emailAddress = emailAddress == null ? null : emailAddress.trim();
    	}
	}
	
	/**
	 * Create an email address with the specified name and email address.
	 * @param displayName the String display name 
	 * @param emailAddress the String email address 
	 * It must pass our email validation check, and be 6-100 characters long.
	 */
	public EsfEmailAddress(String displayName, String emailAddress)
	{
		this.displayName = displayName;
		if ( emailAddress != null )
			emailAddress = emailAddress.trim();
		if ( isValidEmail(emailAddress) )
			this.emailAddress = emailAddress;
		else
			this.emailAddress = null;
	}
	
	/**
	 * Create a EsfEmailAddress from another EsfEmailAddress
	 * @param v the EsfEmailAddress to use
	 */
	public EsfEmailAddress(EsfEmailAddress v)
	{
		displayName	 = v.displayName;
		emailAddress = v.emailAddress;
	}
	
	/**
	 * This will return a new instance using 'fromXml' as the source.  The 'fromXml'
	 * is stored in the name-value pairs using the corresponding toXml() method.
	 * @param fromXml the String that was stored in the XML for name-value pairs.
	 * @return a new value object.
	 * @see #toXml()
	 */
	public static EsfEmailAddress createFromToXml(String fromXml)
	{
		return new EsfEmailAddress( fromXml );
	}
	
	/**
	 * This will return a string that represents this value's "simple type" rather than the class name, suitable for
	 * storing with the name-value data in XML.
	 * @return a String literal "emailaddress"
	 */
	@Override
	public String getType()
	{
		return TYPE;
	}

	@Override
	public boolean isNull() 
	{
		return emailAddress == null;
	}

	/**
	 * This will return a duplicate of the value so that it has the same value, but is a distinct object.
	 * @return a new EsfValue that has the value as this object.
	 */
	@Override
	public EsfEmailAddress duplicate()
	{
		return new EsfEmailAddress( displayName, emailAddress );
	}
	
	/**
	 * Returns a one element array containing this value.
	 */
	@Override
	public EsfEmailAddress[] toArray()
	{
		EsfEmailAddress[] a = new EsfEmailAddress[1];
		a[0] = this;
		return a;
	}
	
	/**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.
	 * @return the email address as pretty String
	 */
    @Override
    public String toString()
    {
    	return getQuotedFullEmailAddress();
    }
    
	/**
	 * Return the value as a "plain" string, with no adornments, such as for storing in a DB
	 * @return the value as plain String; 
	 */
    @Override
    public final String toPlainString()
    {
    	if ( isNull() )
    		return "";
        return ( EsfString.isBlank(displayName) ) ? emailAddress : displayName + " <" + emailAddress + ">";
    }
    
    /**
     * Return the value as a String suitable for HTML 
     * @return the value encoded as HTML
     */
    @Override
    public String toHtml()
    {
    	return HtmlUtil.toEscapedHtml(getQuotedFullEmailAddress());
    }

    /**
     * Return the value as a String suitable for XML 
     * @return the value encoded as XML
     */
    @Override
    public String toXml()
    {
    	return XmlUtil.toEscapedXml(getQuotedFullEmailAddress());
    }
    @Override
    public long getEstimatedLengthXml()
    {
    	int len = ( displayName == null ) ? 0 : displayName.length();
    	len += emailAddress == null ? 0 : emailAddress.length();
    	return len + 2;
    }
	
	
	/**
	 * Change the value of our EsfEmailAddress to the specified EsfEmailAddress
	 * @param v the EsfEmailAddress to use
	 */
	public void setValue(EsfEmailAddress v)
	{
		displayName	 = v.displayName;
		emailAddress = v.emailAddress;
	}
	
	/**
	 * Change the value of our EsfEmailAddress to the specified string
	 * @param v the string to use
	 */
	public void setValue(String v)
	{
    	if ( v.indexOf("<") >= 0 )
    		parseEmail(v);
    	else
    	{
            this.displayName  = null;
            this.emailAddress = v;
    	}
	}
	
	public String getDisplayName()
	{
		return displayName;
	}
	/**
	 * Change the display name
	 * @param v the String display name to use
	 */
	public void setDisplayName(String v)
	{
		displayName = v;
	}
    public String getQuotedDisplayName()
    {
    	if ( EsfString.isBlank(displayName) )
    		return "";
    		
    	int numQuotes = EsfString.countCharInString(displayName,'\"');
    	if ( numQuotes == 0 )
    		return "\"" + displayName + "\"";
    		
		StringBuilder buf = new StringBuilder(displayName.length() + numQuotes);
		buf.append("\"");
		for( int i=0; i < displayName.length(); ++i )
		{
			char c = displayName.charAt(i);
			if ( c == '\"' )
				buf.append("\\\""); // append \" 
			else	
				buf.append(c);
		}
		buf.append("\"");
		return buf.toString();
    }
	
    public String getEmailAddress()
    {
    	return emailAddress;
    }
	/**
	 * Change the email address portion
	 * @param v the String email address portion to use
	 */
	public void setEmailAddress(String v)
	{
		emailAddress = v;
	}
    public String getQuotedEmailAddress()
    {
    	return QuoteEmailAddress(isNull() ? "" : emailAddress);
    }
    public static String QuoteEmailAddress(String email)
    {
    	return "<" + email + ">";
    }
    
	public String getQuotedFullEmailAddress()
	{
		if ( EsfString.isBlank(displayName) )
			return isNull() ? "" : emailAddress;
		return getQuotedDisplayName() + " " + getQuotedEmailAddress();
	}

	
	/**
	 * This will return a true if the underlying email address is valid
	 * @return a boolean true if the underlying email address is valid
	 */
	public final boolean isValid()
	{
		return isValidEmail(emailAddress);
	}


	/**
	 * This will return a true if the underlying email address is blank (null, empty or only whitespace)
	 * @return a boolean true if the underlying email address is blank
	 */
	public final boolean isBlank()
	{
		return EsfString.isBlank(emailAddress);
	}

	/**
	 * This will return a true if the underlying email address is not blank (non-null, non-empty has has characters other than whitespace)
	 * @return a boolean true if the underlying email address is not blank
	 */
	public final boolean isNonBlank()
	{
		return EsfString.isNonBlank(emailAddress);
	}
	
    // Two are considered equal if just the email address portion matches regardless of case.
	@Override
    public boolean equals(Object o)
    {
        if ( o instanceof EsfEmailAddress )
            return ((EsfEmailAddress)o).emailAddress.equalsIgnoreCase(emailAddress);
        return false;
    }
    
	@Override
	public int hashCode()
	{
		return emailAddress.hashCode();
	}

    
    public int compareTo(EsfEmailAddress o)
    {
        return emailAddress.compareToIgnoreCase(o.emailAddress);
    }

	
    protected void parseEmail(String emailAddress)
    {
    	boolean inQuote = false;
    	boolean inEmail = false;
    	
    	StringBuilder name  = new StringBuilder(EMAIL_ADDR_MAX);
    	StringBuilder email = new StringBuilder(EMAIL_ADDR_MAX);
    	
    	for( int i=0; i < emailAddress.length(); ++i )
    	{
    		char c = emailAddress.charAt(i);
    		
    		if ( c == '<' )
    		{
    			if ( ! inQuote )
    			{
    				inEmail = true;
    				continue; // We found a starting <, so we're now in the email portion (eat the '<')
    			}
    		}
    		else if ( c == '>' )
    		{
    			if ( inEmail )
    				break; // We found the ending >, so we should be done (eat the '>')
    		}
    		else if ( c == '\"' )
    		{
    			if ( ! inEmail )
    			{
        			inQuote = ! inQuote;
        			continue;  // Found the quote which we'll eat
    			}
    		}
    		else if ( c == '\\' && i < emailAddress.length() ) // If we have a backslash there's another character following, we only the next character (eat the backslash)
    		{
    			++i;
    			c = emailAddress.charAt(i);
    		}
    		
			if ( inEmail )
				email.append(c);
			else
				name.append(c);
    	}
    	
        this.displayName  = name.toString().trim();
        this.emailAddress = email.toString().trim();
    }

	
    

    private static final String INVALID_EMAIL_ADDR_DOMAIN_CHARS = "@\'\\\";:?!()[]{}^|<>/";
    private static final String VALID_EXTRA_EMAIL_ADDR_CHARS = "@-$_#.\'";
    
    /**
     * Validates just the email address portion of an email address (i.e. support@somehost.com without any brackets or display names)
     * @param addr the email address portion to check
     * @return true if the email address is considered to be validly formed (it still not be a working email address)
     */
    public static boolean isValidEmail(String addr)
    {
    	return isValidEmail(addr,null);
    }
    public static boolean isValidEmail(String addr, Errors errors, String... errorFields)
    {
    	MessageFormatFile msgs = Application.getInstance().getServerMessages();
    	
        if ( EsfString.isBlank(addr) || addr.length() < EMAIL_ADDR_MIN )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.tooshort"),errorFields);
            return false;
        }
        
        if ( addr.length() > EMAIL_ADDR_MAX )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.toolong", EMAIL_ADDR_MAX),errorFields);
            return false;
        }
        
        if ( EsfString.containsWhitespace(addr) )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.containsspace"),errorFields);
            return false;
        }
        
        // Replacement because Schwab managed to get an A0 character in (Unicode non-blocking space?) at 
        // the end of an email address, so we'll instead see if we can block if it's not one of the valid
        // but limited set of characters that we'll try to enforce rather than trying to block any of
        // the "illegal" ones.
        for( int i=0; i < addr.length(); ++i )
        {
            char c = addr.charAt(i);
            if ( (c >= '0' && c <= '9') ||
                 (c >= 'A' && c <= 'Z') ||
                 (c >= 'a' && c <= 'z') ||
                 (VALID_EXTRA_EMAIL_ADDR_CHARS.indexOf(c) >= 0)
               )
            {
                // This is okay
            }
            else
            {
                if ( errors != null )
                    errors.addError(msgs.getString("email.error.validcharsonly", VALID_EXTRA_EMAIL_ADDR_CHARS),errorFields);
                return false;
            }
        }
        
        // The username can contain this, but it must not start/end with it.  The domain name cannot have it at all.
        if ( addr.charAt(0) == '\'' )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.startswithsinglquote"),errorFields);
            return false;
        }
            
        if ( addr.indexOf("\'\'") >= 0 )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.hasdoublesinglquote"),errorFields);
            return false;
        }
            
        if ( addr.charAt(0) == '.' )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.startswithperiod"),errorFields);
            return false;
        }
            
        if ( addr.indexOf("..") >= 0 )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.hasdoubleperiod"),errorFields);
            return false;
        }
            
        if ( addr.indexOf("@.") >= 0 )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.hasperiodat"),errorFields);
            return false;
        }

        if ( addr.indexOf(".@") >= 0 )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.hasatperiod"),errorFields);
            return false;
        }

        int at=addr.lastIndexOf("@");
        if (at < 0 )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.missingat"),errorFields);
            return false;
        }
            
        if ( at == 0 )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.startswithat"),errorFields);
            return false;
        }
            
        if ( EsfString.countCharInString(addr,'@') > 1 )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.multiat"),errorFields);
            return false;
        }
            
        //String username = addr.substring(0,at);
        String hostname = addr.substring(at+1);
        if ( hostname == null || hostname.length() < 4 )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.missinghost"),errorFields);
            return false;
        }
        
        for( int i=0; i < hostname.length(); ++i )
        {
            if ( INVALID_EMAIL_ADDR_DOMAIN_CHARS.indexOf(hostname.charAt(i)) >= 0 )
            {
                if ( errors != null )
                    errors.addError(msgs.getString("email.error.validhostcharsonly", INVALID_EMAIL_ADDR_DOMAIN_CHARS),errorFields);
                return false;
            }
        }

        int dot = hostname.lastIndexOf(".");
        if ( dot < 1 )
        {
        	if ( errors != null )
        		errors.addError(msgs.getString("email.error.hostmissingdot"),errorFields);
            return false;
        }
            
        String tld = hostname.substring(dot+1);
        if ( tld == null || tld.length() < 2 || tld.length() > 63 )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.missingtld"),errorFields);
            return false;
        }
        
        // Finally, let's do any other checks that Java provides.
        try
        {
            new InternetAddress(addr);
        }
        catch( javax.mail.internet.AddressException e )
        {
            if ( errors != null )
                errors.addError(msgs.getString("email.error.othererror", e.getMessage()),errorFields);
            return false;
        }
          
        return true; 
    }
    
    /**
     * This method assumes that emailAddress has already been verified regarding proper format. 
     * It does the split at the last '@' since domain names cannot contain such a symbol, while the 
     * username part technically can if escaped.
     * @param emailAddress the VALID String email address to get the domain part only
     * @return the String domain part of the emailAddress
     */
    public static String getDomainPart(String emailAddress)
    {
        int    at     = emailAddress.lastIndexOf('@');
        String domain = emailAddress.substring(at+1);
        return domain.trim();
    }
    
    
    /**
     * This method assumes that emailAddress has already been verified regarding proper format. 
     * It does the split at the last '@' since domain names cannot contain such a symbol, while the 
     * username part technically can if escaped.
     * @param emailAddress the VALID String email address 
     * @return the String[2] with [0] being the username and [1] being the domain part
     */
    public static String[] getParts(String emailAddress)
    {
        int    at     = emailAddress.lastIndexOf('@');

        String[] userDomain = new String[2];
        userDomain[0] = emailAddress.substring(0,at);
        userDomain[1] = emailAddress.substring(at+1);
         
        return userDomain;
    }
    
    public static EsfEmailAddress ensureNotNull(EsfEmailAddress v) 
    {
    	return v == null ? new EsfEmailAddress() : v;
    }

}