// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

/**
 * This class holds a name and value pair and an optional comment.  The value may be single value or an array of values.
 * Neither the name nor the value can be null, though the value may itself point to a null object inside.
 * The name can be an array of names that essentially create a hierarchy/path concept, so that name could be composed
 * of multiple EsfName objects, such as "Group" and "SuperGroup" that are effectively stored under the unique String
 * name of "Group/SuperGroup".  This allows names to be unique within their paths rather than across the name-value map.
 * 
 * @author Yozons, Inc.
 */
public class NameValue
    implements java.io.Serializable, Comparable<NameValue>
{
	private static final long serialVersionUID = -5484087873666846126L;

	protected EsfPathName	pathName = null;	// Calculated value, all lowercase
    protected EsfValue		value  = null;		// We'll either have a single value (normal), or an array of values, but not both
    protected EsfValue[] 	values = null;
    protected EsfString     comment= null;		// An optional comment that can be stored with this name value pair.
    protected boolean 		canClone;			// When false, we don't copy these over when creating a new NameValueMap based on another.
    
    protected NameValue()
    {
    }
    
    public NameValue(EsfName n,EsfValue v)
    {
        this(n,v,true);
    }
    
    public NameValue(EsfName n,EsfValue v,boolean canClone)
    {
        pathName = new EsfPathName(n);
    	value = v;
    	values = null;
        this.canClone = canClone;
    }
    
    public NameValue(EsfPathName p,EsfValue v)
    {
        this(p,v,true);
    }
    
    public NameValue(EsfPathName p,EsfValue v,boolean canClone)
    {
        pathName = p;
    	value = v;
    	values = null;
        this.canClone = canClone;
    }
    
    public NameValue(EsfName n,EsfValue[] v)
    {
        this(n,v,true);
    }
    
    public NameValue(EsfName n,EsfValue[] v,boolean canClone)
    {
        pathName = new EsfPathName(n);
    	value = null;
    	values = v;
        this.canClone = canClone;
    }
    
    // Constructors for use with multi-level names.
    public NameValue(EsfName[] n,EsfValue v)
    {
        this(n,v,true);
    }

    public NameValue(EsfName[] n,EsfValue v,boolean canClone)
    {
        pathName = new EsfPathName(n);
    	value = v;
    	values = null;
        this.canClone = canClone;
    }
    
    public NameValue(EsfName[] n,EsfValue[] v)
    {
        this(n,v,true);
    }

    public NameValue(EsfName[] n,EsfValue[] v,boolean canClone)
    {
        pathName = new EsfPathName(n);
    	value = null;
    	values = v;
        this.canClone = canClone;
    }
    
    public NameValue(EsfPathName p,EsfValue[] v)
    {
        this(p,v,true);
    }

    public NameValue(EsfPathName p,EsfValue[] v,boolean canClone)
    {
        pathName = p;
    	value = null;
    	values = v;
        this.canClone = canClone;
    }
    
    // Copy constructor
    public NameValue(NameValue other)
    {
        pathName 	= other.pathName;
    	value		= other.value;
    	values		= other.values;
    	canClone	= other.canClone;
    }
    
	public NameValue duplicate()
	{
		if ( isArray() )
		{
			EsfValue[] newValues = new EsfValue[values.length];
			for( int i=0; i < values.length; ++i )
				newValues[i] = values[i].duplicate();
			NameValue nv = new NameValue( pathName.duplicate(), newValues, canClone );
			nv.setComment(comment);
			return nv;
		}
		NameValue nv = new NameValue( pathName.duplicate(), value.duplicate(), canClone );
		nv.setComment(comment);
		return nv;
	}

    
    @Override
    public boolean equals(Object o)
    {
    	if ( o instanceof NameValue )
    		return getPathName().equals( ((NameValue)o).getPathName() );
    	return false;
    }
    
    @Override
    public int hashCode()
    {
    	return getPathName().hashCode();
    }
    
    @Override
    public int compareTo(NameValue o)
    {
        return getPathName().compareTo( o.getPathName() );
    }
    
    public boolean isValid()
    {
    	return pathName != null && pathName.isValid() && ( value != null || values != null );
    }
    
    public EsfPathName getPathName()
    {
    	return pathName;
    }

    public EsfName[] getNames()
    {
        return pathName.getNames();
    }


    public EsfValue getValue()
    {
    	if ( values != null && values.length > 0 )
    		return values[0];
        return value;
    }
    
    public EsfValue[] getValues()
    {
    	if ( values != null )
    		return values;
    	if ( value == null )
    		return new EsfValue[0];
    	return value.toArray();
    }
    
    /**
     * This routine routines the values[i] if 'i' is the array's range, else null.
     * If it's not an array, and i==0, it will return the single non-array value, else null.
     * @param i
     * @return
     */
    public EsfValue getValue(int i)
    {
    	if ( values != null )
    	{
    		if ( i >= 0 && i < values.length )
    			return values[i];
    		return null;
    	}
    	return i == 0 ? value : null;
    }
    
    public boolean isArray()
    {
    	return values != null;
    }
    
    public boolean hasComment()
    {
        return comment != null && comment.isNonBlank();
    }
    public EsfString getComment()
    {
        return comment;
    }
    public EsfString getNonNullComment()
    {
        return hasComment() ? comment : new EsfString("");
    }
    public void setComment(EsfString v)
    {
    	comment = v;
    }
    public void setComment(String v)
    {
    	comment = new EsfString(v);
    }

    public boolean canClone()
    {
        return canClone;
    }
}