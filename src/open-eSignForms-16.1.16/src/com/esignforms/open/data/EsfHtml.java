// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

/**
 * Defines an HTML field, which is just a String in reality, but we want to know this type distinct from it
 * for encoding reasons -- that is, an HTML field does no encoding when writing to a web page so it's important
 * that it always be valid HTML.
 *
 * @author Yozons, Inc.
 */
public class EsfHtml 
	extends EsfString 
{
	private static final long serialVersionUID = 8180805031928751242L;

	public static final String TYPE 			= "html";

	/**
	 * Create a null string.
	 */
	public EsfHtml()
	{
		super.value = null;
	}
	
	/**
	 * Create an HTML from another HTML
	 * @param v the EsfHtml to use
	 */
	public EsfHtml(EsfHtml v)
	{
		super.value = v.value;
	}
	
	/**
	 * Create an HTML from a String
	 * @param v the EsfString to use
	 */
	public EsfHtml(EsfString v)
	{
		super.value = v.value;
	}
	
	/**
	 * Create an HTML with the specified value.
	 * @param v the String to set the value to; may be null
	 */
	public EsfHtml(String v)
	{
		super.value = v;
	}
	
	/**
	 * Create an HTML from a UTF-8 byte array
	 * @param v the UTF-8 byte array
	 */
	public EsfHtml(byte[] v)
	{
		super.value = bytesToString(v);
	}
	
	/**
	 * This will return a string that represents this value's "simple type" rather than the class name, suitable for
	 * storing with the name-value data in XML.
	 * @return the String "html"
	 */
	@Override
	public String getType()
	{
		return TYPE;
	}

	/**
	 * Change the value of our HTML to the specified HTML
	 * @param v the EsfHtml to use
	 */
	public void setValue(EsfHtml v)
	{
		super.value = v.value;
	}
	
	/**
	 * This will return a true if the underlying value is blank (null, empty or only whitespace or the "&nbsp;" literal)
	 * @return a boolean true if the underlying value is blank
	 */
	public final boolean isBlankOrNbsp()
	{
		return isBlankOrNbsp(super.value);
	}

	/**
	 * This will return a true if the underlying value is not blank (non-null, non-empty has has characters other than whitespace)
	 * @return a boolean true if the underlying value is not blank
	 */
	public final boolean isNonBlankAndNbsp()
	{
		return ! isBlankOrNbsp();
	}
	
	/**
	 * This will return a duplicate of the value so that it has the same value, but is a distinct object.
	 * @return a new EsfValue that has the value as this object.
	 */
	public EsfHtml duplicate()
	{
		return new EsfHtml( super.value );
	}
	
	public EsfHtml trim()
	{
		return ( value == null ) ? new EsfHtml() : new EsfHtml(value.trim());
	}
	
	/**
	 * Returns a one element array containing this value.
	 */
	@Override
	public EsfHtml[] toArray()
	{
		EsfHtml[] a = new EsfHtml[1];
		a[0] = this;
		return a;
	}
	
	@Override
	public boolean equals(Object other)
	{
		if ( other instanceof EsfHtml )
			return super.value.equals(((EsfHtml)other).value);
		else if ( other instanceof EsfString )
			return super.equals(other);
		return false;
	}

	
	/**
	 * This will return a new instance using 'fromXml' as the source.  The 'fromXml'
	 * is stored in the name-value pairs using the corresponding toXml() method.
	 * @param fromXml the String that was stored in the XML for name-value pairs.
	 * @return a new value object.
	 * @see #toXml()
	 */
	public static EsfHtml createFromToXml(String fromXml)
	{
		return new EsfHtml( fromXml );
	}
	
	/**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.  Will return "&lt;null&gt;" if the value is null.
	 * @return the value as pretty String
	 */
	@Override
    public String toString()
    {
    	return super.value == null ? "&lt;null&gt;" : super.value;
    }

    /**
     * Return the value as a String suitable for HTML 
     * @return the value encoded as HTML
     */
    public String toHtml()
    {
    	return toString();
    }
    
    /**
     * Return the value as a String suitable for pretty display of HTML, such as handling multiple spaces as &nbsp; and converting
     * embedded CR+LF as <br/> tags.
     * @return the value encoded as pretty HTML
     */
    public String toDisplayHtml()
    {
    	return toString();
    }
    
    /**
     * Checks if the string is blank, or the HTML equivalent of just "&nbsp;"
     * @param v the String to test if blank or &nbsp;
     * @return true if 'v' is blank or the &nbsp; HTML equivalent
     */
	public static boolean isBlankOrNbsp(String v)
	{
		return isBlank(v) || "&nbsp;".equalsIgnoreCase(v);
	}
	
    public static EsfHtml ensureNotNull(EsfHtml v) 
    {
    	return v == null ? new EsfHtml() : v;
    }

}