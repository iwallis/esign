// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import com.esignforms.open.Application;
import com.esignforms.open.util.HtmlUtil;

/**
 * Defines a decimal field based on BigDecimal.  
 *
 * @author Yozons, Inc.
 */
public class EsfDecimal
	implements EsfValue, Comparable<EsfDecimal>
{
	private static final long serialVersionUID = -2449073179614805096L;

	public static final String TYPE 	= "decimal";
    
	protected BigDecimal			value;
	protected Locale  				locale	= Application.getInstance().getDefaultLocale();
	protected DecimalFormatSymbols	symbols = new DecimalFormatSymbols(locale);
	
	/**
	 * Create a decimal with value 0.00.
	 */
	public EsfDecimal()
	{
		value = new BigDecimal("0.00");
	}
	
	/**
	 * Create a decimal with the specified string value base 10.  It should only be digits and a leading + or -
	 * @param v the String to set the value to; if null or invalid, our decimal will be null
	 */
	public EsfDecimal(String v)
	{
		try
		{
			boolean isPercent = v != null && v.indexOf('%') > 0;
			v = normalize(v);
			value = (v == null) ? null : new BigDecimal( v );
			if ( v != null && isPercent )
				value = value.scaleByPowerOfTen(-2);
		}
		catch( NumberFormatException e )
		{
			value = null;
		}
	}
	
	/**
	 * Create a decimal from another decimal
	 * @param v the EsfDecimal to use
	 */
	public EsfDecimal(EsfDecimal v)
	{
		value = v == null ? null : v.value;
		if ( v != null )
		{
			locale	= v.locale;
			symbols	= v.symbols;
		}
	}
	
	/**
	 * Create a decimal from a BigDecimal
	 * @param v the EsfDecimal to use
	 */
	public EsfDecimal(BigDecimal v)
	{
		value = v;
	}
	
	/**
	 * This will return a new instance using 'fromXml' as the source.  The 'fromXml'
	 * is stored in the name-value pairs using the corresponding toXml() method.
	 * @param fromXml the String that was stored in the XML for name-value pairs.
	 * @return a new value object.
	 * @see #toXml()
	 */
	public static EsfDecimal createFromToXml(String fromXml)
	{
		return new EsfDecimal( fromXml );
	}
	
	/**
	 * This will return a string that represents this value's "simple type" rather than the class name, suitable for
	 * storing with the name-value data in XML.
	 * @return a String type "decimal"
	 */
	@Override
	public String getType()
	{
		return TYPE;
	}

	/**
	 * This will return a true if the underlying value is null
	 * @return always returns false
	 */
	@Override
	public final boolean isNull()
	{
		return value == null;
	}

	/**
	 * This will return a duplicate of the value so that it has the same value, but is a distinct object.
	 * @return a new EsfDecimal that has the value as this object.
	 */
	@Override
	public EsfDecimal duplicate()
	{
		return new EsfDecimal( isNull() ? null : value.toPlainString() );
	}
	
	/**
	 * Returns a one element array containing this value.
	 */
	@Override
	public EsfDecimal[] toArray()
	{
		EsfDecimal[] a = new EsfDecimal[1];
		a[0] = this;
		return a;
	}
	
	@Override
	public boolean equals(Object other)
	{
		if ( other instanceof EsfDecimal )
			return ((EsfDecimal)other).value.equals(value);
		return false;
	}
	@Override
	public int hashCode()
	{
		return isNull() ? 0 : value.hashCode();
	}

	public final BigDecimal toBigDecimal()
	{
		return value;
	}
	
	public final EsfInteger toEsfInteger()
	{
		return isNull() ? new EsfInteger(0) : new EsfInteger(value.longValue());
	}
	
    /**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.
	 * @return the value as pretty String; returns "" if null
	 */
	@Override
    public String toString()
    {
    	if ( isNull() )
    		return "";
    	
    	String fractionFormat;
    	if ( value.scale() > 0 )
    	{
    		StringBuilder buf = new StringBuilder(value.scale()+1);
    		buf.append(".0");
    		for( int i=1; i < value.scale(); ++i)
    			buf.append("#");
    		fractionFormat = buf.toString();
    	}
    	else
    		fractionFormat = "";
    		
    	DecimalFormat format = new DecimalFormat("###,###,###,###,##0"+fractionFormat,symbols);
    	return format.format(value);
    }
	
    public String toString(int numFractional)
    {
    	if ( isNull() )
    		return "";
    	
    	String fractionFormat;
    	if ( numFractional > 0 )
    	{
    		StringBuilder buf = new StringBuilder(numFractional+1);
    		buf.append(".");
    		for( int i=0; i < numFractional; ++i)
    			buf.append("0");
    		fractionFormat = buf.toString();
    	}
    	else
    		fractionFormat = "";
    		
    	DecimalFormat format = new DecimalFormat("###,###,###,###,##0"+fractionFormat,symbols);
    	return format.format(value);
    }

    public final String toPercentString()
	{
		if ( isNull() )
			return "";
		
		BigDecimal valuePercent = value; // value.scaleByPowerOfTen(2);
    	String fractionFormat;
    	if ( valuePercent.scale() > 2 )
    	{
    		int newScale = valuePercent.scale() - 2;
    		StringBuilder buf = new StringBuilder(newScale+1);
    		buf.append(".0");
    		for( int i=1; i < newScale; ++i)
    			buf.append("#");
    		fractionFormat = buf.toString();
    	}
    	else
    		fractionFormat = "";
    		
    	DecimalFormat format = new DecimalFormat("###,###,###,###,##0"+fractionFormat+"%",symbols);
    	return format.format(valuePercent);
	}
    
    public final String toPercentString(int numFractional)
	{
		if ( isNull() )
			return "";
		
		BigDecimal valuePercent = value; // value.scaleByPowerOfTen(2);
    	String fractionFormat;
    	if ( numFractional > 0 )
    	{
    		StringBuilder buf = new StringBuilder(numFractional+1);
    		buf.append(".");
    		for( int i=0; i < numFractional; ++i)
    			buf.append("0");
    		fractionFormat = buf.toString();
    	}
    	else
    		fractionFormat = "";
    		
    	DecimalFormat format = new DecimalFormat("###,###,###,###,##0"+fractionFormat+"%",symbols);
    	return format.format(valuePercent);
	}
    
	/**
	 * Return the value as a "plain" string, with no adornments, such as for storing in a DB
	 * @return the value as plain String; returns an empty string if null
	 */
	@Override
    public final String toPlainString()
    {
    	return isNull() ? "" : value.toPlainString();
    }
   
    public final String toPlainString(int numFractional)
	{
    	if ( isNull() )
    		return "";
    	
    	String fractionFormat;
    	if ( numFractional > 0 )
    	{
    		StringBuilder buf = new StringBuilder(numFractional+1);
    		buf.append(".");
    		for( int i=0; i < numFractional; ++i)
    			buf.append("0");
    		fractionFormat = buf.toString();
    	}
    	else
    		fractionFormat = "";
    		
    	DecimalFormat format = new DecimalFormat("##############0"+fractionFormat,symbols);
    	return format.format(value);
	}
    
    /**
     * Return the value as a String suitable for HTML 
     * @return the value encoded as HTML
     */
	@Override
    public String toHtml()
    {
    	return toString();
    }

    /**
     * Return the value as a String suitable for XML 
     * @return the value encoded as XML
     */
	@Override
    public String toXml()
    {
    	return toPlainString();
    }
    public long getEstimatedLengthXml()
    {
    	return 10;
    }
    

    /**
     * Gets the locale for this integer. 
     * @return the locale of this object
     */
    public Locale getLocale()
    {
    	return locale;
    }

    /**
     * Sets the locale for this integer. 
     * @param l the locale to use; if null, the default locale is used.
     */
    public void setLocale(Locale l)
    {
    	locale 	= l == null ? Application.getInstance().getDefaultLocale() : l;
    	symbols = new DecimalFormatSymbols(locale);
    }
    
    public DecimalFormatSymbols getSymbols()
    {
    	return symbols;
    }
    
	/**
	 * Change the value of our decimal using the specified string
	 * @param v the EsfString with an decimal string literal inside; if the string value is invalid, our object becomes null
	 */
	public void setValue(EsfString v)
	{
		try
		{
			if ( v == null || v.isNull() )
				value = null;
			else
			{
				String vs = normalize(v.toPlainString());
				value = ( vs==null ) ? null : new BigDecimal( vs );
			}
		}
		catch( NumberFormatException e )
		{
			value = null;
		}
	}
	
	/**
	 * Change the value of our decimal using the specified string
	 * @param v the EsfString with an decimal string literal inside; if the string value is invalid, our object becomes null
	 */
	public void setValue(String v)
	{
		try
		{
			v = normalize(v);
			value = (v == null) ? null : new BigDecimal( v );
		}
		catch( NumberFormatException e )
		{
			value = null;
		}
	}
	
	/**
	 * Change the value of our decimal using the specified decimal
	 * @param v the EsfDecimal value to set our number to
	 */
	public void setValue(EsfDecimal v)
	{
		value = v.value;
	}
	

	/**
	 * Change the value of our decimal using the specified big decimal
	 * @param v the BigDecimal value to set our number to
	 */
	public void setValue(BigDecimal v)
	{
		value = v;
	}
	
	public void add(EsfValue v)
	{
		if ( v == null || v.isNull() )
			return;
		if ( isNull() )
			setValue( BigDecimal.ZERO );
		if ( v instanceof EsfInteger )
			setValue( value.add(((EsfInteger)v).toBigDecimal()) );
		else if ( v instanceof EsfDecimal )
			setValue( value.add(((EsfDecimal)v).toBigDecimal()) );
	}
	
	public void subtract(EsfValue v)
	{
		if ( v == null || v.isNull() )
			return;
		if ( isNull() )
			setValue( BigDecimal.ZERO );
		if ( v instanceof EsfInteger )
			setValue( value.subtract(((EsfInteger)v).toBigDecimal()) );
		else if ( v instanceof EsfDecimal )
			setValue( value.subtract(((EsfDecimal)v).toBigDecimal()) );
	}
	
	public void multiply(EsfValue v)
	{
		if ( v == null || v.isNull() )
			return;
		if ( isNull() )
			setValue( BigDecimal.ZERO );
		if ( v instanceof EsfInteger )
			setValue( value.multiply(((EsfInteger)v).toBigDecimal()) );
		else if ( v instanceof EsfDecimal )
			setValue( value.multiply(((EsfDecimal)v).toBigDecimal()) );
	}
	
	public void multiply(double v)
	{
		if ( isNull() )
			setValue( BigDecimal.ZERO );
		BigDecimal bdv = new BigDecimal(v);
		setValue( value.multiply(bdv) );
	}
	
	public void divide(EsfValue v)
	{
		if ( v == null || v.isNull() )
			return;
		try
		{
			if ( isNull() )
				setValue( BigDecimal.ZERO );
			if ( v instanceof EsfInteger )
			{
				MathContext mc = new MathContext(value.precision()+1,RoundingMode.HALF_UP);
				setValue( value.divide(((EsfInteger)v).toBigDecimal(),mc) );
			}
			else if ( v instanceof EsfDecimal )
			{
				BigDecimal rhs = ((EsfDecimal)v).toBigDecimal();
				int prec = Math.max(value.precision(), rhs.precision());
				MathContext mc = new MathContext(prec+1,RoundingMode.HALF_UP);
				setValue( value.divide(rhs,mc) );
			}
		}
		catch( ArithmeticException e ) {}
	}
	

	public void divide(double v)
	{
		try
		{
			if ( isNull() )
				setValue( BigDecimal.ZERO );
			
			BigDecimal bdv = new BigDecimal(v);
			int prec = value.precision();
			MathContext mc = new MathContext(prec+1,RoundingMode.HALF_UP);
			setValue( value.divide(bdv,mc) );
		}
		catch( ArithmeticException e ) {}
	}
	

	/**
	 * This will return a true if the underlying value is zero.
	 * @return a boolean true if the underlying value is zero
	 */
	public final boolean isZero()
	{
		return value != null && value.compareTo(BigDecimal.ZERO) == 0;
	}
	public final boolean isPositive()
	{
		return value != null && value.compareTo(BigDecimal.ZERO) > 0;
	}
	public final boolean isNegative()
	{
		return value != null && value.compareTo(BigDecimal.ZERO) < 0;
	}
	public final boolean equals(EsfDecimal v)
	{
		return ((value == null || value.equals(BigDecimal.ZERO)) && (v == null || v.value == null || v.value.equals(BigDecimal.ZERO))) || 
		        value != null && v != null && v.value != null && value.compareTo(v.value) == 0;
	}

	public final int compareTo(EsfDecimal v)
	{
		if ( value == null || value.equals(BigDecimal.ZERO) )
			return (v == null || v.value == null || v.value.equals(BigDecimal.ZERO)) ? 0 : -1;
		if ( v == null || v.value == null )
			return 1;
		return value.compareTo(v.value);
	}
	public final boolean isLessThan(EsfDecimal v)
	{
		if ( value == null || value.equals(BigDecimal.ZERO) )
			return (v == null || v.value == null || v.value.equals(BigDecimal.ZERO)) ? false : true;
		if ( v == null || v.value == null )
			return false;
		return value.compareTo(v.value) < 0;
	}
	public final boolean isLessThanEqualTo(EsfDecimal v)
	{
		if ( value == null )
			return (v == null || v.value == null || v.value.equals(BigDecimal.ZERO)) ? true : false;
		if ( v == null || v.value == null )
			return value.equals(BigDecimal.ZERO) ? true : false;
		return value.compareTo(v.value) <= 0;
	}
	public final boolean isGreaterThan(EsfDecimal v)
	{
		if ( value == null )
			return false;
		if ( v == null || v.value == null )
			return value.equals(BigDecimal.ZERO) ? false : true;
		return value.compareTo(v.value) > 0;
	}
	public final boolean isGreaterThanEqualTo(EsfDecimal v)
	{
		if ( value == null )
			return (v == null || v.value == null || v.value.equals(BigDecimal.ZERO)) ? true : false;
		if ( v == null || v.value == null )
			return true;
		return value.compareTo(v.value) >= 0;
	}
	
	/**
	 * Returns the decimal formated using the DecimalFormat specified.
	 * @param decimalFormat the String with a DecimalFormat specifier.  The standard symbols object is used. If null or blank, toString() is returned.
	 * @return the String formatted number.
	 */
    public final String format(String decimalFormat)
    {
    	if ( value == null )
    		return "";
    	if ( EsfString.isBlank(decimalFormat) )
    		return toString();
    		
    	DecimalFormat format = new DecimalFormat(decimalFormat,symbols);
    	return format.format(value);
    }

    public final String formatToHtml(String decimalFormat)
    {
    	if ( value == null )
    		return "";
    	if ( EsfString.isBlank(decimalFormat) )
    		return toHtml();
    		
    	DecimalFormat format = new DecimalFormat(decimalFormat,symbols);
    	return HtmlUtil.toEscapedHtml(format.format(value));
    }

    /**
     * Normalize the String decimal. If it returns null, it implies the decimal is not formatted in a way we can normalize it.
     * @param v
     * @return the normalized decimal or null if invalid; blank is considered to be 0.
     */
    public static String normalize(String v) 
    {
    	if ( v == null )
    		return null;

    	if ( EsfString.isBlank(v) )
    		return "0";
    	
    	v = v.trim();
    	
    	boolean hasPositiveNegativeSign = false;
    	boolean hasDigit = false;
    	boolean hasDecimal = false;
    	boolean hasPercent = false;
    	
        StringBuilder normal = new StringBuilder(v.length());
        for( int i=0; i < v.length(); ++i )
        {
            char c = v.charAt(i);
            
            if ( c == '+' )
            {
            	if ( hasPositiveNegativeSign || hasDigit || hasDecimal || hasPercent ) // can only have one and must precede any digits and decimal point
            		return null;
            	hasPositiveNegativeSign = true;
            	// swallow a leading positive sign
            }
            else if ( c == '-' )
            {
            	if ( hasPositiveNegativeSign || hasDigit || hasDecimal || hasPercent )// can only have one and must precede any digits and decimal point
            		return null;
            	hasPositiveNegativeSign = true;
            	normal.append(c);
            }
            else if ( c == ',' )
            {
            	if ( ! hasDigit ) // allow a comma, but only if we've seen at least one digit
            		return null;
            	if ( hasDecimal || hasPercent ) // if we have a decimal point, no more commas
            		return null;
            	// swallow it
            }
            else if ( c == ' ' )
            {
            	if ( hasDigit || hasDecimal || hasPercent )
            		return null;
            	// swallow it
            }
            else if ( c == '.' )
            {
            	if ( hasDecimal || hasPercent ) // can only have one decimal point
            		return null;
            	hasDecimal = true;
            	normal.append(c);
            }
            else if ( c == '%' )
            {
            	if ( hasPercent )
            		return null;
            	hasPercent = true;
            }
            else if ( Character.isDigit(c) )
            {
            	if ( hasPercent )
            		return null;
            	hasDigit = true;
            	normal.append(c);
            }
            else
            	return null;
        }

        return normal.toString();
    }
    
    public static EsfDecimal ensureNotNull(EsfDecimal v) 
    {
    	return v == null ? new EsfDecimal() : v;
    }

    public static void main(String[] args)
    {
    	/*Application app = */ Application.getInstallationInstance("/");
    	
    	EsfDecimal zero = new EsfDecimal("0");
    	EsfDecimal zeroPointZero = new EsfDecimal("0.0");
    	
    	if ( zero.equals(zeroPointZero) )
    		System.out.println("0 equals 0.0");
    	else
    		System.out.println("0 NOT equals 0.0");
    	
    	EsfDecimal v1 = new EsfDecimal("123.45");
    	System.out.println("v1: " + v1.toString() + " ; " + v1.toPlainString() + " ; " + v1.toPercentString());
    	
    	EsfDecimal v2 = new EsfDecimal("2,123.456");
    	System.out.println("v2: " + v2.toString() + " ; " + v2.toPlainString() + " ; " + v2.toPercentString());
    	
    	EsfDecimal v3 = new EsfDecimal("33,123.4567");
    	System.out.println("v3: " + v3.toString() + " ; " + v3.toPlainString() + " ; " + v3.toPercentString());
    	
    	EsfDecimal v4 = new EsfDecimal("444,123.456789");
    	System.out.println("v4: " + v4.toString() + " ; " + v4.toPlainString() + " ; " + v4.toPercentString());
    	
    	EsfDecimal v5 = new EsfDecimal("5,555,123.");
    	System.out.println("v5: " + v5.toString() + " ; " + v5.toPlainString() + " ; " + v5.toPercentString());
    	
    	EsfDecimal v6 = new EsfDecimal(".1234");
    	System.out.println("v6: " + v6.toString() + " ; " + v6.toPlainString() + " ; " + v6.toPercentString());
    	    	
    	EsfDecimal v7 = new EsfDecimal("10%");
    	System.out.println("v7: " + v7.toString() + " ; " + v7.toPlainString() + " ; " + v7.toPercentString());
    	
    	EsfDecimal v8 = new EsfDecimal("100.00%");
    	System.out.println("v8: " + v8.toString() + " ; " + v8.toPlainString() + " ; " + v8.toPercentString());
    	
    	EsfDecimal v9 = new EsfDecimal("98.45%");
    	System.out.println("v9: " + v9.toString() + " ; " + v9.toPlainString() + " ; " + v9.toPercentString());
    	
    	System.exit(0);
    }
}