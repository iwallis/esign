// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.data;

import com.esignforms.open.config.Literals;

/**
 * Defines an ESF Name field, which is basically a restricted string used to hold names of data elements
 * and other named items in the application.
 * 
 * It must be 1-50 characters, start with a letter, have no whitespace
 * and may contains letters, numbers and the underscore (_).  They are not case sensitive.
 * This set of restrictions should make the name compatible with Java, Javascript, XML and even 
 * PostgreSQL column and table names.
 * 
 * The name cannot be null.  If it is null, the name will be set to NULL_ESFNAME_IS_INVALID, and if
 * is otherwise not valid it will be set to ESFNAME_IS_INVALID.
 *
 * @author Yozons, Inc.
 */
public class EsfName
	implements EsfValue, Comparable<EsfName>
{
	private static final long serialVersionUID = -1272335208451013434L;

	public static final String TYPE 			= "esfname";
    
    public static final String ESF_RESERVED_PREFIX      = "ESF_";
    public static final String NULL_ESFNAME_IS_INVALID	= "invalid_missing_name";
    public static final String ESFNAME_IS_INVALID 		= "invalid_name_specified";

	private String value;
	
	public EsfName()
	{
		value = NULL_ESFNAME_IS_INVALID;
	}
	
	public EsfName(String v)
	{
		if ( v == null )
			value = NULL_ESFNAME_IS_INVALID;
		else
			value = isValidEsfName(v) ? v : ESFNAME_IS_INVALID;
	}
	
	public EsfName(EsfString v)
	{
		if ( v == null || v.isNull() )
			value = NULL_ESFNAME_IS_INVALID;
		else
		{
			String vString = v.toPlainString();
			value = isValidEsfName(vString) ? vString : ESFNAME_IS_INVALID;
		}
	}
	
	public EsfName(EsfName v)
	{
		value = v.value;
	}
	
	public static boolean isReservedPrefix(String v)
	{
		return v != null && v.toLowerCase().startsWith(ESF_RESERVED_PREFIX.toLowerCase());
	}
	
	public static boolean isValidEsfName(String v)
	{
		if ( v == null || EsfString.isBlank(v) )
			return false;
		
		if ( v.length() > Literals.ESFNAME_MAX_LENGTH )
			return false;
		
		// The first character must be a letter.
		if ( ! Character.isLetter(v.charAt(0)) )
			return false;
		
		for( int i=1; i < v.length(); ++i )
		{
			char c = v.charAt(i);
			if ( ! Character.isLetterOrDigit(c) && c != '_' )
				return false;
		}
		
		return true;
	}
	
	/**
	 * This will return a new instance using 'fromXml' as the source.  The 'fromXml'
	 * is stored in the name-value pairs using the corresponding toXml() method.
	 * @param fromXml the String that was stored in the XML for name-value pairs.
	 * @return a new value object.
	 * @see #toXml()
	 */
	public static EsfName createFromToXml(String fromXml)
	{
		return new EsfName( fromXml );
	}
	
	public static EsfName createFromRegularString(String fromString) 
	{
		StringBuilder normalizedString = new StringBuilder(fromString.length());
		if ( fromString.length() < 1 )
			normalizedString.append("X");
		else 
		{
			// The first character must be a letter.
			if ( ! Character.isLetter(fromString.charAt(0)) )
				normalizedString.append("X");
		}
		for( int i=0; i < fromString.length(); ++i )
		{
			char c = fromString.charAt(i);
			if ( c == '-' || c == '/' || c == '.' )
				normalizedString.append('_');
			else if ( Character.isLetterOrDigit(c) || c == '_' )
				normalizedString.append(c);
		}
		
		if ( normalizedString.length() > Literals.ESFNAME_MAX_LENGTH )
			normalizedString.setLength(Literals.ESFNAME_MAX_LENGTH);
		
		return new EsfName(normalizedString.toString());
	}
	
	/**
	 * This will return a string that represents this value's "simple type" rather than the class name, suitable for
	 * storing with the name-value data in XML.
	 * @return a String literal "esfname"
	 */
	@Override
	public String getType()
	{
		return TYPE;
	}

	/**
	 * This will return a true if the underlying value is null
	 * @return a boolean true if the underlying value is null
	 */
	@Override
	public boolean isNull()
	{
		return value == null || value.equals(NULL_ESFNAME_IS_INVALID);
	}

	/**
	 * This will return a duplicate of the value so that it has the same value, but is a distinct object.
	 * @return a new EsfValue that has the value as this object.
	 */
	@Override
	public EsfName duplicate()
	{
		return new EsfName( value );
	}
	
	/**
	 * Returns a one element array containing this value.
	 */
	@Override
	public EsfName[] toArray()
	{
		EsfName[] a = new EsfName[1];
		a[0] = this;
		return a;
	}
	
	/**
	 * Return the value as a "pretty" string, allowing for any adornment that makes sense
	 * for its underlying type.
	 * @return the value as pretty String
	 */
	@Override
    public final String toString()
    {
    	return toPlainString();
    }
    
	/**
	 * Return the value as a "plain" string, with no adornments, such as for storing in a DB
	 * @return the value as plain String; 
	 */
    @Override
    public final String toPlainString()
    {
    	return value;
    }

    /**
     * Return the value as a String suitable for HTML 
     * @return the value encoded as HTML
     */
    public final String toHtml()
    {
    	return toPlainString(); // already HTML safe
    }

    /**
     * Return the value as a String suitable for XML 
     * @return the value encoded as XML
     */
    @Override
    public final String toXml()
    {
    	return toPlainString(); // already XML safe
    }
    public final long getEstimatedLengthXml()
    {
    	return getLength();
    }
    

	/**
	 * Sets a new name if 'v' is valid.  Otherwise it does not change its existing value.
	 * @param v the new String name to use
	 */
	public void setEsfName(String v)
	{
		if ( isValidEsfName(v) )
			value = v;
	}
	
	public void setEsfName(EsfName v)
	{
		value = v.value;
	}
	
	public boolean hasReservedPrefix()
	{
		return isReservedPrefix(value);
	}

	/**
	 * Determines if the EsfName is valid.  It will be unless it's one of the invalid strings we set
	 * it to.
	 * @return true if the name is not NULL_ESFNAME_IS_INVALID or ESFNAME_IS_INVALID.
	 */
	public boolean isValid()
	{
		return ! isNull() && ! value.equals(ESFNAME_IS_INVALID);
	}
	
	public boolean equals(String other)
	{
		if ( value == null )
			return other == null;
		return value.equalsIgnoreCase(((String)other));
	}
	
	@Override
	public boolean equals(Object other)
	{
		if ( other instanceof EsfName )
			return (value == null && ((EsfName)other).value == null) || value.equalsIgnoreCase(((EsfName)other).value);
		return false;
	}
	
	@Override
	public int hashCode()
	{
		return value.toLowerCase().hashCode();
	}
	
	public int compareTo(EsfName other)
	{
		return value==null && other.value==null ? 0 : value.compareToIgnoreCase(other.value);
	}
	
	public String toLowerCase()
	{
		return value.toLowerCase();
	}
	
	public int getLength()
	{
		return isValid() ? value.length() : 0;
	}

    public static EsfName ensureNotNull(EsfName v) 
    {
    	return v == null ? new EsfName() : v;
    }

}