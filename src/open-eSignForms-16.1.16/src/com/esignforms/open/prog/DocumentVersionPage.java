// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfHtml;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DocumentVersion.DocumentImage;
import com.esignforms.open.user.User;
import com.esignforms.open.util.HtmlUtil;
import com.esignforms.open.util.XmlUtil;

/**
* DocumentVersionPage holds the definition of a page in a document version held in a library.  
* 
* @author Yozons, Inc.
*/
public class DocumentVersionPage
	extends com.esignforms.open.db.DatabaseObject
	implements java.lang.Comparable<DocumentVersionPage>, java.io.Serializable
{
	private static final long serialVersionUID = 4982958334322875282L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DocumentVersionPage.class);
	
	public static final String EDIT_REVIEW_TYPE_EDIT_ONLY = "E";
	public static final String EDIT_REVIEW_TYPE_REVIEW_ONLY = "R";
	public static final String EDIT_REVIEW_TYPE_EDIT_AND_REVIEW = "B";
    
    // We use this to find areas of the HTML that we ignore when it comes to pattern matching. We currently
	// ignore all regions between <% and %> characters. We use the non-greedy '.*?' so it doesn't suck up multiple JSP regions on the same line.
    private static java.util.regex.Pattern IgnoreRegionsPattern = java.util.regex.Pattern.compile("<%.*?%>");

    // Finds the words made up of A-Z, a-z, 0-9, '.' or '_' inside a braced field: ${}
    // The "." is a separator between EsfName components.  When used, the first part generally identifies another record/document to get the field, or a propertyset for a given property
	// We allow square brackets to be in there, too, to allow for an array subscript notation such as: ${test[3]} 
    // And we allow a single predefined "source type" expression to identify where the field path belongs, such as ${image:logo} will look for logo, not as a field, but as an image.
    private static java.util.regex.Pattern ExtractFieldPathStringFieldPattern = java.util.regex.Pattern.compile("\\$\\{(document:|html:|label:|image:|file:|field:|transaction:|out:|property:|htmlproperty:|fieldlabel:)?([A-Za-z][A-Za-z0-9_.\\[\\]]*)\\}");
    
    // The layout of this is /CONTEXT-PATH/files/FILE_ESFNAME/originalfilename.doc/?fid=FILEID as specified in the ckeditorFileBrowser.jsp that produces the links. Added non-greedy final '.*?' so it doesn't suck up subsequent tags, too.
    private static java.util.regex.Pattern ExtractFileAHtmlTagFieldPattern = java.util.regex.Pattern.compile("<a (.*?)href=\"[A-Za-z0-9/_-]*/files/([A-Za-z0-9_]{1,50})/.*\\?fid=([0-9a-f-]{36})\"(.*?)>(.*?)</a>");
  
    // The layout of this is /CONTEXT-PATH/images/IMAGE_ESFNAME/?iid=IMAGEID as specified in the ckeditorImageBrowser.jsp that produces the links. Added non-greedy final '.*?' so it doesn't suck up subsequent tags, too.
    private static java.util.regex.Pattern ExtractImgHtmlTagFieldPattern = java.util.regex.Pattern.compile("<img (.+(src=\"[A-Za-z0-9/_-]*/images/([A-Za-z0-9_]{1,50})/\\?iid=([0-9a-f-]{36})\").*?) />");

    protected EsfUUID id;
    protected EsfUUID documentVersionId;
    protected short pageNumber;
    protected EsfName esfname;
    protected String editReviewType;
    protected EsfHtml html;
    protected HashSet<EsfUUID> fieldTemplateIdsOnPage; // we only store fields that can be edited (not just labels or for output)
    
    protected EsfName defaultNewFieldParty; // not persisted; if set, when new fields are created, they are auto-assigned to this party
    
    /**
     * This creates a DocumentVersionPage object from data retrieved from the DB.
     */
    protected DocumentVersionPage(EsfUUID id, EsfName esfname, String editReviewType, EsfUUID documentVersionId, short pageNumber, EsfHtml html, HashSet<EsfUUID> fieldTemplateIdsOnPage)
    {
    	this.id = id;
    	this.esfname = esfname;
    	this.editReviewType = editReviewType;
        this.documentVersionId = documentVersionId;
        this.pageNumber = pageNumber;
        this.html = html;
        this.fieldTemplateIdsOnPage = fieldTemplateIdsOnPage;
    }
    
    protected DocumentVersionPage(DocumentVersion documentVersion, short pageNumber)
    {
    	this.id = new EsfUUID();
    	this.esfname = new EsfName("Page" + pageNumber);
    	this.editReviewType = EDIT_REVIEW_TYPE_EDIT_AND_REVIEW;
        this.documentVersionId = documentVersion.getId();
        this.pageNumber = pageNumber;
        setHtml(null);
        this.fieldTemplateIdsOnPage = new HashSet<EsfUUID>();
    }

    protected DocumentVersionPage(DocumentVersion documentVersion, DocumentVersionPage likeDocVersionPage)
    {
    	this.id = new EsfUUID();
    	this.esfname = likeDocVersionPage.getEsfName();
    	this.editReviewType = likeDocVersionPage.getEditReviewType();
        this.documentVersionId = documentVersion.getId();
        this.pageNumber = likeDocVersionPage.getPageNumber();
        this.html = likeDocVersionPage.getHtml();
        this.fieldTemplateIdsOnPage = likeDocVersionPage.getDuplicatedFieldTemplateIdsOnPage();
    }
    
    public DocumentVersionPage duplicate()
    {
    	DocumentVersionPage docVerPage = new DocumentVersionPage(id, esfname.duplicate(), editReviewType, documentVersionId, pageNumber, html.duplicate(), getDuplicatedFieldTemplateIdsOnPage());
    	docVerPage.setDatabaseObjectLike(this);
    	return docVerPage;
    }

    public EsfUUID getId()
    {
        return id;
    }

    public EsfUUID getDocumentVersionId()
    {
        return documentVersionId;
    }
    
    public DocumentVersion getDocumentVersion()
    {
    	return DocumentVersion.Manager.getById(documentVersionId);
    }
    
    public short getPageNumber()
    {
        return pageNumber;
    }
    public void setPageNumber(short v)
    {
    	if ( v != pageNumber )
    	{
    		pageNumber = v;
    		objectChanged();
    	}
    }
    
    public EsfName getEsfName()
    {
    	return esfname;
    }
    public void setEsfName(EsfName v)
    {
    	if ( v != null && v.isValid() )
    	{
    		esfname = v;
    		objectChanged();
    	}
    }
    
    public String getEditReviewType()
    {
    	return editReviewType;
    }
    public void setEditReviewType(String v)
    {
    	if ( EDIT_REVIEW_TYPE_EDIT_ONLY.equals(v) || EDIT_REVIEW_TYPE_REVIEW_ONLY.equals(v) )
    		editReviewType = v;
    	else
    		editReviewType = EDIT_REVIEW_TYPE_EDIT_AND_REVIEW;
    	objectChanged();
    }
    public static boolean isEditOnly(String v)
    {
    	return EDIT_REVIEW_TYPE_EDIT_ONLY.equals(v);
    }
    public boolean isEditOnly()
    {
    	return isEditOnly(editReviewType);
    }
    public static boolean isEdit(String v)
    {
    	return isEditOnly(v) || isEditAndReview(v);
    }
    public boolean isEdit()
    {
    	return isEdit(editReviewType);
    }
    public static boolean isReviewOnly(String v)
    {
    	return EDIT_REVIEW_TYPE_REVIEW_ONLY.equals(v);
    }
    public boolean isReviewOnly()
    {
    	return isReviewOnly(editReviewType);
    }
    public static boolean isReview(String v)
    {
    	return isReviewOnly(v) || isEditAndReview(v);
    }
    public boolean isReview()
    {
    	return isReview(editReviewType);
    }
    public static boolean isEditAndReview(String v)
    {
    	return EDIT_REVIEW_TYPE_EDIT_AND_REVIEW.equals(v);
    }
    public boolean isEditAndReview()
    {
    	return isEditAndReview(editReviewType);
    }
    
    public EsfHtml getHtml()
    {
        return html;
    }
    public void setHtml(EsfHtml v)
    {
    	if ( v == null || v.isBlank() )
    		html = new EsfHtml(Application.getInstance().getServerMessages().getString("documentVersionPage.new.initialCKEditorValue"));
    	else if ( v.getLength() > Literals.HTML_MAX_LENGTH )
            html = new EsfHtml(v.toPlainString().substring(0,Literals.HTML_MAX_LENGTH).trim());
        else
        	html = v; // we don't trim since CKEditor puts a newline at the end of every line
        objectChanged();
    }
    
    public HashSet<EsfUUID> getFieldTemplateIdsOnPage()
    {
    	return fieldTemplateIdsOnPage;
    }
    public HashSet<EsfUUID> getDuplicatedFieldTemplateIdsOnPage()
    {
    	return new HashSet<EsfUUID>(fieldTemplateIdsOnPage);
    }
    public void resetFieldTemplateIdsOnPage() 
    {
    	fieldTemplateIdsOnPage.clear();
    	objectChanged();
    }
    public void addFieldTemplateOnPage(DocumentVersion docVer, FieldTemplate fieldTemplate)
    {
    	addFieldTemplateIdOnPage(fieldTemplate.getId());
		if ( fieldTemplate.isTypeRadioButton() ) // if it's a radio button, register the radio button group if not already done so
		{
			FieldTemplate radioButtonGroupTemplate = docVer.getFieldTemplate(new EsfName(fieldTemplate.getOutputFormatSpec()));
			if ( radioButtonGroupTemplate != null && radioButtonGroupTemplate.isTypeRadioButtonGroup() && ! hasFieldTemplateIdOnPage(radioButtonGroupTemplate.getId()) )
    			addFieldTemplateIdOnPage(radioButtonGroupTemplate.getId());
		}
    }
    public void addFieldTemplateIdOnPage(EsfUUID fieldTemplateId)
    {
    	if ( ! fieldTemplateIdsOnPage.contains(fieldTemplateId) )
    	{
        	fieldTemplateIdsOnPage.add(fieldTemplateId);
        	objectChanged();
    	}
    }
    public boolean hasFieldTemplateIdOnPage(EsfUUID fieldTemplateId)
    {
    	return fieldTemplateIdsOnPage.contains(fieldTemplateId);
    }
    
    public EsfName getDefaultNewFieldParty()
    {
    	return defaultNewFieldParty;
    }
    public boolean hasDefaultNewFieldParty()
    {
    	return defaultNewFieldParty != null && defaultNewFieldParty.isValid();
    }
    public void setDefaultNewFieldParty(EsfName v)
    {
    	defaultNewFieldParty = v; // not persisted, no need for objectChanged()
    }
    
    /**
     * This routine returns not the HTML the user entered, but the HTML converted to JSP for our generated code.
     * It is also responsible for determining all referenced fields and such.
     * @return the EsfHtml that represents the JSP version of the HTML entered by the user.
     */
    public EsfHtml getGeneratedJSP(DocumentVersion docVer)
    {
    	resetFieldTemplateIdsOnPage();
    	String processedHtml = getGeneratedJSPForFieldPaths(docVer,html.toPlainString());
    	processedHtml = getGeneratedJSPForImages(processedHtml);
    	processedHtml = getGeneratedJSPForFiles(processedHtml);
    	
    	return new EsfHtml(processedHtml);
    }
    
    private String getGeneratedJSPForFiles(String htmlToParse)
    {
        Matcher m = ExtractFileAHtmlTagFieldPattern.matcher(htmlToParse);
        if ( m == null )
            return htmlToParse;
        
        StringBuffer buf = null;
        while( m.find() ) 
        {
        	String matchedText = m.group(0);
            String preHrefAttrs = m.group(1);
            String fileName = m.group(2);
            String fileId = m.group(3);
            String postHrefAttrs = m.group(4);
            String linkText = m.group(5);
            
            String jspTag = makeJspTagForFileAHtmlTag(matchedText,preHrefAttrs,fileName,fileId,postHrefAttrs,linkText);
       	 	if ( buf == null )
       	 		buf = new StringBuffer(htmlToParse.length());
            m.appendReplacement(buf, Matcher.quoteReplacement(jspTag));
        }
   	 	if ( buf == null )
   	 		return htmlToParse;
   	 
   	 	m.appendTail(buf);
   	 	return buf.toString();
    }
    
    private String getGeneratedJSPForImages(String htmlToParse)
    {
        Matcher m = ExtractImgHtmlTagFieldPattern.matcher(htmlToParse);
        if ( m == null )
            return htmlToParse;
        
        StringBuffer buf = null;
        while( m.find() ) 
        {
        	String matchedText = m.group(0);
            String allTags = m.group(1);
            String srcTag = m.group(2);
            String imageName = m.group(3);
            String imageId = m.group(4);
            
            String jspTag = makeJspTagForImgHtmlTag(matchedText,allTags,srcTag,imageName,imageId);
       	 	if ( buf == null )
       	 		buf = new StringBuffer(htmlToParse.length());
            m.appendReplacement(buf, Matcher.quoteReplacement(jspTag));
        }
   	 	if ( buf == null )
   	 		return htmlToParse;
   	 
   	 	m.appendTail(buf);
   	 	return buf.toString();
    }
    
    private boolean isWithinIgnoreRegion(List<IgnoreRegion> ignoreRegionList, int start, int end)
    {
    	for( IgnoreRegion ir : ignoreRegionList )
    	{
    		if ( start >= ir.start && end <= ir.end )
    			return true;
    	}
    	return false;
    }
    private List<IgnoreRegion> createIgnoreRegionList(String htmlToParse)
    {
    	List<IgnoreRegion> ignoreRegionList = null;
    	
        Matcher m = IgnoreRegionsPattern.matcher(htmlToParse);
        if ( m == null )
            return ignoreRegionList;
        
        while( m.find() ) 
        {
        	IgnoreRegion ir = new IgnoreRegion(m.start(),m.end());
        	if ( ignoreRegionList == null )
        		ignoreRegionList = new LinkedList<IgnoreRegion>();
        	ignoreRegionList.add(ir);
        }
        
   	 	return ignoreRegionList;
    }
    class IgnoreRegion
    {
    	IgnoreRegion(int s, int e) 
    	{
    		start = s;
    		end = e;
    	}
    	int start;
    	int end;
    }
    
    private String getGeneratedJSPForFieldPaths(DocumentVersion docVer, String htmlToParse)
    {
    	List<IgnoreRegion> ignoreRegionList = createIgnoreRegionList(htmlToParse);
    	
        Matcher m = ExtractFieldPathStringFieldPattern.matcher(htmlToParse);
        if ( m == null )
            return htmlToParse;
        
        StringBuffer buf = null;
        while( m.find() ) 
        {
        	// We don't do fields that are within our ignore regions list (basically if inside JSP code <% %> segments).
        	if ( ignoreRegionList != null && isWithinIgnoreRegion(ignoreRegionList, m.start(), m.end()) )
        		continue;
        	
        	String matchedText = m.group(0);
            String sourceType = m.group(1);
            String fieldPath = m.group(2);
            
            if ( EsfString.isBlank(sourceType) )
            	sourceType = Literals.FIELD_SOURCE_TYPE_DEFAULT;
            else // strip the colon at the end of our matched text that includes one
            	sourceType = sourceType.substring(0, sourceType.length()-1);
            
            String jspTag = makeJspTagForFieldPath(docVer,matchedText,sourceType,fieldPath);
            if ( jspTag != null )
            {
           	 	if ( buf == null )
           	 		buf = new StringBuffer(htmlToParse.length());
                m.appendReplacement(buf, Matcher.quoteReplacement(jspTag));
            }
        }
   	 	if ( buf == null )
   	 		return htmlToParse;
   	 
   	 	m.appendTail(buf);
   	 	return buf.toString();
    }
    
    // fieldPath is one or more fields, such as:
    // ssn   -- simple fieldlabel named 'ssn' defined in the document so it shows the field and label
    // fieldlabel:ssn -- same as above, since "fieldlabel:" is assumed if no source type specifier is used.
    // field:ssn -- displays the field for 'ssn' without a label
    // option[2] -- field named 'option' that is an array and we'll get the element at index 2
    // ....or a name belonging to the specified field type specified in the first token
    // label:ssn -- the label defined in the simple field named 'ssn' defined in the document
    // out:ssn -- displays the field for 'ssn' without a label and in output/display mode only (no input)
    // document:esfname -- document is special in that all parts are read-only fields regarding the document, including the active party
    // html:logoArea -- an HTML snippet named 'logoArea'
    // file:fileName -- displays a link to access an uploaded File named 'fileName'
    // image:logo -- an image file named 'logo'
    // ...or if it's not a predefined field type, we assume it's a document name, and if not it's a data record name, in the package.
    // I9.ssn -- a field named 'ssn' in a document named 'I9'
    // label:I9.ssn -- the label defined in the field named 'ssn' in a document named 'I9'
    // recordName.ssn -- a field named 'ssn' in a data record named 'recordName' in the package
    private String makeJspTagForFieldPath(DocumentVersion docVer, String matchedText, String sourceType, String fieldPath)
    {
        StringTokenizer st = new StringTokenizer(fieldPath,".");
        
        String fieldName;
        String fieldSource = (st.hasMoreTokens() ) ? st.nextToken() : null;
        if ( fieldSource == null )
        	return null;
        boolean fieldSourceSpecified;
        if ( st.hasMoreTokens() )
        {
        	fieldSourceSpecified = true;
        	fieldName = st.nextToken();
        }
        else
        {
        	fieldName = fieldSource; // no source specified, so it's actually the field name, and our source is the current document
        	fieldSourceSpecified = false;
        	fieldSource = Literals.FIELD_SOURCE_TYPE_DOCUMENT;
        }

    	if ( Literals.FIELD_SOURCE_TYPE_FIELD_LABEL.equals(sourceType) )
        {
    		FieldTemplate ft = docVer.registerField(fieldSource,fieldName,hasDefaultNewFieldParty() ? getDefaultNewFieldParty() : null);
    		if ( ft != null )
    			addFieldTemplateOnPage(docVer,ft);
        	return "<esf:fieldlabel name=\"" + HtmlUtil.toEscapedHtml(fieldName) + "\" source=\"" + HtmlUtil.toEscapedHtml(fieldSource) + "\" fieldSpec=\"" + HtmlUtil.toEscapedHtml(matchedText) +"\"/>";
        }
        if ( Literals.FIELD_SOURCE_TYPE_FIELD_OUT.equals(sourceType) )
        {
        	docVer.registerField(fieldSource,fieldName,hasDefaultNewFieldParty() ? getDefaultNewFieldParty() : null);
    		return "<esf:fieldout name=\"" + HtmlUtil.toEscapedHtml(fieldName) + "\" source=\"" + HtmlUtil.toEscapedHtml(fieldSource) + "\" fieldSpec=\"" + HtmlUtil.toEscapedHtml(matchedText) +"\"/>";
        }
        if ( Literals.FIELD_SOURCE_TYPE_FIELD.equals(sourceType) )
        {
        	FieldTemplate ft = docVer.registerField(fieldSource,fieldName,hasDefaultNewFieldParty() ? getDefaultNewFieldParty() : null);
        	if ( ft != null )
        		addFieldTemplateOnPage(docVer,ft);
        	return "<esf:field name=\"" + HtmlUtil.toEscapedHtml(fieldName) + "\" source=\"" + HtmlUtil.toEscapedHtml(fieldSource) + "\" fieldSpec=\"" + HtmlUtil.toEscapedHtml(matchedText) +"\"/>";
        }
        if ( Literals.FIELD_SOURCE_TYPE_LABEL.equals(sourceType) )
        {
        	docVer.registerField(fieldSource,fieldName,hasDefaultNewFieldParty() ? getDefaultNewFieldParty() : null);
        	return "<esf:labelout name=\"" + HtmlUtil.toEscapedHtml(fieldName) + "\" source=\"" + HtmlUtil.toEscapedHtml(fieldSource) + "\" fieldSpec=\"" + HtmlUtil.toEscapedHtml(matchedText) +"\"/>";
        }
    	if ( Literals.FIELD_SOURCE_TYPE_PROPERTY.equals(sourceType) )
        {
    		if ( ! fieldSourceSpecified ) // we only assume "document" for a property if it was specified
    			fieldSource = "";
        	return "<esf:propertyout name=\"" + HtmlUtil.toEscapedHtml(fieldName) + "\" propertySet=\"" + HtmlUtil.toEscapedHtml(fieldSource) + "\" fieldSpec=\"" + HtmlUtil.toEscapedHtml(matchedText) +"\"/>";
        }
    	if ( Literals.FIELD_SOURCE_TYPE_HTML_PROPERTY.equals(sourceType) )
        {
    		if ( ! fieldSourceSpecified ) // we only assume "document" for a property if it was specified
    			fieldSource = "";
        	return "<esf:propertyout name=\"" + HtmlUtil.toEscapedHtml(fieldName) + "\" propertySet=\"" + HtmlUtil.toEscapedHtml(fieldSource) + "\" fieldSpec=\"" + HtmlUtil.toEscapedHtml(matchedText) +"\" toHtml=\"false\"/>";
        }
        if ( Literals.FIELD_SOURCE_TYPE_HTML.equals(sourceType) )
        {
        	return "<esf:htmlout name=\"" + HtmlUtil.toEscapedHtml(fieldPath) + "\" fieldSpec=\"" + HtmlUtil.toEscapedHtml(matchedText) + "\"/>";   
        }
        if ( Literals.FIELD_SOURCE_TYPE_IMAGE.equals(sourceType) )
        {
        	// Register document image overlay fields as being on this page
        	DocumentImage di = docVer.getDocumentImage( new EsfName(fieldName) );
    		if ( di != null && di.imageVersion.hasOverlayFields() ) {
    			for( ImageVersionOverlayField ivof : di.imageVersion.getOverlayFields() ) {
    				FieldTemplate ft = docVer.getFieldTemplate(ivof.getFieldTemplateId());
    				if ( ft != null )
    					addFieldTemplateOnPage(docVer,ft);
    			}
    		}

        	return "<esf:imageout name=\"" + HtmlUtil.toEscapedHtml(fieldPath) + "\" fieldSpec=\"" + HtmlUtil.toEscapedHtml(matchedText) + "\"/>";
        }
        if ( Literals.FIELD_SOURCE_TYPE_FILE.equals(sourceType) )
        {
        	return "<esf:fileout name=\"" + HtmlUtil.toEscapedHtml(fieldPath) + "\" fieldSpec=\"" + HtmlUtil.toEscapedHtml(matchedText) + "\"/>";
        }
        if ( Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(sourceType) )
        {
        	return "<esf:documentout name=\"" + HtmlUtil.toEscapedHtml(fieldPath) + "\" fieldSpec=\"" + HtmlUtil.toEscapedHtml(matchedText) + "\"/>";
        }
        if ( Literals.FIELD_SOURCE_TYPE_TRANSACTION.equals(sourceType) )
        {
        	return "<esf:transactionout name=\"" + HtmlUtil.toEscapedHtml(fieldPath) + "\" fieldSpec=\"" + HtmlUtil.toEscapedHtml(matchedText) + "\"/>";
        }

    	_logger.error("makeJspTagForFieldPath() - Unexpected sourceType: " + sourceType + "; fieldPath: " + fieldPath + "; in document version page id: " + getId() + "; matchedText: " + matchedText);
    	return null;
    }
    
    private String makeJspTagForFileAHtmlTag(String matchedText, String preHrefAttrs, String fileName, String fileId, String postHrefAttrs, String linkText)
    {
    	return "<esf:fileout name=\"" + HtmlUtil.toEscapedHtml(fileName) + "\" fileId=\"" + fileId + "\" linkText=\"" + HtmlUtil.toEscapedHtml(linkText) + "\" tagExtras=\"" + HtmlUtil.toEscapedHtml(preHrefAttrs + " " + postHrefAttrs) + "\" fieldSpec=\"" + HtmlUtil.toEscapedHtml(matchedText) + "\"/>";
    }
    
    private String makeJspTagForImgHtmlTag(String matchedText, String allTags, String srcTag, String imageName, String imageId)
    {
    	int srcTagBeginIndex = allTags.indexOf(srcTag);
    	int srcTagEndIndex = srcTagBeginIndex + srcTag.length();
    	String allTagsWithoutSrc = allTags.substring(0,srcTagBeginIndex) + allTags.substring(srcTagEndIndex);
    	
    	// Register document image overlay fields as being on this page
    	DocumentVersion docVer = getDocumentVersion();
    	DocumentImage di = docVer == null ? null : docVer.getDocumentImage( new EsfName(imageName) );
		if ( di != null && di.imageVersion.hasOverlayFields() ) {
			for( ImageVersionOverlayField ivof : di.imageVersion.getOverlayFields() ) {
				FieldTemplate ft = docVer.getFieldTemplate(ivof.getFieldTemplateId());
				if ( ft != null )
					addFieldTemplateOnPage(docVer,ft);
			}
		}
  	
    	return "<esf:imageout name=\"" + HtmlUtil.toEscapedHtml(imageName) + "\" imageId=\"" + imageId + "\" tagExtras=\"" + HtmlUtil.toEscapedHtml(allTagsWithoutSrc) + "\" fieldSpec=\"" + HtmlUtil.toEscapedHtml(matchedText) + "\"/>";
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o instanceof DocumentVersionPage )
        {
        	DocumentVersionPage otherPage = (DocumentVersionPage)o;
            return getId().equals(otherPage.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(DocumentVersionPage p)
    {
    	if ( getDocumentVersionId().equals(p.getDocumentVersionId()) )
    		return getPageNumber() - p.getPageNumber();
    	return getDocumentVersionId().compareTo(p.getDocumentVersionId());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<DocumentVersionPage xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <documentVersionId>").append(documentVersionId.toXml()).append("</documentVersionId>\n");
        buf.append(" <pageNumber>").append(pageNumber).append("</pageNumber>\n");
        buf.append(" <esfname>").append(esfname.toXml()).append("</esfname>\n");
        buf.append(" <editReviewType>").append(editReviewType).append("</editReviewType>\n");
        buf.append(" <html>").append(html.toXml()).append("</html>\n");
        for( EsfUUID fieldTemplateId : fieldTemplateIdsOnPage )
            buf.append(" <fieldTemplateId>").append(fieldTemplateId.toXml()).append("</fieldTemplateId>\n");
        buf.append("</DocumentVersionPage>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 205 + id.getEstimatedLengthXml() + documentVersionId.getEstimatedLengthXml() + esfname.getEstimatedLengthXml() + editReviewType.length() + html.getEstimatedLengthXml();
        len += fieldTemplateIdsOnPage.size() * (EsfUUID.DISPLAY_LENGTH + 37);
        return len; 
    }


    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
    // This is our special save routine when we are rewriting all of the pages after we delete them, so this
    // will always use an INSERT to save, never an UPDATE.
	public synchronized boolean saveOnRewrite(final Connection con, final User user)
	    throws SQLException
	{
		doInsert = true;
		return save(con,user);
	}
	
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; esfname: " + esfname + "; documentVersionId: " + documentVersionId + "; pageNumber: " + pageNumber + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	// Save all fields for edit on this page
        	Manager.deleteAllFieldTemplateIdsOnPage(con, id);
        	if ( fieldTemplateIdsOnPage.size() > 0 )
        	{
            	stmt = new EsfPreparedStatement(con, "INSERT INTO esf_library_document_version_page_field_template(library_document_version_page_id,field_template_id) VALUES (?,?)");
        		stmt.set(id);
        		for( EsfUUID fieldTemplateId : fieldTemplateIdsOnPage )
        		{
        			stmt.set(2, fieldTemplateId);
        			stmt.executeUpdate();
        		}
        		stmt.close();
        	}

        	if ( doInsert() )
            {
            	
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_document_version_page (id,library_document_version_id,page_number,esfname,edit_review_type,html) VALUES (?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(documentVersionId);
                stmt.set(pageNumber);
                stmt.set(esfname);
                stmt.set(editReviewType);
                stmt.set(html);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; esfname: " + esfname + "; documentVersionId: " + documentVersionId + "; pageNumber: " + pageNumber);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                
                return true;
            }
            
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_document_version_page SET esfname=?, edit_review_type=?, html=? WHERE id=?"
                						   	   );
                stmt.set(esfname);
                stmt.set(editReviewType);
                stmt.set(html);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for document page id: " + id + "; esfname: " + esfname + "; documentVersionId: " + documentVersionId + "; pageNumber: " + pageNumber);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; esfname: " + esfname + "; documentVersionId: " + documentVersionId + "; pageNumber: " + pageNumber + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; esfname: " + esfname + "; documentVersionId: " + documentVersionId + "; pageNumber: " + pageNumber + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of document version page that was pending an INSERT id: " + id + "; esfname: " + esfname + "; documentVersionId: " + documentVersionId + "; pageNumber: " + pageNumber);
            objectDeleted();
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
        	// Delete all fields in the page
        	Manager.deleteAllFieldTemplateIdsOnPage(con, id);

        	// Delete the document version page
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_document_version_page WHERE id=?");
            stmt.set(id);
            int num = stmt.executeUpdate();
            if ( num == 0 )
            	_logger.warn("delete(con) - Failed to find esf_library_document_version_page database row to delete with id: " + id + "; esfname: " + esfname + "; documentVersionId: " + documentVersionId + "; pageNumber: " + pageNumber);
            stmt.close();
            
            objectDeleted();
            
            return true;
        }
        catch(SQLException e)
        {
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"delete() on id: " + id + "; esfname: " + esfname + "; documentVersionId: " + documentVersionId + "; pageNumber: " + pageNumber + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   		static List<DocumentVersionPage> getAllPagesByDocumentVersion(EsfUUID documentVersionId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<DocumentVersionPage> pageList = new LinkedList<DocumentVersionPage>();
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT id,page_number,esfname,edit_review_type,html " +
   	        			"FROM esf_library_document_version_page WHERE library_document_version_id = ? ORDER BY page_number"
   	        									);
   	        	stmt.set(documentVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            short pageNumber = rs.getShort();
		            EsfName esfname = rs.getEsfName();
		            String editReviewType = rs.getString();
		            EsfHtml html = new EsfHtml(rs.getString());
		            
		            HashSet<EsfUUID> fieldTemplateIds = getAllFieldTemplateIdsOnPage(con, id);
		            
		            DocumentVersionPage docPage = new DocumentVersionPage(id,esfname,editReviewType,documentVersionId,pageNumber,html,fieldTemplateIds);
		            docPage.setLoadedFromDb();
		            pageList.add(docPage);
	            }
	            
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAllPagesByDocumentVersion() - documentVersionId: " + documentVersionId);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        _logger.debug("Manager.getAllPagesByDocumentVersion() - document version documentVersionId: " + documentVersionId + "; numPages: " + pageList.size());
   	        return pageList; 
   		}

   		public static DocumentVersionPage getById(EsfUUID id)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT library_document_version_id,page_number,esfname,edit_review_type,html " +
   	        			"FROM esf_library_document_version_page WHERE id = ?"
   	        									);
   	        	stmt.set(id);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID docVersionId = rs.getEsfUUID();
		            short pageNumber = rs.getShort();
		            EsfName esfname = rs.getEsfName();
		            String editReviewType = rs.getString();
		            EsfHtml html = new EsfHtml(rs.getString());
		            
		            HashSet<EsfUUID> fieldTemplateIds = getAllFieldTemplateIdsOnPage(con, id);
		            
		            DocumentVersionPage docPage = new DocumentVersionPage(id,esfname,editReviewType,docVersionId,pageNumber,html,fieldTemplateIds);
		            docPage.setLoadedFromDb();
		            con.commit();
		            return docPage;
	            }
	            
	            con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - docVersionPageId: " + id);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        _logger.debug("Manager.getById() - document version page docVersionPageId: " + id);
   	        return null; 
   		}

   		static HashSet<EsfUUID> getAllFieldTemplateIdsOnPage(final Connection con, EsfUUID documentVersionPageId)
			throws SQLException
   		{
	        EsfPreparedStatement stmt = null;
	        try
	        {
	        	HashSet<EsfUUID> set = new HashSet<EsfUUID>();
	        	stmt = new EsfPreparedStatement(con, "SELECT field_template_id FROM esf_library_document_version_page_field_template WHERE library_document_version_page_id=?");
	        	stmt.set(documentVersionPageId);
	        	EsfResultSet rs = stmt.executeQuery();
	        	while( rs.next() )
	        	{
	        		EsfUUID fieldTemplateId = rs.getEsfUUID();
	        		set.add(fieldTemplateId);
	        	}
	        	return set;
	        }
	        finally
	        {
	        	cleanupStatement(stmt);
	        }
   		}
   		
   		static int deleteAllFieldTemplateIdsOnPage(final Connection con, EsfUUID documentVersionPageId)
			throws SQLException
		{
	        EsfPreparedStatement stmt = null;
	        try
	        {
	        	stmt = new EsfPreparedStatement(con, "DELETE FROM esf_library_document_version_page_field_template WHERE library_document_version_page_id=?");
	        	stmt.set(documentVersionPageId);
	        	return stmt.executeUpdate();
	        }
	        finally
	        {
	        	cleanupStatement(stmt);
	        }
		}
		
   		public static int deleteAllPages(final Connection con, EsfUUID documentVersionId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
	        	stmt = new EsfPreparedStatement(con, "DELETE FROM esf_library_document_version_page_field_template WHERE library_document_version_page_id IN (SELECT id FROM esf_library_document_version_page WHERE library_document_version_id=?)");
	        	stmt.set(documentVersionId);
	        	stmt.executeUpdate();
	        	stmt.close();

	        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_document_version_page WHERE library_document_version_id=?");
   	            stmt.set(documentVersionId);
   	            return stmt.executeUpdate();
   	        }
   	        finally
   	        {
   	        	cleanupStatement(stmt);
   	        }
   		}
   		
   		public static DocumentVersionPage createNew(DocumentVersion documentVersion, short pageNumber)
   		{
   	    	if ( documentVersion == null || pageNumber < 1 )
   	    		return null;
   	    	return new DocumentVersionPage(documentVersion,pageNumber);
   			
   		}
   		
   		public static DocumentVersionPage createLike(DocumentVersion documentVersion, DocumentVersionPage likeDocVersionPage)
   		{
   	    	if ( documentVersion == null || likeDocVersionPage == null )
   	    		return null;
   	    	return new DocumentVersionPage(documentVersion,likeDocVersionPage);
   		}
   		
   	    public static DocumentVersionPage createFromJDOM(EsfUUID documentVersionId, Element e)
   	    {
   	    	if ( documentVersionId == null || documentVersionId.isNull() || e == null )
   	    		return null;
   	    	
   	    	EsfUUID id = new EsfUUID();
   	    	
            HashSet<EsfUUID> fieldTemplateIds = new HashSet<EsfUUID>();
            for( Element fieldTemplateIdElement : e.getChildren("fieldTemplateId", e.getNamespace()) )
            	fieldTemplateIds.add( EsfUUID.createFromToXml(fieldTemplateIdElement.getTextTrim()) );
   	    	
   	    	DocumentVersionPage newDocVerPage = new DocumentVersionPage(
   	    			id, 
   	    			EsfName.createFromToXml(e.getChildTextTrim("esfname", e.getNamespace())),
   	    			e.getChildTextTrim("editReviewType", e.getNamespace()),
   	    			documentVersionId,
   	    			Application.getInstance().stringToShort(e.getChildTextTrim("pageNumber", e.getNamespace()), (short)0),
   	    			EsfHtml.createFromToXml(e.getChildTextTrim("html", e.getNamespace())),
   	    			fieldTemplateIds
   	    			);
   	    	newDocVerPage.setNewObject();
   	    	return newDocVerPage;
   	    }
   	    
   	} // Manager
    
}