// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.Record;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.user.User;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* DocumentStyleVersion holds the definition of a specific version of a document style held in a library.
* A document style has one or more choices.
* 
* @author Yozons, Inc.
*/
public class DocumentStyleVersion
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<DocumentStyleVersion>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable
{
	private static final long serialVersionUID = -2608197013368654414L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DocumentStyleVersion.class);
    
    private static final String CSS_FILE_SUFFIX = 	Application.NOCACHE_FILENAME + ".css";

    private static final EsfString INHERIT = new EsfString("inherit");
    private static final EsfName DOCUMENT_FONT_ESFNAME = new EsfName("DocumentFont");
    private static final EsfName DOCUMENT_FONT_SIZE_ESFNAME = new EsfName("DocumentFontSize");
    private static final EsfName DOCUMENT_FONT_STYLE_ESFNAME = new EsfName("DocumentFontStyle");
    private static final EsfName DOCUMENT_FONT_COLOR_ESFNAME = new EsfName("DocumentFontColor");
    private static final EsfName DOCUMENT_BACKGROUND_COLOR_ESFNAME = new EsfName("DocumentBackgroundColor");

    private static final EsfName SIGNATURE_FONT_ESFNAME = new EsfName("SignatureFont");
    private static final EsfName SIGNATURE_FONT_SIZE_ESFNAME = new EsfName("SignatureFontSize");
    private static final EsfName SIGNATURE_FONT_STYLE_ESFNAME = new EsfName("SignatureFontStyle");
    private static final EsfName SIGNATURE_FONT_COLOR_ESFNAME = new EsfName("SignatureFontColor");
    private static final EsfName SIGNATURE_BACKGROUND_COLOR_ESFNAME = new EsfName("SignatureBackgroundColor");
    
    private static final EsfName FIELD_DATA_FONT_ESFNAME = new EsfName("FieldDataFont");
    private static final EsfName FIELD_DATA_FONT_SIZE_ESFNAME = new EsfName("FieldDataFontSize");
    private static final EsfName FIELD_DATA_FONT_STYLE_ESFNAME = new EsfName("FieldDataFontStyle");
    private static final EsfName FIELD_DATA_FONT_COLOR_ESFNAME = new EsfName("FieldDataFontColor");
    private static final EsfName FIELD_DATA_BACKGROUND_COLOR_ESFNAME = new EsfName("FieldDataBackgroundColor");
    
    private static final EsfName INPUT_FIELD_ERROR_FONT_ESFNAME = new EsfName("InputFieldErrorFont");
    private static final EsfName INPUT_FIELD_ERROR_FONT_SIZE_ESFNAME = new EsfName("InputFieldErrorFontSize");
    private static final EsfName INPUT_FIELD_ERROR_FONT_STYLE_ESFNAME = new EsfName("InputFieldErrorFontStyle");
    private static final EsfName INPUT_FIELD_ERROR_FONT_COLOR_ESFNAME = new EsfName("InputFieldErrorFontColor");
    private static final EsfName INPUT_FIELD_ERROR_BACKGROUND_COLOR_ESFNAME = new EsfName("InputFieldErrorBackgroundColor");
    private static final EsfName INPUT_FIELD_ERROR_BORDER_TYPES_ESFNAME = new EsfName("InputFieldErrorBorderTypes");
    
    private static final EsfName INPUT_FIELD_REQUIRED_FONT_ESFNAME = new EsfName("InputFieldRequiredFont");
    private static final EsfName INPUT_FIELD_REQUIRED_FONT_SIZE_ESFNAME = new EsfName("InputFieldRequiredFontSize");
    private static final EsfName INPUT_FIELD_REQUIRED_FONT_STYLE_ESFNAME = new EsfName("InputFieldRequiredFontStyle");
    private static final EsfName INPUT_FIELD_REQUIRED_FONT_COLOR_ESFNAME = new EsfName("InputFieldRequiredFontColor");
    private static final EsfName INPUT_FIELD_REQUIRED_BACKGROUND_COLOR_ESFNAME = new EsfName("InputFieldRequiredBackgroundColor");
    private static final EsfName INPUT_FIELD_REQUIRED_BORDER_TYPES_ESFNAME = new EsfName("InputFieldRequiredBorderTypes");
    
    private static final EsfName INPUT_FIELD_OPTIONAL_FONT_ESFNAME = new EsfName("InputFieldOptionalFont");
    private static final EsfName INPUT_FIELD_OPTIONAL_FONT_SIZE_ESFNAME = new EsfName("InputFieldOptionalFontSize");
    private static final EsfName INPUT_FIELD_OPTIONAL_FONT_STYLE_ESFNAME = new EsfName("InputFieldOptionalFontStyle");
    private static final EsfName INPUT_FIELD_OPTIONAL_FONT_COLOR_ESFNAME = new EsfName("InputFieldOptionalFontColor");
    private static final EsfName INPUT_FIELD_OPTIONAL_BACKGROUND_COLOR_ESFNAME = new EsfName("InputFieldOptionalBackgroundColor");
    private static final EsfName INPUT_FIELD_OPTIONAL_BORDER_TYPES_ESFNAME = new EsfName("InputFieldOptionalBorderTypes");
    
    private static final EsfName NORMAL_LABEL_FONT_ESFNAME = new EsfName("NormalLabelFont");
    private static final EsfName NORMAL_LABEL_FONT_SIZE_ESFNAME = new EsfName("NormalLabelFontSize");
    private static final EsfName NORMAL_LABEL_FONT_STYLE_ESFNAME = new EsfName("NormalLabelFontStyle");
    private static final EsfName NORMAL_LABEL_FONT_COLOR_ESFNAME = new EsfName("NormalLabelFontColor");
    private static final EsfName NORMAL_LABEL_BACKGROUND_COLOR_ESFNAME = new EsfName("NormalLabelBackgroundColor");
    
    private static final EsfName NORMAL_LABEL_ERROR_FONT_ESFNAME = new EsfName("NormalLabelErrorFont");
    private static final EsfName NORMAL_LABEL_ERROR_FONT_SIZE_ESFNAME = new EsfName("NormalLabelErrorFontSize");
    private static final EsfName NORMAL_LABEL_ERROR_FONT_STYLE_ESFNAME = new EsfName("NormalLabelErrorFontStyle");
    private static final EsfName NORMAL_LABEL_ERROR_FONT_COLOR_ESFNAME = new EsfName("NormalLabelErrorFontColor");
    private static final EsfName NORMAL_LABEL_ERROR_BACKGROUND_COLOR_ESFNAME = new EsfName("NormalLabelErrorBackgroundColor");
    
    private static final EsfName SMALL_LABEL_FONT_ESFNAME = new EsfName("SmallLabelFont");
    private static final EsfName SMALL_LABEL_FONT_SIZE_ESFNAME = new EsfName("SmallLabelFontSize");
    private static final EsfName SMALL_LABEL_FONT_STYLE_ESFNAME = new EsfName("SmallLabelFontStyle");
    private static final EsfName SMALL_LABEL_FONT_COLOR_ESFNAME = new EsfName("SmallLabelFontColor");
    private static final EsfName SMALL_LABEL_BACKGROUND_COLOR_ESFNAME = new EsfName("SmallLabelBackgroundColor");
    
    private static final EsfName SMALL_LABEL_ERROR_FONT_ESFNAME = new EsfName("SmallLabelErrorFont");
    private static final EsfName SMALL_LABEL_ERROR_FONT_SIZE_ESFNAME = new EsfName("SmallLabelErrorFontSize");
    private static final EsfName SMALL_LABEL_ERROR_FONT_STYLE_ESFNAME = new EsfName("SmallLabelErrorFontStyle");
    private static final EsfName SMALL_LABEL_ERROR_FONT_COLOR_ESFNAME = new EsfName("SmallLabelErrorFontColor");
    private static final EsfName SMALL_LABEL_ERROR_BACKGROUND_COLOR_ESFNAME = new EsfName("SmallLabelErrorBackgroundColor");
    
    protected final EsfUUID id;
    protected final EsfUUID documentStyleId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected Record styles;
   
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();

    /**
     * This creates a DocumentStyleVersion object from data retrieved from the DB.
     */
    protected DocumentStyleVersion(EsfUUID id, EsfUUID documentStyleId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		Record styles)
    {
        this.id = id;
        this.documentStyleId = documentStyleId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.styles = styles;
    }
    
    protected DocumentStyleVersion(DocumentStyle documentstyle, User createdByUser)
    {
        this.id = new EsfUUID();
        this.documentStyleId = documentstyle.getId();
        this.version = documentstyle.getProductionVersion() + 1;
        this.createdTimestamp = new EsfDateTime();
        this.createdByUserId = createdByUser.getId();
        this.lastUpdatedTimestamp = createdTimestamp;
        this.lastUpdatedByUserId = createdByUserId;
        this.styles = new Record(new EsfName("styles"),BlobDb.CompressOption.ENABLE,BlobDb.EncryptOption.DISABLE);
        
        // These values need to match one of the DropDown choices built by DbSetup.
        EsfString defaultValue = new EsfString("");
        EsfString borderDefault = new EsfString("1px solid #7F9DB9"); // "Clean"
        
        setDocumentFont(defaultValue);
        setDocumentFontSize(defaultValue);
        setDocumentFontStyle(defaultValue);
        setDocumentFontColor(defaultValue);
        setDocumentBackgroundColor(defaultValue);
        
        setSignatureFont(defaultValue);
        setSignatureFontSize(defaultValue);
        setSignatureFontStyle(defaultValue);
        setSignatureFontColor(defaultValue);
        setSignatureBackgroundColor(defaultValue);
        
        setFieldDataFont(defaultValue);
        setFieldDataFontSize(defaultValue);
        setFieldDataFontStyle(defaultValue);
        setFieldDataFontColor(defaultValue);
        setFieldDataBackgroundColor(defaultValue);
        
        setInputFieldRequiredFont(defaultValue);
        setInputFieldRequiredFontSize(defaultValue);
        setInputFieldRequiredFontStyle(defaultValue);
        setInputFieldRequiredFontColor(defaultValue);
        setInputFieldRequiredBackgroundColor(defaultValue);
        setInputFieldRequiredBorderTypes(borderDefault);
        
        setInputFieldOptionalFont(defaultValue);
        setInputFieldOptionalFontSize(defaultValue);
        setInputFieldOptionalFontStyle(defaultValue);
        setInputFieldOptionalFontColor(defaultValue);
        setInputFieldOptionalBackgroundColor(defaultValue);
        setInputFieldOptionalBorderTypes(borderDefault);
        
        setInputFieldErrorFont(defaultValue);
        setInputFieldErrorFontSize(defaultValue);
        setInputFieldErrorFontStyle(defaultValue);
        setInputFieldErrorFontColor(defaultValue);
        setInputFieldErrorBackgroundColor(defaultValue);
        setInputFieldErrorBorderTypes(borderDefault);
        
        setNormalLabelFont(defaultValue);
        setNormalLabelFontSize(defaultValue);
        setNormalLabelFontStyle(defaultValue);
        setNormalLabelFontColor(defaultValue);
        setNormalLabelBackgroundColor(defaultValue);
        
        setNormalLabelErrorFont(defaultValue);
        setNormalLabelErrorFontSize(defaultValue);
        setNormalLabelErrorFontStyle(defaultValue);
        setNormalLabelErrorFontColor(defaultValue);
        setNormalLabelErrorBackgroundColor(defaultValue);
        
        setSmallLabelFont(defaultValue);
        setSmallLabelFontSize(defaultValue);
        setSmallLabelFontStyle(defaultValue);
        setSmallLabelFontColor(defaultValue);
        setSmallLabelBackgroundColor(defaultValue);
        
        setSmallLabelErrorFont(defaultValue);
        setSmallLabelErrorFontSize(defaultValue);
        setSmallLabelErrorFontStyle(defaultValue);
        setSmallLabelErrorFontColor(defaultValue);
        setSmallLabelErrorBackgroundColor(defaultValue);
    }
   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getDocumentStyleId()
    {
        return documentStyleId;
    }
    
    public DocumentStyle getDocumentStyle()
    {
    	return DocumentStyle.Manager.getById(documentStyleId);
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}

	public int getVersion()
    {
    	return version;
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }
    
    //  The document body styles cannot really inherit, so if set, we just omit and then they use the standard from esf.css
    public EsfString getDocumentFont()
    {
    	return styles.getStringByName(DOCUMENT_FONT_ESFNAME);
    }
    public String getDocumentFontForBodyTag()
    {
    	EsfString v = getDocumentFont();
    	return ( v.contains(INHERIT) ) ? "" : v.toString();
    }
    public void setDocumentFont(EsfString v)
    {
    	styles.addUpdate(DOCUMENT_FONT_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getDocumentFontSize()
    {
    	return styles.getStringByName(DOCUMENT_FONT_SIZE_ESFNAME);
    }
    public String getDocumentFontSizeForBodyTag()
    {
    	EsfString v = getDocumentFontSize();
    	return ( v.contains(INHERIT) ) ? "" : v.toString();
    }
    public void setDocumentFontSize(EsfString v)
    {
    	styles.addUpdate(DOCUMENT_FONT_SIZE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getDocumentFontStyle()
    {
    	return styles.getStringByName(DOCUMENT_FONT_STYLE_ESFNAME);
    }
    public String getDocumentFontStyleForBodyTag()
    {
    	EsfString v = getDocumentFontStyle();
    	return ( v.contains(INHERIT) ) ? "" : v.toString();
    }
    public void setDocumentFontStyle(EsfString v)
    {
    	styles.addUpdate(DOCUMENT_FONT_STYLE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getDocumentFontColor()
    {
    	return styles.getStringByName(DOCUMENT_FONT_COLOR_ESFNAME);
    }
    public String getDocumentFontColorForBodyTag()
    {
    	EsfString v = getDocumentFontColor();
    	return ( v.contains(INHERIT) ) ? "" : v.toString();
    }
    public void setDocumentFontColor(EsfString v)
    {
    	styles.addUpdate(DOCUMENT_FONT_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getDocumentBackgroundColor()
    {
    	return styles.getStringByName(DOCUMENT_BACKGROUND_COLOR_ESFNAME);
    }
    public String getDocumentBackgroundColorForBodyTag()
    {
    	EsfString v = getDocumentBackgroundColor();
    	return ( v.contains(INHERIT) ) ? "" : v.toString();
    }
    public void setDocumentBackgroundColor(EsfString v)
    {
    	styles.addUpdate(DOCUMENT_BACKGROUND_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSignatureFont()
    {
    	return styles.getStringByName(SIGNATURE_FONT_ESFNAME);
    }
    public void setSignatureFont(EsfString v)
    {
    	styles.addUpdate(SIGNATURE_FONT_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSignatureFontSize()
    {
    	return styles.getStringByName(SIGNATURE_FONT_SIZE_ESFNAME);
    }
    public void setSignatureFontSize(EsfString v)
    {
    	styles.addUpdate(SIGNATURE_FONT_SIZE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSignatureFontStyle()
    {
    	return styles.getStringByName(SIGNATURE_FONT_STYLE_ESFNAME);
    }
    public void setSignatureFontStyle(EsfString v)
    {
    	styles.addUpdate(SIGNATURE_FONT_STYLE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSignatureFontColor()
    {
    	return styles.getStringByName(SIGNATURE_FONT_COLOR_ESFNAME);
    }
    public void setSignatureFontColor(EsfString v)
    {
    	styles.addUpdate(SIGNATURE_FONT_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSignatureBackgroundColor()
    {
    	return styles.getStringByName(SIGNATURE_BACKGROUND_COLOR_ESFNAME);
    }
    public void setSignatureBackgroundColor(EsfString v)
    {
    	styles.addUpdate(SIGNATURE_BACKGROUND_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getFieldDataFont()
    {
    	return styles.getStringByName(FIELD_DATA_FONT_ESFNAME);
    }
    public void setFieldDataFont(EsfString v)
    {
    	styles.addUpdate(FIELD_DATA_FONT_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getFieldDataFontSize()
    {
    	return styles.getStringByName(FIELD_DATA_FONT_SIZE_ESFNAME);
    }
    public void setFieldDataFontSize(EsfString v)
    {
    	styles.addUpdate(FIELD_DATA_FONT_SIZE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getFieldDataFontStyle()
    {
    	return styles.getStringByName(FIELD_DATA_FONT_STYLE_ESFNAME);
    }
    public void setFieldDataFontStyle(EsfString v)
    {
    	styles.addUpdate(FIELD_DATA_FONT_STYLE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getFieldDataFontColor()
    {
    	return styles.getStringByName(FIELD_DATA_FONT_COLOR_ESFNAME);
    }
    public void setFieldDataFontColor(EsfString v)
    {
    	styles.addUpdate(FIELD_DATA_FONT_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getFieldDataBackgroundColor()
    {
    	return styles.getStringByName(FIELD_DATA_BACKGROUND_COLOR_ESFNAME);
    }
    public void setFieldDataBackgroundColor(EsfString v)
    {
    	styles.addUpdate(FIELD_DATA_BACKGROUND_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldErrorFont()
    {
    	return styles.getStringByName(INPUT_FIELD_ERROR_FONT_ESFNAME);
    }
    public void setInputFieldErrorFont(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_ERROR_FONT_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldErrorFontSize()
    {
    	return styles.getStringByName(INPUT_FIELD_ERROR_FONT_SIZE_ESFNAME);
    }
    public void setInputFieldErrorFontSize(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_ERROR_FONT_SIZE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldErrorFontStyle()
    {
    	return styles.getStringByName(INPUT_FIELD_ERROR_FONT_STYLE_ESFNAME);
    }
    public void setInputFieldErrorFontStyle(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_ERROR_FONT_STYLE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldErrorFontColor()
    {
    	return styles.getStringByName(INPUT_FIELD_ERROR_FONT_COLOR_ESFNAME);
    }
    public void setInputFieldErrorFontColor(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_ERROR_FONT_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldErrorBackgroundColor()
    {
    	return styles.getStringByName(INPUT_FIELD_ERROR_BACKGROUND_COLOR_ESFNAME);
    }
    public void setInputFieldErrorBackgroundColor(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_ERROR_BACKGROUND_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldErrorBorderTypes()
    {
    	return styles.getStringByName(INPUT_FIELD_ERROR_BORDER_TYPES_ESFNAME);
    }
    public void setInputFieldErrorBorderTypes(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_ERROR_BORDER_TYPES_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldRequiredFont()
    {
    	return styles.getStringByName(INPUT_FIELD_REQUIRED_FONT_ESFNAME);
    }
    public void setInputFieldRequiredFont(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_REQUIRED_FONT_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldRequiredFontSize()
    {
    	return styles.getStringByName(INPUT_FIELD_REQUIRED_FONT_SIZE_ESFNAME);
    }
    public void setInputFieldRequiredFontSize(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_REQUIRED_FONT_SIZE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldRequiredFontStyle()
    {
    	return styles.getStringByName(INPUT_FIELD_REQUIRED_FONT_STYLE_ESFNAME);
    }
    public void setInputFieldRequiredFontStyle(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_REQUIRED_FONT_STYLE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldRequiredFontColor()
    {
    	return styles.getStringByName(INPUT_FIELD_REQUIRED_FONT_COLOR_ESFNAME);
    }
    public void setInputFieldRequiredFontColor(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_REQUIRED_FONT_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    private String getCheckboxRadioButtonFieldRequiredOutlineColor()
    {
    	String css = getInputFieldRequiredBackgroundColor().toString();
    	css = css.replace("background-color", "outline-color") + " outline-style: solid; outline-width: medium"; // the background-color originally ends with a ';' so we don't need to add one when replace with outline-color
    	return css;
    }
    public EsfString getInputFieldRequiredBackgroundColor()
    {
    	return styles.getStringByName(INPUT_FIELD_REQUIRED_BACKGROUND_COLOR_ESFNAME);
    }
    public void setInputFieldRequiredBackgroundColor(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_REQUIRED_BACKGROUND_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldRequiredBorderTypes()
    {
    	return styles.getStringByName(INPUT_FIELD_REQUIRED_BORDER_TYPES_ESFNAME);
    }
    public void setInputFieldRequiredBorderTypes(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_REQUIRED_BORDER_TYPES_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldOptionalFont()
    {
    	return styles.getStringByName(INPUT_FIELD_OPTIONAL_FONT_ESFNAME);
    }
    public void setInputFieldOptionalFont(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_OPTIONAL_FONT_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldOptionalFontSize()
    {
    	return styles.getStringByName(INPUT_FIELD_OPTIONAL_FONT_SIZE_ESFNAME);
    }
    public void setInputFieldOptionalFontSize(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_OPTIONAL_FONT_SIZE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldOptionalFontStyle()
    {
    	return styles.getStringByName(INPUT_FIELD_OPTIONAL_FONT_STYLE_ESFNAME);
    }
    public void setInputFieldOptionalFontStyle(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_OPTIONAL_FONT_STYLE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldOptionalFontColor()
    {
    	return styles.getStringByName(INPUT_FIELD_OPTIONAL_FONT_COLOR_ESFNAME);
    }
    public void setInputFieldOptionalFontColor(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_OPTIONAL_FONT_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldOptionalBackgroundColor()
    {
    	return styles.getStringByName(INPUT_FIELD_OPTIONAL_BACKGROUND_COLOR_ESFNAME);
    }
    public void setInputFieldOptionalBackgroundColor(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_OPTIONAL_BACKGROUND_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getInputFieldOptionalBorderTypes()
    {
    	return styles.getStringByName(INPUT_FIELD_OPTIONAL_BORDER_TYPES_ESFNAME);
    }
    public void setInputFieldOptionalBorderTypes(EsfString v)
    {
    	styles.addUpdate(INPUT_FIELD_OPTIONAL_BORDER_TYPES_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getNormalLabelFont()
    {
    	return styles.getStringByName(NORMAL_LABEL_FONT_ESFNAME);
    }
    public void setNormalLabelFont(EsfString v)
    {
    	styles.addUpdate(NORMAL_LABEL_FONT_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getNormalLabelFontSize()
    {
    	return styles.getStringByName(NORMAL_LABEL_FONT_SIZE_ESFNAME);
    }
    public void setNormalLabelFontSize(EsfString v)
    {
    	styles.addUpdate(NORMAL_LABEL_FONT_SIZE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getNormalLabelFontStyle()
    {
    	return styles.getStringByName(NORMAL_LABEL_FONT_STYLE_ESFNAME);
    }
    public void setNormalLabelFontStyle(EsfString v)
    {
    	styles.addUpdate(NORMAL_LABEL_FONT_STYLE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getNormalLabelFontColor()
    {
    	return styles.getStringByName(NORMAL_LABEL_FONT_COLOR_ESFNAME);
    }
    public void setNormalLabelFontColor(EsfString v)
    {
    	styles.addUpdate(NORMAL_LABEL_FONT_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getNormalLabelBackgroundColor()
    {
    	return styles.getStringByName(NORMAL_LABEL_BACKGROUND_COLOR_ESFNAME);
    }
    public void setNormalLabelBackgroundColor(EsfString v)
    {
    	styles.addUpdate(NORMAL_LABEL_BACKGROUND_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getNormalLabelErrorFont()
    {
    	return styles.getStringByName(NORMAL_LABEL_ERROR_FONT_ESFNAME);
    }
    public void setNormalLabelErrorFont(EsfString v)
    {
    	styles.addUpdate(NORMAL_LABEL_ERROR_FONT_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getNormalLabelErrorFontSize()
    {
    	return styles.getStringByName(NORMAL_LABEL_ERROR_FONT_SIZE_ESFNAME);
    }
    public void setNormalLabelErrorFontSize(EsfString v)
    {
    	styles.addUpdate(NORMAL_LABEL_ERROR_FONT_SIZE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getNormalLabelErrorFontStyle()
    {
    	return styles.getStringByName(NORMAL_LABEL_ERROR_FONT_STYLE_ESFNAME);
    }
    public void setNormalLabelErrorFontStyle(EsfString v)
    {
    	styles.addUpdate(NORMAL_LABEL_ERROR_FONT_STYLE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getNormalLabelErrorFontColor()
    {
    	return styles.getStringByName(NORMAL_LABEL_ERROR_FONT_COLOR_ESFNAME);
    }
    public void setNormalLabelErrorFontColor(EsfString v)
    {
    	styles.addUpdate(NORMAL_LABEL_ERROR_FONT_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getNormalLabelErrorBackgroundColor()
    {
    	return styles.getStringByName(NORMAL_LABEL_ERROR_BACKGROUND_COLOR_ESFNAME);
    }
    public void setNormalLabelErrorBackgroundColor(EsfString v)
    {
    	styles.addUpdate(NORMAL_LABEL_ERROR_BACKGROUND_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSmallLabelFont()
    {
    	return styles.getStringByName(SMALL_LABEL_FONT_ESFNAME);
    }
    public void setSmallLabelFont(EsfString v)
    {
    	styles.addUpdate(SMALL_LABEL_FONT_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSmallLabelFontSize()
    {
    	return styles.getStringByName(SMALL_LABEL_FONT_SIZE_ESFNAME);
    }
    public void setSmallLabelFontSize(EsfString v)
    {
    	styles.addUpdate(SMALL_LABEL_FONT_SIZE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSmallLabelFontStyle()
    {
    	return styles.getStringByName(SMALL_LABEL_FONT_STYLE_ESFNAME);
    }
    public void setSmallLabelFontStyle(EsfString v)
    {
    	styles.addUpdate(SMALL_LABEL_FONT_STYLE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSmallLabelFontColor()
    {
    	return styles.getStringByName(SMALL_LABEL_FONT_COLOR_ESFNAME);
    }
    public void setSmallLabelFontColor(EsfString v)
    {
    	styles.addUpdate(SMALL_LABEL_FONT_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSmallLabelBackgroundColor()
    {
    	return styles.getStringByName(SMALL_LABEL_BACKGROUND_COLOR_ESFNAME);
    }
    public void setSmallLabelBackgroundColor(EsfString v)
    {
    	styles.addUpdate(SMALL_LABEL_BACKGROUND_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSmallLabelErrorFont()
    {
    	return styles.getStringByName(SMALL_LABEL_ERROR_FONT_ESFNAME);
    }
    public void setSmallLabelErrorFont(EsfString v)
    {
    	styles.addUpdate(SMALL_LABEL_ERROR_FONT_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSmallLabelErrorFontSize()
    {
    	return styles.getStringByName(SMALL_LABEL_ERROR_FONT_SIZE_ESFNAME);
    }
    public void setSmallLabelErrorFontSize(EsfString v)
    {
    	styles.addUpdate(SMALL_LABEL_ERROR_FONT_SIZE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSmallLabelErrorFontStyle()
    {
    	return styles.getStringByName(SMALL_LABEL_ERROR_FONT_STYLE_ESFNAME);
    }
    public void setSmallLabelErrorFontStyle(EsfString v)
    {
    	styles.addUpdate(SMALL_LABEL_ERROR_FONT_STYLE_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSmallLabelErrorFontColor()
    {
    	return styles.getStringByName(SMALL_LABEL_ERROR_FONT_COLOR_ESFNAME);
    }
    public void setSmallLabelErrorFontColor(EsfString v)
    {
    	styles.addUpdate(SMALL_LABEL_ERROR_FONT_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public EsfString getSmallLabelErrorBackgroundColor()
    {
    	return styles.getStringByName(SMALL_LABEL_ERROR_BACKGROUND_COLOR_ESFNAME);
    }
    public void setSmallLabelErrorBackgroundColor(EsfString v)
    {
    	styles.addUpdate(SMALL_LABEL_ERROR_BACKGROUND_COLOR_ESFNAME,v.trim());
    	objectChanged();
    }

    public String getCssFileName()
    {
    	return "/" + Application.getInstance().getLibraryGeneratedCSSPath() + 
		   	   "/" + getDocumentStyleId() + 
		       "/" + getId() +
		       CSS_FILE_SUFFIX;
    }
    
    public String toCss()
    {
    	StringBuilder css = new StringBuilder(3000);
    	
    	String thisCssClass = "." + getId().toNormalizedEsfNameString();
    	
    	// Body is special since can't really inherit, so if they do inherit, we actually omit the style attribute:value
    	css.append("body.esf { ").append(getDocumentFontForBodyTag()).append(' ').append(getDocumentFontSizeForBodyTag()).append(' ').append(getDocumentFontColorForBodyTag()).append(' ').append(getDocumentFontStyleForBodyTag()).append(' ').append(getDocumentBackgroundColorForBodyTag()).append(" } \n");

    	css.append(".esf ").append(thisCssClass).append(" span.signature { ").append(getSignatureFont()).append(' ').append(getSignatureFontSize()).append(' ').append(getSignatureFontColor()).append(' ').append(getSignatureFontStyle()).append(' ').append(getSignatureBackgroundColor()).append(" } \n");
    	css.append(".esf ").append(thisCssClass).append(" span.signature input { ").append(getSignatureFont()).append(' ').append(getSignatureFontSize()).append(' ').append(getSignatureFontColor()).append(' ').append(getSignatureFontStyle()).append(' ').append(getSignatureBackgroundColor()).append(" } \n");
    	
    	css.append(".esf ").append(thisCssClass).append(" span.viewFieldData { ").append(getFieldDataFont()).append(' ').append(getFieldDataFontSize()).append(' ').append(getFieldDataFontColor()).append(' ').append(getFieldDataFontStyle()).append(' ').append(getFieldDataBackgroundColor()).append(" } \n");

    	css.append(".esf ").append(thisCssClass).append(" input[type='checkbox'].required { ").append("border: 0 none !important; ").append(getCheckboxRadioButtonFieldRequiredOutlineColor()).append("; } \n");
    	css.append(".esf ").append(thisCssClass).append(" input[type='radio'].required { ").append("border: 0 none !important; ").append(getCheckboxRadioButtonFieldRequiredOutlineColor()).append("; } \n");

    	css.append(".esf ").append(thisCssClass).append(" input.required { ").append(getInputFieldRequiredFont()).append(' ').append(getInputFieldRequiredFontSize()).append(' ').append(getInputFieldRequiredFontColor()).append(' ').append(getInputFieldRequiredFontStyle()).append(' ').append(getInputFieldRequiredBackgroundColor()).append(" border: ").append(getInputFieldRequiredBorderTypes()).append("; } \n");
    	css.append(".esf ").append(thisCssClass).append(" select.required { ").append(getInputFieldRequiredFont()).append(' ').append(getInputFieldRequiredFontSize()).append(' ').append(getInputFieldRequiredFontColor()).append(' ').append(getInputFieldRequiredFontStyle()).append(' ').append(getInputFieldRequiredBackgroundColor()).append(" border: ").append(getInputFieldRequiredBorderTypes()).append("; } \n");
    	css.append(".esf ").append(thisCssClass).append(" textarea.required { ").append(getInputFieldRequiredFont()).append(' ').append(getInputFieldRequiredFontSize()).append(' ').append(getInputFieldRequiredFontColor()).append(' ').append(getInputFieldRequiredFontStyle()).append(' ').append(getInputFieldRequiredBackgroundColor()).append(" border: ").append(getInputFieldRequiredBorderTypes()).append("; } \n");
    	
    	css.append(".esf ").append(thisCssClass).append(" input.optional { ").append(getInputFieldOptionalFont()).append(' ').append(getInputFieldOptionalFontSize()).append(' ').append(getInputFieldOptionalFontColor()).append(' ').append(getInputFieldOptionalFontStyle()).append(' ').append(getInputFieldOptionalBackgroundColor()).append(" border: ").append(getInputFieldOptionalBorderTypes()).append("; } \n");
    	css.append(".esf ").append(thisCssClass).append(" select.optional { ").append(getInputFieldOptionalFont()).append(' ').append(getInputFieldOptionalFontSize()).append(' ').append(getInputFieldOptionalFontColor()).append(' ').append(getInputFieldOptionalFontStyle()).append(' ').append(getInputFieldOptionalBackgroundColor()).append(" border: ").append(getInputFieldOptionalBorderTypes()).append("; } \n");
    	css.append(".esf ").append(thisCssClass).append(" textarea.optional { ").append(getInputFieldOptionalFont()).append(' ').append(getInputFieldOptionalFontSize()).append(' ').append(getInputFieldOptionalFontColor()).append(' ').append(getInputFieldOptionalFontStyle()).append(' ').append(getInputFieldOptionalBackgroundColor()).append(" border: ").append(getInputFieldOptionalBorderTypes()).append("; } \n");

    	css.append(".esf ").append(thisCssClass).append(" input[type='checkbox'].error { ").append("border: 0 none !important; outline: ").append(getInputFieldErrorBorderTypes()).append("; } \n");
    	css.append(".esf ").append(thisCssClass).append(" input[type='radio'].error { ").append("border: 0 none !important; outline: ").append(getInputFieldErrorBorderTypes()).append("; } \n");

    	css.append(".esf ").append(thisCssClass).append(" input.error { ").append(getInputFieldErrorFont()).append(' ').append(getInputFieldErrorFontSize()).append(' ').append(getInputFieldErrorFontColor()).append(' ').append(getInputFieldErrorFontStyle()).append(' ').append(getInputFieldErrorBackgroundColor()).append(" border: ").append(getInputFieldErrorBorderTypes()).append("; } \n");
    	css.append(".esf ").append(thisCssClass).append(" select.error { ").append(getInputFieldErrorFont()).append(' ').append(getInputFieldErrorFontSize()).append(' ').append(getInputFieldErrorFontColor()).append(' ').append(getInputFieldErrorFontStyle()).append(' ').append(getInputFieldErrorBackgroundColor()).append(" border: ").append(getInputFieldErrorBorderTypes()).append("; } \n");
    	css.append(".esf ").append(thisCssClass).append(" textarea.error { ").append(getInputFieldErrorFont()).append(' ').append(getInputFieldErrorFontSize()).append(' ').append(getInputFieldErrorFontColor()).append(' ').append(getInputFieldErrorFontStyle()).append(' ').append(getInputFieldErrorBackgroundColor()).append(" border: ").append(getInputFieldErrorBorderTypes()).append("; } \n");

    	css.append(".esf ").append(thisCssClass).append(" label.normal { ").append(getNormalLabelFont()).append(' ').append(getNormalLabelFontSize()).append(' ').append(getNormalLabelFontColor()).append(' ').append(getNormalLabelFontStyle()).append(' ').append(getNormalLabelBackgroundColor()).append(" } \n");

    	css.append(".esf ").append(thisCssClass).append(" label.normal-error { ").append(getNormalLabelErrorFont()).append(' ').append(getNormalLabelErrorFontSize()).append(' ').append(getNormalLabelErrorFontColor()).append(' ').append(getNormalLabelErrorFontStyle()).append(' ').append(getNormalLabelErrorBackgroundColor()).append(" } \n");

    	css.append(".esf ").append(thisCssClass).append(" label.small { ").append(getSmallLabelFont()).append(' ').append(getSmallLabelFontSize()).append(' ').append(getSmallLabelFontColor()).append(' ').append(getSmallLabelFontStyle()).append(' ').append(getSmallLabelBackgroundColor()).append(" } \n");

    	css.append(".esf ").append(thisCssClass).append(" label.small-error { ").append(getSmallLabelErrorFont()).append(' ').append(getSmallLabelErrorFontSize()).append(' ').append(getSmallLabelErrorFontColor()).append(' ').append(getSmallLabelErrorFontStyle()).append(' ').append(getSmallLabelErrorBackgroundColor()).append(" } \n");

    	return css.toString();
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof DocumentStyleVersion )
        {
        	DocumentStyleVersion otherDocumentStyle = (DocumentStyleVersion)o;
            return getId().equals(otherDocumentStyle.getId());
        }
        return false;
    }
    
    protected Record getStyles()
    {
        return styles;
    }

    protected boolean saveStyles()
    {
    	return styles.save();
    }


    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(DocumentStyleVersion o)
    {
    	if ( getDocumentStyleId().equals(o.getDocumentStyleId()) )
    		return getVersion() - o.getVersion();
    	return getDocumentStyleId().compareTo(o.getDocumentStyleId());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<DocumentStyleVersion xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <documentStyleId>").append(documentStyleId.toXml()).append("</documentStyleId>\n");
        buf.append(" <version>").append(version).append("</version>\n");
        buf.append(" <createdTimestamp>").append(createdTimestamp.toXml()).append("</createdTimestamp>\n");
        buf.append(" <createdByUserId>").append(createdByUserId.toXml()).append("</createdByUserId>\n");
        buf.append(" <lastUpdatedTimestamp>").append(lastUpdatedTimestamp.toXml()).append("</lastUpdatedTimestamp>\n");
        buf.append(" <lastUpdatedByUserId>").append(lastUpdatedByUserId.toXml()).append("</lastUpdatedByUserId>\n");
        buf.append(" <styles>");
        styles.appendXml(buf);
        buf.append("</styles>\n");

        buf.append("</DocumentStyleVersion>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 316 + id.getEstimatedLengthXml() + documentStyleId.getEstimatedLengthXml() + createdTimestamp.getEstimatedLengthXml() + createdByUserId.getEstimatedLengthXml() + 
        	lastUpdatedTimestamp.getEstimatedLengthXml() + lastUpdatedByUserId.getEstimatedLengthXml();

        len += styles.getEstimatedLengthXml();

        return len; 
    }


    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
    // Creates the CSS code for this document style version.  It puts all of it in the configured CSS directory.
    // The first subdirectory here is the document style id.
    // The CSS file is then written using its document style version id filename.
    public boolean generateCSS()
    {
    	try
    	{
        	// First, if there is no css folder for our document style, create it.
        	java.io.File docDir = new java.io.File(Application.getInstance().getLibraryGeneratedCSSDirectory(),getDocumentStyleId().toPlainString());
        	if ( ! docDir.exists() )
        	{
        		if ( ! docDir.mkdir()           || 
        			 ! docDir.setReadable(true) ||
        			 ! docDir.setWritable(true) ||
        			 ! docDir.setExecutable(true)
        		   )
        		{
        			_logger.error("generateCSS() couldn't create read/write/execute document style directory: " + docDir.getAbsolutePath());
        			return false;
        		}
        	}
        	
        	// If the document style version file exists, let's remove it.
        	String cssFileName = getId().toPlainString() + CSS_FILE_SUFFIX;
        	java.io.File docVerCss = new java.io.File(docDir,cssFileName);
        	if ( docVerCss.exists() )
        	{
        		if ( ! docVerCss.delete() )
        		{
           			_logger.error("generateCSS() could not delete previous CSS file: " + docVerCss.getAbsolutePath());
         			return false;
        		}
        	}
        	
    		if ( ! docVerCss.createNewFile()   ||
    			 ! docVerCss.setReadable(true) ||
    			 ! docVerCss.setWritable(true) 
    		   )
    		{
    			_logger.error("generateCSS() could not create read/write document style version CSS file: " + docVerCss.getAbsolutePath());
    			return false;
    		}
    		writeCSS(docVerCss);
    		
        	return true;
    	}
    	catch( java.io.IOException e )
    	{
    		_logger.error("generateCSS() failed for document style id: " + getDocumentStyleId() + "; version id: " + getId(), e);
    		return false;
    	}
    	finally
    	{
    	}
    }
    
    private void writeCSS(java.io.File cssFile)
    	throws java.io.IOException
    {
    	java.io.OutputStreamWriter outputStreamWriter = new java.io.OutputStreamWriter( new java.io.FileOutputStream(cssFile), EsfString.CHARSET_UTF_8 );
    	java.io.BufferedWriter bufferedWriter = new java.io.BufferedWriter(outputStreamWriter);
    	java.io.PrintWriter printWriter = new java.io.PrintWriter(bufferedWriter);
    	
    	printWriter.println();
    	printWriter.println();
    	printWriter.print("/* Begin document style id: ");
    	printWriter.print(getDocumentStyleId().toString());
    	printWriter.print(", version id: ");
    	printWriter.print(getId().toString());
    	printWriter.println(" */");
    	
    	printWriter.print(toCss()); // Already ends with a new line.
    	printWriter.println("/* End document style */");
    	
    	printWriter.close();
    }
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; documentStyleId: " + documentStyleId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
            	
            	styles.save(con);
                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_documentstyle_version (id,library_documentstyle_id,version,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,styles_record_blob_id) VALUES (?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(documentStyleId);
                stmt.set(version);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(styles.getId());
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; documentStyleId: " + documentStyleId + "; version: " + version);
                    return false;
                }
                
                // Always generate CSS for new document style versions
                generateCSS();
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this);
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new document style version id: " + id + "; documentStyleId: " + documentStyleId + "; version: " + version); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new document style version id: " + id + "; documentStyleId: " + documentStyleId + "; version: " + version);
                }
                
                return true;
            }
            
            // We only save and generate new CSS if we're a Test version.
            if ( getDocumentStyle().getTestVersion() == version )
            {
                if ( styles.hasChanged() )
                    styles.save(con);

                generateCSS();
            }
        	
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_documentstyle_version SET last_updated_timestamp=?,last_updated_by_user_id=? WHERE id=?"
                						   		);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for documentstyle version id: " + id + "; documentStyleId: " + documentStyleId + "; version: " + version);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated document style version id: " + id + "; documentStyleId: " + documentStyleId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated document style version id: " + id + "; documentStyleId: " + documentStyleId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; documentStyleId: " + documentStyleId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; documentStyleId: " + documentStyleId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use 
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of document style version that was pending an INSERT id: " + id + "; documentStyleId: " + documentStyleId + "; version: " + version);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
            // Delete the documentstyle version and its styles
        	styles.delete(con);
            
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_documentstyle_version WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted document style version id: " + id + "; documentStyleId: " + documentStyleId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted document style version id: " + id + "; documentStyleId: " + documentStyleId + "; version: " + version);
            }

            // Let's remove the generated file
        	String cssFileName = getId().toPlainString() + CSS_FILE_SUFFIX;
        	java.io.File docDir = new java.io.File(Application.getInstance().getLibraryGeneratedCSSDirectory(),getDocumentStyleId().toPlainString());
        	java.io.File docVerCss = new java.io.File(docDir,cssFileName);
        	if ( ! docVerCss.delete() )
       			_logger.warn("delete() could not delete previous CSS file: " + docVerCss.getAbsolutePath());
            
            return true;
        }
        catch(SQLException e)
        {
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"delete() on id: " + id + "; documentStyleId: " + documentStyleId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   		private static UUIDCacheReadOptimized<DocumentStyleVersion> cache = new UUIDCacheReadOptimized<DocumentStyleVersion>();
   		
   		static final DocumentStyleVersion getById(Connection con, EsfUUID documentstyleVersionId) throws SQLException
   		{
   			return getById(con,documentstyleVersionId,true);
   		}
   		static DocumentStyleVersion getById(Connection con, EsfUUID documentstyleVersionId, boolean checkCache) throws SQLException
   		{
   			if ( checkCache )
   			{
   				DocumentStyleVersion documentstyleVer = cache.getById(documentstyleVersionId);
   	   			if ( documentstyleVer != null )
   	   				return documentstyleVer;
   			}

   			EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT library_documentstyle_id,version,created_by_user_id,last_updated_by_user_id,created_timestamp,last_updated_timestamp,styles_record_blob_id " +
   	        			"FROM esf_library_documentstyle_version WHERE id = ?"
   	        									);
   	        	stmt.set(documentstyleVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID libraryDocumentStyleId = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID stylesBlobId = rs.getEsfUUID();
		            
		            Record styles = Record.Manager.getById(con,stylesBlobId);
		            
		            DocumentStyleVersion documentstyleVer = new DocumentStyleVersion(documentstyleVersionId,libraryDocumentStyleId,version,
		            											 createdTimestamp,createdByUserId,lastUpdatedTimestamp,lastUpdatedByUserId,
		            											 styles);
		            documentstyleVer.setLoadedFromDb();
		            cache.add(documentstyleVer);
		            
	   	            return documentstyleVer;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + documentstyleVersionId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + documentstyleVersionId + "; failed to find the document style version");
   	        return null; 
  		}
   		
   		public static DocumentStyleVersion getById(EsfUUID documentstyleVersionId)
   		{
   			DocumentStyleVersion documentstyleVer = cache.getById(documentstyleVersionId);
   			if ( documentstyleVer != null )
   				return documentstyleVer;
   	   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	documentstyleVer = getById(con,documentstyleVersionId,false);
   	        	if ( documentstyleVer != null ) 
   	        	{
		            con.commit();
	   	        	return documentstyleVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		static DocumentStyleVersion getByVersion(Connection con, EsfUUID libraryDocumentStyleId, int version) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_documentstyle_version WHERE library_documentstyle_id = ? AND version = ?" );
   	        	stmt.set(libraryDocumentStyleId);
   	        	stmt.set(version);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID docVersionId = rs.getEsfUUID();
	       			return getById(con,docVersionId);
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByVersion() - libraryDocumentStyleId: " + libraryDocumentStyleId + "; version: " + version);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getByVersion() - libraryDocumentStyleId: " + libraryDocumentStyleId + "; version: " + version + "; failed to find the documentstyle version");
   	        return null; 
  		}
   		
   		public static DocumentStyleVersion getByVersion(EsfUUID libraryDocumentStyleId, int version)
   		{
   			// While not always true, if the version is 1, it's possible this is a new object that's not been saved yet so we can't find the id in the DB, so we'll check our cache first just in case
   			if ( version == 1 )
   			{
   				for( EsfUUID id : cache.getAllIds() )
   				{
   					DocumentStyleVersion documentstyleVer = cache.getById(id);
   					if ( documentstyleVer.getDocumentStyleId().equals(libraryDocumentStyleId) && documentstyleVer.getVersion() == version )
   						return documentstyleVer;
   				}
   			}
   			
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	DocumentStyleVersion documentstyleVer = getByVersion(con,libraryDocumentStyleId,version);
   	        	if ( documentstyleVer != null ) 
   	        	{
		            con.commit();
	   	        	return documentstyleVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		
   		public static List<DocumentStyleVersion> getAllByDocumentStyleId(Connection con, EsfUUID libraryDocumentStyleId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<DocumentStyleVersion> list = new LinkedList<DocumentStyleVersion>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_documentstyle_version WHERE library_documentstyle_id = ?" );
   	        	stmt.set(libraryDocumentStyleId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID documentstyleVersionId = rs.getEsfUUID();
	            	
	            	DocumentStyleVersion documentstyleVer = getById(con,documentstyleVersionId);
	       			if ( documentstyleVer != null )
	       				list.add(documentstyleVer);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<DocumentStyleVersion> getAllByDocumentStyleId(EsfUUID libraryDocumentStyleId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<DocumentStyleVersion> list = getAllByDocumentStyleId(con,libraryDocumentStyleId);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return new LinkedList<DocumentStyleVersion>(); 
   		}
   	    
   		
   	    
   	    public static DocumentStyleVersion createTest(DocumentStyle documentstyle, User createdBy)
   	    {
   	    	if ( documentstyle == null || createdBy == null )
   	    		return null;
   	    	DocumentStyleVersion newDocumentStyleVersion = new DocumentStyleVersion(documentstyle,createdBy);
   	    	cache.add(newDocumentStyleVersion);
   	    	return newDocumentStyleVersion;
   	    }

   	    public static DocumentStyleVersion createLike(DocumentStyle documentstyle, DocumentStyleVersion likeDocumentStyleVersion, User createdBy)
   	    {
   	    	if ( documentstyle == null || likeDocumentStyleVersion == null || createdBy == null )
   	    		return null;
   	    	DocumentStyleVersion newDocumentStyleVersion = createTest(documentstyle,createdBy);
   	    	newDocumentStyleVersion.styles = new Record(likeDocumentStyleVersion.getStyles());
   	    	return newDocumentStyleVersion;
   	    }

   	    public static DocumentStyleVersion createFromJDOM(DocumentStyle documentStyle, Element e)
   	    {
   	    	if ( documentStyle == null || e == null )
   	    		return null;
   	    	
   	    	
   	    	EsfUUID id = new EsfUUID();
   	    	
			Element stylesElement = e.getChild("styles", e.getNamespace());
			Element recordElement = stylesElement.getChild("esfRecord", Namespace.getNamespace(XmlUtil.getXmlNamespace2009()));
			
			Record styles = null;
			try
			{
				styles = new Record(new EsfUUID(), recordElement);
			}
			catch( EsfException ex )
			{
				_logger.error("createFromJDOM() failed on styles/esfRecord for document style: " + documentStyle.getEsfName(), ex);
				styles = new Record(new EsfName("styles"),BlobDb.CompressOption.ENABLE,BlobDb.EncryptOption.DISABLE); // bad, but empty is better than nothing?!
			}
			
			DocumentStyleVersion newDocumentStyleVersion = new DocumentStyleVersion(
   	    			id, 
   	    			documentStyle.getId(), 
   	    			documentStyle.getTestVersion(),
   	    			null,
   	    			null,
   	    			null,
   	    			null,
   	    			styles
   	    			);
			newDocumentStyleVersion.setNewObject();
   	    	cache.add(newDocumentStyleVersion);
   	    	return newDocumentStyleVersion;
   	    }
   	    
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    /**
   	     * Finds all document style versions that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}

   	} // Manager
   	
}