// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;

/**
* DropDownInfo is a data-only object that holds information about a drop down selection list.
* It's not a mutable object (you can't save/delete it), just for lists of drop downs.
* 
* @author Yozons, Inc.
*/
public class DropDownInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<DropDownInfo>, java.io.Serializable
{
	private static final long serialVersionUID = -7536793958310232645L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DropDownInfo.class);

    protected EsfUUID id;
    protected EsfName esfname;
    protected String  description;
    protected String  status;
    protected int	  productionVersion;
    protected int	  testVersion;

    /**
     * This creates a new DropDownInfo using the source object.
     * @param dropDown the dropDown to use as the source
     */
    protected DropDownInfo(DropDown dropDown)
    {
    	this.id = dropDown.getId();
    	this.esfname = dropDown.getEsfName();
    	this.description = dropDown.getDescription();
    	this.status = dropDown.getStatus();
    	this.productionVersion = dropDown.getProductionVersion();
    	this.testVersion = dropDown.getTestVersion();
    }
    
    /**
     * This creates a DropDownInfo object from data retrieved from the DB.
     */
    protected DropDownInfo(EsfUUID id, EsfName esfname, String description, String status, int productionVersion, int testVersion)
    {
    	this.id = id;
    	this.esfname = esfname;
    	this.description = description;
    	this.status = status;
    	this.productionVersion = productionVersion;
    	this.testVersion = testVersion;
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }

    public int getProductionVersion()
    {
    	return productionVersion;
    }
    
    public int getTestVersion()
    {
    	return testVersion;
    }

    public boolean hasProductionVersion()
    {
    	return productionVersion > 0;
    }

    public boolean hasTestVersion()
    {
    	return testVersion > productionVersion;
    }

    public EsfUUID getId()
    {
        return id;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof DropDownInfo )
            return getId().equals(((DropDownInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(DropDownInfo o)
    {
    	return getEsfName().compareTo(o.getEsfName());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all DropDownInfo objects in the specified container id.
   	     * @return the Collection of DropDownInfo found ordered by esfname.
   	     */
   	    public static Collection<DropDownInfo> getAll(EsfUUID containerId)
   	    {
   	    	LinkedList<DropDownInfo> list = new LinkedList<DropDownInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id,esfname,description,status,production_version,test_version " +
   	    	            "FROM esf_library_dropDown " + 
   	    	            "WHERE container_id=? " +
   	    	            "ORDER BY lower(esfname) ASC"
   	            						   );
   	        	stmt.set(containerId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            EsfName esfname = rs.getEsfName();
		            String description = rs.getString();
		            String  stat = rs.getString();
		            int productionVersion = rs.getInt();
		            int testVersion = rs.getInt();
		            
		            DropDownInfo dropdownInfo = new DropDownInfo(id,esfname,description,stat,productionVersion,testVersion);
		            dropdownInfo.setLoadedFromDb();
		            list.add(dropdownInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - containerId: " + containerId + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - containerId: " + containerId + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static DropDownInfo createNew(EsfUUID containerId)
   	    {
   	    	if ( containerId == null )
   	    		return null;
   	    	
   			DropDown dropDown = DropDown.Manager.createNew(containerId);
   	    	return new DropDownInfo(dropDown);
   	    }
   	    
   	    public static DropDownInfo createLike(DropDown likeDropDown, EsfName newName)
   	    {
   	    	if ( likeDropDown == null || newName == null || ! newName.isValid() )
   	    		return null;

   	    	DropDown newDropDown = DropDown.Manager.createLike(likeDropDown.getContainerId(), likeDropDown, newName);
   	    	return new DropDownInfo(newDropDown);
   	    }
   	    
   	    public static DropDownInfo createFromSource(DropDown dropDown)
   	    {
   	    	if ( dropDown == null )
   	    		return null;
   	    	
   	    	DropDownInfo docInfo = new DropDownInfo(dropDown);
   	    	docInfo.setLoadedFromDb();

   			return docInfo;
   	    }
   	    
   	} // Manager
   	
}