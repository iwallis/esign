// Copyright (C) 2010-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.servlet.StartTransaction;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.Permission;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.util.PathNameUUIDUserListableCacheReadOptimized;

/**
* TransactionTemplate holds a single Open eSignForms template of a transaction, such as the Package it operates on, who can start, cancel, report on, etc.
* 
* @author Yozons, Inc.
*/
public class TransactionTemplate
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<TransactionTemplate>, 
    			PathNameUUIDUserListableCacheReadOptimized.PathNameUUIDUserListableCacheable, 
    			PathNameUUIDUserListableCacheReadOptimized.TimeCacheable,
    			java.io.Serializable
{
	private static final long serialVersionUID = 216600656161190991L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TransactionTemplate.class);

    protected final EsfUUID id;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected EsfPathName pathName;
    protected EsfPathName originalPathName; // derived, not persisted
    protected String  displayName;
    protected String  description;
    protected String  comments;
    protected String  productionStatus;
    protected String  productionRetentionSpec; // number of units followed by the interval from our drop down, such as "0 forever" or "1 year" or "600 minute" etc.
    protected boolean productionRetentionSpecChanged = false; // flag, not persisted
    protected int productionRetentionNumTimeIntervalUnits; // derived, not persisted
    protected String productionRetentionTimeIntervalUnits; // derived, not persisted
    protected String  testStatus;
    protected String  testRetentionSpec;
    protected boolean testRetentionSpecChanged = false; // flag, not persisted
    protected int testRetentionNumTimeIntervalUnits; // derived, not persisted
    protected String testRetentionTimeIntervalUnits; // derived, not persisted
    protected EsfUUID packageId;
    protected Permission permission; // permission grants related to this transaction template object (create like, delete, list, update, view details of this transaction template object)
                                     // as well as those permissions for users to start them, cancel them, report on them, etc.
    protected EsfUUID brandLibraryId; // Optional brand library to search first.
    
    public static final String PERM_PATH_PREFIX = "TransactionTemplatePerm" + EsfPathName.PATH_SEPARATOR;  // actual name is this prefix followed transaction template's id
    
    // Initial transaction template created for cloning from, etc.
    public static final EsfPathName TEMPLATE_ESFPATHNAME	= new EsfPathName(EsfPathName.ESF_RESERVED_TRANSACTION_TEMPLATE_PATH_PREFIX+"Template");
    
    public enum INCLUDE { ONLY_ENABLED, ONLY_DISABLED, BOTH_ENABLED_AND_DISABLED };
    
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();

    /**
     * This creates a TransactionTemplate object from data retrieved from the DB.
     */
    protected TransactionTemplate(EsfUUID id, EsfPathName pathName, String displayName, String description, String comments, 
    		        EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		        String productionStatus, String productionRetentionSpec, String testStatus, String testRetentionSpec,
    		        EsfUUID packageId, Permission permission, EsfUUID brandLibraryId)
    {
        this.id = id;
        this.pathName = pathName;
        this.originalPathName = pathName.duplicate();
        this.displayName = displayName;
        this.description = description;
        this.comments = comments;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.productionStatus = productionStatus;
        setProductionRetentionSpec(productionRetentionSpec);
        this.testStatus = testStatus;
        setTestRetentionSpec(testRetentionSpec);
        this.packageId = packageId;
        this.permission = permission;
        this.brandLibraryId = brandLibraryId;
        this.productionRetentionSpecChanged = this.testRetentionSpecChanged = false;
    }
    
   
    /**
     * This creates a TransactionTemplate object 
     */
    protected TransactionTemplate(EsfUUID id, EsfPathName pathName, String displayName, String description, String comments, 
    		        EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		        String productionStatus, String productionRetentionSpec, String testStatus, String testRetentionSpec, 
    		        Package pkg, Permission permission, Library brandLibrary)
    {
        this.id = id;
        this.pathName = pathName;
        this.originalPathName = pathName.duplicate();
        this.displayName = displayName;
        this.description = description;
        this.comments = comments;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.productionStatus = productionStatus;
        setProductionRetentionSpec(productionRetentionSpec);
        this.testStatus = testStatus;
        setTestRetentionSpec(testRetentionSpec);
        this.packageId = pkg.getId();
        this.permission = permission;
        this.brandLibraryId = (brandLibrary == null) ? null : brandLibrary.getId();
        this.productionRetentionSpecChanged = this.testRetentionSpecChanged = false;
    }
    
   
    public EsfUUID getId()
    {
        return id;
    }
    
    public String getProductionStatus()
    {
        return productionStatus;
    }
    
    public boolean isProductionEnabled()
    {
        return Literals.STATUS_ENABLED.equals(productionStatus);
    }
     
    public boolean isProductionDisabled()
    {
        return Literals.STATUS_DISABLED.equals(productionStatus);
    }
    
    public void makeProductionEnabled()
    {
    	productionStatus = Literals.STATUS_ENABLED;
        objectChanged();
    }
     
    public void makeProductionDisabled()
    {
    	productionStatus = Literals.STATUS_DISABLED;
        objectChanged();
    }
    
    public void setProductionStatus(String s)
    {
        if ( Literals.STATUS_ENABLED.equals(s) )
            makeProductionEnabled();
        else
            makeProductionDisabled();
    }
    
    public String getTimeIntervalUnitLabel(String unitValue)
    {
    	DropDown dd = DropDown.Manager.getByName(Library.Manager.getTemplate().getId(), Application.getInstance().getDropDownTimeIntervalUnitsEsfName());
    	if ( dd == null )
    		return null;
    	DropDownVersion ddv = dd.getProductionDropDownVersion();
    	if ( ddv == null )
    		return null;
    	for( DropDownVersionOption option : ddv.getOptions() )
    	{
    		if ( option.getValue().equals(unitValue) )
    			return option.getOption();
    	}
		return null;
    }
    
    static String getDatabaseDateIntervalSpec(int numUnits, String unit)
    {
    	if ( numUnits < 1 || Literals.TIME_INTERVAL_UNIT_FOREVER.equals(unit) ) // bad units or forever; jury rigged here since shouldn't actually be used for FORVER where no interval is actually used.
        	return "+ INTERVAL '1000 year'";
    	if ( Literals.TIME_INTERVAL_UNIT_YEAR.equals(unit) )
    		return "+ INTERVAL '" + numUnits + " year'"; 
    	if ( Literals.TIME_INTERVAL_UNIT_MONTH.equals(unit) )
    		return "+ INTERVAL '" + numUnits + " month'"; 
    	if ( Literals.TIME_INTERVAL_UNIT_WEEK.equals(unit) )
    		return "+ INTERVAL '" + numUnits + " week'"; 
    	if ( Literals.TIME_INTERVAL_UNIT_WEEKDAY.equals(unit) ) // no such interval available
    	{
    		int estimatedWeeks = (numUnits+4) / 5; // assuming every 5 weekdays is a real week, we'll add 2 days for ever week's worth of weekdays since generally better to holder longer than shorter
    		return "+ INTERVAL '" + (numUnits + (estimatedWeeks*2)) + " day'"; 
    	}
    	if ( Literals.TIME_INTERVAL_UNIT_DAY.equals(unit) )
    		return "+ INTERVAL '" + numUnits + " day'"; 
    	if ( Literals.TIME_INTERVAL_UNIT_HOUR.equals(unit) )
    		return "+ INTERVAL '" + numUnits + " hour'"; 
    	if ( Literals.TIME_INTERVAL_UNIT_MINUTE.equals(unit) )
    		return "+ INTERVAL '" + numUnits + " minute'"; 
    	if ( Literals.TIME_INTERVAL_UNIT_NOW.equals(unit) )
    		return ""; // no change
    	// Unexpected so we'll treat like forever
    	return "+ INTERVAL '1000 year'";
    }
    public String getProductionDatabaseDateIntervalSpec()
    {
    	return getDatabaseDateIntervalSpec(productionRetentionNumTimeIntervalUnits, productionRetentionTimeIntervalUnits);
    }
    public String getTestDatabaseDateIntervalSpec()
    {
    	return getDatabaseDateIntervalSpec(testRetentionNumTimeIntervalUnits, testRetentionTimeIntervalUnits);
    }
    
    public String getProductionRetentionSpec()
    {
    	return productionRetentionSpec;
    }
    public void setProductionRetentionSpec(String v)
    {
    	v = v == null ? null : v.trim();
    	if ( EsfString.isNonBlank(v) )
    	{
        	String[] parts = v.split(" ");
        	if ( parts.length == 2 )
        	{
        		int numUnits = Application.getInstance().stringToInt(parts[0],0);
        		if ( numUnits < 1 )
        			numUnits = 1;
        		String units = parts[1];
        		String label = getTimeIntervalUnitLabel(units);
        		String newSpec = numUnits + " " + units;
        		if ( label != null && ! newSpec.equals(productionRetentionSpec) ) // it must be one of our known units and it's different from before
        		{
        			productionRetentionNumTimeIntervalUnits = numUnits;
        			productionRetentionTimeIntervalUnits = units;
        			productionRetentionSpec = newSpec;
        	        productionRetentionSpecChanged = true;
                	objectChanged();
        		}
        	}
    	}
    }
    public boolean isProductionRetentionForever() 
    {
    	return Literals.TIME_INTERVAL_UNIT_FOREVER.equals(productionRetentionTimeIntervalUnits);
    }
    public boolean isProductionRetentionNow() 
    {
    	return Literals.TIME_INTERVAL_UNIT_NOW.equals(productionRetentionTimeIntervalUnits);
    }
    public EsfDateTime getProductionRetentionDateTimeFromNow()
    {
    	EsfDateTime retentionDate = new EsfDateTime();
    	retentionDate.addTimeInterval(productionRetentionNumTimeIntervalUnits, productionRetentionTimeIntervalUnits);
    	return retentionDate;
    }
    public int getProductionRetentionNumTimeIntervalUnits() 
    {
    	return productionRetentionNumTimeIntervalUnits;
    }
    public String getProductionRetentionTimeIntervalUnits() 
    {
    	return productionRetentionTimeIntervalUnits;
    }
    
    public String getTestStatus()
    {
        return testStatus;
    }
    
    public boolean isTestEnabled()
    {
        return Literals.STATUS_ENABLED.equals(testStatus);
    }
     
    public boolean isTestDisabled()
    {
        return Literals.STATUS_DISABLED.equals(testStatus);
    }
    
    public void makeTestEnabled()
    {
    	testStatus = Literals.STATUS_ENABLED;
        objectChanged();
    }
     
    public void makeTestDisabled()
    {
    	testStatus = Literals.STATUS_DISABLED;
        objectChanged();
    }
    
    public void setTestStatus(String s)
    {
        if ( Literals.STATUS_ENABLED.equals(s) )
            makeTestEnabled();
        else
            makeTestDisabled();
    }
    
    public String getTestRetentionSpec()
    {
    	return testRetentionSpec;
    }
    public void setTestRetentionSpec(String v)
    {
    	v = v == null ? null : v.trim();
    	if ( EsfString.isNonBlank(v) )
    	{
        	String[] parts = v.split(" ");
        	if ( parts.length == 2 )
        	{
        		int numUnits = Application.getInstance().stringToInt(parts[0],0);
        		if ( numUnits < 1 )
        			numUnits = 1;
        		String units = parts[1];
        		String label = getTimeIntervalUnitLabel(units);
        		String newSpec = numUnits + " " + units;
        		if ( label != null && ! newSpec.equals(testRetentionSpec) ) // it must be one of our known units and it's different from before
        		{
        			testRetentionNumTimeIntervalUnits = numUnits;
        			testRetentionTimeIntervalUnits = units;
        			testRetentionSpec = newSpec;
        	        testRetentionSpecChanged = true;
        	        objectChanged();
        		}
        	}
    	}
    }
    public boolean isTestRetentionForever() 
    {
    	return Literals.TIME_INTERVAL_UNIT_FOREVER.equals(testRetentionTimeIntervalUnits);
    }
    public EsfDateTime getTestRetentionDateTimeFromNow()
    {
    	EsfDateTime retentionDate = new EsfDateTime();
    	retentionDate.addTimeInterval(testRetentionNumTimeIntervalUnits, testRetentionTimeIntervalUnits);
    	return retentionDate;
    }
    public int getTestRetentionNumTimeIntervalUnits() 
    {
    	return testRetentionNumTimeIntervalUnits;
    }
    public String getTestRetentionTimeIntervalUnits() 
    {
    	return testRetentionTimeIntervalUnits;
    }
    
    public EsfPathName getPathName()
    {
        return pathName;
    }
    public void setPathName(EsfPathName v)
    {
    	// Block setting a transaction template name to null, an invalid EsfPathName or if the template current is a reserved name or the new name would be reserved
    	if ( v == null || ! v.isValid() || v.hasReservedPathPrefix() || (pathName != null && pathName.hasReservedPathPrefix()) )
    		return;
        pathName = v;
        objectChanged();
    }
    public EsfPathName getOriginalPathName()
    {
        return originalPathName;
    }
    public void resetOriginalPathName()
    {
    	originalPathName = pathName.duplicate();
    }
    
    public boolean isReserved()
    {
    	return getPathName().hasReservedPathPrefix();
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}
	
	public String getDisplayName()
	{
		return displayName;
	}
	public void setDisplayName(String v)
	{
    	if ( v == null )
    		displayName = pathName.toString();
    	else if ( v.length() > Literals.DISPLAY_NAME_MAX_LENGTH )
    		displayName = v.substring(0,Literals.DISPLAY_NAME_MAX_LENGTH).trim();
        else
        	displayName = v.trim();
        objectChanged();
	}

	public String getDescription()
    {
        return description;
    }
    public void setDescription(String v)
    {
    	if ( v == null )
    		description = v;
    	else if ( v.length() > Literals.DESCRIPTION_MAX_LENGTH )
            description = v.substring(0,Literals.DESCRIPTION_MAX_LENGTH).trim();
        else
        	description = v.trim();
        objectChanged();
    }
    
    public String getComments()
    {
    	return comments;
    }
    public void setComments(String v)
    {
    	if ( v == null )
    		comments = v;
    	else if ( v.length() > Literals.COMMENTS_MAX_LENGTH )
    		comments = v.substring(0,Literals.COMMENTS_MAX_LENGTH).trim();
        else
        	comments = v.trim();
        objectChanged();
    }

    public EsfDateTime getCreatedTimestamp()
    {
        return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
        return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
        return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
        return lastUpdatedByUserId;
    }
        
    public EsfUUID getPackageId()
    {
    	return packageId;
    }
    public Package getPackage()
    {
    	return Package.Manager.getById(packageId);
    }
    public void setPackageId(EsfUUID v)
    {
    	packageId = v;
    	objectChanged();
    }
    
    public EsfUUID getBrandLibraryId()
    {
    	return brandLibraryId;
    }
    public boolean hasBrandLibraryId()
    {
    	return brandLibraryId != null && ! brandLibraryId.isNull();
    }
    public Library getBrandLibrary()
    {
		return hasBrandLibraryId() ? Library.Manager.getById(brandLibraryId) : null;
    }
    public void setBrandLibraryId(EsfUUID v)
    {
    	brandLibraryId = v;
    	objectChanged();
    }
    
    public String getProductionStartUrl()
    {
    	return getStartUrl(true,false);
    }
    
    public String getTestStartUrl()
    {
    	return getStartUrl(false,false);
    }
    
    public String getTestLikeProductionStartUrl()
    {
    	return getStartUrl(false,true);
    }
    
    public String getStartUrl(boolean forProduction, boolean isLikeProduction )
    {
    	return Application.getInstance().getContextPath() + "/S/" + getPathName().toHtml() + "?tid=" + getId().toHtml() + (forProduction ? "" : "&"+StartTransaction.PARAM_ESFTEST+"=Yes" ) + (! forProduction && isLikeProduction ? "&"+StartTransaction.PARAM_ESFLIKEPRODUCTION+"=Yes" : "");
    }

    public String getExternalStartUrl(String externalContextPath, boolean forProduction, boolean forTest)
    {
    	return getExternalStartUrl(externalContextPath,forProduction,forTest,null);
    }

    public String getExternalStartUrl(String externalContextPath, boolean forProduction, boolean forTest, String sessionid)
    {
    	String params, jsessionid;
    	
    	if ( forTest && forProduction )
    		params = "?"+StartTransaction.PARAM_ESFTEST+"=Yes&"+StartTransaction.PARAM_ESFLIKEPRODUCTION+"=Yes";
    	else if ( forTest )
    		params = "?"+StartTransaction.PARAM_ESFTEST+"=Yes";
    	else
    		params = "";
    	
    	if ( EsfString.isNonBlank(sessionid) )
    		jsessionid = ";jsessionid=" + sessionid;
    	else
    		jsessionid = "";
    	
		return externalContextPath + "/S/" + getPathName().toHtml() + jsessionid + params;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof TransactionTemplate )
            return getId().equals(((TransactionTemplate)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(TransactionTemplate o)
    {
    	return getPathName().compareTo(o.getPathName());
    }
    
    @Override
    public boolean canUserList(User user)
    {
		return permission.canUserList(user);
    }
    
    public final boolean hasPermission(User user, EsfName permOptionName)
    {
    	return permission.hasPermission(user, permOptionName);
    }
    public final boolean hasStartPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.TRAN_PERM_OPTION_START);
    }
    public final boolean hasCancelPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.TRAN_PERM_OPTION_CANCEL);
    }
    public final boolean hasReactivatePermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.TRAN_PERM_OPTION_REACTIVATE);
    }
    public final boolean hasSuspendPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.TRAN_PERM_OPTION_SUSPEND);
    }
    public final boolean hasResumePermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.TRAN_PERM_OPTION_RESUME);
    }
    public final boolean hasUseUpdateApiPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.TRAN_PERM_OPTION_USE_UPDATE_API);
    }
    public final boolean hasUseAdminApiPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.TRAN_PERM_OPTION_USE_ADMIN_API);
    }
    
    public final Collection<Group> getPermissionAllowedGroups(User user, EsfName permOptionName)
    {
    	return permission.getAllowedGroups(user, permOptionName);
    }
    public final boolean addPermissionAllowedGroups(EsfName permOptionName, Group group)
    {
    	return permission.addGroupToOptions(group, permOptionName) > 0;
    }
    public final boolean removePermissionAllowedGroups(EsfName permOptionName, Group group)
    {
    	return permission.removeGroupFromOptions(group, permOptionName) > 0;
    }
    
    public final boolean _dbsetup_add_UseUpdateApiPermissionOption15_7_11()
    {
    	return permission._dbsetup_createOptionByNameWithNoGroups(PermissionOption.TRAN_PERM_OPTION_USE_UPDATE_API, PermissionOption.TRAN_PERM_OPTION_USE_UPDATE_API + " - 15.7.11 update");
    }
    public final boolean _dbsetup_add_UseAdminApiPermissionOption15_8_23()
    {
    	return permission._dbsetup_createOptionByNameWithNoGroups(PermissionOption.TRAN_PERM_OPTION_USE_ADMIN_API, PermissionOption.TRAN_PERM_OPTION_USE_ADMIN_API + " - 15.8.23 update");
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
 
    @Override
    protected void setLoadedFromDb()
    {
    	super.setLoadedFromDb();
    	this.productionRetentionSpecChanged = this.testRetentionSpecChanged = false;
    }
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; pathName: " + pathName + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
            	
            	permission.save(con);
            	
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_transaction_template " +
                	"(id,path_name,display_name,description,comments,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,production_status,production_retention_spec,test_status,test_retention_spec,package_id,brand_library_id) " + 
                	"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(pathName);
                stmt.set(displayName);
                stmt.set(description);
                stmt.set(comments);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(productionStatus);
                stmt.set(productionRetentionSpec);
                stmt.set(testStatus);
                stmt.set(testRetentionSpec);
                stmt.set(packageId);
                stmt.set(brandLibraryId);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; pathName: " + pathName);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this); // we use replace here because on create, we add to ID-only cache
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new transaction template " + getPathName()); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new transaction template " + getPathName());
                }
                
                return true;
            }
            
            if ( permission.hasChanged() )
            	permission.save(con);

            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_transaction_template SET path_name=?,display_name=?,description=?,comments=?,production_status=?,production_retention_spec=?,test_status=?,test_retention_spec=?,last_updated_timestamp=?,last_updated_by_user_id=?,package_id=?,brand_library_id=? WHERE id=?"
                						   );
                stmt.set(pathName);
                stmt.set(displayName);
                stmt.set(description);
                stmt.set(comments);
                stmt.set(productionStatus);
                stmt.set(productionRetentionSpec);
                stmt.set(testStatus);
                stmt.set(testRetentionSpec);
            	stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(packageId);
                stmt.set(brandLibraryId);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for transaction template id: " + id + "; pathName: " + pathName);
                }
            }

            if ( testRetentionSpecChanged )
            {
            	String dbIntervalSpec = getTestDatabaseDateIntervalSpec();
                int num = Transaction.Manager.resetExpireDatesRetention(con,this,false);
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Updated transaction template " + getPathName() + 
                			"; changed TEST retention spec to: " + getTestRetentionSpec() + " (DB date interval spec: " + dbIntervalSpec + " updated " + num + " TEST transactions).");
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated transaction template " + getPathName() + 
                			"; changed TEST retention spec to: " + getTestRetentionSpec() + " (DB date interval spec: " + dbIntervalSpec + " updated " + num + " TEST transactions).");
                }
            }
            if ( productionRetentionSpecChanged )
            {
            	String dbIntervalSpec = getProductionDatabaseDateIntervalSpec();
            	int num = Transaction.Manager.resetExpireDatesRetention(con,this,true);
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Updated transaction template " + getPathName() + 
                			"; changed PRODUCTION retention spec to: " + getProductionRetentionSpec() + " (DB date interval spec: " + dbIntervalSpec + " updated " + num + " PRODUCTION transactions).");
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated transaction template " + getPathName() + 
                			"; changed PRODUCTION retention spec to: " + getProductionRetentionSpec() + " (DB date interval spec: " + dbIntervalSpec + " updated " + num + " PRODUCTION transactions).");
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated transaction template " + getPathName() + "; prodStatus: " + getProductionStatus() + "; testStatus: " + getTestStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated transaction template " + getPathName() + "; prodStatus: " + getProductionStatus() + "; testStatus: " + getTestStatus()); 
            }
            
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; pathName: " + pathName + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    private synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; pathName: " + pathName + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use by a transaction
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of transaction template that was pending an INSERT id: " + id + "; pathName: " + pathName);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
            // Delete the template
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_transaction_template WHERE id=?");
            stmt.set(id);
            int num = stmt.executeUpdate();
            if ( num == 0 )
            	_logger.warn("delete(con) - Failed to find esf_transaction_template database row to delete with id: " + id);
            
            permission.delete(con);
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted transaction template " + getPathName() + "; prodStatus: " + getProductionStatus() + "; testStatus: " + getTestStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted transaction template " + getPathName() + "; prodStatus: " + getProductionStatus() + "; testStatus: " + getTestStatus()); 
            }
            
            Transaction.Manager.deleteAllByTransactionTemplate(con,id); // presumably only test transactions are deleted by this

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on id: " + id + "; pathName: " + pathName + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }
    
    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
		// We never allow the reserved transaction template to be deleted
		if ( isReserved() )
		{
	    	_logger.error("checkReferential() FAILED RESERVE PATH PREFIX on id: " + id + "; pathName: " + pathName);
	    	if ( errors != null )
	    		errors.addError("You cannot delete the reserved special transaction template: " + getPathName());
			return false;
		}

	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT COUNT(*) FROM esf_transaction WHERE transaction_template_id=? AND tran_type=?"
	                                   );
	        stmt.set(id);
	        stmt.set(Transaction.TRAN_TYPE_PRODUCTION);
	        EsfResultSet rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	int count = rs.getInt();
	        	if ( count > 0 )
	        	{
		        	errors.addError("This transaction template is referenced by " + count + " production transactions.");
		        	return false;
	        	}
	        }
	        stmt.close();
	        
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT COUNT(*) FROM esf_report_template_transaction_template WHERE transaction_template_id=?"
	                                   );
	        stmt.set(id);
	        rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	int count = rs.getInt();
	        	if ( count > 0 )
	        	{
		        	errors.addError("This transaction template is referenced by " + count + " report template(s).");
		        	return false;
	        	}
	        }

	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   	    private static PathNameUUIDUserListableCacheReadOptimized<TransactionTemplate> cache = new PathNameUUIDUserListableCacheReadOptimized<TransactionTemplate>();
 
		public static Comparator<TransactionTemplate> ComparatorByDisplayName = new Comparator<TransactionTemplate>() {
		    public int compare(TransactionTemplate tt1, TransactionTemplate tt2) {
		    	int diff = tt1.getDisplayName().compareToIgnoreCase(tt2.getDisplayName());
		    	if ( diff == 0 )
		    		diff = tt1.getPathName().toString().compareToIgnoreCase(tt2.getPathName().toString());
		        return diff; 
		    }
		};
   	    

   	    private static TransactionTemplate getById(Connection con, EsfUUID tranTemplateId) 
   	    	throws SQLException
   	    {
   	    	EsfPreparedStatement stmt = null;
   	    	
   	        try
   	        {
   	            stmt = new EsfPreparedStatement( con,
   	    	            "SELECT path_name,display_name,description,comments,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,production_status,production_retention_spec,test_status,test_retention_spec,package_id,brand_library_id " +
   	    	            "FROM esf_transaction_template WHERE id = ?"
   	            						   );
   	            stmt.set(tranTemplateId);
   	            
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
		            EsfPathName pathName = rs.getEsfPathName();
		            String displayName = rs.getString();
		            String description = rs.getString();
		            String comments = rs.getString();
		            EsfDateTime created = rs.getEsfDateTime();
		            EsfUUID createdBy   = rs.getEsfUUID();
		            EsfDateTime lastUpdated = rs.getEsfDateTime();
		            EsfUUID lastUpdatedBy   = rs.getEsfUUID();
		            String  prodStat = rs.getString();
		            String  prodRetentionSpec = rs.getString();
		            String  testStat = rs.getString();
		            String  testRetentionSpec = rs.getString();
		            EsfUUID packageId = rs.getEsfUUID();
		            EsfUUID brandLibraryId = rs.getEsfUUID();
		            
		        	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+tranTemplateId.toNormalizedEsfNameString());
		        	Permission permission = Permission.Manager.getByPathName(permPathName);
		        	
		        	TransactionTemplate template = new TransactionTemplate(tranTemplateId,pathName,displayName,description,comments,created,createdBy,lastUpdated,lastUpdatedBy,prodStat,prodRetentionSpec,testStat,testRetentionSpec,packageId,permission,brandLibraryId);
		            
		            template.setLoadedFromDb();
		            cache.add(template);
		            
		            return template;
	            }
   	        }
   	        finally
   	        {
   	        	cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + tranTemplateId + "; failed to find the transaction template");
   	        return null; 
   	    }

   	    /**
   	     * Returns the Template transaction template 
   	     * @return the TransactionTemplate that represents the Template
   	     */
   	    public static TransactionTemplate getTemplate()
   	    {
   	    	return getByPathName(TEMPLATE_ESFPATHNAME);
   	    }
   	     
   	    /**
   	     * 
   	     * Retrieves a transaction template object based on the supplied path name.
   	     * @param pathName the EsfPathName name of the template to retrieve
   	     * @return the TransactionTemplate object as loaded from the database if found; else null
   	     */
   	    public static TransactionTemplate getByPathName(final EsfPathName pathName)
   	    {
   	    	if ( pathName == null || ! pathName.isValid() )
   	    		return null;

   	    	_logger.debug("Manager.getByPathName() for pathName: " + pathName);   	    	
   	    	
   	    	TransactionTemplate template = cache.getByPathName(pathName);
   	    	if ( template != null )
   	    		return template;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT id FROM esf_transaction_template WHERE lower(path_name)=?"
   	        									);
   	        	stmt.set(pathName.toLowerCase());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            con.commit();
	   	            
	   	            return getById(id);
	            }
	            
   	            con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByPathName() - transaction pathName: " + pathName);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        _logger.debug("Manager.getByPathName() - transaction pathName: " + pathName + "; failed to find the transaction template");
   	        return null; 
   	    }
   	    
   	    /**
   	     * 
   	     * Retrieves a TransactionTemplate object based on the supplied pathName.
   	     * @param esfname the EsfPathName pathName of the template to retrieve
   	     * @param u the User to limit visibility list to
   	     * @return the TransactionTemplate object as loaded from the database if found; else null
   	     */
   	    public static TransactionTemplate getByPathName(final EsfPathName pathName, final User u)
   	    {
   	    	_logger.debug("Manager.getByPathName() for pathName: " + pathName + "; for user: " + u.getEmail());
   	    	TransactionTemplate template = cache.getByPathName(pathName,u);
   	    	if ( template != null )
   	    		return template;
   	    	
   	    	template = getByPathName(pathName);
   	    	return (template != null && template.canUserList(u)) ? template : null;
   	    }
   	    
   	    
   	    /**
   	     * 
   	     * Retrieves a TransactionTemplate object based on the supplied id.
   	     * @param id the EsfUUID template id to retrieve
   	     * @return the TransactionTemplate object as loaded from the database if found; else null
   	     */
   	    public static TransactionTemplate getById(final EsfUUID id)
   	    {
   	    	TransactionTemplate template = cache.getById(id);
   	    	if ( template != null )
   	    		return template;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	template = getById(con,id);
   	        	if ( template != null ) 
   	        	{
		            con.commit();
	   	        	return template;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null;
   	    }
   	    
   	    /**
   	     * 
   	     * Retrieves a TransactionTemplate object based on the supplied id.
   	     * @param id the EsfUUID template id to retrieve
   	     * @param u the User to limit visibility to; user must have list/start permission
   	     * @return the TransactionTemplate object as loaded from the database if found; else null
   	     */
   	    public static TransactionTemplate getByIdForList(final EsfUUID id, final User u)
   	    {
   	    	return getById(id,u,true,false);
   	    }
   	    public static TransactionTemplate getByIdForListOrStart(final EsfUUID id, final User u)
   	    {
   	    	return getById(id,u,true,true);
   	    }
   	    public static TransactionTemplate getByIdForStart(final EsfUUID id, final User u)
   	    {
   	    	return getById(id,u,false,true);
   	    }
   	    
   	    public static TransactionTemplate getById(final EsfUUID id, final User u, boolean includeList, boolean includeStart)
   	    {
   	    	_logger.debug("Manager.getById() for id: " + id + "; for user: " + u.getFullDisplayName() + "; include list: " + includeList + "; include start: " + includeStart);
   	    	TransactionTemplate template = cache.getById(id);
   	    	if ( template != null )
   	    		return ((includeList && template.canUserList(u)) || (includeStart && template.hasStartPermission(u))) ? template : null;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	template = getById(con,id);
   	        	if ( template != null ) 
   	        	{
		            con.commit();
		            return ((includeList && template.canUserList(u)) || (includeStart && template.hasStartPermission(u))) ? template : null;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null;
   	    }
   	    
   		public static List<TransactionTemplate> getForUserWithPermission(User user, EsfName permOptionName)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<TransactionTemplate> list = new LinkedList<TransactionTemplate>();
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT DISTINCT ON (lower(TT.path_name)) TT.id,TT.path_name " +
   	        			"FROM esf_transaction_template TT, esf_permission_option_group PERM " +
   	        			"WHERE PERM.permission_id = TT.id AND lower(PERM.permission_option_esfname)=? AND " +
   	        			"( PERM.group_id=? OR PERM.group_id IN (SELECT GU.group_id FROM esf_group_user GU WHERE GU.user_id=?) ) " +
   	        			"ORDER BY lower(TT.path_name) ASC"
   	        									);
   	        	stmt.set(permOptionName.toLowerCase());
   	        	stmt.set(Group.Manager.getAllUsersGroup().getId());
   	        	stmt.set(user.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	   	            
	            	TransactionTemplate tt = getById(id);
	   	            if ( tt != null )
	   	            	list.add(tt);
	            }
	            
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getForUserWithPermission() - userId: " + user.getId() + "; permOptionName: " + permOptionName);
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<TransactionTemplate> getByPackageForUserWithPermission(EsfUUID packageId, User user, EsfName permOptionName)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<TransactionTemplate> list = new LinkedList<TransactionTemplate>();
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT DISTINCT ON (lower(TT.path_name)) TT.id,TT.path_name " +
   	        			"FROM esf_transaction_template TT, esf_permission_option_group PERM " +
   	        			"WHERE TT.package_id=? AND PERM.permission_id = TT.id AND lower(PERM.permission_option_esfname)=? AND " +
   	        			"( PERM.group_id=? OR PERM.group_id IN (SELECT GU.group_id FROM esf_group_user GU WHERE GU.user_id=?) ) " +
   	        			"ORDER BY lower(TT.path_name) ASC"
   	        									);
   	        	stmt.set(packageId);
   	        	stmt.set(permOptionName.toLowerCase());
   	        	stmt.set(Group.Manager.getAllUsersGroup().getId());
   	        	stmt.set(user.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	   	            
	            	TransactionTemplate tt = getById(id);
	   	            if ( tt != null )
	   	            	list.add(tt);
	            }
	            
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByPackageForUserWithPermission() - packageId: " + packageId + "; userId: " + user.getId() + "; permOptionName: " + permOptionName);
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		
   		/**
   		 * Normally this should not be used.  But we do use it in our dbsetup to handle a version update.
   		 * @return list of all Transaction Templates
   		 */
   		public static List<TransactionTemplate> _dbsetup_getAll()
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<TransactionTemplate> list = new LinkedList<TransactionTemplate>();
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_transaction_template" );
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	   	            
	            	TransactionTemplate tt = getById(id);
	   	            if ( tt != null )
	   	            	list.add(tt);
	            }
	            
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll()");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		

   		
   	    /**
   	     * Creates a new transaction template like an existing one.  The new template is automatically added to all of
   	     * the permission lists (it can always list/view/add/change/delete itself unless changed by the creator or others with permission).
   	     * @param likeTemplate the TransactionTemplate that we'll be cloning from
   	     * @param esfname the new template's name
   	     * @param createdBy the User who is creating the new template
   	     * @return
   	     */
   	    public static TransactionTemplate createLike(TransactionTemplate likeTemplate, EsfPathName pathName, User createdBy)
   	    {
   	    	// We don't let you create new reserved objects
   	    	if ( pathName.hasReservedPathPrefix() )
   	    		return null;

   	    	if ( ! likeTemplate.permission.hasPermission(createdBy, PermissionOption.PERM_OPTION_CREATELIKE) )
   	    		return null;
   	    	
   	        EsfUUID id = new EsfUUID();
   	        EsfDateTime nowTimestamp = new EsfDateTime();
   	        
   	        String normalizedId = id.toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+normalizedId);
   	        Permission newPermission = Permission.Manager.createLike(id, permPathName, likeTemplate.permission);

   	        // Note that the template's permissions can contain groups that even the creator doesn't have access to, but
   	        // they remain in the new template (so they can have permission) and the user can't even see them so 
   	        // cannot modify them away.
   	        TransactionTemplate template = new TransactionTemplate( id, pathName, likeTemplate.getDisplayName(), likeTemplate.getDescription(), likeTemplate.getComments(), 
   	    							 	   nowTimestamp, createdBy.getId(), nowTimestamp, createdBy.getId(),
   	    							 	   likeTemplate.getProductionStatus(), likeTemplate.getProductionRetentionSpec(), likeTemplate.getTestStatus(), likeTemplate.getTestRetentionSpec(),
   	    							 	   likeTemplate.getPackageId(), newPermission, likeTemplate.getBrandLibraryId()
   	    						   		 );
   	        cache.addIdOnly(template);
   	    	return template;
   	    }
   	    
   	    /**
   	     * Creates a new test transaction template.  The new template is automatically added to all of
   	     * the permission lists (it can always list/view/add/change/delete itself unless changed by the creator or others with permission).
   	     * @param likeTemplate the TransactionTemplate that we'll be cloning from
   	     * @param esfname the new template's name
   	     * @param createdBy the User who is creating the test template
   	     * @return
   	     */
   	    public static TransactionTemplate createForTestDocument(Package pkg, Library brandLibrary, User createdBy)
   	    {
   	        EsfUUID id = new EsfUUID();
   	        String normalizedId = id.toNormalizedEsfNameString();

   	        EsfPathName pathName = new EsfPathName("TestTransactionTemplate"+EsfPathName.PATH_SEPARATOR+normalizedId);
   	        EsfDateTime nowTimestamp = new EsfDateTime();

   	    	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+normalizedId);
   	    	Permission newPermission = Permission.Manager._dbsetup_createTransactionTemplateForSuperGroup(id, permPathName, "Template transaction template permissions", Group.Manager.getSuperGroup());
   	    	newPermission.addGroupToOptions(Group.Manager.getAllUsersGroup(), PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS, 
   	    										 PermissionOption.TRAN_PERM_OPTION_START,
   	    										 PermissionOption.TRAN_PERM_OPTION_SUSPEND, PermissionOption.TRAN_PERM_OPTION_RESUME, 
   	    										 PermissionOption.TRAN_PERM_OPTION_CANCEL, PermissionOption.TRAN_PERM_OPTION_REACTIVATE);

   	        TransactionTemplate template = new TransactionTemplate( id, pathName, pathName.toString(), "Test transaction template", null,
   	    							 	   nowTimestamp, createdBy.getId(), nowTimestamp, createdBy.getId(),
   	    							 	   Literals.STATUS_DISABLED, "0 forever", Literals.STATUS_ENABLED, "0 forever", 
   	    							 	   pkg, newPermission, brandLibrary
   	    						   		 						  );
   	        cache.add(template); // do a regular cache add since won't be saving this one
   	    	return template;
   	    }
   	    
   	    // Only called by DbSetup
   	    public static TransactionTemplate _dbsetup_createTemplate(Group superGroup, Group systemAdminGroup)
   	    {
   	        EsfDateTime nowTimestamp = new EsfDateTime();
   	        
   	        EsfUUID deployId = Application.getInstance().getDeployId();

   	        TransactionTemplate template = new TransactionTemplate( new EsfUUID(), TEMPLATE_ESFPATHNAME, "Template only", "Sample transaction template.", null, nowTimestamp, deployId, 
   	    							       nowTimestamp, deployId, Literals.STATUS_DISABLED, "1 forever", Literals.STATUS_DISABLED, "3 month",  
   	    							       Package.Manager.getTemplate().getId(), null, null );
   	        template.setComments("The transaction template used as the root for creating all other runnable transactions (package with run-time permissions and branding libraries). Admins generally create a template for each company/group so they can create their own transaction templates with similar permission.");

   	    	String normalizedId = template.getId().toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+normalizedId);
   	    	template.permission = Permission.Manager._dbsetup_createTransactionTemplateForSuperGroup(template.getId(), permPathName, "Sample transaction template permissions", superGroup);
   	    	template.permission.addGroupToAllOptions(systemAdminGroup);
   	    	// We don't want anybody to have update or admin api permissions by default
   	    	template.permission.removeGroupFromOptions( systemAdminGroup, PermissionOption.TRAN_PERM_OPTION_USE_UPDATE_API);
   	    	template.permission.removeGroupFromOptions( systemAdminGroup, PermissionOption.TRAN_PERM_OPTION_USE_ADMIN_API);
   	        cache.addIdOnly(template);
   	    	return template;
   	    }
   	    

   	    /**
   	     * Returns the total number of transaction templates objects in the cache
   	     * @return the total number of transaction templates in the cache
   	     */
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all packages that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}
   	    
   	} // Manager
   	
}