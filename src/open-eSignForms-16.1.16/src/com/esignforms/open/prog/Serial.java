// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.User;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* Serial defines a test and production serial number in a library. The actual serial numbers don't vary by version, but the format can.
* It can be overridden by name in a branding library.
* 
* @author Yozons, Inc.
*/
public class Serial
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<Serial>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable
{
	private static final long serialVersionUID = 6989134236264932553L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(Serial.class);

    protected final EsfUUID id;
    protected final EsfUUID containerId;
    protected EsfName esfname;
    protected EsfName originalEsfName;
    protected String  description;
    protected String  comments;
    protected String  status;
    protected int	  productionVersion;
    protected int	  testVersion;

    protected EsfInteger  nextProductionSerial;
    protected EsfInteger  nextTestSerial;

    protected EsfDateTime lastAccessFromCache = new EsfDateTime();

    /**
     * This creates a Serial object from data retrieved from the DB.
     */
    protected Serial(EsfUUID id, EsfUUID containerId, EsfName esfname, String description, String comments, 
    				 String status, int productionVersion, int testVersion, 
    				 EsfInteger nextProductionSerial, EsfInteger nextTestSerial)
    {
        this.id = id;
        this.containerId = containerId;
        this.esfname = esfname;
        this.originalEsfName = esfname.duplicate();
        this.description = description;
        this.comments = comments;
        this.status = status;
        this.productionVersion = productionVersion;
        this.testVersion = testVersion;
        
        this.nextProductionSerial = nextProductionSerial;
        this.nextTestSerial = nextTestSerial;
    }
    
    protected Serial(EsfUUID containerId)
    {
        this.id = new EsfUUID();
        this.containerId = containerId;
        this.esfname = new EsfName("NewEmptySerial_PleaseRename");
        this.originalEsfName = esfname.duplicate();
        this.description = null;
        this.comments = null;
        this.status = Literals.STATUS_ENABLED;
        this.productionVersion = 0;
        this.testVersion = 1;

        this.nextProductionSerial = new EsfInteger(0);
        this.nextTestSerial = new EsfInteger(0);
    }

   
    public Serial duplicate()
    {
    	Serial s = new Serial(id, containerId, esfname.duplicate(), description, comments,
    						status, productionVersion, testVersion, nextProductionSerial.duplicate(), nextTestSerial.duplicate());
    	s.setDatabaseObjectLike(this);
    	return s;
    }

   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getContainerId()
    {
        return containerId;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }
    
    public void makeEnabled()
    {
        status = Literals.STATUS_ENABLED;
        objectChanged();
    }
     
    public void makeDisabled()
    {
        status = Literals.STATUS_DISABLED;
        objectChanged();
    }
    
    public void setStatus(String s)
    {
        if ( Literals.STATUS_ENABLED.equals(s) )
            makeEnabled();
        else
            makeDisabled();
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    public void setEsfName(EsfName v)
    {
    	// Block setting a document name to null or an invalid EsfName
    	if ( v == null || ! v.isValid() )
    		return;
        esfname = v;
        objectChanged();
    }
    public EsfName getOriginalEsfName()
    {
        return originalEsfName;
    }
    public void resetOriginalEsfName()
    {
    	originalEsfName = esfname.duplicate();
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}
    
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String v)
    {
    	if ( v == null )
    		description = v;
    	else if ( v.length() > Literals.DESCRIPTION_MAX_LENGTH )
            description = v.substring(0,Literals.DESCRIPTION_MAX_LENGTH).trim();
        else
        	description = v.trim();
        objectChanged();
    }

    public String getComments()
    {
    	return comments;
    }
    public void setComments(String v)
    {
    	if ( v == null )
    		comments = v;
    	else if ( v.length() > Literals.COMMENTS_MAX_LENGTH )
    		comments = v.substring(0,Literals.COMMENTS_MAX_LENGTH).trim();
        else
        	comments = v.trim();
        objectChanged();
    }
    
    public String getVersionLabel(int checkVersion)
    {
    	if ( checkVersion > productionVersion )
    		return Literals.VERSION_LABEL_TEST;
    	
    	if ( checkVersion == productionVersion )
    		return Literals.VERSION_LABEL_PRODUCTION;
    	
		return Literals.VERSION_LABEL_NOT_CURRENT;
    }

    public int getProductionVersion()
    {
    	return productionVersion;
    }
    public boolean hasProductionVersion() 
    {
    	return productionVersion > 0;
    }
    public SerialVersion getProductionSerialVersion()
    {
    	return hasProductionVersion() ? SerialVersion.Manager.getByVersion(id, productionVersion) : null;
    }
    
    public int getTestVersion()
    {
    	return testVersion;
    }
    public boolean hasTestVersion() 
    {
    	return testVersion > productionVersion;
    }
    public void promoteTestVersionToProduction()
    {
    	if ( hasTestVersion() )
    	{
        	productionVersion = testVersion;
        	objectChanged();
    	}
    }
    public void revertProductionVersionBackToTest()
    {
       	if ( hasProductionVersion() && ! hasTestVersion() ) 
    	{
       		--productionVersion;
    		objectChanged();
    	}
    }
    public void bumpTestVersion()
    {
    	if ( ! hasTestVersion() ) 
    	{
    		++testVersion;
    		objectChanged();
    	}
    }
    public void dropTestVersion()
    {
    	if ( hasTestVersion() ) 
    	{
    		--testVersion;
    		objectChanged();
    	}
    }
    public SerialVersion getTestSerialVersion() // may be the same as the production version 
    {
    	return SerialVersion.Manager.getByVersion(id, testVersion);
    }
    
    public EsfInteger getNextProductionSerial()
    {
    	return nextProductionSerial;
    }
    public void setNextProductionSerial(EsfInteger v)
    {
    	if ( doInsert() && v != null && v.isGreaterThanEqualTo(0) )
    	{
    		nextProductionSerial = v;
    		objectChanged();
    	}
    }
    public synchronized String nextFormattedProductionSerial()
    {
		SerialVersion ver = getProductionSerialVersion();
    	if ( ver != null )
    	{
    		EsfInteger nextSerial = Manager.nextSerial(id,true);
    		if ( nextSerial != null )
    		{
        		nextProductionSerial = new EsfInteger( nextSerial.toLong() + 1 );
        		return nextSerial.format(ver.getDecimalFormat());
    		}
    	}
    	return null;
    }
    
    public EsfInteger getNextTestSerial()
    {
    	return nextTestSerial;
    }
    public void setNextTestSerial(EsfInteger v)
    {
    	if ( doInsert() && v != null && v.isGreaterThanEqualTo(0) )
    	{
    		nextTestSerial = v;
    		objectChanged();
    	}
    }
    public synchronized String nextFormattedTestSerial()
    {
		SerialVersion ver = getTestSerialVersion();
    	if ( ver != null )
    	{
    		EsfInteger nextSerial = Manager.nextSerial(id,false);
    		if ( nextSerial != null )
    		{
        		nextTestSerial = new EsfInteger( nextSerial.toLong() + 1 );
        		return nextSerial.format(ver.getDecimalFormat());
    		}
    	}
    	return null;
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof Serial )
            return getId().equals(((Serial)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(Serial o)
    {
    	return getEsfName().compareTo(o.getEsfName());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<Serial xmlns=\"").append(XmlUtil.getXmlNamespace2012()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <containerId>").append(containerId.toXml()).append("</containerId>\n");
        buf.append(" <esfname>").append(esfname.toXml()).append("</esfname>\n");
       	buf.append(" <description>").append(escapeXml(description)).append("</description>\n");
        buf.append(" <comments>").append(escapeXml(comments)).append("</comments>\n");
        buf.append(" <status>").append(escapeXml(status)).append("</status>\n");
        buf.append(" <productionVersion>").append(productionVersion).append("</productionVersion>\n");
        buf.append(" <testVersion>").append(testVersion).append("</testVersion>\n");
        buf.append(" <nextProductionSerial>").append(nextProductionSerial.toXml()).append("</nextProductionSerial>\n");
        buf.append(" <nextTestSerial>").append(nextTestSerial.toXml()).append("</nextTestSerial>\n");
        
        buf.append("</Serial>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 302 + id.getEstimatedLengthXml() + containerId.getEstimatedLengthXml() + esfname.getEstimatedLengthXml() + 
        			status.length() + nextProductionSerial.getEstimatedLengthXml() + nextTestSerial.getEstimatedLengthXml();
        if ( description != null )
            len += description.length();
        if ( comments != null )
            len += comments.length();
        return len; 
    }

    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; esfname: " + esfname + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
            if ( doInsert() )
            {
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_serial (id,container_id,esfname,description,comments,status,production_version,test_version,next_production_serial,next_test_serial) VALUES (?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(containerId);
                stmt.set(esfname);
                stmt.set(description);
                stmt.set(comments);
                stmt.set(status);
                stmt.set(productionVersion);
                stmt.set(testVersion);
                stmt.set(nextProductionSerial);
                stmt.set(nextTestSerial);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; esfname: " + esfname);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this); // We use replace instead of 'add' because on object create, we added to the ID-only cache
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new serial " + getEsfName() + "; next production: " + nextProductionSerial + "; next test: " + nextTestSerial); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new serial " + getEsfName() + "; next production: " + nextProductionSerial + "; next test: " + nextTestSerial);
                }
                
                return true;
            }
            
            if ( hasChanged() )
            {
                // We assume we'll update it instead; can't update the serial numbers themselves
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_serial SET esfname=?,description=?,comments=?,status=?,production_version=?,test_version=? WHERE id=?"
                						   );
                stmt.set(esfname);
                stmt.set(description);
                stmt.set(comments);
                stmt.set(status);
            	stmt.set(productionVersion);
                stmt.set(testVersion);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for Serial id: " + id + "; esfname: " + esfname);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated serial " + getEsfName() + "; status; " + getStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated serial " + getEsfName() + "; status; " + getStatus());
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; esfname: " + esfname + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; esfname: " + esfname + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of serial that was pending an INSERT id: " + id + "; esfname: " + esfname);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
        	// Delete all our versions...
        	for( SerialVersion serialVer : SerialVersion.Manager.getAllBySerialId(con,id) )
        	{
        		if ( ! serialVer.delete(con,null,null) )
        		{
        			if ( errors != null )
        				errors.addError("Could not delete serial version id: " + serialVer.getId());
        			return false;
        		}
        	}
        	
            // Delete the Serial itself
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_serial WHERE id=?");
            stmt.set(id);
            int num = stmt.executeUpdate();
            if ( num == 0 )
            	_logger.warn("delete(con) - Failed to find esf_library_serial database row to delete with id: " + id);
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted serial " + getEsfName() + "; status; " + getStatus() + "; next production: " + nextProductionSerial + "; next test: " + nextTestSerial); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted serial " + getEsfName() + "; status; " + getStatus() + "; next production: " + nextProductionSerial + "; next test: " + nextTestSerial);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on id: " + id + "; esfname: " + esfname + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   	    private static UUIDCacheReadOptimized<Serial> cache = new UUIDCacheReadOptimized<Serial>();

   	    public static EsfInteger nextSerial(EsfUUID id, boolean doProductionResolve)
   		{
   	    	if ( id == null )
   	    		return null;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	String serialFieldName = doProductionResolve ? "next_production_serial" : "next_test_serial";
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT " + serialFieldName + " FROM esf_library_serial WHERE id=? FOR UPDATE"
   	        									);
   	        	stmt.set(id);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfInteger serial = rs.getEsfInteger();
	            	
	            	stmt.close();
	                stmt = new EsfPreparedStatement( con, 
	                		"UPDATE esf_library_serial SET " + serialFieldName + "=" + serialFieldName + "+1 WHERE id=?"
	                						   );
	                stmt.set(id);
	                int num = stmt.executeUpdate();
	                if ( num == 1 )
	                {
			            con.commit();		   	            
		   	            return serial;
	                }
	                
	                _logger.error("Manager.nextSerial() - id: " + id + "; doProductionResolve: " + doProductionResolve + "; retrieved serial: " + serial + "; but couldn't update to next value.");
	            }
	            
   	            con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.nextSerial() - id: " + id + "; doProductionResolve: " + doProductionResolve);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        _logger.debug("Manager.nextFormattedSerial() - id: " + id + "; doProductionResolve: " + doProductionResolve+ "; failed to find the Serial");
   	        return null; 
   		}
   		
   	    public static Serial getByName(EsfUUID containerId, EsfName esfname)
   	    {
   	    	if ( containerId == null || esfname == null || ! esfname.isValid() )
   	    		return null;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT id FROM esf_library_serial WHERE container_id = ? AND lower(esfname) = ?"
   	        									);
   	        	stmt.set(containerId);
   	        	stmt.set(esfname.toLowerCase());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            con.commit();
	   	            
	   	            return getById(id);
	            }
	            
   	            con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByName() - containerId: " + containerId + "; Serial: " + esfname);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        _logger.debug("Manager.getByName() - containerId: " + containerId + "; Serial: " + esfname + "; failed to find the Serial");
   	        return null; 
   	    }

   		static Serial getById(Connection con, EsfUUID serialId) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT container_id,esfname,description,comments,status,production_version,test_version,next_production_serial,next_test_serial " +
   	        			"FROM esf_library_serial WHERE id=?"
   	        									);
   	        	stmt.set(serialId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID containerId = rs.getEsfUUID();
		            EsfName esfname = rs.getEsfName();
		            String description = rs.getString();
		            String comments = rs.getString();
		            String  stat = rs.getString();
		            int productionVersion = rs.getInt();
		            int testVersion = rs.getInt();
		            EsfInteger nextProductionSerial = rs.getEsfInteger();
		            EsfInteger nextTestSerial = rs.getEsfInteger();
		            
		            Serial serial = new Serial(serialId,containerId,esfname,description,comments,stat,productionVersion,testVersion,nextProductionSerial,nextTestSerial);
		            serial.setLoadedFromDb();
		            cache.add(serial);
		            
	   	            return serial;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + serialId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + serialId + "; failed to find the serial");
   	        return null; 
   		}
   		
   		public static Serial getById(EsfUUID serialId)
   		{
   	    	Serial serial = cache.getById(serialId);
   	    	if ( serial != null )
   	    		return serial;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	serial = getById(con,serialId);
   	        	if ( serial != null ) 
   	        	{
		            con.commit();
	   	        	return serial;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   	    /**
   	     * Retrieves all Serial objects held in the specified container.
   	     * @return the List of Serial found in name order.
   	     */
   	    public static List<Serial> getAll(EsfUUID containerId)
   	    {
   	    	LinkedList<Serial> list = new LinkedList<Serial>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id FROM esf_library_serial WHERE container_id=? ORDER BY lower(esfname) ASC"
   	            						   );
   	        	stmt.set(containerId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            
	            	Serial serial = getById(id);
	            	
	            	list.add(serial);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - containerId: " + containerId + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - containerId: " + containerId + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	         list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static Serial createNew(EsfUUID containerId)
   	    {
   	    	if ( containerId == null )
   	    		return null;
   	    	Serial newSerial = new Serial(containerId);
   	    	cache.add(newSerial);
   	    	return newSerial;
   	    }

   	    public static Serial createLike(EsfUUID containerId, Serial likeSerial, EsfName newName)
   	    {
   	    	if ( containerId == null || likeSerial == null || newName == null || ! newName.isValid() )
   	    		return null;
   	    	
   	    	Serial newSerial = createNew(containerId);
   	    	newSerial.setEsfName(newName);
   	    	newSerial.setDescription( likeSerial.getDescription() );
   	    	newSerial.setComments( likeSerial.getComments() );
   	    	return newSerial;
   	    }
   	    
   	    public static Serial createFromJDOM(EsfUUID containerId, Element e)
   	    {
   	    	if ( containerId == null || e == null )
   	    		return null;
   	    	
   	    	Serial newSerial = new Serial(
   	    			new EsfUUID(), 
   	    			containerId, 
   	    			EsfName.createFromToXml(e.getChildText("esfname", e.getNamespace())),
   	    			e.getChildText("description", e.getNamespace()), 
   	    			e.getChildText("comments", e.getNamespace()),
   	    			e.getChildText("status", e.getNamespace()), 
   	    			0, 1, 
   	    			new EsfInteger(0), new EsfInteger(0)
   	    										);
   	    	newSerial.setNewObject();
   	    	cache.add(newSerial);
   	    	return newSerial;
   	    }

   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all files that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}

   	} // Manager
   	
}