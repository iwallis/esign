// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;

/**
* DropDownVersionInfo is a data-only object that holds information about a drop down version.
* It's not a mutable object (you can't save/delete it), just for lists of dropDown versions and such.
* 
* @author Yozons, Inc.
*/
public class DropDownVersionInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<DropDownVersionInfo>, java.io.Serializable
{
	private static final long serialVersionUID = 5321313453126093614L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DropDownVersionInfo.class);

    protected EsfUUID id;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    
    /**
     * This creates a new DropDownVersionInfo using the source object.
     * @param dropDownVersion the dropDown version to use as the source
     */
    protected DropDownVersionInfo(DropDownVersion dropDownVersion)
    {
        this.id = dropDownVersion.getId();
        this.version = dropDownVersion.getVersion();
        this.createdTimestamp = dropDownVersion.getCreatedTimestamp();
        this.createdByUserId = dropDownVersion.getCreatedByUserId();
        this.lastUpdatedTimestamp = dropDownVersion.getLastUpdatedTimestamp();
        this.lastUpdatedByUserId = dropDownVersion.getLastUpdatedByUserId();
    }
    
    /**
     * This creates a DropDownVersionInfo object from data retrieved from the DB.
     */
    protected DropDownVersionInfo(
    		EsfUUID id, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId)
    {
        this.id = id;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public int getVersion()
    {
    	return version;
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }

    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }

    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }

    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }


    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof DropDownVersionInfo )
            return getId().equals(((DropDownVersionInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(DropDownVersionInfo d)
    {
    	return getVersion() - d.getVersion();
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all DropDownVersionInfo objects in the specified library.
   	     * @return the Collection of DropDownVersionInfo found with most recent versions first.
   	     */
   	    public static Collection<DropDownVersionInfo> getAll(DropDown dropDown)
   	    {
   	    	LinkedList<DropDownVersionInfo> list = new LinkedList<DropDownVersionInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id, version, created_timestamp, created_by_user_id, last_updated_timestamp, last_updated_by_user_id " +
   	    	            "FROM esf_library_dropdown_version " + 
   	    	            "WHERE library_dropdown_id = ? " +
   	    	            "ORDER BY version DESC"
   	            						   );
   	        	stmt.set(dropDown.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            
		            DropDownVersionInfo dropDownVerInfo = new DropDownVersionInfo(id, version,
		            							createdTimestamp, createdByUserId, lastUpdatedTimestamp, lastUpdatedByUserId
		            		 												);
		            dropDownVerInfo.setLoadedFromDb();
		            list.add(dropDownVerInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - DropDown: " + dropDown.getEsfName() + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - DropDown: " + dropDown.getEsfName() + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static DropDownVersionInfo createLike(DropDown likeDropDown, DropDownVersion likeDropDownVersion, User createdBy)
   	    {
   			DropDownVersion newDropDownVersion = DropDownVersion.Manager.createLike(likeDropDown, likeDropDownVersion, createdBy);
   			DropDownVersionInfo newDropDownVerInfo = new DropDownVersionInfo(newDropDownVersion);
   			return newDropDownVerInfo;
   	    }

   	    public static DropDownVersionInfo createNew(DropDown dropDown, User createdBy)
   	    {
   			DropDownVersion newDropDownVersion = DropDownVersion.Manager.createTest(dropDown, createdBy);
   			DropDownVersionInfo newDropDownVerInfo = new DropDownVersionInfo(newDropDownVersion);
   			return newDropDownVerInfo;
   	    }

   	    public static DropDownVersionInfo createFromSource(DropDownVersion dropDownVersion)
   	    {
   	    	if ( dropDownVersion == null )
   	    		return null;
   	    	
   			DropDownVersionInfo dropDownVerInfo = new DropDownVersionInfo(dropDownVersion);
   			dropDownVerInfo.setLoadedFromDb();

   			return dropDownVerInfo;
   	    }
   	    
   	} // Manager
   	
}