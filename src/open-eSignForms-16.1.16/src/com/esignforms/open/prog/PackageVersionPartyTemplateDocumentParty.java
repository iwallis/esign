// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.util.XmlUtil;

/**
* PackageVersionPartyTemplateDocumentParty holds the definition of a single party template defined in a document that is associated with 
* a PackageVersionPartyTemplate.  That is, it maps a package party to a document party (or just the document for view only).
* 
* @author Yozons, Inc.
*/
public class PackageVersionPartyTemplateDocumentParty
	extends com.esignforms.open.db.DatabaseObject
{
	private static final long serialVersionUID = 7997655767127913010L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackageVersionPartyTemplateDocumentParty.class);

    protected EsfUUID packageVersionPartyTemplateId;
    protected EsfUUID documentId; 
    protected EsfName documentPartyTemplateName;
    protected String snapshotDocumentStatus; 
    protected String snapshotDataStatus;
    
    /**
     * This creates a PackageVersionPartyTemplateDocumentParty object from data retrieved from the DB.
     */
    protected PackageVersionPartyTemplateDocumentParty(EsfUUID packageVersionPartyTemplateId, EsfUUID documentId, EsfName documentPartyTemplateName, String snapshotDocumentStatus, String snapshotDataStatus )
    {
    	this.packageVersionPartyTemplateId = packageVersionPartyTemplateId;
    	this.documentId = documentId;
    	this.documentPartyTemplateName = documentPartyTemplateName;
    	this.snapshotDocumentStatus = snapshotDocumentStatus;
    	this.snapshotDataStatus = snapshotDataStatus;
    }
    protected PackageVersionPartyTemplateDocumentParty(EsfUUID packageVersionPartyTemplateId, PackageVersionPartyTemplateDocumentParty likeTemplate )
    {
    	this.packageVersionPartyTemplateId = packageVersionPartyTemplateId;
    	this.documentId = likeTemplate.documentId;
    	this.documentPartyTemplateName = likeTemplate.documentPartyTemplateName;
    	this.snapshotDocumentStatus = likeTemplate.snapshotDocumentStatus;
    	this.snapshotDataStatus = likeTemplate.snapshotDataStatus;
    }
    
    public PackageVersionPartyTemplateDocumentParty duplicate()
    {
    	PackageVersionPartyTemplateDocumentParty ptdp = new PackageVersionPartyTemplateDocumentParty(packageVersionPartyTemplateId,documentId,documentPartyTemplateName.duplicate(),snapshotDocumentStatus,snapshotDataStatus);
    	ptdp.setDatabaseObjectLike(this);
    	return ptdp;
    }

    
    public EsfUUID getPackageVersionPartyTemplateId()
    {
        return packageVersionPartyTemplateId;
    }
    
    public EsfUUID getDocumentId()
    {
        return documentId;
    }
    
    public EsfName getDocumentPartyTemplateName()
    {
        return documentPartyTemplateName;
    }
    public boolean isDocumentPartyTemplateViewOnly()
    {
    	return PartyTemplate.ESF_PARTY_NAME_VIEW_ONLY.equals(documentPartyTemplateName);
    }
    public boolean isDocumentPartyTemplateViewOptional()
    {
    	return PartyTemplate.ESF_PARTY_NAME_VIEW_OPTIONAL.equals(documentPartyTemplateName);
    }
    public boolean isDocumentPartyTemplateEsfReportsAccess()
    {
    	return PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS.equals(documentPartyTemplateName);
    }
    public boolean isDocumentPartyTemplateView()
    {
    	return isDocumentPartyTemplateViewOnly() || isDocumentPartyTemplateViewOptional();
    }
    public boolean isMatchingDocumentPartyTemplateName(EsfName docPartyTemplateName)
    {
    	return documentPartyTemplateName.equals(docPartyTemplateName);
    }
    
    public String getSnapshotDocumentStatus()
    {
    	return snapshotDocumentStatus;
    }
    public boolean isSnapshotDocumentStatusEnabled()
    {
    	return Literals.STATUS_ENABLED.equals(snapshotDocumentStatus);
    }
    public void setSnapshotDocumentStatus(String v)
    {
    	snapshotDocumentStatus = v == null ? Literals.STATUS_DISABLED : v.trim();
    	objectChanged();
    }
    
    public String getSnapshotDataStatus()
    {
    	return snapshotDataStatus;
    }
    public boolean isSnapshotDataStatusEnabled()
    {
    	return Literals.STATUS_ENABLED.equals(snapshotDataStatus);
    }
    public void setSnapshotDataStatus(String v)
    {
    	snapshotDataStatus = v == null ? Literals.STATUS_DISABLED : v.trim();
    	objectChanged();
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof PackageVersionPartyTemplateDocumentParty )
        {
        	PackageVersionPartyTemplateDocumentParty other = (PackageVersionPartyTemplateDocumentParty)o;
            if ( getPackageVersionPartyTemplateId().equals(other.getPackageVersionPartyTemplateId())  &&
            	 getDocumentId().equals(other.getDocumentId())
               ) 
            {
            	return isMatchingDocumentPartyTemplateName(other.getDocumentPartyTemplateName());
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getPackageVersionPartyTemplateId().hashCode() + getDocumentId().hashCode() +
    	       getDocumentPartyTemplateName().hashCode();
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<PackageVersionPartyTemplateDocumentParty xmlns=\"").append(XmlUtil.getXmlNamespace2013()).append("\">\n");
        
        buf.append(" <packageVersionPartyTemplateId>").append(packageVersionPartyTemplateId.toXml()).append("</packageVersionPartyTemplateId>\n");
        buf.append(" <documentId>").append(documentId.toXml()).append("</documentId>\n");
       	buf.append(" <documentPartyTemplateName>").append(documentPartyTemplateName.toXml()).append("</documentPartyTemplateName>\n");
       	buf.append(" <snapshotDocumentStatus>").append(escapeXml(snapshotDocumentStatus)).append("</snapshotDocumentStatus>\n");
       	buf.append(" <snapshotDataStatus>").append(escapeXml(snapshotDataStatus)).append("</snapshotDataStatus>\n");
       	
        buf.append("</PackageVersionPartyTemplateDocumentParty>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 139 + 187 + packageVersionPartyTemplateId.getEstimatedLengthXml() + documentId.getEstimatedLengthXml() +
        	       58 + documentPartyTemplateName.getEstimatedLengthXml();
        return len; 
    }

    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */

	public synchronized boolean save(final Connection con)
		throws SQLException
	{
		_logger.debug("save(con) on packageVersionPartyTemplateId: " + packageVersionPartyTemplateId + "; documentId: " + documentId + "; documentPartyTemplateName: " + documentPartyTemplateName + "; insert: " + doInsert());
	    
	    clearLastSQLException();
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        if ( doInsert() )
	        {
	            stmt = new EsfPreparedStatement(con, 
	            	"INSERT INTO esf_package_version_party_document_party (package_party_template_id,document_id,document_party_template_esfname,snapshot_document_status,snapshot_data_status) VALUES (?,?,?,?,?)");
	            stmt.set(packageVersionPartyTemplateId);
	            stmt.set(documentId);
	            stmt.set(documentPartyTemplateName);
	            stmt.set(snapshotDocumentStatus);
	            stmt.set(snapshotDataStatus);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Insert failed for packageVersionPartyTemplateId: " + packageVersionPartyTemplateId + "; documentId: " + documentId + "; documentPartyTemplateName: " + documentPartyTemplateName);
	                return false;
	            }
	            
	            // Now we mark this object as if it were loaded fresh from the database
	            setLoadedFromDb();
	            
	            return true;
	        }
	        
	        if ( hasChanged() )
	        {
	            // We assume we'll update it instead
	            stmt = new EsfPreparedStatement( con, 
	            		"UPDATE esf_package_version_party_document_party SET snapshot_document_status=?, snapshot_data_status=? WHERE package_party_template_id=? AND document_id=?"
	            						   		);
	            stmt.set(snapshotDocumentStatus);
	            stmt.set(snapshotDataStatus);
	            stmt.set(packageVersionPartyTemplateId);
	            stmt.set(documentId);
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Update failed for packageVersionPartyTemplateId: " + packageVersionPartyTemplateId + "; documentId: " + documentId + "; documentPartyTemplateName: " + documentPartyTemplateName);
	            }
	        }

	        setLoadedFromDb();
	        return true;
	    }
	    catch(SQLException e)
	    {
        	_logger.sqlerr(e,"save(con) on packageVersionPartyTemplateId: " + packageVersionPartyTemplateId + "; documentId: " + documentId + "; documentPartyTemplateName: " + documentPartyTemplateName + "; insert: " + doInsert());
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}
    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
            if ( result )
            	con.commit();
            else
            	con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }


    public synchronized boolean delete(final Connection con, final Errors errors)
	    throws SQLException
	{
		_logger.debug("delete(con) on packageVersionPartyTemplateId: " + packageVersionPartyTemplateId + "; documentId: " + documentId + "; documentPartyTemplateName: " + documentPartyTemplateName);
	    
	    clearLastSQLException();
	
	    // Let's do some referential integrity checks to be sure it's not in use 
	    if ( ! checkReferential(con,errors) )
	        return false;
	
	    if ( doInsert() )
	    {
	    	_logger.warn("delete(con) - Ignored delete of package version party template to document party that was pending an INSERT packageVersionPartyTemplateId: " + 
	    			 	 packageVersionPartyTemplateId + "; documentId: " + documentId + "; documentPartyTemplateName: " + documentPartyTemplateName);
	        objectDeleted();
	        return true;
	    }
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        // Delete the mapping
	    	String sql = "DELETE FROM esf_package_version_party_document_party WHERE package_party_template_id=? AND document_id=? AND lower(document_party_template_esfname)=?";
	    	stmt = new EsfPreparedStatement(con,sql);
	        stmt.set(packageVersionPartyTemplateId);
	        stmt.set(documentId);
	        stmt.set(documentPartyTemplateName.toLowerCase());
	        int num = stmt.executeUpdate();
	        if ( num == 0 )
	        	_logger.warn("delete(con) - Failed to find esf_package_version_party_document_party database row to delete with packageVersionPartyTemplateId: " + 
	        				packageVersionPartyTemplateId + "; documentId: " + documentId + "; documentPartyTemplateName: " + documentPartyTemplateName);
	        
	        objectDeleted();
	        
	        return true;
	    }
	    catch(SQLException e)
	    {
	    	_logger.sqlerr(e,"delete(con) on packageVersionPartyTemplateId: " + packageVersionPartyTemplateId + "; documentId: " + documentId + "; documentPartyTemplateName: " + documentPartyTemplateName);
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}

	public boolean delete(final Errors errors)
	{
	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        boolean result = delete(con,errors);
	        con.commit();
	        return result;
	    }
	    catch(SQLException e) 
	    {
	        pool.rollbackIgnoreException(con,e);
	        return false;
	    }
	    finally
	    {
	    	Application.getInstance().cleanupPool(pool,con,null);
	    }
	}
	public synchronized boolean delete()
	{
		Errors nullErrors = null;
	    return delete(nullErrors);
	}
    
	protected boolean checkReferential(final Connection con, final Errors errors)
    	throws SQLException
    {
		return true;
	}

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all PackageVersionPartyTemplateDocumentParty objects held in the package version party template
   	     * @return the List of PackageVersionPartyTemplateDocumentParty found.
   	     */
   	    public static List<PackageVersionPartyTemplateDocumentParty> getAllForParty(EsfUUID packagePartyId)
   	    {
   	    	_logger.debug("Manager.getAllForParty() - packagePartyId: " + packagePartyId);
   	    	
   	    	LinkedList<PackageVersionPartyTemplateDocumentParty> list = new LinkedList<PackageVersionPartyTemplateDocumentParty>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, 
   	        			"SELECT document_id,document_party_template_esfname,snapshot_document_status,snapshot_data_status FROM esf_package_version_party_document_party WHERE package_party_template_id=?"
   	            						   		);
   	        	stmt.set(packagePartyId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID docId = rs.getEsfUUID();
	            	EsfName docPartyTemplateName = rs.getEsfName();
	            	String snapshotDocumentStatus = rs.getString();
	            	String snapshotDataStatus = rs.getString();
		            
	            	PackageVersionPartyTemplateDocumentParty partyToParty = new PackageVersionPartyTemplateDocumentParty(packagePartyId,docId,docPartyTemplateName,snapshotDocumentStatus,snapshotDataStatus);
	            	partyToParty.setLoadedFromDb();
		            list.add(partyToParty);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAllForParty() - packagePartyId: " + packagePartyId + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAllForParty() - packagePartyId: " + packagePartyId + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	            list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static PackageVersionPartyTemplateDocumentParty createLike(EsfUUID packageVersionPartyId, PackageVersionPartyTemplateDocumentParty likeTemplate)
   	    {
   	    	return new PackageVersionPartyTemplateDocumentParty(packageVersionPartyId, likeTemplate );
   	    }
   	    
   	    public static PackageVersionPartyTemplateDocumentParty createNew(EsfUUID packageVersionPartyId, EsfUUID docId, EsfName docPartyTemplateName, String snapshotDocumentStatus, String snapshotDataStatus )
   	    {
   	    	return new PackageVersionPartyTemplateDocumentParty(packageVersionPartyId, docId, docPartyTemplateName, snapshotDocumentStatus, snapshotDataStatus);
   	    }
   	    
   	    public static PackageVersionPartyTemplateDocumentParty createFromJDOM(Element element, EsfUUID packageVersionPartyTemplateId, Map<EsfUUID,EsfUUID> idMap)
   	    {
   	    	EsfUUID documentId = new EsfUUID(element.getChildText("documentId",element.getNamespace()));
   	    	if ( idMap.containsKey(documentId) )
   	    		documentId = idMap.get(documentId);
   	    	
   	    	EsfName documentPartyTemplateName = new EsfName(element.getChildText("documentPartyTemplateName",element.getNamespace()));
   	    	if ( ! documentPartyTemplateName.isValid() )
   	    		documentPartyTemplateName = PartyTemplate.ESF_PARTY_NAME_VIEW_ONLY;
   	    	
   	    	String snapshotDocumentStatus = element.getChildTextTrim("snapshotDocumentStatus", element.getNamespace());
   	    	String snapshotDataStatus = element.getChildTextTrim("snapshotDataStatus", element.getNamespace());
   	    	
   	    	return createNew(packageVersionPartyTemplateId, documentId, documentPartyTemplateName, snapshotDocumentStatus, snapshotDataStatus);
   	    }

   	} // Manager
    
}