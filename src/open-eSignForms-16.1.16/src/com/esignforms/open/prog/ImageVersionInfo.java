// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;

/**
* ImageVersionInfo is a data-only object that holds information about a image version.
* It's not a mutable object (you can't save/delete it), just for lists of image versions and such.
* 
* @author Yozons, Inc.
*/
public class ImageVersionInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<ImageVersionInfo>, java.io.Serializable
{
 	private static final long serialVersionUID = 7355744000029087328L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ImageVersionInfo.class);

    protected EsfUUID id;
    protected EsfUUID imageId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected String imageFileName;
    
    /**
     * This creates a new ImageVersionInfo using the source object.
     * @param imageVersion the image version to use as the source
     */
    protected ImageVersionInfo(ImageVersion imageVersion)
    {
        this.id = imageVersion.getId();
        this.imageId = imageVersion.getImageId();
        this.version = imageVersion.getVersion();
        this.createdTimestamp = imageVersion.getCreatedTimestamp();
        this.createdByUserId = imageVersion.getCreatedByUserId();
        this.lastUpdatedTimestamp = imageVersion.getLastUpdatedTimestamp();
        this.lastUpdatedByUserId = imageVersion.getLastUpdatedByUserId();
        this.imageFileName = imageVersion.getImageFileName();
    }
    
    /**
     * This creates a ImageVersionInfo object from data retrieved from the DB.
     */
    protected ImageVersionInfo(
    		EsfUUID id, EsfUUID imageId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId, String imageFileName)
    {
        this.id = id;
        this.imageId = imageId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.imageFileName = imageFileName;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public EsfUUID getImageId()
    {
        return imageId;
    }
    
    public int getVersion()
    {
    	return version;
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }

    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }

    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }

    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }
    
    public String getImageFileName()
    {
    	return imageFileName;
    }


    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof ImageVersionInfo )
            return getId().equals(((ImageVersionInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(ImageVersionInfo o)
    {
    	return getVersion() - o.getVersion();
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all ImageVersionInfo objects in the specified library.
   	     * @return the Collection of ImageVersionInfo found with most recent versions first.
   	     */
   	    public static Collection<ImageVersionInfo> getAll(Image image)
   	    {
   	    	LinkedList<ImageVersionInfo> list = new LinkedList<ImageVersionInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id, version, created_timestamp, created_by_user_id, last_updated_timestamp, last_updated_by_user_id, image_file_name " +
   	    	            "FROM esf_library_image_version " + 
   	    	            "WHERE library_image_id = ? " +
   	    	            "ORDER BY version DESC"
   	            						   );
   	        	stmt.set(image.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            String imageFileName = rs.getString();
		            
		            ImageVersionInfo imageVerInfo = new ImageVersionInfo(id, image.getId(), version,
		            							createdTimestamp, createdByUserId, lastUpdatedTimestamp, lastUpdatedByUserId, imageFileName
		            		 												);
		            imageVerInfo.setLoadedFromDb();
		            list.add(imageVerInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - Image: " + image.getEsfName() + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - Image: " + image.getEsfName() + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static ImageVersionInfo createLike(Image likeImage, ImageVersion likeImageVersion, User createdBy)
   	    {
   			ImageVersion newImageVersion = ImageVersion.Manager.createLike(likeImage, likeImageVersion, createdBy);
   			ImageVersionInfo newImageVerInfo = new ImageVersionInfo(newImageVersion);
   			return newImageVerInfo;
   	    }

   	    public static ImageVersionInfo createNew(Image image, User createdBy)
   	    {
   			ImageVersion newImageVersion = ImageVersion.Manager.createTest(image, createdBy);
   			ImageVersionInfo newImageVerInfo = new ImageVersionInfo(newImageVersion);
   			return newImageVerInfo;
   	    }

   	    public static ImageVersionInfo createFromSource(ImageVersion imageVersion)
   	    {
   	    	if ( imageVersion == null )
   	    		return null;
   	    	
   			ImageVersionInfo imageVerInfo = new ImageVersionInfo(imageVersion);
   			imageVerInfo.setLoadedFromDb();

   			return imageVerInfo;
   	    }
   	    
   	} // Manager
   	
}