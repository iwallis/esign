// Copyright (C) 2011-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfDecimal;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;
import com.esignforms.open.vaadin.widget.TranFilePopupButton;
import com.esignforms.open.vaadin.widget.TranLatestSnapshotsAsPdfButton;
import com.esignforms.open.vaadin.widget.TranResumeButton;
import com.esignforms.open.vaadin.widget.TranSnapshotPopupButton;
import com.esignforms.open.vaadin.widget.TranSnapshotsAsPdfPopupButton;
import com.esignforms.open.vaadin.widget.TranSnapshotsDownloadPopupButton;

/**
* ReportFieldTemplate holds the definition of a single report field template, one that can be added to a report.
* 
* @author Yozons, Inc.
*/
public class ReportFieldTemplate
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<ReportFieldTemplate>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable
{
	private static final long serialVersionUID = 6306574825829772474L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ReportFieldTemplate.class);
	
	public final static EsfName BUILT_IN_DOWNLOAD_PDF_LATEST_SNAPSHOTS   = new EsfName("esf_download_pdf_latest_snapshots");
	public final static EsfName BUILT_IN_DOWNLOAD_SELECTED_SNAPSHOTS = new EsfName("esf_download_selected_snapshots");
	public final static EsfName BUILT_IN_DOWNLOAD_HTML_SELECTED_SNAPSHOT = new EsfName("esf_download_html_selected_snapshot");
	public final static EsfName BUILT_IN_RESUME_TRANSACTION_BUTTON = new EsfName("esf_resume_transaction_button");

	@Deprecated // should use BUILT_IN_DOWNLOAD_SELECTED_SNAPSHOTS esf_download_selected_snapshots instead as of version 14.12.6
	public final static EsfName BUILT_IN_DOWNLOAD_PDF_SELECTED_SNAPSHOTS = new EsfName("esf_download_pdf_selected_snapshots");

    public final static String FIELD_TYPE_BUILT_IN = "B";
    public final static String FIELD_TYPE_STRING = "S";
    public final static String FIELD_TYPE_NUMERIC_ONLY = "1"; // string
    public final static String FIELD_TYPE_ALPHANUMERIC_ONLY = "Z"; // string
    public final static String FIELD_TYPE_INTEGER = "I";
    public final static String FIELD_TYPE_DECIMAL = "N"; // decimal, numeric, money
    public final static String FIELD_TYPE_DATE = "D"; // date, date-time, sign-date
    public final static String FIELD_TYPE_FILE = "F";
    
    public static boolean isFieldTypeBuiltIn(String v) { return FIELD_TYPE_BUILT_IN.equals(v); }
    public static boolean isFieldTypeString(String v) { return FIELD_TYPE_STRING.equals(v); }
    public static boolean isFieldTypeNumericOnly(String v) { return FIELD_TYPE_NUMERIC_ONLY.equals(v); }
    public static boolean isFieldTypeAlphaNumericOnly(String v) { return FIELD_TYPE_ALPHANUMERIC_ONLY.equals(v); }
    public static boolean isFieldTypeStringOrRelated(String v) { return isFieldTypeString(v) || isFieldTypeNumericOnly(v) || isFieldTypeAlphaNumericOnly(v); }
    public static boolean isFieldTypeInteger(String v) { return FIELD_TYPE_INTEGER.equals(v); }
    public static boolean isFieldTypeDecimal(String v) { return FIELD_TYPE_DECIMAL.equals(v); }
    public static boolean isFieldTypeDate(String v) { return FIELD_TYPE_DATE.equals(v); }
    public static boolean isFieldTypeFile(String v) { return FIELD_TYPE_FILE.equals(v); }

    protected EsfUUID id;
    protected EsfName fieldName; 
    protected String fieldType;
    protected String fieldLabel; 
    protected String fieldTooltip;
    
    protected int numReportsReferenced; // computed on DB load

    protected EsfDateTime lastAccessFromCache = new EsfDateTime();
    
    /**
     * This creates a ReportFieldTemplate object from data retrieved from the DB. Normally, this is called by one of the Manager.create routines.
     */
    public ReportFieldTemplate(EsfUUID id, EsfName fieldName, String fieldType, String fieldLabel, String fieldTooltip, int numReportsReferenced )
    {
    	this.id = id;
    	this.fieldName = fieldName;
    	this.fieldType = fieldType;
    	this.fieldLabel = fieldLabel;
    	this.fieldTooltip = fieldTooltip;
    	this.numReportsReferenced = numReportsReferenced;
    }
    protected ReportFieldTemplate(ReportFieldTemplate likeTemplate )
    {
    	this.id = new EsfUUID();
    	this.fieldName = likeTemplate.fieldName;
    	this.fieldType = likeTemplate.fieldType;
    	this.fieldLabel = likeTemplate.fieldLabel;
    	this.fieldTooltip = likeTemplate.fieldTooltip;
    	this.numReportsReferenced = likeTemplate.numReportsReferenced;
    }
    
    public ReportFieldTemplate duplicate()
    {
    	ReportFieldTemplate rft = new ReportFieldTemplate(id,fieldName.duplicate(),fieldType,fieldLabel,fieldTooltip,numReportsReferenced);
    	rft.setDatabaseObjectLike(this);
    	return rft;
    }

    
    public EsfUUID getId()
    {
        return id;
    }
    
    public EsfName getFieldName()
    {
        return fieldName;
    }
    public void setFieldName(EsfName v)
    {
    	// Block setting a report field name to null or an invalid EsfName or one with a reserved prefix or if we're a reserved one
    	if ( ! isFieldTypeBuiltIn() && v != null && v.isValid() && ! v.hasReservedPrefix() )
    	{
        	fieldName = v;
            objectChanged();
    	}
    }
    
    public String getFieldType()
    {
        return fieldType;
    }
    public void setFieldType(String v) 
    {
    	if ( isFieldTypeString() && isFieldTypeStringOrRelated(v) && ! fieldType.equals(v) )
    	{
    		fieldType = v;
    		objectChanged();
    	}
    }
    public boolean isFieldTypeBuiltIn() { return isFieldTypeBuiltIn(fieldType); }
    public boolean isFieldTypeString() { return isFieldTypeString(fieldType); }
    public boolean isFieldTypeInteger() { return isFieldTypeInteger(fieldType); }
    public boolean isFieldTypeDecimal() { return isFieldTypeDecimal(fieldType); }
    public boolean isFieldTypeDate() { return isFieldTypeDate(fieldType); }
    public boolean isFieldTypeFile() { return isFieldTypeFile(fieldType); }
    public boolean isFieldTypeNumericOnly() { return isFieldTypeNumericOnly(fieldType); }
    public boolean isFieldTypeAlphaNumericOnly() { return isFieldTypeAlphaNumericOnly(fieldType); }
    
    public boolean isFieldTypeStringOrRelated() { return isFieldTypeString(fieldType) || isFieldTypeNumericOnly(fieldType) || isFieldTypeAlphaNumericOnly(fieldType); }

    @SuppressWarnings("deprecation")
    public Class<?> getFieldValueClass()
    {
    	if ( isFieldTypeFile() )
    		return TranFilePopupButton.class;
    	if ( fieldName.equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_LATEST_SNAPSHOTS) )
    		return TranLatestSnapshotsAsPdfButton.class;
    	if ( fieldName.equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_SELECTED_SNAPSHOTS) )
    		return TranSnapshotsAsPdfPopupButton.class;
    	if ( fieldName.equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_SELECTED_SNAPSHOTS) )
    		return TranSnapshotsDownloadPopupButton.class;
    	if ( fieldName.equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_HTML_SELECTED_SNAPSHOT) )
    		return TranSnapshotPopupButton.class;
    	if ( fieldName.equals(ReportFieldTemplate.BUILT_IN_RESUME_TRANSACTION_BUTTON) )
    		return TranResumeButton.class;
    	if ( isFieldTypeStringOrRelated() )
    		return EsfString.class;
    	if ( isFieldTypeInteger() )
    		return EsfInteger.class;
    	if ( isFieldTypeDecimal() )
    		return EsfDecimal.class;
    	if ( isFieldTypeDate() )
    		return EsfDate.class;
    	if ( isFieldTypeFile() )
    		return EsfUUID[].class;
    	if ( isFieldTypeBuiltIn() ) 
    	{
    		if ( fieldName.equals("esf_start_timestamp") ||
    			 fieldName.equals("esf_last_updated_timestamp") ||
    			 fieldName.equals("esf_expire_timestamp") ||
    			 fieldName.equals("esf_stall_timestamp") ||
    			 fieldName.equals("esf_cancel_timestamp") 
    		   )
    			return EsfDateTime.class;
    		if ( fieldName.equals("esf_transaction_id") || fieldName.equals("esf_created_by_user") || fieldName.equals("esf_last_updated_by_user") ) 
    			return EsfUUID.class;
    		if ( fieldName.equals("esf_transaction_template_name") || fieldName.equals("esf_package_name") ) 
    			return EsfPathName.class;
    		return EsfString.class;
    	}
    	return null;
    }
    
    public boolean isFieldRendersAsButton()
    {
		if ( isFieldTypeFile() ) 
			return true;
		if ( getFieldName().equals(BUILT_IN_DOWNLOAD_PDF_LATEST_SNAPSHOTS) ) 
			return true;
		if ( getFieldName().equals(BUILT_IN_DOWNLOAD_PDF_SELECTED_SNAPSHOTS) ) 
			return true;
		if ( getFieldName().equals(BUILT_IN_DOWNLOAD_SELECTED_SNAPSHOTS) ) 
			return true;
		if ( getFieldName().equals(BUILT_IN_DOWNLOAD_HTML_SELECTED_SNAPSHOT) ) 
			return true;
		if ( getFieldName().equals(BUILT_IN_RESUME_TRANSACTION_BUTTON) ) 
			return true;

		return false;
    }

    
    public String getFieldLabel()
    {
        return fieldLabel;
    }
    public void setFieldLabel(String v) 
    {
    	if ( v == null )
    		fieldLabel = v;
    	else if ( v.length() > Literals.LABEL_MAX_LENGTH )
    		fieldLabel = v.substring(0,Literals.LABEL_MAX_LENGTH).trim();
        else
        	fieldLabel = v.trim();
    	objectChanged();
    }
    
    public String getFieldTooltip()
    {
        return fieldTooltip;
    }
    public void setFieldTooltip(String v) 
    {
    	fieldTooltip = v == null ? null : v.trim();
    	objectChanged();
    }
    
    public int getNumReportsReferenced()
    {
        return numReportsReferenced;
    }
    public void refreshNumReportsReferenced()
    {
    	numReportsReferenced = Manager.getNumReportsReferenced(id);
    }
    
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof ReportFieldTemplate )
        {
        	ReportFieldTemplate other = (ReportFieldTemplate)o;
            return getId().equals(other.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    
    @Override
    public int compareTo(ReportFieldTemplate o)
    {
    	return getFieldName().compareTo(o.getFieldName());
    }

    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<ReportFieldTemplate xmlns=\"").append(XmlUtil.getXmlNamespace2013()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <fieldName>").append(fieldName.toXml()).append("</fieldName>\n");
        buf.append(" <fieldType>").append(escapeXml(fieldType)).append("</fieldType>\n");
        buf.append(" <fieldLabel>").append(escapeXml(fieldLabel)).append("</fieldLabel>\n");
        buf.append(" <fieldTooltip>").append(escapeXml(fieldTooltip)).append("</fieldTooltip>\n");
       	
        buf.append("</ReportFieldTemplate>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 119 +  + id.getEstimatedLengthXml() + fieldName.getEstimatedLengthXml() + fieldType.length();
        if ( fieldLabel != null )
        	len += fieldLabel.length();
        if ( fieldTooltip != null )
        	len += fieldTooltip.length();
        return len; 
    }

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con)
		throws SQLException
	{
		_logger.debug("save(con) on id: " + id + "; fieldName: " + fieldName + "; insert: " + doInsert());
	    
	    clearLastSQLException();
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        if ( doInsert() )
	        {
	            stmt = new EsfPreparedStatement(con, 
	            	"INSERT INTO esf_report_field_template (id, field_esfname, field_type, field_label, field_tooltip) VALUES (?,?,?,?,?)");
	            stmt.set(id);
	            stmt.set(fieldName);
	            stmt.set(fieldType);
	            stmt.set(fieldLabel);
	            stmt.set(fieldTooltip);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Insert failed for id: " + id + "; fieldName: " + fieldName + "; insert: " + doInsert());
	                return false;
	            }
	            
	            // Now we mark this object as if it were loaded fresh from the database
	            setLoadedFromDb();
	            Manager.cache.replace(this); // We use replace instead of 'add' because on object create, we added to the ID-only cache
	            
	            return true;
	        }
	        
	        if ( hasChanged() )
	        {
	            // We assume we'll update it instead
	            stmt = new EsfPreparedStatement( con, 
	            		"UPDATE esf_report_field_template SET field_esfname=?, field_type=?, field_label=?, field_tooltip=? WHERE id=?"
	            						   		);
	            stmt.set(fieldName);
	            stmt.set(fieldType);
	            stmt.set(fieldLabel);
	            stmt.set(fieldTooltip);
	            stmt.set(id);
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Update failed for id: " + id + "; fieldName: " + fieldName);
	            }
	        }

	        setLoadedFromDb();
            refreshNumReportsReferenced();
	        Manager.cache.replace(this);
	        return true;
	    }
	    catch(SQLException e)
	    {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; fieldName: " + fieldName + "; insert: " + doInsert());
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}
    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
            if ( result )
            	con.commit();
            else
            	con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }


    public synchronized boolean delete(final Connection con, final Errors errors)
	    throws SQLException
	{
		_logger.debug("delete(con) on id: " + id + "; fieldName: " + fieldName);
	    
	    clearLastSQLException();
	
	    // Let's do some referential integrity checks to be sure it's not in use 
	    if ( ! checkReferential(con,errors) )
	        return false;
	
	    if ( doInsert() )
	    {
	    	_logger.warn("delete(con) - Ignored delete of report field template that was pending an INSERT id: " + id + "; fieldName: " + fieldName);
	        objectDeleted();
	        Manager.cache.remove(this);
	        return true;
	    }
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	    	// If any packages were feeding data into this field, we'll remove them now.
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_package_version_report_field WHERE report_field_template_id=?");
            stmt.set(id);
	        int num = stmt.executeUpdate();
	        if ( num == 0 )
	        	_logger.warn("delete(con) - Failed to find esf_package_version_report_field database row to delete with report_field_template_id: " + id + "; fieldName: " + fieldName);

	    	// If any reports make use of this field, we'll remove them now, though this shouldn't happen since our check referential should have blocked if they existed
	    	stmt.close();
	        stmt = new EsfPreparedStatement(con,"DELETE FROM esf_report_template_report_field WHERE report_field_template_id=?");
            stmt.set(id);
	        num = stmt.executeUpdate();
	        if ( num != 0 )
	        	_logger.warn("delete(con) - Unexpectedly found " + num + " esf_report_template_report_field database rows to delete with report_field_template_id: " + id + "; fieldName: " + fieldName);
	        
	        // Delete any report fields we have that mapped to us
	    	stmt.close();
	        stmt = new EsfPreparedStatement(con,"DELETE FROM esf_tran_report_field_string WHERE report_field_template_id=?");
            stmt.set(id);
	        num = stmt.executeUpdate();
	        if ( num != 0 )
	        	_logger.warn("delete(con) - Removed " + num + " esf_tran_report_field_string database rows referencing report_field_template_id: " + id + "; fieldName: " + fieldName);

	    	stmt.close();
	        stmt = new EsfPreparedStatement(con,"DELETE FROM esf_tran_report_field_long WHERE report_field_template_id=?");
            stmt.set(id);
	        num = stmt.executeUpdate();
	        if ( num != 0 )
	        	_logger.warn("delete(con) - Removed " + num + " esf_tran_report_field_long database rows referencing report_field_template_id: " + id + "; fieldName: " + fieldName);
	        
	    	stmt.close();
	        stmt = new EsfPreparedStatement(con,"DELETE FROM esf_tran_report_field_numeric WHERE report_field_template_id=?");
            stmt.set(id);
	        num = stmt.executeUpdate();
	        if ( num != 0 )
	        	_logger.warn("delete(con) - Removed " + num + " esf_tran_report_field_numeric database rows referencing report_field_template_id: " + id + "; fieldName: " + fieldName);
	        
	    	stmt.close();
	        stmt = new EsfPreparedStatement(con,"DELETE FROM esf_tran_report_field_date WHERE report_field_template_id=?");
            stmt.set(id);
	        num = stmt.executeUpdate();
	        if ( num != 0 )
	        	_logger.warn("delete(con) - Removed " + num + " esf_tran_report_field_date database rows referencing report_field_template_id: " + id + "; fieldName: " + fieldName);
	        
	    	stmt.close();
	        stmt = new EsfPreparedStatement(con,"DELETE FROM esf_tran_report_field_tranfileid WHERE report_field_template_id=?");
            stmt.set(id);
	        num = stmt.executeUpdate();
	        if ( num != 0 )
	        	_logger.warn("delete(con) - Removed " + num + " esf_tran_report_field_tranfileid database rows referencing report_field_template_id: " + id + "; fieldName: " + fieldName);
	        
	        // Now delete the actual template itself
	    	stmt.close();
	        stmt = new EsfPreparedStatement(con,"DELETE FROM esf_report_field_template WHERE id=?");
            stmt.set(id);
	        num = stmt.executeUpdate();
	        if ( num == 0 )
	        	_logger.warn("delete(con) - Failed to find esf_report_field_template database row to delete with id: " + id + "; fieldName: " + fieldName);
	        
	        objectDeleted();
	        Manager.cache.remove(this);
	        
	        return true;
	    }
	    catch(SQLException e)
	    {
	    	_logger.sqlerr(e,"delete(con) on id: " + id + "; fieldName: " + fieldName);
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}

	public boolean delete(final Errors errors)
	{
	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        boolean result = delete(con,errors);
	        con.commit();
	        return result;
	    }
	    catch(SQLException e) 
	    {
	        pool.rollbackIgnoreException(con,e);
	        return false;
	    }
	    finally
	    {
	    	Application.getInstance().cleanupPool(pool,con,null);
	    }
	}
	public synchronized boolean delete()
	{
		Errors nullErrors = null;
	    return delete(nullErrors);
	}
    
	protected boolean checkReferential(final Connection con, final Errors errors)
    	throws SQLException
    {
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT RT.path_name FROM esf_report_template_report_field RTRF, esf_report_template RT WHERE RTRF.report_field_template_id=? AND RTRF.report_template_id =  RT.id"
	                                   	);
	        stmt.set(id);
	        EsfResultSet rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	do
	        	{
		        	EsfPathName pathName = rs.getEsfPathName();
		        	if ( errors != null )
		        		errors.addError("This report field is still referenced as the report " + pathName + ".");
	        	} while ( rs.next() );
	        	 
	        	return false;
	        }

	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    private static UUIDCacheReadOptimized<ReportFieldTemplate> cache = new UUIDCacheReadOptimized<ReportFieldTemplate>();

   		static int getNumReportsReferenced(EsfUUID reportFieldTemplateId) 
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	           
   	        int count = 0;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT COUNT(*) FROM esf_report_template_report_field WHERE report_field_template_id = ?"
   	        									);
   	        	stmt.set(reportFieldTemplateId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
		            count = rs.getInt();
		            con.commit();
	            }
	            con.rollback();
	            return count;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	            return -1;
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }
   		}
   		
   		static ReportFieldTemplate getById(Connection con, EsfUUID reportFieldTemplateId) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT field_esfname,field_type,field_label,field_tooltip " +
   	        			"FROM esf_report_field_template WHERE id=?"
   	        									);
   	        	stmt.set(reportFieldTemplateId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
		            EsfName fieldName = rs.getEsfName();
		            String fieldType = rs.getString();
		            String fieldLabel = rs.getString();
		            String fieldTooltip = rs.getString();
		            
		            int numReportsReferenced = getNumReportsReferenced(reportFieldTemplateId);
		            
		            ReportFieldTemplate field = new ReportFieldTemplate(reportFieldTemplateId,fieldName,fieldType,fieldLabel,fieldTooltip,numReportsReferenced);
		            field.setLoadedFromDb();
		            cache.add(field);
		            
	   	            return field;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + reportFieldTemplateId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + reportFieldTemplateId + "; failed to find the report field template");
   	        return null; 
   		}
   		
   		public static ReportFieldTemplate getById(EsfUUID reportFieldTemplateId)
   		{
   			if ( reportFieldTemplateId == null )
   				return null;
   			
   			ReportFieldTemplate field = cache.getById(reportFieldTemplateId);
   	    	if ( field != null )
   	    		return field;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	field = getById(con,reportFieldTemplateId);
   	        	if ( field != null ) 
   	        	{
		            con.commit();
	   	        	return field;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   		public static List<ReportFieldTemplate> getAll()
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<ReportFieldTemplate> list = new LinkedList<ReportFieldTemplate>();
   	        
   	        try
   	        {
   	        	
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_report_field_template ORDER BY lower(field_esfname)" );
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            
		            ReportFieldTemplate rft = getById(id);
		            list.add(rft);
	            }
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.error("Manager.getAll()", e);
   	            pool.rollbackIgnoreException(con,e);
   	            list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<ReportFieldTemplate> getAllNonBuiltIn()
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<ReportFieldTemplate> list = new LinkedList<ReportFieldTemplate>();
   	        
   	        try
   	        {
   	        	
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_report_field_template WHERE field_type <> ? ORDER BY lower(field_esfname)" );
   	        	stmt.set(FIELD_TYPE_BUILT_IN);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            
		            ReportFieldTemplate rft = getById(id);
		            list.add(rft);
	            }
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.error("Manager.getAllNonBuiltin()", e);
   	            pool.rollbackIgnoreException(con,e);
   	            list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<ReportFieldTemplate> getAllBuiltIn()
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<ReportFieldTemplate> list = new LinkedList<ReportFieldTemplate>();
   	        
   	        try
   	        {
   	        	
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_report_field_template WHERE field_type=? ORDER BY lower(field_esfname)" );
   	        	stmt.set(FIELD_TYPE_BUILT_IN);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            
		            ReportFieldTemplate rft = getById(id);
		            list.add(rft);
	            }
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.error("Manager.getAllNonBuiltin()", e);
   	            pool.rollbackIgnoreException(con,e);
   	            list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		
   		public static ReportFieldTemplate getByName(EsfName fieldName)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_report_field_template WHERE lower(field_esfname) = ?" );
   	        	stmt.set(fieldName.toLowerCase());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            
		            ReportFieldTemplate rft = getById(id);
		            con.commit();
		            return rft;
	            }
	            con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.error("Manager.getAllByName() fieldName: " + fieldName, e);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return null; 
   		}
   		
   		public static List<ReportFieldTemplate> getByIdList(List<EsfUUID> idList)
   		{
   	        LinkedList<ReportFieldTemplate> list = new LinkedList<ReportFieldTemplate>();
   	        
   	        for( EsfUUID id : idList )
   	        	list.add( getById(id) );
   	        
   	        return list;
   		}
   		
   	    public static ReportFieldTemplate createNew(FieldTemplate fieldTemplate)
   	    {
   	    	if ( fieldTemplate == null )
   	    		return null;
   	    	
   	    	String fieldType;
   	    	if ( fieldTemplate.isTypeCreditCard() || fieldTemplate.isTypeSsnEin() )
   	    		fieldType = FIELD_TYPE_NUMERIC_ONLY;
   	    	else if ( fieldTemplate.isTypeDate() || fieldTemplate.isTypeSignDate() || fieldTemplate.isTypeDateTime() )
   	    		fieldType = FIELD_TYPE_DATE;
   	    	else if ( fieldTemplate.isTypeDecimal() || fieldTemplate.isTypeMoney() )
   	    		fieldType = FIELD_TYPE_DECIMAL;
   	    	else if ( fieldTemplate.isTypeInteger() )
   	    		fieldType = FIELD_TYPE_INTEGER;
   	    	else if ( fieldTemplate.isTypeFile() )
   	    		fieldType = FIELD_TYPE_FILE;
   	    	else
   	    		fieldType = FIELD_TYPE_STRING;
   	    	
   	    	ReportFieldTemplate field = new ReportFieldTemplate(new EsfUUID(), fieldTemplate.getEsfName(), fieldType, fieldTemplate.getLabelTemplate().getLabel(), fieldTemplate.getTooltip(), (short)0 );
   	    	cache.add(field);
   	    	return field;
   	    }

   	    
		
		
		public static ReportFieldTemplate createFromJDOM(Element element)
   	    {
			EsfName fieldName = new EsfName(element.getChildText("fieldName",element.getNamespace()));
			String fieldType = element.getChildTextTrim("fieldType",element.getNamespace());
			String fieldLabel = element.getChildTextTrim("fieldLabel",element.getNamespace());
			String fieldTooltip = element.getChildTextTrim("fieldTooltip",element.getNamespace());
			
   	    	ReportFieldTemplate field = new ReportFieldTemplate(new EsfUUID(), fieldName, fieldType, fieldLabel, fieldTooltip, (short)0 );
   	    	cache.add(field);
   	    	return field;
   	    }


   	    /**
   	     * Returns the total number of objects in the cache
   	     * @return the total number in the cache
   	     */
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	
   	    /**
   	     * Finds all report field templates that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}

   	} // Manager
    
}