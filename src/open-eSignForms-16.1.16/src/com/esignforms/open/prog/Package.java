// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.Permission;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.util.PathNameUUIDUserListableCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* Package holds the definition of a package of document(s), the parties to those documents and the routing logic
* for those parties.
* 
* @author Yozons, Inc.
*/
public class Package
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<Package>, PathNameUUIDUserListableCacheReadOptimized.PathNameUUIDUserListableCacheable, PathNameUUIDUserListableCacheReadOptimized.TimeCacheable, java.io.Serializable
{
	private static final long serialVersionUID = -2755128335136911783L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(Package.class);

    protected EsfUUID id;
    protected EsfPathName pathName;
    protected EsfPathName originalPathName; // derived, not persisted
    protected String  downloadFileNameSpec;
    protected String  description;
    protected String  comments;
    protected String  status;
    protected int	  productionVersion;
    protected int	  testVersion;
    protected Permission permission; // permission grants related to this package object (create like, delete, list, update, view details of this object)
    
    public static final String PERM_PATH_PREFIX = "PackagePerm" + EsfPathName.PATH_SEPARATOR;  // actual name is this prefix followed package's id
    
    // Initial package created for cloning from, etc.
    public static final EsfPathName TEMPLATE_ESFPATHNAME = new EsfPathName(EsfPathName.ESF_RESERVED_PACKAGE_PATH_PREFIX+"Template");

    protected EsfDateTime lastAccessFromCache = new EsfDateTime();

    /**
     * This creates a Package object from data retrieved from the DB.
     */
    protected Package(EsfUUID id, EsfPathName pathName, String downloadFileNameSpec, String description, String status, int productionVersion, int testVersion, String comments, Permission permission)
    {
        this.id = id;
        this.pathName = pathName;
        this.originalPathName = pathName.duplicate();
        this.downloadFileNameSpec = downloadFileNameSpec;
        this.description = description;
        this.status = status;
        this.productionVersion = productionVersion;
        this.testVersion = testVersion;
        this.comments = comments;
        this.permission = permission;
    }
    
    protected Package(EsfUUID id, EsfPathName pathName, Permission permission)
    {
        this.id = id;
        this.pathName = pathName;
        this.originalPathName = pathName.duplicate();
        this.downloadFileNameSpec = null;
        this.description = null;
        this.status = Literals.STATUS_ENABLED;
        this.productionVersion = 0;
        this.testVersion = 1;
        this.comments = null;
        this.permission = permission;
    }

   
    public EsfUUID getId()
    {
        return id;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }
    
    public void makeEnabled()
    {
        status = Literals.STATUS_ENABLED;
        objectChanged();
    }
     
    public void makeDisabled()
    {
        status = Literals.STATUS_DISABLED;
        objectChanged();
    }
    
    public void setStatus(String s)
    {
        if ( Literals.STATUS_ENABLED.equals(s) )
            makeEnabled();
        else
            makeDisabled();
    }
    
    public EsfPathName getPathName()
    {
        return pathName;
    }
    public void setPathName(EsfPathName v)
    {
    	// Block setting a transaction template name to null, an invalid EsfPathName or if the template current is a reserved name or the new name would be reserved
    	if ( v == null || ! v.isValid() || v.hasReservedPathPrefix() || (pathName != null && pathName.hasReservedPathPrefix()) )
    		return;
        pathName = v;
        objectChanged();
    }
    public EsfPathName getOriginalPathName()
    {
        return originalPathName;
    }
    public void resetOriginalPathName()
    {
    	originalPathName = pathName.duplicate();
    }
    
    public boolean isReserved()
    {
    	return getPathName().hasReservedPathPrefix();
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}

    public String getDownloadFileNameSpec()
    {
    	return downloadFileNameSpec;
    }
    public boolean hasDownloadFileNameSpec()
    {
    	return EsfString.isNonBlank(downloadFileNameSpec);
    }
    public void setDownloadFileNameSpec(String v)
    {
		downloadFileNameSpec = EsfString.ensureTrimmedLength(v, Literals.FILE_NAME_MAX_LENGTH);
		objectChanged();
    }
    
	public String getDescription()
    {
        return description;
    }
    public void setDescription(String v)
    {
    	description = EsfString.ensureTrimmedLength(v, Literals.DESCRIPTION_MAX_LENGTH);
        objectChanged();
    }
    
    public String getVersionLabel(int checkVersion)
    {
    	if ( checkVersion > productionVersion )
    		return Literals.VERSION_LABEL_TEST;
    	
    	if ( checkVersion == productionVersion )
    		return Literals.VERSION_LABEL_PRODUCTION;
    	
		return Literals.VERSION_LABEL_NOT_CURRENT;
    }

    public int getProductionVersion()
    {
    	return productionVersion;
    }
    public boolean hasProductionVersion() 
    {
    	return productionVersion > 0;
    }
    public PackageVersion getProductionPackageVersion()
    {
    	return hasProductionVersion() ? PackageVersion.Manager.getByVersion(id, productionVersion) : null;
    }
    
    public int getTestVersion()
    {
    	return testVersion;
    }
    public boolean hasTestVersion() 
    {
    	return testVersion > productionVersion;
    }
    public void promoteTestVersionToProduction()
    {
    	if ( hasTestVersion() )
    	{
        	productionVersion = testVersion;
        	objectChanged();
    	}
    }
    public void revertProductionVersionBackToTest()
    {
       	if ( hasProductionVersion() && ! hasTestVersion() ) 
    	{
       		--productionVersion;
    		objectChanged();
    	}
    }
    public void bumpTestVersion()
    {
    	if ( ! hasTestVersion() ) 
    	{
    		++testVersion;
    		objectChanged();
    	}
    }
    public void dropTestVersion()
    {
    	if ( hasTestVersion() ) 
    	{
    		--testVersion;
    		objectChanged();
    	}
    }
    public PackageVersion getTestPackageVersion() // may be the same as the production version 
    {
    	return PackageVersion.Manager.getByVersion(id, testVersion);
    }
    
    public String getComments()
    {
    	return comments;
    }
    public void setComments(String v)
    {
    	if ( v == null )
    		comments = v;
    	else if ( v.length() > Literals.COMMENTS_MAX_LENGTH )
    		comments = v.substring(0,Literals.COMMENTS_MAX_LENGTH).trim();
        else
        	comments = v.trim();
        objectChanged();
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof Package )
            return getId().equals(((Package)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(Package o)
    {
    	return getPathName().compareTo(o.getPathName());
    }
    
    
    @Override
    public boolean canUserList(User user)
    {
		return permission.canUserList(user);
    }
    
    public final boolean hasPermission(User user, EsfName permOptionName)
    {
    	return permission.hasPermission(user, permOptionName);
    }
    
    public final Collection<Group> getPermissionAllowedGroups(User user, EsfName permOptionName)
    {
    	return permission.getAllowedGroups(user, permOptionName);
    }
    public final boolean addPermissionAllowedGroups(EsfName permOptionName, Group group)
    {
    	return permission.addGroupToOptions(group, permOptionName) > 0;
    }
    public final boolean removePermissionAllowedGroups(EsfName permOptionName, Group group)
    {
    	return permission.removeGroupFromOptions(group, permOptionName) > 0;
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<Package xmlns=\"").append(XmlUtil.getXmlNamespace2013()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <pathName>").append(pathName.toXml()).append("</pathName>\n");
        buf.append(" <downloadFileNameSpec>").append(escapeXml(downloadFileNameSpec)).append("</downloadFileNameSpec>\n");
       	buf.append(" <description>").append(escapeXml(description)).append("</description>\n");
        buf.append(" <status>").append(escapeXml(status)).append("</status>\n");
        buf.append(" <comments>").append(escapeXml(comments)).append("</comments>\n");
        buf.append(" <productionVersion>").append(productionVersion).append("</productionVersion>\n");
        buf.append(" <testVersion>").append(testVersion).append("</testVersion>\n");
        // no import/export of permissions
        
        buf.append("</Package>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 69 + 120 + id.getEstimatedLengthXml() + pathName.getEstimatedLengthXml();
        if ( downloadFileNameSpec != null )
        	len += downloadFileNameSpec.length();
        if ( description != null )
            len += description.length();
        len += status.length();
        if ( comments != null )
            len += comments.length();
        len += 8; // versions
        return len; 
    }

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; pathName: " + pathName + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
            if ( doInsert() )
            {
            	permission.save(con);

            	stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_package (id,path_name,download_file_name_spec,description,comments,status,production_version,test_version) VALUES (?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(pathName);
                stmt.set(downloadFileNameSpec);
                stmt.set(description);
                stmt.set(comments);
                stmt.set(status);
                stmt.set(productionVersion);
                stmt.set(testVersion);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; pathName: " + pathName);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this); // we use replace here because on create, we add to ID-only cache
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new package " + getPathName()); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new package " + getPathName());
                }
                
                return true;
            }
            
            if ( permission.hasChanged() )
            	permission.save(con);

            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_package SET path_name=?,download_file_name_spec=?,description=?,comments=?,status=?,production_version=?,test_version=? WHERE id=?"
                						   );
                stmt.set(pathName);
                stmt.set(downloadFileNameSpec);
                stmt.set(description);
                stmt.set(comments);
                stmt.set(status);
            	stmt.set(productionVersion);
                stmt.set(testVersion);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for package id: " + id + "; pathName: " + pathName);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated package " + getPathName() + "; status; " + getStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated package " + getPathName() + "; status; " + getStatus() + "; prodVersion: " + getProductionVersion());
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; pathName: " + pathName + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; pathName: " + pathName + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use.
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of package that was pending an INSERT id: " + id + "; pathName: " + pathName);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
        	// Delete all our versions...
        	for( PackageVersion pkgVer : PackageVersion.Manager.getAllByPackageId(con,id) )
        	{
        		if ( ! pkgVer.delete(con,errors,user) )
        		{
        			if ( errors != null )
        				errors.addError("Could not delete package version id: " + pkgVer.getId());
        			return false;
        		}
        	}
        	
            // Delete the package
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_package WHERE id=?");
            stmt.set(id);
            int num = stmt.executeUpdate();
            if ( num == 0 )
            	_logger.warn("delete(con) - Failed to find esf_package database row to delete with id: " + id);
            
            permission.delete(con);
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted package " + getPathName() + "; status; " + getStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted package " + getPathName() + "; status; " + getStatus());
            }

            return true;
        }
        catch(SQLException e)
        {
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"delete() on id: " + id + "; pathName: " + pathName + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        stmt = new EsfPreparedStatement( con, "SELECT path_name FROM esf_transaction_template WHERE package_id=?" );
	        stmt.set(id);
	        EsfResultSet rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	do
	        	{
		        	EsfPathName pathName = rs.getEsfPathName();
		            errors.addError("The package is still referenced in the transaction template " + pathName + ".");
	        	} while ( rs.next() );
	        	 
	        	return false;
	        }
	        stmt.close();
	        
	        stmt = new EsfPreparedStatement( con, "SELECT COUNT(*) FROM esf_transaction WHERE package_id=?" );
	        stmt.set(id);
	        rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	int count = rs.getInt();
	        	if ( count > 0 )
	        	{
		        	errors.addError("The package is still referenced in " + count + " transaction(s).");
		        	return false;
	        	}
	        }
	        
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   	    private static PathNameUUIDUserListableCacheReadOptimized<Package> cache = new PathNameUUIDUserListableCacheReadOptimized<Package>();

   	    public static Package getByPathName(final EsfPathName pathName)
   	    {
   	    	if ( pathName == null || ! pathName.isValid() )
   	    		return null;
   	    	
   	    	_logger.debug("getByName() for pathName: " + pathName);   	    	

   	    	Package pkg = cache.getByPathName(pathName);
   	    	if ( pkg != null )
   	    		return pkg;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT id FROM esf_package WHERE lower(path_name) = ?"
   	        									);
   	        	stmt.set(pathName.toLowerCase());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            con.commit();
	   	            
	   	            return getById(id);
	            }
	            
   	            con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByName() - package pathName: " + pathName);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        _logger.debug("Manager.getByName() - package pathName: " + pathName + "; failed to find the package");
   	        return null; 
   	    }

   		public static Package getById(Connection con, EsfUUID pkgId) 
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT path_name,download_file_name_spec,description,status,production_version,test_version,comments " +
   	        			"FROM esf_package WHERE id = ?"
   	        									);
   	        	stmt.set(pkgId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
		            EsfPathName pathName = rs.getEsfPathName();
		            String downloadFileNameSpec = rs.getString();
		            String description = rs.getString();
		            String  stat = rs.getString();
		            int productionVersion = rs.getInt();
		            int testVersion = rs.getInt();
		            String comments = rs.getString();
		            
		        	Permission permission = Permission.Manager.getById(pkgId); // the package id is also the package's permission id

		        	Package pkg = new Package(pkgId,pathName,downloadFileNameSpec,description,stat,productionVersion,testVersion,comments,permission);
		            pkg.setLoadedFromDb();
		            cache.add(pkg);

	   	            return pkg;
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + pkgId + "; failed to find the package");
   	        return null; 
   		}
   		
   		public static Package getById(EsfUUID pkgId)
   		{
   	    	Package pkg = cache.getById(pkgId);
   	    	if ( pkg != null )
   	    		return pkg;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	pkg = getById(con,pkgId);
   	        	if ( pkg != null ) 
   	        	{
		            con.commit();
	   	        	return pkg;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   		public static Collection<Package> getForUserWithPermission(User user, EsfName permOptionName)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<Package> list = new LinkedList<Package>();
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT PKG.id,PKG.path_name " +
   	        			"FROM esf_package PKG, esf_permission_option_group PERM " +
   	        			"WHERE PERM.permission_id = PKG.id AND lower(PERM.permission_option_esfname)=? AND " +
   	        			"( PERM.group_id=? OR PERM.group_id IN (SELECT GU.group_id FROM esf_group_user GU WHERE GU.user_id=?) ) " +
   	        			"ORDER BY lower(PKG.path_name) ASC"
   	        									);
   	        	stmt.set(permOptionName.toLowerCase());
   	        	stmt.set(Group.Manager.getAllUsersGroup().getId());
   	        	stmt.set(user.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	   	            
	   	            Package pkg = getById(id);
	   	            if ( pkg != null )
	   	            	list.add(pkg);
	            }
	            
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getForUserWithPermission() - userId: " + user.getId() + "; permOptionName: " + permOptionName);
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		
   	    public static Package createLike(Package likePkg, EsfPathName pathName, User createdBy)
   	    {
   	    	// We don't let you create new reserved objects
   	    	if ( pathName.hasReservedPathPrefix() )
   	    		return null;

   	    	if ( ! likePkg.permission.hasPermission(createdBy, PermissionOption.PERM_OPTION_CREATELIKE) )
   	    		return null;	
   	    	
   	        EsfUUID id = new EsfUUID();
   	        String normalizedId = id.toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+normalizedId);
   	        Permission newPermission = Permission.Manager.createLike(id, permPathName, likePkg.permission);

   	        // Note that the template's permissions can contain groups that even the creator doesn't have access to, but
   	        // they remain in the new template (so they can have permission) and the user can't even see them so 
   	        // cannot modify them away.
   	    	Package newPkg = new Package(id,pathName,newPermission);
   	    	newPkg.setDownloadFileNameSpec( likePkg.getDownloadFileNameSpec() );
   	    	newPkg.setDescription( likePkg.getDescription() );
   	    	cache.addIdOnly(newPkg);
   	    	return newPkg;
   	    }

   	    public static Package createNew(EsfPathName pathName, User createdBy)
   	    {
   	        EsfUUID id = new EsfUUID();
   	    	String normalizedId = id.toNormalizedEsfNameString();

   	    	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+normalizedId);
   	    	Permission newPermission = Permission.Manager._dbsetup_createForSuperGroup(id, permPathName, "Package permissions", Group.Manager.getSuperGroup());
   	    	newPermission.addGroupToOptions(Group.Manager.getAllUsersGroup(), PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);

   	    	Package pkg = new Package(id,new EsfPathName("TestDocument/Package/"+normalizedId),newPermission);
   	    	pkg.setDescription("Test document package only");
   	    	cache.addIdOnly(pkg);
   	    	return pkg;
   	    }

   	    public static Package createForTestDocument(User createdBy)
   	    {
   	        EsfUUID id = new EsfUUID();
   	    	String normalizedId = id.toNormalizedEsfNameString();

   	    	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+normalizedId);
   	    	Permission newPermission = Permission.Manager._dbsetup_createForSuperGroup(permPathName, "Package permissions", Group.Manager.getSuperGroup());
   	    	newPermission.addGroupToOptions(Group.Manager.getAllUsersGroup(), PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);

   	    	Package pkg = new Package(id,new EsfPathName("TestDocument/Package/"+normalizedId),newPermission);
   	    	pkg.setDescription("Test document package only");
   	    	cache.addIdOnly(pkg);
   	    	return pkg;
   	    }

   	    // Only called by DbSetup
   	    public static Package _dbsetup_createTemplate(Group superGroup, Group systemAdminGroup)
   	    {
   	        Package template = new Package(new EsfUUID(),TEMPLATE_ESFPATHNAME,null);
   	        template.setDescription("Template package");
   	        template.setComments("The template package used as the root for creating all other packages. Admins generally create a template for each company/group so they can create their own packages with similar permission.");

   	    	String normalizedId = template.getId().toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+normalizedId);
   	    	template.permission = Permission.Manager._dbsetup_createForSuperGroup(template.getId(), permPathName, "Template package permissions", superGroup);
   	    	template.permission.addGroupToAllOptions(systemAdminGroup);
   	    	cache.addIdOnly(template);
   	    	return template;
   	    }
   	    

   	    /**
   	     * Returns the Template package 
   	     * @return the Package that represents the Template
   	     */
   	    public static Package getTemplate()
   	    {
   	    	return getByPathName(TEMPLATE_ESFPATHNAME);
   	    }
   	     
   	    /**
   	     * Returns the total number of package objects in the cache
   	     * @return the total number of packages in the cache
   	     */
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all packages that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}

   	} // Manager
   	
}