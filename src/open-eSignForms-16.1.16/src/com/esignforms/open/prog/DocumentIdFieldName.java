// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;

/**
* DocumentIdFieldName is just a utility class that keeps a document id and field name together.
* 
* @author Yozons, Inc.
*/
public class DocumentIdFieldName implements java.io.Serializable
{
	private static final long serialVersionUID = -8767836969293007516L;

	public EsfUUID documentId;
	public EsfName fieldName;
	
	public EsfName documentName; // optional for caching matching name
	
	public DocumentIdFieldName(EsfUUID documentId, EsfName fieldName)
	{
		this.documentId = documentId;
		this.fieldName = fieldName;
		this.documentName = null;
	}
	
	public DocumentIdFieldName(EsfUUID documentId, EsfName fieldName, EsfName documentName)
	{
		this.documentId = documentId;
		this.fieldName = fieldName;
		this.documentName = documentName;
	}
	
	public EsfUUID getDocumentId() { return documentId; }
	public EsfName getFieldName() { return fieldName; }
	public EsfName getDocumentName() { return documentName; }
	public boolean hasDocumentName() { return documentName != null && documentName.isValid(); }
	
	@Override
	public boolean equals(Object o) 
	{	
		if ( o instanceof DocumentIdFieldName )
		{
			DocumentIdFieldName other = (DocumentIdFieldName)o;
			if ( documentId.equals(other.documentId) ) 
			{
				return fieldName.equals(other.fieldName);
			} 
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return documentId.hashCode() + fieldName.hashCode();
	}
}
