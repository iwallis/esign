// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;

/**
* DocumentVersionInfo is a data-only object that holds information about a document version.
* It's not a mutable object (you can't save/delete it), just for lists of document versions and such.
* 
* @author Yozons, Inc.
*/
public class DocumentVersionInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<DocumentVersionInfo>, java.io.Serializable
{
	private static final long serialVersionUID = -10267214117135606L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DocumentVersionInfo.class);

    protected EsfUUID id;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    
    /**
     * This creates a new DocumentVersionInfo using the source object.
     * @param documentVersion the document version to use as the source
     */
    protected DocumentVersionInfo(DocumentVersion documentVersion)
    {
        this.id = documentVersion.getId();
        this.version = documentVersion.getVersion();
        this.createdTimestamp = documentVersion.getCreatedTimestamp();
        this.createdByUserId = documentVersion.getCreatedByUserId();
        this.lastUpdatedTimestamp = documentVersion.getLastUpdatedTimestamp();
        this.lastUpdatedByUserId = documentVersion.getLastUpdatedByUserId();
    }
    
    /**
     * This creates a DocumentVersionInfo object from data retrieved from the DB.
     */
    protected DocumentVersionInfo(
    		EsfUUID id, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId)
    {
        this.id = id;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public int getVersion()
    {
    	return version;
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }

    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }

    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }

    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }


    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof DocumentVersionInfo )
            return getId().equals(((DocumentVersionInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(DocumentVersionInfo o)
    {
    	return getVersion() - o.getVersion();
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all DocumentVersionInfo objects in the specified library.
   	     * @return the Collection of DocumentVersionInfo found with most recent versions first.
   	     */
   	    public static Collection<DocumentVersionInfo> getAll(Document document)
   	    {
   	    	LinkedList<DocumentVersionInfo> list = new LinkedList<DocumentVersionInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id, version, created_timestamp, created_by_user_id, last_updated_timestamp, last_updated_by_user_id " +
   	    	            "FROM esf_library_document_version " + 
   	    	            "WHERE library_document_id = ? " +
   	    	            "ORDER BY version DESC"
   	            						   );
   	        	stmt.set(document.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            
		            DocumentVersionInfo docVerInfo = new DocumentVersionInfo(id, version,
		            							createdTimestamp, createdByUserId, lastUpdatedTimestamp, lastUpdatedByUserId
		            		 												);
		            docVerInfo.setLoadedFromDb();
		            list.add(docVerInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - Document: " + document.getEsfName() + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - Document: " + document.getEsfName() + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static DocumentVersionInfo createLike(Document likeDocument, DocumentVersion likeDocumentVersion, User createdBy)
   	    {
   			DocumentVersion newDocumentVersion = DocumentVersion.Manager.createLike(likeDocument, likeDocumentVersion, createdBy);
   			DocumentVersionInfo newDocVerInfo = new DocumentVersionInfo(newDocumentVersion);
   			return newDocVerInfo;
   	    }

   	    public static DocumentVersionInfo createNew(Document document, User createdBy)
   	    {
   			DocumentVersion newDocumentVersion = DocumentVersion.Manager.createTest(document, createdBy);
   			DocumentVersionInfo newDocVerInfo = new DocumentVersionInfo(newDocumentVersion);
   			return newDocVerInfo;
   	    }

   	    public static DocumentVersionInfo createFromSource(DocumentVersion documentVersion)
   	    {
   	    	if ( documentVersion == null )
   	    		return null;
   	    	
   			DocumentVersionInfo docVerInfo = new DocumentVersionInfo(documentVersion);
   			docVerInfo.setLoadedFromDb();

   			return docVerInfo;
   	    }
   	} // Manager
   	
}