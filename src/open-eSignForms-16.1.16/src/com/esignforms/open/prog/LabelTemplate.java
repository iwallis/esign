// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import org.jdom2.Element;

import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.util.HtmlUtil;
import com.esignforms.open.util.XmlUtil;

/**
* LabelTemplate holds the definition of a single label that goes with a given FieldTemplate.
* 
* @author Yozons, Inc.
*/
public class LabelTemplate
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<LabelTemplate>, java.io.Serializable
{
	private static final long serialVersionUID = 2657162845264252739L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LabelTemplate.class);

    // Position of label in relationship to the field
    public static final String LABEL_POSITION_NO_AUTO_SHOW = "N"; // No label should be auto-emitted for this field -- or document manually does label 
    public static final String LABEL_POSITION_TOP_LEFT = "TL";
    public static final String LABEL_POSITION_TOP_CENTER = "TC";
    public static final String LABEL_POSITION_TOP_RIGHT = "TR";
    public static final String LABEL_POSITION_BOTTOM_LEFT = "BL";
    public static final String LABEL_POSITION_BOTTOM_CENTER = "BC";
    public static final String LABEL_POSITION_BOTTOM_RIGHT = "BR";
    public static final String LABEL_POSITION_LEFT = "L";
    public static final String LABEL_POSITION_RIGHT = "R";
    
    public static final String LABEL_SEPARATOR_NONE = "";
    public static final String LABEL_SEPARATOR_SPACE = " ";
    public static final String LABEL_SEPARATOR_COLON_SPACE = ": ";

    public static final String LABEL_SIZE_NORMAL = "N"; // use either normal or small labels
    public static final String LABEL_SIZE_SMALL = "S";
    
    public static final String SUPPRESS_NON_INPUT = "Y";
    public static final String SHOW_NON_INPUT = "N"; // any value other than Y actually

    
    protected EsfUUID id;
    protected String  label;
    protected String  position;
    protected String  separator;
    protected String  size;
    protected String  suppressNonInput;

    /**
     * This creates a LabelTemplate object from data retrieved from the DB.
     */
    protected LabelTemplate(EsfUUID id, String label, String position, String separator, String size, String suppressNonInput)
    {
    	this.id = id;
    	this.label = label;
    	this.position = position;
    	this.separator = separator;
    	this.size = size;
    	this.suppressNonInput = suppressNonInput;
    }
    
    // Creates a new label like a given label
    protected LabelTemplate(LabelTemplate likeTemplate)
    {
    	this.id = new EsfUUID();
       	this.label = likeTemplate.label;
    	this.position = likeTemplate.position;
    	this.separator = likeTemplate.separator;
    	this.size = likeTemplate.size;
    	this.suppressNonInput = likeTemplate.suppressNonInput;
    }
    
    // Creates a new label with default values
    protected LabelTemplate()
    {
    	this.id = new EsfUUID();
       	this.label = "";
    	this.position = LABEL_POSITION_TOP_LEFT;
    	this.separator = LABEL_SEPARATOR_COLON_SPACE;
    	this.size = LABEL_SIZE_SMALL;
    	this.suppressNonInput = SHOW_NON_INPUT;
    }
    
    public LabelTemplate duplicate()
    {
    	LabelTemplate lt = new LabelTemplate(id, label, position, separator, size, suppressNonInput);
    	lt.setDatabaseObjectLike(this);
    	return lt;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public String getLabel()
    {
    	return label;
    }
    public String getLabelForHtml()
    {
    	return HtmlUtil.toEscapedHtml(label);
    }
    public void setLabel(String v)
    {
    	if ( v == null )
    		label = v;
    	else if ( v.length() > Literals.LABEL_MAX_LENGTH )
    		label = v.substring(0,Literals.LABEL_MAX_LENGTH).trim();
        else
        	label = v.trim();
    	objectChanged();
    }
    
    public String getPosition()
    {
    	return position;
    }
    public boolean isPositionStacked()
    {
    	return isPositionTop() || isPositionBottom();
    }
    public boolean isPositionTop()
    {
    	return isPositionTopLeft() || isPositionTopCenter() || isPositionTopRight();
    }
    public boolean isPositionBottom()
    {
    	return isPositionBottomLeft() || isPositionBottomCenter() || isPositionBottomRight();
    }
    public boolean isPositionNoAutoShow()
    {
    	return LABEL_POSITION_NO_AUTO_SHOW.equals(position);
    }
    public boolean isPositionTopLeft()
    {
    	return LABEL_POSITION_TOP_LEFT.equals(position);
    }
    public boolean isPositionTopCenter()
    {
    	return LABEL_POSITION_TOP_CENTER.equals(position);
    }
    public boolean isPositionTopRight()
    {
    	return LABEL_POSITION_TOP_RIGHT.equals(position);
    }
    public boolean isPositionBottomLeft()
    {
    	return LABEL_POSITION_BOTTOM_LEFT.equals(position);
    }
    public boolean isPositionBottomCenter()
    {
    	return LABEL_POSITION_BOTTOM_CENTER.equals(position);
    }
    public boolean isPositionBottomRight()
    {
    	return LABEL_POSITION_BOTTOM_RIGHT.equals(position);
    }
    public boolean isPositionLeft()
    {
    	return LABEL_POSITION_LEFT.equals(position);
    }
    public boolean isPositionRight()
    {
    	return LABEL_POSITION_RIGHT.equals(position);
    }
    public void setPosition(String v)
    {
    	position = v == null ? LABEL_POSITION_NO_AUTO_SHOW : v.trim();
    	objectChanged();
    }
    
    public String getSeparator()
    {
    	return separator;
    }
    public String getSeparatorForHtml()
    {
    	String html = HtmlUtil.toEscapedHtml(separator);
    	html = html.replaceAll(" ", "&nbsp;");
    	return html;
    }
    public void setSeparator(String v)
    {
    	separator = v; // don't trim
    	objectChanged();
    }
    
    public String getSize()
    {
    	return size;
    }
    public boolean isSizeNormal()
    {
    	return LABEL_SIZE_NORMAL.equals(size);
    }
    public boolean isSizeSmall()
    {
    	return LABEL_SIZE_SMALL.equals(size);
    }
    public void setSize(String v)
    {
    	size = v == null ? LABEL_SIZE_NORMAL : v.trim();
    	objectChanged();
    }
    
    public String getSuppressNonInput()
    {
    	return suppressNonInput;
    }
    public boolean isSuppressNonInput()
    {
    	return SUPPRESS_NON_INPUT.equals(suppressNonInput);
    }
    public void setSuppressNonInput(String v)
    {
    	suppressNonInput = v == null ? SHOW_NON_INPUT : v.trim();
    	objectChanged();
    }
    
    public String getLabelAndSeparatorForHtml()
    {
    	if ( isPositionRight() )
    		return getSeparatorForHtml() + getLabelForHtml(); // for right, we put the separator first
    	return getLabelForHtml() + getSeparatorForHtml();
    }

    public String getCss(boolean isError)
    {
    	StringBuilder css = new StringBuilder();
		if ( isSizeSmall() )
			css.append("small");
		else
			css.append("normal");
		if ( isError )
			css.append("-error");
		
    	return css.toString();
    }
    public String getCss()
    {
    	return getCss(false);
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof LabelTemplate )
            return getId().equals(((LabelTemplate)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(LabelTemplate o)
    {
    	return getId().compareTo(o.getId());
    }

    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<LabelTemplate xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <label>").append(escapeXml(label)).append("</label>\n");
        buf.append(" <position>").append(escapeXml(position)).append("</position>\n");
        buf.append(" <separator>").append(escapeXml(separator)).append("</separator>\n");
        buf.append(" <size>").append(escapeXml(size)).append("</size>\n");
        buf.append(" <suppressNonInput>").append(escapeXml(suppressNonInput)).append("</suppressNonInput>\n");

        buf.append("</LabelTemplate>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 175 + id.getEstimatedLengthXml() + label.length() + position.length() + separator.length() + size.length() + suppressNonInput.length();
        return len; 
    }

    public synchronized boolean save(final Connection con)
		throws SQLException
	{
		//_logger.debug("save(con) on id: " + id + "; label: " + label + "; insert: " + doInsert());
	    
	    clearLastSQLException();
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        if ( doInsert() )
	        {
	            stmt = new EsfPreparedStatement(con, "INSERT INTO esf_label_template (id,label,position,separator,size,suppress_non_input) VALUES (?,?,?,?,?,?)");
	            stmt.set(id);
	            stmt.set(label);
	            stmt.set(position);
	            stmt.set(separator);
	            stmt.set(size);
	            stmt.set(suppressNonInput);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Insert failed for id: " + id + "; label: " + label);
	                return false;
	            }
	            
	            // Now we mark this object as if it were loaded fresh from the database
	            setLoadedFromDb();
	            
	            return true;
	        }
	        
	        if ( hasChanged() )
	        {
	            // We assume we'll update it instead
	            stmt = new EsfPreparedStatement( con, 
	            		"UPDATE esf_label_template SET label=?,position=?,separator=?,size=?,suppress_non_input=? WHERE id=?"
	            						   		);
	            stmt.set(label);
	            stmt.set(position);
	            stmt.set(separator);
	            stmt.set(size);
	            stmt.set(suppressNonInput);
	            stmt.set(id);
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Update failed for id: " + id + "; label: " + label);
	            }
	        }
	
	        // Now we mark this object as if it were loaded fresh from the database
	        setLoadedFromDb();
	        
	        return true;
	    }
	    catch(SQLException e)
	    {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; label: " + label + "; insert: " + doInsert());
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}
    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }


    synchronized boolean delete(final Connection con)
	    throws SQLException
	{
		_logger.debug("delete(con) on id: " + id + "; label: " + label);
	    
	    if ( doInsert() )
	    {
	    	_logger.warn("delete(con) - Ignored delete of label template that was pending an INSERT id: " + id + "; label: " + label);
	        objectDeleted();
	        return true;
	    }
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        // Delete the library
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_label_template WHERE id=?");
	        stmt.set(id);
	        int num = stmt.executeUpdate();
	        if ( num == 0 )
	        	_logger.warn("delete(con) - Failed to find esf_label_template database row to delete with id: " + id);
	        
	        objectDeleted();
	        return true;
	    }
	    catch(SQLException e)
	    {
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves the LabelTemplate with the specified id.
   	     */
   	    public static LabelTemplate getById(Connection con, EsfUUID id)
   	    	throws SQLException
   	    {
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT label,position,separator,size,suppress_non_input FROM esf_label_template WHERE id=? " );
   	        	stmt.set(id);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	String  value = rs.getString();
	            	String  position = rs.getString();
	            	String  separator = rs.getString();
	            	String  size = rs.getString();
	            	String  suppressNonInput = rs.getString();
		            
	            	LabelTemplate label = new LabelTemplate(id,value,position,separator,size,suppressNonInput);
	            	label.setLoadedFromDb();
	            	return label;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + id);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return null;
   	    }
   	    
   	    public static LabelTemplate getById(EsfUUID id)
   	    {
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	LabelTemplate labelTemplate = getById(con,id);
   	        	if ( labelTemplate == null )
   	        	{
   	        		con.rollback();
   	        		return null;
   	        	}
   	        	con.commit();
   	        	return labelTemplate;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + id);
   	            pool.rollbackIgnoreException(con,e);
   	   	        return null;
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	    }
   	    
   	    public static LabelTemplate createLike(LabelTemplate likeTemplate)
   	    {
   	    	LabelTemplate newTemplate = new LabelTemplate(likeTemplate);
   	    	return newTemplate;
   	    }
   	    
   	    public static LabelTemplate createNew()
   	    {
   	    	LabelTemplate newTemplate = new LabelTemplate();
   	    	return newTemplate;
   	    }
   	    
   	    public static LabelTemplate createFromJDOM(Element e)
   	    {
   	    	if ( e == null )
   	    		return null;
   	    	
   	    	LabelTemplate newLabelTemplate = new LabelTemplate(
   	    			new EsfUUID(), 
   	    			e.getChildText("label", e.getNamespace()),
   	    			e.getChildText("position", e.getNamespace()),
   	    			e.getChildText("separator", e.getNamespace()),
   	    			e.getChildText("size", e.getNamespace()),
   	    			e.getChildText("suppressNonInput", e.getNamespace())
   	    			);
   	    	newLabelTemplate.setNewObject();
   	    	return newLabelTemplate;
   	    }

   	} // Manager
    
}