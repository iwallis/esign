// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;

import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* FileVersion holds the definition of a specific version of a file held in a library or document version.
* 
* @author Yozons, Inc.
*/
public class FileVersion
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<FileVersion>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable, java.io.Serializable
{
	private static final long serialVersionUID = 613611809944414623L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(FileVersion.class);
    
    protected final EsfUUID id;
    protected final EsfUUID fileId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected String fileFileName;
    protected String fileMimeType;
    protected EsfUUID fileDataBlobId;
    protected byte[] fileData;
    protected int fileSize;
    
    private boolean fileDataChanged = false;
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();

    /**
     * This creates a FileVersion object from data retrieved from the DB.
     */
    protected FileVersion(EsfUUID id, EsfUUID fileId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		String fileFileName, String fileMimeType, EsfUUID fileDataBlobId, byte[] fileData)
    {
        this.id = id;
        this.fileId = fileId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.fileFileName = fileFileName;
        this.fileMimeType = fileMimeType;
        this.fileDataBlobId = fileDataBlobId;
        this.fileData = fileData;
        this.fileSize = fileData == null ? 0 : fileData.length;
    }
    
    protected FileVersion(File file, User createdByUser)
    {
        this.id = new EsfUUID();
        this.fileId = file.getId();
        this.version = file.getProductionVersion() + 1;
        this.createdTimestamp = new EsfDateTime();
        this.createdByUserId = createdByUser.getId();
        this.lastUpdatedTimestamp = createdTimestamp;
        this.lastUpdatedByUserId = createdByUserId;
        this.fileFileName = null;
        this.fileMimeType = null;
        this.fileDataBlobId = new EsfUUID();
        this.fileData = null;
        this.fileSize = 0;
    }
    
    public FileVersion duplicate()
    {
    	FileVersion iv = new FileVersion(id, fileId, version, 
        		createdTimestamp.duplicate(), createdByUserId,  lastUpdatedTimestamp.duplicate(), lastUpdatedByUserId,
        		fileFileName, fileMimeType, fileDataBlobId, fileData == null ? null : fileData.clone());
    	iv.setDatabaseObjectLike(this);
    	return iv;
    }
   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getFileId()
    {
        return fileId;
    }
    
    public File getFile()
    {
    	return File.Manager.getById(fileId);
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}

	public int getVersion()
    {
    	return version;
    }
    
	public String getVersionLabel()
	{
		return getFile().getVersionLabel(version);
	}
	public String getFileNameVersion()
	{
		return getFile().getEsfName() + " [" + version + "]";
	}
	public String getFileNameVersionWithLabel()
	{
		File file = getFile();
		return file.getEsfName() + " [" + version + "] (" + file.getVersionLabel(version) + ")";
	}
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }
    
    public String getFileFileName()
    {
    	return fileFileName;
    }
	public boolean hasFileFileName() {
		return EsfString.isNonBlank(fileFileName);
	}
    public void setFileFileName(String v)
    {
    	if ( v != null )
    		v = v.trim();
    	fileFileName = v;
    	objectChanged();
    }
    
    public String getFileMimeType()
    {
    	return fileMimeType;
    }
    public void setFileMimeType(String v)
    {
    	if ( v != null )
    		v = v.trim();
    	fileMimeType = v;
    	objectChanged();
    }
    public boolean isContentTypeBrowserSafe()
    {
        return Application.getInstance().isContentTypeBrowserSafe(fileMimeType);
    }
    public boolean isContentTypePDF()
    {
    	return Application.CONTENT_TYPE_PDF.equals(fileMimeType);
    }
    
    public EsfUUID getFileDataBlobId()
    {
        return fileDataBlobId;
    }

    public byte[] getFileData()
    {
        return fileData;
    }
    public void setFileData(byte[] v)
    {
    	fileData = v;
    	fileSize = v.length;
    	fileDataChanged = true;
    	objectChanged();
    }
    public String getFileUrl()
    {
    	return getFileUrlWithRandomToAvoidBrowserCache(false);
    }
    public String getFileUrlWithRandomToAvoidBrowserCache(boolean addRandom)
    {
    	String random = addRandom ? "?rand="+Application.getInstance().getRandomKey().getAlphaNumericString(8) : "";
    	return Application.getInstance().getContextPath() + "/files/" + getFile().getEsfName() + "/" + getFileFileName() + random;
    }
    public String getFileByIdUrl() // used internally
    {
    	return Application.getInstance().getContextPath() + "/filesById/" + getId() + "/" + getFileFileName() + "?rand="+Application.getInstance().getRandomKey().getAlphaNumericString(8);
    }

    public int getFileSize()
    {
    	return fileSize;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof FileVersion )
        {
        	FileVersion otherFile = (FileVersion)o;
            return getId().equals(otherFile.getId());
        }
        return false;
    }
    
    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(FileVersion o)
    {
    	if ( getFileId().equals(o.getFileId()) )
    		return getVersion() - o.getVersion();
    	return getFileId().compareTo(o.getFileId());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<FileVersion xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <fileId>").append(fileId.toXml()).append("</fileId>\n");
        buf.append(" <version>").append(version).append("</version>\n");
        buf.append(" <createdTimestamp>").append(createdTimestamp.toXml()).append("</createdTimestamp>\n");
        buf.append(" <createdByUserId>").append(createdByUserId.toXml()).append("</createdByUserId>\n");
        buf.append(" <lastUpdatedTimestamp>").append(lastUpdatedTimestamp.toXml()).append("</lastUpdatedTimestamp>\n");
        buf.append(" <lastUpdatedByUserId>").append(lastUpdatedByUserId.toXml()).append("</lastUpdatedByUserId>\n");
        buf.append(" <fileFileName>").append(escapeXml(fileFileName)).append("</fileFileName>\n");
        buf.append(" <fileMimeType>").append(escapeXml(fileMimeType)).append("</fileMimeType>\n");
        buf.append(" <fileData>").append(base64encode(fileData)).append("</fileData>\n");

        buf.append("</FileVersion>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 190 + id.getEstimatedLengthXml() + fileId.getEstimatedLengthXml() + createdTimestamp.getEstimatedLengthXml() + createdByUserId.getEstimatedLengthXml() +
        	lastUpdatedTimestamp.getEstimatedLengthXml() + lastUpdatedByUserId.getEstimatedLengthXml() + fileFileName.length() + fileMimeType.length();
        len += (4 * fileData.length) / 3;
        return len; 
    }

    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; fileId: " + fileId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
            	
				getBlobDb().insert(con, fileDataBlobId, fileData, BlobDb.CompressOption.DISABLE, BlobDb.EncryptOption.DISABLE);
				
				stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_file_version (id,library_file_id,version,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,file_file_name,file_mime_type,file_blob_id) VALUES (?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(fileId);
                stmt.set(version);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(fileFileName);
                stmt.set(fileMimeType);
                stmt.set(fileDataBlobId);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; fileId: " + fileId + "; version: " + version);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                fileDataChanged = false;
                setLoadedFromDb();
                Manager.cache.replace(this);
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new file version id: " + id + "; fileId: " + fileId + "; version: " + version); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new file version id: " + id + "; fileId: " + fileId + "; version: " + version);
                }
                
                return true;
            }
            
			if ( fileDataChanged && ! getBlobDb().update(con, fileDataBlobId, fileData, BlobDb.CompressOption.DISABLE, BlobDb.EncryptOption.DISABLE) )
				return false;
        	
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_file_version SET last_updated_timestamp=?,last_updated_by_user_id=?,file_file_name=?,file_mime_type=? WHERE id=?"
                						   		);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(fileFileName);
                stmt.set(fileMimeType);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for file version id: " + id + "; fileId: " + fileId + "; version: " + version);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
			fileDataChanged = false;
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated file version id: " + id + "; fileId: " + fileId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated file version id: " + id + "; fileId: " + fileId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; fileId: " + fileId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; fileId: " + fileId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use 
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of file version that was pending an INSERT id: " + id + "; fileId: " + fileId + "; version: " + version);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
            // Delete the file version and its file data
        	getBlobDb().delete(con,fileDataBlobId);
            
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_file_version WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted file version id: " + id + "; fileId: " + fileId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted file version id: " + id + "; fileId: " + fileId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"delete() on id: " + id + "; fileId: " + fileId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   		private static UUIDCacheReadOptimized<FileVersion> cache = new UUIDCacheReadOptimized<FileVersion>();
   		
   		static final FileVersion getById(Connection con, EsfUUID fileVersionId) throws SQLException
   		{
   			return getById(con,fileVersionId,true);
   		}
   		static FileVersion getById(Connection con, EsfUUID fileVersionId, boolean checkCache) throws SQLException
   		{
   			if ( checkCache )
   			{
   				FileVersion fileVer = cache.getById(fileVersionId);
   	   			if ( fileVer != null )
   	   				return fileVer;
   			}

   			EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT library_file_id,version,created_by_user_id,last_updated_by_user_id,created_timestamp,last_updated_timestamp," +
   	        			"file_file_name,file_mime_type,file_blob_id " +
   	        			"FROM esf_library_file_version WHERE id=?"
   	        									);
   	        	stmt.set(fileVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID libraryFileId = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            String fileFileName = rs.getString();
		            String fileMimeType = rs.getString();
		            EsfUUID fileDataBlobId = rs.getEsfUUID();
		            
		            byte[] fileData = Application.getInstance().getBlobDb().select(con, fileDataBlobId);
		            
		            FileVersion fileVer = new FileVersion(fileVersionId,libraryFileId,version,
		            											 createdTimestamp,createdByUserId,lastUpdatedTimestamp,lastUpdatedByUserId,
		            											 fileFileName,fileMimeType,fileDataBlobId,fileData);
		            fileVer.setLoadedFromDb();
		            cache.add(fileVer);
		            
	   	            return fileVer;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + fileVersionId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + fileVersionId + "; failed to find the file version");
   	        return null; 
  		}
   		
   		public static FileVersion getById(EsfUUID fileVersionId)
   		{
   			FileVersion fileVer = cache.getById(fileVersionId);
   			if ( fileVer != null )
   				return fileVer;
   	   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	fileVer = getById(con,fileVersionId,false);
   	        	if ( fileVer != null ) 
   	        	{
		            con.commit();
	   	        	return fileVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		static FileVersion getByVersion(Connection con, EsfUUID libraryFileId, int version) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_file_version WHERE library_file_id=? AND version=?" );
   	        	stmt.set(libraryFileId);
   	        	stmt.set(version);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID docVersionId = rs.getEsfUUID();
	       			return getById(con,docVersionId);
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByVersion() - libraryFileId: " + libraryFileId + "; version: " + version);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getByVersion() - libraryFileId: " + libraryFileId + "; version: " + version + "; failed to find the file version");
   	        return null; 
  		}
   		
   		public static FileVersion getByVersion(EsfUUID libraryFileId, int version)
   		{
   			// While not always true, if the version is 1, it's possible this is a new object that's not been saved yet so we can't find the id in the DB, so we'll check our cache first just in case
   			if ( version == 1 )
   			{
   				for( EsfUUID id : cache.getAllIds() )
   				{
   					FileVersion fileVer = cache.getById(id);
   					if ( fileVer.getFileId().equals(libraryFileId) && fileVer.getVersion() == version )
   						return fileVer;
   				}
   			}
   			
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	FileVersion fileVer = getByVersion(con,libraryFileId,version);
   	        	if ( fileVer != null ) 
   	        	{
		            con.commit();
	   	        	return fileVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		
   		public static List<FileVersion> getAllByFileId(Connection con, EsfUUID libraryFileId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<FileVersion> list = new LinkedList<FileVersion>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_file_version WHERE library_file_id=?" );
   	        	stmt.set(libraryFileId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID fileVersionId = rs.getEsfUUID();
	            	
	            	FileVersion fileVer = getById(con,fileVersionId);
	       			if ( fileVer != null )
	       				list.add(fileVer);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<FileVersion> getAllByFileId(EsfUUID libraryFileId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<FileVersion> list = getAllByFileId(con,libraryFileId);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return new LinkedList<FileVersion>(); 
   		}
   	    
   		
   	    
   	    public static FileVersion createTest(File file, User createdBy)
   	    {
   	    	if ( file == null || createdBy == null )
   	    		return null;
   	    	FileVersion newFileVersion = new FileVersion(file,createdBy);
   	    	cache.add(newFileVersion);
   	    	return newFileVersion;
   	    }

   	    public static FileVersion createLike(File file, FileVersion likeFileVersion, User createdBy)
   	    {
   	    	if ( file == null || likeFileVersion == null || createdBy == null )
   	    		return null;
   	    	FileVersion newFileVersion = createTest(file,createdBy);
   	    	newFileVersion.fileFileName = likeFileVersion.fileFileName;
   	    	newFileVersion.fileMimeType = likeFileVersion.fileMimeType;
   	    	newFileVersion.fileData = likeFileVersion.fileData;

   	    	return newFileVersion;
   	    }

   	    public static FileVersion createFromJDOM(File file, Element e)
   	    {
   	    	if ( file == null || e == null )
   	    		return null;
   	    	
   	    	EsfUUID id = new EsfUUID();
   	    	
			FileVersion newFileVersion = new FileVersion(
   	    			id, 
   	    			file.getId(), 
   	    			file.getTestVersion(),
   	    			null,
   	    			null,
   	    			null,
   	    			null,
   	    			e.getChildText("fileFileName", e.getNamespace()),
   	    			e.getChildText("fileMimeType", e.getNamespace()),
   	    			new EsfUUID(),
   	    			com.esignforms.open.util.Base64.decode((e.getChildText("fileData", e.getNamespace())))
   	    			);
			newFileVersion.setNewObject();
   	    	cache.add(newFileVersion);
   	    	return newFileVersion;
   	    }
   	    
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all file versions that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }

   		public static void clearCache()
   		{
   			cache.clear();
   		}
   		
   		public static void dropFromCache(FileVersion fileVersion)
   		{
   	   		cache.remove(fileVersion);
   		}
   		public static void replaceInCache(FileVersion fileVersion)
   		{
   	   		cache.remove(fileVersion);
   	   		cache.add(fileVersion);
   		}
   		
   	} // Manager
   	
}