// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;

/**
* ReportTemplateReportField holds the definition of a single report field in a single report.
* We're not truly a database object since we retrieve and save as a group.
* When used in a Report, it also has an EsfValue for the value that represents this column in the report.
* 
* @author Yozons, Inc.
*/
public class ReportTemplateReportField
	extends com.esignforms.open.db.DatabaseObject
	implements java.io.Serializable
{
	private static final long serialVersionUID = 8568005785026687312L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ReportTemplateReportField.class);

	public final static String ALLOW_SEARCH = Literals.Y;
    public final static String DO_NOT_ALLOW_SEARCH = Literals.N;

    protected EsfUUID reportTemplateId;
    protected EsfUUID reportFieldTemplateId;
    protected short fieldOrder; 
    protected String fieldLabel; 
    protected String outputFormatSpec; 
    protected String allowSearch; 
    
    protected EsfValue fieldValue; // only present when used in a Report
    protected EsfValue[] fieldValues; // only present when used in a Report
    
    /**
     * This creates a ReportTemplateReportField object from data retrieved from the DB.
     */
    protected ReportTemplateReportField(EsfUUID reportTemplateId, EsfUUID reportFieldTemplateId, short fieldOrder, String fieldLabel, String outputFormatSpec, String allowSearch )
    {
    	this.reportTemplateId = reportTemplateId;
    	this.reportFieldTemplateId = reportFieldTemplateId;
    	this.fieldOrder = fieldOrder;
    	this.fieldLabel = fieldLabel;
    	this.outputFormatSpec = outputFormatSpec;
    	this.allowSearch = allowSearch;
    }
    
    public ReportTemplateReportField duplicate()
    {
    	ReportTemplateReportField rtrf = new ReportTemplateReportField(reportTemplateId,reportFieldTemplateId,fieldOrder,fieldLabel,outputFormatSpec,allowSearch);
    	return rtrf;
    }

    
    public EsfUUID getReportTemplateId()
    {
        return reportTemplateId;
    }
    public ReportTemplate getReportTemplate()
    {
    	return ReportTemplate.Manager.getById(reportTemplateId);
    }
    
    public EsfUUID getReportFieldTemplateId()
    {
        return reportFieldTemplateId;
    }
    public ReportFieldTemplate getReportFieldTemplate()
    {
    	return ReportFieldTemplate.Manager.getById(reportFieldTemplateId);
    }
    public void setReportFieldTemplateId(EsfUUID v)
    {
    	if ( v != null && ! v.isNull() )
    	{
    		reportFieldTemplateId = v;
    		objectChanged();
    	}
    }
    
    public short getFieldOrder()
    {
        return fieldOrder;
    }
    public void setFieldOrder(short v)
    {
    	if ( fieldOrder != v )
    	{
    		fieldOrder = v;
    		objectChanged();
    	}
    }
    
    public String getFieldLabel()
    {
    	return fieldLabel;
    }
    public void setFieldLabel(String v)
    {
    	if ( v == null )
    		fieldLabel = v;
    	else if ( v.length() > Literals.LABEL_MAX_LENGTH )
    		fieldLabel = v.substring(0,Literals.LABEL_MAX_LENGTH).trim();
        else
        	fieldLabel = v.trim();
    	objectChanged();
    }
    
    public String getOutputFormatSpec()
    {
    	return outputFormatSpec;
    }
    public boolean hasOutputFormatSpec()
    {
    	return EsfString.isNonBlank(outputFormatSpec);
    }
    public void setOutputFormatSpec(String v)
    {
    	outputFormatSpec = v == null ? null : v.trim();
    	objectChanged();
    }
    
    public String getAllowSearch()
    {
    	return allowSearch;
    }
    public boolean isAllowSearch()
    {
    	return ALLOW_SEARCH.equals(allowSearch);
    }
    public void setAllowSearch(String v)
    {
    	if ( ! allowSearch.equals(v) )
    	{
    		allowSearch = ALLOW_SEARCH.equals(v) ? ALLOW_SEARCH : DO_NOT_ALLOW_SEARCH;
    		objectChanged();
    	}
    }
    public void setAllowSearch(boolean v)
    {
    	setAllowSearch( v ? ALLOW_SEARCH : DO_NOT_ALLOW_SEARCH );
    }
    
    public boolean hasFieldValueOrValues()
    {
    	return hasFieldValue() || hasFieldValues();
    }
    
    public EsfValue getFieldValue()
    {
    	return fieldValue;
    }
    public boolean hasFieldValue()
    {
    	return fieldValue != null && ! fieldValue.isNull();
    }
    public void setFieldValue(EsfValue v)
    {
    	fieldValue = v;
    	fieldValues = null;
    }
    
    public EsfValue[] getFieldValues()
    {
    	return fieldValues;
    }
    public boolean hasFieldValues()
    {
    	return fieldValues != null && fieldValues.length > 0;
    }
    public void setFieldValues(EsfValue[] v)
    {
    	fieldValue = null;
    	fieldValues = v;
    }
    
    /* We don't do this since these objects don't have real equality checks. We tried below, but since we use Vaadin DnD on these objects,
     * reordering the report fields didn't work out as expected trying to use them for equality/hashcode.  We'll just use the default object rules.
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof ReportTemplateReportField )
        {
        	ReportTemplateReportField other = (ReportTemplateReportField)o;
            return getReportTemplateId().equals(other.getReportTemplateId()) && 
            		getReportFieldTemplateId().equals(other.getReportFieldTemplateId()) && 
            		getFieldOrder() == other.getFieldOrder(); 
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getReportTemplateId().hashCode() + getReportFieldTemplateId().hashCode() + getFieldOrder();
    }
    */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all ReportTemplateReportField objects held for a given report template.
   	     * @return the List of ReportTemplateReportField found.
   	     */
   	    public static List<ReportTemplateReportField> getAll(EsfUUID reportTemplateId)
   	    {
   	    	_logger.debug("Manager.getAll() - reportTemplateId: " + reportTemplateId);
   	    	
   	    	LinkedList<ReportTemplateReportField> list = new LinkedList<ReportTemplateReportField>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, 
   	        			"SELECT report_field_template_id,field_order,field_label,output_format_spec,allow_search FROM esf_report_template_report_field WHERE report_template_id=? ORDER BY field_order"
   	            						   		);
   	        	stmt.set(reportTemplateId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID reportFieldTemplateId = rs.getEsfUUID();
	            	short fieldOrder = rs.getShort();
	            	String fieldLabel = rs.getString();
	            	String outputFormatSpec = rs.getString();
	            	String allowSearch = rs.getString();
		            
	            	ReportTemplateReportField rtrf = new ReportTemplateReportField(reportTemplateId,reportFieldTemplateId,fieldOrder,fieldLabel,outputFormatSpec,allowSearch);
	            	rtrf.setLoadedFromDb();
		            list.add(rtrf);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - reportTemplateId: " + reportTemplateId + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - reportTemplateId: " + reportTemplateId + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	            list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }

   	    public static int save(Connection con, EsfUUID reportTemplateId, List<ReportTemplateReportField> list)
   	    	throws SQLException
   	    {
   	    	_logger.debug("Manager.save(con) - reportTemplateId: " + reportTemplateId + "; num fields: " + list.size());
   	    	
   	        EsfPreparedStatement stmt = null;
   	        int numFieldsInserted = 0;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_report_template_report_field WHERE report_template_id=?");
   	        	stmt.set(reportTemplateId);
   	        	int num = stmt.executeUpdate();
   	        	
   	        	stmt.close();
   	        	stmt = new EsfPreparedStatement(con,
   	        			"INSERT INTO esf_report_template_report_field (report_template_id,report_field_template_id,field_order,field_label,output_format_spec,allow_search) " +
   	        			"VALUES (?,?,?,?,?,?)");
   	        	short order = 1;
   	        	for( ReportTemplateReportField rtrf : list )
   	        	{
   	   	        	stmt.set(reportTemplateId);
   	   	        	stmt.set(rtrf.getReportFieldTemplateId());
   	   	        	stmt.set(order);
   	   	        	stmt.set(rtrf.getFieldLabel());
   	   	        	stmt.set(rtrf.getOutputFormatSpec());
   	   	        	stmt.set(rtrf.getAllowSearch());
   	   	        	num = stmt.executeUpdate();
   	   	        	if ( num != 1 )
   	   	        	{
   	    	            _logger.warn("Manager.save(con) - Failed insert for reportTemplateId: " + reportTemplateId + "; reportFieldTemplateId: " + rtrf.getReportFieldTemplateId() + "; order: " + order);
   	   	        	}
   	   	        	else
   	   	        		++numFieldsInserted;
   	   	        	++order;
   	        	}
   	        	
   	            _logger.debug("Manager.save(con) - reportTemplateId: " + reportTemplateId + "; numFieldsInserted: " + numFieldsInserted);
   	            return numFieldsInserted;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.save(con) - reportTemplateId: " + reportTemplateId + "; num fields: " + list.size());
   	            throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }
   	    }

   	    public static int save(EsfUUID reportTemplateId, List<ReportTemplateReportField> list)
   	    {
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	int numFields = save(con, reportTemplateId, list);
   	            con.commit();
   	            return numFields;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	            list.clear();
   	            return 0;
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }
   	    }
   	    
   	    public static ReportTemplateReportField createLike(EsfUUID reportTemplateId, ReportTemplateReportField likeReportTemplateReportField)
   	    {
   	    	ReportTemplateReportField rtrf = new ReportTemplateReportField(reportTemplateId,likeReportTemplateReportField.getReportFieldTemplateId(),
   	    			likeReportTemplateReportField.getFieldOrder(),likeReportTemplateReportField.getFieldLabel(),likeReportTemplateReportField.getOutputFormatSpec(),
   	    			likeReportTemplateReportField.getAllowSearch());
   	    	return rtrf;
   	    }

   	    public static ReportTemplateReportField createNew(EsfUUID reportTemplateId, short order)
   	    {
   	    	ReportTemplateReportField rtrf = new ReportTemplateReportField(reportTemplateId, new EsfUUID("0-0-0-0-0"), order, "", "", DO_NOT_ALLOW_SEARCH);
   	    	return rtrf;
   	    }
   	} // Manager
    
}