// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.Version;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.action.Action;
import com.esignforms.open.user.User;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* DocumentVersion holds the definition of a specific version of a document held in a library.
* A document has one or more pages.
* 
* @author Yozons, Inc.
*/
public class DocumentVersion
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<DocumentVersion>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable, java.io.Serializable
{
	private static final long serialVersionUID = -3362675987613750087L;

	public static String PAGE_ORIENTATION_PORTRAIT  = "P"; // Default
	public static String PAGE_ORIENTATION_LANDSCAPE = "L";
	
	public static String ESIGN_PROCESS_RECORD_LOCATION_FINAL_OWN_PAGE  = "F"; // Default, on a new page that will be the last page (inserts a page break for printing)
	public static String ESIGN_PROCESS_RECORD_LOCATION_LAST_DOCUMENT_PAGE = "D"; // Last page of the document (together with the last page of the document if it fits, no page break inserted)
	public static String ESIGN_PROCESS_RECORD_LOCATION_NONE = "N"; // None - do not include
	
    private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DocumentVersion.class);

    protected final EsfUUID id;
    protected final EsfUUID documentId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected EsfUUID documentStyleId;
    protected String pageOrientation; 
    protected String esignProcessRecordLocation; 
    protected EsfUUID programmingBlobId; // may be null; if present, is a blob id that contains our DocumentProgramming configuration (in XML).
    protected DocumentProgramming documentProgramming; // is null until loaded, if any such programming has been configured

    protected List<DocumentVersionPage> pageList;
    protected boolean pageListChanged;
    protected Map<EsfName,FieldTemplate> fieldTemplateMap;
    protected List<FieldTemplate> fieldTemplatesToRemoveOnSave;
    protected List<PartyTemplate> partyTemplateList; // We use a list since the order is important (generally ordered by processOrder)
    protected List<PartyTemplate> partyTemplatesToRemoveOnSave;
    
    public static class DocumentFile 
    	implements java.io.Serializable
    {
		private static final long serialVersionUID = -2546086770091877455L;

		public File file;
    	public FileVersion fileVersion;
    	public DocumentFile() {}
    	public DocumentFile(File f, FileVersion fv)
    	{
    		this.file = f;
    		this.fileVersion = fv;
    	}
    }
    protected List<DocumentFile> documentFileList;
    protected List<DocumentFile> documentFilesToRemoveOnSave;
    
    public static class DocumentImage 
    	implements java.io.Serializable
    {
		private static final long serialVersionUID = 2636828105261171847L;

		public Image image;
    	public ImageVersion imageVersion;
    	public DocumentImage() {}
    	public DocumentImage(Image i, ImageVersion iv)
    	{
    		this.image = i;
    		this.imageVersion = iv;
    	}
    }
    protected List<DocumentImage> documentImageList;
    protected List<DocumentImage> documentImagesToRemoveOnSave;
    
    public static class DocumentDropdown 
		implements java.io.Serializable
	{
		private static final long serialVersionUID = -426452246280155138L;

		public DropDown dropdown;
		public DropDownVersion dropdownVersion;
		public DocumentDropdown() {}
		public DocumentDropdown(DropDown d, DropDownVersion dv)
		{
			this.dropdown = d;
			this.dropdownVersion = dv;
		}
	}
	protected List<DocumentDropdown> documentDropdownList;
	protected List<DocumentDropdown> documentDropdownsToRemoveOnSave;
    
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();

    // Used during page parsing to search the libraries
	private Map<EsfName,FieldTemplate> libraryFieldTemplateMap;
	private Map<EsfName,FieldTemplate> templateLibraryFieldTemplateMap;

    /**
     * This creates a DocumentVersion object from data retrieved from the DB.
     */
    protected DocumentVersion(EsfUUID id, EsfUUID documentId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		EsfUUID documentStyleId, String pageOrientation, String esignProcessRecordLocation,
    		List<DocumentVersionPage> pageList, Map<EsfName,FieldTemplate> fieldTemplateMap, List<PartyTemplate> partyTemplateList,
    		List<DocumentFile> documentFileList, List<DocumentImage> documentImageList, List<DocumentDropdown> documentDropdownList, EsfUUID programmingBlobId)
    {
        this.id = id;
        this.documentId = documentId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.documentStyleId = documentStyleId;
        this.pageOrientation = pageOrientation;
        this.esignProcessRecordLocation = esignProcessRecordLocation;
        this.pageList = pageList;
        this.pageListChanged = false;
        this.fieldTemplateMap = fieldTemplateMap;
        this.partyTemplateList = partyTemplateList;
        this.documentFileList = documentFileList;
        this.documentImageList = documentImageList;
        this.documentDropdownList = documentDropdownList;
        this.programmingBlobId = programmingBlobId;
    }
    
    protected DocumentVersion(Document document, User createdByUser)
    {
        this.id = new EsfUUID();
        this.documentId = document.getId();
        this.version = document.getProductionVersion() + 1;
        this.createdTimestamp = new EsfDateTime();
        this.createdByUserId = createdByUser.getId();
        this.lastUpdatedTimestamp = createdTimestamp;
        this.lastUpdatedByUserId = createdByUserId;
        this.documentStyleId = document.getLibrary().getDefaultDocumentStyleId();
        this.pageOrientation = PAGE_ORIENTATION_PORTRAIT;
        this.esignProcessRecordLocation = ESIGN_PROCESS_RECORD_LOCATION_FINAL_OWN_PAGE;
        this.programmingBlobId = null;
        
        pageList = new LinkedList<DocumentVersionPage>();
        DocumentVersionPage page1 = new DocumentVersionPage(this,(short)1);
        pageList.add(page1);
        pageListChanged = true;
        
        fieldTemplateMap = new TreeMap<EsfName,FieldTemplate>();
        partyTemplateList = new LinkedList<PartyTemplate>();
        documentFileList = new LinkedList<DocumentFile>();
        documentImageList = new LinkedList<DocumentImage>();
        documentDropdownList = new LinkedList<DocumentDropdown>();
    }

   
    // Used by the APIs that need a copy to update, but don't want to update our "real" object that is likely cached.
    public DocumentVersion duplicate() 
    {
    	DocumentVersion docVer = new DocumentVersion(id, documentId, version, createdTimestamp, createdByUserId, lastUpdatedTimestamp, lastUpdatedByUserId,
        		documentStyleId, pageOrientation, esignProcessRecordLocation, getDuplicatePages(), getDuplicateFieldTemplateMap(), getDuplicatePartyTemplateList(), 
        		getDuplicateDocumentFileList(), getDuplicateDocumentImageList(), getDuplicateDocumentDropdownList(), programmingBlobId);
    	if ( hasDocumentProgramming() )
    		docVer.documentProgramming = getDocumentProgramming().duplicate();
    	docVer.setDatabaseObjectLike(this);
    	return docVer;
    }
    
	@Override
    public boolean hasChanged()
    {
		if ( hasChanged )
			return true;
		
        // It's possible if there was no package programming that we loaded it, and only if it changed do we consider this object to have changed.
        if ( documentProgramming != null && documentProgramming.hasChanged() )
        	return true;

        // Let's check if any of our pages have changed
        if ( pageListChanged )
        	return true;
		for( DocumentVersionPage p : pageList ) 
		{
			if ( p.hasChanged() ) 
				return true;
		}
			
		// Let's check if any of our parties have changed
		for( PartyTemplate p : partyTemplateList ) 
		{
			if ( p.hasChanged() )
				return true;
		}
		
		// Let's check if any of our parties have changed
		for( EsfName fieldName : fieldTemplateMap.keySet() ) 
		{
			FieldTemplate f = fieldTemplateMap.get(fieldName);
			if ( f.hasChanged() ) 
				return true;
		}
		
		for( DocumentFile df : documentFileList ) 
		{
			if ( df.file.hasChanged() || df.fileVersion.hasChanged() )
				return true;
		}
		
		for( DocumentImage di : documentImageList ) 
		{
			if ( di.image.hasChanged() || di.imageVersion.hasChanged() )
				return true;
		}
		
		for( DocumentDropdown dd : documentDropdownList ) 
		{
			if ( dd.dropdown.hasChanged() || dd.dropdownVersion.hasChanged() )
				return true;
		}
		
		return false;
    }

	    
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getDocumentId()
    {
        return documentId;
    }
    
    public Document getDocument()
    {
    	return Document.Manager.getById(documentId);
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}

	public int getVersion()
    {
    	return version;
    }
	public String getVersionLabel()
	{
		return getDocument().getVersionLabel(version);
	}
	public String getDocumentNameVersion()
	{
		return getDocument().getEsfName() + " [" + version + "]";
	}
	public String getDocumentNameVersionWithLabel()
	{
		Document document = getDocument();
		return document.getEsfName() + " [" + version + "] (" + document.getVersionLabel(version) + ")";
	}
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }
    
    public EsfUUID getDocumentStyleId()
    {
        return documentStyleId;
    }
    public void setDocumentStyleId(EsfUUID v)
    {
        documentStyleId = v;
        objectChanged();
    }
    
    public DocumentStyle getDocumentStyle()
    {
    	return DocumentStyle.Manager.getById(documentStyleId);
    }
    
    public String getPageOrientation()
    {
    	return pageOrientation;
    }
    public boolean isPortrait()
    {
    	return ! isLandscape();
    }
    public boolean isLandscape()
    {
    	return PAGE_ORIENTATION_LANDSCAPE.equals(pageOrientation);
    }
    public void setPageOrientation(String v)
    {
    	pageOrientation = PAGE_ORIENTATION_LANDSCAPE.equals(v) ? PAGE_ORIENTATION_LANDSCAPE : PAGE_ORIENTATION_PORTRAIT;
    	objectChanged();
    }
    
    public String getEsignProcessRecordLocation()
    {
    	return esignProcessRecordLocation;
    }
    public boolean isEsignProcessRecordLocationFinalOwnPage()
    {
    	return ESIGN_PROCESS_RECORD_LOCATION_FINAL_OWN_PAGE.equals(esignProcessRecordLocation);
    }
    public boolean isEsignProcessRecordLocationLastDocumentPage()
    {
    	return ESIGN_PROCESS_RECORD_LOCATION_LAST_DOCUMENT_PAGE.equals(esignProcessRecordLocation);
    }
    public boolean isEsignProcessRecordLocationNone()
    {
    	return ESIGN_PROCESS_RECORD_LOCATION_NONE.equals(esignProcessRecordLocation);
    }
    public void setEsignProcessRecordLocation(String v)
    {
    	if ( ESIGN_PROCESS_RECORD_LOCATION_LAST_DOCUMENT_PAGE.equals(v) || ESIGN_PROCESS_RECORD_LOCATION_NONE.equals(v) )
    		esignProcessRecordLocation = v;
    	else
    		esignProcessRecordLocation = ESIGN_PROCESS_RECORD_LOCATION_FINAL_OWN_PAGE;
    	objectChanged();
    }
    
    
    public synchronized List<DocumentVersionPage> getPages()
    {
    	return new LinkedList<DocumentVersionPage>(pageList);
    }
    public synchronized int getNumPages()
    {
    	return pageList.size();
    }
    public synchronized boolean isMultiPage()
    {
    	return pageList.size() > 1;
    }
    public synchronized List<DocumentVersionPage> getDuplicatePages()
    {
    	LinkedList<DocumentVersionPage> list = new LinkedList<DocumentVersionPage>();
    	for( DocumentVersionPage p : pageList )
    		list.add(p.duplicate());
    	return list;
    }
    public synchronized List<DocumentVersionPage> getEditPages()
    {
    	LinkedList<DocumentVersionPage> list = new LinkedList<DocumentVersionPage>();
    	for( DocumentVersionPage p : pageList )
    	{
    		if ( p.isEdit() )
    			list.add(p);
    	}
    	return list;
    }
    public synchronized int getNumEditPages()
    {
    	int numEditPages = 0;
    	for( DocumentVersionPage p : pageList )
    	{
    		if ( p.isEdit() )
    			++numEditPages;
    	}
    	return numEditPages;
    }
    public synchronized List<DocumentVersionPage> getReviewPages()
    {
    	LinkedList<DocumentVersionPage> list = new LinkedList<DocumentVersionPage>();
    	for( DocumentVersionPage p : pageList )
    	{
    		if ( p.isReview() )
    			list.add(p);
    	}
    	return list;
    }
    public synchronized int getNumReviewPages()
    {
    	int numReviewPages = 0;
    	for( DocumentVersionPage p : pageList )
    	{
    		if ( p.isReview() )
    			++numReviewPages;
    	}
    	return numReviewPages;
    }
    public synchronized boolean hasReviewOnlyPages()
    {
    	for( DocumentVersionPage p : pageList )
    	{
    		if ( p.isReviewOnly() )
    			return true;
    	}
    	return false;
    }

    public synchronized DocumentVersionPage getPageByName(EsfName esfname)
    {
    	for( DocumentVersionPage page : pageList ) {
    		if ( page.getEsfName().equals(esfname) )
    			return page;
    	}
    	return null;
    }
    
    public synchronized DocumentVersionPage getPageNumber(int pageNum)
    {
    	int numPages = getNumPages();
    	return getPages().get( (pageNum < 1 || pageNum > numPages) ? 0 : pageNum-1 );
    }
    
    public synchronized DocumentVersionPage getPageById(EsfUUID pageId)
    {
    	for( DocumentVersionPage page : pageList ) {
    		if ( page.getId().equals(pageId) )
    			return page;
    	}
    	return null;
    }

	public synchronized void addPage(DocumentVersionPage newPage) 
	{
		pageList.add(newPage);
		pageListChanged = true;
		objectChanged();
	}
	
	public synchronized void removePage(DocumentVersionPage delPage) 
	{
		pageList.remove(delPage);
    	pageListChanged = true;
		objectChanged();
	}
    
	public synchronized void setPageOrder(LinkedList<EsfName> pageOrderNames) {
		
		if ( pageOrderNames == null || pageOrderNames.size() == 0 || getNumPages() == 0 )
			return;
		
		LinkedList<DocumentVersionPage> origPageList = new LinkedList<DocumentVersionPage>(pageList);
    	LinkedList<DocumentVersionPage> newPageList = new LinkedList<DocumentVersionPage>();

    	// For each page requested, if we find it, we move it into our new order.
    	short pageNum = 1;
    	for( EsfName pageName : pageOrderNames ) {
        	ListIterator<DocumentVersionPage> origPageListIter = origPageList.listIterator();
        	while( origPageListIter.hasNext() ) {
        		DocumentVersionPage p = origPageListIter.next();
        		if ( p.getEsfName().equals(pageName) ) {
            		p.setPageNumber(pageNum++);
        			newPageList.add(p);
        			origPageListIter.remove();
        			break;
        		}
        	}
    	}
    	
    	// If we have any pages remaining (were not in the pageOrderNames list), let's move them in too
    	ListIterator<DocumentVersionPage> origPageListIter = origPageList.listIterator();
    	while( origPageListIter.hasNext() ) {
    		DocumentVersionPage p = origPageListIter.next();
    		p.setPageNumber(pageNum++);
    		newPageList.add(p);
    	}
    	
     	pageList = newPageList;
     	pageListChanged = true;
     	objectChanged();
	}
	
	public Map<EsfName,FieldTemplate> getFieldTemplateMap()
	{
		return new TreeMap<EsfName,FieldTemplate>(fieldTemplateMap);
	}
	public Map<EsfName,FieldTemplate> _getFieldTemplateMapInternal()
	{
		return fieldTemplateMap;
	}
	public Map<EsfName,FieldTemplate> getDuplicateFieldTemplateMap()
	{
		TreeMap<EsfName,FieldTemplate> map = new TreeMap<EsfName,FieldTemplate>();
    	for( EsfName name : fieldTemplateMap.keySet() ) 
    	{
    		FieldTemplate template = fieldTemplateMap.get(name);
    		map.put(name.duplicate(), template.duplicate());
    	}
    	return map;
	}

	public FieldTemplate getFieldTemplate(EsfName fieldTemplateName)
	{
		return fieldTemplateName == null ? null : fieldTemplateMap.get(fieldTemplateName);
	}
	public void setFieldTemplate(FieldTemplate fieldTemplate)
	{
		if ( fieldTemplateMap.put(fieldTemplate.getEsfName(),fieldTemplate) != null )
			objectChanged();
	}
	public void removeFieldTemplate(FieldTemplate fieldTemplate)
	{
		if ( fieldTemplateMap.remove(fieldTemplate.getEsfName()) != null ) 
		{
			addToFieldTemplatesToRemoveOnSave(fieldTemplate);
			// Remove it from any party mappings too
			for( PartyTemplate party : getPartyTemplateList() )
				party.removePartyTemplateFieldTemplate(fieldTemplate);
			// Remove it from any image overlays
			for( DocumentImage di : getDocumentImageList() )
				di.imageVersion.removeFieldTemplateFromOverlayFields(fieldTemplate.getId());
			objectChanged();
		}
	}
	public FieldTemplate getFieldTemplate(EsfUUID fieldTemplateId)
	{
    	for( EsfName name : fieldTemplateMap.keySet() ) 
    	{
    		FieldTemplate template = fieldTemplateMap.get(name);
    		if ( fieldTemplateId.equals(template.getId()) )
    			return template;
    	}
    	return null;
	}
	
	public List<FieldTemplate> getRadioButtonFieldTemplates(EsfName radioButtonGroupName)
	{
		LinkedList<FieldTemplate> list = new LinkedList<FieldTemplate>();
		String groupName = radioButtonGroupName.toString();
    	for( EsfName name : fieldTemplateMap.keySet() ) 
    	{
    		FieldTemplate template = fieldTemplateMap.get(name);
    		if ( template.isTypeRadioButton() && template.getOutputFormatSpec().equals(groupName) )
    			list.add(template);
    	}
    	return list;
	}

	private void resetAllContainerReferenceCounts()
	{
    	for( EsfName name : fieldTemplateMap.keySet() ) 
    	{
    		FieldTemplate template = fieldTemplateMap.get(name);
    		template.resetContainerReferenceCount();
    	}
	}
	
	synchronized void addToFieldTemplatesToRemoveOnSave(FieldTemplate delField)
	{
		if ( fieldTemplatesToRemoveOnSave == null )
			fieldTemplatesToRemoveOnSave = new LinkedList<FieldTemplate>();
		fieldTemplatesToRemoveOnSave.add(delField);
	}

	
	// Generally called by the DocumentVersionPage as it writes out its data. Returns the field template registered.
	public FieldTemplate registerField(String fieldSource, String fieldNameString, EsfName defaultNewFieldParty)
	{
		// If the field source is "document" then it belongs to us
		if ( Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(fieldSource) )
		{
			EsfName fieldName = new EsfName(fieldNameString);
			if ( fieldName.isValid() )
			{
				// First, let's see if we know this field already
				FieldTemplate fieldTemplate = fieldTemplateMap.get(fieldName);
				if ( fieldTemplate == null )
				{
					fieldTemplate = libraryFieldTemplateMap.get(fieldName);
					if ( fieldTemplate == null && templateLibraryFieldTemplateMap != null )
						fieldTemplate = templateLibraryFieldTemplateMap.get(fieldName);
					if ( fieldTemplate == null )
						fieldTemplate = FieldTemplate.Manager.createNew(id, fieldName);
					else
						fieldTemplate = FieldTemplate.Manager.createLike(id, fieldTemplate);
					// It's a newly created field, so if we have a default new field party defined, let's give that party access to this new field (except no need for radio buttons, just the group is for update)
					if ( defaultNewFieldParty != null && defaultNewFieldParty.isValid() && ! fieldTemplate.isTypeRadioButton() )
					{
						PartyTemplate party = getPartyTemplate(defaultNewFieldParty);
						if ( party != null )
						{
							PartyTemplateFieldTemplate partyTemplateFieldTemplate = PartyTemplateFieldTemplate.Manager.createNew(party.getId(), fieldTemplate.getId());
							party.addPartyTemplateFieldTemplates(partyTemplateFieldTemplate);
						}
					}
				}
				fieldTemplate.bumpContainerReferenceCount();
				fieldTemplateMap.put(fieldName, fieldTemplate);
				if ( fieldTemplate.isTypeRadioButton() ) // if it's a radio button, bump the count on the group
				{
					FieldTemplate radioButtonGroupTemplate = fieldTemplateMap.get(new EsfName(fieldTemplate.getOutputFormatSpec()));
					if ( radioButtonGroupTemplate != null && radioButtonGroupTemplate.isTypeRadioButtonGroup() )
						radioButtonGroupTemplate.bumpContainerReferenceCount();
				}
				objectChanged();
				return fieldTemplate;
			}
		}
		return null; // references a field on another document
	}
	
	// Our library search cache is limited to the library the document is in, and if the document is in a library other than the template library, then the template library is searched too.
    private void setupLibrarySearchFieldTemplateCache()
    {
		EsfUUID myLibraryId = getDocument().getLibraryId();
		EsfUUID templateLibraryId = Library.Manager.getTemplate().getId();

		libraryFieldTemplateMap = FieldTemplate.Manager.getAll(myLibraryId);
		
		if ( myLibraryId.equals(templateLibraryId) )
			templateLibraryFieldTemplateMap = null; // we're in the template library, so no need to search it again
		else
			templateLibraryFieldTemplateMap = FieldTemplate.Manager.getAll(templateLibraryId);
    }
    private void clearLibrarySearchFieldTemplateCache()
    {
    	if ( libraryFieldTemplateMap != null )
    	{
        	libraryFieldTemplateMap.clear();
        	libraryFieldTemplateMap = null;
    	}
    	if ( templateLibraryFieldTemplateMap != null )
    	{
    		templateLibraryFieldTemplateMap.clear();
    		templateLibraryFieldTemplateMap = null;
    	}
    }

	public synchronized void setPartyTemplateOrder(LinkedList<EsfName> partyOrderNames) {
		
		if ( partyOrderNames == null || partyOrderNames.size() == 0 || getNumPartyTemplates() < partyOrderNames.size() )
			return;
		
		LinkedList<PartyTemplate> origPartyList = new LinkedList<PartyTemplate>(partyTemplateList);
    	LinkedList<PartyTemplate> newPartyList = new LinkedList<PartyTemplate>();

    	// For each party requested, if we find it, we move it into our new order.
    	short partyNum = 1;
    	for( EsfName partyName : partyOrderNames ) {
        	ListIterator<PartyTemplate> origPartyListIter = origPartyList.listIterator();
        	while( origPartyListIter.hasNext() ) {
        		PartyTemplate p = origPartyListIter.next();
        		if ( p.getEsfName().equals(partyName) ) {
            		p.setProcessOrder(partyNum++);
            		newPartyList.add(p);
        			origPartyListIter.remove();
        			break;
        		}
        	}
    	}
    	
    	// If we have any parties remaining (were not in the partyOrderNames list), let's move them in too
    	ListIterator<PartyTemplate> origPartyListIter = origPartyList.listIterator();
    	while( origPartyListIter.hasNext() ) {
    		PartyTemplate p = origPartyListIter.next();
    		p.setProcessOrder(partyNum++);
    		newPartyList.add(p);
    	}
    	
    	partyTemplateList = newPartyList;
     	objectChanged();
	}
	
	public List<PartyTemplate> getPartyTemplateList()
	{
		return new LinkedList<PartyTemplate>(partyTemplateList);
	}
	public List<PartyTemplate> _getPartyTemplateListInternal()
	{
		return partyTemplateList;
	}
	public int getNumPartyTemplates()
	{
		return partyTemplateList.size();
	}
	public List<PartyTemplate> getDuplicatePartyTemplateList()
	{
		LinkedList<PartyTemplate> list = new LinkedList<PartyTemplate>();
		for( PartyTemplate p : partyTemplateList )
		{
			list.add(p.duplicate());
		}
		return list;
	}

	public PartyTemplate getFirstPartyTemplate()
	{
		return ( partyTemplateList.size() == 0 ) ? null : partyTemplateList.get(0);
	}
	public PartyTemplate getPartyTemplate(EsfUUID partyTemplateId)
	{
		for( PartyTemplate party : partyTemplateList )
		{
			if ( partyTemplateId.equals(party.getId()) )
				return party;
		}
		return null;
	}
	public PartyTemplate getPartyTemplate(EsfName partyName)
	{
		for( PartyTemplate party : partyTemplateList )
		{
			if ( partyName.equals(party.getEsfName()) )
				return party;
		}
		return null;
	}
	public void addPartyTemplate(PartyTemplate partyTemplate)
	{
		List<PartyTemplate> newList = getPartyTemplateList();
		newList.add(partyTemplate);
		partyTemplate.setProcessOrder((short)newList.size());
		partyTemplateList = newList;
		objectChanged();
	}
	public void removePartyTemplate(EsfName partyName)
	{
		List<PartyTemplate> newList = getPartyTemplateList();
	   	ListIterator<PartyTemplate> newListIter = newList.listIterator();
    	while( newListIter.hasNext() ) {
    		PartyTemplate p = newListIter.next();
    		if ( partyName.equals(p.getEsfName()) )
    		{
    			addToPartyTemplatesToRemoveOnSave(p);
    			newListIter.remove();
    			break;
    		}
    	}
		partyTemplateList = newList;
		objectChanged();
	}
	public void removePartyTemplate(PartyTemplate delParty)
	{
		List<PartyTemplate> newList = getPartyTemplateList();
	   	ListIterator<PartyTemplate> newListIter = newList.listIterator();
    	while( newListIter.hasNext() ) {
    		PartyTemplate p = newListIter.next();
    		if ( delParty.equals(p) )
    		{
    			addToPartyTemplatesToRemoveOnSave(p);
    			newListIter.remove();
    			break;
    		}
    	}
		partyTemplateList = newList;
		objectChanged();
	}
	
	synchronized void addToPartyTemplatesToRemoveOnSave(PartyTemplate delParty)
	{
		if ( partyTemplatesToRemoveOnSave == null )
			partyTemplatesToRemoveOnSave = new LinkedList<PartyTemplate>();
		partyTemplatesToRemoveOnSave.add(delParty);
	}
	
	public List<DocumentFile> getDocumentFileList()
	{
		return new LinkedList<DocumentFile>(documentFileList);
	}
	public List<DocumentFile> _getDocumentFileListInternal()
	{
		return documentFileList;
	}
	public List<DocumentFile> getDuplicateDocumentFileList()
	{
		LinkedList<DocumentFile> list = new LinkedList<DocumentFile>();
    	for( DocumentFile df : documentFileList ) 
    	{
    		DocumentFile dfDup = new DocumentFile(df.file.duplicate(),df.fileVersion.duplicate());
    		list.add(dfDup);
    	}
    	return list;
	}
	
	public DocumentFile getDocumentFile(EsfName fileName)
	{
		for( DocumentFile df : documentFileList )
		{
			if ( df.file.getEsfName().equals(fileName) )
				return df;
		}
		return null;
	}
	
	public boolean hasDocumentFile(EsfName imageName)
	{
	   	ListIterator<DocumentFile> newListIter = documentFileList.listIterator();
    	while( newListIter.hasNext() ) {
    		DocumentFile existing = newListIter.next();
    		if ( existing.file.getEsfName().equals(imageName) )
    			return true;
    	}
		return false;
	}
	public boolean hasDocumentFile(DocumentFile df)
	{
		return hasDocumentFile(df.file.getEsfName());
	}
	
	public boolean addDocumentFile(DocumentFile df)
	{
		if ( hasDocumentFile(df) )
			return false;
		
		List<DocumentFile> newList = getDocumentFileList();
		newList.add(df);
		documentFileList = newList;
		objectChanged();
		return true;
	}
	public void removeDocumentFile(EsfName fileName)
	{
		List<DocumentFile> newList = getDocumentFileList();
	   	ListIterator<DocumentFile> newListIter = newList.listIterator();
    	while( newListIter.hasNext() ) {
    		DocumentFile df = newListIter.next();
    		if ( fileName.equals(df.file.getEsfName()) )
    		{
    			addToDocumentFilesToRemoveOnSave(df);
    			newListIter.remove();
    			break;
    		}
    	}
    	documentFileList = newList;
		objectChanged();
	}
	public void removeDocumentFile(DocumentFile delFile)
	{
		List<DocumentFile> newList = getDocumentFileList();
	   	ListIterator<DocumentFile> newListIter = newList.listIterator();
    	while( newListIter.hasNext() ) {
    		DocumentFile df = newListIter.next();
    		if ( delFile.equals(df) )
    		{
    			addToDocumentFilesToRemoveOnSave(df);
    			newListIter.remove();
    			break;
    		}
    	}
    	documentFileList = newList;
		objectChanged();
	}
	
	synchronized void addToDocumentFilesToRemoveOnSave(DocumentFile delFile)
	{
		if ( documentFilesToRemoveOnSave == null )
			documentFilesToRemoveOnSave = new LinkedList<DocumentFile>();
		documentFilesToRemoveOnSave.add(delFile);
	}
	
	public List<DocumentImage> getDocumentImageList()
	{
		return new LinkedList<DocumentImage>(documentImageList);
	}
	public List<DocumentImage> _getDocumentImageListInternal()
	{
		return documentImageList;
	}
	public List<DocumentImage> getDuplicateDocumentImageList()
	{
		LinkedList<DocumentImage> list = new LinkedList<DocumentImage>();
    	for( DocumentImage di : documentImageList ) 
    	{
    		DocumentImage diDup = new DocumentImage(di.image.duplicate(),di.imageVersion.duplicate());
    		list.add(diDup);
    	}
    	return list;
	}
	
	public DocumentImage getDocumentImage(EsfName imageName)
	{
		for( DocumentImage di : documentImageList )
		{
			if ( di.image.getEsfName().equals(imageName) )
				return di;
		}
		return null;
	}
	
	public boolean hasDocumentImage(EsfName imageName)
	{
	   	ListIterator<DocumentImage> newListIter = documentImageList.listIterator();
    	while( newListIter.hasNext() ) {
    		DocumentImage existing = newListIter.next();
    		if ( existing.image.getEsfName().equals(imageName) )
    			return true;
    	}
		return false;
	}
	public boolean hasDocumentImage(DocumentImage di)
	{
		return hasDocumentImage(di.image.getEsfName());
	}
	
	public boolean addDocumentImage(DocumentImage di)
	{
		if ( hasDocumentImage(di) )
			return false;
		
		List<DocumentImage> newList = getDocumentImageList();
		newList.add(di);
		documentImageList = newList;
		objectChanged();
		return true;
	}
	public void removeDocumentImage(EsfName imageName)
	{
		List<DocumentImage> newList = getDocumentImageList();
	   	ListIterator<DocumentImage> newListIter = newList.listIterator();
    	while( newListIter.hasNext() ) {
    		DocumentImage di = newListIter.next();
    		if ( imageName.equals(di.image.getEsfName()) )
    		{
    			addToDocumentImagesToRemoveOnSave(di);
    			newListIter.remove();
    			break;
    		}
    	}
    	documentImageList = newList;
		objectChanged();
	}
	public void removeDocumentImage(DocumentImage delImage)
	{
		List<DocumentImage> newList = getDocumentImageList();
	   	ListIterator<DocumentImage> newListIter = newList.listIterator();
    	while( newListIter.hasNext() ) {
    		DocumentImage di = newListIter.next();
    		if ( delImage.equals(di) )
    		{
    			addToDocumentImagesToRemoveOnSave(di);
    			newListIter.remove();
    			break;
    		}
    	}
		documentImageList = newList;
		objectChanged();
	}
	
	synchronized void addToDocumentImagesToRemoveOnSave(DocumentImage delImage)
	{
		if ( documentImagesToRemoveOnSave == null )
			documentImagesToRemoveOnSave = new LinkedList<DocumentImage>();
		documentImagesToRemoveOnSave.add(delImage);
	}
	
	public List<DocumentDropdown> getDocumentDropdownList()
	{
		return new LinkedList<DocumentDropdown>(documentDropdownList);
	}
	public List<DocumentDropdown> _getDocumentDropdownListInternal()
	{
		return documentDropdownList;
	}
	public List<DocumentDropdown> getDuplicateDocumentDropdownList()
	{
		LinkedList<DocumentDropdown> list = new LinkedList<DocumentDropdown>();
    	for( DocumentDropdown dd : documentDropdownList ) 
    	{
    		DocumentDropdown ddDup = new DocumentDropdown(dd.dropdown.duplicate(),dd.dropdownVersion.duplicate());
    		list.add(ddDup);
    	}
    	return list;
	}
	
	public DocumentDropdown getDocumentDropdown(EsfName dropdownName)
	{
		for( DocumentDropdown dd : documentDropdownList )
		{
			if ( dd.dropdown.getEsfName().equals(dropdownName) )
				return dd;
		}
		return null;
	}
	
	public boolean hasDocumentDropdown(EsfName dropdownName)
	{
	   	ListIterator<DocumentDropdown> newListIter = documentDropdownList.listIterator();
    	while( newListIter.hasNext() ) {
    		DocumentDropdown existing = newListIter.next();
    		if ( existing.dropdown.getEsfName().equals(dropdownName) )
    			return true;
    	}
		return false;
	}
	public boolean hasDocumentDropdown(DocumentDropdown dd)
	{
		return hasDocumentDropdown(dd.dropdown.getEsfName());
	}
	
	public boolean addDocumentDropdown(DocumentDropdown dd)
	{
		if ( hasDocumentDropdown(dd) )
			return false;
		
		List<DocumentDropdown> newList = getDocumentDropdownList();
		newList.add(dd);
		documentDropdownList = newList;
		objectChanged();
		return true;
	}
	public void removeDocumentDropdown(EsfName dropdownName)
	{
		List<DocumentDropdown> newList = getDocumentDropdownList();
	   	ListIterator<DocumentDropdown> newListIter = newList.listIterator();
    	while( newListIter.hasNext() ) {
    		DocumentDropdown dd = newListIter.next();
    		if ( dropdownName.equals(dd.dropdown.getEsfName()) )
    		{
    			addToDocumentDropdownsToRemoveOnSave(dd);
    			newListIter.remove();
    			break;
    		}
    	}
    	documentDropdownList = newList;
		objectChanged();
	}
	public void removeDocumentDropdown(DocumentDropdown delDropdown)
	{
		List<DocumentDropdown> newList = getDocumentDropdownList();
	   	ListIterator<DocumentDropdown> newListIter = newList.listIterator();
    	while( newListIter.hasNext() ) {
    		DocumentDropdown dd = newListIter.next();
    		if ( delDropdown.equals(dd) )
    		{
    			addToDocumentDropdownsToRemoveOnSave(dd);
    			newListIter.remove();
    			break;
    		}
    	}
		documentDropdownList = newList;
		objectChanged();
	}
	
	synchronized void addToDocumentDropdownsToRemoveOnSave(DocumentDropdown delDropdown)
	{
		if ( documentDropdownsToRemoveOnSave == null )
			documentDropdownsToRemoveOnSave = new LinkedList<DocumentDropdown>();
		documentDropdownsToRemoveOnSave.add(delDropdown);
	}
	
	
    public synchronized DocumentProgramming getDocumentProgramming()
    {
        if ( documentProgramming == null )
        {
        	if ( programmingBlobId == null )
        	{
    			documentProgramming = new DocumentProgramming();
    			programmingBlobId = documentProgramming.getId();
    			//objectChanged(); -- don't mark it as changed since our hasChanged() method will check if this subobject has actually changed or not
        	}
        	else
        	{
            	documentProgramming = DocumentProgramming.Manager.getById(programmingBlobId);
        	}
        }
        return documentProgramming;
    }
    public boolean hasDocumentProgramming()
    {
    	return programmingBlobId != null;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof DocumentVersion )
        {
        	DocumentVersion otherDocument = (DocumentVersion)o;
            return getId().equals(otherDocument.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(DocumentVersion o)
    {
    	if ( getDocumentId().equals(o.getDocumentId()) )
    		return getVersion() - o.getVersion();
    	return getDocumentId().compareTo(o.getDocumentId());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<DocumentVersion xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <documentId>").append(documentId.toXml()).append("</documentId>\n");
        buf.append(" <version>").append(version).append("</version>\n");
        buf.append(" <createdTimestamp>").append(createdTimestamp.toXml()).append("</createdTimestamp>\n");
        buf.append(" <createdByUserId>").append(createdByUserId.toXml()).append("</createdByUserId>\n");
        buf.append(" <lastUpdatedTimestamp>").append(lastUpdatedTimestamp.toXml()).append("</lastUpdatedTimestamp>\n");
        buf.append(" <lastUpdatedByUserId>").append(lastUpdatedByUserId.toXml()).append("</lastUpdatedByUserId>\n");
        buf.append(" <documentStyleEsfName>").append(getDocumentStyle().getEsfName().toXml()).append("</documentStyleEsfName>\n"); // include this to aid import finding matching doc style
        buf.append(" <documentStyleId>").append(documentStyleId.toXml()).append("</documentStyleId>\n");
        buf.append(" <pageOrientation>").append(pageOrientation).append("</pageOrientation>\n");
        buf.append(" <esignProcessRecordLocation>").append(esignProcessRecordLocation).append("</esignProcessRecordLocation>\n");

        for( DocumentVersionPage dvPage : pageList )
        	dvPage.appendXml(buf);
        
        for( EsfName fieldName : fieldTemplateMap.keySet() )
        {
        	FieldTemplate ft = fieldTemplateMap.get(fieldName);
        	ft.appendXml(buf);
        }

        for( PartyTemplate pt : partyTemplateList )
        	pt.appendXml(buf);

        for( DocumentFile df : documentFileList )
        {
        	df.file.appendXml(buf);
        	df.fileVersion.appendXml(buf);
        }

        for( DocumentImage di : documentImageList )
        {
        	di.image.appendXml(buf);
        	di.imageVersion.appendXml(buf);
        }
        
        for( DocumentDropdown dd : documentDropdownList )
        {
        	dd.dropdown.appendXml(buf);
        	dd.dropdownVersion.appendXml(buf);
        }
        
        if ( hasDocumentProgramming() )
        {
        	DocumentProgramming documentProgramming = getDocumentProgramming();
        	documentProgramming.appendXml(buf);
        }

        buf.append("</DocumentVersion>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 412 + id.getEstimatedLengthXml() + documentId.getEstimatedLengthXml() + createdTimestamp.getEstimatedLengthXml() + createdByUserId.getEstimatedLengthXml() + 
        	lastUpdatedTimestamp.getEstimatedLengthXml() + lastUpdatedByUserId.getEstimatedLengthXml() + documentStyleId.getEstimatedLengthXml() + 
        	pageOrientation.length() + esignProcessRecordLocation.length();

        for( DocumentVersionPage dvPage : pageList )
        	len += dvPage.getEstimatedLengthXml();
        
        for( EsfName fieldName : fieldTemplateMap.keySet() )
        {
        	FieldTemplate ft = fieldTemplateMap.get(fieldName);
        	len += ft.getEstimatedLengthXml();
        }

        for( PartyTemplate pt : partyTemplateList )
        	len += pt.getEstimatedLengthXml();

        for( DocumentFile df : documentFileList )
        	len += df.file.getEstimatedLengthXml() + df.fileVersion.getEstimatedLengthXml();

        for( DocumentImage di : documentImageList )
        	len += di.image.getEstimatedLengthXml() + di.imageVersion.getEstimatedLengthXml();

        for( DocumentDropdown dd : documentDropdownList )
        	len += dd.dropdown.getEstimatedLengthXml() + dd.dropdownVersion.getEstimatedLengthXml();

        if ( hasDocumentProgramming() )
        {
        	DocumentProgramming documentProgramming = getDocumentProgramming();
        	len += documentProgramming.getEstimatedLengthXml();
        }

        return len; 
    }

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
    // Called to just rewrite the pages without making object update/saves
    public boolean regenerateCodeWithoutVersionSave()
    {
    	boolean hasChangedBeforeGenerateCode = hasChanged();
    	boolean ret = generateCode();
    	if ( ! hasChangedBeforeGenerateCode && hasChanged() )
    	{
    		setLoadedFromDb();
    		
    		for( DocumentVersionPage docVerPage : pageList )
    		{
    			docVerPage.save();
    		}
    		
    		for( FieldTemplate fieldTemplate : fieldTemplateMap.values() )
    		{
    			fieldTemplate.save();
    		}
    		
    		for( PartyTemplate partyTemplate : partyTemplateList )
    		{
    			partyTemplate.save();
    		}

    		for( DocumentFile df : documentFileList )
    		{
    			df.file.save();
    			df.fileVersion.save();
    		}

    		for( DocumentImage di : documentImageList )
    		{
    			di.image.save();
    			di.imageVersion.save();
    		}

    		for( DocumentDropdown dd : documentDropdownList )
    		{
    			dd.dropdown.save();
    			dd.dropdownVersion.save();
    		}
    	}
    	return ret;
    }
    
    // Creates the JSP code for this document version.  It puts all of it in the configured JSP document directory.
    // For multi-page documents, it also creates a JSP containing all review pages.
    // The first subdirectory here is the document id.
    // The subdirectory under the document id is the document version id.
    // The JSP files for each page are then written using their id filename.
    public boolean generateCode()
    {
    	// If we never even loaded our pages, there's nothing for us to do here...
    	if ( pageList == null )
    		return true;
    	
    	try
    	{
    		String osName = System.getProperty("os.name");
        	// First, if there is no document folder, create it.
        	java.io.File docDir = new java.io.File(Application.getInstance().getLibraryGeneratedJSPDocumentDirectory(),getDocumentId().toPlainString());
        	if ( ! docDir.exists() )
        	{
        		if ( ! docDir.mkdir()           || 
        			 ! docDir.setReadable(true) ||
        			 ! docDir.setExecutable(true) ||
        			 (! osName.startsWith("Windows") && ! docDir.setWritable(true) ) // Windows doesn't let us make directories writable, yet can still delete files created inside so it's okay
        		   )
        		{
        			_logger.error("generateCode() couldn't create read/write/execute document directory: " + docDir.getAbsolutePath());
        			return false;
        		}
        	}
        	//DEBUG_showFileInfo(docDir.getAbsolutePath());
        	
        	// If the document version folder exists, let's remove it and its contents and recreate it empty.
        	java.io.File docVerDir = new java.io.File(docDir,getId().toPlainString());
        	if ( docVerDir.exists() )
        	{
        		if ( ! deleteAllFilesInDirectory(docVerDir) )
        			return false;
        	}
        	else
        	{
        		if ( ! docVerDir.mkdir()           || 
        			 ! docVerDir.setReadable(true) ||
        			 ! docVerDir.setExecutable(true) ||
        			 (! osName.startsWith("Windows") && ! docDir.setWritable(true) ) // Windows doesn't let us make directories writable, yet can still delete files created inside so it's okay
        		   )
        		{
        			_logger.error("generateCode() could not create read/write/execute document version directory: " + docVerDir.getAbsolutePath());
        			return false;
        		}
        	}
        	//DEBUG_showFileInfo(docVerDir.getAbsolutePath());
        	
        	setupLibrarySearchFieldTemplateCache(); // save()->generatePage()->foreachpage.getGeneratedJSP()->registerField() and registerField uses this library search cache...

        	// Generate the special multi-page review page first if needed since we'll reset our field usage
        	// counts after (we don't want to double count fields on reviewable pages would also appear in this multi-page review page).
        	if ( isMultiPage() )
        	{
        		java.io.File reviewFile = new java.io.File(docVerDir,getId()+".jsp"); // for review JSP, we'll just use the docversion id.
        		if ( ! reviewFile.createNewFile()   ||
        			 ! reviewFile.setReadable(true) ||
        			 ! reviewFile.setWritable(true) 
        		   )
        		{
        			_logger.error("generateCode() could not create read/write multi-page document version review JSP file: " + reviewFile.getAbsolutePath());
        			return false;
        		}
        		generateMultiPageReviewPage(reviewFile, getReviewPages());
        	}

        	resetAllContainerReferenceCounts();
        	for( DocumentVersionPage page : pageList )
        	{
        		java.io.File pageFile = new java.io.File(docVerDir,page.getId()+".jsp");
        		if ( ! pageFile.createNewFile()   ||
        			 ! pageFile.setReadable(true) ||
        			 ! pageFile.setWritable(true) 
        		   )
        		{
        			_logger.error("generateCode() could not create read/write document version page JSP file: " + pageFile.getAbsolutePath());
        			return false;
        		}
            	//DEBUG_showFileInfo(pageFile.getAbsolutePath());
        		generatePage(pageFile,page);
        	}
    		
        	// Let's count all fields referenced as overlay fields of our images, too
        	for( DocumentImage di : getDocumentImageList() ) {
        		if ( di.imageVersion.hasOverlayFields() ) {
        			for( ImageVersionOverlayField ivof : di.imageVersion.getOverlayFields() ) {
        				FieldTemplate ft = getFieldTemplate(ivof.getFieldTemplateId());
        				if ( ft != null )
        				{
        					ft.bumpContainerReferenceCount();
        					if ( ft.isTypeRadioButton() ) // if it's a radio button, bump the count on the group
        					{
        						FieldTemplate radioButtonGroupTemplate = fieldTemplateMap.get(new EsfName(ft.getOutputFormatSpec()));
        						if ( radioButtonGroupTemplate != null && radioButtonGroupTemplate.isTypeRadioButtonGroup() )
        							radioButtonGroupTemplate.bumpContainerReferenceCount();
        					}
        				}
        			}
        		}
        	}
        	
        	return true;
    	}
    	catch( java.io.IOException e )
    	{
    		_logger.error("generateCode() failed for document id: " + getDocumentId() + "; version id: " + getId(), e);
    		return false;
    	}
    	finally
    	{
        	clearLibrarySearchFieldTemplateCache();
    	}
    }
    
    public static void DEBUG_showFileInfo(String pathToCheck) {
    	try {
            java.nio.file.Path path = java.nio.file.Paths.get(pathToCheck);
            System.out.println("---------------------------------------------------");
            System.out.println("Path: " + path);
            java.nio.file.attribute.AclFileAttributeView view = java.nio.file.Files.getFileAttributeView(path, java.nio.file.attribute.AclFileAttributeView.class);
            List<java.nio.file.attribute.AclEntry> aclEntryList = view.getAcl();
            for( java.nio.file.attribute.AclEntry aclEntry : aclEntryList ) {
            	System.out.println("User principal name: " + aclEntry.principal().getName());
            	System.out.println("ACL entry type: " + aclEntry.type());
                System.out.println("ACL Entry Flags:");
            	for( java.nio.file.attribute.AclEntryFlag flag : aclEntry.flags() ) {
                        System.out.print(flag.name() + " ");
            	}
            	System.out.println();
            	
            	System.out.println("Permissions:");
                for( java.nio.file.attribute.AclEntryPermission permission : aclEntry.permissions() ) {
                    System.out.print(permission.name() + " ");
                }
                System.out.println();
            }
    	} catch(Exception e) {
    		System.out.println("Exception showing info for path: " + pathToCheck + "; e: " + e.getMessage());
    	}
     }
    
    private void generatePage(java.io.File pageFile, DocumentVersionPage page)
    	throws java.io.IOException
    {
    		PrintWriter pw = null;
        	try
        	{
        		pw = new PrintWriter( new BufferedWriter( new OutputStreamWriter( new FileOutputStream(pageFile), EsfString.CHARSET_UTF_8 ) ) );
            	
            	// Output DOCTYPE
        		pw.println(Version.getDocTypeXhtml10Transitional());
            	
            	// Output JSP initialization code
        		pw.println("<%@ page pageEncoding=\"UTF-8\" contentType=\"text/html; charset=UTF-8\" session=\"true\" isELIgnored=\"true\" %>");
        		pw.println("<%@ taglib uri=\"http://open.esignforms.com/libdocsgen/taglib\" prefix=\"esf\" %>");
            	
        		pw.print("<jsp:useBean id=\"");
        		pw.print(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId);
        		pw.println("\" scope=\"request\" class=\"com.esignforms.open.jsp.libdocsgen.DocumentPageBean\" />");
            	
        		pw.print("<% com.esignforms.open.jsp.libdocsgen.EsfDocumentPageInterface "); 
        		pw.print(com.esignforms.open.jsp.libdocsgen.taglib.Html.EsfDocumentPageInterfaceVariableName);
        		pw.print(" = ");
        		pw.print(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId);
        		pw.println("; %>");
            	
            	// Output page itself
        		pw.println("<esf:capture><esf:html>");
        		pw.println("<esf:errorsout />");
        		pw.println("<esf:form>");
        		pw.println("<div id=\"" + page.getId().toNormalizedEsfNameString() + "\" class=\"" + 
	        			page.getEsfName() + "\">");
        		pw.println("<% docPage.context.documentPageNumber = " + page.getPageNumber() + "; %>");
        		pw.println(page.getGeneratedJSP(this));
        		pw.println("</div>");
        		pw.println("<esf:esignrecord />");
        		pw.println("<esf:buttons />");
        		pw.println("</esf:form>");
        		pw.println("</esf:html></esf:capture>");
            	
            	// Output JSP cleanup code
        		pw.print("<% ");
        		pw.print(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId);
        		pw.print(".endSessionIfRequested(); %>");
        		pw.print("<!-- JSP generated on ");
        		pw.print( (new EsfDateTime()).toDateTimeMsecString() );
        		pw.println(" -->");
        	}
        	catch( java.io.IOException e )
        	{
        		throw e;
        	}
        	catch( Exception e )
        	{
        		_logger.error("generatePage() - Unexpected non-IOException",e);
        		throw new java.io.IOException("generatePage() - Unexpected non-IOException: " + e.getMessage());
        	}
        	finally
        	{
        		if ( pw != null ) pw.close();
        	}
    }
    
    private void generateMultiPageReviewPage(java.io.File pageFile, List<DocumentVersionPage> reviewPages)
		throws java.io.IOException
	{
    		PrintWriter pw = null;
	    	try
	    	{
        		pw = new PrintWriter( new BufferedWriter( new OutputStreamWriter( new FileOutputStream(pageFile), EsfString.CHARSET_UTF_8 ) ) );
	        	
	        	// Output DOCTYPE
        		pw.println(Version.getDocTypeXhtml10Transitional());
	        	
	        	// Output JSP initialization code
        		pw.println("<%@ page pageEncoding=\"UTF-8\" contentType=\"text/html; charset=UTF-8\" session=\"true\" isELIgnored=\"true\" %>");
        		pw.println("<%@ taglib uri=\"http://open.esignforms.com/libdocsgen/taglib\" prefix=\"esf\" %>");
        		pw.print("<jsp:useBean id=\"");
        		pw.print(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId);
        		pw.println("\" scope=\"request\" class=\"com.esignforms.open.jsp.libdocsgen.DocumentPageBean\" />");
	        	//pw.println("<% if ( ! docPage.processRequest() ) return; %>"); // Our docPage is already initialized by the PickupTransaction servlet that includes us.
	        	
        		pw.print("<% com.esignforms.open.jsp.libdocsgen.EsfDocumentPageInterface "); 
        		pw.print(com.esignforms.open.jsp.libdocsgen.taglib.Html.EsfDocumentPageInterfaceVariableName);
        		pw.print(" = ");
        		pw.print(com.esignforms.open.jsp.libdocsgen.taglib.Html.DocumentPageBeanAttributeId);
        		pw.println("; %>");

                // Output page itself
        		pw.println("<esf:capture><esf:html>");
        		pw.println("<esf:errorsout />");
        		pw.println("<esf:form>");
	        	boolean needsPageBreak = false;
	        	for( DocumentVersionPage page : reviewPages )
	        	{
	        		if ( needsPageBreak )
	        			pw.println("<div class=\"newPage\">&nbsp;</div>");
	        		pw.println("<div id=\"" + page.getId().toNormalizedEsfNameString() + "\" class=\"" + 
	        			page.getEsfName() + "\">");
	        		pw.println("<% docPage.context.documentPageNumber = " + page.getPageNumber() + "; %>");
	        		pw.println(page.getGeneratedJSP(this));
	        		pw.println("</div>");
	        		needsPageBreak = true;
	        	}
	        	pw.println("<esf:esignrecord />");
	        	pw.println("<esf:buttons />");
	        	pw.println("</esf:form>");
	        	pw.println("</esf:html></esf:capture>");
	        	
	        	// Output JSP cleanup code
	        	pw.print("<% docPage.endSessionIfRequested(); %>");
	        	pw.print("<!-- JSP generated on ");
	        	pw.print( (new EsfDateTime()).toDateTimeMsecString() );
	        	pw.println(" -->");
	    	}
	    	catch( java.io.IOException e )
	    	{
	    		throw e;
	    	}
	    	catch( Exception e )
	    	{
	    		_logger.error("generatePage() - Unexpected non-IOException",e);
	    		throw new java.io.IOException("generatePage() - Unexpected non-IOException: " + e.getMessage());
	    	}
	    	finally
	    	{
	    		if ( pw != null ) pw.close();
	    	}
	}

    private boolean deleteAllFilesInDirectory(java.io.File dir)
    {
    	if ( dir == null || ! dir.isDirectory() )
    		return false;
    	
    	for ( java.io.File file : dir.listFiles() )
    	{
    		if ( file.isDirectory() )
    		{
    			if ( ! deleteAllFilesInDirectory(file) )
    				return false;
    		}
    		if ( ! file.delete() )
    		{
    			_logger.error("deleteAllFilesInDirectory() could not delete: " + file.getAbsolutePath());
    			return false;
    		}
    	}
    	return true;
    }
    
    private void saveFieldTemplateMap(final Connection con)
    	throws SQLException
    {
    	if ( fieldTemplatesToRemoveOnSave != null )
    	{
    		for( FieldTemplate ft : fieldTemplatesToRemoveOnSave )
    		{
    			ft.delete(con, null, null);
    		}
    		fieldTemplatesToRemoveOnSave.clear();
    		fieldTemplatesToRemoveOnSave = null;
    	}

    	// We'll do all updates before inserts
    	LinkedList<FieldTemplate> newTemplateList = null;
    	LinkedList<EsfName> killTemplateNameList = null; // for fields that fail to save
    	
    	for( EsfName name : fieldTemplateMap.keySet() ) 
    	{
    		FieldTemplate template = fieldTemplateMap.get(name);
    		if ( template.doInsert() ) 
    		{
    			if ( newTemplateList == null )
    				newTemplateList = new LinkedList<FieldTemplate>();
    			newTemplateList.add(template);
    		}
    		else
    		{
    			if ( ! template.save(con) )
    			{
        			if ( killTemplateNameList == null )
        				killTemplateNameList = new LinkedList<EsfName>();
        			killTemplateNameList.add(name);
    			}
    		}
   	    }
    	
    	if ( killTemplateNameList != null )
    	{
    		for ( EsfName name : killTemplateNameList )
    		{
    			fieldTemplateMap.remove(name);
    		}
    		killTemplateNameList.clear();
    		killTemplateNameList = null;
    	}
    	
    	// Now do the inserts...
    	if ( newTemplateList != null )
    	{
    		for( FieldTemplate template : newTemplateList )
    			template.save(con);
    	}
    }
    
    private void savePartyTemplateList(final Connection con)
		throws SQLException
	{
    	if ( partyTemplatesToRemoveOnSave != null )
    	{
    		for( PartyTemplate pt : partyTemplatesToRemoveOnSave )
    			pt.delete(con, null, null);
    		partyTemplatesToRemoveOnSave = null;
    	}

    	short processOrder = 1;
    	for( PartyTemplate template : partyTemplateList ) 
		{
    		template.setProcessOrder(processOrder++);
			template.save(con);
		}
 	}

    private void saveDocumentFileList(final Connection con, final User user)
		throws SQLException
	{
		for( DocumentFile df : documentFileList ) 
		{
			df.file.save(con,user);
			df.fileVersion.save(con,user);
		}
	}

    private void saveDocumentImageList(final Connection con, final User user)
		throws SQLException
	{
		for( DocumentImage di : documentImageList ) 
		{
			di.image.save(con,user);
			di.imageVersion.save(con,user);
		}
	}
    
    private void saveDocumentDropdownList(final Connection con, final User user)
		throws SQLException
	{
		for( DocumentDropdown dd : documentDropdownList ) 
		{
			dd.dropdown.save(con,user);
			dd.dropdownVersion.save(con,user);
		}
	}

    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; documentId: " + documentId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
        	// We only save if we have them loaded (if we have them, but not loaded, then no need save them)
    		if ( documentProgramming != null ) 
    			documentProgramming.save(con);
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_document_version (id,library_document_id,version,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,library_documentstyle_id,page_orientation,esign_process_record_location,programming_blob_id) VALUES (?,?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(documentId);
                stmt.set(version);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(documentStyleId);
                stmt.set(pageOrientation);
                stmt.set(esignProcessRecordLocation);
                stmt.set(programmingBlobId);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; documentId: " + documentId + "; version: " + version);
                    return false;
                }
                
                // Always generate code for new document version
            	generateCode();
                
                // Always save pages when it's a new document version
            	for( DocumentVersionPage docPage : pageList )
            		docPage.saveOnRewrite(con, user);
            	pageListChanged = false;
            	
            	saveFieldTemplateMap(con);
            	savePartyTemplateList(con);
            	saveDocumentFileList(con,user);
            	saveDocumentImageList(con,user);
            	saveDocumentDropdownList(con,user);
            	
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this);
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new document version id: " + id + "; documentId: " + documentId + "; version: " + version); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new document version id: " + id + "; documentId: " + documentId + "; version: " + version);
                }
                
                return true;
            }
            
            // We only generate code and save pages, field, parties, images if we're a Test version.
            if ( getDocument().getTestVersion() == version )
            {
            	// If any of the pages themselves have changed, we save them all again after we delete them
                boolean savePages = pageListChanged;
                if ( pageList != null )
                {
                	for( DocumentVersionPage docPage : pageList )
                	{
                		if ( docPage.hasChanged() )
                		{
                			objectChanged(); // want to update our base object too
                			savePages = true;
                			break;
                		}
                	}
                }
            	
                generateCode();
                
            	if ( savePages )
            	{
            		// Before we save our pages, which may be reordered, we delete whatever was there before and then save each page again
        			DocumentVersionPage.Manager.deleteAllPages(con, id);
                	for( DocumentVersionPage docPage : pageList )
                		docPage.saveOnRewrite(con, user);
                	pageListChanged = false;
            	}
            	
            	saveFieldTemplateMap(con);
            	savePartyTemplateList(con);
            	saveDocumentFileList(con,user);
            	saveDocumentImageList(con,user);
            	saveDocumentDropdownList(con,user);
            }
        	
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_document_version SET last_updated_timestamp=?,last_updated_by_user_id=?,library_documentstyle_id=?,page_orientation=?,esign_process_record_location=?,programming_blob_id=? WHERE id=?"
                						   		);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(documentStyleId);
                stmt.set(pageOrientation);
                stmt.set(esignProcessRecordLocation);
                stmt.set(programmingBlobId);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for document version id: " + id + "; documentId: " + documentId + "; version: " + version);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated document version id: " + id + "; documentId: " + documentId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated document version id: " + id + "; documentId: " + documentId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; documentId: " + documentId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; documentId: " + documentId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use in the transaction definition.
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of document version that was pending an INSERT id: " + id + "; documentId: " + documentId + "; version: " + version);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
        	// If we have a programming blob id and it's been saved, delete it
        	if ( programmingBlobId != null && (documentProgramming == null || documentProgramming.doUpdate()) )
        		getBlobDb().delete(con, programmingBlobId);
        	
            // Delete the document version and its pages and field templates
        	for( DocumentVersionPage page : pageList )
        		page.delete(con,errors,user);
            
        	for( EsfName fieldName : fieldTemplateMap.keySet() )
        	{
        		FieldTemplate field = fieldTemplateMap.get(fieldName);
        		field.delete(con,errors,user);
        	}
            
        	for( PartyTemplate party : partyTemplateList )
        	{
        		party.delete(con,errors,user);
        	}
            
    		for( DocumentFile df : documentFileList ) 
    		{
    			df.file.delete(con,errors,user);
    		}

    		for( DocumentImage di : documentImageList ) 
    		{
    			di.image.delete(con,errors,user);
    		}

    		for( DocumentDropdown dd : documentDropdownList ) 
    		{
    			dd.dropdown.delete(con,errors,user);
    		}

    		stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_document_version WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted document version id: " + id + "; documentId: " + documentId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted document version id: " + id + "; documentId: " + documentId + "; version: " + version);
            }
            
            // Delete the generated JSPs
        	java.io.File docDir = new java.io.File(Application.getInstance().getLibraryGeneratedJSPDocumentDirectory(),getDocumentId().toPlainString());
        	java.io.File docVerDir = new java.io.File(docDir,getId().toPlainString());
            deleteAllFilesInDirectory(docVerDir);

            return true;
        }
        catch(SQLException e)
        {
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"delete() on id: " + id + "; documentId: " + documentId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT COUNT(*) FROM esf_transaction_document WHERE library_document_version_id = ?"
	                                   );
	        stmt.set(id);
	        EsfResultSet rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	int count = rs.getInt();
	        	if ( count > 0 )
	        	{
		        	errors.addError("This document version is referenced by " + count + " transactions.");
		        	return false;
	        	}
	        }

	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   		private static UUIDCacheReadOptimized<DocumentVersion> cache = new UUIDCacheReadOptimized<DocumentVersion>();
   		
   		static final DocumentVersion getById(Connection con, EsfUUID docVersionId) throws SQLException
   		{
   			return getById(con,docVersionId,true);
   		}
   		static DocumentVersion getById(Connection con, EsfUUID docVersionId, boolean checkCache) throws SQLException
   		{
   			if ( checkCache )
   			{
   	   			DocumentVersion docVer = cache.getById(docVersionId);
   	   			if ( docVer != null )
   	   				return docVer;
   			}

   			EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT library_document_id,version,created_by_user_id,last_updated_by_user_id,created_timestamp,last_updated_timestamp,library_documentstyle_id,page_orientation,esign_process_record_location,programming_blob_id " +
   	        			"FROM esf_library_document_version WHERE id = ?"
   	        									);
   	        	stmt.set(docVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID libraryDocumentId = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID documentStyleId = rs.getEsfUUID();
		            String pageOrientation = rs.getString();
		            String esignProcessRecordLocation = rs.getString();
		            EsfUUID programmingBlobId = rs.getEsfUUID();
		            
		            List<DocumentVersionPage> pageList = DocumentVersionPage.Manager.getAllPagesByDocumentVersion(docVersionId);
		            Map<EsfName,FieldTemplate> fieldMap = FieldTemplate.Manager.getAll(docVersionId);
		            List<PartyTemplate> partyList = PartyTemplate.Manager.getAll(docVersionId);
		            
		            LinkedList<DocumentFile> documentFileList = new LinkedList<DocumentFile>();
		            Collection<File> filesList = File.Manager.getAll(docVersionId);
		            for( File file : filesList )
		            {
		            	DocumentFile df = new DocumentFile(file,file.getTestFileVersion());
		            	documentFileList.add(df);
		            }
		            
		            LinkedList<DocumentImage> documentImageList = new LinkedList<DocumentImage>();
		            Collection<Image> imageList = Image.Manager.getAll(docVersionId);
		            for( Image image : imageList )
		            {
		            	DocumentImage di = new DocumentImage(image,image.getTestImageVersion());
		            	documentImageList.add(di);
		            }
		            
		            LinkedList<DocumentDropdown> documentDropdownList = new LinkedList<DocumentDropdown>();
		            Collection<DropDown> dropdownList = DropDown.Manager.getAll(docVersionId);
		            for( DropDown dropdown : dropdownList )
		            {
		            	DocumentDropdown dd = new DocumentDropdown(dropdown,dropdown.getTestDropDownVersion());
		            	documentDropdownList.add(dd);
		            }
		            
		            DocumentVersion docVer = new DocumentVersion(docVersionId,libraryDocumentId,version,
		            											 createdTimestamp,createdByUserId,lastUpdatedTimestamp,lastUpdatedByUserId,
		            											 documentStyleId,pageOrientation,esignProcessRecordLocation,pageList,fieldMap,partyList,
		            											 documentFileList,documentImageList,documentDropdownList,programmingBlobId);
		            docVer.setLoadedFromDb();
		            cache.add(docVer);
		            
	   	            return docVer;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + docVersionId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + docVersionId + "; failed to find the document version");
   	        return null; 
  		}
   		
   		public static DocumentVersion getById(EsfUUID docVersionId)
   		{
   			DocumentVersion docVer = cache.getById(docVersionId);
   			if ( docVer != null )
   				return docVer;
   	   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	docVer = getById(con,docVersionId,false);
   	        	if ( docVer != null ) 
   	        	{
		            con.commit();
	   	        	return docVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		static DocumentVersion getByVersion(Connection con, EsfUUID libraryDocumentId, int version) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_document_version WHERE library_document_id=? AND version=?" );
   	        	stmt.set(libraryDocumentId);
   	        	stmt.set(version);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID docVersionId = rs.getEsfUUID();
	       			return getById(con,docVersionId);
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByVersion() - docId: " + libraryDocumentId + "; version: " + version);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getByVersion() - docId: " + libraryDocumentId + "; version: " + version + "; failed to find the document version");
   	        return null; 
  		}
   		
   		public static DocumentVersion getByVersion(EsfUUID libraryDocumentId, int version)
   		{
   			// While not always true, if the version is 1, it's possible this is a new object that's not been saved yet so we can't find the id in the DB, so we'll check our cache first just in case
   			if ( version == 1 )
   			{
   				for( EsfUUID id : cache.getAllIds() )
   				{
   					DocumentVersion docVer = cache.getById(id);
   					if ( docVer.getDocumentId().equals(libraryDocumentId) && docVer.getVersion() == version )
   						return docVer;
   				}
   			}
   			
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	DocumentVersion docVer = getByVersion(con,libraryDocumentId,version);
   	        	if ( docVer != null ) 
   	        	{
		            con.commit();
	   	        	return docVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		
   		public static List<DocumentVersion> getAllByDocumentId(Connection con, EsfUUID libraryDocumentId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<DocumentVersion> list = new LinkedList<DocumentVersion>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_document_version WHERE library_document_id=?" );
   	        	stmt.set(libraryDocumentId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID docVersionId = rs.getEsfUUID();
	            	
	       			DocumentVersion docVer = getById(con,docVersionId);
	       			if ( docVer != null )
	       				list.add(docVer);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   	    
   		public static List<DocumentVersion> getAllByDocumentId(EsfUUID libraryDocumentId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<DocumentVersion> list = getAllByDocumentId(con,libraryDocumentId);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		
   	    public static DocumentVersion createTest(Document document, User createdBy)
   	    {
   	    	if ( document == null || createdBy == null )
   	    		return null;
   	    	DocumentVersion newDocVersion = new DocumentVersion(document,createdBy);
   	        
   	    	// For new test docs, we'll auto-create an initial party
   	        PartyTemplate firstParty = PartyTemplate.Manager.createNew(newDocVersion.getId(), new EsfName(Application.getInstance().getServerMessages().getString("documentVersion.new.initialPartyEsfName")));
   	        newDocVersion.addPartyTemplate(firstParty);

   	    	cache.add(newDocVersion);
   	    	return newDocVersion;
   	    }

   	    public static DocumentVersion createNew(Document document, User createdBy)
   	    {
   	    	if ( document == null || createdBy == null )
   	    		return null;
   	    	DocumentVersion newDocVersion = new DocumentVersion(document,createdBy);
   	    	cache.add(newDocVersion);
   	    	return newDocVersion;
   	    }

   	    public static DocumentVersion createLike(Document document, DocumentVersion likeDocVersion, User createdBy)
   	    {
   	    	if ( document == null || likeDocVersion == null || createdBy == null )
   	    		return null;
   	    	
   	    	DocumentVersion newDocVersion = createNew(document,createdBy);
   	    	
   	    	TreeMap<EsfUUID,EsfUUID> documentIdMapping = new TreeMap<EsfUUID,EsfUUID>();
   	    	documentIdMapping.put(likeDocVersion.getDocumentId(), document.getId());
   	    	
   	    	newDocVersion.esignProcessRecordLocation = likeDocVersion.esignProcessRecordLocation;
   	    	newDocVersion.pageOrientation = likeDocVersion.pageOrientation;
   	    	newDocVersion.documentStyleId = likeDocVersion.documentStyleId;
   	    	
   	    	newDocVersion.pageList.clear();
   	    	TreeMap<EsfUUID,EsfUUID> documentVersionPageIdMapping = new TreeMap<EsfUUID,EsfUUID>();
   	    	for( DocumentVersionPage p : likeDocVersion.getPages() ) 
   	    	{
   	    		DocumentVersionPage newPage = DocumentVersionPage.Manager.createLike(newDocVersion, p);
   	    		newDocVersion.pageList.add( newPage );
   	    		documentVersionPageIdMapping.put(p.getId(), newPage.getId());
   	    	}
   	    	
   	    	Map<EsfName,FieldTemplate> likeDocFieldTemplateMap = likeDocVersion.getFieldTemplateMap();
   	    	for( EsfName name : likeDocFieldTemplateMap.keySet() ) 
   	    	{
   	    		newDocVersion.fieldTemplateMap.put( name, FieldTemplate.Manager.createLike(newDocVersion.getId(),likeDocFieldTemplateMap.get(name)) );
   	    	}
   	    	
   	    	TreeMap<EsfUUID,EsfUUID> partyIdMapping = new TreeMap<EsfUUID,EsfUUID>();
   	    	for( PartyTemplate party : likeDocVersion.partyTemplateList ) 
   	    	{
   	    		PartyTemplate newPartyTemplate = PartyTemplate.Manager.createLike(newDocVersion.getId(),party);
   	    		partyIdMapping.put(party.getId(), newPartyTemplate.getId()); // save the party mapping for later
   	    		
   	    		// Now, let's fix up the new party to fields mapping
   	    		for( PartyTemplateFieldTemplate ft : newPartyTemplate.getPartyTemplateFieldTemplates() )
   	    		{
   	    			EsfUUID origFieldTemplateId = ft.fieldTemplateId;
   	    	    	for( EsfName name : likeDocFieldTemplateMap.keySet() ) 
   	    	    	{
   	    	    		FieldTemplate origFieldTemplate = likeDocFieldTemplateMap.get(name);
   	    	    		if ( origFieldTemplate.getId().equals(origFieldTemplateId) )
   	    	    		{
   	    	    			FieldTemplate newFieldTemplate = newDocVersion.fieldTemplateMap.get(name);
   	    	    			ft.fieldTemplateId = newFieldTemplate == null ? null : newFieldTemplate.getId();
   	    	    			break;
   	    	    		}
   	    	    	}
  	    		}
   	    		
   	    		newDocVersion.partyTemplateList.add( newPartyTemplate );
   	    	}
   	    	
   	    	for ( DocumentFile df : likeDocVersion.documentFileList )
   	    	{
   	    		File newFile = File.Manager.createLike(newDocVersion.getId(), df.file, df.file.getEsfName());
   	    		FileVersion newFileVersion = FileVersion.Manager.createLike(newFile, df.fileVersion, createdBy);

   	    		DocumentFile newDf = new DocumentFile(newFile, newFileVersion);
   	    		newDocVersion.addDocumentFile(newDf);
   	    	}
   	    	
   	    	for ( DocumentImage di : likeDocVersion.documentImageList )
   	    	{
   	    		Image newImage = Image.Manager.createLike(newDocVersion.getId(), di.image, di.image.getEsfName());
   	    		ImageVersion newImageVersion = ImageVersion.Manager.createLike(newImage, di.imageVersion, createdBy);
   	    		
   	    		if ( newImageVersion.hasOverlayFields() )
   	    		{
   	   	    		// Fix up the field template ids for our overlay fields
   	   	    		for( ImageVersionOverlayField newOverlayField : newImageVersion.getOverlayFields() )
   	   	    		{
   	   	    			EsfUUID origFieldTemplateId = newOverlayField.fieldTemplateId;
   	   	    	    	for( EsfName name : likeDocFieldTemplateMap.keySet() ) 
   	   	    	    	{
   	   	    	    		FieldTemplate origFieldTemplate = likeDocFieldTemplateMap.get(name);
   	   	    	    		if ( origFieldTemplate.getId().equals(origFieldTemplateId) )
   	   	    	    		{
   	   	    	    			FieldTemplate newFieldTemplate = newDocVersion.fieldTemplateMap.get(name);
   	   	    	    			if ( newFieldTemplate != null )
   	   	    	    				newOverlayField.fieldTemplateId = newFieldTemplate.getId();
   	   	    	    			else
   	   	    	    			{
   	   	    	    				_logger.error("createLike() likeDocVersion id: " + likeDocVersion.getId() + "; had image overlay field template: " + name + 
   	   	    	    						"; not found in new document version id: " + newDocVersion.getId());
   	   	    	    				newImageVersion.overlayFieldList.remove(newOverlayField);
   	   	    	    			}
   	   	    	    			break;
   	   	    	    		}
   	   	    	    	}
   	  	    		}
   	    		}

   	    		DocumentImage newDi = new DocumentImage(newImage, newImageVersion);
   	    		newDocVersion.addDocumentImage(newDi);
   	    	}
   	    	
   	    	for ( DocumentDropdown dd : likeDocVersion.documentDropdownList )
   	    	{
   	    		DropDown newDropDown = DropDown.Manager.createLike(newDocVersion.getId(), dd.dropdown, dd.dropdown.getEsfName());
   	    		DropDownVersion newDropDownVersion = DropDownVersion.Manager.createLike(newDropDown, dd.dropdownVersion, createdBy);

   	    		DocumentDropdown newDd = new DocumentDropdown(newDropDown, newDropDownVersion);
   	    		newDocVersion.addDocumentDropdown(newDd);
   	    	}
   	    	
   	    	if ( likeDocVersion.hasDocumentProgramming() )
   	    	{
   	    		newDocVersion.documentProgramming = DocumentProgramming.Manager.createLike(likeDocVersion.getDocumentProgramming(),documentIdMapping,documentVersionPageIdMapping,partyIdMapping);
   	    		newDocVersion.programmingBlobId = newDocVersion.documentProgramming.getId();
   	    	}
  	    	
   	    	return newDocVersion;
   	    }

   	    public static DocumentVersion createFromJDOM(Document document, Element e)
   	    {
   	    	if ( document == null || e == null )
   	    		return null;
   	    	
   	    	EsfUUID id = new EsfUUID();
   	    	
   	    	LinkedList<DocumentVersionPage> pageList = new LinkedList<DocumentVersionPage>();
   	    	TreeMap<EsfName,FieldTemplate> fieldTemplateMap = new TreeMap<EsfName,FieldTemplate>();
   	    	LinkedList<PartyTemplate> partyTemplateList = new LinkedList<PartyTemplate>();
   	    	LinkedList<DocumentFile> documentFileList = new LinkedList<DocumentFile>();
   	    	LinkedList<DocumentImage> documentImageList = new LinkedList<DocumentImage>();
   	    	LinkedList<DocumentDropdown> documentDropdownList = new LinkedList<DocumentDropdown>();
   	    	
   	    	TreeMap<EsfUUID,EsfUUID> documentIdMapping = new TreeMap<EsfUUID,EsfUUID>();
   	    	EsfUUID origDocumentId = new EsfUUID(e.getChildText("documentId", e.getNamespace()));
   	    	documentIdMapping.put(origDocumentId, document.getId());
   	    	
   	    	// Let's see if we can figure out the document style by id, then by name in the doc's library, then the template library, else use the default doc style
   	    	EsfUUID docStyleId = new EsfUUID(e.getChildText("documentStyleId", e.getNamespace()));
   	    	DocumentStyle docStyle = DocumentStyle.Manager.getById(docStyleId);
   	    	if ( docStyle == null )
   	    	{
   	    		EsfName docStyleName = new EsfName(e.getChildText("documentStyleEsfName", e.getNamespace()));
   	    		docStyle = DocumentStyle.Manager.getByName(document.getLibrary(), docStyleName);
   	    		if ( docStyle == null )
   	    		{
   	   	    		docStyle = DocumentStyle.Manager.getByName(Library.Manager.getTemplate(), docStyleName);
   	   	    		if ( docStyle == null )
   	   	    			docStyle = DocumentStyle.Manager.getDefault();
   	    		}
   	    	}
   	    	
   	    	String pageOrientation = e.getChildText("pageOrientation", e.getNamespace());
   	    	if ( EsfString.isBlank(pageOrientation) )
   	    		pageOrientation = PAGE_ORIENTATION_PORTRAIT;

   	    	String esignProcessRecordLocation = e.getChildText("esignProcessRecordLocation", e.getNamespace());
   	    	if ( EsfString.isBlank(esignProcessRecordLocation) )
   	    		esignProcessRecordLocation = ESIGN_PROCESS_RECORD_LOCATION_FINAL_OWN_PAGE;

   	    	TreeMap<EsfUUID,EsfUUID> pageIdMapping = new TreeMap<EsfUUID,EsfUUID>();
   	    	
   	    	List<Element> pageElements = e.getChildren("DocumentVersionPage", e.getNamespace());
   	    	ListIterator<Element> pageIter = pageElements.listIterator();
			while( pageIter.hasNext() )
			{
				Element pageElement = pageIter.next();
				EsfUUID origPageId = new EsfUUID( pageElement.getChildText("id", e.getNamespace()));
				DocumentVersionPage p = DocumentVersionPage.Manager.createFromJDOM(id,pageElement);
				pageList.add(p);
				pageIdMapping.put(origPageId,p.getId());
			}  	    	
			
			TreeMap<EsfUUID,EsfUUID> fieldTemplateIdMapping = new TreeMap<EsfUUID,EsfUUID>();
			
   	    	List<Element> fieldElements = e.getChildren("FieldTemplate", e.getNamespace());
   	    	ListIterator<Element> fieldIter = fieldElements.listIterator();
			while( fieldIter.hasNext() ) 
			{
				Element fieldElement = fieldIter.next();
				EsfUUID origFieldTemplateId = new EsfUUID( fieldElement.getChildText("id", e.getNamespace()));
				FieldTemplate ft = FieldTemplate.Manager.createFromJDOM(id,fieldElement);
				fieldTemplateMap.put(ft.getEsfName(),ft);
				fieldTemplateIdMapping.put(origFieldTemplateId,ft.getId());
			}  	    	
			
   	    	TreeMap<EsfUUID,EsfUUID> partyIdMapping = new TreeMap<EsfUUID,EsfUUID>();

   	    	List<Element> partyElements = e.getChildren("PartyTemplate", e.getNamespace());
   	    	ListIterator<Element> partyIter = partyElements.listIterator();
			while( partyIter.hasNext() ) 
			{
				Element partyElement = partyIter.next();
				EsfUUID origPartyId = new EsfUUID( partyElement.getChildText("id", e.getNamespace()));
				PartyTemplate pt = PartyTemplate.Manager.createFromJDOM(id,fieldTemplateMap,fieldElements,partyElement);
				partyTemplateList.add(pt);
				partyIdMapping.put(origPartyId,pt.getId());
			}  	    	
			
   	    	List<Element> docFileElements = e.getChildren("File", e.getNamespace());
   	    	List<Element> docVersionFileElements = e.getChildren("FileVersion", e.getNamespace());
   	    	ListIterator<Element> docFileIter = docFileElements.listIterator();
   	    	ListIterator<Element> docVersionFileIter = docVersionFileElements.listIterator();
			while( docFileIter.hasNext() ) 
			{
				Element docFileElement = docFileIter.next();
				Element docVersionFileElement = docVersionFileIter.next();
				File file = File.Manager.createFromJDOM(id,docFileElement);
				FileVersion fileVersion = FileVersion.Manager.createFromJDOM(file,docVersionFileElement);
				DocumentFile df = new DocumentFile(file,fileVersion);
				documentFileList.add(df);
			}  	    	
			
   	    	List<Element> docImageElements = e.getChildren("Image", e.getNamespace());
   	    	List<Element> docVersionImageElements = e.getChildren("ImageVersion", e.getNamespace());
   	    	ListIterator<Element> docImageIter = docImageElements.listIterator();
   	    	ListIterator<Element> docVersionImageIter = docVersionImageElements.listIterator();
			while( docImageIter.hasNext() ) 
			{
				Element docImageElement = docImageIter.next();
				Element docVersionImageElement = docVersionImageIter.next();
				Image image = Image.Manager.createFromJDOM(id,docImageElement);
				ImageVersion imageVersion = ImageVersion.Manager.createFromJDOM(fieldTemplateIdMapping,image,docVersionImageElement);
				DocumentImage di = new DocumentImage(image,imageVersion);
				documentImageList.add(di);
			}
			
   	    	List<Element> docDropDownElements = e.getChildren("DropDown", e.getNamespace());
   	    	List<Element> docVersionDropDownElements = e.getChildren("DropDownVersion", e.getNamespace());
   	    	ListIterator<Element> docDropDownIter = docDropDownElements.listIterator();
   	    	ListIterator<Element> docVersionDropDownIter = docVersionDropDownElements.listIterator();
			while( docDropDownIter.hasNext() ) 
			{
				Element docDropDownElement = docDropDownIter.next();
				Element docVersionDropDownElement = docVersionDropDownIter.next();
				DropDown dropdown = DropDown.Manager.createFromJDOM(id,docDropDownElement);
				DropDownVersion dropdownVersion = DropDownVersion.Manager.createFromJDOM(dropdown,docVersionDropDownElement);
				DocumentDropdown dd = new DocumentDropdown(dropdown,dropdownVersion);
				documentDropdownList.add(dd);
			}
			
			Namespace documentProgrammingNS = Namespace.getNamespace(XmlUtil.getXmlNamespace2012());
			DocumentProgramming documentProgramming = null;
			Element documentProgrammingElement = e.getChild("DocumentProgramming", documentProgrammingNS);
			if ( documentProgrammingElement != null )
			{
				try
				{
					documentProgramming = new DocumentProgramming(documentProgrammingElement,new EsfUUID());
					for( DocumentProgrammingRule rule : documentProgramming.getRules() )
					{
						rule.updatePageIds(pageIdMapping);
						rule.updatePartyIds(partyIdMapping);
						for( Action action : rule.getActions() )
						{
							action.updatePackagePartyIds(partyIdMapping); // not really package parties, but will get translated at runtime
							action.updateDocumentIds(documentIdMapping);
							action.updateDocumentVersionPageIds(pageIdMapping); 
						}
					}
				}
				catch( EsfException ex )
				{
					_logger.error("DocumentVersion.createFromJDOM() - failed to load package programming", ex);
				}
			}
			
			DocumentVersion newDocumentVersion = new DocumentVersion(
   	    			id, 
   	    			document.getId(), 
   	    			document.getTestVersion(),
   	    			null,
   	    			null,
   	    			null,
   	    			null,
   	    			docStyle.getId(),
   	    			pageOrientation,
   	    			esignProcessRecordLocation,
   	    			pageList,
   	    			fieldTemplateMap,
   	    			partyTemplateList,
   	    			documentFileList,
   	    			documentImageList,
   	    			documentDropdownList,
   	    			documentProgramming == null ? null : documentProgramming.getId()
   	    			);
			newDocumentVersion.documentProgramming = documentProgramming;
			newDocumentVersion.setNewObject();
   	    	cache.add(newDocumentVersion);
   	    	return newDocumentVersion;
   	    }
   	    
   	    /**
   	     * Returns the total number of document version objects in the cache
   	     * @return the total number of document versions in the cache
   	     */
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all document versions that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}
   		
   	} // Manager
   	
}