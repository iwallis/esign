// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;

/**
* FileVersionInfo is a data-only object that holds information about a file version.
* It's not a mutable object (you can't save/delete it), just for lists of file versions and such.
* 
* @author Yozons, Inc.
*/
public class FileVersionInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<FileVersionInfo>, java.io.Serializable
{
	private static final long serialVersionUID = 1551811082765769074L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(FileVersionInfo.class);

    protected EsfUUID id;
    protected EsfUUID fileId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected String fileFileName;
    protected int fileSize;
    
    /**
     * This creates a new FileVersionInfo using the source object.
     * @param fileVersion the file version to use as the source
     */
    protected FileVersionInfo(FileVersion fileVersion)
    {
        this.id = fileVersion.getId();
        this.fileId = fileVersion.getFileId();
        this.version = fileVersion.getVersion();
        this.createdTimestamp = fileVersion.getCreatedTimestamp();
        this.createdByUserId = fileVersion.getCreatedByUserId();
        this.lastUpdatedTimestamp = fileVersion.getLastUpdatedTimestamp();
        this.lastUpdatedByUserId = fileVersion.getLastUpdatedByUserId();
        this.fileFileName = fileVersion.getFileFileName();
        this.fileSize = fileVersion.getFileSize();
    }
    
    /**
     * This creates a FileVersionInfo object from data retrieved from the DB.
     */
    protected FileVersionInfo(
    		EsfUUID id, EsfUUID fileId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId, 
    		String fileFileName, int fileSize)
    {
        this.id = id;
        this.fileId = fileId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.fileFileName = fileFileName;
        this.fileSize = fileSize;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public EsfUUID getFileId()
    {
        return fileId;
    }
    
    public int getVersion()
    {
    	return version;
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }

    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }

    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }

    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }
    
    public String getFileFileName()
    {
    	return fileFileName;
    }

    public int getFileSize()
    {
    	return fileSize;
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof FileVersionInfo )
            return getId().equals(((FileVersionInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(FileVersionInfo o)
    {
    	return getVersion() - o.getVersion();
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all FileVersionInfo objects in the specified library.
   	     * @return the Collection of FileVersionInfo found with most recent versions first.
   	     */
   	    public static Collection<FileVersionInfo> getAll(File file)
   	    {
   	    	LinkedList<FileVersionInfo> list = new LinkedList<FileVersionInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT F.id, F.version, F.created_timestamp, F.created_by_user_id, F.last_updated_timestamp, F.last_updated_by_user_id, F.file_file_name, B.orig_size " +
   	    	            "FROM esf_library_file_version F, esf_blob B " + 
   	    	            "WHERE F.library_file_id=? AND F.file_blob_id=B.id " +
   	    	            "ORDER BY F.version DESC"
   	            						   );
   	        	stmt.set(file.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            String fileFileName = rs.getString();
		            int fileSize = rs.getInt();
		            
		            FileVersionInfo fileVerInfo = new FileVersionInfo(id, file.getId(), version,
		            							createdTimestamp, createdByUserId, lastUpdatedTimestamp, lastUpdatedByUserId, fileFileName, fileSize
		            		 												);
		            fileVerInfo.setLoadedFromDb();
		            list.add(fileVerInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - File: " + file.getEsfName() + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - File: " + file.getEsfName() + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static FileVersionInfo createLike(File likeFile, FileVersion likeFileVersion, User createdBy)
   	    {
   			FileVersion newFileVersion = FileVersion.Manager.createLike(likeFile, likeFileVersion, createdBy);
   			FileVersionInfo newFileVerInfo = new FileVersionInfo(newFileVersion);
   			return newFileVerInfo;
   	    }

   	    public static FileVersionInfo createNew(File file, User createdBy)
   	    {
   			FileVersion newFileVersion = FileVersion.Manager.createTest(file, createdBy);
   			FileVersionInfo newFileVerInfo = new FileVersionInfo(newFileVersion);
   			return newFileVerInfo;
   	    }

   	    public static FileVersionInfo createFromSource(FileVersion fileVersion)
   	    {
   	    	if ( fileVersion == null )
   	    		return null;
   	    	
   			FileVersionInfo fileVerInfo = new FileVersionInfo(fileVersion);
   			fileVerInfo.setLoadedFromDb();

   			return fileVerInfo;
   	    }
   	    
   	} // Manager
   	
}