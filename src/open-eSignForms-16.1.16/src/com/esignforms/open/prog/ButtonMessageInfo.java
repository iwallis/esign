// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;

/**
* ButtonMessageInfo is a data-only object that holds information about a button message selection list.
* It's not a mutable object (you can't save/delete it), just for lists of button messages.
* 
* @author Yozons, Inc.
*/
public class ButtonMessageInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<ButtonMessageInfo>, java.io.Serializable
{
	private static final long serialVersionUID = -6318010745781518394L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ButtonMessageInfo.class);

    public enum INCLUDE { ONLY_ENABLED, ONLY_DISABLED, BOTH_ENABLED_AND_DISABLED };

    protected EsfUUID id;
    protected EsfName esfname;
    protected String  description;
    protected String  status;
    protected int	  productionVersion;
    protected int	  testVersion;

    /**
     * This creates a new ButtonMessageInfo using the source object.
     * @param buttonmessage the buttonmessage to use as the source
     */
    protected ButtonMessageInfo(ButtonMessage buttonmessage)
    {
    	this.id = buttonmessage.getId();
    	this.esfname = buttonmessage.getEsfName();
    	this.description = buttonmessage.getDescription();
    	this.status = buttonmessage.getStatus();
    	this.productionVersion = buttonmessage.getProductionVersion();
    	this.testVersion = buttonmessage.getTestVersion();
    }
    
    /**
     * This creates a ButtonMessageInfo object from data retrieved from the DB.
     */
    protected ButtonMessageInfo(EsfUUID id, EsfName esfname, String description, String status, int productionVersion, int testVersion)
    {
    	this.id = id;
    	this.esfname = esfname;
    	this.description = description;
    	this.status = status;
    	this.productionVersion = productionVersion;
    	this.testVersion = testVersion;
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }

    public int getProductionVersion()
    {
    	return productionVersion;
    }
    
    public int getTestVersion()
    {
    	return testVersion;
    }

    public boolean hasProductionVersion()
    {
    	return productionVersion > 0;
    }

    public boolean hasTestVersion()
    {
    	return testVersion > productionVersion;
    }

    public EsfUUID getId()
    {
        return id;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof ButtonMessageInfo )
            return getId().equals(((ButtonMessageInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(ButtonMessageInfo d)
    {
    	return getEsfName().compareTo(d.getEsfName());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all ButtonMessageInfo objects in the specified library.
   	     * @return the Collection of ButtonMessageInfo found ordered by esfname.
   	     */
   	    public static Collection<ButtonMessageInfo> getAll(Library library, INCLUDE include)
   	    {
   	    	LinkedList<ButtonMessageInfo> list = new LinkedList<ButtonMessageInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id,esfname,description,status,production_version,test_version " +
   	    	            "FROM esf_library_buttonmessage " + 
   	    	            "WHERE library_id=? " +
   	    	            ((include == INCLUDE.BOTH_ENABLED_AND_DISABLED) ? "" : "AND status=? " ) +
   	    	            "ORDER BY lower(esfname) ASC"
   	            						   );
   	        	stmt.set(library.getId());
   	        	if ( include == INCLUDE.ONLY_ENABLED )
   	        		stmt.set(Literals.STATUS_ENABLED);
   	        	else if ( include == INCLUDE.ONLY_DISABLED )
   	        		stmt.set(Literals.STATUS_DISABLED);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            EsfName esfname = rs.getEsfName();
		            String description = rs.getString();
		            String  stat = rs.getString();
		            int productionVersion = rs.getInt();
		            int testVersion = rs.getInt();
		            
		            ButtonMessageInfo docInfo = new ButtonMessageInfo(id,esfname,description,stat,productionVersion,testVersion);
		            docInfo.setLoadedFromDb();
		            list.add(docInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - Library: " + library.getPathName() + "; found: " + list.size() + "; include: " + include);
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - Library: " + library.getPathName() + "; found: " + list.size() + " entries added before exception" + "; include: " + include);
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static Collection<ButtonMessageInfo> getAll(Library library)
   	    {
   	    	return getAll(library,INCLUDE.BOTH_ENABLED_AND_DISABLED);
   	    }
   	    
   	    public static ButtonMessageInfo createNew(Library library)
   	    {
   	    	if ( library == null )
   	    		return null;
   	    	
   			ButtonMessage buttonmessage = ButtonMessage.Manager.createNew(library.getId());
   	    	return new ButtonMessageInfo(buttonmessage);
   	    }
   	    
   	    public static ButtonMessageInfo createLike(ButtonMessage likeButtonMessage, EsfName newName)
   	    {
   	    	if ( likeButtonMessage == null || newName == null || ! newName.isValid() )
   	    		return null;

   	    	ButtonMessage newButtonMessage = ButtonMessage.Manager.createLike(likeButtonMessage.getLibraryId(), likeButtonMessage, newName);
   	    	return new ButtonMessageInfo(newButtonMessage);
   	    }
   	    
   	    public static ButtonMessageInfo createFromSource(ButtonMessage buttonmessage)
   	    {
   	    	if ( buttonmessage == null )
   	    		return null;
   	    	
   	    	ButtonMessageInfo docInfo = new ButtonMessageInfo(buttonmessage);
   	    	docInfo.setLoadedFromDb();

   			return docInfo;
   	    }
   	    
   	} // Manager
   	
}