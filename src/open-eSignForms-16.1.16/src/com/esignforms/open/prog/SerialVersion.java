// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;

import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* SerialVersion holds the definition of a specific version of a serial number generator held in a library.
* 
* @author Yozons, Inc.
*/
public class SerialVersion
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<SerialVersion>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable
{
	private static final long serialVersionUID = 1075730037318106572L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(SerialVersion.class);
    
    protected final EsfUUID id;
    protected final EsfUUID serialId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected String  decimalFormat;
    
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();

    /**
     * This creates a SerialVersion object from data retrieved from the DB.
     */
    protected SerialVersion(EsfUUID id, EsfUUID serialId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		String decimalFormat)
    {
        this.id = id;
        this.serialId = serialId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.decimalFormat = decimalFormat;
    }
    
    protected SerialVersion(Serial serial, User createdByUser)
    {
        this.id = new EsfUUID();
        this.serialId = serial.getId();
        this.version = serial.getProductionVersion() + 1;
        this.createdTimestamp = new EsfDateTime();
        this.createdByUserId = createdByUser.getId();
        this.lastUpdatedTimestamp = createdTimestamp;
        this.lastUpdatedByUserId = createdByUserId;
        this.decimalFormat = "###########0";
    }
    
    public SerialVersion duplicate()
    {
    	SerialVersion sv = new SerialVersion(id, serialId, version, 
        		createdTimestamp.duplicate(), createdByUserId,  lastUpdatedTimestamp.duplicate(), lastUpdatedByUserId,
        		decimalFormat);
    	sv.setDatabaseObjectLike(this);
    	return sv;
    }
   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getSerialId()
    {
        return serialId;
    }
    
    public Serial getSerial()
    {
    	return Serial.Manager.getById(serialId);
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}

	public int getVersion()
    {
    	return version;
    }
    
	public String getVersionLabel()
	{
		return getSerial().getVersionLabel(version);
	}
	public String getSerialNameVersion()
	{
		return getSerial().getEsfName() + " [" + version + "]";
	}
	public String getSerialNameVersionWithLabel()
	{
		Serial serial = getSerial();
		return serial.getEsfName() + " [" + version + "] (" + serial.getVersionLabel(version) + ")";
	}
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }
    
    public String getDecimalFormat()
    {
        return decimalFormat;
    }
    public void setDecimalFormat(String v)
    {
    	if ( v != null )
    	{
        	if ( v.length() > Literals.SERIAL_FORMAT_MAX_LENGTH )
        		decimalFormat = v.substring(0,Literals.SERIAL_FORMAT_MAX_LENGTH).trim();
            else
            	decimalFormat = v.trim();
            objectChanged();
    	}
    }

    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof SerialVersion )
        {
        	SerialVersion other = (SerialVersion)o;
            return getId().equals(other.getId());
        }
        return false;
    }
    
    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(SerialVersion o)
    {
    	if ( getSerialId().equals(o.getSerialId()) )
    		return getVersion() - o.getVersion();
    	return getSerialId().compareTo(o.getSerialId());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<SerialVersion xmlns=\"").append(XmlUtil.getXmlNamespace2012()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <serialId>").append(serialId.toXml()).append("</serialId>\n");
        buf.append(" <version>").append(version).append("</version>\n");
        buf.append(" <createdTimestamp>").append(createdTimestamp.toXml()).append("</createdTimestamp>\n");
        buf.append(" <createdByUserId>").append(createdByUserId.toXml()).append("</createdByUserId>\n");
        buf.append(" <lastUpdatedTimestamp>").append(lastUpdatedTimestamp.toXml()).append("</lastUpdatedTimestamp>\n");
        buf.append(" <lastUpdatedByUserId>").append(lastUpdatedByUserId.toXml()).append("</lastUpdatedByUserId>\n");
        buf.append(" <decimalFormat>").append(escapeXml(decimalFormat)).append("</decimalFormat>\n");

        buf.append("</SerialVersion>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 140 + id.getEstimatedLengthXml() + serialId.getEstimatedLengthXml() + createdTimestamp.getEstimatedLengthXml() + createdByUserId.getEstimatedLengthXml() +
        	lastUpdatedTimestamp.getEstimatedLengthXml() + lastUpdatedByUserId.getEstimatedLengthXml() + decimalFormat.length();
        return len; 
    }

    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; serialId: " + serialId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
            	
				stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_serial_version (id,library_serial_id,version,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,decimal_format) VALUES (?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(serialId);
                stmt.set(version);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(decimalFormat);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; serialId: " + serialId + "; version: " + version);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this);
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new serial version id: " + id + "; serialId: " + serialId + "; version: " + version); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new serial version id: " + id + "; serialId: " + serialId + "; version: " + version);
                }
                
                return true;
            }
            
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_serial_version SET last_updated_timestamp=?,last_updated_by_user_id=?,decimal_format=? WHERE id=?"
                						   		);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(decimalFormat);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for serial version id: " + id + "; serialId: " + serialId + "; version: " + version);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated serial version id: " + id + "; serialId: " + serialId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated serial version id: " + id + "; serialId: " + serialId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; serialId: " + serialId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; serialId: " + serialId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use 
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of serial version that was pending an INSERT id: " + id + "; serialId: " + serialId + "; version: " + version);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_serial_version WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted serial version id: " + id + "; serialId: " + serialId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted serial version id: " + id + "; serialId: " + serialId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on id: " + id + "; serialId: " + serialId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   		private static UUIDCacheReadOptimized<SerialVersion> cache = new UUIDCacheReadOptimized<SerialVersion>();
   		
   		static final SerialVersion getById(Connection con, EsfUUID serialVersionId) throws SQLException
   		{
   			return getById(con,serialVersionId,true);
   		}
   		static SerialVersion getById(Connection con, EsfUUID serialVersionId, boolean checkCache) throws SQLException
   		{
   			if ( checkCache )
   			{
   				SerialVersion serialVer = cache.getById(serialVersionId);
   	   			if ( serialVer != null )
   	   				return serialVer;
   			}

   			EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT library_serial_id,version,created_by_user_id,last_updated_by_user_id,created_timestamp,last_updated_timestamp,decimal_format " +
   	        			"FROM esf_library_serial_version WHERE id=?"
   	        									);
   	        	stmt.set(serialVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID librarySerialId = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            String decimalFormat = rs.getString();
		            
		            SerialVersion serialVer = new SerialVersion(serialVersionId,librarySerialId,version,
		            											 createdTimestamp,createdByUserId,lastUpdatedTimestamp,lastUpdatedByUserId,
		            											 decimalFormat);
		            serialVer.setLoadedFromDb();
		            cache.add(serialVer);
		            
	   	            return serialVer;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + serialVersionId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + serialVersionId + "; failed to find the serial version");
   	        return null; 
  		}
   		
   		public static SerialVersion getById(EsfUUID serialVersionId)
   		{
   			SerialVersion serialVer = cache.getById(serialVersionId);
   			if ( serialVer != null )
   				return serialVer;
   	   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	serialVer = getById(con,serialVersionId,false);
   	        	if ( serialVer != null ) 
   	        	{
		            con.commit();
	   	        	return serialVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		static SerialVersion getByVersion(Connection con, EsfUUID librarySerialId, int version) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_serial_version WHERE library_serial_id=? AND version=?" );
   	        	stmt.set(librarySerialId);
   	        	stmt.set(version);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID versionId = rs.getEsfUUID();
	       			return getById(con,versionId);
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByVersion() - librarySerialId: " + librarySerialId + "; version: " + version);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getByVersion() - librarySerialId: " + librarySerialId + "; version: " + version + "; failed to find the serial version");
   	        return null; 
  		}
   		
   		public static SerialVersion getByVersion(EsfUUID librarySerialId, int version)
   		{
   			// While not always true, if the version is 1, it's possible this is a new object that's not been saved yet so we can't find the id in the DB, so we'll check our cache first just in case
   			if ( version == 1 )
   			{
   				for( EsfUUID id : cache.getAllIds() )
   				{
   					SerialVersion serialVer = cache.getById(id);
   					if ( serialVer.getSerialId().equals(librarySerialId) && serialVer.getVersion() == version )
   						return serialVer;
   				}
   			}
   			
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	SerialVersion serialVer = getByVersion(con,librarySerialId,version);
   	        	if ( serialVer != null ) 
   	        	{
		            con.commit();
	   	        	return serialVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		
   		public static List<SerialVersion> getAllBySerialId(Connection con, EsfUUID librarySerialId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<SerialVersion> list = new LinkedList<SerialVersion>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_serial_version WHERE library_serial_id=?" );
   	        	stmt.set(librarySerialId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID serialVersionId = rs.getEsfUUID();
	            	
	            	SerialVersion serialVer = getById(con,serialVersionId);
	       			if ( serialVer != null )
	       				list.add(serialVer);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<SerialVersion> getAllBySerialId(EsfUUID librarySerialId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<SerialVersion> list = getAllBySerialId(con,librarySerialId);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return new LinkedList<SerialVersion>(); 
   		}
   	    
   		
   	    
   	    public static SerialVersion createTest(Serial serial, User createdBy)
   	    {
   	    	if ( serial == null || createdBy == null )
   	    		return null;
   	    	SerialVersion newSerialVersion = new SerialVersion(serial,createdBy);
   	    	cache.add(newSerialVersion);
   	    	return newSerialVersion;
   	    }

   	    public static SerialVersion createLike(Serial serial, SerialVersion likeSerialVersion, User createdBy)
   	    {
   	    	if ( serial == null || likeSerialVersion == null || createdBy == null )
   	    		return null;
   	    	SerialVersion newSerialVersion = createTest(serial,createdBy);
   	    	newSerialVersion.decimalFormat = likeSerialVersion.decimalFormat;

   	    	return newSerialVersion;
   	    }

   	    public static SerialVersion createFromJDOM(Serial serial, Element e)
   	    {
   	    	if ( serial == null || e == null )
   	    		return null;
   	    	
   	    	EsfUUID id = new EsfUUID();
   	    	
			SerialVersion newSerialVersion = new SerialVersion(
   	    			id, 
   	    			serial.getId(), 
   	    			serial.getTestVersion(),
   	    			null,
   	    			null,
   	    			null,
   	    			null,
   	    			e.getChildText("decimalFormat", e.getNamespace())
   	    			);
			newSerialVersion.setNewObject();
   	    	cache.add(newSerialVersion);
   	    	return newSerialVersion;
   	    }
   	    
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all serial versions that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }

   		public static void clearCache()
   		{
   			cache.clear();
   		}
   		
   		public static void dropFromCache(SerialVersion serialVersion)
   		{
   	   		cache.remove(serialVersion);
   		}
   		public static void replaceInCache(SerialVersion serialVersion)
   		{
   	   		cache.remove(serialVersion);
   	   		cache.add(serialVersion);
   		}
   		
   	} // Manager
   	
}