// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* DropDownVersion holds the definition of a specific version of a drop down held in a library.
* A drop down has one or more choices.
* 
* @author Yozons, Inc.
*/
public class DropDownVersion
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<DropDownVersion>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable
{
	private static final long serialVersionUID = 8549530051562446231L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DropDownVersion.class);
    
    private final static String MULTI_SELECTION_ALLOW = "Y";
    private final static String MULTI_SELECTION_NOT_ALLOWED = "N";

    protected final EsfUUID id;
    protected final EsfUUID dropDownId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected String allowMultiSelection;
    
    protected List<DropDownVersionOption> optionList;
    
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();

    /**
     * This creates a DropDownVersion object from data retrieved from the DB.
     */
    protected DropDownVersion(EsfUUID id, EsfUUID dropDownId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		String allowMultiSelection, List<DropDownVersionOption> optionList)
    {
        this.id = id;
        this.dropDownId = dropDownId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.allowMultiSelection = allowMultiSelection;
        this.optionList = optionList;
    }
    
    protected DropDownVersion(DropDown dropDown, User createdByUser)
    {
        this.id = new EsfUUID();
        this.dropDownId = dropDown.getId();
        this.version = dropDown.getProductionVersion() + 1;
        this.createdTimestamp = new EsfDateTime();
        this.createdByUserId = createdByUser.getId();
        this.lastUpdatedTimestamp = createdTimestamp;
        this.lastUpdatedByUserId = createdByUserId;
        this.allowMultiSelection = MULTI_SELECTION_NOT_ALLOWED;
        
        optionList = new LinkedList<DropDownVersionOption>();
        DropDownVersionOption option1 = new DropDownVersionOption(this,(short)1);
        optionList.add(option1);
    }

    public DropDownVersion duplicate()
    {
    	DropDownVersion dv = new DropDownVersion(id, dropDownId, version, 
        		createdTimestamp.duplicate(), createdByUserId,  lastUpdatedTimestamp.duplicate(), lastUpdatedByUserId,
        		allowMultiSelection, getDuplicatedOptions());
    	dv.setDatabaseObjectLike(this);
    	return dv;
    }
   

    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getDropDownId()
    {
        return dropDownId;
    }
    
    public DropDown getDropDown()
    {
    	return DropDown.Manager.getById(dropDownId);
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}

	public int getVersion()
    {
    	return version;
    }
    
	public String getVersionLabel()
	{
		return getDropDown().getVersionLabel(version);
	}
	public String getDropDownNameVersion()
	{
		return getDropDown().getEsfName() + " [" + version + "]";
	}
	public String getDropDownNameVersionWithLabel()
	{
		DropDown dropdown = getDropDown();
		return dropdown.getEsfName() + " [" + version + "] (" + dropdown.getVersionLabel(version) + ")";
	}

	public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }
    
    public String getAllowMultiSelection()
    {
    	return allowMultiSelection;
    }
    public void setAllowMultiSelection(String v)
    {
    	allowMultiSelection = EsfBoolean.toBoolean(v) ? MULTI_SELECTION_ALLOW : MULTI_SELECTION_NOT_ALLOWED;
    	objectChanged();
    }
    public void setAllowMultiSelection(boolean v)
    {
    	allowMultiSelection = v ? MULTI_SELECTION_ALLOW : MULTI_SELECTION_NOT_ALLOWED;
    	objectChanged();
    }
    public boolean isAllowMultiSelection()
    {
    	return MULTI_SELECTION_ALLOW.equals(allowMultiSelection);
    }
    
    public List<DropDownVersionOption> getOptions()
    {
    	return new LinkedList<DropDownVersionOption>(optionList);
    }
    public int getNumOptions()
    {
    	return optionList.size();
    }
    public void setOptions(List<DropDownVersionOption> list)
    {
    	optionList.clear();
    	
    	short order = 1;
    	for( DropDownVersionOption option: list )
    	{
    		option.setListOrder(order++);
    		optionList.add(option);
    	}
    	objectChanged();
    }
    public List<DropDownVersionOption> getDuplicatedOptions()
    {
    	LinkedList<DropDownVersionOption> list = new LinkedList<DropDownVersionOption>();
    	for( DropDownVersionOption o : optionList )
    		list.add( o.duplicate() );
    	return list;
    }

	public synchronized void setOptionOrder(LinkedList<String> optionOrderOptions) {
		
		if ( optionOrderOptions == null || optionOrderOptions.size() == 0 || getNumOptions() == 0 )
			return;
		
		LinkedList<DropDownVersionOption> origOptionList = new LinkedList<DropDownVersionOption>(optionList);
    	LinkedList<DropDownVersionOption> newOptionList = new LinkedList<DropDownVersionOption>();

    	// For each option requested, if we find it, we move it into our new order.
    	short optionNum = 1;
    	for( String optionName : optionOrderOptions ) {
        	ListIterator<DropDownVersionOption> origOptionListIter = origOptionList.listIterator();
        	while( origOptionListIter.hasNext() ) {
        		DropDownVersionOption o = origOptionListIter.next();
        		if ( o.getOption().equals(optionName) ) {
            		o.setListOrder(optionNum++);
            		newOptionList.add(o);
            		origOptionListIter.remove();
        			break;
        		}
        	}
    	}
    	
    	// If we have any options remaining (were not in the optionOrderNames list), let's move them in too
    	ListIterator<DropDownVersionOption> origOptionListIter = origOptionList.listIterator();
    	while( origOptionListIter.hasNext() ) {
    		DropDownVersionOption o = origOptionListIter.next();
    		o.setListOrder(optionNum++);
    		newOptionList.add(o);
    	}
    	
     	optionList = newOptionList;
     	objectChanged();
	}
	
	public String getOptionByValue(String value)
	{
		for ( DropDownVersionOption ddvOption : optionList )
		{
			if ( ddvOption.getValue().equals(value) )
				return ddvOption.getOption();
		}
		return null;
	}
	
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof DropDownVersion )
        {
        	DropDownVersion otherDropDown = (DropDownVersion)o;
            return getId().equals(otherDropDown.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(DropDownVersion o)
    {
    	if ( getDropDownId().equals(o.getDropDownId()) )
    		return getVersion() - o.getVersion();
    	return getDropDownId().compareTo(o.getDropDownId());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<DropDownVersion xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <dropDownId>").append(dropDownId.toXml()).append("</dropDownId>\n");
        buf.append(" <version>").append(version).append("</version>\n");
        buf.append(" <createdTimestamp>").append(createdTimestamp.toXml()).append("</createdTimestamp>\n");
        buf.append(" <createdByUserId>").append(createdByUserId.toXml()).append("</createdByUserId>\n");
        buf.append(" <lastUpdatedTimestamp>").append(lastUpdatedTimestamp.toXml()).append("</lastUpdatedTimestamp>\n");
        buf.append(" <lastUpdatedByUserId>").append(lastUpdatedByUserId.toXml()).append("</lastUpdatedByUserId>\n");
        buf.append(" <allowMultiSelection>").append(escapeXml(allowMultiSelection)).append("</allowMultiSelection>\n");

        for( DropDownVersionOption ddvo : optionList )
        	ddvo.appendXml(buf);
        
        buf.append("</DropDownVersion>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 315 + id.getEstimatedLengthXml() + dropDownId.getEstimatedLengthXml() + createdTimestamp.getEstimatedLengthXml() + createdByUserId.getEstimatedLengthXml() + 
        	lastUpdatedTimestamp.getEstimatedLengthXml() + lastUpdatedByUserId.getEstimatedLengthXml() + allowMultiSelection.length();

        for( DropDownVersionOption ddvo : optionList )
        	len += ddvo.getEstimatedLengthXml();
        
        return len; 
    }

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; dropDownId: " + dropDownId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_dropdown_version (id,library_dropdown_id,version,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,allow_multi_selection) VALUES (?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(dropDownId);
                stmt.set(version);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(allowMultiSelection);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; dropDownId: " + dropDownId + "; version: " + version);
                    return false;
                }
                
                // Always save options when it's a new dropdown version
            	for( DropDownVersionOption option : optionList )
            		option.saveOnRewrite(con, user);
            	
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this);
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new drop down version id: " + id + "; dropDownId: " + dropDownId + "; version: " + version); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new drop down version id: " + id + "; dropDownId: " + dropDownId + "; version: " + version);
                }
                
                return true;
            }
            
            // We only save options if we're a Test version.
            if ( getDropDown().getTestVersion() == version )
            {
            	// If any of the options themselves have changed, we save them all again after we delete them
                boolean saveOptions = false;
                if ( optionList != null )
                {
                	for( DropDownVersionOption option : optionList )
                	{
                		if ( option.hasChanged() )
                		{
                			objectChanged(); // want to update our base object too
                			saveOptions = true;
                			break;
                		}
                	}
                }
            	
            	if ( saveOptions )
            	{
            		// Before we save our options, which may be reordered, we delete whatever was there before and then save each option again
            		DropDownVersionOption.Manager.deleteAllOptions(con, id);
                	for( DropDownVersionOption option : optionList )
                		option.saveOnRewrite(con, user);
            	}
            }
        	
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_dropdown_version SET last_updated_timestamp=?,last_updated_by_user_id=?,allow_multi_selection=? WHERE id=?"
                						   		);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(allowMultiSelection);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for dropdown version id: " + id + "; dropDownId: " + dropDownId + "; version: " + version);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated drop down version id: " + id + "; dropDownId: " + dropDownId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated drop down version id: " + id + "; dropDownId: " + dropDownId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; dropDownId: " + dropDownId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; dropDownId: " + dropDownId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use in the transaction definition.
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of drop down version that was pending an INSERT id: " + id + "; dropDownId: " + dropDownId + "; version: " + version);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
            // Delete the dropdown version and its options
        	for( DropDownVersionOption option : optionList )
        		option.delete(con,errors,user);
            
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_dropdown_version WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted drop down version id: " + id + "; dropDownId: " + dropDownId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted drop down version id: " + id + "; dropDownId: " + dropDownId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"delete() on id: " + id + "; dropDownId: " + dropDownId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   		private static UUIDCacheReadOptimized<DropDownVersion> cache = new UUIDCacheReadOptimized<DropDownVersion>();
   		
   		static final DropDownVersion getById(Connection con, EsfUUID dropDownVersionId) throws SQLException
   		{
   			return getById(con,dropDownVersionId,true);
   		}
   		static DropDownVersion getById(Connection con, EsfUUID dropDownVersionId, boolean checkCache) throws SQLException
   		{
   			if ( checkCache )
   			{
   				DropDownVersion dropDownVer = cache.getById(dropDownVersionId);
   	   			if ( dropDownVer != null )
   	   				return dropDownVer;
   			}

   			EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT library_dropdown_id,version,created_by_user_id,last_updated_by_user_id,created_timestamp,last_updated_timestamp,allow_multi_selection " +
   	        			"FROM esf_library_dropdown_version WHERE id = ?"
   	        									);
   	        	stmt.set(dropDownVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID libraryDropDownId = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            String allowMultiSelection = rs.getString();
		            
		            List<DropDownVersionOption> optionList = DropDownVersionOption.Manager.getAllOptionsByDropDownVersion(dropDownVersionId);
		            
		            DropDownVersion dropDownVer = new DropDownVersion(dropDownVersionId,libraryDropDownId,version,
		            											 createdTimestamp,createdByUserId,lastUpdatedTimestamp,lastUpdatedByUserId,
		            											 allowMultiSelection,optionList);
		            dropDownVer.setLoadedFromDb();
		            cache.add(dropDownVer);
		            
	   	            return dropDownVer;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + dropDownVersionId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + dropDownVersionId + "; failed to find the drop down version");
   	        return null; 
  		}
   		
   		public static DropDownVersion getById(EsfUUID dropDownVersionId)
   		{
   			DropDownVersion dropDownVer = cache.getById(dropDownVersionId);
   			if ( dropDownVer != null )
   				return dropDownVer;
   	   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	dropDownVer = getById(con,dropDownVersionId,false);
   	        	if ( dropDownVer != null ) 
   	        	{
		            con.commit();
	   	        	return dropDownVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		static DropDownVersion getByVersion(Connection con, EsfUUID libraryDropDownId, int version) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_dropdown_version WHERE library_dropdown_id = ? AND version = ?" );
   	        	stmt.set(libraryDropDownId);
   	        	stmt.set(version);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID docVersionId = rs.getEsfUUID();
	       			return getById(con,docVersionId);
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByVersion() - libraryDropDownId: " + libraryDropDownId + "; version: " + version);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getByVersion() - libraryDropDownId: " + libraryDropDownId + "; version: " + version + "; failed to find the dropdown version");
   	        return null; 
  		}
   		
   		public static DropDownVersion getByVersion(EsfUUID libraryDropDownId, int version)
   		{
   			// While not always true, if the version is 1, it's possible this is a new object that's not been saved yet so we can't find the id in the DB, so we'll check our cache first just in case
   			if ( version == 1 )
   			{
   				for( EsfUUID id : cache.getAllIds() )
   				{
   					DropDownVersion dropDownVer = cache.getById(id);
   					if ( dropDownVer.getDropDownId().equals(libraryDropDownId) && dropDownVer.getVersion() == version )
   						return dropDownVer;
   				}
   			}
   			
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	DropDownVersion dropDownVer = getByVersion(con,libraryDropDownId,version);
   	        	if ( dropDownVer != null ) 
   	        	{
		            con.commit();
	   	        	return dropDownVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		
   		public static List<DropDownVersion> getAllByDropDownId(Connection con, EsfUUID libraryDropDownId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<DropDownVersion> list = new LinkedList<DropDownVersion>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_dropdown_version WHERE library_dropdown_id = ?" );
   	        	stmt.set(libraryDropDownId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID dropDownVersionId = rs.getEsfUUID();
	            	
	            	DropDownVersion dropDownVer = getById(con,dropDownVersionId);
	       			if ( dropDownVer != null )
	       				list.add(dropDownVer);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<DropDownVersion> getAllByDropDownId(EsfUUID libraryDropDownId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<DropDownVersion> list = getAllByDropDownId(con,libraryDropDownId);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return new LinkedList<DropDownVersion>(); 
   		}
   	    
   		
   	    
   	    public static DropDownVersion createTest(DropDown dropDown, User createdBy)
   	    {
   	    	if ( dropDown == null || createdBy == null )
   	    		return null;
   	    	DropDownVersion newDropDownVersion = new DropDownVersion(dropDown,createdBy);
   	    	cache.add(newDropDownVersion);
   	    	return newDropDownVersion;
   	    }

   	    public static DropDownVersion createLike(DropDown dropDown, DropDownVersion likeDropDownVersion, User createdBy)
   	    {
   	    	if ( dropDown == null || likeDropDownVersion == null || createdBy == null )
   	    		return null;
   	    	DropDownVersion newDropDownVersion = createTest(dropDown,createdBy);
   	    	newDropDownVersion.optionList.clear();
   	    	for( DropDownVersionOption o : likeDropDownVersion.getOptions() ) 
   	    	{
   	    		newDropDownVersion.optionList.add( DropDownVersionOption.Manager.createLike(newDropDownVersion, o) );
   	    	}
   	    	return newDropDownVersion;
   	    }

   	    public static DropDownVersion createFromJDOM(DropDown dropDown, Element e)
   	    {
   	    	if ( dropDown == null || e == null )
   	    		return null;
   	    	
   	    	LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
   	    	
   	    	EsfUUID id = new EsfUUID();
   	    	
   	    	List<Element> optionsElements = e.getChildren("DropDownVersionOption", e.getNamespace());
   	    	ListIterator<Element> optionsIter = optionsElements.listIterator();
			while( optionsIter.hasNext() ) 
			{
				Element optionElement = optionsIter.next();
				DropDownVersionOption option = DropDownVersionOption.Manager.createFromJDOM(id,optionElement);
				optionList.add(option);
			}  	    	
			
   	    	DropDownVersion newDropDownVersion = new DropDownVersion(
   	    			id, 
   	    			dropDown.getId(), 
   	    			dropDown.getTestVersion(),
   	    			null,
   	    			null,
   	    			null,
   	    			null,
   	    			e.getChildText("allowMultiSelection", e.getNamespace()),
   	    			optionList
   	    			);
   	    	newDropDownVersion.setNewObject();
   	    	cache.add(newDropDownVersion);
   	    	return newDropDownVersion;
   	    }
   	    
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all drop down versions that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void dropFromCache(DropDownVersion dropdownVersion)
   		{
   	   		cache.remove(dropdownVersion);
   		}
   		public static void replaceInCache(DropDownVersion dropdownVersion)
   		{
   	   		cache.remove(dropdownVersion);
   	   		cache.add(dropdownVersion);
   		}
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}

   	} // Manager
   	
}