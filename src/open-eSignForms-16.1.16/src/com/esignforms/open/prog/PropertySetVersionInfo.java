// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;

/**
* PropertySetVersionInfo is a data-only object that holds information about a property set version.
* It's not a mutable object (you can't save/delete it), just for lists of propertySet versions and such.
* 
* @author Yozons, Inc.
*/
public class PropertySetVersionInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<PropertySetVersionInfo>, java.io.Serializable
{
	private static final long serialVersionUID = 2616703157937668971L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PropertySetVersionInfo.class);

    protected EsfUUID id;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected int numProperties;
    
    /**
     * This creates a new PropertySetVersionInfo using the source object.
     * @param propertySetVersion the propertySet version to use as the source
     */
    protected PropertySetVersionInfo(PropertySetVersion propertySetVersion)
    {
        this.id = propertySetVersion.getId();
        this.version = propertySetVersion.getVersion();
        this.createdTimestamp = propertySetVersion.getCreatedTimestamp();
        this.createdByUserId = propertySetVersion.getCreatedByUserId();
        this.lastUpdatedTimestamp = propertySetVersion.getLastUpdatedTimestamp();
        this.lastUpdatedByUserId = propertySetVersion.getLastUpdatedByUserId();
        this.numProperties = propertySetVersion.getNumProperties();
    }
    
    /**
     * This creates a PropertySetVersionInfo object from data retrieved from the DB.
     */
    protected PropertySetVersionInfo(
    		EsfUUID id, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId, int numProperties)
    {
        this.id = id;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.numProperties = numProperties;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public int getVersion()
    {
    	return version;
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }

    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }

    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }

    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }

    public int getNumProperties()
    {
    	return numProperties;
    }
    

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof PropertySetVersionInfo )
            return getId().equals(((PropertySetVersionInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(PropertySetVersionInfo d)
    {
    	return getVersion() - d.getVersion();
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all PropertySetVersionInfo objects in the specified library.
   	     * @return the Collection of PropertySetVersionInfo found with most recent versions first.
   	     */
   	    public static Collection<PropertySetVersionInfo> getAll(PropertySet propertySet)
   	    {
   	    	_logger.debug("Manager.getAll() - PropertySet: " + propertySet.getEsfName());
   	    	
   	    	LinkedList<PropertySetVersionInfo> list = new LinkedList<PropertySetVersionInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id, version, created_timestamp, created_by_user_id, last_updated_timestamp, last_updated_by_user_id, num_properties " +
   	    	            "FROM esf_library_propertyset_version " + 
   	    	            "WHERE library_propertyset_id = ? " +
   	    	            "ORDER BY version DESC"
   	            						   );
   	        	stmt.set(propertySet.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            int numProperties = rs.getInt();
		            
		            PropertySetVersionInfo propertySetVerInfo = new PropertySetVersionInfo(id, version,
		            							createdTimestamp, createdByUserId, lastUpdatedTimestamp, lastUpdatedByUserId, numProperties
		            		 												);
		            propertySetVerInfo.setLoadedFromDb();
		            list.add(propertySetVerInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - PropertySet: " + propertySet.getEsfName() + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - PropertySet: " + propertySet.getEsfName() + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static PropertySetVersionInfo createLike(PropertySet likePropertySet, PropertySetVersion likePropertySetVersion, User createdBy)
   	    {
   			PropertySetVersion newPropertySetVersion = PropertySetVersion.Manager.createLike(likePropertySet, likePropertySetVersion, createdBy);
   			PropertySetVersionInfo newPropertySetVerInfo = new PropertySetVersionInfo(newPropertySetVersion);
   			return newPropertySetVerInfo;
   	    }

   	    public static PropertySetVersionInfo createNew(PropertySet propertySet, User createdBy)
   	    {
   			PropertySetVersion newPropertySetVersion = PropertySetVersion.Manager.createTest(propertySet, createdBy);
   			PropertySetVersionInfo newPropertySetVerInfo = new PropertySetVersionInfo(newPropertySetVersion);
   			return newPropertySetVerInfo;
   	    }

   	    public static PropertySetVersionInfo createFromSource(PropertySetVersion propertySetVersion)
   	    {
   	    	if ( propertySetVersion == null )
   	    		return null;
   	    	
   			PropertySetVersionInfo propertySetVerInfo = new PropertySetVersionInfo(propertySetVersion);
   			propertySetVerInfo.setLoadedFromDb();

   			return propertySetVerInfo;
   	    }
   	    
   	} // Manager
   	
}