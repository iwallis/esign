// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;

/**
* DocumentAndVersionInfo is a data-only object that holds information about a document version (a combination of Document and DocumentVersion).
* It's not a mutable object (you can't save/delete it), just for lists of documents and such.
* 
* @author Yozons, Inc.
*/
public class DocumentAndVersionInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<DocumentAndVersionInfo>
{
	private static final long serialVersionUID = 7011893147117606669L;

	private static final com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DocumentAndVersionInfo.class);

    // Library document is in
    protected Library library;
    
    // From Document
    protected EsfName esfname;
    protected String  description;
    protected String  status;
    protected int	  productionVersion;
    protected int	  testVersion;

	// From DocumentVersion
    protected EsfUUID id;
    protected EsfUUID documentId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    
    /**
     * This creates a new DocumentAndVersionInfo using the source objects.
     * @param library the library to use as the source
     * @param document the document to use as the source
     * @param documentVersion the document version to use as the source
     */
    protected DocumentAndVersionInfo(Library library, Document document, DocumentVersion documentVersion)
    {
    	this.library = library;
    	this.esfname = document.getEsfName();
    	this.description = document.getDescription();
    	this.status = document.getStatus();
    	this.productionVersion = document.getProductionVersion();
    	this.testVersion = document.getTestVersion();
        this.documentId = document.getId();
    	
        this.id = documentVersion.getId();
        this.version = documentVersion.getVersion();
        this.createdTimestamp = documentVersion.getCreatedTimestamp();
        this.createdByUserId = documentVersion.getCreatedByUserId();
        this.lastUpdatedTimestamp = documentVersion.getLastUpdatedTimestamp();
        this.lastUpdatedByUserId = documentVersion.getLastUpdatedByUserId();
    }
    
    /**
     * This creates a DocumentAndVersionInfo object from data retrieved from the DB.
     */
    protected DocumentAndVersionInfo(
    		Library library,
    		EsfName esfname, String description, String status, int productionVersion, int testVersion,
    		EsfUUID id, EsfUUID documentId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId)
    {
    	this.library = library;
    	this.esfname = esfname;
    	this.description = description;
    	this.status = status;
    	this.productionVersion = productionVersion;
    	this.testVersion = testVersion;
    	
        this.id = id;
        this.documentId = documentId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
    }
    
    public Library getLibrary()
    {
    	return library;
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }

    public int getProductionVersion()
    {
    	return productionVersion;
    }
    
    public int getTestVersion()
    {
    	return testVersion;
    }

    public boolean isProductionVersion()
    {
    	return version == productionVersion;
    }
    public boolean hasProductionVersion()
    {
    	return productionVersion > 0;
    }

    public boolean isTestVersion()
    {
    	return hasTestVersion() && version == testVersion;
    }
    public boolean hasTestVersion()
    {
    	return testVersion > productionVersion;
    }

    // "Current" here means latest production or test version.
    public boolean isCurrentVersion()
    {
    	return version >= productionVersion;
    }

    public boolean isOldVersion()
    {
    	return version < productionVersion;
    }

    public EsfUUID getId()
    {
        return id;
    }
    
    public EsfUUID getDocumentId()
    {
        return documentId;
    }
    
    public int getVersion()
    {
    	return version;
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }

    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }

    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }

    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }


    @Override
    public boolean equals(Object o)
    {
        if ( o instanceof DocumentAndVersionInfo )
            return getId().equals(((DocumentAndVersionInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(DocumentAndVersionInfo d)
    {
    	int nameDiff = getEsfName().compareTo(d.getEsfName());
    	return nameDiff == 0 ? (getVersion() - d.getVersion()) : nameDiff;
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all DocumentAndVersionInfo objects in the specified library.
   	     * @return the Collection of DocumentAndVersionInfo found ordered by esfname, with most recent versions first.
   	     */
   	    public static Collection<DocumentAndVersionInfo> getAll(Library library)
   	    {
   	    	LinkedList<DocumentAndVersionInfo> list = new LinkedList<DocumentAndVersionInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT d.esfname,d.description,d.status,d.production_version,d.test_version," +
   	    	            "v.id, v.library_document_id, v.version, v.created_timestamp, v.created_by_user_id, v.last_updated_timestamp, v.last_updated_by_user_id " +
   	    	            "FROM esf_library_document d, esf_library_document_version v " + 
   	    	            "WHERE d.library_id=? AND d.id = v.library_document_id " +
   	    	            "ORDER BY lower(d.esfname) ASC, v.version DESC"
   	            						   );
   	        	stmt.set(library.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfName esfname = rs.getEsfName();
		            String description = rs.getString();
		            String  stat = rs.getString();
		            int productionVersion = rs.getInt();
		            int testVersion = rs.getInt();
		            EsfUUID versionId = rs.getEsfUUID();
		            EsfUUID docId = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            
		            DocumentAndVersionInfo docVerInfo = new DocumentAndVersionInfo(library,esfname,description,stat,productionVersion,testVersion,
		            											versionId, docId, version,
		            		 									createdTimestamp, createdByUserId, lastUpdatedTimestamp, lastUpdatedByUserId
		            		 											);
		            docVerInfo.setLoadedFromDb();
		            list.add(docVerInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - Library: " + library.getPathName() + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - Library: " + library.getPathName() + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static DocumentAndVersionInfo createNew(Library library, User createdBy)
   	    {
   	    	if ( library == null || createdBy == null )
   	    		return null;
   	    	
   			Document document = Document.Manager.createNew(library.getId());
   			DocumentVersion documentVersion = DocumentVersion.Manager.createTest(document, createdBy);
   	    	return new DocumentAndVersionInfo(library,document,documentVersion);
   	    }
   	    
   	    public static DocumentAndVersionInfo createLike(Library library, Document likeDocument, DocumentVersion likeDocumentVersion, EsfName newName, User createdBy)
   	    {
   	    	if ( library == null || likeDocument == null || likeDocumentVersion == null || newName == null || ! newName.isValid() || createdBy == null )
   	    		return null;

   	    	Document newDocument = Document.Manager.createLike(likeDocument.getLibraryId(), likeDocument, newName);
   			DocumentVersion newDocumentVersion = DocumentVersion.Manager.createLike(newDocument, likeDocumentVersion, createdBy);
   	    	return new DocumentAndVersionInfo(library,newDocument,newDocumentVersion);
   	    }
   	    
   	    public static DocumentAndVersionInfo createNextVersion(Library library, Document document, DocumentVersion likeDocumentVersion, User createdBy)
   	    {
   	    	if ( library == null || document == null || document.hasTestVersion() || likeDocumentVersion == null || createdBy == null )
   	    		return null;

   	    	document.bumpTestVersion();
   			DocumentVersion newDocumentVersion = DocumentVersion.Manager.createLike(document, likeDocumentVersion, createdBy);
   	    	return new DocumentAndVersionInfo(library,document,newDocumentVersion);
   	    }
   	    
   	    public static DocumentAndVersionInfo createFromSource(Library library, Document document, DocumentVersion documentVersion)
   	    {
   	    	if ( library == null || document == null || documentVersion == null )
   	    		return null;
   	    	
   	    	DocumentAndVersionInfo docVerInfo = new DocumentAndVersionInfo(library,document,documentVersion);
   			docVerInfo.setLoadedFromDb();

   			return docVerInfo;
   	    }
   	    
   	} // Manager
   	
    
}