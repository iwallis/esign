// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;

import com.esignforms.open.Application;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.action.Action;
import com.esignforms.open.util.XmlUtil;

/**
* PackageProgramming holds custom programming logic associated with a package.
* 
* @author Yozons, Inc.
*/
public class PackageProgramming
	extends com.esignforms.open.db.DatabaseObject 
	implements java.io.Serializable
{
	private static final long serialVersionUID = -7751898339739379248L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackageProgramming.class);

    EsfUUID id;
    LinkedList<PackageProgrammingRule> rules; // rules are ordered as of 5/21/2012

   	protected PackageProgramming()
   	{
   		this(new EsfUUID());
   	}
   	protected PackageProgramming(EsfUUID id)
   	{
   		this.id = id;
   		rules = new LinkedList<PackageProgrammingRule>();
   	}
   	
   	public PackageProgramming(byte[] xmlBlob) throws EsfException {
   		String xml = EsfString.bytesToString(xmlBlob);
   		loadXml(xml);
   	}
   	
   	public PackageProgramming(Element packageProgrammingElement, EsfUUID packageProgrammingId) throws EsfException {
		loadFromJdom(packageProgrammingElement, "<fromJdomElement>PSEUDO XML</fromJdomElement>");
		if ( packageProgrammingId != null && ! packageProgrammingId.isNull() )
			id = packageProgrammingId;
   	}
   	
   	public EsfUUID getId()
   	{
   		return id;
   	}
   	
   	public List<PackageProgrammingRule> getRules()
   	{
   		return rules;
   	}
   	public List<PackageProgrammingRule> getDuplicateRules()
   	{
   		LinkedList<PackageProgrammingRule> dupRules = new LinkedList<PackageProgrammingRule>();
   		int order = 1;
   		for( PackageProgrammingRule rule : rules )
   		{
   			PackageProgrammingRule dupRule = rule.duplicate();
   			dupRule.setOrder(order++);
   			dupRules.add(dupRule);
   		}
   		return dupRules;
   	}
   	public void setRules(List<PackageProgrammingRule> rules)
   	{
   		this.rules = new LinkedList<PackageProgrammingRule>(rules);
   		objectChanged();
   	}
   	public void clearRules()
   	{
   		rules.clear();
   		objectChanged();
   	}
   	
    public PackageProgramming duplicate() 
    {
    	PackageProgramming programming = new PackageProgramming(id);
    	programming.setRules(rules);
    	programming.setDatabaseObjectLike(this);
    	return programming;
    }

   	
	protected void loadXml(String xml) throws EsfException 
	{
		StringReader sr = null;

		SAXBuilder saxBuilder = new SAXBuilder(XMLReaders.NONVALIDATING);
		saxBuilder.setIgnoringElementContentWhitespace(true);

		try 
		{
			sr = new StringReader(xml);

			Document doc = saxBuilder.build(sr);
			Element rootElement = doc.getRootElement();

			loadFromJdom(rootElement, xml);
		} 
		catch (EsfException e) 
		{
			throw e;
		} 
		catch (java.io.IOException e) 
		{
			_logger.error("loadXml() - could not read programming from the XML string: " + xml, e);
			throw new EsfException("Could not load the programming configuration.");
		} 
		catch (JDOMException e) 
		{
			_logger.error("loadXml() - could not XML parse programming from the XML string: " + xml, e);
			throw new EsfException("Could not XML parse and load the programming configuration.");
		} 
		finally 
		{
			if (sr != null)
			{
				try 
				{
					sr.close();
				} 
				catch (Exception e) {}
			}
		}
	}

	// Utility routine for the constructors to load the XML programming from the string
	// representation into this object
	protected void loadFromJdom(Element rootElement, String xml) throws EsfException 
	{
		Namespace ns = rootElement.getNamespace();

		try 
		{
			String rootName = rootElement.getName();
			if (!rootName.equals("PackageProgramming")) 
			{
				_logger.error("loadFromJdom(): Root element is not PackageProgramming.  Found instead: " + rootName + "; in XML: "+ xml);
				throw new EsfException("The PackageProgramming root tag is missing.");
			}

			id = EsfUUID.createFromToXml(rootElement.getChildText("id", ns));
			if (id.isNull()) 
			{
				_logger.error("loadFromJdom(): required id element is missing in XML: " + xml);
				throw new EsfException("The PackageProgramming.id element is missing.");
			}

			rules = new LinkedList<PackageProgrammingRule>();
			List<Element> rulesList = rootElement.getChildren("PackageProgrammingRule", ns);
			if (rulesList != null) 
			{
				ListIterator<Element> iter = rulesList.listIterator();
				int order = 1;
				while (iter.hasNext()) 
				{
					Element ruleElement = iter.next();
					PackageProgrammingRule rule = new PackageProgrammingRule(ruleElement,ns,_logger);
					rule.setOrder(order++);
					rules.add(rule);
				}
			} 
			else 
			{
				_logger.warn("loadFromJdom(): No PackageProgramming.PackageProgrammingRule elements were found in XML: " + xml);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
	}

   	// Very similar to the load above but we don't reset the ID and map other IDs from the import XML to our new system's IDs
	public void importFromJDOM(Element rootElement, Map<EsfUUID,EsfUUID> idMap)
	{
		Namespace ns = rootElement.getNamespace();

		String rootName = rootElement.getName();
		if (!rootName.equals("PackageProgramming")) 
		{
			_logger.error("importFromJDOM(): Root element is not PackageProgramming.  Found instead: " + rootName);
			return;
		}

		rules = new LinkedList<PackageProgrammingRule>();
		List<Element> rulesList = rootElement.getChildren("PackageProgrammingRule", ns);
		if (rulesList != null) 
		{
			ListIterator<Element> iter = rulesList.listIterator();
			int order = 1;
			while (iter.hasNext()) 
			{
				Element ruleElement = iter.next();
				try 
				{
					PackageProgrammingRule rule = new PackageProgrammingRule(ruleElement,ns,_logger);
					rule._updateDocumentAndPartyIds(idMap);
					rule.setOrder(order++);
					rules.add(rule);
				}
				catch( EsfException e )
				{
					_logger.error("importFromJDOM() - Failed to create the PackageProgrammingRule",e);
				}
			}
		} 
		else 
		{
			_logger.warn("importFromJDOM(): No PackageProgramming.PackageProgrammingRule elements were found.");
		}
	}

   	
   	public StringBuilder appendXml(StringBuilder buf)
   	{
   		buf.append("<PackageProgramming xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
		buf.append(" <id>").append(id.toXml()).append("</id>\n");
		
		for( PackageProgrammingRule rule : rules )
			rule.appendXml(buf);
   		
   		buf.append("</PackageProgramming>\n");
   		return buf;
   	}
   	
	public int getEstimatedLengthXml() {
		int size = 200;
		for( PackageProgrammingRule rule : rules )
			size += rule.getEstimatedLengthXml();
		return size;
	}

   	public String toXml()
   	{
   		StringBuilder buf = new StringBuilder(getEstimatedLengthXml());
   		return appendXml(buf).toString();
   	}
	public byte[] toXmlByteArray() {
		String xml = toXml();
		return EsfString.stringToBytes(xml);
	}
	
	/*
	 * =========================== BEGIN DATABASE RELATED ROUTINES =============================
	 */
	
	public boolean save(Connection con) throws SQLException 
	{
		try 
		{
			if ( doInsert() ) 
			{
				byte[] xmlBlob = toXmlByteArray();

				getBlobDb().insert(con, id, xmlBlob, BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE);

				// Now we mark this object as if it were loaded fresh from the database
				setLoadedFromDb();
				return true;
			}

			// This must be an update request. But if it hasn't changed, we can
			// just treat this as a null operation

			if ( ! hasChanged() )
				return true;

			byte[] xmlBlob = toXmlByteArray();

			getBlobDb().update(con, id, xmlBlob, BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE);

			// Now we mark this object as if it were loaded fresh from the database
			setLoadedFromDb();
			return true;
		} 
		catch (SQLException e) 
		{
			_logger.sqlerr(e, "save(con) doInsert: " + doInsert() + "; id: " + id);
			setLastSQLException(e);
			throw e;
		}
	}

	public boolean save() 
	{
		_logger.debug("save() doInsert: " + doInsert() + "; id: " + id);

		ConnectionPool pool = getConnectionPool();
		Connection con = pool.getConnection();
		try 
		{
			boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
			return result;
		} 
		catch (SQLException e) 
		{
			pool.rollbackIgnoreException(con, e);
			return false;
		} 
		finally 
		{
			cleanupPool(pool, con, null);
		}
	}

	public boolean delete(Connection con) throws SQLException 
	{
		try 
		{
			getBlobDb().delete(con, id);
			objectDeleted();
			return true;
		} 
		catch (SQLException e) 
		{
			_logger.sqlerr(e, "delete(con) - id: " + id);
			setLastSQLException(e);
			throw e;
		}
	}

	public boolean delete() 
	{
		_logger.debug("delete() - id: " + id);

		ConnectionPool pool = getConnectionPool();
		Connection con = pool.getConnection();
		try 
		{
			boolean result = delete(con);
			con.commit();
			return result;
		} 
		catch (SQLException e) 
		{
			pool.rollbackIgnoreException(con, e);
			return false;
		} 
		finally 
		{
			cleanupPool(pool, con, null);
		}
	}

	public static class Manager	
	{		
		public static PackageProgramming createFromJdom(Element rootElement)
		{
			PackageProgramming programming = new PackageProgramming();
			try
			{
				programming.loadFromJdom(rootElement, "");
			}
			catch( Exception e )
			{
				return null;
			}
			return programming;
		}
		
		public static PackageProgramming createLike(PackageProgramming likePackageProgramming, TreeMap<EsfUUID,EsfUUID> partyIdMapping)
		{
			PackageProgramming programming = new PackageProgramming();
			LinkedList<PackageProgrammingRule> newRules = new LinkedList<PackageProgrammingRule>();
			int order = 1;
			for( PackageProgrammingRule dupRule : likePackageProgramming.getDuplicateRules() )
			{
				HashSet<EsfUUID> newPartyIds = new HashSet<EsfUUID>();
				for( EsfUUID origPartyId : dupRule.getOnPartyIds() )
				{
					EsfUUID mappedToPartyId = partyIdMapping.get(origPartyId);
					if (mappedToPartyId != null)
						newPartyIds.add(mappedToPartyId);
					dupRule.setOnPartyIds(newPartyIds);
				}
				for( Action action : dupRule.getActions() )
					action.updatePackagePartyIds(partyIdMapping);
				dupRule.setOrder(order++);
				newRules.add(dupRule);
			}
			programming.setRules(newRules);
			return programming;
		}
		
		
		public static PackageProgramming getById(Connection con, EsfUUID id) throws SQLException 
		{
			try 
			{
				byte[] xmlBlob;
				xmlBlob = Application.getInstance().getBlobDb().select(con, id);
				if (xmlBlob == null) 
				{
					_logger.error("Manager.getById(con) id: " + id + "; found no XML programming blob");
					return null;
				}

				PackageProgramming programming = new PackageProgramming(xmlBlob);

				if ( ! id.equals(programming.getId()) ) 
				{
					_logger.error("Manager.getById(con) id: " + id + "; loaded XML with MISMATCHED embedded id: " + programming.getId());
					return null;
				}

				// Now we mark this object as if it were loaded fresh from the database
				programming.setLoadedFromDb();

				return programming;
			} 
			catch (EsfException e) 
			{
				_logger.error("Manager.getById(con) id: " + id + "; could not load XML programming",e);
				return null;
			}
		}

		public static PackageProgramming getById(EsfUUID id) 
		{
			ConnectionPool pool = getConnectionPool();
			Connection con = pool.getConnection();
			try 
			{
				PackageProgramming p = getById(con, id);
				con.commit();
				return p;
			} 
			catch (SQLException e) 
			{
				_logger.sqlerr(e, "Manager.getById() id: " + id);
				pool.rollbackIgnoreException(con, e);
				return null;
			} 
			finally 
			{
				cleanupPool(pool, con, null);
			}
		}
	}

}