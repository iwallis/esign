// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;

import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.data.NameValue;
import com.esignforms.open.data.Record;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.user.User;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* PropertySetVersion holds the definition of a specific version of a property set held in a container.
* A property set has one or more name-value pairs.
* 
* @author Yozons, Inc.
*/
public class PropertySetVersion
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<PropertySetVersion>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable
{
	private static final long serialVersionUID = 4816067687415431795L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PropertySetVersion.class);
    
    protected final EsfUUID id;
    protected final EsfUUID propertySetId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected Record properties;
    
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();

    /**
     * This creates a PropertySetVersion object from data retrieved from the DB.
     */
    protected PropertySetVersion(EsfUUID id, EsfUUID propertySetId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		Record properties)
    {
        this.id = id;
        this.propertySetId = propertySetId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.properties = properties;
    }
    
    protected PropertySetVersion(PropertySet propertySet, User createdByUser)
    {
        this.id = new EsfUUID();
        this.propertySetId = propertySet.getId();
        this.version = propertySet.getProductionVersion() + 1;
        this.createdTimestamp = new EsfDateTime();
        this.createdByUserId = createdByUser.getId();
        this.lastUpdatedTimestamp = createdTimestamp;
        this.lastUpdatedByUserId = createdByUserId;
        this.properties = new Record(new EsfName("props"),BlobDb.CompressOption.ENABLE,BlobDb.EncryptOption.ENABLE); // encrypt since often have passwords and such stored here
    }

   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getPropertySetId()
    {
        return propertySetId;
    }
    
    public PropertySet getPropertySet()
    {
    	return PropertySet.Manager.getById(propertySetId);
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}

	public int getVersion()
    {
    	return version;
    }
    
	public String getVersionLabel()
	{
		return getPropertySet().getVersionLabel(version);
	}
	public String getPropertySetNameVersion()
	{
		return getPropertySet().getEsfName() + " [" + version + "]";
	}
	public String getPropertySetNameVersionWithLabel()
	{
		PropertySet propertyset = getPropertySet();
		return propertyset.getEsfName() + " [" + version + "] (" + propertyset.getVersionLabel(version) + ")";
	}
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }
    
    public Record getProperties()
    {
    	return new Record(properties);
    }
    public int getNumProperties()
    {
    	return properties.getSize();
    }
    public void setProperties(Record v)
    {
    	properties.removeAll();
    	
    	for( NameValue nv : v.getSortedNameValues() )
    		properties.addUpdate(nv);
    	objectChanged();
    }

    public EsfValue getPropertyValue(EsfName esfname)
    {
    	return properties.getValueByName(esfname);
    }

    public EsfValue getPropertyValue(EsfPathName pathName)
    {
    	return properties.getValueByName(pathName);
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof PropertySetVersion )
        {
        	PropertySetVersion otherPropertySet = (PropertySetVersion)o;
            return getId().equals(otherPropertySet.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(PropertySetVersion o)
    {
    	if ( getPropertySetId().equals(o.getPropertySetId()) )
    		return getVersion() - o.getVersion();
    	return getPropertySetId().compareTo(o.getPropertySetId());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<PropertySetVersion xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <propertySetId>").append(propertySetId.toXml()).append("</propertySetId>\n");
        buf.append(" <version>").append(version).append("</version>\n");
        buf.append(" <createdTimestamp>").append(createdTimestamp.toXml()).append("</createdTimestamp>\n");
        buf.append(" <createdByUserId>").append(createdByUserId.toXml()).append("</createdByUserId>\n");
        buf.append(" <lastUpdatedTimestamp>").append(lastUpdatedTimestamp.toXml()).append("</lastUpdatedTimestamp>\n");
        buf.append(" <lastUpdatedByUserId>").append(lastUpdatedByUserId.toXml()).append("</lastUpdatedByUserId>\n");
        buf.append(" <properties>");
        properties.appendXml(buf);
        buf.append("</properties>\n");

        buf.append("</PropertySetVersion>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 316 + id.getEstimatedLengthXml() + propertySetId.getEstimatedLengthXml() + createdTimestamp.getEstimatedLengthXml() + createdByUserId.getEstimatedLengthXml() + 
        	lastUpdatedTimestamp.getEstimatedLengthXml() + lastUpdatedByUserId.getEstimatedLengthXml();

        len += properties.getEstimatedLengthXml();

        return len; 
    }

    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; propertySetId: " + propertySetId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
            	
            	properties.save(con);
                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_propertyset_version (id,library_propertyset_id,version,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,num_properties,properties_blob_id) VALUES (?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(propertySetId);
                stmt.set(version);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(getNumProperties());
                stmt.set(properties.getId());
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; propertySetId: " + propertySetId + "; version: " + version);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this);
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new property set version id: " + id + "; propertySetId: " + propertySetId + "; version: " + version); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new property set version id: " + id + "; propertySetId: " + propertySetId + "; version: " + version);
                }
                
                return true;
            }
            
        	if ( properties.hasChanged() )
        		properties.save(con);
            	
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_propertyset_version SET last_updated_timestamp=?,last_updated_by_user_id=?,num_properties=? WHERE id=?"
                						   		);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(getNumProperties());
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for propertyset version id: " + id + "; propertySetId: " + propertySetId + "; version: " + version);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated property set version id: " + id + "; propertySetId: " + propertySetId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated property set version id: " + id + "; propertySetId: " + propertySetId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; propertySetId: " + propertySetId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; propertySetId: " + propertySetId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use in the transaction definition.
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of property set version that was pending an INSERT id: " + id + "; propertySetId: " + propertySetId + "; version: " + version);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_propertyset_version WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            properties.delete(con);
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted property set version id: " + id + "; propertySetId: " + propertySetId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted property set version id: " + id + "; propertySetId: " + propertySetId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on id: " + id + "; propertySetId: " + propertySetId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   		private static UUIDCacheReadOptimized<PropertySetVersion> cache = new UUIDCacheReadOptimized<PropertySetVersion>();
   		
   		static final PropertySetVersion getById(Connection con, EsfUUID propertySetVersionId) throws SQLException
   		{
   			return getById(con,propertySetVersionId,true);
   		}
   		static PropertySetVersion getById(Connection con, EsfUUID propertySetVersionId, boolean checkCache) throws SQLException
   		{
   			if ( checkCache )
   			{
   				PropertySetVersion propertySetVer = cache.getById(propertySetVersionId);
   	   			if ( propertySetVer != null )
   	   				return propertySetVer;
   			}

   			EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT library_propertyset_id,version,created_by_user_id,last_updated_by_user_id,created_timestamp,last_updated_timestamp,properties_blob_id " +
   	        			"FROM esf_library_propertyset_version WHERE id = ?"
   	        									);
   	        	stmt.set(propertySetVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID libraryPropertySetId = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID propId = rs.getEsfUUID();
		            
		            Record properties = Record.Manager.getById(con,propId);
		            
		            PropertySetVersion propertySetVer = new PropertySetVersion(propertySetVersionId,libraryPropertySetId,version,
		            											 createdTimestamp,createdByUserId,lastUpdatedTimestamp,lastUpdatedByUserId,
		            											 properties);
		            propertySetVer.setLoadedFromDb();
		            cache.add(propertySetVer);
		            
	   	            return propertySetVer;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + propertySetVersionId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + propertySetVersionId + "; failed to find the property set version");
   	        return null; 
  		}
   		
   		public static PropertySetVersion getById(EsfUUID propertySetVersionId)
   		{
   			PropertySetVersion propertySetVer = cache.getById(propertySetVersionId);
   			if ( propertySetVer != null )
   				return propertySetVer;
   	   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	propertySetVer = getById(con,propertySetVersionId,false);
   	        	if ( propertySetVer != null ) 
   	        	{
		            con.commit();
	   	        	return propertySetVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		static PropertySetVersion getByVersion(Connection con, EsfUUID libraryPropertySetId, int version) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_propertyset_version WHERE library_propertyset_id=? AND version=?" );
   	        	stmt.set(libraryPropertySetId);
   	        	stmt.set(version);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	       			return getById(con,id);
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByVersion() - libraryPropertySetId: " + libraryPropertySetId + "; version: " + version);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getByVersion() - libraryPropertySetId: " + libraryPropertySetId + "; version: " + version + "; failed to find the propertyset version");
   	        return null; 
  		}
   		
   		public static PropertySetVersion getByVersion(EsfUUID libraryPropertySetId, int version)
   		{
   			// While not always true, if the version is 1, it's possible this is a new object that's not been saved yet so we can't find the id in the DB, so we'll check our cache first just in case
   			if ( version == 1 )
   			{
   				for( EsfUUID id : cache.getAllIds() )
   				{
   					PropertySetVersion propertySetVer = cache.getById(id);
   					if ( propertySetVer.getPropertySetId().equals(libraryPropertySetId) && propertySetVer.getVersion() == version )
   						return propertySetVer;
   				}
   			}
   			
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	PropertySetVersion propertySetVer = getByVersion(con,libraryPropertySetId,version);
   	        	if ( propertySetVer != null ) 
   	        	{
		            con.commit();
	   	        	return propertySetVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		
   		public static List<PropertySetVersion> getAllByPropertySetId(Connection con, EsfUUID libraryPropertySetId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<PropertySetVersion> list = new LinkedList<PropertySetVersion>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_propertyset_version WHERE library_propertyset_id=?" );
   	        	stmt.set(libraryPropertySetId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID propertySetVersionId = rs.getEsfUUID();
	            	
	            	PropertySetVersion propertySetVer = getById(con,propertySetVersionId);
	       			if ( propertySetVer != null )
	       				list.add(propertySetVer);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<PropertySetVersion> getAllByPropertySetId(EsfUUID libraryPropertySetId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<PropertySetVersion> list = getAllByPropertySetId(con,libraryPropertySetId);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return new LinkedList<PropertySetVersion>(); 
   		}
   	    
   		
   	    
   	    public static PropertySetVersion createTest(PropertySet propertySet, User createdBy)
   	    {
   	    	if ( propertySet == null || createdBy == null )
   	    		return null;
   	    	PropertySetVersion newPropertySetVersion = new PropertySetVersion(propertySet,createdBy);
   	    	cache.add(newPropertySetVersion);
   	    	return newPropertySetVersion;
   	    }

   	    public static PropertySetVersion createLike(PropertySet propertySet, PropertySetVersion likePropertySetVersion, User createdBy)
   	    {
   	    	if ( propertySet == null || likePropertySetVersion == null || createdBy == null )
   	    		return null;
   	    	PropertySetVersion newPropertySetVersion = createTest(propertySet,createdBy);
   	    	for( NameValue nv : likePropertySetVersion.properties.getSortedNameValues() )
   	    		newPropertySetVersion.properties.addUpdate(nv);
   	    	return newPropertySetVersion;
   	    }

   	    public static PropertySetVersion createFromJDOM(PropertySet propertySet, Element e)
   	    {
   	    	if ( propertySet == null || e == null )
   	    		return null;
   	    	
   	    	EsfUUID id = new EsfUUID();
   	    	
			Element propertiesElement = e.getChild("properties", e.getNamespace());
			Element recordElement = propertiesElement.getChild("esfRecord", Namespace.getNamespace(XmlUtil.getXmlNamespace2009()));
			
			Record properties = null;
			try
			{
				properties = new Record(new EsfUUID(), recordElement);
			}
			catch( EsfException ex )
			{
				_logger.error("createFromJDOM() failed on properties/esfRecord for property set: " + propertySet.getEsfName(), ex);
				properties = new Record(new EsfName("props"),BlobDb.CompressOption.ENABLE,BlobDb.EncryptOption.ENABLE); // bad, but empty is better than nothing?!
			}
			
   	    	PropertySetVersion newPropertySetVersion = new PropertySetVersion(
   	    			id, 
   	    			propertySet.getId(), 
   	    			propertySet.getTestVersion(),
   	    			null,
   	    			null,
   	    			null,
   	    			null,
   	    			properties
   	    			);
   	    	newPropertySetVersion.setNewObject();
   	    	cache.add(newPropertySetVersion);
   	    	return newPropertySetVersion;
   	    }
   	    
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all property set versions that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}

   	} // Manager
   	
}