// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.Permission;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.util.PathNameUUIDUserListableCacheReadOptimized;

/**
* Library holds a single Open eSignForms library (of documents, HTML, CSS, images, files), along with the permissions to manage the Library itself.
* 
* The libraries are all cached permanently.
* 
* @author Yozons, Inc.
*/
public class Library
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<Library>, PathNameUUIDUserListableCacheReadOptimized.PathNameUUIDUserListableCacheable, java.io.Serializable
{
 	private static final long serialVersionUID = -3104011940777277526L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(Library.class);

    protected final EsfUUID id;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected EsfPathName pathName;
    protected EsfPathName originalPathName; // derived, not persisted
    protected String  description;
    protected String  comments;
    protected String  status;
    protected EsfUUID defaultDocumentStyleId;
    protected Permission permission; // permission grants related to this library object (create like, delete, list, update, view details of this library object)
    
    public static final String PERM_PATH_PREFIX = "LibraryPerm" + EsfPathName.PATH_SEPARATOR;  // actual name is this prefix followed library's id
    
    // Initial library created for cloning from, etc.
    public static final EsfPathName TEMPLATE_ESFPATHNAME	= new EsfPathName(EsfPathName.ESF_RESERVED_LIBRARY_PATH_PREFIX+"Template");
    public static final String TEST_PROPERTY_SUFFIX			= "_TEST";
    public static final int MAX_PROPERTY_NAME_LENGTH_WITHOUT_TEST_SUFFIX = Literals.ESFNAME_MAX_LENGTH - TEST_PROPERTY_SUFFIX.length(); // 50 - 5 = 45
    
    public enum INCLUDE { ONLY_ENABLED, ONLY_DISABLED, BOTH_ENABLED_AND_DISABLED };
    
    /**
     * This creates a Library object from data retrieved from the DB.
     */
    protected Library(EsfUUID id, EsfPathName pathName, String description, 
    		        EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		        EsfUUID defaultDocumentStyleId, String status, String comments, Permission permission)
    {
        this.id = id;
        this.pathName = pathName;
        this.originalPathName = pathName.duplicate();
        this.description = description;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.defaultDocumentStyleId = defaultDocumentStyleId;
        this.status = status;
        this.comments = comments;
        this.permission = permission;
    }
    
   
    public EsfUUID getId()
    {
        return id;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }
    
    public void makeEnabled()
    {
        status = Literals.STATUS_ENABLED;
        objectChanged();
    }
     
    public void makeDisabled()
    {
        status = Literals.STATUS_DISABLED;
        objectChanged();
    }
    
    public void setStatus(String s)
    {
        if ( Literals.STATUS_ENABLED.equals(s) )
            makeEnabled();
        else
            makeDisabled();
    }
    
    public EsfPathName getPathName()
    {
        return pathName;
    }
    public void setPathName(EsfPathName v)
    {
    	// Block setting a library name to null, an invalid EsfPathName or if the library current is a reserved name or the new name would be reserved
    	if ( v == null || ! v.isValid() || v.hasReservedPathPrefix() || (pathName != null && pathName.hasReservedPathPrefix()) )
    		return;
        pathName = v;
        objectChanged();
    }
    public EsfPathName getOriginalPathName()
    {
        return originalPathName;
    }
    public void resetOriginalPathName()
    {
    	originalPathName = pathName.duplicate();
    }
    
    public boolean isReserved()
    {
    	return getPathName().hasReservedPathPrefix();
    }
    
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String v)
    {
    	if ( v == null )
    		description = v;
    	else if ( v.length() > Literals.DESCRIPTION_MAX_LENGTH )
            description = v.substring(0,Literals.DESCRIPTION_MAX_LENGTH).trim();
        else
        	description = v.trim();
        objectChanged();
    }
    
    public String getComments()
    {
    	return comments;
    }
    public void setComments(String v)
    {
    	if ( v == null )
    		comments = v;
    	else if ( v.length() > Literals.COMMENTS_MAX_LENGTH )
    		comments = v.substring(0,Literals.COMMENTS_MAX_LENGTH).trim();
        else
        	comments = v.trim();
        objectChanged();
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
        return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
        return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
        return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
        return lastUpdatedByUserId;
    }
    
    public EsfUUID getDefaultDocumentStyleId()
    {
    	return defaultDocumentStyleId;
    }
    public void setDefaultDocumentStyleId(EsfUUID v)
    {
    	defaultDocumentStyleId = v;
    	objectChanged();
    }
    
    public DocumentStyle getDefaultDocumentStyle()
    {
    	return DocumentStyle.Manager.getById(defaultDocumentStyleId);
    }
        
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof Library )
            return getId().equals(((Library)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(Library l)
    {
    	return getPathName().compareTo(l.getPathName());
    }
    
    @Override
    public boolean canUserList(User user)
    {
		return permission.canUserList(user);
    }
    
    public final boolean hasPermission(User user, EsfName permOptionName)
    {
    	return permission.hasPermission(user, permOptionName);
    }
    
    public final Collection<Group> getPermissionAllowedGroups(User user, EsfName permOptionName)
    {
    	return permission.getAllowedGroups(user, permOptionName);
    }
    public final boolean addPermissionAllowedGroups(EsfName permOptionName, Group group)
    {
    	return permission.addGroupToOptions(group, permOptionName) > 0;
    }
    public final boolean removePermissionAllowedGroups(EsfName permOptionName, Group group)
    {
    	return permission.removeGroupFromOptions(group, permOptionName) > 0;
    }
    
    
	/**
	 * Finds the specified property name in the specified propertyset, if provided, in this library.
	 * @param propertySetName the name of the property set to look in; if null, all property sets are checked in alpha order
	 * @param propertyName the property name
	 * @param doProductionResolve - resolve property sets for production or testing
	 * @param doCheckForTestProperty - if true, look for propertyName_TEST property before propertyName.
	 * @return the EsfValue of the property
	 */
	public EsfValue getPropertyValue(EsfName propertySetName, EsfPathName propertyName, boolean doProductionResolve, boolean doCheckForTestPropertyFirst)
	{
		if ( propertySetName == null )
		{
			for( PropertySetInfo psInfo : PropertySetInfo.Manager.getAll(getId()) )
			{
				PropertySet propertySet = PropertySet.Manager.getById(psInfo.getId());
				if ( propertySet != null && propertySet.isEnabled() )
				{
					PropertySetVersion propertySetVersion = doProductionResolve ? propertySet.getProductionPropertySetVersion() : propertySet.getTestPropertySetVersion();
					if ( propertySetVersion != null )
					{
						EsfValue value = null;
						// If we doing a test, we'll see if we can find a PropName_TEST version first and use it if we do
						if ( doCheckForTestPropertyFirst && propertyName.getLength() < MAX_PROPERTY_NAME_LENGTH_WITHOUT_TEST_SUFFIX )
							value = propertySetVersion.getPropertyValue( new EsfPathName(propertyName.toString()+TEST_PROPERTY_SUFFIX) );
						if ( value == null )
							value = propertySetVersion.getPropertyValue(propertyName);
						return value;
					}
				}
			}
		}
		else
		{
			PropertySet propertySet = PropertySet.Manager.getByName(getId(), propertySetName);
			if ( propertySet != null && propertySet.isEnabled() )
			{
				PropertySetVersion propertySetVersion = doProductionResolve ? propertySet.getProductionPropertySetVersion() : propertySet.getTestPropertySetVersion();
				if ( propertySetVersion != null )
				{
					EsfValue value = null;
					// If we doing a test, we'll see if we can find a PropName_TEST version first and use it if we do
					if ( doCheckForTestPropertyFirst && propertyName.getLength() < MAX_PROPERTY_NAME_LENGTH_WITHOUT_TEST_SUFFIX )
						value = propertySetVersion.getPropertyValue( new EsfPathName(propertyName.toString()+TEST_PROPERTY_SUFFIX) );
					if ( value == null )
						value = propertySetVersion.getPropertyValue(propertyName);
					return value;
				}
			}
		}
		
		return null;
	}
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; pathName: " + pathName + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
            	
            	permission.save(con);
                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library (id,path_name,description,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,library_documentstyle_id,status,comments) VALUES(?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(pathName);
                stmt.set(description);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(defaultDocumentStyleId);
                stmt.set(status);
                stmt.set(comments);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; pathName: " + pathName);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.add(this);
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new library " + getPathName()); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new library " + getPathName());
                }
                
                return true;
            }
            
            if ( permission.hasChanged() )
            	permission.save(con);

            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library SET path_name=?,description=?,status=?,comments=?,last_updated_timestamp=?,last_updated_by_user_id=?,library_documentstyle_id=? WHERE id=?"
                						   );
                stmt.set(pathName);
                stmt.set(description);
                stmt.set(status);
                stmt.set(comments);
            	stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(defaultDocumentStyleId);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for library id: " + id + "; pathName: " + pathName);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated library " + getPathName() + "; status; " + getStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated library " + getPathName() + "; status; " + getStatus());
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; pathName: " + pathName + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    private synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; pathName: " + pathName + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        //TODO: If we allow library deletes, we'll need to chain to delete it's components. May be too much to ask a person to delete all the various
        // things that can be defined in a library.  Instead, if the library is not referenced by any live transactions or package/tran templates, let
        // it be deleted and force delete all other objects contained inside.

        // Let's do some referential integrity checks to be sure it's not in use in the transaction definition.
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of library that was pending an INSERT id: " + id + "; pathName: " + pathName);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
            permission.delete(con);
            
            // Delete the library
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library WHERE id=?");
            stmt.set(id);
            int num = stmt.executeUpdate();
            if ( num == 0 )
            	_logger.warn("delete(con) - Failed to find esf_library database row to delete with id: " + id);
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted library " + getPathName() + "; status; " + getStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted library " + getPathName() + "; status; " + getStatus());
            }

            return true;
        }
        catch(SQLException e)
        {
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"delete() on id: " + id + "; pathName: " + pathName + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
		// We never allow the reserved libraries to be deleted
		if ( isReserved() )
		{
	    	_logger.error("checkReferential() FAILED RESERVE PATH PREFIX on id: " + id + "; pathName: " + pathName);
			errors.addError("You cannot delete the reserved special library: " + getPathName());
			return false;
		}

	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	    	/*
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT T.path_name FROM esf_transaction_template T,esf_transaction_template_searchlibrary SL WHERE SL.search_library_id=? AND SL.transaction_template_id = T.id"
	                                   );
	        stmt.set(id);
	        EsfResultSet rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	do
	        	{
		        	EsfPathName pathName = rs.getEsfPathName();
		        	if ( errors != null )
		        		errors.addError("The library is still referenced in the transaction template " + pathName + ".");
	        	} while ( rs.next() );
	        	 
	        	return false;
	        }
	        
	        stmt.close();
	        */
	    	
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT esfname FROM esf_field_template WHERE container_id=?"
	                                   );
	        stmt.set(id);
	        EsfResultSet rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	do
	        	{
		        	EsfName esfname = rs.getEsfName();
		        	if ( errors != null )
		        		errors.addError("The library still contains the field template '" + esfname + "'.");
	        	} while ( rs.next() );
	        	 
	        	return false;
	        }
	        
	        stmt.close();
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT esfname FROM esf_party_template WHERE container_id=?"
	                                   );
	        stmt.set(id);
	        rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	do
	        	{
		        	EsfName esfname = rs.getEsfName();
		        	if ( errors != null )
		        		errors.addError("The library still contains the party template '" + esfname + "'.");
	        	} while ( rs.next() );
	        	 
	        	return false;
	        }
	        
	        stmt.close();
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT esfname FROM esf_library_document WHERE library_id=?"
	                                   );
	        stmt.set(id);
	        rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	do
	        	{
		        	EsfName esfname = rs.getEsfName();
		        	if ( errors != null )
		        		errors.addError("The library still contains the document '" + esfname + "'.");
	        	} while ( rs.next() );
	        	 
	        	return false;
	        }

	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   	    private static PathNameUUIDUserListableCacheReadOptimized<Library> cache;
   	    
   	    static 
   	    {
   	    	loadCache();
   	    }
   	    
   	    /**
   	     * Loads the Library Cache
   	     */
   	    private static void loadCache()
   	    {
   	    	_logger.debug("Manager.loadCache()");

   	    	cache = new PathNameUUIDUserListableCacheReadOptimized<Library>();
   	        
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	            stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id,path_name,description,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,library_documentstyle_id,status,comments " +
   	    	            "FROM esf_library"
   	            						   );
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            EsfPathName pathName = rs.getEsfPathName();
		            String description = rs.getString();
		            EsfDateTime created = rs.getEsfDateTime();
		            EsfUUID createdBy   = rs.getEsfUUID();
		            EsfDateTime lastUpdated = rs.getEsfDateTime();
		            EsfUUID lastUpdatedBy   = rs.getEsfUUID();
		            EsfUUID defaultDocumentStyleId = rs.getEsfUUID();
		            String stat = rs.getString();
		            String comments = rs.getString();
		            
		        	Permission permission = Permission.Manager.getById(id); // the library id is also the library's permission id
		            
		            Library library = new Library(id,pathName,description,created,createdBy,lastUpdated,lastUpdatedBy,defaultDocumentStyleId,stat,comments,permission);
		            
		            library.setLoadedFromDb();
		            cache._addDuringInitializationOnly(library);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.loadCache() size: " + cache.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.loadCache() - clearing " + cache.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	cache.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }
   	    }


   	    /**
   	     * Creates a new library like an existing library.  The new library is automatically added to all of
   	     * the permission lists (it can always list/view/add/change/delete itself unless changed by the creator or others with permission).
   	     * @param likeLibrary the Library that we'll be cloning from
   	     * @param esfname the new library's name
   	     * @param createdBy the User who is creating the new library
   	     * @return
   	     */
   	    public static Library createLike(Library likeLibrary, EsfPathName pathName, User createdBy)
   	    {
   	    	// We don't let you create new reserved libraries
   	    	if ( pathName.hasReservedPathPrefix() )
   	    		return null;

   	    	if ( ! likeLibrary.permission.hasPermission(createdBy, PermissionOption.PERM_OPTION_CREATELIKE) )
   	    		return null;
   	    	
   	        EsfUUID id = new EsfUUID();
   	        EsfDateTime nowTimestamp = new EsfDateTime();
   	        
   	        String normalizedId = id.toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+normalizedId);
   	        Permission newPermission = Permission.Manager.createLike(id, permPathName, likeLibrary.permission);

   	        // Note that the library's permissions can contain groups that even the creator doesn't have access to, but
   	        // they remain in the new library (so they can have permission) and the user can't even see them so 
   	        // cannot modify them away.  Also, the default document style for a new library is template's.
   	        // If we ever allow a new library to be created that contains all of the contents of like library, we can fix this up.
   	    	Library library = new Library( id, pathName, likeLibrary.getDescription(), 
   	    							 	   nowTimestamp, createdBy.getId(), nowTimestamp, createdBy.getId(),
   	    							 	   Library.Manager.getTemplate().getDefaultDocumentStyleId(),likeLibrary.getStatus(), likeLibrary.getComments(), newPermission
   	    						   		 );
   	    	
   	    	return library;
   	    }
   	    
   	    // Only called by DbSetup
   	    public static Library _dbsetup_createTemplate(Group superGroup, Group systemAdminGroup)
   	    {
   	        EsfDateTime nowTimestamp = new EsfDateTime();

   	        EsfUUID deployId = Application.getInstance().getDeployId();
   	        
   	    	Library library = new Library( new EsfUUID(), TEMPLATE_ESFPATHNAME, "Template library.", nowTimestamp, deployId, 
   	    							       nowTimestamp, deployId, deployId, Literals.STATUS_ENABLED, null, null );

   	    	String normalizedId = library.getId().toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+normalizedId);
   	    	library.permission = Permission.Manager._dbsetup_createForSuperGroup(library.getId(), permPathName, "Template library permissions", superGroup);
   	    	library.permission.addGroupToOptions(systemAdminGroup, PermissionOption.PERM_OPTION_LIST, PermissionOption.PERM_OPTION_VIEWDETAILS);
   	    	return library;
   	    }
   	    // Only called by DbSetup
   	    public static Library _dbsetup_createSample(Group superGroup, Group systemAdminGroup, EsfPathName libPathName)
   	    {
   	        EsfDateTime nowTimestamp = new EsfDateTime();

   	        EsfUUID deployId = Application.getInstance().getDeployId();

   	        Library library = new Library( new EsfUUID(), libPathName, "Initial library for your own use.", nowTimestamp, deployId, 
   	    							       nowTimestamp, deployId, deployId, Literals.STATUS_ENABLED, null, null );

   	    	String normalizedId = library.getId().toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+normalizedId);
   	    	library.permission = Permission.Manager._dbsetup_createForSuperGroup(library.getId(), permPathName, "Initial library permissions", superGroup);
   	    	library.permission.addGroupToAllOptions(systemAdminGroup);  // As the base library to create like from, the system admin has full control.
   	    	return library;
   	    }

   	    /**
   	     * Returns the Template library 
   	     * @return the Library that represents the Template
   	     */
   	    public static Library getTemplate()
   	    {
   	    	return getByPathName(TEMPLATE_ESFPATHNAME);
   	    }
   	     
   	    /**
   	     * 
   	     * Retrieves a Library object based on the supplied path name.
   	     * @param pathName the EsfPathName name of the library to retrieve
   	     * @return the Library object as loaded from the database if found; else null
   	     */
   	    public static Library getByPathName(final EsfPathName pathName)
   	    {
   	    	Library l = cache.getByPathName(pathName);
   	    	if ( l == null )
   	    		_logger.debug("Manager.getByPathName() could not find for pathName: " + pathName);
   	        return l;
   	    }
   	    
   	    /**
   	     * 
   	     * Retrieves a Library object based on the supplied pathName.
   	     * @param esfname the EsfPathName pathName of the library to retrieve
   	     * @param u the User to limit visibility list to
   	     * @return the Library object as loaded from the database if found; else null
   	     */
   	    public static Library getByPathName(final EsfPathName pathName, final User u)
   	    {
   	    	Library l = cache.getByPathName(pathName,u);
   	    	if ( l == null )
   	    		_logger.debug("Manager.getByPathName() could not find for pathName: " + pathName + "; for user: " + u.getEmail());
   	        return l;
   	    }
   	    
   	    
   	    /**
   	     * 
   	     * Retrieves a Library object based on the supplied id.
   	     * @param id the EsfUUID library id to retrieve
   	     * @return the Library object as loaded from the database if found; else null
   	     */
   	    public static Library getById(final EsfUUID id)
   	    {
   	    	Library l = cache.getById(id);
   	    	if ( l == null )
   	    		_logger.debug("Manager.getById() could not find for id: " + id);
   	        return l;
   	    }
   	    
   	    /**
   	     * 
   	     * Retrieves a Library object based on the supplied id.
   	     * @param id the EsfUUID library id to retrieve
   	     * @param u the User to limit visibility list to
   	     * @return the Library object as loaded from the database if found; else null
   	     */
   	    public static Library getById(final EsfUUID id, final User u)
   	    {
   	    	Library l = cache.getById(id,u);
   	    	if ( l == null ) {
   	    		String userEmail = u == null ? "(unknown user)" : u.getEmail();
   	    		_logger.debug("Manager.getById() could not find for id: " + id + "; for user: " + userEmail);
   	    	}
   	        return l;
   	    }
   	    
   	    /**
   	     * Returns the total number of library objects
   	     * @return the total number of library objects
   	     */
   	    public static int getNumLibraries()
   	    {
   	    	return cache.size();
   	    }
   	    public static int getNumLibraries(INCLUDE include)
   	    {
   	    	if ( include == INCLUDE.BOTH_ENABLED_AND_DISABLED )
   	    		return getNumLibraries();
   	    	
   	    	int count = 0;
   	    	for( Library library : cache.getAllById() )
   	    	{
   	    		if ( include == INCLUDE.ONLY_ENABLED && library.isEnabled() )
   	    			++count;
   	    		else if ( include == INCLUDE.ONLY_DISABLED && library.isDisabled() )
   	    			++count;
   	    	}
   	    	return count;
   	    }

   	    /**
   	     * Retrieves all Library objects 
   	     * @return the Collection of Library found ordered by path name
   	     */
   	    public static Collection<Library> getAll()
   	    {
   	    	_logger.debug("Manager.getAll() cache size: " + cache.size());
   	       	return cache.getAllByPathName();
   	    }
   	    public static Collection<Library> getAll(INCLUDE include)
   	    {
   	    	if ( include == INCLUDE.BOTH_ENABLED_AND_DISABLED )
   	    		return getAll();
   	    	
   	    	LinkedList<Library> libraryList = new LinkedList<Library>();
   	    	for( Library library : cache.getAllByPathName() )
   	    	{
   	    		if ( include == INCLUDE.ONLY_ENABLED && library.isEnabled() )
   	    			libraryList.add(library);
   	    		else if ( include == INCLUDE.ONLY_DISABLED && library.isDisabled() )
   	    			libraryList.add(library);
   	    	}
   	    	return libraryList;
   	    }
   	    
   	    public static Collection<Library> getForUserWithPermission(User user, EsfName permOptionName)
   	    {
   	    	if ( PermissionOption.PERM_OPTION_LIST.equals(permOptionName) )
   	    		return cache.getAllByPathName(user); // Only gets active Libraries the user can list
   	    	
   	    	LinkedList<Library> libraryList = new LinkedList<Library>();
   	    	for( Library l : cache.getAllByPathName(user) ) 
   	    	{
   	    		if ( l.hasPermission(user, permOptionName) )
   	    			libraryList.add(l);
   	    	}
   	    	return libraryList;
   	    }
   	    public static Collection<Library> getForUserWithPermission(User user, EsfName permOptionName,INCLUDE include)
   	    {
   	    	if ( include == INCLUDE.BOTH_ENABLED_AND_DISABLED )
   	    		return getForUserWithPermission(user,permOptionName);
   	    	
   	    	LinkedList<Library> libraryList = new LinkedList<Library>();
   	    	for( Library l : cache.getAllByPathName(user) ) 
   	    	{
   	    		if ( l.hasPermission(user, permOptionName) )
   	    		{
   	   	    		if ( include == INCLUDE.ONLY_ENABLED && l.isEnabled() )
   	   	    			libraryList.add(l);
   	   	    		else if ( include == INCLUDE.ONLY_DISABLED && l.isDisabled() )
   	   	    			libraryList.add(l);
   	    		}
   	    	}
   	    	return libraryList;
   	    }
   	    public static Collection<Library> getForUserWithListPermission(User user)
   	    {
   	    	return getForUserWithPermission(user, PermissionOption.PERM_OPTION_LIST);
   	    }
   	    public static Collection<Library> getForUserWithListPermission(User user,INCLUDE include)
   	    {
   	    	return getForUserWithPermission(user, PermissionOption.PERM_OPTION_LIST,include);
   	    }
   	    public static Collection<Library> getForUserWithViewDetailsPermission(User user)
   	    {
   	    	return getForUserWithPermission(user, PermissionOption.PERM_OPTION_VIEWDETAILS);
   	    }
   	    public static Collection<Library> getForUserWithViewDetailsPermission(User user,INCLUDE include)
   	    {
   	    	return getForUserWithPermission(user, PermissionOption.PERM_OPTION_VIEWDETAILS,include);
   	    }
   	    
   	} // Manager
    
}