// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;

/**
* EmailTemplateVersionInfo is a data-only object that holds information about a email template version.
* It's not a mutable object (you can't save/delete it), just for lists of emailTemplate versions and such.
* 
* @author Yozons, Inc.
*/
public class EmailTemplateVersionInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<EmailTemplateVersionInfo>, java.io.Serializable
{
	private static final long serialVersionUID = 347148082578240088L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(EmailTemplateVersionInfo.class);

    protected EsfUUID id;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected String emailSubject;
    
    /**
     * This creates a new EmailTemplateVersionInfo using the source object.
     * @param emailTemplateVersion the emailTemplate version to use as the source
     */
    protected EmailTemplateVersionInfo(EmailTemplateVersion emailTemplateVersion)
    {
        this.id = emailTemplateVersion.getId();
        this.version = emailTemplateVersion.getVersion();
        this.createdTimestamp = emailTemplateVersion.getCreatedTimestamp();
        this.createdByUserId = emailTemplateVersion.getCreatedByUserId();
        this.lastUpdatedTimestamp = emailTemplateVersion.getLastUpdatedTimestamp();
        this.lastUpdatedByUserId = emailTemplateVersion.getLastUpdatedByUserId();
        this.emailSubject = emailTemplateVersion.getEmailSubject();
    }
    
    /**
     * This creates a EmailTemplateVersionInfo object from data retrieved from the DB.
     */
    protected EmailTemplateVersionInfo(
    		EsfUUID id, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId, String emailSubject)
    {
        this.id = id;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.emailSubject = emailSubject;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public int getVersion()
    {
    	return version;
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }

    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }

    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }

    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }

    public String getEmailSubject()
    {
    	return emailSubject;
    }
    

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof EmailTemplateVersionInfo )
            return getId().equals(((EmailTemplateVersionInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(EmailTemplateVersionInfo d)
    {
    	return getVersion() - d.getVersion();
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all EmailTemplateVersionInfo objects in the specified library.
   	     * @return the Collection of EmailTemplateVersionInfo found with most recent versions first.
   	     */
   	    public static Collection<EmailTemplateVersionInfo> getAll(EmailTemplate emailTemplate)
   	    {
   	    	LinkedList<EmailTemplateVersionInfo> list = new LinkedList<EmailTemplateVersionInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id, version, created_timestamp, created_by_user_id, last_updated_timestamp, last_updated_by_user_id, email_subject " +
   	    	            "FROM esf_library_email_template_version " + 
   	    	            "WHERE library_email_template_id = ? " +
   	    	            "ORDER BY version DESC"
   	            						   );
   	        	stmt.set(emailTemplate.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            String emailSubject = rs.getString();
		            
		            EmailTemplateVersionInfo emailTemplateVerInfo = new EmailTemplateVersionInfo(id, version,
		            							createdTimestamp, createdByUserId, lastUpdatedTimestamp, lastUpdatedByUserId, emailSubject
		            		 												);
		            emailTemplateVerInfo.setLoadedFromDb();
		            list.add(emailTemplateVerInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - EmailTemplate: " + emailTemplate.getEsfName() + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - EmailTemplate: " + emailTemplate.getEsfName() + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static EmailTemplateVersionInfo createLike(EmailTemplate likeEmailTemplate, EmailTemplateVersion likeEmailTemplateVersion, User createdBy)
   	    {
   			EmailTemplateVersion newEmailTemplateVersion = EmailTemplateVersion.Manager.createLike(likeEmailTemplate, likeEmailTemplateVersion, createdBy);
   			EmailTemplateVersionInfo newEmailTemplateVerInfo = new EmailTemplateVersionInfo(newEmailTemplateVersion);
   			return newEmailTemplateVerInfo;
   	    }

   	    public static EmailTemplateVersionInfo createNew(EmailTemplate emailTemplate, User createdBy)
   	    {
   			EmailTemplateVersion newEmailTemplateVersion = EmailTemplateVersion.Manager.createTest(emailTemplate, createdBy);
   			EmailTemplateVersionInfo newEmailTemplateVerInfo = new EmailTemplateVersionInfo(newEmailTemplateVersion);
   			return newEmailTemplateVerInfo;
   	    }

   	    public static EmailTemplateVersionInfo createFromSource(EmailTemplateVersion emailTemplateVersion)
   	    {
   	    	if ( emailTemplateVersion == null )
   	    		return null;
   	    	
   			EmailTemplateVersionInfo emailTemplateVerInfo = new EmailTemplateVersionInfo(emailTemplateVersion);
   			emailTemplateVerInfo.setLoadedFromDb();

   			return emailTemplateVerInfo;
   	    }
   	    
   	} // Manager
   	
}