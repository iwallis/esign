// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;

/**
* SerialVersionInfo is a data-only object that holds information about a serial version.
* It's not a mutable object (you can't save/delete it), just for lists of serial versions and such.
* 
* @author Yozons, Inc.
*/
public class SerialVersionInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<SerialVersionInfo>
{
	private static final long serialVersionUID = 2603250103123562378L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(SerialVersionInfo.class);

    protected EsfUUID id;
    protected EsfUUID serialId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected String decimalFormat;
    
    /**
     * This creates a new SerialVersionInfo using the source object.
     * @param serialVersion the serial version to use as the source
     */
    protected SerialVersionInfo(SerialVersion serialVersion)
    {
        this.id = serialVersion.getId();
        this.serialId = serialVersion.getSerialId();
        this.version = serialVersion.getVersion();
        this.createdTimestamp = serialVersion.getCreatedTimestamp();
        this.createdByUserId = serialVersion.getCreatedByUserId();
        this.lastUpdatedTimestamp = serialVersion.getLastUpdatedTimestamp();
        this.lastUpdatedByUserId = serialVersion.getLastUpdatedByUserId();
        this.decimalFormat = serialVersion.getDecimalFormat();
    }
    
    /**
     * This creates a SerialVersionInfo object from data retrieved from the DB.
     */
    protected SerialVersionInfo(
    		EsfUUID id, EsfUUID serialId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId, 
    		String decimalFormat)
    {
        this.id = id;
        this.serialId = serialId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.decimalFormat = decimalFormat;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public EsfUUID getSerialId()
    {
        return serialId;
    }
    
    public int getVersion()
    {
    	return version;
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }

    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }

    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }

    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }
    
    public String getDecimalFormat()
    {
    	return decimalFormat;
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof SerialVersionInfo )
            return getId().equals(((SerialVersionInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(SerialVersionInfo o)
    {
    	return getVersion() - o.getVersion();
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all SerialVersionInfo objects in the specified library.
   	     * @return the Collection of SerialVersionInfo found with most recent versions first.
   	     */
   	    public static Collection<SerialVersionInfo> getAll(Serial serial)
   	    {
   	    	LinkedList<SerialVersionInfo> list = new LinkedList<SerialVersionInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id, version, created_timestamp, created_by_user_id, last_updated_timestamp, last_updated_by_user_id, decimal_format " +
   	    	            "FROM esf_library_serial_version " + 
   	    	            "WHERE library_serial_id=? " +
   	    	            "ORDER BY version DESC"
   	            						   );
   	        	stmt.set(serial.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            String decimalFormat = rs.getString();
		            
		            SerialVersionInfo serialVerInfo = new SerialVersionInfo(id, serial.getId(), version,
		            							createdTimestamp, createdByUserId, lastUpdatedTimestamp, lastUpdatedByUserId, decimalFormat
		            		 												);
		            serialVerInfo.setLoadedFromDb();
		            list.add(serialVerInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - Serial: " + serial.getEsfName() + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - Serial: " + serial.getEsfName() + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static SerialVersionInfo createLike(Serial likeSerial, SerialVersion likeSerialVersion, User createdBy)
   	    {
   			SerialVersion newSerialVersion = SerialVersion.Manager.createLike(likeSerial, likeSerialVersion, createdBy);
   			SerialVersionInfo newSerialVerInfo = new SerialVersionInfo(newSerialVersion);
   			return newSerialVerInfo;
   	    }

   	    public static SerialVersionInfo createNew(Serial file, User createdBy)
   	    {
   			SerialVersion newSerialVersion = SerialVersion.Manager.createTest(file, createdBy);
   			SerialVersionInfo newSerialVerInfo = new SerialVersionInfo(newSerialVersion);
   			return newSerialVerInfo;
   	    }

   	    public static SerialVersionInfo createFromSource(SerialVersion serialVersion)
   	    {
   	    	if ( serialVersion == null )
   	    		return null;
   	    	
   			SerialVersionInfo serialVerInfo = new SerialVersionInfo(serialVersion);
   			serialVerInfo.setLoadedFromDb();

   			return serialVerInfo;
   	    }
   	    
   	} // Manager
   	
}