// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;

/**
* ButtonMessageVersionInfo is a data-only object that holds information about a button message version.
* It's not a mutable object (you can't save/delete it), just for lists of buttonmessage versions and such.
* 
* @author Yozons, Inc.
*/
public class ButtonMessageVersionInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<ButtonMessageVersionInfo>, java.io.Serializable
{
	private static final long serialVersionUID = -1138466260541302868L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ButtonMessageVersionInfo.class);

    protected EsfUUID id;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    
    /**
     * This creates a new ButtonMessageVersionInfo using the source object.
     * @param buttonmessageVersion the buttonmessage version to use as the source
     */
    protected ButtonMessageVersionInfo(ButtonMessageVersion buttonmessageVersion)
    {
        this.id = buttonmessageVersion.getId();
        this.version = buttonmessageVersion.getVersion();
        this.createdTimestamp = buttonmessageVersion.getCreatedTimestamp();
        this.createdByUserId = buttonmessageVersion.getCreatedByUserId();
        this.lastUpdatedTimestamp = buttonmessageVersion.getLastUpdatedTimestamp();
        this.lastUpdatedByUserId = buttonmessageVersion.getLastUpdatedByUserId();
    }
    
    /**
     * This creates a ButtonMessageVersionInfo object from data retrieved from the DB.
     */
    protected ButtonMessageVersionInfo(
    		EsfUUID id, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId)
    {
        this.id = id;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public int getVersion()
    {
    	return version;
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }

    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }

    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }

    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }


    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof ButtonMessageVersionInfo )
            return getId().equals(((ButtonMessageVersionInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(ButtonMessageVersionInfo d)
    {
    	return getVersion() - d.getVersion();
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all ButtonMessageVersionInfo objects in the specified library.
   	     * @return the Collection of ButtonMessageVersionInfo found with most recent versions first.
   	     */
   	    public static Collection<ButtonMessageVersionInfo> getAll(ButtonMessage buttonmessage)
   	    {
   	    	_logger.debug("Manager.getAll() - ButtonMessage: " + buttonmessage.getEsfName());
   	    	
   	    	LinkedList<ButtonMessageVersionInfo> list = new LinkedList<ButtonMessageVersionInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id, version, created_timestamp, created_by_user_id, last_updated_timestamp, last_updated_by_user_id " +
   	    	            "FROM esf_library_buttonmessage_version " + 
   	    	            "WHERE library_buttonmessage_id = ? " +
   	    	            "ORDER BY version DESC"
   	            						   );
   	        	stmt.set(buttonmessage.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            
		            ButtonMessageVersionInfo buttonmessageVerInfo = new ButtonMessageVersionInfo(id, version,
		            							createdTimestamp, createdByUserId, lastUpdatedTimestamp, lastUpdatedByUserId
		            		 												);
		            buttonmessageVerInfo.setLoadedFromDb();
		            list.add(buttonmessageVerInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - ButtonMessage: " + buttonmessage.getEsfName() + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - ButtonMessage: " + buttonmessage.getEsfName() + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static ButtonMessageVersionInfo createLike(ButtonMessage likeButtonMessage, ButtonMessageVersion likeButtonMessageVersion, User createdBy)
   	    {
   			ButtonMessageVersion newButtonMessageVersion = ButtonMessageVersion.Manager.createLike(likeButtonMessage, likeButtonMessageVersion, createdBy);
   			ButtonMessageVersionInfo newButtonMessageVerInfo = new ButtonMessageVersionInfo(newButtonMessageVersion);
   			return newButtonMessageVerInfo;
   	    }

   	    public static ButtonMessageVersionInfo createNew(ButtonMessage buttonmessage, User createdBy)
   	    {
   			ButtonMessageVersion newButtonMessageVersion = ButtonMessageVersion.Manager.createTest(buttonmessage, createdBy);
   			ButtonMessageVersionInfo newButtonMessageVerInfo = new ButtonMessageVersionInfo(newButtonMessageVersion);
   			return newButtonMessageVerInfo;
   	    }

   	    public static ButtonMessageVersionInfo createFromSource(ButtonMessageVersion buttonmessageVersion)
   	    {
   	    	if ( buttonmessageVersion == null )
   	    		return null;
   	    	
   			ButtonMessageVersionInfo buttonmessageVerInfo = new ButtonMessageVersionInfo(buttonmessageVersion);
   			buttonmessageVerInfo.setLoadedFromDb();

   			return buttonmessageVerInfo;
   	    }
   	    
   	} // Manager
   	
}