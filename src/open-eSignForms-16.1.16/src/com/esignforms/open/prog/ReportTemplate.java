// Copyright (C) 2011-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.Permission;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.util.PathNameUUIDUserListableCacheReadOptimized;

/**
* ReportTemplate holds a single Open eSignForms template of a report, such as the transaction templates it operates on, who can run it, etc.
* 
* @author Yozons, Inc.
*/
public class ReportTemplate
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<ReportTemplate>, 
    			PathNameUUIDUserListableCacheReadOptimized.PathNameUUIDUserListableCacheable, 
    			PathNameUUIDUserListableCacheReadOptimized.TimeCacheable,
    			java.io.Serializable
{
	private static final long serialVersionUID = -8453538628608617632L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ReportTemplate.class);

    protected final EsfUUID id;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected EsfPathName pathName;
    protected EsfPathName originalPathName; // derived, not persisted
    protected String  displayName;
    protected String  description;
    protected String  comments;
    protected String  status;
    protected List<EsfUUID> transactionTemplateIdList;
    protected List<EsfUUID> limitDocumentIdList;
    protected List<ReportTemplateReportField> reportTemplateReportFieldList;
    protected Permission permission; // permission grants related to this report template object (create like, delete, list, update, view details of this report template object)
                                     // as well as those permissions for users to report on them, etc.
    
    public static final String PERM_PATH_PREFIX = "ReportTemplatePerm" + EsfPathName.PATH_SEPARATOR;  // actual name is this prefix followed report template's id
    
    // Initial report template created for cloning from, etc.
    public static final EsfPathName TEMPLATE_ESFPATHNAME	= new EsfPathName(EsfPathName.ESF_RESERVED_REPORT_TEMPLATE_PATH_PREFIX+"Template");
    
    public enum INCLUDE { ONLY_ENABLED, ONLY_DISABLED, BOTH_ENABLED_AND_DISABLED };
    
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();

    /**
     * This creates a ReportTemplate object from data retrieved from the DB.
     */
    protected ReportTemplate(EsfUUID id, EsfPathName pathName, String displayName, String description, String comments, 
    		        EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		        String status, Permission permission, List<EsfUUID> transactionTemplateIdList, List<EsfUUID> limitDocumentIdList,
    		        List<ReportTemplateReportField> reportTemplateReportFieldList)
    {
        this.id = id;
        this.pathName = pathName;
        this.originalPathName = pathName.duplicate();
        this.displayName = displayName;
        this.description = description;
        this.comments = comments;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.status = status;
        this.permission = permission;
        this.transactionTemplateIdList = transactionTemplateIdList;
        this.limitDocumentIdList = limitDocumentIdList;
        this.reportTemplateReportFieldList = reportTemplateReportFieldList;
    }
    
   
    /**
     * This creates a ReportTemplate object 
     */
    protected ReportTemplate(EsfUUID id, EsfPathName pathName, String displayName, String description, String comments, 
    		        EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		        String status, Permission permission)
    {
        this.id = id;
        this.pathName = pathName;
        this.originalPathName = pathName.duplicate();
        this.displayName = displayName;
        this.description = description;
        this.comments = comments;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.status = status;
        this.permission = permission;
        this.transactionTemplateIdList = new LinkedList<EsfUUID>();
        this.limitDocumentIdList = new LinkedList<EsfUUID>();
        this.reportTemplateReportFieldList = new LinkedList<ReportTemplateReportField>();
    }
    
    public ReportTemplate duplicate()
    {
    	ReportTemplate rt = new ReportTemplate(id, pathName.duplicate(), displayName, description, comments, 
		        createdTimestamp, createdByUserId, lastUpdatedTimestamp, lastUpdatedByUserId,
		        status, permission.duplicate(), getTransactionTemplateIdList(), getLimitDocumentIdList(), getDuplicateReportTemplateReportFieldList());
    	rt.setDatabaseObjectLike(this);
    	return rt;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public EsfPathName getPathName()
    {
        return pathName;
    }
    public void setPathName(EsfPathName v)
    {
    	// Block setting a report template name to null, an invalid EsfPathName or if the template current is a reserved name or the new name would be reserved
    	if ( v == null || ! v.isValid() || v.hasReservedPathPrefix() || (pathName != null && pathName.hasReservedPathPrefix()) )
    		return;
        pathName = v;
        objectChanged();
    }
    public EsfPathName getOriginalPathName()
    {
        return originalPathName;
    }
    public void resetOriginalPathName()
    {
    	originalPathName = pathName.duplicate();
    }
    
    public boolean isReserved()
    {
    	return getPathName().hasReservedPathPrefix();
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }
    
    public void makeEnabled()
    {
        status = Literals.STATUS_ENABLED;
        objectChanged();
    }
     
    public void makeDisabled()
    {
        status = Literals.STATUS_DISABLED;
        objectChanged();
    }
    
    public void setStatus(String s)
    {
        if ( Literals.STATUS_ENABLED.equals(s) )
            makeEnabled();
        else
            makeDisabled();
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}

	public String getDisplayName()
	{
		return displayName;
	}
	public void setDisplayName(String v)
	{
    	if ( v == null )
    		displayName = pathName.toString();
    	else if ( v.length() > Literals.DISPLAY_NAME_MAX_LENGTH )
    		displayName = v.substring(0,Literals.DISPLAY_NAME_MAX_LENGTH).trim();
        else
        	displayName = v.trim();
        objectChanged();
	}

	public String getDescription()
    {
        return description;
    }
    public void setDescription(String v)
    {
    	if ( v == null )
    		description = v;
    	else if ( v.length() > Literals.DESCRIPTION_MAX_LENGTH )
            description = v.substring(0,Literals.DESCRIPTION_MAX_LENGTH).trim();
        else
        	description = v.trim();
        objectChanged();
    }
    
    public String getComments()
    {
    	return comments;
    }
    public void setComments(String v)
    {
    	if ( v == null )
    		comments = v;
    	else if ( v.length() > Literals.COMMENTS_MAX_LENGTH )
    		comments = v.substring(0,Literals.COMMENTS_MAX_LENGTH).trim();
        else
        	comments = v.trim();
        objectChanged();
    }

    public EsfDateTime getCreatedTimestamp()
    {
        return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
        return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
        return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
        return lastUpdatedByUserId;
    }
    
    public List<EsfUUID> getTransactionTemplateIdListInternal()
    {
    	return transactionTemplateIdList;
    }
    public List<EsfUUID> getTransactionTemplateIdList()
    {
    	return new LinkedList<EsfUUID>(transactionTemplateIdList);
    }
    public List<EsfUUID> getTransactionTemplateIdListForUser(User forUser)
    {
    	List<EsfUUID> list = new LinkedList<EsfUUID>();
    	for( EsfUUID id : transactionTemplateIdList )
    	{
    		TransactionTemplate tt = TransactionTemplate.Manager.getByIdForList(id, forUser);
    		if ( tt != null )
    			list.add(id);
    	}
    	return list;
    }
    public void setTransactionTemplateIdList(List<EsfUUID> v) 
    {
    	transactionTemplateIdList = v;
    	objectChanged();
    }
    
    public List<EsfUUID> getLimitDocumentIdListInternal()
    {
    	return limitDocumentIdList;
    }
    public List<EsfUUID> getLimitDocumentIdList()
    {
    	return new LinkedList<EsfUUID>(limitDocumentIdList);
    }
    public void setLimitDocumentIdList(List<EsfUUID> v) 
    {
    	limitDocumentIdList = v;
    	objectChanged();
    }
    
    public ReportTemplateReportField getReportTemplateReportField(EsfUUID reportFieldTemplateId)
    {
    	for( ReportTemplateReportField rtrf : reportTemplateReportFieldList )
    	{
    		if ( rtrf.getReportFieldTemplateId().equals(reportFieldTemplateId) )
    			return rtrf;
    	}
    	return null;
    }
    public List<ReportTemplateReportField> getReportTemplateReportFieldListInternal()
    {
    	return reportTemplateReportFieldList;
    }
    public List<ReportTemplateReportField> getReportTemplateReportFieldList()
    {
    	return new LinkedList<ReportTemplateReportField>(reportTemplateReportFieldList);
    }
    public List<ReportTemplateReportField> getDuplicateReportTemplateReportFieldList()
    {
    	LinkedList<ReportTemplateReportField> list = new LinkedList<ReportTemplateReportField>();
    	for( ReportTemplateReportField rtrf : getReportTemplateReportFieldList() )
    		list.add(rtrf.duplicate());
    	return list;
    }
    public void setReportTemplateReportFieldList(List<ReportTemplateReportField> v) 
    {
    	reportTemplateReportFieldList = v;
    	objectChanged();
    }
    
    public List<ReportTemplateReportField> getSearchableReportTemplateReportFieldList()
    {
    	LinkedList<ReportTemplateReportField> list = new LinkedList<ReportTemplateReportField>();
    	for( ReportTemplateReportField rtrf : getReportTemplateReportFieldList() )
    	{
    		if ( rtrf.isAllowSearch() )
    			list.add(rtrf.duplicate());
    	}
    	return list;
    }
        
    public List<ReportTemplateReportField> getReportTemplateReportFileFieldList()
    {
    	LinkedList<ReportTemplateReportField> list = new LinkedList<ReportTemplateReportField>();
    	for( ReportTemplateReportField rtrf : getReportTemplateReportFieldList() )
    	{
    		if ( rtrf.getReportFieldTemplate().isFieldTypeFile() )
    			list.add(rtrf.duplicate());
    	}
    	return list;
    }
        
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof ReportTemplate )
            return getId().equals(((ReportTemplate)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(ReportTemplate o)
    {
    	return getPathName().compareTo(o.getPathName());
    }
    
    @Override
    public boolean canUserList(User user)
    {
		return permission.canUserList(user);
    }
    
    public final boolean hasLimitedDocuments()
    {
    	return limitDocumentIdList.size() > 0;
    }
    public final boolean isDocumentAllowed(EsfUUID docId)
    {
    	return ! hasLimitedDocuments() || limitDocumentIdList.contains(docId);
    }
    
    /**
     * @param tran the Transaction to check
     * @return true if the transaction as allowed documents that also have snapshots
     */
    public boolean hasAllowedTransactionDocumentSnapshots(Transaction tran)
    {
    	for( TransactionDocument tranDoc : tran.getAllTransactionDocuments() ) 
    	{	
    		if ( isDocumentAllowed(tranDoc.getDocumentId()) )
    		{
            	for( TransactionParty tranParty : tran.getAllTransactionParties() ) 
            	{
                	for( TransactionPartyDocument tranPartyDoc : tranParty.getNonSkippedTransactionPartyDocuments() ) 
                	{
                		if ( tranPartyDoc.hasSnapshotXmlOrBlobId() && tranPartyDoc.getTransactionDocumentId().equals(tranDoc.getId()) ) 
                			return true;
                	}
            	}
    		}
    	}
    	
    	return false;
    }

    /**
     * @param tran the Transaction to check
     * @return true if the transaction as allowed documents regardless of whether snapshots exist
     */
    public boolean hasAllowedTransactionDocuments(Transaction tran)
    {
    	for( TransactionDocument tranDoc : tran.getAllTransactionDocuments() ) 
    	{	
    		if ( isDocumentAllowed(tranDoc.getDocumentId()) )
    			return true;
    	}
    	
    	return false;
    }

    /**
     * @param tran the Transaction to check
     * @return List of TransactionDocuments the report allows, regardless of whether there are snapshots or not
     */
    public List<TransactionDocument> getAllAllowedTransactionDocuments(Transaction tran)
    {
    	List<TransactionDocument> tranDocList = tran.getAllTransactionDocuments();
    	ListIterator<TransactionDocument> tranDocListIter = tranDocList.listIterator();
    	while( tranDocListIter.hasNext() ) 
    	{	
    		TransactionDocument tranDoc = tranDocListIter.next();
    		if ( ! isDocumentAllowed(tranDoc.getDocumentId()) )
    			tranDocListIter.remove();
    	}
    	return tranDocList;
    }
    
    public int getNumAllowedTransactionDocumentsWithDocumentSnapshot(Transaction tran)
    {
    	int numWithSnapshots = 0;
    	
    	for( TransactionDocument tranDoc : getAllAllowedTransactionDocuments(tran) )
    	{
    		TransactionPartyDocument tpd = tran.getLatestTransactionPartyDocumentWithSnapshot(tranDoc);
    		if ( tpd != null && ! tpd.isSnapshotDocumentSkipped() )
    			++numWithSnapshots;
    	}
    	
    	return numWithSnapshots;
    }
    
    public final boolean hasPermission(User user, EsfName permOptionName)
    {
    	return permission.hasPermission(user, permOptionName);
    }
    public final boolean hasRunReportPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.REPORT_PERM_OPTION_RUN_REPORT);
    }
    public final boolean hasViewStartedByExternalUsersPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.REPORT_PERM_OPTION_VIEW_STARTED_BY_EXTERNAL_USERS);
    }
    public final boolean hasViewStartedByAnyUserPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.REPORT_PERM_OPTION_VIEW_STARTED_BY_ANY_USER);
    }
    public final boolean hasViewAnyUserPartyToPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO);
    }
    public final boolean hasViewProductionPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.REPORT_PERM_OPTION_VIEW_PRODUCTION);
    }
    public final boolean hasDownloadCsvPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.REPORT_PERM_OPTION_DOWNLOAD_CSV);
    }
    public final boolean hasDownloadArchivePermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.REPORT_PERM_OPTION_DOWNLOAD_ARCHIVE);
    }
    public final boolean hasViewActivityLogPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.REPORT_PERM_OPTION_VIEW_ACTIVITY_LOG);
    }
    public final boolean hasViewEmailLogPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.REPORT_PERM_OPTION_VIEW_EMAIL_LOG);
    }
    public final boolean hasViewSnapshotDataPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.REPORT_PERM_OPTION_VIEW_SNAPSHOT_DATA);
    }
    public final boolean hasViewSnapshotDocumentPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.REPORT_PERM_OPTION_VIEW_SNAPSHOT_DOCUMENT);
    }
    public final boolean hasEsfReportsAccessViewPermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_VIEW);
    }
    public final boolean hasEsfReportsAccessUpdatePermission(User user)
    {
    	return permission.hasPermission(user, PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE);
    }
    
    public final Collection<Group> getPermissionAllowedGroups(User user, EsfName permOptionName)
    {
    	return permission.getAllowedGroups(user, permOptionName);
    }
    public final boolean addPermissionAllowedGroups(EsfName permOptionName, Group group)
    {
    	return permission.addGroupToOptions(group, permOptionName) > 0;
    }
    public final boolean removePermissionAllowedGroups(EsfName permOptionName, Group group)
    {
    	return permission.removeGroupFromOptions(group, permOptionName) > 0;
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; pathName: " + pathName + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
            	
            	permission.save(con);
            	
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_report_template " +
                	"(id,path_name,display_name,description,comments,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,status) " + 
                	"VALUES(?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(pathName);
                stmt.set(displayName);
                stmt.set(description);
                stmt.set(comments);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(status);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; pathName: " + pathName);
                    return false;
                }
                
                stmt.close();
                stmt = new EsfPreparedStatement(con, 
                    	"INSERT INTO esf_report_template_transaction_template (report_template_id,transaction_template_id) VALUES (?,?)");
                stmt.set(id);
                for( EsfUUID ttId : transactionTemplateIdList )
                {
                	stmt.set(2,ttId);
                	num = stmt.executeUpdate();
                    if ( num != 1 )
                    {
                    	_logger.warn("save(con) - Insert failed for id: " + id + "; pathName: " + pathName + "; for transaction template id: " + ttId);
                        return false;
                    }
                }
                
                stmt.close();
                stmt = new EsfPreparedStatement(con, 
                    	"INSERT INTO esf_report_template_limit_document (report_template_id,library_document_id) VALUES (?,?)");
                stmt.set(id);
                for( EsfUUID ldId : limitDocumentIdList )
                {
                	stmt.set(2,ldId);
                	num = stmt.executeUpdate();
                    if ( num != 1 )
                    {
                    	_logger.warn("save(con) - Insert failed for id: " + id + "; pathName: " + pathName + "; for limit document id: " + ldId);
                        return false;
                    }
                }
                
                ReportTemplateReportField.Manager.save(con, id, reportTemplateReportFieldList);
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this); // we use replace here because on create, we add to ID-only cache
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new report template " + getPathName()); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new report template " + getPathName());
                }
                
                return true;
            }
            
            if ( permission.hasChanged() )
            	permission.save(con);

            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_report_template SET path_name=?,display_name=?,description=?,comments=?,last_updated_timestamp=?,last_updated_by_user_id=?,status=? WHERE id=?"
                						   );
                stmt.set(pathName);
                stmt.set(displayName);
                stmt.set(description);
                stmt.set(comments);
            	stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(status);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for report template id: " + id + "; pathName: " + pathName);
                }
                
                stmt.close();
                stmt = new EsfPreparedStatement(con, "DELETE FROM esf_report_template_transaction_template WHERE report_template_id=?");
                stmt.set(id);
                num = stmt.executeUpdate();

                stmt.close();
                stmt = new EsfPreparedStatement(con, "INSERT INTO esf_report_template_transaction_template (report_template_id,transaction_template_id) VALUES (?,?)");
                stmt.set(id);
                for( EsfUUID ttId : transactionTemplateIdList )
                {
                	stmt.set(2,ttId);
                	num = stmt.executeUpdate();
                    if ( num != 1 )
                    {
                    	_logger.warn("save(con) - Update failed for id: " + id + "; pathName: " + pathName + "; for transaction template id: " + ttId);
                    }
                }

                stmt.close();
                stmt = new EsfPreparedStatement(con, "DELETE FROM esf_report_template_limit_document WHERE report_template_id=?");
                stmt.set(id);
                num = stmt.executeUpdate();

                stmt.close();
                stmt = new EsfPreparedStatement(con, "INSERT INTO esf_report_template_limit_document (report_template_id,library_document_id) VALUES (?,?)");
                stmt.set(id);
                for( EsfUUID ldId : limitDocumentIdList )
                {
                	stmt.set(2,ldId);
                	num = stmt.executeUpdate();
                    if ( num != 1 )
                    {
                    	_logger.warn("save(con) - Update failed for id: " + id + "; pathName: " + pathName + "; for limit document id: " + ldId);
                        return false;
                    }
                }

                ReportTemplateReportField.Manager.save(con, id, reportTemplateReportFieldList);
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated report template " + getPathName()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated report template " + getPathName()); 
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; pathName: " + pathName + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    private synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; pathName: " + pathName + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use elsewhere
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of report template that was pending an INSERT id: " + id + "; pathName: " + pathName);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
            // Delete the report fields
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_report_template_report_field WHERE report_template_id=?");
            stmt.set(id);
            int num = stmt.executeUpdate();

            // Delete the transaction templates for this report
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_report_template_transaction_template WHERE report_template_id=?");
            stmt.set(id);
            num = stmt.executeUpdate();

            // Delete the limit documents for this report
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_report_template_limit_document WHERE report_template_id=?");
            stmt.set(id);
            num = stmt.executeUpdate();

            // Delete the report template
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_report_template WHERE id=?");
            stmt.set(id);
            num = stmt.executeUpdate();
            if ( num == 0 )
            	_logger.warn("delete(con) - Failed to find esf_report_template database row to delete with id: " + id);
            
            permission.delete(con);
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted report template " + getPathName()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted report template " + getPathName()); 
            }
            
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on id: " + id + "; pathName: " + pathName + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }
    
    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
		// We never allow the reserved report template to be deleted
		if ( isReserved() )
		{
	    	_logger.error("checkReferential() FAILED RESERVE PATH PREFIX on id: " + id + "; pathName: " + pathName);
	    	if ( errors != null )
	    		errors.addError("You cannot delete the reserved special report template: " + getPathName());
			return false;
		}

	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   	    private static PathNameUUIDUserListableCacheReadOptimized<ReportTemplate> cache = new PathNameUUIDUserListableCacheReadOptimized<ReportTemplate>();
   	    
		public static Comparator<ReportTemplate> ComparatorByDisplayName = new Comparator<ReportTemplate>() {
		    public int compare(ReportTemplate rt1, ReportTemplate rt2) {
		    	int diff = rt1.getDisplayName().compareToIgnoreCase(rt2.getDisplayName());
		    	if ( diff == 0 )
		    		diff = rt1.getPathName().toString().compareToIgnoreCase(rt2.getPathName().toString());
		        return diff; 
		    }
		};
   	    
   	    private static ReportTemplate getById(Connection con, EsfUUID reportTemplateId) 
   	    	throws SQLException
   	    {
   	    	EsfPreparedStatement stmt = null;
   	    	
   	    	LinkedList<EsfUUID> tranTemplateIdList = new LinkedList<EsfUUID>();
   	    	LinkedList<EsfUUID> limitDocumentIdList = new LinkedList<EsfUUID>();
   	    	
   	        try
   	        {
   	            stmt = new EsfPreparedStatement( con,
   	    	            "SELECT transaction_template_id FROM esf_report_template_transaction_template WHERE report_template_id = ?"
   	            						   );
   	            stmt.set(reportTemplateId);
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            	tranTemplateIdList.add(rs.getEsfUUID());
	            
	            stmt.close();
   	            stmt = new EsfPreparedStatement( con,
   	    	            "SELECT library_document_id FROM esf_report_template_limit_document WHERE report_template_id = ?"
   	            						   );
   	            stmt.set(reportTemplateId);
	            rs = stmt.executeQuery();
	            while ( rs.next() )
	            	limitDocumentIdList.add(rs.getEsfUUID());
	            
	            stmt.close();
	            stmt = new EsfPreparedStatement( con,
   	    	            "SELECT path_name,display_name,description,comments,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,status " +
   	    	            "FROM esf_report_template WHERE id = ?"
   	            						   );
   	            stmt.set(reportTemplateId);
	            rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
		            EsfPathName pathName = rs.getEsfPathName();
		            String displayName = rs.getString();
		            String description = rs.getString();
		            String comments = rs.getString();
		            EsfDateTime created = rs.getEsfDateTime();
		            EsfUUID createdBy   = rs.getEsfUUID();
		            EsfDateTime lastUpdated = rs.getEsfDateTime();
		            EsfUUID lastUpdatedBy   = rs.getEsfUUID();
		            String  stat = rs.getString();
		            
		        	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+reportTemplateId.toNormalizedEsfNameString());
		        	Permission permission = Permission.Manager.getByPathName(permPathName);
		        	
		        	List<ReportTemplateReportField> reportTemplateReportFieldList = ReportTemplateReportField.Manager.getAll(reportTemplateId);
		        	
		        	ReportTemplate template = new ReportTemplate(reportTemplateId,pathName,displayName,description,comments,created,createdBy,lastUpdated,lastUpdatedBy,
		        			stat,permission,tranTemplateIdList,limitDocumentIdList,reportTemplateReportFieldList);
		            
		            template.setLoadedFromDb();
		            cache.add(template);
		            
		            return template;
	            }
   	        }
   	        finally
   	        {
   	        	cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + reportTemplateId + "; failed to find the report template");
   	        return null; 
   	    }

   	    /**
   	     * Returns the Template report template 
   	     * @return the ReportTemplate that represents the Template
   	     */
   	    public static ReportTemplate getTemplate()
   	    {
   	    	return getByPathName(TEMPLATE_ESFPATHNAME);
   	    }
   	     
   	    /**
   	     * 
   	     * Retrieves a report template object based on the supplied path name.
   	     * @param pathName the EsfPathName name of the template to retrieve
   	     * @return the ReportTemplate object as loaded from the database if found; else null
   	     */
   	    public static ReportTemplate getByPathName(final EsfPathName pathName)
   	    {
   	    	if ( pathName == null || ! pathName.isValid() )
   	    		return null;

   	    	_logger.debug("Manager.getByPathName() for pathName: " + pathName);   	    	
   	    	
   	    	ReportTemplate template = cache.getByPathName(pathName);
   	    	if ( template != null )
   	    		return template;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT id FROM esf_report_template WHERE lower(path_name)=?"
   	        									);
   	        	stmt.set(pathName.toLowerCase());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            con.commit();
	   	            
	   	            return getById(id);
	            }
	            
   	            con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByPathName() - report template pathName: " + pathName);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        _logger.debug("Manager.getByPathName() - report template pathName: " + pathName + "; failed to find the report template");
   	        return null; 
   	    }
   	    
   	    /**
   	     * 
   	     * Retrieves a ReportTemplate object based on the supplied pathName.
   	     * @param esfname the EsfPathName pathName of the template to retrieve
   	     * @param u the User to limit visibility list to
   	     * @return the ReportTemplate object as loaded from the database if found; else null
   	     */
   	    public static ReportTemplate getByPathName(final EsfPathName pathName, final User u)
   	    {
   	    	_logger.debug("Manager.getByPathName() for pathName: " + pathName + "; for user: " + u.getEmail());
   	    	ReportTemplate template = cache.getByPathName(pathName,u);
   	    	if ( template != null )
   	    		return template;
   	    	
   	    	template = getByPathName(pathName);
   	    	return (template != null && template.canUserList(u)) ? template : null;
   	    }
   	    
   	    
   	    /**
   	     * 
   	     * Retrieves a ReportTemplate object based on the supplied id.
   	     * @param id the EsfUUID template id to retrieve
   	     * @return the ReportTemplate object as loaded from the database if found; else null
   	     */
   	    public static ReportTemplate getById(final EsfUUID id)
   	    {
   	    	ReportTemplate template = cache.getById(id);
   	    	if ( template != null )
   	    		return template;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	template = getById(con,id);
   	        	if ( template != null ) 
   	        	{
		            con.commit();
	   	        	return template;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null;
   	    }
   	    
   	    /**
   	     * 
   	     * Retrieves a ReportTemplate object based on the supplied id.
   	     * @param id the EsfUUID template id to retrieve
   	     * @param u the User to limit visibility to; user must have list permission
   	     * @return the ReportTemplate object as loaded from the database if found; else null
   	     */
   	    public static ReportTemplate getById(final EsfUUID id, final User u)
   	    {
   	    	_logger.debug("Manager.getById() for id: " + id + "; for user: " + u.getFullDisplayName());
   	    	ReportTemplate template = cache.getById(id);
   	    	if ( template != null )
   	    		return (template.canUserList(u)) ? template : null;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	template = getById(con,id);
   	        	if ( template != null ) 
   	        	{
		            con.commit();
		            return (template.canUserList(u)) ? template : null;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null;
   	    }
   	    
   		public static List<ReportTemplate> getAllThatIncludesReportFieldTemplate(ReportFieldTemplate reportFieldTemplate)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<ReportTemplate> list = new LinkedList<ReportTemplate>();
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT DISTINCT ON (lower(RT.path_name)) RT.id,RT.path_name " +
   	        			"FROM esf_report_template RT, esf_report_template_report_field RTRF " +
   	        			"WHERE RTRF.report_field_template_id=? AND RTRF.report_template_id=RT.id " +
   	        			"ORDER BY lower(RT.path_name) ASC"
   	        									);
   	        	stmt.set(reportFieldTemplate.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	   	            
	            	ReportTemplate rt = getById(id);
	   	            if ( rt != null )
	   	            	list.add(rt);
	            }
	            
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAllThatIncludesReportFieldTemplate() - reportFieldTemplate.id: " + reportFieldTemplate.getId());
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<ReportTemplate> getForUserWithPermission(User user, EsfName permOptionName)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<ReportTemplate> list = new LinkedList<ReportTemplate>();
   	        
   	        try
   	        {
   	   	        if ( user == null ) // likely only occurs when requests are made when no longer logged in
   	   	        {
   	   	        	_logger.error("Manager.getForUserWithPermission() has no user object; permOptionName: " + permOptionName);
   	   	        	return list;
   	   	        }

   	   	        stmt = new EsfPreparedStatement( con,
   	        			"SELECT DISTINCT ON (lower(RT.path_name)) RT.id,RT.path_name " +
   	        			"FROM esf_report_template RT, esf_permission_option_group PERM " +
   	        			"WHERE PERM.permission_id = RT.id AND lower(PERM.permission_option_esfname)=? AND " +
   	        			"( PERM.group_id=? OR PERM.group_id IN (SELECT GU.group_id FROM esf_group_user GU WHERE GU.user_id=?) ) " +
   	        			"ORDER BY lower(RT.path_name) ASC"
   	        									);
   	        	stmt.set(permOptionName.toLowerCase());
   	        	stmt.set(Group.Manager.getAllUsersGroup().getId());
   	        	stmt.set(user.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	   	            
	            	ReportTemplate rt = getById(id);
	   	            if ( rt != null )
	   	            	list.add(rt);
	            }
	            
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getForUserWithPermission() - userId: " + user.getId() + "; permOptionName: " + permOptionName);
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<ReportTemplate> getEnabledForUserWithPermission(User user, EsfName permOptionName)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<ReportTemplate> list = new LinkedList<ReportTemplate>();
   	        
   	        try
   	        {
   	   	        if ( user == null ) // likely only occurs when requests are made when no longer logged in
   	   	        {
   	   	        	_logger.error("Manager.getEnabledForUserWithPermission() has no user object; permOptionName: " + permOptionName);
   	   	        	return list;
   	   	        }

   	   	        stmt = new EsfPreparedStatement( con,
   	        			"SELECT DISTINCT ON (lower(RT.path_name)) RT.id,RT.path_name " +
   	        			"FROM esf_report_template RT, esf_permission_option_group PERM " +
   	        			"WHERE RT.status=? AND PERM.permission_id = RT.id AND lower(PERM.permission_option_esfname)=? AND " +
   	        			"( PERM.group_id=? OR PERM.group_id IN (SELECT GU.group_id FROM esf_group_user GU WHERE GU.user_id=?) ) " +
   	        			"ORDER BY lower(RT.path_name) ASC"
   	        									);
   	        	stmt.set(Literals.STATUS_ENABLED);
   	        	stmt.set(permOptionName.toLowerCase());
   	        	stmt.set(Group.Manager.getAllUsersGroup().getId());
   	        	stmt.set(user.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	   	            
	            	ReportTemplate rt = getById(id);
	   	            if ( rt != null )
	   	            	list.add(rt);
	            }
	            
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getEnabledForUserWithPermission() - userId: " + user.getId() + "; permOptionName: " + permOptionName);
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<ReportTemplate> getAll()
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<ReportTemplate> list = new LinkedList<ReportTemplate>();
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_report_template" );
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	   	            
	            	ReportTemplate rt = getById(id);
	   	            if ( rt != null )
	   	            	list.add(rt);
	            }
	            
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll()");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		
   	    /**
   	     * Creates a new report template like an existing one.  The new template is automatically added to all of
   	     * the permission lists (it can always list/view/add/change/delete itself unless changed by the creator or others with permission).
   	     * @param likeTemplate the ReportTemplate that we'll be cloning from
   	     * @param esfname the new template's name
   	     * @param createdBy the User who is creating the new template
   	     * @return
   	     */
   	    public static ReportTemplate createLike(ReportTemplate likeTemplate, EsfPathName pathName, User createdBy)
   	    {
   	    	// We don't let you create new reserved objects
   	    	if ( pathName.hasReservedPathPrefix() )
   	    		return null;

   	    	if ( ! likeTemplate.permission.hasPermission(createdBy, PermissionOption.PERM_OPTION_CREATELIKE) )
   	    		return null;
   	    	
   	        EsfUUID id = new EsfUUID();
   	        EsfDateTime nowTimestamp = new EsfDateTime();
   	        
   	        String normalizedId = id.toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+normalizedId);
   	        Permission newPermission = Permission.Manager.createLike(id, permPathName, likeTemplate.permission);
   	        
   	        // It is possible that a user has permission to create like a report template that includes transaction templates the user doesn't have
   	        // permission to list, so those are removed here.
   	        LinkedList<EsfUUID> newTranTemplateIdList = new LinkedList<EsfUUID>(likeTemplate.getTransactionTemplateIdList());
   	        ListIterator<EsfUUID> newTranTemplateIter = newTranTemplateIdList.listIterator();
   	        while( newTranTemplateIter.hasNext() )
   	        {
   	        	EsfUUID tranTemplateId = newTranTemplateIter.next();
   	        	TransactionTemplate tranTemplate = TransactionTemplate.Manager.getByIdForList(tranTemplateId,createdBy);
   	        	if ( tranTemplate == null ) 
   	        		newTranTemplateIter.remove();
   	        }

   	        // Note that the template's permissions can contain groups that even the creator doesn't have access to, but
   	        // they remain in the new template (so they can have permission) and the user can't even see them so 
   	        // cannot modify them away.
   	        ReportTemplate template = new ReportTemplate( id, pathName, likeTemplate.getDisplayName(), likeTemplate.getDescription(), likeTemplate.getComments(), 
   	    							 	   nowTimestamp, createdBy.getId(), nowTimestamp, createdBy.getId(),
   	    							 	   likeTemplate.getStatus(), newPermission, 
   	    							 	   newTranTemplateIdList, likeTemplate.getLimitDocumentIdList(), likeTemplate.getReportTemplateReportFieldList()
   	    						   		 );
   	        cache.addIdOnly(template);
   	    	return template;
   	    }
   	    
   	    
   	    // Only called by DbSetup
   	    public static ReportTemplate _dbsetup_createTemplate(Group superGroup, Group systemAdminGroup)
   	    {
   	        EsfDateTime nowTimestamp = new EsfDateTime();

   	        EsfUUID deployId = Application.getInstance().getDeployId();
   	        
   	        ReportTemplate template = new ReportTemplate( new EsfUUID(), TEMPLATE_ESFPATHNAME, "Template only", "Sample report template.", null, nowTimestamp, deployId, 
   	    							       nowTimestamp, deployId, Literals.STATUS_DISABLED, null, new LinkedList<EsfUUID>(), new LinkedList<EsfUUID>(), new LinkedList<ReportTemplateReportField>() );
   	        template.setComments("The report template used as the root for creating all other runnable reports. Admins generally create a template for each company/group so they can create their own report templates with similar permission.");

   	    	String normalizedId = template.getId().toNormalizedEsfNameString();
   	    	EsfPathName permPathName = new EsfPathName(PERM_PATH_PREFIX+normalizedId);
   	    	template.permission = Permission.Manager._dbsetup_createReportTemplateForSuperGroup(template.getId(), permPathName, "Sample report template permissions", superGroup);
   	    	template.permission.addGroupToAllOptions(systemAdminGroup);
   	        cache.addIdOnly(template);
   	    	return template;
   	    }
   	    

   	    /**
   	     * Returns the total number of report templates objects in the cache
   	     * @return the total number of templates in the cache
   	     */
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all reports that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}

   	} // Manager
   	
}