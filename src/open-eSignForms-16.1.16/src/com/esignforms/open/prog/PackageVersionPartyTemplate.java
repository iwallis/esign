// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.User;
import com.esignforms.open.util.XmlUtil;

/**
* PackageVersionPartyTemplate holds the definition of a single party defined in a package version.  
* It is mapped to one or more party names defined in the package's documents.
* 
* @author Yozons, Inc.
*/
public class PackageVersionPartyTemplate
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<PackageVersionPartyTemplate>, java.io.Serializable
{
	private static final long serialVersionUID = 4153895564535134686L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackageVersionPartyTemplate.class);
    
    public static final String LANDING_PAGE_PACKAGE = "P";
    public static final String LANDING_PAGE_FIRST_DOC = "1";
    public static final String LANDING_PAGE_FIRST_TODO_OR_FIRST_DOC = "2";
    public static final String LANDING_PAGE_FIRST_TODO_OR_PACKAGE = "3";

    protected EsfUUID id;
    protected EsfUUID packageVersionId; 
    protected EsfName esfname;
    protected String displayName;
    protected short processOrder;
    protected EsfName libraryPartyTemplateTodoEsfName;
    protected String notifyAllTodo; // Y=Yes; N=No
    protected EsfName libraryEmailTemplateEsfName;
    protected String emailFieldExpression; // defines an email address or ${} field expression that expands to one
    protected String completedUrlFieldExpression; // defines an URL or ${} field expression that expands to one to redirect to if party completes all docs
    protected String notCompletedUrlFieldExpression; // defines an URL or ${} field expression that expands to one to redirect to if user does not complete all docs
    protected String renotifySpec; // semicolon separated list of renotification specs
    protected String requireLogin; // Y=Yes; N=No
    protected String allowDeleteTranIfUnsigned; // Y=Yes; N=No
    protected String landingPage; 
    protected EsfUUID overridePackageDocumentId; // Optional; if present, overrides package version's package+disclosure document for this party
    protected EsfUUID overrideButtonMessageId; // Optional; if present, overrides package version's button message set for this party
    
    // These only have values when the party template is being imported; accessed directly from the PackageVersion when importing
    EsfName _importedOverridePackageDocumentName;
    EsfName _importedOverrideButtonMessageName;
    
    protected List<PackageVersionPartyTemplateDocumentParty> docVerPartyTemplates;
    protected List<PackageVersionPartyTemplateDocumentParty> docVerPartiesToRemoveOnSave;

    /**
     * This creates a PackageVersionPartyTemplate object from data retrieved from the DB.
     */
    protected PackageVersionPartyTemplate(EsfUUID id, EsfUUID packageVersionId, short processOrder, EsfName esfname, String displayName,
    		EsfName libraryPartyTemplateTodoEsfName, String notifyAllTodo, EsfName libraryEmailTemplateEsfName, 
    		String emailFieldExpression, String completedUrlFieldExpression, String notCompletedUrlFieldExpression, String renotifySpec, 
    		String requireLogin, String allowDeleteTranIfUnsigned, String landingPage, 
    		EsfUUID overridePackageDocumentId, EsfUUID overrideButtonMessageId,
    		List<PackageVersionPartyTemplateDocumentParty> docVerPartyTemplates)
    {
    	this.id = id;
    	this.packageVersionId = packageVersionId;
    	this.processOrder = processOrder;
    	this.esfname = esfname;
    	this.displayName = displayName;
    	this.libraryPartyTemplateTodoEsfName = libraryPartyTemplateTodoEsfName;
    	this.notifyAllTodo = notifyAllTodo;
    	this.libraryEmailTemplateEsfName = libraryEmailTemplateEsfName;
    	this.emailFieldExpression = emailFieldExpression;
    	this.completedUrlFieldExpression = completedUrlFieldExpression;
    	this.notCompletedUrlFieldExpression = notCompletedUrlFieldExpression;
    	setRenotifySpec(renotifySpec);
    	this.requireLogin = requireLogin;
    	this.allowDeleteTranIfUnsigned = allowDeleteTranIfUnsigned;
    	this.landingPage = landingPage;
    	this.overridePackageDocumentId = overridePackageDocumentId != null && overridePackageDocumentId.isNull() ? null : overridePackageDocumentId;
    	this.overrideButtonMessageId = overrideButtonMessageId != null && overrideButtonMessageId.isNull() ? null : overrideButtonMessageId;
    	this.docVerPartyTemplates = docVerPartyTemplates;
    }
    
    // Creates a new package party with default values
    // Creates a new package party like a given party
    protected PackageVersionPartyTemplate(EsfUUID newPackageVersionId, PackageVersionPartyTemplate likeTemplate)
    {
    	this.id = new EsfUUID();
    	this.packageVersionId = newPackageVersionId;
    	this.processOrder = likeTemplate.processOrder;
    	this.esfname = likeTemplate.esfname;
    	this.displayName = likeTemplate.displayName;
    	this.libraryPartyTemplateTodoEsfName = likeTemplate.libraryPartyTemplateTodoEsfName == null ? null : likeTemplate.libraryPartyTemplateTodoEsfName.duplicate();
    	this.notifyAllTodo = likeTemplate.notifyAllTodo;
    	this.libraryEmailTemplateEsfName = likeTemplate.libraryEmailTemplateEsfName == null ? null : likeTemplate.libraryEmailTemplateEsfName.duplicate();
    	this.emailFieldExpression = likeTemplate.emailFieldExpression;
    	this.completedUrlFieldExpression = likeTemplate.completedUrlFieldExpression;
    	this.notCompletedUrlFieldExpression = likeTemplate.notCompletedUrlFieldExpression;
    	setRenotifySpec(likeTemplate.renotifySpec);
    	this.requireLogin = likeTemplate.requireLogin;
    	this.allowDeleteTranIfUnsigned = likeTemplate.allowDeleteTranIfUnsigned;
    	this.landingPage = likeTemplate.landingPage;
    	this.overridePackageDocumentId = likeTemplate.overridePackageDocumentId;
    	this.overrideButtonMessageId = likeTemplate.overrideButtonMessageId;
    	this.docVerPartyTemplates = new LinkedList<PackageVersionPartyTemplateDocumentParty>();
    	for( PackageVersionPartyTemplateDocumentParty pt : likeTemplate.getPackageVersionPartyTemplateDocumentParties() )
    	{
    		PackageVersionPartyTemplateDocumentParty newPT = PackageVersionPartyTemplateDocumentParty.Manager.createLike(id,pt);
    		this.docVerPartyTemplates.add(newPT);
    	}
    }
    
    // Creates a new package party with default values
    protected PackageVersionPartyTemplate(EsfUUID newPackageVersionId, EsfName name, String displayName)
    {
    	this.id = new EsfUUID();
    	this.packageVersionId = newPackageVersionId;
    	this.processOrder = 1;
    	this.esfname = name;
    	this.displayName = displayName;
    	this.libraryPartyTemplateTodoEsfName = null;
    	this.notifyAllTodo = Literals.N;
    	this.libraryEmailTemplateEsfName = Application.getInstance().getEmailTemplateDefaultPickupNotificationEsfName();
    	this.emailFieldExpression = null;
    	this.completedUrlFieldExpression = null;
    	this.notCompletedUrlFieldExpression = null;
    	setRenotifySpec(null);
    	this.requireLogin = Literals.N;
    	this.allowDeleteTranIfUnsigned = Literals.N;
    	this.landingPage = LANDING_PAGE_PACKAGE;
    	this.overridePackageDocumentId = null;
		this.overrideButtonMessageId = null;
    	this.docVerPartyTemplates = new LinkedList<PackageVersionPartyTemplateDocumentParty>();
    }
    
    // Creates a new package party like a given party package, used for cloning the data so updates to that object don't reflect the original
    public PackageVersionPartyTemplate duplicate()
    {
    	List<PackageVersionPartyTemplateDocumentParty> newList = new LinkedList<PackageVersionPartyTemplateDocumentParty>();
    	for( PackageVersionPartyTemplateDocumentParty pt : docVerPartyTemplates )
    		newList.add(pt.duplicate());
    	PackageVersionPartyTemplate template = new PackageVersionPartyTemplate(id, packageVersionId, processOrder, esfname.duplicate(), displayName,
    			libraryPartyTemplateTodoEsfName == null ? null : libraryPartyTemplateTodoEsfName.duplicate(), notifyAllTodo,
    			libraryEmailTemplateEsfName == null ? null : libraryEmailTemplateEsfName.duplicate(), 
    		    emailFieldExpression, completedUrlFieldExpression, notCompletedUrlFieldExpression, renotifySpec, 
    			requireLogin, allowDeleteTranIfUnsigned, landingPage, overridePackageDocumentId, overrideButtonMessageId, newList);
    	template.setDatabaseObjectLike(this);
    	return template;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public EsfUUID getPackageVersionId()
    {
        return packageVersionId;
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    public void setEsfName(EsfName v) // package version party cannot be named the built in view parties
    {
    	if ( v == null || ! v.isValid() )
    		return;
    	if ( v.hasReservedPrefix() && ! PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS.equals(v) ) // the only reserved name allowed is the reports access name
    		return;
    	esfname = v;
    	objectChanged();
    }
    public boolean hasReservedPrefix() 
    {
    	return esfname.hasReservedPrefix();
    }
    public boolean isEsfReportsAccess()
    {
    	return PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS.equals(esfname);
    }
    
    public short getProcessOrder()
    {
    	return processOrder;
    }
    public void setProcessOrder(short v)
    {
    	if ( processOrder != v )
    	{
        	processOrder = v;
        	objectChanged();
    	}
    }
    
    public String getDisplayName()
    {
    	return displayName;
    }
    public void setDisplayName(String v)
    {
    	if ( EsfString.isNonBlank(v) )
    	{
        	if ( v.length() > Literals.DISPLAY_NAME_MAX_LENGTH )
        		displayName = v.substring(0,Literals.DISPLAY_NAME_MAX_LENGTH).trim();
            else
            	displayName = v.trim();
    		objectChanged();
    	}
    }
    
    public EsfName getLibraryPartyTemplateTodoEsfName()
    {
    	return libraryPartyTemplateTodoEsfName;
    }
    public boolean hasLibraryPartyTemplateTodoEsfName()
    {
    	return libraryPartyTemplateTodoEsfName != null;
    }
    public void setLibraryPartyTemplateTodoEsfName(EsfName v)
    {
    	libraryPartyTemplateTodoEsfName = v == null || ! v.isValid() ? null : v;
    	objectChanged();
    }
    
    public String getNotifyAllTodo()
    {
        return notifyAllTodo;
    }
    public boolean isNotifyAllTodo()
    {
        return Literals.Y.equals(notifyAllTodo);
    }
    public void makeNotifyAllTodo()
    {
    	notifyAllTodo = Literals.Y;
        objectChanged();
    }
    public void makeNotNotifyAllTodo()
    {
    	notifyAllTodo = Literals.N;
        objectChanged();
    }
    public void setNotifyAllTodo(String v)
    {
        if ( Literals.Y.equals(v) )
        	makeNotifyAllTodo();
        else
        	makeNotNotifyAllTodo();
    }
    public void setNotifyAllTodo(boolean v)
    {
    	if ( v )  
    		makeNotifyAllTodo();
    	else
    		makeNotNotifyAllTodo();
    }
  
    public EsfName getLibraryEmailTemplateEsfName()
    {
    	return libraryEmailTemplateEsfName;
    }
    public boolean hasLibraryEmailTemplateEsfName()
    {
    	return libraryEmailTemplateEsfName != null;
    }
    public void setLibraryEmailTemplateEsfName(EsfName v)
    {
    	libraryEmailTemplateEsfName = v == null || ! v.isValid() ? null : v;
    	objectChanged();
    }
    
    public String getEmailFieldExpression()
    {
    	return emailFieldExpression;
    }
    public boolean hasEmailFieldExpression()
    {
    	return EsfString.isNonBlank(emailFieldExpression);
    }
    public void setEmailFieldExpression(String v)
    {
    	if ( v == null )
    		emailFieldExpression = v;
    	else if ( v.length() > Literals.FIELD_EXPRESSION_MAX_LENGTH )
    		emailFieldExpression = v.substring(0,Literals.FIELD_EXPRESSION_MAX_LENGTH).trim();
        else
        	emailFieldExpression = v.trim();
    	objectChanged();
    }
    
    public String getCompletedUrlFieldExpression()
    {
    	return completedUrlFieldExpression;
    }
    public boolean hasCompletedUrlFieldExpression()
    {
    	return EsfString.isNonBlank(completedUrlFieldExpression);
    }
    public void setCompletedUrlFieldExpression(String v)
    {
    	if ( v == null )
    		completedUrlFieldExpression = v;
    	else if ( v.length() > Literals.FIELD_EXPRESSION_MAX_LENGTH )
    		completedUrlFieldExpression = v.substring(0,Literals.FIELD_EXPRESSION_MAX_LENGTH).trim();
        else
        	completedUrlFieldExpression = v.trim();
    	objectChanged();
    }
    
    public String getNotCompletedUrlFieldExpression()
    {
    	return notCompletedUrlFieldExpression;
    }
    public boolean hasNotCompletedUrlFieldExpression()
    {
    	return EsfString.isNonBlank(notCompletedUrlFieldExpression);
    }
    public void setNotCompletedUrlFieldExpression(String v)
    {
    	if ( v == null )
    		notCompletedUrlFieldExpression = v;
    	else if ( v.length() > Literals.FIELD_EXPRESSION_MAX_LENGTH )
    		notCompletedUrlFieldExpression = v.substring(0,Literals.FIELD_EXPRESSION_MAX_LENGTH).trim();
        else
        	notCompletedUrlFieldExpression = v.trim();
    	objectChanged();
    }
    
    private String NEVER_RENOTIFY_SPEC = "0 day";
    public String getRenotifySpec()
    {
    	return renotifySpec;
    }
    public boolean isRenotifyNever()
    {
    	return EsfString.isBlank(renotifySpec) || NEVER_RENOTIFY_SPEC.equals(renotifySpec);
    }
    public String[] getRenotifySpecs()
    {
    	if ( renotifySpec == null )
    	{
    		String[] r = new String[1];
    		r[0] = NEVER_RENOTIFY_SPEC;
    		return r;
    	}
    	return renotifySpec.split(";");
    }
    public void setRenotifySpec(String v)
    {
    	if ( v == null )
    		renotifySpec = v;
    	else if ( v.length() > Literals.DESCRIPTION_MAX_LENGTH )
    		renotifySpec = v.substring(0,Literals.RENOTIFY_SPEC_MAX_LENGTH).trim();
        else
        	renotifySpec = v.trim();
    	if ( EsfString.isBlank(renotifySpec) || renotifySpec.startsWith(NEVER_RENOTIFY_SPEC+";") || renotifySpec.indexOf(";"+NEVER_RENOTIFY_SPEC) > 0 ) // if "never" is specified, it's all that's allowed
    		renotifySpec = NEVER_RENOTIFY_SPEC;
    	objectChanged();
    }
    public EsfDateTime[] getRenotificationDateTimes()
    {
    	if ( isRenotifyNever() )
    		return null;
    	
    	String[] specs = getRenotifySpecs();
    	EsfDateTime[] dateTimes = new EsfDateTime[specs.length];
    	for( int i=0; i < specs.length; ++i )
    	{
    		String[] spec = specs[i].split("\\s");
    		int numIntervals = Application.getInstance().stringToInt(spec[0], 0);
    		String interval = spec[1];
    		
    		if ( numIntervals < 1 )
    		{
    			_logger.warn("getRenotificationDateTimes() - id: " + id + "; invalid spec number: '" + specs[i] + "; full spec: " + getRenotifySpec());
    			dateTimes[i] = null;
    		}
    		else
    		{
    			dateTimes[i] = new EsfDateTime();
    			dateTimes[i].addTimeInterval(numIntervals, interval);
    		}
    	}
    	
    	return dateTimes;
    }
    
    public String getRequireLogin()
    {
        return requireLogin;
    }
    public boolean isRequireLogin()
    {
        return Literals.Y.equals(requireLogin);
    }
    public void makeRequireLogin()
    {
    	requireLogin = Literals.Y;
        objectChanged();
    }
    public void makeNotRequireLogin()
    {
    	requireLogin = Literals.N;
        objectChanged();
    }
    public void setRequireLogin(String v)
    {
    	setRequireLogin( Literals.Y.equals(v) );
    }
    public void setRequireLogin(boolean v)
    {
    	if ( v )  
    		makeRequireLogin();
    	else
    		makeNotRequireLogin();
    }
    
    public String getAllowDeleteTranIfUnsigned()
    {
        return allowDeleteTranIfUnsigned;
    }
    public boolean isAllowDeleteTranIfUnsigned()
    {
        return Literals.Y.equals(allowDeleteTranIfUnsigned);
    }
    public void makeAllowDeleteTranIfUnsigned()
    {
    	allowDeleteTranIfUnsigned = Literals.Y;
        objectChanged();
    }
    public void makeDisallowDeleteTranIfUnsigned()
    {
    	allowDeleteTranIfUnsigned = Literals.N;
        objectChanged();
    }
    public void setAllowDeleteTranIfUnsigned(String v)
    {
    	setAllowDeleteTranIfUnsigned( Literals.Y.equals(v) );
    }
    public void setAllowDeleteTranIfUnsigned(boolean v)
    {
    	if ( v )  
    		makeAllowDeleteTranIfUnsigned();
    	else
    		makeDisallowDeleteTranIfUnsigned();
    }
    
    public String getLandingPage()
    {
    	return landingPage;
    }
    public boolean isLandingPagePackage()
    {
    	return LANDING_PAGE_PACKAGE.equals(landingPage);
    }
    public boolean isLandingPageFirstDoc()
    {
    	return LANDING_PAGE_FIRST_DOC.equals(landingPage);
    }
    public boolean isLandingPageFirstToDoOrFirstDoc()
    {
    	return LANDING_PAGE_FIRST_TODO_OR_FIRST_DOC.equals(landingPage);
    }
    public boolean isLandingPageFirstToDoOrPackage()
    {
    	return LANDING_PAGE_FIRST_TODO_OR_PACKAGE.equals(landingPage);
    }
    public void setLandingPage(String v)
    {
    	landingPage = v == null ? LANDING_PAGE_PACKAGE : v.trim();
    	objectChanged();
    }
    public void setLandingPagePackage()
    {
    	setLandingPage(LANDING_PAGE_PACKAGE);
    }
    public void setLandingPageFirstDoc()
    {
    	setLandingPage(LANDING_PAGE_FIRST_DOC);
    }
    public void setLandingPageFirstToDoOrFirstDoc()
    {
    	setLandingPage(LANDING_PAGE_FIRST_TODO_OR_FIRST_DOC);
    }
    public void setLandingFirstToDoOrPackage()
    {
    	setLandingPage(LANDING_PAGE_FIRST_TODO_OR_PACKAGE);
    }
    
    public EsfUUID getOverridePackageDocumentId()
    {
        return overridePackageDocumentId;
    }
    public Document getOverridePackageDocument()
    {
    	return Document.Manager.getById(overridePackageDocumentId);
    }
    public boolean hasOverridePackageDocument()
    {
    	return overridePackageDocumentId != null && ! overridePackageDocumentId.isNull();
    }
    public void setOverridePackageDocumentId(EsfUUID v)
    {
    	overridePackageDocumentId = v != null && v.isNull() ? null : v;
    	objectChanged();
    }
    
    
    public EsfUUID getOverrideButtonMessageId()
    {
        return overrideButtonMessageId;
    }
    public ButtonMessage getOverrideButtonMessage()
    {
    	return ButtonMessage.Manager.getById(overrideButtonMessageId);
    }
    public boolean hasOverrideButtonMessage()
    {
    	return overrideButtonMessageId != null && ! overrideButtonMessageId.isNull();
    }
    public void setOverrideButtonMessageId(EsfUUID v)
    {
	 	overrideButtonMessageId = v != null && v.isNull() ? null : v;
    	objectChanged();
    }
    
    public synchronized List<PackageVersionPartyTemplateDocumentParty> getPackageVersionPartyTemplateDocumentParties()
    {
    	return new LinkedList<PackageVersionPartyTemplateDocumentParty>(docVerPartyTemplates);
    }
    public synchronized int getNumPackageVersionPartyTemplateDocumentParties() 
    {
    	return docVerPartyTemplates.size();
    }
    public synchronized void setPackageVersionPartyTemplateDocumentParties(List<PackageVersionPartyTemplateDocumentParty> docVerPartyTemplates)
    {
    	this.docVerPartyTemplates = docVerPartyTemplates;
    	objectChanged();
    }
    public synchronized void addPackageVersionPartyTemplateDocumentParty(PackageVersionPartyTemplateDocumentParty docVerPartyTemplate)
    {
    	List<PackageVersionPartyTemplateDocumentParty> newList = getPackageVersionPartyTemplateDocumentParties();
    	newList.add(docVerPartyTemplate);
    	setPackageVersionPartyTemplateDocumentParties(newList);
    }
    public synchronized void removePackageVersionPartyTemplateDocumentParty(PackageVersionPartyTemplateDocumentParty docVerPartyTemplate)
    {
    	if ( docVerPartyTemplates.remove(docVerPartyTemplate) )
    	{
    		objectChanged();
    		// If we've not even saved this package party, there's no need to delete the mapping to a document/party.
    		if ( doUpdate() )
    		{
            	if ( docVerPartiesToRemoveOnSave == null )
            		docVerPartiesToRemoveOnSave = new LinkedList<PackageVersionPartyTemplateDocumentParty>();
            	docVerPartiesToRemoveOnSave.add(docVerPartyTemplate);
    		}
    	}
    }
    
    public synchronized PackageVersionPartyTemplateDocumentParty getPackageVersionPartyTemplateDocumentPartyByDocumentIdPartyTemplateName(EsfUUID docId, EsfName docPartyTemplateName)
    {
    	for( PackageVersionPartyTemplateDocumentParty partyMapping : docVerPartyTemplates )
    	{
    		if ( partyMapping.getDocumentId().equals(docId) && partyMapping.isMatchingDocumentPartyTemplateName(docPartyTemplateName) )
    			return partyMapping;
    	}
    	return null;
    }
    
    public synchronized List<PackageVersionPartyTemplateDocumentParty> getPackageVersionPartyTemplateDocumentPartiesByDocumentId(EsfUUID docId)
    {
    	List<PackageVersionPartyTemplateDocumentParty> list = new LinkedList<PackageVersionPartyTemplateDocumentParty>();
    	for( PackageVersionPartyTemplateDocumentParty partyMapping : docVerPartyTemplates )
    	{
    		if ( partyMapping.getDocumentId().equals(docId) )
    			list.add(partyMapping);
    	}
    	return list;
    }
    
    public List<PackageVersionPartyTemplateDocumentParty> getPackageVersionPartyTemplateDocumentPartiesByDocumentIds(List<EsfUUID> docIdList)
    {
    	List<PackageVersionPartyTemplateDocumentParty> list = new LinkedList<PackageVersionPartyTemplateDocumentParty>();
    	for( EsfUUID docId : docIdList )
    	{
    		list.addAll(getPackageVersionPartyTemplateDocumentPartiesByDocumentId(docId));
    	}
    	return list;
    }
    
    public synchronized boolean isPartyToDocument(EsfUUID docId)
    {
    	for( PackageVersionPartyTemplateDocumentParty partyMapping : docVerPartyTemplates )
    	{
    		if ( partyMapping.getDocumentId().equals(docId) )
    			return true;
    	}
    	return false;
    }

    // The package version party is view only for the specified document only if we find it in the list and the document party is view only
    public synchronized boolean isViewOnly(EsfUUID docId)
    {
    	boolean found = false;
    	for( PackageVersionPartyTemplateDocumentParty partyMapping : docVerPartyTemplates )
    	{
    		if ( partyMapping.getDocumentId().equals(docId) )
    		{
    			found = true;
    			if ( ! partyMapping.isDocumentPartyTemplateViewOnly() )
    				return false;
    		}
    	}
    	return found;
    }

    public synchronized boolean isViewOptional(EsfUUID docId)
    {
    	boolean found = false;
    	for( PackageVersionPartyTemplateDocumentParty partyMapping : docVerPartyTemplates )
    	{
    		if ( partyMapping.getDocumentId().equals(docId) )
    		{
    			found = true;
    			if ( ! partyMapping.isDocumentPartyTemplateViewOptional() )
    				return false;
    		}
    	}
    	return found;
    }

    public synchronized boolean isView(EsfUUID docId)
    {
    	boolean found = false;
    	for( PackageVersionPartyTemplateDocumentParty partyMapping : docVerPartyTemplates )
    	{
    		if ( partyMapping.getDocumentId().equals(docId) )
    		{
    			found = true;
    			if ( ! partyMapping.isDocumentPartyTemplateViewOnly() && ! partyMapping.isDocumentPartyTemplateViewOptional() )
    				return false;
    		}
    	}
    	return found;
    }
    
    public void fixupReservedParties()
    {
    	if ( isEsfReportsAccess() )
    	{
    		setLibraryPartyTemplateTodoEsfName(null);
    		makeNotNotifyAllTodo();
    		setLibraryEmailTemplateEsfName(null);
    		setEmailFieldExpression(null);
    		setCompletedUrlFieldExpression(null);
    		setNotCompletedUrlFieldExpression(null);
    		setRenotifySpec(null);
    		makeRequireLogin();
    	}
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof PackageVersionPartyTemplate )
            return getId().equals(((PackageVersionPartyTemplate)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(PackageVersionPartyTemplate o)
    {
    	return getProcessOrder() != o.getProcessOrder() ? (getProcessOrder() - o.getProcessOrder()) : getEsfName().compareTo(o.getEsfName());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<PackageVersionPartyTemplate xmlns=\"").append(XmlUtil.getXmlNamespace2013()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <packageVersionId>").append(packageVersionId.toXml()).append("</packageVersionId>\n");
        buf.append(" <esfname>").append(esfname.toXml()).append("</esfname>\n");
        buf.append(" <displayName>").append(escapeXml(displayName)).append("</displayName>\n");
        buf.append(" <processOrder>").append(processOrder).append("</processOrder>\n");
        if ( libraryPartyTemplateTodoEsfName != null )
        	buf.append(" <libraryPartyTemplateTodoEsfName>").append(libraryPartyTemplateTodoEsfName.toXml()).append("</libraryPartyTemplateTodoEsfName>\n");
       	buf.append(" <notifyAllTodo>").append(escapeXml(notifyAllTodo)).append("</notifyAllTodo>\n");
       	if ( libraryEmailTemplateEsfName != null )
       		buf.append(" <libraryEmailTemplateEsfName>").append(libraryEmailTemplateEsfName.toXml()).append("</libraryEmailTemplateEsfName>\n");
       	buf.append(" <emailFieldExpression>").append(escapeXml(emailFieldExpression)).append("</emailFieldExpression>\n");
       	buf.append(" <completedUrlFieldExpression>").append(escapeXml(completedUrlFieldExpression)).append("</completedUrlFieldExpression>\n");
       	buf.append(" <notCompletedUrlFieldExpression>").append(escapeXml(notCompletedUrlFieldExpression)).append("</notCompletedUrlFieldExpression>\n");
       	buf.append(" <renotifySpec>").append(escapeXml(renotifySpec)).append("</renotifySpec>\n");
       	buf.append(" <requireLogin>").append(escapeXml(requireLogin)).append("</requireLogin>\n");
       	buf.append(" <allowDeleteTranIfUnsigned>").append(escapeXml(allowDeleteTranIfUnsigned)).append("</allowDeleteTranIfUnsigned>\n");
       	buf.append(" <landingPage>").append(escapeXml(landingPage)).append("</landingPage>\n");
        buf.append(" <overridePackageDocumentId>").append(hasOverridePackageDocument() ? overridePackageDocumentId.toXml() : "").append("</overridePackageDocumentId>\n");
        buf.append(" <overridePackageDocumentEsfName>").append(hasOverridePackageDocument() ? getOverridePackageDocument().getEsfName().toXml() : "").append("</overridePackageDocumentEsfName>\n");
        buf.append(" <overrideButtonMessageId>").append(hasOverrideButtonMessage() ? overrideButtonMessageId.toXml() : "").append("</overrideButtonMessageId>\n");
        buf.append(" <overrideButtonMessageEsfName>").append(hasOverrideButtonMessage() ? getOverrideButtonMessage().getEsfName().toXml() : "").append("</overrideButtonMessageEsfName>\n");
        for( PackageVersionPartyTemplateDocumentParty pvptdp : docVerPartyTemplates )
        	pvptdp.appendXml(buf);
        buf.append("</PackageVersionPartyTemplate>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 113 + 730 + id.getEstimatedLengthXml() + packageVersionId.getEstimatedLengthXml() + esfname.getEstimatedLengthXml();
        if ( displayName != null )
        	len += displayName.length();
        len += 4; // processOrder
        if ( libraryPartyTemplateTodoEsfName != null )
        	len += 71 + libraryPartyTemplateTodoEsfName.getEstimatedLengthXml();
        if ( libraryEmailTemplateEsfName != null )
        	len += 61 + libraryEmailTemplateEsfName.getEstimatedLengthXml();
        len += notifyAllTodo.length();
        if ( emailFieldExpression != null )
        	len += emailFieldExpression.length();
        if ( completedUrlFieldExpression != null )
        	len += completedUrlFieldExpression.length();
        if ( notCompletedUrlFieldExpression != null )
        	len += notCompletedUrlFieldExpression.length();
        if ( renotifySpec != null )
        	len += renotifySpec.length();
        len += requireLogin.length() + allowDeleteTranIfUnsigned.length() + landingPage.length();
        if ( hasOverridePackageDocument() )
        	len += overridePackageDocumentId.getEstimatedLengthXml() + getOverridePackageDocument().getEsfName().getEstimatedLengthXml();
        if ( hasOverrideButtonMessage() )
        	len += overrideButtonMessageId.getEstimatedLengthXml() + getOverrideButtonMessage().getEsfName().getEstimatedLengthXml();
        for( PackageVersionPartyTemplateDocumentParty pvptdp : docVerPartyTemplates )
        	len += pvptdp.getEstimatedLengthXml();
        return len; 
    }

    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
 
	public synchronized boolean save(final Connection con)
		throws SQLException
	{
		_logger.debug("save(con) on id: " + id + "; esfname: " + esfname + "; insert: " + doInsert());
	    
	    clearLastSQLException();
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        if ( doInsert() )
	        {
	            stmt = new EsfPreparedStatement(con, 
	            	"INSERT INTO esf_package_version_party_template " +
	            	"(id,package_version_id,process_order,esfname,display_name,library_party_template_todo_esfname,library_email_template_esfname," + 
	            	"email_field_expression,completed_url_field_expression,notcompleted_url_field_expression,renotify_spec," +
	            	"require_login,allow_delete_tran_if_unsigned,notify_all_todo,landing_page,override_package_document_id,override_button_message_id) " +
	            	"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	            stmt.set(id);
	            stmt.set(packageVersionId);
	            stmt.set(processOrder);
	            stmt.set(esfname);
	            stmt.set(displayName);
	            stmt.set(libraryPartyTemplateTodoEsfName);
	            stmt.set(libraryEmailTemplateEsfName);
	            stmt.set(emailFieldExpression);
	            stmt.set(completedUrlFieldExpression);
	            stmt.set(notCompletedUrlFieldExpression);
	   	        stmt.set(renotifySpec);
	            stmt.set(requireLogin);
	            stmt.set(allowDeleteTranIfUnsigned);
	            stmt.set(notifyAllTodo);
	            stmt.set(landingPage);
	            stmt.set(overridePackageDocumentId);
	            stmt.set(overrideButtonMessageId);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Insert failed for id: " + id + "; esfname: " + esfname);
	                return false;
	            }
	            
	            for( PackageVersionPartyTemplateDocumentParty pt : docVerPartyTemplates )
	            {
	            	if ( ! pt.save(con) )
	            		return false;
	            }
	            
	            // Now we mark this object as if it were loaded fresh from the database
	            setLoadedFromDb();
	            
	            return true;
	        }
	        
	        if ( docVerPartiesToRemoveOnSave != null ) 
	        {
	        	for( PackageVersionPartyTemplateDocumentParty pvpdp : docVerPartiesToRemoveOnSave )
	        		pvpdp.delete(con,null);
	        	docVerPartiesToRemoveOnSave.clear();
	        }
	        
	        if ( hasChanged() )
	        {
	            for( PackageVersionPartyTemplateDocumentParty pvpdp : docVerPartyTemplates )
	            {
	            	if ( ! pvpdp.save(con) )
	            		return false;
	            }

	            // We assume we'll update it instead
	            stmt = new EsfPreparedStatement( con, 
	            		"UPDATE esf_package_version_party_template " +
	            		"SET process_order=?,esfname=?,display_name=?,library_party_template_todo_esfname=?,library_email_template_esfname=?," +
	            		"email_field_expression=?,completed_url_field_expression=?,notcompleted_url_field_expression=?,renotify_spec=?," +
	            		"require_login=?,allow_delete_tran_if_unsigned=?,notify_all_todo=?,landing_page=?,override_package_document_id=?,override_button_message_id=? " +
	            		"WHERE id=?"
	            						   		);
	            stmt.set(processOrder);
	            stmt.set(esfname);
	            stmt.set(displayName);
	            stmt.set(libraryPartyTemplateTodoEsfName);
	            stmt.set(libraryEmailTemplateEsfName);
	            stmt.set(emailFieldExpression);
	            stmt.set(completedUrlFieldExpression);
	            stmt.set(notCompletedUrlFieldExpression);
	            stmt.set(renotifySpec);
	            stmt.set(requireLogin);
	            stmt.set(allowDeleteTranIfUnsigned);
	            stmt.set(notifyAllTodo);
	            stmt.set(landingPage);
	            stmt.set(overridePackageDocumentId);
	            stmt.set(overrideButtonMessageId);
	            stmt.set(id);
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Update failed for id: " + id + "; esfname: " + esfname);
	            }
	        }
	
	        // Now we mark this object as if it were loaded fresh from the database
	        setLoadedFromDb();
	        
	        return true;
	    }
	    catch(SQLException e)
	    {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; esfname: " + esfname + "; insert: " + doInsert());
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}
    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }


    synchronized boolean delete(final Connection con, final Errors errors, final User user)
	    throws SQLException
	{
		_logger.debug("delete(con) on id: " + id + "; esfname: " + esfname + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
	    
	    clearLastSQLException();
	
	    // Let's do some referential integrity checks to be sure it's not in use 
	    if ( ! checkReferential(con,errors) )
	        return false;
	
	    if ( doInsert() )
	    {
	    	_logger.warn("delete(con) - Ignored delete of package version party template that was pending an INSERT id: " + id + "; esfname: " + esfname);
	        objectDeleted();
	        return true;
	    }
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        // Delete the package version party
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_package_version_party_template WHERE id=?");
	        stmt.set(id);
	        int num = stmt.executeUpdate();
	        if ( num == 0 )
	        	_logger.warn("delete(con) - Failed to find esf_package_version_party_template database row to delete with id: " + id);
	        
	        // If a package party template is deleted, we remove any document party mappings associated with it
	        stmt.close();
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_package_version_party_document_party WHERE package_party_template_id=?");
	        stmt.set(id);
	        num = stmt.executeUpdate();
	        
	        objectDeleted();
	        
	        if ( user != null ) 
	        {
	        	user.logConfigChange(con, "Deleted package party template " + getEsfName()); 
	        	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted package party template " + getEsfName());
	        }
	
	        return true;
	    }
	    catch(SQLException e)
	    {
	    	_logger.sqlerr(e,"delete(con) on id: " + id + "; esfname: " + esfname + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}

	public boolean delete(final Errors errors, final User user)
	{
	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        boolean result = delete(con,errors,user);
	        con.commit();
	        return result;
	    }
	    catch(SQLException e) 
	    {
	        pool.rollbackIgnoreException(con,e);
	        return false;
	    }
	    finally
	    {
	    	Application.getInstance().cleanupPool(pool,con,null);
	    }
	}
	public synchronized boolean delete(final User user)
	{
	    Errors nullErrors = null;
	    return delete(nullErrors,user);
	}
	public synchronized boolean delete()
	{
	    User nullUser = null;
	    return delete(nullUser);
	}
    
	protected boolean checkReferential(final Connection con, final Errors errors)
    	throws SQLException
    {
		if ( esfname.hasReservedPrefix() && ! PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS.equals(esfname) )
		{
	    	_logger.error("checkReferential() FAILED RESERVE PREFIX on id: " + id + "; esfname: " + esfname);
	    	if ( errors != null )
	    		errors.addError("You cannot delete the reserved special package party template: " + getEsfName());
			return false;
		}
		return true;
	}

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all PackageVersionPartyTemplate objects held in the specified package version.
   	     * @return the List of PackageVersionPartyTemplate found indexed by process order.
   	     */
   	    public static List<PackageVersionPartyTemplate> getAll(EsfUUID packageVersionId)
   	    {
   	    	_logger.debug("Manager.getAll() - packageVersionId: " + packageVersionId);
   	    	
   	    	LinkedList<PackageVersionPartyTemplate> partyList = new LinkedList<PackageVersionPartyTemplate>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id,process_order,esfname,display_name,library_party_template_todo_esfname,library_email_template_esfname," +
   	    	            "email_field_expression,completed_url_field_expression,notcompleted_url_field_expression,renotify_spec," +
   	    	            "require_login,allow_delete_tran_if_unsigned,notify_all_todo,landing_page,override_package_document_id,override_button_message_id " +
   	    	            "FROM esf_package_version_party_template WHERE package_version_id=? ORDER BY process_order ASC"
   	            						   );
   	        	stmt.set(packageVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	short order = rs.getShort();
		            EsfName esfname = rs.getEsfName();
		            String displayName = rs.getString();
		            EsfName libraryPartyTemplateTodoEsfName = rs.getEsfName();
		            EsfName libraryEmailTemplateEsfName = rs.getEsfName();
		            String emailFieldExpression = rs.getString();
		            String completedUrlFieldExpression = rs.getString();
		            String notCompletedUrlFieldExpression = rs.getString();
		            String renotifySpec = rs.getString();
		            String requireLogin = rs.getString();
		            String allowDeleteTranIfUnsigned = rs.getString();
		            String notifyAllTodo = rs.getString();
		            String landingPage = rs.getString();
		            EsfUUID overridePackageDocumentId = rs.getEsfUUID();
		            EsfUUID overrideButtonMessageId = rs.getEsfUUID();
		            
		            List<PackageVersionPartyTemplateDocumentParty> docParties = PackageVersionPartyTemplateDocumentParty.Manager.getAllForParty(id);
		            
		            PackageVersionPartyTemplate party = new PackageVersionPartyTemplate(id,packageVersionId,order,esfname,displayName,libraryPartyTemplateTodoEsfName,notifyAllTodo,
		            		libraryEmailTemplateEsfName,emailFieldExpression,completedUrlFieldExpression,notCompletedUrlFieldExpression,renotifySpec,
		            		requireLogin,allowDeleteTranIfUnsigned,landingPage,overridePackageDocumentId,overrideButtonMessageId,docParties);
		            party.setLoadedFromDb();
		            partyList.add(party);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - packageVersionId: " + packageVersionId + "; found: " + partyList.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - packageVersionId: " + packageVersionId + "; found: " + partyList.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	         partyList.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return partyList;
   	    }
   	    
   	    public static PackageVersionPartyTemplate getById(EsfUUID packageVersionPartyTemplateId)
   	    {
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT package_version_id,process_order,esfname,display_name,library_party_template_todo_esfname,library_email_template_esfname,email_field_expression,completed_url_field_expression,notcompleted_url_field_expression,renotify_spec,require_login,allow_delete_tran_if_unsigned,notify_all_todo,landing_page,override_package_document_id,override_button_message_id " +
   	    	            "FROM esf_package_version_party_template WHERE id=?"
   	            						   );
   	        	stmt.set(packageVersionPartyTemplateId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID packageVersionId = rs.getEsfUUID();
	            	short order = rs.getShort();
		            EsfName esfname = rs.getEsfName();
		            String displayName = rs.getString();
		            EsfName libraryPartyTemplateTodoEsfName = rs.getEsfName();
		            EsfName libraryEmailTemplateEsfName = rs.getEsfName();
		            String emailFieldExpression = rs.getString();
		            String completedUrlFieldExpression = rs.getString();
		            String notCompletedUrlFieldExpression = rs.getString();
		            String renotifySpec = rs.getString();
		            String requireLogin = rs.getString();
		            String allowDeleteTranIfUnsigned = rs.getString();
		            String notifyAllTodo = rs.getString();
		            String landingPage = rs.getString();
		            EsfUUID overridePackageDocumentId = rs.getEsfUUID();
		            EsfUUID overrideButtonMessageId = rs.getEsfUUID();
		            
		            List<PackageVersionPartyTemplateDocumentParty> docParties = PackageVersionPartyTemplateDocumentParty.Manager.getAllForParty(packageVersionPartyTemplateId);
		            
		            PackageVersionPartyTemplate party = new PackageVersionPartyTemplate(packageVersionPartyTemplateId,packageVersionId,order,esfname,displayName,libraryPartyTemplateTodoEsfName,notifyAllTodo,
		            		libraryEmailTemplateEsfName,emailFieldExpression,completedUrlFieldExpression,notCompletedUrlFieldExpression,renotifySpec,
		            		requireLogin,allowDeleteTranIfUnsigned,landingPage,overridePackageDocumentId,overrideButtonMessageId,docParties);
		            party.setLoadedFromDb();
	   	            con.commit();
		            return party;
	            }
	            
   	            con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - packageVersionPartyTemplateId: " + packageVersionPartyTemplateId);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return null;
   	    }
   	    
   	    public static PackageVersionPartyTemplate createLike(EsfUUID newPackageVersionId, PackageVersionPartyTemplate likeTemplate)
   	    {
   	    	PackageVersionPartyTemplate newTemplate = new PackageVersionPartyTemplate(newPackageVersionId, likeTemplate);
   	    	return newTemplate;
   	    }
   	    
   	    public static PackageVersionPartyTemplate createNew(EsfUUID newPackageVersionId, EsfName name, String displayName)
   	    {
   	    	PackageVersionPartyTemplate newTemplate = new PackageVersionPartyTemplate(newPackageVersionId,name,displayName);
   	    	return newTemplate;
   	    }
   	    
   	    
   	    public static PackageVersionPartyTemplate createFromJDOM(Element element, EsfUUID packageVersionId, Map<EsfUUID,EsfUUID> idMap)
   	    {
   	    	EsfName esfname = new EsfName(element.getChildText("esfname", element.getNamespace()));
   	    	String displayName = element.getChildTextTrim("displayName", element.getNamespace());
   	    	
   	    	PackageVersionPartyTemplate pvpt = createNew(packageVersionId, esfname, displayName);
   	    	
   	    	EsfName libraryPartyTemplateTodoEsfName = new EsfName(element.getChildText("libraryPartyTemplateTodoEsfName", element.getNamespace()));
   	    	if ( libraryPartyTemplateTodoEsfName.isValid() )
   	    		pvpt.setLibraryPartyTemplateTodoEsfName(libraryPartyTemplateTodoEsfName);
   	    	
   	    	pvpt.setNotifyAllTodo(element.getChildTextTrim("notifyAllTodo", element.getNamespace()));

   	    	EsfName libraryEmailTemplateEsfName = new EsfName(element.getChildText("libraryEmailTemplateEsfName", element.getNamespace()));
   	    	if ( libraryEmailTemplateEsfName.isValid() )
   	    		pvpt.setLibraryEmailTemplateEsfName(libraryEmailTemplateEsfName);
   	    	
   	    	pvpt.setEmailFieldExpression(element.getChildTextTrim("emailFieldExpression", element.getNamespace()));
   	    	pvpt.setCompletedUrlFieldExpression(element.getChildTextTrim("completedUrlFieldExpression", element.getNamespace()));
   	    	pvpt.setNotCompletedUrlFieldExpression(element.getChildTextTrim("notCompletedUrlFieldExpression", element.getNamespace()));
   	    	pvpt.setRenotifySpec(element.getChildTextTrim("renotifySpec", element.getNamespace()));
   	    	pvpt.setRequireLogin(element.getChildTextTrim("requireLogin", element.getNamespace()));
   	    	pvpt.setAllowDeleteTranIfUnsigned(element.getChildTextTrim("allowDeleteTranIfUnsigned", element.getNamespace()));
   	    	pvpt.setLandingPage(element.getChildTextTrim("landingPage", element.getNamespace()));
   	
   	    	pvpt.setOverridePackageDocumentId(new EsfUUID(element.getChildTextTrim("overridePackageDocumentId", element.getNamespace())));
   	    	pvpt.setOverrideButtonMessageId(new EsfUUID(element.getChildTextTrim("overrideButtonMessageId", element.getNamespace())));
   	    	
   	    	// Save these so the import can use to adjust as needed for the new system
   	    	if ( pvpt.hasOverridePackageDocument() )
   	    		pvpt._importedOverridePackageDocumentName = new EsfName(element.getChildTextTrim("overridePackageDocumentEsfName", element.getNamespace()));
   	    	if ( pvpt.hasOverrideButtonMessage() )
   	    		pvpt._importedOverrideButtonMessageName = new EsfName(element.getChildTextTrim("overrideButtonMessageEsfName", element.getNamespace()));
   	    	

   	    	List<Element> packageVersionPartyTemplateDocumentPartyElements = element.getChildren("PackageVersionPartyTemplateDocumentParty", element.getNamespace());
   	    	for( Element packageVersionPartyTemplateDocumentPartyElement : packageVersionPartyTemplateDocumentPartyElements ) 
   	    	{
   	    		PackageVersionPartyTemplateDocumentParty pvptdp = PackageVersionPartyTemplateDocumentParty.Manager.createFromJDOM(packageVersionPartyTemplateDocumentPartyElement,pvpt.getId(),idMap);
   	    		pvpt.addPackageVersionPartyTemplateDocumentParty(pvptdp);
   	    	}
   	    	
   	    	return pvpt;
   	    }
   	    
   	} // Manager

}