// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;

/**
* DocumentStyleInfo is a data-only object that holds information about a document style selection list.
* It's not a mutable object (you can't save/delete it), just for lists of document styles.
* 
* @author Yozons, Inc.
*/
public class DocumentStyleInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<DocumentStyleInfo>, java.io.Serializable
{
	private static final long serialVersionUID = 4525853905723432858L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DocumentStyleInfo.class);

    protected EsfUUID id;
    protected EsfName esfname;
    protected String  description;
    protected String  status;
    protected int	  productionVersion;
    protected int	  testVersion;

    /**
     * This creates a new DocumentStyleInfo using the source object.
     * @param documentstyle the documentstyle to use as the source
     */
    protected DocumentStyleInfo(DocumentStyle documentstyle)
    {
    	this.id = documentstyle.getId();
    	this.esfname = documentstyle.getEsfName();
    	this.description = documentstyle.getDescription();
    	this.status = documentstyle.getStatus();
    	this.productionVersion = documentstyle.getProductionVersion();
    	this.testVersion = documentstyle.getTestVersion();
    }
    
    /**
     * This creates a DocumentStyleInfo object from data retrieved from the DB.
     */
    protected DocumentStyleInfo(EsfUUID id, EsfName esfname, String description, String status, int productionVersion, int testVersion)
    {
    	this.id = id;
    	this.esfname = esfname;
    	this.description = description;
    	this.status = status;
    	this.productionVersion = productionVersion;
    	this.testVersion = testVersion;
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }

    public int getProductionVersion()
    {
    	return productionVersion;
    }
    
    public int getTestVersion()
    {
    	return testVersion;
    }

    public boolean hasProductionVersion()
    {
    	return productionVersion > 0;
    }

    public boolean hasTestVersion()
    {
    	return testVersion > productionVersion;
    }

    public EsfUUID getId()
    {
        return id;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof DocumentStyleInfo )
            return getId().equals(((DocumentStyleInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(DocumentStyleInfo d)
    {
    	return getEsfName().compareTo(d.getEsfName());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all DocumentStyleInfo objects in the specified library.
   	     * @return the Collection of DocumentStyleInfo found ordered by esfname.
   	     */
   	    public static Collection<DocumentStyleInfo> getAll(Library library)
   	    {
   	    	LinkedList<DocumentStyleInfo> list = new LinkedList<DocumentStyleInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id,esfname,description,status,production_version,test_version " +
   	    	            "FROM esf_library_documentstyle " + 
   	    	            "WHERE library_id=? " +
   	    	            "ORDER BY lower(esfname) ASC"
   	            						   );
   	        	stmt.set(library.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            EsfName esfname = rs.getEsfName();
		            String description = rs.getString();
		            String  stat = rs.getString();
		            int productionVersion = rs.getInt();
		            int testVersion = rs.getInt();
		            
		            DocumentStyleInfo docInfo = new DocumentStyleInfo(id,esfname,description,stat,productionVersion,testVersion);
		            docInfo.setLoadedFromDb();
		            list.add(docInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - Library: " + library.getPathName() + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - Library: " + library.getPathName() + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static DocumentStyleInfo createNew(Library library)
   	    {
   	    	if ( library == null )
   	    		return null;
   	    	
   			DocumentStyle documentstyle = DocumentStyle.Manager.createNew(library.getId());
   	    	return new DocumentStyleInfo(documentstyle);
   	    }
   	    
   	    public static DocumentStyleInfo createLike(DocumentStyle likeDocumentStyle, EsfName newName)
   	    {
   	    	if ( likeDocumentStyle == null || newName == null || ! newName.isValid() )
   	    		return null;

   	    	DocumentStyle newDocumentStyle = DocumentStyle.Manager.createLike(likeDocumentStyle.getLibraryId(), likeDocumentStyle, newName);
   	    	return new DocumentStyleInfo(newDocumentStyle);
   	    }
   	    
   	    public static DocumentStyleInfo createFromSource(DocumentStyle documentstyle)
   	    {
   	    	if ( documentstyle == null )
   	    		return null;
   	    	
   	    	DocumentStyleInfo docInfo = new DocumentStyleInfo(documentstyle);
   	    	docInfo.setLoadedFromDb();

   			return docInfo;
   	    }
   	    
   	} // Manager
   	
}