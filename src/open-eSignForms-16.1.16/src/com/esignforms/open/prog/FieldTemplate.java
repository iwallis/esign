// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Map;
import java.util.TreeMap;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.User;
import com.esignforms.open.util.XmlUtil;

/**
* FieldTemplate holds the definition of a single field that appears in a document or is
* otherwise stored along with the document/package.
* 
* @author Yozons, Inc.
*/
public class FieldTemplate
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<FieldTemplate>, java.io.Serializable
{
	private static final long serialVersionUID = -1196266756073628195L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(FieldTemplate.class);

    // Convention: capitals are for HTML input type elements, lowercase for "derived types" that use other input elements but have special formats
    public static final String TYPE_GENERAL			= "I"; // <input type="text">
    public static final String TYPE_CHECKBOX		= "C"; // <input type="checkbox">
    public static final String TYPE_RICHTEXT		= "E"; // <textarea> with CKEditor replacing it
    public static final String TYPE_RADIO_BUTTON	= "R"; // <input type="radio">
    public static final String TYPE_RADIO_BUTTON_GROUP = "G"; // Used to hold the data value/validations of one or more radio buttons
    public static final String TYPE_FILE			= "F"; // <input type="file">
    public static final String TYPE_SELECTION		= "S"; // <select>
    public static final String TYPE_TEXTAREA		= "T"; // <textarea>

    public static final String TYPE_CREDIT_CARD		= "c"; // <input type="text"> GENERAL with validations and masking
    public static final String TYPE_DATE			= "d"; // <input type="text"> GENERAL with Calendar
    public static final String TYPE_DATETIME		= "t"; // <input type="text"> doesn't do input yet, but likely will be GENERAL with Calendar+HHMMSS textfield+Timezone select
    public static final String TYPE_DECIMAL			= "."; // <input type="text"> GENERAL with validations
    public static final String TYPE_EMAIL_ADDRESS	= "e"; // <input type="text"> GENERAL with validations
    public static final String TYPE_FILE_CONFIRM_CLICK = "f"; // <a href>
    public static final String TYPE_INTEGER			= "i"; // <input type="text"> GENERAL with validations
    public static final String TYPE_MONEY			= "$"; // <input type="text"> GENERAL with validations
    public static final String TYPE_PHONE			= "P"; // <input type="text"> GENERAL with validations
    public static final String TYPE_SIGNATURE		= "s"; // <input type="text"> GENERAL with signature CSS
    public static final String TYPE_SIGN_DATE		= "D"; // <input type="text"> doesn't really do input and will display current date on review with signature CSS
    public static final String TYPE_SSN_EIN			= "n"; // <input type="text"> GENERAL with validations and masking
    public static final String TYPE_ZIP_CODE		= "z"; // <input type="text"> GENERAL with validations
    
    // Used to present a list of all types available in a select box, and the order is maintained so the first appears at the top.
    public static final String[] SELECT_LIST_OF_ALL_TYPES = 
    {TYPE_GENERAL,TYPE_CHECKBOX,TYPE_CREDIT_CARD,TYPE_DATE,TYPE_DATETIME,TYPE_DECIMAL,TYPE_SELECTION,TYPE_EMAIL_ADDRESS,TYPE_FILE,TYPE_FILE_CONFIRM_CLICK,TYPE_RICHTEXT,TYPE_INTEGER,TYPE_MONEY,TYPE_PHONE,TYPE_RADIO_BUTTON,TYPE_RADIO_BUTTON_GROUP,TYPE_SIGNATURE,TYPE_SIGN_DATE,TYPE_SSN_EIN,TYPE_TEXTAREA,TYPE_ZIP_CODE};

    public static boolean isTypeCheckbox(String v) { return TYPE_CHECKBOX.equals(v); }
    public static boolean isTypeFileConfirmClick(String v) { return TYPE_FILE_CONFIRM_CLICK.equals(v); }
    public static boolean isTypeCreditCard(String v) { return TYPE_CREDIT_CARD.equals(v); }
    public static boolean isTypeDate(String v) { return TYPE_DATE.equals(v); }
    public static boolean isTypeDateTime(String v) { return TYPE_DATETIME.equals(v); }
    public static boolean isTypeDecimal(String v) { return TYPE_DECIMAL.equals(v); }
    public static boolean isTypeEmailAddress(String v) { return TYPE_EMAIL_ADDRESS.equals(v); }
    public static boolean isTypeFile(String v) { return TYPE_FILE.equals(v); }
    public static boolean isTypeGeneral(String v) { return TYPE_GENERAL.equals(v); }
    public static boolean isTypeInteger(String v) { return TYPE_INTEGER.equals(v); }
    public static boolean isTypeMoney(String v) { return TYPE_MONEY.equals(v); }
    public static boolean isTypePhone(String v) { return TYPE_PHONE.equals(v); }
    public static boolean isTypeRadioButton(String v) { return TYPE_RADIO_BUTTON.equals(v); }
    public static boolean isTypeRadioButtonGroup(String v) { return TYPE_RADIO_BUTTON_GROUP.equals(v); }
    public static boolean isTypeRichTextarea(String v) { return TYPE_RICHTEXT.equals(v); }
    public static boolean isTypeSelection(String v) { return TYPE_SELECTION.equals(v); }
    public static boolean isTypeSignature(String v) { return TYPE_SIGNATURE.equals(v); }
    public static boolean isTypeSignDate(String v) { return TYPE_SIGN_DATE.equals(v); }
    public static boolean isTypeSsnEin(String v) { return TYPE_SSN_EIN.equals(v); }
    public static boolean isTypeTextarea(String v) { return TYPE_TEXTAREA.equals(v); }
    public static boolean isTypeZipCode(String v) { return TYPE_ZIP_CODE.equals(v); }

    public static final String REQUIRED = "Y";
    public static final String OPTIONAL = "N";
    public static final String OPTIONAL_STYLE_REQUIRED = "n"; // behaves like OPTIONAL, looks/styles like REQUIRED.

    public static final String AUTO_COMPLETE_ALLOWED = "Y";
    public static final String AUTO_COMPLETE_NOT_ALLOWED = "N";

    public static final String AUTO_POST_ALLOWED = "Y";
    public static final String AUTO_POST_NOT_ALLOWED = "N";

    public static final String MASK_IN_DATA_SNAPSHOT_ALLOWED = "Y";
    public static final String MASK_IN_DATA_SNAPSHOT_NOT_ALLOWED = "N";

    public static final String MASK_INPUT_ALLOWED = "Y";
    public static final String MASK_INPUT_NOT_ALLOWED = "N";

    public static final String CLONE_ALLOWED = "Y";
    public static final String CLONE_NOT_ALLOWED = "N";
    
    public static final String DISPLAY_EMPTY_VALUE_ENABLED = "Y";
    public static final String DISPLAY_EMPTY_VALUE_DISABLED = "N";

    public static final String WIDTH_UNIT_AUTO = "A";
    public static final String WIDTH_UNIT_PERCENT = "%";
    public static final String WIDTH_UNIT_PIXELS = "P";
    public static final String WIDTH_UNIT_EM = "M";
    public static final String WIDTH_UNIT_EX = "x";
    
    public static final String WIDTH_IGNORE_ON_OUTPUT = "Y";
    public static final String WIDTH_KEEP_ON_OUTPUT = "N";

    public static final String BORDER_NONE = "NNNN";
    public static final String BORDER_BOX = "YYYY";
    public static final String BORDER_SET = "Y";
    public static final String BORDER_CLEAR = "N";
    public static final int BORDER_TOP = 0;
    public static final int BORDER_RIGHT = 1;
    public static final int BORDER_BOTTOM = 2;
    public static final int BORDER_LEFT = 3;

    public static final String ALIGN_INHERIT = "I";
    public static final String ALIGN_LEFT = "L";
    public static final String ALIGN_CENTER = "C";
    public static final String ALIGN_RIGHT = "R";
    
	// Fields that have this path suffix are blocked from further party edits.
	public static final EsfName BLOCK_FURTHER_PARTY_EDITS_PATH_SUFFIX = new EsfName(EsfName.ESF_RESERVED_PREFIX+"blockFurtherPartyEdits");
	public static final EsfName VIEW_DOWNLOAD_OPTIONAL_PATH_SUFFIX = new EsfName(EsfName.ESF_RESERVED_PREFIX+"viewDownloadOptional");
	public static final EsfName VIEW_DOWNLOAD_PROHIBITED_PATH_SUFFIX = new EsfName(EsfName.ESF_RESERVED_PREFIX+"viewDownloadProhibited");

    protected EsfUUID id;
    protected EsfUUID containerId; // The ID of whatever container (document, package, etc.) "owns" this field template.
    protected short   containerReferenceCount; // Number of times this field is referenced in the container
    protected EsfName esfname;
    protected String  type;
    protected short	  width;
    protected String  widthUnit;
    protected String  ignoreWidthOnOutput; // when set, if the field is for output/display/review, we ignore the width setting.
    protected String  border;
    protected String  align; 
    protected String  required;
    protected String  autoComplete;
    protected String  autoPost;
    protected String  maskInDataSnapshot;
    protected String  maskInput;
    protected String  cloneAllowed;
    protected int	  minLength;
    protected int	  maxLength;
    protected String  initialValue;
    protected String  displayEmptyValueEnabled;
    protected String  displayEmptyValue;
    protected String  inputFormatSpec;
    protected String  outputFormatSpec;
    protected String  extraOptions;
    protected String  tooltip;
    protected String  inputPrompt;
    protected LabelTemplate labelTemplate;

    /**
     * This creates a FieldTemplate object from data retrieved from the DB.
     */
    protected FieldTemplate(EsfUUID id, EsfUUID containerId, short containerReferenceCount, EsfName esfname, String type, 
    						short width, String widthUnit, String ignoreWidthOnOutput, String border, String align,
    						String required, String autoComplete, String autoPost, String maskInDataSnapshot, String maskInput, String cloneAllowed, int minLength, int maxLength, 
    						String initialValue, String displayEmptyValueEnabled, String displayEmptyValue, 
    						String inputFormatSpec, String outputFormatSpec, String extraOptions, String tooltip, String inputPrompt, LabelTemplate labelTemplate
    					   )
    {
    	this.id = id;
    	this.containerId = containerId;
    	this.containerReferenceCount = containerReferenceCount;
    	this.esfname = esfname;
    	this.type = type;
    	this.width = width;
    	this.widthUnit = widthUnit;
    	this.ignoreWidthOnOutput = ignoreWidthOnOutput;
    	this.border = border;
    	this.align = align;
    	this.required = required;
    	this.autoComplete = autoComplete;
    	setAutoPost(autoPost);
    	this.maskInDataSnapshot = maskInDataSnapshot;
    	setMaskInput(maskInput);
    	this.cloneAllowed = cloneAllowed;
    	this.minLength = minLength;
    	this.maxLength = maxLength;
    	this.initialValue = initialValue;
    	this.displayEmptyValueEnabled = displayEmptyValueEnabled;
    	this.displayEmptyValue = displayEmptyValue;
    	this.inputFormatSpec = inputFormatSpec;
    	this.outputFormatSpec = outputFormatSpec;
    	this.extraOptions = extraOptions;
    	this.tooltip = tooltip;
    	this.inputPrompt = inputPrompt;
    	this.labelTemplate = labelTemplate;
    }
    
    // Creates a new field like a given field
    protected FieldTemplate(EsfUUID newContainerId, FieldTemplate likeTemplate)
    {
    	this.id = new EsfUUID();
    	this.containerId = newContainerId;
    	this.containerReferenceCount = likeTemplate.containerReferenceCount;
    	this.esfname = likeTemplate.esfname;
    	this.type = likeTemplate.type;
    	this.width = likeTemplate.width;
    	this.widthUnit = likeTemplate.widthUnit;
    	this.ignoreWidthOnOutput = likeTemplate.ignoreWidthOnOutput;
    	this.border = likeTemplate.border;
    	this.align = likeTemplate.align;
    	this.required = likeTemplate.required;
    	this.autoComplete = likeTemplate.autoComplete;
    	this.autoPost = likeTemplate.autoPost;
    	this.maskInDataSnapshot = likeTemplate.maskInDataSnapshot;
    	this.maskInput = likeTemplate.maskInput;
    	this.cloneAllowed = likeTemplate.cloneAllowed;
    	this.minLength = likeTemplate.minLength;
    	this.maxLength = likeTemplate.maxLength;
    	this.initialValue = likeTemplate.initialValue;
    	this.displayEmptyValueEnabled = likeTemplate.displayEmptyValueEnabled;
    	this.displayEmptyValue = likeTemplate.displayEmptyValue;
    	this.inputFormatSpec = likeTemplate.inputFormatSpec;
    	this.outputFormatSpec = likeTemplate.outputFormatSpec;
    	this.extraOptions = likeTemplate.extraOptions;
    	this.tooltip = likeTemplate.tooltip;
    	this.inputPrompt = likeTemplate.inputPrompt;
    	this.labelTemplate = LabelTemplate.Manager.createLike(likeTemplate.getLabelTemplate());
    }
    
    // Creates a new field with default values
    protected FieldTemplate(EsfUUID newContainerId, EsfName name)
    {
    	this.id = new EsfUUID();
    	this.containerId = newContainerId;
    	this.containerReferenceCount = 0;
    	this.esfname = name;
    	this.type = TYPE_GENERAL;
    	this.width = 100;
    	this.widthUnit = WIDTH_UNIT_AUTO;
    	this.ignoreWidthOnOutput = WIDTH_KEEP_ON_OUTPUT;
    	this.border = BORDER_NONE;
    	this.align = ALIGN_INHERIT;
    	this.required = OPTIONAL;
    	this.autoComplete = AUTO_COMPLETE_ALLOWED;
    	this.autoPost = AUTO_POST_NOT_ALLOWED;
    	this.maskInDataSnapshot = MASK_IN_DATA_SNAPSHOT_NOT_ALLOWED;
    	this.maskInput = MASK_INPUT_NOT_ALLOWED;
    	this.cloneAllowed = CLONE_ALLOWED;
    	this.minLength = 0;
    	this.maxLength = 100;
    	this.initialValue = this.displayEmptyValue = this.inputFormatSpec = this.tooltip = this.inputPrompt = this.extraOptions = null;
    	this.displayEmptyValueEnabled = DISPLAY_EMPTY_VALUE_DISABLED;
    	this.outputFormatSpec = "standard";
    	this.labelTemplate = LabelTemplate.Manager.createNew();
    	this.labelTemplate.setLabel(EsfString.capitalizeFirstLetter(esfname.toPlainString()));
    }
    
    public FieldTemplate duplicate()
    {
    	FieldTemplate ft = new FieldTemplate(id, containerId, containerReferenceCount, esfname.duplicate(), type, 
    											width, widthUnit, ignoreWidthOnOutput, border, align,
    											required, autoComplete, autoPost, maskInDataSnapshot, maskInput, cloneAllowed, minLength, maxLength, 
    											initialValue, displayEmptyValueEnabled, displayEmptyValue, 
    											inputFormatSpec, outputFormatSpec, extraOptions, tooltip, inputPrompt, labelTemplate.duplicate());
    	ft.setDatabaseObjectLike(this);
    	return ft;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public EsfUUID getContainerId()
    {
        return containerId;
    }
    
    public short getContainerReferenceCount()
    {
    	return containerReferenceCount;
    }
    public void bumpContainerReferenceCount()
    {
    	++containerReferenceCount;
    	objectChanged();
    }

    public void resetContainerReferenceCount() 
    {
    	containerReferenceCount = 0;
    	objectChanged();
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    public void setEsfName(EsfName v)
    {
    	if ( v == null || ! v.isValid() )
    		return;
    	esfname = v;
    	objectChanged();
    }
    
    public String getType()
    {
    	return type;
    }
    public void setType(String v)
    {
    	type = v == null ? TYPE_GENERAL : v.trim();
    	objectChanged();
    }
    public boolean isTypeCheckbox() { return isTypeCheckbox(type); }
    public boolean isTypeCreditCard() { return isTypeCreditCard(type); }
    public boolean isTypeDate() { return isTypeDate(type); }
    public boolean isTypeDateTime() { return isTypeDateTime(type); }
    public boolean isTypeDecimal() { return isTypeDecimal(type); }
    public boolean isTypeEmailAddress() { return isTypeEmailAddress(type); }
    public boolean isTypeFile() { return isTypeFile(type); }
    public boolean isTypeFileConfirmClick() { return isTypeFileConfirmClick(type); }
    public boolean isTypeGeneral() { return isTypeGeneral(type); }
    public boolean isTypeInteger() { return isTypeInteger(type); }
    public boolean isTypeMoney() { return isTypeMoney(type); }
    public boolean isTypePhone() { return isTypePhone(type); }
    public boolean isTypeRadioButton() { return isTypeRadioButton(type); }
    public boolean isTypeRadioButtonGroup() { return isTypeRadioButtonGroup(type); }
    public boolean isTypeRichTextarea() { return isTypeRichTextarea(type); }
    public boolean isTypeSelection() { return isTypeSelection(type); }
    public boolean isTypeSignature() { return isTypeSignature(type); }
    public boolean isTypeSignDate() { return isTypeSignDate(type); }
    public boolean isTypeSsnEin() { return isTypeSsnEin(type); }
    public boolean isTypeTextarea() { return isTypeTextarea(type); }
    public boolean isTypeZipCode() { return isTypeZipCode(type); }
    
    public boolean isTypeCanCalculate() {
    	return isTypeDecimal() || isTypeInteger() || isTypeMoney();
    }
    
    public short getWidth()
    {
    	return width;
    }
    public void setWidth(short v)
    {
    	width = v < 1 ? 1 : v;
		objectChanged();
    }
    
    public String getWidthUnit()
    {
    	return widthUnit;
    }
    public boolean isWidthUnitPercent()
    {
    	return WIDTH_UNIT_PERCENT.equals(widthUnit);
    }
    public boolean isWidthUnitPixels()
    {
    	return WIDTH_UNIT_PIXELS.equals(widthUnit);
    }
    public boolean isWidthUnitEm()
    {
    	return WIDTH_UNIT_EM.equals(widthUnit);
    }
    public boolean isWidthUnitEx()
    {
    	return WIDTH_UNIT_EX.equals(widthUnit);
    }
    public boolean isWidthUnitAuto()
    {
    	return WIDTH_UNIT_AUTO.equals(widthUnit);
    }
    public void setWidthUnit(String v)
    {
    	widthUnit = v == null ? WIDTH_UNIT_AUTO : v.trim();
		objectChanged();
    }
    public String getWidthStyle()
    {
    	StringBuilder css = new StringBuilder();
    	css.append("width: ");
    	if ( isWidthUnitAuto() )
    		css.append("auto");
    	else
    	{
        	css.append(getWidth());
        	if ( isWidthUnitPercent() )
        		css.append('%');
        	else if ( isWidthUnitPixels() )
        		css.append("px");
        	else if ( isWidthUnitEm() )
        		css.append("em");
        	else if ( isWidthUnitEx() )
        		css.append("ex");
    	}
    	css.append(';');
    	return css.toString();
    }
    public String getWidthStyleOut()
    {
    	return isIgnoreWidthOnOutput() ? "width: auto;" : getWidthStyle();
    }
    
    public String getIgnoreWidthOnOutput()
    {
    	return ignoreWidthOnOutput;
    }
    public boolean isIgnoreWidthOnOutput()
    {
    	return WIDTH_IGNORE_ON_OUTPUT.equals(ignoreWidthOnOutput);
    }
    public void setIgnoreWidthOnOutput(String v)
    {
    	ignoreWidthOnOutput = v == null ? WIDTH_KEEP_ON_OUTPUT : v.trim();
    	objectChanged();
    }
    
    
    public String getBorder()
    {
    	return border;
    }
    public boolean isBorderTop()
    {
    	return EsfBoolean.toBoolean(border.charAt(BORDER_TOP));
    }
    public boolean isBorderRight()
    {
    	return EsfBoolean.toBoolean(border.charAt(BORDER_RIGHT));
    }
    public boolean isBorderBottom()
    {
    	return EsfBoolean.toBoolean(border.charAt(BORDER_BOTTOM));
    }
    public boolean isBorderLeft()
    {
    	return EsfBoolean.toBoolean(border.charAt(BORDER_LEFT));
    }
    public void setBorder(String v)
    {
    	v = v == null ? BORDER_NONE : v.trim();
		if ( v.length() == 4 )
		{
    		border = v;
    		objectChanged();
		}
    }
    public void setBorderTop(boolean v)
    {
    	StringBuilder b = new StringBuilder(border);
    	b.setCharAt(BORDER_TOP, v ? BORDER_SET.charAt(0) : BORDER_CLEAR.charAt(0));
    	setBorder(b.toString());
    }
    public void setBorderRight(boolean v)
    {
    	StringBuilder b = new StringBuilder(border);
    	b.setCharAt(BORDER_RIGHT, v ? BORDER_SET.charAt(0) : BORDER_CLEAR.charAt(0));
    	setBorder(b.toString());
    }
    public void setBorderBottom(boolean v)
    {
    	StringBuilder b = new StringBuilder(border);
    	b.setCharAt(BORDER_BOTTOM, v ? BORDER_SET.charAt(0) : BORDER_CLEAR.charAt(0));
    	setBorder(b.toString());
    }
    public void setBorderLeft(boolean v)
    {
    	StringBuilder b = new StringBuilder(border);
    	b.setCharAt(BORDER_LEFT, v ? BORDER_SET.charAt(0) : BORDER_CLEAR.charAt(0));
    	setBorder(b.toString());
    }
    public String getBorderCssClass()
    {
    	if ( BORDER_NONE.equals(border) )
    		return "";
    	if ( BORDER_BOX.equals(border) )
    		return "box";
    	StringBuilder css = new StringBuilder();
    	if ( isBorderTop() )
    		css.append("boT");
    	if ( isBorderRight() )
    	{
    		if ( css.length() > 0 )
    			css.append(' ');
    		css.append("boR");
    	}
    	if ( isBorderBottom() )
    	{
    		if ( css.length() > 0 )
    			css.append(' ');
    		css.append("boB");
    	}
    	if ( isBorderLeft() )
    	{
    		if ( css.length() > 0 )
    			css.append(' ');
    		css.append("boL");
    	}
    	return css.toString();
    }
    
    public String getAlign()
    {
    	return align;
    }
    public boolean isAlignInherit()
    {
    	return ALIGN_INHERIT.equals(align);
    }
    public boolean isAlignLeft()
    {
    	return ALIGN_LEFT.equals(align);
    }
    public boolean isAlignCenter()
    {
    	return ALIGN_CENTER.equals(align);
    }
    public boolean isAlignRight()
    {
    	return ALIGN_RIGHT.equals(align);
    }
    public String getTextAlignCss()
    {
    	if ( isAlignLeft() )
    		return "text-align: left;";
    	if ( isAlignCenter() )
    		return "text-align: center;";
    	if ( isAlignRight() )
    		return "text-align: right;";
    	return "text-align: inherit;";
    }
    public void setAlign(String v)
    {
    	align = v == null ? ALIGN_INHERIT : v.trim();
    	objectChanged();
    }
    public void setAlignFromTextAlignCss(String css) // we get this set from a DropDown ESF_TextAlign that contains CSS styles
    {
    	if ( css.indexOf("left") > 0 )
    		align = ALIGN_LEFT;
    	else if ( css.indexOf("center") > 0 )
    		align = ALIGN_CENTER;
    	else if ( css.indexOf("right") > 0 )
    		align = ALIGN_RIGHT;
    	else 
    		align = ALIGN_INHERIT;
    }

    
    public LabelTemplate getLabelTemplate()
    {
    	return labelTemplate;
    }
    public void setLabelTemplate(LabelTemplate v)
    {
    	labelTemplate = v;
    	objectChanged();
    }
    
    public String getRequired()
    {
    	return required;
    }
    public void setRequired(String v)
    {
    	if ( REQUIRED.equals(v) )
    		required = REQUIRED;
    	else if ( OPTIONAL_STYLE_REQUIRED.equals(v) )
    		required = OPTIONAL_STYLE_REQUIRED;
    	else 
    		required = OPTIONAL;
    	objectChanged();
    }
    public boolean isRequired()
    {
    	return REQUIRED.equals(required);
    }
    public boolean isOptional()
    {
    	return OPTIONAL.equals(required) || isOptionalStyleRequired();
    }
    public boolean isOptionalStyleRequired()
    {
    	return OPTIONAL_STYLE_REQUIRED.equals(required);
    }
    
    public String getAutoComplete()
    {
    	return autoComplete;
    }
    public void setAutoComplete(String v)
    {
    	autoComplete = AUTO_COMPLETE_ALLOWED.equals(v) ? AUTO_COMPLETE_ALLOWED : AUTO_COMPLETE_NOT_ALLOWED;
    	objectChanged();
    }
    public boolean isAutoCompleteAllowed()
    {
    	return AUTO_COMPLETE_ALLOWED.equals(autoComplete);
    }
    public boolean isAutoCompleteNotAllowed()
    {
    	return AUTO_COMPLETE_NOT_ALLOWED.equals(autoComplete);
    }
    
    public String getAutoPost()
    {
    	return autoPost;
    }
    public void setAutoPost(String v)
    {
    	autoPost = AUTO_POST_ALLOWED.equals(v) ? AUTO_POST_ALLOWED : AUTO_POST_NOT_ALLOWED;
    	objectChanged();
    }
    public boolean isAutoPostAllowed()
    {
    	return AUTO_POST_ALLOWED.equals(autoPost);
    }
    public boolean isAutoPostNotAllowed()
    {
    	return AUTO_POST_NOT_ALLOWED.equals(autoPost);
    }
    
    public String getMaskInDataSnapshot()
    {
    	return maskInDataSnapshot;
    }
    public void setMaskInDataSnapshot(String v)
    {
    	maskInDataSnapshot = MASK_IN_DATA_SNAPSHOT_ALLOWED.equals(v) ? MASK_IN_DATA_SNAPSHOT_ALLOWED : MASK_IN_DATA_SNAPSHOT_NOT_ALLOWED;
    	objectChanged();
    }
    public boolean isMaskInDataSnapshot()
    {
    	return MASK_IN_DATA_SNAPSHOT_ALLOWED.equals(maskInDataSnapshot);
    }

    public String getMaskInput()
    {
    	return maskInput;
    }
    public void setMaskInput(String v)
    {
    	maskInput = MASK_INPUT_ALLOWED.equals(v) ? MASK_INPUT_ALLOWED : MASK_INPUT_NOT_ALLOWED;
    	objectChanged();
    }
    public boolean isMaskInput()
    {
    	return MASK_INPUT_ALLOWED.equals(maskInput);
    }

    public String getCloneAllowed()
    {
    	return cloneAllowed;
    }
    public void setCloneAllowed(String v)
    {
    	cloneAllowed = CLONE_ALLOWED.equals(v) ? CLONE_ALLOWED : CLONE_NOT_ALLOWED;
    	objectChanged();
    }
    public boolean isCloneAllowed()
    {
    	return CLONE_ALLOWED.equals(cloneAllowed);
    }
    
    public int getMinLength()
    {
    	return minLength;
    }
    public void setMinLength(int v)
    {
    	minLength = v;
    	if ( maxLength < minLength )
    		maxLength = minLength;
    	objectChanged();
    }
    
    public int getMaxLength()
    {
    	return maxLength;
    }
    public void setMaxLength(int v)
    {
    	maxLength = v;
    	if ( maxLength < minLength )
    		maxLength = minLength;
    	objectChanged();
    }
    
    public String getInitialValue()
    {
    	return initialValue;
    }
    public boolean hasInitialValue()
    {
    	return EsfString.isNonBlank(initialValue);
    }
    public boolean hasLiteralInitialValue()  // has an initial value that has no substitution field specs (literal string)
    {
    	return initialValue != null && ! initialValue.contains("${"); 
    }
    public boolean hasFieldSpecInitialValue() // has an initial value that includes substitution field specs
    {
    	return initialValue != null && initialValue.contains("${"); 
    }
    public void setInitialValue(String v)
    {
    	if ( v != null )
    	{
    		v = v.trim();
    		if ( v.length() > Literals.FIELD_EXPRESSION_MAX_LENGTH )
        		v = v.substring(0,Literals.FIELD_EXPRESSION_MAX_LENGTH);
    	}
    	initialValue = v;
    	objectChanged();
    }
    
    public String getDisplayEmptyValueEnabled()
    {
    	return displayEmptyValueEnabled;
    }
    public void setDisplayEmptyValueEnabled(String v)
    {
    	displayEmptyValueEnabled = DISPLAY_EMPTY_VALUE_ENABLED.equals(v) ? DISPLAY_EMPTY_VALUE_ENABLED : DISPLAY_EMPTY_VALUE_DISABLED;
    	objectChanged();
    }
    public boolean isDisplayEmptyValueEnabled()
    {
    	return DISPLAY_EMPTY_VALUE_ENABLED.equals(displayEmptyValueEnabled);
    }
    
    public String getDisplayEmptyValue()
    {
    	return displayEmptyValue;
    }
    public void setDisplayEmptyValue(String v)
    {
    	if ( v != null )
    	{
    		v = v.trim();
    		if ( v.length() > Literals.FIELD_EXPRESSION_MAX_LENGTH )
        		v = v.substring(0,Literals.FIELD_EXPRESSION_MAX_LENGTH);
    	}
    		
    	displayEmptyValue = v;
    	objectChanged();
    }
    
    public String getInputFormatSpec()
    {
    	return inputFormatSpec;
    }
    public boolean hasInputFormatSpec()
    {
    	return EsfString.isNonBlank(inputFormatSpec);
    }
    public void setInputFormatSpec(String v)
    {
    	if ( v != null )
    	{
    		v = v.trim();
    		if ( v.length() > Literals.FORMAT_SPEC_MAX_LENGTH )
        		v = v.substring(0,Literals.FORMAT_SPEC_MAX_LENGTH);
    	}
    		
    	inputFormatSpec = v;
    	objectChanged();
    }
    
    public String getOutputFormatSpec()
    {
    	return outputFormatSpec;
    }
    public boolean hasOutputFormatSpec()
    {
    	return EsfString.isNonBlank(outputFormatSpec);
    }
    public boolean isDecimalPercentOutputFormat()
    {
    	return isTypeDecimal() && hasOutputFormatSpec() && outputFormatSpec.endsWith("%");
    }
    public void setOutputFormatSpec(String v)
    {
    	if ( v != null )
    	{
    		v = v.trim();
    		if ( v.length() > Literals.FORMAT_SPEC_MAX_LENGTH )
        		v = v.substring(0,Literals.FORMAT_SPEC_MAX_LENGTH);
    	}
    		
    	outputFormatSpec = v;
    	objectChanged();
    }
    
    public String getExtraOptions()
    {
    	return extraOptions;
    }
    public boolean hasExtraOptions()
    {
    	return EsfString.isNonBlank(extraOptions);
    }
    public void setExtraOptions(String v)
    {
    	if ( v != null )
    	{
    		v = v.trim();
    		if ( v.length() > Literals.EXTRA_OPTIONS_MAX_LENGTH )
    			v = v.substring(0,Literals.EXTRA_OPTIONS_MAX_LENGTH);
    	}
    		
    	extraOptions = v;
    	objectChanged();
    }
    
    public String getTooltip()
    {
    	return tooltip;
    }
    public boolean hasTooltip()
    {
    	return EsfString.isNonBlank(tooltip);
    }
    public void setTooltip(String v)
    {
    	if ( v != null )
    	{
    		v = v.trim();
    		if ( v.length() > Literals.TOOLTIP_MAX_LENGTH )
        		v = v.substring(0,Literals.TOOLTIP_MAX_LENGTH);
    	}
    	tooltip = v;
    	objectChanged();
    }
    
    public String getInputPrompt()
    {
    	return inputPrompt;
    }
    public boolean hasInputPrompt()
    {
    	return EsfString.isNonBlank(inputPrompt);
    }
    public void setInputPrompt(String v)
    {
    	if ( v != null )
    	{
    		v = v.trim();
    		if ( v.length() > Literals.INPUT_PROMPT_MAX_LENGTH )
        		v = v.substring(0,Literals.INPUT_PROMPT_MAX_LENGTH);
    	}
    	inputPrompt = v;
    	objectChanged();
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof FieldTemplate )
            return getId().equals(((FieldTemplate)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(FieldTemplate o)
    {
    	return getEsfName().compareTo(o.getEsfName());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<FieldTemplate xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <containerId>").append(containerId.toXml()).append("</containerId>\n");
        buf.append(" <containerReferenceCount>").append(containerReferenceCount).append("</containerReferenceCount>\n");
        buf.append(" <esfname>").append(esfname.toXml()).append("</esfname>\n");
        buf.append(" <type>").append(escapeXml(type)).append("</type>\n");
        buf.append(" <width>").append(width).append("</width>\n");
        buf.append(" <widthUnit>").append(escapeXml(widthUnit)).append("</widthUnit>\n");
        buf.append(" <ignoreWidthOnOutput>").append(escapeXml(ignoreWidthOnOutput)).append("</ignoreWidthOnOutput>\n");
        buf.append(" <border>").append(escapeXml(border)).append("</border>\n");
        buf.append(" <align>").append(escapeXml(align)).append("</align>\n");
        buf.append(" <required>").append(escapeXml(required)).append("</required>\n");
        buf.append(" <autoComplete>").append(escapeXml(autoComplete)).append("</autoComplete>\n");
        buf.append(" <autoPost>").append(escapeXml(autoPost)).append("</autoPost>\n");
        buf.append(" <maskInDataSnapshot>").append(escapeXml(maskInDataSnapshot)).append("</maskInDataSnapshot>\n");
        buf.append(" <maskInput>").append(escapeXml(maskInput)).append("</maskInput>\n");
        buf.append(" <cloneAllowed>").append(escapeXml(cloneAllowed)).append("</cloneAllowed>\n");
        buf.append(" <minLength>").append(minLength).append("</minLength>\n");
        buf.append(" <maxLength>").append(maxLength).append("</maxLength>\n");
        buf.append(" <initialValue>").append(escapeXml(initialValue)).append("</initialValue>\n");
        buf.append(" <displayEmptyValueEnabled>").append(escapeXml(displayEmptyValueEnabled)).append("</displayEmptyValueEnabled>\n");
        buf.append(" <displayEmptyValue>").append(escapeXml(displayEmptyValue)).append("</displayEmptyValue>\n");
        buf.append(" <inputFormatSpec>").append(escapeXml(inputFormatSpec)).append("</inputFormatSpec>\n");
        buf.append(" <outputFormatSpec>").append(escapeXml(outputFormatSpec)).append("</outputFormatSpec>\n");
        buf.append(" <extraOptions>").append(escapeXml(extraOptions)).append("</extraOptions>\n");
        buf.append(" <tooltip>").append(escapeXml(tooltip)).append("</tooltip>\n");
        buf.append(" <inputPrompt>").append(escapeXml(inputPrompt)).append("</inputPrompt>\n");
        labelTemplate.appendXml(buf);

        buf.append("</FieldTemplate>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 800 + id.getEstimatedLengthXml() + containerId.getEstimatedLengthXml() + esfname.getEstimatedLengthXml() + type.length() +
        	widthUnit.length() + ignoreWidthOnOutput.length() + border.length() + align.length() + required.length() + autoComplete.length() + autoPost.length() +
        	maskInDataSnapshot.length() + maskInput.length() + cloneAllowed.length() + displayEmptyValueEnabled.length();
        if ( initialValue != null )
        	len += initialValue.length();
        if ( displayEmptyValue != null )
        	len += displayEmptyValue.length();
        if ( inputFormatSpec != null )
        	len += inputFormatSpec.length();
        if ( outputFormatSpec != null )
        	len += outputFormatSpec.length();
        if ( extraOptions != null )
        	len += extraOptions.length();
        if ( tooltip != null )
        	len += tooltip.length();
        if ( inputPrompt != null )
        	len += inputPrompt.length();
        len += 12; // lengths
        len += labelTemplate.getEstimatedLengthXml();
        return len; 
    }

    
	public synchronized boolean save(final Connection con)
		throws SQLException
	{
		//_logger.debug("save(con) on id: " + id + "; esfname: " + esfname + "; insert: " + doInsert());
	    
	    clearLastSQLException();
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	    	// On Save, signature and signature date fields are not allowed to be cloned. We won't clone
	    	// even if it's set, but this keeps the data in sync with actual processing logic.
	    	if ( (isTypeSignature() || isTypeSignDate()) && isCloneAllowed() )
	    		setCloneAllowed(CLONE_NOT_ALLOWED);
	    	
	        if ( doInsert() )
	        {
	        	labelTemplate.save(con);
	        	
	            stmt = new EsfPreparedStatement(con, 
	            	"INSERT INTO esf_field_template " +
	            	"(id,container_id,container_reference_count,esfname,field_type,required,auto_complete,auto_post,mask_in_data_snapshot,mask_input,clone_allowed," +
	            	"border,align,width_unit,width,ignore_width_on_output,min_length,max_length," +
	            	"initial_value,display_empty_value_enabled,display_empty_value," +
	            	"input_format_spec,output_format_spec,extra_options,tooltip,input_prompt,label_template_id) " +
	            	"VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	            stmt.set(id);
	            stmt.set(containerId);
	            stmt.set(containerReferenceCount);
	            stmt.set(esfname);
	            stmt.set(type);
	            stmt.set(required);
	            stmt.set(autoComplete);
	            stmt.set(autoPost);
	            stmt.set(maskInDataSnapshot);
	            stmt.set(maskInput);
	            stmt.set(cloneAllowed);
	            stmt.set(border);
	            stmt.set(align);
	            stmt.set(widthUnit);
	            stmt.set(width);
	            stmt.set(ignoreWidthOnOutput);
	            stmt.set(minLength);
	            stmt.set(maxLength);
	            stmt.set(initialValue);
	            stmt.set(displayEmptyValueEnabled);
	            stmt.set(displayEmptyValue);
	            stmt.set(inputFormatSpec);
	            stmt.set(outputFormatSpec);
	            stmt.set(extraOptions);
	            stmt.set(tooltip);
	            stmt.set(inputPrompt);
	            stmt.set(labelTemplate.getId());
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Insert failed for id: " + id + "; esfname: " + esfname);
	                return false;
	            }
	            
	            // Now we mark this object as if it were loaded fresh from the database
	            setLoadedFromDb();
	            
	            return true;
	        }
	        
	        labelTemplate.save(con);
	        
	        if ( hasChanged() )
	        {
	            // We assume we'll update it instead
	            stmt = new EsfPreparedStatement( con, 
	            		"UPDATE esf_field_template SET " +
	            		"container_reference_count=?,esfname=?,field_type=?,required=?,auto_complete=?,auto_post=?,mask_in_data_snapshot=?,mask_input=?,clone_allowed=?," +
	            		"border=?,align=?,width_unit=?,width=?,ignore_width_on_output=?,min_length=?,max_length=?," +
	            		"initial_value=?,display_empty_value_enabled=?,display_empty_value=?,input_format_spec=?,output_format_spec=?,extra_options=?,tooltip=?,input_prompt=? " +
	            		"WHERE id=?"
	            						   		);
	            stmt.set(containerReferenceCount);
	            stmt.set(esfname);
	            stmt.set(type);
	            stmt.set(required);
	            stmt.set(autoComplete);
	            stmt.set(autoPost);
	            stmt.set(maskInDataSnapshot);
	            stmt.set(maskInput);
	            stmt.set(cloneAllowed);
	            stmt.set(border);
	            stmt.set(align);
	            stmt.set(widthUnit);
	            stmt.set(width);
	            stmt.set(ignoreWidthOnOutput);
	            stmt.set(minLength);
	            stmt.set(maxLength);
	            stmt.set(initialValue);
	            stmt.set(displayEmptyValueEnabled);
	            stmt.set(displayEmptyValue);
	            stmt.set(inputFormatSpec);
	            stmt.set(outputFormatSpec);
	            stmt.set(extraOptions);
	            stmt.set(tooltip);
	            stmt.set(inputPrompt);
	            stmt.set(id);
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	if ( Manager.exists(id) )
	            	{
		            	_logger.warn("save(con) - Update for id: " + id + "; esfname: " + esfname + " had no change, but id still exists so ignoring");
	            	}
	            	else
	            	{
		            	_logger.warn("save(con) - Update failed for id: " + id + "; esfname: " + esfname + "; id no longer exists");
	            		return false;
	            	}
	            }
	        }
	
	        // Now we mark this object as if it were loaded fresh from the database
	        setLoadedFromDb();
	        
	        return true;
	    }
	    catch(SQLException e)
	    {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; esfname: " + esfname + "; insert: " + doInsert());
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}
    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }


    synchronized boolean delete(final Connection con, final Errors errors, final User user)
	    throws SQLException
	{
		_logger.debug("delete(con) on id: " + id + "; esfname: " + esfname + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
	    
	    clearLastSQLException();
	
	    // Let's do some referential integrity checks to be sure it's not in use 
	    if ( ! checkReferential(con,errors) )
	        return false;
	
	    if ( doInsert() )
	    {
	    	_logger.warn("delete(con) - Ignored delete of field template that was pending an INSERT id: " + id + "; esfname: " + esfname);
	        objectDeleted();
	        return true;
	    }
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	    	labelTemplate.delete(con);
	    	
	        // Delete the field template
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_field_template WHERE id=?");
	        stmt.set(id);
	        int num = stmt.executeUpdate();
	        if ( num == 0 )
	        	_logger.warn("delete(con) - Failed to find esf_field_template database row to delete with id: " + id);
	        
	        /* Party-Field mapping is deleted when the associated party is updated
	        // If a field template is deleted, it cannot be cross referenced as a field for a given party template either
	        stmt.close();
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_party_template_field_template WHERE field_template_id=?");
	        stmt.set(id);
	        num = stmt.executeUpdate();
	        */
	        
	        // If the field template is defined as an image overlay field, remove it from that image overlay too.
	        stmt.close();
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_image_version_overlay_field WHERE field_template_id=?");
	        stmt.set(id);
	        num = stmt.executeUpdate();

	        objectDeleted();
	        
	        if ( user != null ) 
	        {
	        	user.logConfigChange(con, "Deleted field template " + getEsfName() + "; type; " + getType()); 
	        	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted field template " + getEsfName() + "; type; " + getType());
	        }
	
	        return true;
	    }
	    catch(SQLException e)
	    {
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}

	public boolean delete(final Errors errors, final User user)
	{
	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        boolean result = delete(con,errors,user);
	        con.commit();
	        return result;
	    }
	    catch(SQLException e) 
	    {
	    	_logger.sqlerr(e,"delete() on id: " + id + "; esfname: " + esfname + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
	        pool.rollbackIgnoreException(con,e);
	        return false;
	    }
	    finally
	    {
	    	Application.getInstance().cleanupPool(pool,con,null);
	    }
	}
	public synchronized boolean delete(final User user)
	{
	    Errors nullErrors = null;
	    return delete(nullErrors,user);
	}
	public synchronized boolean delete()
	{
	    User nullUser = null;
	    return delete(nullUser);
	}
    
	protected boolean checkReferential(final Connection con, final Errors errors)
    	throws SQLException
    {
		return true;
	}

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all FieldTemplate objects held in the specified container.
   	     * @return the Map of FieldTemplate found indexed by esfname.
   	     */
   	    public static Map<EsfName,FieldTemplate> getAll(EsfUUID containerId)
   	    {
   	    	TreeMap<EsfName,FieldTemplate> fieldMap = new TreeMap<EsfName,FieldTemplate>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id,container_reference_count,esfname,field_type,required,auto_complete,auto_post,mask_in_data_snapshot,mask_input,clone_allowed," +
   	    	            "border,align,width_unit,width,ignore_width_on_output,min_length,max_length," +
   	    	            "initial_value,display_empty_value_enabled,display_empty_value,input_format_spec,output_format_spec,extra_options,tooltip,input_prompt,label_template_id " +
   	    	            "FROM esf_field_template " + 
   	    	            "WHERE container_id=? " +
   	    	            "ORDER BY lower(esfname) ASC"
   	            						   );
   	        	stmt.set(containerId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	short containerReferenceCount = rs.getShort();
		            EsfName esfname = rs.getEsfName();
		            String type = rs.getString();
		            String required = rs.getString();
		            String autoComplete = rs.getString();
		            String autoPost = rs.getString();
		            String maskInDataSnapshot = rs.getString();
		            String maskInput = rs.getString();
		            String cloneAllowed = rs.getString();
		            String border = rs.getString();
		            String align = rs.getString();
		            String widthUnit = rs.getString();
		            short width = rs.getShort();
		            String ignoreWidthOnOutput = rs.getString();
		            int minLength = rs.getInt();
		            int maxLength = rs.getInt();
		            String initialValue = rs.getString();
		            String displayEmptyValueEnabled = rs.getString();
		            String displayEmptyValue = rs.getString();
		            String inputFormatSpec = rs.getString();
		            String outputFormatSpec = rs.getString();
		            String extraOptions = rs.getString();
		            String tooltip = rs.getString();
		            String inputPrompt = rs.getString();
		            EsfUUID labelTemplateId = rs.getEsfUUID();
		            
		            LabelTemplate labelTemplate = LabelTemplate.Manager.getById(con, labelTemplateId);
		            
		            FieldTemplate field = new FieldTemplate(id,containerId,containerReferenceCount,esfname,type,width,widthUnit,ignoreWidthOnOutput,border,align,required,
		            										autoComplete,autoPost,maskInDataSnapshot,maskInput,cloneAllowed,minLength,maxLength,
		            										initialValue,displayEmptyValueEnabled,displayEmptyValue,
		            										inputFormatSpec,outputFormatSpec,extraOptions,tooltip,inputPrompt,labelTemplate);
		            field.setLoadedFromDb();
		            fieldMap.put(esfname,field);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - containerId: " + containerId + "; found: " + fieldMap.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - containerId: " + containerId + "; found: " + fieldMap.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	            fieldMap.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return fieldMap;
   	    }
   	    
   	    public static boolean exists(EsfUUID id)
   	    {
   		    ConnectionPool    pool = Application.getInstance().getConnectionPool();
   		    Connection        con  = pool.getConnection();
   		    EsfPreparedStatement stmt = null;
   		    try
   		    {
   		    	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_field_template WHERE id=?" );
   		    	stmt.set(id);
   		    	EsfResultSet rs = stmt.executeQuery();
   		    	boolean found = rs.next();
   		        con.commit();
   		        return found;
   		    }
   		    catch(SQLException e) 
   		    {
   		    	_logger.sqlerr(e,"exists() on id: " + id);
   		        pool.rollbackIgnoreException(con,e);
   		        return false;
   		    }
   		    finally
   		    {
   		    	Application.getInstance().cleanupPoolAndEsfPreparedStatement(pool,con,stmt);
   		    }
   	    	
   	    }
   	    
   	    public static FieldTemplate createLike(EsfUUID newContainerId, FieldTemplate likeTemplate)
   	    {
   	    	FieldTemplate newTemplate = new FieldTemplate(newContainerId, likeTemplate);
   	    	newTemplate.containerReferenceCount = 0; // The new field should have no reference count after create like
   	    	return newTemplate;
   	    }
   	    
   	    public static FieldTemplate createNew(EsfUUID newContainerId, EsfName name)
   	    {
   	    	FieldTemplate newTemplate = new FieldTemplate(newContainerId,name);
   	    	return newTemplate;
   	    }
   	    
   	    public static FieldTemplate createFromJDOM(EsfUUID containerId, Element e)
   	    {
   	    	if ( containerId == null || containerId.isNull() || e == null )
   	    		return null;
   	    	
   	    	EsfUUID id = new EsfUUID();
   	    	
   	    	Application app = Application.getInstance();
   	    	
   	    	LabelTemplate newLabelTemplate = LabelTemplate.Manager.createFromJDOM(e.getChild("LabelTemplate", e.getNamespace()));
   	    	
   	    	FieldTemplate newFieldTemplate = new FieldTemplate(
   	    			id, 
   	    			containerId, 
   	    			app.stringToShort(e.getChildText("containerReferenceCount", e.getNamespace()), (short)0),
   	    			EsfName.createFromToXml(e.getChildText("esfname", e.getNamespace())),
   	    			e.getChildText("type", e.getNamespace()),
   	    			app.stringToShort(e.getChildText("width", e.getNamespace()), (short)0),
   	    			e.getChildText("widthUnit", e.getNamespace()),
   	    			e.getChildText("ignoreWidthOnOutput", e.getNamespace()),
   	    			e.getChildText("border", e.getNamespace()),
   	    			e.getChildText("align", e.getNamespace()),
   	    			e.getChildText("required", e.getNamespace()),
   	    			e.getChildText("autoComplete", e.getNamespace()),
   	    			e.getChildText("autoPost", e.getNamespace()),
   	    			e.getChildText("maskInDataSnapshot", e.getNamespace()),
   	    			e.getChildText("maskInput", e.getNamespace()),
   	    			e.getChildText("cloneAllowed", e.getNamespace()),
   	    			app.stringToInt(e.getChildText("minLength", e.getNamespace()), 0),
   	    			app.stringToInt(e.getChildText("maxLength", e.getNamespace()), 0),
   	    			e.getChildText("initialValue", e.getNamespace()),
   	    			e.getChildText("displayEmptyValueEnabled", e.getNamespace()),
   	    			e.getChildText("displayEmptyValue", e.getNamespace()),
   	    			e.getChildText("inputFormatSpec", e.getNamespace()),
   	    			e.getChildText("outputFormatSpec", e.getNamespace()),
   	    			e.getChildText("extraOptions", e.getNamespace()),
   	    			e.getChildText("tooltip", e.getNamespace()),
   	    			e.getChildText("inputPrompt", e.getNamespace()),
   	    			newLabelTemplate
   	    			);
   	    	newFieldTemplate.setNewObject();
   	    	return newFieldTemplate;
   	    }
   	    
   	} // Manager
    
    // From: http://www.irs.gov/Businesses/Small-Businesses-&-Self-Employed/How-EINs-are-Assigned-and-Valid-EIN-Prefixes
	static String[] ValidEINPrefixes = { 
			"10", "12", // Andover
			"60", "67", // Atlanta
			"50", "53", // Austin
			"01", "02", "03", "04", "05", "06", "11", "13", "14", "16", "21", "22", "23", "25", "34", "51", "52", "54", "55", "56", "57", "58", "59", "65", // Brookhaven
			"30", "32", "35", "36", "37", "38", "61", // Cincinnati
			"15", "24", // Fresno
			"40", "44", // Kansas City
			"94", "95", // Memphis
			"80", "90", // Ogden
			"33", "39", "41", "42", "43", "46", "48", "62", "63", "64", "66", "68", "71", "72", "73", "74", "75", "76", "77", "81", "82", "83", "84", "85", "86", "87", "88", "91", "92", "93", "98", "99", // Philadelphia
			"20", "26", "27", "45", "46", "47", /* 47 says for future use, but was given out before so we need to accept it */ // Internet
			"31" // Small Business Administration
	};
    public static boolean isValidEINPrefix(String ein)
    {
    	if ( EsfString.isNonBlank(ein) && ein.length() > 1 )
    	{
        	String prefix = ein.substring(0, 2);
        	for( String validPrefix : ValidEINPrefixes )
        	{
        		if ( validPrefix.equals(prefix) )
        			return true;
        	}
    	}
    	return false;
    }

}