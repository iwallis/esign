// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.User;

/**
* PackageInfo is a data-only object that holds information about a package.
* It's not a mutable object (you can't save/delete it), just for lists of packages.
* 
* @author Yozons, Inc.
*/
public class PackageInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<PackageInfo>, java.io.Serializable
{
	private static final long serialVersionUID = -5626051969149601768L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackageInfo.class);

    protected EsfUUID id;
    protected EsfPathName pathName;
    protected String  downloadFileNameSpec;
    protected String  description;
    protected String  status;
    protected int	  productionVersion;
    protected int	  testVersion;

    /**
     * This creates a new PackageInfo using the source object.
     * @param package the package to use as the source
     */
    protected PackageInfo(Package pkg)
    {
    	this.id = pkg.getId();
    	this.pathName = pkg.getPathName();
       	this.downloadFileNameSpec = pkg.getDownloadFileNameSpec();
        this.description = pkg.getDescription();
    	this.status = pkg.getStatus();
    	this.productionVersion = pkg.getProductionVersion();
    	this.testVersion = pkg.getTestVersion();
    }
    
    /**
     * This creates a PackageInfo object from data retrieved from the DB.
     */
    protected PackageInfo(EsfUUID id, EsfPathName pathName, String downloadFileNameSpec, String description, String status, int productionVersion, int testVersion)
    {
    	this.id = id;
    	this.pathName = pathName;
    	this.downloadFileNameSpec = downloadFileNameSpec;
    	this.description = description;
    	this.status = status;
    	this.productionVersion = productionVersion;
    	this.testVersion = testVersion;
    }
    
    public EsfPathName getPathName()
    {
        return pathName;
    }
    
    public String getDownloadFileNameSpec()
    {
        return downloadFileNameSpec;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }

    public int getProductionVersion()
    {
    	return productionVersion;
    }
    
    public int getTestVersion()
    {
    	return testVersion;
    }

    public boolean hasProductionVersion()
    {
    	return productionVersion > 0;
    }

    public boolean hasTestVersion()
    {
    	return testVersion > productionVersion;
    }

    public EsfUUID getId()
    {
        return id;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof PackageInfo )
            return getId().equals(((PackageInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(PackageInfo o)
    {
    	return getPathName().compareTo(o.getPathName());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all PackageInfo objects that the user has permission to access according to the option specified
   	     * @return the Collection of PackageInfo found ordered by pathName.
   	     */
   	    public static Collection<PackageInfo> getForUserWithPermission(User user, EsfName permOptionName)
   	    {
   	    	LinkedList<PackageInfo> list = new LinkedList<PackageInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT PKG.id,PKG.path_name,PKG.download_file_name_spec,PKG.description,PKG.status,PKG.production_version,PKG.test_version " +
   	        			"FROM esf_package PKG, esf_permission_option_group PERM " +
   	        			"WHERE PERM.permission_id = PKG.id AND lower(PERM.permission_option_esfname)=? AND " +
   	        			"( PERM.group_id=? OR PERM.group_id IN (SELECT GU.group_id FROM esf_group_user GU WHERE GU.user_id=?) ) " +
   	        			"ORDER BY lower(PKG.path_name) ASC"
   	            						   );
   	        	stmt.set(permOptionName.toLowerCase());
   	        	stmt.set(Group.Manager.getAllUsersGroup().getId());
   	        	stmt.set(user.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            EsfPathName pathName = rs.getEsfPathName();
		            String downloadFileNameSpec = rs.getString();
		            String description = rs.getString();
		            String  stat = rs.getString();
		            int productionVersion = rs.getInt();
		            int testVersion = rs.getInt();
		            
		            PackageInfo pkgInfo = new PackageInfo(id,pathName,downloadFileNameSpec,description,stat,productionVersion,testVersion);
		            pkgInfo.setLoadedFromDb();
		            list.add(pkgInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getForUserWithPermission() - userId: " + user.getId() + "; permOptionName: " + permOptionName + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getForUserWithPermission() - userId: " + user.getId() + "; permOptionName: " + permOptionName + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static PackageInfo createLike(Package likePackage, EsfPathName newPathName, User createdBy)
   	    {
   	    	if ( likePackage == null || newPathName == null || ! newPathName.isValid() )
   	    		return null;

   	    	Package newPackage = Package.Manager.createLike(likePackage, newPathName, createdBy);
   	    	return new PackageInfo(newPackage);
   	    }
   	    
   	    public static PackageInfo createFromSource(Package pkg)
   	    {
   	    	if ( pkg == null )
   	    		return null;
   	    	
   	    	PackageInfo pkgInfo = new PackageInfo(pkg);
   	    	pkgInfo.setLoadedFromDb();

   			return pkgInfo;
   	    }
   	    
   	} // Manager
    
}