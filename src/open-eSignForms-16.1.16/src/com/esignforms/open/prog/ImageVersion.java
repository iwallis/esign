// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.TreeMap;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* ImageVersion holds the definition of a specific version of a image held in a library or document version.
* 
* @author Yozons, Inc.
*/
public class ImageVersion
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<ImageVersion>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable, java.io.Serializable
{
	private static final long serialVersionUID = 3594124541885257099L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ImageVersion.class);
    
    protected final EsfUUID id;
    protected final EsfUUID imageId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected String imageFileName;
    protected String imageMimeType;
    protected EsfUUID imageDataBlobId;
    protected byte[] imageData;
    private boolean imageDataChanged = false;
    protected EsfUUID thumbnailDataBlobId;
    protected byte[] thumbnailData;
    private boolean thumbnailDataChanged = false;
    
    protected String useDataUri;
    protected List<ImageVersionOverlayField> overlayFieldList;
   
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();

    /**
     * This creates a ImageVersion object from data retrieved from the DB.
     */
    protected ImageVersion(EsfUUID id, EsfUUID imageId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		String imageFileName, String imageMimeType, EsfUUID imageDataBlobId, byte[] imageData, EsfUUID thumbnailDataBlobId, byte[] thumbnailData,
    		String useDataUri, List<ImageVersionOverlayField> overlayFieldList)
    {
        this.id = id;
        this.imageId = imageId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.imageFileName = imageFileName;
        this.imageMimeType = imageMimeType;
        this.imageDataBlobId = imageDataBlobId;
        this.imageData = imageData;
        this.thumbnailDataBlobId = thumbnailDataBlobId;
        this.thumbnailData = thumbnailData;
        setUseDataUri(useDataUri);
        setOverlayFields(overlayFieldList);
    }
    
    protected ImageVersion(Image image, User createdByUser)
    {
        this.id = new EsfUUID();
        this.imageId = image.getId();
        this.version = image.getProductionVersion() + 1;
        this.createdTimestamp = new EsfDateTime();
        this.createdByUserId = createdByUser.getId();
        this.lastUpdatedTimestamp = createdTimestamp;
        this.lastUpdatedByUserId = createdByUserId;
        this.imageFileName = null;
        this.imageMimeType = null;
        this.imageDataBlobId = new EsfUUID();
        this.imageData = null;
        this.thumbnailDataBlobId = new EsfUUID();
        this.thumbnailData = null;
        setUseDataUri(Literals.N);
        setOverlayFields(null);
    }
    
    public ImageVersion duplicate()
    {
    	List<ImageVersionOverlayField> dupOverlayFieldList = null;
    	if ( overlayFieldList != null )
    	{
    		dupOverlayFieldList = new LinkedList<ImageVersionOverlayField>();
    		for( ImageVersionOverlayField origOverlay : overlayFieldList )
    			dupOverlayFieldList.add( origOverlay.duplicate() );
    	}
    	
    	ImageVersion iv = new ImageVersion(id, imageId, version, 
        		createdTimestamp.duplicate(), createdByUserId,  lastUpdatedTimestamp.duplicate(), lastUpdatedByUserId,
        		imageFileName, imageMimeType, imageDataBlobId, imageData == null ? null : imageData.clone(), thumbnailDataBlobId, thumbnailData == null ? null : thumbnailData.clone(),
        		useDataUri, dupOverlayFieldList);
    	iv.setDatabaseObjectLike(this);
    	return iv;
    }
   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getImageId()
    {
        return imageId;
    }
    
    public Image getImage()
    {
    	return Image.Manager.getById(imageId);
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}

	public int getVersion()
    {
    	return version;
    }
    
	public String getVersionLabel()
	{
		return getImage().getVersionLabel(version);
	}
	public String getImageNameVersion()
	{
		return getImage().getEsfName() + " [" + version + "]";
	}
	public String getImageNameVersionWithLabel()
	{
		Image image = getImage();
		return image.getEsfName() + " [" + version + "] (" + image.getVersionLabel(version) + ")";
	}
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }
    
    public String getImageFileName()
    {
    	return imageFileName;
    }
	public boolean hasImageFileName() {
		return EsfString.isNonBlank(imageFileName);
	}
    public void setImageFileName(String v)
    {
    	if ( v != null )
    		v = v.trim();
    	imageFileName = v;
    	objectChanged();
    }
    
    public String getImageMimeType()
    {
    	return imageMimeType;
    }
    public void setImageMimeType(String v)
    {
    	if ( v != null )
    		v = v.trim();
    	imageMimeType = v;
    	objectChanged();
    }
    
    public EsfUUID getImageDataBlobId()
    {
        return imageDataBlobId;
    }

    public byte[] getImageData()
    {
        return imageData;
    }
    public void setImageData(byte[] v)
    {
    	imageData = v;
    	imageDataChanged = true;
    	objectChanged();
    }
    public String getImageUrl()
    {
    	return getImageUrlWithRandomToAvoidBrowserCache(false);
    }
    public String getImageUrlWithRandomToAvoidBrowserCache(boolean addRandom)
    {
    	String random = addRandom ? "?rand="+Application.getInstance().getRandomKey().getAlphaNumericString(8) : "";
    	return Application.getInstance().getContextPath() + "/images/" + getImage().getEsfName() + "/" + getImageFileName() + random;
    }
    public String getImageByIdUrl() // used internally
    {
    	return Application.getInstance().getContextPath() + "/imagesById/" + getId() + "/" + getImageFileName() + "?rand="+Application.getInstance().getRandomKey().getAlphaNumericString(8);
    }

    public EsfUUID getThumbnailDataBlobId()
    {
        return thumbnailDataBlobId;
    }

    public byte[] getThumbnailData()
    {
        return thumbnailData;
    }
    public void setThumbnailData(byte[] v)
    {
    	thumbnailData = v;
    	thumbnailDataChanged = true;
    	objectChanged();
    }
    public String getThumbnailUrl()
    {
    	return getThumbnailUrlWithRandomToAvoidBrowserCache(false);
    }
    public String getThumbnailUrlWithRandomToAvoidBrowserCache(boolean addRandom)
    {
    	String random = addRandom ? "?rand="+Application.getInstance().getRandomKey().getAlphaNumericString(8) : "";
    	return Application.getInstance().getContextPath() + "/thumbnails/" + getImage().getEsfName() + "/" + getImageFileName() + random;
    }
    public String getThumbnailByIdUrl() // used internally
    {
    	return Application.getInstance().getContextPath() + "/thumbnailsById/" + getId() + "/" + getImageFileName() + "?rand="+Application.getInstance().getRandomKey().getAlphaNumericString(8);
    }

    public String getUseDataUri()
    {
    	return useDataUri;
    }
    public boolean isUseDataUri()
    {
    	return Literals.Y.equals(useDataUri);
    }
    public void setUseDataUri(String v)
    {
    	useDataUri = Literals.Y.equalsIgnoreCase(v) ? Literals.Y : Literals.N;
    	objectChanged();
    }
    
    public String getDataUri()
    {
    	StringBuilder uri = new StringBuilder( 20 + imageMimeType.length() + ((imageData.length*4)/3) );
    	uri.append("data:").append(imageMimeType).append(";base64,").append(base64encode(imageData));
    	return uri.toString();
    }
    
    public List<ImageVersionOverlayField> getOverlayFields()
    {
    	return overlayFieldList;
    }
    public boolean hasOverlayFields()
    {
    	return overlayFieldList != null && overlayFieldList.size() > 0;
    }
    public void setOverlayFields(List<ImageVersionOverlayField> v)
    {
    	if ( v != null )
    		Collections.sort(v);
    	overlayFieldList = v;
    	objectChanged();
    }
    public boolean removeFieldTemplateFromOverlayFields(EsfUUID fieldTemplateId)
    {
    	if ( hasOverlayFields() )
    	{
    		ListIterator<ImageVersionOverlayField> iter = overlayFieldList.listIterator();
    		while( iter.hasNext() )
    		{
    			ImageVersionOverlayField ivof = iter.next();
    			if ( ivof.getFieldTemplateId().equals(fieldTemplateId) )
    			{
    				iter.remove();
    				return true;
    			}
    		}
    	}
    	return false;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof ImageVersion )
        {
        	ImageVersion otherImage = (ImageVersion)o;
            return getId().equals(otherImage.getId());
        }
        return false;
    }
    
    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(ImageVersion o)
    {
    	if ( getImageId().equals(o.getImageId()) )
    		return getVersion() - o.getVersion();
    	return getImageId().compareTo(o.getImageId());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<ImageVersion xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <imageId>").append(imageId.toXml()).append("</imageId>\n");
        buf.append(" <version>").append(version).append("</version>\n");
        buf.append(" <createdTimestamp>").append(createdTimestamp.toXml()).append("</createdTimestamp>\n");
        buf.append(" <createdByUserId>").append(createdByUserId.toXml()).append("</createdByUserId>\n");
        buf.append(" <lastUpdatedTimestamp>").append(lastUpdatedTimestamp.toXml()).append("</lastUpdatedTimestamp>\n");
        buf.append(" <lastUpdatedByUserId>").append(lastUpdatedByUserId.toXml()).append("</lastUpdatedByUserId>\n");
        buf.append(" <imageFileName>").append(escapeXml(imageFileName)).append("</imageFileName>\n");
        buf.append(" <imageMimeType>").append(escapeXml(imageMimeType)).append("</imageMimeType>\n");
        buf.append(" <imageData>").append(base64encode(imageData)).append("</imageData>\n");
        buf.append(" <thumbnailData>").append(base64encode(thumbnailData)).append("</thumbnailData>\n");
        buf.append(" <useDataUri>").append(escapeXml(useDataUri)).append("</useDataUri>\n");

        if ( hasOverlayFields() )
        {
        	for( ImageVersionOverlayField overlay : getOverlayFields() )
        		overlay.appendXml(buf);
        }
        
        buf.append("</ImageVersion>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 218 + id.getEstimatedLengthXml() + imageId.getEstimatedLengthXml() + createdTimestamp.getEstimatedLengthXml() + createdByUserId.getEstimatedLengthXml() +
        	lastUpdatedTimestamp.getEstimatedLengthXml() + lastUpdatedByUserId.getEstimatedLengthXml() + imageFileName.length() + imageMimeType.length() + useDataUri.length();
        len += (4 * imageData.length) / 3;
        len += (4 * thumbnailData.length) / 3;
        
        if ( hasOverlayFields() )
        {
        	for( ImageVersionOverlayField overlay : getOverlayFields() )
        		len += overlay.getEstimatedLengthXml();
        }
        
        return len; 
    }

    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; imageId: " + imageId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
			ImageVersionOverlayField.Manager.deleteAllByImageVersionId(con, id);
			if ( hasOverlayFields() ) {
				for( ImageVersionOverlayField ivof : getOverlayFields() ) {
					ivof.saveByInsert(con);
				}
			}
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
            	
				getBlobDb().insert(con, imageDataBlobId, imageData, BlobDb.CompressOption.DISABLE, BlobDb.EncryptOption.DISABLE);
				getBlobDb().insert(con, thumbnailDataBlobId, thumbnailData, BlobDb.CompressOption.DISABLE, BlobDb.EncryptOption.DISABLE);
				
				stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_image_version (id,library_image_id,version,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,image_file_name,image_mime_type,image_blob_id,thumbnail_blob_id,use_data_uri) " + 
					"VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(imageId);
                stmt.set(version);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(imageFileName);
                stmt.set(imageMimeType);
                stmt.set(imageDataBlobId);
                stmt.set(thumbnailDataBlobId);
                stmt.set(useDataUri);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; imageId: " + imageId + "; version: " + version);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                imageDataChanged = thumbnailDataChanged = false;
                setLoadedFromDb();
                Manager.cache.replace(this);
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new image version id: " + id + "; imageId: " + imageId + "; version: " + version); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new image version id: " + id + "; imageId: " + imageId + "; version: " + version);
                }
                
                return true;
            }
            
			if ( imageDataChanged && ! getBlobDb().update(con, imageDataBlobId, imageData, BlobDb.CompressOption.DISABLE, BlobDb.EncryptOption.DISABLE) )
				return false;
			if ( thumbnailDataChanged && ! getBlobDb().update(con, thumbnailDataBlobId, thumbnailData, BlobDb.CompressOption.DISABLE, BlobDb.EncryptOption.DISABLE) )
				return false;
        	
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_image_version SET last_updated_timestamp=?,last_updated_by_user_id=?,image_file_name=?,image_mime_type=?,use_data_uri=? WHERE id=?"
                						   		);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(imageFileName);
                stmt.set(imageMimeType);
                stmt.set(useDataUri);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for image version id: " + id + "; imageId: " + imageId + "; version: " + version);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
			imageDataChanged = thumbnailDataChanged = false;
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated image version id: " + id + "; imageId: " + imageId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated image version id: " + id + "; imageId: " + imageId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; imageId: " + imageId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; imageId: " + imageId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use 
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of image version that was pending an INSERT id: " + id + "; imageId: " + imageId + "; version: " + version);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
            // Delete the image version and its image/thumb
        	getBlobDb().delete(con,imageDataBlobId);
        	getBlobDb().delete(con,thumbnailDataBlobId);
            
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_image_version WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            // Delete our related overlay fields if any
            ImageVersionOverlayField.Manager.deleteAllByImageVersionId(con, id);
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted image version id: " + id + "; imageId: " + imageId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted image version id: " + id + "; imageId: " + imageId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"delete() on id: " + id + "; imageId: " + imageId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   		private static UUIDCacheReadOptimized<ImageVersion> cache = new UUIDCacheReadOptimized<ImageVersion>();
   		
   		static final ImageVersion getById(Connection con, EsfUUID imageVersionId) throws SQLException
   		{
   			return getById(con,imageVersionId,true);
   		}
   		static ImageVersion getById(Connection con, EsfUUID imageVersionId, boolean checkCache) throws SQLException
   		{
   			if ( checkCache )
   			{
   				ImageVersion imageVer = cache.getById(imageVersionId);
   	   			if ( imageVer != null )
   	   				return imageVer;
   			}

   			EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT library_image_id,version,created_by_user_id,last_updated_by_user_id,created_timestamp,last_updated_timestamp," +
   	        			"image_file_name,image_mime_type,image_blob_id,thumbnail_blob_id,use_data_uri " +
   	        			"FROM esf_library_image_version WHERE id = ?"
   	        									);
   	        	stmt.set(imageVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID libraryImageId = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            String imageFileName = rs.getString();
		            String imageMimeType = rs.getString();
		            EsfUUID imageDataBlobId = rs.getEsfUUID();
		            EsfUUID thumbnailDataBlobId = rs.getEsfUUID();
		            String useDataUri = rs.getString();
		            
		            BlobDb blobDb = Application.getInstance().getBlobDb();
		            byte[] imageData = blobDb.select(con, imageDataBlobId);
		            byte[] thumbnailData = blobDb.select(con, thumbnailDataBlobId);
		            
		            List<ImageVersionOverlayField> overlayFields = ImageVersionOverlayField.Manager.getAllByImageVersionId(con, imageVersionId);
		            if ( overlayFields.size() == 0 )
		            	overlayFields = null;
		            
		            ImageVersion imageVer = new ImageVersion(imageVersionId,libraryImageId,version,
		            										 createdTimestamp,createdByUserId,lastUpdatedTimestamp,lastUpdatedByUserId,
		            										 imageFileName,imageMimeType,imageDataBlobId,imageData,thumbnailDataBlobId,thumbnailData,
		            										 useDataUri,overlayFields);
		            imageVer.setLoadedFromDb();
		            cache.add(imageVer);
		            
	   	            return imageVer;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + imageVersionId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + imageVersionId + "; failed to find the image version");
   	        return null; 
  		}
   		
   		public static ImageVersion getById(EsfUUID imageVersionId)
   		{
   			ImageVersion imageVer = cache.getById(imageVersionId);
   			if ( imageVer != null )
   				return imageVer;
   	   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	imageVer = getById(con,imageVersionId,false);
   	        	if ( imageVer != null ) 
   	        	{
		            con.commit();
	   	        	return imageVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		static ImageVersion getByVersion(Connection con, EsfUUID libraryImageId, int version) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_image_version WHERE library_image_id=? AND version=?" );
   	        	stmt.set(libraryImageId);
   	        	stmt.set(version);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID docVersionId = rs.getEsfUUID();
	       			return getById(con,docVersionId);
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByVersion() - libraryImageId: " + libraryImageId + "; version: " + version);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getByVersion() - libraryImageId: " + libraryImageId + "; version: " + version + "; failed to find the image version");
   	        return null; 
  		}
   		
   		public static ImageVersion getByVersion(EsfUUID libraryImageId, int version)
   		{
   			// While not always true, if the version is 1, it's possible this is a new object that's not been saved yet so we can't find the id in the DB, so we'll check our cache first just in case
   			if ( version == 1 )
   			{
   				for( EsfUUID id : cache.getAllIds() )
   				{
   					ImageVersion imageVer = cache.getById(id);
   					if ( imageVer.getImageId().equals(libraryImageId) && imageVer.getVersion() == version )
   						return imageVer;
   				}
   			}
   			
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	ImageVersion imageVer = getByVersion(con,libraryImageId,version);
   	        	if ( imageVer != null ) 
   	        	{
		            con.commit();
	   	        	return imageVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		
   		public static List<ImageVersion> getAllByImageId(Connection con, EsfUUID libraryImageId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<ImageVersion> list = new LinkedList<ImageVersion>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_image_version WHERE library_image_id=?" );
   	        	stmt.set(libraryImageId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID imageVersionId = rs.getEsfUUID();
	            	
	            	ImageVersion imageVer = getById(con,imageVersionId);
	       			if ( imageVer != null )
	       				list.add(imageVer);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<ImageVersion> getAllByImageId(EsfUUID libraryImageId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<ImageVersion> list = getAllByImageId(con,libraryImageId);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return new LinkedList<ImageVersion>(); 
   		}
   	    
   		
   	    
   	    public static ImageVersion createTest(Image image, User createdBy)
   	    {
   	    	if ( image == null || createdBy == null )
   	    		return null;
   	    	ImageVersion newImageVersion = new ImageVersion(image,createdBy);
   	    	cache.add(newImageVersion);
   	    	return newImageVersion;
   	    }

   	    public static ImageVersion createLike(Image image, ImageVersion likeImageVersion, User createdBy)
   	    {
   	    	if ( image == null || likeImageVersion == null || createdBy == null )
   	    		return null;
   	    	ImageVersion newImageVersion = createTest(image,createdBy);
   	    	newImageVersion.imageFileName = likeImageVersion.imageFileName;
   	    	newImageVersion.imageMimeType = likeImageVersion.imageMimeType;
   	    	newImageVersion.imageData = likeImageVersion.imageData;
   	    	newImageVersion.thumbnailData = likeImageVersion.thumbnailData;
   	    	newImageVersion.useDataUri = likeImageVersion.useDataUri;
   	    	
   	    	if ( likeImageVersion.hasOverlayFields() )
   	    	{
   	    		List<ImageVersionOverlayField> overlayFields = new LinkedList<ImageVersionOverlayField>();
   	    		for( ImageVersionOverlayField likeOverlay : likeImageVersion.getOverlayFields() )
   	    			overlayFields.add( likeOverlay.duplicateUsingImageVersionId(newImageVersion.getId()) );
   	    		newImageVersion.overlayFieldList = overlayFields;
   	    	}

   	    	return newImageVersion;
   	    }

   	    public static ImageVersion createFromJDOM(TreeMap<EsfUUID,EsfUUID> fieldTemplateIdMapping, Image image, Element e)
   	    {
   	    	if ( image == null || e == null )
   	    		return null;
   	    	
   	    	EsfUUID id = new EsfUUID();
   	    	
   	    	List<ImageVersionOverlayField> overlayFields = null;
   	    	List<Element> overlayFieldElements = e.getChildren("ImageVersionOverlayField", e.getNamespace());
   	    	if ( overlayFieldElements != null && overlayFieldElements.size() > 0 )
   	    	{
   	    		Application app = Application.getInstance();
   	    		overlayFields = new LinkedList<ImageVersionOverlayField>();
   	    		ListIterator<Element> elemIter = overlayFieldElements.listIterator();
   	    		while( elemIter.hasNext() )
   	    		{
   	    			Element overlayFieldElement = elemIter.next();
   	    			EsfUUID fieldTemplateId = new EsfUUID( overlayFieldElement.getChildTextTrim("fieldTemplateId", e.getNamespace()));
   	    			if ( fieldTemplateIdMapping != null )
   	    				fieldTemplateId = fieldTemplateIdMapping.get(fieldTemplateId);
   	    			short positionLeft = app.stringToShort(overlayFieldElement.getChildTextTrim("positionLeft", e.getNamespace()), (short)0);
   	    			short positionTop = app.stringToShort(overlayFieldElement.getChildTextTrim("positionTop", e.getNamespace()), (short)0);
   	    			short positionWidth = app.stringToShort(overlayFieldElement.getChildTextTrim("positionWidth", e.getNamespace()), (short)50);
   	    			short positionHeight = app.stringToShort(overlayFieldElement.getChildTextTrim("positionHeight", e.getNamespace()), (short)20);
   	    			String displayMode = overlayFieldElement.getChildTextTrim("displayMode", e.getNamespace());
   	    			String backgroundColor = overlayFieldElement.getChildTextTrim("backgroundColor", e.getNamespace());
   	     	    			
   	    			ImageVersionOverlayField overlayField = new ImageVersionOverlayField(id,fieldTemplateId,positionLeft,positionTop,positionWidth,positionHeight,displayMode,backgroundColor);
   	    			overlayFields.add(overlayField);
   	    		}
   	    	}
   	    	
			ImageVersion newImageVersion = new ImageVersion(
   	    			id, 
   	    			image.getId(), 
   	    			image.getTestVersion(),
   	    			null,
   	    			null,
   	    			null,
   	    			null,
   	    			e.getChildText("imageFileName", e.getNamespace()),
   	    			e.getChildText("imageMimeType", e.getNamespace()),
   	    			new EsfUUID(),
   	    			com.esignforms.open.util.Base64.decode((e.getChildText("imageData", e.getNamespace()))),
   	    			new EsfUUID(),
   	    			com.esignforms.open.util.Base64.decode((e.getChildText("thumbnailData", e.getNamespace()))),
   	    			e.getChildText("useDataUri", e.getNamespace()),
   	    			overlayFields
   	    			);
			newImageVersion.setNewObject();
   	    	cache.add(newImageVersion);
   	    	return newImageVersion;
   	    }
   	    
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all image versions that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }

   		public static void clearCache()
   		{
   			cache.clear();
   		}
   		
   		public static void dropFromCache(ImageVersion imageVersion)
   		{
   	   		cache.remove(imageVersion);
   		}
   		public static void replaceInCache(ImageVersion imageVersion)
   		{
   	   		cache.remove(imageVersion);
   	   		cache.add(imageVersion);
   		}
   	} // Manager
   	
}