// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.action.Action;


/**
* Based on PackageProgrammingRule.java as of 5/23/2012.
* DocumentProgrammingRule defines a rule that can fire when a document is running live in a transaction.
* It essentially checks for a matching event, optional party, optional page, and if matches those has a list of actions to be performed.
* 
* @author Yozons, Inc.
*/
public class DocumentProgrammingRule
	implements java.io.Serializable
{
	private static final long serialVersionUID = 5174025031009860154L;

	int order = 1; // not persisted, but maintains the order of this rule with respect to other rules
 	
 	HashSet<EsfName> onEventEsfNames = new HashSet<EsfName>();
    HashSet<EsfUUID> onPageIds = new HashSet<EsfUUID>();
    HashSet<EsfUUID> onPartyIds = new HashSet<EsfUUID>();
    LinkedList<Action> actionList = new LinkedList<Action>();
    
    public DocumentProgrammingRule() 
    {
    	// see below for how to create this object from JDOM/XML
    }
    
    public DocumentProgrammingRule duplicate()
    {
    	DocumentProgrammingRule rule = new DocumentProgrammingRule();
    	for( EsfName name : onEventEsfNames )
    		rule.addOnEventEsfName( name.duplicate() );
    	for( EsfUUID id : onPageIds )
    		rule.addOnPageId( id );
    	for( EsfUUID id : onPartyIds )
    		rule.addOnPartyId( id );
    	for( Action action : actionList )
    		rule.addAction( action.duplicate() );
    	return rule;
    }
    
    public int getOrder() { return order; }
    public void setOrder(int v) { order = v; }
    
    public Set<EsfName> getOnEventEsfNames()
    {
    	return onEventEsfNames;
    }
    public void setOnEventEsfNames(Set<EsfName> v)
    {
    	onEventEsfNames = new HashSet<EsfName>(v);
    }
    public void addOnEventEsfName(EsfName name)
    {
    	onEventEsfNames.add(name);
    }
    public boolean isForEvent(EsfName eventName)
    {
    	return onEventEsfNames.contains(eventName);
    }

    public Set<EsfUUID> getOnPageIds()
    {
    	return onPageIds;
    }
    public void setOnPageIds(Set<EsfUUID> v)
    {
    	onPageIds = new HashSet<EsfUUID>(v);
    }
    public void addOnPageId(EsfUUID id)
    {
    	onPageIds.add(id);
    }
    public boolean isForPage(EsfUUID pageId)
    {
    	return onPageIds.size() == 0 || onPageIds.contains(pageId);
    }
    
    // For imports where we need to change all page ids from one value to another
    public void updatePageIds(Map<EsfUUID,EsfUUID> pageIdMapping)
    {
    	HashSet<EsfUUID> newPageIds = new HashSet<EsfUUID>();
    	for( EsfUUID origId : onPageIds )
    	{
    		EsfUUID newId = pageIdMapping.get(origId);
    		newPageIds.add( newId != null ? newId : origId );
    	}
    	onPageIds = newPageIds;
    }

    
    public Set<EsfUUID> getOnPartyIds()
    {
    	return onPartyIds;
    }
    public void setOnPartyIds(Set<EsfUUID> v)
    {
    	onPartyIds = new HashSet<EsfUUID>(v);
    }
    public void addOnPartyId(EsfUUID id)
    {
    	onPartyIds.add(id);
    }
    public boolean isForParty(EsfUUID partyId)
    {
    	return onPartyIds.size() == 0 || onPartyIds.contains(partyId);
    }
    public boolean isForParty(List<PartyTemplate> partyTemplateList)
    {
    	if ( onPartyIds.size() == 0 )
    		return true;
    	if ( partyTemplateList != null )
    	{
        	for( PartyTemplate partyTemplate : partyTemplateList )
        	{
        		if ( onPartyIds.contains(partyTemplate.getId()) )
        			return true;
        	}
    	}
    	return false;
    }
    
    // For imports where we need to change all party ids from one value to another
    public void updatePartyIds(Map<EsfUUID,EsfUUID> partyIdMapping)
    {
    	HashSet<EsfUUID> newPartyIds = new HashSet<EsfUUID>();
    	for( EsfUUID origId : onPartyIds )
    	{
    		EsfUUID newId = partyIdMapping.get(origId);
    		newPartyIds.add( newId != null ? newId : origId );
    	}
    	onPartyIds = newPartyIds;
    }

    
    public List<Action> getActions()
    {
    	return actionList;
    }
    public void setActions(List<Action> v)
    {
    	actionList = new LinkedList<Action>(v);
    }
    public void addAction(Action v)
    {
    	actionList.add(v);
    }
    
   	public StringBuilder appendXml(StringBuilder buf)
   	{
   		buf.append("<DocumentProgrammingRule>\n");
   		
   		buf.append(" <onEventEsfNames>\n");
   		for( EsfName name : onEventEsfNames )
   			buf.append("  <EsfName>").append(name.toXml()).append("</EsfName>\n");
   		buf.append(" </onEventEsfNames>\n");
   		
   		buf.append(" <onPageIds>\n");
   		for( EsfUUID id : onPageIds )
   			buf.append("  <EsfUUID>").append(id.toXml()).append("</EsfUUID>\n");
   		buf.append(" </onPageIds>\n");

   		buf.append(" <onPartyIds>\n");
   		for( EsfUUID id : onPartyIds )
   			buf.append("  <EsfUUID>").append(id.toXml()).append("</EsfUUID>\n");
   		buf.append(" </onPartyIds>\n");

   		buf.append(" <actions>\n");
   		for( Action action : actionList )
   			action.appendXml(buf);
   		buf.append(" </actions>\n");

   		buf.append("</DocumentProgrammingRule>\n");
   		return buf;
   	}
   	
	public int getEstimatedLengthXml() {
		int size = 175 + (onEventEsfNames.size() * 72 ) + (onPageIds.size() * 72 ) + (onPartyIds.size() * 72 );
		for( Action action : actionList )
			size += action.getEstimatedLengthXml();
		return size;
	}

   	public String toXml()
   	{
   		StringBuilder buf = new StringBuilder(getEstimatedLengthXml());
   		return appendXml(buf).toString();
   	}
	public byte[] toXmlByteArray() {
		String xml = toXml();
		return EsfString.stringToBytes(xml);
	}
	
	public DocumentProgrammingRule(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException 
	{
		try 
		{
			String elementName = element.getName();
			if (!elementName.equals("DocumentProgrammingRule")) 
			{
				_logger.error("DocumentProgrammingRule(): Element is not DocumentProgrammingRule.  Found instead: " + elementName);
				throw new EsfException("The DocumentProgrammingRule tag is missing.");
			}

			Element namesElement = element.getChild("onEventEsfNames", ns);
			if (namesElement != null) 
			{
				List<Element> namesList = namesElement.getChildren("EsfName", ns);
				ListIterator<Element> iter = namesList.listIterator();
				while (iter.hasNext()) 
				{
					Element esfNameElement = iter.next();
					EsfName esfName = new EsfName( esfNameElement.getTextTrim() );
					addOnEventEsfName(esfName);
				}
			} 
			else 
			{
				_logger.warn("DocumentProgrammingRule(): No DocumentProgrammingRule.onEventEsfNames element was found");
			}
			
			Element idsElement = element.getChild("onPageIds", ns);
			if (idsElement != null) 
			{
				List<Element> idsList = idsElement.getChildren("EsfUUID", ns);
				ListIterator<Element> iter = idsList.listIterator();
				while (iter.hasNext()) 
				{
					Element idElement = iter.next();
					EsfUUID id = new EsfUUID( idElement.getTextTrim() );
					addOnPageId(id);
				}
			} 
			else 
			{
				_logger.warn("DocumentProgrammingRule(): No DocumentProgrammingRule.onPageIds element was found");
			}
			
			idsElement = element.getChild("onPartyIds", ns);
			if (idsElement != null) 
			{
				List<Element> idsList = idsElement.getChildren("EsfUUID", ns);
				ListIterator<Element> iter = idsList.listIterator();
				while (iter.hasNext()) 
				{
					Element idElement = iter.next();
					EsfUUID id = new EsfUUID( idElement.getTextTrim() );
					addOnPartyId(id);
				}
			} 
			else 
			{
				_logger.warn("DocumentProgrammingRule(): No DocumentProgrammingRule.onPartyIds element was found");
			}
			
			Element actionsElement = element.getChild("actions", ns);
			if (actionsElement != null) 
			{
				List<Element> actionElementList = actionsElement.getChildren("Action", ns);
				ListIterator<Element> iter = actionElementList.listIterator();
				while (iter.hasNext()) 
				{
					Element actionElement = iter.next();
					
					Action action = Action.createFromJdom(actionElement, ns, _logger);
					if ( action != null )
						actionList.add(action);
					else
						_logger.warn("DocumentProgrammingRule() - Could not create the Action type: " + actionElement.getAttributeValue("type"));
				}
			} 
			else 
			{
				_logger.warn("DocumentProgrammingRule(): No DocumentProgrammingRule.onPartyIds element was found");
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
	}

}