// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.util.XmlUtil;

/**
* PackageVersionReportField holds the definition of a single report field to be extracted from 
* this package's documents' fields.
* 
* @author Yozons, Inc.
*/
public class PackageVersionReportField
	extends com.esignforms.open.db.DatabaseObject
	implements java.io.Serializable
{
	private static final long serialVersionUID = 1350416470695180978L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackageVersionReportField.class);
    
    public final static String SHOW_TO_DO = Literals.Y;
    public final static String DO_NOT_SHOW_TO_DO = Literals.N;

    protected EsfUUID id;
    protected EsfUUID packageVersionId;
    protected EsfUUID documentId; 
    protected EsfName fieldName; 
    protected EsfUUID reportFieldTemplateId;
    protected short fieldOrder; 
    protected String showToDo;
    
    // This is a calculated field
    protected EsfName reportFieldTemplateName;
    
    /**
     * This creates a PackageVersionReportField object from data retrieved from the DB.
     */
    protected PackageVersionReportField(EsfUUID id, EsfUUID packageVersionId, EsfUUID documentId, EsfName fieldName, EsfUUID reportFieldTemplateId, short fieldOrder, String showToDo )
    {
    	this.id = id;
    	this.packageVersionId = packageVersionId;
    	this.documentId = documentId;
    	this.fieldName = fieldName;
    	this.reportFieldTemplateId = reportFieldTemplateId;
    	ReportFieldTemplate rft = getReportFieldTemplate();
    	this.reportFieldTemplateName = rft == null ? null : rft.getFieldName();
    	this.fieldOrder = fieldOrder;
    	this.showToDo = showToDo;
    }
    protected PackageVersionReportField(EsfUUID packageVersionId, PackageVersionReportField likeTemplate )
    {
    	this.id = new EsfUUID();
    	this.packageVersionId = packageVersionId;
    	this.documentId = likeTemplate.documentId;
    	this.fieldName = likeTemplate.fieldName;
    	this.reportFieldTemplateId = likeTemplate.reportFieldTemplateId;
    	ReportFieldTemplate rft = getReportFieldTemplate();
    	this.reportFieldTemplateName = rft == null ? null : rft.getFieldName();
    	this.fieldOrder = likeTemplate.fieldOrder;
    	this.showToDo = likeTemplate.showToDo;
    }
    
    public PackageVersionReportField duplicate()
    {
    	PackageVersionReportField pvrf = new PackageVersionReportField(id,packageVersionId,documentId,fieldName.duplicate(),reportFieldTemplateId,fieldOrder,showToDo);
    	pvrf.setDatabaseObjectLike(this);
    	return pvrf;
    }

    
    public EsfUUID getId()
    {
        return id;
    }
    
    public EsfUUID getPackageVersionId()
    {
        return packageVersionId;
    }
    
    public EsfUUID getDocumentId()
    {
        return documentId;
    }
    public void setDocumentId(EsfUUID v)
    {
    	if ( v != null && ! v.isNull() )
    	{
        	documentId = v;
        	objectChanged();
    	}
    }
    
    public EsfName getFieldName()
    {
        return fieldName;
    }
    public void setFieldName(EsfName v)
    {
    	if ( v != null && v.isValid() )
    	{
        	fieldName = v;
        	objectChanged();
    	}
    }
    
    public EsfUUID getReportFieldTemplateId()
    {
        return reportFieldTemplateId;
    }
    public void setReportFieldTemplateId(EsfUUID v)
    {
    	if ( v != null && ! v.isNull() && ! v.equals(reportFieldTemplateId) )
    	{
    		reportFieldTemplateId = v;
        	objectChanged();
    	}
    }
    public ReportFieldTemplate getReportFieldTemplate()
    {
    	return ReportFieldTemplate.Manager.getById(reportFieldTemplateId);
    }
    // These two are used mostly for Vaadin updates
    public EsfName getReportFieldTemplateName()
    {
    	return reportFieldTemplateName;
    }
    public void setReportFieldTemplateName(EsfName v)
    {
    	if ( v != null && v.isValid() && ! v.equals(reportFieldTemplateName) )
    	{
    		reportFieldTemplateName = v;
    		ReportFieldTemplate rft = ReportFieldTemplate.Manager.getByName(v);
    		if ( rft == null ) 
    		{
    			reportFieldTemplateId = null;
    			objectChanged();
    		}
    		else
    			setReportFieldTemplateId(rft.getId());
    	}
    }
    
    public short getFieldOrder()
    {
        return fieldOrder;
    }
    public void setFieldOrder(short v)
    {
    	if ( fieldOrder != v )
    	{
    		fieldOrder = v;
    		objectChanged();
    	}
    }
    
    public String getShowToDo()
    {
    	return showToDo;
    }
    public boolean isShowToDo()
    {
    	return SHOW_TO_DO.equals(showToDo);
    }
    public void setShowToDo(String v)
    {
    	if ( ! showToDo.equals(v) )
    	{
        	showToDo = SHOW_TO_DO.equals(v) ? SHOW_TO_DO : DO_NOT_SHOW_TO_DO;
        	objectChanged();
    	}
    }
    public void setShowToDo(boolean v)
    {
    	setShowToDo( v ? SHOW_TO_DO : DO_NOT_SHOW_TO_DO );
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof PackageVersionReportField )
        {
        	PackageVersionReportField other = (PackageVersionReportField)o;
            return getId().equals(other.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<PackageVersionReportField xmlns=\"").append(XmlUtil.getXmlNamespace2013()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <packageVersionId>").append(packageVersionId.toXml()).append("</packageVersionId>\n");
        buf.append(" <documentId>").append(documentId.toXml()).append("</documentId>\n");
        buf.append(" <fieldName>").append(fieldName.toXml()).append("</fieldName>\n");
        buf.append(" <reportFieldTemplateId>").append(reportFieldTemplateId.toXml()).append("</reportFieldTemplateId>\n");
        ReportFieldTemplate rft = getReportFieldTemplate();
        if ( rft != null )
        	rft.appendXml(buf);
        buf.append(" <fieldOrder>").append(fieldOrder).append("</fieldOrder>\n");
        buf.append(" <showToDo>").append(escapeXml(showToDo)).append("</showToDo>\n");
       	
        buf.append("</PackageVersionReportField>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 108 + 100 + id.getEstimatedLengthXml() + packageVersionId.getEstimatedLengthXml() + documentId.getEstimatedLengthXml() + fieldName.getEstimatedLengthXml() + reportFieldTemplateId.getEstimatedLengthXml();
        len += 4; // field order
        len += showToDo.length();
        return len; 
    }

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */

    public synchronized boolean save(final Connection con)
		throws SQLException
	{
		_logger.debug("save(con) on id: " + id + "; packageVersionId: " + packageVersionId + "; documentId: " + documentId + "; fieldName: " + fieldName + "; insert: " + doInsert());
	    
	    clearLastSQLException();
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        if ( doInsert() )
	        {
	            stmt = new EsfPreparedStatement(con, 
	            	"INSERT INTO esf_package_version_report_field (id, package_version_id,document_id,field_esfname,report_field_template_id,field_order,show_to_do) VALUES (?,?,?,?,?,?,?)");
	            stmt.set(id);
	            stmt.set(packageVersionId);
	            stmt.set(documentId);
	            stmt.set(fieldName);
	            stmt.set(reportFieldTemplateId);
	            stmt.set(fieldOrder);
	            stmt.set(showToDo);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Insert failed for id: " + id + "; packageVersionId: " + packageVersionId + "; documentId: " + documentId + "; fieldName: " + fieldName);
	                return false;
	            }
	            
	            // Now we mark this object as if it were loaded fresh from the database
	            setLoadedFromDb();
	            
	            return true;
	        }
	        
	        if ( hasChanged() )
	        {
	            // We assume we'll update it instead
	            stmt = new EsfPreparedStatement( con, 
	            		"UPDATE esf_package_version_report_field SET document_id=?, field_esfname=?, report_field_template_id=?, field_order=?, show_to_do=? WHERE id=?"
	            						   		);
	            stmt.set(documentId);
	            stmt.set(fieldName);
	            stmt.set(reportFieldTemplateId);
	            stmt.set(fieldOrder);
	            stmt.set(showToDo);
	            stmt.set(id);
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Update failed for id: " + id + "; packageVersionId: " + packageVersionId + "; documentId: " + documentId + "; fieldName: " + fieldName);
	            }
	        }

	        setLoadedFromDb();
	        return true;
	    }
	    catch(SQLException e)
	    {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; packageVersionId: " + packageVersionId + "; documentId: " + documentId + "; fieldName: " + fieldName + "; insert: " + doInsert());
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}
    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
            if ( result )
            	con.commit();
            else
            	con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }


    public synchronized boolean delete(final Connection con, final Errors errors)
	    throws SQLException
	{
		_logger.debug("delete(con) on id: " + id + "; packageVersionId: " + packageVersionId + "; documentId: " + documentId + "; fieldName: " + fieldName);
	    
	    clearLastSQLException();
	
	    // Let's do some referential integrity checks to be sure it's not in use 
	    if ( ! checkReferential(con,errors) )
	        return false;
	
	    if ( doInsert() )
	    {
	    	_logger.warn("delete(con) - Ignored delete of package version report field that was pending an INSERT id: " + id + 
	    			"; packageVersionId: " + packageVersionId + "; documentId: " + documentId + "; fieldName: " + fieldName);
	        objectDeleted();
	        return true;
	    }
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	    	String sql = "DELETE FROM esf_package_version_report_field WHERE id=?";
	    	stmt = new EsfPreparedStatement(con,sql);
            stmt.set(id);
	        int num = stmt.executeUpdate();
	        if ( num == 0 )
	        	_logger.warn("delete(con) - Failed to find esf_package_version_report_field database row to delete with id: " + id + 
	        			"; packageVersionId: " + packageVersionId + "; documentId: " + documentId + "; fieldName: " + fieldName);
	        
	        objectDeleted();
	        
	        return true;
	    }
	    catch(SQLException e)
	    {
	    	_logger.sqlerr(e,"delete(con) on id: " + id + "; packageVersionId: " + packageVersionId + "; documentId: " + documentId + "; fieldName: " + fieldName);
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}

	public boolean delete(final Errors errors)
	{
	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        boolean result = delete(con,errors);
	        con.commit();
	        return result;
	    }
	    catch(SQLException e) 
	    {
	        pool.rollbackIgnoreException(con,e);
	        return false;
	    }
	    finally
	    {
	    	Application.getInstance().cleanupPool(pool,con,null);
	    }
	}
	public synchronized boolean delete()
	{
		Errors nullErrors = null;
	    return delete(nullErrors);
	}
    
	protected boolean checkReferential(final Connection con, final Errors errors)
    	throws SQLException
    {
		return true;
	}

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all PackageVersionReportField objects held in the package version.
   	     * @return the List of PackageVersionReportField found.
   	     */
   	    public static List<PackageVersionReportField> getAll(EsfUUID packageVersionId)
   	    {
   	    	_logger.debug("Manager.getAll() - packageVersionId: " + packageVersionId);
   	    	
   	    	LinkedList<PackageVersionReportField> list = new LinkedList<PackageVersionReportField>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, 
   	        			"SELECT id,document_id,field_esfname,report_field_template_id,field_order,show_to_do FROM esf_package_version_report_field WHERE package_version_id=? ORDER BY field_order"
   	            						   		);
   	        	stmt.set(packageVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	EsfUUID docId = rs.getEsfUUID();
	            	EsfName fieldName = rs.getEsfName();
	            	EsfUUID reportFieldTemplateId = rs.getEsfUUID();
	            	short fieldOrder = rs.getShort();
	            	String showToDo = rs.getString();
		            
	            	PackageVersionReportField reportField = new PackageVersionReportField(id,packageVersionId,docId,fieldName,reportFieldTemplateId,fieldOrder,showToDo);
	            	reportField.setLoadedFromDb();
		            list.add(reportField);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - packageVersionId: " + packageVersionId + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - packageVersionId: " + packageVersionId + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	            list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }

   	    /**
   	     * Retrieves all report field template ids held in the package version that are set for To Do.
   	     * @return the List of EsfUUIDs for report field templates found marked for use by To Do
   	     */
   	    public static List<EsfUUID> getToDoReportFieldTemplateIdList(EsfUUID packageVersionId)
   	    {
   	    	_logger.debug("Manager.getToDoReportFieldTemplateIdList() - packageVersionId: " + packageVersionId);
   	    	
   	    	LinkedList<EsfUUID> list = new LinkedList<EsfUUID>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, 
   	        			"SELECT report_field_template_id FROM esf_package_version_report_field WHERE package_version_id=? AND show_to_do=? ORDER BY field_order"
   	            						   		);
   	        	stmt.set(packageVersionId);
   	        	stmt.set(SHOW_TO_DO);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            
	            while ( rs.next() )
	            {
	            	EsfUUID reportFieldTemplateId = rs.getEsfUUID();
		            list.add(reportFieldTemplateId);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getToDoReportFieldTemplateIdList() - packageVersionId: " + packageVersionId + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getToDoReportFieldTemplateIdList() - packageVersionId: " + packageVersionId + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	            list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }

   	    public static PackageVersionReportField createLike(EsfUUID packageVersionId, PackageVersionReportField likeTemplate)
   	    {
   	    	return new PackageVersionReportField(packageVersionId, likeTemplate );
   	    }
   	    
		public static PackageVersionReportField createNew(EsfUUID packageVersionId, EsfUUID documentId, EsfUUID defaultReportFieldTemplateId, short fieldOrder)
   	    {
   	    	return new PackageVersionReportField(new EsfUUID(), packageVersionId, documentId, new EsfName(), defaultReportFieldTemplateId, fieldOrder, DO_NOT_SHOW_TO_DO);
   	    }
		
		
		public static PackageVersionReportField createFromJDOM(Element element, EsfUUID packageVersionId, Map<EsfUUID,EsfUUID> idMap)
   	    {
			Application app = Application.getInstance();
			
			EsfUUID reportFieldTemplateId = new EsfUUID(element.getChildText("reportFieldTemplateId", element.getNamespace()));
			Element reportFieldTemplateElement = element.getChild("ReportFieldTemplate",element.getNamespace());
			if ( reportFieldTemplateElement != null )
			{
				ReportFieldTemplate importedRft = ReportFieldTemplate.Manager.createFromJDOM(reportFieldTemplateElement);
				ReportFieldTemplate existingRft = ReportFieldTemplate.Manager.getByName(importedRft.getFieldName());
				if ( existingRft == null )
				{
					if ( importedRft.save() )
						reportFieldTemplateId = importedRft.getId();
				}
				else
					reportFieldTemplateId = existingRft.getId();
			}
			
			EsfUUID documentId = new EsfUUID(element.getChildText("documentId", element.getNamespace()));
			if ( idMap.containsKey(documentId) )
				documentId = idMap.get(documentId);
			
			EsfName fieldName = new EsfName(element.getChildText("fieldName",element.getNamespace()));
			
			short fieldOrder = app.stringToShort(element.getChildTextTrim("fieldOrder",element.getNamespace()), (short)0);
			String showToDo = element.getChildTextTrim("showToDo",element.getNamespace());
			
   	    	return new PackageVersionReportField(new EsfUUID(), packageVersionId, documentId, fieldName, reportFieldTemplateId, fieldOrder, showToDo);
   	    }
		
   	} // Manager
    
}