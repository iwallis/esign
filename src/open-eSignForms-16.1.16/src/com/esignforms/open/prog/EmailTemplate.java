// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.User;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* EmailTemplate holds the definition of a set of properties (name-value pairs) held in a library (or document id).  They are versioned and each is tagged with test/production versions.
* 
* @author Yozons, Inc.
*/
public class EmailTemplate
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<EmailTemplate>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable
{
	private static final long serialVersionUID = -5085627370274321818L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(EmailTemplate.class);

    protected final EsfUUID id;
    protected final EsfUUID containerId;
    protected EsfName esfname;
    protected EsfName originalEsfName;
    protected String  description;
    protected String  comments;
    protected String  status;
    protected int	  productionVersion;
    protected int	  testVersion;
    
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();
    
    public enum INCLUDE { ONLY_ENABLED, ONLY_DISABLED, BOTH_ENABLED_AND_DISABLED };

    /**
     * This creates a EmailTemplate object from data retrieved from the DB.
     */
    protected EmailTemplate(EsfUUID id, EsfUUID containerId, EsfName esfname, String description, 
    		           String status, int productionVersion, int testVersion, String comments)
    {
        this.id = id;
        this.containerId = containerId;
        this.esfname = esfname;
        this.originalEsfName = esfname.duplicate();
        this.description = description;
        this.status = status;
        this.productionVersion = productionVersion;
        this.testVersion = testVersion;
        this.comments = comments;
    }
    
    protected EmailTemplate(EsfUUID containerId)
    {
        this.id = new EsfUUID();
        this.containerId = containerId;
        this.esfname = new EsfName("NewEmptyEmailTemplate_PleaseRename");
        this.originalEsfName = esfname.duplicate();
        this.description = null;
        this.status = Literals.STATUS_ENABLED;
        this.productionVersion = 0;
        this.testVersion = 1;
        this.comments = null;
    }

   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getContainerId()
    {
        return containerId;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }
    
    public void makeEnabled()
    {
        status = Literals.STATUS_ENABLED;
        objectChanged();
    }
     
    public void makeDisabled()
    {
        status = Literals.STATUS_DISABLED;
        objectChanged();
    }
    
    public void setStatus(String s)
    {
        if ( Literals.STATUS_ENABLED.equals(s) )
            makeEnabled();
        else
            makeDisabled();
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    public void setEsfName(EsfName v)
    {
    	// Block setting a document name to null or an invalid EsfName
    	if ( v == null || ! v.isValid() )
    		return;
        esfname = v;
        objectChanged();
    }
    public EsfName getOriginalEsfName()
    {
        return originalEsfName;
    }
    public void resetOriginalEsfName()
    {
    	originalEsfName = esfname.duplicate();
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}
    
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String v)
    {
    	if ( v == null )
    		description = v;
    	else if ( v.length() > Literals.DESCRIPTION_MAX_LENGTH )
            description = v.substring(0,Literals.DESCRIPTION_MAX_LENGTH).trim();
        else
        	description = v.trim();
        objectChanged();
    }
    
    public String getVersionLabel(int checkVersion)
    {
    	if ( checkVersion > productionVersion )
    		return Literals.VERSION_LABEL_TEST;
    	
    	if ( checkVersion == productionVersion )
    		return Literals.VERSION_LABEL_PRODUCTION;
    	
		return Literals.VERSION_LABEL_NOT_CURRENT;
    }

    public int getProductionVersion()
    {
    	return productionVersion;
    }
    public boolean hasProductionVersion() 
    {
    	return productionVersion > 0;
    }
    public EmailTemplateVersion getProductionEmailTemplateVersion()
    {
    	return hasProductionVersion() ? EmailTemplateVersion.Manager.getByVersion(id, productionVersion) : null;
    }
    
    public int getTestVersion()
    {
    	return testVersion;
    }
    public boolean hasTestVersion() 
    {
    	return testVersion > productionVersion;
    }
    public void promoteTestVersionToProduction()
    {
    	if ( hasTestVersion() )
    	{
        	productionVersion = testVersion;
        	objectChanged();
    	}
    }
    public void revertProductionVersionBackToTest()
    {
       	if ( hasProductionVersion() && ! hasTestVersion() ) 
    	{
       		--productionVersion;
    		objectChanged();
    	}
    }
    public void bumpTestVersion()
    {
    	if ( ! hasTestVersion() ) 
    	{
    		++testVersion;
    		objectChanged();
    	}
    }
    public void dropTestVersion()
    {
    	if ( hasTestVersion() ) 
    	{
    		--testVersion;
    		objectChanged();
    	}
    }
    public EmailTemplateVersion getTestEmailTemplateVersion() // may be the same as the production version 
    {
    	return EmailTemplateVersion.Manager.getByVersion(id, testVersion);
    }
    
    public String getComments()
    {
    	return comments;
    }
    public void setComments(String v)
    {
    	if ( v == null )
    		comments = v;
    	else if ( v.length() > Literals.COMMENTS_MAX_LENGTH )
    		comments = v.substring(0,Literals.COMMENTS_MAX_LENGTH).trim();
        else
        	comments = v.trim();
        objectChanged();
    }
    

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof EmailTemplate )
            return getId().equals(((EmailTemplate)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(EmailTemplate o)
    {
    	return getEsfName().compareTo(o.getEsfName());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<EmailTemplate xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <containerId>").append(containerId.toXml()).append("</containerId>\n");
        buf.append(" <esfname>").append(esfname.toXml()).append("</esfname>\n");
       	buf.append(" <description>").append(escapeXml(description)).append("</description>\n");
        buf.append(" <status>").append(escapeXml(status)).append("</status>\n");
        buf.append(" <comments>").append(escapeXml(comments)).append("</comments>\n");
        buf.append(" <productionVersion>").append(productionVersion).append("</productionVersion>\n");
        buf.append(" <testVersion>").append(testVersion).append("</testVersion>\n");
        
        buf.append("</EmailTemplate>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 240 + id.getEstimatedLengthXml() + containerId.getEstimatedLengthXml() + esfname.getEstimatedLengthXml() + status.length();
        if ( description != null )
            len += description.length();
        if ( comments != null )
            len += comments.length();
        len += 8; // versions
        return len; 
    }

    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; esfname: " + esfname + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
            if ( doInsert() )
            {
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_email_template (id,container_id,esfname,description,status,production_version,test_version,comments) VALUES (?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(containerId);
                stmt.set(esfname);
                stmt.set(description);
                stmt.set(status);
                stmt.set(productionVersion);
                stmt.set(testVersion);
                stmt.set(comments);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; esfname: " + esfname);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this); // We use replace instead of 'add' because on object create, we added to the ID-only cache
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new email template " + getEsfName()); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new email template " + getEsfName());
                }
                
                return true;
            }
            
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_email_template SET esfname=?,description=?,status=?,comments=?,production_version=?,test_version=? WHERE id=?"
                						   );
                stmt.set(esfname);
                stmt.set(description);
                stmt.set(status);
                stmt.set(comments);
            	stmt.set(productionVersion);
                stmt.set(testVersion);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for EmailTemplate id: " + id + "; esfname: " + esfname);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated email template " + getEsfName() + "; status; " + getStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated email template " + getEsfName() + "; status; " + getStatus() + "; prodVersion: " + getProductionVersion());
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; esfname: " + esfname + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; esfname: " + esfname + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use in the transaction definition.
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of email template that was pending an INSERT id: " + id + "; esfname: " + esfname);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
        	// Delete all our versions...
        	for( EmailTemplateVersion emailTemplateVer : EmailTemplateVersion.Manager.getAllByEmailTemplateId(con,id) )
        	{
        		if ( ! emailTemplateVer.delete(con,null,null) )
        		{
        			if ( errors != null )
        				errors.addError("Could not delete email template version id: " + emailTemplateVer.getId());
        			return false;
        		}
        	}
        	
            // Delete the EmailTemplate itself
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_email_template WHERE id=?");
            stmt.set(id);
            int num = stmt.executeUpdate();
            if ( num == 0 )
            	_logger.warn("delete(con) - Failed to find esf_library_email_template database row to delete with id: " + id);
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted email template " + getEsfName() + "; status; " + getStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted email template " + getEsfName() + "; status; " + getStatus());
            }

            return true;
        }
        catch(SQLException e)
        {
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"delete() on id: " + id + "; esfname: " + esfname + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   	    private static UUIDCacheReadOptimized<EmailTemplate> cache = new UUIDCacheReadOptimized<EmailTemplate>();

   	    public static EmailTemplate getByName(EsfUUID containerId, EsfName esfname)
   	    {
   	    	if ( containerId == null || esfname == null || ! esfname.isValid() )
   	    		return null;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT id FROM esf_library_email_template WHERE container_id = ? AND lower(esfname) = ?"
   	        									);
   	        	stmt.set(containerId);
   	        	stmt.set(esfname.toLowerCase());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            con.commit();
	   	            
	   	            return getById(id);
	            }
	            
   	            con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByName() - containerId: " + containerId + "; EmailTemplateName: " + esfname);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        //_logger.debug("Manager.getByName() - containerId: " + containerId + "; EmailTemplateName: " + esfname + "; failed to find the EmailTemplate");
   	        return null; 
   	    }

   		static EmailTemplate getById(Connection con, EsfUUID emailTemplateId) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT container_id,esfname,description,status,production_version,test_version,comments " +
   	        			"FROM esf_library_email_template WHERE id = ?"
   	        									);
   	        	stmt.set(emailTemplateId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID containerId = rs.getEsfUUID();
		            EsfName esfname = rs.getEsfName();
		            String description = rs.getString();
		            String  stat = rs.getString();
		            int productionVersion = rs.getInt();
		            int testVersion = rs.getInt();
		            String comments = rs.getString();
		            
		            EmailTemplate emailTemplate = new EmailTemplate(emailTemplateId,containerId,esfname,description,stat,productionVersion,testVersion,comments);
		            emailTemplate.setLoadedFromDb();
		            cache.add(emailTemplate);
		            
	   	            return emailTemplate;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + emailTemplateId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + emailTemplateId + "; failed to find the email template");
   	        return null; 
   		}
   		
   		public static EmailTemplate getById(EsfUUID emailTemplateId)
   		{
   	    	EmailTemplate emailTemplate = cache.getById(emailTemplateId);
   	    	if ( emailTemplate != null )
   	    		return emailTemplate;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	emailTemplate = getById(con,emailTemplateId);
   	        	if ( emailTemplate != null ) 
   	        	{
		            con.commit();
	   	        	return emailTemplate;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   	    /**
   	     * Retrieves all EmailTemplate objects held in the specified container.
   	     * @return the List of EmailTemplate found in name order.
   	     */
   	    public static List<EmailTemplate> getAll(EsfUUID containerId)
   	    {
   	    	LinkedList<EmailTemplate> emailTemplateList = new LinkedList<EmailTemplate>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id FROM esf_library_email_template WHERE container_id=? ORDER BY lower(esfname) ASC"
   	            						   );
   	        	stmt.set(containerId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            
	            	EmailTemplate emailTemplate = getById(id);
	            	emailTemplateList.add(emailTemplate);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - containerId: " + containerId + "; found: " + emailTemplateList.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - containerId: " + containerId + "; found: " + emailTemplateList.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	            emailTemplateList.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return emailTemplateList;
   	    }
   	    
   		public static EmailTemplate getDefaultPickupNotification()
   		{
   			return getByName(Library.Manager.getTemplate().getId(), Application.getInstance().getEmailTemplateDefaultPickupNotificationEsfName());
   		}
   		
   	    public static EmailTemplate createNew(EsfUUID containerId)
   	    {
   	    	if ( containerId == null )
   	    		return null;
   	    	EmailTemplate newEmailTemplate = new EmailTemplate(containerId);
   	    	cache.add(newEmailTemplate);
   	    	return newEmailTemplate;
   	    }

   	    public static EmailTemplate createLike(EsfUUID containerId, EmailTemplate likeEmailTemplate, EsfName newName)
   	    {
   	    	if ( containerId == null || likeEmailTemplate == null || newName == null || ! newName.isValid() )
   	    		return null;
   	    	
   	    	EmailTemplate newEmailTemplate = createNew(containerId);
   	    	newEmailTemplate.setEsfName(newName);
   	    	newEmailTemplate.setDescription( likeEmailTemplate.getDescription() );
   	    	newEmailTemplate.setComments( likeEmailTemplate.getComments() );
   	    	return newEmailTemplate;
   	    }
   	    
   	    public static EmailTemplate createFromJDOM(EsfUUID containerId, Element e)
   	    {
   	    	if ( containerId == null || e == null )
   	    		return null;
   	    	
   	    	EmailTemplate newEmail = new EmailTemplate(
   	    			new EsfUUID(), 
   	    			containerId, 
   	    			EsfName.createFromToXml(e.getChildText("esfname", e.getNamespace())),
   	    			e.getChildText("description", e.getNamespace()), 
   	    			e.getChildText("status", e.getNamespace()), 
   	    			0, 1,
   	    			e.getChildText("comments", e.getNamespace()) 
   	    										);
   	    	newEmail.setNewObject();
   	    	cache.add(newEmail);
   	    	return newEmail;
   	    }
   	    
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all documents that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}

   	} // Manager
   	
}