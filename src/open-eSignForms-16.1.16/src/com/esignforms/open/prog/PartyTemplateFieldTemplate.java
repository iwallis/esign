// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.util.XmlUtil;

/**
* PartyTemplateFieldTemplate holds the definition of a single field that can be updated by a given party template.
* 
* @author Yozons, Inc.
*/
public class PartyTemplateFieldTemplate
	extends com.esignforms.open.db.DatabaseObject
	implements java.io.Serializable
{
	private static final long serialVersionUID = 8930846712675528850L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PartyTemplateFieldTemplate.class);

    protected EsfUUID partyTemplateId;
    protected EsfUUID fieldTemplateId;
    protected String  keepRequired; // used to override the field's required attribute on a per-party basis.s

    /**
     * This creates a PartyTemplateFieldTemplate object from data retrieved from the DB.
     */
    protected PartyTemplateFieldTemplate(EsfUUID partyTemplateId, EsfUUID fieldTemplateId, String keepRequired)
    {
    	this.partyTemplateId = partyTemplateId;
    	this.fieldTemplateId = fieldTemplateId;
    	this.keepRequired = keepRequired;
    }
    protected PartyTemplateFieldTemplate(EsfUUID partyTemplateId, PartyTemplateFieldTemplate likeTemplate )
    {
    	this.partyTemplateId = partyTemplateId;
    	this.fieldTemplateId = likeTemplate.fieldTemplateId;
    	this.keepRequired = likeTemplate.keepRequired;
    }
    
    public PartyTemplateFieldTemplate duplicate()
    {
    	PartyTemplateFieldTemplate ptft = new PartyTemplateFieldTemplate(partyTemplateId, fieldTemplateId, keepRequired);
    	ptft.setDatabaseObjectLike(this);
    	return ptft;
    }
    
    public EsfUUID getPartyTemplateId()
    {
        return partyTemplateId;
    }
    
    public EsfUUID getFieldTemplateId()
    {
        return fieldTemplateId;
    }
    
    public String getKeepRequired()
    {
    	return keepRequired;
    }
    public void setKeepRequired(String v)
    {
    	keepRequired = FieldTemplate.REQUIRED.equals(v) ? FieldTemplate.REQUIRED : FieldTemplate.OPTIONAL;
    	objectChanged();
    }
    public boolean isKeepRequired()
    {
    	return FieldTemplate.REQUIRED.equals(keepRequired);
    }
    public boolean isOverrideAsOptional()
    {
    	return FieldTemplate.OPTIONAL.equals(keepRequired);
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof PartyTemplateFieldTemplate )
        {
        	PartyTemplateFieldTemplate other = (PartyTemplateFieldTemplate)o;
            return getPartyTemplateId().equals(other.getPartyTemplateId()) && getFieldTemplateId().equals(other.getFieldTemplateId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getPartyTemplateId().hashCode() + getFieldTemplateId().hashCode();
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<PartyTemplateFieldTemplate xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <partyTemplateId>").append(partyTemplateId.toXml()).append("</partyTemplateId>\n");
        buf.append(" <fieldTemplateId>").append(fieldTemplateId.toXml()).append("</fieldTemplateId>\n");
        buf.append(" <keepRequired>").append(escapeXml(keepRequired)).append("</keepRequired>\n");

        buf.append("</PartyTemplateFieldTemplate>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 175 + partyTemplateId.getEstimatedLengthXml() + fieldTemplateId.getEstimatedLengthXml() + keepRequired.length();
        return len; 
    }

    
    public synchronized boolean save(final Connection con)
		throws SQLException
	{
		//_logger.debug("save(con) on partyTemplateId: " + partyTemplateId + "; fieldTemplateId: " + fieldTemplateId + "; insert: " + doInsert());
	    
	    clearLastSQLException();
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        if ( doInsert() )
	        {
	            stmt = new EsfPreparedStatement(con, 
	            	"INSERT INTO esf_party_template_field_template (party_template_id,field_template_id,keep_required) " +
	            	"VALUES(?,?,?)");
	            stmt.set(partyTemplateId);
	            stmt.set(fieldTemplateId);
	            stmt.set(keepRequired);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Insert failed for partyTemplateId: " + partyTemplateId + "; fieldTemplateId: " + fieldTemplateId);
	                return false;
	            }
	            
	            // Now we mark this object as if it were loaded fresh from the database
	            setLoadedFromDb();
	            
	            return true;
	        }
	        
	        if ( hasChanged() )
	        {
	            // We assume we'll update it instead
	            stmt = new EsfPreparedStatement( con, 
	            		"UPDATE esf_party_template_field_template SET keep_required=? WHERE party_template_id=? AND field_template_id=?"
	            						   		);
	            stmt.set(keepRequired);
	            stmt.set(partyTemplateId);
	            stmt.set(fieldTemplateId);
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Update failed for partyTemplateId: " + partyTemplateId + "; fieldTemplateId: " + fieldTemplateId);
	            }
	        }

	        setLoadedFromDb();
	        return true;
	    }
	    catch(SQLException e)
	    {
        	_logger.sqlerr(e,"save(con) on partyTemplateId: " + partyTemplateId + "; fieldTemplateId: " + fieldTemplateId + "; insert: " + doInsert());
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}
    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
            if ( result )
            	con.commit();
            else
            	con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }


    public synchronized boolean delete(final Connection con, final Errors errors)
	    throws SQLException
	{
		_logger.debug("delete(con) on partyTemplateId: " + partyTemplateId + "; fieldTemplateId: " + fieldTemplateId);
	    
	    clearLastSQLException();
	
	    // Let's do some referential integrity checks to be sure it's not in use 
	    if ( ! checkReferential(con,errors) )
	        return false;
	
	    if ( doInsert() )
	    {
	    	_logger.warn("delete(con) - Ignored delete of party template to field template that was pending an INSERT partyTemplateId: " + partyTemplateId + "; fieldTemplateId: " + fieldTemplateId);
	        objectDeleted();
	        return true;
	    }
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        // Delete the library
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_party_template_field_template WHERE party_template_id=? AND field_template_id=?");
	        stmt.set(partyTemplateId);
	        stmt.set(fieldTemplateId);
	        int num = stmt.executeUpdate();
	        if ( num == 0 )
	        	_logger.warn("delete(con) - Failed to find esf_party_template_field_template database row to delete with partyTemplateId: " + partyTemplateId + "; fieldTemplateId: " + fieldTemplateId);
	        
	        objectDeleted();
	        
	        return true;
	    }
	    catch(SQLException e)
	    {
	    	_logger.sqlerr(e,"delete(con) on partyTemplateId: " + partyTemplateId + "; fieldTemplateId: " + fieldTemplateId);
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}

	public boolean delete(final Errors errors)
	{
	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        boolean result = delete(con,errors);
	        con.commit();
	        return result;
	    }
	    catch(SQLException e) 
	    {
	        pool.rollbackIgnoreException(con,e);
	        return false;
	    }
	    finally
	    {
	    	Application.getInstance().cleanupPool(pool,con,null);
	    }
	}
	public synchronized boolean delete()
	{
		Errors nullErrors = null;
	    return delete(nullErrors);
	}
    
	protected boolean checkReferential(final Connection con, final Errors errors)
    	throws SQLException
    {
		return true;
	}

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all PartyTemplateFieldTemplate objects held for the specified party id.
   	     * @return the List of PartyTemplateFieldTemplate.
   	     */
   	    public static List<PartyTemplateFieldTemplate> getAllForParty(EsfUUID partyId)
   	    {
   	    	LinkedList<PartyTemplateFieldTemplate> list = new LinkedList<PartyTemplateFieldTemplate>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT field_template_id,keep_required FROM esf_party_template_field_template WHERE party_template_id=?" );
   	        	stmt.set(partyId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID fieldTemplateId = rs.getEsfUUID();
	            	String keepRequired = rs.getString();
		            
	            	PartyTemplateFieldTemplate partyField = new PartyTemplateFieldTemplate(partyId,fieldTemplateId,keepRequired);
	            	partyField.setLoadedFromDb();
		            list.add(partyField);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAllForParty() - partyId: " + partyId + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAllForParty() - partyId: " + partyId + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	            list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static PartyTemplateFieldTemplate createLike(EsfUUID partyId, PartyTemplateFieldTemplate likeTemplate)
   	    {
   	    	return new PartyTemplateFieldTemplate(partyId, likeTemplate );
   	    }
   	    
   	    public static PartyTemplateFieldTemplate createNew(EsfUUID partyTemplateId, EsfUUID fieldTemplateId)
   	    {
   	    	return new PartyTemplateFieldTemplate(partyTemplateId, fieldTemplateId, FieldTemplate.REQUIRED);
   	    }
   	    
   	    public static PartyTemplateFieldTemplate createFromJDOM(EsfUUID partyTemplateId, EsfUUID fieldTemplateId, Element e)
   	    {
   	    	if ( partyTemplateId == null || partyTemplateId.isNull() || fieldTemplateId == null || fieldTemplateId.isNull() || e == null )
   	    		return null;
   	    	
   	    	PartyTemplateFieldTemplate newPartyTemplateFieldTemplate = new PartyTemplateFieldTemplate(
   	    			partyTemplateId, 
   	    			fieldTemplateId, 
   	    			e.getChildText("keepRequired", e.getNamespace())
   	    			);
   	    	newPartyTemplateFieldTemplate.setNewObject();
   	    	return newPartyTemplateFieldTemplate;
   	    }
   	    
   	} // Manager
    
}