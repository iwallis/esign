// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;

/**
* SerialInfo is a data-only object that holds information about a Serial.
* It's not a mutable object (you can't save/delete it), just for lists of serial generators.
* 
* @author Yozons, Inc.
*/
public class SerialInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<SerialInfo>
{
	private static final long serialVersionUID = -2124409576525024243L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(SerialInfo.class);

    protected EsfUUID id;
    protected EsfName esfname;
    protected String  description;
    protected String  comments;
    protected String  status;
    protected int	  productionVersion;
    protected int	  testVersion;
    protected EsfInteger  nextProductionSerial;
    protected EsfInteger  nextTestSerial;

    /**
     * This creates a new SerialInfo using the source object.
     * @param serial the Serial to use as the source
     */
    protected SerialInfo(Serial serial)
    {
    	this.id = serial.getId();
    	this.esfname = serial.getEsfName();
    	this.description = serial.getDescription();
    	this.comments = serial.getComments();
    	this.status = serial.getStatus();
    	this.productionVersion = serial.getProductionVersion();
    	this.testVersion = serial.getTestVersion();
    	
    	this.nextProductionSerial = serial.getNextProductionSerial();
    	this.nextTestSerial = serial.getNextTestSerial();
    }
    
    /**
     * This creates a SerialInfo object from data retrieved from the DB.
     */
    protected SerialInfo(EsfUUID id, EsfName esfname, String description, String comments, String status, int productionVersion, int testVersion, EsfInteger nextProductionSerial, EsfInteger nextTestSerial)
    {
    	this.id = id;
    	this.esfname = esfname;
    	this.description = description;
    	this.status = status;
    	this.comments = comments;
    	this.productionVersion = productionVersion;
    	this.testVersion = testVersion;
    	
    	this.nextProductionSerial = nextProductionSerial;
    	this.nextTestSerial = nextTestSerial;
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    
    public String getDescription()
    {
        return description;
    }
    public boolean hasDescription()
    {
        return EsfString.isNonBlank(description);
    }
    
    public String getComments()
    {
        return comments;
    }
    public boolean hasComments()
    {
        return EsfString.isNonBlank(comments);
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }

    public int getProductionVersion()
    {
    	return productionVersion;
    }
    
    public int getTestVersion()
    {
    	return testVersion;
    }

    public boolean hasProductionVersion()
    {
    	return productionVersion > 0;
    }

    public boolean hasTestVersion()
    {
    	return testVersion > productionVersion;
    }

    public EsfUUID getId()
    {
        return id;
    }
    
    public EsfInteger getNextProductionSerial()
    {
    	return nextProductionSerial;
    }

    public EsfInteger getNextTestSerial()
    {
    	return nextTestSerial;
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof SerialInfo )
            return getId().equals(((SerialInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(SerialInfo o)
    {
    	return getEsfName().compareTo(o.getEsfName());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all SerialInfo objects in the specified container.
   	     * @return the Collection of SerialInfo found ordered by esfname.
   	     */
   	    public static Collection<SerialInfo> getAll(EsfUUID containerId)
   	    {
   	    	LinkedList<SerialInfo> list = new LinkedList<SerialInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id,esfname,description,comments,status,production_version,test_version,next_production_serial,next_test_serial " +
   	    	            "FROM esf_library_serial " + 
   	    	            "WHERE container_id=? " +
   	    	            "ORDER BY lower(esfname) ASC"
   	            						   );
   	        	stmt.set(containerId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            EsfName esfname = rs.getEsfName();
		            String description = rs.getString();
		            String comments = rs.getString();
		            String  stat = rs.getString();
		            int productionVersion = rs.getInt();
		            int testVersion = rs.getInt();
		            EsfInteger nextProductionSerial = rs.getEsfInteger();
		            EsfInteger nextTestSerial = rs.getEsfInteger();
		            
		            SerialInfo SerialInfo = new SerialInfo(id,esfname,description,comments,stat,productionVersion,testVersion,nextProductionSerial,nextTestSerial);
		            SerialInfo.setLoadedFromDb();
		            list.add(SerialInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - containerId: " + containerId + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - containerId: " + containerId + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static SerialInfo createNew(EsfUUID containerId)
   	    {
   	    	if ( containerId == null )
   	    		return null;
   	    	
   			Serial serial = Serial.Manager.createNew(containerId);
   	    	return new SerialInfo(serial);
   	    }
   	    
   	    public static SerialInfo createLike(Serial likeSerial, EsfName newName)
   	    {
   	    	if ( likeSerial == null || newName == null || ! newName.isValid() )
   	    		return null;

   	    	Serial newSerial = Serial.Manager.createLike(likeSerial.getContainerId(), likeSerial, newName);
   	    	return new SerialInfo(newSerial);
   	    }
   	    
   	    public static SerialInfo createFromSource(Serial serial)
   	    {
   	    	if ( serial == null )
   	    		return null;
   	    	
   	    	SerialInfo serialInfo = new SerialInfo(serial);
   	    	serialInfo.setLoadedFromDb();

   			return serialInfo;
   	    }
   	    
   	} // Manager
   	
}