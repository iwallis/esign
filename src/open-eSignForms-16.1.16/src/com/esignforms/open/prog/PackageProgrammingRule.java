// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.action.Action;


/**
* PackageProgrammingRule defines a rule that can fire when a package is running live in a transaction.
* It essentially checks for a matching event, optional document, optional party, and if matches those has a list of actions to be performed.
* 
* @author Yozons, Inc.
*/
public class PackageProgrammingRule
	implements java.io.Serializable
{
	private static final long serialVersionUID = 1329544301934553221L;

	int order = 1; // not persisted, but maintains the order of this rule with respect to other rules
 	
 	HashSet<EsfName> onEventEsfNames = new HashSet<EsfName>();
    HashSet<EsfUUID> onDocumentIds = new HashSet<EsfUUID>();
    HashSet<EsfUUID> onPartyIds = new HashSet<EsfUUID>();
    LinkedList<Action> actionList = new LinkedList<Action>();
    
    public PackageProgrammingRule() 
    {
    	// see below for how to create this object from JDOM/XML
    }
    
    public PackageProgrammingRule duplicate()
    {
    	PackageProgrammingRule rule = new PackageProgrammingRule();
    	for( EsfName name : onEventEsfNames )
    		rule.addOnEventEsfName( name.duplicate() );
    	for( EsfUUID id : onDocumentIds )
    		rule.addOnDocumentId( id );
    	for( EsfUUID id : onPartyIds )
    		rule.addOnPartyId( id );
    	for( Action action : actionList )
    		rule.addAction( action.duplicate() );
    	return rule;
    }
    
    public int getOrder() { return order; }
    public void setOrder(int v) { order = v; }
    
    public Set<EsfName> getOnEventEsfNames()
    {
    	return onEventEsfNames;
    }
    public void setOnEventEsfNames(Set<EsfName> v)
    {
    	onEventEsfNames = new HashSet<EsfName>(v);
    }
    public void addOnEventEsfName(EsfName name)
    {
    	onEventEsfNames.add(name);
    }
    public boolean isForEvent(EsfName eventName)
    {
    	return onEventEsfNames.contains(eventName);
    }

    public Set<EsfUUID> getOnDocumentIds()
    {
    	return onDocumentIds;
    }
    public void setOnDocumentIds(Set<EsfUUID> v)
    {
    	onDocumentIds = new HashSet<EsfUUID>(v);
    }
    public void addOnDocumentId(EsfUUID id)
    {
    	onDocumentIds.add(id);
    }
    public boolean isForDocument(EsfUUID docId)
    {
    	return onDocumentIds.size() == 0 || onDocumentIds.contains(docId);
    }

    public Set<EsfUUID> getOnPartyIds()
    {
    	return onPartyIds;
    }
    public void setOnPartyIds(Set<EsfUUID> v)
    {
    	onPartyIds = new HashSet<EsfUUID>(v);
    }
    public void addOnPartyId(EsfUUID id)
    {
    	onPartyIds.add(id);
    }
    public boolean isForParty(EsfUUID partyId)
    {
    	return onPartyIds.size() == 0 || onPartyIds.contains(partyId);
    }
    
    public List<Action> getActions()
    {
    	return actionList;
    }
    public void setActions(List<Action> v)
    {
    	actionList = new LinkedList<Action>(v);
    }
    public void addAction(Action v)
    {
    	actionList.add(v);
    }
    
    // Used on importing from XML in which these IDs change from the imported values to the new target system
    public void _updateDocumentAndPartyIds(Map<EsfUUID,EsfUUID> idMap)
    {
    	HashSet<EsfUUID> newOnDocumentIds = new HashSet<EsfUUID>();
    	Iterator<EsfUUID> setIter = onDocumentIds.iterator();
    	while( setIter.hasNext()  )
    	{
    		EsfUUID docId = setIter.next();
    		if ( idMap.containsKey(docId) )
    			docId = idMap.get(docId);
    		newOnDocumentIds.add(docId);
    	}
    	setOnDocumentIds(newOnDocumentIds);
    	
    	HashSet<EsfUUID> newOnPartyIds = new HashSet<EsfUUID>();
    	setIter = onPartyIds.iterator();
    	while( setIter.hasNext()  )
    	{
    		EsfUUID partyId = setIter.next();
    		if ( idMap.containsKey(partyId) )
    			partyId = idMap.get(partyId);
    		newOnPartyIds.add(partyId);
    	}
    	setOnPartyIds(newOnPartyIds);

    	for( Action action : actionList )
    	{
    		action.updateDocumentIds(idMap);
    		action.updatePackagePartyIds(idMap);
    		action.updateDocumentVersionPageIds(idMap); // for completeness, but don't think a Package will have actions that reference page ids
    	}
    }
    
   	public StringBuilder appendXml(StringBuilder buf)
   	{
   		buf.append("<PackageProgrammingRule>\n");
   		
   		buf.append(" <onEventEsfNames>\n");
   		for( EsfName name : onEventEsfNames )
   			buf.append("  <EsfName>").append(name.toXml()).append("</EsfName>\n");
   		buf.append(" </onEventEsfNames>\n");
   		
   		buf.append(" <onDocumentIds>\n");
   		for( EsfUUID id : onDocumentIds )
   			buf.append("  <EsfUUID>").append(id.toXml()).append("</EsfUUID>\n");
   		buf.append(" </onDocumentIds>\n");

   		buf.append(" <onPartyIds>\n");
   		for( EsfUUID id : onPartyIds )
   			buf.append("  <EsfUUID>").append(id.toXml()).append("</EsfUUID>\n");
   		buf.append(" </onPartyIds>\n");

   		buf.append(" <actions>\n");
   		for( Action action : actionList )
   			action.appendXml(buf);
   		buf.append(" </actions>\n");

   		buf.append("</PackageProgrammingRule>\n");
   		return buf;
   	}
   	
	public int getEstimatedLengthXml() {
		int size = 175 + (onEventEsfNames.size() * 72 ) + (onDocumentIds.size() * 72 ) + (onPartyIds.size() * 72 );
		for( Action action : actionList )
			size += action.getEstimatedLengthXml();
		return size;
	}

   	public String toXml()
   	{
   		StringBuilder buf = new StringBuilder(getEstimatedLengthXml());
   		return appendXml(buf).toString();
   	}
	public byte[] toXmlByteArray() {
		String xml = toXml();
		return EsfString.stringToBytes(xml);
	}
	
	public PackageProgrammingRule(Element element, Namespace ns, com.esignforms.open.log.Logger _logger) throws EsfException 
	{
		try 
		{
			String elementName = element.getName();
			if (!elementName.equals("PackageProgrammingRule")) 
			{
				_logger.error("PackageProgrammingRule(): Element is not PackageProgrammingRule.  Found instead: " + elementName);
				throw new EsfException("The PackageProgrammingRule tag is missing.");
			}

			Element namesElement = element.getChild("onEventEsfNames", ns);
			if (namesElement != null) 
			{
				List<Element> namesList = namesElement.getChildren("EsfName", ns);
				ListIterator<Element> iter = namesList.listIterator();
				while (iter.hasNext()) 
				{
					Element esfNameElement = iter.next();
					EsfName esfName = new EsfName( esfNameElement.getTextTrim() );
					addOnEventEsfName(esfName);
				}
			} 
			else 
			{
				_logger.warn("PackageProgrammingRule(): No PackageProgrammingRule.onEventEsfNames element was found");
			}
			
			Element idsElement = element.getChild("onDocumentIds", ns);
			if (idsElement != null) 
			{
				List<Element> idsList = idsElement.getChildren("EsfUUID", ns);
				ListIterator<Element> iter = idsList.listIterator();
				while (iter.hasNext()) 
				{
					Element idElement = iter.next();
					EsfUUID id = new EsfUUID( idElement.getTextTrim() );
					addOnDocumentId(id);
				}
			} 
			else 
			{
				_logger.warn("PackageProgrammingRule(): No PackageProgrammingRule.onDocumentIds element was found");
			}
			
			idsElement = element.getChild("onPartyIds", ns);
			if (idsElement != null) 
			{
				List<Element> idsList = idsElement.getChildren("EsfUUID", ns);
				ListIterator<Element> iter = idsList.listIterator();
				while (iter.hasNext()) 
				{
					Element idElement = iter.next();
					EsfUUID id = new EsfUUID( idElement.getTextTrim() );
					addOnPartyId(id);
				}
			} 
			else 
			{
				_logger.warn("PackageProgrammingRule(): No PackageProgrammingRule.onPartyIds element was found");
			}
			
			Element actionsElement = element.getChild("actions", ns);
			if (actionsElement != null) 
			{
				List<Element> actionElementList = actionsElement.getChildren("Action", ns);
				ListIterator<Element> iter = actionElementList.listIterator();
				while (iter.hasNext()) 
				{
					Element actionElement = iter.next();
					
					Action action = Action.createFromJdom(actionElement, ns, _logger);
					if ( action != null )
						actionList.add(action);
					else
						_logger.warn("PackageProgrammingRule() - Could not create the Action type: " + actionElement.getAttributeValue("type"));
				}
			} 
			else 
			{
				_logger.warn("PackageProgrammingRule(): No PackageProgrammingRule.onPartyIds element was found");
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
	}

}