// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.TreeMap;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;

import com.esignforms.open.Application;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.action.Action;
import com.esignforms.open.util.XmlUtil;

/**
* Based on PackageProgramming.java as of 5/23/2012.
* DocumentProgramming holds custom programming logic associated with a document version.
* 
* @author Yozons, Inc.
*/
public class DocumentProgramming
	extends com.esignforms.open.db.DatabaseObject 
	implements java.io.Serializable
{
	private static final long serialVersionUID = -2308608817102697123L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DocumentProgramming.class);

    EsfUUID id;
    LinkedList<DocumentProgrammingRule> rules;

   	protected DocumentProgramming()
   	{
   		this(new EsfUUID());
   	}
   	protected DocumentProgramming(EsfUUID id)
   	{
   		this.id = id;
   		rules = new LinkedList<DocumentProgrammingRule>();
   	}
   	
   	public DocumentProgramming(byte[] xmlBlob) throws EsfException {
   		String xml = EsfString.bytesToString(xmlBlob);
   		loadXml(xml);
   	}
   	
   	public DocumentProgramming(Element documentProgrammingElement, EsfUUID documentProgrammingId) throws EsfException {
		loadFromJdom(documentProgrammingElement, "<fromJdomElement>PSEUDO XML</fromJdomElement>");
		if ( documentProgrammingId != null && ! documentProgrammingId.isNull() )
			id = documentProgrammingId;
   	}
   	
   	public EsfUUID getId()
   	{
   		return id;
   	}
   	
   	public List<DocumentProgrammingRule> getRules()
   	{
   		return rules;
   	}
   	public List<DocumentProgrammingRule> getDuplicateRules()
   	{
   		LinkedList<DocumentProgrammingRule> dupRules = new LinkedList<DocumentProgrammingRule>();
   		int order = 1;
   		for( DocumentProgrammingRule rule : rules )
   		{
   			DocumentProgrammingRule dupRule = rule.duplicate();
   			dupRule.setOrder(order++);
   			dupRules.add(dupRule);
   		}
   		return dupRules;
   	}
   	public void setRules(List<DocumentProgrammingRule> rules)
   	{
   		this.rules = new LinkedList<DocumentProgrammingRule>(rules);
   		objectChanged();
   	}
   	
    public DocumentProgramming duplicate() 
    {
    	DocumentProgramming programming = new DocumentProgramming(id);
    	programming.setRules(rules);
    	programming.setDatabaseObjectLike(this);
    	return programming;
    }

   	
	protected void loadXml(String xml) throws EsfException 
	{
		StringReader sr = null;

		SAXBuilder saxBuilder = new SAXBuilder(XMLReaders.NONVALIDATING);
		saxBuilder.setIgnoringElementContentWhitespace(true);

		try 
		{
			sr = new StringReader(xml);

			Document doc = saxBuilder.build(sr);
			Element rootElement = doc.getRootElement();

			loadFromJdom(rootElement, xml);
		} 
		catch (EsfException e) 
		{
			throw e;
		} 
		catch (java.io.IOException e) 
		{
			_logger.error("loadXml() - could not read programming from the XML string: " + xml, e);
			throw new EsfException("Could not load the programming configuration.");
		} 
		catch (JDOMException e) 
		{
			_logger.error("loadXml() - could not XML parse programming from the XML string: " + xml, e);
			throw new EsfException("Could not XML parse and load the programming configuration.");
		} 
		finally 
		{
			if (sr != null)
			{
				try 
				{
					sr.close();
				} 
				catch (Exception e) {}
			}
		}
	}

	// Utility routine for the constructors to load the XML programming from the string
	// representation into this object
	protected void loadFromJdom(Element rootElement, String xml) throws EsfException 
	{
		Namespace ns = Namespace.getNamespace(XmlUtil.getXmlNamespace2012());

		try 
		{
			String rootName = rootElement.getName();
			if (!rootName.equals("DocumentProgramming")) 
			{
				_logger.error("loadFromJdom(): Root element is not DocumentProgramming.  Found instead: " + rootName + "; in XML: "+ xml);
				throw new EsfException("The DocumentProgramming root tag is missing.");
			}

			id = EsfUUID.createFromToXml(rootElement.getChildText("id", ns));
			if (id.isNull()) 
			{
				_logger.error("loadFromJdom(): required id element is missing in XML: " + xml);
				throw new EsfException("The DocumentProgramming.id element is missing.");
			}

			rules = new LinkedList<DocumentProgrammingRule>();
			List<Element> rulesList = rootElement.getChildren("DocumentProgrammingRule", ns);
			if (rulesList != null) 
			{
				ListIterator<Element> iter = rulesList.listIterator();
				int order = 1;
				while (iter.hasNext()) 
				{
					Element ruleElement = iter.next();
					DocumentProgrammingRule rule = new DocumentProgrammingRule(ruleElement,ns,_logger);
					rule.setOrder(order++);
					rules.add(rule);
				}
			} 
			else 
			{
				_logger.warn("loadFromJdom(): No DocumentProgramming.DocumentProgrammingRule elements were found in XML: " + xml);
			}
		} 
		catch (EsfException e) 
		{
			throw e;
		}
	}

   	
   	public StringBuilder appendXml(StringBuilder buf)
   	{
   		buf.append("<DocumentProgramming xmlns=\"").append(XmlUtil.getXmlNamespace2012()).append("\">\n");
		buf.append(" <id>").append(id.toXml()).append("</id>\n");
		
		for( DocumentProgrammingRule rule : rules )
			rule.appendXml(buf);
   		
   		buf.append("</DocumentProgramming>\n");
   		return buf;
   	}
   	
	public int getEstimatedLengthXml() {
		int size = 200;
		for( DocumentProgrammingRule rule : rules )
			size += rule.getEstimatedLengthXml();
		return size;
	}

   	public String toXml()
   	{
   		StringBuilder buf = new StringBuilder(getEstimatedLengthXml());
   		return appendXml(buf).toString();
   	}
	public byte[] toXmlByteArray() {
		String xml = toXml();
		return EsfString.stringToBytes(xml);
	}
	
	/*
	 * =========================== BEGIN DATABASE RELATED ROUTINES =============================
	 */
	
	public boolean save(Connection con) throws SQLException 
	{
		try 
		{
			if ( doInsert() ) 
			{
				byte[] xmlBlob = toXmlByteArray();

				getBlobDb().insert(con, id, xmlBlob, BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE);

				// Now we mark this object as if it were loaded fresh from the database
				setLoadedFromDb();
				return true;
			}

			// This must be an update request. But if it hasn't changed, we can
			// just treat this as a null operation

			if ( ! hasChanged() )
				return true;

			byte[] xmlBlob = toXmlByteArray();

			getBlobDb().update(con, id, xmlBlob, BlobDb.CompressOption.ENABLE, BlobDb.EncryptOption.ENABLE);

			// Now we mark this object as if it were loaded fresh from the database
			setLoadedFromDb();
			return true;
		} 
		catch (SQLException e) 
		{
			setLastSQLException(e);
			throw e;
		}
	}

	public boolean save() 
	{
		_logger.debug("save() doInsert: " + doInsert() + "; id: " + id);

		ConnectionPool pool = getConnectionPool();
		Connection con = pool.getConnection();
		try 
		{
			boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
			return result;
		} 
		catch (SQLException e) 
		{
			_logger.sqlerr(e, "save() doInsert: " + doInsert() + "; id: " + id);
			pool.rollbackIgnoreException(con, e);
			return false;
		} 
		finally 
		{
			cleanupPool(pool, con, null);
		}
	}

	public boolean delete(Connection con) throws SQLException 
	{
		try 
		{
			getBlobDb().delete(con, id);
			objectDeleted();
			return true;
		} 
		catch (SQLException e) 
		{
			setLastSQLException(e);
			throw e;
		}
	}

	public boolean delete() 
	{
		_logger.debug("delete() - id: " + id);

		ConnectionPool pool = getConnectionPool();
		Connection con = pool.getConnection();
		try 
		{
			boolean result = delete(con);
			con.commit();
			return result;
		} 
		catch (SQLException e) 
		{
			_logger.sqlerr(e, "delete() - id: " + id);
			pool.rollbackIgnoreException(con, e);
			return false;
		} 
		finally 
		{
			cleanupPool(pool, con, null);
		}
	}

	public static class Manager	
	{		
		public static DocumentProgramming createLike(DocumentProgramming likeDocumentProgramming, TreeMap<EsfUUID,EsfUUID> documentIdMapping, TreeMap<EsfUUID,EsfUUID> pageIdMapping, TreeMap<EsfUUID,EsfUUID> partyIdMapping)
		{
			DocumentProgramming programming = new DocumentProgramming();
			LinkedList<DocumentProgrammingRule> newRules = new LinkedList<DocumentProgrammingRule>();
			int order = 1;
			for( DocumentProgrammingRule dupRule : likeDocumentProgramming.getDuplicateRules() )
			{
				HashSet<EsfUUID> newPageIds = new HashSet<EsfUUID>();
				for( EsfUUID origPageId : dupRule.getOnPageIds() )
				{
					EsfUUID mappedToPageId = pageIdMapping.get(origPageId);
					if (mappedToPageId != null)
						newPageIds.add(mappedToPageId);
					dupRule.setOnPageIds(newPageIds);
				}
				HashSet<EsfUUID> newPartyIds = new HashSet<EsfUUID>();
				for( EsfUUID origPartyId : dupRule.getOnPartyIds() )
				{
					EsfUUID mappedToPartyId = partyIdMapping.get(origPartyId);
					if (mappedToPartyId != null)
						newPartyIds.add(mappedToPartyId);
					dupRule.setOnPartyIds(newPartyIds);
				}
				for( Action action : dupRule.getActions() )
				{
					action.updatePackagePartyIds(partyIdMapping); // not really package parties, but will get translated at runtime
					action.updateDocumentIds(documentIdMapping);
					action.updateDocumentVersionPageIds(pageIdMapping);
				}
				dupRule.setOrder(order++);
				newRules.add(dupRule);
			}
			programming.setRules(newRules);
			return programming;
		}
		
		
		public static DocumentProgramming getById(Connection con, EsfUUID id) throws SQLException 
		{
			try 
			{
				byte[] xmlBlob;
				xmlBlob = Application.getInstance().getBlobDb().select(con, id);
				if (xmlBlob == null) 
				{
					_logger.error("Manager.getById(con) id: " + id + "; found no XML programming blob");
					return null;
				}

				DocumentProgramming programming = new DocumentProgramming(xmlBlob);

				if ( ! id.equals(programming.getId()) ) 
				{
					_logger.error("Manager.getById(con) id: " + id + "; loaded XML with MISMATCHED embedded id: " + programming.getId());
					return null;
				}

				// Now we mark this object as if it were loaded fresh from the database
				programming.setLoadedFromDb();

				return programming;
			} 
			catch (EsfException e) 
			{
				_logger.error("Manager.getById(con) id: " + id + "; could not load XML programming",e);
				return null;
			}
		}

		public static DocumentProgramming getById(EsfUUID id) 
		{
			ConnectionPool pool = getConnectionPool();
			Connection con = pool.getConnection();
			try 
			{
				DocumentProgramming p = getById(con, id);
				con.commit();
				return p;
			} 
			catch (SQLException e) 
			{
				_logger.sqlerr(e, "Manager.getById() id: " + id);
				pool.rollbackIgnoreException(con, e);
				return null;
			} 
			finally 
			{
				cleanupPool(pool, con, null);
			}
		}
	}

}