// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.User;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* Document holds the definition of a document held in a library.  Documents are versioned and each is tagged with test/production versions.
* 
* @author Yozons, Inc.
*/
public class Document
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<Document>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable, java.io.Serializable
{
	private static final long serialVersionUID = 3475765120334614148L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(Document.class);
    
    protected final EsfUUID id;
    protected final EsfUUID libraryId;
    protected EsfName esfname;
    protected EsfName originalEsfName;
    protected String  displayName;
    protected String  fileNameSpec;
    protected String  description;
    protected String  status;
    protected String  comments;
    protected int	  productionVersion;
    protected int	  testVersion;
    
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();
    
    public enum INCLUDE { ONLY_ENABLED, ONLY_DISABLED, BOTH_ENABLED_AND_DISABLED };
    
    public static final EsfName STANDARD_PACKAGE_DOCUMENT_ESFNAME = new EsfName("StandardPackageDisclosures");

    /**
     * This creates a Document object from data retrieved from the DB.
     */
    protected Document(EsfUUID id, EsfUUID libraryId, EsfName esfname, String displayName, String fileNameSpec, String description, 
    		           String status, int productionVersion, int testVersion, String comments)
    {
        this.id = id;
        this.libraryId = libraryId;
        this.esfname = esfname;
        this.originalEsfName = esfname.duplicate();
        this.displayName = displayName;
        this.fileNameSpec = fileNameSpec;
        this.description = description;
        this.status = status;
        this.productionVersion = productionVersion;
        this.testVersion = testVersion;
        this.comments = comments;
    }
    
    protected Document(EsfUUID libraryId)
    {
        this.id = new EsfUUID();
        this.libraryId = libraryId;
        this.esfname = new EsfName("NewEmptyDocument_PleaseRename");
        this.originalEsfName = esfname.duplicate();
        this.displayName = "New Empty Document - Please give a user friendly name";
        this.fileNameSpec = null;
        this.description = null;
        this.status = Literals.STATUS_ENABLED;
        this.productionVersion = 0;
        this.testVersion = 1;
        this.comments = null;
    }

   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getLibraryId()
    {
        return libraryId;
    }
    public Library getLibrary()
    {
    	return Library.Manager.getById(libraryId);
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }
    
    public void makeEnabled()
    {
        status = Literals.STATUS_ENABLED;
        objectChanged();
    }
     
    public void makeDisabled()
    {
        status = Literals.STATUS_DISABLED;
        objectChanged();
    }
    
    public void setStatus(String s)
    {
        if ( Literals.STATUS_ENABLED.equals(s) )
            makeEnabled();
        else
            makeDisabled();
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    public void setEsfName(EsfName v)
    {
    	// Block setting a document name to null or an invalid EsfName
    	if ( v == null || ! v.isValid() )
    		return;
        esfname = v;
        objectChanged();
    }
    public EsfName getOriginalEsfName()
    {
        return originalEsfName;
    }
    public void resetOriginalEsfName()
    {
    	originalEsfName = esfname.duplicate();
    }
    
    public String getDisplayName()
    {
    	return displayName;
    }
    public void setDisplayName(String v)
    {
    	if ( EsfString.isNonBlank(v) )
    	{
        	if ( v.length() > Literals.DISPLAY_NAME_MAX_LENGTH )
        		displayName = v.substring(0,Literals.DISPLAY_NAME_MAX_LENGTH).trim();
            else
            	displayName = v.trim();
    		objectChanged();
    	}
    }
    
    public String getFileNameSpec()
    {
    	return fileNameSpec;
    }
    public boolean hasFileNameSpec()
    {
    	return EsfString.isNonBlank(fileNameSpec);
    }
    public void setFileNameSpec(String v)
    {
    	fileNameSpec = EsfString.ensureTrimmedLength(v, Literals.FILE_NAME_MAX_LENGTH);
    	objectChanged();
    }
    
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String v)
    {
    	description = EsfString.ensureTrimmedLength(v, Literals.DESCRIPTION_MAX_LENGTH);
        objectChanged();
    }
    
    public String getVersionLabel(int checkVersion)
    {
    	if ( checkVersion > productionVersion )
    		return Literals.VERSION_LABEL_TEST;
    	
    	if ( checkVersion == productionVersion )
    		return Literals.VERSION_LABEL_PRODUCTION;
    	
		return Literals.VERSION_LABEL_NOT_CURRENT;
    }
    
    public int getProductionVersion()
    {
    	return productionVersion;
    }
    public boolean hasProductionVersion() 
    {
    	return productionVersion > 0;
    }
    public DocumentVersion getProductionDocumentVersion()
    {
    	return hasProductionVersion() ? DocumentVersion.Manager.getByVersion(id, productionVersion) : null;
    }
    
    public int getTestVersion()
    {
    	return testVersion;
    }
    public boolean hasTestVersion() 
    {
    	return testVersion > productionVersion;
    }
    public void promoteTestVersionToProduction()
    {
    	if ( hasTestVersion() )
    	{
        	productionVersion = testVersion;
        	objectChanged();
    	}
    }
    public void revertProductionVersionBackToTest()
    {
       	if ( hasProductionVersion() && ! hasTestVersion() ) 
    	{
       		--productionVersion;
    		objectChanged();
    	}
    }
    public void bumpTestVersion()
    {
    	if ( ! hasTestVersion() ) 
    	{
    		++testVersion;
    		objectChanged();
    	}
    }
    public void dropTestVersion()
    {
    	if ( hasTestVersion() ) 
    	{
    		--testVersion;
    		objectChanged();
    	}
    }
    public DocumentVersion getTestDocumentVersion() // may be the same as the production version 
    {
    	return DocumentVersion.Manager.getByVersion(id, testVersion);
    }
    
    public String getComments()
    {
    	return comments;
    }
    public void setComments(String v)
    {
    	if ( v == null )
    		comments = v;
    	else if ( v.length() > Literals.COMMENTS_MAX_LENGTH )
    		comments = v.substring(0,Literals.COMMENTS_MAX_LENGTH).trim();
        else
        	comments = v.trim();
        objectChanged();
    }
    

	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof Document )
            return getId().equals(((Document)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(Document o)
    {
    	return getEsfName().compareTo(o.getEsfName());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<Document xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <libraryId>").append(libraryId.toXml()).append("</libraryId>\n");
        buf.append(" <esfname>").append(esfname.toXml()).append("</esfname>\n");
        buf.append(" <displayName>").append(escapeXml(displayName)).append("</displayName>\n");
        buf.append(" <fileNameSpec>").append(escapeXml(fileNameSpec)).append("</fileNameSpec>\n");
       	buf.append(" <description>").append(escapeXml(description)).append("</description>\n");
        buf.append(" <status>").append(escapeXml(status)).append("</status>\n");
        buf.append(" <comments>").append(escapeXml(comments)).append("</comments>\n");
        buf.append(" <productionVersion>").append(productionVersion).append("</productionVersion>\n");
        buf.append(" <testVersion>").append(testVersion).append("</testVersion>\n");
        
        buf.append("</Document>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 271 + id.getEstimatedLengthXml() + libraryId.getEstimatedLengthXml() + esfname.getEstimatedLengthXml();
        if ( displayName != null )
            len += displayName.length();
        if ( fileNameSpec != null )
            len += fileNameSpec.length();
        if ( description != null )
            len += description.length();
        len += status.length();
        if ( comments != null )
            len += comments.length();
        len += 8; // versions
        return len; 
    }

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; esfname: " + esfname + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
            if ( doInsert() )
            {
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_document (id,library_id,esfname,display_name,file_name_spec,description,status,production_version,test_version,comments) VALUES (?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(libraryId);
                stmt.set(esfname);
                stmt.set(displayName);
                stmt.set(fileNameSpec);
                stmt.set(description);
                stmt.set(status);
                stmt.set(productionVersion);
                stmt.set(testVersion);
                stmt.set(comments);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; esfname: " + esfname);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this); // We use replace instead of 'add' because on object create, we added to the ID-only cache
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new document " + getEsfName()); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new document " + getEsfName());
                }
                
                return true;
            }
            
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_document SET esfname=?,display_name=?,file_name_spec=?,description=?,status=?,comments=?,production_version=?,test_version=? WHERE id=?"
                						   );
                stmt.set(esfname);
                stmt.set(displayName);
                stmt.set(fileNameSpec);
                stmt.set(description);
                stmt.set(status);
                stmt.set(comments);
            	stmt.set(productionVersion);
                stmt.set(testVersion);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for document id: " + id + "; esfname: " + esfname);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated document " + getEsfName() + "; status; " + getStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated document " + getEsfName() + "; status; " + getStatus() + "; prodVersion: " + getProductionVersion());
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; esfname: " + esfname + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; esfname: " + esfname + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use in the transaction definition.
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of document that was pending an INSERT id: " + id + "; esfname: " + esfname);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
        	// Delete all our versions...
        	for( DocumentVersion docVer : DocumentVersion.Manager.getAllByDocumentId(con,id) )
        	{
        		if ( ! docVer.delete(con,null,null) )
        		{
        			if ( errors != null )
        				errors.addError("Could not delete document version id: " + docVer.getId());
        			return false;
        		}
        	}
        	
            // Delete the document
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_document WHERE id=?");
            stmt.set(id);
            int num = stmt.executeUpdate();
            if ( num == 0 )
            	_logger.warn("delete(con) - Failed to find esf_library_document database row to delete with id: " + id);
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted document " + getEsfName() + "; status; " + getStatus()); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted document " + getEsfName() + "; status; " + getStatus());
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on id: " + id + "; esfname: " + esfname + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT PKG.path_name,PKGVER.version FROM esf_package PKG,esf_package_version PKGVER WHERE PKGVER.package_document_id=? AND PKGVER.package_id = PKG.id"
	                                   	);
	        stmt.set(id);
	        EsfResultSet rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	do
	        	{
		        	EsfPathName pathName = rs.getEsfPathName();
		        	int version = rs.getInt();
		        	if ( errors != null )
		        		errors.addError("This document is still referenced as the package+disclosure document in package " + pathName + " [" + version + "].");
	        	} while ( rs.next() );
	        	 
	        	return false;
	        }
	        stmt.close(); stmt = null;
	        
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT PKG.path_name,PKGVER.version,PKGPARTY.esfname FROM esf_package PKG,esf_package_version PKGVER,esf_package_version_party_template PKGPARTY WHERE PKGPARTY.override_package_document_id=? AND PKGPARTY.package_version_id=PKGVER.id AND PKGVER.package_id = PKG.id"
	                                   	);
	        stmt.set(id);
	        rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	do
	        	{
		        	EsfPathName pathName = rs.getEsfPathName();
		        	int version = rs.getInt();
		        	EsfName partyName = rs.getEsfName();
		        	if ( errors != null )
		        		errors.addError("This document is still referenced as the override package+disclosure document in package " + pathName + " [" + version + "] in party " + partyName + ".");
	        	} while ( rs.next() );
	        	 
	        	return false;
	        }
	        stmt.close(); stmt = null;
	        
	        stmt = new EsfPreparedStatement( con, 
	                "SELECT PKG.path_name,PKGVER.version FROM esf_package PKG,esf_package_version PKGVER,esf_package_version_document PVD " +
	                "WHERE PVD.library_document_id=? AND PVD.package_version_id=PKGVER.id AND PKGVER.package_id = PKG.id"
	                                   	);
	        stmt.set(id);
	        rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	do
	        	{
		        	EsfPathName pathName = rs.getEsfPathName();
		        	int version = rs.getInt();
		        	if ( errors != null )
		        		errors.addError("This document is still referenced in package " + pathName + " [" + version + "].");
	        	} while ( rs.next() );
	        	 
	        	return false;
	        }
	        stmt.close(); stmt = null;

	        stmt = new EsfPreparedStatement( con, "SELECT COUNT(*) FROM esf_transaction_document WHERE library_document_id = ?" );
	        stmt.set(id);
	        rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	int count = rs.getInt();
	        	if ( count > 0 )
	        	{
		        	errors.addError("This document is referenced by " + count + " transactions.");
		        	return false;
	        	}
	        }
	        stmt.close(); stmt = null;

	        stmt = new EsfPreparedStatement( con, "SELECT COUNT(*) FROM esf_report_template_limit_document WHERE library_document_id = ?" );
	        stmt.set(id);
	        rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	int count = rs.getInt();
	        	if ( count > 0 )
	        	{
		        	errors.addError("This document is referenced by " + count + " report templates.");
		        	return false;
	        	}
	        }

	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   	    private static UUIDCacheReadOptimized<Document> cache = new UUIDCacheReadOptimized<Document>();

   	    public static Document getByName(Library library, EsfName esfname)
   	    {
   	    	if ( library == null || esfname == null || ! esfname.isValid() )
   	    		return null;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT id FROM esf_library_document WHERE library_id=? AND lower(esfname)=?"
   	        									);
   	        	stmt.set(library.getId());
   	        	stmt.set(esfname.toLowerCase());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            con.commit();
	   	            
	   	            return getById(id);
	            }
	            
   	            con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByName() - library: " + library.getPathName() + "; libraryId: " + library.getId() + "; documentName: " + esfname);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        _logger.debug("Manager.getByName() - library: " + library.getPathName() + "; libraryId: " + library.getId() + "; documentName: " + esfname + "; failed to find the document");
   	        return null; 
   	    }

   		static Document getById(Connection con, EsfUUID docId) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT library_id,esfname,display_name,file_name_spec,description,status,production_version,test_version,comments " +
   	        			"FROM esf_library_document WHERE id=?"
   	        									);
   	        	stmt.set(docId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID libraryId = rs.getEsfUUID();
		            EsfName esfname = rs.getEsfName();
		            String displayName = rs.getString();
		            String fileNameSpec = rs.getString();
		            String description = rs.getString();
		            String  stat = rs.getString();
		            int productionVersion = rs.getInt();
		            int testVersion = rs.getInt();
		            String comments = rs.getString();
		            
		            Document doc = new Document(docId,libraryId,esfname,displayName,fileNameSpec,description,stat,productionVersion,testVersion,comments);
		            doc.setLoadedFromDb();
		            cache.add(doc);
		            
	   	            return doc;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + docId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + docId + "; failed to find the document");
   	        return null; 
   		}
   		
   		public static Document getById(EsfUUID docId)
   		{
   	    	Document doc = cache.getById(docId);
   	    	if ( doc != null )
   	    		return doc;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	doc = getById(con,docId);
   	        	if ( doc != null ) 
   	        	{
		            con.commit();
	   	        	return doc;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   		public static Document getStandardPackageDocument()
   		{
   			return getByName(Library.Manager.getTemplate(),STANDARD_PACKAGE_DOCUMENT_ESFNAME);
   		}
   		
   	    public static Document createNew(EsfUUID libraryId)
   	    {
   	    	if ( libraryId == null )
   	    		return null;
   	    	Document newDoc = new Document(libraryId);
   	    	cache.add(newDoc);
   	    	return newDoc;
   	    }

   	    public static Document createLike(EsfUUID libraryId, Document likeDoc, EsfName newName)
   	    {
   	    	if ( libraryId == null || likeDoc == null || newName == null || ! newName.isValid() )
   	    		return null;
   	    	
   	    	Document newDoc = createNew(libraryId);
   	    	newDoc.setEsfName(newName);
   	    	newDoc.setDisplayName( likeDoc.getDisplayName() );
   	    	newDoc.setFileNameSpec( likeDoc.getFileNameSpec() );
   	    	newDoc.setDescription( likeDoc.getDescription() );
   	    	newDoc.setComments( likeDoc.getComments() );
   	    	return newDoc;
   	    }
   	    
   	    public static Document createFromJDOM(EsfUUID libraryId, Element e)
   	    {
   	    	if ( libraryId == null || e == null )
   	    		return null;
   	    	
   	    	Document newDoc = new Document(
   	    			new EsfUUID(), 
   	    			libraryId, 
   	    			EsfName.createFromToXml(e.getChildText("esfname", e.getNamespace())),
   	    			e.getChildText("displayName", e.getNamespace()), 
   	    			e.getChildText("fileNameSpec", e.getNamespace()), 
   	    			e.getChildText("description", e.getNamespace()), 
   	    			e.getChildText("status", e.getNamespace()), 
   	    			0, 1,
   	    			e.getChildText("comments", e.getNamespace()) 
   	    										);
   	    	newDoc.setNewObject();
   	    	cache.add(newDoc);
   	    	return newDoc;
   	    }
   	    
   	    /**
   	     * Returns the total number of document objects in the cache
   	     * @return the total number of documents in the cache
   	     */
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all documents that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}

   	} // Manager
   	
}