// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.util.XmlUtil;

/**
* This represents a FieldTemplate that is defined as an overlay field for an ImageVersion.
* Prior to when Yozons was founded in 2000, we had conceived of fields overlaying an image, but browser support then was weak with
* respect to layers and other schemes (like we could build it on the IE browser using DHTML). So we have finally returned to this idea
* now that CSS positioning works well across all browsers.
* 
* @author Yozons, Inc.
*/
public class ImageVersionOverlayField
    extends com.esignforms.open.db.DatabaseObject
    implements java.io.Serializable, Comparable<ImageVersionOverlayField>
{
	private static final long serialVersionUID = -5861492931267519460L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ImageVersionOverlayField.class);
    
	public static final String DISPLAY_MODE_FIELD = "F"; // like ${field:xxx}
	public static final String DISPLAY_MODE_FIELD_LABEL = "L"; // like ${fieldlabel:xxx} or ${xxx}
	public static final String DISPLAY_MODE_OUT = "O"; // like ${out:xxx}
	
    protected final EsfUUID imageVersionId; // The image version that the field will overlay
    protected EsfUUID fieldTemplateId; // The field template that will overlay the related image version
    protected short positionLeft; // CSS positions
    protected short positionTop;
    protected short positionWidth;
    protected short positionHeight;
    protected String displayMode; // F=Field; L=Field+Label; O=Output only
    protected String backgroundColor; // Used to override the background color on a field
    
    // this is only set for overlays to assist in converting a fieldTemplateId to the field's name
    private transient DocumentVersion _documentVersion; 
    
    /**
     * This creates a ImageVersionOverlayField object from data retrieved from the DB.
     */
    protected ImageVersionOverlayField(EsfUUID imageVersionId, EsfUUID fieldTemplateId, 
    									short positionLeft, short positionTop, short positionWidth, short positionHeight,
    									String displayMode, String backgroundColor)
    {
        this.imageVersionId = imageVersionId;
    	this.fieldTemplateId = fieldTemplateId;
    	this.positionLeft = positionLeft;
        this.positionTop = positionTop;
        this.positionWidth = positionWidth;
        this.positionHeight = positionHeight;
        setDisplayMode(displayMode);
        setBackgroundColor(backgroundColor);
    }
    
    public ImageVersionOverlayField(EsfUUID imageVersionId, EsfUUID fieldTemplateId)
    {
        this.imageVersionId = imageVersionId;
    	this.fieldTemplateId = fieldTemplateId;
    	this.positionLeft = 0;
        this.positionTop = 0;
        this.positionWidth = 50;
        this.positionHeight = 20;
        setDisplayModeField();
        setBackgroundColor(null);
    }

    public ImageVersionOverlayField duplicate()
    {
    	return duplicateUsingImageVersionId(imageVersionId);
    }
    public ImageVersionOverlayField duplicateUsingImageVersionId(EsfUUID imageVersionId)
    {
    	ImageVersionOverlayField dup = new ImageVersionOverlayField(imageVersionId,fieldTemplateId,positionLeft,positionTop,positionWidth,positionHeight,displayMode,backgroundColor);
    	dup._documentVersion = _documentVersion;
    	dup.setDatabaseObjectLike(this);
    	return dup;
    }
   
    public final EsfUUID getImageVersionId()
    {
        return imageVersionId;
    }
     
    public final EsfUUID getFieldTemplateId()
    {
        return fieldTemplateId;
    }
    public void _setDocumentVersionToResolveFieldNames(DocumentVersion docVer)
    {
    	_documentVersion = docVer;
    }
    public EsfName getFieldTemplateName()
    {
    	if ( _documentVersion != null )
    	{
    		FieldTemplate ft = _documentVersion.getFieldTemplate(fieldTemplateId);
    		if ( ft != null )
    			return ft.getEsfName();
    	}
    	return null;
    }

    public short getPositionLeft()
    {
    	return positionLeft;
    }
    public void setPositionLeft(short v)
    {
    	positionLeft = v;
    }
    
    public short getPositionTop()
    {
    	return positionTop;
    }
    public void setPositionTop(short v)
    {
    	positionTop = v;
    }
    
    public short getPositionWidth()
    {
    	return positionWidth;
    }
    public void setPositionWidth(short v)
    {
    	positionWidth = v;
    }

    public short getPositionHeight()
    {
    	return positionHeight;
    }
    public void setPositionHeight(short v)
    {
    	positionHeight = v;
    }
    
    public String getDisplayMode()
    {
    	return displayMode;
    }
    public boolean isDisplayModeField()
    {
    	return DISPLAY_MODE_FIELD.equals(displayMode);
    }
    public boolean isDisplayModeFieldLabel()
    {
    	return DISPLAY_MODE_FIELD_LABEL.equals(displayMode);
    }
    public boolean isDisplayModeOut()
    {
    	return DISPLAY_MODE_OUT.equals(displayMode);
    }
    public void setDisplayMode(String v)
    {
    	if ( DISPLAY_MODE_OUT.equalsIgnoreCase(v) )
    		displayMode = DISPLAY_MODE_OUT;
    	else if ( DISPLAY_MODE_FIELD_LABEL.equalsIgnoreCase(v) )
    		displayMode = DISPLAY_MODE_FIELD_LABEL;
    	else
    		displayMode = DISPLAY_MODE_FIELD;
    	objectChanged();
    }
    public void setDisplayModeField()
    {
    	setDisplayMode(DISPLAY_MODE_FIELD);
    }
    public void setDisplayModeFieldLabel()
    {
    	setDisplayMode(DISPLAY_MODE_FIELD_LABEL);
    }
    public void setDisplayModeOut()
    {
    	setDisplayMode(DISPLAY_MODE_OUT);
    }
    
    public String getBackgroundColor()
    {
    	return backgroundColor == null ? "" : backgroundColor;
    }
    public boolean hasBackgroundColor()
    {
    	return EsfString.isNonBlank(backgroundColor);
    }
    public void setBackgroundColor(String v)
    {
    	backgroundColor = EsfString.isBlank(v) ? null : v;
    	objectChanged();
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof ImageVersionOverlayField )
        {
        	ImageVersionOverlayField other = (ImageVersionOverlayField)o;
            return getImageVersionId().equals(other.getImageVersionId()) && getFieldTemplateId().equals(other.getFieldTemplateId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getImageVersionId().hashCode() + getFieldTemplateId().hashCode();
    }
    
    
	@Override
	public int compareTo(ImageVersionOverlayField o) {
		int diff = positionTop - o.positionTop;
		return diff == 0 ? positionLeft - o.positionLeft : diff;
	}
   	
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("  <ImageVersionOverlayField>\n");
        
        buf.append("    <imageVersionId>").append(imageVersionId.toXml()).append("</imageVersionId>\n");
        buf.append("    <fieldTemplateId>").append(fieldTemplateId.toXml()).append("</fieldTemplateId>\n");
        buf.append("    <positionLeft>").append(positionLeft).append("</positionLeft>\n");
        buf.append("    <positionTop>").append(positionTop).append("</positionTop>\n");
        buf.append("    <positionWidth>").append(positionWidth).append("</positionWidth>\n");
        buf.append("    <positionHeight>").append(positionHeight).append("</positionHeight>\n");
        buf.append("    <displayMode>").append(XmlUtil.toEscapedXml(displayMode)).append("</displayMode>\n");
        if ( backgroundColor != null )
        	buf.append("    <backgroundColor>").append(XmlUtil.toEscapedXml(backgroundColor)).append("</backgroundColor>\n");

        buf.append("  </ImageVersionOverlayField>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 59 + 254 + imageVersionId.getEstimatedLengthXml() + fieldTemplateId.getEstimatedLengthXml() + 16 + 1;
        if ( backgroundColor != null )
        	len += 40 + backgroundColor.length();
        return len; 
    }


    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
    // Used for configuration when all overlay fields are deleted and thus whatever we save should be inserted even if 
    // it previously existed.
	public synchronized boolean saveByInsert(final Connection con)
        throws SQLException
    {
		doInsert = true;
		return save(con);
    }
	
	public synchronized boolean save(final Connection con)
        throws SQLException
    {
    	_logger.debug("save(con) on imageVersionId: " + imageVersionId + "; fieldTemplateId: " + fieldTemplateId + "; insert: " + doInsert());
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( doInsert() )
            {                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_image_version_overlay_field (library_image_version_id,field_template_id,position_left,position_top,position_width,position_height,display_mode,background_color) VALUES (?,?,?,?,?,?,?,?)");
                stmt.set(imageVersionId);
                stmt.set(fieldTemplateId);
                stmt.set(positionLeft);
                stmt.set(positionTop);
                stmt.set(positionWidth);
                stmt.set(positionHeight);
                stmt.set(displayMode);
                stmt.set(backgroundColor);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for imageVersionId: " + imageVersionId + "; fieldTemplateId: " + fieldTemplateId);
                    return false;
                }

                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                return true;
            }
            
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_image_version_overlay_field SET position_left=?,position_top=?,position_width=?,position_height=?,display_mode=?,background_color=? WHERE library_image_version_id=? AND field_template_id=?"
                						   );
                stmt.set(positionLeft);
                stmt.set(positionTop);
                stmt.set(positionWidth);
                stmt.set(positionHeight);
                stmt.set(displayMode);
                stmt.set(backgroundColor);
                stmt.set(imageVersionId);
                stmt.set(fieldTemplateId);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for imageVersionId: " + imageVersionId + "; fieldTemplateId: " + fieldTemplateId); 
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on imageVersionId: " + imageVersionId + "; fieldTemplateId: " + fieldTemplateId + "; insert: " + doInsert());
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }

    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    
    
       
   	public static class Manager
   	{
   		public static List<ImageVersionOverlayField> getAllByImageVersionId(Connection con, EsfUUID imageVersionId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<ImageVersionOverlayField> list = new LinkedList<ImageVersionOverlayField>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT field_template_id, position_left, position_top, position_width, position_height, display_mode, background_color " +
   	        			               				  "FROM esf_library_image_version_overlay_field WHERE library_image_version_id=?" );
   	        	stmt.set(imageVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID fieldTemplateId = rs.getEsfUUID();
	            	short positionLeft = rs.getShort();
	            	short positionTop = rs.getShort();
	            	short positionWidth = rs.getShort();
	            	short positionHeight = rs.getShort();
	            	String displayMode = rs.getString();
	            	String backgroundColor = rs.getString();
	            	
	            	ImageVersionOverlayField overlay = new ImageVersionOverlayField(imageVersionId,fieldTemplateId,positionLeft,positionTop,positionWidth,positionHeight,displayMode,backgroundColor);
	            	overlay.setLoadedFromDb();
	            	list.add(overlay);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   		public static List<ImageVersionOverlayField> getAllByImageVersionId(EsfUUID imageVersionId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<ImageVersionOverlayField> list = getAllByImageVersionId(con,imageVersionId);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   		

   		public static int deleteAllByImageVersionId(Connection con, EsfUUID imageVersionId)
			throws SQLException
		{
	        EsfPreparedStatement stmt = null;
	        int numDeleted = 0;
	        
	        try
	        {
	        	stmt = new EsfPreparedStatement( con, "DELETE FROM esf_library_image_version_overlay_field WHERE library_image_version_id=?" );
	        	stmt.set(imageVersionId);
	        	numDeleted = stmt.executeUpdate();
	        }
	        finally
	        {
	            cleanupStatement(stmt);
	        }

	        return numDeleted; 
		}
		
   	} // Manager

}