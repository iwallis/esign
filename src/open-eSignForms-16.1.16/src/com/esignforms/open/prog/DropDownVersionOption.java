// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.User;
import com.esignforms.open.util.XmlUtil;

/**
* DropDownVersionOption holds the definition of an option in a drop down version held in a library/document.  
* 
* @author Yozons, Inc.
*/
public class DropDownVersionOption
	extends com.esignforms.open.db.DatabaseObject
	implements java.lang.Comparable<DropDownVersionOption>
{
	private static final long serialVersionUID = -1724817072281687924L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DropDownVersionOption.class);
    
    protected EsfUUID id;
    protected EsfUUID dropDownVersionId;
    protected short listOrder;
    protected String option;
    protected String value;
    
    /**
     * This creates a DropDownVersionOption object from data retrieved from the DB.
     */
    protected DropDownVersionOption(EsfUUID id, EsfUUID dropDownVersionId, short listOrder, String option, String value)
    {
    	this.id = id;
        this.dropDownVersionId = dropDownVersionId;
        this.listOrder = listOrder;
        this.option = option;
        this.value = value;
    }
    
    protected DropDownVersionOption(DropDownVersion dropDownVersion, short listOrder)
    {
    	this.id = new EsfUUID();
        this.dropDownVersionId = dropDownVersion.getId();
        this.listOrder = listOrder;
        setOption("--SELECT--");
        setValue("");
    }

    protected DropDownVersionOption(DropDownVersion dropDownVersion, DropDownVersionOption likeDropDownVersionOption)
    {
    	this.id = new EsfUUID();
        this.dropDownVersionId = dropDownVersion.getId();
        this.listOrder = likeDropDownVersionOption.getListOrder();
        this.option = likeDropDownVersionOption.getOption();
        this.value = likeDropDownVersionOption.getValue();
    }

    public DropDownVersionOption duplicate()
    {
    	DropDownVersionOption o = new DropDownVersionOption(id, dropDownVersionId, listOrder, option, value);
    	o.setDatabaseObjectLike(this);
    	return o;
    }

    public EsfUUID getId()
    {
        return id;
    }

    public EsfUUID getDropDownVersionId()
    {
        return dropDownVersionId;
    }
    
    public DropDownVersion getDropDownVersion()
    {
    	return DropDownVersion.Manager.getById(dropDownVersionId);
    }
    
    public short getListOrder()
    {
        return listOrder;
    }
    public void setListOrder(short v)
    {
    	if ( v != listOrder )
    	{
    		listOrder = v;
    		objectChanged();
    	}
    }
    
    public String getOption()
    {
        return option;
    }
    public void setOption(String v)
    {
    	if ( EsfString.isBlank(v) )
    		option = "";
    	else if ( v.length() > Literals.SELECTION_OPTION_MAX_LENGTH )
    		option = v.substring(0,Literals.SELECTION_OPTION_MAX_LENGTH).trim();
        else
        	option = v.trim();
        objectChanged();
    }

    public String getValue()
    {
        return value;
    }
    public void setValue(String v)
    {
    	if ( EsfString.isBlank(v) )
    		value = "";
    	else if ( v.length() > Literals.SELECTION_VALUE_MAX_LENGTH )
    		value = v.substring(0,Literals.SELECTION_VALUE_MAX_LENGTH).trim();
        else
        	value = v.trim();
        objectChanged();
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof DropDownVersionOption )
        {
        	DropDownVersionOption otherOption = (DropDownVersionOption)o;
            return getId().equals(otherOption.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(DropDownVersionOption d)
    {
    	if ( getDropDownVersionId().equals(d.getDropDownVersionId()) )
    		return getListOrder() - d.getListOrder();
    	return getDropDownVersionId().compareTo(d.getDropDownVersionId());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<DropDownVersionOption xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <dropDownVersionId>").append(dropDownVersionId.toXml()).append("</dropDownVersionId>\n");
        buf.append(" <listOrder>").append(listOrder).append("</listOrder>\n");
        buf.append(" <option>").append(escapeXml(option)).append("</option>\n");
        buf.append(" <value>").append(escapeXml(value)).append("</value>\n");

        buf.append("</DropDownVersionOption>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 175 + id.getEstimatedLengthXml() + dropDownVersionId.getEstimatedLengthXml() + option.length() + value.length();

        return len; 
    }

    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
    // This is our special save routine when we are rewriting all of the options after we delete them, so this
    // will always use an INSERT to save, never an UPDATE.
	public synchronized boolean saveOnRewrite(final Connection con, final User user)
	    throws SQLException
	{
		doInsert = true;
		return save(con,user);
	}
	
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; dropDownVersionId: " + dropDownVersionId + "; listOrder: " + listOrder + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
            if ( doInsert() )
            {
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_dropdown_version_option (id,library_dropdown_version_id,list_order,selection_option,selection_value) VALUES (?,?,?,?,?)");
                stmt.set(id);
                stmt.set(dropDownVersionId);
                stmt.set(listOrder);
                stmt.set(option);
                stmt.set(value);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; dropDownVersionId: " + dropDownVersionId + "; listOrder: " + listOrder);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                
                return true;
            }
            
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_dropdown_version_option SET selection_option=?, selection_value=? WHERE id=?"
                						   	   );
                stmt.set(option);
                stmt.set(value);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for drop down version option id: " + id + "; dropDownVersionId: " + dropDownVersionId + "; listOrder: " + listOrder);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; dropDownVersionId: " + dropDownVersionId + "; listOrder: " + listOrder + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; dropDownVersionId: " + dropDownVersionId + "; listOrder: " + listOrder + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of drop down version option that was pending an INSERT id: " + id + "; dropDownVersionId: " + dropDownVersionId + "; listOrder: " + listOrder);
            objectDeleted();
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
            // Delete the document
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_dropdown_version_option WHERE id=?");
            stmt.set(id);
            int num = stmt.executeUpdate();
            if ( num == 0 )
            	_logger.warn("delete(con) - Failed to find esf_library_dropdown_version_option database row to delete with id: " + id + "; dropDownVersionId: " + dropDownVersionId + "; listOrder: " + listOrder);
            stmt.close();
            
            objectDeleted();
            
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on id: " + id + "; dropDownVersionId: " + dropDownVersionId + "; listOrder: " + listOrder + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   		public static List<DropDownVersionOption> getAllOptionsByDropDownVersion(EsfUUID dropDownVersionId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<DropDownVersionOption> optionList = new LinkedList<DropDownVersionOption>();
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT id,list_order,selection_option,selection_value " +
   	        			"FROM esf_library_dropdown_version_option WHERE library_dropdown_version_id = ? ORDER BY list_order"
   	        									);
   	        	stmt.set(dropDownVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            short listOrder = rs.getShort();
		            String option = rs.getString();
		            String value = rs.getString();
		            
		            DropDownVersionOption dropDownOption = new DropDownVersionOption(id,dropDownVersionId,listOrder,option,value);
		            dropDownOption.setLoadedFromDb();
		            optionList.add(dropDownOption);
	            }
	            
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAllOptionsByDropDownVersion() - dropDownVersionId: " + dropDownVersionId);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        _logger.debug("Manager.getAllOptionsByDropDownVersion() - drop down version dropDownVersionId: " + dropDownVersionId + "; numOptions: " + optionList.size());
   	        return optionList; 
   		}

   		
   		public static DropDownVersionOption getById(EsfUUID id)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
  	        			"SELECT library_dropdown_version_id,list_order,selection_option,selection_value " +
   	        			"FROM esf_library_dropdown_version_option WHERE id = ?"
   	        									);
   	        	stmt.set(id);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID dropDownVersionId = rs.getEsfUUID();
		            short listOrder = rs.getShort();
		            String option = rs.getString();
		            String value = rs.getString();
		            
		            DropDownVersionOption dropDownOption = new DropDownVersionOption(id,dropDownVersionId,listOrder,option,value);
		            dropDownOption.setLoadedFromDb();
		            con.commit();
		            return dropDownOption;
	            }
	            
	            con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - dropDownVersionOptionId: " + id);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        _logger.debug("Manager.getById() - drop down version option dropDownVersionOptionId: " + id);
   	        return null; 
   		}

   		
   		public static int deleteAllOptions(final Connection con, EsfUUID dropDownVersionId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	   	        stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_dropdown_version_option WHERE library_dropdown_version_id=?");
   	            stmt.set(dropDownVersionId);
   	            return stmt.executeUpdate();
   	        }
   	        finally
   	        {
   	        	cleanupStatement(stmt);
   	        }
   		}
   		
   		public static DropDownVersionOption createNew(DropDownVersion dropDownVersion, short listOrder)
   		{
   	    	if ( dropDownVersion == null || listOrder < 1 )
   	    		return null;
   	    	return new DropDownVersionOption(dropDownVersion,listOrder);
   			
   		}
   		
   		public static DropDownVersionOption createLike(DropDownVersion dropDownVersion, DropDownVersionOption likeOption)
   		{
   	    	if ( dropDownVersion == null || likeOption == null )
   	    		return null;
   	    	return new DropDownVersionOption(dropDownVersion,likeOption);
   		}
   		
   	    public static DropDownVersionOption createFromJDOM(EsfUUID dropDownVersionId, Element e)
   	    {
   	    	if ( dropDownVersionId == null || dropDownVersionId.isNull() || e == null )
   	    		return null;
   	    	
   	    	DropDownVersionOption newDropDownVersionOption = new DropDownVersionOption(
   	    			new EsfUUID(), 
   	    			dropDownVersionId, 
   	    			Application.getInstance().stringToShort(e.getChildText("listOrder", e.getNamespace()), (short)0),
   	    			e.getChildText("option", e.getNamespace()),
   	    			e.getChildText("value", e.getNamespace())
   	    			);
   	    	newDropDownVersionOption.setNewObject();
   	    	return newDropDownVersionOption;
   	    }

   	} // Manager
    
}