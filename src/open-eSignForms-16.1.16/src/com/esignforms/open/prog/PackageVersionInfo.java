// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;

/**
* PackageVersionInfo is a data-only object that holds information about a package version.
* It's not a mutable object (you can't save/delete it), just for lists of package versions and such.
* 
* @author Yozons, Inc.
*/
public class PackageVersionInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<PackageVersionInfo>, java.io.Serializable
{
	private static final long serialVersionUID = -5125624816075639742L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackageVersionInfo.class);

    protected EsfUUID id;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected int numDocuments;
    protected int numParties;
    
    /**
     * This creates a new PackageVersionInfo using the source object.
     * @param packageVersion the package version to use as the source
     */
    public PackageVersionInfo(PackageVersion packageVersion)
    {
        this.id = packageVersion.getId();
        this.version = packageVersion.getVersion();
        this.createdTimestamp = packageVersion.getCreatedTimestamp();
        this.createdByUserId = packageVersion.getCreatedByUserId();
        this.lastUpdatedTimestamp = packageVersion.getLastUpdatedTimestamp();
        this.lastUpdatedByUserId = packageVersion.getLastUpdatedByUserId();
        this.numDocuments = packageVersion.getNumDocuments();
        this.numParties = packageVersion.getNumPackageParties();
    }
    
    /**
     * This creates a PackageVersionInfo object from data retrieved from the DB.
     */
    protected PackageVersionInfo(
    		EsfUUID id, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId, 
    		int numDocuments, int numParties)
    {
        this.id = id;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.numDocuments = numDocuments;
        this.numParties = numParties;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public int getVersion()
    {
    	return version;
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }

    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }

    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }

    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }

    public int getNumDocuments()
    {
    	return numDocuments;
    }

    public int getNumParties()
    {
    	return numParties;
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof PackageVersionInfo )
            return getId().equals(((PackageVersionInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(PackageVersionInfo o)
    {
    	return getVersion() - o.getVersion();
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all PackageVersionInfo objects.
   	     * @return the Collection of PackageVersionInfo found with most recent versions first.
   	     */
   	    public static Collection<PackageVersionInfo> getAll(Package pkg)
   	    {
   	    	LinkedList<PackageVersionInfo> list = new LinkedList<PackageVersionInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id, version, created_timestamp, created_by_user_id, last_updated_timestamp, last_updated_by_user_id, num_documents, num_parties " +
   	    	            "FROM esf_package_version " + 
   	    	            "WHERE package_id = ? " +
   	    	            "ORDER BY version DESC"
   	            						   );
   	        	stmt.set(pkg.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
		            EsfUUID id = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            int numDocuments = rs.getInt();
		            int numParties = rs.getInt();
		            
		            PackageVersionInfo pkgVerInfo = new PackageVersionInfo(id, version,
		            							createdTimestamp, createdByUserId, lastUpdatedTimestamp, lastUpdatedByUserId, numDocuments, numParties
		            		 												);
		            pkgVerInfo.setLoadedFromDb();
		            list.add(pkgVerInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - Package: " + pkg.getPathName() + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - Package: " + pkg.getPathName() + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static PackageVersionInfo createNew(Package pkg, User createdBy)
   	    {
   			PackageVersion newPackageVersion = PackageVersion.Manager.createTest(pkg, createdBy);
   			PackageVersionInfo newDocVerInfo = new PackageVersionInfo(newPackageVersion);
   			return newDocVerInfo;
   	    }

   	    public static PackageVersionInfo createFromSource(PackageVersion packageVersion)
   	    {
   	    	if ( packageVersion == null )
   	    		return null;
   	    	
   			PackageVersionInfo docVerInfo = new PackageVersionInfo(packageVersion);
   			docVerInfo.setLoadedFromDb();

   			return docVerInfo;
   	    }
   	    
   	} // Manager
   	
}