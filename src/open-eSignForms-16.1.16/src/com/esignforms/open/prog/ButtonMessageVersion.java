// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;

import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.Record;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.user.User;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* ButtonMessageVersion holds the definition of a specific version of a button message setup held in a library.
* A button message contains a potential definition for all buttons and related status messages to show the user.
* 
* @author Yozons, Inc.
*/
public class ButtonMessageVersion
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<ButtonMessageVersion>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable
{
	private static final long serialVersionUID = 4264855085283448947L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ButtonMessageVersion.class);
    
    private static final EsfName PACKAGE_FOOTER_HTML_ESFNAME = new EsfName("PackageFooterHtml");
    private static final EsfName DOCUMENT_FOOTER_HTML_ESFNAME = new EsfName("DocumentFooterHtml");
 
    private static final EsfName PACKAGE_BUTTON_CONTINUE_TODO_ESFNAME = new EsfName("PackageButtonContinueToDo");
    private static final EsfName PACKAGE_BUTTON_CONTINUE_TODO_TITLE_ESFNAME = new EsfName("PackageButtonContinueToDoTitle");
    private static final EsfName PACKAGE_BUTTON_CONTINUE_DONE_ESFNAME = new EsfName("PackageButtonContinueDone");
    private static final EsfName PACKAGE_BUTTON_CONTINUE_DONE_TITLE_ESFNAME = new EsfName("PackageButtonContinueDoneTitle");
    public  static final EsfName PACKAGE_BUTTON_EDIT_DOCUMENT_ESFNAME = new EsfName("PackageButtonEditDocument"); // public since added after the original and DbSetup needs to know these values
    public  static final EsfName PACKAGE_BUTTON_EDIT_DOCUMENT_TITLE_ESFNAME = new EsfName("PackageButtonEditDocumentTitle"); // public since added after the original and DbSetup needs to know these values
    public  static final EsfName PACKAGE_BUTTON_VIEW_COMPLETED_DOCUMENT_ESFNAME = new EsfName("PackageButtonViewCompletedDocument"); // public since added after the original and DbSetup needs to know these values
    public  static final EsfName PACKAGE_BUTTON_VIEW_COMPLETED_DOCUMENT_TITLE_ESFNAME = new EsfName("PackageButtonViewCompletedDocumentTitle"); // public since added after the original and DbSetup needs to know these values
    private static final EsfName PACKAGE_BUTTON_DELETE_ESFNAME = new EsfName("PackageButtonDelete");
    private static final EsfName PACKAGE_BUTTON_DELETE_TITLE_ESFNAME = new EsfName("PackageButtonDeleteTitle");
    private static final EsfName PACKAGE_BUTTON_NOT_COMPLETING_ESFNAME = new EsfName("PackageButtonNotCompleting");
    private static final EsfName PACKAGE_BUTTON_NOT_COMPLETING_TITLE_ESFNAME = new EsfName("PackageButtonNotCompletingTitle");
    private static final EsfName PACKAGE_BUTTON_DOWNLOAD_MY_DOCS_AS_PDF_ESFNAME = new EsfName("PackageButtonDownloadMyDocsAsPdf");
    private static final EsfName PACKAGE_BUTTON_DOWNLOAD_MY_DOCS_AS_PDF_TITLE_ESFNAME = new EsfName("PackageButtonDownloadMyDocsAsPdfTitle");
    
    private static final EsfName DOCUMENT_BUTTON_GO_TO_PACKAGE_ESFNAME = new EsfName("DocumentButtonGoToPackage");
    private static final EsfName DOCUMENT_BUTTON_GO_TO_PACKAGE_TITLE_ESFNAME = new EsfName("DocumentButtonGoToPackageTitle");
    private static final EsfName DOCUMENT_BUTTON_GO_TO_PACKAGE_MESSAGE_ESFNAME = new EsfName("DocumentButtonGoToPackageMessage");
    
    private static final EsfName DOCUMENT_EDIT_BUTTON_CONTINUE_NEXT_PAGE_ESFNAME = new EsfName("DocumentEditButtonContinueNextPage");
    private static final EsfName DOCUMENT_EDIT_BUTTON_CONTINUE_NEXT_PAGE_TITLE_ESFNAME = new EsfName("DocumentEditButtonContinueNextPageTitle");
    private static final EsfName DOCUMENT_EDIT_BUTTON_CONTINUE_TO_REVIEW_ESFNAME = new EsfName("DocumentEditButtonContinueToReview");
    private static final EsfName DOCUMENT_EDIT_BUTTON_CONTINUE_TO_REVIEW_TITLE_ESFNAME = new EsfName("DocumentEditButtonContinueToReviewTitle");
    private static final EsfName DOCUMENT_EDIT_BUTTON_CONTINUE_TO_REVIEW_MESSAGE_ESFNAME = new EsfName("DocumentEditButtonContinueToReviewMessage");
    private static final EsfName DOCUMENT_EDIT_BUTTON_PREVIOUS_PAGE_ESFNAME = new EsfName("DocumentEditButtonPreviousPage");
    private static final EsfName DOCUMENT_EDIT_BUTTON_PREVIOUS_PAGE_TITLE_ESFNAME = new EsfName("DocumentEditButtonPreviousPageTitle");
    public  static final EsfName DOCUMENT_EDIT_BUTTON_SAVE_ESFNAME = new EsfName("DocumentEditButtonSave"); // public since added after the original and DbSetup needs to know these values
    public  static final EsfName DOCUMENT_EDIT_BUTTON_SAVE_TITLE_ESFNAME = new EsfName("DocumentEditButtonSaveTitle"); // public since added after the original and DbSetup needs to know these values
    
    private static final EsfName DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_CONTINUE_NEXT_PAGE_ESFNAME = new EsfName("DocumentReviewViewOnlyButtonContinueNextPage");
    private static final EsfName DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_CONTINUE_NEXT_PAGE_TITLE_ESFNAME = new EsfName("DocumentReviewViewOnlyButtonContinueNextPageTitle");
    private static final EsfName DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_CONTINUE_NEXT_PAGE_MESSAGE_ESFNAME = new EsfName("DocumentReviewViewOnlyButtonContinueNextPageMsg"); // abbreviated since 50 char limit in name
    private static final EsfName DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_COMPLETE_ESFNAME = new EsfName("DocumentReviewViewOnlyButtonComplete");
    private static final EsfName DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_COMPLETE_TITLE_ESFNAME = new EsfName("DocumentReviewViewOnlyButtonCompleteTitle");
    private static final EsfName DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_COMPLETE_MESSAGE_ESFNAME = new EsfName("DocumentReviewViewOnlyButtonCompleteMessage");
    private static final EsfName DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_PREVIOUS_PAGE_ESFNAME = new EsfName("DocumentReviewViewOnlyButtonPreviousPage");
    private static final EsfName DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_PREVIOUS_PAGE_TITLE_ESFNAME = new EsfName("DocumentReviewViewOnlyButtonPreviousPageTitle");
    
    private static final EsfName DOCUMENT_REVIEW_BUTTON_COMPLETE_SIGNER_ESFNAME = new EsfName("DocumentReviewButtonCompleteSigner");
    private static final EsfName DOCUMENT_REVIEW_BUTTON_COMPLETE_SIGNER_TITLE_ESFNAME = new EsfName("DocumentReviewButtonCompleteSignerTitle");
    private static final EsfName DOCUMENT_REVIEW_BUTTON_COMPLETE_SIGNER_MESSAGE_ESFNAME = new EsfName("DocumentReviewButtonCompleteSignerMessage");
    private static final EsfName DOCUMENT_REVIEW_BUTTON_COMPLETE_NOT_SIGNER_ESFNAME = new EsfName("DocumentReviewButtonCompleteNotSigner");
    private static final EsfName DOCUMENT_REVIEW_BUTTON_COMPLETE_NOT_SIGNER_TITLE_ESFNAME = new EsfName("DocumentReviewButtonCompleteNotSignerTitle");
    private static final EsfName DOCUMENT_REVIEW_BUTTON_COMPLETE_NOT_SIGNER_MESSAGE_ESFNAME = new EsfName("DocumentReviewButtonCompleteNotSignerMessage");
    private static final EsfName DOCUMENT_REVIEW_BUTTON_RETURN_TO_EDIT_ESFNAME = new EsfName("DocumentReviewButtonReturnToEdit");
    private static final EsfName DOCUMENT_REVIEW_BUTTON_RETURN_TO_EDIT_TITLE_ESFNAME = new EsfName("DocumentReviewButtonReturnToEditTitle");
    private static final EsfName DOCUMENT_REVIEW_BUTTON_RETURN_TO_EDIT_MESSAGE_ESFNAME = new EsfName("DocumentReviewButtonReturnToEditMessage");

    private static final EsfName DOCUMENT_VIEW_ONLY_BUTTON_NEXT_PAGE_ESFNAME = new EsfName("DocumentViewOnlyButtonNextPage");
    private static final EsfName DOCUMENT_VIEW_ONLY_BUTTON_NEXT_PAGE_TITLE_ESFNAME = new EsfName("DocumentViewOnlyButtonNextPageTitle");
    private static final EsfName DOCUMENT_VIEW_ONLY_BUTTON_PREVIOUS_PAGE_ESFNAME = new EsfName("DocumentViewOnlyButtonPreviousPage");
    private static final EsfName DOCUMENT_VIEW_ONLY_BUTTON_PREVIOUS_PAGE_TITLE_ESFNAME = new EsfName("DocumentViewOnlyButtonPreviousPageTitle");
    private static final EsfName DOCUMENT_VIEW_ONLY_BUTTON_NEXT_DOCUMENT_ESFNAME = new EsfName("DocumentViewOnlyButtonNextDocument");
    private static final EsfName DOCUMENT_VIEW_ONLY_BUTTON_NEXT_DOCUMENT_TITLE_ESFNAME = new EsfName("DocumentViewOnlyButtonNextDocumentTitle");
    private static final EsfName DOCUMENT_VIEW_ONLY_BUTTON_PREVIOUS_DOCUMENT_ESFNAME = new EsfName("DocumentViewOnlyButtonPreviousDocument");
    private static final EsfName DOCUMENT_VIEW_ONLY_BUTTON_PREVIOUS_DOCUMENT_TITLE_ESFNAME = new EsfName("DocumentViewOnlyButtonPreviousDocumentTitle");

    private static final EsfName MESSAGE_DOCUMENT_ALREADY_COMPLETED_ESFNAME = new EsfName("DocumentAlreadyCompleted");
    private static final EsfName MESSAGE_DOCUMENT_ALREADY_COMPLETED_CANNOT_EDIT_ESFNAME = new EsfName("DocumentAlreadyCompletedCannotEdit");
    private static final EsfName MESSAGE_VIEW_ONLY_DOCUMENT_FYI_ESFNAME = new EsfName("ViewOnlyFYI");
    private static final EsfName MESSAGE_REVIEW_DOCUMENT_NOT_SIGNER_ESFNAME = new EsfName("ReviewDocumentNotSigner");
    private static final EsfName MESSAGE_REVIEW_DOCUMENT_SIGNER_ESFNAME = new EsfName("ReviewDocumentSigner");
    private static final EsfName MESSAGE_COMPLETE_DOCUMENT_EDITS_ESFNAME = new EsfName("CompleteDocumentEdits");
    private static final EsfName MESSAGE_COMPLETED_ALL_DOCUMENTS_ESFNAME = new EsfName("CompletedAllDocuments");
    private static final EsfName MESSAGE_DELETED_TRANSACTION_ESFNAME = new EsfName("DeletedTransaction");

    
    protected final EsfUUID id;
    protected final EsfUUID buttonMessageId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected Record record;
   
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();

    /**
     * This creates a ButtonMessageVersion object from data retrieved from the DB.
     */
    protected ButtonMessageVersion(EsfUUID id, EsfUUID buttonMessageId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		Record record)
    {
        this.id = id;
        this.buttonMessageId = buttonMessageId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.record = record;
    }
    
    protected ButtonMessageVersion(ButtonMessage buttonmessage, User createdByUser)
    {
        this.id = new EsfUUID();
        this.buttonMessageId = buttonmessage.getId();
        this.version = buttonmessage.getProductionVersion() + 1;
        this.createdTimestamp = new EsfDateTime();
        this.createdByUserId = createdByUser.getId();
        this.lastUpdatedTimestamp = createdTimestamp;
        this.lastUpdatedByUserId = createdByUserId;
        this.record = new Record(new EsfName("ButtonsAndMessages"),BlobDb.CompressOption.ENABLE,BlobDb.EncryptOption.DISABLE);
    }
   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getButtonMessageId()
    {
        return buttonMessageId;
    }
    
    public ButtonMessage getButtonMessage()
    {
    	return ButtonMessage.Manager.getById(buttonMessageId);
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}

	public int getVersion()
    {
    	return version;
    }
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }
    
    public EsfString getPackageFooterHtml()
    {
    	return record.getStringByName(PACKAGE_FOOTER_HTML_ESFNAME);
    }
    public String getPackageFooterHtml(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageFooterHtml().toString()) : getPackageFooterHtml().toString();
    }
    public void setPackageFooterHtml(EsfString v)
    {
    	record.addUpdate(PACKAGE_FOOTER_HTML_ESFNAME,v); // we don't trim HTML
    	objectChanged();
    }

    public EsfString getDocumentFooterHtml()
    {
    	return record.getStringByName(DOCUMENT_FOOTER_HTML_ESFNAME);
    }
    public String getDocumentFooterHtml(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentFooterHtml().toString()) : getDocumentFooterHtml().toString();
    }
    public void setDocumentFooterHtml(EsfString v)
    {
    	record.addUpdate(DOCUMENT_FOOTER_HTML_ESFNAME,v); // we don't trim HTML
     	objectChanged();
    }

    public EsfString getPackageButtonContinueToDo()
    {
    	return record.getStringByName(PACKAGE_BUTTON_CONTINUE_TODO_ESFNAME);
    }
    public String getPackageButtonContinueToDo(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonContinueToDo().toString()) : getPackageButtonContinueToDo().toString();
    }
    public void setPackageButtonContinueToDo(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_CONTINUE_TODO_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getPackageButtonContinueToDoTitle()
    {
    	return record.getStringByName(PACKAGE_BUTTON_CONTINUE_TODO_TITLE_ESFNAME);
    }
    public String getPackageButtonContinueToDoTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonContinueToDoTitle().toString()) : getPackageButtonContinueToDoTitle().toString();
    }
    public void setPackageButtonContinueToDoTitle(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_CONTINUE_TODO_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getPackageButtonContinueDone()
    {
    	return record.getStringByName(PACKAGE_BUTTON_CONTINUE_DONE_ESFNAME);
    }
    public String getPackageButtonContinueDone(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonContinueDone().toString()) : getPackageButtonContinueDone().toString();
    }
    public void setPackageButtonContinueDone(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_CONTINUE_DONE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getPackageButtonContinueDoneTitle()
    {
    	return record.getStringByName(PACKAGE_BUTTON_CONTINUE_DONE_TITLE_ESFNAME);
    }
    public String getPackageButtonContinueDoneTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonContinueDoneTitle().toString()) : getPackageButtonContinueDoneTitle().toString();
    }
    public void setPackageButtonContinueDoneTitle(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_CONTINUE_DONE_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getPackageButtonEditDocument()
    {
    	return record.getStringByName(PACKAGE_BUTTON_EDIT_DOCUMENT_ESFNAME);
    }
    public String getPackageButtonEditDocument(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonEditDocument().toString()) : getPackageButtonEditDocument().toString();
    }
    public void setPackageButtonEditDocument(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_EDIT_DOCUMENT_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getPackageButtonEditDocumentTitle()
    {
    	return record.getStringByName(PACKAGE_BUTTON_EDIT_DOCUMENT_TITLE_ESFNAME);
    }
    public String getPackageButtonEditDocumentTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonEditDocumentTitle().toString()) : getPackageButtonEditDocumentTitle().toString();
    }
    public void setPackageButtonEditDocumentTitle(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_EDIT_DOCUMENT_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }
    
    public EsfString getPackageButtonViewCompletedDocument()
    {
    	return record.getStringByName(PACKAGE_BUTTON_VIEW_COMPLETED_DOCUMENT_ESFNAME);
    }
    public String getPackageButtonViewCompletedDocument(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonViewCompletedDocument().toString()) : getPackageButtonViewCompletedDocument().toString();
    }
    public void setPackageButtonViewCompletedDocument(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_VIEW_COMPLETED_DOCUMENT_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getPackageButtonViewCompletedDocumentTitle()
    {
    	return record.getStringByName(PACKAGE_BUTTON_VIEW_COMPLETED_DOCUMENT_TITLE_ESFNAME);
    }
    public String getPackageButtonViewCompletedDocumentTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonViewCompletedDocumentTitle().toString()) : getPackageButtonViewCompletedDocumentTitle().toString();
    }
    public void setPackageButtonViewCompletedDocumentTitle(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_VIEW_COMPLETED_DOCUMENT_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }
    
    public EsfString getPackageButtonDelete()
    {
    	return record.getStringByName(PACKAGE_BUTTON_DELETE_ESFNAME);
    }
    public String getPackageButtonDelete(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonDelete().toString()) : getPackageButtonDelete().toString();
    }
    public void setPackageButtonDelete(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_DELETE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getPackageButtonDeleteTitle()
    {
    	return record.getStringByName(PACKAGE_BUTTON_DELETE_TITLE_ESFNAME);
    }
    public String getPackageButtonDeleteTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonDeleteTitle().toString()) : getPackageButtonDeleteTitle().toString();
    }
    public void setPackageButtonDeleteTitle(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_DELETE_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getPackageButtonNotCompleting()
    {
    	return record.getStringByName(PACKAGE_BUTTON_NOT_COMPLETING_ESFNAME);
    }
    public String getPackageButtonNotCompleting(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonNotCompleting().toString()) : getPackageButtonNotCompleting().toString();
    }
    public void setPackageButtonNotCompleting(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_NOT_COMPLETING_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getPackageButtonNotCompletingTitle()
    {
    	return record.getStringByName(PACKAGE_BUTTON_NOT_COMPLETING_TITLE_ESFNAME);
    }
    public String getPackageButtonNotCompletingTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonNotCompletingTitle().toString()) : getPackageButtonNotCompletingTitle().toString();
    }
    public void setPackageButtonNotCompletingTitle(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_NOT_COMPLETING_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getPackageButtonDownloadMyDocsAsPdf()
    {
    	return record.getStringByName(PACKAGE_BUTTON_DOWNLOAD_MY_DOCS_AS_PDF_ESFNAME);
    }
    public String getPackageButtonDownloadMyDocsAsPdf(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonDownloadMyDocsAsPdf().toString()) : getPackageButtonDownloadMyDocsAsPdf().toString();
    }
    public void setPackageButtonDownloadMyDocsAsPdf(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_DOWNLOAD_MY_DOCS_AS_PDF_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getPackageButtonDownloadMyDocsAsPdfTitle()
    {
    	return record.getStringByName(PACKAGE_BUTTON_DOWNLOAD_MY_DOCS_AS_PDF_TITLE_ESFNAME);
    }
    public String getPackageButtonDownloadMyDocsAsPdfTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getPackageButtonDownloadMyDocsAsPdfTitle().toString()) : getPackageButtonDownloadMyDocsAsPdfTitle().toString();
    }
    public void setPackageButtonDownloadMyDocsAsPdfTitle(EsfString v)
    {
    	record.addUpdate(PACKAGE_BUTTON_DOWNLOAD_MY_DOCS_AS_PDF_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentButtonGoToPackage()
    {
    	return record.getStringByName(DOCUMENT_BUTTON_GO_TO_PACKAGE_ESFNAME);
    }
    public String getDocumentButtonGoToPackage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentButtonGoToPackage().toString()) : getDocumentButtonGoToPackage().toString();
    }
    public void setDocumentButtonGoToPackage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_BUTTON_GO_TO_PACKAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentButtonGoToPackageTitle()
    {
    	return record.getStringByName(DOCUMENT_BUTTON_GO_TO_PACKAGE_TITLE_ESFNAME);
    }
    public String getDocumentButtonGoToPackageTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentButtonGoToPackageTitle().toString()) : getDocumentButtonGoToPackageTitle().toString();
    }
    public void setDocumentButtonGoToPackageTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_BUTTON_GO_TO_PACKAGE_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentButtonGoToPackageMessage()
    {
    	return record.getStringByName(DOCUMENT_BUTTON_GO_TO_PACKAGE_MESSAGE_ESFNAME);
    }
    public String getDocumentButtonGoToPackageMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentButtonGoToPackageMessage().toString()) : getDocumentButtonGoToPackageMessage().toString();
    }
    public void setDocumentButtonGoToPackageMessage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_BUTTON_GO_TO_PACKAGE_MESSAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentEditButtonContinueNextPage()
    {
    	return record.getStringByName(DOCUMENT_EDIT_BUTTON_CONTINUE_NEXT_PAGE_ESFNAME);
    }
    public String getDocumentEditButtonContinueNextPage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentEditButtonContinueNextPage().toString()) : getDocumentEditButtonContinueNextPage().toString();
    }
    public void setDocumentEditButtonContinueNextPage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_EDIT_BUTTON_CONTINUE_NEXT_PAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentEditButtonContinueNextPageTitle()
    {
    	return record.getStringByName(DOCUMENT_EDIT_BUTTON_CONTINUE_NEXT_PAGE_TITLE_ESFNAME);
    }
    public String getDocumentEditButtonContinueNextPageTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentEditButtonContinueNextPageTitle().toString()) : getDocumentEditButtonContinueNextPageTitle().toString();
    }
    public void setDocumentEditButtonContinueNextPageTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_EDIT_BUTTON_CONTINUE_NEXT_PAGE_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentEditButtonContinueToReview()
    {
    	return record.getStringByName(DOCUMENT_EDIT_BUTTON_CONTINUE_TO_REVIEW_ESFNAME);
    }
    public String getDocumentEditButtonContinueToReview(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentEditButtonContinueToReview().toString()) : getDocumentEditButtonContinueToReview().toString();
    }
    public void setDocumentEditButtonContinueToReview(EsfString v)
    {
    	record.addUpdate(DOCUMENT_EDIT_BUTTON_CONTINUE_TO_REVIEW_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentEditButtonContinueToReviewTitle()
    {
    	return record.getStringByName(DOCUMENT_EDIT_BUTTON_CONTINUE_TO_REVIEW_TITLE_ESFNAME);
    }
    public String getDocumentEditButtonContinueToReviewTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentEditButtonContinueToReviewTitle().toString()) : getDocumentEditButtonContinueToReviewTitle().toString();
    }
    public void setDocumentEditButtonContinueToReviewTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_EDIT_BUTTON_CONTINUE_TO_REVIEW_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentEditButtonContinueToReviewMessage()
    {
    	return record.getStringByName(DOCUMENT_EDIT_BUTTON_CONTINUE_TO_REVIEW_MESSAGE_ESFNAME);
    }
    public String getDocumentEditButtonContinueToReviewMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentEditButtonContinueToReviewMessage().toString()) : getDocumentEditButtonContinueToReviewMessage().toString();
    }
    public void setDocumentEditButtonContinueToReviewMessage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_EDIT_BUTTON_CONTINUE_TO_REVIEW_MESSAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentEditButtonPreviousPage()
    {
    	return record.getStringByName(DOCUMENT_EDIT_BUTTON_PREVIOUS_PAGE_ESFNAME);
    }
    public String getDocumentEditButtonPreviousPage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentEditButtonPreviousPage().toString()) : getDocumentEditButtonPreviousPage().toString();
    }
    public void setDocumentEditButtonPreviousPage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_EDIT_BUTTON_PREVIOUS_PAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentEditButtonPreviousPageTitle()
    {
    	return record.getStringByName(DOCUMENT_EDIT_BUTTON_PREVIOUS_PAGE_TITLE_ESFNAME);
    }
    public String getDocumentEditButtonPreviousPageTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentEditButtonPreviousPageTitle().toString()) : getDocumentEditButtonPreviousPageTitle().toString();
    }
    public void setDocumentEditButtonPreviousPageTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_EDIT_BUTTON_PREVIOUS_PAGE_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentEditButtonSave()
    {
    	return record.getStringByName(DOCUMENT_EDIT_BUTTON_SAVE_ESFNAME);
    }
    public String getDocumentEditButtonSave(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentEditButtonSave().toString()) : getDocumentEditButtonSave().toString();
    }
    public void setDocumentEditButtonSave(EsfString v)
    {
    	record.addUpdate(DOCUMENT_EDIT_BUTTON_SAVE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentEditButtonSaveTitle()
    {
    	return record.getStringByName(DOCUMENT_EDIT_BUTTON_SAVE_TITLE_ESFNAME);
    }
    public String getDocumentEditButtonSaveTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentEditButtonSaveTitle().toString()) : getDocumentEditButtonSaveTitle().toString();
    }
    public void setDocumentEditButtonSaveTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_EDIT_BUTTON_SAVE_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewViewOnlyButtonContinueNextPage()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_CONTINUE_NEXT_PAGE_ESFNAME);
    }
    public String getDocumentReviewViewOnlyButtonContinueNextPage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewViewOnlyButtonContinueNextPage().toString()) : getDocumentReviewViewOnlyButtonContinueNextPage().toString();
    }
    public void setDocumentReviewViewOnlyButtonContinueNextPage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_CONTINUE_NEXT_PAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewViewOnlyButtonContinueNextPageTitle()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_CONTINUE_NEXT_PAGE_TITLE_ESFNAME);
    }
    public String getDocumentReviewViewOnlyButtonContinueNextPageTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewViewOnlyButtonContinueNextPageTitle().toString()) : getDocumentReviewViewOnlyButtonContinueNextPageTitle().toString();
    }
    public void setDocumentReviewViewOnlyButtonContinueNextPageTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_CONTINUE_NEXT_PAGE_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewViewOnlyButtonContinueNextPageMessage()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_CONTINUE_NEXT_PAGE_MESSAGE_ESFNAME);
    }
    public String getDocumentReviewViewOnlyButtonContinueNextPageMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewViewOnlyButtonContinueNextPageMessage().toString()) : getDocumentReviewViewOnlyButtonContinueNextPageMessage().toString();
    }
    public void setDocumentReviewViewOnlyButtonContinueNextPageMessage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_CONTINUE_NEXT_PAGE_MESSAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewViewOnlyButtonComplete()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_COMPLETE_ESFNAME);
    }
    public String getDocumentReviewViewOnlyButtonComplete(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewViewOnlyButtonComplete().toString()) : getDocumentReviewViewOnlyButtonComplete().toString();
    }
    public void setDocumentReviewViewOnlyButtonComplete(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_COMPLETE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewViewOnlyButtonCompleteTitle()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_COMPLETE_TITLE_ESFNAME);
    }
    public String getDocumentReviewViewOnlyButtonCompleteTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewViewOnlyButtonCompleteTitle().toString()) : getDocumentReviewViewOnlyButtonCompleteTitle().toString();
    }
    public void setDocumentReviewViewOnlyButtonCompleteTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_COMPLETE_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewViewOnlyButtonCompleteMessage()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_COMPLETE_MESSAGE_ESFNAME);
    }
    public String getDocumentReviewViewOnlyButtonCompleteMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewViewOnlyButtonCompleteMessage().toString()) : getDocumentReviewViewOnlyButtonCompleteMessage().toString();
    }
    public void setDocumentReviewViewOnlyButtonCompleteMessage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_COMPLETE_MESSAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewViewOnlyButtonPreviousPage()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_PREVIOUS_PAGE_ESFNAME);
    }
    public String getDocumentReviewViewOnlyButtonPreviousPage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewViewOnlyButtonPreviousPage().toString()) : getDocumentReviewViewOnlyButtonPreviousPage().toString();
    }
    public void setDocumentReviewViewOnlyButtonPreviousPage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_PREVIOUS_PAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewViewOnlyButtonPreviousPageTitle()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_PREVIOUS_PAGE_TITLE_ESFNAME);
    }
    public String getDocumentReviewViewOnlyButtonPreviousPageTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewViewOnlyButtonPreviousPageTitle().toString()) : getDocumentReviewViewOnlyButtonPreviousPageTitle().toString();
    }
    public void setDocumentReviewViewOnlyButtonPreviousPageTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_VIEW_ONLY_BUTTON_PREVIOUS_PAGE_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewButtonCompleteSigner()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_BUTTON_COMPLETE_SIGNER_ESFNAME);
    }
    public String getDocumentReviewButtonCompleteSigner(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewButtonCompleteSigner().toString()) : getDocumentReviewButtonCompleteSigner().toString();
    }
    public void setDocumentReviewButtonCompleteSigner(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_BUTTON_COMPLETE_SIGNER_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewButtonCompleteSignerTitle()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_BUTTON_COMPLETE_SIGNER_TITLE_ESFNAME);
    }
    public String getDocumentReviewButtonCompleteSignerTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewButtonCompleteSignerTitle().toString()) : getDocumentReviewButtonCompleteSignerTitle().toString();
    }
    public void setDocumentReviewButtonCompleteSignerTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_BUTTON_COMPLETE_SIGNER_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewButtonCompleteSignerMessage()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_BUTTON_COMPLETE_SIGNER_MESSAGE_ESFNAME);
    }
    public String getDocumentReviewButtonCompleteSignerMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewButtonCompleteSignerMessage().toString()) : getDocumentReviewButtonCompleteSignerMessage().toString();
    }
    public void setDocumentReviewButtonCompleteSignerMessage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_BUTTON_COMPLETE_SIGNER_MESSAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewButtonCompleteNotSigner()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_BUTTON_COMPLETE_NOT_SIGNER_ESFNAME);
    }
    public String getDocumentReviewButtonCompleteNotSigner(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewButtonCompleteNotSigner().toString()) : getDocumentReviewButtonCompleteNotSigner().toString();
    }
    public void setDocumentReviewButtonCompleteNotSigner(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_BUTTON_COMPLETE_NOT_SIGNER_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewButtonCompleteNotSignerTitle()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_BUTTON_COMPLETE_NOT_SIGNER_TITLE_ESFNAME);
    }
    public String getDocumentReviewButtonCompleteNotSignerTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewButtonCompleteNotSignerTitle().toString()) : getDocumentReviewButtonCompleteNotSignerTitle().toString();
    }
    public void setDocumentReviewButtonCompleteNotSignerTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_BUTTON_COMPLETE_NOT_SIGNER_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewButtonCompleteNotSignerMessage()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_BUTTON_COMPLETE_NOT_SIGNER_MESSAGE_ESFNAME);
    }
    public String getDocumentReviewButtonCompleteNotSignerMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewButtonCompleteNotSignerMessage().toString()) : getDocumentReviewButtonCompleteNotSignerMessage().toString();
    }
    public void setDocumentReviewButtonCompleteNotSignerMessage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_BUTTON_COMPLETE_NOT_SIGNER_MESSAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewButtonReturnToEdit()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_BUTTON_RETURN_TO_EDIT_ESFNAME);
    }
    public String getDocumentReviewButtonReturnToEdit(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewButtonReturnToEdit().toString()) : getDocumentReviewButtonReturnToEdit().toString();
    }
    public void setDocumentReviewButtonReturnToEdit(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_BUTTON_RETURN_TO_EDIT_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewButtonReturnToEditTitle()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_BUTTON_RETURN_TO_EDIT_TITLE_ESFNAME);
    }
    public String getDocumentReviewButtonReturnToEditTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewButtonReturnToEditTitle().toString()) : getDocumentReviewButtonReturnToEditTitle().toString();
    }
    public void setDocumentReviewButtonReturnToEditTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_BUTTON_RETURN_TO_EDIT_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentReviewButtonReturnToEditMessage()
    {
    	return record.getStringByName(DOCUMENT_REVIEW_BUTTON_RETURN_TO_EDIT_MESSAGE_ESFNAME);
    }
    public String getDocumentReviewButtonReturnToEditMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentReviewButtonReturnToEditMessage().toString()) : getDocumentReviewButtonReturnToEditMessage().toString();
    }
    public void setDocumentReviewButtonReturnToEditMessage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_REVIEW_BUTTON_RETURN_TO_EDIT_MESSAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentViewOnlyButtonNextPage()
    {
    	return record.getStringByName(DOCUMENT_VIEW_ONLY_BUTTON_NEXT_PAGE_ESFNAME);
    }
    public String getDocumentViewOnlyButtonNextPage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentViewOnlyButtonNextPage().toString()) : getDocumentViewOnlyButtonNextPage().toString();
    }
    public void setDocumentViewOnlyButtonNextPage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_VIEW_ONLY_BUTTON_NEXT_PAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentViewOnlyButtonNextPageTitle()
    {
    	return record.getStringByName(DOCUMENT_VIEW_ONLY_BUTTON_NEXT_PAGE_TITLE_ESFNAME);
    }
    public String getDocumentViewOnlyButtonNextPageTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentViewOnlyButtonNextPageTitle().toString()) : getDocumentViewOnlyButtonNextPageTitle().toString();
    }
    public void setDocumentViewOnlyButtonNextPageTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_VIEW_ONLY_BUTTON_NEXT_PAGE_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentViewOnlyButtonPreviousPage()
    {
    	return record.getStringByName(DOCUMENT_VIEW_ONLY_BUTTON_PREVIOUS_PAGE_ESFNAME);
    }
    public String getDocumentViewOnlyButtonPreviousPage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentViewOnlyButtonPreviousPage().toString()) : getDocumentViewOnlyButtonPreviousPage().toString();
    }
    public void setDocumentViewOnlyButtonPreviousPage(EsfString v)
    {
    	record.addUpdate(DOCUMENT_VIEW_ONLY_BUTTON_PREVIOUS_PAGE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentViewOnlyButtonPreviousPageTitle()
    {
    	return record.getStringByName(DOCUMENT_VIEW_ONLY_BUTTON_PREVIOUS_PAGE_TITLE_ESFNAME);
    }
    public String getDocumentViewOnlyButtonPreviousPageTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentViewOnlyButtonPreviousPageTitle().toString()) : getDocumentViewOnlyButtonPreviousPageTitle().toString();
    }
    public void setDocumentViewOnlyButtonPreviousPageTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_VIEW_ONLY_BUTTON_PREVIOUS_PAGE_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentViewOnlyButtonNextDocument()
    {
    	return record.getStringByName(DOCUMENT_VIEW_ONLY_BUTTON_NEXT_DOCUMENT_ESFNAME);
    }
    public String getDocumentViewOnlyButtonNextDocument(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentViewOnlyButtonNextDocument().toString()) : getDocumentViewOnlyButtonNextDocument().toString();
    }
    public void setDocumentViewOnlyButtonNextDocument(EsfString v)
    {
    	record.addUpdate(DOCUMENT_VIEW_ONLY_BUTTON_NEXT_DOCUMENT_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentViewOnlyButtonNextDocumentTitle()
    {
    	return record.getStringByName(DOCUMENT_VIEW_ONLY_BUTTON_NEXT_DOCUMENT_TITLE_ESFNAME);
    }
    public String getDocumentViewOnlyButtonNextDocumentTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentViewOnlyButtonNextDocumentTitle().toString()) : getDocumentViewOnlyButtonNextDocumentTitle().toString();
    }
    public void setDocumentViewOnlyButtonNextDocumentTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_VIEW_ONLY_BUTTON_NEXT_DOCUMENT_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentViewOnlyButtonPreviousDocument()
    {
    	return record.getStringByName(DOCUMENT_VIEW_ONLY_BUTTON_PREVIOUS_DOCUMENT_ESFNAME);
    }
    public String getDocumentViewOnlyButtonPreviousDocument(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentViewOnlyButtonPreviousDocument().toString()) : getDocumentViewOnlyButtonPreviousDocument().toString();
    }
    public void setDocumentViewOnlyButtonPreviousDocument(EsfString v)
    {
    	record.addUpdate(DOCUMENT_VIEW_ONLY_BUTTON_PREVIOUS_DOCUMENT_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentViewOnlyButtonPreviousDocumentTitle()
    {
    	return record.getStringByName(DOCUMENT_VIEW_ONLY_BUTTON_PREVIOUS_DOCUMENT_TITLE_ESFNAME);
    }
    public String getDocumentViewOnlyButtonPreviousDocumentTitle(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentViewOnlyButtonPreviousDocumentTitle().toString()) : getDocumentViewOnlyButtonPreviousDocumentTitle().toString();
    }
    public void setDocumentViewOnlyButtonPreviousDocumentTitle(EsfString v)
    {
    	record.addUpdate(DOCUMENT_VIEW_ONLY_BUTTON_PREVIOUS_DOCUMENT_TITLE_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentAlreadyCompletedMessage()
    {
    	return record.getStringByName(MESSAGE_DOCUMENT_ALREADY_COMPLETED_ESFNAME);
    }
    public String getDocumentAlreadyCompletedMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentAlreadyCompletedMessage().toString()) : getDocumentAlreadyCompletedMessage().toString();
    }
    public void setDocumentAlreadyCompletedMessage(EsfString v)
    {
    	record.addUpdate(MESSAGE_DOCUMENT_ALREADY_COMPLETED_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDocumentAlreadyCompletedCannotEditMessage()
    {
    	return record.getStringByName(MESSAGE_DOCUMENT_ALREADY_COMPLETED_CANNOT_EDIT_ESFNAME);
    }
    public String getDocumentAlreadyCompletedCannotEditMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDocumentAlreadyCompletedCannotEditMessage().toString()) : getDocumentAlreadyCompletedCannotEditMessage().toString();
    }
    public void setDocumentAlreadyCompletedCannotEditMessage(EsfString v)
    {
    	record.addUpdate(MESSAGE_DOCUMENT_ALREADY_COMPLETED_CANNOT_EDIT_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getViewOnlyDocumentFYIMessage()
    {
    	return record.getStringByName(MESSAGE_VIEW_ONLY_DOCUMENT_FYI_ESFNAME);
    }
    public String getViewOnlyDocumentFYIMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getViewOnlyDocumentFYIMessage().toString()) : getViewOnlyDocumentFYIMessage().toString();
    }
    public void setViewOnlyDocumentFYIMessage(EsfString v)
    {
    	record.addUpdate(MESSAGE_VIEW_ONLY_DOCUMENT_FYI_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getReviewDocumentNotSignerMessage()
    {
    	return record.getStringByName(MESSAGE_REVIEW_DOCUMENT_NOT_SIGNER_ESFNAME);
    }
    public String getReviewDocumentNotSignerMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getReviewDocumentNotSignerMessage().toString()) : getReviewDocumentNotSignerMessage().toString();
    }
    public void setReviewDocumentNotSignerMessage(EsfString v)
    {
    	record.addUpdate(MESSAGE_REVIEW_DOCUMENT_NOT_SIGNER_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getReviewDocumentSignerMessage()
    {
    	return record.getStringByName(MESSAGE_REVIEW_DOCUMENT_SIGNER_ESFNAME);
    }
    public String getReviewDocumentSignerMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getReviewDocumentSignerMessage().toString()) : getReviewDocumentSignerMessage().toString();
    }
    public void setReviewDocumentSignerMessage(EsfString v)
    {
    	record.addUpdate(MESSAGE_REVIEW_DOCUMENT_SIGNER_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getCompleteDocumentEditsMessage()
    {
    	return record.getStringByName(MESSAGE_COMPLETE_DOCUMENT_EDITS_ESFNAME);
    }
    public String getCompleteDocumentEditsMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getCompleteDocumentEditsMessage().toString()) : getCompleteDocumentEditsMessage().toString();
    }
    public void setCompleteDocumentEditsMessage(EsfString v)
    {
    	record.addUpdate(MESSAGE_COMPLETE_DOCUMENT_EDITS_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getCompletedAllDocumentsMessage()
    {
    	return record.getStringByName(MESSAGE_COMPLETED_ALL_DOCUMENTS_ESFNAME);
    }
    public String getCompletedAllDocumentsMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getCompletedAllDocumentsMessage().toString()) : getCompletedAllDocumentsMessage().toString();
    }
    public void setCompletedAllDocumentsMessage(EsfString v)
    {
    	record.addUpdate(MESSAGE_COMPLETED_ALL_DOCUMENTS_ESFNAME,v.trim());
     	objectChanged();
    }

    public EsfString getDeletedTransactionMessage()
    {
    	return record.getStringByName(MESSAGE_DELETED_TRANSACTION_ESFNAME);
    }
    public String getDeletedTransactionMessage(TransactionContext context)
    {
    	return context != null && context.hasTransaction() ? context.transaction.getFieldSpecWithSubstitutions(context.user, context.currDocumentVersion, getDeletedTransactionMessage().toString()) : getDeletedTransactionMessage().toString();
    }
    public void setDeletedTransactionMessage(EsfString v)
    {
    	record.addUpdate(MESSAGE_DELETED_TRANSACTION_ESFNAME,v.trim());
     	objectChanged();
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof ButtonMessageVersion )
        {
        	ButtonMessageVersion otherButtonMessage = (ButtonMessageVersion)o;
            return getId().equals(otherButtonMessage.getId());
        }
        return false;
    }
    
    public Record getRecord()
    {
        return record;
    }

    protected boolean saveRecord()
    {
    	return record.save();
    }


    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(ButtonMessageVersion o)
    {
    	if ( getButtonMessageId().equals(o.getButtonMessageId()) )
    		return getVersion() - o.getVersion();
    	return getButtonMessageId().compareTo(o.getButtonMessageId());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<ButtonMessageVersion xmlns=\"").append(XmlUtil.getXmlNamespace2012()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <buttonMessageId>").append(buttonMessageId.toXml()).append("</buttonMessageId>\n");
        buf.append(" <version>").append(version).append("</version>\n");
        buf.append(" <createdTimestamp>").append(createdTimestamp.toXml()).append("</createdTimestamp>\n");
        buf.append(" <createdByUserId>").append(createdByUserId.toXml()).append("</createdByUserId>\n");
        buf.append(" <lastUpdatedTimestamp>").append(lastUpdatedTimestamp.toXml()).append("</lastUpdatedTimestamp>\n");
        buf.append(" <lastUpdatedByUserId>").append(lastUpdatedByUserId.toXml()).append("</lastUpdatedByUserId>\n");
        buf.append(" <record>");
        record.appendXml(buf);
        buf.append("</record>\n");

        buf.append("</ButtonMessageVersion>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 316 + id.getEstimatedLengthXml() + buttonMessageId.getEstimatedLengthXml() + createdTimestamp.getEstimatedLengthXml() + createdByUserId.getEstimatedLengthXml() + 
        	lastUpdatedTimestamp.getEstimatedLengthXml() + lastUpdatedByUserId.getEstimatedLengthXml();

        len += record.getEstimatedLengthXml();

        return len; 
    }


    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; buttonMessageId: " + buttonMessageId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
            	
            	record.save(con);
                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_buttonmessage_version (id,library_buttonmessage_id,version,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,record_blob_id) VALUES (?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(buttonMessageId);
                stmt.set(version);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(record.getId());
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; buttonMessageId: " + buttonMessageId + "; version: " + version);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this);
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new button message version id: " + id + "; buttonMessageId: " + buttonMessageId + "; version: " + version); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new button message version id: " + id + "; buttonMessageId: " + buttonMessageId + "; version: " + version);
                }
                
                return true;
            }
            
            // We only save if we're a Test version.
            if ( getButtonMessage().getTestVersion() == version )
            {
                if ( record.hasChanged() )
                    record.save(con);
            }
        	
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_buttonmessage_version SET last_updated_timestamp=?,last_updated_by_user_id=? WHERE id=?"
                						   		);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for buttonmessage version id: " + id + "; buttonMessageId: " + buttonMessageId + "; version: " + version);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated button message version id: " + id + "; buttonMessageId: " + buttonMessageId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated button message version id: " + id + "; buttonMessageId: " + buttonMessageId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; buttonMessageId: " + buttonMessageId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; buttonMessageId: " + buttonMessageId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use 
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of button message version that was pending an INSERT id: " + id + "; buttonMessageId: " + buttonMessageId + "; version: " + version);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
            // Delete the buttonmessage version and its record
        	record.delete(con);
            
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_buttonmessage_version WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted button message version id: " + id + "; buttonMessageId: " + buttonMessageId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted button message version id: " + id + "; buttonMessageId: " + buttonMessageId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"delete() on id: " + id + "; buttonMessageId: " + buttonMessageId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   		private static UUIDCacheReadOptimized<ButtonMessageVersion> cache = new UUIDCacheReadOptimized<ButtonMessageVersion>();
   		
   		static final ButtonMessageVersion getById(Connection con, EsfUUID buttonmessageVersionId) throws SQLException
   		{
   			return getById(con,buttonmessageVersionId,true);
   		}
   		static ButtonMessageVersion getById(Connection con, EsfUUID buttonmessageVersionId, boolean checkCache) throws SQLException
   		{
   			if ( checkCache )
   			{
   				ButtonMessageVersion buttonmessageVer = cache.getById(buttonmessageVersionId);
   	   			if ( buttonmessageVer != null )
   	   				return buttonmessageVer;
   			}

   			EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT library_buttonmessage_id,version,created_by_user_id,last_updated_by_user_id,created_timestamp,last_updated_timestamp,record_blob_id " +
   	        			"FROM esf_library_buttonmessage_version WHERE id = ?"
   	        									);
   	        	stmt.set(buttonmessageVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID libraryButtonMessageId = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID recordBlobId = rs.getEsfUUID();
		            
		            Record record = Record.Manager.getById(con,recordBlobId);
		            
		            ButtonMessageVersion buttonmessageVer = new ButtonMessageVersion(buttonmessageVersionId,libraryButtonMessageId,version,
		            											 createdTimestamp,createdByUserId,lastUpdatedTimestamp,lastUpdatedByUserId,
		            											 record);
		            buttonmessageVer.setLoadedFromDb();
		            cache.add(buttonmessageVer);
		            
	   	            return buttonmessageVer;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + buttonmessageVersionId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + buttonmessageVersionId + "; failed to find the button message version");
   	        return null; 
  		}
   		
   		public static ButtonMessageVersion getById(EsfUUID buttonmessageVersionId)
   		{
   			ButtonMessageVersion buttonmessageVer = cache.getById(buttonmessageVersionId);
   			if ( buttonmessageVer != null )
   				return buttonmessageVer;
   	   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	buttonmessageVer = getById(con,buttonmessageVersionId,false);
   	        	if ( buttonmessageVer != null ) 
   	        	{
		            con.commit();
	   	        	return buttonmessageVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		static ButtonMessageVersion getByVersion(Connection con, EsfUUID libraryButtonMessageId, int version) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_buttonmessage_version WHERE library_buttonmessage_id = ? AND version = ?" );
   	        	stmt.set(libraryButtonMessageId);
   	        	stmt.set(version);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID docVersionId = rs.getEsfUUID();
	       			return getById(con,docVersionId);
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByVersion() - libraryButtonMessageId: " + libraryButtonMessageId + "; version: " + version);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getByVersion() - libraryButtonMessageId: " + libraryButtonMessageId + "; version: " + version + "; failed to find the buttonmessage version");
   	        return null; 
  		}
   		
   		public static ButtonMessageVersion getByVersion(EsfUUID libraryButtonMessageId, int version)
   		{
   			// While not always true, if the version is 1, it's possible this is a new object that's not been saved yet so we can't find the id in the DB, so we'll check our cache first just in case
   			if ( version == 1 )
   			{
   				for( EsfUUID id : cache.getAllIds() )
   				{
   					ButtonMessageVersion buttonmessageVer = cache.getById(id);
   					if ( buttonmessageVer.getButtonMessageId().equals(libraryButtonMessageId) && buttonmessageVer.getVersion() == version )
   						return buttonmessageVer;
   				}
   			}
   			
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	ButtonMessageVersion buttonmessageVer = getByVersion(con,libraryButtonMessageId,version);
   	        	if ( buttonmessageVer != null ) 
   	        	{
		            con.commit();
	   	        	return buttonmessageVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		
   		public static List<ButtonMessageVersion> getAllByButtonMessageId(Connection con, EsfUUID libraryButtonMessageId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<ButtonMessageVersion> list = new LinkedList<ButtonMessageVersion>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_buttonmessage_version WHERE library_buttonmessage_id = ?" );
   	        	stmt.set(libraryButtonMessageId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID buttonmessageVersionId = rs.getEsfUUID();
	            	
	            	ButtonMessageVersion buttonmessageVer = getById(con,buttonmessageVersionId);
	       			if ( buttonmessageVer != null )
	       				list.add(buttonmessageVer);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<ButtonMessageVersion> getAllByButtonMessageId(EsfUUID libraryButtonMessageId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<ButtonMessageVersion> list = getAllByButtonMessageId(con,libraryButtonMessageId);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return new LinkedList<ButtonMessageVersion>(); 
   		}
   	    
   		
   	    
   	    public static ButtonMessageVersion createTest(ButtonMessage buttonmessage, User createdBy)
   	    {
   	    	if ( buttonmessage == null || createdBy == null )
   	    		return null;
   	    	ButtonMessageVersion newButtonMessageVersion = new ButtonMessageVersion(buttonmessage,createdBy);
   	    	cache.add(newButtonMessageVersion);
   	    	return newButtonMessageVersion;
   	    }

   	    public static ButtonMessageVersion createLike(ButtonMessage buttonmessage, ButtonMessageVersion likeButtonMessageVersion, User createdBy)
   	    {
   	    	if ( buttonmessage == null || likeButtonMessageVersion == null || createdBy == null )
   	    		return null;
   	    	ButtonMessageVersion newButtonMessageVersion = createTest(buttonmessage,createdBy);
   	    	newButtonMessageVersion.record = new Record(likeButtonMessageVersion.getRecord());
   	    	return newButtonMessageVersion;
   	    }

   	    public static ButtonMessageVersion createFromJDOM(ButtonMessage buttonMessage, Element e)
   	    {
   	    	if ( buttonMessage == null || e == null )
   	    		return null;
   	    	
   	    	
   	    	EsfUUID id = new EsfUUID();
   	    	
			Element recordHolderElement = e.getChild("record", e.getNamespace());
			Element recordElement = recordHolderElement.getChild("esfRecord", Namespace.getNamespace(XmlUtil.getXmlNamespace2009()));
			
			Record record = null;
			try
			{
				record = new Record(new EsfUUID(), recordElement);
			}
			catch( EsfException ex )
			{
				_logger.error("createFromJDOM() failed on ButtonsAndMessages/esfRecord for button message: " + buttonMessage.getEsfName(), ex);
				record = new Record(new EsfName("ButtonsAndMessages"),BlobDb.CompressOption.ENABLE,BlobDb.EncryptOption.DISABLE); // bad, but empty is better than nothing?!
			}
			
			ButtonMessageVersion newButtonMessageVersion = new ButtonMessageVersion(
   	    			id, 
   	    			buttonMessage.getId(), 
   	    			buttonMessage.getTestVersion(),
   	    			null,
   	    			null,
   	    			null,
   	    			null,
   	    			record
   	    			);
			newButtonMessageVersion.setNewObject();
   	    	cache.add(newButtonMessageVersion);
   	    	return newButtonMessageVersion;
   	    }
   	    
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    /**
   	     * Finds all button message versions that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}

   	} // Manager
   	
}