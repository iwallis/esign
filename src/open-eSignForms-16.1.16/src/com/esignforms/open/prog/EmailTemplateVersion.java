// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;

import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* EmailTemplateVersion holds the definition of a specific version of an email template held in a container.
* An email template contains information about the From, To, Cc, Bcc, Subject and body (text and html).
* 
* @author Yozons, Inc.
*/
public class EmailTemplateVersion
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<EmailTemplateVersion>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable
{
 	private static final long serialVersionUID = -8478332390115589936L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(EmailTemplateVersion.class);
    
    protected final EsfUUID id;
    protected final EsfUUID emailTemplateId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected String emailFrom;
    protected String emailTo;
    protected String emailCc;
    protected String emailBcc;
    protected String emailSubject;
    protected String emailText;
    protected String emailHtml;
    
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();

    /**
     * This creates a EmailTemplateVersion object from data retrieved from the DB.
     */
    protected EmailTemplateVersion(EsfUUID id, EsfUUID emailTemplateId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		String emailFrom, String emailTo, String emailCc, String emailBcc, String emailSubject, String emailText, String emailHtml)
    {
        this.id = id;
        this.emailTemplateId = emailTemplateId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.emailFrom = emailFrom;
        this.emailTo = emailTo;
        this.emailCc = emailCc;
        this.emailBcc = emailBcc;
        this.emailSubject = emailSubject;
        this.emailText = emailText;
        this.emailHtml = emailHtml;
    }
    
    protected EmailTemplateVersion(EmailTemplate EmailTemplate, User createdByUser)
    {
        this.id = new EsfUUID();
        this.emailTemplateId = EmailTemplate.getId();
        this.version = EmailTemplate.getProductionVersion() + 1;
        this.createdTimestamp = new EsfDateTime();
        this.createdByUserId = createdByUser.getId();
        this.lastUpdatedTimestamp = createdTimestamp;
        this.lastUpdatedByUserId = createdByUserId;
        this.emailFrom = this.emailTo = this.emailCc = this.emailBcc = this.emailSubject = this.emailText = this.emailHtml = "";
    }

   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getEmailTemplateId()
    {
        return emailTemplateId;
    }
    
    public EmailTemplate getEmailTemplate()
    {
    	return EmailTemplate.Manager.getById(emailTemplateId);
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}

	public int getVersion()
    {
    	return version;
    }
    
	public String getVersionLabel()
	{
		return getEmailTemplate().getVersionLabel(version);
	}
	public String getEmailTemplateNameVersion()
	{
		return getEmailTemplate().getEsfName() + " [" + version + "]";
	}
	public String getEmailTemplateNameVersionWithLabel()
	{
		EmailTemplate EmailTemplate = getEmailTemplate();
		return EmailTemplate.getEsfName() + " [" + version + "] (" + EmailTemplate.getVersionLabel(version) + ")";
	}
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }
    
    public String getEmailFrom()
    {
    	return emailFrom;
    }
    public boolean hasEmailFrom()
    {
    	return EsfString.isNonBlank(emailFrom);
    }
    public void setEmailFrom(String v)
    {
    	if ( v == null )
    		emailFrom = v;
    	else if ( v.length() > Literals.EMAIL_ADDRESS_MAX_LENGTH )
    		emailFrom = v.substring(0,Literals.EMAIL_ADDRESS_MAX_LENGTH).trim();
        else
        	emailFrom = v.trim();
        objectChanged();
    }
    
    public String getEmailTo()
    {
    	return emailTo;
    }
    public boolean hasEmailTo()
    {
    	return EsfString.isNonBlank(emailTo);
    }
    public void setEmailTo(String v)
    {
    	if ( v == null )
    		emailTo = v;
    	else if ( v.length() > Literals.EMAIL_ADDRESS_LIST_MAX_LENGTH )
    		emailTo = v.substring(0,Literals.EMAIL_ADDRESS_LIST_MAX_LENGTH).trim();
        else
        	emailTo = v.trim();
        objectChanged();
    }
    
    public String getEmailCc()
    {
    	return emailCc;
    }
    public boolean hasEmailCc()
    {
    	return EsfString.isNonBlank(emailCc);
    }
    public void setEmailCc(String v)
    {
    	if ( v == null )
    		emailCc = v;
    	else if ( v.length() > Literals.EMAIL_ADDRESS_LIST_MAX_LENGTH )
    		emailCc = v.substring(0,Literals.EMAIL_ADDRESS_LIST_MAX_LENGTH).trim();
        else
        	emailCc = v.trim();
        objectChanged();
    }
    
    public String getEmailBcc()
    {
    	return emailBcc;
    }
    public boolean hasEmailBcc()
    {
    	return EsfString.isNonBlank(emailBcc);
    }
    public void setEmailBcc(String v)
    {
    	if ( v == null )
    		emailBcc = v;
    	else if ( v.length() > Literals.EMAIL_ADDRESS_LIST_MAX_LENGTH )
    		emailBcc = v.substring(0,Literals.EMAIL_ADDRESS_LIST_MAX_LENGTH).trim();
        else
        	emailBcc = v.trim();
        objectChanged();
    }
    
    public String getEmailSubject()
    {
    	return emailSubject;
    }
    public boolean hasEmailSubject()
    {
    	return EsfString.isNonBlank(emailSubject);
    }
    public void setEmailSubject(String v)
    {
    	if ( v == null )
    		emailSubject = v;
    	else if ( v.length() > Literals.EMAIL_SUBJECT_MAX_LENGTH )
    		emailSubject = v.substring(0,Literals.EMAIL_SUBJECT_MAX_LENGTH).trim();
        else
        	emailSubject = v.trim();
        objectChanged();
    }
    
    public String getEmailText()
    {
    	return emailText;
    }
    public boolean hasEmailText()
    {
    	return EsfString.isNonBlank(emailText);
    }
    public void setEmailText(String v)
    {
    	if ( v == null )
    		emailText = v;
    	else if ( v.length() > Literals.EMAIL_TEXT_MAX_LENGTH )
    		emailText = v.substring(0,Literals.EMAIL_TEXT_MAX_LENGTH).trim();
        else
        	emailText = v.trim();
        objectChanged();
    }
    
    public String getEmailHtml()
    {
    	return emailHtml;
    }
    public boolean hasEmailHtml()
    {
    	return EsfString.isNonBlank(emailHtml);
    }
    public void setEmailHtml(String v)
    {
    	if ( v == null )
    		emailHtml = v;
    	else if ( v.length() > Literals.EMAIL_HTML_MAX_LENGTH )
    		emailHtml = v.substring(0,Literals.EMAIL_HTML_MAX_LENGTH).trim();
        else
        	emailHtml = v; // We don't trim since CKEditor puts a newline at the end of each line
        objectChanged();
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof EmailTemplateVersion )
        {
        	EmailTemplateVersion otherEmailTemplate = (EmailTemplateVersion)o;
            return getId().equals(otherEmailTemplate.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(EmailTemplateVersion o)
    {
    	if ( getEmailTemplateId().equals(o.getEmailTemplateId()) )
    		return getVersion() - o.getVersion();
    	return getEmailTemplateId().compareTo(o.getEmailTemplateId());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<EmailTemplateVersion xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <emailTemplateId>").append(emailTemplateId.toXml()).append("</emailTemplateId>\n");
        buf.append(" <version>").append(version).append("</version>\n");
        buf.append(" <createdTimestamp>").append(createdTimestamp.toXml()).append("</createdTimestamp>\n");
        buf.append(" <createdByUserId>").append(createdByUserId.toXml()).append("</createdByUserId>\n");
        buf.append(" <lastUpdatedTimestamp>").append(lastUpdatedTimestamp.toXml()).append("</lastUpdatedTimestamp>\n");
        buf.append(" <lastUpdatedByUserId>").append(lastUpdatedByUserId.toXml()).append("</lastUpdatedByUserId>\n");
        buf.append(" <emailFrom>").append(escapeXml(emailFrom)).append("</emailFrom>\n");
        buf.append(" <emailTo>").append(escapeXml(emailTo)).append("</emailTo>\n");
        buf.append(" <emailCc>").append(escapeXml(emailCc)).append("</emailCc>\n");
        buf.append(" <emailBcc>").append(escapeXml(emailBcc)).append("</emailBcc>\n");
        buf.append(" <emailSubject>").append(escapeXml(emailSubject)).append("</emailSubject>\n");
        buf.append(" <emailText>").append(escapeXml(emailText)).append("</emailText>\n");
        buf.append(" <emailHtml>").append(escapeXml(emailHtml)).append("</emailHtml>\n");

        buf.append("</EmailTemplateVersion>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 500 + id.getEstimatedLengthXml() + emailTemplateId.getEstimatedLengthXml() + createdTimestamp.getEstimatedLengthXml() + createdByUserId.getEstimatedLengthXml() + 
        	lastUpdatedTimestamp.getEstimatedLengthXml() + lastUpdatedByUserId.getEstimatedLengthXml();

        if ( emailFrom != null )
        	len += emailFrom.length();
        if ( emailTo != null )
        	emailTo.length();
        if ( emailCc != null ) 
        	emailCc.length();
        if ( emailBcc != null )
        	emailBcc.length();
        if ( emailSubject != null )
        	emailSubject.length();
        if ( emailText != null )
        	emailText.length();
        if ( emailHtml != null )
        	emailHtml.length();

        return len; 
    }

    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; emailTemplateId: " + emailTemplateId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
            	
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_library_email_template_version (id,library_email_template_id,version,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id," +
                	"email_from,email_to,email_cc,email_bcc,email_subject,email_text,email_html) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(emailTemplateId);
                stmt.set(version);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(emailFrom);
                stmt.set(emailTo);
                stmt.set(emailCc);
                stmt.set(emailBcc);
                stmt.set(emailSubject);
                stmt.set(emailText);
                stmt.set(emailHtml);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; emailTemplateId: " + emailTemplateId + "; version: " + version);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this);
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new property set version id: " + id + "; emailTemplateId: " + emailTemplateId + "; version: " + version); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new property set version id: " + id + "; emailTemplateId: " + emailTemplateId + "; version: " + version);
                }
                
                return true;
            }
            
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_library_email_template_version SET last_updated_timestamp=?,last_updated_by_user_id=?,email_from=?,email_to=?,email_cc=?,email_bcc=?,email_subject=?,email_text=?,email_html=? WHERE id=?"
                						   		);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(emailFrom);
                stmt.set(emailTo);
                stmt.set(emailCc);
                stmt.set(emailBcc);
                stmt.set(emailSubject);
                stmt.set(emailText);
                stmt.set(emailHtml);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for EmailTemplate version id: " + id + "; emailTemplateId: " + emailTemplateId + "; version: " + version);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated EmailTemplate version id: " + id + "; emailTemplateId: " + emailTemplateId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated EmailTemplate version id: " + id + "; emailTemplateId: " + emailTemplateId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; emailTemplateId: " + emailTemplateId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; emailTemplateId: " + emailTemplateId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use in the transaction definition.
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of property set version that was pending an INSERT id: " + id + "; emailTemplateId: " + emailTemplateId + "; version: " + version);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_library_email_template_version WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted property set version id: " + id + "; emailTemplateId: " + emailTemplateId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted property set version id: " + id + "; emailTemplateId: " + emailTemplateId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"delete() on id: " + id + "; emailTemplateId: " + emailTemplateId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   		private static UUIDCacheReadOptimized<EmailTemplateVersion> cache = new UUIDCacheReadOptimized<EmailTemplateVersion>();
   		
   		static final EmailTemplateVersion getById(Connection con, EsfUUID EmailTemplateVersionId) throws SQLException
   		{
   			return getById(con,EmailTemplateVersionId,true);
   		}
   		static EmailTemplateVersion getById(Connection con, EsfUUID emailTemplateVersionId, boolean checkCache) throws SQLException
   		{
   			if ( checkCache )
   			{
   				EmailTemplateVersion EmailTemplateVer = cache.getById(emailTemplateVersionId);
   	   			if ( EmailTemplateVer != null )
   	   				return EmailTemplateVer;
   			}

   			EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT library_email_template_id,version,created_by_user_id,last_updated_by_user_id,created_timestamp,last_updated_timestamp," +
   	        			"email_from,email_to,email_cc,email_bcc,email_subject,email_text,email_html " +
   	        			"FROM esf_library_email_template_version WHERE id = ?"
   	        									);
   	        	stmt.set(emailTemplateVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID emailTemplateId = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
	                String emailFrom = rs.getString();
	                String emailTo = rs.getString();
	                String emailCc = rs.getString();
	                String emailBcc = rs.getString();
	                String emailSubject = rs.getString();
	                String emailText = rs.getString();
	                String emailHtml = rs.getString();
		            
		            EmailTemplateVersion EmailTemplateVer = new EmailTemplateVersion(emailTemplateVersionId,emailTemplateId,version,
		            											 createdTimestamp,createdByUserId,lastUpdatedTimestamp,lastUpdatedByUserId,
		            											 emailFrom,emailTo,emailCc,emailBcc,emailSubject,emailText,emailHtml);
		            EmailTemplateVer.setLoadedFromDb();
		            cache.add(EmailTemplateVer);
		            
	   	            return EmailTemplateVer;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + emailTemplateVersionId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + emailTemplateVersionId + "; failed to find the property set version");
   	        return null; 
  		}
   		
   		public static EmailTemplateVersion getById(EsfUUID emailTemplateVersionId)
   		{
   			EmailTemplateVersion EmailTemplateVer = cache.getById(emailTemplateVersionId);
   			if ( EmailTemplateVer != null )
   				return EmailTemplateVer;
   	   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	EmailTemplateVer = getById(con,emailTemplateVersionId,false);
   	        	if ( EmailTemplateVer != null ) 
   	        	{
		            con.commit();
	   	        	return EmailTemplateVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		static EmailTemplateVersion getByVersion(Connection con, EsfUUID emailTemplateId, int version) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_email_template_version WHERE library_email_template_id=? AND version=?" );
   	        	stmt.set(emailTemplateId);
   	        	stmt.set(version);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	       			return getById(con,id);
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByVersion() - library_email_template_id: " + emailTemplateId + "; version: " + version);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getByVersion() - library_email_template_id: " + emailTemplateId + "; version: " + version + "; failed to find the EmailTemplate version");
   	        return null; 
  		}
   		
   		public static EmailTemplateVersion getByVersion(EsfUUID emailTemplateId, int version)
   		{
   			// While not always true, if the version is 1, it's possible this is a new object that's not been saved yet so we can't find the id in the DB, so we'll check our cache first just in case
   			if ( version == 1 )
   			{
   				for( EsfUUID id : cache.getAllIds() )
   				{
   					EmailTemplateVersion emailTemplateVer = cache.getById(id);
   					if ( emailTemplateVer.getEmailTemplateId().equals(emailTemplateId) && emailTemplateVer.getVersion() == version )
   						return emailTemplateVer;
   				}
   			}
   			
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	EmailTemplateVersion EmailTemplateVer = getByVersion(con,emailTemplateId,version);
   	        	if ( EmailTemplateVer != null ) 
   	        	{
		            con.commit();
	   	        	return EmailTemplateVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		
   		public static List<EmailTemplateVersion> getAllByEmailTemplateId(Connection con, EsfUUID emailTemplateId)
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   			LinkedList<EmailTemplateVersion> list = new LinkedList<EmailTemplateVersion>();
   			
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_library_email_template_version WHERE library_email_template_id=?" );
   	        	stmt.set(emailTemplateId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	
	            	EmailTemplateVersion EmailTemplateVer = getById(con,id);
	       			if ( EmailTemplateVer != null )
	       				list.add(EmailTemplateVer);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<EmailTemplateVersion> getAllByEmailTemplateId(EsfUUID emailTemplateId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<EmailTemplateVersion> list = getAllByEmailTemplateId(con,emailTemplateId);
   	        	con.commit();
   	        	return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return new LinkedList<EmailTemplateVersion>(); 
   		}
   	    
   		
   	    
   	    public static EmailTemplateVersion createTest(EmailTemplate EmailTemplate, User createdBy)
   	    {
   	    	if ( EmailTemplate == null || createdBy == null )
   	    		return null;
   	    	EmailTemplateVersion newEmailTemplateVersion = new EmailTemplateVersion(EmailTemplate,createdBy);
   	    	cache.add(newEmailTemplateVersion);
   	    	return newEmailTemplateVersion;
   	    }

   	    public static EmailTemplateVersion createLike(EmailTemplate EmailTemplate, EmailTemplateVersion likeEmailTemplateVersion, User createdBy)
   	    {
   	    	if ( EmailTemplate == null || likeEmailTemplateVersion == null || createdBy == null )
   	    		return null;
   	    	EmailTemplateVersion newEmailTemplateVersion = createTest(EmailTemplate,createdBy);
   	    	newEmailTemplateVersion.emailFrom = likeEmailTemplateVersion.emailFrom;
   	    	newEmailTemplateVersion.emailTo = likeEmailTemplateVersion.emailTo;
   	    	newEmailTemplateVersion.emailCc = likeEmailTemplateVersion.emailCc;
   	    	newEmailTemplateVersion.emailBcc = likeEmailTemplateVersion.emailBcc;
   	    	newEmailTemplateVersion.emailSubject = likeEmailTemplateVersion.emailSubject;
   	    	newEmailTemplateVersion.emailText = likeEmailTemplateVersion.emailText;
   	    	newEmailTemplateVersion.emailHtml = likeEmailTemplateVersion.emailHtml;
   	    	return newEmailTemplateVersion;
   	    }

   	    public static EmailTemplateVersion createFromJDOM(EmailTemplate email, Element e)
   	    {
   	    	if ( email == null || e == null )
   	    		return null;
   	    	
   	    	EsfUUID id = new EsfUUID();
   	    	
   	    	EmailTemplateVersion newEmailVersion = new EmailTemplateVersion(
   	    			id, 
   	    			email.getId(), 
   	    			email.getTestVersion(),
   	    			null,
   	    			null,
   	    			null,
   	    			null,
   	    			e.getChildText("emailFrom", e.getNamespace()),
   	    			e.getChildText("emailTo", e.getNamespace()),
   	    			e.getChildText("emailCc", e.getNamespace()),
   	    			e.getChildText("emailBcc", e.getNamespace()),
   	    			e.getChildText("emailSubject", e.getNamespace()),
   	    			e.getChildText("emailText", e.getNamespace()),
   	    			e.getChildText("emailHtml", e.getNamespace())
   	    			);
   	    	newEmailVersion.setNewObject();
   	    	cache.add(newEmailVersion);
   	    	return newEmailVersion;
   	    }
   	    
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all property set versions that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}

   	} // Manager
   	
}