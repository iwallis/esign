// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.TreeMap;
import java.util.TreeSet;

import org.jdom2.Element;
import org.jdom2.Namespace;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;
import com.esignforms.open.util.UUIDCacheReadOptimized;
import com.esignforms.open.util.XmlUtil;

/**
* PackageVersion holds the definition of a specific version of a package.
* A package version has one or more documents.
* 
* @author Yozons, Inc.
*/
public class PackageVersion
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<PackageVersion>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable, java.io.Serializable
{
	private static final long serialVersionUID = 7530823389939079804L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackageVersion.class);

    protected EsfUUID id;
    protected EsfUUID packageId;
    protected int	  version;
    protected EsfDateTime createdTimestamp;
    protected EsfUUID createdByUserId;
    protected EsfDateTime lastUpdatedTimestamp;
    protected EsfUUID lastUpdatedByUserId;
    protected EsfUUID packageDocumentId; // the document used as the package handler; can override for a given party
    protected EsfUUID buttonMessageId; // the button message set to use for this package; can override for a given party
    protected EsfUUID programmingBlobId; // may be null; if present, is a blob id that contains our PackageProgramming configuration (in XML).
    protected PackageProgramming packageProgramming; // is null until loaded, if any such programming has been configured
    
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();
    
    // The ordered list of documents in this package
    protected List<EsfUUID> documentIdList;
    // The ordered list of parties
    protected List<PackageVersionPartyTemplate> packageVersionPartyTemplateList;
    // The ordered list of report fields to be populated from this package
    protected List<PackageVersionReportField> packageVersionReportFieldList;
    
    private List<PackageVersionPartyTemplate> removedPackageVersionPartyTemplateList; // when set, on save, we'll remove these party templates from the package version
    private List<PackageVersionReportField> removedPackageVersionReportFieldList; // when set, on save, we'll remove these report fields from the package version

    /**
     * This creates a PackageVersion object from data retrieved from the DB.
     */
    protected PackageVersion(EsfUUID id, EsfUUID packageId, int version, 
    		EsfDateTime createdTimestamp, EsfUUID createdByUserId, EsfDateTime lastUpdatedTimestamp, EsfUUID lastUpdatedByUserId,
    		EsfUUID packageDocumentId, EsfUUID buttonMessageId, List<EsfUUID> documentIdList, List<PackageVersionPartyTemplate> packageVersionPartyTemplateList, 
    		List<PackageVersionReportField> packageVersionReportFieldList, EsfUUID programmingBlobId)
    {
        this.id = id;
        this.packageId = packageId;
        this.version = version;
        this.createdTimestamp = createdTimestamp;
        this.createdByUserId = createdByUserId;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.lastUpdatedByUserId = lastUpdatedByUserId;
        this.packageDocumentId = packageDocumentId;
        this.buttonMessageId = buttonMessageId;
        this.documentIdList = documentIdList;
        this.packageVersionPartyTemplateList = packageVersionPartyTemplateList;
        this.packageVersionReportFieldList = packageVersionReportFieldList;
        this.programmingBlobId = programmingBlobId;
   }
    
    protected PackageVersion(Package pkg, EsfUUID packageDocumentId, EsfUUID buttonMessageId, User createdByUser)
    {
        this.id = new EsfUUID();
        this.packageId = pkg.getId();
        this.version = pkg.getProductionVersion() + 1;
        this.createdTimestamp = new EsfDateTime();
        this.createdByUserId = createdByUser.getId();
        this.lastUpdatedTimestamp = createdTimestamp;
        this.lastUpdatedByUserId = createdByUserId;
        this.packageDocumentId = packageDocumentId;
        this.buttonMessageId = buttonMessageId;
        this.programmingBlobId = null;
        
        documentIdList = new LinkedList<EsfUUID>();
        packageVersionPartyTemplateList = new LinkedList<PackageVersionPartyTemplate>();
        packageVersionReportFieldList = new LinkedList<PackageVersionReportField>();
    }

   
    // Used by the APIs that need a copy to update, but don't want to update our "real" object that is likely cached.
    public PackageVersion duplicate() 
    {
    	PackageVersion pkgVer = new PackageVersion(id,packageId,version,createdTimestamp,createdByUserId,lastUpdatedTimestamp,lastUpdatedByUserId,
    			packageDocumentId,buttonMessageId,getDocumentIdList(),getDuplicatedPackageVersionPartyTemplateList(),getDuplicatedPackageVersionReportFieldList(),programmingBlobId);
    	pkgVer.setDatabaseObjectLike(this);
    	return pkgVer;
    }
    
    @Override
    public boolean hasChanged()
    {
        if ( hasChanged )
        	return true;
        
        // It's possible if there was no package programming that we loaded it, and only if it changed do we consider this object to have changed.
        if ( packageProgramming != null && packageProgramming.hasChanged() )
        	return true;
        
        if ( removedPackageVersionPartyTemplateList != null && removedPackageVersionPartyTemplateList.size() > 0 )
        	return true;
        
        if ( removedPackageVersionReportFieldList != null && removedPackageVersionReportFieldList.size() > 0 )
        	return true;

        for( PackageVersionPartyTemplate pvpt : packageVersionPartyTemplateList )
        {
        	if ( pvpt.hasChanged() )
        		return true;
        }
        
        for( PackageVersionReportField reportField : packageVersionReportFieldList )
        {
        	if ( reportField.hasChanged() )
        		return true;
        }
                
        return false;
    }

    
    public EsfUUID getId()
    {
        return id;
    }
    
    public EsfUUID getPackageId()
    {
        return packageId;
    }
    public Package getPackage()
    {
    	return Package.Manager.getById(packageId);
    }
    
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}

    public int getVersion()
    {
    	return version;
    }
    
	public String getVersionLabel()
	{
		return getPackage().getVersionLabel(version);
	}
	public String getPackagePathNameVersion()
	{
		return getPackage().getPathName() + " [" + version + "]";
	}
	public String getPackagePathNameVersionWithLabel()
	{
		Package pkg = getPackage();
		return pkg.getPathName() + " [" + version + "] (" + pkg.getVersionLabel(version) + ")";
	}
    
    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }
    
    public EsfUUID getCreatedByUserId()
    {
    	return createdByUserId;
    }
    
    public EsfDateTime getLastUpdatedTimestamp()
    {
    	return lastUpdatedTimestamp;
    }
    
    public EsfUUID getLastUpdatedByUserId()
    {
    	return lastUpdatedByUserId;
    }
    
    public EsfUUID getPackageDocumentId()
    {
        return packageDocumentId;
    }
    public Document getPackageDocument()
    {
    	return Document.Manager.getById(packageDocumentId);
    }
    public void setPackageDocumentId(EsfUUID v)
    {
    	packageDocumentId = v;
    	objectChanged();
    }
    
    public EsfUUID getButtonMessageId()
    {
        return buttonMessageId;
    }
    public ButtonMessage getButtonMessage()
    {
    	return ButtonMessage.Manager.getById(buttonMessageId);
    }
    public void setButtonMessageId(EsfUUID v)
    {
    	buttonMessageId = v;
    	objectChanged();
    }
    
    public List<EsfUUID> getDocumentIdList()
    {
    	return new LinkedList<EsfUUID>(documentIdList);
    }
    public List<EsfUUID> _getDocumentIdListInternal()
    {
    	return documentIdList;
    }
    public List<EsfName> getDocumentNameList()
    {
    	LinkedList<EsfName> list = new LinkedList<EsfName>();
    	for( EsfUUID docId : documentIdList ) 
    	{
    		list.add(Document.Manager.getById(docId).getEsfName());
    	}
    	return list;
    }
    public int getNumDocuments()
    {
    	return documentIdList.size();
    }
    public synchronized void setDocumentIdOrder(List<EsfUUID> newDocOrderList)
    {
		if ( newDocOrderList == null )
			return;
		
    	LinkedList<EsfUUID> origDocIdList = new LinkedList<EsfUUID>(documentIdList);
    	LinkedList<EsfUUID> newDocIdList = new LinkedList<EsfUUID>();

    	// For each document id in our new list, if we find it in the original list, we move it into our new list in this new order.
    	for( EsfUUID docId : newDocOrderList ) {
        	ListIterator<EsfUUID> origDocIdListIter = origDocIdList.listIterator();
        	while( origDocIdListIter.hasNext() ) {
        		EsfUUID origDocId = origDocIdListIter.next();
        		if ( origDocId.equals(docId) ) {
        			newDocIdList.add(docId);
        			origDocIdListIter.remove();
        			break;
        		}
        	}
    	}
    	
    	// If we have any document ids remaining (were not specified in the newDocOrderList list), lets put them in too
    	ListIterator<EsfUUID> origDocIdListIter = origDocIdList.listIterator();
    	while( origDocIdListIter.hasNext() ) {
    		EsfUUID origDocId = origDocIdListIter.next();
    		newDocIdList.add(origDocId);
    	}
    	
     	documentIdList = newDocIdList;
     	objectChanged();
    }
    
    public synchronized void addDocumentId(EsfUUID docId)
    {
    	documentIdList.add(docId);
    	objectChanged();
    }
    
    public synchronized void removeDocumentId(EsfUUID docId)
    {
     	// If we find the document in list, we remove it along with any document party mappings.
    	ListIterator<EsfUUID> listIter = documentIdList.listIterator();
    	while( listIter.hasNext() ) {
    		EsfUUID existingDocId = listIter.next();
    		if ( existingDocId.equals(docId) ) {
    			listIter.remove();
        		// If this document is referenced by any of our parties, we'll drop that document from their mapping access
        		for( PackageVersionPartyTemplate partyTemplate : packageVersionPartyTemplateList )
        		{
        			for( PackageVersionPartyTemplateDocumentParty docParty : partyTemplate.getPackageVersionPartyTemplateDocumentPartiesByDocumentId(docId) )
        				partyTemplate.removePackageVersionPartyTemplateDocumentParty(docParty);
        		}
              	objectChanged();
        		break;
    		}
    	}
    }

    public List<PackageVersionPartyTemplate> getPackageVersionPartyTemplateList()
    {
    	return new LinkedList<PackageVersionPartyTemplate>(packageVersionPartyTemplateList);
    }
    public List<PackageVersionPartyTemplate> getDuplicatedPackageVersionPartyTemplateList()
    {
    	LinkedList<PackageVersionPartyTemplate> list = new LinkedList<PackageVersionPartyTemplate>();
    	for( PackageVersionPartyTemplate template : packageVersionPartyTemplateList )
    		list.add(template.duplicate());
    	return list;
    }
    public List<PackageVersionPartyTemplate> _getPackageVersionPartyTemplateListInternal()
    {
    	return packageVersionPartyTemplateList;
    }
    public List<EsfName> getPackageVersionPartyNameList()
    {
    	LinkedList<EsfName> list = new LinkedList<EsfName>();
    	for( PackageVersionPartyTemplate pvpt : packageVersionPartyTemplateList ) 
    	{
    		list.add(pvpt.getEsfName());
    	}
    	return list;
    }
    public int getNumPackageParties()
    {
    	return packageVersionPartyTemplateList.size();
    }
    public PackageVersionPartyTemplate getFirstPackageVersionPartyTemplate()
    {
    	return getNumPackageParties() > 0 ? packageVersionPartyTemplateList.get(0) : null;
    }
    public PackageVersionPartyTemplate getPackageVersionPartyTemplate(EsfUUID partyId)
    {
    	for( PackageVersionPartyTemplate pvpt : packageVersionPartyTemplateList ) 
    	{
    		for ( PackageVersionPartyTemplateDocumentParty pvptdp : pvpt.getPackageVersionPartyTemplateDocumentParties() )
    		{
    			if ( pvptdp.getPackageVersionPartyTemplateId().equals(partyId) )
    				return pvpt;
    		}
    	}    	
    	return null;
    }
    
    public PackageVersionPartyTemplate getPackageVersionPartyTemplateByDocumentPartyTemplateId(EsfUUID docPartyId, boolean doProductionResolve)
    {
    	for( PackageVersionPartyTemplate pvpt : packageVersionPartyTemplateList ) 
    	{
    		for ( PackageVersionPartyTemplateDocumentParty pvptdp : pvpt.getPackageVersionPartyTemplateDocumentParties() )
    		{
    			if ( pvptdp.isDocumentPartyTemplateView() )
    				continue;
    			Document document = Document.Manager.getById(pvptdp.getDocumentId());
    			if ( document == null )
    				continue;
    			DocumentVersion docVer = doProductionResolve ? document.getProductionDocumentVersion() : document.getTestDocumentVersion();
    			if ( docVer == null )
    				continue;
    			PartyTemplate pt = docVer.getPartyTemplate(docPartyId);
    			if ( pt != null )
    				return pvpt;
    		}
    	}    	
    	return null;
    }

    public synchronized void setPackageVersionPartyTemplateIdOrder(List<EsfUUID> newPartyIdOrderList)
    {
		if ( newPartyIdOrderList == null )
			return;
		
    	LinkedList<PackageVersionPartyTemplate> origPartyList = new LinkedList<PackageVersionPartyTemplate>(packageVersionPartyTemplateList);
    	LinkedList<PackageVersionPartyTemplate> newPartyList = new LinkedList<PackageVersionPartyTemplate>();

    	// For each party in our new list, if we find it in the original list, we move it into our new list in this new order
    	for( EsfUUID partyId : newPartyIdOrderList ) {
        	ListIterator<PackageVersionPartyTemplate> origPartyListIter = origPartyList.listIterator();
        	while( origPartyListIter.hasNext() ) {
        		PackageVersionPartyTemplate origParty = origPartyListIter.next();
        		if ( origParty.getId().equals(partyId) ) {
        			newPartyList.add(origParty);
        			origPartyListIter.remove();
        			break;
        		}
        	}
    	}
    	
    	// If we have any parties remaining (were not specified in the newPartyOrderList list), lets put them in too
    	ListIterator<PackageVersionPartyTemplate> origPartyListIter = origPartyList.listIterator();
    	while( origPartyListIter.hasNext() ) {
    		PackageVersionPartyTemplate origParty = origPartyListIter.next();
    		newPartyList.add(origParty);
    	}
    	
    	packageVersionPartyTemplateList = newPartyList;
     	objectChanged();
    }
    
    public synchronized PackageVersionPartyTemplate addViewOnlyParty()
    {
    	int newPartySuffix = 0;
    	EsfName newPartyName = null;
    	String newPartyDisplayName = null;
    	while( newPartyName == null )
    	{
        	String newPartyNameSuffix = newPartySuffix == 0 ? "" : Integer.toString(newPartySuffix);
    		newPartyName = new EsfName( Application.getInstance().getServerMessages().getString("PackageVersionPartyTemplate.newViewOnlyParty.esfname",newPartyNameSuffix) );
    		boolean alreadyExists = false;
    		for( PackageVersionPartyTemplate pvpt : packageVersionPartyTemplateList )
    		{
    			if ( pvpt.getEsfName().equals(newPartyName) )
    			{
    				alreadyExists = true;
    				break;
    			}
    		}
    		if ( alreadyExists )
    		{
    			newPartyName = null;
    			++newPartySuffix;
    		}
    		else
    			newPartyDisplayName = Application.getInstance().getServerMessages().getString("PackageVersionPartyTemplate.newViewOnlyParty.displayName",newPartyNameSuffix);
    	}
    	  	
    	PackageVersionPartyTemplate packageParty = PackageVersionPartyTemplate.Manager.createNew(id, newPartyName, newPartyDisplayName);
    	
    	for( EsfUUID docId : documentIdList )
    	{
    		PackageVersionPartyTemplateDocumentParty mapping = PackageVersionPartyTemplateDocumentParty.Manager.createNew(packageParty.getId(), docId, PartyTemplate.ESF_PARTY_NAME_VIEW_ONLY, Literals.STATUS_ENABLED, Literals.STATUS_ENABLED);
    		packageParty.addPackageVersionPartyTemplateDocumentParty(mapping);
    	}
    	
		addPackageVersionPartyTemplate(packageParty);
		return packageParty;
    }
    
    public synchronized void addPackageVersionPartyTemplate(PackageVersionPartyTemplate packagePartyTemplate)
    {
    	List<PackageVersionPartyTemplate> newList = getPackageVersionPartyTemplateList();
    	newList.add(packagePartyTemplate);
    	packageVersionPartyTemplateList = newList;
    	objectChanged();
    }
    
    public synchronized boolean removePackageVersionPartyTemplate(PackageVersionPartyTemplate packagePartyTemplate)
    {
    	boolean wasRemoved = false;
    	
    	List<PackageVersionPartyTemplate> newList = getPackageVersionPartyTemplateList();
    	
    	ListIterator<PackageVersionPartyTemplate> iter = newList.listIterator();
    	while( iter.hasNext() ) 
    	{
    		PackageVersionPartyTemplate pt = iter.next();
    		if ( pt.equals(packagePartyTemplate) )
    		{
    			if ( removedPackageVersionPartyTemplateList == null )
    				removedPackageVersionPartyTemplateList = new LinkedList<PackageVersionPartyTemplate>();
    			removedPackageVersionPartyTemplateList.add(pt);
    			iter.remove();
    			wasRemoved = true;
    			break;
    		}
    	}
    	if ( wasRemoved )
    	{
        	packageVersionPartyTemplateList = newList;
        	objectChanged();
    	}
    	
    	return wasRemoved;
    }
    
    public PackageVersionPartyTemplate getPackageVersionPartyTemplateByName(EsfName partyName)
    {
    	for( PackageVersionPartyTemplate party : packageVersionPartyTemplateList )
    	{
    		if ( party.getEsfName().equals(partyName) )
    			return party;
    	}
    	return null;
    }
    public PackageVersionPartyTemplate getEsfReportsAccessPackageVersionPartyTemplate()
    {
    	return getPackageVersionPartyTemplateByName(PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS);
    }
    
    public PackageVersionPartyTemplate getPackageVersionPartyTemplateById(EsfUUID partyId)
    {
    	for( PackageVersionPartyTemplate party : packageVersionPartyTemplateList )
    	{
    		if ( party.getId().equals(partyId) )
    			return party;
    	}
    	return null;
    }
    
    // This is not a standard method, but one to handle reports access to live transactions. If the special party
    // is not defined, we create it with view-optional access to all documents.
    public PackageVersionPartyTemplate _createViewOptionalEsfReportsAccessPackageVersionPartyTemplate()
    {
    	PackageVersionPartyTemplate packageParty = getEsfReportsAccessPackageVersionPartyTemplate();
    	if ( packageParty != null )
    	{
    		_logger.warn("_createViewOptionalEsfReportsAccessPackageVersionPartyTemplate() - Unexpectedly found existing ESF_reports_access package version party in packageVersionId: " + id);
    		return packageParty;
    	}
    	
    	String displayName = Application.getInstance().getServerMessages().getString("PackageVersionPartyTemplate.ESF_reports_access.dislayName");
    	
		packageParty = PackageVersionPartyTemplate.Manager.createNew(id,PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS,displayName);
    	if ( documentIdList.size() == 1 )
    		packageParty.setLandingPageFirstDoc();   	
    	packageParty.setRequireLogin(true);
    	packageParty.setAllowDeleteTranIfUnsigned(true);

		for( EsfUUID docId : documentIdList )
		{
			Document doc = Document.Manager.getById(docId);
			PackageVersionPartyTemplateDocumentParty mapping = PackageVersionPartyTemplateDocumentParty.Manager.createNew(packageParty.getId(), doc.getId(), PartyTemplate.ESF_PARTY_NAME_VIEW_OPTIONAL, Literals.STATUS_ENABLED, Literals.STATUS_ENABLED);
			packageParty.addPackageVersionPartyTemplateDocumentParty(mapping);
		}
		
		addPackageVersionPartyTemplate(packageParty);
    	
    	objectChanged();
		return save() ? getEsfReportsAccessPackageVersionPartyTemplate() : null;
    }
    

    
    public List<PartyTemplate> getDocumentVersionPartyTemplates(DocumentVersion documentVersion, PackageVersionPartyTemplate packageVersionPartyTemplate)
    {
    	List<PartyTemplate> list = new LinkedList<PartyTemplate>();
    	
    	// If we don't even know who the package party, we can't do more
    	if ( packageVersionPartyTemplate == null )
    	{
    		_logger.warn("getDocumentVersionPartyTemplates() documentVersion: " + documentVersion.getDocumentNameVersion() + "; missing packageVersionPartyTemplate specification"); 
    		return list;
    	}
    	
		for( PackageVersionPartyTemplateDocumentParty partyMapping : packageVersionPartyTemplate.getPackageVersionPartyTemplateDocumentPartiesByDocumentId(documentVersion.getDocumentId()) )
		{
			// If the mapping is to the document, but not a specific document party, we don't want to return that here
			if ( ! partyMapping.isDocumentPartyTemplateView() )
			{
				PartyTemplate pt = documentVersion.getPartyTemplate(partyMapping.getDocumentPartyTemplateName());
				if ( pt == null )
					_logger.warn("getDocumentVersionPartyTemplates() documentVersion: " + documentVersion.getDocumentNameVersion() + "; missing party template mapping for doc party: " + partyMapping.getDocumentPartyTemplateName());
				else
					list.add(pt);
			}
		}

		return list;
    }
    
    /**
     * This method adds the specified document and its parties to our lists if they are not already present, and it maps
     * the parties to this specific party.  Since this only makes sense for a Test Package Version, we also are looking
     * for Test Document Versions (which may be Production versions if no Test exists).
     * @param document
     */
    public synchronized boolean addDocumentAndPartiesToTestPackage(Document document)
    {
    	if ( document == null || documentIdList.contains(document.getId()) )
    		return false;
    	
    	documentIdList.add(document.getId());
    	objectChanged();
    	
    	// Okay, that's a new document for us, so let's add in all the parties defined in the document to our package
    	DocumentVersion docVer = document.getTestDocumentVersion(); // may be production if no test version exists
    	for ( PartyTemplate docParty : docVer.getPartyTemplateList() )
    	{
    		PackageVersionPartyTemplate packageParty = getPackageVersionPartyTemplateByName(docParty.getEsfName());
    		if ( packageParty == null ) 
    		{
    			// if we don't find it, let's create this party for the package, point to the doc party and add to our list
    			packageParty = PackageVersionPartyTemplate.Manager.createNew(id, docParty.getEsfName(), docParty.getDisplayName());
    			PackageVersionPartyTemplateDocumentParty mapping = PackageVersionPartyTemplateDocumentParty.Manager.createNew(packageParty.getId(), document.getId(), docParty.getEsfName(), Literals.STATUS_ENABLED, Literals.STATUS_ENABLED);
    			packageParty.addPackageVersionPartyTemplateDocumentParty(mapping);
    			addPackageVersionPartyTemplate(packageParty);
    		}
    		else
    		{
    			// We did find the package party, so now we make the mapping if that's not already there too, otherwise create the mapping now
    			PackageVersionPartyTemplateDocumentParty mapping = packageParty.getPackageVersionPartyTemplateDocumentPartyByDocumentIdPartyTemplateName(document.getId(),docParty.getEsfName());
    			if ( mapping == null )
    			{
    				mapping = PackageVersionPartyTemplateDocumentParty.Manager.createNew(packageParty.getId(), document.getId(), docParty.getEsfName(), Literals.STATUS_ENABLED, Literals.STATUS_ENABLED);
    				packageParty.addPackageVersionPartyTemplateDocumentParty(mapping);
    			}
    		}
    	}
    	
    	return true;
    }
    
    // This is not a standard method, but one to handle test documents in which no party has been set.
    // If we find no parties, then for each document we have, create a mapping of a placeholder Initiator to the document as view-only.
    public EsfName _createTestPartyToAllDocumentsIfNoPackagePartiesSet()
    {
    	EsfName createdTestParty = null; 
    	
    	List<PackageVersionPartyTemplate> parties = PackageVersionPartyTemplate.Manager.getAll(id);
    	if ( parties.size() == 0 )
    	{
    		createdTestParty = new EsfName("TestGenerated_NoPartyDefined");
    		for( EsfUUID docId : documentIdList )
    		{
    			Document doc = Document.Manager.getById(docId);
    			PackageVersionPartyTemplate packageParty = PackageVersionPartyTemplate.Manager.createNew(id,createdTestParty,"The un-party");
    			PackageVersionPartyTemplateDocumentParty mapping = PackageVersionPartyTemplateDocumentParty.Manager.createNew(packageParty.getId(), doc.getId(), PartyTemplate.ESF_PARTY_NAME_VIEW_ONLY, Literals.STATUS_DISABLED, Literals.STATUS_DISABLED);
    			packageParty.addPackageVersionPartyTemplateDocumentParty(mapping);
    			addPackageVersionPartyTemplate(packageParty);
    		}
    	}
    	objectChanged();
    	return createdTestParty;
    }
    
    public List<PackageVersionPartyTemplate> getPackageVersionPartyTemplates(EsfUUID docId, EsfName docPartyName) 
    {
    	List<PackageVersionPartyTemplate> list = new LinkedList<PackageVersionPartyTemplate>();
    	
    	for( PackageVersionPartyTemplate party : packageVersionPartyTemplateList )
    	{
    		List<PackageVersionPartyTemplateDocumentParty> packageVersionPartyTemplateDocumentPartyList = party.getPackageVersionPartyTemplateDocumentParties();
    		for( PackageVersionPartyTemplateDocumentParty packageVersionPartyTemplateDocumentParty : packageVersionPartyTemplateDocumentPartyList ) 
    		{
    			if ( packageVersionPartyTemplateDocumentParty.getDocumentId().equals(docId) && packageVersionPartyTemplateDocumentParty.isMatchingDocumentPartyTemplateName(docPartyName) ) 
        			list.add(party);
    		}
    	}
    	
    	return list;
    }
    public List<EsfUUID> getPackageVersionPartyTemplateIds(EsfUUID docId, EsfName docPartyName) 
    {
    	List<EsfUUID> list = new LinkedList<EsfUUID>();
    	
    	for( PackageVersionPartyTemplate party : packageVersionPartyTemplateList )
    	{
    		List<PackageVersionPartyTemplateDocumentParty> packageVersionPartyTemplateDocumentPartyList = party.getPackageVersionPartyTemplateDocumentParties();
    		for( PackageVersionPartyTemplateDocumentParty packageVersionPartyTemplateDocumentParty : packageVersionPartyTemplateDocumentPartyList ) 
    		{
    			if ( packageVersionPartyTemplateDocumentParty.getDocumentId().equals(docId) && packageVersionPartyTemplateDocumentParty.isMatchingDocumentPartyTemplateName(docPartyName) ) 
        			list.add(party.getId());
    		}
    	}
    	
    	return list;
    }
    
    public void mapDocumentPartyToPackageParties(EsfUUID docId, EsfName docPartyName, java.util.Set<EsfUUID> packagePartyIds) 
    {
    	for( PackageVersionPartyTemplate packageParty : packageVersionPartyTemplateList )
    	{
    		boolean isTargetPackageParty = packagePartyIds.contains(packageParty.getId());
    		boolean mappingFound = false;
    		
    		List<PackageVersionPartyTemplateDocumentParty> packageVersionPartyTemplateDocumentPartyList = packageParty.getPackageVersionPartyTemplateDocumentParties();
    		for( PackageVersionPartyTemplateDocumentParty packageVersionPartyTemplateDocumentParty : packageVersionPartyTemplateDocumentPartyList ) 
    		{
    			// If we find a mapping of this package party to the specified document and party, then we remove it if it's not in our target list.
    			if ( packageVersionPartyTemplateDocumentParty.getDocumentId().equals(docId) && packageVersionPartyTemplateDocumentParty.isMatchingDocumentPartyTemplateName(docPartyName) ) 
    			{
    				if ( isTargetPackageParty )
    					mappingFound = true;
    				else
    				{
    					packageParty.removePackageVersionPartyTemplateDocumentParty(packageVersionPartyTemplateDocumentParty);
    					objectChanged();
    				}
    				break;
    			}
    		}
    		
    		if ( isTargetPackageParty && ! mappingFound )
    		{
    			PackageVersionPartyTemplateDocumentParty mapping = PackageVersionPartyTemplateDocumentParty.Manager.createNew(packageParty.getId(), docId, docPartyName, Literals.STATUS_ENABLED, Literals.STATUS_ENABLED);
    			packageParty.addPackageVersionPartyTemplateDocumentParty(mapping);
    			objectChanged();
    		}
    	}
    }
    
    
    public List<PackageVersionReportField> getPackageVersionReportFieldList()
    {
    	return new LinkedList<PackageVersionReportField>(packageVersionReportFieldList);
    }
    public List<PackageVersionReportField> getDuplicatedPackageVersionReportFieldList()
    {
    	LinkedList<PackageVersionReportField> list = new LinkedList<PackageVersionReportField>();
    	for( PackageVersionReportField reportField : packageVersionReportFieldList )
    		list.add(reportField.duplicate());
    	return list;
    }
    public List<PackageVersionReportField> _getPackageVersionReportFieldListInternal()
    {
    	return packageVersionReportFieldList;
    }
    public void _setReportFieldListInternal(List<PackageVersionReportField> v) 
    {
    	packageVersionReportFieldList = v;
     	objectChanged();
    }
    public void setReportFieldList(List<PackageVersionReportField> v) 
    {
		if ( v == null )
			return;
		
    	LinkedList<PackageVersionReportField> origList = new LinkedList<PackageVersionReportField>(packageVersionReportFieldList);
    	LinkedList<PackageVersionReportField> newList = new LinkedList<PackageVersionReportField>();
    	TreeSet<EsfUUID> dupReportFieldTemplateIdChecker = new TreeSet<EsfUUID>();

    	short order = 1;
    	// For each report field in our new list, if we find it in the original list, we move it into our new list in this new order
    	for( PackageVersionReportField reportField : v ) {
    		if ( dupReportFieldTemplateIdChecker.contains(reportField.getReportFieldTemplateId()) ) {
    			Application.getInstance().warning("PackageVersion.setReportFieldList() - Found and skipped duplicate report field template id: " + reportField.getReportFieldTemplateId()); 
    		} else {
    			dupReportFieldTemplateIdChecker.add(reportField.getReportFieldTemplateId());
	    		reportField.setFieldOrder(order++);
				newList.add(reportField);
    		}

			ListIterator<PackageVersionReportField> origListIter = origList.listIterator();
        	while( origListIter.hasNext() ) {
        		PackageVersionReportField origReportField = origListIter.next();
        		if ( origReportField.equals(reportField) ) {
        			origListIter.remove();
        			break;
        		}
        	}
    	}
    	
    	// If we have any report fields remaining (were not specified in the newReportFieldOrderList list), we'll mark them for removal
    	ListIterator<PackageVersionReportField> origListIter = origList.listIterator();
    	while( origListIter.hasNext() ) {
    		PackageVersionReportField origReportField = origListIter.next();
			if ( removedPackageVersionReportFieldList == null )
				removedPackageVersionReportFieldList = new LinkedList<PackageVersionReportField>();
			removedPackageVersionReportFieldList.add(origReportField);
    	}
    	
    	dupReportFieldTemplateIdChecker.clear();
    	_setReportFieldListInternal(newList);
    }
    
    public synchronized boolean removePackageVersionReportField(PackageVersionReportField reportField)
    {
    	boolean wasRemoved = false;
    	
    	List<PackageVersionReportField> newList = getPackageVersionReportFieldList();
    	
    	ListIterator<PackageVersionReportField> iter = newList.listIterator();
    	while( iter.hasNext() ) 
    	{
    		PackageVersionReportField rf = iter.next();
    		if ( rf.equals(reportField) )
    		{
    			if ( removedPackageVersionReportFieldList == null )
    				removedPackageVersionReportFieldList = new LinkedList<PackageVersionReportField>();
    			removedPackageVersionReportFieldList.add(rf);
    			iter.remove();
    			wasRemoved = true;
    			break;
    		}
    	}
    	if ( wasRemoved )
    	{
        	packageVersionReportFieldList = newList;
        	objectChanged();
    	}
    	
    	return wasRemoved;
    }
    

    public synchronized PackageProgramming getPackageProgramming()
    {
        if ( packageProgramming == null )
        {
        	if ( programmingBlobId == null )
        	{
    			packageProgramming = new PackageProgramming();
    			programmingBlobId = packageProgramming.getId();
    			//objectChanged(); -- don't mark it as changed anymore since our hasChanged() method will check if this subobject has actually changed or not
        	}
        	else
        	{
            	packageProgramming = PackageProgramming.Manager.getById(programmingBlobId);
        	}
        }
        return packageProgramming;
    }
    public boolean hasPackageProgramming()
    {
    	return programmingBlobId != null;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof PackageVersion )
        {
        	PackageVersion otherPackage = (PackageVersion)o;
            return getId().equals(otherPackage.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(PackageVersion o)
    {
    	if ( getPackageId().equals(o.getPackageId()) )
    		return getVersion() - o.getVersion();
    	return getPackageId().compareTo(o.getPackageId());
    }
    
    public synchronized void importFromJDOM(Element element, Errors errors, Library defaultSearchLibrary, Collection<Library> allSearchableLibraries)
    {
    	Namespace ns = element.getNamespace();
    	Namespace ns2011 = Namespace.getNamespace(XmlUtil.getXmlNamespace2011());

    	// Remove all documents
    	for( EsfUUID docId : getDocumentIdList() )
    		removeDocumentId(docId);
    	
    	// Remove all package parties
    	for( PackageVersionPartyTemplate pvpt : getPackageVersionPartyTemplateList() )
    		removePackageVersionPartyTemplate(pvpt);
    	
    	// Remove all report fields
    	for( PackageVersionReportField pvrf : getPackageVersionReportFieldList() )
    		removePackageVersionReportField(pvrf);
    	
    	// If we have package programming, let's load it now and clear any rules
    	if ( hasPackageProgramming() )
    	{
    		PackageProgramming pp = getPackageProgramming();
    		pp.clearRules();
    	}
    	
		// Map from the IDs in the XML imported to IDs used in this system and create the imported documents list
    	HashMap<EsfUUID,EsfUUID> idMap = new HashMap<EsfUUID,EsfUUID>();
		List<Element> documentElements = element.getChildren("document", ns);
		for( Element documentElement : documentElements ) 
		{
			EsfUUID documentId = new EsfUUID(documentElement.getChildText("id", ns));
			EsfName documentEsfName = new EsfName(documentElement.getChildText("esfname", ns));
			Document document = findDocument(documentEsfName,defaultSearchLibrary,allSearchableLibraries);
			if ( document == null ) 
			{
				errors.addError(Application.getInstance().getServerMessages().getString("PackageVersion.import.error.documentNotFound",documentEsfName));
				// no mapping can be done
			} 
			else 
			{
				errors.addInfo(Application.getInstance().getServerMessages().getString("PackageVersion.import.info.documentFound",documentEsfName,document.getLibrary().getPathName()));
				idMap.put(documentId, document.getId()); // no mapping can be done
				addDocumentId(document.getId());
			}
		}
		
		// Next we'll create a new package party for each element in the XML using our document id mapping to update as needed
		List<Element> packageVersionPartyTemplateElements = element.getChildren("PackageVersionPartyTemplate", ns);
		for( Element packageVersionPartyTemplateElement : packageVersionPartyTemplateElements ) 
		{
			EsfUUID origPackageVersionPartyTemplateId = new EsfUUID(packageVersionPartyTemplateElement.getChildText("id", ns));
			PackageVersionPartyTemplate pvpt = PackageVersionPartyTemplate.Manager.createFromJDOM(packageVersionPartyTemplateElement,getId(),idMap);
			addPackageVersionPartyTemplate(pvpt);
			idMap.put(origPackageVersionPartyTemplateId, pvpt.getId()); // we'll also map our package party ids
			
			// Map the package document and button message as needed for the override values in the party templates
			if ( pvpt.hasOverridePackageDocument() )
			{
		    	Document packageDocument = findDocument(pvpt._importedOverridePackageDocumentName,defaultSearchLibrary,allSearchableLibraries);
		    	if ( packageDocument != null )
		    		pvpt.setOverridePackageDocumentId(packageDocument.getId());
			}
			if ( pvpt.hasOverrideButtonMessage() )
			{
		    	ButtonMessage buttonMessage = findButtonMessage(pvpt._importedOverrideButtonMessageName,defaultSearchLibrary,allSearchableLibraries);
		    	if ( buttonMessage != null )
		    		pvpt.setOverrideButtonMessageId(buttonMessage.getId());
			}
		}
		
		// Set up the package+disclosure page
    	EsfName packageDocumentEsfName = new EsfName(element.getChildText("packageDocumentEsfName", ns));
    	Document packageDocument = findDocument(packageDocumentEsfName,defaultSearchLibrary,allSearchableLibraries);
    	if ( packageDocument != null )
    		setPackageDocumentId(packageDocument.getId());
    	
    	// Set up the button messages
    	EsfName buttonMessageEsfName = new EsfName(element.getChildText("buttonMessageEsfName", ns));
    	ButtonMessage buttonMessage = findButtonMessage(buttonMessageEsfName,defaultSearchLibrary,allSearchableLibraries);
    	if ( buttonMessage != null )
    		setButtonMessageId(buttonMessage.getId());
    	
    	List<Element> packageVersionReportFieldElements = element.getChildren("PackageVersionReportField",ns);
    	for( Element packageVersionReportFieldElement : packageVersionReportFieldElements ) 
    	{
    		PackageVersionReportField pvrf = PackageVersionReportField.Manager.createFromJDOM(packageVersionReportFieldElement, getId(), idMap);
    		packageVersionReportFieldList.add(pvrf);
    	}
    	
    	// And lastly, set up package programming
    	Element packageProgrammingElement = element.getChild("PackageProgramming",ns2011);
    	if ( packageProgrammingElement != null ) 
    	{
    		// this will either be our empty programming or a new one if none existed before
    		PackageProgramming pp = getPackageProgramming(); 
    		pp.importFromJDOM(packageProgrammingElement,idMap);
    	}
    }
    
	Document findDocument(EsfName documentEsfName, Library defaultSearchLibrary, Collection<Library> allSearchableLibraries) 
	{
		Document doc = Document.Manager.getByName(defaultSearchLibrary, documentEsfName);
		if ( doc != null )
			return doc;
		
		for( Library lib : allSearchableLibraries ) 
		{
			doc = Document.Manager.getByName(lib, documentEsfName);
			if ( doc != null )
				return doc;
		}
		
		return null;
	}
	
	ButtonMessage findButtonMessage(EsfName buttonMessageEsfName, Library defaultSearchLibrary, Collection<Library> allSearchableLibraries) {
		ButtonMessage bm = ButtonMessage.Manager.getByName(defaultSearchLibrary, buttonMessageEsfName);
		if ( bm != null )
			return bm;
		
		for( Library lib : allSearchableLibraries ) {
			bm = ButtonMessage.Manager.getByName(lib, buttonMessageEsfName);
			if ( bm != null )
				return bm;
		}
		
		return null;
	}
	

    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<PackageVersion xmlns=\"").append(XmlUtil.getXmlNamespace2013()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <packageId>").append(packageId.toXml()).append("</packageId>\n");
        buf.append(" <version>").append(version).append("</version>\n");
        buf.append(" <createdTimestamp>").append(createdTimestamp.toXml()).append("</createdTimestamp>\n");
       	buf.append(" <createdByUserId>").append(createdByUserId.toXml()).append("</createdByUserId>\n");
        buf.append(" <lastUpdatedTimestamp>").append(lastUpdatedTimestamp.toXml()).append("</lastUpdatedTimestamp>\n");
       	buf.append(" <lastUpdatedByUserId>").append(lastUpdatedByUserId.toXml()).append("</lastUpdatedByUserId>\n");
       	buf.append(" <packageDocumentId>").append(packageDocumentId.toXml()).append("</packageDocumentId>\n");
       	buf.append(" <packageDocumentEsfName>").append(getPackageDocument().getEsfName().toXml()).append("</packageDocumentEsfName>\n");
       	buf.append(" <buttonMessageId>").append(buttonMessageId.toXml()).append("</buttonMessageId>\n");
       	buf.append(" <buttonMessageEsfName>").append(getButtonMessage().getEsfName().toXml()).append("</buttonMessageEsfName>\n");
        for( EsfUUID documentId : documentIdList )
        {
        	Document document = Document.Manager.getById(documentId);
        	if ( document != null )
        	{
               	buf.append(" <document>");
               	buf.append("   <id>").append(documentId.toXml()).append("</id>\n");
               	buf.append("   <esfname>").append(document.getEsfName().toXml()).append("</esfname>\n");
               	buf.append(" </document>\n");
        	}
        }
        for( PackageVersionPartyTemplate pvpt : packageVersionPartyTemplateList )
        	pvpt.appendXml(buf);

        for( PackageVersionReportField pvrf : packageVersionReportFieldList )
        	pvrf.appendXml(buf);
        
       	if ( programmingBlobId != null )
       		getPackageProgramming().appendXml(buf);
       	
        buf.append("</PackageVersion>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 85 + 401 + id.getEstimatedLengthXml() + packageId.getEstimatedLengthXml();
        len += 4; // version
        len += createdTimestamp.getEstimatedLengthXml() + createdByUserId.getEstimatedLengthXml() + lastUpdatedTimestamp.getEstimatedLengthXml() + lastUpdatedByUserId.getEstimatedLengthXml();
        len += packageDocumentId.getEstimatedLengthXml() + getPackageDocument().getEsfName().getEstimatedLengthXml() + buttonMessageId.getEstimatedLengthXml() + getButtonMessage().getEsfName().getEstimatedLengthXml();
        if ( programmingBlobId != null )
        	len += getPackageProgramming().getEstimatedLengthXml();
        for( EsfUUID documentId : documentIdList )
        {
        	Document document = Document.Manager.getById(documentId);
        	if ( document != null )
        		len += 25 + 36 + documentId.getEstimatedLengthXml() + document.getEsfName().getEstimatedLengthXml();
        }
        for( PackageVersionPartyTemplate pvpt : packageVersionPartyTemplateList )
        	len += pvpt.getEstimatedLengthXml();
        for( PackageVersionReportField pvrf : packageVersionReportFieldList )
        	len += pvrf.getEstimatedLengthXml();
        return len; 
    }

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; packageId: " + packageId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( user != null )
        		lastUpdatedByUserId = user.getId();
        	lastUpdatedTimestamp = new EsfDateTime();
        	
        	// We only save if we have them loaded (if we have them, but not loaded, then no need save them)
    		if ( packageProgramming != null ) 
    			packageProgramming.save(con);
        	
            if ( doInsert() )
            {
            	if ( user != null )
            		createdByUserId = user.getId();
            	createdTimestamp = lastUpdatedTimestamp;
            	
            	if ( documentIdList != null && documentIdList.size() > 0 )
            	{
                	stmt = new EsfPreparedStatement(con,"INSERT INTO esf_package_version_document (package_version_id,library_document_id,document_number) VALUES (?,?,?)");
                    stmt.set(id);
                    int order = 1;
            		for( EsfUUID docId : documentIdList )
            		{
                        stmt.set(2,docId);
                        stmt.set(3,order++);
                        stmt.executeUpdate();
            		}
                	stmt.close();
            	}
            	
            	// Insert our package party templates
            	short order = 1;
            	for(PackageVersionPartyTemplate partyTemplate : packageVersionPartyTemplateList)
            	{
            		partyTemplate.setProcessOrder(order++);
            		partyTemplate.save(con);
            	}
            	    
            	// Insert our package report fields
            	order = 1;
            	for(PackageVersionReportField reportField : packageVersionReportFieldList)
            	{
            		reportField.setFieldOrder(order++);
            		reportField.save(con);
            	}
            	    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_package_version " +
                	"(id,package_id,version,created_timestamp,created_by_user_id,last_updated_timestamp,last_updated_by_user_id,package_document_id,button_message_id,programming_blob_id,num_documents,num_parties) " +
                	"VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(packageId);
                stmt.set(version);
                stmt.set(createdTimestamp);
                stmt.set(createdByUserId);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(packageDocumentId);
                stmt.set(buttonMessageId);
                stmt.set(programmingBlobId);
                stmt.set(getNumDocuments());
                stmt.set(getNumPackageParties());
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; packageId: " + packageId + "; version: " + version);
                    return false;
                }
                
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this);
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new package version id: " + id + "; packageId: " + packageId + "; version: " + version); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new package version id: " + id + "; packageId: " + packageId + "; version: " + version);
                }
                
                return true;
            }
            
            if ( hasChanged() )
            {
            	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_package_version_document WHERE package_version_id=?");
                stmt.set(id);
                stmt.executeUpdate();
            	stmt.close();
               	if ( documentIdList.size() > 0 )
            	{
                	stmt = new EsfPreparedStatement(con,"INSERT INTO esf_package_version_document (package_version_id,library_document_id,document_number) VALUES (?,?,?)");
                    stmt.set(id);
                    int order = 1;
            		for( EsfUUID docId : documentIdList )
            		{
                        stmt.set(2,docId);
                        stmt.set(3,order++);
                        stmt.executeUpdate();
            		}
                	stmt.close();
            	}
            	
            	if ( removedPackageVersionPartyTemplateList != null )
            	{
            		for( PackageVersionPartyTemplate template : removedPackageVersionPartyTemplateList )
            			template.delete(con,null,user);
            		removedPackageVersionPartyTemplateList.clear();
            	}
            	
            	if ( removedPackageVersionReportFieldList != null )
            	{
            		for( PackageVersionReportField reportField : removedPackageVersionReportFieldList )
            			reportField.delete(con,null);
            		removedPackageVersionReportFieldList.clear();
            	}
            	
            	// Update our package party templates
            	short order = 1;
            	for(PackageVersionPartyTemplate partyTemplate : packageVersionPartyTemplateList)
            	{
            		partyTemplate.setProcessOrder(order++);
            		partyTemplate.save(con);
            	}
            	
               	// Update our package report fields
            	order = 1;
            	for(PackageVersionReportField reportField : packageVersionReportFieldList)
            	{
            		reportField.setFieldOrder(order++);
            		reportField.save(con);
            	}
            	
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, 
                		"UPDATE esf_package_version SET last_updated_timestamp=?,last_updated_by_user_id=?,package_document_id=?,button_message_id=?,programming_blob_id=?,num_documents=?,num_parties=? WHERE id=?"
                						   		);
                stmt.set(lastUpdatedTimestamp);
                stmt.set(lastUpdatedByUserId);
                stmt.set(packageDocumentId);
                stmt.set(buttonMessageId);
                stmt.set(programmingBlobId);
                stmt.set(getNumDocuments());
                stmt.set(getNumPackageParties());
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for package version id: " + id + "; packageId: " + packageId + "; version: " + version);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated package version id: " + id + "; packageId: " + packageId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated package version id: " + id + "; packageId: " + packageId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; packageId: " + packageId + "; version: " + version + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
    public synchronized boolean delete(final Connection con, final Errors errors, final User user)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id + "; packageId: " + packageId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
    
        // Let's do some referential integrity checks to be sure it's not in use in the transaction definition.
        if ( ! checkReferential(con,errors) )
            return false;

        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of package version that was pending an INSERT id: " + id + "; packageId: " + packageId + "; version: " + version);
            objectDeleted();
            Manager.cache.remove(this);
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
        	// If we have a programming blob id and it's been saved, delete it
        	if ( programmingBlobId != null && (packageProgramming == null || packageProgramming.doUpdate()) )
        		getBlobDb().delete(con, programmingBlobId);
        	
        	// Delete the document info we maintain for the package version
        	for(PackageVersionPartyTemplate partyTemplate : packageVersionPartyTemplateList)
        		partyTemplate.delete(con,errors,user);
        	
        	// Delete the report fields we maintain for the package version
        	for(PackageVersionReportField reportField : packageVersionReportFieldList)
        		reportField.delete(con,errors);
        	
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_package_version_document WHERE package_version_id=?");
            stmt.set(id);
            stmt.executeUpdate();
        	stmt.close();
        	
            // Delete the package version itself
        	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_package_version WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            objectDeleted();
            Manager.cache.remove(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Deleted package version id: " + id + "; packageId: " + packageId + "; version: " + version); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted package version id: " + id + "; packageId: " + packageId + "; version: " + version);
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on id: " + id + "; packageId: " + packageId + "; version: " + version + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors, final User user)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors,user);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public synchronized boolean delete(final User user)
    {
        Errors nullErrors = null;
        return delete(nullErrors,user);
    }
    public synchronized boolean delete()
    {
        User nullUser = null;
        return delete(nullUser);
    }
        
   protected boolean checkReferential(final Connection con, final Errors errors)
	    throws SQLException
	{
	    EsfPreparedStatement stmt = null;
	    	    
	    try
	    {
	        stmt = new EsfPreparedStatement( con, "SELECT COUNT(*) FROM esf_transaction WHERE package_version_id=?" );
	        stmt.set(id);
	        EsfResultSet rs = stmt.executeQuery();
	        if ( rs.next() )
	        {
	        	int count = rs.getInt();
	        	if ( count > 0 )
	        	{
		        	errors.addError("The package version is still referenced in " + count + " transaction(s).");
		        	return false;
	        	}
	        }

	        return true;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}

   	public static class Manager
   	{
   		private static UUIDCacheReadOptimized<PackageVersion> cache = new UUIDCacheReadOptimized<PackageVersion>();

   		static List<EsfUUID> getAllDocumentIdsByPackageVersion(Connection con, EsfUUID pkgVersionId) 
   			throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<EsfUUID> docIdList = new LinkedList<EsfUUID>();

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT library_document_id FROM esf_package_version_document WHERE package_version_id=? ORDER BY document_number"
   	        									);
   	        	stmt.set(pkgVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID documentId = rs.getEsfUUID();
	            	docIdList.add(documentId);
	            }
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        return docIdList;
  		}

   		public static final PackageVersion getById(Connection con, EsfUUID pkgVersionId) throws SQLException
   		{
   			return getById(con,pkgVersionId,true);
   		}
   		public static PackageVersion getById(Connection con, EsfUUID pkgVersionId, boolean checkCache) throws SQLException
   		{
   			if ( checkCache )
   			{
   	   			PackageVersion pkgVer = cache.getById(pkgVersionId);
   	   			if ( pkgVer != null )
   	   				return pkgVer;
   			}

   			EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT package_id,version,created_by_user_id,last_updated_by_user_id,created_timestamp,last_updated_timestamp,package_document_id,button_message_id,programming_blob_id " +
   	        			"FROM esf_package_version WHERE id = ?"
   	        									);
   	        	stmt.set(pkgVersionId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID packageId = rs.getEsfUUID();
		            int version = rs.getInt();
		            EsfUUID createdByUserId = rs.getEsfUUID();
		            EsfUUID lastUpdatedByUserId = rs.getEsfUUID();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfDateTime lastUpdatedTimestamp = rs.getEsfDateTime();
		            EsfUUID packageDocumentId = rs.getEsfUUID();
		            EsfUUID buttonMessageId = rs.getEsfUUID();
		            EsfUUID programmingBlobId = rs.getEsfUUID();
		           
		            List<EsfUUID> docIdList = getAllDocumentIdsByPackageVersion(con,pkgVersionId);
		            
		            List<PackageVersionPartyTemplate> partyTemplateList = PackageVersionPartyTemplate.Manager.getAll(pkgVersionId);
		            
		            List<PackageVersionReportField> reportFieldList = PackageVersionReportField.Manager.getAll(pkgVersionId);
		            
		            PackageVersion pkgVer = new PackageVersion(pkgVersionId,packageId,version,
		            							createdTimestamp,createdByUserId,lastUpdatedTimestamp,lastUpdatedByUserId,
		            							packageDocumentId,buttonMessageId,docIdList,partyTemplateList,reportFieldList,programmingBlobId);
		            pkgVer.setLoadedFromDb();
		            cache.add(pkgVer);
		            
	   	            return pkgVer;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + pkgVersionId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + pkgVersionId + "; failed to find the package version");
   	        return null; 
  		}
   		
   		public static PackageVersion getById(EsfUUID pkgVersionId)
   		{
   			PackageVersion pkgVer = cache.getById(pkgVersionId);
   			if ( pkgVer != null )
   				return pkgVer;
   	   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	pkgVer = getById(con,pkgVersionId,false);
   	        	if ( pkgVer != null ) 
   	        	{
		            con.commit();
	   	        	return pkgVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		public static PackageVersion getByVersion(Connection con, EsfUUID packageId, int version) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;

   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_package_version WHERE package_id=? AND version=?" );
   	        	stmt.set(packageId);
   	        	stmt.set(version);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID pkgVersionId = rs.getEsfUUID();
	       			return getById(con,pkgVersionId);
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getByVersion() - packageId: " + packageId + "; version: " + version);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getByVersion() - packageId: " + packageId + "; version: " + version + "; failed to find the package version");
   	        return null; 
  		}
   		
   		public static PackageVersion getByVersion(EsfUUID packageId, int version)
   		{
   			// While not always true, if the version is 1, it's possible this is a new object that's not been saved yet so we can't find the id in the DB, so we'll check our cache first just in case
   			if ( version == 1 )
   			{
   				for( EsfUUID id : cache.getAllIds() )
   				{
   					PackageVersion pkgVer = cache.getById(id);
   					if ( pkgVer.getPackageId().equals(packageId) && pkgVer.getVersion() == version )
   						return pkgVer;
   				}
   			}

   			ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	PackageVersion pkgVer = getByVersion(con,packageId,version);
   	        	if ( pkgVer != null ) 
   	        	{
		            con.commit();
	   	        	return pkgVer;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   	    
   		public static List<PackageVersion> getAllByPackageId(Connection con, EsfUUID packageId)
			throws SQLException
		{
	        EsfPreparedStatement stmt = null;
	        
			LinkedList<PackageVersion> list = new LinkedList<PackageVersion>();
			
	        try
	        {
	        	stmt = new EsfPreparedStatement( con, "SELECT id FROM esf_package_version WHERE package_id = ?" );
	        	stmt.set(packageId);
	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID pkgVersionId = rs.getEsfUUID();

	            	PackageVersion pkgVer = getById(con,pkgVersionId);
	       			if ( pkgVer != null )
	       				list.add(pkgVer);
	            }
	        }
	        catch( SQLException e )
	        {
   	        	_logger.sqlerr(e,"Manager.getAllByPackageId(con) - packageId: " + packageId);
   	        	throw e;
	        }
	        finally
	        {
	            cleanupStatement(stmt);
	        }

	        return list; 
		}
   		
   		public static List<PackageVersion> getAllByPackageId(EsfUUID packageId)
   		{
   			ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	List<PackageVersion> list = getAllByPackageId(con,packageId);
   	        	if ( list != null ) 
   	        	{
		            con.commit();
	   	        	return list;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
	    
   		public static Collection<PackageVersion> getAllThatFeedReportFieldTemplate(ReportFieldTemplate reportFieldTemplate)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<PackageVersion> list = new LinkedList<PackageVersion>();
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT PV.id " +
   	        			"FROM esf_package P, esf_package_version PV, esf_package_version_report_field PVRF " +
   	        			"WHERE PVRF.report_field_template_id=? AND PVRF.package_version_id=PV.id AND PV.package_id=P.id " +
   	        			"ORDER BY lower(P.path_name),version"
   	        									);
   	        	stmt.set(reportFieldTemplate.getId());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	   	            
	            	PackageVersion pv = getById(id);
	   	            if ( pv != null )
	   	            	list.add(pv);
	            }
	            
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAllThatFeedReportFieldTemplate() - reportFieldTemplate.id: " + reportFieldTemplate.getId());
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		
   	    public static PackageVersion createTest(Package pkg, EsfUUID packageDocumentId, EsfUUID buttonMessageId, User createdBy)
   	    {
   	    	if ( pkg == null || createdBy == null )
   	    		return null;
   	    	
   	    	if ( packageDocumentId == null || packageDocumentId.isNull() )
   	    		packageDocumentId = Document.Manager.getStandardPackageDocument().getId();
   	    	
   	    	if ( buttonMessageId == null || buttonMessageId.isNull() )
   	    		buttonMessageId = ButtonMessage.Manager.getDefault().getId();
   	    	
   	    	PackageVersion newPkgVersion = new PackageVersion(pkg,packageDocumentId,buttonMessageId,createdBy);
   	    	cache.add(newPkgVersion);
   	    	return newPkgVersion;
   	    }
   	    public static PackageVersion createTest(Package pkg, User createdBy)
   	    {
   	    	return createTest(pkg,null,null,createdBy);
   	    }

   	    public static PackageVersion createForTestDocument(Package pkg, User createdBy)
   	    {
   	    	return createTest(pkg,createdBy);
   	    }
   	 
   	    public static PackageVersion createLike(Package pkg, PackageVersion likePkgVersion, User createdBy)
   	    {
   	    	if ( pkg == null || likePkgVersion == null || createdBy == null )
   	    		return null;
   	    	
   	    	PackageVersion newPkgVersion = createTest(pkg,likePkgVersion.getPackageDocumentId(),likePkgVersion.getButtonMessageId(),createdBy);
   	    	
   	    	newPkgVersion.documentIdList.clear();
   	    	for( EsfUUID did : likePkgVersion.getDocumentIdList() ) 
   	    	{
   	    		newPkgVersion.documentIdList.add( did );
   	    	}
   	    	
   	    	TreeMap<EsfUUID,EsfUUID> partyIdMapping = new TreeMap<EsfUUID,EsfUUID>();
   	    	newPkgVersion.packageVersionPartyTemplateList.clear();
   	    	for( PackageVersionPartyTemplate pvpt : likePkgVersion.getPackageVersionPartyTemplateList() ) 
   	    	{
   	    		PackageVersionPartyTemplate likePvpt = PackageVersionPartyTemplate.Manager.createLike(newPkgVersion.getId(), pvpt);
   	    		partyIdMapping.put(pvpt.getId(), likePvpt.getId()); // save the party mapping for later
   	    		newPkgVersion.packageVersionPartyTemplateList.add( likePvpt );
   	    	}
   	    	
   	    	newPkgVersion.packageVersionReportFieldList.clear();
   	    	for( PackageVersionReportField reportField : likePkgVersion.getPackageVersionReportFieldList() ) 
   	    	{
   	    		PackageVersionReportField likeReportField = PackageVersionReportField.Manager.createLike(newPkgVersion.getId(), reportField);
   	    		newPkgVersion.packageVersionReportFieldList.add( likeReportField );
   	    	}
   	    	
   	    	if ( likePkgVersion.hasPackageProgramming() )
   	    	{
   	    		newPkgVersion.packageProgramming = PackageProgramming.Manager.createLike(likePkgVersion.getPackageProgramming(),partyIdMapping);
   	    		newPkgVersion.programmingBlobId = newPkgVersion.packageProgramming.getId();
   	    	}
   	    	
   	    	return newPkgVersion;
   	    }

   	    /**
   	     * Returns the total number of package version objects in the cache
   	     * @return the total number of package versions in the cache
   	     */
   	    public static int getNumCached()
   	    {
   	    	return cache.size();
   	    }
   	    
   	    /**
   	     * Finds all package versions that have not been accessed within the specified number of minutes.
   	     * @param numMinutes the number of minutes to use for the check (positive and negative values are treated the same to find the early datetime). Zero is the same as one.
   	     * @return the number flushed from the cache
   	     */
   		public static int flushNotAccessedWithinMinutes(int numMinutes)
   	    {
   			return cache.flushNotAccessedWithinMinutes(numMinutes);
   	    }
   		
   		public static void clearCache()
   		{
   			cache.clear();
   		}

   	} // Manager
   	
}