// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;

/**
* FileInfo is a data-only object that holds information about a File.
* It's not a mutable object (you can't save/delete it), just for lists of files.
* 
* @author Yozons, Inc.
*/
public class FileInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<FileInfo>, java.io.Serializable
{
	private static final long serialVersionUID = -2218224417267573532L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(FileInfo.class);

    protected EsfUUID id;
    protected EsfName esfname;
    protected String  displayName;
    protected String  description;
    protected String  status;
    protected int	  productionVersion;
    protected int	  testVersion;

    /**
     * This creates a new FileInfo using the source object.
     * @param file the file to use as the source
     */
    protected FileInfo(File file)
    {
    	this.id = file.getId();
    	this.esfname = file.getEsfName();
    	this.displayName = file.getDisplayName();
    	this.description = file.getDescription();
    	this.status = file.getStatus();
    	this.productionVersion = file.getProductionVersion();
    	this.testVersion = file.getTestVersion();
    }
    
    /**
     * This creates a FileInfo object from data retrieved from the DB.
     */
    protected FileInfo(EsfUUID id, EsfName esfname, String displayName, String description, String status, int productionVersion, int testVersion)
    {
    	this.id = id;
    	this.esfname = esfname;
    	this.displayName = displayName;
    	this.description = description;
    	this.status = status;
    	this.productionVersion = productionVersion;
    	this.testVersion = testVersion;
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    
    public String getDisplayName()
    {
        return displayName;
    }
    public boolean hasDisplayName()
    {
    	return EsfString.isNonBlank(displayName);
    }
    public String getDisplayOrEsfName()
    {
        return hasDisplayName() ? getDisplayName() : getEsfName().toString();
    }

    public String getDescription()
    {
        return description;
    }
    public boolean hasDescription()
    {
        return EsfString.isNonBlank(description);
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }

    public int getProductionVersion()
    {
    	return productionVersion;
    }
    
    public int getTestVersion()
    {
    	return testVersion;
    }

    public boolean hasProductionVersion()
    {
    	return productionVersion > 0;
    }

    public boolean hasTestVersion()
    {
    	return testVersion > productionVersion;
    }

    public EsfUUID getId()
    {
        return id;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof FileInfo )
            return getId().equals(((FileInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(FileInfo o)
    {
    	return getEsfName().compareTo(o.getEsfName());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all FileInfo objects in the specified container.
   	     * @return the Collection of FileInfo found ordered by esfname.
   	     */
   	    public static Collection<FileInfo> getAll(EsfUUID containerId)
   	    {
   	    	LinkedList<FileInfo> list = new LinkedList<FileInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id,esfname,display_name,description,status,production_version,test_version " +
   	    	            "FROM esf_library_file " + 
   	    	            "WHERE container_id=? " +
   	    	            "ORDER BY lower(esfname) ASC"
   	            						   );
   	        	stmt.set(containerId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            EsfName esfname = rs.getEsfName();
		            String displayName = rs.getString();
		            String description = rs.getString();
		            String  stat = rs.getString();
		            int productionVersion = rs.getInt();
		            int testVersion = rs.getInt();
		            
		            FileInfo FileInfo = new FileInfo(id,esfname,displayName,description,stat,productionVersion,testVersion);
		            FileInfo.setLoadedFromDb();
		            list.add(FileInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - containerId: " + containerId + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - containerId: " + containerId + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static FileInfo createNew(EsfUUID containerId)
   	    {
   	    	if ( containerId == null )
   	    		return null;
   	    	
   			File file = File.Manager.createNew(containerId);
   	    	return new FileInfo(file);
   	    }
   	    
   	    public static FileInfo createLike(File likeFile, EsfName newName)
   	    {
   	    	if ( likeFile == null || newName == null || ! newName.isValid() )
   	    		return null;

   	    	File newFile = File.Manager.createLike(likeFile.getContainerId(), likeFile, newName);
   	    	return new FileInfo(newFile);
   	    }
   	    
   	    public static FileInfo createFromSource(File file)
   	    {
   	    	if ( file == null )
   	    		return null;
   	    	
   	    	FileInfo FileInfo = new FileInfo(file);
   	    	FileInfo.setLoadedFromDb();

   			return FileInfo;
   	    }
   	    
   	} // Manager
   	
}