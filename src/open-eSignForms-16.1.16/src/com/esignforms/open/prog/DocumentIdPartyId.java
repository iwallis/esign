// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.util.List;
import java.util.Set;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;

/**
* DocumentIdPartyId is just a utility class that keeps a document id and party id together.
* The document id is required, but the lack of party id typically means "any party id".
* The names are entirely optional mechanisms to cache their respective names.
* 
* @author Yozons, Inc.
*/
public class DocumentIdPartyId implements java.io.Serializable
{
	private static final long serialVersionUID = 5959814346286411657L;

	public static final EsfUUID allLatestId = new EsfUUID("1-1-1-1-1"); // flag value used for document and party selection when "All latest" is desired.

	public EsfUUID documentId;
	public EsfUUID partyId;
	
	public EsfName documentName; // optional for caching matching name
	public EsfName partyName; //optional for caching matching name
	
	public DocumentIdPartyId(EsfUUID documentId, EsfUUID partyId)
	{
		this.documentId = documentId;
		this.partyId = partyId;
		this.documentName = this.partyName = null;
	}
	
	public DocumentIdPartyId(EsfUUID documentId, EsfName documentName, EsfUUID partyId, EsfName partyName)
	{
		this.documentId = documentId;
		this.partyId = partyId;
		this.documentName = documentName;
		this.partyName = partyName;
	}
	
	public EsfUUID getDocumentId() { return documentId; }
	public EsfUUID getPartyId() { return partyId; }
	public boolean hasPartyId() { return partyId != null && ! partyId.isNull(); }
	public EsfName getDocumentName() { return documentName; }
	public boolean hasDocumentName() { return documentName != null && documentName.isValid(); }
	public EsfName getPartyName() { return partyName; }
	public boolean hasPartyName() { return partyName != null && partyName.isValid(); }
	
	@Override
	public boolean equals(Object o) 
	{	
		if ( o instanceof DocumentIdPartyId )
		{
			DocumentIdPartyId other = (DocumentIdPartyId)o;
			if ( documentId.equals(other.documentId) ) 
			{
				if ( (partyId == null || partyId.isNull())  && 
					 (other.partyId == null || other.partyId.isNull() ) 
				   )
					return true; // both are null
				if ( (partyId == null || partyId.isNull())  && 
						 (other.partyId != null && ! other.partyId.isNull() ) 
				   )
					return false; // the first is null, but the second is not
				return partyId.equals(other.partyId);
			} 
		}
		
		return false;
	}
	
	@Override
	public int hashCode() 
	{
		return documentId.hashCode() + ( partyId == null || partyId.isNull() ? 0 : partyId.hashCode());
	}
	
	public static Set<DocumentIdPartyId> fixupSelectedValues(Set<DocumentIdPartyId> selectedValues, List<DocumentIdPartyId> allDocAndParty) 
	{
		for( DocumentIdPartyId dp : selectedValues ) 
		{
			if ( allLatestId.equals(dp.getDocumentId()) && allLatestId.equals(dp.getPartyId()) ) 
			{
				Set<DocumentIdPartyId> fixedSelectedValues = new java.util.HashSet<DocumentIdPartyId>();
				for( DocumentIdPartyId checkdp : allDocAndParty ) 
				{
					if ( (DocumentIdPartyId.allLatestId.equals(checkdp.getDocumentId()) && DocumentIdPartyId.allLatestId.equals(checkdp.getPartyId())) || checkdp.hasPartyId() )
						;
					else
						fixedSelectedValues.add(checkdp);
				}
				return fixedSelectedValues;
			}
		}
		
		return selectedValues;
	}

}
