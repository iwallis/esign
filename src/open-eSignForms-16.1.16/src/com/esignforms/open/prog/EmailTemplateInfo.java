// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;

/**
* EmailTemplateInfo is a data-only object that holds information about an email that can be sent.
* It's not a mutable object (you can't save/delete it), just for lists of email templates.
* 
* @author Yozons, Inc.
*/
public class EmailTemplateInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<EmailTemplateInfo>, java.io.Serializable
{
 	private static final long serialVersionUID = -3495993091793341928L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(EmailTemplateInfo.class);

    protected EsfUUID id;
    protected EsfName esfname;
    protected String  description;
    protected String  status;
    protected int	  productionVersion;
    protected int	  testVersion;

    /**
     * This creates a new EmailTemplateInfo using the source object.
     * @param emailTemplate the emailTemplate to use as the source
     */
    protected EmailTemplateInfo(EmailTemplate emailTemplate)
    {
    	this.id = emailTemplate.getId();
    	this.esfname = emailTemplate.getEsfName();
    	this.description = emailTemplate.getDescription();
    	this.status = emailTemplate.getStatus();
    	this.productionVersion = emailTemplate.getProductionVersion();
    	this.testVersion = emailTemplate.getTestVersion();
    }
    
    /**
     * This creates a EmailTemplateInfo object from data retrieved from the DB.
     */
    protected EmailTemplateInfo(EsfUUID id, EsfName esfname, String description, String status, int productionVersion, int testVersion)
    {
    	this.id = id;
    	this.esfname = esfname;
    	this.description = description;
    	this.status = status;
    	this.productionVersion = productionVersion;
    	this.testVersion = testVersion;
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }

    public int getProductionVersion()
    {
    	return productionVersion;
    }
    
    public int getTestVersion()
    {
    	return testVersion;
    }

    public boolean hasProductionVersion()
    {
    	return productionVersion > 0;
    }

    public boolean hasTestVersion()
    {
    	return testVersion > productionVersion;
    }

    public EsfUUID getId()
    {
        return id;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof EmailTemplateInfo )
            return getId().equals(((EmailTemplateInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(EmailTemplateInfo o)
    {
    	return getEsfName().compareTo(o.getEsfName());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all EmailTemplateInfo objects in the specified container id.
   	     * @return the Collection of EmailTemplateInfo found ordered by esfname.
   	     */
   	    public static Collection<EmailTemplateInfo> getAll(EsfUUID containerId)
   	    {
   	    	LinkedList<EmailTemplateInfo> list = new LinkedList<EmailTemplateInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id,esfname,description,status,production_version,test_version " +
   	    	            "FROM esf_library_email_template " + 
   	    	            "WHERE container_id=? " +
   	    	            "ORDER BY lower(esfname) ASC"
   	            						   );
   	        	stmt.set(containerId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            EsfName esfname = rs.getEsfName();
		            String description = rs.getString();
		            String  stat = rs.getString();
		            int productionVersion = rs.getInt();
		            int testVersion = rs.getInt();
		            
		            EmailTemplateInfo propertysetInfo = new EmailTemplateInfo(id,esfname,description,stat,productionVersion,testVersion);
		            propertysetInfo.setLoadedFromDb();
		            list.add(propertysetInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - containerId: " + containerId + "; found: " + list.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - containerId: " + containerId + "; found: " + list.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    public static EmailTemplateInfo createNew(EsfUUID containerId)
   	    {
   	    	if ( containerId == null )
   	    		return null;
   	    	
   			EmailTemplate emailTemplate = EmailTemplate.Manager.createNew(containerId);
   	    	return new EmailTemplateInfo(emailTemplate);
   	    }
   	    
   	    public static EmailTemplateInfo createLike(EmailTemplate likeEmailTemplate, EsfName newName)
   	    {
   	    	if ( likeEmailTemplate == null || newName == null || ! newName.isValid() )
   	    		return null;

   	    	EmailTemplate newEmailTemplate = EmailTemplate.Manager.createLike(likeEmailTemplate.getContainerId(), likeEmailTemplate, newName);
   	    	return new EmailTemplateInfo(newEmailTemplate);
   	    }
   	    
   	    public static EmailTemplateInfo createFromSource(EmailTemplate emailTemplate)
   	    {
   	    	if ( emailTemplate == null )
   	    		return null;
   	    	
   	    	EmailTemplateInfo docInfo = new EmailTemplateInfo(emailTemplate);
   	    	docInfo.setLoadedFromDb();

   			return docInfo;
   	    }
   	    
   	} // Manager
   	
}