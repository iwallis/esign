// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDecimal;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;

/**
* ReportFieldValue holds the a ReportFieldTemplate and associated value.
* 
* @author Yozons, Inc.
*/
public class ReportFieldValue
	extends com.esignforms.open.db.DatabaseObject 
{
	private static final long serialVersionUID = -6501812222573468601L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ReportFieldValue.class);
    
    protected ReportFieldTemplate reportFieldTemplate;
    protected EsfValue value;
    protected EsfValue[] values;

    /**
     * This creates a ReportFieldValue object from data retrieved from the DB.
     */
    protected ReportFieldValue(ReportFieldTemplate reportFieldTemplate, EsfValue value)
    {
    	this.reportFieldTemplate = reportFieldTemplate;
    	this.value = value;
    	this.values = null;
    }
    protected ReportFieldValue(ReportFieldTemplate reportFieldTemplate, EsfValue[] values)
    {
    	this.reportFieldTemplate = reportFieldTemplate;
    	this.value = null;
    	this.values = values;
    }
    
    public ReportFieldTemplate getReportFieldTemplate()
    {
        return reportFieldTemplate;
    }
    
    public boolean hasValueOrValues()
    {
    	return hasValue() || hasValues();
    }
    
    public boolean hasValue()
    {
    	return value != null && ! value.isNull();
    }
    public EsfValue getValue()
    {
        return value;
    }
    
    public boolean hasValues()
    {
    	return values != null && values.length > 0;
    }
    public EsfValue[] getValues()
    {
    	return values;
    }

    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof ReportFieldValue )
        {
        	ReportFieldValue other = (ReportFieldValue)o;
            return getReportFieldTemplate().equals(other.getReportFieldTemplate()) && getValue().equals(other.getValue());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getReportFieldTemplate().hashCode() + getValue().hashCode();
    }
    
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{

   		static EsfString getEsfStringValue(Connection con, EsfUUID transactionId, EsfUUID reportFieldTemplateId) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT field_value FROM esf_tran_report_field_string WHERE transaction_id=? AND report_field_template_id=?" );
   	        	stmt.set(transactionId);
   	        	stmt.set(reportFieldTemplateId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
		            return rs.getEsfString();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getStringValue() - transactionId: " + transactionId + "; reportFieldTemplateId: " + reportFieldTemplateId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getStringValue() - transactionId: " + transactionId + "; reportFieldTemplateId: " + reportFieldTemplateId + "; failed to find the report field value");
   	        return null; 
   		}
   		
   		static EsfInteger getEsfIntegerValue(Connection con, EsfUUID transactionId, EsfUUID reportFieldTemplateId) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT field_value FROM esf_tran_report_field_long WHERE transaction_id=? AND report_field_template_id=?" );
   	        	stmt.set(transactionId);
   	        	stmt.set(reportFieldTemplateId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
		            return new EsfInteger(rs.getLong());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getEsfIntegerValue() - transactionId: " + transactionId + "; reportFieldTemplateId: " + reportFieldTemplateId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getEsfIntegerValue() - transactionId: " + transactionId + "; reportFieldTemplateId: " + reportFieldTemplateId + "; failed to find the report field value");
   	        return null; 
   		}
   		
   		static EsfDecimal getEsfDecimalValue(Connection con, EsfUUID transactionId, EsfUUID reportFieldTemplateId) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT field_value FROM esf_tran_report_field_numeric WHERE transaction_id=? AND report_field_template_id=?" );
   	        	stmt.set(transactionId);
   	        	stmt.set(reportFieldTemplateId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
		            return rs.getEsfDecimal();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getEsfDecimalValue() - transactionId: " + transactionId + "; reportFieldTemplateId: " + reportFieldTemplateId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getEsfDecimalValue() - transactionId: " + transactionId + "; reportFieldTemplateId: " + reportFieldTemplateId + "; failed to find the report field value");
   	        return null; 
   		}
   		
   		static EsfDate getEsfDateValue(Connection con, EsfUUID transactionId, EsfUUID reportFieldTemplateId) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT field_value FROM esf_tran_report_field_date WHERE transaction_id=? AND report_field_template_id=?" );
   	        	stmt.set(transactionId);
   	        	stmt.set(reportFieldTemplateId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
		            return rs.getEsfDate();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getEsfDateValue() - transactionId: " + transactionId + "; reportFieldTemplateId: " + reportFieldTemplateId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getEsfDateValue() - transactionId: " + transactionId + "; reportFieldTemplateId: " + reportFieldTemplateId + "; failed to find the report field value");
   	        return null; 
   		}
   		
   		static List<EsfUUID> getTranFileEsfUUIDValues(Connection con, EsfUUID transactionId, EsfUUID reportFieldTemplateId) throws SQLException
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<EsfUUID> list = new LinkedList<EsfUUID>();
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con, "SELECT tran_file_id FROM esf_tran_report_field_tranfileid WHERE transaction_id=? AND report_field_template_id=?" );
   	        	stmt.set(transactionId);
   	        	stmt.set(reportFieldTemplateId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            	list.add(rs.getEsfUUID());
	   	        return list; 
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getFileEsfUUIDValues() - transactionId: " + transactionId + "; reportFieldTemplateId: " + reportFieldTemplateId);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }
   		}
   		
   		public static List<ReportFieldValue> get(EsfUUID transactionId, List<ReportFieldTemplate> reportFieldTemplateList)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<ReportFieldValue> list = new LinkedList<ReportFieldValue>();
   	        
   	        try
   	        {
   	        	for( ReportFieldTemplate reportFieldTemplate : reportFieldTemplateList )
   	        	{
   	        		EsfValue value;
   	        		if ( reportFieldTemplate.isFieldTypeStringOrRelated() )
   	        			value = getEsfStringValue(con,transactionId,reportFieldTemplate.getId());
   	        		else if ( reportFieldTemplate.isFieldTypeInteger() )
   	        			value = getEsfIntegerValue(con,transactionId,reportFieldTemplate.getId());
   	        		else if ( reportFieldTemplate.isFieldTypeDecimal() )
   	        			value = getEsfDecimalValue(con,transactionId,reportFieldTemplate.getId());
   	        		else if ( reportFieldTemplate.isFieldTypeDate() )
   	        			value = getEsfDateValue(con,transactionId,reportFieldTemplate.getId());
   	        		else if ( reportFieldTemplate.isFieldTypeFile() )
   	        		{
   	        			List<EsfUUID> tranFileIdList = getTranFileEsfUUIDValues(con,transactionId,reportFieldTemplate.getId());
   	        			if ( tranFileIdList == null || tranFileIdList.size() == 0 )
   	        				value = null;
   	        			else
   	        			{
   	        				EsfUUID[] ids = new EsfUUID[tranFileIdList.size()];
   	        				tranFileIdList.toArray(ids);
   	        				ReportFieldValue reportFieldValue = new ReportFieldValue(reportFieldTemplate,ids);
   	    	        		list.add(reportFieldValue);
   	    	        		continue; // special case
   	        			}
   	        		}
   	        		else
   	        			value = null;
   	        		
   	        		ReportFieldValue reportFieldValue = new ReportFieldValue(reportFieldTemplate,value);
   	        		list.add(reportFieldValue);
   	        	}
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.error("Manager.get() transactionId: " + transactionId, e);
   	            pool.rollbackIgnoreException(con,e);
   	            list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		
   		public static List<ReportTemplateReportField> getCustom(EsfUUID transactionId, List<ReportTemplateReportField> reportFieldTemplateList)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<ReportTemplateReportField> list = new LinkedList<ReportTemplateReportField>();
   	        
   	        try
   	        {
   	        	for( ReportTemplateReportField rtrf : reportFieldTemplateList )
   	        	{
   	        		ReportFieldTemplate reportFieldTemplate = rtrf.getReportFieldTemplate();
   	        		EsfValue value;
   	        		if ( reportFieldTemplate.isFieldTypeStringOrRelated() )
   	        			value = getEsfStringValue(con,transactionId,reportFieldTemplate.getId());
   	        		else if ( reportFieldTemplate.isFieldTypeInteger() )
   	        			value = getEsfIntegerValue(con,transactionId,reportFieldTemplate.getId());
   	        		else if ( reportFieldTemplate.isFieldTypeDecimal() )
   	        			value = getEsfDecimalValue(con,transactionId,reportFieldTemplate.getId());
   	        		else if ( reportFieldTemplate.isFieldTypeDate() )
   	        			value = getEsfDateValue(con,transactionId,reportFieldTemplate.getId());
   	        		else if ( reportFieldTemplate.isFieldTypeFile() )
   	        		{
   	        			List<EsfUUID> tranFileIdList = getTranFileEsfUUIDValues(con,transactionId,reportFieldTemplate.getId());
   	        			if ( tranFileIdList == null || tranFileIdList.size() == 0 )
   	        				value = null;
   	        			else
   	        			{
   	        				EsfUUID[] ids = new EsfUUID[tranFileIdList.size()];
   	        				tranFileIdList.toArray(ids);
   	   	   	        		ReportTemplateReportField rtrfClone = rtrf.duplicate();
   	   	   	        		rtrfClone.setFieldValues(ids);
   	   	   	        		list.add(rtrfClone);
   	   	   	        		continue; // special case
   	        			}
   	        		}
   	        		else if ( reportFieldTemplate.isFieldTypeBuiltIn() ) 
   	        			value = null; // we fill built-in values later
   	        		else
   	        			value = null;
   	        		
   	        		ReportTemplateReportField rtrfClone = rtrf.duplicate();
   	        		rtrfClone.setFieldValue(value);
   	        		list.add(rtrfClone);
   	        	}
	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.error("Manager.getCustom() transactionId: " + transactionId, e);
   	            pool.rollbackIgnoreException(con,e);
   	            list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list; 
   		}
   		
   	} // Manager
    
}