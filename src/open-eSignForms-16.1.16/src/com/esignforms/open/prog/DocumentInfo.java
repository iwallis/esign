// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.Collection;
import java.util.LinkedList;

import com.esignforms.open.config.Literals;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;

/**
* DocumentInfo is a data-only object that holds information about a document.
* It's not a mutable object (you can't save/delete it), just for lists of documents.
* 
* @author Yozons, Inc.
*/
public class DocumentInfo
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<DocumentInfo>, java.io.Serializable
{
 	private static final long serialVersionUID = 6725856296255590873L;

 	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DocumentInfo.class);
    
    public enum INCLUDE { ONLY_ENABLED, ONLY_DISABLED, BOTH_ENABLED_AND_DISABLED };

    protected EsfUUID id;
    protected EsfName esfname;
    protected String  displayName;
    protected String  description;
    protected String  status;
    protected int	  productionVersion;
    protected int	  testVersion;

    /**
     * This creates a new DocumentInfo using the source object.
     * @param document the document to use as the source
     */
    protected DocumentInfo(Document document)
    {
    	this.id = document.getId();
    	this.esfname = document.getEsfName();
    	this.displayName = document.getDisplayName();
    	this.description = document.getDescription();
    	this.status = document.getStatus();
    	this.productionVersion = document.getProductionVersion();
    	this.testVersion = document.getTestVersion();
    }
    
    /**
     * This creates a DocumentInfo object from data retrieved from the DB.
     */
    protected DocumentInfo(EsfUUID id, EsfName esfname, String displayName, String description, String status, int productionVersion, int testVersion)
    {
    	this.id = id;
    	this.esfname = esfname;
    	this.displayName = displayName;
    	this.description = description;
    	this.status = status;
    	this.productionVersion = productionVersion;
    	this.testVersion = testVersion;
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    
    public String getDisplayName()
    {
        return displayName;
    }
    
    public String getDescription()
    {
        return description;
    }
    
    public String getStatus()
    {
        return status;
    }
    
    public boolean isEnabled()
    {
        return Literals.STATUS_ENABLED.equals(status);
    }
     
    public boolean isDisabled()
    {
        return Literals.STATUS_DISABLED.equals(status);
    }

    public int getProductionVersion()
    {
    	return productionVersion;
    }
    
    public int getTestVersion()
    {
    	return testVersion;
    }

    public boolean hasProductionVersion()
    {
    	return productionVersion > 0;
    }

    public boolean hasTestVersion()
    {
    	return testVersion > productionVersion;
    }

    public EsfUUID getId()
    {
        return id;
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof DocumentInfo )
            return getId().equals(((DocumentInfo)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(DocumentInfo o)
    {
    	return getEsfName().compareTo(o.getEsfName());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all DocumentInfo objects in the specified library.
   	     * @return the Collection of DocumentInfo found ordered by esfname.
   	     */
   	    public static Collection<DocumentInfo> getAll(Library library, INCLUDE include)
   	    {
   	    	LinkedList<DocumentInfo> list = new LinkedList<DocumentInfo>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id,esfname,display_name,description,status,production_version,test_version " +
   	    	            "FROM esf_library_document " + 
   	    	            "WHERE library_id=? " +
   	    	            ((include == INCLUDE.BOTH_ENABLED_AND_DISABLED) ? "" : "AND status=? " ) +
   	    	            "ORDER BY lower(esfname) ASC"
   	            						   );
   	        	stmt.set(library.getId());
   	        	if ( include == INCLUDE.ONLY_ENABLED )
   	        		stmt.set(Literals.STATUS_ENABLED);
   	        	else if ( include == INCLUDE.ONLY_DISABLED )
   	        		stmt.set(Literals.STATUS_DISABLED);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            EsfName esfname = rs.getEsfName();
		            String displayName = rs.getString();
		            String description = rs.getString();
		            String  stat = rs.getString();
		            int productionVersion = rs.getInt();
		            int testVersion = rs.getInt();
		            
		            DocumentInfo docInfo = new DocumentInfo(id,esfname,displayName,description,stat,productionVersion,testVersion);
		            docInfo.setLoadedFromDb();
		            list.add(docInfo);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - Library: " + library.getPathName() + "; found: " + list.size() + "; include: " + include);
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - Library: " + library.getPathName() + "; found: " + list.size() + " entries added before exception" + "; include: " + include);
   	            pool.rollbackIgnoreException(con,e);
   	        	list.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return list;
   	    }
   	    
   	    /**
   	     * Retrieves all DocumentInfo objects in the specified library.
   	     * @return the Collection of DocumentInfo found ordered by esfname.
   	     */
   	    public static Collection<DocumentInfo> getAll(Library library)
   	    {
   	    	return getAll(library,INCLUDE.BOTH_ENABLED_AND_DISABLED);
   	    }
   	    
   	    public static DocumentInfo createNew(Library library)
   	    {
   	    	if ( library == null )
   	    		return null;
   	    	
   			Document document = Document.Manager.createNew(library.getId());
   	    	return new DocumentInfo(document);
   	    }
   	    
   	    public static DocumentInfo createLike(Document likeDocument, EsfName newName)
   	    {
   	    	if ( likeDocument == null || newName == null || ! newName.isValid() )
   	    		return null;

   	    	Document newDocument = Document.Manager.createLike(likeDocument.getLibraryId(), likeDocument, newName);
   	    	return new DocumentInfo(newDocument);
   	    }
   	    
   	    public static DocumentInfo createFromSource(Document document)
   	    {
   	    	if ( document == null )
   	    		return null;
   	    	
   	    	DocumentInfo docInfo = new DocumentInfo(document);
   	    	docInfo.setLoadedFromDb();

   			return docInfo;
   	    }
   	    
   	} // Manager
   	
    
}