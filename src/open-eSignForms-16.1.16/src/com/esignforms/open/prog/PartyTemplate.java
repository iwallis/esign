// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.prog;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.TreeMap;

import org.jdom2.Element;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.User;
import com.esignforms.open.util.XmlUtil;

/**
* PartyTemplate holds the definition of a single party that accesses a document or is defined in a library.
* 
* @author Yozons, Inc.
*/
public class PartyTemplate
	extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<PartyTemplate>, java.io.Serializable
{
	private static final long serialVersionUID = -8672508526436152862L;

	public static final EsfName ESF_PARTY_NAME_VIEW_ONLY     = new EsfName("ESF_partyname_view_only"); // the party that must view a document to process it
	public static final EsfName ESF_PARTY_NAME_VIEW_OPTIONAL = new EsfName("ESF_partyname_view_optional"); // the party that can view a document, but doesn't have to process it
	public static final EsfName ESF_PARTY_NAME_REPORTS_ACCESS = new EsfName("ESF_reports_access"); // the party used when accessing a transaction for live view/edit from the reports
	
	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PartyTemplate.class);

    protected EsfUUID id;
    protected EsfUUID containerId; // The ID of whatever container (document, library) "owns" this party template.
    protected EsfName esfname;
    protected String displayName;
    protected short processOrder;
    protected EsfUUID todoGroupId; // null if none set; otherwise group that allowed access via To Do for Production transactions
    protected EsfUUID testTodoGroupId; // null if none set; otherwise group that allowed access via To Do for Test transactions
    
    protected List<PartyTemplateFieldTemplate> fieldTemplates;
    protected List<PartyTemplateFieldTemplate> fieldTemplatesToRemoveOnSave;

    /**
     * This creates a PartyTemplate object from data retrieved from the DB.
     */
    protected PartyTemplate(EsfUUID id, EsfUUID containerId, short processOrder, EsfName esfname, String displayName, EsfUUID todoGroupId, EsfUUID testTodoGroupId, 
    						List<PartyTemplateFieldTemplate> fieldTemplates)
    {
    	this.id = id;
    	this.containerId = containerId;
    	this.processOrder = processOrder;
    	this.esfname = esfname;
    	this.displayName = displayName;
    	this.todoGroupId = todoGroupId;
    	this.testTodoGroupId = testTodoGroupId;
    	this.fieldTemplates = fieldTemplates;
    }
    
    // Creates a new party like a given party
    protected PartyTemplate(EsfUUID newContainerId, PartyTemplate likeTemplate)
    {
    	this.id = new EsfUUID();
    	this.containerId = newContainerId;
    	this.processOrder = likeTemplate.processOrder;
    	this.esfname = likeTemplate.esfname;
    	this.displayName = likeTemplate.displayName;
    	this.todoGroupId = likeTemplate.todoGroupId;
    	this.testTodoGroupId = likeTemplate.testTodoGroupId;
    	this.fieldTemplates = new LinkedList<PartyTemplateFieldTemplate>();
    	for( PartyTemplateFieldTemplate ft : likeTemplate.getPartyTemplateFieldTemplates() )
    	{
    		PartyTemplateFieldTemplate newFT = PartyTemplateFieldTemplate.Manager.createLike(id,ft);
    		this.fieldTemplates.add(newFT);
    	}
    }
    
    // Creates a new party with default values
    protected PartyTemplate(EsfUUID newContainerId, EsfName name)
    {
    	this.id = new EsfUUID();
    	this.containerId = newContainerId;
    	this.processOrder = 1;
    	this.esfname = name;
    	this.displayName = name.toString();
    	this.todoGroupId = null;
    	this.testTodoGroupId = null;
    	this.fieldTemplates = new LinkedList<PartyTemplateFieldTemplate>();
    }
    
    public PartyTemplate duplicate()
    {
    	PartyTemplate pt = new PartyTemplate(id, containerId, processOrder, esfname.duplicate(), displayName, todoGroupId, testTodoGroupId, getDuplicatePartyTemplateFieldTemplates());
    	pt.setDatabaseObjectLike(this);
    	return pt;
    }
    
    public EsfUUID getId()
    {
        return id;
    }
    
    public EsfUUID getContainerId()
    {
        return containerId;
    }
    
    public EsfName getEsfName()
    {
        return esfname;
    }
    public void setEsfName(EsfName v)
    {
    	if ( v == null || ! v.isValid() )
    		return;
    	if ( v.hasReservedPrefix() && ! PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS.equals(v) ) // the only reserved name allowed is the reports access name
    		return;
    	esfname = v;
    	objectChanged();
    }
    public boolean hasReservedPrefix() 
    {
    	return esfname.hasReservedPrefix();
    }
    public boolean isEsfReportsAccess()
    {
    	return ESF_PARTY_NAME_REPORTS_ACCESS.equals(esfname);
    }
    
    public short getProcessOrder()
    {
    	return processOrder;
    }
    public void setProcessOrder(short v)
    {
    	if ( processOrder != v )
    	{
        	processOrder = v;
        	objectChanged();
    	}
    }
    
    public String getDisplayName()
    {
    	return displayName;
    }
    public void setDisplayName(String v)
    {
    	if ( EsfString.isNonBlank(v) )
    	{
        	if ( v.length() > Literals.DISPLAY_NAME_MAX_LENGTH )
        		displayName = v.substring(0,Literals.DISPLAY_NAME_MAX_LENGTH).trim();
            else
            	displayName = v.trim();
    		objectChanged();
    	}
    }
    
    public EsfUUID getTodoGroupId()
    {
    	return todoGroupId;
    }
    public boolean hasTodoGroupId()
    {
    	return todoGroupId != null;
    }
    public void setTodoGroupId(EsfUUID v)
    {
    	todoGroupId = v;
    	objectChanged();
    }
    
    public EsfUUID getTestTodoGroupId()
    {
    	return testTodoGroupId;
    }
    public boolean hasTestTodoGroupId()
    {
    	return testTodoGroupId != null;
    }
    public void setTestTodoGroupId(EsfUUID v)
    {
    	testTodoGroupId = v;
    	objectChanged();
    }
    
    public synchronized List<PartyTemplateFieldTemplate> getPartyTemplateFieldTemplates()
    {
    	return new LinkedList<PartyTemplateFieldTemplate>(fieldTemplates);
    }
    public synchronized List<PartyTemplateFieldTemplate> getDuplicatePartyTemplateFieldTemplates()
    {
    	LinkedList<PartyTemplateFieldTemplate> list = new LinkedList<PartyTemplateFieldTemplate>();
    	for( PartyTemplateFieldTemplate f : fieldTemplates )
    		list.add(f.duplicate());
    	return list;
    }

    public synchronized void setPartyTemplateFieldTemplates(List<PartyTemplateFieldTemplate> fieldTemplates)
    {
    	this.fieldTemplates = fieldTemplates;
    	objectChanged();
    }
    public synchronized void addPartyTemplateFieldTemplates(PartyTemplateFieldTemplate fieldTemplate)
    {
    	fieldTemplates.add(fieldTemplate);
    	objectChanged();
    }
    public synchronized void clearPartyTemplateFieldTemplates()
    {
    	fieldTemplates.clear();
    	objectChanged();
    }
    public synchronized void removePartyTemplateFieldTemplate(PartyTemplateFieldTemplate fieldTemplate)
    {
    	if ( fieldTemplatesToRemoveOnSave == null )
    		fieldTemplatesToRemoveOnSave = new LinkedList<PartyTemplateFieldTemplate>();
    	fieldTemplatesToRemoveOnSave.add(fieldTemplate);
    	fieldTemplates.remove(fieldTemplate);
    }
    public synchronized void removePartyTemplateFieldTemplate(FieldTemplate fieldTemplate)
    {
    	for( PartyTemplateFieldTemplate ptft : getPartyTemplateFieldTemplates() )
    	{
    		// If this party made use of the specified field template, remove the mapping to it...
    		if ( ptft.getFieldTemplateId().equals(fieldTemplate.getId() ) )
    			removePartyTemplateFieldTemplate(ptft);
    	}
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof PartyTemplate )
            return getId().equals(((PartyTemplate)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(PartyTemplate o)
    {
    	return getProcessOrder() != o.getProcessOrder() ? (getProcessOrder() - o.getProcessOrder()) : getEsfName().compareTo(o.getEsfName());
    }
    
    public String toXml()
    {
        StringBuilder buf = new StringBuilder((int)getEstimatedLengthXml());
        return appendXml(buf).toString();
    }
    
    public StringBuilder appendXml(StringBuilder buf)
    {
        if ( buf == null )
            buf = new StringBuilder((int)getEstimatedLengthXml());
        else
            buf.ensureCapacity((int)getEstimatedLengthXml());
        
        buf.append("<PartyTemplate xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        
        buf.append(" <id>").append(id.toXml()).append("</id>\n");
        buf.append(" <containerId>").append(containerId.toXml()).append("</containerId>\n");
        buf.append(" <esfname>").append(esfname.toXml()).append("</esfname>\n");
        buf.append(" <displayName>").append(escapeXml(displayName)).append("</displayName>\n");
        buf.append(" <processOrder>").append(processOrder).append("</processOrder>\n");
        if ( todoGroupId != null )
        	buf.append(" <todoGroupId>").append(todoGroupId.toXml()).append("</todoGroupId>\n");
        if ( testTodoGroupId != null )
        	buf.append(" <testTodoGroupId>").append(testTodoGroupId.toXml()).append("</testTodoGroupId>\n");
        for( PartyTemplateFieldTemplate ptft : fieldTemplates )
        	ptft.appendXml(buf);

        buf.append("</PartyTemplate>\n");
        
        return buf;
    }
    
    public long getEstimatedLengthXml()
    {
        long len = 41 + 166 + id.getEstimatedLengthXml() + containerId.getEstimatedLengthXml() + esfname.getEstimatedLengthXml() + displayName.length();
        if ( todoGroupId != null )
        	len += 30 + todoGroupId.getEstimatedLengthXml();
        if ( testTodoGroupId != null )
        	len += 38 + testTodoGroupId.getEstimatedLengthXml();
        for( PartyTemplateFieldTemplate ptft : fieldTemplates )
        	len += ptft.getEstimatedLengthXml();
        return len; 
    }

	public synchronized boolean save(final Connection con)
		throws SQLException
	{
		_logger.debug("save(con) on id: " + id + "; esfname: " + esfname + "; insert: " + doInsert());
	    
	    clearLastSQLException();
	    EsfPreparedStatement stmt = null;
	    
	    // It's not valid to have one, but not the other, set. Both can be null, or both must be set.
	    if ( hasTodoGroupId() && ! hasTestTodoGroupId() )
	    	setTestTodoGroupId(getTodoGroupId());
	    else if ( ! hasTodoGroupId() && hasTestTodoGroupId() )
	    	setTodoGroupId(getTestTodoGroupId());
	    
	    try
	    {
	        if ( doInsert() )
	        {
	            stmt = new EsfPreparedStatement(con, 
	            	"INSERT INTO esf_party_template (id,container_id,process_order,esfname,display_name,todo_group_id,test_todo_group_id) VALUES (?,?,?,?,?,?,?)");
	            stmt.set(id);
	            stmt.set(containerId);
	            stmt.set(processOrder);
	            stmt.set(esfname);
	            stmt.set(displayName);
	            stmt.set(todoGroupId);
	            stmt.set(testTodoGroupId);
	            
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Insert failed for id: " + id + "; esfname: " + esfname);
	                return false;
	            }
	            
	            for( PartyTemplateFieldTemplate pf : fieldTemplates )
	            {
	            	if ( ! pf.save(con) )
	            		return false;
	            }
	            
	            // Now we mark this object as if it were loaded fresh from the database
	            setLoadedFromDb();
	            
	            return true;
	        }
	        
	        if ( fieldTemplatesToRemoveOnSave != null ) 
	        {
	        	for( PartyTemplateFieldTemplate ptft : fieldTemplatesToRemoveOnSave )
	        		ptft.delete(con,null);
	        	fieldTemplatesToRemoveOnSave = null;
	        }
	        
	        if ( hasChanged() )
	        {
	            // We assume we'll update it instead
	            stmt = new EsfPreparedStatement( con, 
	            		"UPDATE esf_party_template SET process_order=?, esfname=?, display_name=?, todo_group_id=?, test_todo_group_id=? WHERE id=?"
	            						   		);
	            stmt.set(processOrder);
	            stmt.set(esfname);
	            stmt.set(displayName);
	            stmt.set(todoGroupId);
	            stmt.set(testTodoGroupId);
	            stmt.set(id);
	            int num = stmt.executeUpdate();
	            if ( num != 1 )
	            {
	            	_logger.warn("save(con) - Update failed for id: " + id + "; esfname: " + esfname);
	            }

	            for( PartyTemplateFieldTemplate pf : fieldTemplates )
	            {
	            	if ( ! pf.save(con) )
	            		return false;
	            }
	        }
	
	        // Now we mark this object as if it were loaded fresh from the database
	        setLoadedFromDb();
	        
	        return true;
	    }
	    catch(SQLException e)
	    {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; esfname: " + esfname + "; insert: " + doInsert());
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	        cleanupStatement(stmt);
	    }
	}
    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }


    synchronized boolean delete(final Connection con, final Errors errors, final User user)
	    throws SQLException
	{
		_logger.debug("delete(con) on id: " + id + "; esfname: " + esfname + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
	    
	    clearLastSQLException();
	
	    // Let's do some referential integrity checks to be sure it's not in use 
	    if ( ! checkReferential(con,errors) )
	        return false;
	
	    if ( doInsert() )
	    {
	    	_logger.warn("delete(con) - Ignored delete of party template that was pending an INSERT id: " + id + "; esfname: " + esfname);
	        objectDeleted();
	        return true;
	    }
	    
	    EsfPreparedStatement stmt = null;
	    
	    try
	    {
	        // Delete the party
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_party_template WHERE id=?");
	        stmt.set(id);
	        int num = stmt.executeUpdate();
	        if ( num == 0 )
	        	_logger.warn("delete(con) - Failed to find esf_party_template database row to delete with id: " + id);
	        
	        // If a party template is deleted, we remove any field templates associated with it
	        stmt.close();
	    	stmt = new EsfPreparedStatement(con,"DELETE FROM esf_party_template_field_template WHERE party_template_id=?");
	        stmt.set(id);
	        num = stmt.executeUpdate();
	        
	        objectDeleted();
	        
	        if ( user != null ) 
	        {
	        	user.logConfigChange(con, "Deleted party template " + getEsfName()); 
	        	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted party template " + getEsfName());
	        }
	
	        return true;
	    }
	    catch(SQLException e)
	    {
	    	_logger.sqlerr(e,"delete(con) on id: " + id + "; esfname: " + esfname + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
	        setLastSQLException(e);
	        throw e;
	    }
	    finally
	    {
	    	cleanupStatement(stmt);
	    }
	}

	public boolean delete(final Errors errors, final User user)
	{
	    ConnectionPool    pool = Application.getInstance().getConnectionPool();
	    Connection        con  = pool.getConnection();
	    try
	    {
	        boolean result = delete(con,errors,user);
	        con.commit();
	        return result;
	    }
	    catch(SQLException e) 
	    {
	        pool.rollbackIgnoreException(con,e);
	        return false;
	    }
	    finally
	    {
	    	Application.getInstance().cleanupPool(pool,con,null);
	    }
	}
	public synchronized boolean delete(final User user)
	{
	    Errors nullErrors = null;
	    return delete(nullErrors,user);
	}
	public synchronized boolean delete()
	{
	    User nullUser = null;
	    return delete(nullUser);
	}
    
	protected boolean checkReferential(final Connection con, final Errors errors)
    	throws SQLException
    {
		if ( esfname.hasReservedPrefix() )
		{
	    	_logger.error("checkReferential() FAILED RESERVE PREFIX on id: " + id + "; esfname: " + esfname);
	    	if ( errors != null )
	    		errors.addError("You cannot delete the reserved special party template: " + getEsfName());
			return false;
		}
		return true;
	}

    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
   	public static class Manager
   	{
   	    /**
   	     * Retrieves all PartyTemplate objects held in the specified container.
   	     * @return the List of PartyTemplate found indexed by process order.
   	     */
   	    public static List<PartyTemplate> getAll(EsfUUID containerId)
   	    {
   	    	LinkedList<PartyTemplate> partyList = new LinkedList<PartyTemplate>();
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id,process_order,esfname,display_name,todo_group_id,test_todo_group_id FROM esf_party_template WHERE container_id=? ORDER BY process_order ASC"
   	            						   );
   	        	stmt.set(containerId);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	short order = rs.getShort();
		            EsfName esfname = rs.getEsfName();
		            String displayName = rs.getString();
		            EsfUUID todoGroupId = rs.getEsfUUID();
		            EsfUUID testTodoGroupId = rs.getEsfUUID();
		            
		            List<PartyTemplateFieldTemplate> fieldTemplates = PartyTemplateFieldTemplate.Manager.getAllForParty(id);
		            
		            PartyTemplate party = new PartyTemplate(id,containerId,order,esfname,displayName,todoGroupId,testTodoGroupId,fieldTemplates);
		            party.setLoadedFromDb();
		            partyList.add(party);
	            }
	            
   	            con.commit();
   	            
   	            _logger.debug("Manager.getAll() - containerId: " + containerId + "; found: " + partyList.size());
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - containerId: " + containerId + "; found: " + partyList.size() + " entries added before exception");
   	            pool.rollbackIgnoreException(con,e);
   	         partyList.clear();
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        return partyList;
   	    }
   	    
   	    /**
   	     * Retrieves the PartyTemplate objects held in the specified container with the specified name
   	     * @return the PartyTemplate found or null if not found
   	     */
   	    public static PartyTemplate getByName(EsfUUID containerId, EsfName templateName)
   	    {
   	    	_logger.debug("Manager.getByName() - containerId: " + containerId + "; templateName: " + templateName);
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	    	            "SELECT id,process_order,esfname,display_name,todo_group_id,test_todo_group_id FROM esf_party_template WHERE container_id=? AND lower(esfname)=?"
   	            						   );
   	        	stmt.set(containerId);
   	        	stmt.set(templateName.toLowerCase());
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	short order = rs.getShort();
		            EsfName esfname = rs.getEsfName();
		            String displayName = rs.getString();
		            EsfUUID todoGroupId = rs.getEsfUUID();
		            EsfUUID testTodoGroupId = rs.getEsfUUID();
		            
		            List<PartyTemplateFieldTemplate> fieldTemplates = PartyTemplateFieldTemplate.Manager.getAllForParty(id);
		            
		            PartyTemplate party = new PartyTemplate(id,containerId,order,esfname,displayName,todoGroupId,testTodoGroupId,fieldTemplates);
		            party.setLoadedFromDb();
	   	            con.commit();
	   	            return party;
	            }
	            con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() - containerId: " + containerId + "; templateName: " + templateName);
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

            return null;
   	    }
   	    
   	    public static PartyTemplate createLike(EsfUUID newContainerId, PartyTemplate likeTemplate)
   	    {
   	    	PartyTemplate newTemplate = new PartyTemplate(newContainerId, likeTemplate);
   	    	return newTemplate;
   	    }
   	    
   	    public static PartyTemplate createNew(EsfUUID newContainerId, EsfName name)
   	    {
   	    	PartyTemplate newTemplate = new PartyTemplate(newContainerId,name);
   	    	return newTemplate;
   	    }
   	    
   	    public static PartyTemplate createFromJDOM(EsfUUID containerId, TreeMap<EsfName,FieldTemplate> fieldTemplateMap, List<Element> fieldElements, Element e)
   	    {
   	    	if ( containerId == null || containerId.isNull() || fieldTemplateMap == null || fieldElements == null || e == null )
   	    		return null;
   	    	
   	    	EsfUUID id = new EsfUUID();
   	    	
   	    	LinkedList<PartyTemplateFieldTemplate> partyTemplateFieldTemplateList = new LinkedList<PartyTemplateFieldTemplate>();

   	    	List<Element> ptftElements = e.getChildren("PartyTemplateFieldTemplate", e.getNamespace());
   	    	ListIterator<Element> ptftIter = ptftElements.listIterator();
			while( ptftIter.hasNext() ) 
			{
				Element ptftElement = ptftIter.next();
				
				// Now, let's use the embedded field template id to find that field template in the field elements, and then use that name
				// to find it in our map so we can use it's id
				FieldTemplate fieldTemplate = null;
				EsfUUID targetFieldTemplateId = new EsfUUID(ptftElement.getChildText("fieldTemplateId", e.getNamespace()));
	   	    	ListIterator<Element> fieldIter = fieldElements.listIterator();
				while( fieldIter.hasNext() ) 
				{
					Element fieldElement = fieldIter.next();
					EsfUUID checkFieldTemplateId = EsfUUID.createFromToXml(fieldElement.getChildText("id", e.getNamespace()));
					if ( targetFieldTemplateId.equals(checkFieldTemplateId) )
					{
						EsfName fieldName = EsfName.createFromToXml(fieldElement.getChildText("esfname", e.getNamespace()));
						fieldTemplate = fieldTemplateMap.get(fieldName);
						break;
					}
				}  	    	
				
				if ( fieldTemplate != null )
				{
					PartyTemplateFieldTemplate ptft = PartyTemplateFieldTemplate.Manager.createFromJDOM(id,fieldTemplate.getId(),ptftElement);
					partyTemplateFieldTemplateList.add(ptft);
				}
				else
					_logger.error("Manager.createFromJDOM() - Could not find a FieldTemplate. targetFieldTemplateId: " + targetFieldTemplateId);
			}  	    	


   	    	PartyTemplate newPartyTemplate = new PartyTemplate(
   	    			id, 
   	    			containerId,
   	    			Application.getInstance().stringToShort(e.getChildText("processOrder", e.getNamespace()), (short)0),
   	    			EsfName.createFromToXml(e.getChildText("esfname", e.getNamespace())),
   	    			e.getChildText("displayName", e.getNamespace()),
   	    			null,null,
   	    			partyTemplateFieldTemplateList
   	    			);
   	    	newPartyTemplate.setNewObject();
   	    	return newPartyTemplate;
   	    }
   	    
   	} // Manager
    
}