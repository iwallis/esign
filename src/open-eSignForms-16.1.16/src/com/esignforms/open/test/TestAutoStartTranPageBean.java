// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.test;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.esignforms.open.data.EsfString;
import com.esignforms.open.jsp.PageBean;



public class TestAutoStartTranPageBean extends PageBean 
{
	// static values that are buttons
	public static final String TEST_BUTTON  = "testButton";
	public static final String POST_TO_URL = "http://localhost/open-eSignFormsVaadin/S/TestTran";
	
	public PageProcessing doGet() throws java.io.IOException {
		return PageProcessing.CONTINUE;
	}

	public PageProcessing doPost()	throws java.io.IOException {
	    String button = getParam(TEST_BUTTON,null);
	    if ( button != null ) {
	        if ( ! doTestPost() ) 
	        	errors.addError("The test post failed to get the redirect URL.");
	        return PageProcessing.CONTINUE;
	    }

	    errors.addError("You pressed an unexpected button. Please try again.");
		return PageProcessing.CONTINUE;
	}


	private boolean doTestPost()
	{
		java.net.HttpURLConnection con = null;
		java.io.BufferedReader     br  = null;
		java.net.URL url = null;

		try
		{
	        try
	        {
	        	url = new java.net.URL( POST_TO_URL );
	        }
	        catch( Exception e ) 
	        {
	        	errors.addError("Could not connect to URL: " + POST_TO_URL);
	        	return false;
	        }
	        
	        StringBuilder buf = new StringBuilder(4096);
	        buf.append("ESFTEST=Yes");
	        buf.append("&firstName=David");
	        buf.append("&lastName=Wall");
	        buf.append("\r");
	        
			byte[] postDataBytes = EsfString.stringToBytes(buf.toString());
			int contentLength = postDataBytes.length;
			
			// Create the connection and set the connection properties
			con = (java.net.HttpURLConnection)url.openConnection();
			con.setRequestMethod("POST");
			con.setAllowUserInteraction(false);  // this is program, so no popups
			con.setDoInput(true);      // we will read from the URL
			con.setDoOutput(true);     // we will write to the URL
			con.setIfModifiedSince(0); // always get
			con.setUseCaches(false);   // don't use cache
			con.setRequestProperty("Content-Length", String.valueOf(contentLength));
			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
			con.setRequestProperty("Accept", "*/*");
			con.setRequestProperty("user-agent", request.getHeader("user-agent"));
			con.setRequestProperty("Referer", request.getHeader("Referer"));
			con.setInstanceFollowRedirects(false);
			
			con.connect();

			// Construct the POST message
			java.io.OutputStream os = con.getOutputStream();
			os.write( postDataBytes );
			os.close();
			
			int responseCode = con.getResponseCode();
			String responseMessage = con.getResponseMessage();
			errors.addSuccess("Response code: " + responseCode + "; message: " + responseMessage);

			if ( responseCode == 302 )
			{
				 Map<String, List<String>> headerFields = con.getHeaderFields();
/*
				 List<String> cookies = headerFields.get("Set-Cookie");
				 if ( cookies != null && cookies.size() > 0 )
				 {
					 for( String cookie : cookies )
					 {
						 response.addHeader("Set-Cookie", cookie);
					 }
				 }

				 List<String> redirectToUrl = headerFields.get("Location");
				 if ( redirectToUrl != null && redirectToUrl.size() == 1 )
				 {
					 response.sendRedirect(redirectToUrl.get(0));
				 }
*/
				 List<String> redirectToUrl = headerFields.get("Location");
				 if ( redirectToUrl != null && redirectToUrl.size() == 1 )
				 {
					 errors.addSuccess("Found pickup URL to use: " + redirectToUrl.get(0));
				 }
				 return true;
			}
			
			java.io.InputStream is = con.getInputStream();
			if ( is == null )
			{
				errors.addError("Could not get the input stream from our POST");
				return false;
			}

			contentLength = con.getContentLength();
			
			if ( contentLength < 1 )
				contentLength = 2 * 1024;

			StringBuilder response = new StringBuilder(contentLength+32);
			
			br = new java.io.BufferedReader( new java.io.InputStreamReader(con.getInputStream()));
			String str;
			while ( (str = br.readLine()) != null )
			{
				if ( EsfString.isBlank(str) )
					continue;
				response.append(str).append('\n');
			}
			br.close(); br = null;

			str = response.toString();
			
			errors.addSuccess("Got a response page: ");
			errors.addSuccess(str);
			
			return true;
		}
		catch( java.io.IOException e )
		{
			errors.addError("Failed to get input stream from URL: " + url + "; e: " + e.getMessage());
			try
			{
				java.io.InputStream is = con == null ? null : con.getErrorStream();
				if ( is != null )
				{
					br = new java.io.BufferedReader( new java.io.InputStreamReader(is));
					StringBuilder response = new StringBuilder(1024);
					String str;
					while ( (str = br.readLine()) != null )
					{
						response.append(str).append('\n');
					}
					br.close(); br = null;

					str = response.toString();
					
					errors.addError("** ERROR response is:");
					errors.addError(str);
					errors.addError("** End of ERROR response.");
					return false;
				}

				errors.addError("Failed to get error stream from URL: " + url);
			}
			catch( Exception e2 ) {}
			errors.addError("Failed to contact URL: " + url);
			return false;
		}
		finally
		{
			try
			{
				if ( br != null )
					br.close();
				if ( con != null )
					con.disconnect();
			}
			catch( IOException e ) {}
		}
	}

}