// Copyright (C) 2009-2011 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.test;


import java.security.SecureRandom;
import java.util.LinkedList;
import java.util.List;

public class ListTest 
{
    public static void main(String[] args)
    	throws java.io.IOException
    {
    	byte rand[] = new byte[41];
    	SecureRandom defaultRandom = new SecureRandom();
    	defaultRandom.nextBytes(rand);

    	System.out.println(System.currentTimeMillis() + " - ListTest: BEGIN unsync");
    	List<Long> list1 = new LinkedList<Long>();
    	for( int i=0; i < 100000; ++i )
    	{
    		long v = defaultRandom.nextLong();
    		list1.add( new Long(v) );
    	}
    	System.out.println(System.currentTimeMillis() + " - ListTest: END BUILD unsync");
    	long count = 0;
    	for( int i=0; i < list1.size(); ++i )
    	{
    		count++;
    	}
    	System.out.println(System.currentTimeMillis() + " - ListTest: END ITERATOR unsync; count: " + count);

    	System.out.println(System.currentTimeMillis() + " - ListTest: BEGIN sync");
    	List<Long> list2 = java.util.Collections.synchronizedList(new LinkedList<Long>());
    	for( int i=0; i < 100000; ++i )
    	{
    		long v = defaultRandom.nextLong();
    		list2.add( new Long(v) );
    	}
    	System.out.println(System.currentTimeMillis() + " - ListTest: END BUILD sync");
    	count = 0;
    	for( int i=0; i < list2.size(); ++i )
    	{
    		count++;
    	}
    	System.out.println(System.currentTimeMillis() + " - ListTest: END ITERATOR sync; count: " + count);
    	
    	System.exit(0);
    }
}