// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2009 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.test;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

public class RandomTest 
{
    public static void main(String[] args)
    	throws java.io.IOException, NoSuchAlgorithmException, NoSuchProviderException
    {
    	//java.io.BufferedReader in = new java.io.BufferedReader( new java.io.InputStreamReader(System.in) );
    	//String command = "";
        byte[] rand = new byte[41];
    	
    	System.out.println(System.currentTimeMillis() + " - RandomTest: DEBUG starting default random");
    	SecureRandom defaultRandom = new SecureRandom();
    	defaultRandom.nextBytes(rand);
    	System.out.println(System.currentTimeMillis() + " - RandomTest: DEBUG started default random; alg: " + defaultRandom.getAlgorithm() +
        		"; prov: " + defaultRandom.getProvider() + "; next long: " + defaultRandom.nextLong());
    	defaultRandom.setSeed(0);
    	System.out.println(System.currentTimeMillis() + " - RandomTest: DEBUG after setSeed(0); next long: " + defaultRandom.nextLong());
    	defaultRandom.setSeed(0);
    	System.out.println(System.currentTimeMillis() + " - RandomTest: DEBUG after setSeed(0) again; next long: " + defaultRandom.nextLong());
        
    	System.out.println(System.currentTimeMillis() + " - RandomTest: DEBUG starting SUN random");
    	SecureRandom sunRandom = SecureRandom.getInstance("SHA1PRNG", "SUN");
    	sunRandom.nextBytes(rand);
    	System.out.println(System.currentTimeMillis() + " - RandomTest: DEBUG started SUN random; alg: " + sunRandom.getAlgorithm() +
        		"; prov: " + sunRandom.getProvider() + "; next long: " + sunRandom.nextLong());
    	sunRandom.setSeed(0);
    	System.out.println(System.currentTimeMillis() + " - RandomTest: DEBUG after setSeed(0); next long: " + sunRandom.nextLong());
    	sunRandom.setSeed(0);
    	System.out.println(System.currentTimeMillis() + " - RandomTest: DEBUG after setSeed(0) again; next long: " + sunRandom.nextLong());
    	
    	for(int i=0; i < 1000000; ++i )
    	{
        	System.out.println(System.currentTimeMillis() + " - RandomTest: DEBUG next long#"+i+": " + defaultRandom.nextLong());
        	System.out.println(System.currentTimeMillis() + " - RandomTest: DEBUG next long#"+i+": " + sunRandom.nextLong());
    	}
    	
    	System.exit(0);
    }
}