// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2009 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.test;

import java.util.UUID;

/**
 * Application class for the Open eSignForms custom web applications. 
 *
 * @author Yozons, Inc.
 */
public class UUIDTest 
{
    public static void main(String[] args)
    	throws java.io.IOException
    {
    	java.io.BufferedReader in = new java.io.BufferedReader( new java.io.InputStreamReader(System.in) );
    	String command = "";
    	
    	while( ! "quit".equals(command) )
    	{
    		System.out.println();
    		System.out.print("Enter to get a new UUID (or 'quit'): ");
    		command = in.readLine();
    		if ( ! "quit".equals(command) )
    		{
    			UUID uuid = UUID.randomUUID();
    			String uuidString = uuid.toString();
    			System.out.print("UUID generated: " + uuidString);
    			
    			UUID uuid2 = UUID.fromString(uuidString);
    			if ( uuid.equals(uuid2) )
    				System.out.println(" - Matched when UUID created from String");
    			else
    				System.out.println(" - DID NOT Match when UUID created from String");
    		}
    	}
   	
    	System.exit(0);
    }
}