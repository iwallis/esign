// Copyright (C) 2011-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.report;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.ReportTemplateReportField;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionFile;
import com.esignforms.open.runtime.reports.ReportListingInfo;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.widget.ReportAccessToTransactionButton;
import com.esignforms.open.vaadin.widget.TranFilePopupButton;
import com.esignforms.open.vaadin.widget.TranLatestSnapshotsAsPdfButton;
import com.esignforms.open.vaadin.widget.TranResumeButton;
import com.esignforms.open.vaadin.widget.TranSnapshotPopupButton;
import com.esignforms.open.vaadin.widget.TranSnapshotsAsPdfPopupButton;
import com.esignforms.open.vaadin.widget.TranSnapshotsDownloadPopupButton;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Table.Align;


public class ReportContainer extends IndexedContainer implements Serializable {
	private static final long serialVersionUID = -4211391217564120650L;

	ReportTemplate reportTemplate;
	User user;
	boolean reportAccessView;
	boolean reportAccessUpdate;
	ColID reportAccessViewColID = new ColID(new EsfUUID("1-1-1-1-1"), (short)-1);
	ColID reportAccessUpdateColID = new ColID(new EsfUUID("2-2-2-2-2"), (short)-2);
	LinkedList<Object> fieldNameList;
	LinkedList<String> fieldHeaderList;
	LinkedList<Align> fieldAlignmentList;
	
	@SuppressWarnings("deprecation")
	public ReportContainer(ReportTemplate reportTemplate, User user, boolean reportAccessView, boolean reportAccessUpdate) throws InstantiationException, IllegalAccessException {
		this.reportTemplate = reportTemplate;
		this.user = user;
		this.reportAccessView = reportAccessView;
		this.reportAccessUpdate = reportAccessUpdate;
		this.fieldNameList = new LinkedList<Object>();
		this.fieldHeaderList = new LinkedList<String>();
		this.fieldAlignmentList = new LinkedList<Align>();

		if ( reportAccessView ) {
			Object fieldName = reportAccessViewColID;
			this.fieldNameList.add(fieldName);
			this.fieldHeaderList.add("");
			this.fieldAlignmentList.add(Align.CENTER);
			addContainerProperty(fieldName, ReportAccessToTransactionButton.class, null);
		} 
		if ( reportAccessUpdate ) {
			Object fieldName = reportAccessUpdateColID;
			this.fieldNameList.add(fieldName);
			this.fieldHeaderList.add("");
			this.fieldAlignmentList.add(Align.CENTER);
			addContainerProperty(fieldName, ReportAccessToTransactionButton.class, null);
		}

		for( ReportTemplateReportField rtrf : reportTemplate.getReportTemplateReportFieldList() ) {
			Object fieldName = new ColID(rtrf);
			this.fieldNameList.add(fieldName);
			
			ReportFieldTemplate reportFieldTemplate = rtrf.getReportFieldTemplate();
			if ( reportFieldTemplate == null ) {
				this.fieldHeaderList.add("???");
				addContainerProperty(fieldName, String.class, "???");
			} else {
				this.fieldHeaderList.add(rtrf.getFieldLabel());
				addContainerProperty(fieldName, reportFieldTemplate.getFieldValueClass(), null);
			}
			
			if ( reportFieldTemplate != null ) {
				if ( reportFieldTemplate.isFieldTypeInteger() || reportFieldTemplate.isFieldTypeDecimal() ) {
					this.fieldAlignmentList.add(Align.RIGHT);
				} else if ( reportFieldTemplate.isFieldTypeDate() || 
						    reportFieldTemplate.isFieldTypeFile() || 
						    reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_LATEST_SNAPSHOTS) ||
						    reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_SELECTED_SNAPSHOTS) ||
						    reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_SELECTED_SNAPSHOTS) ||
						    reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_HTML_SELECTED_SNAPSHOT) ||
						    reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_RESUME_TRANSACTION_BUTTON)
						  ) {
					this.fieldAlignmentList.add(Align.CENTER);
				} else {
					this.fieldAlignmentList.add(Align.LEFT);
				}
			} else {
				this.fieldAlignmentList.add(Align.LEFT);
			}
		}
	}
	
	public Object[] getFieldNameList() {
		return fieldNameList.toArray();
	}
	
	public String[] getFieldHeaderList() {
		String[] headers = new String[fieldHeaderList.size()];
		fieldHeaderList.toArray(headers);
		return headers;
	}
	
	public Align[] getFieldAlignmentList() {
		Align[] alignments = new Align[fieldAlignmentList.size()];
		fieldAlignmentList.toArray(alignments);
		return alignments;
	}
	
	@SuppressWarnings({ "unchecked", "deprecation" })
	public void refresh(List<ReportListingInfo> list) {
		removeAllItems();
		
		for( ReportListingInfo reportList : list ) {
			Item item = addItem(reportList.getId());
			
			Transaction transaction = null;
			
			if ( reportAccessView ) {
				Object fieldName = reportAccessViewColID;
				ReportAccessToTransactionButton button = new ReportAccessToTransactionButton(reportList.getId(), reportTemplate, false);
				item.getItemProperty(fieldName).setValue(button);
			} 
			if ( reportAccessUpdate ) {
				Object fieldName = reportAccessUpdateColID;
				ReportAccessToTransactionButton button = new ReportAccessToTransactionButton(reportList.getId(), reportTemplate, true);
				item.getItemProperty(fieldName).setValue(button);
			}

			for( ReportTemplateReportField rtrf : reportList.getReportFieldValueList() ) {
				Object fieldName = new ColID(rtrf);
				ReportFieldTemplate reportFieldTemplate = rtrf.getReportFieldTemplate();
				if ( reportFieldTemplate.isFieldTypeFile() ) {
					EsfUUID[] tranFileIds = (EsfUUID[])rtrf.getFieldValues();
					if ( tranFileIds == null || tranFileIds.length < 1 )
						item.getItemProperty(fieldName).setValue(null);
					else {
						if ( transaction == null ) {
							transaction = Transaction.Manager.getById(reportList.getId());
							if ( transaction == null ) {
								item.getItemProperty(fieldName).setValue(null);
								continue;
							}
						}
						
						List<TransactionFile> tranFileList = new LinkedList<TransactionFile>();
						for( EsfUUID tranFileId : tranFileIds ) {
							TransactionFile tranFile = transaction.getTransactionFileById(tranFileId);
							TransactionDocument tranDoc = transaction.getTransactionDocument(tranFile.getTransactionDocumentId());
							if ( tranDoc != null && reportTemplate.isDocumentAllowed(tranDoc.getDocumentId()) ) {
								tranFileList.add( tranFile );
							}
						}
						
						if ( tranFileList.size() > 0 ) {
							TranFilePopupButton button = new TranFilePopupButton(transaction,tranFileList);
							item.getItemProperty(fieldName).setValue(button);
						} else {
							item.getItemProperty(fieldName).setValue(null);
						}
					}
				} else if ( reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_LATEST_SNAPSHOTS) ) {
					if ( transaction == null ) {
						transaction = Transaction.Manager.getById(reportList.getId());
						if ( transaction == null ) {
							item.getItemProperty(fieldName).setValue(null);
							continue;
						}
					}
					
					if ( reportTemplate.hasAllowedTransactionDocumentSnapshots(transaction) ) {
						TranLatestSnapshotsAsPdfButton button = new TranLatestSnapshotsAsPdfButton(transaction, reportTemplate);
						if ( button.getNumDocuments() < 1 ) {
							item.getItemProperty(fieldName).setValue(null);
						} else {
							item.getItemProperty(fieldName).setValue(button);
						}
					} else {
						item.getItemProperty(fieldName).setValue(null);
					}
				} else if ( reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_PDF_SELECTED_SNAPSHOTS) ) {
					if ( transaction == null ) {
						transaction = Transaction.Manager.getById(reportList.getId());
						if ( transaction == null ) {
							item.getItemProperty(fieldName).setValue(null);
							continue;
						}
					}
					
					if ( reportTemplate.hasAllowedTransactionDocumentSnapshots(transaction) ) {
						TranSnapshotsAsPdfPopupButton button = new TranSnapshotsAsPdfPopupButton(transaction, reportTemplate);
						if ( button.getNumDocuments() < 1 ) {
							item.getItemProperty(fieldName).setValue(null);
						} else {
							item.getItemProperty(fieldName).setValue(button);
						}
					} else {
						item.getItemProperty(fieldName).setValue(null);
					}
				} else if ( reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_SELECTED_SNAPSHOTS) ) {
					if ( transaction == null ) {
						transaction = Transaction.Manager.getById(reportList.getId());
						if ( transaction == null ) {
							item.getItemProperty(fieldName).setValue(null);
							continue;
						}
					}
					
					if ( reportTemplate.hasAllowedTransactionDocumentSnapshots(transaction) ) {
						TranSnapshotsDownloadPopupButton button = new TranSnapshotsDownloadPopupButton(transaction, reportTemplate);
						if ( button.getNumDocuments() < 1 ) {
							item.getItemProperty(fieldName).setValue(null);
						} else {
							item.getItemProperty(fieldName).setValue(button);
						}
					} else {
						item.getItemProperty(fieldName).setValue(null);
					}
				} else if ( reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_DOWNLOAD_HTML_SELECTED_SNAPSHOT) ) {
					if ( transaction == null ) {
						transaction = Transaction.Manager.getById(reportList.getId());
						if ( transaction == null ) {
							item.getItemProperty(fieldName).setValue(null);
							continue;
						}
					}
					
					if ( reportTemplate.hasAllowedTransactionDocumentSnapshots(transaction) ) {
						TranSnapshotPopupButton button = new TranSnapshotPopupButton(transaction, reportTemplate);
						if ( button.getNumDocuments() < 1 ) {
							item.getItemProperty(fieldName).setValue(null);
						} else {
							item.getItemProperty(fieldName).setValue(button);
						}
					} else {
						item.getItemProperty(fieldName).setValue(null);
					}
				} else if ( reportFieldTemplate.getFieldName().equals(ReportFieldTemplate.BUILT_IN_RESUME_TRANSACTION_BUTTON) ) {
					if ( transaction == null ) {
						transaction = Transaction.Manager.getById(reportList.getId());
						if ( transaction == null ) {
							item.getItemProperty(fieldName).setValue(null);
							continue;
						}
					}
					
					if ( transaction.isSuspended() && transaction.getTransactionTemplate().hasResumePermission(user) ) {
						TranResumeButton button = new TranResumeButton(transaction.getId(), reportTemplate);
						item.getItemProperty(fieldName).setValue(button);
					} else {
						item.getItemProperty(fieldName).setValue(null);
					}
				} else {
					item.getItemProperty(fieldName).setValue(rtrf.getFieldValue());
				}
			} // end for each report field
			
			if ( transaction != null ) 
				Transaction.Manager.removeFromCache(transaction);
			
		} // end for each ReportListingInfo
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
	
	public static class ColID implements java.io.Serializable {
		private static final long serialVersionUID = -3009100613585609852L;

		EsfUUID id;
		short colNumber;
		
		public ColID(EsfUUID id, short colNumber) {
			this.id = id;
			this.colNumber = colNumber;
		}
		public ColID(ReportTemplateReportField rtrf) {
			this.id = rtrf.getReportFieldTemplateId();
			this.colNumber = rtrf.getFieldOrder();
		}
		public EsfUUID getId() {
			return id;
		}
		
		public boolean equals(Object other)
		{
			if ( other != null && other instanceof ColID ) {
				return id.equals(((ColID)other).id) && colNumber == ((ColID)other).colNumber;
			}
			return false;
		}
		
		@Override
		public int hashCode()
		{
			return id.hashCode() + colNumber;
		}
		
	}
}