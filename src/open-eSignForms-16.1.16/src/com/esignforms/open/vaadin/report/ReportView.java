// Copyright (C) 2011-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.report;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.vaadin.peter.buttongroup.ButtonGroup;
import org.vaadin.resetbuttonfortextfield.ResetButtonForTextField;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfDecimal;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.ReportTemplateReportField;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.reports.ReportListingInfo;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.EsfUUIDValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.ListSelectValid;
import com.vaadin.addon.tableexport.CsvExport;
import com.vaadin.addon.tableexport.ExcelExport;
import com.vaadin.addon.tableexport.PropertyFormatTable;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.WebBrowser;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class ReportView extends Panel implements EsfView {
	private static final long serialVersionUID = 7229607068731077053L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ReportView.class);
	
    String reportTemplateIdParam;
	ReportTemplate reportTemplate;
	String reportTemplateIdPropertySuffix;
	Collection<User> allUsers;
	
	String foundTranListPickupCode;
	List<ReportListingInfo> foundTranList;
    
    final ReportView thisView;
    ReportContainer container;
    ReportTable table;
	
	// For search bar
	private static final String PROP_TRANSACTION_TEMPLATE_IDS = "ReportView/transactionTemplateIds/";
	private static final String PROP_TABSHEET_STARTED_BY_TAB_ACTIVE = "ReportView/tabsheetStartedByTabActive/";
	private static final String PROP_STARTED_BY_USER_IDS = "ReportView/startedByUserIds/";
	private static final String PROP_TABSHEET_PARTY_TO_TAB_ACTIVE = "ReportView/tabsheetPartyToTabActive/";
	private static final String PROP_PARTY_TO_USER_IDS = "ReportView/partyToUserIds/";
	private static final String PROP_DATE_TYPE = "ReportView/dateType/";
	private static final String PROP_DATE_RANGE = "ReportView/dateRange/";
	private static final String PROP_FROM_DATE = "ReportView/fromDate/";
	private static final String PROP_TO_DATE = "ReportView/toDate/";
	private static final String PROP_ID = "ReportView/id/";
	private static final String PROP_PARTY_EMAIL = "ReportView/partyEmail/";
	private static final String PROP_IN_PROGRESS = "ReportView/inProgress/";
	private static final String PROP_COMPLETED = "ReportView/completed/";
	private static final String PROP_CANCELED = "ReportView/canceled/";
	private static final String PROP_SUSPENDED = "ReportView/suspended/";
	private static final String PROP_STALLED = "ReportView/stalled/";
	private static final String PROP_MAX_TRANSACTIONS = "ReportView/maxTransactions/";
	private static final String PROP_USER_SEARCHFIELD = "ReportView/usersearch/";
	
	VerticalLayout searchBarLayout;
	
	final static EsfUUID anyId = new EsfUUID("1-1-1-1-1"); // don't change this flag value since users who select it will have stored it in their properties
	
	final static EsfUUID meUserId = new EsfUUID("2-2-2-2-2"); // don't change this flag value since users who select it will have stored it in their properties
	final static EsfUUID externalUserId = new EsfUUID("3-3-3-3-3"); // don't change this flag value since users who select it will have stored it in their properties

	ListSelect transactionTemplateSelect;
	
	TabSheet startedByPartyTabSheet;
	Tab startedByUserTab;
	Tab partyToUserTab;
	ListSelect startedByUserSelect;
	ListSelect partyToUserSelect;
	
	OptionGroup searchDateType;
	NativeSelect searchDateRange;
	PopupDateField searchFromDate;
	PopupDateField searchToDate;
	NativeSelect maxFoundSelect;
	TextField searchId;
	TextField searchPartyEmail;
	CheckBox searchInProgress;
	CheckBox searchCompleted;
	CheckBox searchCanceled;
	CheckBox searchSuspended;
	CheckBox searchStalledOnly;
	HorizontalLayout buttonLayout;
	Button findButton;
	String findButtonLabel;
	Button excelButton;
	Button csvButton;
	Button exportButton;
	
	HorizontalLayout userSearchLayout;
	List<ReportTemplateReportField> userSearchFields;
	
	public ReportView() {
		super();
		thisView = this;
		setStyleName("ReportView");
		setSizeFull();
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();
		
		try {
			reportTemplateIdPropertySuffix = reportTemplate.getId().toNormalizedEsfNameString();
			
			if ( container != null )
				container.removeAllItems();
			container = new ReportContainer(reportTemplate, vaadinUi.getUser(),
					reportTemplate.hasEsfReportsAccessViewPermission(vaadinUi.getUser()),
					reportTemplate.hasEsfReportsAccessUpdatePermission(vaadinUi.getUser()));
			
			VerticalLayout layout = new VerticalLayout();
			setContent(layout);
			layout.setSizeFull();
			layout.setSpacing(false);
			layout.setMargin(false);

			searchBarLayout = new VerticalLayout();
			searchBarLayout.setWidth(100,Unit.PERCENTAGE);
			searchBarLayout.setMargin(false);
			searchBarLayout.setSpacing(false);

			GridLayout searchBar = new GridLayout(5,3);
	    	searchBar.addStyleName("searchBar");
	    	searchBar.setSpacing(true);
	    	searchBar.setMargin(false);
	    	searchBarLayout.addComponent(searchBar);
	    	
	    	List<EsfUUID> allTransactionTemplateIds = reportTemplate.getTransactionTemplateIdListForUser(vaadinUi.getUser());
	    	HashSet<EsfUUID> initialTransactionTemplateSelectValues = new HashSet<EsfUUID>();

        	EsfUUID[] propTransactionTemplateIds = vaadinUi.getUser().getUserDefinedUUIDsProperty(PROP_TRANSACTION_TEMPLATE_IDS+reportTemplateIdPropertySuffix);
        	if ( propTransactionTemplateIds == null || propTransactionTemplateIds.length == 0 ) {
        		initialTransactionTemplateSelectValues.add(anyId);
        	} else {
        		for( EsfUUID id : propTransactionTemplateIds ) {
        			if ( anyId.equals(id) || allTransactionTemplateIds.contains(id) )
        				initialTransactionTemplateSelectValues.add(id);
        		}
        	}
        	transactionTemplateSelect = new ListSelectValid();
        	transactionTemplateSelect.setDescription(vaadinUi.getMsg("ReportView.searchBar.transactionTemplate.tooltip"));
        	transactionTemplateSelect.addValidator(new SelectValidator(transactionTemplateSelect));
	    	transactionTemplateSelect.setNullSelectionAllowed(false); 
	    	transactionTemplateSelect.setImmediate(true);
	    	transactionTemplateSelect.setRequired(true);
	    	transactionTemplateSelect.setMultiSelect(true);
	    	transactionTemplateSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    		transactionTemplateSelect.addItem(anyId);
    		transactionTemplateSelect.setItemCaption( anyId, vaadinUi.getMsg("ReportView.searchBar.anyTransactionTemplateId.label") );
	    	for( EsfUUID transactionTemplateId : allTransactionTemplateIds ) {
	    		TransactionTemplate transactionTemplate = TransactionTemplate.Manager.getById(transactionTemplateId);
	    		if ( transactionTemplate == null ) {
	    			_logger.warn("buildLayout() - Report Template id: " + reportTemplate.getId() + "; report template name: " + reportTemplate.getPathName() + "; references unknown transaction template id: " + transactionTemplateId);
	    		} else {
	        		transactionTemplateSelect.addItem(transactionTemplate.getId());
	        		transactionTemplateSelect.setItemCaption( transactionTemplate.getId(), transactionTemplate.getPathName().toString() );
	    		}
	    	}
        	transactionTemplateSelect.setValue(initialTransactionTemplateSelectValues);
        	transactionTemplateSelect.setRows(Math.min(5,transactionTemplateSelect.size()));
        	searchBar.addComponent(transactionTemplateSelect,0,0,0,1);
	    	
        	// Set up the Started by Users selection list
	    	allUsers = User.Manager.getAll(vaadinUi.getUser());
	    	
	    	HashSet<EsfUUID> initialStartedByUserSelectValues = new HashSet<EsfUUID>();
	    	
        	EsfUUID[] propStartedByUserIds = vaadinUi.getUser().getUserDefinedUUIDsProperty(PROP_STARTED_BY_USER_IDS+reportTemplateIdPropertySuffix);
        	if ( propStartedByUserIds == null || propStartedByUserIds.length == 0 ) {
        		initialStartedByUserSelectValues.add(meUserId);
        	} else {
        		for( EsfUUID id : propStartedByUserIds ) {
        			if ( meUserId.equals(id) )
        				initialStartedByUserSelectValues.add(id);
        			else if ( anyId.equals(id) && reportTemplate.hasViewStartedByAnyUserPermission(vaadinUi.getUser()) )
        				initialStartedByUserSelectValues.add(id);
        			else if ( externalUserId.equals(id) && reportTemplate.hasViewStartedByExternalUsersPermission(vaadinUi.getUser()) )
        				initialStartedByUserSelectValues.add(id);
        			else if ( reportTemplate.hasViewStartedByAnyUserPermission(vaadinUi.getUser()) ) {
        				for( User u : allUsers ) {
        					if ( u.getId().equals(id) ) {
        						initialStartedByUserSelectValues.add(id);
        						break;
        					}
        				}
        			}
        		}
        	}
        	startedByUserSelect = new ListSelectValid();
        	startedByUserSelect.setWidth(100, Unit.PERCENTAGE);
        	startedByUserSelect.setDescription(vaadinUi.getMsg("ReportView.searchBar.startedByUserId.tooltip"));
        	startedByUserSelect.addValidator(new SelectValidator(startedByUserSelect));
        	startedByUserSelect.setNullSelectionAllowed(false); 
        	startedByUserSelect.setImmediate(true);
        	startedByUserSelect.setRequired(true);
        	startedByUserSelect.setMultiSelect(true);
        	startedByUserSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
        	if ( reportTemplate.hasViewStartedByExternalUsersPermission(vaadinUi.getUser()) ) {
            	startedByUserSelect.addItem(externalUserId);
            	startedByUserSelect.setItemCaption( externalUserId, vaadinUi.getMsg("ReportView.searchBar.startedByUserId.externalUserId.label") );
        	}
        	boolean allowStartedByAnyUser = reportTemplate.hasViewStartedByAnyUserPermission(vaadinUi.getUser());
        	if ( allowStartedByAnyUser ) {
            	startedByUserSelect.addItem(anyId);
            	startedByUserSelect.setItemCaption( anyId, vaadinUi.getMsg("ReportView.searchBar.startedByUserId.anyUserId.label") );
        	}
        	startedByUserSelect.addItem(meUserId);
        	startedByUserSelect.setItemCaption( meUserId, vaadinUi.getMsg("ReportView.searchBar.startedByUserId.meUserId.label") );
        	if ( allowStartedByAnyUser ) {
		    	for( User u : allUsers ) {
	    			startedByUserSelect.addItem(u.getId());
	    			startedByUserSelect.setItemCaption( u.getId(), u.getFullDisplayName() );
		    	}
        	}
	    	startedByUserSelect.setValue(initialStartedByUserSelectValues);
	    	startedByUserSelect.setRows(Math.min(5,startedByUserSelect.size()));
	    	
	    	HashSet<EsfUUID> initialPartyToUserSelectValues = new HashSet<EsfUUID>();
	    	
        	EsfUUID[] propPartyToUserIds = vaadinUi.getUser().getUserDefinedUUIDsProperty(PROP_PARTY_TO_USER_IDS+reportTemplateIdPropertySuffix);
        	if ( propPartyToUserIds == null || propPartyToUserIds.length == 0 ) {
        		initialPartyToUserSelectValues.add(meUserId);
        	} else {
        		for( EsfUUID id : propPartyToUserIds ) {
        			if ( meUserId.equals(id) )
        				initialPartyToUserSelectValues.add(id);
        			else if ( anyId.equals(id) && reportTemplate.hasViewAnyUserPartyToPermission(vaadinUi.getUser()) )
        				initialPartyToUserSelectValues.add(id);
        			else if ( reportTemplate.hasViewAnyUserPartyToPermission(vaadinUi.getUser()) ) {
        				for( User u : allUsers ) {
        					if ( u.getId().equals(id) ) {
        						initialPartyToUserSelectValues.add(id);
        						break;
        					}
        				}
        			}
        		}
        	}
        	partyToUserSelect = new ListSelectValid();
        	partyToUserSelect.setWidth(100, Unit.PERCENTAGE);
        	partyToUserSelect.setDescription(vaadinUi.getMsg("ReportView.searchBar.partyToUserId.tooltip"));
        	partyToUserSelect.addValidator(new SelectValidator(partyToUserSelect));
        	partyToUserSelect.setNullSelectionAllowed(false); 
        	partyToUserSelect.setImmediate(true);
        	partyToUserSelect.setRequired(true);
        	partyToUserSelect.setMultiSelect(true);
        	partyToUserSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
        	boolean allowAnyUserPartyTo = reportTemplate.hasViewAnyUserPartyToPermission(vaadinUi.getUser());
        	if ( allowAnyUserPartyTo ) {
        		partyToUserSelect.addItem(anyId);
        		partyToUserSelect.setItemCaption( anyId, vaadinUi.getMsg("ReportView.searchBar.partyToUserId.anyUserId.label") );
        	}
        	partyToUserSelect.addItem(meUserId);
        	partyToUserSelect.setItemCaption( meUserId, vaadinUi.getMsg("ReportView.searchBar.partyToUserId.meUserId.label") );
        	if ( allowAnyUserPartyTo ) {
		    	for( User u : allUsers ) {
		    		partyToUserSelect.addItem(u.getId());
		    		partyToUserSelect.setItemCaption( u.getId(), u.getFullDisplayName() );
		    	}
        	}
        	partyToUserSelect.setValue(initialStartedByUserSelectValues);
        	partyToUserSelect.setRows(Math.min(5,partyToUserSelect.size()));
	    	
	    	startedByPartyTabSheet = new TabSheet();
	    	startedByPartyTabSheet.setWidth(350,Unit.PIXELS);
	    	startedByPartyTabSheet.setStyleName(Reindeer.TABSHEET_MINIMAL);
	    	startedByUserTab = startedByPartyTabSheet.addTab(startedByUserSelect,vaadinUi.getMsg("ReportView.searchBar.TabSheet.startedByTab.label"));
	    	startedByUserTab.setClosable(false);
	    	partyToUserTab = startedByPartyTabSheet.addTab(partyToUserSelect,vaadinUi.getMsg("ReportView.searchBar.TabSheet.partyToTab.label"));
	    	partyToUserTab.setClosable(false);
        	searchBar.addComponent(startedByPartyTabSheet,2,0,2,1);
        	
        	EsfBoolean startedByTabActive = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_TABSHEET_STARTED_BY_TAB_ACTIVE+reportTemplateIdPropertySuffix);
        	if ( startedByTabActive == null )
        		startedByTabActive = new EsfBoolean(true);
        	EsfBoolean partyToTabActive = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_TABSHEET_PARTY_TO_TAB_ACTIVE+reportTemplateIdPropertySuffix);
           	if ( partyToTabActive == null )
           		partyToTabActive = new EsfBoolean(false);
        	if ( startedByTabActive.isTrue() )
        		startedByPartyTabSheet.setSelectedTab(startedByUserTab);
        	else if ( partyToTabActive.isTrue() )
        		startedByPartyTabSheet.setSelectedTab(partyToUserTab);
        	
        	EsfString propDateTypeEsfString = vaadinUi.getUser().getUserDefinedStringProperty(PROP_DATE_TYPE+reportTemplateIdPropertySuffix);
        	if ( propDateTypeEsfString == null || propDateTypeEsfString.isBlank() )
        		propDateTypeEsfString = new EsfString(Literals.DATE_LAST_UPDATED);
        	searchDateType = new OptionGroup();
        	searchDateType.setStyleName("inline");
        	searchDateType.setDescription(vaadinUi.getMsg("ReportView.searchBar.date.optiongroup.tooltip"));
        	searchDateType.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
        	searchDateType.addItem(Literals.DATE_STARTED);
        	searchDateType.setItemCaption(Literals.DATE_STARTED, vaadinUi.getMsg("ReportView.searchBar.date.optiongroup.started.label"));
        	searchDateType.addItem(Literals.DATE_LAST_UPDATED);
        	searchDateType.setItemCaption(Literals.DATE_LAST_UPDATED, vaadinUi.getMsg("ReportView.searchBar.date.optiongroup.lastUpdated.label"));
        	searchDateType.addItem(Literals.DATE_CANCELS);
        	searchDateType.setItemCaption(Literals.DATE_CANCELS, vaadinUi.getMsg("ReportView.searchBar.date.optiongroup.cancels.label"));
        	searchDateType.addItem(Literals.DATE_EXPIRES);
        	searchDateType.setItemCaption(Literals.DATE_EXPIRES, vaadinUi.getMsg("ReportView.searchBar.date.optiongroup.expires.label"));
        	searchDateType.setValue( propDateTypeEsfString.toString() );
	    	searchBar.addComponent(searchDateType,3,0,4,0);
	    	searchBar.setComponentAlignment(searchDateType, Alignment.MIDDLE_CENTER);
	    	
	    	HorizontalLayout searchDateRangeLayout = new HorizontalLayout();
	    	searchDateRangeLayout.setMargin(false);
	    	searchDateRangeLayout.setSpacing(true);
	    	searchBar.addComponent(searchDateRangeLayout,3,1,4,1);
	    	
        	EsfDate userFromDate;
        	EsfString propFromDateString = vaadinUi.getUser().getUserDefinedStringProperty(PROP_FROM_DATE+reportTemplateIdPropertySuffix);
        	if ( propFromDateString == null || propFromDateString.isNull() ) {
        		userFromDate = new EsfDate(vaadinUi.getUser().getTimezoneTz());
        	} else if ( propFromDateString.isBlank() ) {
        		userFromDate = null;
        	} else {
        		userFromDate = EsfDate.CreateFromYMD(propFromDateString.toString());
        	}
	    	searchFromDate = new PopupDateField();
	    	searchFromDate.setImmediate(true);
	    	searchFromDate.setDateFormat(vaadinUi.getEsfapp().getDefaultDateFormat());
	    	searchFromDate.setTimeZone(vaadinUi.getUser().getTimezoneTz());
	    	searchFromDate.setInputPrompt(vaadinUi.getMsg("ReportView.searchBar.searchFromDate.label"));
	    	searchFromDate.setDescription(vaadinUi.getMsg("ReportView.searchBar.searchFromDate.tooltip"));
	    	searchFromDate.setResolution(Resolution.DAY);
	    	searchFromDate.setValue(userFromDate == null || userFromDate.isNull() ? null : userFromDate.toDate());
	    	//searchFromDate.setLocale(vaadinUi.getLocale());

        	EsfDate userToDate;
        	EsfString propToDateString = vaadinUi.getUser().getUserDefinedStringProperty(PROP_TO_DATE+reportTemplateIdPropertySuffix);
        	if ( propToDateString == null || propToDateString.isBlank() ) {
        		userToDate = null;
        	} else {
        		userToDate = EsfDate.CreateFromYMD(propToDateString.toString());
        	}
	    	searchToDate = new PopupDateField();
	    	searchToDate.setImmediate(true);
	    	searchToDate.setDateFormat(vaadinUi.getEsfapp().getDefaultDateFormat());
	    	searchToDate.setTimeZone(vaadinUi.getUser().getTimezoneTz());
	    	searchToDate.setInputPrompt(vaadinUi.getMsg("ReportView.searchBar.searchToDate.label"));
	    	searchToDate.setDescription(vaadinUi.getMsg("ReportView.searchBar.searchToDate.tooltip"));
	    	searchToDate.setResolution(Resolution.DAY);
	    	searchToDate.setValue(userToDate == null || userToDate.isNull() ? null : userToDate.toDate());
	    	//searchToDate.setLocale(vaadinUi.getLocale());

	    	// We put this after the date range since setting its value will change what appears in those fields
        	EsfString propDateRangeEsfString = vaadinUi.getUser().getUserDefinedStringProperty(PROP_DATE_RANGE+reportTemplateIdPropertySuffix);
        	if ( propDateRangeEsfString == null || propDateRangeEsfString.isBlank() )
        		propDateRangeEsfString = new EsfString(Literals.DATE_LAST_7_DAYS);
	    	searchDateRange = new NativeSelect(vaadinUi.getMsg("ReportView.searchBar.daterange.select.label"));
	    	searchDateRange.setDescription(vaadinUi.getMsg("ReportView.searchBar.daterange.select.tooltip"));
	    	searchDateRange.addValidator(new SelectValidator(searchDateRange));
	    	searchDateRange.setNullSelectionAllowed(false); 
	    	searchDateRange.setImmediate(true);
	    	searchDateRange.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
	    	searchDateRange.addItem(Literals.DATE_TODAY);
	    	searchDateRange.setItemCaption( Literals.DATE_TODAY, vaadinUi.getMsg("ReportView.searchBar.daterange.select.today.label") );
	    	searchDateRange.addItem(Literals.DATE_YESTERDAY);
	    	searchDateRange.setItemCaption( Literals.DATE_YESTERDAY, vaadinUi.getMsg("ReportView.searchBar.daterange.select.yesterday.label") );
	    	searchDateRange.addItem(Literals.DATE_LAST_7_DAYS);
	    	searchDateRange.setItemCaption( Literals.DATE_LAST_7_DAYS, vaadinUi.getMsg("ReportView.searchBar.daterange.select.last7.label") );
	    	searchDateRange.addItem(Literals.DATE_LAST_30_DAYS);
	    	searchDateRange.setItemCaption( Literals.DATE_LAST_30_DAYS, vaadinUi.getMsg("ReportView.searchBar.daterange.select.last30.label") );
	    	searchDateRange.addItem(Literals.DATE_LAST_90_DAYS);
	    	searchDateRange.setItemCaption( Literals.DATE_LAST_90_DAYS, vaadinUi.getMsg("ReportView.searchBar.daterange.select.last90.label") );
	    	searchDateRange.addItem(Literals.DATE_LAST_365_DAYS);
	    	searchDateRange.setItemCaption( Literals.DATE_LAST_365_DAYS, vaadinUi.getMsg("ReportView.searchBar.daterange.select.last365.label") );
	    	searchDateRange.addItem(Literals.DATE_RANGE);
	    	searchDateRange.setItemCaption( Literals.DATE_RANGE, vaadinUi.getMsg("ReportView.searchBar.daterange.select.range.label") );
	    	searchDateRange.addValueChangeListener(new Property.ValueChangeListener() {
				private static final long serialVersionUID = 8198794806078201364L;

				@Override
				public void valueChange(ValueChangeEvent event) {
					String selectedDateRange = event.getProperty().getValue().toString();
					if ( Literals.DATE_RANGE.equals(selectedDateRange) ) {
						searchFromDate.setEnabled(true);
						searchToDate.setEnabled(true);
					} else {
						calculateDateRange(selectedDateRange);
						searchFromDate.setEnabled(false);
						searchToDate.setEnabled(false);
					}
				}
	    	});
	    	searchDateRange.setValue(propDateRangeEsfString.toString());
	    	searchDateRangeLayout.addComponent(searchDateRange);
	    	searchDateRangeLayout.setComponentAlignment(searchDateRange, Alignment.BOTTOM_CENTER);
	    	searchDateRangeLayout.addComponent(searchFromDate);
	    	searchDateRangeLayout.setComponentAlignment(searchFromDate, Alignment.BOTTOM_CENTER);
	    	searchDateRangeLayout.addComponent(searchToDate);
	    	searchDateRangeLayout.setComponentAlignment(searchToDate, Alignment.BOTTOM_CENTER);
	    	
	    	EsfInteger maxTransactions = vaadinUi.getUser().getUserDefinedIntegerProperty(PROP_MAX_TRANSACTIONS+reportTemplateIdPropertySuffix);
	    	if ( maxTransactions == null ) 
	    		maxTransactions = new EsfInteger(100);
	    	else if ( maxTransactions.isNull() || ! maxTransactions.isPositive() ) // don't allow "unlimited" to remain a default
	    		maxTransactions.setValue(100);
	    	maxFoundSelect = new NativeSelect(vaadinUi.getMsg("ReportView.searchBar.maxFoundSelect.label"));
	    	maxFoundSelect.setNullSelectionAllowed(false);
	    	maxFoundSelect.addItem(25);
	    	maxFoundSelect.setItemCaption(25, vaadinUi.getMsg("ReportView.searchBar.maxFoundSelect.number",25));
	    	maxFoundSelect.addItem(50);
	    	maxFoundSelect.setItemCaption(50, vaadinUi.getMsg("ReportView.searchBar.maxFoundSelect.number",50));
	    	maxFoundSelect.addItem(100);
	    	maxFoundSelect.setItemCaption(100, vaadinUi.getMsg("ReportView.searchBar.maxFoundSelect.number",100));
	    	maxFoundSelect.addItem(250);
	    	maxFoundSelect.setItemCaption(250, vaadinUi.getMsg("ReportView.searchBar.maxFoundSelect.number",250));
	    	maxFoundSelect.addItem(500);
	    	maxFoundSelect.setItemCaption(500, vaadinUi.getMsg("ReportView.searchBar.maxFoundSelect.number",500));
	    	maxFoundSelect.addItem(750);
	    	maxFoundSelect.setItemCaption(750, vaadinUi.getMsg("ReportView.searchBar.maxFoundSelect.number",750));
	    	maxFoundSelect.addItem(0);
	    	maxFoundSelect.setItemCaption(0, vaadinUi.getMsg("ReportView.searchBar.maxFoundSelect.unlimited"));
	    	maxFoundSelect.setValue(maxTransactions.toInt());
	    	searchDateRangeLayout.addComponent(maxFoundSelect);
	    	searchDateRangeLayout.setComponentAlignment(maxFoundSelect, Alignment.BOTTOM_CENTER);
	    	
        	EsfString propPartyEmail = vaadinUi.getUser().getUserDefinedStringProperty(PROP_PARTY_EMAIL+reportTemplateIdPropertySuffix);
	    	searchPartyEmail = new TextField(vaadinUi.getMsg("ReportView.searchBar.searchPartyEmail.label"));
	    	ResetButtonForTextField.extend(searchPartyEmail);
	    	searchPartyEmail.setStyleName("searchPartyEmail");
	    	searchPartyEmail.setNullRepresentation("");
	    	searchPartyEmail.setImmediate(false);
	    	searchPartyEmail.setDescription(vaadinUi.getMsg("ReportView.searchBar.searchPartyEmail.tooltip"));
	    	searchPartyEmail.setValue(propPartyEmail == null ? null : propPartyEmail.toPlainString());
	    	searchBar.addComponent(searchPartyEmail,1,0);
	    	
	    	EsfUUID userTranId;
        	EsfString propId = vaadinUi.getUser().getUserDefinedStringProperty(PROP_ID+reportTemplateIdPropertySuffix);
        	if ( propId == null || propId.isBlank() ) {
        		userTranId = null;
        	} else {
        		userTranId = new EsfUUID(propId.toPlainString());
        	}
	    	searchId = new TextField();
	    	ResetButtonForTextField.extend(searchId);
	    	searchId.setStyleName("searchId");
	    	searchId.setNullRepresentation("");
	    	searchId.addValidator( new EsfUUIDValidator() );
	    	searchId.setImmediate(true);
	    	searchId.setInputPrompt(vaadinUi.getMsg("ReportView.searchBar.searchId.label"));
	    	searchId.setDescription(vaadinUi.getMsg("ReportView.searchBar.searchId.tooltip"));
	    	searchId.setValue(userTranId == null || userTranId.isNull() ? null : userTranId.toPlainString());
	    	searchBar.addComponent(searchId,1,1);
	    	
	    	HorizontalLayout statusLayout = new HorizontalLayout();
	    	statusLayout.setSpacing(true);
	    	statusLayout.setMargin(false);
        	EsfBoolean propInProgress = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_IN_PROGRESS+reportTemplateIdPropertySuffix);
        	if ( propInProgress == null ) {
        		propInProgress = new EsfBoolean(true);
        	}

			searchInProgress = new CheckBox(vaadinUi.getMsg("ReportView.searchBar.searchInProgress.label"));
			searchInProgress.setDescription(vaadinUi.getMsg("ReportView.searchBar.searchInProgress.tooltip"));
			searchInProgress.setValue(propInProgress.isTrue());
			statusLayout.addComponent(searchInProgress);
	    	
        	EsfBoolean propCompleted = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_COMPLETED+reportTemplateIdPropertySuffix);
        	if ( propCompleted == null ) {
        		propCompleted = new EsfBoolean(true);
        	}
	    	searchCompleted = new CheckBox(vaadinUi.getMsg("ReportView.searchBar.searchCompleted.label"));
	    	searchCompleted.setDescription(vaadinUi.getMsg("ReportView.searchBar.searchCompleted.tooltip"));
	    	searchCompleted.setValue(propCompleted.isTrue());
	    	statusLayout.addComponent(searchCompleted);
	    	
        	EsfBoolean propCanceled = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_CANCELED+reportTemplateIdPropertySuffix);
        	if ( propCanceled == null ) {
        		propCanceled = new EsfBoolean(true);
        	}
	    	searchCanceled = new CheckBox(vaadinUi.getMsg("ReportView.searchBar.searchCanceled.label"));
	    	searchCanceled.setDescription(vaadinUi.getMsg("ReportView.searchBar.searchCanceled.tooltip"));
	    	searchCanceled.setValue(propCanceled.isTrue());
	    	statusLayout.addComponent(searchCanceled);
	    	
        	EsfBoolean propSuspended = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_SUSPENDED+reportTemplateIdPropertySuffix);
        	if ( propSuspended == null ) {
        		propSuspended = new EsfBoolean(true);
        	}
	    	searchSuspended = new CheckBox(vaadinUi.getMsg("ReportView.searchBar.searchSuspended.label"));
	    	searchSuspended.setDescription(vaadinUi.getMsg("ReportView.searchBar.searchSuspended.tooltip"));
	    	searchSuspended.setValue(propSuspended.isTrue());
	    	statusLayout.addComponent(searchSuspended);
	    	
        	EsfBoolean propStalled = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_STALLED+reportTemplateIdPropertySuffix);
        	if ( propStalled == null ) {
        		propStalled = new EsfBoolean(false);
        	}
	    	searchStalledOnly = new CheckBox(vaadinUi.getMsg("ReportView.searchBar.searchStalledOnly.label"));
	    	searchStalledOnly.setDescription(vaadinUi.getMsg("ReportView.searchBar.searchStalledOnly.tooltip"));
	    	searchStalledOnly.setImmediate(true);
	    	searchStalledOnly.addValueChangeListener( new ValueChangeListener() {
				private static final long serialVersionUID = 4963189317238802408L;

				@Override
				public void valueChange(ValueChangeEvent event) {
					Boolean isChecked = (Boolean)event.getProperty().getValue();
					searchInProgress.setEnabled(!isChecked);
					searchCompleted.setEnabled(!isChecked);
					searchCanceled.setEnabled(!isChecked);
					searchSuspended.setEnabled(!isChecked);
				}
	    	});
	    	statusLayout.addComponent(searchStalledOnly);
	    	searchStalledOnly.setValue(propStalled.isTrue()); // set later so valueChange will fire as necessary.
	    	
	    	searchBar.addComponent(statusLayout,0,2,2,2);
	    	
	    	buttonLayout = new HorizontalLayout();
	    	buttonLayout.setSpacing(true);
	    	buttonLayout.setStyleName("searchButtons");
	    	searchBar.addComponent(buttonLayout,3,2,4,2);
	    	
	    	findButtonLabel = vaadinUi.getMsg("ReportView.searchBar.findButton.label");
	    	findButton = new Button(findButtonLabel);
	    	findButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.showAll.icon")));
	    	findButton.setDescription(vaadinUi.getMsg("ReportView.searchBar.findButton.tooltip"));
	    	findButton.setStyleName(Reindeer.BUTTON_SMALL);
	    	findButton.setDisableOnClick(true);
	    	findButton.addClickListener( new Button.ClickListener() {			
				private static final long serialVersionUID = 3656538422103317071L;

				@Override
				public void buttonClick(ClickEvent event) {
					runReport();
					findButton.setEnabled(true);
				}
			});
	    	buttonLayout.addComponent(findButton);
	    	
	        ButtonGroup buttonGroup = new ButtonGroup();
	        buttonGroup.addStyleName(Reindeer.BUTTON_SMALL);
	        buttonLayout.addComponent(buttonGroup);
	    	
	    	// Need download CSV permission to do Excel and CSV downloads.
	    	if ( reportTemplate.hasDownloadCsvPermission(vaadinUi.getUser()) ) {
		    	excelButton = new Button(vaadinUi.getMsg("ReportView.searchBar.excelButton.label"));
		    	excelButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportView.searchBar.excelButton.icon")));
		    	excelButton.setDescription(vaadinUi.getMsg("ReportView.searchBar.excelButton.tooltip"));
		    	excelButton.setStyleName(Reindeer.BUTTON_SMALL);
		    	excelButton.setDisableOnClick(true);
		    	excelButton.addClickListener(new Button.ClickListener() {
					private static final long serialVersionUID = 5435784925662656460L;

					public void buttonClick(final ClickEvent event) {
						EsfVaadinUI.getInstance().ensureLoggedIn();
						
						// Collapse columns we don't need for excel like button fields
						List<Object> collapsedPropertyIds = getForcedColumnCollapsePropertyIds();

						String reportName = reportTemplate.getPathName().toString().replaceAll("/", "-");
		            	ExcelExport excelExport = new ExcelExport(table,reportName);
		                excelExport.excludeCollapsedColumns();
		                excelExport.setUseTableFormatPropertyValue(true);
		                excelExport.setExportFileName(reportName+".xls");
		                excelExport.setDisplayTotals(false);
		                excelExport.export();
		                
		                // Restore any we collapsed
		                restoreForcedColumnCollapsePropertyIds(collapsedPropertyIds);
		            
		                excelButton.setEnabled(true);
		            }
		        });
		    	buttonGroup.addButton(excelButton);
		    	excelButton.setVisible(false);

		    	csvButton = new Button(vaadinUi.getMsg("ReportView.searchBar.csvButton.label"));
		    	csvButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportView.searchBar.csvButton.icon")));
		    	csvButton.setDescription(vaadinUi.getMsg("ReportView.searchBar.csvButton.tooltip"));
		    	csvButton.setStyleName(Reindeer.BUTTON_SMALL);
		    	csvButton.setDisableOnClick(true);
		    	csvButton.addClickListener(new Button.ClickListener() {
					private static final long serialVersionUID = -899275988162025964L;

					public void buttonClick(final ClickEvent event) {
						EsfVaadinUI.getInstance().ensureLoggedIn();
						
						// Collapse columns we don't need for excel like button fields
						List<Object> collapsedPropertyIds = getForcedColumnCollapsePropertyIds();

		                String reportName = reportTemplate.getPathName().toString().replaceAll("/", "-");
						CsvExport csvExport = new CsvExport(table,reportName);
						csvExport.excludeCollapsedColumns();
						csvExport.setUseTableFormatPropertyValue(true);
						csvExport.setExportFileName(reportName+".csv");
						csvExport.setDisplayTotals(false);
						csvExport.export();
		                
		                // Restore any we collapsed
		                restoreForcedColumnCollapsePropertyIds(collapsedPropertyIds);
		            
						csvButton.setEnabled(true);
		            }
		        });
		    	buttonGroup.addButton(csvButton);
		    	csvButton.setVisible(false);
	    	}

	    	if ( reportTemplate.hasDownloadArchivePermission(vaadinUi.getUser()) ) {
	    		exportButton = new Button(vaadinUi.getMsg("ReportView.searchBar.exportButton.label"));
	    		exportButton.setDescription(vaadinUi.getMsg("ReportView.searchBar.exportButton.tooltip"));
	    		exportButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportView.searchBar.exportButton.icon")));
	    		exportButton.setStyleName(Reindeer.BUTTON_SMALL);
	    		exportButton.setDisableOnClick(true);
	    		exportButton.addClickListener(new Button.ClickListener() {
					private static final long serialVersionUID = 4528386345601555339L;

					public void buttonClick(final ClickEvent event) {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						vaadinUi.ensureLoggedIn();
			    		WebBrowser wb = Page.getCurrent().getWebBrowser();
						String url = vaadinUi.getEsfapp().getExternalContextPath()+"/reports/?type=export-transaction-list&rtid="+reportTemplate.getId()+"&c="+foundTranListPickupCode;
						Page.getCurrent().open(url, wb.isChrome() || wb.isSafari() ? "_top" : "_blank");
						exportButton.setEnabled(true);
		            }
		        });
	    		buttonGroup.addButton(exportButton);
		    	exportButton.setVisible(false);
	    	}
	    	
	    	layout.addComponent(searchBarLayout);

	    	userSearchFields = reportTemplate.getSearchableReportTemplateReportFieldList();
	    	if ( userSearchFields.size() > 0 ) {
	        	userSearchLayout = new HorizontalLayout();
	        	userSearchLayout.setStyleName("userSearchLayout");
	        	userSearchLayout.setSpacing(true);
	        	userSearchLayout.setMargin(new MarginInfo(false,true,false,true));
	    		setupUserSearchFields();
		    	layout.addComponent(userSearchLayout);
	    	}
	    	
	    	table = new ReportTable(container);
	    	
	    	layout.addComponent(table);
	    	layout.setExpandRatio(table, 1);
	    	
	    	vaadinUi.getMainView().setSearchCurrentModeStyleName(searchBarLayout);
			
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("ReportView view exception", e.getMessage());
		}
	}
	
	// Finds all columns that are not our data fields (and not already collapsed) and collapses them for the export Excel or CSV as these columns are not data columns.
	List<Object> getForcedColumnCollapsePropertyIds()
	{
		LinkedList<Object> collapsedPropertyIds = new LinkedList<Object>();
		for( Object propId : table.getContainerPropertyIds() )
		{
			Class<?> propClass = table.getContainerDataSource().getType(propId);
			if ( ! EsfValue.class.isAssignableFrom(propClass) && ! table.isColumnCollapsed(propId) )
			{
				table.setColumnCollapsed(propId, true);
				collapsedPropertyIds.add(propId);
			}
		}
		return collapsedPropertyIds;
	}
	
	// Restores the columns from getForcedColumnCollapsePropertyIds()
    void restoreForcedColumnCollapsePropertyIds(List<Object> collapsedPropertyIds)
    {
    	for( Object propId : collapsedPropertyIds )
    		table.setColumnCollapsed(propId, false);
    }

	
	void setupUserSearchFields() {	
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	for( ReportTemplateReportField rtrf : userSearchFields ) {
    		ReportFieldTemplate rft = rtrf.getReportFieldTemplate();
    		if ( rft.isFieldTypeDate() ) {
    			HorizontalLayout dateLayout = new HorizontalLayout();
    			dateLayout.setMargin(false);
    			dateLayout.setSpacing(false);
    			dateLayout.setStyleName("userSearchFieldDateLayout");
    			PopupDateField searchFieldFromDate = new PopupDateField();
    			searchFieldFromDate.setStyleName("userSearchFieldFromDate");
    			searchFieldFromDate.setCaption(vaadinUi.getMsg("ReportView.userSearch.searchFieldFromDate.label",rtrf.getFieldLabel()));
    			searchFieldFromDate.setInputPrompt(rtrf.getOutputFormatSpec());
    			searchFieldFromDate.setDescription(vaadinUi.getMsg("ReportView.userSearch.searchFieldFromDate.tooltip"));
    			searchFieldFromDate.setDateFormat(rtrf.getOutputFormatSpec());
    			searchFieldFromDate.setTimeZone(vaadinUi.getUser().getTimezoneTz());
    			searchFieldFromDate.setResolution(Resolution.DAY);
    			dateLayout.addComponent(searchFieldFromDate);
    			
    			PopupDateField searchFieldToDate = new PopupDateField();
    			searchFieldToDate.setStyleName("userSearchFieldToDate");
    			searchFieldToDate.setCaption(vaadinUi.getMsg("ReportView.userSearch.searchFieldToDate.label",rtrf.getFieldLabel()));
    			searchFieldToDate.setInputPrompt(rtrf.getOutputFormatSpec());
    			searchFieldToDate.setDescription(vaadinUi.getMsg("ReportView.userSearch.searchFieldToDate.tooltip"));
    			searchFieldToDate.setDateFormat(rtrf.getOutputFormatSpec());
    			searchFieldToDate.setTimeZone(vaadinUi.getUser().getTimezoneTz());
    			searchFieldToDate.setResolution(Resolution.DAY);
    			dateLayout.addComponent(searchFieldToDate);
    			
    			userSearchLayout.addComponent(dateLayout);
    		} else {
    			
            	EsfString propString = vaadinUi.getUser().getUserDefinedStringProperty(PROP_USER_SEARCHFIELD+reportTemplateIdPropertySuffix+"/"+rtrf.getReportFieldTemplateId().toNormalizedEsfNameString());
    	    	TextField searchField = new TextField();
    	    	ResetButtonForTextField.extend(searchField);
    	    	searchField.setCaption(rtrf.getFieldLabel());
    	    	//searchField.setInputPrompt(rtrf.getFieldLabel());
    	    	searchField.setStyleName("userSearchField");
    	    	searchField.setNullRepresentation("");
    	    	searchField.setWidth(30, Unit.EX);
    	    	searchField.setImmediate(false);
    	    	searchField.setRequired(false);
    	    	searchField.setDescription(vaadinUi.getMsg("ReportView.userSearch.searchField.tooltip"));
    	    	searchField.setValue(propString != null && propString.isNonBlank() ? propString.toString() : "");
    	    	userSearchLayout.addComponent(searchField);
    		}
    	}
	}
	
	void setupFoundTransactions() {
		if ( foundTranList != null ) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			foundTranListPickupCode = vaadinUi.getEsfapp().getRandomKey().getLoginAuthorizationString();
			vaadinUi.getHttpSession().setAttribute(foundTranListPickupCode, foundTranList);
		}
	}
	
	void clearFoundTransactions() {
		if ( foundTranListPickupCode != null ) {
			EsfVaadinUI.getInstance().getHttpSession().removeAttribute(foundTranListPickupCode);
			foundTranListPickupCode = null;
		}
		if ( foundTranList != null ) {
			foundTranList.clear();
			foundTranList = null;
		}
	}
	
	void runReport() {
		clearFoundTransactions();

		if ( excelButton != null ) excelButton.setVisible(false);
		if ( csvButton != null ) csvButton.setVisible(false);
		if ( exportButton != null ) exportButton.setVisible(false);
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();
		User currentUser = vaadinUi.getUser();
		
		vaadinUi.getMainView().setSearchCurrentModeStyleName(searchBarLayout);
		
		// If in production mode, but the user cannot list production transactions, block this request.
		if ( vaadinUi.isProductionMode() && ! reportTemplate.hasViewProductionPermission(currentUser) ) {
			Errors errors = new Errors();
			errors.addError(vaadinUi.getMsg("ReportView.error.productionNotAllowed"));
			vaadinUi.show(errors);
			return;
		}
		
		if ( ! searchFromDate.isValid() || ! searchToDate.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.data.invalid"));
			return;
		}
		
		@SuppressWarnings("unchecked")
		Set<EsfUUID> transactionTemplateIds = (Set<EsfUUID>)transactionTemplateSelect.getValue();
		boolean anyTransactionTemplateIdRequested = false;
		LinkedList<EsfUUID> transactionTemplateIdList = new LinkedList<EsfUUID>();
		for( EsfUUID id : transactionTemplateIds ) {
			if ( id.equals(anyId) ) {
				anyTransactionTemplateIdRequested = true;
				transactionTemplateIdList.clear();
				transactionTemplateIdList.addAll(reportTemplate.getTransactionTemplateIdListForUser(currentUser));
				break;
			}
			transactionTemplateIdList.add(id);
		}
		if ( transactionTemplateIdList.size() == 0 ) {
			Errors errors = new Errors();
			errors.addError(vaadinUi.getMsg("ReportView.error.noTransactionTemplatesSelected"));
			vaadinUi.show(errors);
			return;
		}
		
		EsfUUID[] propTransactionTemplateIds;
		if ( anyTransactionTemplateIdRequested ) {
			propTransactionTemplateIds = new EsfUUID[1];
			propTransactionTemplateIds[0] = anyId;
		} else {
			propTransactionTemplateIds = new EsfUUID[transactionTemplateIdList.size()];
			transactionTemplateIdList.toArray(propTransactionTemplateIds);
		}
		currentUser.setUserDefinedUUIDsProperty(PROP_TRANSACTION_TEMPLATE_IDS+reportTemplateIdPropertySuffix, propTransactionTemplateIds);
		
		// Started by user id 
		@SuppressWarnings("unchecked")
		Set<EsfUUID> selectedStartedByUserIds = (Set<EsfUUID>)startedByUserSelect.getValue();
		EsfUUID[] propStartedByUserIds = new EsfUUID[selectedStartedByUserIds.size()];
		selectedStartedByUserIds.toArray(propStartedByUserIds);
		LinkedList<EsfUUID> startedByUserIdList = new LinkedList<EsfUUID>();
		for( EsfUUID id : selectedStartedByUserIds ) {
			if ( id.equals(externalUserId) )
				startedByUserIdList.add( new EsfUUID((String)null) );
			else if ( id.equals(meUserId) ) {
				EsfUUID myUserId = currentUser.getId();
				if ( ! startedByUserIdList.contains(myUserId) )
					startedByUserIdList.add(myUserId);
			}
			else if ( id.equals(anyId) ) {
				for( User u : allUsers ) {
					EsfUUID userId = u.getId();
					if ( ! startedByUserIdList.contains(userId) )
						startedByUserIdList.add(userId);
				}
			} else {
				if ( ! startedByUserIdList.contains(id) )
					startedByUserIdList.add(id);
			}
		}
		currentUser.setUserDefinedUUIDsProperty(PROP_STARTED_BY_USER_IDS+reportTemplateIdPropertySuffix, propStartedByUserIds);

		// Party to user id 
		@SuppressWarnings("unchecked")
		Set<EsfUUID> selectedPartyToUserIds = (Set<EsfUUID>)partyToUserSelect.getValue();
		EsfUUID[] propPartyToUserIds = new EsfUUID[selectedPartyToUserIds.size()];
		selectedPartyToUserIds.toArray(propPartyToUserIds);
		LinkedList<EsfUUID> partyToUserIdList = new LinkedList<EsfUUID>();
		for( EsfUUID id : selectedPartyToUserIds ) {
			if ( id.equals(meUserId) ) {
				EsfUUID myUserId = currentUser.getId();
				if ( ! partyToUserIdList.contains(myUserId) )
					partyToUserIdList.add(myUserId);
			}
			else if ( id.equals(anyId) ) {
				for( User u : allUsers ) {
					EsfUUID userId = u.getId();
					if ( ! partyToUserIdList.contains(userId) )
						partyToUserIdList.add(userId);
				}
			} else {
				if ( ! partyToUserIdList.contains(id) )
					partyToUserIdList.add(id);
			}
		}
		currentUser.setUserDefinedUUIDsProperty(PROP_PARTY_TO_USER_IDS+reportTemplateIdPropertySuffix, propPartyToUserIds);
		
		Tab currentTab = startedByPartyTabSheet.getTab(startedByPartyTabSheet.getSelectedTab());
		EsfBoolean isCurrentTabStartedBy = new EsfBoolean( currentTab == startedByUserTab );
		EsfBoolean isCurrentTabPartyTo = new EsfBoolean( currentTab == partyToUserTab );
		currentUser.setUserDefinedBooleanProperty(PROP_TABSHEET_STARTED_BY_TAB_ACTIVE+reportTemplateIdPropertySuffix, isCurrentTabStartedBy);
		currentUser.setUserDefinedBooleanProperty(PROP_TABSHEET_PARTY_TO_TAB_ACTIVE+reportTemplateIdPropertySuffix, isCurrentTabPartyTo);

		String dateType = (String)searchDateType.getValue();
		currentUser.setUserDefinedStringProperty(PROP_DATE_TYPE+reportTemplateIdPropertySuffix, new EsfString(dateType));
		
		String dateRange = (String)searchDateRange.getValue();
		currentUser.setUserDefinedStringProperty(PROP_DATE_RANGE+reportTemplateIdPropertySuffix, new EsfString(dateRange));
		
		Date fromDate = (Date)searchFromDate.getValue();
		EsfString propFromDateString = fromDate == null ? new EsfString("") : new EsfString((new EsfDate(fromDate)).toYMDString());
		currentUser.setUserDefinedStringProperty(PROP_FROM_DATE+reportTemplateIdPropertySuffix, propFromDateString);
		
		Date toDate = (Date)searchToDate.getValue();
		EsfString propToDateString = toDate == null ? new EsfString("") : new EsfString((new EsfDate(toDate)).toYMDString());
		currentUser.setUserDefinedStringProperty(PROP_TO_DATE+reportTemplateIdPropertySuffix, propToDateString);
		
		EsfDateTime esfFromDate = fromDate == null ? null : new EsfDateTime(fromDate);
		if ( esfFromDate != null ) {
			esfFromDate.normalizeTimeToZero(currentUser.getTimezoneTz());
		}
		EsfDateTime esfToDate = toDate == null ? null : new EsfDateTime(toDate);
		if ( esfToDate != null ) {
			esfToDate.normalizeTimeToEndOfDay(currentUser.getTimezoneTz());
		}
		
		EsfUUID tranId = null;
		String tranIdString = (String)searchId.getValue();
		if ( EsfString.isNonBlank(tranIdString) ) {
			tranId = new EsfUUID(tranIdString);
			if ( tranId.isNull() )
			{
				Errors errors = new Errors();
				errors.addError(vaadinUi.getMsg("ReportView.error.invalidTranId"));
				vaadinUi.show(errors);
				return;
			}
		}
		currentUser.setUserDefinedStringProperty(PROP_ID+reportTemplateIdPropertySuffix, new EsfString(tranIdString));
		
		String partyEmailString = (String)searchPartyEmail.getValue();
		currentUser.setUserDefinedStringProperty(PROP_PARTY_EMAIL+reportTemplateIdPropertySuffix, new EsfString(partyEmailString));
		
		Boolean showInProgress = searchInProgress.getValue();
		currentUser.setUserDefinedBooleanProperty(PROP_IN_PROGRESS+reportTemplateIdPropertySuffix, new EsfBoolean(showInProgress));
		
		Boolean showCompleted = searchCompleted.getValue();
		currentUser.setUserDefinedBooleanProperty(PROP_COMPLETED+reportTemplateIdPropertySuffix, new EsfBoolean(showCompleted));

		Boolean showCanceled = searchCanceled.getValue();
		currentUser.setUserDefinedBooleanProperty(PROP_CANCELED+reportTemplateIdPropertySuffix, new EsfBoolean(showCanceled));
		
		Boolean showSuspended = searchSuspended.getValue();
		currentUser.setUserDefinedBooleanProperty(PROP_SUSPENDED+reportTemplateIdPropertySuffix, new EsfBoolean(showSuspended));
		
		Boolean showStalledOnly = searchStalledOnly.getValue();
		if ( showStalledOnly && ! showInProgress ) {
			showInProgress = true;
			currentUser.setUserDefinedBooleanProperty(PROP_IN_PROGRESS+reportTemplateIdPropertySuffix, new EsfBoolean(showInProgress));
		}
		currentUser.setUserDefinedBooleanProperty(PROP_STALLED+reportTemplateIdPropertySuffix, new EsfBoolean(showStalledOnly));
		
		if ( ! showInProgress && ! showCompleted && ! showCanceled && ! showSuspended ) {
			Errors errors = new Errors();
			errors.addError(vaadinUi.getMsg("ReportView.error.noStatusSelected"));
			vaadinUi.show(errors);
			return;
		}
		
		LinkedList<ReportTemplateReportField> actualBuiltInSearchFields = new LinkedList<ReportTemplateReportField>();
		LinkedList<ReportTemplateReportField> actualUserSearchFields = new LinkedList<ReportTemplateReportField>();
		int userSearchIndex = 0;
    	for( ReportTemplateReportField rtrf : userSearchFields ) {
    		Component c = userSearchLayout.getComponent(userSearchIndex++);
    		
    		ReportFieldTemplate reportFieldTemplate = rtrf.getReportFieldTemplate();
    		if ( reportFieldTemplate.isFieldTypeDate() ) {
    			HorizontalLayout dateLayout = (HorizontalLayout)c;
    			PopupDateField searchFieldFromDate = (PopupDateField)dateLayout.getComponent(0);
    			PopupDateField searchFieldToDate = (PopupDateField)dateLayout.getComponent(1);
    			EsfDate searchValueFromDate = new EsfDate(searchFieldFromDate.getValue());
    			EsfDate searchValueToDate = new EsfDate(searchFieldToDate.getValue());
    			if ( ! searchValueFromDate.isNull() || ! searchValueToDate.isNull() ) {
    				EsfDate[] searchValueDates = { searchValueFromDate, searchValueToDate };
    				rtrf.setFieldValues(searchValueDates);
    				actualUserSearchFields.add(rtrf);
    			}
    		} else {
    	    	TextField searchField = (TextField)c;
    	    	EsfString searchValue = new EsfString((String)searchField.getValue());
	    		searchValue = searchValue.trim();
    	    	if ( searchValue.isNonBlank() ) {
    	    		rtrf.setFieldValue(searchValue);
    	    		searchField.setValue(searchValue.toString());
    	    		if ( reportFieldTemplate.isFieldTypeBuiltIn() ) {
    	    			actualBuiltInSearchFields.add(rtrf);
    	    		} else {
    	    			actualUserSearchFields.add(rtrf);
    	    		}
    	    	}
	    		currentUser.setUserDefinedStringProperty(PROP_USER_SEARCHFIELD+reportTemplateIdPropertySuffix+"/"+rtrf.getReportFieldTemplateId().toNormalizedEsfNameString(), new EsfString(searchValue));
    		}
    	}
		
    	String searchType = vaadinUi.isProductionMode() ? vaadinUi.getMsg("transaction.production") : vaadinUi.getMsg("transaction.test");
    	int maxToFind = (int)maxFoundSelect.getValue();
    	currentUser.setUserDefinedIntegerProperty(PROP_MAX_TRANSACTIONS+reportTemplateIdPropertySuffix,new EsfInteger(maxToFind));

    	foundTranList = ReportListingInfo.Manager.getMatching(reportTemplate,tranId,
    						dateType,esfFromDate,esfToDate,transactionTemplateIdList,
    						isCurrentTabStartedBy.isTrue() ? startedByUserIdList : null,
    						isCurrentTabPartyTo.isTrue() ? partyToUserIdList : null,
    						partyEmailString,
    						vaadinUi.isProductionMode(),showInProgress,showCompleted,showCanceled,showSuspended,showStalledOnly,
    						actualBuiltInSearchFields,actualUserSearchFields,maxToFind > 0 ? maxToFind+1 : 0);
    	if ( foundTranList == null ) {
			Errors errors = new Errors();
			errors.addError(vaadinUi.getMsg("ReportView.error.reportFailed"));
			vaadinUi.show(errors);
			return;
    	}
    	
    	// Make sure our tran id matches our allowed types
    	if ( tranId != null && foundTranList.size() == 1 ) {
    		boolean foundTranIsAllowed = true;
    		ReportListingInfo foundTran = foundTranList.get(0);
    		TransactionTemplate foundTranTemplate = TransactionTemplate.Manager.getByPathName(foundTran.getTemplatePathName());
    		if ( foundTranTemplate == null )
    			foundTranIsAllowed = false;
    		else if ( ! reportTemplate.getTransactionTemplateIdList().contains(foundTranTemplate.getId()) )
    			foundTranIsAllowed = false;
    		else {// let's check on the started by
    			Transaction tran = Transaction.Manager.getById(foundTran.getId());
    			if ( tran == null )
    				foundTranIsAllowed = false;
    			else {
    				if ( tran.hasCreatedByUserId() ) {
    					if ( tran.getCreatedByUserId().equals(currentUser.getId()) )
    						/*allowed*/;
    					else {
    						boolean foundInAllUsers = false;
    						for( User u : allUsers ) {
    							if ( tran.getCreatedByUserId().equals(u.getId()) ) {
    								foundInAllUsers = true;
    								break;
    							}
    						}
    						if ( ! foundInAllUsers )
    							foundTranIsAllowed = false;
    					}
    				} else if ( ! reportTemplate.hasViewStartedByExternalUsersPermission(currentUser) ) {
    					foundTranIsAllowed = false;
    				}
    			}
    		}
    		if ( ! foundTranIsAllowed ) {
    			clearFoundTransactions();
    			Errors errors = new Errors();
    			errors.addError(vaadinUi.getMsg("ReportView.error.noPermissionToTranId",tranId));
    			vaadinUi.show(errors);
    			return;
    		}
    	}
    	
		String findButtonSuffix;
    	if ( foundTranList.size() > maxToFind && maxToFind > 0 ) {
    		foundTranList.remove(foundTranList.size()-1); // remove that extra one we did to see if there are more
    		findButtonSuffix = "+)";
    	} else {
    		findButtonSuffix = ")";
    	}

    	container.refresh(foundTranList);		
		
    	findButton.setCaption(findButtonLabel + " (" + container.size() + findButtonSuffix);

		boolean hasBothDates = esfToDate != null && esfFromDate != null;
		if ( esfToDate == null )
			esfToDate = new EsfDateTime();
		if ( esfFromDate == null )
			esfFromDate = new EsfDateTime();
		long diffDays = (esfToDate.toNumber() - esfFromDate.toNumber()) / (1000L * 60 * 60 * 24);
		if ( hasBothDates )
			++diffDays;
		_logger.debug("runReport() - Report '" + reportTemplate.getPathName() + "' found " + container.size() + " " + searchType +
				" transactions over " + diffDays + " days each holding " + reportTemplate.getReportTemplateReportFieldListInternal().size() + 
				" columns. Export allowed: " + (exportButton!=null) +" for user: " + currentUser.getFullDisplayName());
		
		vaadinUi.showStatus(vaadinUi.getMsg("ReportView.findButton.findCount",container.size(),searchType));
		vaadinUi.getMainView().setTabTitle(thisView, vaadinUi.getMsg("ReportView.afterSearch.tabTitle",
				reportTemplate.getPathName(),reportTemplate.getDisplayName(),reportTemplate.getDescription(),container.size()));

		if ( container.size() > 0 ) {
			if ( excelButton != null ) excelButton.setVisible(true);
			if ( csvButton != null ) csvButton.setVisible(true);
			if ( exportButton != null ) {
				setupFoundTransactions();
				exportButton.setVisible(true);
			} else {
				clearFoundTransactions(); // we only need to keep this around if they can possibly export them
			}
		} else {
			clearFoundTransactions();
		}
		
	}
	
	void calculateDateRange(String selectedDateRange) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		User currentUser = vaadinUi.getUser();
		TimeZone tz = currentUser == null ? com.esignforms.open.util.DateUtil.getDefaultTimeZone() : currentUser.getTimezoneTz();
		
		if ( Literals.DATE_TODAY.equals(selectedDateRange) ) {
			searchFromDate.setValue( new EsfDate(tz).toDate() );
			searchToDate.setValue(null);
		} else if ( Literals.DATE_YESTERDAY.equals(selectedDateRange) ) {
			EsfDate startday = new EsfDate(tz);
			startday.addDays(-1);
			searchFromDate.setValue(startday.toDate());
			searchToDate.setValue(startday.toDate());
		} else if ( Literals.DATE_LAST_7_DAYS.equals(selectedDateRange) ) {
			EsfDate startday = new EsfDate(tz);
			startday.addDays(-7);
			searchFromDate.setValue(startday.toDate());
			searchToDate.setValue(null);
		} else if ( Literals.DATE_LAST_30_DAYS.equals(selectedDateRange) ) {
			EsfDate startday = new EsfDate(tz);
			startday.addDays(-30);
			searchFromDate.setValue(startday.toDate());
			searchToDate.setValue(null);
		} else if ( Literals.DATE_LAST_90_DAYS.equals(selectedDateRange) ) {
			EsfDate startday = new EsfDate(tz);
			startday.addDays(-90);
			searchFromDate.setValue(startday.toDate());
			searchToDate.setValue(null);
		} else if ( Literals.DATE_LAST_365_DAYS.equals(selectedDateRange) ) {
			EsfDate startday = new EsfDate(tz);
			startday.addDays(-365);
			searchFromDate.setValue(startday.toDate());
			searchToDate.setValue(null);
		}
	}
	
	@Override
	public void detach() {
		clearFoundTransactions();
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();
		reportTemplateIdParam = params;
		EsfUUID reportTemplateId = new EsfUUID(reportTemplateIdParam);
		if ( reportTemplateId.isNull() )
			vaadinUi.showError("Invalid report id parameter", "The report template id passed in is not valid.");
		else {
			reportTemplate = ReportTemplate.Manager.getById(reportTemplateId);
			if ( reportTemplate.isEnabled() && reportTemplate.hasRunReportPermission(vaadinUi.getUser()) ) {
		    	buildLayout();
			}
			else
				vaadinUi.showError("No permission", "The report template is not enabled or you do not have permission to run it.");
		}
	}

	@Override
	public String checkDirty() {
		return "";
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
	class ReportTable extends PropertyFormatTable {
		private static final long serialVersionUID = 4336922194369555125L;

		public ReportTable(ReportContainer container) {
			super();
	    	
			setContainerDataSource(container);
			
			Object[] fieldNameList = container.getFieldNameList();
			setVisibleColumns(fieldNameList);
			setColumnHeaders(container.getFieldHeaderList());
			
			Align[] fieldAlignmentList = container.getFieldAlignmentList();
			for( int i=0; i < fieldNameList.length; ++i ) {
				setColumnAlignment(fieldNameList[i], fieldAlignmentList[i]);				
			}
			
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setSizeFull();
			setSelectable(true);
			setImmediate(true);
	        addValueChangeListener(new Property.ValueChangeListener() {
				private static final long serialVersionUID = -1998484397698781429L;

				public void valueChange(Property.ValueChangeEvent event) {
					Object eventValue = getValue(); 
    	        	Item item = getItem(eventValue);
    	        	
    	        	if ( item == null )
    	        		return;
    	        	
    	    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	    		vaadinUi.ensureLoggedIn();

    	    		EsfUUID tranId = (EsfUUID)eventValue;
    	        	Transaction tran = Transaction.Manager.getById(tranId);
    	        	if ( tran == null ) {
    	        		Errors errors = new Errors();
    	        		errors.addError(vaadinUi.getMsg("ReportView.error.tranNotFound",tranId));
    	        		vaadinUi.show(errors);
    	        		return;
    	        	}
    	        	
    	            final ReportDetailView detailView = new ReportDetailView(tran,reportTemplate);
    	            detailView.initView();
    	    		
    	            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("ReportDetailView.window.caption",tranId), detailView);
    	            w.center();
    	        	w.setWidth(90, Unit.PERCENTAGE);
    	        	w.setHeight(90, Unit.PERCENTAGE);

    	        	detailView.activateView(EsfView.OpenMode.WINDOW, "");
    	        	vaadinUi.addWindowToUI(w);
    	            select(null);
	            }
	        });
	        
	        setCellStyleGenerator( new CellStyleGenerator() {
				private static final long serialVersionUID = 134813707026618374L;

				@Override
				public String getStyle(Table table, Object itemId, Object propertyId) {
					if ( propertyId != null ) {
						ReportContainer.ColID uuidAndColumn = (ReportContainer.ColID)propertyId;
						ReportTemplateReportField rtrf = reportTemplate.getReportTemplateReportField(uuidAndColumn.getId());
						if ( rtrf != null ) {
							ReportFieldTemplate reportFieldTemplate = rtrf.getReportFieldTemplate();
							String fieldName = reportFieldTemplate.getFieldName().toString();
							if ( "esf_status".equals(fieldName) ) {
								Item item = getItem(itemId);
								EsfString status = (EsfString)item.getItemProperty(propertyId).getValue();
								if ( status == null )
									return null;
								
								if ( status.equals(Transaction.TRAN_STATUS_IN_PROGRESS) ) return "inprogress";
								if ( status.equals(Transaction.TRAN_STATUS_COMPLETED) ) return null;
								if ( status.equals(Transaction.TRAN_STATUS_CANCELED) ) return "canceled";
								if ( status.equals(Transaction.TRAN_STATUS_SUSPENDED) ) return "suspended";
								return "suspended";
							}
						}
					}
					return null;
				}});
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			ReportContainer.ColID uuidAndColumn = (ReportContainer.ColID)colId;
			ReportTemplateReportField rtrf = reportTemplate.getReportTemplateReportField(uuidAndColumn.getId());
			if ( rtrf != null ) {
				ReportFieldTemplate reportFieldTemplate = rtrf.getReportFieldTemplate();
				String fieldName = reportFieldTemplate.getFieldName().toString();
				
				// Note that the formatting options here are also present in tranExport/tranIndex.jsp for report listings for exported transactions
				if ( reportFieldTemplate.isFieldTypeStringOrRelated() ) {
					EsfString fieldValue = (EsfString)property.getValue();
					if ( fieldValue == null || fieldValue.isNull() )
						return "";
					if ( "left4mask".equals(rtrf.getOutputFormatSpec()) ) 
						return vaadinUi.getEsfapp().maskLeft(fieldValue.toString());
					if ( "right4mask".equals(rtrf.getOutputFormatSpec()) ) 
						return vaadinUi.getEsfapp().maskRight(fieldValue.toString());
					return fieldValue.toString();
				} 
				
				if ( reportFieldTemplate.isFieldTypeDate() ) {
					EsfDate fieldValue = (EsfDate)property.getValue();
					if ( fieldValue == null || fieldValue.isNull() )
						return "";
		   			String dateFormatSpec = rtrf.hasOutputFormatSpec() ? rtrf.getOutputFormatSpec() : vaadinUi.getEsfapp().getDefaultDateFormat();
					return fieldValue.format(dateFormatSpec);
				} 
				
				if ( reportFieldTemplate.isFieldTypeDecimal() ) {
					EsfDecimal fieldValue = (EsfDecimal)property.getValue();
					if ( fieldValue == null || fieldValue.isNull() )
						return "";
					if ( rtrf.hasOutputFormatSpec() )
						return fieldValue.format(rtrf.getOutputFormatSpec());
					return fieldValue.toString();
				} 
				
				if ( reportFieldTemplate.isFieldTypeInteger() ) {
					EsfInteger fieldValue = (EsfInteger)property.getValue();
					if ( fieldValue == null || fieldValue.isNull() )
						return "";
					if ( rtrf.hasOutputFormatSpec() )
						return fieldValue.format(rtrf.getOutputFormatSpec());
					return fieldValue.toString();
				} 
				if ( reportFieldTemplate.isFieldTypeBuiltIn() ) {
					if ( "esf_last_updated_timestamp".equals(fieldName) ||
						 "esf_start_timestamp".equals(fieldName) ||
						 "esf_cancel_timestamp".equals(fieldName) ||
						 "esf_expire_timestamp".equals(fieldName) ||
						 "esf_stall_timestamp".equals(fieldName)
					   ) {
							EsfDateTime fieldValue = (EsfDateTime)property.getValue();
							if ( fieldValue == null || fieldValue.isNull() )
								return "";
				   			String dateFormatSpec = rtrf.hasOutputFormatSpec() ? rtrf.getOutputFormatSpec() : vaadinUi.getEsfapp().getDefaultDateTimeFormat();
							return fieldValue.format(vaadinUi.getUser(),dateFormatSpec);
						}
						
					if ( "esf_last_updated_by_user".equals(fieldName) || "esf_created_by_user".equals(fieldName) ) {
						EsfUUID userId = (EsfUUID)property.getValue();
						if ( userId == null || userId.isNull() )
							return "";
						if ( rtrf.getOutputFormatSpec().equals("id") ) 
							return userId.toString();
						User user = User.Manager.getById(userId);
						if ( user == null )
							return "???";
						if ( "displayname".equals(rtrf.getOutputFormatSpec()) ) 
							return user.getDisplayName();
						if ( "fulldisplayname".equals(rtrf.getOutputFormatSpec()) ) 
							return user.getFullDisplayName();
						if ( "personalname".equals(rtrf.getOutputFormatSpec()) ) 
							return user.getPersonalName();
						if ( "familyname".equals(rtrf.getOutputFormatSpec()) ) 
							return user.getFamilyName();
						return user.getEmail();
					}
					
					if ( "esf_status".equals(fieldName) ) {
						EsfString status = (EsfString)property.getValue();
						if ( status == null || status.isNull() )
							return "";
							
						if ( status.equals(Transaction.TRAN_STATUS_IN_PROGRESS) ) return vaadinUi.getMsg("transaction.status.inProgress");
						if ( status.equals(Transaction.TRAN_STATUS_COMPLETED) ) return vaadinUi.getMsg("transaction.status.completed");
						if ( status.equals(Transaction.TRAN_STATUS_CANCELED) ) return vaadinUi.getMsg("transaction.status.canceled");
						if ( status.equals(Transaction.TRAN_STATUS_SUSPENDED) ) return vaadinUi.getMsg("transaction.status.suspended");
						return "???";
					}
				}
			}
			
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // ReportTable

}