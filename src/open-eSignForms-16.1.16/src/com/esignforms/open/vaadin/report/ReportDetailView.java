// Copyright (C) 2011-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.report;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.vaadin.hene.popupbutton.PopupButton;
import org.vaadin.peter.buttongroup.ButtonGroup;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.email.OutboundEmailMessage;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentIdPartyId;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionFile;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyAssignment;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.runtime.workflow.TransactionEngine;
import com.esignforms.open.servlet.TranSnapshotsDownload;
import com.esignforms.open.util.ServletUtil;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.log.email.EmailLogView;
import com.esignforms.open.vaadin.log.httpsend.HttpSendLogView;
import com.esignforms.open.vaadin.log.transaction.LogView;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.widget.OnDemandFileDownloader;
import com.esignforms.open.vaadin.widget.TranFilePopupButton;
import com.esignforms.open.vaadin.widget.TranSnapshotPopupButton;
import com.esignforms.open.vaadin.widget.TranSnapshotsDownloadPopupButton;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.WebBrowser;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

public class ReportDetailView extends Panel implements EsfView {
	private static final long serialVersionUID = 4776972564260789227L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ReportDetailView.class);

	Button refreshButton;
    PopupButton statusChangeButton;
    TextField statusChangeReason;
    TextField currentStatus;
    Label lastStatusText;
    TextField lastUpdated;
    TextField lastUpdatedByUser;
    
    LinkedList<String> downloadContextPickupCodeList = new LinkedList<String>();
    
    final ReportDetailView thisView;
    Transaction transaction;
    final ReportTemplate reportTemplate;
    PartyTreeTable partyTreeTable;
	
	public ReportDetailView(Transaction tran,ReportTemplate reportTemplate) {
		super();
		this.thisView = this;
		this.transaction = tran;
		this.reportTemplate = reportTemplate;
		setStyleName("ReportDetailView");
		setSizeFull();
	}
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();

		try {
			resetDownloadContextPickupCodeList();
			VerticalLayout layout = new VerticalLayout();
			setContent(layout);
			layout.setWidth(100, Unit.PERCENTAGE);
			layout.setSpacing(true);
			layout.setMargin(false);

			layout.removeAllComponents(); // we do this since on refresh, we need to get rid of what we previously built
			layout.addComponent(createOverview());
			partyTreeTable = createPartyTable();
			layout.addComponent(partyTreeTable);
			layout.addComponent(createDocumentPartyList());
	    	
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("TransactionDetail view exception", e.getMessage());
		}
	}
		
	protected GridLayout createOverview() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		GridLayout layout = new GridLayout(4,4);
		layout.setSpacing(true);
		layout.setMargin(true);
		layout.setColumnExpandRatio(0, 0.15f);
		layout.setColumnExpandRatio(1, 0.35f);
		layout.setColumnExpandRatio(2, 0.15f);
		layout.setColumnExpandRatio(3, 0.35f);
		layout.setWidth(100, Unit.PERCENTAGE);
		
        // The tran template name, refresh and clone buttons are together
        HorizontalLayout nameRefreshCloneLayout = new HorizontalLayout();
        nameRefreshCloneLayout.setSpacing(true);
        nameRefreshCloneLayout.setMargin(false);

		TextField tf = new TextField();
		tf.setWidth(100, Unit.PERCENTAGE);
		if ( transaction.isProduction() )
			tf.setCaption(vaadinUi.getMsg("ReportDetailView.productionTransactionTemplate.label"));
		else
			tf.setCaption(vaadinUi.getMsg("ReportDetailView.testTransactionTemplate.label"));
        tf.setValue(transaction.getTransactionTemplate().getPathName().toString());
    	tf.setReadOnly(true);
    	nameRefreshCloneLayout.addComponent(tf);
    	
    	VerticalLayout refreshCloneButtonLayout = new VerticalLayout();
    	refreshCloneButtonLayout.setMargin(false);
    	refreshCloneButtonLayout.setSpacing(true);
    	nameRefreshCloneLayout.addComponent(refreshCloneButtonLayout);
    	
		refreshButton = new Button(vaadinUi.getMsg("button.refresh.label"), new ClickListener() {
			private static final long serialVersionUID = 6172591221758001605L;

			@Override
			public void buttonClick(ClickEvent event) {
				Transaction.Manager.removeFromCache(transaction); // let's get it clean from the database
				transaction = Transaction.Manager.getById(transaction.getId());
				buildLayout();
			}
		});
		refreshButton.setDescription(vaadinUi.getMsg("button.refresh.tooltip"));
		refreshButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.refresh.icon")));
		refreshButton.setStyleName(Reindeer.BUTTON_SMALL);
		refreshButton.setDisableOnClick(true);
		refreshButton.setVisible(! transaction.isEndState());
		refreshCloneButtonLayout.addComponent(refreshButton);
		refreshCloneButtonLayout.setComponentAlignment(refreshButton, Alignment.TOP_LEFT);
   	
        final TransactionTemplate tranTemplate = transaction.getTransactionTemplate();
        if ( tranTemplate.hasStartPermission(vaadinUi.getUser()) && (tranTemplate.isProductionEnabled() || tranTemplate.isTestEnabled()) ) {
            PopupButton createLikeButton = new PopupButton(vaadinUi.getMsg("TransactionDetailView.button.createLike.popup.label"));
            createLikeButton.setDescription(vaadinUi.getMsg("TransactionDetailView.button.createLike.popup.tooltip"));
            createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("TransactionDetailView.button.createLike.popup.icon")));
            createLikeButton.setStyleName(Reindeer.BUTTON_SMALL);
        	VerticalLayout createLikeLayout = new VerticalLayout();
            createLikeLayout.setSizeUndefined();
            createLikeLayout.setMargin(false);
            createLikeLayout.setSpacing(true);
            createLikeButton.setContent(createLikeLayout);
            
            refreshCloneButtonLayout.addComponent(createLikeButton);
            refreshCloneButtonLayout.setComponentAlignment(createLikeButton, Alignment.BOTTOM_LEFT);
            
            if ( tranTemplate.isProductionEnabled() ) {
				Button subButton = new Button(vaadinUi.getMsg("TransactionDetailView.button.createLike.startProduction.label"), new ClickListener() {
					private static final long serialVersionUID = 2456068550604577903L;

					@Override
					public void buttonClick(ClickEvent event) {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						vaadinUi.getHttpSession().setAttribute(Transaction.SESSION_ATTR_CLONE_TRANSACTION_ID, transaction.getId().toString());
						vaadinUi.getMainView().startTransactionInBrowserWindow(tranTemplate.getId(),true,true);
						event.getButton().setEnabled(true);
						((PopupButton)event.getButton().getParent().getParent()).setPopupVisible(false);
					}
				});
				subButton.setDisableOnClick(true);
				subButton.setStyleName(Reindeer.BUTTON_LINK);
				createLikeLayout.addComponent(subButton);
            }

            if ( tranTemplate.isTestEnabled() ) {
				Button subButton = new Button(vaadinUi.getMsg("TransactionDetailView.button.createLike.startTest.label"), new ClickListener() {
					private static final long serialVersionUID = 1732919353141592997L;

					@Override
					public void buttonClick(ClickEvent event) {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						vaadinUi.getHttpSession().setAttribute(Transaction.SESSION_ATTR_CLONE_TRANSACTION_ID, transaction.getId().toString());
						vaadinUi.getMainView().startTransactionInBrowserWindow(tranTemplate.getId(),false,false);
						event.getButton().setEnabled(true);
						((PopupButton)event.getButton().getParent().getParent()).setPopupVisible(false);
					}
				});
				subButton.setDisableOnClick(true);
				subButton.setStyleName(Reindeer.BUTTON_LINK);
				createLikeLayout.addComponent(subButton);
            }

            if ( tranTemplate.isProductionEnabled() && tranTemplate.isTestEnabled() ) {
				Button subButton = new Button(vaadinUi.getMsg("TransactionDetailView.button.createLike.startTestLikeProduction.label"), new ClickListener() {
					private static final long serialVersionUID = 6349893980282869394L;

					@Override
					public void buttonClick(ClickEvent event) {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						vaadinUi.getHttpSession().setAttribute(Transaction.SESSION_ATTR_CLONE_TRANSACTION_ID, transaction.getId().toString());
						vaadinUi.getMainView().startTransactionInBrowserWindow(tranTemplate.getId(),false,true);
						event.getButton().setEnabled(true);
						((PopupButton)event.getButton().getParent().getParent()).setPopupVisible(false);
					}
				});
				subButton.setDisableOnClick(true);
				subButton.setStyleName(Reindeer.BUTTON_LINK);
				createLikeLayout.addComponent(subButton);
            }
        }
        layout.addComponent(nameRefreshCloneLayout, 0, 0);
		
		tf = new TextField();
		tf.setWidth(100, Unit.PERCENTAGE);
        tf.setCaption(vaadinUi.getMsg("ReportDetailView.package.label"));
        tf.setValue(transaction.getPackageVersion().getPackagePathNameVersionWithLabel());
    	tf.setReadOnly(true);
        layout.addComponent(tf, 1, 0);
		
		tf = new TextField();
		tf.setWidth(100, Unit.PERCENTAGE);
        tf.setCaption(vaadinUi.getMsg("ReportDetailView.brand.label"));
        tf.setValue( transaction.hasBrandLibraryId() ? transaction.getBrandLibrary().getPathName().toString() : "");
    	tf.setReadOnly(true);
        layout.addComponent(tf, 2, 0);
		
		tf = new TextField();
		tf.setWidth(100, Unit.PERCENTAGE);
        tf.setCaption(vaadinUi.getMsg("ReportDetailView.id.label"));
        tf.setValue(transaction.getId().toString());
    	tf.setReadOnly(true);
        layout.addComponent(tf, 3, 0);
		
        // The status and status text are together
        HorizontalLayout statusLayout = new HorizontalLayout();
        statusLayout.setSpacing(true);
        statusLayout.setMargin(false);
        
        currentStatus = new TextField();
        currentStatus.setWidth(100, Unit.PERCENTAGE);
        currentStatus.setCaption(vaadinUi.getMsg("ReportDetailView.status.label"));
        currentStatus.setReadOnly(true);
        statusLayout.addComponent(currentStatus);

        lastStatusText = new Label("");
        statusLayout.addComponent(lastStatusText);
        statusLayout.setComponentAlignment(lastStatusText, Alignment.BOTTOM_LEFT);
        
        layout.addComponent(statusLayout, 0, 1);
        
		tf = new TextField();
		tf.setWidth(100, Unit.PERCENTAGE);
        tf.setCaption(vaadinUi.getMsg("ReportDetailView.expires.label"));
        if ( transaction.hasExpireTimestamp() )
        	tf.setValue(transaction.getExpireTimestamp().toLogString(vaadinUi.getUser()));
        else
        	tf.setValue(vaadinUi.getMsg("ReportDetailView.expires.never.value"));
    	tf.setReadOnly(true);
        layout.addComponent(tf, 1, 1);
		
		tf = new TextField();
		tf.setWidth(100, Unit.PERCENTAGE);
        tf.setCaption(vaadinUi.getMsg("ReportDetailView.autoCancels.label"));
        if ( transaction.hasCancelTimestamp() )
        	tf.setValue(transaction.getCancelTimestamp().toLogString(vaadinUi.getUser()));
        else
        	tf.setValue(vaadinUi.getMsg("ReportDetailView.autoCancels.never.value"));
    	tf.setReadOnly(true);
        layout.addComponent(tf, 2, 1);
		
		tf = new TextField();
		tf.setWidth(100, Unit.PERCENTAGE);
        tf.setCaption(vaadinUi.getMsg("ReportDetailView.stalled.label"));
        if ( transaction.isStalled() ) {
        	tf.setValue(transaction.getStallTimestamp().toLogString(vaadinUi.getUser()));
            tf.setStyleName("caution");
        } else {
        	tf.setValue(vaadinUi.getMsg("ReportDetailView.notStalled.value"));
        }
    	tf.setReadOnly(true);
        layout.addComponent(tf, 3, 1);
        
		tf = new TextField();
		tf.setWidth(100, Unit.PERCENTAGE);
        tf.setCaption(vaadinUi.getMsg("ReportDetailView.started.label"));
    	tf.setValue(transaction.getCreatedTimestamp().toLogString(vaadinUi.getUser()));
    	tf.setReadOnly(true);
        layout.addComponent(tf, 0, 2);

		tf = new TextField();
		tf.setWidth(100, Unit.PERCENTAGE);
        tf.setCaption(vaadinUi.getMsg("ReportDetailView.startedByUser.label"));
        if ( transaction.hasCreatedByUserId() )
        	tf.setValue(vaadinUi.getPrettyCode().userDisplayName(transaction.getCreatedByUserId()));
    	tf.setReadOnly(true);
        layout.addComponent(tf, 1, 2);

        lastUpdated = new TextField();
        lastUpdated.setWidth(100, Unit.PERCENTAGE);
        lastUpdated.setCaption(vaadinUi.getMsg("ReportDetailView.lastUpdated.label"));
        lastUpdated.setReadOnly(true);
        layout.addComponent(lastUpdated, 2, 2);
		
		lastUpdatedByUser = new TextField();
		lastUpdatedByUser.setWidth(100, Unit.PERCENTAGE);
		lastUpdatedByUser.setCaption(vaadinUi.getMsg("ReportDetailView.lastUpdatedByUser.label"));
        if ( transaction.hasLastUpdatedByUserId() )
        	lastUpdatedByUser.setValue(vaadinUi.getPrettyCode().userDisplayName(transaction.getLastUpdatedByUserId()));
        lastUpdatedByUser.setReadOnly(true);
        layout.addComponent(lastUpdatedByUser, 3, 2);
        
        HorizontalLayout reasonLayout = new HorizontalLayout();
        reasonLayout.setMargin(false);
        reasonLayout.setSpacing(true);
        layout.addComponent(reasonLayout, 0, 3);
        
        statusChangeReason = new TextField();
        statusChangeReason.setWidth(100, Unit.PERCENTAGE);
        statusChangeReason.setInputPrompt(vaadinUi.getMsg("ReportDetailView.statusChange.reason.label"));
        statusChangeReason.setValue(null);
        statusChangeReason.setNullRepresentation("");
        statusChangeReason.setReadOnly(false);
        reasonLayout.addComponent(statusChangeReason);
        
        statusChangeButton = new PopupButton(vaadinUi.getMsg("ReportDetailView.button.statusChange.popup.label"));
        statusChangeButton.setStyleName(Reindeer.BUTTON_SMALL);
        statusChangeButton.setContent(new VerticalLayout());
		reloadStatusChange();
		reasonLayout.addComponent(statusChangeButton);

        HorizontalLayout buttonLayout = new HorizontalLayout();
        buttonLayout.setMargin(false);
        buttonLayout.setSpacing(true);
        layout.addComponent(buttonLayout, 1, 3, 3, 3);
        layout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_RIGHT);

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.addStyleName(Reindeer.BUTTON_SMALL);
        buttonLayout.addComponent(buttonGroup);
        
        if ( reportTemplate.hasViewActivityLogPermission(vaadinUi.getUser()) ) {
        	Button viewLogButton = new Button(vaadinUi.getMsg("ReportDetailView.button.viewLog.label"), new ClickListener(){
				private static final long serialVersionUID = 4949769496826607406L;

				@Override
    			public void buttonClick(ClickEvent event) {
    				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	            final LogView logView = new LogView(transaction);
    	            logView.initView();
    	            logView.doSearch();
    	    		
    	            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("TransactionLogView.window.caption",transaction.getTransactionTemplate().getPathName(),transaction.getId()), logView);
    	            w.center();
    	        	w.setWidth(80, Unit.PERCENTAGE);
    	        	w.setHeight(80, Unit.PERCENTAGE);

    	            logView.activateView(EsfView.OpenMode.WINDOW, "");
    	            vaadinUi.addWindowToUI(w);
    	            event.getButton().setEnabled(true);
    			}
            });
        	viewLogButton.setDisableOnClick(true);
        	viewLogButton.setStyleName("viewLogButton");
        	viewLogButton.addStyleName(Reindeer.BUTTON_SMALL);
        	viewLogButton.setDescription(vaadinUi.getMsg("ReportDetailView.button.viewLog.tooltip"));
        	viewLogButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportDetailView.button.viewLog.icon")));
        	buttonGroup.addButton(viewLogButton);
        }
    	         
        if ( reportTemplate.hasViewEmailLogPermission(vaadinUi.getUser()) ) {
        	Button viewEmailButton = new Button(vaadinUi.getMsg("ReportDetailView.button.viewEmail.label"), new ClickListener(){
				private static final long serialVersionUID = 6814043380569625101L;

				@Override
    			public void buttonClick(ClickEvent event) {
    				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	            final EmailLogView emailLogView = new EmailLogView(OutboundEmailMessage.LINK_TYPE_TRANSACTION,transaction.getId());
    	            emailLogView.initView();
    	            emailLogView.doSearch();
    	    		
    	            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("TransactionEmailLogView.window.caption",transaction.getTransactionTemplate().getPathName(),transaction.getId()), emailLogView);
    	            w.center();
    	        	w.setWidth(90, Unit.PERCENTAGE);
    	        	w.setHeight(80, Unit.PERCENTAGE);

    	        	emailLogView.activateView(EsfView.OpenMode.WINDOW, "");
    	        	vaadinUi.addWindowToUI(w);
    	            event.getButton().setEnabled(true);
    			}
            });
        	viewEmailButton.setDisableOnClick(true);
        	viewEmailButton.setStyleName("viewEmailButton");
        	viewEmailButton.addStyleName(Reindeer.BUTTON_SMALL);
        	viewEmailButton.setDescription(vaadinUi.getMsg("ReportDetailView.button.viewEmail.tooltip"));
        	viewEmailButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportDetailView.button.viewEmail.icon")));
        	buttonGroup.addButton(viewEmailButton);
        }
    	
        // HTTP Send logs we treat as needing the same permission as the activity log and view data snapshots
        if ( reportTemplate.hasViewSnapshotDataPermission(vaadinUi.getUser()) && reportTemplate.hasViewActivityLogPermission(vaadinUi.getUser()) ) {
            Button viewHttpSendButton = new Button(vaadinUi.getMsg("ReportDetailView.button.viewHttpSend.label"), new ClickListener(){
				private static final long serialVersionUID = -8549017222321221701L;

				@Override
    			public void buttonClick(ClickEvent event) {
    				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	            final HttpSendLogView httpSendLogView = new HttpSendLogView(transaction.getId());
    	            httpSendLogView.initView();
    	            httpSendLogView.doSearch();
    	    		
    	            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("HttpSendLogView.window.caption",transaction.getTransactionTemplate().getPathName(),transaction.getId()), httpSendLogView);
    	            w.center();
    	        	w.setWidth(90, Unit.PERCENTAGE);
    	        	w.setHeight(80, Unit.PERCENTAGE);

    	        	httpSendLogView.activateView(EsfView.OpenMode.WINDOW, "");
    	        	vaadinUi.addWindowToUI(w);
    	            event.getButton().setEnabled(true);
    			}
            });
        	viewHttpSendButton.setDisableOnClick(true);
        	viewHttpSendButton.setStyleName("viewHttpSendButton");
        	viewHttpSendButton.addStyleName(Reindeer.BUTTON_SMALL);
        	viewHttpSendButton.setDescription(vaadinUi.getMsg("ReportDetailView.button.viewHttpSend.tooltip"));
        	viewHttpSendButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportDetailView.button.viewHttpSend.icon")));
        	buttonGroup.addButton(viewHttpSendButton);
        }
    	
        if ( reportTemplate.hasViewSnapshotDocumentPermission(vaadinUi.getUser()) ) {
        	if ( reportTemplate.hasAllowedTransactionDocumentSnapshots(transaction) ) {
        		TranSnapshotPopupButton viewHtmlDocumentsButton = new TranSnapshotPopupButton(transaction,reportTemplate);
        		buttonLayout.addComponent(viewHtmlDocumentsButton);
        	}
        }
        
        if ( reportTemplate.hasViewSnapshotDataPermission(vaadinUi.getUser()) ) {
            Button downloadTransactionRecordButton = new Button(vaadinUi.getMsg("ReportDetailView.button.downloadTransactionRecord.label"));
            downloadTransactionRecordButton.setStyleName(Reindeer.BUTTON_SMALL);
            downloadTransactionRecordButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.download.icon")));
            downloadTransactionRecordButton.setDescription(vaadinUi.getMsg("ReportDetailView.button.downloadTransactionRecord.tooltip"));
            
            OnDemandFileDownloader downloaderTransactionRecord = new OnDemandFileDownloader( downloadTransactionRecordButton, new OnDemandFileDownloader.OnDemandStreamSource() {
				private static final long serialVersionUID = -359290104819210134L;

				int contentLength = -1;

				@Override
    			public InputStream getStream() {
    				byte[] data = transaction.getRecord().toXmlByteArray(true);
                	EsfVaadinUI.getInstance().getEsfapp().debug("ReportDetailView downloadTransactionRecordButton.OnDemandFileDownloader.getStream() - tranId: " + transaction.getId() + "; data length: " + data.length);
                	contentLength = data.length;
                	return new BufferedInputStream(new ByteArrayInputStream(data));
    			}

    			@Override
    			public String getFilename() {
    				return EsfVaadinUI.getInstance().getMsg("ReportDetailView.button.downloadTransactionRecord.filename");
    			}
    			
    			@Override
    			public int getContentLength() {
    				return contentLength;
    			}
            });
            downloadTransactionRecordButton.setDisableOnClick(true);
            downloaderTransactionRecord.extend(downloadTransactionRecordButton);
            buttonLayout.addComponent(downloadTransactionRecordButton);
        }
    	
		return layout;
	}
	
	protected void reloadStatusChange() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		refreshButton.setVisible(! transaction.isEndState());
		
    	currentStatus.setReadOnly(false);
        if ( transaction.isInProgress() ) {
        	currentStatus.setValue(vaadinUi.getMsg("transaction.status.inProgress"));
        	currentStatus.setStyleName("inprogress");
        } else if ( transaction.isCompleted() ) {
        	currentStatus.setValue(vaadinUi.getMsg("transaction.status.completed"));
        } else if ( transaction.isCanceled() ) {
        	currentStatus.setValue(vaadinUi.getMsg("transaction.status.canceled"));
        	currentStatus.setStyleName("canceled");
        } else if ( transaction.isSuspended() ) {
        	currentStatus.setValue(vaadinUi.getMsg("transaction.status.suspended"));
        	currentStatus.setStyleName("suspended");
        } else {
        	currentStatus.setValue("???");
        	currentStatus.setStyleName("caution");
        }
    	currentStatus.setReadOnly(true);
    	
    	lastStatusText.setValue("["+transaction.getStatusText()+"]");

    	lastUpdated.setReadOnly(false);
        lastUpdated.setValue(transaction.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser()));
    	lastUpdated.setReadOnly(true);
        if ( transaction.hasLastUpdatedByUserId() ) {
        	lastUpdatedByUser.setReadOnly(false);
        	lastUpdatedByUser.setValue(vaadinUi.getPrettyCode().userDisplayName(transaction.getLastUpdatedByUserId()));
        	lastUpdatedByUser.setReadOnly(true);
        }

        VerticalLayout statusChangeButtonLayout = new VerticalLayout();
        statusChangeButtonLayout.setSizeUndefined();
        statusChangeButtonLayout.setMargin(false);
        statusChangeButtonLayout.setSpacing(true);
        statusChangeButton.setContent(statusChangeButtonLayout);
    	
        TransactionTemplate tranTemplate = transaction.getTransactionTemplate();
		
		if ( transaction.isInProgress() ) {
			if ( tranTemplate.hasCancelPermission(vaadinUi.getUser())) {
				Button subButton = new Button(vaadinUi.getMsg("ReportDetailView.button.statusChange.inProgress.toCanceled.button.label"), new ClickListener() {
					private static final long serialVersionUID = -8549017222321221701L;

					@Override
					public void buttonClick(ClickEvent event) {
						processStatusChangeButton("inProgress.toCanceled",Transaction.TRAN_STATUS_IN_PROGRESS,Transaction.TRAN_STATUS_CANCELED);
						event.getButton().setEnabled(true);
					}
				});
				subButton.setDisableOnClick(true);
				subButton.setStyleName(Reindeer.BUTTON_LINK);
				statusChangeButtonLayout.addComponent(subButton);
			}

			if ( tranTemplate.hasSuspendPermission(vaadinUi.getUser())) {
				Button subButton = new Button(vaadinUi.getMsg("ReportDetailView.button.statusChange.inProgress.toSuspended.button.label"), new ClickListener() {
					private static final long serialVersionUID = 2899119868610036547L;

					@Override
					public void buttonClick(ClickEvent event) {
						processStatusChangeButton("inProgress.toSuspended",Transaction.TRAN_STATUS_IN_PROGRESS,Transaction.TRAN_STATUS_SUSPENDED);
						event.getButton().setEnabled(true);
					}
				});
				subButton.setDisableOnClick(true);
				subButton.setStyleName(Reindeer.BUTTON_LINK);
				statusChangeButtonLayout.addComponent(subButton);
			}
		} else if ( transaction.isCanceled() ) {
			if ( tranTemplate.hasReactivatePermission(vaadinUi.getUser())) {
				Button subButton = new Button(vaadinUi.getMsg("ReportDetailView.button.statusChange.canceled.toInProgress.button.label"), new ClickListener() {
					private static final long serialVersionUID = 1956650042322037901L;

					@Override
					public void buttonClick(ClickEvent event) {
						processStatusChangeButton("canceled.toInProgress",Transaction.TRAN_STATUS_CANCELED,Transaction.TRAN_STATUS_IN_PROGRESS);
						event.getButton().setEnabled(true);
					}
				});
				subButton.setDisableOnClick(true);
				subButton.setStyleName(Reindeer.BUTTON_LINK);
				statusChangeButtonLayout.addComponent(subButton);
			}
		} else if ( transaction.isSuspended() ) {
			if ( tranTemplate.hasCancelPermission(vaadinUi.getUser())) {
				Button subButton = new Button(vaadinUi.getMsg("ReportDetailView.button.statusChange.suspended.toCanceled.button.label"), new ClickListener() {
					private static final long serialVersionUID = -5695603961121711529L;

					@Override
					public void buttonClick(ClickEvent event) {
						processStatusChangeButton("suspended.toCanceled",Transaction.TRAN_STATUS_SUSPENDED,Transaction.TRAN_STATUS_CANCELED);
						event.getButton().setEnabled(true);
					}
				});
				subButton.setDisableOnClick(true);
				subButton.setStyleName(Reindeer.BUTTON_LINK);
				statusChangeButtonLayout.addComponent(subButton);
			}

			if ( tranTemplate.hasResumePermission(vaadinUi.getUser())) {
				Button subButton = new Button(vaadinUi.getMsg("ReportDetailView.button.statusChange.suspended.toInProgress.button.label"), new ClickListener() {
					private static final long serialVersionUID = -1278093000890193994L;

					@Override
					public void buttonClick(ClickEvent event) {
						processStatusChangeButton("suspended.toInProgress",Transaction.TRAN_STATUS_SUSPENDED,Transaction.TRAN_STATUS_IN_PROGRESS);
						event.getButton().setEnabled(true);
					}
				});
				subButton.setDisableOnClick(true);
				subButton.setStyleName(Reindeer.BUTTON_LINK);
				statusChangeButtonLayout.addComponent(subButton);
			}
		}
		
		Button subButton = new Button(vaadinUi.getMsg("ReportDetailView.button.statusChange.log.button.label"), new ClickListener() {
			private static final long serialVersionUID = -8843009675365925106L;

			@Override
			public void buttonClick(ClickEvent event) {
				processStatusChangeButton("log",null,null);
				event.getButton().setEnabled(true);
			}
		});
		subButton.setDisableOnClick(true);
		subButton.setStyleName(Reindeer.BUTTON_LINK);
		statusChangeButtonLayout.addComponent(subButton);
	}
	
	protected void processStatusChangeButton(String messageFragment, String fromTranStatus, String toTranStatus) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		String reasonText = (String)statusChangeReason.getValue();
    	if ( EsfString.isBlank(reasonText) ) {
    		Errors errors = new Errors();
    		errors.addWarning(vaadinUi.getMsg("ReportDetailView.statusChange.reason.error"));
    		vaadinUi.show(errors);
    		statusChangeReason.addStyleName("error");
    	} else if ( fromTranStatus != null && ! transaction.getStatus().equals(fromTranStatus) ) {
    		Errors errors = new Errors();
    		errors.addWarning(vaadinUi.getMsg("ReportDetailView.button.statusChange." + messageFragment + ".button.error"));
    		vaadinUi.show(errors);
    	} else {
    		statusChangeReason.removeStyleName("error");
    		statusChangeReason.setValue(null);

    		String logText = vaadinUi.getMsg("ReportDetailView.button.statusChange." + messageFragment + ".button.logText",vaadinUi.getUser().getFullDisplayName(),reasonText);
        	transaction.logGeneral(logText);
    		vaadinUi.getUser().logConfigChange(logText);
        	vaadinUi.showStatus(logText);
        	statusChangeButton.setPopupVisible(false);
        	
        	if ( toTranStatus != null ) {
        		if ( Transaction.isCanceled(toTranStatus) ) { // we now fire an event when a transaction is canceled
        			TransactionContext tranContext = new TransactionContext(vaadinUi.getUser(), transaction.getId());
        			TransactionEngine engine = new TransactionEngine(tranContext);
        			engine.queueTransactionCanceledEvent(reasonText);
        			engine.doWork();
        		} else if ( Transaction.isSuspended(toTranStatus) ) { // we now fire an event when a transaction is suspended
        			TransactionContext tranContext = new TransactionContext(vaadinUi.getUser(), transaction.getId());
        			TransactionEngine engine = new TransactionEngine(tranContext);
        			engine.queueTransactionSuspendedEvent(reasonText);
        			engine.doWork();
        		} else if ( Transaction.isSuspended(fromTranStatus) && Transaction.isInProgress(toTranStatus) ) { // we now fire an event when a transaction is resumed from suspended
        			TransactionContext tranContext = new TransactionContext(vaadinUi.getUser(), transaction.getId());
        			TransactionEngine engine = new TransactionEngine(tranContext);
        			engine.queueTransactionResumedEvent(reasonText);
        			engine.doWork();
        		} else {
            		transaction.setStatus(toTranStatus);
            		transaction.save();
        		}
        		reloadStatusChange();
        	}
    	}
	}
	
	protected PartyTreeTable createPartyTable() {
		try {
			PartyContainer container = new PartyContainer();
			List<TransactionParty> tranPartyList = transaction.getAllTransactionParties();
			container.refresh(tranPartyList);
			
			PartyTreeTable table = new PartyTreeTable(container);
			return table;
		} catch( Exception e ) { return null; }
	}
	
	protected Table createDocumentPartyTable(TransactionDocument tranDoc, List<TransactionParty> tranPartyList, List<DocumentIdPartyId> allDocAndParty) {
		try {
			DocumentPartyContainer container = new DocumentPartyContainer();
			container.refresh(tranDoc, tranPartyList, allDocAndParty);
			
			DocumentPartyTable table = new DocumentPartyTable(container);
			return table;
		} catch( Exception e ) { return new Table(); }
	}
	
	protected VerticalLayout createDocumentPartyList() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		List<TransactionDocument> tranDocList = reportTemplate.getAllAllowedTransactionDocuments(transaction);
		List<TransactionParty> tranPartyList = transaction.getAllTransactionParties();
		
		VerticalLayout layout = new VerticalLayout();
		
		final List<DocumentIdPartyId> allDocAndParty = new LinkedList<DocumentIdPartyId>();
		List<String> skippedDocumentList = null;
		
		for( final TransactionDocument tranDoc : tranDocList ) {
			final EsfName tranDocName = tranDoc.getDocument().getEsfName();
			
			boolean tranPartyDocumentAllSkipped = true;
			for( TransactionPartyDocument tpd : transaction.getAllTransactionPartyDocumentsForTransactionDocument(tranDoc) ) {
				if ( ! tpd.isSkipped() ) {
					tranPartyDocumentAllSkipped = false;
					break;
				}
			}
			if ( tranPartyDocumentAllSkipped ) {
				if ( skippedDocumentList == null ) {
					skippedDocumentList = new LinkedList<String>();
				}
				skippedDocumentList.add(tranDocName + " - " + tranDoc.getId());
				continue;
			}
			
			HorizontalLayout documentLayout = new HorizontalLayout();
			documentLayout.setWidth(100, Unit.PERCENTAGE);
			documentLayout.setSpacing(true);
			documentLayout.setMargin(true);
		
			DocumentVersion docVer = tranDoc.getDocumentVersion();
			if ( docVer == null ) {
				String errorText = vaadinUi.getMsg("ReportDetailView.documentParty.documentVersion.missing.warning",tranDocName,tranDoc.getDocumentVersionId());
	    		Errors errors = new Errors();
	    		errors.addWarning(errorText);
	    		vaadinUi.show(errors);
	    		_logger.warn("createDocumentPartyList() - " + errorText);
	    		continue;
			}
			
			String documentNameAndVersion = tranDoc.getDocumentVersion().getDocumentNameVersionWithLabel();
			TextField tf = new TextField();
			tf.setStyleName("documentName");
			tf.setWidth(100, Unit.PERCENTAGE);
			tf.setCaption(vaadinUi.getMsg("ReportDetailView.document.name.label"));
	        tf.setValue(documentNameAndVersion);
	    	tf.setReadOnly(true);
	    	documentLayout.addComponent(tf);
	        
			tf = new TextField();
			tf.setWidth(100, Unit.PERCENTAGE);
			tf.setCaption(vaadinUi.getMsg("ReportDetailView.document.id.label"));
	        tf.setValue(tranDoc.getId().toString());
	    	tf.setReadOnly(true);
	    	documentLayout.addComponent(tf);
	        
			if ( reportTemplate.hasViewSnapshotDataPermission(vaadinUi.getUser()) ) {
		        Button downloadDocumentRecordButton = new Button(vaadinUi.getMsg("ReportDetailView.button.downloadDocumentRecord.label",tranDocName));
		        downloadDocumentRecordButton.setStyleName(Reindeer.BUTTON_SMALL);
		        downloadDocumentRecordButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.download.icon")));
		        downloadDocumentRecordButton.setDescription(vaadinUi.getMsg("ReportDetailView.button.downloadDocumentRecord.tooltip",documentNameAndVersion));

		        OnDemandFileDownloader downloaderDocumentRecord = new OnDemandFileDownloader( downloadDocumentRecordButton, new OnDemandFileDownloader.OnDemandStreamSource() {
					private static final long serialVersionUID = 3182589256937125641L;

					int contentLength = -1;

					@Override
					public InputStream getStream() {
						byte[] data = tranDoc.getRecord().toXmlByteArray(true);
			           	EsfVaadinUI.getInstance().getEsfapp().debug("ReportDetailView downloadDocumentRecordButton.OnDemandFileDownloader.getStream() - tranId: " + transaction.getId() + "; data length: " + data.length);
			           	contentLength = data.length;
			           	return new BufferedInputStream(new ByteArrayInputStream(data));
					}

					@Override
					public String getFilename() {
						return EsfVaadinUI.getInstance().getMsg("ReportDetailView.button.downloadDocumentRecord.filename",tranDocName);
					}
	    			
	    			@Override
	    			public int getContentLength() {
	    				return contentLength;
	    			}
		        });
		        downloadDocumentRecordButton.setDisableOnClick(true);
		        downloaderDocumentRecord.extend(downloadDocumentRecordButton);
		        documentLayout.addComponent(downloadDocumentRecordButton);
		        documentLayout.setComponentAlignment(downloadDocumentRecordButton, Alignment.BOTTOM_CENTER);

				List<TransactionFile> tranFileList = transaction.getAllTransactionFilesForDocument(tranDoc.getId());
				if ( tranFileList.size() > 0 ) {
					TranFilePopupButton tranFileDownloadButton = new TranFilePopupButton(transaction, tranFileList);
			        documentLayout.addComponent(tranFileDownloadButton);
			        documentLayout.setComponentAlignment(tranFileDownloadButton, Alignment.BOTTOM_CENTER);
				}
			}
	        
	        layout.addComponent(documentLayout);
	        
	        Table documentPartyTable = createDocumentPartyTable(tranDoc,tranPartyList,allDocAndParty);
    		layout.addComponent(documentPartyTable);
    		layout.setComponentAlignment(documentPartyTable, Alignment.MIDDLE_CENTER);
		}  // end for each TransactionDocument
		
		if ( allDocAndParty.size() > 0 ) {
			TranSnapshotsDownloadPopupButton button = new TranSnapshotsDownloadPopupButton(transaction, reportTemplate, vaadinUi.getMsg("ReportDetailView.button.TranSnapshotsDownloadPopupButton.label"));
			button.setStyleName("SelectableDownloadButton");
			layout.addComponent(button);
		}
		
		if ( skippedDocumentList != null ) {
			ListSelect skippedDocuments = new ListSelect(vaadinUi.getMsg("ReportDetailView.skippedDocumentList.label"));
			skippedDocuments.setDescription(vaadinUi.getMsg("ReportDetailView.skippedDocumentList.tooltip"));
			skippedDocuments.setNullSelectionAllowed(false);
			for( String skippedDoc : skippedDocumentList ) {
				skippedDocuments.addItem(skippedDoc);
			}
			skippedDocuments.setRows(Math.min(2,skippedDocuments.size()));
			skippedDocuments.setReadOnly(true);
			layout.addComponent(skippedDocuments);
		}
		
        return layout;
	}
	
	void resetDownloadContextPickupCodeList() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		for( String puc : downloadContextPickupCodeList ) {
			vaadinUi.removeSessionAttribute(puc);
			_logger.debug("DEBUG-PICKUP-0: Remove download context pickup code: " + puc + "; IP: " + vaadinUi.getRequestHostIp());
		}
		downloadContextPickupCodeList.clear();
	}
	
	@Override
	public void detach() {
		resetDownloadContextPickupCodeList();
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return "";
	}

	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
	
	public class PartyContainer extends HierarchicalContainer {
		private static final long serialVersionUID = -8536374811297902476L;
		
		private int numParents = 0;
		public int getNumParents() { return numParents; }

		public PartyContainer() throws InstantiationException, IllegalAccessException {
			super();
			// Non-visible, just for us...
			this.addContainerProperty("TransactionParty", TransactionParty.class, null);
			this.addContainerProperty("TransactionPartyAssignment", TransactionPartyAssignment.class, null);
			// For the TreeTable
			this.addContainerProperty("partyName", EsfName.class, null);
			this.addContainerProperty("status", String.class, "");
			this.addContainerProperty("numDocuments", Integer.class, 0);
			this.addContainerProperty("email", String.class, "");
			this.addContainerProperty("user", String.class, "");
	        this.addContainerProperty("createdTimestamp", EsfDateTime.class, null);
	        this.addContainerProperty("lastAccessedTimestamp", EsfDateTime.class, null);
	        this.addContainerProperty("emailPopupButton", PopupButton.class, null);
		}
		
		@SuppressWarnings("unchecked")
		public void refresh(List<TransactionParty> tranPartyList) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			removeAllItems();
			numParents = 0;
			
			if ( tranPartyList == null || tranPartyList.size() == 0 )
				return;
			
			for( TransactionParty tranParty : tranPartyList ) {
				Object pid = addItem();
				Item pitem = getItem(pid);
				++numParents;
				pitem.getItemProperty("TransactionParty").setValue(tranParty);
				pitem.getItemProperty("partyName").setValue(tranParty.getPackageVersionPartyTemplate().getEsfName());
				
				// Let's get the assignments
				List<TransactionPartyAssignment> tranPartyAssignmentList = TransactionPartyAssignment.Manager.getAllByTransactionPartyId(tranParty.getId());
				setChildrenAllowed(pid,tranPartyAssignmentList != null && tranPartyAssignmentList.size() > 1);
				if ( tranPartyAssignmentList != null ) {
					boolean isFirstAssignment = true;
					for( TransactionPartyAssignment tranPartyAssignment : tranPartyAssignmentList )	{
						Object cid;
						final Item citem;
						if ( isFirstAssignment ) {
							cid = pid;
							citem = pitem;
						} else {
							cid = addItem(); // new subrow
							citem = getItem(cid);
							setChildrenAllowed(cid,false); // there are no children of children
						}

						citem.getItemProperty("status").setValue(tranPartyAssignment.getStatus());
						citem.getItemProperty("numDocuments").setValue(tranParty.getNonSkippedTransactionPartyDocuments().size());
						citem.getItemProperty("email").setValue(tranPartyAssignment.hasEmailAddress() ? tranPartyAssignment.getEmailAddress() : "");
						citem.getItemProperty("user").setValue(tranPartyAssignment.hasUserId() ? tranPartyAssignment.getUser().getFullDisplayName() : "");
						citem.getItemProperty("createdTimestamp").setValue(tranPartyAssignment.getCreatedTimestamp());
						citem.getItemProperty("lastAccessedTimestamp").setValue(tranPartyAssignment.getLastAccessedTimestamp());
						citem.getItemProperty("TransactionPartyAssignment").setValue(tranPartyAssignment);

						if ( isFirstAssignment ) {
							isFirstAssignment = false;
						} else {
							setParent(cid,pid);
							continue; // we don't want any buttons on older transaction party assignments
						}
						
						final EsfUUID tranPartyId = tranParty.getId();
						final EsfName tranPartyName = tranParty.getPackageVersionPartyTemplate().getEsfName();
						
						if ( ! tranParty.getPackageVersionPartyTemplate().isEsfReportsAccess() ) {
			    			VerticalLayout emailPopupLayout = new VerticalLayout();
			    			emailPopupLayout.setSizeUndefined();
			    			emailPopupLayout.setMargin(false);
			    			emailPopupLayout.setSpacing(true);

			    			PopupButton emailPopupButton = new PopupButton(vaadinUi.getMsg("ReportDetailView.button.email.popup.label"));
			    			emailPopupButton.setStyleName(Reindeer.BUTTON_SMALL);
			    			emailPopupButton.setContent(emailPopupLayout);
			    			citem.getItemProperty("emailPopupButton").setValue(emailPopupButton);

			    			if ( reportTemplate.hasViewEmailLogPermission(vaadinUi.getUser()) ) {
						    	Button viewEmailButton = new Button(vaadinUi.getMsg("ReportDetailView.button.email.viewEmail.button.label"), new ClickListener() {
									private static final long serialVersionUID = -9185546988006585212L;

									@Override
									public void buttonClick(ClickEvent event) {
										EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
							            final EmailLogView emailLogView = new EmailLogView(OutboundEmailMessage.LINK_TYPE_TRANSACTION_PARTY,tranPartyId);
							            emailLogView.initView();
							            emailLogView.doSearch();
							    		
							            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("TransactionEmailLogView.party.window.caption",tranPartyName,tranPartyId), emailLogView);
							            w.center();
							        	w.setWidth(90, Unit.PERCENTAGE);
							        	w.setHeight(80, Unit.PERCENTAGE);

							        	emailLogView.activateView(EsfView.OpenMode.WINDOW, "");
							        	vaadinUi.addWindowToUI(w);
							        	event.getButton().setEnabled(true);
							        	((PopupButton)event.getButton().getParent().getParent()).setPopupVisible(false);
									}
						        });
						    	viewEmailButton.setDisableOnClick(true);
						    	viewEmailButton.setStyleName(Reindeer.BUTTON_LINK);
						    	viewEmailButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportDetailView.button.email.viewEmail.button.icon")));
						    	emailPopupLayout.addComponent(viewEmailButton);
							}
					    	
					    	// If we have an email address and we're either in progress or have already been completed, allow an email renotification to take place
					    	if ( (tranParty.hasTodoGroupId() || tranPartyAssignment.hasEmailAddress()) && 
					    		 (tranPartyAssignment.isActivated() || tranPartyAssignment.isRetrieved() || tranPartyAssignment.isCompleted()) ) {
						    	Button renotifyButton = new Button(vaadinUi.getMsg("ReportDetailView.button.email.renotify.button.label"), new ClickListener() {
									private static final long serialVersionUID = 5311882447024021260L;

									@Override
									public void buttonClick(ClickEvent event) {
										EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
								        ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
								        Connection        con  = pool.getConnection();
								        try {
								        	TransactionParty tranParty = (TransactionParty)citem.getItemProperty("TransactionParty").getValue();
								        	
											// Now let's refresh our information to ensure this is still allowed
											Transaction refreshedTran = Transaction.Manager.getById(transaction.getId());
											TransactionParty refreshedTranParty = refreshedTran.getTransactionParty(tranPartyName);
											TransactionPartyAssignment refreshedTranPartyAssignment = refreshedTranParty.getCurrentAssignment();
											
											if ( (refreshedTranParty.hasTodoGroupId() || refreshedTranPartyAssignment.hasEmailAddress()) && 
										    	 (refreshedTranPartyAssignment.isActivated() || refreshedTranPartyAssignment.isRetrieved() || refreshedTranPartyAssignment.isCompleted()) ) {
												refreshedTranParty.renotifyParty(con);
												refreshedTran._setChangedForInternalUseOnly();
												refreshedTran.save(con,vaadinUi.getUser());
												con.commit();
												transaction = refreshedTran;

												vaadinUi.showStatus(vaadinUi.getMsg("ReportDetailView.button.email.renotify.success",tranParty.getPackageVersionPartyTemplate().getEsfName()));
												reloadStatusChange();
											} else {
												vaadinUi.showWarning(null, vaadinUi.getMsg("ReportDetailView.button.email.renotify.currentPartyChangedWarning"));
											}
								        } catch(SQLException e) {
								        	_logger.sqlerr("Renotify party failed", e);
								            pool.rollbackIgnoreException(con,e);
								        } finally {
								        	vaadinUi.getEsfapp().cleanupPool(pool,con,null);
								        	event.getButton().setEnabled(true);
								        	((PopupButton)event.getButton().getParent().getParent()).setPopupVisible(false);
								        }
									}
						        });
						    	renotifyButton.setDisableOnClick(true);
						    	renotifyButton.setStyleName(Reindeer.BUTTON_LINK);
						    	renotifyButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportDetailView.button.email.renotify.button.icon")));
						    	emailPopupLayout.addComponent(renotifyButton);
					    	}
					    	
					    	// If we're in progress, let them set the email address (transfer party)
					    	if ( tranPartyAssignment.isActivated() || tranPartyAssignment.isRetrieved() ) {
					    		TextField newEmail = new TextField();
					    		newEmail.setImmediate(true);
					    		newEmail.setInputPrompt(vaadinUi.getMsg("ReportDetailView.button.email.newEmailTextField.inputPrompt"));
					    		emailPopupLayout.addComponent(newEmail);
					    		newEmail.addValueChangeListener( new TextField.ValueChangeListener() {
									private static final long serialVersionUID = 8416184844356424054L;

									@Override
									public void valueChange(ValueChangeEvent event) {
										EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
										TextField tf = (TextField)event.getProperty();
										String newEmailValue = (String)tf.getValue();
										EsfEmailAddress validateEmail = new EsfEmailAddress(newEmailValue);
										if ( ! validateEmail.isValid() ) {
											vaadinUi.showError(null, vaadinUi.getMsg("ReportDetailView.button.email.newEmailTextField.invalidEmail",newEmailValue));
										} else {
											TransactionPartyAssignment tranPartyAssignment = (TransactionPartyAssignment)citem.getItemProperty("TransactionPartyAssignment").getValue();
											// Now let's refresh our information to ensure this is still allowed
											Transaction refreshedTran = Transaction.Manager.getById(transaction.getId());
											TransactionParty refreshedTranParty = refreshedTran.getTransactionParty(tranPartyName);
											TransactionPartyAssignment refreshedTranPartyAssignment = refreshedTranParty.getCurrentAssignment();
											
											if ( refreshedTranPartyAssignment.equals(tranPartyAssignment) && ( refreshedTranPartyAssignment.isActivated() || refreshedTranPartyAssignment.isRetrieved() ) ) {
												String origEmail = tranPartyAssignment.getEmailAddress();
												if ( ! newEmailValue.equalsIgnoreCase(origEmail) ) {
													
											        ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
											        Connection        con  = pool.getConnection();
											        try {
											        	boolean needsPartyActivation = refreshedTranPartyAssignment.isActivated() || refreshedTranPartyAssignment.isRetrieved(); // we'll only activate the new party if the original was
											        	
											        	TransactionContext pseudoContext = new TransactionContext(vaadinUi.getUser(),"",tranPartyName,refreshedTran.getId(),
											        															  vaadinUi.getRequestIpAddress(), vaadinUi.getRequestHostIp(), vaadinUi.getRequestUserAgent());
											        	TransactionEngine engine = new TransactionEngine(pseudoContext);
											        	engine.queuePartyTransferredEvent(tranPartyName, newEmailValue);
											        	
											        	if ( needsPartyActivation )
											        		engine.queuePartyActivatedEvent(tranPartyName);
											        	
											        	Errors errors = new Errors();
											        	engine.doWork(con, errors);
											        	pseudoContext.transaction.save(con,vaadinUi.getUser());
														con.commit();
											        	transaction = pseudoContext.transaction;

														List<TransactionParty> tranPartyList = transaction.getAllTransactionParties();
														refresh(tranPartyList);
														vaadinUi.showStatus(vaadinUi.getMsg("ReportDetailView.button.email.newEmailTextField.success",newEmailValue));
														
														partyTreeTable.setPageLength();
														reloadStatusChange();
											        } catch( EsfException e ) {
											        	pool.rollbackIgnoreException(con,null);
											        	_logger.error("Transfer party failed", e);
											        	vaadinUi.showError(null, vaadinUi.getMsg("ReportDetailView.button.email.newEmailTextField.success",e.getMessage()));
											        } catch(SQLException e) {
											        	_logger.sqlerr("Transfer party failed", e);
											            pool.rollbackIgnoreException(con,e);
											        } finally {
											        	vaadinUi.getEsfapp().cleanupPool(pool,con,null);
											        }
												} else {
													vaadinUi.showWarning(null, vaadinUi.getMsg("ReportDetailView.button.email.newEmailTextField.success"));
												}
											} else {
												vaadinUi.showWarning(null, vaadinUi.getMsg("ReportDetailView.button.email.newEmailTextField.currentPartyChangedWarning"));
											}
										}
										PopupButton popupButton = (PopupButton)tf.getParent().getParent();
										popupButton.setPopupVisible(false);
									}
					    		});
					    	}
						} // end if ESF_reports_access
					}
				}
			}
		}
	}
	
	// This is the table that lists all of the parties and assignments without respect to which documents are referenced.
	class PartyTreeTable extends TreeTable {
		private static final long serialVersionUID = -4746986925398799678L;
		
		final PartyTreeTable thisPartyTreeTable;
		
		public void setPageLength() {
			// If we have any actual hierarchical data, we'll make it one bigger since since it seems to have a rendering issue otherwise when it's expanded
			int size = getContainerDataSource().size();
			for( Object itemId : getContainerDataSource().getItemIds() ) {
				if ( getContainerDataSource().hasChildren(itemId) ) {
					++size;
					break;
				}
			}
			super.setPageLength( size );
		}

		public PartyTreeTable(PartyContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			thisPartyTreeTable = this;
			this.alwaysRecalculateColumnWidths = true;
			setWidth(100, Unit.PERCENTAGE);
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("ReportDetailView.party.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("ReportDetailView.party.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setColumnAlignment("numDocuments", Align.CENTER);
			//setSelectable(true);
			//setImmediate(true);
			setPageLength();

			setCellStyleGenerator( new CellStyleGenerator() {
				private static final long serialVersionUID = -2418584946952669750L;

				@Override
				public String getStyle(Table table, Object itemId, Object propertyId) {
					if ( "status".equals(propertyId) ) {
						String status = (String)thisPartyTreeTable.getItem(itemId).getItemProperty("status").getValue();		
						if ( TransactionPartyAssignment.STATUS_ACTIVATED.equals(status) ) return "activated";
						if ( TransactionPartyAssignment.STATUS_RETRIEVED.equals(status) ) return "retrieved";
						if ( TransactionPartyAssignment.STATUS_COMPLETED.equals(status) ) return "completed";
						if ( TransactionPartyAssignment.STATUS_REJECTED.equals(status) ) return "rejected";
						if ( TransactionPartyAssignment.STATUS_TRANSFERRED.equals(status) ) return "transferred";
						if ( TransactionPartyAssignment.STATUS_UNDEFINED.equals(status) ) return "undefined";
						if ( TransactionPartyAssignment.STATUS_SKIPPED.equals(status) ) return "skipped";
						return "suspended";
					}
					
					return null;
				}
			});
		}
		
		
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			if ( "createdTimestamp".equals(colId) || "lastAccessedTimestamp".equals(colId) ) {
				EsfDateTime myDate = (EsfDateTime)property.getValue();
				if ( myDate == null || myDate.isNull() ) 
					return "";
				return myDate.toLogString(vaadinUi.getUser());
			}
			
			if ( "status".equals(colId) ) {
				String status = (String)property.getValue();
				return vaadinUi.getPrettyCode().transactionPartyAssignmentStatus(status);
			}
			
			return super.formatPropertyValue(rowId,colId,property);
		}

	} // PartyTreeTable

	
	
	// This is the document list that also shows each party's actions on the document.
	public class DocumentPartyContainer extends IndexedContainer {
		private static final long serialVersionUID = -8536374811297902476L;

		public DocumentPartyContainer() throws InstantiationException, IllegalAccessException {
			super();
			this.addContainerProperty("partyName", EsfName.class, null);
			this.addContainerProperty("status", String.class, "");
			this.addContainerProperty("docStatus", String.class, "");
	        this.addContainerProperty("lastUpdatedTimestamp", EsfDateTime.class, null);
			this.addContainerProperty("snapshotTimestamp", EsfDateTime.class, null);
			this.addContainerProperty("downloadSnapshotPopupButton", PopupButton.class, null);
		}
		
		@SuppressWarnings("unchecked")
		public void refresh(TransactionDocument tranDoc, List<TransactionParty> tranPartyList, List<DocumentIdPartyId> allDocAndParty) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			removeAllItems();
			
			if ( tranPartyList == null || tranPartyList.size() == 0 )
				return;
			
			boolean foundFirstPartySnapshot = false;
			
			for( TransactionParty tranParty : tranPartyList ) {
	    		final EsfName tranPartyName = tranParty.getPackageVersionPartyTemplate().getEsfName();
	    		TransactionPartyDocument foundTranPartyDoc = null;
            	//List<TransactionPartyDocument> transactionPartyDocuments = tranParty.getNonSkippedTransactionPartyDocuments();
            	List<TransactionPartyDocument> transactionPartyDocuments = tranParty.getTransactionPartyDocuments();
            	for( TransactionPartyDocument tpd : transactionPartyDocuments ) {
            		if ( tpd.getTransactionDocumentId().equals(tranDoc.getId()) ) {
            			foundTranPartyDoc = tpd;
            			break;
            		}
            	}
            	
            	if ( foundTranPartyDoc == null ) {
            		continue;
            	}
        		
	    		final TransactionPartyDocument tranPartyDoc = foundTranPartyDoc;

				Object pid = addItem();
				Item pitem = getItem(pid);
				pitem.getItemProperty("partyName").setValue(tranPartyName);
				pitem.getItemProperty("status").setValue(tranParty.getStatus());
				pitem.getItemProperty("docStatus").setValue(tranPartyDoc.getStatus());
				pitem.getItemProperty("lastUpdatedTimestamp").setValue(tranPartyDoc.getLastUpdatedTimestamp());
				pitem.getItemProperty("snapshotTimestamp").setValue(tranPartyDoc.hasSnapshotXmlOrBlobId() ? tranPartyDoc.getSnapshotTimestamp() : null);
				
	    		if ( tranPartyDoc.hasSnapshotXmlOrBlobId() ) {
					pitem.getItemProperty("snapshotTimestamp").setValue(tranPartyDoc.getSnapshotTimestamp());

					boolean isDocumentAllowed = reportTemplate.isDocumentAllowed(tranDoc.getDocumentId());
					if ( isDocumentAllowed ) {
						Document document = tranDoc.getDocument();
						final EsfName tranDocName = document.getEsfName();

						if ( ! foundFirstPartySnapshot ) {
							foundFirstPartySnapshot = true;
							DocumentIdPartyId dp = new DocumentIdPartyId(document.getId(),null);  // Add in for this document, latest
							dp.documentName = document.getEsfName();
							allDocAndParty.add(dp);
						}
						DocumentIdPartyId dp = new DocumentIdPartyId(document.getId(),document.getEsfName(),
										tranParty.getPackageVersionPartyTemplateId(),tranParty.getPackageVersionPartyTemplate().getEsfName()); // Add in for this document and party
						allDocAndParty.add(dp); 			
					
						VerticalLayout downloadPopupLayout = new VerticalLayout();
		    			downloadPopupLayout.setSizeUndefined();
		    			downloadPopupLayout.setMargin(false);
		    			downloadPopupLayout.setSpacing(true);
		    			
		    			final PopupButton downloadSnapshotPopupButton = new PopupButton(vaadinUi.getMsg("ReportDetailView.button.downloadDocumentSnapshot.popup.label"));
		    			downloadSnapshotPopupButton.setStyleName(Reindeer.BUTTON_SMALL);
		    			downloadSnapshotPopupButton.setContent(downloadPopupLayout);
		    			pitem.getItemProperty("downloadSnapshotPopupButton").setValue(downloadSnapshotPopupButton);
	
		    			// We'll use these for our various download/view options
		    	        WebBrowser wb = Page.getCurrent().getWebBrowser();
		    	        String downloadTarget = wb.isChrome() || wb.isSafari() ? "_top" : "_blank";
		    	        
		    			if ( reportTemplate.hasViewSnapshotDocumentPermission(vaadinUi.getUser()) ) {
			    	        // View document link
			    	        String downloadContextPickupCode = vaadinUi.getEsfapp().getRandomKey().getEsfReportsAccessPickupCodeString();
			    	        TranSnapshotsDownload.DownloadContext downloadContext = new TranSnapshotsDownload.DownloadContext();
			    			downloadContext.transactionId = transaction.getId();
			    			downloadContext.formatType = TranSnapshotsDownload.FORMAT_HTML;
							if ( tranPartyDoc.hasDocumentFileName() )
								downloadContext.downloadFilename = tranPartyDoc.getDocumentFileName();
							else 
								downloadContext.downloadFilename = vaadinUi.getMsg("ReportDetailView.button.downloadDocumentSnapshot.document.filename",tranDocName,tranPartyName);
			    			downloadContext.docAndPartyList.add(dp);
			    			vaadinUi.setSessionAttribute(downloadContextPickupCode,downloadContext);
			    			downloadContextPickupCodeList.add(downloadContextPickupCode);
			    			_logger.debug("DEBUG-PICKUP-1: Set download context pickup code: " + downloadContextPickupCode + "; file: "+downloadContext.downloadFilename + "; IP: " + vaadinUi.getRequestHostIp() );
			    	        
			    	    	String servletUrl = vaadinUi.getEsfapp().getExternalContextPath() + "/" + "transnapshotsdownload?" + TranSnapshotsDownload.PARAM_PICKUP_CODE + "=" + downloadContextPickupCode;
			    	    	String downloadUrl = ServletUtil.appendUrlParam(servletUrl, TranSnapshotsDownload.PARAM_MODE, TranSnapshotsDownload.MODE_VIEW);
			    	    	Link downloadDocumentSubLink = new Link(vaadinUi.getMsg("ReportDetailView.button.downloadDocumentSnapshot.document.link.label"), new ExternalResource(downloadUrl));
			    	    	downloadDocumentSubLink.setTargetName("_blank"); // for view, we always do it in another browser window/tab
			    	    	downloadPopupLayout.addComponent(downloadDocumentSubLink);

			    	    	// View PDF document link
			    	        downloadContextPickupCode = vaadinUi.getEsfapp().getRandomKey().getEsfReportsAccessPickupCodeString();
			    	        downloadContext = new TranSnapshotsDownload.DownloadContext();
			    			downloadContext.transactionId = transaction.getId();
			    			downloadContext.formatType = tranDoc.getDocumentVersion().isLandscape() ? TranSnapshotsDownload.FORMAT_PDF_LANDSCAPE : TranSnapshotsDownload.FORMAT_PDF_PORTRAIT;
        					if ( tranPartyDoc.hasDocumentFileName() )
        						downloadContext.downloadFilename = tranPartyDoc.getDocumentFileName();
        					else 
        						downloadContext.downloadFilename = transaction.getPackageDownloadFileName(vaadinUi.getUser(), tranDoc.getDocumentVersion());
			    			downloadContext.docAndPartyList.add(dp);
			    			vaadinUi.setSessionAttribute(downloadContextPickupCode,downloadContext);
			    			downloadContextPickupCodeList.add(downloadContextPickupCode);
			    			_logger.debug("DEBUG-PICKUP-2: Set download context pickup code: " + downloadContextPickupCode + "; file: "+downloadContext.downloadFilename + "; IP: " + vaadinUi.getRequestHostIp() );
			    				    	        
			    			servletUrl = vaadinUi.getEsfapp().getExternalContextPath() + "/" + "transnapshotsdownload?" + TranSnapshotsDownload.PARAM_PICKUP_CODE + "=" + downloadContextPickupCode;
			    			downloadUrl = ServletUtil.appendUrlParam(servletUrl, TranSnapshotsDownload.PARAM_MODE, TranSnapshotsDownload.MODE_DOWNLOAD);
			    	    	Link downloadDocumentAsPDFSubLink = new Link(vaadinUi.getMsg("ReportDetailView.button.downloadDocumentSnapshot.documentAsPDF.link.label"), new ExternalResource(downloadUrl));
			    	    	downloadDocumentAsPDFSubLink.setTargetName(downloadTarget);
			    	    	downloadPopupLayout.addComponent(downloadDocumentAsPDFSubLink);
	    				}
	    			
		    			if ( reportTemplate.hasViewSnapshotDataPermission(vaadinUi.getUser()) ) {
			    	        String downloadContextPickupCode = vaadinUi.getEsfapp().getRandomKey().getEsfReportsAccessPickupCodeString();
			    	        TranSnapshotsDownload.DownloadContext downloadContext = new TranSnapshotsDownload.DownloadContext();
			    			downloadContext.transactionId = transaction.getId();
			    			downloadContext.formatType = TranSnapshotsDownload.FORMAT_XML_DATA_SNAPSHOT;
							if ( tranPartyDoc.hasDocumentFileName() )
								downloadContext.downloadFilename = tranPartyDoc.getDocumentFileName();
							else 
								downloadContext.downloadFilename = vaadinUi.getMsg("ReportDetailView.button.downloadDocumentSnapshot.data.filename",tranDocName,tranPartyName);
			    			downloadContext.docAndPartyList.add(dp);
			    			vaadinUi.setSessionAttribute(downloadContextPickupCode,downloadContext);
			    			downloadContextPickupCodeList.add(downloadContextPickupCode);
			    			_logger.debug("DEBUG-PICKUP-3: Set download context pickup code: " + downloadContextPickupCode + "; file: "+downloadContext.downloadFilename + "; IP: " + vaadinUi.getRequestHostIp() );
			    						    	        
			    			String servletUrl = vaadinUi.getEsfapp().getExternalContextPath() + "/" + "transnapshotsdownload?" + TranSnapshotsDownload.PARAM_PICKUP_CODE + "=" + downloadContextPickupCode;
			    			String downloadUrl = ServletUtil.appendUrlParam(servletUrl, TranSnapshotsDownload.PARAM_MODE, TranSnapshotsDownload.MODE_DOWNLOAD);
			    	    	Link downloadDataSubLink = new Link(vaadinUi.getMsg("ReportDetailView.button.downloadDocumentSnapshot.data.link.label"), new ExternalResource(downloadUrl));
			    	    	downloadDataSubLink.setTargetName(downloadTarget);
			    	    	downloadPopupLayout.addComponent(downloadDataSubLink);
		    			}
	
		    			if ( reportTemplate.hasViewSnapshotDataPermission(vaadinUi.getUser()) && reportTemplate.hasViewSnapshotDocumentPermission(vaadinUi.getUser()) ) {
			    	        String downloadContextPickupCode = vaadinUi.getEsfapp().getRandomKey().getEsfReportsAccessPickupCodeString();
			    	        TranSnapshotsDownload.DownloadContext downloadContext = new TranSnapshotsDownload.DownloadContext();
			    			downloadContext.transactionId = transaction.getId();
			    			downloadContext.formatType = TranSnapshotsDownload.FORMAT_XML_SNAPSHOTS;
							if ( tranPartyDoc.hasDocumentFileName() )
								downloadContext.downloadFilename = tranPartyDoc.getDocumentFileName();
							else 
								downloadContext.downloadFilename = vaadinUi.getMsg("ReportDetailView.button.downloadDocumentSnapshot.full.filename",tranDocName,tranPartyName);
			    			downloadContext.docAndPartyList.add(dp);
			    			vaadinUi.setSessionAttribute(downloadContextPickupCode,downloadContext);
			    			downloadContextPickupCodeList.add(downloadContextPickupCode);
			    	        
			    			String servletUrl = vaadinUi.getEsfapp().getExternalContextPath() + "/" + "transnapshotsdownload?" + TranSnapshotsDownload.PARAM_PICKUP_CODE + "=" + downloadContextPickupCode;
			    			String downloadUrl = ServletUtil.appendUrlParam(servletUrl, TranSnapshotsDownload.PARAM_MODE, TranSnapshotsDownload.MODE_DOWNLOAD);
			    	    	Link downloadFullSubLink = new Link(vaadinUi.getMsg("ReportDetailView.button.downloadDocumentSnapshot.full.link.label"), new ExternalResource(downloadUrl));
			    	    	downloadFullSubLink.setTargetName(downloadTarget);
			    	    	downloadPopupLayout.addComponent(downloadFullSubLink);
		    			}
					}
			    }
			}
		}
	}
	
	class DocumentPartyTable extends Table {
		private static final long serialVersionUID = -9058094750981629903L;

		final DocumentPartyTable thisDocumentPartyTable;

		public DocumentPartyTable(DocumentPartyContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			thisDocumentPartyTable = this;
			this.alwaysRecalculateColumnWidths = true;
			setWidth(90,Unit.PERCENTAGE);
			addStyleName("documentPartyTable");
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("ReportDetailView.documentParty.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("ReportDetailView.documentParty.showColumnHeaders"));
			//setColumnAlignment("downloadSnapshotMenu", Align.CENTER); - doesn't appear to be aligning the menu 'button' itself.
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setPageLength(container.size());

			setCellStyleGenerator( new CellStyleGenerator() {
				private static final long serialVersionUID = 6980605787081939078L;

				@Override
				public String getStyle(Table table, Object itemId, Object propertyId) {
					if ( "status".equals(propertyId) ) {
						String status = (String)thisDocumentPartyTable.getItem(itemId).getItemProperty("status").getValue();						
						if ( TransactionParty.STATUS_ACTIVE.equals(status) ) return "active";
						if ( TransactionParty.STATUS_COMPLETED.equals(status) ) return "completed";
						if ( TransactionParty.STATUS_SKIPPED.equals(status) ) return "skipped";
						if ( TransactionParty.STATUS_UNDEFINED.equals(status) ) return "undefined";
						if ( TransactionParty.STATUS_REPORTS.equals(status) ) return "reports";
						return "caution";
					}
					
					if ( "docStatus".equals(propertyId) ) {
						String status = (String)thisDocumentPartyTable.getItem(itemId).getItemProperty("docStatus").getValue();						
						if ( TransactionPartyDocument.STATUS_RETRIEVED.equals(status) ) return "retrieved";
						if ( TransactionPartyDocument.STATUS_FIXED_REQUESTED.equals(status) ) return "retrieved";
						if ( TransactionPartyDocument.STATUS_COMPLETED.equals(status) ) return null;
						if ( TransactionPartyDocument.STATUS_REJECTED.equals(status) ) return "rejected";
						if ( TransactionPartyDocument.STATUS_NOT_YET_RETRIEVED.equals(status) ) return "undefined";
						if ( TransactionPartyDocument.STATUS_VIEW_OPTIONAL.equals(status) ) return "undefined";
						if ( TransactionPartyDocument.STATUS_VIEW_OPTIONAL_VIEWED.equals(status) ) return null;
						if ( TransactionPartyDocument.STATUS_SKIPPED.equals(status) ) return "undefined";
						return "caution";
					}
					
					return null;
				}});
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			if ( "lastUpdatedTimestamp".equals(colId) ) {
				EsfDateTime myDate = (EsfDateTime)property.getValue();
				if ( myDate == null ) 
					return "";
				return myDate.toLogString(vaadinUi.getUser());
			}
			
			if ( "snapshotTimestamp".equals(colId) ) {
				EsfDateTime myDate = (EsfDateTime)property.getValue();
				if ( myDate == null ) 
					return "";
				return myDate.toLogString(vaadinUi.getUser());
			}
			
			if ( "status".equals(colId) ) {
				String status = (String)property.getValue();
				return vaadinUi.getPrettyCode().transactionPartyStatus(status);
			}
			
			if ( "docStatus".equals(colId) ) {
				String status = (String)property.getValue();
				return vaadinUi.getPrettyCode().transactionPartyDocumentStatus(status);
			}
			
			return super.formatPropertyValue(rowId,colId,property);
		}

	} // DocumentPartyTable
}