// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.reloadTransactionData;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.runtime.reports.TransactionListingInfo;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.EsfUUIDValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.ListSelectValid;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

public class ReloadTransactionDataView extends Panel implements EsfView {
	private static final long serialVersionUID = 6764957741752569177L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ReloadTransactionDataView.class);
	
	String packageTemplateIdPropertySuffix;
	com.esignforms.open.prog.Package pkg;
	PackageVersion pkgVersion;
    
    final ReloadTransactionDataView thisView;
	
	// For search bar
	private static final String PROP_TRANSACTION_TEMPLATE_IDS = "ReloadTransactionDataView/transactionTemplateIds/";
	private static final String PROP_DATE_TYPE = "ReloadTransactionDataView/dateType/";
	private static final String PROP_DATE_RANGE = "ReloadTransactionDataView/dateRange/";
	private static final String PROP_FROM_DATE = "ReloadTransactionDataView/fromDate/";
	private static final String PROP_TO_DATE = "ReloadTransactionDataView/toDate/";
	private static final String PROP_ID = "ReloadTransactionDataView/id/";
	private static final String PROP_PARTY_EMAIL = "ReloadTransactionDataView/partyEmail/";
	private static final String PROP_IN_PROGRESS = "ReloadTransactionDataView/inProgress/";
	private static final String PROP_COMPLETED = "ReloadTransactionDataView/completed/";
	private static final String PROP_CANCELED = "ReloadTransactionDataView/canceled/";
	private static final String PROP_SUSPENDED = "ReloadTransactionDataView/suspended/";

	final static EsfUUID anyId = new EsfUUID("1-1-1-1-1"); // don't change this flag value since users who select it will have stored it in their properties
	
	List<EsfUUID> allTransactionTemplateIds;
	GridLayout searchBar;
	ListSelect transactionTemplateSelect;
	OptionGroup searchDateType;
	NativeSelect searchDateRange;
	PopupDateField searchFromDate;
	PopupDateField searchToDate;
	TextField searchId;
	TextField searchPartyEmail;
	CheckBox searchInProgress;
	CheckBox searchCompleted;
	CheckBox searchCanceled;
	CheckBox searchSuspended;
	HorizontalLayout buttonLayout;
	Button reloadButton;
	
	public ReloadTransactionDataView(PackageVersion pkgVersion) {
		super();
		this.thisView = this;
		this.pkgVersion = pkgVersion;
		this.pkg = pkgVersion.getPackage();
		this.packageTemplateIdPropertySuffix = pkg.getId().toNormalizedEsfNameString();
		setStyleName("ReloadTransactionDataView");
		setSizeFull();
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			
			VerticalLayout layout = new VerticalLayout();
			setContent(layout);
			layout.setSizeFull();
			layout.setSpacing(false);
			layout.setMargin(false);

	    	searchBar = new GridLayout(4,3);
	    	searchBar.addStyleName("searchBar");
	    	searchBar.setSpacing(true);
	    	searchBar.setMargin(false);
	    	
	    	Collection<TransactionTemplate> allTransactionTemplates = TransactionTemplate.Manager.getByPackageForUserWithPermission(pkg.getId(), vaadinUi.getUser(), PermissionOption.PERM_OPTION_LIST);
	    	allTransactionTemplateIds = new LinkedList<EsfUUID>();
	    	for( TransactionTemplate tranTemplate : allTransactionTemplates ) {
	    		allTransactionTemplateIds.add(tranTemplate.getId());
	    	}
	    	HashSet<EsfUUID> initialTransactionTemplateSelectValues = new HashSet<EsfUUID>();

        	EsfUUID[] propTransactionTemplateIds = vaadinUi.getUser().getUserDefinedUUIDsProperty(PROP_TRANSACTION_TEMPLATE_IDS+packageTemplateIdPropertySuffix);
        	if ( propTransactionTemplateIds == null || propTransactionTemplateIds.length == 0 ) {
        		initialTransactionTemplateSelectValues.add(anyId);
        	} else {
        		for( EsfUUID id : propTransactionTemplateIds ) {
        			if ( anyId.equals(id) || allTransactionTemplateIds.contains(id) )
        				initialTransactionTemplateSelectValues.add(id);
        		}
        	}
        	transactionTemplateSelect = new ListSelectValid();
        	transactionTemplateSelect.setDescription(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.transactionTemplate.tooltip"));
        	transactionTemplateSelect.addValidator(new SelectValidator(transactionTemplateSelect));
	    	transactionTemplateSelect.setNullSelectionAllowed(false); 
	    	transactionTemplateSelect.setImmediate(false);
	    	transactionTemplateSelect.setRequired(true);
	    	transactionTemplateSelect.setMultiSelect(true);
	    	transactionTemplateSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    		transactionTemplateSelect.addItem(anyId);
    		transactionTemplateSelect.setItemCaption( anyId, vaadinUi.getMsg("ReloadTransactionDataView.searchBar.anyTransactionTemplateId.label") );
	    	for( TransactionTemplate transactionTemplate : allTransactionTemplates ) {
	    		transactionTemplateSelect.addItem(transactionTemplate.getId());
	    		transactionTemplateSelect.setItemCaption( transactionTemplate.getId(), transactionTemplate.getPathName().toString() );
	    	}
        	transactionTemplateSelect.setValue(initialTransactionTemplateSelectValues);
        	transactionTemplateSelect.setRows(Math.min(5,transactionTemplateSelect.size()));
        	searchBar.addComponent(transactionTemplateSelect,0,0,0,1);
	    	
        	HorizontalLayout searchDateLayout = new HorizontalLayout();
        	searchDateLayout.setMargin(false);
        	searchDateLayout.setSpacing(true);
        	searchBar.addComponent(searchDateLayout,2,1,3,1);
        	
        	EsfString propDateTypeEsfString = vaadinUi.getUser().getUserDefinedStringProperty(PROP_DATE_TYPE+packageTemplateIdPropertySuffix);
        	if ( propDateTypeEsfString == null || propDateTypeEsfString.isBlank() )
        		propDateTypeEsfString = new EsfString(Literals.DATE_LAST_UPDATED);
        	searchDateType = new OptionGroup();
        	searchDateType.setStyleName("inline");
        	searchDateType.setDescription(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.date.optiongroup.tooltip"));
        	searchDateType.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
        	searchDateType.addItem(Literals.DATE_STARTED);
        	searchDateType.setItemCaption(Literals.DATE_STARTED, vaadinUi.getMsg("ReloadTransactionDataView.searchBar.date.optiongroup.started.label"));
        	searchDateType.addItem(Literals.DATE_LAST_UPDATED);
        	searchDateType.setItemCaption(Literals.DATE_LAST_UPDATED, vaadinUi.getMsg("ReloadTransactionDataView.searchBar.date.optiongroup.lastUpdated.label"));
        	searchDateType.addItem(Literals.DATE_CANCELS);
        	searchDateType.setItemCaption(Literals.DATE_CANCELS, vaadinUi.getMsg("ReloadTransactionDataView.searchBar.date.optiongroup.cancels.label"));
        	searchDateType.addItem(Literals.DATE_EXPIRES);
        	searchDateType.setItemCaption(Literals.DATE_EXPIRES, vaadinUi.getMsg("ReloadTransactionDataView.searchBar.date.optiongroup.expires.label"));
        	searchDateType.setValue( propDateTypeEsfString.toString() );
        	searchBar.addComponent(searchDateType,2,0,3,0);
        	//searchBar.setComponentAlignment(searchDateType, Alignment.MIDDLE_CENTER);
	    	
        	EsfDate userFromDate;
        	EsfString propFromDateString = vaadinUi.getUser().getUserDefinedStringProperty(PROP_FROM_DATE+packageTemplateIdPropertySuffix);
        	if ( propFromDateString == null || propFromDateString.isNull() ) {
        		userFromDate = new EsfDate(vaadinUi.getUser().getTimezoneTz());
        	} else if ( propFromDateString.isBlank() ) {
        		userFromDate = null;
        	} else {
        		userFromDate = EsfDate.CreateFromYMD(propFromDateString.toString());
        	}
	    	searchFromDate = new PopupDateField();
	    	searchFromDate.setImmediate(true);
	    	searchFromDate.setTimeZone(vaadinUi.getUser().getTimezoneTz());
	    	searchFromDate.setInputPrompt(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchFromDate.label"));
	    	searchFromDate.setDescription(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchFromDate.tooltip"));
	    	searchFromDate.setResolution(Resolution.DAY);
	    	searchFromDate.setValue(userFromDate == null || userFromDate.isNull() ? null : userFromDate.toDate());
	    	//searchFromDate.setLocale(vaadinUi.getLocale());

        	EsfDate userToDate;
        	EsfString propToDateString = vaadinUi.getUser().getUserDefinedStringProperty(PROP_TO_DATE+packageTemplateIdPropertySuffix);
        	if ( propToDateString == null || propToDateString.isBlank() ) {
        		userToDate = null;
        	} else {
        		userToDate = EsfDate.CreateFromYMD(propToDateString.toString());
        	}
	    	searchToDate = new PopupDateField();
	    	searchFromDate.setImmediate(true);
	    	searchToDate.setTimeZone(vaadinUi.getUser().getTimezoneTz());
	    	searchToDate.setInputPrompt(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchToDate.label"));
	    	searchToDate.setDescription(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchToDate.tooltip"));
	    	searchToDate.setResolution(Resolution.DAY);
	    	searchToDate.setValue(userToDate == null || userToDate.isNull() ? null : userToDate.toDate());
	    	//searchToDate.setLocale(vaadinUi.getLocale());
	    	
	    	// We put this after the date range since setting its value will change what appears in those fields
        	EsfString propDateRangeEsfString = vaadinUi.getUser().getUserDefinedStringProperty(PROP_DATE_RANGE+packageTemplateIdPropertySuffix);
        	if ( propDateRangeEsfString == null || propDateRangeEsfString.isBlank() )
        		propDateRangeEsfString = new EsfString(Literals.DATE_LAST_7_DAYS);
	    	searchDateRange = new NativeSelect(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.daterange.select.label"));
	    	searchDateRange.setDescription(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.daterange.select.tooltip"));
	    	searchDateRange.addValidator(new SelectValidator(searchDateRange));
	    	searchDateRange.setNullSelectionAllowed(false); 
	    	searchDateRange.setImmediate(true);
	    	searchDateRange.setRequired(true);
	    	searchDateRange.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
	    	searchDateRange.addItem(Literals.DATE_TODAY);
	    	searchDateRange.setItemCaption( Literals.DATE_TODAY, vaadinUi.getMsg("ReloadTransactionDataView.searchBar.daterange.select.today.label") );
	    	searchDateRange.addItem(Literals.DATE_YESTERDAY);
	    	searchDateRange.setItemCaption( Literals.DATE_YESTERDAY, vaadinUi.getMsg("ReloadTransactionDataView.searchBar.daterange.select.yesterday.label") );
	    	searchDateRange.addItem(Literals.DATE_LAST_7_DAYS);
	    	searchDateRange.setItemCaption( Literals.DATE_LAST_7_DAYS, vaadinUi.getMsg("ReloadTransactionDataView.searchBar.daterange.select.last7.label") );
	    	searchDateRange.addItem(Literals.DATE_LAST_30_DAYS);
	    	searchDateRange.setItemCaption( Literals.DATE_LAST_30_DAYS, vaadinUi.getMsg("ReloadTransactionDataView.searchBar.daterange.select.last30.label") );
	    	searchDateRange.addItem(Literals.DATE_LAST_90_DAYS);
	    	searchDateRange.setItemCaption( Literals.DATE_LAST_90_DAYS, vaadinUi.getMsg("ReloadTransactionDataView.searchBar.daterange.select.last90.label") );
	    	searchDateRange.addItem(Literals.DATE_LAST_365_DAYS);
	    	searchDateRange.setItemCaption( Literals.DATE_LAST_365_DAYS, vaadinUi.getMsg("ReloadTransactionDataView.searchBar.daterange.select.last365.label") );
	    	searchDateRange.addItem(Literals.DATE_RANGE);
	    	searchDateRange.setItemCaption( Literals.DATE_RANGE, vaadinUi.getMsg("ReloadTransactionDataView.searchBar.daterange.select.range.label") );
	    	searchDateRange.addValueChangeListener(new Property.ValueChangeListener() {
				private static final long serialVersionUID = 3655810912068546134L;

				@Override
				public void valueChange(ValueChangeEvent event) {
					String selectedDateRange = event.getProperty().getValue().toString();
					if ( Literals.DATE_RANGE.equals(selectedDateRange) ) {
						searchFromDate.setEnabled(true);
						searchToDate.setEnabled(true);
					} else {
						calculateDateRange(selectedDateRange);
						searchFromDate.setEnabled(false);
						searchToDate.setEnabled(false);
					}
				}
	    	});
	    	searchDateRange.setValue(propDateRangeEsfString.toString());
        	searchDateLayout.addComponent(searchDateRange);
	        searchDateLayout.setComponentAlignment(searchDateRange, Alignment.BOTTOM_CENTER);
        	searchDateLayout.addComponent(searchFromDate);
	        searchDateLayout.setComponentAlignment(searchFromDate, Alignment.BOTTOM_CENTER);
        	searchDateLayout.addComponent(searchToDate);
	        searchDateLayout.setComponentAlignment(searchToDate, Alignment.BOTTOM_CENTER);


        	EsfString propPartyEmail = vaadinUi.getUser().getUserDefinedStringProperty(PROP_PARTY_EMAIL+packageTemplateIdPropertySuffix);
	    	searchPartyEmail = new TextField(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchPartyEmail.label"));
	    	searchPartyEmail.setNullRepresentation("");
	    	searchPartyEmail.setWidth(38, Unit.EX);
	    	searchPartyEmail.setImmediate(false);
	    	searchPartyEmail.setDescription(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchPartyEmail.tooltip"));
	    	searchPartyEmail.setValue(propPartyEmail == null ? null : propPartyEmail.toPlainString());
	    	searchBar.addComponent(searchPartyEmail,1,1);
	    	
	    	EsfUUID userTranId;
        	EsfString propId = vaadinUi.getUser().getUserDefinedStringProperty(PROP_ID+packageTemplateIdPropertySuffix);
        	if ( propId == null || propId.isBlank() ) {
        		userTranId = null;
        	} else {
        		userTranId = new EsfUUID(propId.toPlainString());
        	}
	    	searchId = new TextField();
	    	searchId.setInputPrompt(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchId.label"));
	    	searchId.setNullRepresentation("");
	    	searchId.setWidth(38, Unit.EX);
	    	searchId.addValidator( new EsfUUIDValidator() );
	    	searchId.setImmediate(true);
	    	searchId.setDescription(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchId.tooltip"));
	    	searchId.setValue(userTranId == null || userTranId.isNull() ? null : userTranId.toPlainString());
	    	searchBar.addComponent(searchId,1,0);
	    	
	    	HorizontalLayout statusLayout = new HorizontalLayout();
	    	statusLayout.setSpacing(true);
	    	statusLayout.setMargin(false);
        	EsfBoolean propInProgress = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_IN_PROGRESS+packageTemplateIdPropertySuffix);
        	if ( propInProgress == null ) {
        		propInProgress = new EsfBoolean(true);
        	}

			searchInProgress = new CheckBox(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchInProgress.label"));
			searchInProgress.setDescription(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchInProgress.tooltip"));
			searchInProgress.setValue(propInProgress.isTrue());
			statusLayout.addComponent(searchInProgress);
	    	
        	EsfBoolean propCompleted = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_COMPLETED+packageTemplateIdPropertySuffix);
        	if ( propCompleted == null ) {
        		propCompleted = new EsfBoolean(true);
        	}
	    	searchCompleted = new CheckBox(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchCompleted.label"));
	    	searchCompleted.setDescription(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchCompleted.tooltip"));
	    	searchCompleted.setValue(propCompleted.isTrue());
	    	statusLayout.addComponent(searchCompleted);
	    	
        	EsfBoolean propCanceled = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_CANCELED+packageTemplateIdPropertySuffix);
        	if ( propCanceled == null ) {
        		propCanceled = new EsfBoolean(true);
        	}
	    	searchCanceled = new CheckBox(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchCanceled.label"));
	    	searchCanceled.setDescription(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchCanceled.tooltip"));
	    	searchCanceled.setValue(propCanceled.isTrue());
	    	statusLayout.addComponent(searchCanceled);
	    	
        	EsfBoolean propSuspended = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_SUSPENDED+packageTemplateIdPropertySuffix);
        	if ( propSuspended == null ) {
        		propSuspended = new EsfBoolean(true);
        	}
	    	searchSuspended = new CheckBox(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchSuspended.label"));
	    	searchSuspended.setDescription(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.searchSuspended.tooltip"));
	    	searchSuspended.setValue(propSuspended.isTrue());
	    	statusLayout.addComponent(searchSuspended);
	    	
	    	searchBar.addComponent(statusLayout,0,2,1,2);
	    	
	    	buttonLayout = new HorizontalLayout();
	    	buttonLayout.setSpacing(true);
	    	buttonLayout.setStyleName("searchButtons");
	    	searchBar.addComponent(buttonLayout,2,2,3,2);
	    	
	    	reloadButton = new Button(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.reloadButton.label"));
	    	reloadButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.showAll.icon")));
	    	reloadButton.setDescription(vaadinUi.getMsg("ReloadTransactionDataView.searchBar.reloadButton.tooltip"));
	    	reloadButton.setDisableOnClick(true);
	    	reloadButton.addClickListener( new Button.ClickListener() {			
				private static final long serialVersionUID = 7262404793657362517L;

				@Override
				public void buttonClick(ClickEvent event) {
					reloadTransactions();
					reloadButton.setEnabled(true);
				}
			});
	    	buttonLayout.addComponent(reloadButton);
	    	
	    	layout.addComponent(searchBar);
	    	
			vaadinUi.getMainView().setSearchCurrentModeStyleName(searchBar);

	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("ReportView view exception", e.getMessage());
		}
	}
	
	void reloadTransactions() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		vaadinUi.getMainView().setSearchCurrentModeStyleName(searchBar);

		@SuppressWarnings("unchecked")
		Set<EsfUUID> transactionTemplateIds = (Set<EsfUUID>)transactionTemplateSelect.getValue();
		boolean anyTransactionTemplateIdRequested = false;
		LinkedList<EsfUUID> transactionTemplateIdList = new LinkedList<EsfUUID>();
		for( EsfUUID id : transactionTemplateIds ) {
			if ( id.equals(anyId) ) {
				anyTransactionTemplateIdRequested = true;
				transactionTemplateIdList.clear();
				transactionTemplateIdList.addAll(allTransactionTemplateIds);
				break;
			}
			transactionTemplateIdList.add(id);
		}
		if ( transactionTemplateIdList.size() == 0 ) {
			Errors errors = new Errors();
			errors.addError(vaadinUi.getMsg("ReloadTransactionDataView.error.noTransactionTemplatesSelected"));
			vaadinUi.show(errors);
			return;
		}
		
		EsfUUID[] propTransactionTemplateIds;
		if ( anyTransactionTemplateIdRequested ) {
			propTransactionTemplateIds = new EsfUUID[1];
			propTransactionTemplateIds[0] = anyId;
		} else {
			propTransactionTemplateIds = new EsfUUID[transactionTemplateIdList.size()];
			transactionTemplateIdList.toArray(propTransactionTemplateIds);
		}
		vaadinUi.getUser().setUserDefinedUUIDsProperty(PROP_TRANSACTION_TEMPLATE_IDS+packageTemplateIdPropertySuffix, propTransactionTemplateIds);
		
		String dateType = (String)searchDateType.getValue();
		vaadinUi.getUser().setUserDefinedStringProperty(PROP_DATE_TYPE+packageTemplateIdPropertySuffix, new EsfString(dateType));
		
		String dateRange = (String)searchDateRange.getValue();
		vaadinUi.getUser().setUserDefinedStringProperty(PROP_DATE_RANGE+packageTemplateIdPropertySuffix, new EsfString(dateRange));
		
		Date fromDate = (Date)searchFromDate.getValue();
		EsfString propFromDateString = fromDate == null ? new EsfString("") : new EsfString((new EsfDate(fromDate)).toYMDString());
		vaadinUi.getUser().setUserDefinedStringProperty(PROP_FROM_DATE+packageTemplateIdPropertySuffix, propFromDateString);
		
		Date toDate = (Date)searchToDate.getValue();
		EsfString propToDateString = toDate == null ? new EsfString("") : new EsfString((new EsfDate(toDate)).toYMDString());
		vaadinUi.getUser().setUserDefinedStringProperty(PROP_TO_DATE+packageTemplateIdPropertySuffix, propToDateString);
		
		EsfDateTime esfFromDate = fromDate == null ? null : new EsfDateTime(fromDate);
		if ( esfFromDate != null ) {
			esfFromDate.normalizeTimeToZero(vaadinUi.getUser().getTimezoneTz());
		}
		EsfDateTime esfToDate = toDate == null ? null : new EsfDateTime(toDate);
		if ( esfToDate != null ) {
			esfToDate.normalizeTimeToEndOfDay(vaadinUi.getUser().getTimezoneTz());
		}
		
		EsfUUID tranId = null;
		String tranIdString = (String)searchId.getValue();
		if ( EsfString.isNonBlank(tranIdString) ) {
			tranId = new EsfUUID(tranIdString);
			if ( tranId.isNull() )
			{
				Errors errors = new Errors();
				errors.addError(vaadinUi.getMsg("ReloadTransactionDataView.error.invalidTranId"));
				vaadinUi.show(errors);
				return;
			}
		}
		vaadinUi.getUser().setUserDefinedStringProperty(PROP_ID+packageTemplateIdPropertySuffix, new EsfString(tranIdString));
		
		String partyEmailString = (String)searchPartyEmail.getValue();
		vaadinUi.getUser().setUserDefinedStringProperty(PROP_PARTY_EMAIL+packageTemplateIdPropertySuffix, new EsfString(partyEmailString));
		
		Boolean showInProgress = searchInProgress.getValue();
		vaadinUi.getUser().setUserDefinedBooleanProperty(PROP_IN_PROGRESS+packageTemplateIdPropertySuffix, new EsfBoolean(showInProgress));
		
		Boolean showCompleted = searchCompleted.getValue();
		vaadinUi.getUser().setUserDefinedBooleanProperty(PROP_COMPLETED+packageTemplateIdPropertySuffix, new EsfBoolean(showCompleted));

		Boolean showCanceled = searchCanceled.getValue();
		vaadinUi.getUser().setUserDefinedBooleanProperty(PROP_CANCELED+packageTemplateIdPropertySuffix, new EsfBoolean(showCanceled));
		
		Boolean showSuspended = searchSuspended.getValue();
		vaadinUi.getUser().setUserDefinedBooleanProperty(PROP_SUSPENDED+packageTemplateIdPropertySuffix, new EsfBoolean(showSuspended));
		
		if ( ! showInProgress && ! showCompleted && ! showCanceled && ! showSuspended ) {
			Errors errors = new Errors();
			errors.addError(vaadinUi.getMsg("ReloadTransactionDataView.error.noStatusSelected"));
			vaadinUi.show(errors);
			return;
		}
		
		String searchType = vaadinUi.isProductionMode() ? vaadinUi.getMsg("transaction.production") : vaadinUi.getMsg("transaction.test");

    	List<TransactionListingInfo> foundTranList = TransactionListingInfo.Manager.getMatching(tranId,null,esfFromDate,esfToDate,transactionTemplateIdList,pkgVersion.getId(),partyEmailString,vaadinUi.isProductionMode(),showInProgress,showCompleted,showCanceled,showSuspended,false,0);

    	if ( foundTranList == null ) {
			Errors errors = new Errors();
			errors.addError(vaadinUi.getMsg("ReloadTransactionDataView.error.reloadFailed"));
			vaadinUi.show(errors);
			return;
    	}
    	
    	// Make sure our tran id matches our allowed types
    	if ( tranId != null && foundTranList.size() == 1 ) {
    		TransactionListingInfo foundTran = foundTranList.get(0);
    		TransactionTemplate foundTranTemplate = TransactionTemplate.Manager.getByPathName(foundTran.getTemplatePathName());
    		if ( foundTranTemplate == null || ! allTransactionTemplateIds.contains(foundTranTemplate.getId()) ) {
    			Errors errors = new Errors();
    			errors.addError(vaadinUi.getMsg("ReloadTransactionDataView.error.noPermissionToTranId",tranId));
    			vaadinUi.show(errors);
    			return;
    		}
    	}
    	
    	// Reload the data for these transactions
    	int numReloaded = TransactionListingInfo.Manager.reloadTransactionData(foundTranList);
		
		vaadinUi.showStatus(vaadinUi.getMsg("ReloadTransactionDataView.reloadButton.reloadCount",numReloaded,foundTranList.size(),searchType));
	}
	
	void calculateDateRange(String selectedDateRange) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( Literals.DATE_TODAY.equals(selectedDateRange) ) {
			searchFromDate.setValue( new EsfDate(vaadinUi.getUser().getTimezoneTz()).toDate() );
			searchToDate.setValue(null);
		} else if ( Literals.DATE_YESTERDAY.equals(selectedDateRange) ) {
			EsfDate startday = new EsfDate(vaadinUi.getUser().getTimezoneTz());
			startday.addDays(-1);
			searchFromDate.setValue(startday.toDate());
			searchToDate.setValue(startday.toDate());
		} else if ( Literals.DATE_LAST_7_DAYS.equals(selectedDateRange) ) {
			EsfDate startday = new EsfDate(vaadinUi.getUser().getTimezoneTz());
			startday.addDays(-7);
			searchFromDate.setValue(startday.toDate());
			searchToDate.setValue(null);
		} else if ( Literals.DATE_LAST_30_DAYS.equals(selectedDateRange) ) {
			EsfDate startday = new EsfDate(vaadinUi.getUser().getTimezoneTz());
			startday.addDays(-30);
			searchFromDate.setValue(startday.toDate());
			searchToDate.setValue(null);
		} else if ( Literals.DATE_LAST_90_DAYS.equals(selectedDateRange) ) {
			EsfDate startday = new EsfDate(vaadinUi.getUser().getTimezoneTz());
			startday.addDays(-90);
			searchFromDate.setValue(startday.toDate());
			searchToDate.setValue(null);
		} else if ( Literals.DATE_LAST_365_DAYS.equals(selectedDateRange) ) {
			EsfDate startday = new EsfDate(vaadinUi.getUser().getTimezoneTz());
			startday.addDays(-365);
			searchFromDate.setValue(startday.toDate());
			searchToDate.setValue(null);
		}
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return "";
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
}