// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.transactionListing;

import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.runtime.reports.TransactionListingInfo;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;

public class TransactionListingBean implements java.io.Serializable {
	private static final long serialVersionUID = 8672531823104080163L;

	TransactionListingInfo tranListing;
	
	public TransactionListingBean(TransactionListingInfo tranListing) {
		this.tranListing = tranListing;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o !=null && o instanceof TransactionListingBean )
			return tranListing.equals(((TransactionListingBean)o).tranListing);
		return false;
	}
	@Override
    public int hashCode() {
    	return tranListing.hashCode();
    }
	public int compareTo(TransactionListingBean o) {
		return tranListing.compareTo(o.tranListing);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public TransactionListingInfo tranListing() {
		return tranListing;
	}

	public EsfUUID getId()
	{
		return tranListing.getId();
	}
	
	public String getStatus()
	{
		return tranListing.getStatus();
	}
	
	public String getStatusText()
	{
		return tranListing.getStatusText();
	}
	
	public EsfPathName getTemplatePathName()
	{
		return tranListing.getTemplatePathName();
	}
	
	public EsfPathName getPackagePathName()
	{
		return tranListing.getPackagePathName();
	}
	
	public EsfDateTime getLastUpdatedTimestamp()
	{
		return tranListing.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp(User forUser) {
		return getLastUpdatedTimestamp().toString(forUser);
	}
	
	public EsfDateTime getCreatedTimestamp()
	{
		return tranListing.getCreatedTimestamp();
	}
	public String formatCreatedTimestamp(User forUser) {
		return getCreatedTimestamp().toString(forUser);
	}
	
	public EsfDateTime getCancelTimestamp()
	{
		return tranListing.getCancelTimestamp();
	}
	public String formatCanceledTimestamp(User forUser) {
		EsfDateTime t = getCancelTimestamp();
		return t == null ? "" : t.toString(forUser);
	}
	
	public EsfDateTime getExpireTimestamp()
	{
		return tranListing.getExpireTimestamp();
	}
	public String formatExpireTimestamp() {
		EsfDateTime t = getExpireTimestamp();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return t == null ? vaadinUi.getMsg("TransactionListingView.table.nullExpireTimestamp") : t.toString(vaadinUi.getUser());
	}

	public EsfDateTime getStallTimestamp()
	{
		return tranListing.getStallTimestamp();
	}
	public String formatStallTimestamp(User forUser) {
		EsfDateTime t = getStallTimestamp();
		return t == null ? "" : t.toString(forUser);
	}

}