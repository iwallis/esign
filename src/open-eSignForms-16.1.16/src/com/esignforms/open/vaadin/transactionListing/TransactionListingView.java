// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.transactionListing;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.vaadin.resetbuttonfortextfield.ResetButtonForTextField;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.reports.TransactionListingInfo;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.EsfUUIDValidator;
import com.esignforms.open.vaadin.widget.ListSelectValid;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class TransactionListingView extends Panel implements EsfView {
	private static final long serialVersionUID = -5226783498071540661L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TransactionListingView.class);
	
    final TransactionListingView thisView;
    TransactionListingContainer container;
    TransactionListingTable table;
	
	// For search bar
	private static final String PROP_TRANSACTION_TEMPLATE_IDS = "TransactionListingView/transactionTemplateIds";
	private static final String PROP_FROM_DATE = "TransactionListingView/fromDate";
	private static final String PROP_TO_DATE = "TransactionListingView/toDate";
	private static final String PROP_ID = "TransactionListingView/id";
	private static final String PROP_PARTY_PICKUP_CODE = "TransactionListingView/partyPickupCode";
	private static final String PROP_PARTY_EMAIL = "TransactionListingView/partyEmail";
	private static final String PROP_IN_PROGRESS = "TransactionListingView/inProgress";
	private static final String PROP_COMPLETED = "TransactionListingView/completed";
	private static final String PROP_CANCELED = "TransactionListingView/canceled";
	private static final String PROP_SUSPENDED = "TransactionListingView/suspended";
	private static final String PROP_STALLED = "TransactionListingView/stalled";
	private static final String PROP_MAX_TRANSACTIONS = "TransactionListingView/maxTransactions";

	VerticalLayout searchBarLayout;

	ListSelect transactionTemplateSelect;
	PopupDateField searchFromDate;
	PopupDateField searchToDate;
	TextField searchId;
	TextField searchPartyPickupCode;
	TextField searchPartyEmail;
	CheckBox searchInProgress;
	CheckBox searchCompleted;
	CheckBox searchCanceled;
	CheckBox searchSuspended;
	CheckBox searchStalledOnly;
	Button findButton;
	String findButtonLabel;
	NativeSelect maxFoundSelect;
	
	public TransactionListingView() {
		super();
		thisView = this;
		setStyleName("TransactionListingView");
		setSizeFull();
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();

		User currentUser = vaadinUi.getUser();
		try {
			container = new TransactionListingContainer();
			
			VerticalLayout layout = new VerticalLayout();
			setContent(layout);
			layout.setSizeFull();
			layout.setSpacing(false);
			layout.setMargin(false);

			searchBarLayout = new VerticalLayout();
			searchBarLayout.setWidth(100,Unit.PERCENTAGE);
			searchBarLayout.setMargin(false);
			searchBarLayout.setSpacing(false);
			
			GridLayout searchBar = new GridLayout(6,2);
	    	searchBar.addStyleName("searchBar");
	    	searchBar.setSpacing(true);
	    	searchBar.setMargin(false);
	    	searchBarLayout.addComponent(searchBar);
	    	
    		TransactionTemplate templateTransactionTemplate = TransactionTemplate.Manager.getTemplate();
        	Collection<TransactionTemplate> allTransactionTemplates = TransactionTemplate.Manager.getForUserWithPermission(currentUser, PermissionOption.PERM_OPTION_LIST);
        	
	    	HashSet<EsfUUID> initialTransactionTemplateSelectValues = new HashSet<EsfUUID>();

        	EsfUUID[] propTransactionTemplateIds = currentUser.getUserDefinedUUIDsProperty(PROP_TRANSACTION_TEMPLATE_IDS);
        	if ( propTransactionTemplateIds == null || propTransactionTemplateIds.length == 0 ) {
            	for( TransactionTemplate transactionTemplate : allTransactionTemplates ) {
            		if ( templateTransactionTemplate.equals(transactionTemplate) )
            			continue;
            		initialTransactionTemplateSelectValues.add(transactionTemplate.getId());
            	}
        	} else {
        		for( EsfUUID id : propTransactionTemplateIds ) { // only use the property-saved transaction ids if they are in our list still
            		if ( id.equals(templateTransactionTemplate.getId()) )
            			continue;
                	for( TransactionTemplate transactionTemplate : allTransactionTemplates ) {
                		if ( id.equals(transactionTemplate.getId()) ) {
                			initialTransactionTemplateSelectValues.add(transactionTemplate.getId());
                			break;
                		}
                	}
        		}
        	}
        	transactionTemplateSelect = new ListSelectValid();
	    	transactionTemplateSelect.setNullSelectionAllowed(false); 
	    	transactionTemplateSelect.setImmediate(true);
	    	transactionTemplateSelect.setRequired(true);
	    	transactionTemplateSelect.setMultiSelect(true);
	    	transactionTemplateSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
        	for( TransactionTemplate transactionTemplate : allTransactionTemplates ) {
        		if ( templateTransactionTemplate.equals(transactionTemplate) )
        			continue;
        		transactionTemplateSelect.addItem(transactionTemplate.getId());
        		transactionTemplateSelect.setItemCaption( transactionTemplate.getId(), transactionTemplate.getPathName().toString() );
        	}        	
        	transactionTemplateSelect.setValue(initialTransactionTemplateSelectValues);
        	transactionTemplateSelect.setRows(Math.min(5,transactionTemplateSelect.size()));
        	searchBar.addComponent(transactionTemplateSelect,0,0,0,1);
	    	
        	EsfDate userFromDate;
        	EsfString propFromDateString = currentUser.getUserDefinedStringProperty(PROP_FROM_DATE);
        	if ( propFromDateString == null || propFromDateString.isNull() ) {
        		userFromDate = new EsfDate(currentUser.getTimezoneTz());
        	} else if ( propFromDateString.isBlank() ) {
        		userFromDate = null;
        	} else {
        		userFromDate = EsfDate.CreateFromYMD(propFromDateString.toString());
        	}
	    	searchFromDate = new PopupDateField();
	    	searchFromDate.setImmediate(true);
	    	searchFromDate.setDateFormat(vaadinUi.getEsfapp().getDefaultDateFormat());
	    	searchFromDate.setTimeZone(currentUser.getTimezoneTz());
	    	searchFromDate.setInputPrompt(vaadinUi.getMsg("TransactionListingView.searchBar.searchFromDate.label"));
	    	searchFromDate.setDescription(vaadinUi.getMsg("TransactionListingView.searchBar.searchFromDate.tooltip"));
	    	searchFromDate.setResolution(Resolution.DAY);
	    	searchFromDate.setValue(userFromDate == null || userFromDate.isNull() ? null : userFromDate.toDate());
	    	searchFromDate.setConversionError(vaadinUi.getMsg("tooltip.invalid.date"));
	    	//searchFromDate.setLocale(vaadinUi.getLocale());
	    	searchBar.addComponent(searchFromDate,1,0);

        	EsfDate userToDate;
        	EsfString propToDateString = currentUser.getUserDefinedStringProperty(PROP_TO_DATE);
        	if ( propToDateString == null || propToDateString.isBlank() ) {
        		userToDate = null;
        	} else {
        		userToDate = EsfDate.CreateFromYMD(propToDateString.toString());
        	}
	    	searchToDate = new PopupDateField();
	    	searchToDate.setImmediate(true);
	    	searchToDate.setDateFormat(vaadinUi.getEsfapp().getDefaultDateFormat());
	    	searchToDate.setTimeZone(currentUser.getTimezoneTz());
	    	searchToDate.setInputPrompt(vaadinUi.getMsg("TransactionListingView.searchBar.searchToDate.label"));
	    	searchToDate.setDescription(vaadinUi.getMsg("TransactionListingView.searchBar.searchToDate.tooltip"));
	    	searchToDate.setResolution(Resolution.DAY);
	    	searchToDate.setValue(userToDate == null || userToDate.isNull() ? null : userToDate.toDate());
	    	searchToDate.setConversionError(vaadinUi.getMsg("tooltip.invalid.date"));
	    	//searchToDate.setLocale(vaadinUi.getLocale());
	    	searchBar.addComponent(searchToDate,2,0);
	    	
	    	EsfUUID userTranId;
        	EsfString propId = currentUser.getUserDefinedStringProperty(PROP_ID);
        	if ( propId == null || propId.isBlank() ) {
        		userTranId = null;
        	} else {
        		userTranId = new EsfUUID(propId.toPlainString());
        	}
	    	searchId = new TextField();
	    	ResetButtonForTextField.extend(searchId);
	    	searchId.setStyleName("searchId");
	    	searchId.setNullRepresentation("");
	    	searchId.addValidator( new EsfUUIDValidator() );
	    	searchId.setImmediate(true);
	    	searchId.setInputPrompt(vaadinUi.getMsg("TransactionListingView.searchBar.searchId.label"));
	    	searchId.setDescription(vaadinUi.getMsg("TransactionListingView.searchBar.searchId.tooltip"));
	    	searchId.addValueChangeListener( new ValueChangeListener() {
				private static final long serialVersionUID = 7889587036190479743L;

				@Override
				public void valueChange(ValueChangeEvent event) {
					String v = searchId.getValue();
					boolean isBlank = v == null || v.length() == 0;
					if ( ! isBlank )
						searchPartyPickupCode.setValue(null);
					searchPartyPickupCode.setEnabled( isBlank );
				}
	    	});
	    	searchBar.addComponent(searchId,3,0);
	    	
	    	// If we have a property setting for the tran id, we'll ignore any setting for pickup code
        	EsfString propPartyPickupCode = null;
        	if ( userTranId == null )
        		propPartyPickupCode = currentUser.getUserDefinedStringProperty(PROP_PARTY_PICKUP_CODE);
        	
	    	searchPartyPickupCode = new TextField();
	    	ResetButtonForTextField.extend(searchPartyPickupCode);
	    	searchPartyPickupCode.setStyleName("searchPartyPickupCode");
	    	searchPartyPickupCode.setNullRepresentation("");
	    	searchPartyPickupCode.setInputPrompt(vaadinUi.getMsg("TransactionListingView.searchBar.searchPartyPickupCode.label"));
	    	searchPartyPickupCode.setDescription(vaadinUi.getMsg("TransactionListingView.searchBar.searchPartyPickupCode.tooltip"));
	    	searchPartyPickupCode.setImmediate(true);
	    	searchPartyPickupCode.addValueChangeListener( new ValueChangeListener() {
				private static final long serialVersionUID = 618054973515797839L;

				@Override
				public void valueChange(ValueChangeEvent event) {
					String v = searchPartyPickupCode.getValue();
					boolean isBlank = v == null || v.length() == 0;
					if ( ! isBlank )
						searchId.setValue(null);
					searchId.setEnabled( isBlank );
				}
	    	});
	    	searchBar.addComponent(searchPartyPickupCode,4,0);
	    	
	    	// Now that the two related fields are in place, let's set their initial values
	    	searchId.setValue(userTranId == null || userTranId.isNull() ? null : userTranId.toPlainString());
	    	searchPartyPickupCode.setValue(propPartyPickupCode == null ? null : propPartyPickupCode.toPlainString());
	    	
        	EsfString propPartyEmail = currentUser.getUserDefinedStringProperty(PROP_PARTY_EMAIL);
	    	searchPartyEmail = new TextField();
	    	ResetButtonForTextField.extend(searchPartyEmail);
	    	searchPartyEmail.setStyleName("searchPartyEmail");
	    	searchPartyEmail.setNullRepresentation("");
	    	searchPartyEmail.setImmediate(false);
	    	searchPartyEmail.setInputPrompt(vaadinUi.getMsg("TransactionListingView.searchBar.searchPartyEmail.label"));
	    	searchPartyEmail.setDescription(vaadinUi.getMsg("TransactionListingView.searchBar.searchPartyEmail.tooltip"));
	    	searchPartyEmail.setValue(propPartyEmail == null ? null : propPartyEmail.toPlainString());
	    	searchBar.addComponent(searchPartyEmail,5,0);
	    	
	    	HorizontalLayout statusLayout = new HorizontalLayout();
	    	statusLayout.setSpacing(true);
	    	statusLayout.setMargin(false);
        	EsfBoolean propInProgress = currentUser.getUserDefinedBooleanProperty(PROP_IN_PROGRESS);
        	if ( propInProgress == null ) {
        		propInProgress = new EsfBoolean(true);
        	}

			searchInProgress = new CheckBox(vaadinUi.getMsg("TransactionListingView.searchBar.searchInProgress.label"));
			searchInProgress.setDescription(vaadinUi.getMsg("TransactionListingView.searchBar.searchInProgress.tooltip"));
			searchInProgress.setValue(propInProgress.isTrue());
			statusLayout.addComponent(searchInProgress);
	    	
        	EsfBoolean propCompleted = currentUser.getUserDefinedBooleanProperty(PROP_COMPLETED);
        	if ( propCompleted == null ) {
        		propCompleted = new EsfBoolean(true);
        	}
	    	searchCompleted = new CheckBox(vaadinUi.getMsg("TransactionListingView.searchBar.searchCompleted.label"));
	    	searchCompleted.setDescription(vaadinUi.getMsg("TransactionListingView.searchBar.searchCompleted.tooltip"));
	    	searchCompleted.setValue(propCompleted.isTrue());
	    	statusLayout.addComponent(searchCompleted);
	    	
        	EsfBoolean propCanceled = currentUser.getUserDefinedBooleanProperty(PROP_CANCELED);
        	if ( propCanceled == null ) {
        		propCanceled = new EsfBoolean(true);
        	}
	    	searchCanceled = new CheckBox(vaadinUi.getMsg("TransactionListingView.searchBar.searchCanceled.label"));
	    	searchCanceled.setDescription(vaadinUi.getMsg("TransactionListingView.searchBar.searchCanceled.tooltip"));
	    	searchCanceled.setValue(propCanceled.isTrue());
	    	statusLayout.addComponent(searchCanceled);
	    	
        	EsfBoolean propSuspended = currentUser.getUserDefinedBooleanProperty(PROP_SUSPENDED);
        	if ( propSuspended == null ) {
        		propSuspended = new EsfBoolean(true);
        	}
	    	searchSuspended = new CheckBox(vaadinUi.getMsg("TransactionListingView.searchBar.searchSuspended.label"));
	    	searchSuspended.setDescription(vaadinUi.getMsg("TransactionListingView.searchBar.searchSuspended.tooltip"));
	    	searchSuspended.setValue(propSuspended.isTrue());
	    	statusLayout.addComponent(searchSuspended);
	    	
        	EsfBoolean propStalled = currentUser.getUserDefinedBooleanProperty(PROP_STALLED);
        	if ( propStalled == null ) {
        		propStalled = new EsfBoolean(false);
        	}
	    	searchStalledOnly = new CheckBox(vaadinUi.getMsg("TransactionListingView.searchBar.searchStalledOnly.label"));
	    	searchStalledOnly.setDescription(vaadinUi.getMsg("TransactionListingView.searchBar.searchStalledOnly.tooltip"));
	    	searchStalledOnly.setImmediate(true);
	    	searchStalledOnly.addValueChangeListener( new ValueChangeListener() {
				private static final long serialVersionUID = 7889587036190479743L;

				@Override
				public void valueChange(ValueChangeEvent event) {
					Boolean isChecked = (Boolean)event.getProperty().getValue();
					searchInProgress.setEnabled(!isChecked);
					searchCompleted.setEnabled(!isChecked);
					searchCanceled.setEnabled(!isChecked);
					searchSuspended.setEnabled(!isChecked);
				}
	    	});
	    	statusLayout.addComponent(searchStalledOnly);
	    	searchStalledOnly.setValue(propStalled.isTrue()); // set later so valueChange will fire as necessary.
	    	
	    	searchBar.addComponent(statusLayout,1,1,3,1);
	    	
	    	findButtonLabel = vaadinUi.getMsg("TransactionListingView.searchBar.findButton.label");
	    	findButton = new Button(findButtonLabel);
	    	findButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.showAll.icon")));
	    	findButton.setDescription(vaadinUi.getMsg("TransactionListingView.searchBar.findButton.tooltip"));
	    	findButton.setStyleName(Reindeer.BUTTON_SMALL);
	    	//findButton.addStyleName(Reindeer.BUTTON_DEFAULT);
	    	//findButton.setClickShortcut(KeyCode.ENTER);
	    	findButton.setDisableOnClick(true);
	    	findButton.addClickListener( new Button.ClickListener() {			
				private static final long serialVersionUID = 3656538422103317071L;

				@SuppressWarnings("unchecked")
				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					vaadinUi.ensureLoggedIn();
					User currentUser = vaadinUi.getUser();
					
					vaadinUi.getMainView().setSearchCurrentModeStyleName(searchBarLayout);
					
					if ( ! searchFromDate.isValid() || ! searchToDate.isValid() ) {
						vaadinUi.showWarning(null, vaadinUi.getMsg("form.data.invalid"));
						findButton.setEnabled(true);
						return;
					}

					Set<EsfUUID> transactionTemplateIds = (Set<EsfUUID>)transactionTemplateSelect.getValue();
					LinkedList<EsfUUID> transactionTemplateIdList = new LinkedList<EsfUUID>();
					for( EsfUUID id : transactionTemplateIds ) {
						transactionTemplateIdList.add(id);
					}
					if ( transactionTemplateIdList.size() == 0 ) {
						Errors errors = new Errors();
						errors.addError(vaadinUi.getMsg("TransactionListingView.error.noTransactionTemplatesSelected"));
						vaadinUi.show(errors);
						findButton.setEnabled(true);
						return;
					}
					
					EsfUUID[] propTransactionTemplateIds = new EsfUUID[transactionTemplateIdList.size()];
					transactionTemplateIdList.toArray(propTransactionTemplateIds);
					currentUser.setUserDefinedUUIDsProperty(PROP_TRANSACTION_TEMPLATE_IDS, propTransactionTemplateIds);
					
					Date fromDate = (Date)searchFromDate.getValue();
					EsfString propFromDateString = fromDate == null ? new EsfString("") : new EsfString((new EsfDate(fromDate)).toYMDString());
					currentUser.setUserDefinedStringProperty(PROP_FROM_DATE, propFromDateString);
					
					Date toDate = (Date)searchToDate.getValue();
					EsfString propToDateString = toDate == null ? new EsfString("") : new EsfString((new EsfDate(toDate)).toYMDString());
					currentUser.setUserDefinedStringProperty(PROP_TO_DATE, propToDateString);
					
					EsfDateTime esfFromDate = fromDate == null ? null : new EsfDateTime(fromDate);
					if ( esfFromDate != null ) {
						esfFromDate.normalizeTimeToZero(currentUser.getTimezoneTz());
					}
					EsfDateTime esfToDate = toDate == null ? null : new EsfDateTime(toDate);
					if ( esfToDate != null ) {
						esfToDate.normalizeTimeToEndOfDay(currentUser.getTimezoneTz());
					}
					
					EsfUUID tranId = null;
					String tranIdString = searchId.getValue();
					if ( EsfString.isNonBlank(tranIdString) ) {
						tranIdString = tranIdString.trim();
						tranId = new EsfUUID(tranIdString);
						if ( tranId.isNull() ) {
							Errors errors = new Errors();
							errors.addError(vaadinUi.getMsg("TransactionListingView.error.invalidTranId"));
							vaadinUi.show(errors);
							findButton.setEnabled(true);
							return;
						}
						searchId.setValue(tranIdString);
					}
					currentUser.setUserDefinedStringProperty(PROP_ID, new EsfString(tranIdString));
					
					String partyPickupCode = searchPartyPickupCode.getValue();
					if ( EsfString.isNonBlank(partyPickupCode) ) {
						partyPickupCode = partyPickupCode.trim();
						if ( ! vaadinUi.getEsfapp().getRandomKey().isCorrectPickupCodeLength(partyPickupCode) ) {
							Errors errors = new Errors();
							errors.addError(vaadinUi.getMsg("TransactionListingView.error.invalidPartyPickupCode"));
							vaadinUi.show(errors);
							findButton.setEnabled(true);
							return;
						}
						searchPartyPickupCode.setValue(partyPickupCode);
					}
					currentUser.setUserDefinedStringProperty(PROP_PARTY_PICKUP_CODE, new EsfString(partyPickupCode));
					
					String partyEmailString = searchPartyEmail.getValue();
					if ( EsfString.isNonBlank(partyEmailString) ) {
						partyEmailString = partyEmailString.trim();
						searchPartyEmail.setValue(partyEmailString);
					}
					currentUser.setUserDefinedStringProperty(PROP_PARTY_EMAIL, new EsfString(partyEmailString));
					
					Boolean showInProgress = searchInProgress.getValue();
					currentUser.setUserDefinedBooleanProperty(PROP_IN_PROGRESS, new EsfBoolean(showInProgress));
					
					Boolean showCompleted = searchCompleted.getValue();
					currentUser.setUserDefinedBooleanProperty(PROP_COMPLETED, new EsfBoolean(showCompleted));

					Boolean showCanceled = searchCanceled.getValue();
					currentUser.setUserDefinedBooleanProperty(PROP_CANCELED, new EsfBoolean(showCanceled));
					
					Boolean showSuspended = searchSuspended.getValue();
					currentUser.setUserDefinedBooleanProperty(PROP_SUSPENDED, new EsfBoolean(showSuspended));
					
					Boolean showStalledOnly = searchStalledOnly.getValue();
					if ( showStalledOnly && ! showInProgress ) {
						showInProgress = true;
						currentUser.setUserDefinedBooleanProperty(PROP_IN_PROGRESS, new EsfBoolean(showInProgress));
					}
					currentUser.setUserDefinedBooleanProperty(PROP_STALLED, new EsfBoolean(showStalledOnly));
					
					if ( ! showInProgress && ! showCompleted && ! showCanceled && ! showSuspended ) {
						Errors errors = new Errors();
						errors.addError(vaadinUi.getMsg("TransactionListingView.error.noStatusSelected"));
						vaadinUi.show(errors);
						findButton.setEnabled(true);
						return;
					}
					
			    	String searchType = vaadinUi.isProductionMode() ? vaadinUi.getMsg("transaction.production") : vaadinUi.getMsg("transaction.test");
			    	int maxToFind = (int)maxFoundSelect.getValue();
			    	currentUser.setUserDefinedIntegerProperty(PROP_MAX_TRANSACTIONS,new EsfInteger(maxToFind));
			    	
			    	List<TransactionListingInfo> foundTranList = TransactionListingInfo.Manager.getMatching(tranId,partyPickupCode,esfFromDate,esfToDate,transactionTemplateIdList,null/*no limit on package versions*/,partyEmailString,vaadinUi.isProductionMode(),showInProgress,showCompleted,showCanceled,showSuspended,showStalledOnly,maxToFind > 0 ? maxToFind+1 : 0);
			    	// Make sure our tran id matches our allowed types
			    	if ( tranId != null && foundTranList.size() == 1 ) {
			    		TransactionListingInfo foundTran = foundTranList.get(0);
			    		TransactionTemplate foundTranTemplate = TransactionTemplate.Manager.getByPathName(foundTran.getTemplatePathName(),currentUser);
			    		if ( foundTranTemplate == null ) {
			    			Errors errors = new Errors();
			    			errors.addError(vaadinUi.getMsg("TransactionListingView.error.noPermissionToTranId",tranId));
			    			vaadinUi.show(errors);
			    			findButton.setEnabled(true);
			    			return;
			    		}
			    	} else if ( EsfString.isNonBlank(partyPickupCode) && foundTranList.size() == 1 ) {
			    		TransactionListingInfo foundTran = foundTranList.get(0);
			    		TransactionTemplate foundTranTemplate = TransactionTemplate.Manager.getByPathName(foundTran.getTemplatePathName(),currentUser);
			    		if ( foundTranTemplate == null ) {
			    			Errors errors = new Errors();
			    			errors.addError(vaadinUi.getMsg("TransactionListingView.error.noPermissionToPartyPickupCode",partyPickupCode));
			    			vaadinUi.show(errors);
			    			findButton.setEnabled(true);
			    			return;
			    		}
			    	}
			    	
					String findButtonSuffix;
			    	if ( foundTranList.size() > maxToFind && maxToFind > 0 ) {
			    		foundTranList.remove(foundTranList.size()-1); // remove that extra one we did to see if there are more
			    		findButtonSuffix = "+)";
			    	} else {
			    		findButtonSuffix = ")";
			    	}
			    	
					container.refresh(foundTranList);
					vaadinUi.showStatus(vaadinUi.getMsg("TransactionListingView.findButton.findCount",container.size(),searchType));
					vaadinUi.getMainView().setTabTitle(thisView, vaadinUi.getMsg("TransactionListingView.afterSearch.tabTitle",container.size()));
					findButton.setCaption(findButtonLabel + " (" + container.size() + findButtonSuffix);
					findButton.setEnabled(true);
				}
			});
	    	searchBar.addComponent(findButton,4,1);
	    	
	    	EsfInteger maxTransactions = currentUser.getUserDefinedIntegerProperty(PROP_MAX_TRANSACTIONS);
	    	if ( maxTransactions == null ) 
	    		maxTransactions = new EsfInteger(100);
	    	else if ( maxTransactions.isNull() || ! maxTransactions.isPositive() ) // don't allow "unlimited" to remain a default
	    		maxTransactions.setValue(100);
	    	maxFoundSelect = new NativeSelect();
	    	maxFoundSelect.setNullSelectionAllowed(false);
	    	maxFoundSelect.addItem(25);
	    	maxFoundSelect.setItemCaption(25, vaadinUi.getMsg("TransactionListingView.searchBar.maxFoundSelect.number",25));
	    	maxFoundSelect.addItem(50);
	    	maxFoundSelect.setItemCaption(50, vaadinUi.getMsg("TransactionListingView.searchBar.maxFoundSelect.number",50));
	    	maxFoundSelect.addItem(100);
	    	maxFoundSelect.setItemCaption(100, vaadinUi.getMsg("TransactionListingView.searchBar.maxFoundSelect.number",100));
	    	maxFoundSelect.addItem(250);
	    	maxFoundSelect.setItemCaption(250, vaadinUi.getMsg("TransactionListingView.searchBar.maxFoundSelect.number",250));
	    	maxFoundSelect.addItem(500);
	    	maxFoundSelect.setItemCaption(500, vaadinUi.getMsg("TransactionListingView.searchBar.maxFoundSelect.number",500));
	    	maxFoundSelect.addItem(750);
	    	maxFoundSelect.setItemCaption(750, vaadinUi.getMsg("TransactionListingView.searchBar.maxFoundSelect.number",750));
	    	maxFoundSelect.addItem(0);
	    	maxFoundSelect.setItemCaption(0, vaadinUi.getMsg("TransactionListingView.searchBar.maxFoundSelect.unlimited"));
	    	maxFoundSelect.setValue(maxTransactions.toInt());
	    	searchBar.addComponent(maxFoundSelect,5,1);

	    	table = new TransactionListingTable(this,container);
	    	
	    	layout.addComponent(searchBarLayout);
	    	layout.addComponent(table);
	    	layout.setExpandRatio(table, 1.0f);
	    	
	    	vaadinUi.getMainView().setSearchCurrentModeStyleName(searchBarLayout);
			
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("TransactionListingView view exception", e.getMessage());
		}
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return "";
	}

	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
	class TransactionListingTable extends Table {
		private static final long serialVersionUID = -5325101649068152197L;

		public TransactionListingTable(TransactionListingView view, TransactionListingContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("TransactionListingView.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("TransactionListingView.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setSizeFull();
			setSelectable(true);
			setImmediate(true);
	        addValueChangeListener(new Property.ValueChangeListener() {
				private static final long serialVersionUID = 1107746168148796438L;

				public void valueChange(Property.ValueChangeEvent event) {
    	        	Item item = getItem(getValue());
    	        	
    	        	if ( item == null )
    	        		return;
    	        	
    	        	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	    		vaadinUi.ensureLoggedIn();

    	    		EsfUUID tranId = (EsfUUID)item.getItemProperty("id").getValue();
    	        	Transaction tran = Transaction.Manager.getById(tranId);
    	        	if ( tran == null ) {
    	        		Errors errors = new Errors();
    	        		errors.addError(vaadinUi.getMsg("TransactionListingView.error.tranNotFound",tranId));
    	        		vaadinUi.show(errors);
    	        		return;
    	        	}
    	        	
    	            final TransactionDetailView detailView = new TransactionDetailView(tran);
    	            detailView.initView();
    	    		
    	            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("TransactionDetailView.window.caption",tranId), detailView);
    	            w.center();
    	        	w.setWidth(90, Unit.PERCENTAGE);
    	        	w.setHeight(90, Unit.PERCENTAGE);

    	        	detailView.activateView(EsfView.OpenMode.WINDOW, "");
    	        	vaadinUi.addWindowToUI(w);
    	            select(null);
	            }
	        });
	        
	        setCellStyleGenerator( new CellStyleGenerator() {
				private static final long serialVersionUID = 134813707026618374L;

				@Override
				public String getStyle(Table table, Object itemId, Object propertyId) {
					if ( "status".equals(propertyId) ) {
						String status = ((TransactionListingBean)itemId).getStatus();
						
						if ( Transaction.TRAN_STATUS_IN_PROGRESS.equals(status) ) return "inprogress";
						if ( Transaction.TRAN_STATUS_COMPLETED.equals(status) ) return null;
						if ( Transaction.TRAN_STATUS_CANCELED.equals(status) ) return "canceled";
						if ( Transaction.TRAN_STATUS_SUSPENDED.equals(status) ) return "suspended";
						return "suspended";
					}
					
					return null;
				}});
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			User currentUser = vaadinUi.getUser();
			if ( currentUser == null )
				return "?(no user)?";
			
			if ( "lastUpdatedTimestamp".equals(colId) ) {
				return ((TransactionListingBean)rowId).formatLastUpdatedTimestamp(currentUser);
			}
			if ( "createdTimestamp".equals(colId) ) {
				return ((TransactionListingBean)rowId).formatCreatedTimestamp(currentUser);
			}
			if ( "cancelTimestamp".equals(colId) ) {
				return ((TransactionListingBean)rowId).formatCanceledTimestamp(currentUser);
			}
			if ( "expireTimestamp".equals(colId) ) {
				return ((TransactionListingBean)rowId).formatExpireTimestamp();
			}
			if ( "stallTimestamp".equals(colId) ) {
				return ((TransactionListingBean)rowId).formatStallTimestamp(currentUser);
			}
			
			if ( "status".equals(colId) ) {
				String status = ((TransactionListingBean)rowId).getStatus();
				
				if ( Transaction.TRAN_STATUS_IN_PROGRESS.equals(status) ) return vaadinUi.getMsg("transaction.status.inProgress");
				if ( Transaction.TRAN_STATUS_COMPLETED.equals(status) ) return vaadinUi.getMsg("transaction.status.completed");
				if ( Transaction.TRAN_STATUS_CANCELED.equals(status) ) return vaadinUi.getMsg("transaction.status.canceled");
				if ( Transaction.TRAN_STATUS_SUSPENDED.equals(status) ) return vaadinUi.getMsg("transaction.status.suspended");
				return "???";
			}
			
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // TransactionListingTable

}