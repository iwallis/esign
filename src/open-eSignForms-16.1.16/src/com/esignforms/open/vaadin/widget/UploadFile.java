// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.LinkedList;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;


/**
 * Handles the file upload function for File or byte array.
 * 
 * Reworked UploadFileWithProgress.java without the progress indicator which does not seem reliable in being viewed (sometimes appears, sometimes not, rarely showing real progress)
 * and it will sometimes get stuck in a polling loop even though the upload is completed/aborted until the user
 * navigates away from the view where the upload is taking place.
 * 
 * @author Yozons Inc.
 */
public abstract class UploadFile extends VerticalLayout 
	implements Upload.SucceededListener, Upload.FailedListener,	Upload.Receiver, Upload.ProgressListener, Upload.StartedListener
{
	private static final long serialVersionUID = 2315876283469238795L;

	protected Upload upload;
	protected String directoryPath;
	protected String fileName;
	protected String mimeType;
	protected File file;
	protected ByteArrayOutputStream fileDataByteArrayOutputStream;
	protected byte[] fileData;
	protected int maxSizeInBytes; 
	protected boolean isCanceled = false;
	protected boolean isNotAllowedMimeType = false;
	protected boolean isTooBig = false;
	protected Integer contentLength;
	protected LinkedList<String> allowedMimeTypes;
	
	public UploadFile(String fieldCaption, String buttonCaption, String directoryPath, int maxSizeInBytes) {
		upload = new Upload(fieldCaption, null);
		upload.setImmediate(true); // Want to upload right after selecting the file.
		this.addComponent(upload);
		this.maxSizeInBytes = maxSizeInBytes < 1 ? Integer.MAX_VALUE : maxSizeInBytes;
		this.directoryPath = directoryPath;
		upload.setButtonCaption(buttonCaption);
		upload.setReceiver(this);
		upload.addSucceededListener((Upload.SucceededListener) this);
		upload.addFailedListener((Upload.FailedListener) this);
		upload.addProgressListener((Upload.ProgressListener) this);
		upload.addStartedListener((Upload.StartedListener) this);
	}
	
	/**
	 * Use this constructor when you want the data just held in memory.
	 * @param fieldCaption
	 * @param buttonCaption
	 * @param maxSizeInBytes the max size of the file upload (limited to MAX_INT).  Zero or less means MAX_INT.
	 */
	public UploadFile(String fieldCaption, String buttonCaption, int maxSizeInBytes) {
		this(fieldCaption,buttonCaption,null,maxSizeInBytes);
	}
	
	public void setAllowedMimeTypesForImages() {
		allowedMimeTypes = new LinkedList<String>();
		allowedMimeTypes.add(Application.CONTENT_TYPE_GIF);
		allowedMimeTypes.add(Application.CONTENT_TYPE_JPEG);
		allowedMimeTypes.add(Application.CONTENT_TYPE_PJPEG);
		allowedMimeTypes.add(Application.CONTENT_TYPE_PNG);
		allowedMimeTypes.add(Application.CONTENT_TYPE_XPNG);
	}
	
	public void setAllowedMimeTypesForXml() {
		allowedMimeTypes = new LinkedList<String>();
		allowedMimeTypes.add(Application.CONTENT_TYPE_XML);
		allowedMimeTypes.add(Application.CONTENT_TYPE_TEXT_XML);
	}
	
	public void setButtonCaption(String buttonCaption) {
		upload.setButtonCaption(buttonCaption);
		upload.requestRepaint();
	}
	
	boolean isAllowedMimeType(String mimeType) {
		if ( allowedMimeTypes == null )
			return true;
		for( String allowMimeType : allowedMimeTypes ) {
			if ( allowMimeType.equals(mimeType) ) {
				return true;
			}
		}
		return false;
	}

	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		resetUpload();
		this.fileName = filename;
		this.mimeType = mimeType;
		if ( ! isAllowedMimeType(mimeType) ) {
			isNotAllowedMimeType = true;
			upload.interruptUpload();
			return receiveUploadToBuffer(filename,mimeType);
		}
		return hasDirectoryPath() ? receiveUploadToFile(filename,mimeType) : receiveUploadToBuffer(filename,mimeType);
	}
	
	OutputStream receiveUploadToFile(String filename, String mimeType) {
		File directoryDir = new File(directoryPath);
		if ( ! directoryDir.exists() ) {
			directoryDir.mkdirs();
		}
		
		this.fileName = filename;
		file = new File(directoryDir, filename);
		fileData = null;

		try {
			return new FileOutputStream(file);
		} catch (final java.io.FileNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	OutputStream receiveUploadToBuffer(String filename, String mimeType) {
		try {
			fileDataByteArrayOutputStream = new ByteArrayOutputStream(maxSizeInBytes);
			return fileDataByteArrayOutputStream;
		} catch (final Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void updateProgress(long readBytes, long contentLength) {
		this.contentLength = new Long(contentLength).intValue();
		if (isNotAllowedMimeType || readBytes > maxSizeInBytes || contentLength > maxSizeInBytes) {
			upload.interruptUpload();
			return;
		}
		// Application.getInstance().debug("XXX updateProgress(): " + ( (float)readBytes / (float)contentLength) );
	}

	@Override
	public void uploadStarted(Upload.StartedEvent event) {
		if ( ! isAllowedMimeType(event.getMIMEType()) ) {
			Application.getInstance().debug("UploadFile.uploadedStarted() - INVALID MIME TYPE: " + event.getMIMEType());
			isNotAllowedMimeType = true;
			upload.interruptUpload();
			return;
		}

		if( event.getContentLength() > maxSizeInBytes ) { 
			Application.getInstance().debug("UploadFile.uploadedStarted() - TOO BIG; content-length: " + event.getContentLength() + "; maxSize: " + maxSizeInBytes);
			isTooBig = true;
			upload.interruptUpload();
			return;
		}
	}

	@Override
	public void uploadFailed(Upload.FailedEvent event) {
		isTooBig = false;
		if (contentLength != null && contentLength > maxSizeInBytes) {
			isTooBig = true;
		} else if (isNotAllowedMimeType || isCanceled) {
			// Nothing to do here
		} else {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			vaadinUi.getEsfapp().except(event.getReason(), "UploadFile.uploadFailed()");
		}

		afterUploadFailed(fileName, isCanceled, isNotAllowedMimeType, isTooBig, contentLength == null ? 0 : contentLength, maxSizeInBytes);

		resetUpload();
	}
	
	/**
	 * Override this method to handle the successful upload.
	 */
	@Override
	public void uploadSucceeded(Upload.SucceededEvent event) {
		if ( fileDataByteArrayOutputStream != null ) {
			try {
				fileDataByteArrayOutputStream.flush();
				fileData = fileDataByteArrayOutputStream.toByteArray();
				fileDataByteArrayOutputStream.close(); // presume the upload actually closes this
			} catch (Exception e) {}
			finally {
				fileDataByteArrayOutputStream = null;
			}
		}
		if ( isNotAllowedMimeType ) { // small files can be fully uploaded before we detect an invalid mime type
			Application.getInstance().debug("UploadFile.uploadSucceeded() - INVALID MIME TYPE detected after successful upload: " + mimeType);
			afterUploadFailed(fileName,isCanceled,isNotAllowedMimeType,false,contentLength,maxSizeInBytes); 
			resetUpload();
		} else {
			afterUploadSucceeded();
		}
	}

	protected void showNotification(String caption, String message) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.showError(caption, message);
	}

	public String getDirectoryPath() {
		return directoryPath;
	}
	public boolean hasDirectoryPath() {
		return EsfString.isNonBlank(directoryPath);
	}

	public File getFile() {
		return file;
	}
	public boolean hasFile() {
		return file != null;
	}
	
	public byte[] getFileData() {
		if ( hasFile() && ! hasFileData() ) {
			return null;  // TODO: return the bytes in the file
		}
		return fileData;
	}
	public boolean hasFileData() {
		return fileData != null;
	}
	
	public String getFileName() {
		return fileName;
	}
	public String getMimeType() {
		return mimeType;
	}
	public int getContentLength() {
		return contentLength;
	}
	public boolean hasContentLength() {
		return contentLength >= 0;
	}

	
	protected void resetUpload() {
		if ( hasFile() ) {
			try {
				file.delete();
			} catch (Exception e) {}
			finally {
				file = null;
			}
		}
		if ( fileDataByteArrayOutputStream != null ) {
			try {
				fileDataByteArrayOutputStream.close();
			} catch (Exception e) {}
			finally {
				fileDataByteArrayOutputStream = null;
			}
		}
		fileData = null;
		mimeType = null;
		fileName = null;
		isCanceled = isNotAllowedMimeType = isTooBig = false;
		contentLength = -1;
	}

	/**
	 * Override this method to handle the successful upload.
	 */
	public abstract void afterUploadSucceeded();

	/**
	 * Override this method to handle the failure upload 
	 */
	public abstract void afterUploadFailed(String fileName, boolean wasCanceled, boolean wasInvalidMimeType, boolean wasTooBig, int contentLength, int maxSize);
}