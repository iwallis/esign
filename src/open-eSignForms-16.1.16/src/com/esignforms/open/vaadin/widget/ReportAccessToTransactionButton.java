// Copyright (C) 2014-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.servlet.EsfReportsAccessTransaction;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.BorderStyle;
import com.vaadin.ui.Button;
import com.vaadin.ui.themes.Reindeer;

/**
 * This is a button that optionally appears in a report if the user has view or update permission for the pseudo esf_report_access party
 * to view transactions in "live" mode.
 * 
 * @author Yozons Inc.
 */
public class ReportAccessToTransactionButton extends Button {
	private static final long serialVersionUID = 8618049743638433723L;

	ReportTemplate reportTemplate;
	EsfUUID transactionId;
	boolean forUpdate;
	
	/**
	 * Creates a button for VIEW access to the specified transaction
	 * @param tranId
	 */
	public ReportAccessToTransactionButton(EsfUUID transactionIdParam, ReportTemplate reportTemplateParam) {
		this(transactionIdParam,reportTemplateParam,false);
	}
	
	public ReportAccessToTransactionButton(EsfUUID transactionIdParam, ReportTemplate reportTemplateParam, boolean forUpdateParam) {
		super();
		
		this.transactionId = transactionIdParam;
		this.reportTemplate = reportTemplateParam;
		this.forUpdate = forUpdateParam;
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		setStyleName(Reindeer.BUTTON_LINK);
		
		if ( forUpdate ) {
			setIcon(new ThemeResource(vaadinUi.getMsg("ReportView.results.button.reportAccessToTransactionUpdate.icon")));
			setDescription(vaadinUi.getMsg("ReportView.results.button.reportAccessToTransactionUpdate.tooltip"));
		} else {
			setIcon(new ThemeResource(vaadinUi.getMsg("ReportView.results.button.reportAccessToTransactionView.icon")));
			setDescription(vaadinUi.getMsg("ReportView.results.button.reportAccessToTransactionView.tooltip"));
		}
		
		addClickListener( new ClickListener() {
			private static final long serialVersionUID = 2176458187399718483L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				vaadinUi.ensureLoggedIn();
				
				// We'll do register for reports access pickup via the http session
				EsfReportsAccessTransaction.EsfReportsAccessContext ctx = new EsfReportsAccessTransaction.EsfReportsAccessContext(vaadinUi.getUser(),transactionId,reportTemplate,forUpdate);
				vaadinUi.getHttpSession().setAttribute(ctx.getPickupCode(), ctx);

				Page.getCurrent().open(vaadinUi.getEsfapp().getExternalContextPath()+"/ESF_reports_access/"+ctx.getPickupCode(), "_blank", 800, 800, BorderStyle.DEFAULT);
			}
		});
	}
	
	// This is actually called when reports are exported as CSV/Excel and the report contains this button
	public String toString()
	{
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return forUpdate ? vaadinUi.getMsg("ReportView.results.button.reportAccessToTransactionUpdate.toString") : vaadinUi.getMsg("ReportView.results.button.reportAccessToTransactionView.toString");
	}


}