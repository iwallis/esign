// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import java.util.HashSet;
import java.util.Set;

import com.vaadin.data.Property;
import com.vaadin.ui.ListSelect;

/**
 * This is a simple extension of ListSelect that ensures the value set is valid: one of the values allowed.
 * 
 * @author Yozons Inc.
 */
public class ListSelectValid extends ListSelect {
	private static final long serialVersionUID = 4386503122903062992L;

	public ListSelectValid() {
		super();
    }
    
	public ListSelectValid(String caption) {
		super(caption);
    }
    
	@Override
    protected void setValue(Object newValue, boolean repaintIsNotNeeded) throws Property.ReadOnlyException {
		if ( newValue == null ) {
			super.setValue(newValue,repaintIsNotNeeded);
		} else {
			if ( isMultiSelect() && newValue instanceof Set ) {
				Set<?> newValueSet = (Set<?>)newValue;
	        	HashSet<?> updatedItemIds = null;
	    		for( Object newValueItemId : newValueSet ) {
	    			boolean found = false;
	    	    	for( Object itemId : getItemIds() ) {
	    	    		if ( (itemId == null && newValueItemId == null) || (itemId != null && itemId.equals(newValueItemId)) ) {
	    	    			found = true;
	    	    			break;
	    	    		}
	    	    	}
	    	    	if ( ! found ) {
	    	    		if ( updatedItemIds == null ) {
	    	    			updatedItemIds = new HashSet<Object>(newValueSet);
	    	    		}
	    	    		updatedItemIds.remove(newValueItemId);
	    	    	}
	    		}
				if ( updatedItemIds != null ) {
					super.setValue(updatedItemIds,repaintIsNotNeeded);
				} else {
					super.setValue(newValue,repaintIsNotNeeded);
				}
			} else {
		    	for( Object itemId : getItemIds() ) {
		    		if ( itemId.equals(newValue) ) {
		    			super.setValue(newValue,repaintIsNotNeeded);
		    			break;
		    		}
		    	}
			}
		}
	}
}