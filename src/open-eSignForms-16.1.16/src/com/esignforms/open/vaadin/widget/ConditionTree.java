// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import java.lang.reflect.Constructor;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.runtime.condition.AndCondition;
import com.esignforms.open.runtime.condition.CompoundCondition;
import com.esignforms.open.runtime.condition.Condition;
import com.esignforms.open.runtime.condition.OrCondition;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.main.EsfViewWithConfirmDiscardFormChangesWindowParent;
import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.event.Action;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.shared.MouseEventDetails.MouseButton;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.ui.Tree;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

/**
 * The ConditionTree is a Tree widget that display a conditional expression in hierarchical form.  The root node is considered to be
 * either an AND or OR condition at a minimum. The tree supports re-ordering via drag and drop.
 * @author Yozons Inc.
 */
public class ConditionTree extends Tree implements ItemClickListener, Action.Handler {
	private static final long serialVersionUID = -601949881077037288L;

	private static final com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ConditionTree.class);
	private static final String PROP_LABEL = "label";
	private static final String PROP_CONDITION = "condition";
	
	final HierarchicalContainer container;
	CompoundCondition rootCondition; // must be compound condition: AndCondition or OrCondition
	
	// If set, the conditions are in relationship to a give document version
	DocumentVersion documentVersion;
	
	// If set, the conditions are in relationship to a give package version
	PackageVersion packageVersion;
	
	// Our special actions. All of the custom actions are configurable and have no special code to support them.
    private Action actionNegate;
    private Action actionOrToAnd;
    private Action actionAndToOr;
    private Action actionAddOr;
    private Action actionAddAnd;
    private Action actionDeleteCondition;
    
    static class MyAction extends Action {
		private static final long serialVersionUID = -2336007342038956815L;

		String conditionName;
    	public MyAction(EsfVaadinUI vaadinUi, String conditionName) {
    		super(vaadinUi.getPrettyCode().conditionName(conditionName));
    		this.conditionName = conditionName;
    	}
    	public String getConditionName() { return conditionName; }
    }
    
    private Action[] simpleConditionActions;
    private Action[] orConditionActions;
    private Action[] andConditionActions;
    private Action[] rootOrConditionActions;
    private Action[] rootAndConditionActions;

	public ConditionTree() {
		this(EsfVaadinUI.getInstance().getMsg("ConditionTree.default.label"));
	}
	
	public ConditionTree(String caption) {
		super(caption);
		container = new HierarchicalContainer();
		container.addContainerProperty(PROP_LABEL, String.class, null);
		container.addContainerProperty(PROP_CONDITION, Condition.class, null);
		setItemCaptionPropertyId(PROP_LABEL);
        setContainerDataSource(container);
        setSelectable(true);
        setNullSelectionAllowed(true);
        setImmediate(true);
	}
	
	void setupActions()
	{
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		String conditionListName = packageVersion!=null ? "PrettyCode.package.condition.list" : "PrettyCode.document.condition.list";
		
        actionNegate = new Action(vaadinUi.getPrettyCode().builtinConditionName("Negate"));
        actionOrToAnd = new Action(vaadinUi.getPrettyCode().builtinConditionName("OrToAnd"));
        actionAndToOr = new Action(vaadinUi.getPrettyCode().builtinConditionName("AndToOr"));
        actionAddOr = new Action(vaadinUi.getPrettyCode().builtinConditionName("AddOr"));
        actionAddAnd = new Action(vaadinUi.getPrettyCode().builtinConditionName("AddAnd"));
        actionDeleteCondition = new Action(vaadinUi.getPrettyCode().builtinConditionName("Delete"));
        
        // Create all other actions we have
        List<MyAction> otherActionsList = new LinkedList<MyAction>();
        for( String conditionName : vaadinUi.getMsg(conditionListName).split(";") ) {
        	MyAction otherAction = new MyAction(vaadinUi,conditionName);
        	otherActionsList.add(otherAction);
        }
        
        simpleConditionActions = new Action[]  { actionNegate, actionDeleteCondition };
        
        List<Action> setupActionList = new LinkedList<Action>();
        setupActionList.add(actionNegate);
        setupActionList.add(actionOrToAnd);
        setupActionList.add(actionAddOr);
        setupActionList.add(actionAddAnd);
        setupActionList.addAll(otherActionsList);
        setupActionList.add(actionDeleteCondition);
        orConditionActions = new Action[setupActionList.size()];
        setupActionList.toArray(orConditionActions);

        setupActionList.clear();
        setupActionList.add(actionNegate);
        setupActionList.add(actionAndToOr);
        setupActionList.add(actionAddOr);
        setupActionList.add(actionAddAnd);
        setupActionList.addAll(otherActionsList);
        setupActionList.add(actionDeleteCondition);
        andConditionActions = new Action[setupActionList.size()];
        setupActionList.toArray(andConditionActions);

        setupActionList.clear();
        setupActionList.add(actionNegate);
        setupActionList.add(actionOrToAnd);
        setupActionList.add(actionAddOr);
        setupActionList.add(actionAddAnd);
        setupActionList.addAll(otherActionsList);
        rootOrConditionActions = new Action[setupActionList.size()];
        setupActionList.toArray(rootOrConditionActions);

        setupActionList.clear();
        setupActionList.add(actionNegate);
        setupActionList.add(actionAndToOr);
        setupActionList.add(actionAddOr);
        setupActionList.add(actionAddAnd);
        setupActionList.addAll(otherActionsList);
        rootAndConditionActions = new Action[setupActionList.size()];
        setupActionList.toArray(rootAndConditionActions);

        addItemClickListener( (ItemClickListener)this );
        addActionHandler(this);
        setRootCondition((AndCondition)null);
        
        initializeDND();		
	}
	
	public HierarchicalContainer getContainer() {
		return container;
	}
	
	public void initializeDND() {
		setDropHandler(new DropHandler() {
			private static final long serialVersionUID = -7690112830026708197L;

			public void drop(DragAndDropEvent dropEvent) {
                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
                Object sourceItemId = (Object)t.getItemId();
                if ( sourceItemId == null )
                	return;
                
            	TreeTargetDetails dropData = ((TreeTargetDetails)dropEvent.getTargetDetails());
                Object targetItemId = dropData.getItemIdOver();
                if ( targetItemId == null )
                	return;
                
                // No move if source and target are the same
                if ( sourceItemId == targetItemId )
                	return;

                VerticalDropLocation location = dropData.getDropLocation();

        		Condition targetCondition = (Condition)container.getItem(targetItemId).getItemProperty(PROP_CONDITION).getValue();
        		
        		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

        		// Ignore any drop before or after the root condition unless it's dropped on the root condition.
        		if ( targetCondition == rootCondition && location != VerticalDropLocation.MIDDLE ) {
        			vaadinUi.showWarning(null, vaadinUi.getMsg("ConditionTree.dragOutsideRoot.warning.message"));
        			return;
        		}
                
                // If drop on an item that's isn't an AND or OR condition, we'll treat it as putting it before.
                if ( location == VerticalDropLocation.MIDDLE && ! (targetCondition instanceof CompoundCondition) )
                	location = VerticalDropLocation.TOP;
            	
                if ( location == VerticalDropLocation.MIDDLE ) {
                    if ( container.setParent(sourceItemId, targetItemId) && container.hasChildren(targetItemId) ) {
                        // move first in the container
                        container.moveAfterSibling(sourceItemId, null);
                    }
                } else if ( location == VerticalDropLocation.TOP ) {
                    Object parentId = container.getParent(targetItemId);
                    if ( container.setParent(sourceItemId, parentId) ) {
                        // reorder only the two items, moving source above target
                        container.moveAfterSibling(sourceItemId, targetItemId);
                        container.moveAfterSibling(targetItemId, sourceItemId);
                    }
                } else if ( location == VerticalDropLocation.BOTTOM ) {
                    Object parentId = container.getParent(targetItemId);
                    if ( container.setParent(sourceItemId, parentId) ) {
                        container.moveAfterSibling(sourceItemId, targetItemId);
                    }
                }
                
                // Let's rebuild our conditions based on how the tree looks.
                rebuildRootConditionFromTree();
            }

            public AcceptCriterion getAcceptCriterion() {
                return new And(SourceIsTarget.get(), AcceptItem.ALL);
            }
        });
	}
	
	void rebuildRootConditionFromTree() {
		// We should only have one root item, our rootCondition.
		for( Object rootItemId : rootItemIds() )
		{
			Item rootItem = container.getItem(rootItemId);
			CompoundCondition rootCondition = (CompoundCondition)rootItem.getItemProperty(PROP_CONDITION).getValue();
			rebuildChildrenConditionsFromTree(rootCondition,rootItemId);
		}
	}
	
	void rebuildChildrenConditionsFromTree(CompoundCondition compoundCondition, Object compoundConditionItemId) {
		// Clear any previous conditions in our AND or OR condition
		compoundCondition.removeAllConditions();
		
		// If we have sub-conditions, add them in now.
		if ( container.hasChildren(compoundConditionItemId) ) {
			for( Object childItemId : container.getChildren(compoundConditionItemId) ) {
				Item childItem = container.getItem(childItemId);
				Condition childCondition = (Condition)childItem.getItemProperty(PROP_CONDITION).getValue();
				compoundCondition.addCondition(childCondition);
				// If this sub-condition itself is an AND or OR condition, add it's child conditions in.
				if ( childCondition instanceof CompoundCondition )
					rebuildChildrenConditionsFromTree((CompoundCondition)childCondition,childItemId);
			}
		}
	}
	
	public CompoundCondition getRootCondition() {
		return rootCondition;
	}
	public void setRootCondition(AndCondition c) {
		if ( c == null )
			this.rootCondition = new AndCondition();
		else
			this.rootCondition = c;
		buildTree();
	}
	public void setRootCondition(OrCondition c) {
		if ( c == null )
			this.rootCondition = new OrCondition();
		else 
			this.rootCondition = c;
		buildTree();
	}
	public void setRootCondition(CompoundCondition c) {
		if ( c == null || c instanceof AndCondition )
			setRootCondition((AndCondition)c);
		else
			setRootCondition((OrCondition)c);
	}
	
	public DocumentVersion getDocumentVersion() {
		return documentVersion;
	}
	public boolean hasDocumentVersion() {
		return documentVersion != null;
	}
	public void setDocumentVersion(DocumentVersion docVer) {
		documentVersion = docVer;
		setupActions();
	}
	
	public PackageVersion getPackageVersion() {
		return packageVersion;
	}
	public boolean hasPackageVersion() {
		return packageVersion != null;
	}
	public void setPackageVersion(PackageVersion pkgVer) {
		packageVersion = pkgVer;
		setupActions();
	}
	
	
	void buildTree() {
		container.removeAllItems();
		addCondition(null,rootCondition);
		for (Object id : rootItemIds()) {
            expandItemsRecursively(id);
        }
	}
	
	void setupConditionItem(Condition condition, Object itemId, Object parentItemId) {
		Item item = container.getItem(itemId);
		if ( item == null ) {
			_logger.error("setupConditionItem() - no item found with itemId: " + itemId + "; parentItemId: " + parentItemId + "; for Condition: " + condition.toString());
		} else {
			item.getItemProperty(PROP_LABEL).setValue(condition.toString());
			item.getItemProperty(PROP_CONDITION).setValue(condition);
			
			if ( condition instanceof AndCondition ) {
				container.setChildrenAllowed(itemId, true);
				expandItem(itemId);
				AndCondition andCondition = (AndCondition)condition;
				if ( ! andCondition.hasContainedCondition() ) {
					item.getItemProperty(PROP_LABEL).setValue("(Right-click to add conditions to AND)");
				}
			} else if ( condition instanceof OrCondition ) {
				container.setChildrenAllowed(itemId, true);
				expandItem(itemId);
				OrCondition orCondition = (OrCondition)condition;
				if ( ! orCondition.hasContainedCondition() ) {
					item.getItemProperty(PROP_LABEL).setValue("(Right-click to add conditions to OR)");				
				}
			} else {
				container.setChildrenAllowed(itemId, false);
			}
			
			container.setParent(itemId, parentItemId);
			if ( parentItemId != null ) {
				Item parentItem = container.getItem(parentItemId);
				Condition parentCondition = (Condition)parentItem.getItemProperty(PROP_CONDITION).getValue();
				parentItem.getItemProperty(PROP_LABEL).setValue(parentCondition.toString());
				collapseItem(parentItemId);
				expandItem(parentItemId);
			}
		}
	}
	
	Object addCondition(Object parentItemId, Condition condition) {
        // Create the node item
		Object itemId = container.addItem();
		setupConditionItem(condition, itemId, parentItemId);
		
        if ( condition instanceof CompoundCondition ) {
        	CompoundCondition compoundCondition = (CompoundCondition)condition;
        	for( Condition subCondition : compoundCondition.getContainedConditions() ) {
        		addCondition( itemId, subCondition );
        	}
        }
        return itemId;
	}
	
	void openConditionWindow(final Object itemId, final Condition condition) {
		final Object parentItemId = getParent(itemId);
		String conditionName = condition.getClassSimpleName();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			EsfViewWithConfirmDiscardFormChangesWindowParent view = null;
			
			String conditionViewClassName = "com.esignforms.open.vaadin.config.pkg.rule." + conditionName + ".ConditionView";
			Class<?> conditionViewClass = Class.forName(conditionViewClassName); 
			if ( hasDocumentVersion() ) {
				Constructor<?> conditionViewConstructor = conditionViewClass.getConstructor(new Class[]{DocumentVersion.class,condition.getClass()});
				view = (EsfViewWithConfirmDiscardFormChangesWindowParent)conditionViewConstructor.newInstance(new Object[]{documentVersion,condition});
			} else {
				Constructor<?> conditionViewConstructor = conditionViewClass.getConstructor(new Class[]{PackageVersion.class,condition.getClass()});
				view = (EsfViewWithConfirmDiscardFormChangesWindowParent)conditionViewConstructor.newInstance(new Object[]{packageVersion,condition});
			}
			view.initView();
            
			String captionKey = condition.isNegated() ? "ConditionTree."+conditionName+".view.negated.window.caption" : "ConditionTree."+conditionName+".view.window.caption";
			
			ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg(captionKey), view);
            w.center();
        	w.setWidth(vaadinUi.getMsg("ConditionTree."+conditionName+".view.window.width"));
        	w.setHeight(vaadinUi.getMsg("ConditionTree."+conditionName+".view.window.height"));
        	w.setModal(true);
			w.addCloseListener( new CloseListener() {
				private static final long serialVersionUID = -6136125336408829041L;

				@Override
				public void windowClose(CloseEvent e) {
					if ( ! isReadOnly() )
						setupConditionItem(condition, itemId, parentItemId);
				}
			});
        	view.activateView(EsfView.OpenMode.WINDOW, "");
            view.setReadOnly(isReadOnly());
        	view.setParentWindow(w);
        	vaadinUi.addWindowToUI(w);	

        } catch( Exception e ) {
			vaadinUi.showError(vaadinUi.getMsg("ConditionTree.configuration.view.error.caption"),
								vaadinUi.getMsg("ConditionTree.configuration.view.error.message",conditionName));
			_logger.error("openConditionWindow() with unexpected conditionName for view: " + conditionName,e);
			return;
		}	
	}
	
	@Override
	public void itemClick(final ItemClickEvent event) {
		final String label = (String)event.getItem().getItemProperty(PROP_LABEL).getValue();
		final Condition condition = (Condition)event.getItem().getItemProperty(PROP_CONDITION).getValue();
		final Object itemId = event.getItemId();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		if (event.getButton() == MouseButton.RIGHT) {
			_logger.debug("IGNORING right click on tree item label: " + label + "; condition: " + condition);
		}
		else {
			if ( condition instanceof CompoundCondition )
				vaadinUi.showWarning(null, vaadinUi.getMsg("ConditionTree.compound.condition.click.message"));
			else 
				openConditionWindow(itemId,condition);
			_logger.debug("itemClicked - label: " + label + "; condition: " + condition);
		}
	}
	
	@Override
	public Action[] getActions(Object target, Object sender) {
		if ( target == null )
			return null;

		// Add our right-click action handler
		Item item = getItem(target);
		
		Condition condition = (Condition)item.getItemProperty(PROP_CONDITION).getValue();
		
		if ( condition instanceof AndCondition )
			return condition == rootCondition ? rootAndConditionActions : andConditionActions;
		
		if ( condition instanceof OrCondition )
			return condition == rootCondition ? rootOrConditionActions : orConditionActions;
		
		return simpleConditionActions;
	}

	@Override
	public void handleAction(Action action, Object sender, Object target) {
		Item item = getItem(target);
		if ( item == null ) {
			_logger.error("handleAction() - no item found with target itemId: " + target);
			return;
		} 

		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Object parentItemId = getParent(target);
		final String label = (String)item.getItemProperty(PROP_LABEL).getValue();
		final Condition condition = (Condition)item.getItemProperty(PROP_CONDITION).getValue();
		_logger.debug("handleAction - label: " + label + "; Condition: " + condition.toString());
		
		if ( action == actionNegate ) {
			condition.negate();
			setupConditionItem(condition, target, parentItemId);
		} else if ( action == actionAndToOr ) {
			OrCondition orCondition = new OrCondition((AndCondition)condition);
			if ( condition == rootCondition ) {
				rootCondition = orCondition;
			} else {
				Item parentItem = getItem(parentItemId);
				Condition parentCondition = (Condition)parentItem.getItemProperty(PROP_CONDITION).getValue();
				if ( parentCondition instanceof CompoundCondition ) {
					CompoundCondition parentCompoundCondition = (CompoundCondition)parentCondition;
					parentCompoundCondition.replaceCondition(condition,orCondition);
				}
			}
			setupConditionItem(orCondition, target, parentItemId);
		} else if ( action == actionOrToAnd ) {
			AndCondition andCondition = new AndCondition((OrCondition)condition);
			if ( condition == rootCondition ) {
				rootCondition = andCondition;
			} else {
				Item parentItem = getItem(parentItemId);
				Condition parentCondition = (Condition)parentItem.getItemProperty(PROP_CONDITION).getValue();
				if ( parentCondition instanceof CompoundCondition ) {
					CompoundCondition parentCompoundCondition = (CompoundCondition)parentCondition;
					parentCompoundCondition.replaceCondition(condition,andCondition);
				}
			}
			setupConditionItem(andCondition, target, parentItemId);
		} else if ( action == actionAddOr ) {
			OrCondition newCondition = new OrCondition();
			if ( condition instanceof CompoundCondition ) {
				CompoundCondition compoundCondition = (CompoundCondition)condition;
				compoundCondition.addCondition(newCondition);
			}
			addCondition(target, newCondition);
		} else if ( action == actionAddAnd ) {
			AndCondition newCondition = new AndCondition();
			if ( condition instanceof CompoundCondition ) {
				CompoundCondition compoundCondition = (CompoundCondition)condition;
				compoundCondition.addCondition(newCondition);
			}
			addCondition(target, newCondition);
		} else if ( action == actionDeleteCondition ) {
			if ( parentItemId != null ) {
				Item parentItem = getItem(parentItemId);
				Condition parentCondition = (Condition)parentItem.getItemProperty(PROP_CONDITION).getValue();
				if ( parentCondition instanceof CompoundCondition ) {
					CompoundCondition parentCompoundCondition = (CompoundCondition)parentCondition;
					parentCompoundCondition.removeCondition(condition);
				}
			}
			//removeItem(target);
			container.removeItemRecursively(target);
		} else if ( action instanceof MyAction ) { // is one of our variable conditions
			MyAction myAction = (MyAction)action;
			try {
				Condition newCondition = (Condition)Class.forName("com.esignforms.open.runtime.condition."+myAction.getConditionName()).newInstance();
				if ( condition instanceof CompoundCondition ) {
					CompoundCondition compoundCondition = (CompoundCondition)condition;
					compoundCondition.addCondition(newCondition);
				}
				Object itemId = addCondition(target, newCondition);
				openConditionWindow(itemId,newCondition);
			} catch( Exception e ) {
				_logger.error("handleAction() for condition: " + myAction.getConditionName(), e);
				vaadinUi.showError(null, vaadinUi.getMsg("ConditionTree.undefined.condition",myAction.getConditionName()));
			}
		} else {
			_logger.error("handleAction() unexpected action: " + action.getCaption());
			vaadinUi.showError(null, vaadinUi.getMsg("ConditionTree.unknown.condition",action.getCaption()));
		}
		
		// Since we are manipulating the tree, we'll just keep it open rather than auto-close after an action is handled.
		//((PopupButton)getParent().getParent()).setPopupVisible(false);
	}

	public boolean isDirty() {
		CompoundCondition rootCondition = getRootCondition();
		return rootCondition.isChanged();
	}
	
	public void clearDirty() {
		CompoundCondition rootCondition = getRootCondition();
		rootCondition.clearObjectChanged();
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		setDragMode( v ? TreeDragMode.NONE : TreeDragMode.NODE);
	}
}