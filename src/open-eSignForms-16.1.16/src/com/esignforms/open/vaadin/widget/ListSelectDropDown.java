// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import java.util.List;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.DropDownVersionOption;
import com.esignforms.open.prog.Library;

/**
 * This is a simple extension of ListSelectValid, which extends ListSelect, that allows us to configure them from our DropDown configurations in the library.
 * By default, it looks for Test mode DropDown from the Template Library first, and if not found, it will also then look for a Production version.
 * It is able to support multiple selections.
 * 
 * @author Yozons Inc.
 */
public class ListSelectDropDown extends ListSelectValid {
	private static final long serialVersionUID = -4905038758017690505L;

	private int numOptions = 0;

	public ListSelectDropDown(String caption, EsfName dropDownName) {
		this(caption,dropDownName,false);
    }
    
	public ListSelectDropDown(String caption, EsfName dropDownName, boolean valueIsEsfString) {
		super(caption);
		setNullSelectionAllowed(false);
        DropDown dd = DropDown.Manager.getByName(Library.Manager.getTemplate().getId(), dropDownName);
        if ( dd != null && dd.isEnabled() ) {
        	DropDownVersion ddv = dd.getTestDropDownVersion();
        	if ( ddv == null ) {
            	ddv = dd.getProductionDropDownVersion();
        	}
        	setMultiSelect(ddv.isAllowMultiSelection());
        	List<DropDownVersionOption> optionList = ddv.getOptions();
        	for( DropDownVersionOption ddvo : optionList ) {
        		Object value = valueIsEsfString ? new EsfString(ddvo.getValue()) : ddvo.getValue();
        		addItem(value);
        		setItemCaption(value, ddvo.getOption());
        	}
        	numOptions = optionList.size();
        	// sets a default size so as not to be too big though the caller/creator can call setRows() to set to any other value.
        	if ( numOptions < 3 ) { 
        		setRows(numOptions);
        	} else {
        		setRows(3);
        	}
        } else {
    		setNullSelectionAllowed(true);
        }
    }
    
	public int getNumOptions() {
		return numOptions;
	}
}