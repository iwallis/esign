// Copyright (C) 2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.runtime.workflow.TransactionEngine;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.themes.Reindeer;

/**
 * This is a button that optionally appears in a report if the user has resume permission for specified transaction
 * and that transaction is suspended.
 * 
 * @author Yozons Inc.
 */
public class TranResumeButton extends Button {
	private static final long serialVersionUID = 3656482366172285311L;

	final EsfUUID transactionId;
	final ReportTemplate reportTemplate;
	
	/**
	 * Creates a button for RESUME suspended transaction
	 * @param tranId
	 */
	public TranResumeButton(EsfUUID transactionIdParam, ReportTemplate reportTemplateParam) {
		super();
		
		this.transactionId = transactionIdParam;
		this.reportTemplate = reportTemplateParam;
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		setCaption(vaadinUi.getMsg("TranResumeButton.label"));
		setIcon(new ThemeResource(vaadinUi.getMsg("TranResumeButton.icon")));
		setDescription(vaadinUi.getMsg("TranResumeButton.tooltip"));
		setStyleName(Reindeer.BUTTON_SMALL);
		setDisableOnClick(true);
		
		addClickListener( new ClickListener() {
			private static final long serialVersionUID = 381885968012945825L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				vaadinUi.ensureLoggedIn();
				
				Transaction transaction = Transaction.Manager.getById(transactionId);
				if ( transaction != null )
				{
					if ( transaction.isSuspended() )
					{
						String logText = "User " + vaadinUi.getUser().getFullDisplayName() + " RESUMED suspended transaction id " + transactionId + " via button in report " + reportTemplate.getPathName();
						
	        			TransactionContext tranContext = new TransactionContext(vaadinUi.getUser(), transactionId);
	        			TransactionEngine engine = new TransactionEngine(tranContext);
	        			engine.queueTransactionResumedEvent(logText);
	        			engine.doWork();
	        			transaction = Transaction.Manager.getById(transactionId);
	        			if ( transaction == null || ! transaction.isSuspended() )
	        			{
	        				event.getButton().setVisible(false);
	        				vaadinUi.getUser().logConfigChange(logText);
	        				vaadinUi.showStatus(vaadinUi.getMsg("TranResumeButton.showStatus.success"));
	        			}
	        			else
	        			{
	        				event.getButton().setEnabled(true); // keep enabled
	        				vaadinUi.showStatus(vaadinUi.getMsg("TranResumeButton.showStatus.tranNotResumed"));
	        			}
					}
					else
					{
						event.getButton().setVisible(false);
						vaadinUi.showStatus(vaadinUi.getMsg("TranResumeButton.showStatus.tranNotSuspended"));
					}
				}
				else
				{
					event.getButton().setVisible(false);
					vaadinUi.showStatus(vaadinUi.getMsg("TranResumeButton.showStatus.noTranFound"));
				}
			}
		});
	}
	
	// This is actually called when reports are exported as CSV/Excel and the report contains this button (1/12/2016 - CSV export no longer includes button fields)
	public String toString()
	{
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("TranResumeButton.label");
	}


}