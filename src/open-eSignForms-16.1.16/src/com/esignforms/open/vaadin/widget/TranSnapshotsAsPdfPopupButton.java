// Copyright (C) 2013-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import java.util.LinkedList;
import java.util.List;

import org.vaadin.hene.popupbutton.PopupButton;

import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentIdPartyId;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.servlet.TranSnapshotsDownload;
import com.esignforms.open.util.ServletUtil;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.WebBrowser;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Link;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * This is a PopupButton object designed to allow for downloading transaction document snapshots selected by the user as a PDF.
 * This is mostly deprecated and should use TranSnapshotsDownloadPopupButton instead.
 * 
 * @author Yozons Inc.
 */
@Deprecated
public class TranSnapshotsAsPdfPopupButton extends PopupButton {
	private static final long serialVersionUID = 2837117453036094955L;

	protected int numDocuments = 0;
	protected final Transaction transaction;
	protected final ReportTemplate reportTemplate;
	protected final TranSnapshotsAsPdfPopupButton thisButton;
	protected Link downloadDocumentSnapshotsAsPDFButton;
	protected final TranSnapshotsDownload.DownloadContext downloadContext;
	protected final String tranSnapshotsDownloadContextPickupCode;

	public TranSnapshotsAsPdfPopupButton(Transaction forTran, ReportTemplate forReport) {
		this(forTran, forReport, EsfVaadinUI.getInstance().getMsg("TranSnapshotsAsPdfPopupButton.label"));
	}
	
	public TranSnapshotsAsPdfPopupButton(Transaction forTran, ReportTemplate forReport, String caption) {
		super();

		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		this.thisButton = this;
		this.transaction = forTran;
		this.reportTemplate = forReport;

		this.tranSnapshotsDownloadContextPickupCode = vaadinUi.getEsfapp().getRandomKey().getEsfReportsAccessPickupCodeString(); // not really for ESF_Reports_Access, but close enough!
		
		downloadContext = new TranSnapshotsDownload.DownloadContext();
		downloadContext.transactionId = forTran.getId();
		vaadinUi.setSessionAttribute(tranSnapshotsDownloadContextPickupCode,downloadContext);
		vaadinUi.getEsfapp().debug("DEBUG-PICKUP TranSnapshotsAsPdfPopupButton: Set download context pickup code: " + tranSnapshotsDownloadContextPickupCode + "; file: "+downloadContext.downloadFilename + "; IP: " + vaadinUi.getRequestHostIp() );

		setCaption(caption);
		setIcon(new ThemeResource(vaadinUi.getMsg("button.download.pdf.icon")));
		setStyleName(Reindeer.BUTTON_SMALL);

		VerticalLayout buttonLayout = new VerticalLayout();
		buttonLayout.setSizeUndefined();
		buttonLayout.setMargin(true);
		buttonLayout.setSpacing(true);
		setContent(buttonLayout);
		
		this.addPopupVisibilityListener( new PopupVisibilityListener() {
			private static final long serialVersionUID = 7153865578189655681L;

			@Override
			public void popupVisibilityChange(PopupVisibilityEvent event) {
				if ( event.isPopupVisible() ) {
					if ( ((VerticalLayout)getContent()).getComponentCount() == 0 ) {
						buildButtonLayout();
					}
				}
			}
		});
		
		numDocuments = reportTemplate == null ? transaction.getNumTransactionDocumentsWithDocumentSnapshot() : reportTemplate.getNumAllowedTransactionDocumentsWithDocumentSnapshot(transaction);
	}
	
	protected void buildButtonLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		VerticalLayout layout = (VerticalLayout)getContent();
		
    	String partyLatest = vaadinUi.getMsg("TranSnapshotsAsPdfPopupButton.select.latestParty");
    	String allLatest = vaadinUi.getMsg("TranSnapshotsAsPdfPopupButton.select.allLatestParty");
    	
    	final List<DocumentIdPartyId> allDocAndParty = new LinkedList<DocumentIdPartyId>();
    	for( final TransactionDocument tranDoc : reportTemplate == null ? transaction.getAllTransactionDocuments() : reportTemplate.getAllAllowedTransactionDocuments(transaction) ) {
    		
    		boolean foundFirstPartySnapshot = false;
    		
        	for( TransactionParty tranParty : transaction.getAllTransactionParties() ) {
        		TransactionPartyDocument foundTranPartyDoc = null;
        		List<TransactionPartyDocument> transactionPartyDocuments = tranParty.getNonSkippedTransactionPartyDocuments();
            	for( TransactionPartyDocument tpd : transactionPartyDocuments ) {
            		if ( tpd.getTransactionDocumentId().equals(tranDoc.getId()) ) {
            			foundTranPartyDoc = tpd;
            			break;
            		}
            	}
            	
            	if ( foundTranPartyDoc == null ) {
            		continue;
            	}
	    		if ( foundTranPartyDoc.hasSnapshotXmlOrBlobId() ) {
					Document document = tranDoc.getDocument();
					if ( ! foundFirstPartySnapshot ) {
						foundFirstPartySnapshot = true;
						DocumentIdPartyId dp = new DocumentIdPartyId(document.getId(),null);  // Add in for this document, latest
						dp.documentName = document.getEsfName();
						allDocAndParty.add(dp);
					}
					DocumentIdPartyId dp = new DocumentIdPartyId(document.getId(),document.getEsfName(),
									tranParty.getPackageVersionPartyTemplateId(),tranParty.getPackageVersionPartyTemplate().getEsfName()); // Add in for this document and party
					allDocAndParty.add(dp); 			
	    		}
        	}
    	}

    	String servletUrl = vaadinUi.getEsfapp().getExternalContextPath() + "/" + "transnapshotsdownload?" + TranSnapshotsDownload.PARAM_PICKUP_CODE + "=" + tranSnapshotsDownloadContextPickupCode;
    	String downloadUrl = ServletUtil.appendUrlParam(servletUrl, TranSnapshotsDownload.PARAM_MODE, TranSnapshotsDownload.MODE_DOWNLOAD);

    	// Create (but not yet set up) this button-like lik so it can be used in the next select's value change listener to enable/disable it
        WebBrowser wb = Page.getCurrent().getWebBrowser();
        String downloadTarget = wb.isChrome() || wb.isSafari() ? "_top" : "_blank";	    	        

        downloadDocumentSnapshotsAsPDFButton = new Link(vaadinUi.getMsg("TranSnapshotsAsPdfPopupButton.button.downloadDocumentSnapshotsAsPDF.label"), new ExternalResource(downloadUrl));
    	downloadDocumentSnapshotsAsPDFButton.setStyleName(Reindeer.BUTTON_SMALL);
    	downloadDocumentSnapshotsAsPDFButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.download.pdf.icon")));
    	downloadDocumentSnapshotsAsPDFButton.setTargetName(downloadTarget);
    	downloadDocumentSnapshotsAsPDFButton.setEnabled(false);

    	final ListSelect select = new ListSelectValid(vaadinUi.getMsg("TranSnapshotsAsPdfPopupButton.documentIdSet.label"));
    	select.setNullSelectionAllowed(true);
    	select.setRequired(false);
    	select.setImmediate(true);
    	select.setMultiSelect(true);
    	select.setRows( Math.min(10, allDocAndParty.size()+1)); // +1 for the allLatestDocAndParty we add in addition
        select.addValidator(new SelectValidator(select));
    	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	
    	if ( allDocAndParty.size() > 0 ) {
        	DocumentIdPartyId allLatestDocAndParty = new DocumentIdPartyId(DocumentIdPartyId.allLatestId,DocumentIdPartyId.allLatestId);
        	select.addItem(allLatestDocAndParty);
        	select.setItemCaption(allLatestDocAndParty,allLatest);
    	}
    	
    	for( DocumentIdPartyId docAndParty : allDocAndParty ) {
    		select.addItem(docAndParty);
    		String partyName = docAndParty.partyName == null ? partyLatest : docAndParty.partyName.toString();
    		select.setItemCaption(docAndParty, docAndParty.documentName.toString() + " (" + partyName + ")");
    	}
    	
    	select.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = 8140878323703405745L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				@SuppressWarnings("unchecked")
				java.util.Set<DocumentIdPartyId> selectedValues = (java.util.Set<DocumentIdPartyId>)select.getValue();
				if ( selectedValues != null ) {
					downloadContext.docAndPartyList = DocumentIdPartyId.fixupSelectedValues( selectedValues, allDocAndParty );
				}
				setupDownloadLink();
			}
    	});
    	layout.addComponent(select);

    	layout.addComponent(downloadDocumentSnapshotsAsPDFButton);	
	}
	
	void setupDownloadLink() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();
		
		LinkedList<DocumentIdPartyId> docPartyList = new LinkedList<DocumentIdPartyId>(downloadContext.docAndPartyList);
		List<TransactionPartyDocument> tranPartyDocumentList = transaction.getTransactionPartyDocumentsWithNonSkippedSnapshot(docPartyList);

		downloadDocumentSnapshotsAsPDFButton.setEnabled(tranPartyDocumentList.size() > 0);

		// Multiple files to be downloaded?
		if ( tranPartyDocumentList.size() > 1 ) {
			downloadContext.downloadFilename = transaction.getPackageDownloadFileName(vaadinUi.getUser(), null);
		}
		else if ( docPartyList.size() == 1 )
		{
			DocumentIdPartyId dp = docPartyList.getFirst();
			downloadContext.downloadFilename = dp.getDocumentName().toString();
		}
		
		boolean useLandscape = false;
		for( TransactionDocument tranDoc : reportTemplate == null ? transaction.getAllTransactionDocuments() : reportTemplate.getAllAllowedTransactionDocuments(transaction) ) {
			TransactionPartyDocument tpd = transaction.getLatestTransactionPartyDocumentWithSnapshot(tranDoc);
			if ( tpd != null && ! tpd.isSnapshotDocumentSkipped() ) {
				TransactionDocument td = transaction.getTransactionDocument(tpd.getTransactionDocumentId());
				if ( td.getDocumentVersion().isLandscape() )
					useLandscape = true;
			}
		}
		
		downloadContext.formatType = useLandscape ? TranSnapshotsDownload.FORMAT_PDF_LANDSCAPE : TranSnapshotsDownload.FORMAT_PDF_PORTRAIT;
	}
	
	public int getNumDocuments() {
		return numDocuments;
	}
	
	public String toString()
	{
		return Integer.toString(numDocuments);
	}
	
	@Override
	public void detach() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.removeSessionAttribute(tranSnapshotsDownloadContextPickupCode);
		vaadinUi.getEsfapp().debug("DEBUG-PICKUP TranSnapshotsAsPdfPopupButton.detach(): Remove download context pickup code: " + tranSnapshotsDownloadContextPickupCode + "; IP: " + vaadinUi.getRequestHostIp());
		super.detach();
	}
}