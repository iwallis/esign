// Copyright (C) 2014-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import java.util.LinkedList;
import java.util.List;

import org.vaadin.hene.popupbutton.PopupButton;

import com.esignforms.open.data.EsfString;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentIdPartyId;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.servlet.TranSnapshotsDownload;
import com.esignforms.open.util.ServletUtil;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.WebBrowser;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Link;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * This is a PopupButton object designed to allow for downloading transaction document snapshots selected by the user as a PDF or HTML.
 * It makes use of the related servlet TranSnapshotsDownload by passing a session-based ID to actually gather and download the data.
 *  
 * This is our attempt to rid ourselves of the never-ending headaches caused by the Vaadin FileDownloaders, including PDFs that never download at all, 
 * inability to use server push for downloading PDFs, and to allow a bit more flexibility for the user in a more universal download that with any luck
 * will be used instead of TranLatestSnapshotsAsPdfButton (all latest as PDF), TranSnapshotPopupButton (HTML viewer), and 
 * TranSnapshotsAsPdfPopupButton (selectable as PDF).
 * 
 * @author Yozons Inc.
 */
public class TranSnapshotsDownloadPopupButton extends PopupButton {
	private static final long serialVersionUID = -3465725175767977762L;

	private static final String PROP_FORMAT = "TranSnapshotsDownloadPopupButton/format/";

	protected int numDocuments = 0;
	protected final Transaction transaction;
	protected final ReportTemplate reportTemplate;
	protected final EsfVaadinUI vaadinUi;

	protected final String reportTemplateIdPropertySuffix;

	protected final TranSnapshotsDownloadPopupButton thisButton;
	protected NativeSelect formatSelect;
	protected ListSelect documentSelect;
	protected Link viewLink;
	protected Link downloadLink;

	protected final TranSnapshotsDownload.DownloadContext downloadContext;
	
	protected final String tranSnapshotsDownloadContextPickupCode;
	
	public TranSnapshotsDownloadPopupButton(Transaction forTran, ReportTemplate forReport) {
		this(forTran, forReport, EsfVaadinUI.getInstance().getMsg("TranSnapshotsDownloadPopupButton.label"));
	}
	
	public TranSnapshotsDownloadPopupButton(Transaction forTran, ReportTemplate forReport, String caption) {
		super();

		this.thisButton = this;
		this.vaadinUi = EsfVaadinUI.getInstance();
		this.transaction = forTran;
		this.reportTemplate = forReport;
		this.tranSnapshotsDownloadContextPickupCode = vaadinUi.getEsfapp().getRandomKey().getEsfReportsAccessPickupCodeString(); // not really for ESF_Reports_Access, but close enough!

		reportTemplateIdPropertySuffix = reportTemplate == null ? "TransactionDetailView" : reportTemplate.getId().toNormalizedEsfNameString();
		
		downloadContext = new TranSnapshotsDownload.DownloadContext();
		downloadContext.transactionId = forTran.getId();
		vaadinUi.setSessionAttribute(tranSnapshotsDownloadContextPickupCode,downloadContext);
		vaadinUi.getEsfapp().debug("DEBUG-PICKUP-9 TranSnapshotsDownloadPopupButton: Set download context pickup code: " + tranSnapshotsDownloadContextPickupCode + "; file: "+downloadContext.downloadFilename + "; IP: " + vaadinUi.getRequestHostIp() );
		
		setCaption(caption);
		setIcon(new ThemeResource(vaadinUi.getMsg("TranSnapshotsDownloadPopupButton.icon")));
		setStyleName(Reindeer.BUTTON_SMALL);

		VerticalLayout layout = new VerticalLayout();
		layout.setStyleName("TranSnapshotsDownloadPopupButtonLayout");
		layout.setSizeUndefined();
		layout.setMargin(false);
		layout.setSpacing(true);
		setContent(layout);
		
		this.addPopupVisibilityListener( new PopupVisibilityListener() {
			private static final long serialVersionUID = -1252026941723480440L;

			@Override
			public void popupVisibilityChange(PopupVisibilityEvent event) {
				if ( event.isPopupVisible() ) {
					if ( ((VerticalLayout)getContent()).getComponentCount() == 0 ) {
						buildPopupLayout();
					}
				}
			}
		});
		
		numDocuments = reportTemplate == null ? transaction.getNumTransactionDocumentsWithDocumentSnapshot() : reportTemplate.getNumAllowedTransactionDocumentsWithDocumentSnapshot(transaction);
	}
	
	protected void buildPopupLayout() {
		VerticalLayout layout = (VerticalLayout)getContent();
		
    	String partyLatest = vaadinUi.getMsg("TranSnapshotsDownloadPopupButton.select.latestParty");
    	String allLatest = vaadinUi.getMsg("TranSnapshotsDownloadPopupButton.select.allLatestParty");
    	
    	String servletUrl = vaadinUi.getEsfapp().getExternalContextPath() + "/" + "transnapshotsdownload?" + TranSnapshotsDownload.PARAM_PICKUP_CODE + "=" + tranSnapshotsDownloadContextPickupCode;
    	
    	String viewUrl = ServletUtil.appendUrlParam(servletUrl, TranSnapshotsDownload.PARAM_MODE, TranSnapshotsDownload.MODE_VIEW);
    	viewLink = new Link(vaadinUi.getMsg("TranSnapshotsDownloadPopupButton.link.view.label"), new ExternalResource(viewUrl));
    	viewLink.setTargetName("_blank");
    	viewLink.setEnabled(false);

    	String downloadUrl = ServletUtil.appendUrlParam(servletUrl, TranSnapshotsDownload.PARAM_MODE, TranSnapshotsDownload.MODE_DOWNLOAD);
    	downloadLink = new Link(vaadinUi.getMsg("TranSnapshotsDownloadPopupButton.link.download.label"), new ExternalResource(downloadUrl));
    	downloadLink.setTargetName("_blank");
    	downloadLink.setEnabled(false);
    	
		formatSelect = new NativeSelect(vaadinUi.getMsg("TranSnapshotsDownloadPopupButton.formatType.label"));
		formatSelect.setRequired(true);
		formatSelect.setNullSelectionAllowed(false);
		formatSelect.setImmediate(true);
		formatSelect.addItem(TranSnapshotsDownload.FORMAT_HTML);
		formatSelect.setItemCaption(TranSnapshotsDownload.FORMAT_HTML, vaadinUi.getPrettyCode().tranSnapshotsDownloadFormat(TranSnapshotsDownload.FORMAT_HTML));
		formatSelect.addItem(TranSnapshotsDownload.FORMAT_HTML_ZIP);
		formatSelect.setItemCaption(TranSnapshotsDownload.FORMAT_HTML_ZIP, vaadinUi.getPrettyCode().tranSnapshotsDownloadFormat(TranSnapshotsDownload.FORMAT_HTML_ZIP));
		formatSelect.addItem(TranSnapshotsDownload.FORMAT_PDF_PORTRAIT);
		formatSelect.setItemCaption(TranSnapshotsDownload.FORMAT_PDF_PORTRAIT, vaadinUi.getPrettyCode().tranSnapshotsDownloadFormat(TranSnapshotsDownload.FORMAT_PDF_PORTRAIT));
		formatSelect.addItem(TranSnapshotsDownload.FORMAT_PDF_LANDSCAPE);
		formatSelect.setItemCaption(TranSnapshotsDownload.FORMAT_PDF_LANDSCAPE, vaadinUi.getPrettyCode().tranSnapshotsDownloadFormat(TranSnapshotsDownload.FORMAT_PDF_LANDSCAPE));
		formatSelect.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = 1140417949433917411L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				String formatType = (String)event.getProperty().getValue();
				vaadinUi.getUser().setUserDefinedStringProperty(PROP_FORMAT+reportTemplateIdPropertySuffix, new EsfString(formatType));
				downloadContext.formatType = formatType;
				setupViewDownloadLinks();
			}
		});
		layout.addComponent(formatSelect);
		
    	final List<DocumentIdPartyId> allDocAndParty = new LinkedList<DocumentIdPartyId>();
    	for( final TransactionDocument tranDoc : reportTemplate == null ? transaction.getAllTransactionDocuments() : reportTemplate.getAllAllowedTransactionDocuments(transaction) ) {
    		
    		boolean foundFirstPartySnapshot = false;
    		
        	for( TransactionParty tranParty : transaction.getAllTransactionParties() ) {
        		TransactionPartyDocument foundTranPartyDoc = null;
        		List<TransactionPartyDocument> transactionPartyDocuments = tranParty.getNonSkippedTransactionPartyDocuments();
            	for( TransactionPartyDocument tpd : transactionPartyDocuments ) {
            		if ( tpd.getTransactionDocumentId().equals(tranDoc.getId()) ) {
            			foundTranPartyDoc = tpd;
            			break;
            		}
            	}
            	
            	if ( foundTranPartyDoc == null ) {
            		continue;
            	}
	    		if ( foundTranPartyDoc.hasSnapshotXmlOrBlobId() ) {
					Document document = tranDoc.getDocument();
					if ( ! foundFirstPartySnapshot ) {
						foundFirstPartySnapshot = true;
						DocumentIdPartyId dp = new DocumentIdPartyId(document.getId(),null);  // Add in for this document, latest
						dp.documentName = document.getEsfName();
						allDocAndParty.add(dp);
					}
					DocumentIdPartyId dp = new DocumentIdPartyId(document.getId(),document.getEsfName(),
									tranParty.getPackageVersionPartyTemplateId(),tranParty.getPackageVersionPartyTemplate().getEsfName()); // Add in for this document and party
					allDocAndParty.add(dp); 			
	    		}
        	}
    	}

    	HorizontalLayout buttonLayout = new HorizontalLayout();
    	buttonLayout.setMargin(false);
    	buttonLayout.setSpacing(true);
    	
    	buttonLayout.addComponent(viewLink);
    	buttonLayout.addComponent(downloadLink);
    	
    	Button cancelButton = new Button(vaadinUi.getMsg("TranSnapshotsDownloadPopupButton.button.cancel.label"));
    	cancelButton.setStyleName(Reindeer.BUTTON_LINK);
    	cancelButton.setDisableOnClick(true);
    	cancelButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = 2022551699974784272L;

			@Override
			public void buttonClick(ClickEvent event) {
				thisButton.setPopupVisible(false);
				event.getButton().setEnabled(true);
			}
    	});
    	buttonLayout.addComponent(cancelButton);
    	buttonLayout.setComponentAlignment(cancelButton, Alignment.BOTTOM_RIGHT);
    	
    	documentSelect = new ListSelectValid(vaadinUi.getMsg("TranSnapshotsAsPdfPopupButton.documentIdSet.label"));
    	documentSelect.setNullSelectionAllowed(true);
    	documentSelect.setRequired(false);
    	documentSelect.setImmediate(true);
    	documentSelect.setMultiSelect(true);
    	documentSelect.setRows( Math.min(10, allDocAndParty.size()+1)); // +1 for the allLatestDocAndParty we add in addition
    	documentSelect.addValidator(new SelectValidator(documentSelect));
    	documentSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	
    	if ( allDocAndParty.size() > 0 ) {
        	DocumentIdPartyId allLatestDocAndParty = new DocumentIdPartyId(DocumentIdPartyId.allLatestId,DocumentIdPartyId.allLatestId);
        	documentSelect.addItem(allLatestDocAndParty);
        	documentSelect.setItemCaption(allLatestDocAndParty,allLatest);
    	}
    	
    	for( DocumentIdPartyId docAndParty : allDocAndParty ) {
    		documentSelect.addItem(docAndParty);
    		String partyName = docAndParty.partyName == null ? partyLatest : docAndParty.partyName.toString();
    		documentSelect.setItemCaption(docAndParty, docAndParty.documentName.toString() + " (" + partyName + ")");
    	}
    	
    	documentSelect.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = 5431317531204468141L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				@SuppressWarnings("unchecked")
				java.util.Set<DocumentIdPartyId> selectedValues = (java.util.Set<DocumentIdPartyId>)documentSelect.getValue();
				if ( selectedValues != null ) {
					downloadContext.docAndPartyList = DocumentIdPartyId.fixupSelectedValues( selectedValues, allDocAndParty );
				}
				setupViewDownloadLinks();
			}
    	});
    	layout.addComponent(documentSelect);
    	
    	layout.addComponent(buttonLayout);
    	
    	// Set the format to use based on the last time it was used on this report
		EsfString propFormat = vaadinUi.getUser().getUserDefinedStringProperty(PROP_FORMAT+reportTemplateIdPropertySuffix);
		String initialFormat = EsfString.isBlank(propFormat) ? TranSnapshotsDownload.FORMAT_HTML : propFormat.toString();
		formatSelect.setValue(initialFormat);
	}
	
	void setupViewDownloadLinks() {
		vaadinUi.ensureLoggedIn();

		LinkedList<DocumentIdPartyId> docPartyList = new LinkedList<DocumentIdPartyId>(downloadContext.docAndPartyList);
		List<TransactionPartyDocument> tranPartyDocumentList = transaction.getTransactionPartyDocumentsWithNonSkippedSnapshot(docPartyList);

		viewLink.setEnabled(tranPartyDocumentList.size() > 0);
		downloadLink.setEnabled(tranPartyDocumentList.size() > 0);

		// Multiple files to be view/downloaded?
		if ( tranPartyDocumentList.size() > 1 ) {
			if ( downloadContext.isHTML() )
				formatSelect.setValue(TranSnapshotsDownload.FORMAT_HTML_ZIP);
			downloadContext.downloadFilename = transaction.getPackageDownloadFileName(vaadinUi.getUser(), null);
		}
		else if ( docPartyList.size() == 1 )
		{
			DocumentIdPartyId dp = docPartyList.getFirst();
			downloadContext.downloadFilename = dp.getDocumentName().toString();
		}

		if ( downloadContext.isHTMLZip() )
			viewLink.setEnabled(false); // ZIP files cannot be viewed
		
		WebBrowser wb = Page.getCurrent().getWebBrowser();
		String downloadTarget = wb.isChrome() || wb.isSafari() ? "_top" : "_blank";
		
		viewLink.setTargetName("_blank");
		downloadLink.setTargetName(downloadTarget);
	}
	
	public int getNumDocuments() {
		return numDocuments;
	}
	
	public String toString()
	{
		return Integer.toString(numDocuments);
	}
	
	@Override
	public void detach() {
		vaadinUi.removeSessionAttribute(tranSnapshotsDownloadContextPickupCode);
		vaadinUi.getEsfapp().debug("DEBUG-PICKUP TranSnapshotsDownloadPopupButton.detach(): Remove download context pickup code: " + tranSnapshotsDownloadContextPickupCode + "; IP: " + vaadinUi.getRequestHostIp());
		super.detach();
	}
}