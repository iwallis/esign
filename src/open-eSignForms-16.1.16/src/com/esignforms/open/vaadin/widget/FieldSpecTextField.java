// Copyright (C) 2014-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import com.vaadin.data.Buffered;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.TextField;


/**
 * The FieldSpecTextField is a combination of a TextField and a button in a horizontal layout that when clicked allows for a field spec to be
 * looked up and when selected, have the field spec appended to the text field's value.
 * @author Yozons Inc.
 */
public class FieldSpecTextField extends CustomField<String> {

	private static final long serialVersionUID = -5026825274671011031L;
	CssLayout layout;
	TextField textField;
	FieldSpecPopupView fieldSpecPopupView;
	
	public FieldSpecTextField() {
		setStyleName("FieldSpecTextField");
		
		layout = new CssLayout();
		layout.setWidth(100, Unit.PERCENTAGE);
		textField = new TextField();
		textField.addStyleName("TextField");
		textField.setWidth(100, Unit.PERCENTAGE);
		layout.addComponent(textField);
		fieldSpecPopupView = new FieldSpecPopupView( textField );
		layout.addComponent(fieldSpecPopupView); 
	}
	
	public FieldSpecTextField(String caption) {
		this();
		setCaption(caption);
	}
	
	public int getMaxLength() {
		return textField.getMaxLength();
	}
	public void setMaxLength(int maxLength) {
		textField.setMaxLength(maxLength);
	}
	
	@Override
	public String getCaption() {
		return textField.getCaption();
	}
	@Override
	public void setCaption(String v) {
		textField.setCaption(v);
	}
	
	@Override
	public String getDescription() {
		return textField.getDescription();
	}
	@Override
	public void setDescription(String v) {
		textField.setDescription(v);
	}
	
	public String getInputPrompt() {
		return textField.getInputPrompt();
	}
	public void setInputPrompt(String inputPrompt) {
		textField.setInputPrompt(inputPrompt);
	}
	
	public String getNullRepresentation() {
        return textField.getNullRepresentation();
    }
	public void setNullRepresentation(String nullRepresentation) {
		textField.setNullRepresentation(nullRepresentation);
	}

	public boolean isNullSettingAllowed() {
		return textField.isNullSettingAllowed();
	}
	public void setNullSettingAllowed(boolean v) {
		textField.setNullSettingAllowed(v);
	}

	@Override
	public boolean isRequired() {
		return textField.isRequired();
	}
	@Override
	public void setRequired(boolean v) {
		textField.setRequired(v);
	}
	
	@Override
	public String getRequiredError() {
		return textField.getRequiredError();
	}
	@Override
	public void setRequiredError(String v) {
		textField.setRequiredError(v);
	}
	
	@Override
	public boolean isImmediate() {
		return textField.isImmediate();
	}
	@Override
	public void setImmediate(boolean v) {
		textField.setImmediate(v);
	}
	
	@Override
    protected void setInternalValue(String v) {
        textField.setValue(v);
	}
	
	@Override
	public String getValue() {
		return textField.getValue();
	}
	@Override
	public void setValue(String v) {
		textField.setValue(v);
	}
	
	@Override
	public boolean isValid() {
		return textField.isValid();
	}
	
	@Override
    public void validate() throws Validator.InvalidValueException {
		textField.validate();
	}
	
	@Override 
	public void discard() throws Buffered.SourceException {
		textField.discard();
	}
	
	@Override 
	public void commit() throws Buffered.SourceException, InvalidValueException {
		textField.commit();
	}
	
	@Override
	protected Component initContent() {
		return layout;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
    public Property getPropertyDataSource() {
        return textField.getPropertyDataSource();
    }
	
	@SuppressWarnings("rawtypes")
	@Override
    public void setPropertyDataSource(com.vaadin.data.Property newDataSource) {
		textField.setPropertyDataSource(newDataSource);
	}
	
	@Override
    public boolean isInvalidCommitted() {
        return textField.isInvalidCommitted();
    }
	
	@Override
    public void setInvalidCommitted(boolean isCommitted) {
        textField.setInvalidCommitted(isCommitted);
    }
	
	@Override
    public boolean isModified() {
        return textField.isModified();
    }
	
	@Override
    public boolean isBuffered() {
        return textField.isBuffered();
    }
	@Override
    public void setBuffered(boolean buffered) {
		textField.setBuffered(buffered);
	}
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	textField.setReadOnly(readOnly);
    	fieldSpecPopupView.setReadOnly(readOnly);
    }

	@Override
	public Class<? extends String> getType() {
		return String.class;
	}
	
}