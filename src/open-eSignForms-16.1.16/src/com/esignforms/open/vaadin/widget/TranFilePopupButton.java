// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import org.vaadin.hene.popupbutton.PopupButton;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionFile;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.WebBrowser;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * This is a PopupButton object designed to allow for downloading transaction files given to it.
 * 
 * @author Yozons Inc.
 */
public class TranFilePopupButton extends PopupButton {
	private static final long serialVersionUID = -5727232064212344344L;

	int numFiles = 0;
	protected final TranFilePopupButton thisButton;
	
	public TranFilePopupButton(Transaction transaction, List<TransactionFile> tranFileList) {
		super();
		thisButton = this;
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		setCaption(vaadinUi.getMsg("TranFilePopupButton.popup.label"));
		setIcon(new ThemeResource(vaadinUi.getMsg("TranFilePopupButton.popup.icon")));
		setStyleName(Reindeer.BUTTON_SMALL);

		VerticalLayout buttonLayout = new VerticalLayout();
		buttonLayout.setSizeUndefined();
		buttonLayout.setMargin(false);
		buttonLayout.setSpacing(true);
		setContent(buttonLayout);
		
		for( final TransactionFile tranFile : tranFileList ) {
			TransactionDocument tranDoc = transaction.getTransactionDocument(tranFile.getTransactionDocumentId());
			
			String downloadCaption = vaadinUi.getMsg("TranFilePopupButton.fileDownload.button.label",tranDoc.getDocument().getEsfName(),tranFile.getFieldName(),tranFile.getFileName(),EsfInteger.byteSizeInUnits(tranFile.getFileSize()));
			
			Button downloadTranFileButton = new Button(downloadCaption, new ClickListener() {
				private static final long serialVersionUID = -2583638019386906626L;

				@Override
				public void buttonClick(ClickEvent event) {
					try {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						vaadinUi.ensureLoggedIn();
		            	final byte[] downloadFileData = tranFile.getFileDataFromDatabase();
		            	if ( downloadFileData == null || downloadFileData.length < 1 ) {
		            		vaadinUi.showStatus(vaadinUi.getMsg("TranFilePopupButton.nodata",tranFile.getFileName()));
		            		return;
		            	}
		            	
						StreamResource.StreamSource ss = new StreamResource.StreamSource() {
							private static final long serialVersionUID = -6448361999669609487L;

							InputStream is = new BufferedInputStream(new ByteArrayInputStream(downloadFileData));
							@Override
							public InputStream getStream() {
								return is;
							}
						};
						
						WebBrowser wb = Page.getCurrent().getWebBrowser();
						String target;
						boolean asPopup;
						
						StreamResource sr = new StreamResource(ss, tranFile.getFileName());
						
						if ( vaadinUi.getEsfapp().isContentTypeBrowserSafeOrPDF(tranFile.getFileMimeType()) ) {
							sr.setMIMEType(tranFile.getFileMimeType());
							sr.getStream().setParameter("Content-Disposition", "inline; filename=\"" + tranFile.getFileName() + "\"");
							target = "_blank";
							asPopup = true;
						} else {
							sr.setMIMEType(Application.CONTENT_TYPE_BINARY);
							sr.getStream().setParameter("Content-Disposition", "attachment; filename=\"" + tranFile.getFileName() + "\"");
							target = wb.isChrome() || wb.isSafari() ? "_top" : "_blank";
							asPopup = false;
						}
						
						sr.setCacheTime(0); // Disable cache
						sr.getStream().setParameter("Content-Length", Integer.toString(downloadFileData.length));
						vaadinUi.getEsfapp().debug("TranFilePopupButton.buttonClick() - tranFileName: " + tranFile.getFileName() + "; File length: " + downloadFileData.length);
						Page.getCurrent().open(sr, target, asPopup);
					} finally {
						event.getButton().setEnabled(true);
						thisButton.setPopupVisible(false);
					}
				}
			});
			downloadTranFileButton.setDisableOnClick(true);
			downloadTranFileButton.setStyleName(Reindeer.BUTTON_LINK);
			buttonLayout.addComponent(downloadTranFileButton);	
			++numFiles;
		}
	}
	
	public String toString()
	{
		return Integer.toString(numFiles);
	}
}