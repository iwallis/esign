// Copyright (C) 2010-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.DropDownVersionOption;
import com.esignforms.open.prog.Library;
import com.vaadin.ui.NativeSelect;

/**
 * This is a simple extension of NativeSelect that allows us to configure them from our DropDown configurations in the library.
 * By default, it looks for Test mode DropDown from the Template Library first, and if not found, it will also then look for a Production version.
 * It is NOT able to support multiple selections because NativeSelect doesn't.
 * 
 * @author Yozons Inc.
 */
public class NativeSelectDropDown extends NativeSelect {
	private static final long serialVersionUID = -5824997253277905021L;

	private int numOptions = 0;
	
	public NativeSelectDropDown(String caption, EsfName dropDownName, EsfName... extraDropDownNames) {
		this(caption,dropDownName,false,extraDropDownNames);
    }
    
	public NativeSelectDropDown(String caption, EsfName dropDownName, boolean valueIsEsfString,EsfName... extraDropDownNames) {
		super(caption);
		setNullSelectionAllowed(false);
        DropDown dd = DropDown.Manager.getByName(Library.Manager.getTemplate().getId(), dropDownName);
        if ( dd != null && dd.isEnabled() ) {
        	DropDownVersion ddv = dd.getTestDropDownVersion();
        	if ( ddv == null )
            	ddv = dd.getProductionDropDownVersion();
        	if ( ddv == null )
        	{
        		Application.getInstance().err("NativeSelectDropDown called with dropDownName: " + dropDownName + "; but no test or production version found.");
        		setEnabled(false);
        		return;
        	}
        	if ( ddv.isAllowMultiSelection() ) {
        		Application.getInstance().err("NativeSelectDropDown called with dropDownName: " + dropDownName + "; allows multi-selection but NativeSelect does not allow it...ignoring...");
            	//setMultiSelect(ddv.isAllowMultiSelection());
        	}
        	List<DropDownVersionOption> optionList = ddv.getOptions();
        	for( DropDownVersionOption ddvo : optionList ) {
        		Object value = valueIsEsfString ? new EsfString(ddvo.getValue()) : ddvo.getValue();
        		addItem(value);
        		setItemCaption(value, ddvo.getOption());
        	}
        	
        	// For building a select list from multiple drop down definitions.
        	if ( extraDropDownNames != null ) {
        		for( EsfName ddName : extraDropDownNames ) {
        	        dd = DropDown.Manager.getByName(Library.Manager.getTemplate().getId(), ddName);
        	        if ( dd != null && dd.isEnabled() ) {
        	        	ddv = dd.getTestDropDownVersion();
        	        	if ( ddv == null )
        	            	ddv = dd.getProductionDropDownVersion();
        	        	if ( ddv == null )
        	        	{
        	        		Application.getInstance().err("NativeSelectDropDown called for dropDownName: " + dropDownName + "; extraDropDownName: " + ddName + "; but no test or production version found.");
        	        		setEnabled(false);
        	        		return;
        	        	}
        	        	if ( ddv.isAllowMultiSelection() ) {
        	        		Application.getInstance().err("NativeSelectDropDown called for dropDownName: " + dropDownName + "; extraDropDownName: " + ddName + "; allows multi-selection but NativeSelect does not allow it...ignoring...");
        	        	}
        	        	optionList = ddv.getOptions();
        	        	for( DropDownVersionOption ddvo : optionList ) {
        	        		Object value = valueIsEsfString ? new EsfString(ddvo.getValue()) : ddvo.getValue();
        	        		addItem(value);
        	        		setItemCaption(value, ddvo.getOption());
        	        	}
        	        }
        		}
        	}
        	
        	numOptions = optionList.size();
        } else {
    		setNullSelectionAllowed(true);
        }
    }
    
	public int getNumOptions() {
		return numOptions;
	}

}