// Copyright (C) 2014-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.vaadin.hene.popupbutton.PopupButton;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * This is a PopupButton object designed to allow for downloading a transaction document snapshot selected by the user as native HTML.
 * 
 * @author Yozons Inc.
 */
public class TranSnapshotPopupButton extends PopupButton {
	private static final long serialVersionUID = 4117472848388056027L;

	protected int numDocuments = 0;
	protected final Transaction transaction;
	protected final ReportTemplate reportTemplate;
	protected final TranSnapshotPopupButton thisButton;
	
	// Simple class to hold values we want for our selection list
	class TPD {
		TransactionPartyDocument tpd;
		String documentName;
		String partyName;
	}

	
	public TranSnapshotPopupButton(Transaction forTran, ReportTemplate forReport) {
		this(forTran, forReport, EsfVaadinUI.getInstance().getMsg("TranSnapshotPopupButton.label"));
	}
	
	public TranSnapshotPopupButton(Transaction forTran, ReportTemplate forReport, String caption) {
		super();

		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		this.thisButton = this;
		this.transaction = forTran;
		this.reportTemplate = forReport;

		setCaption(caption);
		setIcon(new ThemeResource(vaadinUi.getMsg("TranSnapshotPopupButton.icon")));
		addStyleName(Reindeer.BUTTON_SMALL);

		VerticalLayout buttonLayout = new VerticalLayout();
		buttonLayout.setSizeUndefined();
		buttonLayout.setMargin(false);
		buttonLayout.setSpacing(false);
		setContent(buttonLayout);
		
		addPopupVisibilityListener( new PopupVisibilityListener() {
			private static final long serialVersionUID = -8804648299304876356L;

			@Override
			public void popupVisibilityChange(PopupVisibilityEvent event) {
				//EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				VerticalLayout buttonLayout = (VerticalLayout)getContent();
				int componentCount = buttonLayout.getComponentCount();
				//vaadinUi.getEsfapp().debug("XXX TranSnapshotPopupButton.popupVisibilityChange() - Popup visible event: " + 
				//		event.isPopupVisible() + "; componentCount: " + componentCount);
				if ( event.isPopupVisible() ) {
					if ( componentCount == 0 ) {
						buildButtonLayout();
					}
				} else {
					// Re-enable any buttons we may have in our layout as they are disabled on click.
					Iterator<Component> iter = buttonLayout.iterator();
					while( iter.hasNext() ) {
						Component c = iter.next();
						if ( c instanceof Button ) {
							Button b = (Button)c;
							if ( ! b.isEnabled() ) {
								b.setEnabled(true);
							}
						}
					}
				}
			}
		});
		
		numDocuments = reportTemplate == null ? transaction.getNumTransactionDocumentsWithDocumentSnapshot() : reportTemplate.getNumAllowedTransactionDocumentsWithDocumentSnapshot(transaction);
	}
	
	protected void buildButtonLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		//vaadinUi.getEsfapp().debug("XXX TranSnapshotPopupButton.buildButtonLayout() BEGIN");
		
		VerticalLayout buttonLayout = (VerticalLayout)getContent();
		
		buttonLayout.setSizeUndefined();
		buttonLayout.setMargin(false);
		buttonLayout.setSpacing(true);
		buttonLayout.setStyleName("TranSnapshotPopupButtonLayout");
		
    	String partyLatest = vaadinUi.getMsg("TranSnapshotPopupButton.latestParty");
    	
    	final List<TPD> allTPD = new LinkedList<TPD>();
    	final List<String> allDocumentNames = new LinkedList<String>();
    	
    	for( final TransactionDocument tranDoc : reportTemplate == null ? transaction.getAllTransactionDocuments() : reportTemplate.getAllAllowedTransactionDocuments(transaction) ) {
    		
    		boolean foundFirstPartySnapshot = false;
    		TPD latestTPD = new TPD();
    		
        	for( TransactionParty tranParty : transaction.getAllTransactionParties() ) {
        		TransactionPartyDocument foundTranPartyDoc = null;
        		List<TransactionPartyDocument> transactionPartyDocuments = tranParty.getNonSkippedTransactionPartyDocuments();
            	for( TransactionPartyDocument tranPartyDoc : transactionPartyDocuments ) {
            		if ( tranPartyDoc.getTransactionDocumentId().equals(tranDoc.getId()) ) {
            			foundTranPartyDoc = tranPartyDoc;
            			break;
            		}
            	}
            	
            	if ( foundTranPartyDoc != null ) {
		    		if ( ! foundTranPartyDoc.isSnapshotDocumentSkipped() && foundTranPartyDoc.hasSnapshotXmlOrBlobId() ) {
						Document document = tranDoc.getDocument();
						if ( ! foundFirstPartySnapshot ) {
							foundFirstPartySnapshot = true;
							latestTPD.tpd = foundTranPartyDoc;
							latestTPD.documentName = document.getEsfName().toString();
							latestTPD.partyName = partyLatest;
							allTPD.add(latestTPD);
							allDocumentNames.add(document.getEsfName() + " (" + partyLatest + ")");
						}
						
						// If this tran party document is more current, we'll set our latest to this version
						if ( latestTPD.tpd.getLastUpdatedTimestamp().isBefore(foundTranPartyDoc.getLastUpdatedTimestamp()) ) {
							latestTPD.tpd = foundTranPartyDoc;
							latestTPD.documentName = document.getEsfName().toString();				    		
						}						
						
			    		TPD tpd = new TPD();
		    			tpd.tpd = foundTranPartyDoc;
		    			tpd.documentName = document.getEsfName().toString();				    		
						tpd.partyName = tranParty.getPackageVersionPartyTemplate().getEsfName().toString();
						allTPD.add(tpd);
						allDocumentNames.add(document.getEsfName() + " (" + tpd.partyName + ")");
		    		}
            	}
        	}
    	}

    	ListIterator<TPD> allTPDIter = allTPD.listIterator();
    	ListIterator<String> allDocumentNamesIter = allDocumentNames.listIterator();
    	while( allTPDIter.hasNext() && allDocumentNamesIter.hasNext() ) {
    		final TPD tpd = allTPDIter.next();
    		String label = allDocumentNamesIter.next();

    		final Button downloadDocumentSubButton = new Button(label);
    		downloadDocumentSubButton.setStyleName(Reindeer.BUTTON_LINK);
    		downloadDocumentSubButton.setDisableOnClick(true);
    		
			String htmlFileName;
			if ( tpd.tpd.hasDocumentFileName() )
				htmlFileName = tpd.tpd.getDocumentFileName();
			else {
				htmlFileName = vaadinUi.getMsg("TranSnapshotPopupButton.document.filename",tpd.documentName,tpd.partyName);
			}

    		OnDemandBrowserWindowOpener opener = new OnDemandBrowserWindowOpener(htmlFileName, new OnDemandBrowserWindowOpener.OnDemandStreamSource()  {
				private static final long serialVersionUID = -5972767156481962377L;

				@Override
				public InputStream getStream() {
					try {
						EsfVaadinUI.getInstance().ensureLoggedIn();

						//EsfVaadinUI.getInstance().getEsfapp().debug("XXX TranSnapshotPopupButton.getStream() calling setPopupVisible(false)");
						// Do this now to help ensure it goes back with the other UIDL requests.
						thisButton.setPopupVisible(false);

						String html = tpd.tpd.getSnapshotDocument();
		            	byte[] downloadHtmlData = EsfString.stringToBytes(html);
		            	html = null;
		            	EsfVaadinUI.getInstance().getEsfapp().debug("TranSnapshotPopupButton.OnDemandBrowserWindowOpener.getStream() - tranId: " + transaction.getId() + "; HTML length: " + downloadHtmlData.length);
		            	return new BufferedInputStream(new ByteArrayInputStream(downloadHtmlData));
					} finally {
					}
				}
    		});
			opener.setContentType(Application.CONTENT_TYPE_HTML+Application.CONTENT_TYPE_CHARSET_UTF_8);
			opener.setCacheTime(0); // Disable cache
			opener.setFeatures("location=0,scrollbars=1,resizable=1");
    		opener.extend(downloadDocumentSubButton);
    		buttonLayout.addComponent(downloadDocumentSubButton);	
    	}

    	// If we have lots of docs, make it fixed in size with scrollbars rather than trying to find room to put the entire thing.
    	if ( allTPD.size() > 15 ) {
    		buttonLayout.setHeight(200, Unit.PIXELS);
    	} else if ( allTPD.size() == 0 ) {
    		Label noneCompletedYet = new Label(vaadinUi.getMsg("TranSnapshotPopupButton.noDocuments"));
    		buttonLayout.addComponent(noneCompletedYet);
    	}
		//vaadinUi.getEsfapp().debug("XXX TranSnapshotPopupButton.buildButtonLayout() END");
	}
	
	public int getNumDocuments() {
		return numDocuments;
	}
	
	public String toString()
	{
		return Integer.toString(numDocuments);
	}
}