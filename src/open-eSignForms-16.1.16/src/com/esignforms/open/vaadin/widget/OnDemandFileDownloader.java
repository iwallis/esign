// Copyright (C) 2013-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import java.io.IOException;
import java.util.HashMap;

import com.esignforms.open.data.EsfString;
import com.vaadin.server.ConnectorResource;
import com.vaadin.server.DownloadStream;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.Resource;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import com.vaadin.ui.Button;


/**
 * OnDemandFileDownloader is based on the code at: https://vaadin.com/wiki/-/wiki/Main/Letting%20the%20user%20download%20a%20file
 * 
 * Extended though to give us access to the DownloadStream so we can set parameters if necessary, as well as convert
 * the default "attachment" download into an "inline" download if desired, and we reduce the default cache time unless set.
 * 
 * @author Yozons Inc.
 */
public class OnDemandFileDownloader extends FileDownloader {
	private static final long serialVersionUID = 4370210829891490428L;

	/**
	 * Provide both the {@link StreamSource} and the filename in an on-demand way.
	 */
	public interface OnDemandStreamSource extends StreamSource {
		public String getFilename();
		public int getContentLength();
	}
	
	public static class OnDemandStreamResource extends StreamResource {
		private static final long serialVersionUID = -6990689634424583465L;

		protected HashMap<String,String> parameterMap;
		protected boolean downloadInline = false;
		
		public OnDemandStreamResource(OnDemandStreamSource streamSource) {
			super(streamSource,"dummyFileName");
		}
		
		public DownloadStream getStream() {
			DownloadStream ds = super.getStream();
			
			if ( parameterMap != null ) {
				for( String key : parameterMap.keySet() ) {
					ds.setParameter(key, parameterMap.get(key));
				}
			}
			
			if ( downloadInline ) {
                ds.setParameter("Content-Disposition", "inline; filename=\"" + ds.getFileName() + "\"");
			}
			
			// If the cache time is still the default, let's reduce for our dynamic content that shouldn't be cached for a day.
			if ( ds.getCacheTime() == DownloadStream.DEFAULT_CACHETIME ) {
				ds.setCacheTime(0); // disable cache
			}
			
			return ds;
		}
		
		public void setDownloadInline(boolean v) {
			downloadInline = v;
		}

		public void setParameterMap(HashMap<String,String> parameterMap) {
			this.parameterMap = parameterMap;
		}
	}

	protected final OnDemandStreamSource onDemandStreamSource;
	protected boolean downloadInline = false;
	protected HashMap<String,String> parameterMap;
	protected Button buttonToEnableWhenDone;
	protected String contentType;

	public OnDemandFileDownloader(OnDemandStreamSource onDemandStreamSource) {
		super( new OnDemandStreamResource(onDemandStreamSource) );
		this.onDemandStreamSource = onDemandStreamSource;
	}

	public OnDemandFileDownloader(Button buttonToEnableWhenDone, OnDemandStreamSource onDemandStreamSource) {
		this(onDemandStreamSource);
		this.buttonToEnableWhenDone = buttonToEnableWhenDone;
	}

	@Override
	public boolean handleConnectorRequest(VaadinRequest request, VaadinResponse response, String path) throws IOException {
		if ( ! path.matches("dl(/.*)?") ) {
			//System.err.println((new com.esignforms.open.data.EsfDateTime()).toDateTimeMsecString() + " *DEBUG* OnDemandFileDownloader.handleConnectorRequest() - ignoring request for path: " + path);
			// Ignore if it isn't for us
			return false;
		}

		try {
			OnDemandStreamResource sr = getResource();
			sr.setFilename( onDemandStreamSource.getFilename() );
			sr.setDownloadInline(downloadInline);
			if ( contentType != null ) {
				sr.setMIMEType(contentType);
			}
			if ( parameterMap != null ) {
				sr.setParameterMap(parameterMap);
			}
			
			// Code snippet from com.vaadin.server.FileDownloader.handleConnectorRequest() with our content-length code
			// added inside.
	        VaadinSession session = getSession();

	        session.lock();
	        DownloadStream stream;

	        try {
	            Resource resource = getFileDownloadResource();
	            if (!(resource instanceof ConnectorResource)) {
	                return false;
	            }
	            stream = ((ConnectorResource) resource).getStream();

				int contentLength = onDemandStreamSource.getContentLength();
				if ( contentLength > 0 ) {
					stream.setParameter("Content-Length", Integer.toString(contentLength));
				}
	            
	            if (stream.getParameter("Content-Disposition") == null) {
	                // Content-Disposition: attachment generally forces download
	                stream.setParameter("Content-Disposition",
	                        "attachment; filename=\"" + stream.getFileName() + "\"");
	            }

	            // Content-Type to block eager browser plug-ins from hijacking
	            // the file
	            if (isOverrideContentType()) {
	                stream.setContentType("application/octet-stream;charset=UTF-8");
	            }
	        } finally {
	            session.unlock();
	        }
	        stream.writeResponse(request, response);
	        return true;	
	        // Moved the superclass code above so we can set the content length after we get the stream
			//return super.handleConnectorRequest(request, response, path);
		} finally {
			if ( buttonToEnableWhenDone != null ) {
				buttonToEnableWhenDone.setEnabled(true);
			}
		}
	}

	public OnDemandStreamResource getResource() {
		return (OnDemandStreamResource)this.getResource("dl");
	}
	
	public void setDownloadInline(boolean v) {
		downloadInline = v;
	}
	
	public void setContentType(String v) {
		contentType = v;
		setOverrideContentType(EsfString.isBlank(contentType)); // if blank, set override to true; if set to a value, set override to false
	}
	
	public void setStreamSourceParameter(String name, String value) {
		if ( parameterMap == null )
			parameterMap = new HashMap<String,String>();
		parameterMap.put(name, value);
	}
}