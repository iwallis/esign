// Copyright (C) 2013-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentIdPartyId;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.servlet.TranSnapshotsDownload;
import com.esignforms.open.util.ServletUtil;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.WebBrowser;
import com.vaadin.ui.Link;
import com.vaadin.ui.themes.Reindeer;

/**
 * This is a Link object is designed to allow for downloading all transaction document snapshots (latest versions) given to it.
 * 
 * In version 14.12.6 we changed from a Button to a Link and use our servlet to serve the PDF downloads since Vaadin's button-based FileDownloader
 * just hasn't been working reliably and definitely fails with push enabled.
 * 
 * @author Yozons Inc.
 */
public class TranLatestSnapshotsAsPdfButton extends Link {
	private static final long serialVersionUID = 1503425306268023393L;

	protected int numDocuments = 0;
	final Transaction transaction;
	
	protected final String tranSnapshotsDownloadContextPickupCode;
	
	public TranLatestSnapshotsAsPdfButton(final Transaction transaction, final ReportTemplate reportTemplate) {
		this(transaction, reportTemplate, EsfVaadinUI.getInstance().getMsg("TranLatestSnapshotsAsPdfButton.label"));
	}
	
	public TranLatestSnapshotsAsPdfButton(final Transaction transaction, final ReportTemplate reportTemplate, String caption) {
		super();
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();

		this.transaction = transaction;
		this.tranSnapshotsDownloadContextPickupCode = vaadinUi.getEsfapp().getRandomKey().getEsfReportsAccessPickupCodeString();
		
		setCaption(caption);
		setDescription(vaadinUi.getMsg("TranLatestSnapshotsAsPdfButton.tooltip"));
		setIcon(new ThemeResource(vaadinUi.getMsg("button.download.pdf.icon")));
		setStyleName(Reindeer.BUTTON_SMALL);

        TranSnapshotsDownload.DownloadContext downloadContext = new TranSnapshotsDownload.DownloadContext();
		downloadContext.transactionId = transaction.getId();
		downloadContext.downloadFilename = transaction.getPackageDownloadFileName(EsfVaadinUI.getInstance().getUser(), null);

		boolean useLandscape = false;
		for( TransactionDocument tranDoc : reportTemplate == null ? transaction.getAllTransactionDocuments() : reportTemplate.getAllAllowedTransactionDocuments(transaction) ) {
			TransactionPartyDocument tpd = transaction.getLatestTransactionPartyDocumentWithSnapshot(tranDoc);
			if ( tpd != null && ! tpd.isSnapshotDocumentSkipped() ) {
				TransactionDocument td = transaction.getTransactionDocument(tpd.getTransactionDocumentId());
				if ( td.getDocumentVersion().isLandscape() )
					useLandscape = true;

				Document document = tranDoc.getDocument();
				DocumentIdPartyId dp = new DocumentIdPartyId(document.getId(),null);  // Add in for this document, latest
				dp.documentName = document.getEsfName();
				downloadContext.docAndPartyList.add(dp);
			}
		}
		downloadContext.formatType = useLandscape ? TranSnapshotsDownload.FORMAT_PDF_LANDSCAPE : TranSnapshotsDownload.FORMAT_PDF_PORTRAIT;
		vaadinUi.setSessionAttribute(tranSnapshotsDownloadContextPickupCode,downloadContext);
		vaadinUi.getEsfapp().debug("DEBUG-PICKUP-8 TranLatestSnapshotsAsPdfButton: Set download context pickup code: " + tranSnapshotsDownloadContextPickupCode + "; file: "+downloadContext.downloadFilename + "; IP: " + vaadinUi.getRequestHostIp() );
		        
        WebBrowser wb = Page.getCurrent().getWebBrowser();
        String downloadTarget = wb.isChrome() || wb.isSafari() ? "_top" : "_blank";	    	        
    	String servletUrl = vaadinUi.getEsfapp().getExternalContextPath() + "/" + "transnapshotsdownload?" + TranSnapshotsDownload.PARAM_PICKUP_CODE + "=" + tranSnapshotsDownloadContextPickupCode;
    	String downloadUrl = ServletUtil.appendUrlParam(servletUrl, TranSnapshotsDownload.PARAM_MODE, TranSnapshotsDownload.MODE_DOWNLOAD);
    	setResource(new ExternalResource(downloadUrl));
    	setTargetName(downloadTarget);

		numDocuments = reportTemplate == null ? transaction.getNumTransactionDocumentsWithDocumentSnapshot() : reportTemplate.getNumAllowedTransactionDocumentsWithDocumentSnapshot(transaction);
	}
	
	public int getNumDocuments() {
		return numDocuments;
	}
	
	public String toString()
	{
		return Integer.toString(numDocuments);
	}
	
	
	@Override
	public void detach() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.removeSessionAttribute(tranSnapshotsDownloadContextPickupCode);
		vaadinUi.getEsfapp().debug("DEBUG-PICKUP TranLatestSnapshotsAsPdfButton.detach(): Remove download context pickup code: " + tranSnapshotsDownloadContextPickupCode + "; IP: " + vaadinUi.getRequestHostIp());
		super.detach();
	}
}