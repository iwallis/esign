// Copyright (C) 2014-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.widget;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.NameValue;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentInfo;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.PropertySet;
import com.esignforms.open.prog.PropertySetInfo;
import com.esignforms.open.prog.PropertySetVersion;
import com.esignforms.open.prog.Serial;
import com.esignforms.open.prog.SerialInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;


/**
 * The FieldSpecPopupView is a popup view that when clicked allows for a field spec to be
 * looked up, showing the syntax.  It shows the field spec icon until clicked to open the popup.
 * And if associated with a TextField and it's related 'Append' button is clicked, 
 * the field spec notation is appended to the TextField.
 * @author Yozons Inc.
 */
public class FieldSpecPopupView extends PopupView {
	private static final long serialVersionUID = -7088231030472998921L;

	private static final String PROP_TYPE = "FieldSpecPopupView/type";
	private static final String PROP_LIBRARY_ID = "FieldSpecPopupView/libraryId";
	private static final String PROP_DOCUMENT_ID = "FieldSpecPopupView/documentId";
	private static final String PROP_PROPERTYSET_ID = "FieldSpecPopupView/propertySetId";

	SHOW_VIEW currentView = SHOW_VIEW.DOCUMENT;
	
	enum SHOW_VIEW { DOCUMENT, PROPERTY, HTMLPROPERTY, SERIAL, DOCUMENT_BUILT_IN, TRANSACTION_BUILT_IN };
	
	public FieldSpecPopupView() {
		this(null);
	}
	
	public FieldSpecPopupView(TextField relatedTextField) {
		super( new _DummyPopupViewContent() );
		FieldSpecPopupViewContent content = new FieldSpecPopupViewContent(this,relatedTextField);
		setContent(content);
		setStyleName("FieldSpecPopupView");
		setHideOnMouseOut(false);
	}
	
	public class FieldSpecPopupViewContent implements PopupView.Content {
		private static final long serialVersionUID = -2501664948003380473L;

		final FieldSpecPopupView popupView;
		final TextField relatedTextField;
		final VerticalLayout layout;
		final EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		NativeSelect typeSelect = null;
		TextField expandedFieldSpec = null;

		NativeSelect librarySelect = null;
		
		NativeSelect documentSelect = null;
		NativeSelect fieldSelect = null;

		NativeSelect documentBuiltInSelect = null;
		NativeSelect transactionBuiltInSelect = null;
		
		NativeSelect propertySetSelect = null;
		NativeSelect propertySelect = null;
		
		NativeSelect serialSelect = null;

		Button appendButton = null;
		
		public FieldSpecPopupViewContent(FieldSpecPopupView popupView, TextField relatedTextField) {
			this.popupView = popupView;
			this.relatedTextField = relatedTextField;
			this.layout = new VerticalLayout();
			layout.setMargin(true);
			layout.setSpacing(true);
		}

		@Override
		public String getMinimizedValueAsHTML() {
			String align = relatedTextField == null ? "" : " align=\"middle\"";
			String src = " src=\"" + vaadinUi.getEsfapp().getContextPath() + "/" + vaadinUi.getMsg("FieldSpecPopupView.small.icon") + "\"";
			String alt = " alt =\"" + vaadinUi.getMsg("FieldSpecPopupView.small.alt") + "\"";
			String title = " title =\"" + vaadinUi.getMsg("FieldSpecPopupView.small.title") + "\"";
			
			return "<img" + src + alt + title + align + "/>";
		}

		@Override
		public Component getPopupComponent() {
			if ( typeSelect == null ) {
				createWidgets();
				
				EsfUUID prevLibId = vaadinUi.getUser().getUserDefinedUUIDProperty(PROP_LIBRARY_ID);
				if ( prevLibId != null && ! prevLibId.isNull() && librarySelect.containsId(prevLibId) )
					librarySelect.setValue(prevLibId);
				EsfUUID prevDocumentId = vaadinUi.getUser().getUserDefinedUUIDProperty(PROP_DOCUMENT_ID);
				if ( prevDocumentId != null && ! prevDocumentId.isNull() && documentSelect.containsId(prevDocumentId) )
					documentSelect.setValue(prevDocumentId);
				EsfUUID prevPropertySetId = vaadinUi.getUser().getUserDefinedUUIDProperty(PROP_PROPERTYSET_ID);
				if ( prevPropertySetId != null && ! prevPropertySetId.isNull() && propertySetSelect.containsId(prevPropertySetId) )
					propertySetSelect.setValue(prevPropertySetId);
				
				EsfUUID libId = (EsfUUID)librarySelect.getValue();
				if ( libId == null || libId.isNull() ) {
					Collection<?> libraryIds = librarySelect.getItemIds();
					if ( libraryIds.isEmpty() ) {
						librarySelect.setEnabled(false);
					} else {
						Iterator<?> libraryIdsIter = libraryIds.iterator();
						librarySelect.setValue(libraryIdsIter.next());
					}
				} else {
					librarySelect.setEnabled(true);
				}

				EsfString prevType = vaadinUi.getUser().getUserDefinedStringProperty(PROP_TYPE);
				if ( prevType != null && prevType.isNonBlank() && typeSelect.containsId(prevType.toPlainString()) )
					typeSelect.setValue(prevType.toPlainString()); // triggers value change that will set up the prior view
				else
					typeSelect.setValue("document"); // triggers value change that will set up the document view
			} else {
				showCurrentView();
			}
			return layout;
		}
		
		void updateExpandedFieldSpec() {
			boolean specIsComplete = false;
			
			StringBuilder spec = new StringBuilder(100);
			
			if ( currentView == SHOW_VIEW.PROPERTY || currentView == SHOW_VIEW.HTMLPROPERTY) {
				if ( currentView == SHOW_VIEW.PROPERTY )
					spec.append("${property:");
				else
					spec.append("${htmlproperty:");
				
				EsfUUID propSetId = (EsfUUID)propertySetSelect.getValue();
				if( propSetId == null || propSetId.isNull() )
					spec.append("?");
				else {
					PropertySet propSet = PropertySet.Manager.getById(propSetId);
					if ( propSet == null )
						spec.append("???");
					else
						spec.append(propSet.getEsfName());
				}

				spec.append(".");

				EsfPathName propertyName = (EsfPathName)propertySelect.getValue();
				if ( propertyName == null || ! propertyName.isValid() )
					spec.append("?");
				else {
					specIsComplete = true;
					spec.append(propertyName);
				}
				
				spec.append("}");
			} else if ( currentView == SHOW_VIEW.SERIAL ) {
				spec.append("${serial:");
				
				EsfUUID serialId = (EsfUUID)serialSelect.getValue();
				if( serialId == null || serialId.isNull() )
					spec.append("?");
				else {
					Serial serial = Serial.Manager.getById(serialId);
					if ( serial == null )
						spec.append("???");
					else {
						spec.append(serial.getEsfName());
						specIsComplete = true;
					}
				}
				spec.append("}");
			} else if ( currentView == SHOW_VIEW.DOCUMENT_BUILT_IN ) {
				spec.append("${document:");
				
				String builtIn = (String)documentBuiltInSelect.getValue();
				if( builtIn == null )
					spec.append("?");
				else {
					spec.append(builtIn);
					specIsComplete = true;
				}
				spec.append("}");
			} else if ( currentView == SHOW_VIEW.TRANSACTION_BUILT_IN ) {
				spec.append("${transaction:");
				
				String builtIn = (String)transactionBuiltInSelect.getValue();
				if( builtIn == null )
					spec.append("?");
				else {
					spec.append(builtIn);
					specIsComplete = true;
				}
				spec.append("}");
			} else {
				spec.append("${");	
				
				EsfUUID docId = (EsfUUID)documentSelect.getValue();
				if( docId == null || docId.isNull() )
					spec.append("?");
				else {
					Document doc = Document.Manager.getById(docId);
					if ( doc == null )
						spec.append("???");
					else
						spec.append(doc.getEsfName());
				}

				spec.append(".");

				EsfName fieldName = (EsfName)fieldSelect.getValue();
				if ( fieldName == null || ! fieldName.isValid() )
					spec.append("?");
				else {
					specIsComplete = true;
					spec.append(fieldName);
				}
				
				spec.append("}");
			}
			
			setExpandedFieldSpec(spec.toString());
			
			if ( appendButton != null ) {
				appendButton.setEnabled(specIsComplete && !isReadOnly());
			}
		}
		
		void createWidgets() {
			if ( typeSelect == null ) {
				typeSelect = new NativeSelect(vaadinUi.getMsg("FieldSpecPopupView.type.select.label"));
				typeSelect.setRequired(true);
				typeSelect.setNullSelectionAllowed(false);
				typeSelect.setImmediate(true);
				typeSelect.addItem("document");
				typeSelect.setItemCaption("document", vaadinUi.getPrettyCode().fieldSpecType("document"));
				typeSelect.addItem("property");
				typeSelect.setItemCaption("property", vaadinUi.getPrettyCode().fieldSpecType("property"));
				typeSelect.addItem("htmlproperty");
				typeSelect.setItemCaption("htmlproperty", vaadinUi.getPrettyCode().fieldSpecType("htmlproperty"));
				typeSelect.addItem("serial");
				typeSelect.setItemCaption("serial", vaadinUi.getPrettyCode().fieldSpecType("serial"));
				typeSelect.addItem("documentBuiltIn");
				typeSelect.setItemCaption("documentBuiltIn", vaadinUi.getPrettyCode().fieldSpecType("documentBuiltIn"));
				typeSelect.addItem("transactionBuiltIn");
				typeSelect.setItemCaption("transactionBuiltIn", vaadinUi.getPrettyCode().fieldSpecType("transactionBuiltIn"));
				typeSelect.addValueChangeListener( new ValueChangeListener() {
					private static final long serialVersionUID = 980784669768042021L;

					@Override
					public void valueChange(ValueChangeEvent event) {
						String type = (String)event.getProperty().getValue();
						vaadinUi.getUser().setUserDefinedStringProperty(PROP_TYPE, new EsfString(type));
						if ( "property".equals(type) )
							currentView = SHOW_VIEW.PROPERTY;
						else if ( "htmlproperty".equals(type) )
							currentView = SHOW_VIEW.HTMLPROPERTY;
						else if ( "serial".equals(type) )
							currentView = SHOW_VIEW.SERIAL;
						else if ( "documentBuiltIn".equals(type) )
							currentView = SHOW_VIEW.DOCUMENT_BUILT_IN;
						else if ( "transactionBuiltIn".equals(type) )
							currentView = SHOW_VIEW.TRANSACTION_BUILT_IN;
						else
							currentView = SHOW_VIEW.DOCUMENT;
						showCurrentView();
					}
				});
			}
			
			if ( expandedFieldSpec == null ) {
				expandedFieldSpec = new TextField(vaadinUi.getMsg("FieldSpecPopupView.expandedFieldSpec.label"));
				expandedFieldSpec.setStyleName("ExpandedFieldSpec");
				setExpandedFieldSpec("${?}");
			} // end create expanded field spec

			if ( librarySelect == null ) {
				librarySelect = new NativeSelect(vaadinUi.getMsg("FieldSpecPopupView.library.select.label"));
				librarySelect.setNullSelectionAllowed(false);
				librarySelect.setRequired(true);
				librarySelect.setImmediate(true);
				librarySelect.setEnabled(false);
				
				Collection<Library> list = Library.Manager.getForUserWithListPermission(vaadinUi.getUser(), Library.INCLUDE.ONLY_ENABLED);
				for( Library lib : list ) {
					librarySelect.addItem(lib.getId());
					librarySelect.setItemCaption(lib.getId(), lib.getPathName().toString());
				}
				librarySelect.addValueChangeListener( new ValueChangeListener() {
					private static final long serialVersionUID = -1806296087807482366L;

					@Override
					public void valueChange(ValueChangeEvent event) {
						EsfUUID id = (EsfUUID)event.getProperty().getValue();
						vaadinUi.getUser().setUserDefinedUUIDProperty(PROP_LIBRARY_ID, id);
						setupDocumentList(id);
						setupPropertySetList(id);
						setupSerialList(id);
					}
				});
			} // end create library select

			if ( documentSelect == null ) {
				documentSelect = new NativeSelect(vaadinUi.getMsg("FieldSpecPopupView.document.select.label"));
				documentSelect.setRequired(true);
				documentSelect.setNullSelectionAllowed(false);
				documentSelect.setImmediate(true);
				documentSelect.setEnabled(false);
				documentSelect.addValueChangeListener( new ValueChangeListener() {
					private static final long serialVersionUID = 2805909303518055641L;

					@Override
					public void valueChange(ValueChangeEvent event) {
						EsfUUID id = (EsfUUID)event.getProperty().getValue();
						vaadinUi.getUser().setUserDefinedUUIDProperty(PROP_DOCUMENT_ID, id);
						setupFieldList(id);
					}
				});
			} // end create document select
			
			if ( fieldSelect == null ) {
				fieldSelect = new NativeSelect(vaadinUi.getMsg("FieldSpecPopupView.field.select.label"));
				fieldSelect.setRequired(true);
				fieldSelect.setNullSelectionAllowed(false);
				fieldSelect.setImmediate(true);
				fieldSelect.setEnabled(false);
				fieldSelect.addValueChangeListener( new ValueChangeListener() {
					private static final long serialVersionUID = 8210722457929466387L;

					@Override
					public void valueChange(ValueChangeEvent event) {
						updateExpandedFieldSpec();
					}
				});
			} // end create field select
			
			if ( documentBuiltInSelect == null ) {
				documentBuiltInSelect = new NativeSelect(vaadinUi.getMsg("FieldSpecPopupView.documentBuiltIn.select.label"));
				documentBuiltInSelect.setRequired(true);
				documentBuiltInSelect.setNullSelectionAllowed(false);
				documentBuiltInSelect.setImmediate(true);
				documentBuiltInSelect.setEnabled(true); // Always enabled (nothing to choose before it can be used)
				String[] builtIns = vaadinUi.getMsg("FieldSpecPopupView.documentBuiltIn.select.values").split(";"); // fixed values that never change
				for( String builtIn : builtIns )
					documentBuiltInSelect.addItem(builtIn);
				documentBuiltInSelect.addValueChangeListener( new ValueChangeListener() {
					private static final long serialVersionUID = -8283789705965391364L;

					@Override
					public void valueChange(ValueChangeEvent event) {
						updateExpandedFieldSpec();
					}
				});
			} // end create document built-in select
			
			if ( transactionBuiltInSelect == null ) {
				transactionBuiltInSelect = new NativeSelect(vaadinUi.getMsg("FieldSpecPopupView.transactionBuiltIn.select.label"));
				transactionBuiltInSelect.setRequired(true);
				transactionBuiltInSelect.setNullSelectionAllowed(false);
				transactionBuiltInSelect.setImmediate(true);
				transactionBuiltInSelect.setEnabled(true); // Always enabled (nothing to choose before it can be used)
				String[] builtIns = vaadinUi.getMsg("FieldSpecPopupView.transactionBuiltIn.select.values").split(";"); // fixed values that never change
				for( String builtIn : builtIns )
					transactionBuiltInSelect.addItem(builtIn);
				transactionBuiltInSelect.addValueChangeListener( new ValueChangeListener() {
					private static final long serialVersionUID = -2183175147260655730L;

					@Override
					public void valueChange(ValueChangeEvent event) {
						updateExpandedFieldSpec();
					}
				});
			} // end create transaction built-in select
			
			if ( propertySetSelect == null ) {
				propertySetSelect = new NativeSelect(vaadinUi.getMsg("FieldSpecPopupView.propertySet.select.label"));
				propertySetSelect.setRequired(true);
				propertySetSelect.setNullSelectionAllowed(false);
				propertySetSelect.setImmediate(true);
				propertySetSelect.setEnabled(false);
				propertySetSelect.addValueChangeListener( new ValueChangeListener() {
					private static final long serialVersionUID = -8283789705965391364L;

					@Override
					public void valueChange(ValueChangeEvent event) {
						EsfUUID id = (EsfUUID)event.getProperty().getValue();
						vaadinUi.getUser().setUserDefinedUUIDProperty(PROP_PROPERTYSET_ID, id);
						setupPropertyList(id);
					}
				});
			} // end create propertyset select
			
			if ( propertySelect == null ) {
				propertySelect = new NativeSelect(vaadinUi.getMsg("FieldSpecPopupView.property.select.label"));
				propertySelect.setRequired(true);
				propertySelect.setNullSelectionAllowed(false);
				propertySelect.setImmediate(true);
				propertySelect.setEnabled(false);
				propertySelect.addValueChangeListener( new ValueChangeListener() {
					private static final long serialVersionUID = -2183175147260655730L;

					@Override
					public void valueChange(ValueChangeEvent event) {
						updateExpandedFieldSpec();
					}
				});
			} // end create property select
			
			if ( serialSelect == null ) {
				serialSelect = new NativeSelect(vaadinUi.getMsg("FieldSpecPopupView.serial.select.label"));
				serialSelect.setRequired(true);
				serialSelect.setNullSelectionAllowed(false);
				serialSelect.setImmediate(true);
				serialSelect.setEnabled(false);
				serialSelect.addValueChangeListener( new ValueChangeListener() {
					private static final long serialVersionUID = 742736404635353045L;

					@Override
					public void valueChange(ValueChangeEvent event) {
						updateExpandedFieldSpec();
					}
				});
			} // end create serialSelect select
			
		}
		
		void showCurrentView() {
			layout.removeAllComponents();
			
			addStandardTypeSelector();
			
			if ( currentView == SHOW_VIEW.PROPERTY )
				showPropertyView(false);
			else if ( currentView == SHOW_VIEW.HTMLPROPERTY)
				showPropertyView(true);
			else if ( currentView == SHOW_VIEW.SERIAL)
				showSerialView();
			else if ( currentView == SHOW_VIEW.DOCUMENT_BUILT_IN)
				showDocumentBuiltInView();
			else if ( currentView == SHOW_VIEW.TRANSACTION_BUILT_IN)
				showTransactionBuiltInView();
			else
				showDocumentView();
			
			addStandardButtons();
			
			updateExpandedFieldSpec();
		}
		
		void showPropertyView(boolean isHtml) {
			HorizontalLayout line1 = new HorizontalLayout();
			line1.setMargin(false);
			line1.setSpacing(true);
			layout.addComponent(line1);
			
			librarySelect.setEnabled( librarySelect.size() > 0 );
			propertySetSelect.setEnabled( librarySelect.isEnabled() && propertySetSelect.size() > 0 );
			propertySelect.setEnabled( propertySetSelect.isEnabled() && propertySelect.size() > 0 );
			
			line1.addComponent(librarySelect);
			line1.addComponent(propertySetSelect);
			line1.addComponent(propertySelect);
		}
		
		void showSerialView() {
			HorizontalLayout line1 = new HorizontalLayout();
			line1.setMargin(false);
			line1.setSpacing(true);
			layout.addComponent(line1);
			
			librarySelect.setEnabled( librarySelect.size() > 0 );
			serialSelect.setEnabled( librarySelect.isEnabled() && serialSelect.size() > 0 );
			
			line1.addComponent(librarySelect);
			line1.addComponent(serialSelect);
		}
		
		void showDocumentBuiltInView() {
			HorizontalLayout line1 = new HorizontalLayout();
			line1.setMargin(false);
			line1.setSpacing(true);
			layout.addComponent(line1);
			
			line1.addComponent(documentBuiltInSelect);
		}
		
		void showTransactionBuiltInView() {
			HorizontalLayout line1 = new HorizontalLayout();
			line1.setMargin(false);
			line1.setSpacing(true);
			layout.addComponent(line1);
			
			line1.addComponent(transactionBuiltInSelect);
		}
		
		void showDocumentView() {
			HorizontalLayout line1 = new HorizontalLayout();
			line1.setMargin(false);
			line1.setSpacing(true);
			layout.addComponent(line1);
			
			librarySelect.setEnabled( librarySelect.size() > 0 );
			documentSelect.setEnabled( librarySelect.isEnabled() && documentSelect.size() > 0 );
			fieldSelect.setEnabled( documentSelect.isEnabled() && fieldSelect.size() > 0 );
			
			line1.addComponent(librarySelect);
			line1.addComponent(documentSelect);
			line1.addComponent(fieldSelect);
		}
		
		void setupDocumentList(EsfUUID libraryId) {
			EsfName currentDocumentName = null;

			EsfUUID docId = (EsfUUID)documentSelect.getValue();
			if ( docId != null && ! docId.isNull() ) {
				Document doc = Document.Manager.getById(docId);
				if ( doc != null )
					currentDocumentName = doc.getEsfName();
			}
			
			documentSelect.removeAllItems();
			
			boolean hasCurrentDocumentName = false;
			Library library = Library.Manager.getById(libraryId, vaadinUi.getUser());
			if ( library != null ) {
				Collection<DocumentInfo> list = DocumentInfo.Manager.getAll(library, DocumentInfo.INCLUDE.ONLY_ENABLED);
				for( DocumentInfo docInfo : list ) {
					if ( docInfo.getEsfName().equals(currentDocumentName) )
						hasCurrentDocumentName = true;
					documentSelect.addItem(docInfo.getId());
					documentSelect.setItemCaption(docInfo.getId(),docInfo.getEsfName().toString());
				}
			}

			if ( hasCurrentDocumentName )
				documentSelect.setValue(currentDocumentName);
			else
				documentSelect.setValue(null);

			documentSelect.setEnabled( librarySelect.isEnabled() && documentSelect.size() > 0 );
			updateExpandedFieldSpec();
		}
		
		void setupFieldList(EsfUUID docId) {
			EsfName currentFieldName = (EsfName)fieldSelect.getValue();
			
			fieldSelect.removeAllItems();
			
			boolean hasCurrentFieldName = false;
			Document doc = Document.Manager.getById(docId);
			if ( doc != null ) {
				DocumentVersion docVer = null;
				if ( vaadinUi.isProductionMode() || vaadinUi.isTestLikeProductionMode() ) {
					docVer = doc.getProductionDocumentVersion();
				} else {
					docVer = doc.getTestDocumentVersion();
				}
				if ( docVer != null ) {
					Map<EsfName,FieldTemplate> fieldMap = docVer.getFieldTemplateMap();
					for( EsfName fieldName : fieldMap.keySet() ) {
						FieldTemplate template = fieldMap.get(fieldName);
						if ( template.isTypeRadioButton() )  // radio buttons are not normal fields (have no value) should use the radio button group instead
							continue;
						if ( fieldName.equals(currentFieldName) )
							hasCurrentFieldName = true;
						fieldSelect.addItem(fieldName);
						fieldSelect.setItemCaption(fieldName,fieldName.toString());
					}
				}
			}
			
			if ( hasCurrentFieldName )
				fieldSelect.setValue(currentFieldName);
			else
				fieldSelect.setValue(null);
			
			fieldSelect.setEnabled( documentSelect.isEnabled() && fieldSelect.size() > 0 );
			updateExpandedFieldSpec();
		}
		
		void setupPropertySetList(EsfUUID libraryId) {
			EsfName currentPropertySetName = null;

			EsfUUID propSetId = (EsfUUID)propertySetSelect.getValue();
			if ( propSetId != null && ! propSetId.isNull() ) {
				PropertySet propSet = PropertySet.Manager.getById(propSetId);
				if ( propSet != null )
					currentPropertySetName = propSet.getEsfName();
			}
			
			propertySetSelect.removeAllItems();
			
			boolean hasCurrentPropertySetName = false;
			Library library = Library.Manager.getById(libraryId, vaadinUi.getUser());
			if ( library != null ) {
				Collection<PropertySetInfo> list = PropertySetInfo.Manager.getAll(libraryId);
				for( PropertySetInfo propSetInfo : list ) {
					if ( propSetInfo.getEsfName().equals(currentPropertySetName) )
						hasCurrentPropertySetName = true;
					propertySetSelect.addItem(propSetInfo.getId());
					propertySetSelect.setItemCaption(propSetInfo.getId(),propSetInfo.getEsfName().toString());
				}
			}

			if ( hasCurrentPropertySetName )
				propertySetSelect.setValue(currentPropertySetName);
			else
				propertySetSelect.setValue(null);

			propertySetSelect.setEnabled( librarySelect.isEnabled() && propertySetSelect.size() > 0 );
			updateExpandedFieldSpec();
		}
		
		void setupPropertyList(EsfUUID propSetId) {
			EsfPathName currentPropertyName = (EsfPathName)propertySelect.getValue();
			
			propertySelect.removeAllItems();
			
			boolean hasCurrentPropertyName = false;
			PropertySet propSet = PropertySet.Manager.getById(propSetId);
			if ( propSet != null ) {
				PropertySetVersion propSetVer = null;
				if ( vaadinUi.isProductionMode() || vaadinUi.isTestLikeProductionMode() ) {
					propSetVer = propSet.getProductionPropertySetVersion();
				} else {
					propSetVer = propSet.getTestPropertySetVersion();
				}
				if ( propSetVer != null ) {
					for( NameValue nv : propSetVer.getProperties().getSortedNameValues() ) {
						if ( nv.getPathName().toString().endsWith("_TEST") ) // Don't include TEST property names
							continue;
						if ( nv.getPathName().equals(currentPropertyName) )
							hasCurrentPropertyName = true;
						propertySelect.addItem(nv.getPathName());
						propertySelect.setItemCaption(nv.getPathName(),nv.getPathName().toString());
					}
				}
			}
			
			if ( hasCurrentPropertyName )
				propertySelect.setValue(currentPropertyName);
			else
				propertySelect.setValue(null);
			
			propertySelect.setEnabled( propertySetSelect.isEnabled() && propertySelect.size() > 0 );
			updateExpandedFieldSpec();
		}
		
		void setupSerialList(EsfUUID libraryId) {
			EsfName currentSerialName = null;

			EsfUUID serialId = (EsfUUID)serialSelect.getValue();
			if ( serialId != null && ! serialId.isNull() ) {
				Serial serial = Serial.Manager.getById(serialId);
				if ( serial != null )
					currentSerialName = serial.getEsfName();
			}
			
			serialSelect.removeAllItems();
			
			boolean hasCurrentSerialName = false;
			Library library = Library.Manager.getById(libraryId, vaadinUi.getUser());
			if ( library != null ) {
				Collection<SerialInfo> list = SerialInfo.Manager.getAll(libraryId);
				for( SerialInfo serialInfo : list ) {
					if ( serialInfo.getEsfName().equals(currentSerialName) )
						hasCurrentSerialName = true;
					serialSelect.addItem(serialInfo.getId());
					serialSelect.setItemCaption(serialInfo.getId(),serialInfo.getEsfName().toString());
				}
			}

			if ( hasCurrentSerialName )
				serialSelect.setValue(currentSerialName);
			else
				serialSelect.setValue(null);

			serialSelect.setEnabled( librarySelect.isEnabled() && serialSelect.size() > 0 );
			updateExpandedFieldSpec();
		}
		
		void addStandardTypeSelector() {
			HorizontalLayout line1 = new HorizontalLayout();
			line1.setMargin(false);
			line1.setSpacing(true);
			layout.addComponent(line1);
			line1.addComponent(typeSelect);
			line1.addComponent(expandedFieldSpec);
			line1.setExpandRatio(expandedFieldSpec, 1.0f);
		}
		
		void setExpandedFieldSpec(String v) {
			expandedFieldSpec.setReadOnly(false);
			expandedFieldSpec.setValue(v);
			expandedFieldSpec.setReadOnly(true);
		}
		
		void addStandardButtons() {
			HorizontalLayout buttonLayout = new HorizontalLayout();
			buttonLayout.setMargin(false);
			buttonLayout.setSpacing(true);
			layout.addComponent(buttonLayout);
			
			Button closeButton = new Button(vaadinUi.getMsg("FieldSpecPopupView.closeButton.label"));
			closeButton.setDisableOnClick(true);
			closeButton.addClickListener( new ClickListener() {
				private static final long serialVersionUID = 7129427350181553475L;

				@Override
				public void buttonClick(ClickEvent event) {
					popupView.setPopupVisible(false);
				}
			});
			buttonLayout.addComponent(closeButton);
			
			if ( relatedTextField != null ) {
				appendButton = new Button(vaadinUi.getMsg("FieldSpecPopupView.appendButton.label"));
				appendButton.setEnabled(false);
				appendButton.setDisableOnClick(true);
				appendButton.addClickListener( new ClickListener() {
					private static final long serialVersionUID = 7617240244508627210L;

					@Override
					public void buttonClick(ClickEvent event) {
						String v = relatedTextField.getValue();
						if ( v == null )
							v = "";
						relatedTextField.setValue( v + expandedFieldSpec.getValue() );
						popupView.setPopupVisible(false);
					}
				});
				buttonLayout.addComponent(appendButton);
			}
		}
	}
	
	
	// This is just a dummy popup view for initialization.
	private static class _DummyPopupViewContent implements PopupView.Content {
		private static final long serialVersionUID = -7799387932672482332L;

		@Override
		public String getMinimizedValueAsHTML() {
			return "";
		}

		@Override
		public Component getPopupComponent() {
			return null;
		}
	}

}