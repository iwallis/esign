// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.main;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;

import fi.jasoft.dragdroplayouts.DDTabSheet;
import fi.jasoft.dragdroplayouts.client.ui.LayoutDragMode;
import fi.jasoft.dragdroplayouts.drophandlers.DefaultTabSheetDropHandler;

public class MainTabSheet extends DDTabSheet implements TabSheet.CloseHandler {
	private static final long serialVersionUID = -895044470994402330L;

	public MainTabSheet() {
    	super();
    	setCloseHandler(this);
    	
    	// ipad and iphone won't work if drag enabled for tabs
    	User loggedInUser = EsfVaadinUI.getInstance().getUser();
    	if ( loggedInUser == null || loggedInUser.isIPAD() || loggedInUser.isIPHONE() ) { 
        	setDragMode(LayoutDragMode.NONE);
    	} else {
        	setDragMode(LayoutDragMode.CLONE);
        	setDropHandler(new DefaultTabSheetDropHandler());
    	}
	}
	
    public void setTabState() {
		// If we have more than 1 tab, make them all closable, else make the last tab non-closable
		int numTabs = 0;
		Component lastComponent = null;
		Iterator<Component> iter = iterator();
		while( iter.hasNext() ) {
			++numTabs;
			lastComponent = iter.next();
		}
		if ( numTabs == 1 ) {
			Tab tab = getTab(lastComponent);
			tab.setClosable(false);
		} else if ( numTabs > 1 ) {
			iter = iterator();
			while( iter.hasNext() ) {
				Tab tab = getTab(iter.next());
				tab.setClosable(true);
			}
		}
    }
 
	public void superClose(TabSheet tabsheet, Component tabContent) {
		EsfVaadinUI.getInstance().getMainView().tabClosed(tabsheet.getTab(tabContent));
		tabsheet.removeComponent(tabContent);
		setTabState();
	}
	
	public Tab getCurrentSelectedTab() {
		return getTab(getSelectedTab());
	}
	
	public List<Tab> getTabList() {
		LinkedList<Tab> tabList = new LinkedList<Tab>();
		Iterator<Component> iter = iterator();
		while( iter.hasNext() ) {
			Tab tab = getTab(iter.next());
			if ( tab != null )
				tabList.add(tab);
		}
		return tabList;
	}
	
	@Override
	public void onTabClose(final TabSheet tabsheet, final Component tabContent) {
		EsfView view = (EsfView)tabContent;
		if ( view != null && view.isDirty() ) {
			new ConfirmDiscardChangesDialog(view.checkDirty(), new ConfirmDiscardChangesListener() {

				@Override
				public void doContinueKeepChanges() {
				}

				@Override
				public void doDiscardChanges() {
					superClose(tabsheet,tabContent);
				}
				
			});
		} else {
			superClose(tabsheet, tabContent);
		}
		
	}
}