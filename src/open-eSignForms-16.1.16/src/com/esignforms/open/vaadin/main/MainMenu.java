// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.main;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.config.MessageFormatFile;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.prog.Library.INCLUDE;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.UI;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.EsfVaadinUI.LibraryChangeListener;
import com.esignforms.open.vaadin.EsfVaadinUI.Mode;
import com.esignforms.open.vaadin.EsfVaadinUI.ModeChangeListener;
import com.esignforms.open.vaadin.EsfVaadinUI.ReportTemplateChangeListener;
import com.esignforms.open.vaadin.EsfVaadinUI.TransactionTemplateChangeListener;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.BorderStyle;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;

/**
 * Based on MainNavigationTree from Vaadin 6, but we've switch from a Tree to a Menu.
 */
public class MainMenu extends CustomComponent implements ModeChangeListener, LibraryChangeListener, ReportTemplateChangeListener, TransactionTemplateChangeListener {
	private static final long serialVersionUID = 449892147643618839L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(MainMenu.class);

	final MainMenu thisMenu;
	MessageFormatFile navigationSetup;
	UI ui;
	
	String defaultViewName;
	String defaultViewParams;
	OpenInTabCommand defaultOpenInTabCommand;
	
	ComboBox modeSelector;
	MenuBar menuBar;
	MenuItem startTransactionMenuItem;
	String startTransactionLabelKey;
	MenuItem reportsMenuItem;
	String reportsLabelKey;
	MenuItem librariesMenuItem;
	String librariesLabelKey;
	HorizontalLayout layout;
	
    public MainMenu(MainView mainView) { // We pass in the main view since it's under construction when this is called
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		thisMenu = this;
		ui = vaadinUi.getEsfapp().getUI();
    	addStyleName("MainMenu");
    	setWidth(100, Unit.PERCENTAGE);
    	
    	navigationSetup = new MessageFormatFile("com.esignforms.open.vaadin.navigation", getLocale());
    	vaadinUi.setLogoutURL(navigationSetup.getString("logoutUrl"));
    	
    	defaultViewName = navigationSetup.getString("defaultView");
    	if ( EsfString.isBlank(defaultViewName) )
    		defaultViewName = "WelcomeTipsView";
    	defaultViewParams = navigationSetup.getString("defaultView.params");
    	if ( EsfString.isBlank(defaultViewParams) )
    		defaultViewParams = "Welcome";
    	
    	layout = new HorizontalLayout();
    	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(false);
    	layout.setSpacing(false);
    	setCompositionRoot(layout);
    	
    	// Add our mode selector
    	modeSelector = createModeSelector();
    	layout.addComponent(modeSelector);
    	
    	menuBar = createMenuBar();
    	layout.addComponent(menuBar);
    	layout.setExpandRatio(menuBar, 1.0f);
    	
    	vaadinUi.addModeChangeListener(this);
    	vaadinUi.addLibraryChangeListener(this);
    	vaadinUi.addReportTemplateChangeListener(this);
    	vaadinUi.addTransactionTemplateChangeListener(this);
    }
    
    public OpenInTabCommand getDefaultOpenInTabCommand() {
    	return defaultOpenInTabCommand;
    }
    
    public OpenInTabCommand getOpenInTabCommand(String viewName, String params) {
    	return getOpenInTabCommand(null,viewName,params);
    }
    public OpenInTabCommand getOpenInTabCommand(MenuItem menuItem, String viewName, String params) {
    	List<MenuItem> itemList = menuItem == null ? menuBar.getItems() : menuItem.getChildren();
    	for( MenuItem mi : itemList ) {
    		MenuBar.Command c = mi.getCommand();
    		if ( c != null && c instanceof OpenInTabCommand ) {
    			OpenInTabCommand otc = (OpenInTabCommand)c;
    			if ( otc.getViewName().equals(viewName) && otc.getViewParams().equals(params) ) {
    				return otc;
    			}
    		}
    		// See if it's stored in a submenu
    		if ( mi.hasChildren() ) {
    			OpenInTabCommand otc = getOpenInTabCommand(mi,viewName,params);
    			if ( otc != null ) {
    				return otc;
    			}
    		}
    	}
    	return null;
    }
    
    ComboBox createModeSelector() {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	ComboBox combo = new ComboBox();
    	combo.setStyleName("modeSelector");
    	combo.setDescription(vaadinUi.getMsg("MainNavigationMenu.modeSelector.tooltip"));
    	
    	if ( vaadinUi.getEsfapp().allowProductionTransactions() ) {
        	combo.addItem(EsfVaadinUI.Mode.PRODUCTION);
        	combo.setItemCaption(EsfVaadinUI.Mode.PRODUCTION, vaadinUi.getPrettyCode().modeSelector(EsfVaadinUI.Mode.PRODUCTION));
    	}
    	if ( vaadinUi.getEsfapp().allowTestTransactions() ) {
        	combo.addItem(EsfVaadinUI.Mode.TEST_LIKE_PRODUCTION);
        	combo.setItemCaption(EsfVaadinUI.Mode.TEST_LIKE_PRODUCTION, vaadinUi.getPrettyCode().modeSelector(EsfVaadinUI.Mode.TEST_LIKE_PRODUCTION));
        	combo.addItem(EsfVaadinUI.Mode.TEST);
        	combo.setItemCaption(EsfVaadinUI.Mode.TEST, vaadinUi.getPrettyCode().modeSelector(EsfVaadinUI.Mode.TEST));
    	}    	
    	combo.setFilteringMode(FilteringMode.OFF);
    	combo.setTextInputAllowed(false);
    	combo.setValue(vaadinUi.getMode());
    	combo.setImmediate(true);
    	combo.setNullSelectionAllowed(false);
    	combo.setNewItemsAllowed(false);
    	combo.setNullSelectionAllowed(false);
    	combo.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = 3514130262840078312L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				EsfVaadinUI.Mode newMode = (EsfVaadinUI.Mode)event.getProperty().getValue();
				vaadinUi.setMode(newMode);
				vaadinUi.getMainView().setCurrentModeStyleName();
			}
    		
    	});
    	return combo;
    }
    
    MenuBar createMenuBar() {
    	MenuBar menu = new MenuBar();
    	menu.setStyleName("menu");
    	menu.setWidth(100, Unit.PERCENTAGE);
    	
    	loadChildren(menu,null,"nav.");
    	
    	return menu;
    }
    
    MenuItem loadChildren(MenuBar menu, MenuItem menuItem, String prefix) {
    	int index = 1;
    	
    	MenuItem lastMenuItem = null;
    	
    	while( true ) {
    		String itemPrefix = prefix + index + ".";
        	try {
        		String checkSpecialView = navigationSetup.getString(itemPrefix+"view.name");
        		if ( "#libraryprogramming".equalsIgnoreCase(checkSpecialView) ) {
        			if ( librariesMenuItem == null ) { 
        				librariesMenuItem = menuItem;
        			}
        			librariesLabelKey = itemPrefix+"label";
        			createLibrariesMenuItems();
        			// There are no children of reports menu item..., but continue with next item
            		++index;
            		continue;
        		} else if ( "#reports".equalsIgnoreCase(checkSpecialView) ) {
        			if ( reportsMenuItem == null ) { 
        				reportsMenuItem = menuItem;
        			}
        			reportsLabelKey = itemPrefix+"label";
        			createReportsMenuItems();
        			// There are no children of reports menu item..., but continue with next item
            		++index;
            		continue;
        		} else if ( "#starttransaction".equalsIgnoreCase(checkSpecialView) ) {
        			if ( startTransactionMenuItem == null ) { 
        				startTransactionMenuItem = menuItem;
        			}
        			startTransactionLabelKey = itemPrefix+"label";
        			createStartTransactionMenuItems();
        			// There are no children of start tran menu item..., but continue with next item
            		++index;
        			continue;
        		} else if ( "#spacer".equalsIgnoreCase(checkSpecialView) ) {
        			MenuItem spacer;
        	    	if ( menuItem == null && menu != null ) {
        	    		spacer = menu.addItem("|",null);
        	    	} else {
        	    		spacer = menuItem.addItem("",null);
        	    	}
    	    		spacer.setStyleName("spacer");
            		spacer.setEnabled(false);
        			// There are no children of a spacer, but continue with next index
            		++index;
        			continue;
        		}
        		
        		// This one is also special, but it can have children
	    		if ( "#labelonly".equalsIgnoreCase(checkSpecialView) ) {
	    			String label = navigationSetup.getString(itemPrefix+"label");
	    			String icon = navigationSetup.getString(itemPrefix+"icon");
	    			ThemeResource iconThemeResource = EsfString.isNonBlank(icon) ? new ThemeResource(icon) : null;
	    	    	if ( menuItem == null && menu != null ) {
	    	    		lastMenuItem = menu.addItem(label,iconThemeResource,null);
	    	    	} else {
	    	    		lastMenuItem = menuItem.addItem(label,iconThemeResource,null);
	    	    	}
	    		} else {
	    			// Regular menu item 
	        		lastMenuItem = createMenuItem(menu,menuItem,itemPrefix);
	    		}
        		
        		if ( lastMenuItem == null )
        			break;
            	
            	// Now load the children of this menu item
        		loadChildren(null, lastMenuItem, itemPrefix);
        	} catch( Exception e ) {
        		// no permission, so try next 
        	}
        	
        	++index;
        }
    	
    	return lastMenuItem;
    }
    
	@SuppressWarnings("unchecked")
	MenuItem createMenuItem(MenuBar menu, MenuItem menuItem, String prefix) throws Exception {
    	String label = navigationSetup.getString(prefix+"label");
    	String viewName = navigationSetup.getString(prefix+"view.name");
    	
    	// If we have no label or view, then we're done
    	if ( EsfString.areAnyBlank(label,viewName) ) {
    		return null;
    	}
    	
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	// Check it's permission
    	if ( ! ui.canList(vaadinUi.getUser(), new EsfName(viewName)) )
    		throw new Exception("No permission");
    	
    	String icon = navigationSetup.getString(prefix+"icon");
    	Class<EsfView> viewClass = null;
    	String viewParams = null;
    	String tabTitle = null;
    	String tabTooltip = null;
		String viewClassName = navigationSetup.getString(viewName+".class");
		try {
			Class<?> checkViewClass = Class.forName(viewClassName);
			if ( EsfView.class.isAssignableFrom(checkViewClass) ) {
				viewClass = (Class<EsfView>)checkViewClass;
			}
		} catch( ClassNotFoundException e ) {
			_logger.error("createMenuItem() - Could not find view class for view name: " + viewName + "; view class: " + viewClassName + "; key: " + prefix+"view.name");
		}
		// If we don't have an icon on the navigation element, see if there's a view icon to use
		if ( EsfString.isBlank(icon) ) {
    		icon = navigationSetup.getString(viewName+".icon");
		}
		viewParams = navigationSetup.getString(viewName+".params");
		tabTitle = navigationSetup.getString(viewName+".tabTitle");
		tabTooltip = navigationSetup.getString(viewName+".tabTooltip");
     	
    	ThemeResource iconThemeResource = EsfString.isNonBlank(icon) ? new ThemeResource(icon) : null;
    	
    	// In this case, we'll create the menu item under the menubar
    	MenuItem newMenuItem;
    	
    	OpenInTabCommand openInTabCommand = new OpenInTabCommand(viewName, viewParams, viewClass, iconThemeResource, tabTitle, tabTooltip);
    	if ( viewName.equals(defaultViewName) && viewParams.equals(defaultViewParams) ) {
    		defaultOpenInTabCommand = openInTabCommand;
    	}
    	
    	if ( menuItem == null && menu != null ) {
    		newMenuItem = menu.addItem(label, iconThemeResource, openInTabCommand);
    	} else {
    		newMenuItem = menuItem.addItem(label, iconThemeResource, openInTabCommand);
    	}
    	
    	return newMenuItem;
    }
	    
	
	void createStartTransactionMenuItems() throws Exception {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		// Let's remove all start transaction menu items, but leave anything else (this assumes all start trans appear together before anything else like the Tips)
		MenuItem firstNonStartTransactionMenuItem = null;
		if ( startTransactionMenuItem.hasChildren() ) {
			List<MenuItem> children = new LinkedList<MenuItem>(startTransactionMenuItem.getChildren());
			for( MenuItem mi : children ) {
				if ( mi.getCommand() instanceof OpenInBrowserWindowCommand )
					startTransactionMenuItem.removeChild(mi);
				else if ( firstNonStartTransactionMenuItem == null )
					firstNonStartTransactionMenuItem = mi;
			}
		}
	    	
		// Get the list of transaction templates the user can start.
		List<TransactionTemplate> templates = TransactionTemplate.Manager.getForUserWithPermission(vaadinUi.getUser(), PermissionOption.TRAN_PERM_OPTION_START);
		if ( templates.size() > 1 ) {
			// Let's show these in display name order
			Collections.sort(templates, TransactionTemplate.Manager.ComparatorByDisplayName);
		}

		String icon = navigationSetup.getString("StartTransaction.icon");

		ThemeResource iconThemeResource = EsfString.isNonBlank(icon) ? new ThemeResource(icon) : null;
	    	
		TransactionTemplate templateTransactionTemplate = TransactionTemplate.Manager.getTemplate();
	    	
		int numTransactions = 0;
		for( TransactionTemplate template : templates ) {
			// If not enabled for either test or production, skip it.
			if ( template.isProductionDisabled() && template.isTestDisabled() ) {
				continue;
	    	}
			// Let's not put in the Template Transaction Template...
			if ( template.equals(templateTransactionTemplate) ) {
				continue;
			}
	    		
			// If it's production mode and it can start in production, or we're in test mode and test is enabled, add to the menu.
			if ( (vaadinUi.isProductionMode() && template.isProductionEnabled()) || 
				 (vaadinUi.isTestLikeProductionMode() && template.isProductionEnabled() && template.isTestEnabled()) ||
				 (vaadinUi.isTestMode() && template.isTestEnabled())
			   ) {
				String label = navigationSetup.getString(startTransactionLabelKey,template.getDisplayName(),template.getPathName().toString(),template.getDescription());
				
				String url;
				if( vaadinUi.isProductionMode() ) {
					url = template.getProductionStartUrl();
				} else if ( vaadinUi.isTestLikeProductionMode() ) {
					url = template.getTestLikeProductionStartUrl();
				} else {
					url = template.getTestStartUrl();
				}
				
				OpenInBrowserWindowCommand openInBrowserWindowCommand = new OpenInBrowserWindowCommand(url);
	        	// In this case, we'll create the menu item under the start transaction menu item
				if ( firstNonStartTransactionMenuItem == null )
					startTransactionMenuItem.addItem(label, iconThemeResource, openInBrowserWindowCommand);
				else
					startTransactionMenuItem.addItemBefore(label, iconThemeResource, openInBrowserWindowCommand, firstNonStartTransactionMenuItem);
				//MenuItem newMenuItem = startTransactionMenuItem.addItem(label, iconThemeResource, openInBrowserWindowCommand);
				//BrowserWindowOpener startTranOpener = new BrowserWindowOpener("");
				//startTranOpener.extend(newMenuItem);
				
				++numTransactions;
			}
		}
	     	
		if ( numTransactions == 0 )
			throw new Exception("No start tran permission");
	}
		
	@SuppressWarnings("unchecked")
	void createReportsMenuItems() throws Exception {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		// Let's remove all reports, but leave anything else (this assumes all reports appear together before anything else like the Tips)
		MenuItem firstNonReportMenuItem = null;
		if ( reportsMenuItem.hasChildren() ) {
			List<MenuItem> children = new LinkedList<MenuItem>(reportsMenuItem.getChildren());
			for( MenuItem mi : children ) {
				if ( mi.getCommand() instanceof OpenInTabCommand ) {
					OpenInTabCommand command = (OpenInTabCommand)mi.getCommand();
					if ( command.getViewName().equals("Report") )
						reportsMenuItem.removeChild(mi);
					else if ( firstNonReportMenuItem == null )
						firstNonReportMenuItem = mi;
				}
			}
		}
		
    	// Get the list of enabled report templates the user can run
		List<ReportTemplate> templates = ReportTemplate.Manager.getEnabledForUserWithPermission(vaadinUi.getUser(), PermissionOption.REPORT_PERM_OPTION_RUN_REPORT);
		if ( templates.size() == 0 )
			throw new Exception("No reporting permission");
		if ( templates.size() > 1 ) {
			// Let's show these in display name order
			Collections.sort(templates, ReportTemplate.Manager.ComparatorByDisplayName);
		}
		
    	String viewName = "Report";
		String icon = navigationSetup.getString(viewName+".icon");
		Class<EsfView> viewClass = null;
		
		String viewClassName = navigationSetup.getString(viewName+".class");
		try {
			Class<?> checkViewClass = Class.forName(viewClassName);
			if ( EsfView.class.isAssignableFrom(checkViewClass) ) {
				viewClass = (Class<EsfView>)checkViewClass;
			} else {
				_logger.error("createReportsMenuItems() - Class for view is not EsfView type: " + viewName + "; class: " + viewClassName);
			}
		} catch( ClassNotFoundException e ) {
			_logger.error("createReportsMenuItems() - Could not find view class for view name: " + viewName + "; view class: " + viewClassName);
		}
    	
    	ThemeResource iconThemeResource = EsfString.isNonBlank(icon) ? new ThemeResource(icon) : null;
		
		ReportTemplate templateReportTemplate = ReportTemplate.Manager.getTemplate();
		
		int numReports = 0;
		for( ReportTemplate template : templates ) {
			// If not enabled, skip it.  Should not happen since above we only get enabled reports.
			if ( template.isDisabled() ) {
				continue;
			}
			// Let's not put in the template Report Template...
			if ( template.equals(templateReportTemplate) ) {
				continue;
			}
			
			// If we're in test mode, or if production and the user has permission for production, include in our menu.
			if ( ! vaadinUi.isProductionMode() || template.hasViewProductionPermission(vaadinUi.getUser()) ) {
	    		String tabTitle = navigationSetup.getString(viewName+".tabTitle",template.getPathName(),template.getDisplayName(),template.getDescription());
	    		String tabTooltip = navigationSetup.getString(viewName+".tabTooltip",template.getPathName(),template.getDisplayName(),template.getDescription());
	    		String label = navigationSetup.getString(reportsLabelKey,template.getDisplayName(),template.getPathName().toString(),template.getDescription());
	    		String viewParams = template.getId().toString();
	    		
	        	OpenInTabCommand openInTabCommand = new OpenInTabCommand(viewName, viewParams, viewClass, iconThemeResource, tabTitle, tabTooltip);
	        	// In this case, we'll create the menu item under the reports menu item
	        	if ( firstNonReportMenuItem == null )
	        		reportsMenuItem.addItem(label, iconThemeResource, openInTabCommand);
	        	else
	        		reportsMenuItem.addItemBefore(label, iconThemeResource, openInTabCommand, firstNonReportMenuItem);

		        ++numReports;
			}
		}
	 	
		if ( numReports == 0 )
			throw new Exception("No reporting permission");
	}

	@SuppressWarnings("unchecked")
	void createLibrariesMenuItems() throws Exception {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		if ( librariesMenuItem.hasChildren() ) {
			librariesMenuItem.removeChildren();
		}
		
    	// Get the list of enabled libraries the user can ViewDetails on
    	Collection<Library> libraries = Library.Manager.getForUserWithPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS, INCLUDE.ONLY_ENABLED);
    	if ( libraries.size() == 0 )
    		throw new Exception("No permission");
		
    	String viewName = "LibraryProgramming";
		String icon = navigationSetup.getString(viewName+".icon");
		Class<EsfView> viewClass = null;
		
		String viewClassName = navigationSetup.getString(viewName+".class");
		try {
			Class<?> checkViewClass = Class.forName(viewClassName);
			if ( EsfView.class.isAssignableFrom(checkViewClass) ) {
				viewClass = (Class<EsfView>)checkViewClass;
			} else {
				_logger.error("createLibrariesMenuItems() - Class for view is not EsfView type: " + viewName + "; class: " + viewClassName);
			}
		} catch( ClassNotFoundException e ) {
			_logger.error("createLibrariesMenuItems() - Could not find view class for view name: " + viewName + "; view class: " + viewClassName);
		}
    	
    	ThemeResource iconThemeResource = EsfString.isNonBlank(icon) ? new ThemeResource(icon) : null;
		
		for( Library library : libraries ) {
			// If not enabled, skip it.
			if ( library.isDisabled() ) {
				continue;
			}
			
    		String tabTitle = navigationSetup.getString(viewName+".tabTitle",library.getPathName().toString());
    		String tabTooltip = navigationSetup.getString(viewName+".tabTooltip",library.getPathName().toString());
    		String label = navigationSetup.getString(librariesLabelKey,library.getPathName().toString(),library.getDescription());
    		String viewParams = library.getId().toString();
    		
        	OpenInTabCommand openInTabCommand = new OpenInTabCommand(viewName, viewParams, viewClass, iconThemeResource, tabTitle, tabTooltip);
        	// In this case, we'll create the menu item under the library menu item
        	librariesMenuItem.addItem(label, iconThemeResource, openInTabCommand);
		}
	}


	public class OpenInTabCommand implements MenuBar.Command, java.io.Serializable {
		private static final long serialVersionUID = -6037232023260269834L;

		private String viewName;
		private String viewParams;
		private Class<EsfView> viewClass;
		private ThemeResource icon;
		private String tabTitle;
		private String tabTooltip;
		
		public OpenInTabCommand(String viewName, String viewParams, Class<EsfView> viewClass, ThemeResource icon, String tabTitle, String tabTooltip) {
			this.viewName = viewName;
			this.viewParams = viewParams == null ? "" : viewParams;
			this.viewClass = viewClass;
			this.icon = icon;
			this.tabTitle = tabTitle;
			this.tabTooltip = tabTooltip;
		}
		
		public String getViewName() {
			return viewName;
		}

		public String getViewParams() {
			return viewParams;
		}
		public boolean hasViewParams() {
			return EsfString.isNonBlank(viewParams);
		}
		
		public Class<EsfView> getViewClass() {
			return viewClass;
		}
		public boolean hasViewClass() {
			return viewClass != null;
		}
		
		public ThemeResource getIcon() {
			return icon;
		}
		
		public String getTabTitle() {
			return tabTitle;
		}
		
		public String getTabTooltip() {
			return tabTooltip;
		}
		public boolean hasTabTooltip() {
			return EsfString.isNonBlank(tabTooltip);
		}
		
		@Override
		public void menuSelected(MenuItem selectedItem) {
			_logger.debug("OpenInTabCommand: selectedItem: " + selectedItem.toString() + "; view name: " + viewName + "; params: " + viewParams);
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			vaadinUi.getMainView().showViewInTab(this);
		}
	}
	
	
	public class OpenInBrowserWindowCommand implements MenuBar.Command, java.io.Serializable {
		private static final long serialVersionUID = -7966936906443764633L;
		private String url;
		
		public OpenInBrowserWindowCommand(String url) {
			this.url = url;
		}
		
		public String getUrl() {
			return url;
		}

		@Override
		public void menuSelected(MenuItem selectedItem) {
			_logger.debug("OpenInBrowserWindowCommand: selectedItem: " + selectedItem.toString() + "; URL: " + url);
			Page.getCurrent().open(url, "_blank", 800, 800, BorderStyle.DEFAULT);
		}
	}


	@Override
	public void modeChanged(Mode newMode) {
		try {
			if ( startTransactionMenuItem != null )
				createStartTransactionMenuItems();
		} catch( Exception e ) {
			// ignore if no transactions found, we'll just have no children listed to start
		}
		try {
			if ( reportsMenuItem != null )
				createReportsMenuItems();
		} catch( Exception e ) {
			// ignore if no reports found, we'll just have no children listed to start
		}
	}


	@Override
	public void libraryChanged(Library library) {
		if ( librariesMenuItem == null ) {
			return;
		}
		
		try {
			createLibrariesMenuItems();
		} catch( Exception e ) {
			// ignore if no libraries found, we'll just have no children listed to program
		}
	}
	
	@Override
	public void reportTemplateChanged(ReportTemplate report) {
		if ( reportsMenuItem == null ) {
			return;
		}
		
		try {
			createReportsMenuItems();
		} catch( Exception e ) {
			// ignore if no reports found, we'll just have no children listed to view
		}
	}

	@Override
	public void transactionTemplateChanged(TransactionTemplate tranTemplate) {
		if ( startTransactionMenuItem == null ) {
			return;
		}
		
		try {
			createStartTransactionMenuItems();
		} catch( Exception e ) {
			// ignore if no transactions found, we'll just have no children listed to start
		}
	}

	@Override
	public void detach() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.removeModeChangeListener(this);
		vaadinUi.removeLibraryChangeListener(this);
		vaadinUi.removeReportTemplateChangeListener(this);
		vaadinUi.removeTransactionTemplateChangeListener(this);
		super.detach();
	}
}