// Copyright (C) 2013-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.main;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.Version;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.user.User;
import com.esignforms.open.util.StringReplacement;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.MainMenu.OpenInTabCommand;
import com.esignforms.open.vaadin.main.MainTabSheet;
import com.vaadin.server.BrowserWindowOpener;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.WebBrowser;
import com.vaadin.server.WrappedSession;
import com.vaadin.shared.ui.BorderStyle;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.themes.Reindeer;

/**
 * This is the main view for the UI.  It includes all of the navigation, main tab sheet, etc. 
 * It also keeps track of opened tabs so they can be re-opened on restart.
 * It is based on the prior MainWindow class used under Vaadin 6.
 */
public class MainView extends CustomComponent {
	private static final long serialVersionUID = 5515397270024858692L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(MainView.class);
	
	//private static final String PROP_CURRENT_VIEW_NAME = "MainView/MainTabSheet/CurrentViewName";
	//private static final String PROP_CURRENT_VIEW_PARAMS = "MainView/MainTabSheet/CurrentViewParams";
	private static final String PROP_TAB_VIEW_NAMES = "MainView/MainTabSheet/ViewNames";
	private static final String PROP_TAB_VIEW_PARAMS = "MainView/MainTabSheet/ViewParams";
    
	class TabViewNameParams implements java.io.Serializable {
		private static final long serialVersionUID = -3173410025404289363L;

		TabViewNameParams(Tab t, String vn, String vp) { tab = t; viewName = vn; viewParams = vp == null ? "" : vp; }
		Tab tab;
		String viewName;
		String viewParams;
	}
	
	LinkedList<TabViewNameParams> tabToViewNameParamsList = new LinkedList<TabViewNameParams>();
	MainMenu mainMenu;
	MainTabSheet mainViewTabs;
	
	VerticalLayout layout;
	Label userNameLabel;
	Label currentLogin;
	Label prevLogin;
	Label lastServerCommunications;
	Button logoffButton;
	

	public MainView() {
		Page.getCurrent().setTitle(EsfVaadinUI.getInstance().getMsg("MainView.initialWindowTitle"));
		buildLayout();
	}
	
	protected void buildLayout() {
		_logger.debug("buildLayout()");

		setStyleName("MainView");
		setCurrentModeStyleName();
		setSizeFull();

		layout = new VerticalLayout();
        layout.setSizeFull();
        setCompositionRoot(layout);
		
        layout.addComponent(createHeader());
        
        mainMenu = new MainMenu(this);
        layout.addComponent(mainMenu);
        
        // Put the tabsheet into the right side
        mainViewTabs = new MainTabSheet();
        mainViewTabs.setSizeFull();
        mainViewTabs.setStyleName(Reindeer.TABSHEET_BORDERLESS);
        mainViewTabs.addStyleName(Reindeer.TABSHEET_HOVER_CLOSABLE);
        mainViewTabs.addStyleName(Reindeer.TABSHEET_SELECTED_CLOSABLE);
        layout.addComponent(mainViewTabs);
        layout.setExpandRatio(mainViewTabs, 1.0f);
        
        layout.addComponent(createFooter());
	}
	
	public void saveView() {
        EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
        if ( vaadinUi == null ) {
        	_logger.debug("saveView() - Could not get EsfVaadinUI instance"); // likely happens when session expired
        	return;
        }
        
        // Save open tabs so we can restore on next login
        WebBrowser browser = vaadinUi.getPage().getWebBrowser();
		User user = vaadinUi.getUser();
		if ( user != null && ! user.getEmail().equalsIgnoreCase("demo@yozons.com") && ! browser.isAndroid() && ! browser.isIOS() ) {     			
			synchronized(tabToViewNameParamsList) {
    			List<Tab> tabList = mainViewTabs.getTabList();
    			EsfString[] tabViewNames = new EsfString[tabList.size()];
    			EsfString[] tabViewParams = new EsfString[tabList.size()];
    			int i = 0;
    			for( Tab tab : tabList ) {
    				TabViewNameParams tvnp = getTabViewNameParams(tab);
    				if ( tvnp != null ) {
        				tabViewNames[i] = new EsfString(tvnp.viewName);
        				tabViewParams[i] = new EsfString(tvnp.viewParams);
        				++i;
    				}
    			}
    			user.setUserDefinedStringsProperty(PROP_TAB_VIEW_NAMES, tabViewNames);
    			user.setUserDefinedStringsProperty(PROP_TAB_VIEW_PARAMS, tabViewParams);
    			
    			/* We're currently not going to reopen the last opened tab because we want the welcome tab
    			 * to always be displayed on login.
    			TabViewNameParams tvnp = getTabViewNameParams(mainViewTabs.getCurrentSelectedTab());
    			if ( tvnp != null ) {
        			user.setUserDefinedStringProperty(PROP_CURRENT_VIEW_NAME, new EsfString(tvnp.viewName));
        			user.setUserDefinedStringProperty(PROP_CURRENT_VIEW_PARAMS, new EsfString(tvnp.viewParams));
    			}
    			*/
			}
		}
	}
	
	public void restoreView() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
        //boolean tabsRestored = false;
		WebBrowser browser = vaadinUi.getPage().getWebBrowser();
		User user = vaadinUi.getUser();
		if ( user != null && ! user.getEmail().equalsIgnoreCase("demo@yozons.com") && ! browser.isAndroid() && ! browser.isIOS() ) {
			
			EsfString[] viewNames = user.getUserDefinedStringsProperty(PROP_TAB_VIEW_NAMES);
			EsfString[] viewParams = user.getUserDefinedStringsProperty(PROP_TAB_VIEW_PARAMS);
			
			// No longer restore the last opened tab, but instead we'll always display the the welcome tab.
			//EsfString currentViewName = user.getUserDefinedStringProperty(PROP_CURRENT_VIEW_NAME);
			//EsfString currentViewParam = user.getUserDefinedStringProperty(PROP_CURRENT_VIEW_PARAMS);
			//EsfView currentView = null;
			
			if ( viewNames != null && viewParams != null && viewNames.length == viewParams.length ) {
				for( int i=0; i < viewNames.length; ++i ) {
					if ( viewParams[i] == null )
						viewParams[i] = new EsfString("");
					if ( viewNames[i] != null ) {
						OpenInTabCommand viewOpenInTabCommand = mainMenu.getOpenInTabCommand(viewNames[i].toString(),viewParams[i].toString());
						if ( viewOpenInTabCommand != null ) {
							showViewInTab(viewOpenInTabCommand);
							//EsfView toView = showViewInTab(viewOpenInTabCommand);
							//tabsRestored = true;
							//if ( viewNames[i].equals(currentViewName) && viewParams[i].equals(currentViewParam) ) {
							//	currentView = toView;
							//}
						}
					}
				}
			}
			
			//if ( currentView != null )
			//	mainViewTabs.setSelectedTab(currentView);
		}

		//if ( ! tabsRestored ) {
	        OpenInTabCommand defaultViewOpenInTabCommand = mainMenu.getDefaultOpenInTabCommand();
	        if ( defaultViewOpenInTabCommand != null ) {
	        	showViewInTab(defaultViewOpenInTabCommand);
	        }
		//}
        
        updateMainViewInfo();
	}
	
	public void setTabTitle(EsfView view, String newTabTitle) {
		Tab tab = mainViewTabs.getTab(view);
		if ( tab != null ) {
			tab.setCaption(newTabTitle);
		}
	}
	
	public void setCurrentModeStyleName(AbstractComponent component) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		component.removeStyleName("esf-production-mode");
		component.removeStyleName("esf-test-like-production-mode");
		component.removeStyleName("esf-test-mode");
		if ( vaadinUi.isTestLikeProductionMode() )
			component.addStyleName("esf-test-like-production-mode");
		else if ( vaadinUi.isTestMode() )
			component.addStyleName("esf-test-mode");
		else
			component.addStyleName("esf-production-mode");
	}
	public void setCurrentModeStyleName() {
		setCurrentModeStyleName(this);
	}
	public void setSearchCurrentModeStyleName(AbstractComponent component) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		component.removeStyleName("esf-search-production-mode");
		component.removeStyleName("esf-search-test-mode");
		if ( vaadinUi.isProductionMode() )
			component.addStyleName("esf-search-production-mode");
		else
			component.addStyleName("esf-search-test-mode");
	}
	
    protected HorizontalLayout createHeader() {
        EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
        
    	HorizontalLayout lo = new HorizontalLayout();
        lo.setWidth(100, Unit.PERCENTAGE);
        lo.setStyleName("header");

        com.esignforms.open.prog.Image logoForApp = com.esignforms.open.prog.Image.Manager.getByName(Library.Manager.getTemplate().getId(), vaadinUi.getEsfapp().getImageDefaultLogoForAppEsfName());
        ImageVersion logoVersionForApp = logoForApp == null ? null : logoForApp.getTestImageVersion();
        
        Image headerLogo = new Image();
        headerLogo.setStyleName("logo");
        headerLogo.setAlternateText("logo");
        headerLogo.setDescription(vaadinUi.getMsg("MainView.logo.tooltip"));
        if ( logoVersionForApp != null ) 
        	headerLogo.setSource(new com.vaadin.server.ExternalResource(logoVersionForApp.getImageByIdUrl(),logoVersionForApp.getImageMimeType()));
    	lo.addComponent(headerLogo);
    	lo.setComponentAlignment(headerLogo, Alignment.TOP_LEFT);
    	lo.setExpandRatio(headerLogo,0.2f);
    	
    	BrowserWindowOpener logoOpenNewWindow = new BrowserWindowOpener(vaadinUi.getRequestContextPath()+"/ui/");
    	logoOpenNewWindow.setWindowName("_blank");
    	logoOpenNewWindow.setFeatures("menubar=no,location=no,status=no,resizable=yes,scrollbars=yes");
    	logoOpenNewWindow.extend(headerLogo);
    	
    	VerticalLayout nameLayout = new VerticalLayout();
    	lo.addComponent(nameLayout);
    	lo.setExpandRatio(nameLayout, 0.6f);
    	lo.setComponentAlignment(nameLayout, Alignment.MIDDLE_CENTER);
    	
    	String deploymentNameHtml = StringReplacement.replace("${htmlproperty:ESF.AppTitleInHtml}");
    	Label deploymentNameLabel = new Label(deploymentNameHtml);
    	deploymentNameLabel.setContentMode(ContentMode.HTML);
    	nameLayout.addComponent(deploymentNameLabel);
    	
    	userNameLabel = new Label();
    	userNameLabel.addStyleName("userName");
    	userNameLabel.setContentMode(ContentMode.TEXT);
    	nameLayout.addComponent(userNameLabel);
    	
    	logoffButton = new Button(vaadinUi.getMsg("MainView.logoffButtonLabel"));
    	logoffButton.setIcon( new ThemeResource(vaadinUi.getMsg("MainView.logoffButtonIcon")) );
    	logoffButton.setStyleName(BaseTheme.BUTTON_LINK);
    	logoffButton.addStyleName("logoffButton");
    	logoffButton.setDescription(vaadinUi.getMsg("MainView.logoffButtonTitle"));
    	logoffButton.addClickListener( new Button.ClickListener() {
			private static final long serialVersionUID = 4614266426926071069L;

			@Override
			public void buttonClick(final ClickEvent event) {
                EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				vaadinUi.logoffUser();
			}
		}); 
        lo.addComponent(logoffButton);
    	lo.setComponentAlignment(logoffButton, Alignment.MIDDLE_RIGHT);
    	lo.setExpandRatio(logoffButton, 0.2f);
    	
        return lo;
    }
    
    protected VerticalLayout createFooter() {
        EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	_logger.debug("createFooter()");
    	VerticalLayout lo = new VerticalLayout();
    	lo.setStyleName("footer");
        lo.setWidth(100, Unit.PERCENTAGE);
    	lo.setMargin(false);
    	lo.setSpacing(false);
        
    	HorizontalLayout line1 = new HorizontalLayout();
    	line1.setWidth(100, Unit.PERCENTAGE);
    	line1.setMargin(false);
    	line1.setSpacing(false);
    	lo.addComponent(line1);
    	
    	HorizontalLayout line2 = new HorizontalLayout();
    	line2.setWidth(100, Unit.PERCENTAGE);
    	line2.setMargin(false);
    	line2.setSpacing(false);
    	lo.addComponent(line2);
    	
        currentLogin = new Label();
        currentLogin.setSizeUndefined();
        currentLogin.setStyleName("padLeft5p");
        line1.addComponent(currentLogin);
        line1.setComponentAlignment(currentLogin, Alignment.MIDDLE_LEFT);
        
        prevLogin = new Label();
        prevLogin.setSizeUndefined();
        prevLogin.setStyleName("padLeft5p");
        line2.addComponent(prevLogin);
        line2.setComponentAlignment(prevLogin, Alignment.MIDDLE_LEFT);

        HorizontalLayout lastServerCommunicationsInfo = new HorizontalLayout();
        lastServerCommunicationsInfo.setMargin(false);
        lastServerCommunicationsInfo.setSpacing(false);
        lastServerCommunicationsInfo.setSizeUndefined();
        Label lastServerCommunicationsCaption = new Label(vaadinUi.getMsg("MainView.lastSentToServerLabel"));
        lastServerCommunicationsCaption.setWidth(-1, Unit.PIXELS);
        lastServerCommunications = new Label();
        lastServerCommunications.setSizeUndefined();
        lastServerCommunications.setStyleName("padLeft5p");
        lastServerCommunicationsInfo.addComponent(lastServerCommunicationsCaption);
        lastServerCommunicationsInfo.addComponent(lastServerCommunications);
        line1.addComponent(lastServerCommunicationsInfo);
        line1.setComponentAlignment(lastServerCommunicationsInfo, Alignment.MIDDLE_CENTER);

    	Label copyrightLabel = new Label(Version.getHtmlCopyright());
    	copyrightLabel.setSizeUndefined();
    	copyrightLabel.setStyleName("copyrightVersionInfo");
    	copyrightLabel.setContentMode(ContentMode.HTML);
    	line1.addComponent(copyrightLabel);
    	line1.setComponentAlignment(copyrightLabel, Alignment.MIDDLE_RIGHT);
    	
    	Label versionLabel = new Label("<a href=\"../versionHistory.jsp\" target=\"blank\">"+Version.getReleaseString()+"</a>");
    	versionLabel.setSizeUndefined();
    	versionLabel.setContentMode(ContentMode.HTML);
    	versionLabel.setStyleName("copyrightVersionInfo");
    	line2.addComponent(versionLabel);
    	line2.setComponentAlignment(versionLabel, Alignment.MIDDLE_RIGHT);
    	
    	return lo;
    }
    
    public void updateMainViewInfo()
    {
        EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	User user = vaadinUi.getLoggedInUser();
    	
    	if ( user != null ) {
        	_logger.debug("updateMainViewInfo() - logged in user: " + user.getFullDisplayName());
        	mainMenu.setEnabled(true);
        	mainViewTabs.setEnabled(true);
    		userNameLabel.setValue(user.getFullDisplayName());
    		currentLogin.setValue(vaadinUi.getMsg("MainView.currLoginLabel",user.getLoginTimestamp().toString(user),user.getLoginIP()));
    		if ( user.hasLastLoginTimestamp() ) {
    			prevLogin.setValue(vaadinUi.getMsg("MainView.prevLoginLabel",user.getLastLoginTimestamp().toString(user),user.getLastLoginIP()));
    		} else {
    			prevLogin.setValue(vaadinUi.getMsg("MainView.prevLoginLabel.firstlogin"));
    		}
    		updateLastServerCommunications(vaadinUi);
        	logoffButton.setEnabled(true);
    	} else  {
        	_logger.debug("updateMainViewInfo() - user is not logged in");
        	mainMenu.setEnabled(false);
        	mainViewTabs.setEnabled(false);
    		userNameLabel.setValue(vaadinUi.getMsg("MainView.userNameNotLoggedIn"));
    		currentLogin.setValue("");
    		prevLogin.setValue("");
    		lastServerCommunications.setValue("");
        	logoffButton.setEnabled(true);
    	}
    }
    
    public void updateLastServerCommunications(EsfVaadinUI vaadinUi)
    {
    	User user = vaadinUi.getUser();
    	
    	if ( user != null ) {
    		lastServerCommunications.setValue((new EsfDateTime().toLogTimeString(user)));
    	} else {
    		lastServerCommunications.setValue((new EsfDateTime().toLogTimeString()));
    	}
    }

    public boolean isDirty() {
    	EsfView currentView = (EsfView)mainViewTabs.getSelectedTab();
    	return currentView != null && currentView.isDirty();
    }
    
    public String checkDirty() {
        EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	EsfView currentView = (EsfView)mainViewTabs.getSelectedTab();
    	return currentView == null ? vaadinUi.getMsg("ConfirmDiscardChangesDialog.defaultMessage") : currentView.checkDirty();
    }
    
    public int getNumberOfTabs() {
    	return mainViewTabs.getComponentCount();
    }
    
    public void showViewInTab(MainMenu.OpenInTabCommand openInTabCommand) {
    	_logger.debug("showViewInTab() - viewName: " + openInTabCommand.getViewName());
		EsfVaadinUI.getInstance().ensureLoggedIn();
    	
    	// If we have it in a tab, show that tab and be done with it.
    	TabViewNameParams tvnp = getTabViewNameParams(openInTabCommand.getViewName(), openInTabCommand.getViewParams());
    	if ( tvnp != null ) {
    		mainViewTabs.setSelectedTab(tvnp.tab);
    		//mainViewTabs.setTabPosition(viewTab, 0);
    		mainViewTabs.focus();
    		return;
    	}
    	
        EsfView toView = createView(openInTabCommand);

        Tab newTab = mainViewTabs.addTab(toView);
        tvnp = new TabViewNameParams(newTab, openInTabCommand.getViewName(), openInTabCommand.getViewParams());
        synchronized(tabToViewNameParamsList) {
        	tabToViewNameParamsList.add(tvnp); // keep track of all opened tabs
        }
        mainViewTabs.setSelectedTab(newTab);
		newTab.setCaption(openInTabCommand.getTabTitle());
		ThemeResource icon = openInTabCommand.getIcon();
		newTab.setIcon( icon ); // even if null, want to clear any that may be there 
		if ( openInTabCommand.hasTabTooltip() ) {
			newTab.setDescription(openInTabCommand.getTabTooltip());
		}
		mainViewTabs.setTabState();
        mainViewTabs.focus();
        toView.activateView(EsfView.OpenMode.WINDOW, openInTabCommand.getViewParams());
		
		// If we have 15+ tabs open, notify the user once per login.
		if ( getNumberOfTabs() >= 15 ) {
			WrappedSession wrappedSession = VaadinSession.getCurrent().getSession();
			Object notificationDone = wrappedSession.getAttribute("MainView_manyTabs_notified");
			if ( notificationDone == null ) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				vaadinUi.showWarning(null, vaadinUi.getMsg("MainView.warning.manyTabs",15));
				wrappedSession.setAttribute("MainView_manyTabs_notified", new EsfDateTime());
			}
		}
    }
    
    void tabClosed(Tab tab) {
    	if ( tab != null ) {
            synchronized(tabToViewNameParamsList) {
            	Iterator<TabViewNameParams> iter = tabToViewNameParamsList.iterator();
            	while( iter.hasNext() ) {
            		TabViewNameParams tvnp = iter.next();
            		if ( tvnp.tab.equals(tab) ) {
            			iter.remove();
            			break;
            		}
            	}
            }
    	}
    }
    
	TabViewNameParams getTabViewNameParams(String vn, String vp) {
		synchronized(tabToViewNameParamsList) {
        	Iterator<TabViewNameParams> iter = tabToViewNameParamsList.iterator();
        	while( iter.hasNext() ) {
        		TabViewNameParams tvnp = iter.next();
        		if ( tvnp.viewName.equals(vn) && tvnp.viewParams.equals(vp) ) {
        			return tvnp;
        		}
        	}
		}
		return null;
	}
	

    
    TabViewNameParams getTabViewNameParams(Tab tab) {
		synchronized(tabToViewNameParamsList) {
			for( TabViewNameParams tvnp : tabToViewNameParamsList ) {
				if ( tvnp.tab.equals(tab) )
					return tvnp;
			}
		}
		return null;
    }

    public void startTransactionInBrowserWindow(EsfUUID tranTemplateId, boolean isProduction, boolean isLikeProduction) {
        EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();
    	_logger.debug("startTransactionInBrowserWindow() - tranTemplateId: " + tranTemplateId + "; isProduction: " + isProduction +
    			"; isLikeProduction: " + isLikeProduction);
    	
    	TransactionTemplate transactionTemplate = TransactionTemplate.Manager.getByIdForStart(tranTemplateId, vaadinUi.getUser());
    	if ( transactionTemplate == null ) {
    		vaadinUi.showError(vaadinUi.getMsg("MainView.startTransaction.error.caption"),vaadinUi.getMsg("MainView.startTransaction.unknownTransactionTemplate"));
    		return;
    	} else {
    		if ( transactionTemplate.isProductionDisabled() && isProduction ) {
        		vaadinUi.showWarning(vaadinUi.getMsg("MainView.startTransaction.error.caption"),vaadinUi.getMsg("MainView.startTransaction.productionIsDisabled")); 
        		return;
    		}
    		if ( transactionTemplate.isTestDisabled() && ! isProduction ) {
        		vaadinUi.showWarning(vaadinUi.getMsg("MainView.startTransaction.error.caption"),vaadinUi.getMsg("MainView.startTransaction.testIsDisabled")); 
        		return;
    		}
    	}
    	
    	Page.getCurrent().open(transactionTemplate.getStartUrl(isProduction,isLikeProduction), "_blank", 800, 800, BorderStyle.DEFAULT);
    }

    
	public EsfView createView(MainMenu.OpenInTabCommand openInTabCommand) {
		EsfView toView = null;
		
		try {
			toView = openInTabCommand.getViewClass().newInstance();
			toView.initView();
		} catch( InstantiationException e ) {
			_logger.error("createView() - could not instantiate instance of class: " + openInTabCommand.getViewClass().getName());
		} catch( IllegalAccessException e ) {
			_logger.error("createView() - illegal access: could not instantiate instance of class: " + openInTabCommand.getViewClass().getName());
		}
		
		return toView;
	}
	
}
