// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.main;

import com.esignforms.open.data.EsfHtml;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.Record;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

public class TipsView extends Panel implements EsfView {
	private static final long serialVersionUID = -7648878236230999891L;

	String tipsPrefix;
	
	Label tipsAreaHtml;
	
	public TipsView() {
		setIcon(null);
        setStyleName("TipsView");
        setSizeFull();
        setStyleName(Reindeer.PANEL_LIGHT);
    }
    
	protected void buildLayout() {
        tipsAreaHtml = new Label();
        tipsAreaHtml.setWidth(98, Unit.PERCENTAGE);
        tipsAreaHtml.addStyleName("tipsArea");
        tipsAreaHtml.setContentMode(ContentMode.HTML);
    	
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
        layout.setMargin(true);
        layout.setSpacing(false);
    	layout.addComponent(tipsAreaHtml);
	}

	@Override
	public void activateView(OpenMode mode, String params) {
        EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		String tipsPrefix = EsfString.isBlank(params) ? "" : params;
        Record deploymentProperties = Record.Manager.getById(vaadinUi.getEsfapp().getDeploymentPropertiesId());
        EsfHtml html = deploymentProperties.getHtmlByName(vaadinUi.getEsfapp().getTipsHtmlEsfName(tipsPrefix));
        String showHtml = ( html == null ) ? vaadinUi.getMsg(tipsPrefix+"TipsView.tipsAreaHtml.default") : html.toPlainString();
    	tipsAreaHtml.setValue(showHtml);
	}

	@Override
	public void initView() {
    	buildLayout();
	}
	
	@Override
	public String checkDirty() {
		return null;
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
}