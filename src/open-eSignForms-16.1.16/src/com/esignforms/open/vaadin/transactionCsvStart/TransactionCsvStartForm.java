// Copyright (C) 2014-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.transactionCsvStart;

import java.io.BufferedReader;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;

import javax.servlet.http.HttpSession;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.servlet.StartTransaction;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.util.CSVParser;
import com.esignforms.open.util.ServletUtil;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.widget.UploadFileWithProgress;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.VerticalLayout;

/**
 * The TransactionCsvStartForm allows a user to select a transaction type and then upload a CSV file that is used to
 * start a transaction for each row in the CSV file.  The first line of the CSV file MUST BE the field names for each
 * column, with each subsequent line being a row of data.  The data is essentially sent as name-value pairs to the
 * transaction's start URL.  Blank lines and comment lines after the first line are skipped.  Comment lines start with: //##
 * Also, if any line starts with two commas (,,), we consider that to be an artificial EOF
 * 
 * CSV File layout example:
 * Field1,Field2,Field3
 * value11,value12,value13
 * 
 * value21,value22,value23
 * //## this is a comment line
 * value31,value32,value33
 * 
 * This will create 3 transactions using initial POST data (the blank line and comment line from above are ignored):
 *   Transaction 1 is started with params: Field1=value11&Field2=value12&Field3=value13
 *   Transaction 2 is started with params: Field1=value21&Field2=value22&Field3=value23
 *   Transaction 3 is started with params: Field1=value31&Field2=value32&Field3=value33
 * 
 * @author Yozons Inc.
 */
public class TransactionCsvStartForm extends CustomComponent implements EsfView, ClickListener {
	private static final long serialVersionUID = -3411038151954615415L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TransactionCsvStartForm.class);
	
	enum SHOW_VIEW { UPLOAD_CSV, START_TRANSACTIONS };
	SHOW_VIEW currentView;
	
	final TransactionCsvStartForm thisForm;
	VerticalLayout layout;
	
	// UPLOAD_CSV view fields
	class DataLineInfo {
		public DataLineInfo(int ln, String data, String[] parsed) {
			lineNumber = ln;
			dataLine = data;
			parsedDataLine = parsed;
		}
		public int lineNumber;
		public String dataLine;
		public String[] parsedDataLine;
	}
	
	CheckBox autoCompleteFirstParty;
	NativeSelect transactionTemplateSelect;
	TransactionTemplate selectedTransactionTemplate;
	UploadFileWithProgress uploadFile;
	String headerLine;
	String[] parsedHeaderLine;
	LinkedList<DataLineInfo> dataLineInfoList = new LinkedList<DataLineInfo>();
	int numCsvFields;
	
	// START_TRANSACTIONS view fields
	HorizontalLayout startTransactionsButtonLayout;
	int numStartedOkay;
	int numStartedError;
	
    public TransactionCsvStartForm() {
    	setStyleName("TransactionCsvStartForm");
    	this.thisForm = this;
    }
    
    void resetCaches() {
    	dataLineInfoList.clear();
    	selectedTransactionTemplate = null;
    	headerLine = null;
    	parsedHeaderLine = null;
    }
    
    void showCorrectView() {
    	layout.removeAllComponents();
    	
    	if ( currentView == SHOW_VIEW.UPLOAD_CSV )
    		setupUploadCsvView();
    	else 
    		setupStartTransactionsView();
    }
    
    void setupUploadCsvView() {
		final EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();

    	layout.addComponent(new Label(vaadinUi.getMsg("TransactionCsvStartForm.uploadCsv.headingHtml.label"), ContentMode.HTML));
    	
    	autoCompleteFirstParty = new CheckBox(vaadinUi.getMsg("TransactionCsvStartForm.autoCompleteFirstParty.checkbox.label"));
    	autoCompleteFirstParty.setValue(true);
    	layout.addComponent(autoCompleteFirstParty);
    	
    	transactionTemplateSelect = new NativeSelect(vaadinUi.getMsg("TransactionCsvStartForm.transactionTemplate.select.label"));
    	transactionTemplateSelect.setNullSelectionAllowed(false);
    	transactionTemplateSelect.setRequired(true);
    	transactionTemplateSelect.setImmediate(true);
    	transactionTemplateSelect.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = -9093178190533703365L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				EsfUUID id = (EsfUUID)transactionTemplateSelect.getValue();
				if ( id != null && ! id.isNull() ) {
					selectedTransactionTemplate = TransactionTemplate.Manager.getByIdForStart(id, vaadinUi.getUser());
				} else {
					selectedTransactionTemplate = null;
				}
				if ( uploadFile != null ) {
					uploadFile.setVisible(selectedTransactionTemplate != null);
				}
			}
    	});
    	transactionTemplateSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);

		TransactionTemplate templateTransactionTemplate = TransactionTemplate.Manager.getTemplate();
		List<TransactionTemplate> allTransactionTemplates = TransactionTemplate.Manager.getForUserWithPermission(vaadinUi.getUser(), PermissionOption.TRAN_PERM_OPTION_START);
    	if ( allTransactionTemplates.size() > 1 ) {
			// Let's show these in display name order
			Collections.sort(allTransactionTemplates, TransactionTemplate.Manager.ComparatorByDisplayName);
    	}
		
    	for( TransactionTemplate transactionTemplate : allTransactionTemplates ) {
			// If not enabled for either test or production, skip it.
			if ( transactionTemplate.isProductionDisabled() && transactionTemplate.isTestDisabled() ) {
				continue;
	    	}
			// Can't start the Template transaction template
    		if ( templateTransactionTemplate.equals(transactionTemplate) ) {
    			continue;
    		}
			if ( (vaadinUi.isProductionMode() && transactionTemplate.isProductionEnabled()) || 
				 (vaadinUi.isTestLikeProductionMode() && transactionTemplate.isProductionEnabled() && transactionTemplate.isTestEnabled()) ||
				 (vaadinUi.isTestMode() && transactionTemplate.isTestEnabled())
			   ) {
				String label = vaadinUi.getMsg("TransactionCsvStartForm.transactionTemplate.select.options.label",transactionTemplate.getDisplayName(),transactionTemplate.getPathName().toString(),transactionTemplate.getDescription());

				transactionTemplateSelect.addItem(transactionTemplate.getId());
				transactionTemplateSelect.setItemCaption( transactionTemplate.getId(), label );
			}
    	}

    	layout.addComponent(transactionTemplateSelect);
    	
		uploadFile = new UploadFileWithProgress(null,vaadinUi.getMsg("TransactionCsvStartForm.button.upload.label"),(int)vaadinUi.getEsfapp().getUploadFileMaxBytes()) {
			private static final long serialVersionUID = -3883824068355883974L;

			@Override
			public void afterUploadSucceeded() {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				String csvData = EsfString.bytesToString(uploadFile.getFileData());
				Errors errors = processUploadedCsv(csvData);
				if ( errors.hasError() ) {
					vaadinUi.show(errors);
				} else {
	            	_logger.debug("setupUploadCsvView() uploadFile.afterUploadSucceeded() - Uploaded CSV file: " + uploadFile.getFileName() + "; # CSV lines: " + dataLineInfoList.size());
					vaadinUi.showStatus(vaadinUi.getMsg("TransactionCsvStartForm.upload.successful",uploadFile.getFileName(),EsfInteger.byteSizeInUnits(uploadFile.getFileData().length) ));
					currentView = SHOW_VIEW.START_TRANSACTIONS;
					showCorrectView();
				}
			}
			@Override
			public void afterUploadFailed(String fileName, boolean wasCanceled, boolean wasInvalidMimeType, boolean wasTooBig, int contentLength, int maxSize) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				if ( wasTooBig ) {
					vaadinUi.showError( 
							vaadinUi.getMsg("TransactionCsvStartForm.fileTooLarge.caption"), 
							vaadinUi.getMsg("TransactionCsvStartForm.fileTooLarge.message",EsfInteger.byteSizeInUnits(contentLength),EsfInteger.byteSizeInUnits(maxSize))
							);
				} else if ( wasCanceled ) {
					vaadinUi.showStatus(vaadinUi.getMsg("TransactionCsvStartForm.upload.canceled"));
				} else if ( wasInvalidMimeType ) {
					vaadinUi.showError(
							vaadinUi.getMsg("TransactionCsvStartForm.invalidType.caption"), 
							vaadinUi.getMsg("TransactionCsvStartForm.invalidType.message",EsfString.isBlank(fileName)?"(unknown)":fileName)
							);
				}
			}
		}; 
		uploadFile.setAllowedMimeTypesForCsv();
		uploadFile.setVisible(false);
		layout.addComponent(uploadFile);
    }
    
    void setupStartTransactionsView() {
		final EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();
		
    	layout.addComponent(new Label(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.headingHtml.label"), ContentMode.HTML));
    	layout.addComponent(new Label(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.fileInfo.label",uploadFile.getFileName(),dataLineInfoList.size())));
    	layout.addComponent(new Label(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.header.label",parsedHeaderLine.length,headerLine)));
    	layout.addComponent(new Label(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.autoCompleteFirstPartyInfo.label",autoCompleteFirstParty.getValue())));
    	layout.addComponent(new Label(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.tranInfo.label",selectedTransactionTemplate.getPathName(),selectedTransactionTemplate.getId())));

    	startTransactionsButtonLayout = new HorizontalLayout();
    	startTransactionsButtonLayout.setSpacing(true);
    	layout.addComponent(startTransactionsButtonLayout);
    	
    	if ( dataLineInfoList.size() > 0 ) {
        	Button startButton = new Button(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.button.start.label",dataLineInfoList.size()));
        	startButton.setDisableOnClick(true);
        	startButton.addClickListener( new ClickListener() {
    			private static final long serialVersionUID = 4403469888173983670L;

    			@Override
    			public void buttonClick(ClickEvent event) {
    				startTransactions();
    			}
        	});
        	startTransactionsButtonLayout.addComponent(startButton);   		
    	}

    	Button cancelButton = new Button(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.button.cancel.label"));
    	cancelButton.setDisableOnClick(true);
    	cancelButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = 8514154694438226798L;

			@Override
			public void buttonClick(ClickEvent event) {
				vaadinUi.showStatus(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.canceled.label"));
				currentView = SHOW_VIEW.UPLOAD_CSV;
				showCorrectView();
			}
    	});
    	startTransactionsButtonLayout.addComponent(cancelButton);
    }
    
    String getJsessionId(EsfVaadinUI vaadinUi) {
    	HttpSession sess = vaadinUi.getHttpSession();
    	if ( sess != null ) {
        	try { return sess.getId(); } catch( Exception e ) {}
    	}
    	return null;
    }
    
    void startTransactions() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();

		numStartedOkay = 0;
    	numStartedError = 0;

    	String externalContextPath = vaadinUi.getRequestExternalContextPath();
    	String jsessionid = getJsessionId(vaadinUi);
    	
    	boolean forProduction, forTest;
    	if ( vaadinUi.isProductionMode() ) {
    		forProduction = true;
    		forTest = false;
    	} else if ( vaadinUi.isTestLikeProductionMode() ) {
    		forProduction = true;
    		forTest = true;
    	} else {
    		forProduction = false;
    		forTest = true;
    	}
    	
    	String startUrl = selectedTransactionTemplate.getExternalStartUrl(externalContextPath, forProduction, forTest, jsessionid);
    	if ( autoCompleteFirstParty.getValue() ) {
    		startUrl = ServletUtil.appendUrlParam(startUrl, StartTransaction.PARAM_ESFAUTOCOMPLETEFIRSTPARTY, "Yes");
    	}
    	
    	Errors errors = new Errors();
    	
    	for( DataLineInfo dli : dataLineInfoList ) {
    		if ( startTransaction(errors,startUrl,vaadinUi.getUser(),uploadFile.getFileName(),dli.lineNumber,headerLine,parsedHeaderLine,dli.dataLine,dli.parsedDataLine) )
    			++numStartedOkay;
    		else
    			++numStartedError;
    	}
    	
    	if ( errors.hasError() ) {
    		if ( numStartedOkay == 0 )
    			layout.addComponent(new Label(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.unsuccessfulHtml.label",numStartedError),ContentMode.HTML));
    		else
    			layout.addComponent(new Label(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.mixedSuccessfulHtml.label",numStartedOkay,numStartedError),ContentMode.HTML));
    		
    		Label errorLabel = new Label();
    		errorLabel.setStyleName("errorMessageLabel");
    		errorLabel.setContentMode(ContentMode.HTML);
    		errorLabel.setValue(errors.toHtml());
    		errorLabel.setWidth(98, Unit.PERCENTAGE);
    		errorLabel.setHeight(20, Unit.EM);
			layout.addComponent(errorLabel);
    	} else {
        	layout.addComponent(new Label(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.successHtml.label",numStartedOkay),ContentMode.HTML));
    	}
    	
    	startTransactionsButtonLayout.setVisible(false);
    	resetCaches();
    }
    
    boolean startTransaction(Errors errors, String url, User startedBy, String csvFileName, int lineNumber, String headerCsvLine, String[] fieldNames, String csvLine, String[] fields ) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
		java.net.HttpURLConnection httpCon = null;
		java.io.BufferedReader     br  = null;
		java.io.InputStream        is = null;
		java.io.OutputStream       os = null;

		String postParams = createPostParams(startedBy,csvFileName,lineNumber,headerCsvLine,fieldNames,csvLine,fields);
		
		try {
			java.net.URL httpUrl = null;
	        try {
	        	httpUrl = new java.net.URL( url );
	        } catch( Exception e ) {
	        	_logger.error("startTransaction() - Invalid start URL: " + url,e);
	        	errors.addError(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.error.invalidStartUrl",url,lineNumber,csvFileName,csvLine));
	        	return false;
	        }
	        
			// Create the connection and set the connection properties
			httpCon = (java.net.HttpURLConnection)httpUrl.openConnection();
			httpCon.setRequestMethod("POST");
			httpCon.setAllowUserInteraction(false);  // this is program, so no popups
			httpCon.setDoInput(true);      // we will read from the URL
			httpCon.setDoOutput(true);     // we will write to the URL
			httpCon.setIfModifiedSince(0); // always get
			httpCon.setUseCaches(false);   // don't use cache
			
			byte[] postDataBytes = EsfString.stringToBytes(postParams);
			int contentLength = 0;
	        contentLength = postDataBytes.length;
			httpCon.setRequestProperty("Content-Length", String.valueOf(contentLength));
			httpCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
			httpCon.setRequestProperty("Accept", "*/*");
			httpCon.setRequestProperty("HTTP_USER_AGENT", "Yozons-Open-eSignForms-CSV-Upload");
			String rua = vaadinUi.getRequestUserAgent();
			if ( EsfString.isNonBlank(rua) ) {
				httpCon.setRequestProperty("user-agent", rua);
			}
			String rr = vaadinUi.getRequestReferer();
			if ( EsfString.isNonBlank(rr) ) {
				httpCon.setRequestProperty("Referer", rr);
			}
			
			httpCon.connect();
	
			// Construct the POST message
			os = httpCon.getOutputStream();
			os.write( postDataBytes );
			os.close(); os = null;
			
			is = httpCon.getInputStream();
			if ( is == null ) {
	        	_logger.error("startTransaction() - Could not getInputStream() from URL: " + url);
	        	errors.addError(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.error.noInputStream",url,lineNumber,csvFileName,csvLine));
	        	return false;
			}
	
			contentLength = httpCon.getContentLength();
			String contentType = httpCon.getContentType();
			if ( EsfString.isBlank(contentType) )
				contentType = Application.CONTENT_TYPE_TEXT;
			int httpResponseCode = httpCon.getResponseCode();
			String httpResponseMessage = httpCon.getResponseMessage();
			
			if ( contentLength < 1 )
				contentLength = 4 * Literals.KB;
	
			StringBuilder responseBuf = new StringBuilder(contentLength+32);
			
			br = new java.io.BufferedReader( new java.io.InputStreamReader(is));
			String responseString;
			while ( (responseString = br.readLine()) != null )
			{
				if ( EsfString.isBlank(responseString) )
					continue;
				responseBuf.append(responseString).append('\n');
			}
			br.close(); br = null;
	
			responseString = responseBuf.toString();
			
			if ( httpResponseCode != 200 ) {
	        	_logger.error("startTransaction() - Non-200 HTTP Status from URL: " + url + 
	        			"; httpResponseCode: " + httpResponseCode + "; httpResponseMessage: " + httpResponseMessage + 
	        			"; line #" + lineNumber + " in CSV file '" + csvFileName + "'" +
	        			"; csvLine: " + csvLine +
	        			"; response: " + responseString);
	        	errors.addError(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.error.badHttpStatus",httpResponseCode,url,lineNumber,csvFileName,csvLine));
	        	return false;
			}
			
			// If we're doing autocomplete, we'll get back API type responses we can check further
			if ( autoCompleteFirstParty.getValue() ) {
				// We have a response, so let's check if it's successful or not
				if ( responseString.startsWith("OK:") ) {
					String tid = responseString.substring(3);
					startedBy.logConfigChange("User started and autocompleted the first party on transaction id: " + tid + " from line #" + lineNumber + " in CSV file '" + csvFileName + "'.");
					_logger.debug("startTransaction() - Successfully started and autocompleted first party on transaction with response: " + responseString + "; by user: " + startedBy.getFullDisplayName());
					return true;
				} 
				
	        	_logger.error("startTransaction() - Error response from URL: " + url + "; httpResponseCode: " + httpResponseCode + "; response: " + responseString + 
	        			"; line #" + lineNumber + "; csvLine: " + csvLine + "; by user: " + startedBy.getFullDisplayName());
				startedBy.logConfigChange("User failed to start and autocomplete the first party for transaction type: " + selectedTransactionTemplate.getPathName() + "; initial data from line #" + lineNumber + " in CSV file '" + csvFileName +  "'; httpResponseCode: " + httpResponseCode);
	        	errors.addError(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.error.badResponse",url,lineNumber,csvFileName,csvLine,responseString));
	        	return false;
			}
			
			// We'll assume any non-200 response is a successful start when not autocompleting the first party
			String tid = extractTransactionIdFromDocumentPage(responseString);
			startedBy.logConfigChange("User started the first party for transaction type: " + selectedTransactionTemplate.getPathName() + "; transactionId: " + tid + "; initial data from line #" + lineNumber + " in CSV file '" + csvFileName + "'");
			_logger.debug("startTransaction() - Successfully started the first party on transaction type: " + selectedTransactionTemplate.getPathName() + "; transactionId: " + tid + "; by user: " + startedBy.getFullDisplayName());
			return true;
			
		} catch( Exception e ) {
			int httpResponseCode = -1;
			try { httpResponseCode = httpCon.getResponseCode(); } catch(Exception ignore) {}
			
			_logger.error("startTransaction() exception w/http status: " + httpResponseCode + "; on url: " + url,e);
        	errors.addError(vaadinUi.getMsg("TransactionCsvStartForm.startTransactions.error.exception",httpResponseCode,url,lineNumber,csvFileName,csvLine));
        	return false;
		} finally {
			try { if ( os != null ) os.close(); } catch(Exception e){} finally { os = null; }
			try { if ( is != null ) is.close(); } catch(Exception e){} finally { is = null; }
			try { if ( br != null ) br.close(); } catch(Exception e){} finally { br = null; }
			try { if ( httpCon != null ) httpCon.disconnect(); } catch(Exception e){} finally { httpCon = null; }
		}
    }
    
    private static java.util.regex.Pattern ExtractTransactionIdFromDocumentPagePattern = java.util.regex.Pattern.compile("<meta name=\"TransactionId\" content=\"([a-f0-9-]+)\" />");
    String extractTransactionIdFromDocumentPage(String pageText) {
        Matcher m = ExtractTransactionIdFromDocumentPagePattern.matcher(pageText);
        if ( m != null ) {
            if ( m.find() ) {
                String contentAttribute = m.group(1);
                EsfUUID tranId = new EsfUUID(contentAttribute);
                return tranId.isNull() ? null : tranId.toPlainString();
            }
        }
        return null;
    }
    
    // Assumes both fields and headerFieldNames are the same length array
    String createPostParams(User startedBy, String csvFileName, int lineNumber, String headerCsvLine, String[] headerFieldNames, String csvLine, String[] fields) {
    	StringBuilder buf = new StringBuilder( 2048 + (100*fields.length) );
    	
    	// First, append in the CSV params.
    	for( int i=0; i < fields.length; ++i ) {
    		if ( buf.length() > 0 )
    			buf.append('&');
    		buf.append( ServletUtil.urlEncode(headerFieldNames[i]) ).append('=').append( ServletUtil.urlEncode(fields[i]) );
    	}
    	
    	// Now append in the various CSV Start built-ins that we pass along to help debug and track
		buf.append('&').append( ServletUtil.urlEncode(Transaction.TRAN_RECORD_CSV_UPLOAD_STARTED_BY_EMAIL_ESFNAME.toPlainString()) ).append('=').append( ServletUtil.urlEncode(startedBy.getEmail()) );
		buf.append('&').append( ServletUtil.urlEncode(Transaction.TRAN_RECORD_CSV_UPLOAD_FILENAME_ESFNAME.toPlainString()) ).append('=').append( ServletUtil.urlEncode(csvFileName) );
		buf.append('&').append( ServletUtil.urlEncode(Transaction.TRAN_RECORD_CSV_UPLOAD_FILE_LINE_NUMBER_ESFNAME.toPlainString()) ).append('=').append( lineNumber );
		buf.append('&').append( ServletUtil.urlEncode(Transaction.TRAN_RECORD_CSV_UPLOAD_HEADER_LINE_ESFNAME.toPlainString()) ).append('=').append( ServletUtil.urlEncode(headerCsvLine) );
		buf.append('&').append( ServletUtil.urlEncode(Transaction.TRAN_RECORD_CSV_UPLOAD_LINE_ESFNAME.toPlainString()) ).append('=').append( ServletUtil.urlEncode(csvLine) );
    	
    	return buf.toString();
    }
    
    Errors processUploadedCsv(String csvData) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		numCsvFields = 0;
    	headerLine = "";
    	dataLineInfoList.clear();

    	int lineNumber = 0;
    	
    	Errors errors = new Errors();
    	
        BufferedReader dataReader = null;
        try {
            CSVParser parser = new CSVParser();
            dataReader = new BufferedReader( new java.io.StringReader(csvData) );
            String csvLine;

            readingLoop:
            while( ( csvLine = dataReader.readLine() ) != null ) {
            	++lineNumber;
            	
                // If we come to a line with no values, this is like an EOF for the CSV file.
                if ( csvLine.startsWith(",,") )
                    break;
                
                String[] fields = parser.parseToArray(csvLine);
                
                if ( lineNumber == 1 ) {
                	if ( EsfString.isBlank(csvLine) ) {
                		errors.addError(vaadinUi.getMsg("TransactionCsvStartForm.upload.error.emptyFirstHeaderLine"));
                		break;
                	} else {
                		// All field names must be valid EsfNames
                		for( int i=0; i < fields.length; ++i ) {
                			EsfName fn = new EsfName(fields[i]);
                			if ( ! fn.isValid() ) {
                        		errors.addError(vaadinUi.getMsg("TransactionCsvStartForm.upload.error.badFirstHeaderLineField",fields[i]));
                				break readingLoop;
                			}
                		}
                		
                    	headerLine = csvLine;
                    	numCsvFields = fields.length;
                    	parsedHeaderLine = fields;
                        continue;
                	}
                }
                
                // Skip blank lines and "comment" lines that begin with //## after the header line
                if ( EsfString.isBlank(csvLine) || csvLine.startsWith("//##") )
                	continue;
                
            	// check column counts
                if ( fields.length != numCsvFields ) {
                	if ( errors.getNumErrorEntries() < 20 ) {
                		errors.addError(vaadinUi.getMsg("TransactionCsvStartForm.upload.error.badCsvRecord",lineNumber,numCsvFields,fields.length));
                	} else if ( errors.getNumErrorEntries() == 20 ) {
                		errors.addError(vaadinUi.getMsg("TransactionCsvStartForm.upload.error.manyBadCsvRecords"));
                	}
                } else {
                	DataLineInfo dli = new DataLineInfo(lineNumber,csvLine,fields);
                    dataLineInfoList.add(dli);
                }
            } // end while not EOF
        } catch( java.io.IOException e ) {
        	_logger.error("processUploadedCsv() - At line number: " + lineNumber, e);
            errors.addError(vaadinUi.getMsg("TransactionCsvStartForm.upload.error.exceptionCsvRecord",lineNumber,e.getMessage()));
        }
        finally {
            if ( dataReader != null ) {
                try { dataReader.close(); } catch( Exception e ) {}
            }
        }
    	
        // If we have a header line and there are no data lines, report this.
    	if ( EsfString.isNonBlank(headerLine) && dataLineInfoList.size() < 1 ) {
    		errors.addError(vaadinUi.getMsg("TransactionCsvStartForm.upload.error.noCsvRecords"));
    	}
    	
    	return errors;
    }

    @Override
	public void activateView(EsfView.OpenMode mode, String params) {
		currentView = SHOW_VIEW.UPLOAD_CSV;
		showCorrectView();
	}

	@Override
	public void buttonClick(ClickEvent event) {
		// Not used
	}
	
	@Override
	public void initView() {
		layout = new VerticalLayout();
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setCompositionRoot(layout);
	}
	
	@Override
	public boolean isDirty() {
		return false; // NOT A REGULAR OBJECT UPDATE FORM
	}

	@Override
	public String checkDirty() { // OBSOLETE SINCE VERSION FORM HAS NO BUTTONS FOR SAVING, CANCELING
		return "";
	}
    
	@Override
	public void detach() {
		resetCaches();
		super.detach();
	}
}