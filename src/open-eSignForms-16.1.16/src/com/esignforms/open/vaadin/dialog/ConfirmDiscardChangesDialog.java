// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.dialog;

import com.esignforms.open.data.EsfString;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class ConfirmDiscardChangesDialog extends Window {
	private static final long serialVersionUID = 8091066353159182869L;

	ConfirmDiscardChangesListener listener;
	ConfirmDiscardChangesDialog myself;
	
	ConfirmDiscardFormChangesWindow discardWindow = null;
	
	public ConfirmDiscardChangesDialog(final String message, final ConfirmDiscardChangesListener listener) {
    	super(EsfVaadinUI.getInstance().getMsg("ConfirmDiscardChangesDialog.caption"));
    	this.listener = listener;
    	this.myself = this;
    	buildLayout(message);
	}
	public ConfirmDiscardChangesDialog(final ConfirmDiscardChangesListener listener) {
		this(EsfVaadinUI.getInstance().getMsg("ConfirmDiscardChangesDialog.defaultMessage"), listener);

	}
	public ConfirmDiscardChangesDialog(final String message) {
		this(message, null);
	}
    
	protected void buildLayout(String message) {
        setModal(true);

		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
        layout.setMargin(true);
        layout.setSpacing(true);
        layout.setWidth("400px");
        if ( EsfString.isNonBlank(message) ) {
        	layout.addComponent(new Label(message));
        }
        layout.addComponent(new Label(vaadinUi.getMsg("ConfirmDiscardChangesDialog.cancelTip",vaadinUi.getMsg("ConfirmDiscardChangesDialog.button.discard.label"))));
        layout.addComponent(new Label(vaadinUi.getMsg("ConfirmDiscardChangesDialog.continueTip",vaadinUi.getMsg("ConfirmDiscardChangesDialog.button.continue.label"))));
        
        vaadinUi.addWindow(this);
        
        Button cancel = new Button(vaadinUi.getMsg("ConfirmDiscardChangesDialog.button.discard.label"), new Button.ClickListener() {
			private static final long serialVersionUID = 2813065641320607823L;

			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				vaadinUi.removeWindow(myself);
				if ( listener != null ) {
	            	listener.doDiscardChanges();
				}
            }
        });
        //cancel.setStyleName(Reindeer.BUTTON_DEFAULT);
        //cancel.setClickShortcut(KeyCode.ENTER);
        
        Button cont = new Button(vaadinUi.getMsg("ConfirmDiscardChangesDialog.button.continue.label"), new Button.ClickListener() {
 			private static final long serialVersionUID = -3021157977653628239L;

			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				vaadinUi.removeWindow(myself);
				if ( listener != null ) {
	            	listener.doContinueKeepChanges();
				}
            }
        });
        
        HorizontalLayout hLayout = new HorizontalLayout();
        hLayout.addComponent(cancel);
        hLayout.addComponent(cont);
        hLayout.setSpacing(true);
        layout.addComponent(hLayout);
        layout.setComponentAlignment(hLayout,Alignment.MIDDLE_RIGHT);
	}
	
	@Override
	public void close() {
		listener.doContinueKeepChanges();
		super.close();
	}
	
	public ConfirmDiscardFormChangesWindow getDiscardWindow() {
		return discardWindow;
	}
	public void setDiscardWindow(final ConfirmDiscardFormChangesWindow w, final ConfirmDiscardChangesListener listener) {
		discardWindow = w;
		this.listener = listener;
	}
	
	public interface ConfirmDiscardChangesListener {
		public void doDiscardChanges();
		public void doContinueKeepChanges();
	}
}