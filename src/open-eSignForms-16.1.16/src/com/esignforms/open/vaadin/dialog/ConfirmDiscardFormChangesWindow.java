// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.dialog;

import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class ConfirmDiscardFormChangesWindow extends Window {
	private static final long serialVersionUID = -5118473476422688933L;

	EsfView view;
	
	public ConfirmDiscardFormChangesWindow(final String windowCaption, final EsfView view) {
		this(windowCaption,view,true);
	}
	
	public ConfirmDiscardFormChangesWindow(final String windowCaption, final EsfView view, boolean setSizeFull) {
    	super(windowCaption);
    	super.setWidth(80, Unit.PERCENTAGE);
    	super.setHeight(80, Unit.PERCENTAGE);
    	
    	// We'll create a full size panel to replace the default VerticalLayout of the Window so we can have scrollbars for our content (the view)
        Panel windowPanel = new Panel();
        VerticalLayout panelLayout = new VerticalLayout();
        windowPanel.setContent(panelLayout);
        panelLayout.setMargin(false);
        panelLayout.setSpacing(false);
        windowPanel.setSizeFull();
        if ( setSizeFull )
        	panelLayout.setSizeFull(); // Using this as most of the data is visible, but it has no scrollbar support. Oy!
        else {
            panelLayout.setWidth(100, Unit.PERCENTAGE); // Tried these two in tandem, got the width right (but with no horizontal scrollbars), but the height issues remain
            panelLayout.setHeight(Sizeable.SIZE_UNDEFINED, Unit.PIXELS);
        }
                    	
        //panelLayout.setSizeUndefined(); // Tried, but creates scrollbars, but the pages don't render nicely (bad widths and often data missing)
        //panelLayout.setWidth(100, Unit.PERCENTAGE); // Tried these two in tandem, got the width right (but with no horizontal scrollbars), but the height issues remain
        //panelLayout.setHeight(-1, Unit.PIXELS);
        
        super.setContent(windowPanel); // Set this before we set our view since that routine will expect the Panel to be our window's content.

        setView(view);
	}
	public ConfirmDiscardFormChangesWindow(final String windowCaption) {
		this(windowCaption, null);
	}
	
	@Override
	public void close() {
		if ( view != null && view.isDirty() ) {
			final ConfirmDiscardChangesDialog dialog = new ConfirmDiscardChangesDialog(view.checkDirty());
			
			dialog.setDiscardWindow(this, new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
				
				@Override
				public void doDiscardChanges() {
					dialog.getDiscardWindow().superClose();
				}
				
				@Override
				public void doContinueKeepChanges() {
				}
			});
		} else {
			superClose();
		}
	}
	
	public void superClose() {
		super.close();
	}
	
	public void setView(EsfView v) {
        if ( view == null ) {
        	if ( v != null ) {
        		getLayout().addComponent(v);
        	}
        } else {
        	if ( v == null ) {
        		getLayout().removeComponent(view);
        	} else {
            	getLayout().replaceComponent(view,v);
        	}
        }
        
		view = v;
	}
	
	public VerticalLayout getLayout() {
		Panel windowPanel = (Panel)super.getContent();
		return (VerticalLayout)windowPanel.getContent();
	}
}