// Copyright (C) 2013-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.converter;

import java.util.Locale;

import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.vaadin.data.util.converter.Converter;

/**
 * This is our converter to/from EsfUUID and String
 *
 * @author Yozons, Inc.
 */
public class EsfUUIDConverter implements Converter<String,EsfUUID>
{
	private static final long serialVersionUID = 6833921961498402217L;

	@Override
	public EsfUUID convertToModel(String value, Class<? extends EsfUUID> targetType, Locale locale) throws com.vaadin.data.util.converter.Converter.ConversionException {
		if ( EsfString.isNonBlank(value) ) {
			EsfUUID v = new EsfUUID(value.trim());
			if ( ! v.isNull() )
				return v;
		}
		throw new com.vaadin.data.util.converter.Converter.ConversionException("String is not a valid EsfUUID: " + value);
	}

	@Override
	public String convertToPresentation(EsfUUID value, Class<? extends String> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		return value == null || value.isNull() ? null : value.toPlainString();
	}

	@Override
	public Class<EsfUUID> getModelType() {
		return EsfUUID.class;
	}

	@Override
	public Class<String> getPresentationType() {
		return String.class;
	}

}