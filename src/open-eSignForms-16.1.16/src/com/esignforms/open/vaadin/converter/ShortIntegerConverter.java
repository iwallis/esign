// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.converter;

import java.util.Locale;

import com.esignforms.open.data.EsfInteger;
import com.vaadin.data.util.converter.Converter;

/**
 * This is our converter to/from Integer and Short
 *
 * @author Yozons, Inc.
 */
public class ShortIntegerConverter implements Converter<Short,Integer>
{
	private static final long serialVersionUID = -2697463317086062345L;

	@Override
	public Integer convertToModel(Short value, Class<? extends Integer> targetType, Locale locale) throws com.vaadin.data.util.converter.Converter.ConversionException {
		return new Integer(value);
	}

	@Override
	public Short convertToPresentation(Integer value, Class<? extends Short> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		if ( value != null ) {
			EsfInteger i = new EsfInteger(value);
			if ( ! i.isNull() && i.isGreaterThanEqualTo(-32768) && i.isLessThanEqualTo(32767) )
				return i.toShort();
		}
		throw new com.vaadin.data.util.converter.Converter.ConversionException("Integer is not a valid Short integer: " + value);
	}

	@Override
	public Class<Integer> getModelType() {
		return Integer.class;
	}

	@Override
	public Class<Short> getPresentationType() {
		return Short.class;
	}

}