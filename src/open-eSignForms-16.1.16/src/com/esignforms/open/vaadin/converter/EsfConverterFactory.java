// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.converter;

import java.util.Date;

import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfHtml;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.util.converter.DefaultConverterFactory;

/**
 * This is our converter factory that converts all of the types we know about that are not handled by Vaadin.
 *
 * @author Yozons, Inc.
 */
public class EsfConverterFactory extends DefaultConverterFactory
{
 	private static final long serialVersionUID = -6983418392219060903L;

	@SuppressWarnings("unchecked")
	@Override
    public <PRESENTATION, MODEL> Converter<PRESENTATION, MODEL> createConverter(Class<PRESENTATION> presentationType, Class<MODEL> modelType) {
        // EsfUUID
        if (String.class == presentationType && EsfUUID.class == modelType)
            return (Converter<PRESENTATION, MODEL>)new EsfUUIDConverter();

        // EsfName
        if (String.class == presentationType && EsfName.class == modelType)
            return (Converter<PRESENTATION, MODEL>)new EsfNameConverter();

        // EsfPathName
        if (String.class == presentationType && EsfPathName.class == modelType)
            return (Converter<PRESENTATION, MODEL>)new EsfPathNameConverter();

        // EsfString
        if (String.class == presentationType && EsfString.class == modelType)
            return (Converter<PRESENTATION, MODEL>)new EsfStringConverter();

        // EsfInteger
        if (String.class == presentationType && EsfInteger.class == modelType)
            return (Converter<PRESENTATION, MODEL>)new EsfIntegerConverter();

        // EsfBoolean
        if (String.class == presentationType && EsfBoolean.class == modelType)
            return (Converter<PRESENTATION, MODEL>)new EsfBooleanConverter();

        // EsfDate
        if (String.class == presentationType && EsfDate.class == modelType)
            return (Converter<PRESENTATION, MODEL>)new EsfDateConverter();

        // EsfDate to Date
        if (Date.class == presentationType && EsfDate.class == modelType)
            return (Converter<PRESENTATION, MODEL>)new EsfDateToDateConverter();

        // EsfHtml
        if (String.class == presentationType && EsfHtml.class == modelType)
            return (Converter<PRESENTATION, MODEL>)new EsfHtmlConverter();

        // Short
        if (String.class == presentationType && Short.class == modelType)
            return (Converter<PRESENTATION, MODEL>)new ShortConverter();

        // Long
        if (String.class == presentationType && Long.class == modelType)
            return (Converter<PRESENTATION, MODEL>)new LongConverter();

        // IntegerShort
        if (Integer.class == presentationType && Short.class == modelType)
            return (Converter<PRESENTATION, MODEL>)new IntegerShortConverter();

        // ShortInteger
        if (Short.class == presentationType && Integer.class == modelType)
            return (Converter<PRESENTATION, MODEL>)new ShortIntegerConverter();

        // Default to the supertype
        return super.createConverter(presentationType, modelType);
    }

}