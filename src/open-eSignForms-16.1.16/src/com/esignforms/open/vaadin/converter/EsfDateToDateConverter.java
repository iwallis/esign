// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.converter;

import java.util.Date;
import java.util.Locale;

import com.esignforms.open.data.EsfDate;
import com.vaadin.data.util.converter.Converter;

/**
 * This is our converter to/from EsfDate and Date
 *
 * @author Yozons, Inc.
 */
public class EsfDateToDateConverter implements Converter<Date,EsfDate>
{
	private static final long serialVersionUID = 6967424179589083696L;

	@Override
	public EsfDate convertToModel(Date value, Class<? extends EsfDate> targetType, Locale locale) throws com.vaadin.data.util.converter.Converter.ConversionException {
		EsfDate d = new EsfDate(value);
		d.setLocale(locale);
		return d;
	}

	@Override
	public Date convertToPresentation(EsfDate value, Class<? extends Date> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		return value == null || value.isNull() ? null : value.toDate();
	}

	@Override
	public Class<EsfDate> getModelType() {
		return EsfDate.class;
	}

	@Override
	public Class<Date> getPresentationType() {
		return Date.class;
	}

}