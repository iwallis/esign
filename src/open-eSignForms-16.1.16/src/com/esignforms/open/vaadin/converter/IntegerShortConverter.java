// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.converter;

import java.util.Locale;

import com.esignforms.open.data.EsfInteger;
import com.vaadin.data.util.converter.Converter;

/**
 * This is our converter to/from Short and Integer
 *
 * @author Yozons, Inc.
 */
public class IntegerShortConverter implements Converter<Integer,Short>
{
	private static final long serialVersionUID = 9045589316001921652L;

	@Override
	public Short convertToModel(Integer value, Class<? extends Short> targetType, Locale locale) throws com.vaadin.data.util.converter.Converter.ConversionException {
		if ( value != null ) {
			EsfInteger i = new EsfInteger(value);
			if ( ! i.isNull() && i.isGreaterThanEqualTo(-32768) && i.isLessThanEqualTo(32767) )
				return i.toShort();
		}
		throw new com.vaadin.data.util.converter.Converter.ConversionException("Integer is not a valid Short integer: " + value);
	}

	@Override
	public Integer convertToPresentation(Short value, Class<? extends Integer> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		return value == null ? null : new Integer(value);
	}

	@Override
	public Class<Short> getModelType() {
		return Short.class;
	}

	@Override
	public Class<Integer> getPresentationType() {
		return Integer.class;
	}

}