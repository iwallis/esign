// Copyright (C) 2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.exception;

/**
 * This exception is thrown from the UI whenever it detects a user is not logged in, but should be.
 * It's a runtime exception because this can really occur anywhere and there's no point in catching it
 * anywhere but in the main Vaadin UI/Session.
 *
 * @author Yozons, Inc.
 */
public class NotLoggedInException 
	extends java.lang.RuntimeException
{
	private static final long serialVersionUID = -3614393920049038948L;

	/**
     * Constructs an "empty" exception with no text
     */
    public NotLoggedInException()
    {
        super("User is no longer logged in. Please logoff and log back in.");
    }
    public NotLoggedInException(Exception e)
    {
        super(e);
    }

    /**
     * Constructs an exception with the specified text.
     * @param errorText the String that describes the exception that occurred
     */
    public NotLoggedInException(String errorText)
    {
        super(errorText);
    }
     
    /**
     * Returns a string version of the exception such that it shows it was a EsfException.
     * @return the String showing this exception.
     */
    public String toLabeledString()
    {
        return "NotLoggedInException: " + super.toString();
    }
}