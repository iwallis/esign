// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2009-2010 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.data;

/**
 * Defines a simple String Integer so we can sort them so 1,2,15 is the order rather than 1,15,2 when integers are sorted as strings
 * in a sortable Integer column to a Table/List in Vaadin UI.
 *
 * @author Yozons, Inc.
 */
public class StringInteger
	implements Comparable<StringInteger>, java.io.Serializable
{
	private static final long serialVersionUID = 2869991162213378759L;

	private int value;
	private String stringValue;
	
	/**
	 * Create a string integer with no value (empty string).
	 */
	public StringInteger()
	{
		value = 0;
		stringValue = "";
	}

	public StringInteger(int v)
	{
		value = v;
		stringValue = Integer.toString(v);
	}

	public StringInteger(String v)
	{
		try
		{
			value = Integer.parseInt(v);
			stringValue = Integer.toString(value);
		}
		catch( NumberFormatException e )
		{
			value = 0;
			stringValue = "";
		}
	}

	@Override
	public boolean equals(Object other)
	{
		if ( other != null && other instanceof StringInteger )
			return value == ((StringInteger)other).value;
		return false;
	}
	@Override
	public int hashCode()
	{
		return stringValue.hashCode();
	}

	
	@Override
    public final String toString()
    {
    	return stringValue;
    }

	@Override
	public int compareTo(StringInteger other) {
		return value - other.value;
	}
    
}