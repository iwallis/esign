// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.data;

import java.util.HashSet;
import java.util.Set;

import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.ListSelect;

/**
 * This class holds any "data scrubbing" that we run into when we integrate with Vaadin.
 *
 * @author Yozons, Inc.
 */
public class DataScrubber
{
    // HACK: It is possible that our multi-selects have values in them are not allowed in our current list select, so we'll remove them now
    // from the ListSelect's allowable values so if the object is saved, only the selected values that are also in the list will be saved.
	// This can be used so it's only done when you first set a new data source so it only takes place then, or use the new ListSelectValid subclass
	// that has the code implemented for the ListSelect.setValue().
	// Reported under Vaadin Ticket 8349 (http://dev.vaadin.com/ticket/8349) and forum https://vaadin.com/forum/-/message_boards/view_message/1112578
	//
	/**
	 * @param form the Form the list select field is on
	 * @param listSelectFieldId the Object id of the list select in the form
	 * @return true if the select list was updated with removed values
	 */
    public static boolean fixupMultiSelectListSelects(Form form, Object listSelectFieldId) {
    	if ( form == null )
    		return false;
    	Field field = form.getField(listSelectFieldId);
    	if ( field != null && field instanceof ListSelect ) {
    		ListSelect select = (ListSelect)field;
    		if ( ! select.isMultiSelect() )
    			return false;
    		Set<?> currListSelectItemIds = (Set<?>) select.getValue();
    		if ( currListSelectItemIds == null || currListSelectItemIds.size() < 1 ) {
    			return false;
    		}
        	HashSet<?> updatedListSelectItemIds = null;
    		for( Object currItemId : currListSelectItemIds ) {
    			boolean found = false;
    	    	for( Object itemId : select.getItemIds() ) {
    	    		if ( currItemId.equals(itemId) ) {
    	    			found = true;
    	    			break;
    	    		}
    	    	}
    	    	if ( ! found ) {
    	    		if ( updatedListSelectItemIds == null ) {
    	    			updatedListSelectItemIds = new HashSet<Object>(currListSelectItemIds);
    	    		}
    	    		updatedListSelectItemIds.remove(currItemId);
    	    	}
    		}
			if ( updatedListSelectItemIds != null ) {
				select.setValue(updatedListSelectItemIds);
				return true;
			}
    	}
    	return false;
    }

}