// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.log.email;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.email.OutboundEmailMessage;
import com.esignforms.open.email.OutboundEmailMessageAttachment;
import com.esignforms.open.email.OutboundEmailMessageResponse;
import com.esignforms.open.util.ServletUtil;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

public class EmailView extends Panel implements EsfView {
	private static final long serialVersionUID = 9067061685912184394L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(EmailView.class);

    final EmailView thisView;
    final Item emailLogItem;
    String htmlDataSessionAttributeName;
	
	public EmailView(Item emailLogItem) {
		super();
		thisView = this;
		this.emailLogItem = emailLogItem;
		setStyleName("EmailView");
		setSizeFull();
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();
		try {
			VerticalLayout layout = new VerticalLayout();
			setContent(layout);
			layout.setWidth(100, Unit.PERCENTAGE);
			layout.setSpacing(true);
			layout.setMargin(true);

	    	OutboundEmailMessage outboundEmailMessage = (OutboundEmailMessage)emailLogItem.getItemProperty("OutboundEmailMessage").getValue();
	    	OutboundEmailMessageResponse outboundEmailMessageResponse = (OutboundEmailMessageResponse)emailLogItem.getItemProperty("OutboundEmailMessageResponse").getValue();
	    	
	    	String timestampString = "";
	    	String from = "";
	    	String to = "";
	    	String cc = "";
	    	String subject = "";
	    	String text = "";
	    	String html = "";
	    	
	    	if ( outboundEmailMessage != null ) {
	    		timestampString = outboundEmailMessage.hasBeenSent() 
	    			? vaadinUi.getMsg("EmailView.timestamp.sent.label",outboundEmailMessage.getSentTimestamp().toLogString(vaadinUi.getUser())) 
	    			: vaadinUi.getMsg("EmailView.timestamp.queued.label",outboundEmailMessage.getCreatedTimestamp().toLogString(vaadinUi.getUser()));
	    		from = outboundEmailMessage.getEmailFrom();
	    		to = outboundEmailMessage.getEmailTo();
	    		cc = outboundEmailMessage.getEmailCc();
	    		subject = outboundEmailMessage.getEmailSubject();
	    		text = outboundEmailMessage.getEmailText();
	    		html = outboundEmailMessage.getEmailHtml();
	    	} else if ( outboundEmailMessageResponse != null ) {
	    		timestampString = vaadinUi.getMsg("EmailView.timestamp.received.label",outboundEmailMessageResponse.getReceivedTimestamp().toLogString(vaadinUi.getUser()));
	    		from = outboundEmailMessageResponse.getEmailFrom();
	    		to = outboundEmailMessageResponse.getEmailTo();
	    		cc = "";
	    		subject = outboundEmailMessageResponse.getEmailSubject();
	    		text = outboundEmailMessageResponse.getEmailText();
	    		html = outboundEmailMessageResponse.getEmailHtml();
	    	}
	    	
	    	layout.addComponent( new Label(timestampString) );
	    	layout.addComponent( new Label(vaadinUi.getMsg("EmailView.from.label",from)) );
	    	layout.addComponent( new Label(vaadinUi.getMsg("EmailView.to.label",to)) );
	    	if ( EsfString.isNonBlank(cc) ) {
		    	layout.addComponent( new Label(vaadinUi.getMsg("EmailView.cc.label",cc)) );
	    	}
	    	layout.addComponent( new Label(vaadinUi.getMsg("EmailView.subject.label",subject)) );
	    	if ( EsfString.isNonBlank(html) ) {
	    		Panel panel = new Panel(vaadinUi.getMsg("EmailView.html.panel.label"));
				panel.setWidth(70, Unit.PERCENTAGE);
				panel.setHeight(250, Unit.PIXELS);
				panel.setStyleName("HtmlPanel");
				VerticalLayout panelLayout = new VerticalLayout();
				panelLayout.setSizeFull();
				panel.setContent(panelLayout);
				
				htmlDataSessionAttributeName = vaadinUi.getEsfapp().getRandomKey().getPickupCodeString();
				VaadinSession.getCurrent().getSession().setAttribute(htmlDataSessionAttributeName, html);
				
				BrowserFrame emailFrame = new BrowserFrame();
				emailFrame.setWidth(100,Unit.PERCENTAGE);
				emailFrame.setHeight(98,Unit.PERCENTAGE);
				
				String url = vaadinUi.getRequestContextPath()+"/VaadinSessionDataRetrieval/";
				url = ServletUtil.appendUrlParam(url, "type", "string");
				url = ServletUtil.appendUrlParam(url, "name", htmlDataSessionAttributeName);
				url = ServletUtil.appendUrlParam(url, "contentType", Application.CONTENT_TYPE_HTML);
				
				emailFrame.setSource(new ExternalResource(url));
				panelLayout.addComponent( emailFrame );
	    		layout.addComponent(panel);
	    	}
	    	
	    	if ( EsfString.isNonBlank(text) ) {
	    		Panel panel = new Panel(vaadinUi.getMsg("EmailView.text.panel.label"));
				panel.setSizeUndefined();
				panel.setStyleName("TextPanel");
				VerticalLayout panelLayout = new VerticalLayout();
				panel.setContent(panelLayout);
				panelLayout.setSizeUndefined();
	    		EsfString textString = new EsfString(text);
	    		panelLayout.addComponent( new Label(textString.toDisplayHtml(),ContentMode.HTML) );
	    		layout.addComponent(panel);
	    	}
	    	
	    	if ( outboundEmailMessage != null && outboundEmailMessage.hasAttachments() ) {
	    		Panel panel = new Panel(vaadinUi.getMsg("EmailView.attachments.panel.label"));
				panel.setSizeUndefined();
				panel.setStyleName("AttachmentsPanel");
				VerticalLayout panelLayout = new VerticalLayout();
				panel.setContent(panelLayout);
				panelLayout.setSizeUndefined();
				
				for( final OutboundEmailMessageAttachment att : outboundEmailMessage.getAttachments() ) {
					Button downloadAttachmentButton = new Button(vaadinUi.getMsg("EmailView.attachment.download.button.label",att.getFileName(),EsfInteger.byteSizeInUnits(att.getFileSize())));
					FileDownloader downloader = new FileDownloader( new StreamResource( new StreamSource() {
						private static final long serialVersionUID = -2683823279470470827L;

						@Override
						public InputStream getStream() {
							
							final byte[] data = att.getFileDataFromDatabase();
							return new BufferedInputStream(new ByteArrayInputStream(data));
						}
						
					},att.getFileName()));
					downloader.setOverrideContentType(false);
					downloader.extend(downloadAttachmentButton);
	    			downloadAttachmentButton.setStyleName(Reindeer.BUTTON_LINK);
	    			panelLayout.addComponent(downloadAttachmentButton);
				}
				
	    		layout.addComponent(panel);
	    	}
			
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Email view exception", e.getMessage());
		}
	}
	
	@Override
	public void detach() {
		if ( htmlDataSessionAttributeName != null ) {
			VaadinSession.getCurrent().getSession().removeAttribute(htmlDataSessionAttributeName);
		}
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return "";
	}

	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
}