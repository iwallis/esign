// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.log.httpsend;

import java.util.Date;

import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.integration.httpsend.HttpSendRequest;
import com.esignforms.open.integration.httpsend.HttpSendResponse;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class HttpSendLogView extends Panel implements EsfView {
	private static final long serialVersionUID = -5575266971966065126L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(HttpSendLogView.class);

    EsfUUID transactionId;
	
    final HttpSendLogView thisView;
    HttpSendLogContainer container;
    HttpSendLogTreeTable table;
	
	// For search bar
	PopupDateField searchFromDate;
	PopupDateField searchToDate;
	Button findButton;

	
	public HttpSendLogView(EsfUUID transactionId) {
		super();
		thisView = this;
		this.transactionId = transactionId;
		setStyleName("HttpSendLogView");
		setSizeFull();
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			container = new HttpSendLogContainer();
			
			VerticalLayout layout = new VerticalLayout();
			setContent(layout);
			layout.setSizeFull();
			layout.setSpacing(false);
			layout.setMargin(false);

	    	HorizontalLayout searchBar = new HorizontalLayout();
	    	searchBar.addStyleName("searchBar");
	    	searchBar.setSpacing(true);
	    	searchBar.setMargin(false);
	    	
	    	searchFromDate = new PopupDateField();
	    	searchFromDate.setImmediate(true);
	    	searchFromDate.setDateFormat(vaadinUi.getEsfapp().getDefaultDateFormat());
	    	searchFromDate.setInputPrompt(vaadinUi.getMsg("HttpSendLogView.searchBar.searchFromDate.label"));
	    	searchFromDate.setDescription(vaadinUi.getMsg("HttpSendLogView.searchBar.searchFromDate.tooltip"));
	    	searchFromDate.setResolution(Resolution.DAY);
	    	//searchFromDate.setLocale(vaadinUi.getLocale());
	    	searchBar.addComponent(searchFromDate);

	    	searchToDate = new PopupDateField();
	    	searchToDate.setImmediate(true);
	    	searchToDate.setDateFormat(vaadinUi.getEsfapp().getDefaultDateFormat());
	    	searchToDate.setInputPrompt(vaadinUi.getMsg("HttpSendLogView.searchBar.searchToDate.label"));
	    	searchToDate.setDescription(vaadinUi.getMsg("HttpSendLogView.searchBar.searchToDate.tooltip"));
	    	searchToDate.setResolution(Resolution.DAY);
	    	//searchToDate.setLocale(vaadinUi.getLocale());
	    	searchBar.addComponent(searchToDate);
	    	
	    	findButton = new Button(vaadinUi.getMsg("HttpSendLogView.searchBar.findButton.label"));
	    	findButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.showAll.icon")));
	    	findButton.setDescription(vaadinUi.getMsg("HttpSendLogView.searchBar.findButton.tooltip"));
	    	findButton.setStyleName(Reindeer.BUTTON_SMALL);
	    	//findButton.addStyleName(Reindeer.BUTTON_DEFAULT);
	    	//findButton.setClickShortcut(KeyCode.ENTER);
	    	findButton.addClickListener( new Button.ClickListener() {			
				private static final long serialVersionUID = -3385115962722269561L;

				@Override
				public void buttonClick(ClickEvent event) {
					doSearch();
				}
			});
	    	searchBar.addComponent(findButton);

	    	table = new HttpSendLogTreeTable(this,container);
	    	
	    	layout.addComponent(searchBar);
	    	layout.addComponent(table);
	    	layout.setExpandRatio(table, 1);
			
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Log view exception", e.getMessage());
		}
	}
	
	public void doSearch() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		if ( ! searchFromDate.isValid() || ! searchToDate.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.data.invalid"));
			return;
		}

		Date fromDate = (Date)searchFromDate.getValue();
		Date toDate = (Date)searchToDate.getValue();
		EsfDateTime esfFromDate = fromDate == null ? null : new EsfDateTime(fromDate);
		if ( esfFromDate != null )
			esfFromDate.normalizeTimeToZero(vaadinUi.getUser().getTimezoneTz());
		EsfDateTime esfToDate = toDate == null ? null : new EsfDateTime(toDate);
		if ( esfToDate != null )
			esfToDate.normalizeTimeToEndOfDay(vaadinUi.getUser().getTimezoneTz());
		
		container.refresh(HttpSendRequest.Manager.getMatching(esfFromDate,esfToDate,transactionId));
		vaadinUi.showStatus(vaadinUi.getMsg("HttpSendLogView.findButton.foundCount",container.size()));
		for( Object itemId : container.getItemIds() ) {
			table.setCollapsed(itemId, false);
		}
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return "";
	}

	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
	class HttpSendLogTreeTable extends TreeTable {
		private static final long serialVersionUID = 4123379953478851596L;

		public HttpSendLogTreeTable(HttpSendLogView view, HttpSendLogContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("HttpSendLogView.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("HttpSendLogView.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setSelectable(true);
			setImmediate(true);
			setSizeFull();
	        addValueChangeListener(new Property.ValueChangeListener() {
				private static final long serialVersionUID = -2009406359540237204L;

				public void valueChange(Property.ValueChangeEvent event) {
    	        	Item item = getItem(getValue());
    	        	
    	        	if ( item == null )
    	        		return;

    	        	HttpSendRequest httpSendRequest   = (HttpSendRequest)item.getItemProperty("HttpSendRequest").getValue();
    	        	HttpSendResponse httpSendResponse = (HttpSendResponse)item.getItemProperty("HttpSendResponse").getValue();
    	        	EsfUUID httpSendId = httpSendRequest != null ? httpSendRequest.getId() : httpSendResponse.getHttpSendRequestId();
    	        	
    	    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	            final HttpSendView httpSendView = new HttpSendView(httpSendRequest,httpSendResponse);
    	            httpSendView.initView();
    	    		
    	            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("HttpSendLogView.HttpSendView.window.caption",httpSendId), httpSendView);
    	        	w.center();
    	        	w.setWidth(90, Unit.PERCENTAGE);
    	        	w.setHeight(90, Unit.PERCENTAGE);

    	        	httpSendView.activateView(EsfView.OpenMode.WINDOW, "");
    	            vaadinUi.addWindowToUI(w);
    	            select(null);
	            }
	        });
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			if ( "beginTimestamp".equals(colId) || "endTimestamp".equals(colId) ) {
				EsfDateTime myDate = (EsfDateTime)property.getValue();
				if ( myDate == null ) 
					return "";
				return myDate.toLogString(vaadinUi.getUser());
			}
			
			return super.formatPropertyValue(rowId,colId,property);
		}

	} // HttpSendLogTreeTable

}