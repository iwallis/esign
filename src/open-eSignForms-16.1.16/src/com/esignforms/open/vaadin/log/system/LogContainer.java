// Copyright (C) 2011 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.log.system;

import java.io.Serializable;
import java.util.List;

import com.esignforms.open.log.SystemActivityLogInfo;
import com.vaadin.data.util.BeanItemContainer;

public class LogContainer extends BeanItemContainer<LogBean> implements Serializable {
	private static final long serialVersionUID = 5549530399134168534L;

	public LogContainer() throws InstantiationException, IllegalAccessException {
		super(LogBean.class);
	}
	
	public void refresh(List<SystemActivityLogInfo> logList) {
		removeAllItems();
		
		for( SystemActivityLogInfo logInfo : logList ) {
			addItem( new LogBean(logInfo) );	
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}