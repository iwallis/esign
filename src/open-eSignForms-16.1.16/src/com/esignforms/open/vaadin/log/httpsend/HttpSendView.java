// Copyright (C) 2013-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.log.httpsend;

import com.esignforms.open.data.EsfString;
import com.esignforms.open.integration.httpsend.HttpSendRequest;
import com.esignforms.open.integration.httpsend.HttpSendResponse;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

/**
 * HttpSendView just displays the details on an HttpSendRequest, if present, else an HttpSendResponse.
 * @author Yozons, Inc.
 */
public class HttpSendView extends Panel implements EsfView {
	private static final long serialVersionUID = -1049296172280239249L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(HttpSendView.class);

    final HttpSendView thisView;
    final HttpSendRequest httpSendRequest;
    final HttpSendResponse httpSendResponse;
    String htmlDataSessionAttributeName;
	
	public HttpSendView(HttpSendRequest httpSendRequest, HttpSendResponse httpSendResponse) {
		super();
		thisView = this;
		this.httpSendRequest = httpSendRequest;
		this.httpSendResponse = httpSendResponse;
		setStyleName("HttpSendView");
		setSizeFull();
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();
		try {
			VerticalLayout layout = new VerticalLayout();
			setContent(layout);
			layout.setWidth(100, Unit.PERCENTAGE);
			layout.setSpacing(true);
			layout.setMargin(true);

			if ( httpSendRequest != null ) {
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.sectionHeader"),ContentMode.HTML) );
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.id.label",httpSendRequest.getId())) );
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.createdTimestamp.label",httpSendRequest.getCreatedTimestamp().toLogString(vaadinUi.getUser()))) );
				if ( httpSendRequest.hasBeenCompleted() )
					layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.completedTimestamp.label",httpSendRequest.getCompletedTimestamp().toLogString(vaadinUi.getUser()))) );
				else
					layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.completedTimestamp.notYetCompleted.label")) );
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.attempts.label",httpSendRequest.getNumAttempts(),httpSendRequest.getMaxAttempts())) );
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.retryDelay.label",httpSendRequest.getRetryDelayMinutes())) );
				if ( httpSendRequest.getNumAttempts() > 0 )
					layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.lastAttemptedTimestamp.label",httpSendRequest.getLastAttemptedTimestamp().toLogString(vaadinUi.getUser()))) );
				
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.reason.label",httpSendRequest.getReason())) );
				if ( httpSendRequest.hasLimitSuccessfulHttpStatusCodes() )
					layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.limitSuccessfulHttpStatusCodes.label",httpSendRequest.getLimitSuccessfulHttpStatusCodesAsString())) );
				else
					layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.limitSuccessfulHttpStatusCodes.none.label")) );
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.responseMatchType.label",vaadinUi.getPrettyCode().httpSendResponseMatchType(httpSendRequest.getResponseMatchType()))) );
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.responseMatchValue.label",httpSendRequest.getResponseMatchValue())) );
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.method.label",httpSendRequest.getRequestMethod())) );
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.request.url.label",httpSendRequest.getUrl())) );

	    		Panel panel = new Panel(vaadinUi.getMsg("HttpSendView.request.nameValueParam.panel.label"));
				panel.setSizeUndefined();
				panel.setStyleName("TextPanel");
				VerticalLayout panelLayout = new VerticalLayout();
				panel.setContent(panelLayout);
				panelLayout.setSizeUndefined();
	    		EsfString textString = new EsfString(httpSendRequest.getParams());
	    		if ( textString.isBlank() )
	    			textString.setValue(vaadinUi.getMsg("HttpSendView.request.nameValueParam.emptyValue"));
	    		panelLayout.addComponent( new Label(EsfString.breakLongStringLines(textString.toPlainString(),150),ContentMode.PREFORMATTED) );
	    		layout.addComponent(panel);
			}
			
			if ( httpSendResponse != null ) {
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.response.sectionHeader"),ContentMode.HTML) );
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.response.id.label",httpSendResponse.getHttpSendRequestId())) );
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.response.sendTimestamp.label",httpSendResponse.getSentTimestamp().toLogString(vaadinUi.getUser()))) );
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.response.responseTimestamp.label",httpSendResponse.getResponseTimestamp().toLogString(vaadinUi.getUser()))) );
				String successFailure = vaadinUi.getPrettyCode().httpSendResponseStatus(httpSendResponse.getStatus());
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.response.status.label",successFailure)) );
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.response.httpStatusCode.label",httpSendResponse.getHttpStatusCode())) );
				layout.addComponent( new Label(vaadinUi.getMsg("HttpSendView.response.contentType.label",httpSendResponse.getHttpResponseContentType())) );

	    		Panel panel = new Panel(vaadinUi.getMsg("HttpSendView.response.text.panel.label"));
				panel.setSizeUndefined();
				panel.setStyleName("TextPanel");
				VerticalLayout panelLayout = new VerticalLayout();
				panel.setContent(panelLayout);
				panelLayout.setSizeUndefined();
	    		EsfString textString = new EsfString(httpSendResponse.getHttpResponse());
	    		if ( textString.isBlank() )
	    			textString.setValue(vaadinUi.getMsg("HttpSendView.response.text.emptyValue"));
	    		panelLayout.addComponent( new Label(EsfString.breakLongStringLines(textString.toPlainString(),150),ContentMode.PREFORMATTED) );
	    		layout.addComponent(panel);
			}
			
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("HTTP Send view exception", e.getMessage());
		}
	}
	
	@Override
	public void detach() {
		if ( htmlDataSessionAttributeName != null ) {
			VaadinSession.getCurrent().getSession().removeAttribute(htmlDataSessionAttributeName);
		}
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return "";
	}

	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
}