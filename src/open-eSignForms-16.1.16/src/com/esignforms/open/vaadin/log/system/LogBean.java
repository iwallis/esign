// Copyright (C) 2011 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.log.system;

import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.log.SystemActivityLogInfo;
import com.esignforms.open.user.User;

public class LogBean implements java.io.Serializable
{
	private static final long serialVersionUID = 6179964623755253214L;

	SystemActivityLogInfo logInfo;
	
	public LogBean(SystemActivityLogInfo logInfo) {
		this.logInfo = logInfo;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o !=null && o instanceof LogBean )
			return logInfo.equals(((LogBean)o).logInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return logInfo.hashCode();
    }
	public int compareTo(LogBean l) {
		return logInfo.compareTo(l.logInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public SystemActivityLogInfo logInfo() {
		return logInfo;
	}

	public EsfDateTime getTimestamp() {
		return logInfo.getTimestamp();
	}
	public String formatTimestamp(User forUser) {
		return getTimestamp().toLogString(forUser);
	}

	
	public short getType() {
		return logInfo.getType();
	}
	
	public String getText() {
		return logInfo.getText();
	}

}