// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.log.system;

import java.util.Date;
import java.util.LinkedList;

import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.log.ActivityLog;
import com.esignforms.open.log.SystemActivityLogInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Property;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class LogView extends Panel implements EsfView {
	private static final long serialVersionUID = 2554653268881942842L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LogView.class);

    final LogView thisView;
	LogContainer container;
	LogTable table;
	
	// For search bar
	private static final String PROP_FROM_DATE = "SystemLogView/fromDate";
	private static final String PROP_TO_DATE = "SystemLogView/toDate";
	private static final String PROP_VERSION = "SystemLogView/version";
	private static final String PROP_START_STOP = "SystemLogView/startStop";
	private static final String PROP_USER_ACTIVITY = "SystemLogView/userActivity";
	private static final String PROP_CONFIG = "SystemLogView/config";
	private static final String PROP_TEXT = "SystemLogView/text";

	PopupDateField searchFromDate;
	PopupDateField searchToDate;
	CheckBox searchVersion;
	CheckBox searchStartStop;
	CheckBox searchUserActivity;
	CheckBox searchConfig;
	TextField searchText;
	Button findButton;

	
	public LogView() {
		super();
		thisView = this;
		setStyleName("SystemLogView");
		setSizeFull();
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();
		try {
			container = new LogContainer();
			
			VerticalLayout layout = new VerticalLayout();
			setContent(layout);
			layout.setSizeFull();
			layout.setSpacing(false);
			layout.setMargin(false);

	    	HorizontalLayout searchBar = new HorizontalLayout();
	    	searchBar.addStyleName("searchBar");
	    	searchBar.setSpacing(true);
	    	searchBar.setMargin(false);
	    	
        	EsfDate useFromDate;
        	EsfString propFromDateString = vaadinUi.getUser().getUserDefinedStringProperty(PROP_FROM_DATE);
        	if ( propFromDateString == null || propFromDateString.isNull() ) {
        		useFromDate = new EsfDate(vaadinUi.getUser().getTimezoneTz());
        	} else if ( propFromDateString.isBlank() ) {
        		useFromDate = null;
        	} else {
        		useFromDate = EsfDate.CreateFromYMD(propFromDateString.toString());
        	}
        	searchFromDate = new PopupDateField();
        	searchFromDate.setImmediate(true);
	    	searchFromDate.setDateFormat(vaadinUi.getEsfapp().getDefaultDateFormat());
	    	searchFromDate.setInputPrompt(vaadinUi.getMsg("SystemLogView.searchBar.searchFromDate.label"));
	    	searchFromDate.setDescription(vaadinUi.getMsg("SystemLogView.searchBar.searchFromDate.tooltip"));
	    	searchFromDate.setResolution(Resolution.DAY);
	    	searchFromDate.setValue(useFromDate == null || useFromDate.isNull() ? null : useFromDate.toDate());
	    	//searchFromDate.setLocale(vaadinUi.getLocale());
	    	searchBar.addComponent(searchFromDate);

        	EsfDate useToDate;
        	EsfString propToDateString = vaadinUi.getUser().getUserDefinedStringProperty(PROP_TO_DATE);
        	if ( propToDateString == null || propToDateString.isBlank() ) {
        		useToDate = null;
        	} else {
        		useToDate = EsfDate.CreateFromYMD(propToDateString.toString());
        	}
	    	searchToDate = new PopupDateField();
        	searchToDate.setImmediate(true);
	    	searchToDate.setDateFormat(vaadinUi.getEsfapp().getDefaultDateFormat());
	    	searchToDate.setInputPrompt(vaadinUi.getMsg("SystemLogView.searchBar.searchToDate.label"));
	    	searchToDate.setDescription(vaadinUi.getMsg("SystemLogView.searchBar.searchToDate.tooltip"));
	    	searchToDate.setResolution(Resolution.DAY);
	    	searchToDate.setValue(useToDate == null || useToDate.isNull() ? null : useToDate.toDate());
	    	//searchToDate.setLocale(vaadinUi.getLocale());
	    	searchBar.addComponent(searchToDate);
	    	
        	EsfBoolean propVersion = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_VERSION);
        	if ( propVersion == null ) {
        		propVersion = new EsfBoolean(true);
        	}
	    	searchVersion = new CheckBox(vaadinUi.getMsg("SystemLogView.searchBar.searchVersion.label"));
	    	searchVersion.setDescription(vaadinUi.getMsg("SystemLogView.searchBar.searchVersion.tooltip"));
	    	searchVersion.setValue(propVersion.isTrue());
	    	searchBar.addComponent(searchVersion);
	    	
        	EsfBoolean propStartStop = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_START_STOP);
        	if ( propStartStop == null ) {
        		propStartStop = new EsfBoolean(true);
        	}
	    	searchStartStop = new CheckBox(vaadinUi.getMsg("SystemLogView.searchBar.searchStartStop.label"));
	    	searchStartStop.setDescription(vaadinUi.getMsg("SystemLogView.searchBar.searchStartStop.tooltip"));
	    	searchStartStop.setValue(propStartStop.isTrue());
	    	searchBar.addComponent(searchStartStop);
	    	
        	EsfBoolean propUserActivity = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_USER_ACTIVITY);
        	if ( propUserActivity == null ) {
        		propUserActivity = new EsfBoolean(true);
        	}
	    	searchUserActivity = new CheckBox(vaadinUi.getMsg("SystemLogView.searchBar.searchUserActivity.label"));
	    	searchUserActivity.setDescription(vaadinUi.getMsg("SystemLogView.searchBar.searchUserActivity.tooltip"));
	    	searchUserActivity.setValue(propUserActivity.isTrue());
	    	searchBar.addComponent(searchUserActivity);
	    	
        	EsfBoolean propConfig = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_CONFIG);
        	if ( propConfig == null ) {
        		propConfig = new EsfBoolean(true);
        	}
	    	searchConfig = new CheckBox(vaadinUi.getMsg("SystemLogView.searchBar.searchConfig.label"));
	    	searchConfig.setDescription(vaadinUi.getMsg("SystemLogView.searchBar.searchConfig.tooltip"));
	    	searchConfig.setValue(propConfig.isTrue());
	    	searchBar.addComponent(searchConfig);
	    	
        	EsfString propText = vaadinUi.getUser().getUserDefinedStringProperty(PROP_TEXT);
	    	searchText = new TextField();
	    	searchText.setStyleName(Reindeer.TEXTFIELD_SMALL);
	    	searchText.setInputPrompt(vaadinUi.getMsg("SystemLogView.searchBar.searchText.label"));
	    	searchText.setDescription(vaadinUi.getMsg("SystemLogView.searchBar.searchText.tooltip"));
	    	searchText.setValue(propText==null?"":propText.toString());
	    	searchBar.addComponent(searchText);
	    	
	    	findButton = new Button(vaadinUi.getMsg("SystemLogView.searchBar.findButton.label"));
	    	findButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.showAll.icon")));
	    	findButton.setDescription(vaadinUi.getMsg("SystemLogView.searchBar.findButton.tooltip"));
	    	findButton.setStyleName(Reindeer.BUTTON_SMALL);
	    	//findButton.addStyleName(Reindeer.BUTTON_DEFAULT);
	    	//findButton.setClickShortcut(KeyCode.ENTER);
	    	findButton.addClickListener( new Button.ClickListener() {			
				private static final long serialVersionUID = -424140778910067108L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					vaadinUi.ensureLoggedIn();
					
					if ( ! searchFromDate.isValid() || ! searchToDate.isValid() ) {
						vaadinUi.showWarning(null, vaadinUi.getMsg("form.data.invalid"));
						return;
					}
					
					Date fromDate = (Date)searchFromDate.getValue();
					EsfString propFromDateString = fromDate == null ? new EsfString("") : new EsfString((new EsfDate(fromDate)).toYMDString());
					vaadinUi.getUser().setUserDefinedStringProperty(PROP_FROM_DATE, propFromDateString);
					
					Date toDate = (Date)searchToDate.getValue();
					EsfString propToDateString = toDate == null ? new EsfString("") : new EsfString((new EsfDate(toDate)).toYMDString());
					vaadinUi.getUser().setUserDefinedStringProperty(PROP_TO_DATE, propToDateString);

					EsfDateTime esfFromDate = fromDate == null ? null : new EsfDateTime(fromDate);
					if ( esfFromDate != null ) {
						esfFromDate.normalizeTimeToZero(vaadinUi.getUser().getTimezoneTz());
					}
					EsfDateTime esfToDate = toDate == null ? null : new EsfDateTime(toDate);
					if ( esfToDate != null ) {
						esfToDate.normalizeTimeToEndOfDay(vaadinUi.getUser().getTimezoneTz());
					}
					
					Boolean includeVersion = searchVersion.getValue();
					vaadinUi.getUser().setUserDefinedBooleanProperty(PROP_VERSION, new EsfBoolean(includeVersion));

					Boolean includeStartStop = searchStartStop.getValue();
					vaadinUi.getUser().setUserDefinedBooleanProperty(PROP_START_STOP, new EsfBoolean(includeStartStop));

					Boolean includeUserActivity = searchUserActivity.getValue();
					vaadinUi.getUser().setUserDefinedBooleanProperty(PROP_USER_ACTIVITY, new EsfBoolean(includeUserActivity));

					Boolean includeConfig = searchConfig.getValue();
					vaadinUi.getUser().setUserDefinedBooleanProperty(PROP_CONFIG, new EsfBoolean(includeConfig));
					
					LinkedList<Short> limitLogTypes = null;
					if ( ! includeVersion || ! includeStartStop || ! includeUserActivity || ! includeConfig ) {
						limitLogTypes = new LinkedList<Short>();
						if ( includeVersion ) limitLogTypes.add((short)(ActivityLog.LOG_TYPE_SYSTEM_VERSION_CHANGE_PERMANENT * -1));
						if ( includeStartStop ) limitLogTypes.add(ActivityLog.LOG_TYPE_SYSTEM_START_STOP);
						if ( includeUserActivity ) limitLogTypes.add(ActivityLog.LOG_TYPE_SYSTEM_USER_ACTIVITY);
						if ( includeConfig ) limitLogTypes.add(ActivityLog.LOG_TYPE_SYSTEM_CONFIG_CHANGE);
					}
					
					String text = (String)searchText.getValue();
					vaadinUi.getUser().setUserDefinedStringProperty(PROP_TEXT, new EsfString(text));
					
					container.refresh(SystemActivityLogInfo.Manager.getMatching(esfFromDate,esfToDate,limitLogTypes,text));
					vaadinUi.showStatus(vaadinUi.getMsg("SystemLogView.findButton.foundCount",container.size()));
				}
			});
	    	searchBar.addComponent(findButton);

	    	table = new LogTable(this,container);
	    	
	    	layout.addComponent(searchBar);
	    	layout.addComponent(table);
	    	layout.setExpandRatio(table, 1);
			
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Log view exception", e.getMessage());
		}
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return "";
	}

	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
	class LogTable extends Table {
		private static final long serialVersionUID = 476955171576768986L;

		public LogTable(LogView view, LogContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns(vaadinUi.getStringArray("SystemLogView.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("SystemLogView.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setSizeFull();
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			if ( "timestamp".equals(colId) ) {
				return ((LogBean)rowId).formatTimestamp(vaadinUi.getUser());
			}
			
			if ( "type".equals(colId) ) {
				switch( ((LogBean)rowId).getType() ) {
				case ActivityLog.LOG_TYPE_SYSTEM_CONFIG_CHANGE: return vaadinUi.getMsg("SystemLogView.searchBar.searchConfig.label");
				case ActivityLog.LOG_TYPE_SYSTEM_START_STOP: return vaadinUi.getMsg("SystemLogView.searchBar.searchStartStop.label");
				case ActivityLog.LOG_TYPE_SYSTEM_USER_ACTIVITY: return vaadinUi.getMsg("SystemLogView.searchBar.searchUserActivity.label");
				case ActivityLog.LOG_TYPE_SYSTEM_VERSION_CHANGE_PERMANENT: return vaadinUi.getMsg("SystemLogView.searchBar.searchVersion.label");
				default: return vaadinUi.getMsg("SystemLogView.type.unknown.label",((LogBean)rowId).getType());
				}
			}
			
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // LogTable

}