// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.log.transaction;

import java.util.Date;
import java.util.LinkedList;

import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.log.ActivityLog;
import com.esignforms.open.log.TransactionActivityLogInfo;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.log.transaction.LogBean;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Property;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PopupDateField;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class LogView extends Panel implements EsfView {
	private static final long serialVersionUID = 244268161019470636L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LogView.class);

    Transaction transaction;
	
    final LogView thisView;
	LogContainer container;
	LogTable table;
	
	// For search bar
	private static final String PROP_FROM_DATE = "TransactionLogView/fromDate";
	private static final String PROP_TO_DATE = "TransactionLogView/toDate";
	private static final String PROP_GENERAL = "TransactionLogView/general";
	private static final String PROP_BASIC_TRACE = "TransactionLogView/basicTrace";
	private static final String PROP_DETAIL_TRACE = "TransactionLogView/detailTrace";
	private static final String PROP_TEXT = "TransactionLogView/text";

	PopupDateField searchFromDate;
	PopupDateField searchToDate;
	CheckBox searchGeneral;
	CheckBox searchBasicTrace;
	CheckBox searchDetailTrace;
	TextField searchText;
	Button findButton;

	
	public LogView(Transaction transaction) {
		super();
		thisView = this;
		this.transaction = transaction;
		setStyleName("TransactionLogView");
		setSizeFull();
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();

		try {
			container = new LogContainer();
			
			VerticalLayout layout = new VerticalLayout();
			setContent(layout);
			layout.setSizeFull();
			layout.setSpacing(false);
			layout.setMargin(false);

	    	HorizontalLayout searchBar = new HorizontalLayout();
	    	searchBar.addStyleName("searchBar");
	    	searchBar.setSpacing(true);
	    	searchBar.setMargin(false);
	    	
        	EsfDate useFromDate;
        	EsfString propFromDateString = vaadinUi.getUser().getUserDefinedStringProperty(PROP_FROM_DATE);
        	if ( propFromDateString == null || propFromDateString.isNull() ) {
        		useFromDate = new EsfDate(vaadinUi.getUser().getTimezoneTz());
        	} else if ( propFromDateString.isBlank() ) {
        		useFromDate = null;
        	} else {
        		useFromDate = EsfDate.CreateFromYMD(propFromDateString.toString());
        	}
	    	searchFromDate = new PopupDateField();
	    	searchFromDate.setImmediate(true);
	    	searchFromDate.setDateFormat(vaadinUi.getEsfapp().getDefaultDateFormat());
	    	searchFromDate.setInputPrompt(vaadinUi.getMsg("TransactionLogView.searchBar.searchFromDate.label"));
	    	searchFromDate.setDescription(vaadinUi.getMsg("TransactionLogView.searchBar.searchFromDate.tooltip"));
	    	searchFromDate.setResolution(Resolution.DAY);
	    	searchFromDate.setValue(useFromDate == null || useFromDate.isNull() ? null : useFromDate.toDate());
	    	//searchFromDate.setLocale(vaadinUi.getLocale());
	    	searchBar.addComponent(searchFromDate);

        	EsfDate useToDate;
        	EsfString propToDateString = vaadinUi.getUser().getUserDefinedStringProperty(PROP_TO_DATE);
        	if ( propToDateString == null || propToDateString.isBlank() ) {
        		useToDate = null;
        	} else {
        		useToDate = EsfDate.CreateFromYMD(propToDateString.toString());
        	}
	    	searchToDate = new PopupDateField();
	    	searchToDate.setImmediate(true);
	    	searchToDate.setDateFormat(vaadinUi.getEsfapp().getDefaultDateFormat());
	    	searchToDate.setInputPrompt(vaadinUi.getMsg("TransactionLogView.searchBar.searchToDate.label"));
	    	searchToDate.setDescription(vaadinUi.getMsg("TransactionLogView.searchBar.searchToDate.tooltip"));
	    	searchToDate.setResolution(Resolution.DAY);
	    	searchToDate.setValue(useToDate == null || useToDate.isNull() ? null : useToDate.toDate());
	    	//searchToDate.setLocale(vaadinUi.getLocale());
	    	searchBar.addComponent(searchToDate);
	    	
        	EsfBoolean propGeneral = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_GENERAL);
        	if ( propGeneral == null ) {
        		propGeneral = new EsfBoolean(true);
        	}
	    	searchGeneral = new CheckBox(vaadinUi.getMsg("TransactionLogView.searchBar.searchGeneral.label"));
	    	searchGeneral.setDescription(vaadinUi.getMsg("TransactionLogView.searchBar.searchGeneral.tooltip"));
	    	searchGeneral.setValue(propGeneral.isTrue());
	    	searchBar.addComponent(searchGeneral);
	    	
        	EsfBoolean propBasicTrace = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_BASIC_TRACE);
        	if ( propBasicTrace == null ) {
        		propBasicTrace = new EsfBoolean(true);
        	}
	    	searchBasicTrace = new CheckBox(vaadinUi.getMsg("TransactionLogView.searchBar.searchBasicTrace.label"));
	    	searchBasicTrace.setDescription(vaadinUi.getMsg("TransactionLogView.searchBar.searchBasicTrace.tooltip"));
	    	searchBasicTrace.setValue(propBasicTrace.isTrue());
	    	searchBar.addComponent(searchBasicTrace);
	    	
        	EsfBoolean propDetailTrace = vaadinUi.getUser().getUserDefinedBooleanProperty(PROP_DETAIL_TRACE);
        	if ( propDetailTrace == null ) {
        		propDetailTrace = new EsfBoolean(true);
        	}
	    	searchDetailTrace = new CheckBox(vaadinUi.getMsg("TransactionLogView.searchBar.searchDetailTrace.label"));
	    	searchDetailTrace.setDescription(vaadinUi.getMsg("TransactionLogView.searchBar.searchDetailTrace.tooltip"));
	    	searchDetailTrace.setValue(propDetailTrace.isTrue());
	    	searchBar.addComponent(searchDetailTrace);
	    	
        	EsfString propText = vaadinUi.getUser().getUserDefinedStringProperty(PROP_TEXT);
	    	searchText = new TextField();
	    	searchText.setStyleName(Reindeer.TEXTFIELD_SMALL);
	    	searchText.setInputPrompt(vaadinUi.getMsg("TransactionLogView.searchBar.searchText.label"));
	    	searchText.setDescription(vaadinUi.getMsg("TransactionLogView.searchBar.searchText.tooltip"));
	    	searchText.setValue(propText==null?"":propText.toString());
	    	searchBar.addComponent(searchText);
	    	
	    	findButton = new Button(vaadinUi.getMsg("TransactionLogView.searchBar.findButton.label"));
	    	findButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.showAll.icon")));
	    	findButton.setDescription(vaadinUi.getMsg("TransactionLogView.searchBar.findButton.tooltip"));
	    	findButton.setStyleName(Reindeer.BUTTON_SMALL);
	    	//findButton.addStyleName(Reindeer.BUTTON_DEFAULT);
	    	//findButton.setClickShortcut(KeyCode.ENTER);
	    	findButton.addClickListener( new Button.ClickListener() {			
				private static final long serialVersionUID = -1073669678111089890L;

				@Override
				public void buttonClick(ClickEvent event) {
					doSearch();
				}
			});
	    	searchBar.addComponent(findButton);

	    	table = new LogTable(this,container);
	    	
	    	layout.addComponent(searchBar);
	    	layout.addComponent(table);
	    	layout.setExpandRatio(table, 1);
			
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Log view exception", e.getMessage());
		}
	}
	
	public void doSearch(boolean includeGeneral, boolean includeBasicTrace, boolean includeDetailTrace) 
	{
		searchGeneral.setValue(includeGeneral);
		searchBasicTrace.setValue(includeBasicTrace);
		searchDetailTrace.setValue(includeDetailTrace);
		doSearch();
	}
	
	public void doSearch() 
	{
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();
		
		if ( ! searchFromDate.isValid() || ! searchToDate.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.data.invalid"));
			return;
		}
		
		Date fromDate = (Date)searchFromDate.getValue();
		EsfString propFromDateString = fromDate == null ? new EsfString("") : new EsfString((new EsfDate(fromDate)).toYMDString());
		vaadinUi.getUser().setUserDefinedStringProperty(PROP_FROM_DATE, propFromDateString);

		Date toDate = (Date)searchToDate.getValue();
		EsfString propToDateString = toDate == null ? new EsfString("") : new EsfString((new EsfDate(toDate)).toYMDString());
		vaadinUi.getUser().setUserDefinedStringProperty(PROP_TO_DATE, propToDateString);

		EsfDateTime esfFromDate = fromDate == null ? null : new EsfDateTime(fromDate);
		if ( esfFromDate != null )
			esfFromDate.normalizeTimeToZero(vaadinUi.getUser().getTimezoneTz());
		EsfDateTime esfToDate = toDate == null ? null : new EsfDateTime(toDate);
		if ( esfToDate != null )
			esfToDate.normalizeTimeToEndOfDay(vaadinUi.getUser().getTimezoneTz());
		
		Boolean includeGeneral = searchGeneral.getValue();
		vaadinUi.getUser().setUserDefinedBooleanProperty(PROP_GENERAL, new EsfBoolean(includeGeneral));

		Boolean includeBasicTrace = searchBasicTrace.getValue();
		vaadinUi.getUser().setUserDefinedBooleanProperty(PROP_BASIC_TRACE, new EsfBoolean(includeBasicTrace));

		Boolean includeDetailTrace = searchDetailTrace.getValue();
		vaadinUi.getUser().setUserDefinedBooleanProperty(PROP_DETAIL_TRACE, new EsfBoolean(includeDetailTrace));
		
		LinkedList<Short> limitLogTypes = null;
		if ( ! includeGeneral || ! includeBasicTrace || ! includeDetailTrace ) {
			limitLogTypes = new LinkedList<Short>();
			if ( includeGeneral ) limitLogTypes.add(ActivityLog.LOG_TYPE_TRANSACTION_GENERAL);
			if ( includeBasicTrace ) limitLogTypes.add(ActivityLog.LOG_TYPE_TRANSACTION_ACTION_BASIC_TRACE);
			if ( includeDetailTrace ) limitLogTypes.add(ActivityLog.LOG_TYPE_TRANSACTION_ACTION_DETAIL_TRACE);
		}
		
		String text = (String)searchText.getValue();
		vaadinUi.getUser().setUserDefinedStringProperty(PROP_TEXT, new EsfString(text));

		container.refresh(TransactionActivityLogInfo.Manager.getMatching(transaction.getId(),esfFromDate,esfToDate,limitLogTypes,text));
		vaadinUi.showStatus(vaadinUi.getMsg("TransactionLogView.findButton.foundCount",container.size()));
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return "";
	}

	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
	class LogTable extends Table {
		private static final long serialVersionUID = 245549301232377010L;

		public LogTable(LogView view, LogContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("TransactionLogView.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("TransactionLogView.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setSizeFull();
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			if ( "timestamp".equals(colId) ) {
				return ((LogBean)rowId).formatTimestamp(vaadinUi.getUser());
			}
			
			if ( "type".equals(colId) ) {
				switch( ((LogBean)rowId).getType() ) {
				case ActivityLog.LOG_TYPE_TRANSACTION_GENERAL: return vaadinUi.getMsg("TransactionLogView.searchBar.searchGeneral.label");
				case ActivityLog.LOG_TYPE_TRANSACTION_ACTION_BASIC_TRACE: return vaadinUi.getMsg("TransactionLogView.searchBar.searchBasicTrace.label");
				case ActivityLog.LOG_TYPE_TRANSACTION_ACTION_DETAIL_TRACE: return vaadinUi.getMsg("TransactionLogView.searchBar.searchDetailTrace.label");
				default: return vaadinUi.getMsg("TransactionLogView.type.unknown.label",((LogBean)rowId).getType());
				}
			}

			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // LogTable

}