// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.log.email;

import java.util.List;

import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.email.OutboundEmailMessage;
import com.esignforms.open.email.OutboundEmailMessageResponse;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.server.ThemeResource;

public class EmailLogContainer extends HierarchicalContainer {
	private static final long serialVersionUID = -8450950530899398892L;

	public EmailLogContainer() throws InstantiationException, IllegalAccessException {
		super();
		// Non-visible, just for us...
		this.addContainerProperty("OutboundEmailMessage", OutboundEmailMessage.class, null);
		this.addContainerProperty("OutboundEmailMessageResponse", OutboundEmailMessageResponse.class, null);
		// For the TreeTable
		this.addContainerProperty("timestamp", EsfDateTime.class, null);
		this.addContainerProperty("status", String.class, "");
		this.addContainerProperty("from", String.class, "");
		this.addContainerProperty("to", String.class, "");
		this.addContainerProperty("subject", String.class, "");
        this.addContainerProperty("icon", ThemeResource.class, null);
	}
	
	public void refresh(List<OutboundEmailMessage> emailList) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		removeAllItems();
		
		if ( emailList == null || emailList.size() == 0 )
			return;
		
		for( OutboundEmailMessage message : emailList )
		{
			Object pid = addItem();
			Item pitem = getItem(pid);
			pitem.getItemProperty("OutboundEmailMessage").setValue(message);
			EsfDateTime timestamp = message.hasBeenSent() ? message.getSentTimestamp() : message.getCreatedTimestamp();
			pitem.getItemProperty("timestamp").setValue(timestamp);
			String val = message.getSendStatus();
			if ( val != null )
				pitem.getItemProperty("status").setValue(val);
			val = message.getEmailFrom();
			if ( val != null )
				pitem.getItemProperty("from").setValue(val);
			val = message.getEmailTo();
			if ( val != null )
				pitem.getItemProperty("to").setValue(val);
			val = message.getEmailSubject();
			if ( val != null )
				pitem.getItemProperty("subject").setValue(val);
			ThemeResource iconThemeResource = new ThemeResource(vaadinUi.getMsg("EmailLogContainer.sentEmail.icon"));
			pitem.getItemProperty("icon").setValue(iconThemeResource);
			
			// Let's get any responses to this email
			List<OutboundEmailMessageResponse> responseList = OutboundEmailMessageResponse.Manager.getAll(message.getId());
			if ( responseList == null || responseList.size() == 0 ) {
				setChildrenAllowed(pid,false);
			} else {
				setChildrenAllowed(pid,true);
				
				for( OutboundEmailMessageResponse response : responseList )
				{
					Object cid = addItem();
					Item citem = getItem(cid);
					citem.getItemProperty("OutboundEmailMessageResponse").setValue(response);
					citem.getItemProperty("timestamp").setValue(response.getReceivedTimestamp());
					citem.getItemProperty("status").setValue(vaadinUi.getEsfapp().getServerMessages().getString("OutboundEmailMessage.sendStatus.response"));
					val = response.getEmailFrom();
					if ( val != null )
						citem.getItemProperty("from").setValue(val);
					val = response.getEmailTo();
					if ( val != null )
						citem.getItemProperty("to").setValue(val);
					val = response.getEmailSubject();
					if ( val != null )
						citem.getItemProperty("subject").setValue(val);
					iconThemeResource = new ThemeResource(vaadinUi.getMsg("EmailLogContainer.responseEmail.icon"));
					citem.getItemProperty("icon").setValue(iconThemeResource);
					setChildrenAllowed(cid,false);
					setParent(cid,pid);
				}
			}
		}
	}
}