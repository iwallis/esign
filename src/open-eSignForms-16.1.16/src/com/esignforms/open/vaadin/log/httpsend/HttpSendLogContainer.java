// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.log.httpsend;

import java.util.List;

import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.integration.httpsend.HttpSendRequest;
import com.esignforms.open.integration.httpsend.HttpSendResponse;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;

public class HttpSendLogContainer extends HierarchicalContainer {
	private static final long serialVersionUID = -4602896701475507087L;

	public HttpSendLogContainer() throws InstantiationException, IllegalAccessException {
		super();
		// Non-visible, just for us...
		this.addContainerProperty("HttpSendRequest", HttpSendRequest.class, null);
		this.addContainerProperty("HttpSendResponse", HttpSendResponse.class, null);
		// For the TreeTable
		this.addContainerProperty("beginTimestamp", EsfDateTime.class, null);
		this.addContainerProperty("endTimestamp", EsfDateTime.class, null);
		this.addContainerProperty("status", String.class, "");
	}
	
	public void refresh(List<HttpSendRequest> httpSendList) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		removeAllItems();
		
		if ( httpSendList == null || httpSendList.size() == 0 )
			return;
		
		for( HttpSendRequest request : httpSendList )
		{
			Object pid = addItem();
			Item pitem = getItem(pid);
			pitem.getItemProperty("HttpSendRequest").setValue(request);
			EsfDateTime beginTimestamp = request.getCreatedTimestamp();
			pitem.getItemProperty("beginTimestamp").setValue(beginTimestamp);
			EsfDateTime endTimestamp = request.getCompletedTimestamp();
			pitem.getItemProperty("endTimestamp").setValue(endTimestamp);
			String status = vaadinUi.getMsg("HttpSendLogContainer.HttpSendRequest.status",request.getNumAttempts(),request.getMaxAttempts(),request.getUrl());
			pitem.getItemProperty("status").setValue(status);
			
			// Let's get any responses to this request
			List<HttpSendResponse> responseList = HttpSendResponse.Manager.getAll(request.getId());
			if ( responseList == null || responseList.size() == 0 ) {
				setChildrenAllowed(pid,false);
			} else {
				setChildrenAllowed(pid,true);
				
				for( HttpSendResponse response : responseList )
				{
					Object cid = addItem();
					Item citem = getItem(cid);
					citem.getItemProperty("HttpSendResponse").setValue(response);
					beginTimestamp = response.getSentTimestamp();
					citem.getItemProperty("beginTimestamp").setValue(beginTimestamp);
					endTimestamp = response.getResponseTimestamp();
					citem.getItemProperty("endTimestamp").setValue(endTimestamp);
					String successFailure = vaadinUi.getPrettyCode().httpSendResponseStatus(response.getStatus());
					String responseSubstring = EsfString.ensureNotNull(response.getHttpResponse());
					responseSubstring = EsfString.truncateTrimmedString(responseSubstring, 20);
					status = vaadinUi.getMsg("HttpSendLogContainer.HttpSendResponse.status",successFailure,response.getHttpStatusCode(),responseSubstring);
					citem.getItemProperty("status").setValue(status);
					setChildrenAllowed(cid,false);
					setParent(cid,pid);
				}
			}
		}
	}
}