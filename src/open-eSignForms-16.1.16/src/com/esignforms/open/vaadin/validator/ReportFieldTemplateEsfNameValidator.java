// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;

/**
 * Validator for a ReportFieldTemplate's EsfName.  Not only must be the EsfName be valid, but it cannot already exist.
 */

public class ReportFieldTemplateEsfNameValidator extends EsfNameValidator {
	private static final long serialVersionUID = 4440068443691804116L;

	String defaultErrorMessage;
	ReportFieldTemplate reportFieldTemplate;

    public ReportFieldTemplateEsfNameValidator(ReportFieldTemplate rft) {
        super();
        this.defaultErrorMessage = super.getErrorMessage();
        this.reportFieldTemplate = rft;
    }
    
	@Override
	public void validate(Object value) throws InvalidValueException {
		setErrorMessage(defaultErrorMessage);
    	super.validate(value);
    	
    	EsfName esfname = (EsfName)value;
    	
    	ReportFieldTemplate checkReportFieldTemplate = ReportFieldTemplate.Manager.getByName(esfname);
		if ( checkReportFieldTemplate != null && ! checkReportFieldTemplate.getId().equals(reportFieldTemplate.getId()) ) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			String label0 = vaadinUi.getMsg("ReportFieldTemplateForm.form.duplicatekey.message.param0");
			setErrorMessage(vaadinUi.getMsg("form.duplicatekey.message",label0,esfname));
			throw new InvalidValueException(getErrorMessage());
		}
    }
}
