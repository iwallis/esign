// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.vaadin.EsfVaadinUI;

/**
 * Validator for a Library dropdown's EsfName.  Not only must be the EsfName be valid, but it cannot already exist
 * in the same library.
 */

public class LibraryDropDownEsfNameValidator extends EsfNameValidator {
	private static final long serialVersionUID = 7342189489060510314L;

	EsfUUID containerId;
	DocumentVersion documentVersion; // used for the document editor where drop downs are not necessarily in the library
	DropDown dropdown;

    public LibraryDropDownEsfNameValidator(EsfUUID containerId, DropDown dropdown) {
        super();
        this.containerId = containerId;
        this.dropdown = dropdown;
    }
    
    public LibraryDropDownEsfNameValidator(DocumentVersion documentVersion, DropDown dropdown) {
        super();
        this.documentVersion = documentVersion;
        this.dropdown = dropdown;
    }
    
	@Override
	public void validate(Object value) throws InvalidValueException {
    	super.validate(value);
    	
    	EsfName esfname = (EsfName)value;
    	
		if ( documentVersion != null ) {
        	DocumentVersion.DocumentDropdown checkDropdown = documentVersion.getDocumentDropdown(esfname);
        	if ( checkDropdown != null && ! checkDropdown.dropdown.getId().equals(dropdown.getId()) ) {
        		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			String label0 = vaadinUi.getMsg("LibDropDownForm.form.duplicatekey.message.param0");
        		throw new InvalidValueException(vaadinUi.getMsg("form.duplicatekey.message",label0,esfname));
        	}
    	} else {
        	DropDown checkDropdown = DropDown.Manager.getByName(containerId, esfname);
        	if ( checkDropdown != null && ! checkDropdown.getId().equals(dropdown.getId()) ) {
        		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			String label0 = vaadinUi.getMsg("LibDropDownForm.form.duplicatekey.message.param0");
        		throw new InvalidValueException(vaadinUi.getMsg("form.duplicatekey.message",label0,esfname));
        	}
    	}
    }
}
