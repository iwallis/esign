// Copyright (C) 2013-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfString;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.validator.AbstractStringValidator;

/**
 * Basic validation for an HTTP(S) URL. It's not rigorous, but just confirms it starts with http:// or https:// or ${ for a field spec.
 * and it can be created into a java.net.URL.
 */

public class HttpUrlValidator extends AbstractStringValidator {
	private static final long serialVersionUID = 8226616285550727777L;

	public HttpUrlValidator() {
        super(EsfVaadinUI.getInstance().getMsg("validator.httpUrl.message"));
    }
    
    public HttpUrlValidator(String errorMessage) {
        super(errorMessage);
    }
    
    @Override
    protected boolean isValidValue(String value) {
    	if ( EsfString.isBlank(value) )
    		return true;
    	
    	String lowerValue = value.toLowerCase();
    	if ( lowerValue.startsWith("${") )
    		return true;
    	
    	if ( ! lowerValue.startsWith("http://") && ! lowerValue.startsWith("https://") )
    		return false;
    	
    	try {
    		new java.net.URL(value);
    	} catch ( java.net.MalformedURLException e ) {
    		return false;
    	}
    	
    	return true;
    }
}
