// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfValue;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.validator.AbstractValidator;
import com.vaadin.ui.AbstractSelect;

/**
 * Validator for a select box. Ensures that the value passed to isValid() is one of the entries in the select box's items.
 */

public class SelectValidator extends AbstractValidator<Object> {
	private static final long serialVersionUID = -8735348237700477086L;

	AbstractSelect select;
	String defaultErrorMessage;

    public SelectValidator(AbstractSelect select) {
    	super(EsfVaadinUI.getInstance().getMsg("validator.select.message"));
    	this.defaultErrorMessage = super.getErrorMessage();
        this.select = select;
    }
    
    
	@Override
	protected boolean isValidValue(Object value) {
    	setErrorMessage(defaultErrorMessage);
    	
    	if ( value == null ) {
    		return select.isNullSelectionAllowed();
    	}
    	if ( value instanceof EsfValue ) {
    		if ( ((EsfValue)value).isNull() )
    			return select.isNullSelectionAllowed();
    	}

    	// Special logic for a multi-select for TwinColSelect and ListSelect that operate on Set/HashSet (order not preserved)
		if ( value instanceof java.util.Set ) {
			java.util.Set<?> set = (java.util.Set<?>)value;
			for( Object setValue : set ) {
		    	if ( ! isValid(setValue) ) {
		    		return false;
		    	}
			}
			return true;
		}
		
    	// Special logic for a multi-select ListBuilder that operates on Collection/List/LinkedList (order preserved)
		if ( value instanceof java.util.Collection ) {
			java.util.Collection<?> list = (java.util.Collection<?>)value;
			for( Object setValue : list ) {
		    	if ( ! isValid(setValue) ) {
		    		return false;
		    	}
			}
			return true;
		}
		
    	for( Object itemId : select.getItemIds() ) {
    		if ( value.equals(itemId) ) {
    			return true;
    		}
    	}
    	
		return false;
	}


	@Override
	public Class<Object> getType() {
		return Object.class;
	}
}
