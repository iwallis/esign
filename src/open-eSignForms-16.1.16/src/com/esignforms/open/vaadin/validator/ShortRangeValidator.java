// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.vaadin.data.validator.RangeValidator;

/**
 * Creates a validator for checking that an Short is within a given range.
 * 
 * Based on Vaadin class com.vaadin.data.validator.IntegerRangeValidator
 */
public class ShortRangeValidator extends RangeValidator<Short> {
	private static final long serialVersionUID = 3903052457626948198L;

	public ShortRangeValidator(String errorMessage, Short minValue, Short maxValue) {
        super(errorMessage, Short.class, minValue, maxValue);
    }

}
