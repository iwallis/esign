// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.user.Group;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.group.GroupBean;

/**
 * Validator for a Group's EsfPathName.  Not only must be the EsfPathName be valid, but it cannot already exist.
 */

public class GroupEsfPathNameValidator extends EsfPathNameValidator {
	private static final long serialVersionUID = -8082363903593626072L;

	GroupBean bean;

    public GroupEsfPathNameValidator(GroupBean bean) {
        super();
        this.bean = bean;
    }
    
	@Override
	public void validate(Object value) throws InvalidValueException {
    	super.validate(value);
    	
    	EsfPathName pathName = (EsfPathName)value;
    	
		Group checkGroup = Group.Manager.getByPathName(pathName);
		if ( checkGroup != null && ! checkGroup.getId().equals(bean.group().getId()) ) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			String label0 = vaadinUi.getMsg("groupForm.form.duplicatekey.message.param0");
			throw new InvalidValueException(vaadinUi.getMsg("form.duplicatekey.message",label0,pathName));
		}
    }
}
