// Copyright (C) 2011 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.validator.RegexpValidator;

/**
 * US Zip code validator for 5 digit zip code, or 5+4 zip code.  
 */

public class USZipCodeValidator extends RegexpValidator {
	private static final long serialVersionUID = -2502105129062254741L;

	public static final String REGEX = "^\\d{5}(-\\d{4})?$";

    public USZipCodeValidator(EsfVaadinUI vaadinUi) {
        super(REGEX, true, vaadinUi.getMsg("validator.postalcode.usa.message"));
    }
    
    public USZipCodeValidator(String errorMessage) {
        super(REGEX, true, errorMessage);
    }
    
    public USZipCodeValidator() {
        super(REGEX, true, "A valid US Zip Code is 5 digits, or 5 digits, a hyphen and 4 more digits.");
    }

}
