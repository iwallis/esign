// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.PropertySet;
import com.esignforms.open.vaadin.EsfVaadinUI;

/**
 * Validator for a Library propertyset's EsfName.  Not only must be the EsfName be valid, but it cannot already exist
 * in the same library.
 */

public class LibraryPropertySetEsfNameValidator extends EsfNameValidator {
	private static final long serialVersionUID = 2344526439929742103L;

	EsfUUID containerId;
	PropertySet propertyset;

    public LibraryPropertySetEsfNameValidator(EsfUUID containerId, PropertySet propertyset) {
        super();
        this.containerId = containerId;
        this.propertyset = propertyset;
    }
    
	@Override
	public void validate(Object value) throws InvalidValueException {
    	super.validate(value);
    	
    	EsfName esfname = (EsfName)value;
    	
    	PropertySet checkDropdown = PropertySet.Manager.getByName(containerId, esfname);
    	if ( checkDropdown != null && ! checkDropdown.getId().equals(propertyset.getId()) ) {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			String label0 = vaadinUi.getMsg("LibPropertySetForm.form.duplicatekey.message.param0");
    		throw new InvalidValueException(vaadinUi.getMsg("form.duplicatekey.message",label0,esfname));
    	}
    }
}
