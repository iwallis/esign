// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.reportTemplate.ReportTemplateBean;

/**
 * Validator for a ReportTemplate's EsfPathName.  Not only must be the EsfPathName be valid, but it cannot already exist.
 */

public class ReportTemplateEsfPathNameValidator extends EsfPathNameValidator {
	private static final long serialVersionUID = -4910389258324214031L;

	ReportTemplateBean bean;

    public ReportTemplateEsfPathNameValidator(ReportTemplateBean bean) {
        super();
        this.bean = bean;
    }
    
	@Override
	public void validate(Object value) throws InvalidValueException {
    	super.validate(value);
    	
    	EsfPathName pathName = (EsfPathName)value;
    	
    	ReportTemplate checkReportTemplate = ReportTemplate.Manager.getByPathName(pathName);
		if ( checkReportTemplate != null && ! checkReportTemplate.getId().equals(bean.duplicatedReportTemplate().getId()) ) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			String label0 = vaadinUi.getMsg("ReportTemplateForm.form.duplicatekey.message.param0");
			throw new InvalidValueException(vaadinUi.getMsg("form.duplicatekey.message",label0,pathName));
		}
    }
}
