// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.prog.DocumentStyle;
import com.esignforms.open.prog.Library;
import com.esignforms.open.vaadin.EsfVaadinUI;

/**
 * Validator for a Library documentstyle's EsfName.  Not only must be the EsfName be valid, but it cannot already exist
 * in the same library.
 */

public class LibraryDocumentStyleEsfNameValidator extends EsfNameValidator {
	private static final long serialVersionUID = 4478046509457379651L;

	Library library;
	DocumentStyle documentstyle;

    public LibraryDocumentStyleEsfNameValidator(Library library, DocumentStyle documentstyle) {
        super();
        this.library = library;
        this.documentstyle = documentstyle;
    }
    
	@Override
	public void validate(Object value) throws InvalidValueException {
    	super.validate(value);
    	
    	EsfName esfname = (EsfName)value;
    	
    	DocumentStyle checkDropdown = DocumentStyle.Manager.getByName(library, esfname);
    	if ( checkDropdown != null && ! checkDropdown.getId().equals(documentstyle.getId()) ) {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			String label0 = vaadinUi.getMsg("LibDocumentStyleForm.form.duplicatekey.message.param0");
    		throw new InvalidValueException(vaadinUi.getMsg("form.duplicatekey.message",label0,esfname));
    	}
    }
}
