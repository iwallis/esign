// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.validator.AbstractStringValidator;

/**
 * Validator for an EsfUUID.
 */

public class EsfUUIDValidator extends AbstractStringValidator {
	private static final long serialVersionUID = -7281764601710801747L;

	public EsfUUIDValidator(String errorMessage) {
        super(errorMessage);
    }
    
    public EsfUUIDValidator() {
        this(EsfVaadinUI.getInstance().getMsg("validator.esfuuid.message"));
    }

    /**
     * Checks if the given string is valid for EsfUUID
     * 
     * @param value String to check. May be null if coming from an optional field.
     * @return true if the string is valid, false otherwise
     */
    @Override
    protected boolean isValidValue(String value) {
    	if ( EsfString.isBlank(value) )
    		return true;
    	EsfUUID id = new EsfUUID(value.trim());
    	return ! id.isNull();
    }

}
