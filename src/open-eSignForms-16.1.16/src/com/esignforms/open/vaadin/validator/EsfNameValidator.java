// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Validator;

/**
 * Validator for an EsfName. On port to Vaadin 7, turned into its own validator rather than extending RegexpValidator
 * since we have to work on model objects that have already been instantiated even though the data entered by the user was never validated yet. Go figure, Vaadin 7!
 */

public class EsfNameValidator implements Validator {
	private static final long serialVersionUID = 5024689476398871695L;

	String errorMessage;
	
	public EsfNameValidator(String errorMessage) {
		this.errorMessage = errorMessage;
    }
    
    public EsfNameValidator() {
        this(EsfVaadinUI.getInstance().getMsg("validator.esfname.message"));
    }
    
    protected String getErrorMessage() {
    	return errorMessage;
    }
    protected void setErrorMessage(String v) {
    	errorMessage = v;
    }

	@Override
	public void validate(Object value) throws InvalidValueException {
		if ( value != null ) {
			if ( value instanceof EsfName ) {
				EsfName v = (EsfName)value;
				if ( ! v.isValid() ) {
					throw new InvalidValueException(errorMessage);
				}
			}
		}
	}

}
