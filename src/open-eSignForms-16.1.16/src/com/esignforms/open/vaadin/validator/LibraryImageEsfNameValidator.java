// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.Image;
import com.esignforms.open.vaadin.EsfVaadinUI;

/**
 * Validator for a Library Image's EsfName.  Not only must be the EsfName be valid, but it cannot already exist
 * in the same container (library or document version).
 */

public class LibraryImageEsfNameValidator extends EsfNameValidator {
	private static final long serialVersionUID = 2476162868421650842L;

	EsfUUID containerId;
	DocumentVersion documentVersion; // used for the document editor where images are not necessarily on disk
	Image image;

    public LibraryImageEsfNameValidator(EsfUUID containerId, Image image) {
        super();
        this.containerId = containerId;
        this.image = image;
    }
    
    public LibraryImageEsfNameValidator(DocumentVersion documentVersion, Image image) {
        super();
        this.documentVersion = documentVersion;
        this.image = image;
    }
    
	@Override
	public void validate(Object value) throws InvalidValueException {
    	super.validate(value);
    	
    	EsfName esfname = (EsfName)value;
    	
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	if ( documentVersion != null ) {
        	DocumentVersion.DocumentImage checkImage = documentVersion.getDocumentImage(esfname);
        	if ( checkImage != null && ! checkImage.image.getId().equals(image.getId()) ) {
    			String label0 = vaadinUi.getMsg("LibImageForm.form.duplicatekey.message.param0");
        		throw new InvalidValueException(vaadinUi.getMsg("form.duplicatekey.message",label0,esfname));
        	}
    	} else {
        	Image checkImage = Image.Manager.getByName(containerId, esfname);
        	if ( checkImage != null && ! checkImage.getId().equals(image.getId()) ) {
    			String label0 = vaadinUi.getMsg("LibImageForm.form.duplicatekey.message.param0");
        		throw new InvalidValueException(vaadinUi.getMsg("form.duplicatekey.message",label0,esfname));
        	}
    	}
    }
}
