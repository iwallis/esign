// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Serial;
import com.esignforms.open.vaadin.EsfVaadinUI;

/**
 * Validator for an Serial EsfName.  Not only must be the EsfName be valid, but it cannot already exist
 * in the same container (library or document version).
 */

public class LibrarySerialEsfNameValidator extends EsfNameValidator {
	private static final long serialVersionUID = -5401169149982742592L;

	EsfUUID containerId;
	Serial serial;

    public LibrarySerialEsfNameValidator(EsfUUID containerId, Serial serial) {
        super();
        this.containerId = containerId;
        this.serial = serial;
    }
    
	@Override
	public void validate(Object value) throws InvalidValueException {
    	super.validate(value);
    	
    	EsfName esfname = (EsfName)value;
    	
    	Serial checkTemplate = Serial.Manager.getByName(containerId, esfname);
    	if ( checkTemplate != null && ! checkTemplate.getId().equals(serial.getId()) ) {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			String label0 = vaadinUi.getMsg("LibSerialForm.form.duplicatekey.message.param0");
    		throw new InvalidValueException(vaadinUi.getMsg("form.duplicatekey.message",label0,esfname));
    	}
    }
}
