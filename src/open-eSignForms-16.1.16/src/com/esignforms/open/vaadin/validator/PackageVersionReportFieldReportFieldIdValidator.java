// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.PackageVersionReportField;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.pkg.reportField.PackageReportFieldContainer;
import com.esignforms.open.vaadin.config.pkg.reportField.PackageReportFieldForm;
import com.vaadin.data.validator.AbstractValidator;

/**
 * Validator for a package version report field -- only one can point to any given ReportFieldTemplate id.
 */

public class PackageVersionReportFieldReportFieldIdValidator extends AbstractValidator<EsfUUID> {
	private static final long serialVersionUID = 2353483386812602185L;

	PackageReportFieldForm form;
	PackageReportFieldContainer container;

    public PackageVersionReportFieldReportFieldIdValidator(PackageReportFieldForm form, PackageReportFieldContainer container) {
    	super("");
        this.form = form;
        this.container = container;
    }
    
	@Override
	protected boolean isValidValue(EsfUUID value) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	PackageVersionReportField pvrf = form.getCurrentBean();
    	
    	EsfUUID checkReportFieldTemplateId = (EsfUUID)value;
    	
		for( PackageVersionReportField reportField : container.getItemIds() ) {
			if ( ! reportField.equals(pvrf) ) {
				if ( checkReportFieldTemplateId.equals(reportField.getReportFieldTemplateId()) ) {
			    	setErrorMessage(vaadinUi.getMsg("PackageReportFieldForm.PackageVersionReportFieldReportFieldIdValidator.error",reportField.getFieldName()));
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public Class<EsfUUID> getType() {
		return EsfUUID.class;
	}
}
