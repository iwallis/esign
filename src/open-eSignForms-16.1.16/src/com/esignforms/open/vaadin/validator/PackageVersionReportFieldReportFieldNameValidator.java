// Copyright (C) 2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.prog.PackageVersionReportField;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.pkg.reportField.PackageReportFieldContainer;
import com.esignforms.open.vaadin.config.pkg.reportField.PackageReportFieldForm;

/**
 * Validator for a package version report field -- only one can point to any given ReportFieldTemplate name.
 * That is, you can't map two fields to the same report field.
 */

public class PackageVersionReportFieldReportFieldNameValidator extends EsfNameValidator {
	private static final long serialVersionUID = -62188474774006571L;

	String defaultErrorMessage;
	PackageReportFieldForm form;
	PackageReportFieldContainer container;

    public PackageVersionReportFieldReportFieldNameValidator(PackageReportFieldForm form, PackageReportFieldContainer container) {
    	super();
        this.defaultErrorMessage = super.getErrorMessage();
        this.form = form;
        this.container = container;
    }
    
	@Override
	public void validate(Object value) throws InvalidValueException {
		setErrorMessage(defaultErrorMessage);
		
		EsfName checkReportFieldTemplateName;
		if ( value instanceof EsfName )
	    	checkReportFieldTemplateName = (EsfName)value;    
		else if ( value instanceof String )
			checkReportFieldTemplateName = new EsfName((String)value);
		else
			checkReportFieldTemplateName = new EsfName();
		
    	super.validate(value);
    	
    	ReportFieldTemplate checkReportFieldTemplate = ReportFieldTemplate.Manager.getByName(checkReportFieldTemplateName);
    	if ( checkReportFieldTemplate == null )
    		return;
    	
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	PackageVersionReportField currentReportField = form.getCurrentBean();
    	
		for( PackageVersionReportField reportField : container.getItemIds() ) {
			if ( ! reportField.equals(currentReportField) ) {
				if ( checkReportFieldTemplate.getId().equals(reportField.getReportFieldTemplateId()) ) {
					short order = container.getOrderOfReportFieldTemplateId(checkReportFieldTemplate.getId());
			    	setErrorMessage(vaadinUi.getMsg("PackageReportFieldForm.PackageVersionReportFieldReportFieldNameValidator.error",reportField.getFieldName(),order,checkReportFieldTemplateName));
			    	throw new InvalidValueException(getErrorMessage());
				}
			}
		}
	}
}
