// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.prog.Package;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.pkg.PackageBean;

/**
 * Validator for a Package's EsfPathName.  Not only must be the EsfPathName be valid, but it cannot already exist.
 */

public class PackageEsfPathNameValidator extends EsfPathNameValidator {
	private static final long serialVersionUID = 5593730768353596530L;

	PackageBean bean;
	String defaultErrorMessage;

    public PackageEsfPathNameValidator(PackageBean bean) {
        super();
        this.defaultErrorMessage = super.getErrorMessage();
        this.bean = bean;
    }
    
	@Override
	public void validate(Object value) throws InvalidValueException {
		setErrorMessage(defaultErrorMessage);
    	super.validate(value);
    	
    	EsfPathName pathName = (EsfPathName)value;
    	
		Package checkPackage = Package.Manager.getByPathName(pathName);
		if ( checkPackage != null && ! checkPackage.getId().equals(bean.pkg().getId()) ) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			String label0 = vaadinUi.getMsg("packageForm.form.duplicatekey.message.param0");
			setErrorMessage(vaadinUi.getMsg("form.duplicatekey.message",label0,pathName));
			throw new InvalidValueException(getErrorMessage());
		}
    }
}
