// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.validator.RegexpValidator;

/**
 * New code written based largely on com.vaadin.data.validator.EmailValidator written by IT Mill Ltd. under an Apache 2.0 license
 * that demonstrated how to subclass the Vaadin RegexpValidator.  
 * 
 * It's been adapted for our use.
 */

public class EmailValidator extends RegexpValidator {
	private static final long serialVersionUID = 98800772739115818L;

	public static final String REGEX = 
		"^([a-zA-Z0-9_\\.\\-'+])+@(([a-zA-Z0-9-])+\\.)+([a-zA-Z0-9]{2,4})+$";

    public EmailValidator() {
        super(REGEX, true, EsfVaadinUI.getInstance().getMsg("validator.email.message"));
    }
    
    public EmailValidator(String errorMessage) {
        super(REGEX, true, errorMessage);
    }
}
