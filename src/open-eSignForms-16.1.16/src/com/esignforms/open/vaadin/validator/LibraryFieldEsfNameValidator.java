// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.validator;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.ui.Form;

/**
 * Validator for a field's EsfName.  Not only must be the EsfName be valid, but it cannot already exist
 * in the same field templates map.
 */

public class LibraryFieldEsfNameValidator extends EsfNameValidator {
	private static final long serialVersionUID = -6012378630123383079L;

	java.util.Map<EsfName,FieldTemplate> fieldTemplateMap;
	FieldTemplate fieldTemplate;
	Form form;

    public LibraryFieldEsfNameValidator(java.util.Map<EsfName,FieldTemplate> fieldTemplateMap, FieldTemplate fieldTemplate, Form form) {
        super();
        this.fieldTemplateMap = fieldTemplateMap;
        this.fieldTemplate = fieldTemplate;
        this.form = form;
    }
    
	@Override
	public void validate(Object value) throws InvalidValueException {
    	super.validate(value);
    	
    	EsfName esfname = (EsfName)value;
    	
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	FieldTemplate checkField = fieldTemplateMap.get(esfname);
    	if ( checkField != null && ! checkField.getId().equals(fieldTemplate.getId()) ) {
			String label0 = vaadinUi.getMsg("LibFieldForm.form.duplicatekey.message.param0");
    		throw new InvalidValueException(vaadinUi.getMsg("form.duplicatekey.message",label0,esfname));
    	}
    	
    	// For sign date, the name must end in "Date" such that the leading portion is also a signature field.
    	if ( FieldTemplate.TYPE_SIGN_DATE.equals((String)form.getField("type").getValue()) ) {
    		String fieldNameString = esfname.toPlainString();
    		if ( fieldNameString.toLowerCase().endsWith("date") )
    		{
    			fieldNameString = fieldNameString.substring(0,fieldNameString.length()-4); // remove the Date suffix
    			FieldTemplate checkSignatureField = fieldTemplateMap.get( new EsfName(fieldNameString) );
    			if ( checkSignatureField != null && checkSignatureField.isTypeSignature() )
    				return;
    		}
    		throw new InvalidValueException(vaadinUi.getMsg("LibFieldForm.form.esfname.signDate.invalid"));
    	}
    }
}
