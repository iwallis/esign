// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.todoListing;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.prog.ReportFieldValue;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionFile;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyAssignment;
import com.esignforms.open.runtime.reports.TodoListingInfo;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.runtime.workflow.TransactionEngine;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.TranFilePopupButton;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.BorderStyle;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

public class TodoListingView extends Panel implements EsfView {
	private static final long serialVersionUID = 7544341352034092367L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TodoListingView.class);
	
    final TodoListingView thisView;
    LinkedList<TodoListingTable> todoListingTableList;
    Label todoTableNotice;
    User targetUser;
    
	// For search bar
    VerticalLayout searchBarLayout;
	Button refreshButton;
	NativeSelect usersAllowManageToDoSelect;
	
	public TodoListingView() {
		super();
		thisView = this;
		setStyleName("TodoListingView");
		setSizeFull();
	}
	
	public void setTargetUser(User targetUser) {
		this.targetUser = targetUser;
	}
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();

		try {
			todoListingTableList = new LinkedList<TodoListingTable>();
			
			VerticalLayout layout = new VerticalLayout();
			setContent(layout);
			layout.setWidth(100,Unit.PERCENTAGE);  
			layout.setHeightUndefined(); // leave height undefined so we get scrollbars if lots of different trans queued up
			// leave height undefined so we get scrollbars if lots of different trans queued up
			layout.setSpacing(false);
			layout.setMargin(false);
			
			searchBarLayout = new VerticalLayout();
			searchBarLayout.setWidth(100,Unit.PERCENTAGE);
			searchBarLayout.setMargin(false);
			searchBarLayout.setSpacing(false);
			
			HorizontalLayout searchBar = new HorizontalLayout();
	    	searchBar.addStyleName("searchBar");
	    	searchBar.setSpacing(true);
	    	searchBar.setMargin(false);
	    	searchBarLayout.addComponent(searchBar);
	    	
	    	refreshButton = new Button(vaadinUi.getMsg("button.refresh.label"));
	    	refreshButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.refresh.icon")));
	    	refreshButton.setDescription(vaadinUi.getMsg("button.refresh.tooltip"));
	    	refreshButton.setStyleName(Reindeer.BUTTON_SMALL);
	    	//refreshButton.addStyleName(Reindeer.BUTTON_DEFAULT);
	    	//refreshButton.setClickShortcut(KeyCode.ENTER);
	    	refreshButton.addClickListener( new Button.ClickListener() {			
				private static final long serialVersionUID = 1281265823509867837L;

				@Override
				public void buttonClick(ClickEvent event) {
					buildToDoTables();
				}
			});
	    	searchBar.addComponent(refreshButton);
	    	
	    	// See if this user can manage the To Do of others
	    	User loggedInUser = vaadinUi.getUser();
	    	Collection<User> allUsersAllowManageToDo = User.Manager.getAll(loggedInUser);
	    	Iterator<User> allUserAllowManageToDoIter = allUsersAllowManageToDo.iterator();
	    	while( allUserAllowManageToDoIter.hasNext() ) {
	    		User possibleManageToDoUser = allUserAllowManageToDoIter.next();
	    		if ( ! possibleManageToDoUser.canUserManageToDo(loggedInUser) ) {
	    			allUserAllowManageToDoIter.remove();
	    		}
	    	}
	    	// If we have some, we'll go ahead and add the select box to choose from users they can manage
	    	if ( allUsersAllowManageToDo.size() > 0 ) {
	    		usersAllowManageToDoSelect = new NativeSelect(vaadinUi.getMsg("TodoListingView.searchBar.manageToDo.select.label"));
	    		usersAllowManageToDoSelect.setDescription(vaadinUi.getMsg("TodoListingView.searchBar.manageToDo.select.tooltip"));
	    		usersAllowManageToDoSelect.addValidator(new SelectValidator(usersAllowManageToDoSelect));
	    		usersAllowManageToDoSelect.setNullSelectionAllowed(false); 
	    		usersAllowManageToDoSelect.setImmediate(true);
	    		usersAllowManageToDoSelect.setRequired(true);
	    		usersAllowManageToDoSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
	    		
		    	// They can always manage themselves, too
		    	if ( ! allUsersAllowManageToDo.contains(loggedInUser) ) {
		    		usersAllowManageToDoSelect.addItem(loggedInUser.getId());
		    		usersAllowManageToDoSelect.setItemCaption( loggedInUser.getId(), loggedInUser.getFullDisplayName() );
		    	}
	    		
		    	for( User u : allUsersAllowManageToDo ) {
		    		usersAllowManageToDoSelect.addItem(u.getId());
		    		usersAllowManageToDoSelect.setItemCaption( u.getId(), u.getFullDisplayName() );
		    	}
		    	allUsersAllowManageToDo.clear(); 
		    	allUsersAllowManageToDo = null;
		    	
		    	usersAllowManageToDoSelect.addValueChangeListener(new Property.ValueChangeListener() {
					private static final long serialVersionUID = -5220914276707221927L;

					@Override
					public void valueChange(ValueChangeEvent event) {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						EsfUUID selectedUserId = (EsfUUID)event.getProperty().getValue();
						User selectedUser = User.Manager.getById(selectedUserId);
						if ( selectedUser == null )
							vaadinUi.showWarning(vaadinUi.getMsg("TodoListingView.searchBar.manageToDo.select.nosuchuser.caption"), vaadinUi.getMsg("TodoListingView.searchBar.manageToDo.select.nosuchuser.warning"));
						else {
							setTargetUser(selectedUser);
							buildToDoTables();
						}
					}
		    	});
		    	usersAllowManageToDoSelect.setValue(loggedInUser.getId());
		    	searchBar.addComponent(usersAllowManageToDoSelect);
	    	}

	    	layout.addComponent(searchBarLayout);
			
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("TodoListingView view exception", e.getMessage());
		}
	}
	
	void buildToDoTables() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();
		
		vaadinUi.getMainView().setSearchCurrentModeStyleName(searchBarLayout);
		
		VerticalLayout layout = (VerticalLayout)super.getContent();
		
		if ( todoTableNotice != null ) {
			layout.removeComponent(todoTableNotice);
			todoTableNotice = null;
		}
		
		for( TodoListingTable table : todoListingTableList ) {
			layout.removeComponent(table);
		}
		todoListingTableList.clear();
		
    	String searchType = vaadinUi.isProductionMode() ? vaadinUi.getMsg("transaction.production") : vaadinUi.getMsg("transaction.test");

    	int numTodo = 0;
    	
		List<TodoListingInfo> todoListingInfoList = TodoListingInfo.Manager.getMatching(vaadinUi.isProductionMode(),targetUser.getId(),targetUser.getEmailAddress().getEmailAddress());
		if ( todoListingInfoList.size() == 0 ) {
			todoTableNotice = new Label(vaadinUi.getMsg("TodoListingView.noTransactions.message",searchType));
			layout.addComponent(todoTableNotice);
			layout.setExpandRatio(todoTableNotice, 1);
		} else {
			LinkedList<TodoListingContainer> containerList = new LinkedList<TodoListingContainer>();
			TodoListingContainer currContainer = null;
			EsfUUID currPackageVersionId = new EsfUUID(); // we create a container per package version
			for( TodoListingInfo todoListingInfo : todoListingInfoList ) {
				if ( ! currPackageVersionId.equals(todoListingInfo.getPackageVersionId()) ) {
					currContainer = new TodoListingContainer(todoListingInfo.getReportFieldValueList());
					containerList.add(currContainer);
					currPackageVersionId = todoListingInfo.getPackageVersionId();
				}
				currContainer.addTodo(todoListingInfo);
				++numTodo;
			}
			
			currPackageVersionId = new EsfUUID(); // now we create a table per package version
			ListIterator<TodoListingContainer> containerIter = containerList.listIterator();
			for( TodoListingInfo todoListingInfo : todoListingInfoList ) {
				if ( ! currPackageVersionId.equals(todoListingInfo.getPackageVersionId()) ) {
					TodoListingTable currTable = new TodoListingTable(containerIter.next(),todoListingInfo.getReportFieldValueList());
					currTable.setCaption(vaadinUi.getMsg("TodoListingView.TodoListingTable.caption",todoListingInfo.getTemplateDisplayName(),todoListingInfo.getTemplatePathName().toString(),todoListingInfo.getPackagePathNameVersionWithLabel()));
					todoListingTableList.add(currTable);
					layout.addComponent(currTable);
					currPackageVersionId = todoListingInfo.getPackageVersionId();
				}
			}
		}

		vaadinUi.showStatus(vaadinUi.getMsg("TodoListingView.refreshButton.todoCount",numTodo,searchType));
		vaadinUi.getMainView().setTabTitle(thisView, vaadinUi.getMsg("TodoListingView.afterSearch.tabTitle",numTodo));
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
    	buildToDoTables();
	}

	@Override
	public String checkDirty() {
		return "";
	}
	
	@Override
	public void initView() {
    	if ( targetUser == null )
    		targetUser = EsfVaadinUI.getInstance().getUser();
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
	class TodoListingContainer extends IndexedContainer {
		private static final long serialVersionUID = -3736007959064848043L;

		final TodoListingContainer thisTodoListingContainer;
		
		public TodoListingContainer(List<ReportFieldValue> reportFieldValueList) {
			super();
			thisTodoListingContainer = this;
			addContainerProperty("esf_unlockButton", Button.class, null);
			for( ReportFieldValue reportFieldValue : reportFieldValueList ) {
				ReportFieldTemplate reportFieldTemplate = reportFieldValue.getReportFieldTemplate();
				String fieldName = reportFieldTemplate.getFieldName().toString();
				if ( reportFieldTemplate.isFieldTypeFile() )
					addContainerProperty(fieldName, TranFilePopupButton.class, null); // for a File, we'll actually display a popup button
				else
					addContainerProperty(fieldName, reportFieldTemplate.getFieldValueClass(), null);
			}
			//addContainerProperty("esf_templatePathName", EsfPathName.class, null);
			addContainerProperty("esf_partyName", EsfName.class, null);
			addContainerProperty("esf_statusText", String.class, null);
			addContainerProperty("esf_lastUpdatedTimestamp", EsfDateTime.class, null);
			addContainerProperty("esf_partyDisplayName", String.class, null);
		}
		
		@SuppressWarnings("unchecked")
		public void addTodo(final TodoListingInfo todoListingInfo) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

			Transaction transaction = null;
			
			final Item item = addItem(todoListingInfo.getPickupCode()); // id will be our pickup code
			
			for( ReportFieldValue reportFieldValue : todoListingInfo.getReportFieldValueList() ) {
				ReportFieldTemplate reportFieldTemplate = reportFieldValue.getReportFieldTemplate();
				String fieldName = reportFieldTemplate.getFieldName().toString();
				
				if ( reportFieldTemplate.isFieldTypeFile() ) {
					EsfUUID[] tranFileIds = (EsfUUID[])reportFieldValue.getValues();
					if ( tranFileIds == null || tranFileIds.length < 1 )
						item.getItemProperty(fieldName).setValue(null);
					else {
						if ( transaction == null ) {
							transaction = Transaction.Manager.getById(todoListingInfo.getTransactionId());
							if ( transaction == null ) {
								item.getItemProperty(fieldName).setValue(null);
								continue;
							}
						}
						
						List<TransactionFile> tranFileList = new LinkedList<TransactionFile>();
						for( EsfUUID tranFileId : tranFileIds ) {
							tranFileList.add( transaction.getTransactionFileById(tranFileId));
						}
						
						TranFilePopupButton button = new TranFilePopupButton(transaction,tranFileList);
						item.getItemProperty(fieldName).setValue(button);
						Transaction.Manager.removeFromCache(transaction);
					}
				} else {
					item.getItemProperty(fieldName).setValue(reportFieldValue.getValue());
				}
			}
			//item.getItemProperty("esf_templatePathName").setValue(todoListingInfo.getTemplatePathName());
			item.getItemProperty("esf_partyName").setValue(todoListingInfo.getPartyName());
			item.getItemProperty("esf_statusText").setValue(todoListingInfo.getStatusText());
			item.getItemProperty("esf_lastUpdatedTimestamp").setValue(todoListingInfo.getLastUpdatedTimestamp());
			item.getItemProperty("esf_partyDisplayName").setValue(todoListingInfo.getPartyDisplayName());
			
			// You can only unlock a transaction that has both a user id set, and a to do group id allowing possibly others to process it.
			if ( todoListingInfo.hasUserId() && todoListingInfo.hasToDoGroupId() ) {
		    	Button unlockButton = new Button(vaadinUi.getMsg("TodoListingView.TodoListingTable.button.unlock.button.label"), new ClickListener() {
					private static final long serialVersionUID = -4278142174267977928L;

					@Override
					public void buttonClick(ClickEvent event) {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						
				        ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
				        Connection        con  = pool.getConnection();
				        try {
				        	Transaction unlockTran = Transaction.Manager.getById(con,todoListingInfo.getTransactionId());
				        	if ( unlockTran != null ) {
				        		if ( unlockTran.isEndState() ) {
				        			vaadinUi.showStatus(vaadinUi.getMsg("TodoListingView.TodoListingTable.button.unlock.warn.transactionEndState"));
				        			thisTodoListingContainer.removeItem(todoListingInfo.getPickupCode());
				        		} else {
					        		for( TransactionParty tp : unlockTran.getAllTransactionParties() ) {
					        			if ( todoListingInfo.getPickupCode().equals(tp.getTransactionPartyAssignmentPickupCode()) ) {
					        				if ( ! tp.isActive() ) {
							        			vaadinUi.showStatus(vaadinUi.getMsg("TodoListingView.TodoListingTable.button.unlock.warn.partyEndState"));
							        			thisTodoListingContainer.removeItem(todoListingInfo.getPickupCode());
					        				} else {
						        				TransactionPartyAssignment tpa = tp.getCurrentAssignment();
						        				if ( tpa != null && tpa.hasUserId() && tpa.getUserId().equals(todoListingInfo.getUserId()) ) {
						        					if ( tpa.isEndState() ) {
									        			vaadinUi.showStatus(vaadinUi.getMsg("TodoListingView.TodoListingTable.button.unlock.warn.partyEndState"));
									        			thisTodoListingContainer.removeItem(todoListingInfo.getPickupCode());
						        					} else {
							        					User userWithLock = User.Manager.getById(tpa.getUserId());
							        					String usernameWithLock = (userWithLock == null) ? ("unknown user id " + tpa.getUserId()) : userWithLock.getFullDisplayName();
							        					
							        					EsfName tranPartyName = tp.getPackageVersionPartyTemplate().getEsfName();
							        					
											        	TransactionContext pseudoContext = new TransactionContext(vaadinUi.getUser(),"",tranPartyName,unlockTran.getId(),
											        															  vaadinUi.getRequestIpAddress(), vaadinUi.getRequestHostIp(), vaadinUi.getRequestUserAgent());
											        	TransactionEngine engine = new TransactionEngine(pseudoContext);
											        	engine.queuePartyTransferredEvent(tranPartyName, null);
											        	
											        	Errors errors = new Errors();
											        	engine.doWork(con, errors);
											        	if ( unlockTran.save(con,vaadinUi.getUser()) )
											        	{
							        						unlockTran.logGeneral(con,"Party " + todoListingInfo.getPartyName() + " unlocked from user '" + usernameWithLock + "' via To Do by user: " + vaadinUi.getUser().getFullDisplayName());
							        						vaadinUi.getUser().logConfigChange(con, "Unlocked party " + todoListingInfo.getPartyName() + " from user '" + usernameWithLock + "' via To Do for transaction id: " + unlockTran.getId());
							        						vaadinUi.showStatus(vaadinUi.getMsg("TodoListingView.TodoListingTable.button.unlock.success"));
										        			thisTodoListingContainer.removeItem(todoListingInfo.getPickupCode());
											        	}
						        					}
						        				} else {
								        			vaadinUi.showStatus(vaadinUi.getMsg("TodoListingView.TodoListingTable.button.unlock.warn.partyEndState"));
								        			thisTodoListingContainer.removeItem(todoListingInfo.getPickupCode());
						        				}
					        				}
					        				break;
					        			} // end tran party match on pickup code
					        		} // end for each tran party
				        		} // end tran is active
				        	} // end tran found
							con.commit();
				        } catch(SQLException e) {
				        	_logger.sqlerr("addTodo(unlockButton)", e);
				            pool.rollbackIgnoreException(con,e);
				        } catch(EsfException e) {
				        	_logger.warn("addTodo(unlockButton)", e);
				        	pool.rollbackIgnoreException(con);
				        } finally {
				        	vaadinUi.getEsfapp().cleanupPool(pool,con,null);
				        }
					}
		        }); // end button click listener
		    	
		    	unlockButton.setStyleName(Reindeer.BUTTON_SMALL);
		    	unlockButton.setDescription(vaadinUi.getMsg("TodoListingView.TodoListingTable.button.unlock.button.tooltip"));
		    	unlockButton.setIcon(new ThemeResource(vaadinUi.getMsg("TodoListingView.TodoListingTable.button.unlock.button.icon")));
		    	item.getItemProperty("esf_unlockButton").setValue(unlockButton);
			} else {
				item.getItemProperty("esf_unlockButton").setValue(null);
			}
			
		}
	}
	
	class TodoListingTable extends Table {
		private static final long serialVersionUID = -153716951818804288L;

		public TodoListingTable(TodoListingContainer container, List<ReportFieldValue> reportFieldValueList) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			LinkedList<String> columnFields = new LinkedList<String>();
			LinkedList<String> columnHeaders = new LinkedList<String>();
			LinkedList<Align> columnAlignments = new LinkedList<Align>();
			
			// Put all user-defined fields before common fields
			for( ReportFieldValue reportFieldValue : reportFieldValueList ) {
				ReportFieldTemplate reportFieldTemplate = reportFieldValue.getReportFieldTemplate();
				String fieldName = reportFieldTemplate.getFieldName().toString();
				columnFields.add(fieldName);
				columnHeaders.add(reportFieldTemplate.getFieldLabel());
				if ( reportFieldTemplate.isFieldTypeInteger() || reportFieldTemplate.isFieldTypeDecimal() ) {
					columnAlignments.add(Align.RIGHT);
				} else if ( reportFieldTemplate.isFieldTypeDate() ) {
					columnAlignments.add(Align.CENTER);
				} else {
					columnAlignments.add(Align.LEFT);
				}
			}
			//columnFields.add("esf_templatePathName");
			//columnHeaders.add(vaadinUi.getMsg("TodoListingView.TodoListingTable.columnHeader.templatePathName"));
			//columnAlignments.add(Align.LEFT);

			columnFields.add("esf_partyName");
			columnHeaders.add(vaadinUi.getMsg("TodoListingView.TodoListingTable.columnHeader.partyName"));
			columnAlignments.add(Align.LEFT);
			
			columnFields.add("esf_statusText");
			columnHeaders.add(vaadinUi.getMsg("TodoListingView.TodoListingTable.columnHeader.statusText"));
			columnAlignments.add(Align.LEFT);
			
			columnFields.add("esf_lastUpdatedTimestamp");
			columnHeaders.add(vaadinUi.getMsg("TodoListingView.TodoListingTable.columnHeader.lastUpdatedTimestamp"));
			columnAlignments.add(Align.CENTER);
			
			columnFields.add("esf_partyDisplayName");
			columnHeaders.add(vaadinUi.getMsg("TodoListingView.TodoListingTable.columnHeader.partyDisplayName"));
			columnAlignments.add(Align.LEFT);
			
			// Our unlock button will be last if needed
			columnFields.add("esf_unlockButton");
			columnHeaders.add(vaadinUi.getMsg("TodoListingView.TodoListingTable.columnHeader.unlockButton"));
			columnAlignments.add(Align.CENTER);
			
			String[] columnHeaderArray = new String[columnHeaders.size()];
			columnHeaders.toArray(columnHeaderArray);
			
			setContainerDataSource(container);
			setVisibleColumns(columnFields.toArray());
			setColumnHeaders(columnHeaderArray);
			
			ListIterator<Align> columnAlignmentIter = columnAlignments.listIterator();
			for( String columnField : columnFields ) {
				setColumnAlignment(columnField, columnAlignmentIter.next());
			}
			
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setWidth(100,Unit.PERCENTAGE);
			setPageLength(container.size()+1);
			setSelectable(true);
			setImmediate(true);
	        addValueChangeListener(new Property.ValueChangeListener() {
				private static final long serialVersionUID = -5776902609950006493L;

				public void valueChange(Property.ValueChangeEvent event) {
					String pickupCode = (String)getValue();
    	        	if ( pickupCode == null )
    	        		return;

    	        	Item item = getItem(pickupCode);    	        	
    	        	if ( item == null )
    	        		return;
    	        	
    	    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	        	String url = vaadinUi.getEsfapp().getContextPath() + "/P/" + pickupCode;
    	        	
    	        	Page.getCurrent().open(url, "_blank", 800, 800, BorderStyle.DEFAULT);

    	            select(null);
	            }
	        });
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			if ( "esf_lastUpdatedTimestamp".equals(colId) ) {
				return ((EsfDateTime)property.getValue()).toLogString(vaadinUi.getUser());
			}
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // TodoListingTable

}