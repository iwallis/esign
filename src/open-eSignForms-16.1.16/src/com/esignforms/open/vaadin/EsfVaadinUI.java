// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin;

import java.net.URI;
import java.util.Collection;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.vaadin.hene.popupbutton.PopupButton;

import com.esignforms.open.Errors;
import com.esignforms.open.Version;
import com.esignforms.open.admin.SessionTracker;
import com.esignforms.open.config.MessageFormatFile;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.user.User;
import com.esignforms.open.util.ServletUtil;
import com.esignforms.open.vaadin.config.PrettyCode;
import com.esignforms.open.vaadin.converter.EsfConverterFactory;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.exception.NotLoggedInException;
import com.esignforms.open.vaadin.main.MainView;
import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.data.Validator;
import com.vaadin.server.CustomizedSystemMessages;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.WebBrowser;
import com.vaadin.server.communication.PushConnection;
import com.vaadin.shared.Position;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Field;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TooltipConfiguration;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;

/**
 * An EsfVaadinUI is created for each user who starts up a GUI with its own session.
 * 
 * @author Yozons Inc.
 * 
 */
@PreserveOnRefresh
@Theme("openesf")
public class EsfVaadinUI extends com.vaadin.ui.UI {
	private static final long serialVersionUID = -4908560279865206133L;

	private static final com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(EsfVaadinUI.class);
	
	private static final String PROP_MODE = "EsfVaadinUI/Mode";

	public enum Mode { PRODUCTION, TEST_LIKE_PRODUCTION, TEST };

	private volatile transient String requestIpAddress;
	private volatile transient String requestHost;
	private volatile transient String requestUserAgent;
	private volatile transient String requestReferer;
	private volatile transient String requestContextPath;
	private volatile transient String requestExternalContextPath;
	private volatile transient HttpSession requestHttpSession;

	private transient PrettyCode prettyCode;
	private transient MessageFormatFile vaadinMessages;
	private transient com.esignforms.open.Application esfapp;
	
	CustomizedSystemMessages customizedSystemMessages;
	
	public static final String DEFAULT_LOGOUT_URL = "./logoff.jsp";
	String standardLogoutUrl = DEFAULT_LOGOUT_URL;
	String logoutUrl;
	Mode mode;
	MainView mainView;
	boolean isFirstLoggedInRequest = true;
	
	
	// @return the current application instance	  	
	public static EsfVaadinUI getInstance() { 		
		return (EsfVaadinUI)UI.getCurrent(); 	
	} 	 
	
	@Override
	protected void init(VaadinRequest vaadinRequest) {
		_logger.debug("init(VaadinRequest) on Vaadin " + getVaadinVersion() + "; VaadinRequest contextPath: " + vaadinRequest.getContextPath());
		
		// 3/12/2014 found these as the default values: OpenDelay=750/CloseTimeout=300 MaxWidth=500 QuickOpenDelay=100/QuickOpenTimeout=1000
		// We find tooltips often popup too quickly and interfere with the UI itself.
		TooltipConfiguration tooltipConfig = getTooltipConfiguration();
		tooltipConfig.setQuickOpenDelay(tooltipConfig.getOpenDelay());
		
		reloadAfterSessionReactivation();
		
		setLocale( getEsfapp().getDefaultLocale() );
		getSession().setLocale( getEsfapp().getDefaultLocale() );
		
		VaadinSession vaadinSession = VaadinSession.getCurrent();
		setHttpRequest(vaadinRequest);
		setupCustomizedSystemMessages();
		
		vaadinSession.setErrorHandler( new ErrorHandler() {
			private static final long serialVersionUID = 2240348166075560604L;

			@Override
			public void error(com.vaadin.server.ErrorEvent event) {
				Throwable ex = event.getThrowable();
				_logger.error("init().VaadinSession.ErrorHandler.error()", ex);
				if ( ex instanceof NotLoggedInException ) {
					showWarning( getMsg("UI.NotLoggedInException.caption"), getMsg("UI.NotLoggedInException.message") );
					VaadinSession.getCurrent().close();
				} else {
					showPlatformError(ex.getMessage());
				}
			}
		});
		
		standardLogoutUrl = vaadinMessages.getString("logoutUrl");
		
		// Create our main view that handles all navigation and such for our application
		
		User user = getUser();
        EsfString previousModeString = user == null ? null : user.getUserDefinedStringProperty(PROP_MODE);
        if ( previousModeString != null )
        	setMode(previousModeString.toString());
        else
        	setProductionMode();
		mainView = new MainView();
		setContent(mainView);

		VaadinSession.getCurrent().setConverterFactory( new EsfConverterFactory() );
		
		mainView.restoreView();
		
		// Hack to turn off PUSH on older web browsers...
		if ( isTooOldForPush() ) {
			_logger.warn("init() - Disabling PUSH for UI because isTooOldForPush()..."); 
			getPushConfiguration().setPushMode(PushMode.DISABLED);
		}
	}
	
	boolean isTooOldForPush() {
		WebBrowser wb = Page.getCurrent().getWebBrowser();

		if ( wb.isIE() ) {
			if ( wb.getBrowserMajorVersion() < 10 ) {
				_logger.warn("isTooOldForPush() - TOO OLD: IE < 10: browserApp: " + wb.getBrowserApplication() + "; major:" +
					wb.getBrowserMajorVersion() + "; minor: " + wb.getBrowserMinorVersion());
				return true;
			}
			_logger.debug("isTooOldForPush() - IE SHOULD BE OKAY! for browserApp: " + wb.getBrowserApplication() + "; major:" +
					wb.getBrowserMajorVersion() + "; minor: " + wb.getBrowserMinorVersion());
		} else if ( wb.isSafari() ) {
			if ( wb.getBrowserMajorVersion() < 7 ) {
				_logger.warn("isTooOldForPush() - TOO OLD: Safari < 7: browserApp: " + wb.getBrowserApplication() + "; major:" +
						wb.getBrowserMajorVersion() + "; minor: " + wb.getBrowserMinorVersion());
				return true;
			}
			_logger.debug("isTooOldForPush() - SAFARI SHOULD BE OKAY! for browserApp: " + wb.getBrowserApplication() + "; major:" +
					wb.getBrowserMajorVersion() + "; minor: " + wb.getBrowserMinorVersion());
		} else if ( wb.isFirefox() ) {
			if ( wb.getBrowserMajorVersion() < 31 ) {
				_logger.warn("isTooOldForPush() - TOO OLD: Firefox < 31: browserApp: " + wb.getBrowserApplication() + "; major:" +
						wb.getBrowserMajorVersion() + "; minor: " + wb.getBrowserMinorVersion());
				return true;
			}
			_logger.debug("isTooOldForPush() - FIREFOX SHOULD BE OKAY! for browserApp: " + wb.getBrowserApplication() + "; major:" +
					wb.getBrowserMajorVersion() + "; minor: " + wb.getBrowserMinorVersion());
		} else if ( wb.isOpera() ) {
			if ( wb.getBrowserMajorVersion() < 26 ) {
				_logger.warn("isTooOldForPush() - TOO OLD: Opera < 26: browserApp: " + wb.getBrowserApplication() + "; major:" +
						wb.getBrowserMajorVersion() + "; minor: " + wb.getBrowserMinorVersion());
				return true;
			}
			_logger.debug("isTooOldForPush() - OPERA SHOULD BE OKAY! for browserApp: " + wb.getBrowserApplication() + "; major:" +
					wb.getBrowserMajorVersion() + "; minor: " + wb.getBrowserMinorVersion());
		} else if ( wb.isChrome() ) {
			if ( wb.getBrowserMajorVersion() < 37 ) {
				_logger.warn("isTooOldForPush() - TOO OLD: Chrome < 37: browserApp: " + wb.getBrowserApplication() + "; major:" +
						wb.getBrowserMajorVersion() + "; minor: " + wb.getBrowserMinorVersion());
				return true;
			}
			_logger.debug("isTooOldForPush() - CHROME SHOULD BE OKAY! for browserApp: " + wb.getBrowserApplication() + "; major:" +
					wb.getBrowserMajorVersion() + "; minor: " + wb.getBrowserMinorVersion());
		}

		return false;
	}
	
	public void showPlatformError(String msg) {
		if ( msg == null )
			msg = "";
		showError( getMsg("UI.errorHandler.caption"), getMsg("UI.errorHandler.message",msg) );
	}

	public void reloadAfterSessionReactivation() {
		esfapp = com.esignforms.open.Application.getInstance();
		VaadinSession.getCurrent().setLocale(esfapp.getDefaultLocale());

		// Get the messages we'll use for this vaadin app
		vaadinMessages = new MessageFormatFile("com.esignforms.open.vaadin.messages", getLocale());

		prettyCode = new PrettyCode(this);
	}
	
	@Override
	protected void refresh(VaadinRequest request) {
		PushConnection pc = getPushConnection();
		if ( pc != null && pc.isConnected() ) {
			_logger.debug("Working around missed disconnect on refresh");
			pc.disconnect();
	    }
		super.refresh(request);
	}

	
	public Mode getMode() {
		return mode;
	}
	public void setMode(Mode v) {	
		 if ( v == Mode.PRODUCTION ) {
			 mode = getEsfapp().allowProductionTransactions() ? v : Mode.TEST_LIKE_PRODUCTION;
		 } else {
			 mode = getEsfapp().allowTestTransactions() ? v : Mode.PRODUCTION;
		 }
		
		if ( mainView != null ) 
			mainView.setCurrentModeStyleName();
		User user = getUser();
		if ( user != null )
			user.setUserDefinedStringProperty(PROP_MODE, new EsfString(mode.toString()));
		notifyModeChangeListeners();
	}
	public void setMode(String v) {
		Mode m = Mode.valueOf(v);
		if ( m != null ) {
			setMode(m);
		}
	}
	public void setProductionMode() {
		setMode(Mode.PRODUCTION);
	}
	public void setTestLikeProductionMode() {
		setMode(Mode.TEST_LIKE_PRODUCTION);
	}
	public void setTestMode() {
		setMode(Mode.TEST);
	}
	public boolean isProductionMode() {
		return mode == Mode.PRODUCTION;
	}
	public boolean isTestLikeProductionMode() {
		return mode == Mode.TEST_LIKE_PRODUCTION;
	}
	public boolean isTestMode() {
		return mode == Mode.TEST;
	}
	
	/////////////////   MODE CHANGE LISTENERS
	LinkedList<ModeChangeListener> modeChangeListenerList = new LinkedList<ModeChangeListener>();
	public void addModeChangeListener(ModeChangeListener listener) {
		synchronized(modeChangeListenerList) {
			if ( ! modeChangeListenerList.contains(listener) )
				modeChangeListenerList.add(listener);
		}
	}
	public void removeModeChangeListener(ModeChangeListener listener) {
		synchronized(modeChangeListenerList) {
			modeChangeListenerList.remove(listener);
		}
	}
	void notifyModeChangeListeners() {
		synchronized(modeChangeListenerList) {
			for( ModeChangeListener listener : modeChangeListenerList ) {
				listener.modeChanged(mode);
			}
		}
	}
	
	public interface ModeChangeListener {
		public void modeChanged(Mode newMode);
	};
	
	/////////////////   LIBRARY CHANGE LISTENERS
	LinkedList<LibraryChangeListener> libraryChangeListenerList = new LinkedList<LibraryChangeListener>();
	public void addLibraryChangeListener(LibraryChangeListener listener) {
		synchronized(libraryChangeListenerList) {
			if ( ! libraryChangeListenerList.contains(listener) )
				libraryChangeListenerList.add(listener);
		}
	}
	public void removeLibraryChangeListener(LibraryChangeListener listener) {
		synchronized(libraryChangeListenerList) {
			libraryChangeListenerList.remove(listener);
		}
	}
	public void notifyLibraryChangeListeners(Library library) {
		synchronized(libraryChangeListenerList) {
			for( LibraryChangeListener listener : libraryChangeListenerList ) {
				listener.libraryChanged(library);
			}
		}
	}
	
	public interface LibraryChangeListener {
		public void libraryChanged(Library library);
	};
	
	/////////////////   REPORT TEMPLATE CHANGE LISTENERS
	LinkedList<ReportTemplateChangeListener> reportTemplateChangeListenerList = new LinkedList<ReportTemplateChangeListener>();
	public void addReportTemplateChangeListener(ReportTemplateChangeListener listener) {
		synchronized(reportTemplateChangeListenerList) {
			if ( ! reportTemplateChangeListenerList.contains(listener) )
				reportTemplateChangeListenerList.add(listener);
		}
	}
	public void removeReportTemplateChangeListener(ReportTemplateChangeListener listener) {
		synchronized(reportTemplateChangeListenerList) {
			reportTemplateChangeListenerList.remove(listener);
		}
	}
	public void notifyReportTemplateChangeListeners(ReportTemplate report) {
		synchronized(reportTemplateChangeListenerList) {
			for( ReportTemplateChangeListener listener : reportTemplateChangeListenerList ) {
				listener.reportTemplateChanged(report);
			}
		}
	}
	
	public interface ReportTemplateChangeListener {
		public void reportTemplateChanged(ReportTemplate report);
	};
	
	/////////////////   TRANSACTION TEMPLATE CHANGE LISTENERS
	LinkedList<TransactionTemplateChangeListener> transactionTemplateChangeListenerList = new LinkedList<TransactionTemplateChangeListener>();
	public void addTransactionTemplateChangeListener(TransactionTemplateChangeListener listener) {
		synchronized(transactionTemplateChangeListenerList) {
			if ( ! transactionTemplateChangeListenerList.contains(listener) )
				transactionTemplateChangeListenerList.add(listener);
		}
	}
	public void removeTransactionTemplateChangeListener(TransactionTemplateChangeListener listener) {
		synchronized(transactionTemplateChangeListenerList) {
			transactionTemplateChangeListenerList.remove(listener);
		}
	}
	public void notifyTransactionTemplateChangeListeners(TransactionTemplate tranTemplate) {
		synchronized(transactionTemplateChangeListenerList) {
			for( TransactionTemplateChangeListener listener : transactionTemplateChangeListenerList ) {
				listener.transactionTemplateChanged(tranTemplate);
			}
		}
	}
	
	public interface TransactionTemplateChangeListener {
		public void transactionTemplateChanged(TransactionTemplate tranTemplate);
	};
	

	/////////////////   END LISTENERS

	
    public String getThemeBase() {
        try {
            URI uri = new URI(requestExternalContextPath + "/VAADIN/themes/" + getTheme() + "/");
            String themeBase = uri.normalize().toString();
            _logger.debug("getThemeBase() - " + themeBase);
            return themeBase;
        } catch (Exception e) {
            _logger.error("getThemeBase() - Theme base location could not be resolved:" + e);
        }
        String themeBase = "/VAADIN/themes/" + getTheme() + "/";
        _logger.debug("getThemeBase() trying with default - " + themeBase);
        return themeBase;
    }

	@Override
	public void close() {
		_logger.debug("close()");
		User user = getLoggedInUser();
		if ( user != null ) {
			user.logLoginLogoff("EsfVaadinUI closed on session expiration while user was still logged in for " + user.getFullDisplayName());
			removeSessionAttribute(SessionTracker.USER_SESSION_ATTRIBUTE_NAME);
		}
		
		super.close();
	}
	
	public String getVersion() {
		return Version.getVersionString();
	}
	public String getVaadinVersion() {
		return com.vaadin.shared.Version.getFullVersion();
	}
	
	@Override
	public void detach() {
		_logger.debug("detach()");
	}

	public synchronized void setHttpRequest(VaadinRequest vaadinRequest) {
		HttpServletRequest request = (HttpServletRequest)vaadinRequest;

		String localRequestUrl = ServletUtil.getLocalRequestUrl(request);
		
		setRequestIpAddress(request.getRemoteAddr());
		setRequestHost(request.getRemoteHost());
		try {
			// If we have no host, let's see if we can resolve it further
			if (requestIpAddress.equals(requestHost)) {
				java.net.InetAddress iaddr = java.net.InetAddress.getByName(requestIpAddress);
				setRequestHost(iaddr.getHostName());
			}
		} catch (java.net.UnknownHostException e) {
		}

		setRequestUserAgent(request.getHeader("user-agent"));
		setRequestReferer(request.getHeader("Referer"));
		setRequestContextPath(request.getContextPath());
		setRequestExternalContextPath(ServletUtil.getExternalUrlContextPath(request));
		setHttpSession(request.getSession());
		
		_logger.debug("setHttpRequest() requestUrl: " + localRequestUrl
				+ "; hostIp: " + getRequestHostIp() 
				+ "; useragent: " + getRequestUserAgent() 
				+ "; referer: " + getRequestReferer() 
				+ "; loggedInUser: " + getLoggedInUserFullDisplayName());
		
		// This can be called even before our main view has been created in our init() method
		if ( mainView != null ) {
			if ( isFirstLoggedInRequest ) {
				if ( isUserLoggedIn() ) {
					_logger.debug("setHttpRequest() - main view created, isFirstLoggedInRequest & isUserLoggedIn");
					mainView.updateMainViewInfo();
					isFirstLoggedInRequest = false;
				}
			}
			mainView.updateLastServerCommunications(this);
		}
	}
	
	public String getRequestIpAddress() {
		return requestIpAddress;
	}

	protected void setRequestIpAddress(String v) {
		if (v != null) {
			requestIpAddress = v;
		}
	}

	public String getRequestHost() {
		return requestHost;
	}

	protected void setRequestHost(String v) {
		if (v != null) {
			requestHost = v;
		}
	}

	public String getRequestHostIp() {
		return (requestIpAddress != null && requestIpAddress
				.equals(requestHost)) ? requestIpAddress : requestHost + " ("
				+ requestIpAddress + ")";
	}

	public String getRequestUserAgent() {
		return requestUserAgent;
	}

	protected void setRequestUserAgent(String v) {
		if (v != null) {
			requestUserAgent = v;
		}
	}

	public String getRequestReferer() {
		return requestReferer;
	}

	protected void setRequestReferer(String v) {
		if (v != null) {
			requestReferer = v;
		}
	}

	public String getRequestContextPath() {
		return requestContextPath;
	}

	protected void setRequestContextPath(String v) {
		if (v != null) {
			requestContextPath = v;
		}
	}

	public String getRequestExternalContextPath() {
		return requestExternalContextPath;
	}

	protected void setRequestExternalContextPath(String v) {
		if (v != null) {
			requestExternalContextPath = v;
		}
	}

	public HttpSession getHttpSession() {
		return requestHttpSession;
	}
	public void setHttpSession(HttpSession v) {
		if (v != null) {
			requestHttpSession = v;
		}
	}

	/**
	 * Gets an object from the session object by name.
	 * 
	 * @param name
	 *            the String name of the session attribute previously stored
	 * @return the Object associated with the name; null if no such object
	 */
	public Object getSessionAttribute(final String name) {
		try {
			HttpSession sess = getHttpSession();
			return sess == null ? null : sess.getAttribute(name);
		} catch (IllegalStateException e) {
			return null;
		}
	}

	public void setSessionAttribute(final String name, Object object) {
		try {
			HttpSession sess = getHttpSession();
			if ( sess != null )
				sess.setAttribute(name, object);
		} catch (IllegalStateException e) {
			// ignore
		}
	}

	public void removeSessionAttribute(final String name) {
		try {
			HttpSession sess = getHttpSession();
			if ( sess != null )
				sess.removeAttribute(name);
		} catch (IllegalStateException e) {
			// ignore
		}
	}

	public void endSession() {
		try {
			HttpSession sess = getHttpSession();
			if ( sess != null )
				sess.invalidate();
		} catch (IllegalStateException e) {
			// ignore
		}
	}

	/**
	 * Gets the user object from the session.
	 * 
	 * @return null if not present; else the user's object
	 */
	public User getUser() {
		return (User)getSessionAttribute(SessionTracker.USER_SESSION_ATTRIBUTE_NAME);
	}
	
	public void ensureLoggedIn() {
		if ( getLoggedInUser() == null )
			throw new NotLoggedInException();
	}

	/**
	 * Determines the User in the session is logged in or not.
	 * 
	 * @return true if the session has a User object stored that is logged in
	 */
	public boolean isUserLoggedIn() {
		User user = getUser();
		return user != null && user.hasLoginTimestamp();
	}

	/**
	 * Gets the logged in user object.
	 * 
	 * @return null if not logged in; else the logged in user's object
	 */
	public User getLoggedInUser() {
		User user = getUser();
		return user != null && user.hasLoginTimestamp() ? user : null;
	}

	/**
	 * Determines the User in the session is logged in or not and if so returns the user's full display name.
	 * 
	 * @return true the name of the logged in user
	 */
	public String getLoggedInUserFullDisplayName() {
		return isUserLoggedIn() ? getUser().getFullDisplayName() : "(Not logged in)";
	}

	public void logoffUser() {
		User user = getUser();
		
		if ( user != null && user.hasLoginTimestamp() ) {
			mainView.saveView();
			String msg = "User " + user.getFullDisplayName() + " logoff via UI from IP: " + getRequestHostIp(); 
			user.logLoginLogoff(msg);
			esfapp.getActivityLog().logSystemUserActivity(msg); 
			user.logoff();
			removeSessionAttribute(SessionTracker.USER_SESSION_ATTRIBUTE_NAME);
		}
		
		// Hack to turn off PUSH on all UIs first. Doing it this way to try to avoid spurious PUSH sessions reactivating.
		for( final UI ui : VaadinSession.getCurrent().getUIs() ) {
			ui.access(new Runnable() {
				@Override
				public void run() {
					ui.getPushConfiguration().setPushMode(PushMode.DISABLED);
				}
			});
		}
		// Force all UIs to be logged off when any UI logs off
		for( final UI ui : VaadinSession.getCurrent().getUIs() ) {
			ui.access(new Runnable() {
				@Override
				public void run() {
					if ( ui instanceof EsfVaadinUI ) {
						EsfVaadinUI vui = (EsfVaadinUI)ui;
						vui.setLogoffUrlNormal();
						vui.getPage().setLocation(logoutUrl); // set the logoff page before we close the session
				    }
				}
			});
		}
		
		VaadinSession.getCurrent().close();
	}

	public void setLogoffUrlNormal() {
		setLogoutURL(getRequestExternalContextPath() + "/" + standardLogoutUrl);
	}

	public void setLogoutURL(String url) {
		logoutUrl = url;
	}
	
	public CustomizedSystemMessages getCustomizedSystemMessages() {
		return customizedSystemMessages;
	}
	public static CustomizedSystemMessages SetupCustomizedSystemMessages(String url) {
		CustomizedSystemMessages csm = new CustomizedSystemMessages();
		csm.setSessionExpiredURL( ServletUtil.appendUrlParam(url, "t", "to") );
		csm.setCommunicationErrorURL( ServletUtil.appendUrlParam(url, "t", "to") );
		csm.setInternalErrorURL( ServletUtil.appendUrlParam(url, "t", "err") );
		csm.setOutOfSyncURL( ServletUtil.appendUrlParam(url, "t", "err") );
		csm.setCookiesDisabledURL( ServletUtil.appendUrlParam(url, "t", "err") );
		return csm;
	}
	void setupCustomizedSystemMessages() {
		HttpSession sess = getHttpSession();
		String url;
		if (sess != null) {
			url = getRequestExternalContextPath() + "/" + standardLogoutUrl + "?min=" + (sess.getMaxInactiveInterval() / 60);
		} else {
			url = getRequestExternalContextPath() + "/" + standardLogoutUrl;
		}
		customizedSystemMessages = SetupCustomizedSystemMessages(url);
		setLogoutURL(url);
	}
	
	public Window getWindow(AbstractComponent source) {
		return source.findAncestor(Window.class);
	}
	public Window getWindow() {
		return findAncestor(Window.class);
	}
	public ConfirmDiscardFormChangesWindow getConfirmDiscardFormChangesWindow() {
		return findAncestor(ConfirmDiscardFormChangesWindow.class);
	}
	public ConfirmDiscardFormChangesWindow getConfirmDiscardFormChangesWindow(AbstractComponent source) {
		return source.findAncestor(ConfirmDiscardFormChangesWindow.class);
	}
	public PopupButton getPopupButton(AbstractComponent source) {
		return source.findAncestor(PopupButton.class);
	}

	public PrettyCode getPrettyCode() {
		return prettyCode;
	}
	
	public MessageFormatFile getVaadinMessages() {
		return vaadinMessages;
	}
	public String[] getStringArray(String key) {
		return vaadinMessages.getStringArray(key);
	}
	public String getMsg(String key) {
		return vaadinMessages.getString(key);
	}
	public String getMsg(String key, Object...params) {
		return vaadinMessages.getString(key,params);
	}
	
	public String getServerMsg(String key) {
		return getEsfapp().getServerMessages().getString(key);
	}
	public String getServerMsg(String key, Object...params) {
		return getEsfapp().getServerMessages().getString(key,params);
	}
	
	public com.esignforms.open.Application getEsfapp() {
		return esfapp;
	}

    public MainView getMainView() {
        return mainView;
    }
	
	public void showHtmlMessage(String caption, String htmlMessage) {
		Notification n = new Notification(caption,htmlMessage,Notification.Type.HUMANIZED_MESSAGE);
		n.setHtmlContentAllowed(true);
		n.show(Page.getCurrent());
	}

	public void showMessage(String caption, String message) {
		Notification.show(caption, message, Notification.Type.HUMANIZED_MESSAGE);
	}

	public void showMessage(String message) {
		showMessage(null,message);
	}

	public void showHtmlWarning(String caption, String htmlMessage) {
		Notification n = new Notification(caption,htmlMessage,Notification.Type.WARNING_MESSAGE);
		n.setHtmlContentAllowed(true);
		n.show(Page.getCurrent());
	}

	public void showWarning(String caption, String message) {
		Notification.show(caption, message, Notification.Type.WARNING_MESSAGE);
	}

	public void showHtmlError(String caption, String htmlMessage) {
		Notification n = new Notification(caption,htmlMessage,Notification.Type.ERROR_MESSAGE);
		n.setHtmlContentAllowed(true);
		n.show(Page.getCurrent());
	}

	public void showError(String caption, String message) {
		Notification.show(caption, message, Notification.Type.ERROR_MESSAGE);
	}

	public void showHtmlStatus(String caption, String htmlMessage) {
		Notification n = new Notification(caption,htmlMessage,Notification.Type.TRAY_NOTIFICATION);
		n.setPosition(Position.TOP_RIGHT);
		n.setHtmlContentAllowed(true);
		n.show(Page.getCurrent());
	}

	public void showStatus(String caption, String message) {
		Notification n = new Notification(caption,message,Notification.Type.TRAY_NOTIFICATION);
		n.setPosition(Position.TOP_RIGHT);
		n.show(Page.getCurrent());
	}

	public void showHtmlStatus(String htmlMessage) {
		Notification n = new Notification(null,htmlMessage,Notification.Type.TRAY_NOTIFICATION);
		n.setPosition(Position.TOP_RIGHT);
		n.setHtmlContentAllowed(true);
		n.show(Page.getCurrent());
	}

	public void showStatus(String message) {
		Notification n = new Notification(null,message,Notification.Type.TRAY_NOTIFICATION);
		n.setPosition(Position.TOP_RIGHT);
		n.show(Page.getCurrent());
	}

	public void show(Errors errors) {
		if (errors.hasError()) {
			showHtmlError("Error", errors.toHtml());
		} else if (errors.hasWarning()) {
			showHtmlWarning("Warning", errors.toHtml());
		} else {
			showHtmlMessage("Information", errors.toHtml());
		}
	}

	public void addWindowToUI(Window addWindow) {
		UI.getCurrent().addWindow(addWindow);
	}
	
	/**
	 * Utility routine to remove all validators from a given field.
	 * @param field the Field that will have all validators removed from it.
	 */
    public void removeAllValidators(Field<?> field) {
    	if ( field == null ) return;
		try { field.discard(); } catch( Exception e ) {};
    	Collection<Validator> validators = field.getValidators();
    	if ( validators == null || validators.size() == 0 )
    		return;
    	for( Validator v : (new LinkedList<Validator>(validators)) ) {
    		field.removeValidator(v);
    	}
    }

}
	
