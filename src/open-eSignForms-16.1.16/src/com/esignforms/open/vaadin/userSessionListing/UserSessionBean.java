// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.userSessionListing;

import java.io.Serializable;

import com.esignforms.open.admin.SessionTracker;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

public class UserSessionBean implements Serializable {
	private static final long serialVersionUID = 4250405919248694957L;

	transient SessionTracker.Session session;
	UserSessionView view;
	Button endSessionButton;
	
	public UserSessionBean( SessionTracker.Session sessionParam, UserSessionView viewParam ) {
		this.session = sessionParam;
		this.view = viewParam;
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		if ( vaadinUi.getEsfapp().getUI().canDelete(vaadinUi.getLoggedInUser(), com.esignforms.open.user.UI.UI_USER_SESSION_VIEW) ) {
			this.endSessionButton = new Button(vaadinUi.getMsg("UserSessionView.button.endSession.label"), new ClickListener() {
				private static final long serialVersionUID = 7040062038263894897L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					if ( session != null && session.getSession() != null ) {
						try {
							User user = vaadinUi.getLoggedInUser();
							String message = "User " + user.getFullDisplayName() + " forcibly ended the web session for name: " + session.getName() + "; party: " + session.getPartyName() + "; email: "+ session.getEmail() + "; started: " + session.getStartedDateTimeString(user);
							user.logSecurity(message);
							vaadinUi.getEsfapp().getActivityLog().logSystemUserActivity(message);
							session.getSession().invalidate();
							view.loadSessions();
						} catch( IllegalStateException e ) {}
					}
				}
	        });
			this.endSessionButton.setStyleName(Reindeer.BUTTON_SMALL);
			this.endSessionButton.setDescription(vaadinUi.getMsg("UserSessionView.button.endSession.tooltip"));
			this.endSessionButton.setIcon(new ThemeResource(vaadinUi.getMsg("UserSessionView.button.endSession.icon")));
		} else {
			this.endSessionButton = null;
		}
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o !=null && o instanceof UserSessionBean )
			return session.equals(((UserSessionBean)o).session);
		return false;
	}
	@Override
    public int hashCode() {
    	return session == null ? 0 : session.hashCode();
    }
	public int compareTo(UserSessionBean o) {
		return session == null ? -1 : session.getSessionId().compareTo(o.session.getSessionId());
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public SessionTracker.Session session() {
		return session;
	}

	public String getSessionId()
	{
		return session.getSessionId();
	}
	
	public EsfDateTime getStartedTimestamp()
	{
		return session.getStarted();
	}
	public String formatStartedTimestamp(User forUser) {
		EsfDateTime t = getStartedTimestamp();
		return t == null ? "" : t.toLogString(forUser);
	}
	
	public EsfDateTime getLastAccessedTimestamp()
	{
		return session.getLastAccessed();
	}
	public String formatLastAccessedTimestamp(User forUser) {
		EsfDateTime t = getLastAccessedTimestamp();
		return t == null ? "" : t.toLogString(forUser);
	}
	
	public int getSessionTimeoutMinutes()
	{
		return session.getSessionTimeoutMinutes();
	}
	
	public String getName()
	{
		return session.getName();
	}

	public String getEmail()
	{
		return session.getEmail();
	}

	public String getIpAddress()
	{
		return session.getSessionIP();
	}
	
	public Button getEndSessionButton()
	{
		return endSessionButton;
	}
}