// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.userSessionListing;

import com.esignforms.open.admin.SessionTracker;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Property;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class UserSessionView extends Panel implements EsfView {
	private static final long serialVersionUID = 6674831628478038802L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(UserSessionView.class);
	
    final UserSessionView thisView;
    UserSessionContainer container;
    UserSessionTable table;
	
	// For search bar
	Button refreshButton;
	
	public UserSessionView() {
		super();
		thisView = this;
		this.setIcon(null);
		setStyleName("UserSessionView");
		setSizeFull();
	}

	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();

		try {
			container = new UserSessionContainer(this);
			
			VerticalLayout layout = new VerticalLayout();
			setContent(layout);
			layout.setSizeFull();
			layout.setSpacing(false);
			layout.setMargin(false);

	    	HorizontalLayout searchBar = new HorizontalLayout();
	    	searchBar.addStyleName("searchBar");
	    	searchBar.setSpacing(true);
	    	searchBar.setMargin(false);

	    	refreshButton = new Button(vaadinUi.getMsg("button.refresh.label"));
	    	refreshButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.refresh.icon")));
	    	refreshButton.setDescription(vaadinUi.getMsg("button.refresh.tooltip"));
	    	refreshButton.setStyleName(Reindeer.BUTTON_SMALL);
	    	//refreshButton.addStyleName(Reindeer.BUTTON_DEFAULT);
	    	//refreshButton.setClickShortcut(KeyCode.ENTER);
	    	refreshButton.addClickListener( new Button.ClickListener() {			
				private static final long serialVersionUID = -5025159806514900573L;

				@Override
				public void buttonClick(ClickEvent event) {
					loadSessions();
				}
			});
	    	searchBar.addComponent(refreshButton);

	    	table = new UserSessionTable(this,container);
	    	
	    	layout.addComponent(searchBar);
	    	layout.addComponent(table);
	    	layout.setExpandRatio(table, 1);
			
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("UserSessionView view exception", e.getMessage());
		}
	}
	
	public void loadSessions() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		vaadinUi.ensureLoggedIn();
		SessionTracker sessionTracker = vaadinUi.getEsfapp().getSessionTracker();
		container.refresh(sessionTracker.getSessionsArray());
		vaadinUi.showStatus(vaadinUi.getMsg("UserSessionView.refreshButton.sessionCount",container.size()));
		vaadinUi.getMainView().setTabTitle(thisView, vaadinUi.getMsg("UserSessionView.afterSearch.tabTitle",container.size()));
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		loadSessions();
	}

	@Override
	public String checkDirty() {
		return "";
	}
	
	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
	class UserSessionTable extends Table {
		private static final long serialVersionUID = -160099671570950550L;

		public UserSessionTable(UserSessionView view, UserSessionContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("UserSessionView.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("UserSessionView.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setColumnAlignment("endSessionButton", Align.CENTER);
			setSizeFull();
			setSelectable(false);
			setImmediate(false);
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			if ( "lastAccessedTimestamp".equals(colId) ) {
				return ((UserSessionBean)rowId).formatLastAccessedTimestamp(vaadinUi.getUser());
			} 
			if ( "startedTimestamp".equals(colId) ) {
				return ((UserSessionBean)rowId).formatStartedTimestamp(vaadinUi.getUser());
			}
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // UserSessionTable

}