// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.ButtonMessageInfo;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentInfo;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.prog.PackageVersionPartyTemplateDocumentParty;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.PackagePartyEsfNameValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.FieldSpecTextField;
import com.esignforms.open.vaadin.widget.ListSelectDropDown;
import com.esignforms.open.vaadin.widget.ListSelectValid;
import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * The PackagePartyToDocumentPartyForm is used to map the common package parties to the various document parties configured in each document.
 * It is also possible to map a package party to a document, but not any particular party of the document, to create a view-only access to that document.
 * @author Yozons Inc.
 */
public class PackagePartyToDocumentPartyForm extends Form implements EsfView, ClickListener {

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackagePartyToDocumentPartyForm.class);
	
	Button saveButton;
	Button cancelButton;
	
	PackageVersionBean packageVersionBean;
	PackageVersion packageVersion;
	EsfUUID packageVersionPartyTemplateId;
	PackageVersionPartyTemplate packageVersionPartyTemplate;
	
	class DocumentAndParty implements java.io.Serializable {
		private static final long serialVersionUID = 4194035409152503643L;

		public EsfUUID documentId;
		public EsfName partyName; 
		public String label; // may be null for values
		
		public DocumentAndParty(EsfUUID documentId, EsfName partyName, String label) {
			this.documentId = documentId;
			this.partyName = partyName;
			this.label = label;
		}
		@Override
		public boolean equals(Object o) {
			if ( o instanceof DocumentAndParty ) {
				DocumentAndParty dp = (DocumentAndParty)o;
				return documentId.equals(dp.documentId) && partyName.equals(dp.partyName);	
			}
			return false;
		}
		public boolean equals(PackageVersionPartyTemplateDocumentParty mapping) {
			return documentId.equals(mapping.getDocumentId()) && mapping.isMatchingDocumentPartyTemplateName(partyName);
		}
		@Override
		public int hashCode() {
			return documentId.hashCode() + partyName.hashCode();
		}
		public boolean isViewOnly() {
			return PartyTemplate.ESF_PARTY_NAME_VIEW_ONLY.equals(partyName);
		}
		public boolean isViewOptional() {
			return PartyTemplate.ESF_PARTY_NAME_VIEW_OPTIONAL.equals(partyName);
		}
		public boolean isView() {
			return isViewOnly() || isViewOptional();
		}
		public boolean isEsfReportsAccess() {
			return PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS.equals(partyName);
		}
	}

	List<DocumentAndParty> allDocumentsAndParties;
	HashSet<DocumentAndParty> selectedDocumentsAndParties; // contains the selected documents and parties for this package party
	HashSet<String> selectedRenotifySpec; // contains the selected renotification intervals
	
	List<EsfName> allPartyTemplateEsfNames;
	List<EsfName> allEmailTemplateEsfNames;

	ConfirmDiscardFormChangesWindow parentWindow;
    GridLayout layout;
    

    public PackagePartyToDocumentPartyForm(PackageVersionBean packageVersionBean, EsfUUID packageVersionPartyTemplateId) {
    	setStyleName("PackagePartyToDocumentPartyForm");
    	this.packageVersionBean = packageVersionBean;
    	this.packageVersionPartyTemplateId = packageVersionPartyTemplateId;
     }
    
	@SuppressWarnings("unchecked")
	public void buttonClick(ClickEvent event) {
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
        		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
        		vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
			boolean isReserved = packageVersionPartyTemplate.hasReservedPrefix();
			
			EsfName packagePartyName = new EsfName((String)getField("packagePartyName").getValue());
			packageVersionPartyTemplate.setEsfName(packagePartyName);
			
			String packagePartyDisplayName = (String)getField("packagePartyDisplayName").getValue();
			packageVersionPartyTemplate.setDisplayName(packagePartyDisplayName);
			
			String landingPage = (String)getField("landingPage").getValue();
			packageVersionPartyTemplate.setLandingPage(landingPage);
			
			EsfUUID overridePackageDocumentId = (EsfUUID)getField("overridePackageDocumentId").getValue();
			packageVersionPartyTemplate.setOverridePackageDocumentId(overridePackageDocumentId);
			
			EsfUUID overrideButtonMessageId = (EsfUUID)getField("overrideButtonMessageId").getValue();
			packageVersionPartyTemplate.setOverrideButtonMessageId(overrideButtonMessageId);

			Boolean requireLogin = new Boolean( (Boolean)getField("requireLogin").getValue());
			packageVersionPartyTemplate.setRequireLogin(requireLogin.booleanValue());
			
			Boolean allowDeleteTranIfUnsigned = new Boolean( (Boolean)getField("allowDeleteTranIfUnsigned").getValue());
			packageVersionPartyTemplate.setAllowDeleteTranIfUnsigned(allowDeleteTranIfUnsigned.booleanValue());
			
			EsfName libraryPartyTemplateTodoEsfName = (EsfName)getField("libraryPartyTemplateTodoEsfName").getValue();
			packageVersionPartyTemplate.setLibraryPartyTemplateTodoEsfName(libraryPartyTemplateTodoEsfName);

			Boolean notifyAllTodo = new Boolean( (Boolean)getField("notifyAllTodo").getValue());
			packageVersionPartyTemplate.setNotifyAllTodo(notifyAllTodo.booleanValue());
			
			EsfName libraryEmailTemplateEsfName = (EsfName)getField("libraryEmailTemplateEsfName").getValue();
			packageVersionPartyTemplate.setLibraryEmailTemplateEsfName(libraryEmailTemplateEsfName);

			String emailFieldExpression = (String)getField("emailFieldExpression").getValue();
			packageVersionPartyTemplate.setEmailFieldExpression(emailFieldExpression);
			
			String completedUrlFieldExpression = (String)getField("completedUrlFieldExpression").getValue();
			packageVersionPartyTemplate.setCompletedUrlFieldExpression(completedUrlFieldExpression);
			
			String notCompletedUrlFieldExpression = (String)getField("notCompletedUrlFieldExpression").getValue();
			packageVersionPartyTemplate.setNotCompletedUrlFieldExpression(notCompletedUrlFieldExpression);
			
			Set<String> updatedSelectedRenotifySpec = (java.util.Set<String>)getField("selectedRenotifySpec").getValue();
			StringBuilder renotifySpecBuf = new StringBuilder((updatedSelectedRenotifySpec.size()+1) * 10);
			for( String v : updatedSelectedRenotifySpec ) {
				if ( renotifySpecBuf.length() > 0 ) {
					renotifySpecBuf.append(";");
				}
				renotifySpecBuf.append(v);
			}
			packageVersionPartyTemplate.setRenotifySpec(renotifySpecBuf.toString());

			// We need a new set since the one returned is non-modifiable...
			HashSet<DocumentAndParty> updatedSelectedDocumentsAndParties = new HashSet<DocumentAndParty>( (java.util.Set<DocumentAndParty>)getField("selectedDocumentsAndParties").getValue() );
			scrubDuplicateViewOnlyAndViewOptional(updatedSelectedDocumentsAndParties);
			
			// First, we'll check if the current mappings are still in our updated mappings.  If so, we keep them (but remove from our updated list), otherwise we remove that mapping.
    		List<PackageVersionPartyTemplateDocumentParty> currentMappings = packageVersionPartyTemplate.getPackageVersionPartyTemplateDocumentParties();
    		ListIterator<PackageVersionPartyTemplateDocumentParty> currentMappingsIter = currentMappings.listIterator();
			while( currentMappingsIter.hasNext() ) {
				PackageVersionPartyTemplateDocumentParty mapping = currentMappingsIter.next();
				
				boolean currentMappingIsInUpdatedListToo = false;
				for( DocumentAndParty dp : updatedSelectedDocumentsAndParties ) {
					if ( dp.equals(mapping) ) {
						updatedSelectedDocumentsAndParties.remove(dp); // we'll not need to add this one to our mappings since we already have it
						currentMappingIsInUpdatedListToo = true;
						break;
					}
				}
				if ( ! currentMappingIsInUpdatedListToo ) {
					packageVersionPartyTemplate.removePackageVersionPartyTemplateDocumentParty(mapping);
				}
			}
			
			// Now, whatever we have left in our updated list, we'll add those mappings
			for( DocumentAndParty dp : updatedSelectedDocumentsAndParties ) {
				PackageVersionPartyTemplateDocumentParty mapping = PackageVersionPartyTemplateDocumentParty.Manager.createNew(packageVersionPartyTemplateId, dp.documentId, dp.partyName, Literals.STATUS_ENABLED, Literals.STATUS_ENABLED);
				packageVersionPartyTemplate.addPackageVersionPartyTemplateDocumentParty(mapping);
			}
			
			packageVersionPartyTemplate.fixupReservedParties();
			
			parentWindow.close();
        } else if ( source == cancelButton ) {
    		discard();
			parentWindow.close();
        }
    }
	
	// This routine removes all "view optional" parties if there's also a "view only" for that document
	// and removes all "view" parties if there's a non-view party
	void scrubDuplicateViewOnlyAndViewOptional(HashSet<DocumentAndParty> docAndPartySet) {
		LinkedList<EsfUUID> documentsWithParty = new LinkedList<EsfUUID>();
		LinkedList<EsfUUID> documentsViewOnly = new LinkedList<EsfUUID>();
		
		// In our first pass, we figure out which documents have real parties and which have view only parties 
		java.util.Iterator<DocumentAndParty> docAndPartySetIter = docAndPartySet.iterator();
		while( docAndPartySetIter.hasNext() ) {
			DocumentAndParty docAndParty = docAndPartySetIter.next();
			if ( docAndParty.isViewOnly() ) {
				documentsViewOnly.add(docAndParty.documentId);
			} else if ( ! docAndParty.isViewOptional() ) {
				documentsWithParty.add(docAndParty.documentId);
			}
		}
		
		// In our second pass, if we find a view optional that either is a real party or a view only party, we remove it.
		// Otherwise, if we find a view party that is also a real party, we remove it.
		docAndPartySetIter = docAndPartySet.iterator();
		while( docAndPartySetIter.hasNext() ) {
			DocumentAndParty docAndParty = docAndPartySetIter.next();
			
			if ( docAndParty.isViewOptional() ) {
				if ( documentsWithParty.contains(docAndParty.documentId) || documentsViewOnly.contains(docAndParty.documentId) )
					docAndPartySetIter.remove();
			} else if ( docAndParty.isViewOnly() ) {
				if ( documentsWithParty.contains(docAndParty.documentId) )
					docAndPartySetIter.remove();
			}
		}
		
		documentsWithParty.clear();
		documentsViewOnly.clear();
	}
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	saveButton.setVisible(!readOnly);
    	
    	if ( ! readOnly ) {
    		boolean makeReservedReadOnly = packageVersionPartyTemplate.hasReservedPrefix();
    		getField("packagePartyName").setReadOnly(makeReservedReadOnly);
    		getField("requireLogin").setReadOnly(makeReservedReadOnly);
    		getField("libraryPartyTemplateTodoEsfName").setReadOnly(makeReservedReadOnly);
    		getField("notifyAllTodo").setReadOnly(makeReservedReadOnly);
    		getField("libraryEmailTemplateEsfName").setReadOnly(makeReservedReadOnly);
    		getField("emailFieldExpression").setReadOnly(makeReservedReadOnly);
    		getField("completedUrlFieldExpression").setReadOnly(makeReservedReadOnly);
    		getField("notCompletedUrlFieldExpression").setReadOnly(makeReservedReadOnly);
    		getField("selectedRenotifySpec").setReadOnly(makeReservedReadOnly);
    		getField("overridePackageDocumentId").setReadOnly(makeReservedReadOnly);
    		getField("overrideButtonMessageId").setReadOnly(makeReservedReadOnly);
    	}
    }
    
    
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
		PropertysetItem item = new PropertysetItem();
		item.addItemProperty("packagePartyName", new ObjectProperty<EsfName>(packageVersionPartyTemplate.getEsfName()));
		item.addItemProperty("packagePartyDisplayName", new ObjectProperty<String>(packageVersionPartyTemplate.getDisplayName(),String.class));
		item.addItemProperty("landingPage", new ObjectProperty<String>(packageVersionPartyTemplate.getLandingPage(),String.class));
		item.addItemProperty("overridePackageDocumentId", new ObjectProperty<EsfUUID>(packageVersionPartyTemplate.getOverridePackageDocumentId(),EsfUUID.class));
		item.addItemProperty("overrideButtonMessageId", new ObjectProperty<EsfUUID>(packageVersionPartyTemplate.getOverrideButtonMessageId(),EsfUUID.class));
		item.addItemProperty("requireLogin", new ObjectProperty<Boolean>(packageVersionPartyTemplate.isRequireLogin()));
		item.addItemProperty("allowDeleteTranIfUnsigned", new ObjectProperty<Boolean>(packageVersionPartyTemplate.isAllowDeleteTranIfUnsigned()));
		EsfName libraryPartyTemplateTodoEsfName = packageVersionPartyTemplate.getLibraryPartyTemplateTodoEsfName();
		item.addItemProperty("libraryPartyTemplateTodoEsfName", new ObjectProperty<EsfName>(libraryPartyTemplateTodoEsfName,EsfName.class));
		item.addItemProperty("notifyAllTodo", new ObjectProperty<Boolean>(packageVersionPartyTemplate.isNotifyAllTodo()));
		EsfName libraryEmailTemplateEsfName = packageVersionPartyTemplate.getLibraryEmailTemplateEsfName();
		item.addItemProperty("libraryEmailTemplateEsfName", new ObjectProperty<EsfName>((libraryEmailTemplateEsfName==null ? new EsfName() : libraryEmailTemplateEsfName),EsfName.class));
		String emailFieldExpression = packageVersionPartyTemplate.getEmailFieldExpression();
		item.addItemProperty("emailFieldExpression", new ObjectProperty<String>(emailFieldExpression,String.class));
		String completedUrlFieldExpression = packageVersionPartyTemplate.getCompletedUrlFieldExpression();
		item.addItemProperty("completedUrlFieldExpression", new ObjectProperty<String>(completedUrlFieldExpression,String.class));
		String notCompletedUrlFieldExpression = packageVersionPartyTemplate.getNotCompletedUrlFieldExpression();
		item.addItemProperty("notCompletedUrlFieldExpression", new ObjectProperty<String>(notCompletedUrlFieldExpression,String.class));
		item.addItemProperty("selectedRenotifySpec", new ObjectProperty(selectedRenotifySpec,java.util.Set.class));
		//DOES NOT WORK: item.addItemProperty("selectedDocumentsAndParties", new ObjectProperty(selectedDocumentsAndParties));
		//DOES NOT WORK: item.addItemProperty("selectedDocumentsAndParties", new ObjectProperty<java.util.Set<DocumentAndParty>>(selectedDocumentsAndParties));
		//ALSO GOOD (BUT WHY BOTHER): item.addItemProperty("selectedDocumentsAndParties", new ObjectProperty<java.util.Set>(selectedDocumentsAndParties,java.util.Set.class));
		item.addItemProperty("selectedDocumentsAndParties", new ObjectProperty(selectedDocumentsAndParties,java.util.Set.class));

		setItemDataSource(item);
	}
	
	public void setParentWindow(ConfirmDiscardFormChangesWindow parentWindow) {
		this.parentWindow = parentWindow;
	}
	
	@Override
	public void initView() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	// Set up our data structures.
    	packageVersion = packageVersionBean.duplicatedPackageVersion();
    	packageVersionPartyTemplate = packageVersion.getPackageVersionPartyTemplateById(packageVersionPartyTemplateId);
    	
    	selectedRenotifySpec = new HashSet<String>();
    	String[] renotifySpecs = packageVersionPartyTemplate.getRenotifySpecs();
		for( String spec : renotifySpecs ) {
			selectedRenotifySpec.add(spec);
		}
    	
    	allDocumentsAndParties = new LinkedList<DocumentAndParty>();
    	for( EsfUUID docId : packageVersion.getDocumentIdList() ) {
    		Document document = Document.Manager.getById(docId);
    		DocumentVersion documentVersion = packageVersionBean.isProductionVersion() ? document.getProductionDocumentVersion() : document.getTestDocumentVersion();

    		DocumentAndParty dpViewOnly = new DocumentAndParty(docId, PartyTemplate.ESF_PARTY_NAME_VIEW_ONLY, document.getEsfName().toString()+" - "+vaadinUi.getMsg("PackagePartyToDocumentPartyForm.viewOnlyParty.label.suffix"));
			allDocumentsAndParties.add(dpViewOnly);
    		
    		DocumentAndParty dpViewOptional = new DocumentAndParty(docId, PartyTemplate.ESF_PARTY_NAME_VIEW_OPTIONAL, document.getEsfName().toString()+" - "+vaadinUi.getMsg("PackagePartyToDocumentPartyForm.viewOptionalParty.label.suffix"));
			allDocumentsAndParties.add(dpViewOptional);
    		
    		for( PartyTemplate docParty : documentVersion.getPartyTemplateList() ) {
    			DocumentAndParty dp = new DocumentAndParty(docId, docParty.getEsfName(), document.getEsfName()+" - "+docParty.getEsfName());
    			allDocumentsAndParties.add(dp);
    		}
    	}
    	
    	selectedDocumentsAndParties = new HashSet<DocumentAndParty>();
    	for( PackageVersionPartyTemplateDocumentParty mapping : packageVersionPartyTemplate.getPackageVersionPartyTemplateDocumentParties() ) {
    		DocumentAndParty dp = new DocumentAndParty(mapping.getDocumentId(), mapping.getDocumentPartyTemplateName(), null);
    		if ( allDocumentsAndParties.contains(dp) ) // if we have a mapping we can't find anymore, we'll get rid of it
    			selectedDocumentsAndParties.add(dp);
    	}
    	
		Collection<Library> libraries = Library.Manager.getForUserWithViewDetailsPermission(vaadinUi.getUser(), Library.INCLUDE.ONLY_ENABLED);

		allPartyTemplateEsfNames = new LinkedList<EsfName>();
		for( Library library : libraries ) {
			List<PartyTemplate> list = PartyTemplate.Manager.getAll(library.getId());
			for( PartyTemplate partyTemplate : list ) {
				// Skip if the party is already in our list or it doesn't have a TodoGroup setting
				boolean hasToDoGroupId = packageVersionBean.isProductionVersion() ? partyTemplate.hasTodoGroupId() : partyTemplate.hasTestTodoGroupId();
				if ( allPartyTemplateEsfNames.contains(partyTemplate.getEsfName()) || ! hasToDoGroupId )
					continue;
				allPartyTemplateEsfNames.add(partyTemplate.getEsfName());
			}
		}

		allEmailTemplateEsfNames = new LinkedList<EsfName>();
		List<EsfName> passwordRelatedEmailTemplateNames = vaadinUi.getEsfapp().getAllPasswordRelatedEmailTemplateEsfNames();
		for( Library library : libraries ) {
			List<EmailTemplate> list = EmailTemplate.Manager.getAll(library.getId());
			for( EmailTemplate emailTemplate : list ) {
				// We'll skip some well known email templates
				if ( passwordRelatedEmailTemplateNames.contains(emailTemplate.getEsfName()) )
					continue;
				// We'll skip any names we already have (defined in multiple libraries)
				if ( allEmailTemplateEsfNames.contains(emailTemplate.getEsfName()) )
					continue;
				allEmailTemplateEsfNames.add(emailTemplate.getEsfName());
			}
		}

    	// Setup layout
    	layout = new GridLayout(3,9);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.3f);
    	layout.setColumnExpandRatio(1, 0.1f);
    	layout.setColumnExpandRatio(2, 0.6f);
    	setLayout(layout);
    	
    	Label packagePartyIdLabel = new Label(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.packagePartyId.label",packageVersionPartyTemplateId));
    	packagePartyIdLabel.setContentMode(ContentMode.TEXT);
    	packagePartyIdLabel.setStyleName("smallInfo");
    	layout.addComponent(packagePartyIdLabel, 0, 8, 2, 8);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 3218350886180633262L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				
				if ( propertyId.equals("packagePartyName") ) {
					TextField tf = (TextField)super.createField(item, propertyId, uiContext);
	                tf.setWidth(100, Unit.PERCENTAGE);
	                tf.setNullRepresentation("");
	                tf.addValidator(new PackagePartyEsfNameValidator(packageVersion.getPackageVersionPartyTemplateList(),packageVersionPartyTemplate));
	                tf.setConversionError(vaadinUi.getMsg("validator.esfpathname.message"));
                	tf.setRequired(true);
                	tf.setCaption(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.packagePartyName.label"));
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
					return tf;
				}
				
				if ( propertyId.equals("packagePartyDisplayName") ) {
					TextField tf = (TextField)super.createField(item, propertyId, uiContext);
	                tf.setWidth(100, Unit.PERCENTAGE);
	                tf.setNullRepresentation("");
	                tf.setRequired(true);
                	tf.setCaption(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.packagePartyDisplayName.label"));
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
					return tf;
				}
				
				if ( propertyId.equals("requireLogin") ) {
					CheckBox cb = new CheckBox(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.requireLogin.label"));
					cb.setDescription(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.requireLogin.tooltip"));
					return cb;
				}
				
				if ( propertyId.equals("allowDeleteTranIfUnsigned") ) {
					CheckBox cb = new CheckBox(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.allowDeleteTranIfUnsigned.label"));
					cb.setDescription(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.allowDeleteTranIfUnsigned.tooltip"));
					return cb;
				}
				
				if ( propertyId.equals("landingPage") ) {
					NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.landingPage.label"));
					select.setRequired(true);
					select.setNullSelectionAllowed(false);
                	select.setDescription(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.landingPage.tooltip"));
	                select.addValidator(new SelectValidator(select));
                	select.addItem(PackageVersionPartyTemplate.LANDING_PAGE_PACKAGE);
                	select.setItemCaption(PackageVersionPartyTemplate.LANDING_PAGE_PACKAGE, vaadinUi.getPrettyCode().packageVersionPartyTemplateLandingPage(PackageVersionPartyTemplate.LANDING_PAGE_PACKAGE));
                	select.addItem(PackageVersionPartyTemplate.LANDING_PAGE_FIRST_DOC);
                	select.setItemCaption(PackageVersionPartyTemplate.LANDING_PAGE_FIRST_DOC, vaadinUi.getPrettyCode().packageVersionPartyTemplateLandingPage(PackageVersionPartyTemplate.LANDING_PAGE_FIRST_DOC));
                	select.addItem(PackageVersionPartyTemplate.LANDING_PAGE_FIRST_TODO_OR_FIRST_DOC);
                	select.setItemCaption(PackageVersionPartyTemplate.LANDING_PAGE_FIRST_TODO_OR_FIRST_DOC, vaadinUi.getPrettyCode().packageVersionPartyTemplateLandingPage(PackageVersionPartyTemplate.LANDING_PAGE_FIRST_TODO_OR_FIRST_DOC));
                	select.addItem(PackageVersionPartyTemplate.LANDING_PAGE_FIRST_TODO_OR_PACKAGE);
                	select.setItemCaption(PackageVersionPartyTemplate.LANDING_PAGE_FIRST_TODO_OR_PACKAGE, vaadinUi.getPrettyCode().packageVersionPartyTemplateLandingPage(PackageVersionPartyTemplate.LANDING_PAGE_FIRST_TODO_OR_PACKAGE));
					return select;
				}
				
				if ( propertyId.equals("libraryPartyTemplateTodoEsfName") ) {
					NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.libraryPartyTemplateTodoEsfName.label"));
                	select.setDescription(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.libraryPartyTemplateTodoEsfName.tooltip"));               	
	                select.addValidator(new SelectValidator(select));
					for( EsfName partyName : allPartyTemplateEsfNames ) {
						select.addItem(partyName);
					}
					return select;
				}
				
				if ( propertyId.equals("notifyAllTodo") ) {
					CheckBox cb = new CheckBox(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.notifyAllTodo.label"));
					return cb;
				}
				
				if ( propertyId.equals("emailFieldExpression") ) {
					FieldSpecTextField tf = new FieldSpecTextField();
					tf.setNullRepresentation("");
	                tf.setWidth(100, Unit.PERCENTAGE);
                	tf.setCaption(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.emailFieldExpression.label"));
                	tf.setDescription(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.emailFieldExpression.tooltip"));
					return tf;
				}
				
				if ( propertyId.equals("completedUrlFieldExpression") ) {
					FieldSpecTextField tf = new FieldSpecTextField();
					tf.setNullRepresentation("");
	                tf.setWidth(100, Unit.PERCENTAGE);
                	tf.setCaption(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.completedUrlFieldExpression.label"));
                	tf.setDescription(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.completedUrlFieldExpression.tooltip"));
					return tf;
				}
				
				if ( propertyId.equals("notCompletedUrlFieldExpression") ) {
					FieldSpecTextField tf = new FieldSpecTextField();
					tf.setNullRepresentation("");
	                tf.setWidth(100, Unit.PERCENTAGE);
                	tf.setCaption(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.notCompletedUrlFieldExpression.label"));
                	tf.setDescription(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.notCompletedUrlFieldExpression.tooltip"));
					return tf;
				}
				
				if ( propertyId.equals("libraryEmailTemplateEsfName") ) {
					NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.libraryEmailTemplateEsfName.label"));
                	select.setDescription(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.libraryEmailTemplateEsfName.tooltip"));               	
	                select.addValidator(new SelectValidator(select));
					for( EsfName emailName : allEmailTemplateEsfNames ) {
						select.addItem(emailName);
					}
					return select;
				}
				
				if ( propertyId.equals("selectedRenotifySpec") ) {
					ListSelectDropDown dd = new ListSelectDropDown(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.renotifySpec.label"),vaadinUi.getEsfapp().getDropDownPartyRenotifyTimesEsfName());
                	dd.setDescription(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.renotifySpec.tooltip"));
                	dd.setRows(dd.getNumOptions());
	                dd.addValidator(new SelectValidator(dd));
					return dd;
				}
				
				if ( propertyId.equals("selectedDocumentsAndParties") ) {
		    		ListSelect select = new ListSelectValid(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.documentsAndParties.label"));
		    		select.setDescription(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.documentsAndParties.tooltip"));
					select.setMultiSelect(true);
					select.setNullSelectionAllowed(true);
					select.setWidth(100, Unit.PERCENTAGE);
	                select.addValidator(new SelectValidator(select));
					
			    	for( DocumentAndParty dp : allDocumentsAndParties ) {
			    		select.addItem(dp);
			    		select.setItemCaption((Object)dp, dp.label);
			    	}
			    	select.setRows(Math.min(12, allDocumentsAndParties.size()));
		    		return select;
				}
				
				if ( propertyId.equals("overridePackageDocumentId") ) {
					NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.overridePackageDocumentId.label"));
					select.setDescription(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.overridePackageDocumentId.tooltip"));
					select.setNullSelectionAllowed(true);
					select.setImmediate(true);
					select.setRequired(false);
					select.setMultiSelect(false);
	                select.addValidator(new SelectValidator(select));
					select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					
					Collection<Library> libraries = Library.Manager.getForUserWithViewDetailsPermission(vaadinUi.getUser(), Library.INCLUDE.ONLY_ENABLED);
					for( Library library : libraries ) {
						Collection<DocumentInfo> documents = DocumentInfo.Manager.getAll(library,DocumentInfo.INCLUDE.ONLY_ENABLED);
						for( DocumentInfo docInfo : documents ) {
							select.addItem(docInfo.getId());
							select.setItemCaption(docInfo.getId(), library.getPathName().toString() + "/" + docInfo.getEsfName().toString());
						}
					}

                	return select;
				}
				
				if ( propertyId.equals("overrideButtonMessageId") ) {
					NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.overrideButtonMessageId.label"));
					select.setDescription(vaadinUi.getMsg("PackagePartyToDocumentPartyForm.overrideButtonMessageId.tooltip"));
					select.setNullSelectionAllowed(true);
					select.setImmediate(true);
					select.setRequired(false);
					select.setMultiSelect(false);
	                select.addValidator(new SelectValidator(select));
					select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					
					Collection<Library> libraries = Library.Manager.getForUserWithViewDetailsPermission(vaadinUi.getUser(), Library.INCLUDE.ONLY_ENABLED);
					for( Library library : libraries ) {
						Collection<ButtonMessageInfo> buttonMessageSets = ButtonMessageInfo.Manager.getAll(library,ButtonMessageInfo.INCLUDE.ONLY_ENABLED);
						for( ButtonMessageInfo bm : buttonMessageSets ) {
							select.addItem(bm.getId());
							select.setItemCaption(bm.getId(), library.getPathName().toString() + "/" + bm.getEsfName().toString());
						}
					}

                	return select;
				}

				return null;
    	    }
    	 });

    	_logger.debug("Form created");
	}

	@Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("packagePartyName")) {
            layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("packagePartyDisplayName")) {
        	layout.addComponent(field, 1, 0);        
        } else if (propertyId.equals("landingPage")) {
        	layout.addComponent(field, 2, 0);
        } else if (propertyId.equals("requireLogin")) {
        	layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("allowDeleteTranIfUnsigned")) {
        	layout.addComponent(field, 1, 1);   
        } else if (propertyId.equals("libraryPartyTemplateTodoEsfName")) {
        	layout.addComponent(field, 0, 2);        
        } else if (propertyId.equals("notifyAllTodo")) {
        	layout.addComponent(field, 0, 3);        
        } else if (propertyId.equals("emailFieldExpression")) {
        	layout.addComponent(field, 0, 4);        
        } else if (propertyId.equals("libraryEmailTemplateEsfName")) {
        	layout.addComponent(field, 0, 5);        
        } else if (propertyId.equals("selectedRenotifySpec")) {
        	layout.addComponent(field, 1, 2, 1, 5);        
        } else if (propertyId.equals("selectedDocumentsAndParties")) {
        	layout.addComponent(field, 2, 1, 2, 5);   
        } else if (propertyId.equals("completedUrlFieldExpression")) {
        	layout.addComponent(field, 0, 6, 1, 6);        
        } else if (propertyId.equals("notCompletedUrlFieldExpression")) {
        	layout.addComponent(field, 2, 6);        
        } else if (propertyId.equals("overridePackageDocumentId")) {
        	layout.addComponent(field, 0, 7, 1, 7);        
        } else if (propertyId.equals("overrideButtonMessageId")) {
        	layout.addComponent(field, 2, 7);        
        }
    }


	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageAndVersionsMainView.Package.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}