// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.reportTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.jouni.animator.Disclosure;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.UI;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.reportTemplate.reportField.ReportTemplateReportFieldView;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.ReportTemplateEsfPathNameValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.ListSelectValid;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The ReportTemplateForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class ReportTemplateForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -5681787095952628610L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ReportTemplateForm.class);
	
	static List<String> _orderedProperties;
    
	Button saveButton;
	Button createLikeButton;
	Button deleteButton;
	Button cancelButton;
	Button editButton;
	Button fieldsInReportButton;

	ReportTemplateView view;
	ReportTemplateViewContainer container;
    GridLayout layout;
	
	Label id;
	Label lastUpdatedTimestamp;
	Label createdByInfo;
	
	// We store the prevBean in case we abandon working on the newBean when we 'create like'
	ReportTemplateBean prevBean;
	ReportTemplateBean newBean;
    
    // We use this for all our select lists, so we only reload once per Bean being set as our datasource
	EsfUUID[] allGroupListIds;
	EsfPathName[] allGroupListNames;
	
	EsfUUID[] allTransactionTemplateListIds;
	EsfPathName[] allTransactionTemplateListNames;

	EsfUUID[] allDocumentListIds;
	EsfPathName[] allDocumentListNames;

	Disclosure permDisclosure;
	Disclosure permReportDisclosure;

	public ReportTemplateForm(ReportTemplateView view, ReportTemplateViewContainer container) {
    	setStyleName("ReportTemplateForm");
       	setWidth(100, Unit.PERCENTAGE); // hack because seems to have "horizontal scrollbar" issues if the form is shown with vertical scrollbars in place
    	this.view = view;
    	this.container = container;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	
    	layout = new GridLayout(4,10);
       	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.40f);
    	layout.setColumnExpandRatio(1, 0.40f);
    	layout.setColumnExpandRatio(2, 0.10f);
    	layout.setColumnExpandRatio(3, 0.10f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id, 0, 0);

		lastUpdatedTimestamp = new Label();
		lastUpdatedTimestamp.setContentMode(ContentMode.TEXT);
		lastUpdatedTimestamp.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedTimestamp,1,0,3,0);
		
		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,9,3,9);
		
		permDisclosure = new Disclosure(vaadinUi.getMsg("ReportTemplateForm.permDisclosure.label"));
		permDisclosure.setWidth(100, Unit.PERCENTAGE);
		permDisclosure.setVisible(false);
		layout.addComponent(permDisclosure,0,7,3,7);
		
		permReportDisclosure = new Disclosure(vaadinUi.getMsg("ReportTemplateForm.permReportDisclosure.label"));
		permReportDisclosure.setWidth(100, Unit.PERCENTAGE);
		permReportDisclosure.setVisible(false);
		layout.addComponent(permReportDisclosure,0,8,3,8);
		
		VerticalLayout footer = new VerticalLayout();
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);

    	HorizontalLayout footer1 = new HorizontalLayout();
    	footer1.setStyleName("footer");
    	footer1.setSpacing(false);
    	footer1.setMargin(false);

    	HorizontalLayout footer2 = new HorizontalLayout();
    	footer2.setStyleName("footer");
    	footer2.setSpacing(false);
    	footer2.setMargin(false);

    	footer.addComponent(footer1);
    	footer.addComponent(footer2);

    	fieldsInReportButton = new Button(vaadinUi.getMsg("ReportTemplateForm.button.fieldsInReportButton.label"), (ClickListener)this);
    	fieldsInReportButton.setStyleName(Reindeer.BUTTON_SMALL);
    	fieldsInReportButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportTemplateForm.button.fieldsInReportButton.icon")));
    	fieldsInReportButton.setDescription(vaadinUi.getMsg("ReportTemplateForm.button.fieldsInReportButton.tooltip"));
    	footer1.addComponent(fieldsInReportButton);
    	
    	User loggedInUser = vaadinUi.getUser();
		UI ui = new UI();
		if ( ui.showSaveButton(loggedInUser, UI.UI_REPORT_TEMPLATE_VIEW) ) {
			saveButton = new Button(vaadinUi.getMsg("button.save.label"), (ClickListener)this);
			//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
			//saveButton.setClickShortcut(KeyCode.ENTER);
	    	saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
	    	saveButton.setDescription(vaadinUi.getMsg("button.save.tooltip"));
        	footer2.addComponent(saveButton);
		}

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer2.addComponent(cancelButton);

    	if ( ui.canUpdate(loggedInUser, UI.UI_REPORT_TEMPLATE_VIEW) ) {
        	editButton = new Button(vaadinUi.getMsg("button.edit.label"), (ClickListener)this);
        	editButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportTemplateForm.button.edit.icon")));
        	editButton.setDescription(vaadinUi.getMsg("button.edit.tooltip"));
    		footer2.addComponent(editButton);
    	}

    	if ( ui.canCreateLike(loggedInUser, UI.UI_REPORT_TEMPLATE_VIEW) ) {
	    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
	    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportTemplateForm.button.createLike.icon")));
	    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    		footer2.addComponent(createLikeButton);
		}

    	if ( ui.canDelete(loggedInUser, UI.UI_REPORT_TEMPLATE_VIEW) ) {
        	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
        	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportTemplateForm.button.delete.icon")));
        	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
        	deleteButton.addStyleName("deleteButton");
        	deleteButton.addStyleName("caution");
    		footer2.addComponent(deleteButton);
    	}
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -7590654954804531407L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				// Load our cache if we don't have it yet since we'll need it for all our permission selection boxes
				if ( allGroupListIds == null ) {
					ReportTemplateBean bean = getCurrentBean();
					allGroupListIds = bean.allGroupListIds();
					allGroupListNames = bean.allGroupListNames();
					allTransactionTemplateListIds = bean.allTransactionTemplateListIds();
					allTransactionTemplateListNames = bean.allTransactionTemplateListNames();
					allDocumentListIds = bean.allDocumentListIds();
					allDocumentListNames = bean.allDocumentListNames();
				}

				// Status will be an option group (radio buttons and not a text field)
				if ( propertyId.equals("status") ) {
					// Create the radio buttons to have the two possible values.
					OptionGroup statusOptionGroup = new OptionGroup();
					statusOptionGroup.addItem(Literals.STATUS_ENABLED);
					statusOptionGroup.addItem(Literals.STATUS_DISABLED);
					// Associate different labels/captions to those values
					statusOptionGroup.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					statusOptionGroup.setItemCaption(Literals.STATUS_ENABLED, vaadinUi.getMsg("ReportTemplateForm.status.enabled"));
					statusOptionGroup.setItemCaption(Literals.STATUS_DISABLED, vaadinUi.getMsg("ReportTemplateForm.status.disabled"));
					statusOptionGroup.setStyleName("inline");
					return statusOptionGroup;
				}
					
				if ( propertyId.equals("permListIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permList.label");
				} 
				
				if ( propertyId.equals("permViewDetailsIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permViewDetails.label");
				}
				
				if ( propertyId.equals("permCreateLikeIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permCreateLike.label");
				}
				
				if ( propertyId.equals("permUpdateIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permUpdate.label");
				}
				
				if ( propertyId.equals("permDeleteIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permDelete.label");
				}
				
				if ( propertyId.equals("permRunReportIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permRunReport.label");
				} 
				
				if ( propertyId.equals("permViewStartedByExternalUsersIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permViewStartedByExternalUsers.label");
				} 
				
				if ( propertyId.equals("permViewStartedByAnyUserIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permViewStartedByAnyUser.label");
				} 
				
				if ( propertyId.equals("permViewAnyUserPartyToIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permViewAnyUserPartyTo.label");
				} 
				
				if ( propertyId.equals("permViewProductionIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permViewProduction.label");
				} 
				
				if ( propertyId.equals("permDownloadCsvIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permDownloadCsv.label");
				} 
				
				if ( propertyId.equals("permDownloadArchiveIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permDownloadArchive.label");
				} 
				
				if ( propertyId.equals("permViewActivityLogIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permViewActivityLog.label");
				} 
				
				if ( propertyId.equals("permViewEmailLogIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permViewEmailLog.label");
				} 
				
				if ( propertyId.equals("permViewSnapshotDataIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permViewSnapshotData.label");
				} 
				
				if ( propertyId.equals("permViewSnapshotDocumentIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permViewSnapshotDocument.label");
				} 
				
				if ( propertyId.equals("permEsfReportsAccessViewIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permEsfReportsAccessView.label");
				} 
				
				if ( propertyId.equals("permEsfReportsAccessUpdateIds") ) {
					return createPermissionSelectBox("ReportTemplateForm.permEsfReportsAccessUpdate.label");
				} 
				
				if ( propertyId.equals("transactionTemplateIds") ) {
					final ListSelect select = new ListSelectValid(vaadinUi.getMsg("ReportTemplateForm.transactionTemplateIds.label"));
					select.setNullSelectionAllowed(false); 
					select.setRequired(true);
					select.setMultiSelect(true);
					//select.setImmediate(true);
					select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					int i = 0;
		        	for( EsfUUID tranTemplateId : allTransactionTemplateListIds ) {
		        		select.addItem(tranTemplateId);
		        		select.setItemCaption( tranTemplateId, allTransactionTemplateListNames[i++].toString() );
		        	}
		        	select.addValueChangeListener(new ValueChangeListener() {

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							Set<EsfUUID> selectSet = (Set<EsfUUID>)select.getValue();
							rebuildLimitDocumentIdSelect(selectSet);
						}
		        		
		        	});
                	return select;
				}
				
				if ( propertyId.equals("limitDocumentIds") ) {
					ListSelect select = new ListSelectValid(vaadinUi.getMsg("ReportTemplateForm.limitDocumentIds.label"));
					select.setNullSelectionAllowed(true); 
					select.setRequired(false);
					select.setMultiSelect(true);
					select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					int i = 0;
		        	for( EsfUUID documentId : allDocumentListIds ) {
		        		select.addItem(documentId);
		        		select.setItemCaption( documentId, allDocumentListNames[i++].toString() );
		        	}
                	return select;
				}
				
				if ( propertyId.equals("comments") ) {
	            	TextArea ta = new TextArea();
	            	ta.setWidth(100, Unit.PERCENTAGE);
	            	ta.setNullRepresentation("");
	            	ta.setRows(3);
	            	ta.setInputPrompt(vaadinUi.getMsg("inputPrompt.comments"));
	            	ta.setCaption(vaadinUi.getMsg("caption.comments"));
	            	ta.setDescription(vaadinUi.getMsg("tooltip.comments"));
	            	return ta;
				}
				
				Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("pathName")) {
                	TextField tf = (TextField)field;
                    tf.addValidator(new ReportTemplateEsfPathNameValidator(getCurrentBean()));
                    tf.setConversionError(vaadinUi.getMsg("validator.esfpathname.message"));
                    tf.setImmediate(true);
                    tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    tf.setCaption(vaadinUi.getMsg("caption.esfPathName"));
                    tf.setDescription(vaadinUi.getMsg("ReportTemplateForm.pathName.tooltip"));
                } else if ( propertyId.equals("displayName") ) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setInputPrompt(vaadinUi.getMsg("inputPrompt.displayName"));
                    tf.setCaption(vaadinUi.getMsg("caption.displayName"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.displayName"));
                } else if ( propertyId.equals("description") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                	tf.setInputPrompt(vaadinUi.getMsg("inputPrompt.description"));
                    tf.setCaption(vaadinUi.getMsg("caption.description"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.description"));
                }

                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
		allGroupListIds = null;
		allGroupListNames = null;
		allTransactionTemplateListIds = null;
		allTransactionTemplateListNames = null;
		allDocumentListIds = null;
		allDocumentListNames = null;
    }
    
    void rebuildLimitDocumentIdSelect(Set<EsfUUID> tranTemplateIdSet) {
		ReportTemplateBean bean = getCurrentBean();
		bean.createAllDocumentLists(tranTemplateIdSet);
		allDocumentListIds = bean.allDocumentListIds();
		allDocumentListNames = bean.allDocumentListNames();
		ListSelect limitDocumentIdsSelect = (ListSelect)getField("limitDocumentIds");
		if ( limitDocumentIdsSelect != null ) {
			Set<EsfUUID> currValue = (Set<EsfUUID>)limitDocumentIdsSelect.getValue();
			limitDocumentIdsSelect.removeAllItems();
			int i = 0;
        	for( EsfUUID documentId : allDocumentListIds ) {
        		limitDocumentIdsSelect.addItem(documentId);
        		limitDocumentIdsSelect.setItemCaption( documentId, allDocumentListNames[i++].toString() );
        	}
        	try {
        		bean._suppressSaveToObject();
        		limitDocumentIdsSelect.setValue(currValue);
        		limitDocumentIdsSelect.commit();
        	} catch( Exception e ) {
        		// ignore
        	} finally {
        		bean._allowSaveToObject();
        	}
		}
    }
    
    private TwinColSelect createPermissionSelectBox(String vaadinMessageCaption) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	TwinColSelect selectList = new TwinColSelect();
    	selectList.setLeftColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".left"));
    	selectList.setRightColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".right"));
    	selectList.setWidth(100, Unit.PERCENTAGE);
        selectList.addValidator(new SelectValidator(selectList));
		
		// Set all the possible values
		for( int i=0; i < allGroupListIds.length; ++i ) {
			selectList.addItem(allGroupListIds[i]);
		}
		// Associate labels with our ID values
		selectList.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		for( int i=0; i < allGroupListIds.length; ++i ) {
			selectList.setItemCaption(allGroupListIds[i], allGroupListNames[i].toPlainString());
		}
		
		selectList.setRows( Math.min(allGroupListIds.length,5) );
		selectList.setNullSelectionAllowed(true);
		selectList.setMultiSelect(true);
		return selectList;
    }
    
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
    		// If we're saving a new bean
			Errors errors = new Errors();
    		if ( newBean != null ) {
        		if ( newBean.save(errors) ) {
        			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",newBean.getPathName()) );
            		vaadinUi.notifyReportTemplateChangeListeners(newBean.duplicatedReportTemplate());
            		// We need to add the new bean to the container
            		container.addItem(newBean);
            		view.select(newBean); // reselect our bean so all updates cleanly
            		prevBean = newBean = null; // and we're not longer working on a new bean
        		} else {
        			vaadinUi.show(errors);
        		}
    		} else {
    			ReportTemplateBean currBean = getCurrentBean();
    			if ( currBean.save(errors) ) {
        			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",currBean.getPathName()) );
            		vaadinUi.notifyReportTemplateChangeListeners(currBean.duplicatedReportTemplate());
        			container.refresh();  // refresh our list after a change
            		view.select(currBean); // reselect our bean so all updates cleanly
        		} else {
        			vaadinUi.show(errors);
        		}
    		}
        } else if ( source == cancelButton ) {
    		discard();
    		vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
        	if ( newBean != null ) {
        		setItemDataSource(null);
        		view.unselectAll(); // we'll unselect and the reselect our prevBean
        		view.select(prevBean);
        		prevBean = newBean = null; // we're not doing a new bean anymore
        	}
        	ReportTemplateBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		currBean.resetCaches();
        	}
        	setReadOnly(true);
        } else if ( source == editButton ) {
        	setReadOnly(false);
        } else if ( source == deleteButton ) {
	    	ConfirmDialog.show(com.vaadin.ui.UI.getCurrent(), 
	    			vaadinUi.getMsg("ReportTemplateForm.button.delete.ConfirmDialog.caption"),
	    			vaadinUi.getMsg("ReportTemplateForm.button.delete.ConfirmDialog.message"),
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
	    	        new ConfirmDialog.Listener() {
						private static final long serialVersionUID = 3277753638639052067L;

						public void onClose(ConfirmDialog dialog) {
	    	                if (dialog.isConfirmed()) {
	    	                	discard();
	    	                	ReportTemplateBean currBean = getCurrentBean();
	    	                	if ( currBean != null ) {
	    	                		setItemDataSource(null);
	    	                		view.unselectAll(); // we'll unselect it and remove it from our container
	    	                		if ( newBean == null ) {
	    	                			Errors errors = new Errors();
	    	                			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	                			if ( currBean.delete(errors) ) {
	    	                        		vaadinUi.notifyReportTemplateChangeListeners(currBean.duplicatedReportTemplate());
	    	                        		container.removeItem(currBean);
	    	                        		vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",currBean.getPathName()) );
	    	                			} else {
	    	                				vaadinUi.show(errors);
	    	                			}
	    	                		} else {
	    	                			newBean.delete(null);
	    	                    		view.select(prevBean);
	    	                			prevBean = newBean = null;
	    	                		}
	    	                	}	
	    	                }
	    	            }
	    	        });
        } else if ( source == createLikeButton ) {
        	ReportTemplateBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		createLike(currBean);
        	}	
    	} else if ( source == fieldsInReportButton ) {
        	ReportTemplateBean currBean = getCurrentBean();
    		ReportTemplateReportFieldView reportTemplateReportFieldView = new ReportTemplateReportFieldView(currBean.duplicatedReportTemplate());
    		reportTemplateReportFieldView.initView();
    		
            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("ReportTemplateReportFieldView.window.caption",currBean.duplicatedReportTemplate().getPathName().toString()), reportTemplateReportFieldView);
            w.center();
        	w.setWidth(90, Unit.PERCENTAGE);
        	w.setHeight(95, Unit.PERCENTAGE);

        	reportTemplateReportFieldView.activateView(EsfView.OpenMode.WINDOW, "");
        	reportTemplateReportFieldView.setReadOnly(isReadOnly());
        	reportTemplateReportFieldView.setParentWindow(w);
        	vaadinUi.addWindowToUI(w);	
        }
    }
	
	public String getCurrentBeanName() {
		ReportTemplateBean bean = getCurrentBean();
		return bean == null ? "(None)" : bean.getPathName().toString();
	}
    
	ReportTemplateBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public ReportTemplateBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<ReportTemplateBean> bi = (BeanItem<ReportTemplateBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached list
		resetCaches();
		
    	if (newDataSource != null) {
    		ReportTemplateBean bean = getBean(newDataSource);
    		
    		if ( _orderedProperties == null ) { 
    			// we recreate the limitDocumentIds before the tranTemplateIds since when the tran template list changes, our limitDocument changes as it depends on the selected transaction templates
    			_orderedProperties = Arrays.asList("status","pathName","displayName","description","comments","limitDocumentIds","transactionTemplateIds",
    					"permListIds","permViewDetailsIds","permCreateLikeIds","permUpdateIds","permDeleteIds",
    					"permRunReportIds","permViewStartedByExternalUsersIds","permViewStartedByAnyUserIds","permViewAnyUserPartyToIds","permViewProductionIds","permDownloadCsvIds",
    					"permDownloadArchiveIds","permViewActivityLogIds","permViewEmailLogIds","permViewSnapshotDataIds","permViewSnapshotDocumentIds",
    					"permEsfReportsAccessViewIds","permEsfReportsAccessUpdateIds"
    					);
    		}
    		super.setItemDataSource(newDataSource, _orderedProperties);
    		
    		setupForm(bean);
    		
    		setReadOnly( ! bean.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		newBean = bean.duplicatedReportTemplate().doInsert() ? bean : null;
    		if ( newBean != null ) {
    			TextField tf = (TextField)getField("pathName");
    			tf.selectAll();
    			tf.focus();
    		}
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	ReportTemplateBean bean = getCurrentBean();
    	
    	// We have these only while editing/creating new
    	if ( saveButton != null ) {
    		saveButton.setVisible(!readOnly);
    	}
    	cancelButton.setVisible(!readOnly);

    	// We have this only while in view mode
    	if ( editButton != null ) {
        	editButton.setVisible(readOnly && bean != null && bean.hasPermUpdate());
    	}
    	
    	// This one we show this in all modes if has permission
    	if ( createLikeButton != null ) {
        	createLikeButton.setVisible(bean != null && bean.hasPermCreateLike() && bean.duplicatedReportTemplate().doUpdate()); // only want create like on an existing bean
    	}
    	
    	if ( fieldsInReportButton != null ) {
    		fieldsInReportButton.setVisible(bean != null && bean.duplicatedReportTemplate().doUpdate());
    	}
    	
    	if ( deleteButton != null ) {
        	deleteButton.setVisible(bean != null && bean.hasPermDelete());
    	}
    	
    	// We don't want any updates to the status, name or delete permissions on these specialty report template
    	if ( bean != null && bean.duplicatedReportTemplate().isReserved() ) {
    		getField("pathName").setReadOnly(true);
    		getField("status").setReadOnly(true);
    		getField("permDeleteIds").setReadOnly(true);
    		getField("permRunReportIds").setReadOnly(true);
    		getField("permViewStartedByExternalUsersIds").setReadOnly(true);
    		getField("permViewStartedByAnyUserIds").setReadOnly(true);
    		getField("permViewAnyUserPartyToIds").setReadOnly(true);
    		getField("permViewProductionIds").setReadOnly(true);
    		getField("permDownloadCsvIds").setReadOnly(true);
    		getField("permDownloadArchiveIds").setReadOnly(true);
    		getField("permViewActivityLogIds").setReadOnly(true);
    		getField("permViewEmailLogIds").setReadOnly(true);
    		getField("permViewSnapshotDataIds").setReadOnly(true);
    		getField("permViewSnapshotDocumentIds").setReadOnly(true);
    		getField("permEsfReportsAccessViewIds").setReadOnly(true);
    		getField("permEsfReportsAccessUpdateIds").setReadOnly(true);
    	}
    }
    
    public void createLike(ReportTemplateBean likeBean) {
        // Create a temporary item for the form
    	ReportTemplateBean createdBean = likeBean.createLike();
        
        BeanItem<ReportTemplateBean> bi = new BeanItem<ReportTemplateBean>( createdBean );
        setItemDataSource(bi);
    	prevBean = likeBean;
        setReadOnly(false);
    }
    
    void setupForm(ReportTemplateBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("ReportTemplateForm.label.id",bean.getId()) );
		
		if ( bean.duplicatedReportTemplate().doInsert() ) {
			lastUpdatedTimestamp.setCaption(vaadinUi.getMsg("ReportTemplateForm.label.lastUpdated.whenNew"));
			lastUpdatedTimestamp.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.duplicatedReportTemplate().getLastUpdatedByUserId());
			lastUpdatedTimestamp.setCaption( vaadinUi.getMsg("ReportTemplateForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedTimestamp.setIcon(null);
		}
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.duplicatedReportTemplate().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("ReportTemplateForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );

		createPermDisclosure();
		createPermReportDisclosure();
    }
    
    void createPermDisclosure() {
    	VerticalLayout permLayout = new VerticalLayout();
    	permLayout.setMargin(false);
    	permLayout.setSpacing(true);
    	
		permLayout.addComponent(getField("permListIds"));
		permLayout.addComponent(getField("permViewDetailsIds"));
		permLayout.addComponent(getField("permCreateLikeIds"));
		permLayout.addComponent(getField("permUpdateIds"));
		permLayout.addComponent(getField("permDeleteIds"));
				
		permDisclosure.setContent(permLayout);
		permDisclosure.setVisible(true);
    }

    void createPermReportDisclosure() {
    	VerticalLayout permLayout = new VerticalLayout();
    	permLayout.setMargin(false);
    	permLayout.setSpacing(true);
    	
		permLayout.addComponent(getField("permRunReportIds"));
		permLayout.addComponent(getField("permViewStartedByExternalUsersIds"));
		permLayout.addComponent(getField("permViewStartedByAnyUserIds"));
		permLayout.addComponent(getField("permViewAnyUserPartyToIds"));
		permLayout.addComponent(getField("permViewProductionIds"));
		permLayout.addComponent(getField("permDownloadCsvIds"));
		permLayout.addComponent(getField("permViewActivityLogIds"));
		permLayout.addComponent(getField("permViewEmailLogIds"));
		permLayout.addComponent(getField("permViewSnapshotDocumentIds"));
		permLayout.addComponent(getField("permViewSnapshotDataIds"));
		permLayout.addComponent(getField("permDownloadArchiveIds"));
		permLayout.addComponent(getField("permEsfReportsAccessViewIds"));
		permLayout.addComponent(getField("permEsfReportsAccessUpdateIds"));
				
		permReportDisclosure.setContent(permLayout);
		permReportDisclosure.setVisible(true);
    }

    
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("status")) {
            layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("pathName")) {
        	layout.addComponent(field, 0, 2);
        } else if (propertyId.equals("displayName")) {
            layout.addComponent(field, 1, 2);
        } else if (propertyId.equals("description")) {
            layout.addComponent(field, 0, 3, 1, 3);
        } else if (propertyId.equals("transactionTemplateIds")) {
        	layout.addComponent(field, 2, 2, 2, 6);
        } else if (propertyId.equals("limitDocumentIds")) {
        	layout.addComponent(field, 3, 2, 3, 6);
        } else if (propertyId.equals("comments")) {
        	layout.addComponent(field, 0, 4, 1, 6);
        }
    }

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("ReportTemplateView.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		ReportTemplateBean currBean = getCurrentBean();
		if ( currBean != null && currBean.duplicatedReportTemplate().hasChanged() ) // may be modified by report field templates
			return true;
		return isModified();
	}
}