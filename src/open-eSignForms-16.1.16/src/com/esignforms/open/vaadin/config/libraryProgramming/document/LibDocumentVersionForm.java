// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.ListIterator;

import org.vaadin.dialogs.ConfirmDialog;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.prog.DocumentStyleInfo;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DocumentVersionPage;
import com.esignforms.open.prog.Library;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The LibDocumentVersionForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class LibDocumentVersionForm extends CustomComponent implements EsfView {
	private static final long serialVersionUID = -6509709814576750391L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibDocumentVersionForm.class);
	
	public class PageFormOpened implements java.io.Serializable {
		private static final long serialVersionUID = -4492546264859621094L;

		public ConfirmDiscardFormChangesWindow w;
		public LibDocumentVersionPageView pageView;
		public PageFormOpened(ConfirmDiscardFormChangesWindow w, LibDocumentVersionPageView pageView) {
			this.w = w;
			this.pageView = pageView;
		}
		public boolean equals(Object o) {
			if ( o instanceof PageFormOpened )
				return w.equals( ((PageFormOpened)o).w );
			return false;
		}
		public int hashCode() {
			return w.hashCode();
		}
	}
	LinkedList<PageFormOpened> pageFormOpenedList;
    
	final LibDocumentVersionForm thisForm;
	final LibDocumentAndVersionsMainView mainView;
	LibDocumentVersionBeanContainer container;
	FieldGroup fieldGroup;
	VerticalLayout layout;
	
	Label id;
	Label createdByInfo;
	Label lastUpdatedByInfo;
	Label versionInfo;

	Button testToProdButton;
	Button createTestFromProdButton;
	Button revertProdToTestButton;
	Button testButton;	
	Button deleteButton;

	final PageInfoList pageInfoList;
	boolean pageInfoListModified;
	Button addPageButton;
	
	public LibDocumentVersionForm(LibDocumentAndVersionsMainView mainViewParam, LibDocumentVersionBeanContainer container) {
    	setStyleName("LibDocumentVersionForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.mainView = mainViewParam;
    	this.container = container;
    	this.thisForm = this;
    	this.fieldGroup = new FieldGroup();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	pageFormOpenedList = new LinkedList<PageFormOpened>();

    	setImmediate(true); // so our validators can run
    	
    	layout = new VerticalLayout();
    	layout.setVisible(false);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setCompositionRoot(layout);
		
    	HorizontalLayout line1 = new HorizontalLayout();
    	line1.setWidth(100,Unit.PERCENTAGE);
    	layout.addComponent(line1);
		versionInfo = new Label();
		versionInfo.setContentMode(ContentMode.TEXT);	
		line1.addComponent(versionInfo);
		
		addPageButton = new Button(vaadinUi.getMsg("LibDocumentVersionForm.pageInfoList.button.addPage.label"), new ClickListener() {
			private static final long serialVersionUID = -3259387784830969664L;

			@Override
			public void buttonClick(ClickEvent event) {
				LibDocumentVersionBean bean = getCurrentBean();
				int pageNum = pageInfoList.getNumPages() + 1;
				DocumentVersionPage docVerPage = DocumentVersionPage.Manager.createNew( bean.duplicateDocumentVersion(), (short)pageNum );
				// If our page name happens to match an existing page name, we'll rename it further until it's unique...
				boolean isUniqueName = false;
				int suffixIndex = 1;
				while( ! isUniqueName )
				{
					isUniqueName = true;
					for( EsfName existingName : pageInfoList.getPageNames() )
					{
						if ( existingName.equals(docVerPage.getEsfName()) )
						{
							isUniqueName = false;
							docVerPage.setEsfName( new EsfName(docVerPage.getEsfName() + "_" + suffixIndex++) );
							break;
						}
					}
				}	
				pageInfoList.addPageInfo(docVerPage);
				bean.duplicateDocumentVersion().addPage(docVerPage);
			}
			
		});
		addPageButton.setStyleName(Reindeer.BUTTON_SMALL);
		addPageButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.add.icon")));
		line1.addComponent(addPageButton);
		line1.setComponentAlignment(addPageButton, Alignment.BOTTOM_RIGHT);

		pageInfoList = new PageInfoList();
		pageInfoList.setWidth(100, Unit.PERCENTAGE);
		pageInfoList.initializeDND();
		layout.addComponent(pageInfoList);
		
    	HorizontalLayout lineOptions = new HorizontalLayout();
    	lineOptions.setWidth(100,Unit.PERCENTAGE);
    	layout.addComponent(lineOptions);
    	
		NativeSelect documentStyleSelect = new NativeSelect(vaadinUi.getMsg("LibDocumentVersionForm.documentStyleId.label"));
		documentStyleSelect.setNullSelectionAllowed(false); 
		documentStyleSelect.setImmediate(true);
		documentStyleSelect.setRequired(true);
		documentStyleSelect.setMultiSelect(false);
		documentStyleSelect.addValidator(new SelectValidator(documentStyleSelect));
		documentStyleSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		// actual options set in setupDocumentStyleSelect()
    	lineOptions.addComponent(documentStyleSelect);
    	fieldGroup.bind(documentStyleSelect, "documentStyleId");
    	
		NativeSelect pageOrientationSelect = new NativeSelect(vaadinUi.getMsg("LibDocumentVersionForm.pageOrientation.label"));
		pageOrientationSelect.setNullSelectionAllowed(false); 
		pageOrientationSelect.setImmediate(true);
		pageOrientationSelect.setRequired(true);
		pageOrientationSelect.setMultiSelect(false);
		pageOrientationSelect.addValidator(new SelectValidator(pageOrientationSelect));
		pageOrientationSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		pageOrientationSelect.addItem(DocumentVersion.PAGE_ORIENTATION_PORTRAIT);
		pageOrientationSelect.addItem(DocumentVersion.PAGE_ORIENTATION_LANDSCAPE);
		pageOrientationSelect.setItemCaption(DocumentVersion.PAGE_ORIENTATION_PORTRAIT, vaadinUi.getPrettyCode().pageOrientation(DocumentVersion.PAGE_ORIENTATION_PORTRAIT));
		pageOrientationSelect.setItemCaption(DocumentVersion.PAGE_ORIENTATION_LANDSCAPE, vaadinUi.getPrettyCode().pageOrientation(DocumentVersion.PAGE_ORIENTATION_LANDSCAPE));
		lineOptions.addComponent(pageOrientationSelect);
    	fieldGroup.bind(pageOrientationSelect, "pageOrientation");

		NativeSelect esignProcessRecordLocationSelect = new NativeSelect(vaadinUi.getMsg("LibDocumentVersionForm.esignProcessRecordLocation.label"));
		esignProcessRecordLocationSelect.setNullSelectionAllowed(false); 
		esignProcessRecordLocationSelect.setImmediate(true);
		esignProcessRecordLocationSelect.setRequired(true);
		esignProcessRecordLocationSelect.setMultiSelect(false);
		esignProcessRecordLocationSelect.addValidator(new SelectValidator(esignProcessRecordLocationSelect));
		esignProcessRecordLocationSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		esignProcessRecordLocationSelect.addItem(DocumentVersion.ESIGN_PROCESS_RECORD_LOCATION_FINAL_OWN_PAGE);
		esignProcessRecordLocationSelect.addItem(DocumentVersion.ESIGN_PROCESS_RECORD_LOCATION_LAST_DOCUMENT_PAGE);
		esignProcessRecordLocationSelect.addItem(DocumentVersion.ESIGN_PROCESS_RECORD_LOCATION_NONE);
		esignProcessRecordLocationSelect.setItemCaption(DocumentVersion.ESIGN_PROCESS_RECORD_LOCATION_FINAL_OWN_PAGE, vaadinUi.getPrettyCode().esignProcessRecordLocation(DocumentVersion.ESIGN_PROCESS_RECORD_LOCATION_FINAL_OWN_PAGE));
		esignProcessRecordLocationSelect.setItemCaption(DocumentVersion.ESIGN_PROCESS_RECORD_LOCATION_LAST_DOCUMENT_PAGE, vaadinUi.getPrettyCode().esignProcessRecordLocation(DocumentVersion.ESIGN_PROCESS_RECORD_LOCATION_LAST_DOCUMENT_PAGE));
		esignProcessRecordLocationSelect.setItemCaption(DocumentVersion.ESIGN_PROCESS_RECORD_LOCATION_NONE, vaadinUi.getPrettyCode().esignProcessRecordLocation(DocumentVersion.ESIGN_PROCESS_RECORD_LOCATION_NONE));
		lineOptions.addComponent(esignProcessRecordLocationSelect);
    	fieldGroup.bind(esignProcessRecordLocationSelect, "esignProcessRecordLocation");

		lastUpdatedByInfo = new Label();
		lastUpdatedByInfo.setContentMode(ContentMode.TEXT);
		lastUpdatedByInfo.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedByInfo);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo);		

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id);

		VerticalLayout footer = new VerticalLayout();
		footer.setStyleName("footer");
    	footer.setSpacing(true);
    	footer.setMargin(false);
    	layout.addComponent(footer);
    	
    	HorizontalLayout buttonArea1 = new HorizontalLayout();
    	buttonArea1.setMargin(false);
    	buttonArea1.setSpacing(true);
    	testToProdButton = new Button(vaadinUi.getMsg("LibDocumentVersionForm.button.testToProd.label"));
    	testToProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionForm.button.testToProd.icon")));
    	testToProdButton.setDescription(vaadinUi.getMsg("LibDocumentVersionForm.button.testToProd.tooltip"));
    	testToProdButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = -1981840850019506070L;

			@Override
			public void buttonClick(ClickEvent event) {
				mainView.promoteTestToProductionVersion();
			}
    		
    	});
    	buttonArea1.addComponent(testToProdButton);

    	createTestFromProdButton = new Button(vaadinUi.getMsg("LibDocumentVersionForm.button.createTestFromProd.label"));
    	createTestFromProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionForm.button.createTestFromProd.icon")));
    	createTestFromProdButton.setDescription(vaadinUi.getMsg("LibDocumentVersionForm.button.createTestFromProd.tooltip"));
    	createTestFromProdButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = 7051380800006900329L;

			@Override
			public void buttonClick(ClickEvent event) {
				mainView.createNextVersion();
			}
    		
    	});
    	buttonArea1.addComponent(createTestFromProdButton);

    	revertProdToTestButton = new Button(vaadinUi.getMsg("LibDocumentVersionForm.button.revertProdToTest.label"));
    	revertProdToTestButton.addStyleName("caution");
    	revertProdToTestButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionForm.button.revertProdToTest.icon")));
    	revertProdToTestButton.setDescription(vaadinUi.getMsg("LibDocumentVersionForm.button.revertProdToTest.tooltip"));
    	revertProdToTestButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = -7863471976227243891L;

				@Override
    			public void buttonClick(ClickEvent event) {
    				mainView.revertProductionVersionBackToTest();
    			}
        		
        	});
    	buttonArea1.addComponent(revertProdToTestButton);

    	if ( mainView.hasPermDelete() ) {
    		deleteButton = new Button(vaadinUi.getMsg("LibDocumentVersionForm.button.delete.label"));
    		deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionForm.button.delete.icon")));
    		deleteButton.setDescription(vaadinUi.getMsg("LibDocumentVersionForm.button.delete.tooltip"));
	    	deleteButton.addStyleName("deleteButton");
	    	deleteButton.addStyleName("caution");
	    	deleteButton.addClickListener( new ClickListener() {
				private static final long serialVersionUID = 7408440181527482548L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			    	ConfirmDialog.show(UI.getCurrent(), 
			    			vaadinUi.getMsg("LibDocumentVersionForm.button.delete.ConfirmDialog.caption"),
			    			vaadinUi.getMsg("LibDocumentVersionForm.button.delete.ConfirmDialog.message"),
			    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
			    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
			    	        new ConfirmDialog.Listener() {
								private static final long serialVersionUID = 2630503648343098682L;

								public void onClose(ConfirmDialog dialog) {
			    	                if (dialog.isConfirmed()) {
			    	                	mainView.delete();
			    	                }
			    	            }
			    	        });
				}
	    	});
	    	buttonArea1.addComponent(deleteButton);
		}
    	footer.addComponent(buttonArea1);
    	
    	HorizontalLayout buttonArea2 = new HorizontalLayout();
    	buttonArea2.setMargin(false);
    	buttonArea2.setSpacing(true);
    	testButton = new Button(vaadinUi.getMsg("LibDocumentVersionForm.button.test.label"));
    	testButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionForm.button.test.icon")));
    	testButton.setDescription(vaadinUi.getMsg("LibDocumentVersionForm.button.test.tooltip"));
    	testButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = -7863471976227243891L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		        final LibDocumentVersionTester docTester = new LibDocumentVersionTester(getCurrentBean().duplicateDocumentVersion(),1);
		        docTester.initView();
		        docTester.activateView(OpenMode.WINDOW, "");
				ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("LibDocumentVersionTester.caption",getCurrentBeanName()));
				w.center();
				w.addStyleName("LibDocumentVersionTester");
		        w.setWidth(800, Unit.PIXELS);
		        w.setHeight(95, Unit.PERCENTAGE);
		        w.setView(docTester); // so we can detect if our form has changes when closing
		        w.getLayout().addComponent(docTester);
		        vaadinUi.addWindowToUI(w);
			}
    	});
    	buttonArea2.addComponent(testButton);
    	
    	footer.addComponent(buttonArea2);
    	
    	setReadOnly(true);   	

    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
    	LibDocumentVersionBean currBean = getCurrentBean();
    	if ( currBean != null ) {
    		currBean.loadDuplicateDocumentVersion();
    	}
    }
    
    
    LibDocumentVersionBean save(Connection con) throws SQLException {
    	LibDocumentVersionBean currBean = getCurrentBean();
    	if ( currBean != null ) {
    		currBean.setPageOrder(pageInfoList.getPageNames());
    		if ( currBean.save(con,null) ) {
    			pageInfoListModified = false;
    			return currBean;
    		}
    	}
    	return null;
    }
    	
    public String getCurrentBeanName() {
    	LibDocumentVersionBean currBean = getCurrentBean();
		return currBean == null ? "(None)" : mainView.getEsfNameWithVersion();
	}
    
	LibDocumentVersionBean getCurrentBean() {
    	return getBean(fieldGroup.getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public LibDocumentVersionBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibDocumentVersionBean> bi = (BeanItem<LibDocumentVersionBean>)dataSource;
		return bi.getBean();
    }
    
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached stuff
		resetCaches();
		
    	if (newDataSource != null) {
    		closeAllPageForms();

    		LibDocumentVersionBean bean = getBean(newDataSource);
    		
    		setupDocumentStyleSelect(bean); // We want to set this up before we set the values from our data source
    		
    		fieldGroup.setItemDataSource(newDataSource);
    		
    		setupForm(bean);
    		
    		setReadOnly( ! mainView.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		layout.setVisible(true);
    	} else {
    		fieldGroup.setReadOnly(false);
    		fieldGroup.setItemDataSource(null);
    		layout.setVisible(false);
    	}
    }
	

    @Override
    public void setReadOnly(boolean readOnly) {
    	LibDocumentVersionBean bean = getCurrentBean();

    	boolean makeVisible = !readOnly && bean != null;
    	
    	testToProdButton.setVisible(makeVisible && mainView.isTestVersion(bean.docVerInfo()) && bean.duplicateDocumentVersion().doUpdate() && mainView.getLibDocumentBean().isEnabled());
    	createTestFromProdButton.setVisible(makeVisible && mainView.isProductionVersion(bean.docVerInfo()) && !mainView.hasTestVersion());
    	revertProdToTestButton.setVisible(makeVisible && mainView.isProductionVersion(bean.docVerInfo()) && !mainView.hasTestVersion());
    	
    	// Our test button is always available if we have document version bean set and it's not new, enabled and is the test version or is production if there is no test version
    	testButton.setVisible(bean != null && bean.duplicateDocumentVersion().doUpdate() && 
    			              mainView.getLibDocumentBean().isEnabled() && 
    			              (mainView.isTestVersion(bean.docVerInfo()) || (mainView.isProductionVersion(bean.docVerInfo()) && !mainView.hasTestVersion())));

    	// Show this only if the object is created and is the Test version
    	if ( deleteButton != null ) {
    		deleteButton.setVisible(makeVisible && bean.duplicateDocumentVersion().doUpdate() && mainView.isTestVersion(bean.docVerInfo()));
    	}

    	// If the request is to be not-readonly, but this is not the test version, we convert it back to readonly
    	if ( !readOnly && bean != null && !mainView.isTestVersion(bean.docVerInfo())) {
    		readOnly = true;
    	}
    	super.setReadOnly(readOnly);
    	if ( bean != null )
    		fieldGroup.setReadOnly(readOnly);
    	addPageButton.setVisible(makeVisible);
    	pageInfoList.setButtonsVisible(makeVisible);
    	pageInfoList.setReadOnly(readOnly);
    }
    
    void setupForm(LibDocumentVersionBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("LibDocumentVersionForm.label.id",bean.getId()) );
		
		versionInfo.setValue( vaadinUi.getMsg("LibDocumentVersionForm.label.version",bean.getVersion(),mainView.getVersionLabel(bean.docVerInfo())) );
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.docVerInfo().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("LibDocumentVersionForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );
		
		if ( bean.duplicateDocumentVersion().doInsert() ) {
			lastUpdatedByInfo.setValue( vaadinUi.getMsg("LibDocumentVersionForm.label.lastUpdated.whenNew") );
			lastUpdatedByInfo.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.docVerInfo().getLastUpdatedByUserId());
			lastUpdatedByInfo.setValue( vaadinUi.getMsg("LibDocumentVersionForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedByInfo.setIcon(null);
		}

		setPages(bean.duplicateDocumentVersion());
		pageInfoList.setVisible(true);
		pageInfoListModified = false;
    }
    
    void setupDocumentStyleSelect(LibDocumentVersionBean bean) {
    	NativeSelect documentStyleSelect = (NativeSelect)fieldGroup.getField("documentStyleId");
    	if ( documentStyleSelect != null ) {
    		boolean documentStyleSelectIsReadOnly = documentStyleSelect.isReadOnly();
    		if ( documentStyleSelectIsReadOnly )
    			documentStyleSelect.setReadOnly(false);
    		
        	documentStyleSelect.removeAllItems();
    		// Update our document styles to include both the current library and the template library
    		Library templateLibrary = Library.Manager.getTemplate();
        	Collection<DocumentStyleInfo> allDocumentStyles;
    		if ( bean != null ) {
            	allDocumentStyles = DocumentStyleInfo.Manager.getAll(mainView.library);
        		if ( ! mainView.library.equals(templateLibrary) ) {
    	        	allDocumentStyles.addAll(DocumentStyleInfo.Manager.getAll(templateLibrary));
        		}
    		} else { // shouldn't be the case, but we'll at least have the template's document style
    			allDocumentStyles = DocumentStyleInfo.Manager.getAll(templateLibrary);
    		}
        	for( DocumentStyleInfo dsi : allDocumentStyles ) {
        		documentStyleSelect.addItem(dsi.getId());
        		documentStyleSelect.setItemCaption( dsi.getId(), dsi.getEsfName().toString() );
        	}

        	if ( documentStyleSelectIsReadOnly )
    			documentStyleSelect.setReadOnly(true);
    	}
    }
    
    void setPages(DocumentVersion documentVersion) {
    	pageInfoList.setupPages(documentVersion);
    }
    
    void pageFormOpened( ConfirmDiscardFormChangesWindow w, LibDocumentVersionPageView pageView ) {
    	synchronized(pageFormOpenedList) {
    		PageFormOpened pfo = new PageFormOpened(w,pageView);
    		pageFormOpenedList.add(pfo);
    	}
    }

    void pageFormClosed( ConfirmDiscardFormChangesWindow w ) {
    	synchronized(pageFormOpenedList) {
    		ListIterator<PageFormOpened> iter = pageFormOpenedList.listIterator();
    		while( iter.hasNext() ) {
    			PageFormOpened pfo = iter.next();
    			if ( pfo.w.equals(w) ) {
    				if ( pfo.pageView.isDirty() ) {
    					discard();
    				}
    	    		iter.remove();
    	    		break;
    			}
    		}
    	}
    }
    
    void closeAllPageForms() {
    	synchronized(pageFormOpenedList) {
   			// closing the window will call the page view's detach which will remove the window from this list, so we make our own copy
    		for( PageFormOpened pfo : new LinkedList<PageFormOpened>(pageFormOpenedList) ) { 
    			pfo.w.superClose();
    		}
    		pageFormOpenedList.clear();
    	}
    }

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}
	
	@Override
	public void detach() {
    	discard();
    	super.detach();
	}

	@Override
	public String checkDirty() { // OBSOLETE SINCE VERSION FORM HAS NO BUTTONS FOR SAVING, CANCELING
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibDocumentAndVersionsMainView.Document.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		if ( fieldGroup.isModified() || pageInfoListModified ) {
			return true;
		}
		LibDocumentVersionBean bean = getCurrentBean();
		if ( bean == null ) {
			return false;
		}
		
		if ( bean.isModified() )
			return true;
		
		return false;
	}
	
	public boolean isValid() {
		return fieldGroup.isValid();
	}
	
    public void commit() throws CommitException {
    	fieldGroup.commit();
    	synchronized(pageFormOpenedList) {
    		for( PageFormOpened pfo : pageFormOpenedList ) {
    			pfo.pageView.commit();
    		}
    	}
    }
    
    public void discard() {
    	fieldGroup.discard();
    	LibDocumentVersionBean bean = getCurrentBean();
    	if ( bean != null ) {
    		bean.loadDuplicateDocumentVersion();
    		pageInfoList.setupPages(bean.duplicateDocumentVersion());
    	}
    }

    //*************************  PageList is our table of pages in this document ******************************
	class PageInfoContainer extends BeanItemContainer<PageInfo> implements Serializable {
		private static final long serialVersionUID = 5505705684876636348L;

		PageInfoContainer() {
			super(PageInfo.class);
		}

		void setPages(DocumentVersion documentVersion) {
			super.removeAllItems();
			
			int pageNumber = 1;
			for( DocumentVersionPage page : documentVersion.getPages() ) {
				PageInfo pi = new PageInfo(pageNumber++, page);
				addItem( pi );
			}
		}
	}
	
	
	public class PageInfo implements java.io.Serializable {
		private static final long serialVersionUID = 1917347494934018300L;

		final PageInfo thisInfo;
		int pageNumber;
		DocumentVersionPage docVerPage;
		Button deleteButton;
		
		public PageInfo(int pageNumber, DocumentVersionPage docVerPage) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			this.thisInfo = this;
			this.pageNumber = pageNumber;
			this.docVerPage = docVerPage;
			this.deleteButton = new Button(vaadinUi.getMsg("LibDocumentVersionForm.pageInfoList.button.delete.label"), new ClickListener() {
				private static final long serialVersionUID = 5754937080428280757L;

				@Override
				public void buttonClick(ClickEvent event) {
					pageInfoList.removePageInfo(thisInfo);
					LibDocumentVersionBean bean = getCurrentBean();
					bean.duplicateDocumentVersion().removePage(thisInfo.getDocumentVersionPage());
				}
	        });
			this.deleteButton.setStyleName(Reindeer.BUTTON_SMALL);
			this.deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
		}
		
		public int getPageNumber() {
			return pageNumber;
		}
		public void setPageNumber(int v) {
			pageNumber = v;
		}

		public EsfName getPageName() {
			return docVerPage.getEsfName();
		}
		
		public String getEditReviewType() {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			return vaadinUi.getPrettyCode().documentPageEditReviewType(docVerPage.getEditReviewType());
		}
		
		public DocumentVersionPage getDocumentVersionPage() {
			return docVerPage;
		}
		
		public Button getDeleteButton() {
			return deleteButton;
		}
	}
    
	class PageInfoList extends Table {
		private static final long serialVersionUID = -8312621333080438659L;

		PageInfoContainer pageInfoContainer;
		final PageInfoList thisList;
		
		PageInfoList() {
			super();
			thisList = this;
			addStyleName("pageInfoList");
			// Experimental code in Vaadin
			this.alwaysRecalculateColumnWidths = true;
			
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			pageInfoContainer = new PageInfoContainer();
			setContainerDataSource(pageInfoContainer);
			setVisibleColumns( (Object[])vaadinUi.getStringArray("LibDocumentVersionForm.pageInfoList.columnProperties") );
			setColumnHeaders( vaadinUi.getStringArray("LibDocumentVersionForm.pageInfoList.columnHeaders") );
			setColumnAlignment("pageNumber", Align.CENTER); 
			setColumnWidth("pageNumber", 60);
			setColumnExpandRatio("pageName", 0.5f);
			setColumnWidth("editReviewType", 100);
			setColumnWidth("deleteButton", 85);
			setSortEnabled(false);
			setNullSelectionAllowed(true);
			setSelectable(true);
			setImmediate(true);
			
			addValueChangeListener( new Property.ValueChangeListener() {
				private static final long serialVersionUID = 1939637889032857427L;

				@Override
				public void valueChange(Property.ValueChangeEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					PageInfo pageInfo = (PageInfo)getValue();
					if ( pageInfo != null ) {
			   	 		LibDocumentVersionBean currBean = getCurrentBean();
			   	 		
			   	 		if ( currBean.duplicateDocumentVersion().doInsert() || anyUnsavedPages() ) {
			   	 			vaadinUi.showWarning(vaadinUi.getMsg("LibDocumentVersionForm.pageInfo.notsaved.caption"), vaadinUi.getMsg("LibDocumentVersionForm.pageInfo.notsaved.message"));
				            setValue(null); // Unselect our row so it can be selected again easily
			   	 			return;
			   	 		}
			   	 		
			            final LibDocumentVersionPageView pageView = new LibDocumentVersionPageView(mainView, thisForm, currBean, pageInfo.getPageName(), pageInfo.getPageNumber());
			            pageView.initView();

			            String pageTitle = vaadinUi.getMsg("LibDocumentVersionForm.pageWindow.caption",
			            				thisForm.mainView.getEsfNameWithVersion(),
			            				pageInfo.getPageName());
			            
			            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(pageTitle, pageView);
			            w.center();
			        	w.setWidth(98, Unit.PERCENTAGE);
			        	w.setHeight(95, Unit.PERCENTAGE);
			   	 		pageView.setReadOnly(thisForm.isReadOnly() || ! thisForm.mainView.isTestVersion(currBean.docVerInfo()) );
			            pageView.activateView(EsfView.OpenMode.WINDOW, "");
			            vaadinUi.addWindowToUI(w);
			            pageFormOpened(w,pageView);
			            setValue(null); // Unselect our row so it can be selected again easily
					}
				}
			});
		}
		
		boolean anyUnsavedPages() {
            for( PageInfo pageInfo : pageInfoContainer.getItemIds() ) {
            	if ( pageInfo.docVerPage.doInsert() )
            		return true;
            }
            return false;
		}
		
		@SuppressWarnings("unchecked")
		void reorder() {
            // Fix up the order now
			LinkedList<EsfName> orderList = new LinkedList<EsfName>();
            int order = 1;
            for( PageInfo pageInfo : pageInfoContainer.getItemIds() ) {
            	Item item = pageInfoContainer.getItem(pageInfo);
            	item.getItemProperty("pageNumber").setValue(order);
            	pageInfo.setPageNumber(order++);
            	orderList.add((EsfName)item.getItemProperty("pageName").getValue());
            }
            getCurrentBean().setPageOrder(orderList);
    		pageInfoListModified = true;
		}
		
		void setButtonsVisible(boolean makeVisible) {
			boolean hasMoreThanOnePage = pageInfoContainer.size() > 1;
            for( PageInfo pageInfo : pageInfoContainer.getItemIds() ) {
            	BeanItem<PageInfo> beanItem = pageInfoContainer.getItem(pageInfo);
            	PageInfo bean = beanItem.getBean();
            	bean.getDeleteButton().setVisible(makeVisible && hasMoreThanOnePage);
            }
            
            EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		int size = pageInfoList.getNumPages();
    		String caption ;
    		if ( size > 1 ) {
    			caption = vaadinUi.getMsg("LibDocumentVersionForm.pageInfoList.label.multipages");
    		} else {
    			caption = vaadinUi.getMsg("LibDocumentVersionForm.pageInfoList.label");
    		}

			pageInfoList.setPageLength( size );
    		pageInfoList.setCaption(caption);
		}
		
	    @Override
	    public void setReadOnly(boolean readOnly) {
	        //super.setReadOnly(false); // we want to keep the Table active so we can still click on entries to view their details
	    	setDragMode( readOnly ? TableDragMode.NONE : TableDragMode.ROW);
	    }
		
		void initializeDND() {
			setDropHandler(new DropHandler() {
				private static final long serialVersionUID = 4786896937709502073L;

				public void drop(DragAndDropEvent dropEvent) {
	                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
	                
	                Container sourceContainer = t.getSourceContainer();
	                if ( sourceContainer == pageInfoContainer ) {
	                	// reordering within the table rows
	                	PageInfo sourceItemId = (PageInfo)t.getItemId();

		                AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails)dropEvent.getTargetDetails());
		                PageInfo targetItemId = (PageInfo)dropData.getItemIdOver();
		                
		                // No move if source and target are the same
		                if ( sourceItemId == targetItemId )
		                	return;
		                
		                // Let's remove the source of the drag so we can add it back where requested...
		                pageInfoContainer.removeItem(sourceItemId);
		                if ( targetItemId == null ) {
		                	pageInfoContainer.addItem(sourceItemId);
		                } else if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
		                	pageInfoContainer.addItemAfter(targetItemId,sourceItemId);
		            	} else {
		                    Object prevItemId = pageInfoContainer.prevItemId(targetItemId);
		                    pageInfoContainer.addItemAfter(prevItemId, sourceItemId);
		            	}
		                
		                reorder();
	                }
	            }

	            public AcceptCriterion getAcceptCriterion() {
	                return new And(SourceIsTarget.get(), new Not(AbstractSelect.VerticalLocationIs.MIDDLE)); // dragging within the table
	            }
	        });
			
		} // initializeDND
		
		void setupPages(DocumentVersion documentVersion) {
			pageInfoContainer.removeAllItems();
			if ( documentVersion == null )
				return;
			
			int order = 1;
			for( DocumentVersionPage docVerPage : documentVersion.getPages() ) {
				PageInfo pageInfo = new PageInfo(order++, docVerPage);
				pageInfoContainer.addBean(pageInfo);
			}
			
			pageInfoListModified = false;
		}
		
		void addPageInfo(DocumentVersionPage docVerPage) {
			PageInfo pageInfo = new PageInfo(pageInfoContainer.size()+1,docVerPage);
			pageInfoContainer.addBean(pageInfo);
			pageInfoListModified = true;
    		setButtonsVisible( true ); 
		}
		
		void removePageInfo(PageInfo pageInfo) {
			pageInfoContainer.removeItem(pageInfo);
			pageInfoListModified = true;
    		setButtonsVisible( true ); 
		}
		
		LinkedList<EsfName> getPageNames() {
			LinkedList<EsfName> nameList = new LinkedList<EsfName>();
			for( PageInfo pageInfo : pageInfoContainer.getItemIds() ) {
				nameList.add(pageInfo.getPageName());
			}
			return nameList;
		}
		
		int getNumPages() {
			return pageInfoContainer.size();
		}
		
	} // PageInfoList
	
}