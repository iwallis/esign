// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.image;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;

import org.vaadin.dialogs.ConfirmDialog;

import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.image.Thumbnail;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.widget.UploadFileWithProgress;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.BorderStyle;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * The LibImageVersionForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class LibImageVersionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -5035335427801156001L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibImageVersionForm.class);
	
	final LibImageVersionForm thisForm;
	final LibImageAndVersionsMainView view;
	LibImageVersionBeanContainer container;
	GridLayout layout;
	
	Label id;
	Label createdByInfo;
	Label lastUpdatedByInfo;
	Label versionInfo;
	Label imageInfo;
	Image thumbImage;
	Link viewImageLink;
	UploadFileWithProgress uploadFile;
	
	Button testToProdButton;
	Button createTestFromProdButton;
	Button revertProdToTestButton;
	Button deleteButton;

	public LibImageVersionForm(LibImageAndVersionsMainView view, LibImageVersionBeanContainer container) {
    	setStyleName("LibImageVersionForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.view = view;
    	this.container = container;
    	this.thisForm = this;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);

    	layout = new GridLayout(2,6);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
		
		versionInfo = new Label();
		versionInfo.setContentMode(ContentMode.TEXT);	
		layout.addComponent(versionInfo,0,0);
		
		lastUpdatedByInfo = new Label();
		lastUpdatedByInfo.setContentMode(ContentMode.TEXT);
		lastUpdatedByInfo.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedByInfo,0,1,1,1);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,2,1,2);		

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id,0,3,1,3);
		
		imageInfo = new Label();
		imageInfo.setStyleName("imageInfo");
		imageInfo.setContentMode(ContentMode.TEXT);
		layout.addComponent(imageInfo,0,4);
		
		thumbImage = new Image(vaadinUi.getMsg("LibImageVersionForm.image.thumbnail.label"));
        layout.addComponent(thumbImage,1,4);
        thumbImage.setVisible(false);

		// TODO: 5MB max for an image in our library, make tunable
		uploadFile = new UploadFileWithProgress(null,vaadinUi.getMsg("LibImageVersionForm.button.uploadImage.noimage.label"),(int)vaadinUi.getEsfapp().getUploadImageMaxBytes()) {
			private static final long serialVersionUID = -7747438546039111275L;

			@Override
			public void afterUploadSucceeded() {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				LibImageVersionBean currBean = getCurrentBean();
				byte[] imageData = uploadFile.getFileData();
				try {
					Thumbnail thumb = new Thumbnail(imageData);
					if ( thumb.createThumbnail(100) ) { // TODO: Make thumbnail max width tunable
						byte[] thumbData = thumb.saveThumbnailToBytesByMimeType(uploadFile.getMimeType());
						currBean.setThumbnailData(thumbData);
						if ( thumbData == null ) {
							vaadinUi.showError(vaadinUi.getMsg("LibImageVersionForm.thumbnail.failure.caption"), vaadinUi.getMsg("LibImageVersionForm.thumbnail.failure.message"));
							currBean.setThumbnailData(new byte[0]);
						}
					} else {
						currBean.setThumbnailData(new byte[0]);
						vaadinUi.showError(vaadinUi.getMsg("LibImageVersionForm.thumbnail.failure.caption"), vaadinUi.getMsg("LibImageVersionForm.thumbnail.failure.message"));
					}
				} catch( EsfException e ) {
					_logger.error("Failed to create thumbnail in afterUploadSucceeded()",e);
					currBean.setThumbnailData(new byte[0]);
					vaadinUi.showError(vaadinUi.getMsg("LibImageVersionForm.thumbnail.failure.caption"), vaadinUi.getMsg("LibImageVersionForm.thumbnail.failure.message"));
				}
				currBean.setImageData(imageData);
				currBean.setImageFileName(uploadFile.getFileName());
				currBean.setImageMimeType(uploadFile.getMimeType());
				vaadinUi.showStatus(vaadinUi.getMsg("LibImageVersionForm.upload.successful",uploadFile.getFileName(),EsfInteger.byteSizeInUnits(uploadFile.getFileData().length) ));
				imageInfo.setValue(uploadFile.getFileName());
				imageInfo.removeStyleName("imageInfoError");
				uploadFile.setButtonCaption(vaadinUi.getMsg("LibImageVersionForm.button.uploadImage.hasimage.label"));
				if ( currBean.hasThumbnailData() ) {
					thumbImage.setSource(new com.vaadin.server.ExternalResource(currBean.getThumbnailByIdUrl()));
					thumbImage.setVisible(true);
				} else {
					thumbImage.setVisible(false);
				}
				viewImageLink.setResource(new com.vaadin.server.ExternalResource(currBean.getImageByIdUrl()));
				viewImageLink.setVisible(true);
			}
			@Override
			public void afterUploadFailed(String fileName, boolean wasCanceled, boolean wasInvalidMimeType, boolean wasTooBig, int contentLength, int maxSize) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				if ( wasTooBig ) {
					vaadinUi.showError(
							vaadinUi.getMsg("LibImageVersionForm.fileTooLarge.caption"), 
							vaadinUi.getMsg("LibImageVersionForm.fileTooLarge.message",EsfInteger.byteSizeInUnits(contentLength),EsfInteger.byteSizeInUnits(maxSize))
							);
				} else if ( wasCanceled ) {
					vaadinUi.showStatus(vaadinUi.getMsg("LibImageVersionForm.upload.canceled"));
				} else if ( wasInvalidMimeType ) {
					vaadinUi.showError(
							vaadinUi.getMsg("LibImageVersionForm.invalidType.caption"), 
							vaadinUi.getMsg("LibImageVersionForm.invalidType.message",EsfString.isBlank(fileName)?"(unknown)":fileName)
							);
				}
			}
		}; 
		uploadFile.setAllowedMimeTypesForImages();
		uploadFile.setVisible(false);
		layout.addComponent(uploadFile,0,5);
		
		viewImageLink = new Link();
		viewImageLink.setCaption(vaadinUi.getMsg("LibImageVersionForm.link.viewImage.label"));
		viewImageLink.setTargetName("_blank");
		viewImageLink.setTargetWidth(300);
		viewImageLink.setTargetHeight(300);
		viewImageLink.setTargetBorder(BorderStyle.NONE);
		layout.addComponent(viewImageLink,1,5);
		viewImageLink.setVisible(false);
		
		HorizontalLayout footer = new HorizontalLayout();
		footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);
    	
    	testToProdButton = new Button(vaadinUi.getMsg("LibImageVersionForm.button.testToProd.label"), (ClickListener)this);
    	testToProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibImageVersionForm.button.testToProd.icon")));
    	testToProdButton.setDescription(vaadinUi.getMsg("LibImageVersionForm.button.testToProd.tooltip"));
    	footer.addComponent(testToProdButton);

    	createTestFromProdButton = new Button(vaadinUi.getMsg("LibImageVersionForm.button.createTestFromProd.label"), (ClickListener)this);
    	createTestFromProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibImageVersionForm.button.createTestFromProd.icon")));
    	createTestFromProdButton.setDescription(vaadinUi.getMsg("LibImageVersionForm.button.createTestFromProd.tooltip"));
    	footer.addComponent(createTestFromProdButton);

    	revertProdToTestButton = new Button(vaadinUi.getMsg("LibImageVersionForm.button.revertProdToTest.label"), (ClickListener)this);
    	revertProdToTestButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibImageVersionForm.button.revertProdToTest.icon")));
    	revertProdToTestButton.setDescription(vaadinUi.getMsg("LibImageVersionForm.button.revertProdToTest.tooltip"));
    	footer.addComponent(revertProdToTestButton);

    	if ( view.hasPermDelete() ) {
    		deleteButton = new Button(vaadinUi.getMsg("LibImageVersionForm.button.delete.label"), (ClickListener)this);
    		deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibImageVersionForm.button.delete.icon")));
    		deleteButton.setDescription(vaadinUi.getMsg("LibImageVersionForm.button.delete.tooltip"));
	    	deleteButton.addStyleName("deleteButton");
	    	deleteButton.addStyleName("caution");
	    	footer.addComponent(deleteButton);
		}
    	
    	setFooter(footer);
    	setReadOnly(true);   	

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -4489442677445158196L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

				if ( propertyId.equals("useDataUri") ) {
					CheckBox cb = new CheckBox(vaadinUi.getMsg("LibImageVersionForm.useDataUri.label"));
					cb.setImmediate(false);
					cb.setDescription(vaadinUi.getMsg("LibImageVersionForm.useDataUri.tooltip"));
					return cb;
				}
				
				Field field = super.createField(item, propertyId, uiContext);
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
    }
    
    @Override
	public void buttonClick(ClickEvent event) {
    	LibImageVersionBean currBean = getCurrentBean();
    	if ( currBean == null ) return;
    	
    	Button source = event.getButton();
    	if ( source == testToProdButton ) {
    		view.promoteTestToProductionVersion();
    	} else if ( source == createTestFromProdButton ) {
    		view.createNextVersion();
    	} else if ( source == revertProdToTestButton ) {
    		view.revertProductionVersionBackToTest();
    	} else if ( source == deleteButton ) {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	ConfirmDialog.show(UI.getCurrent(), 
	    			vaadinUi.getMsg("LibImageVersionForm.button.delete.ConfirmDialog.caption"),
	    			vaadinUi.getMsg("LibImageVersionForm.button.delete.ConfirmDialog.message"),
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
	    	        new ConfirmDialog.Listener() {
						private static final long serialVersionUID = -4822982152376198193L;

						public void onClose(ConfirmDialog dialog) {
	    	                if (dialog.isConfirmed()) {
	    	                    view.delete();
	    	                }
	    	            }
	    	        });
    	}
     }
	
    LibImageVersionBean save(Connection con) throws SQLException {
    	LibImageVersionBean currBean = getCurrentBean();
    	if ( currBean != null ) {
    		if ( currBean.save(con,null) ) {
    			return currBean;
    		}
    	}
    	return null;
    }
    	
    public String getCurrentBeanName() {
    	LibImageVersionBean currBean = getCurrentBean();
		return currBean == null || currBean.imageVersion() == null ? "(None)" : view.getEsfNameWithVersion();
	}
    
	LibImageVersionBean getCurrentBean() {
		LibImageVersionBean currBean = getBean(getItemDataSource());
    	return currBean == null || currBean.imageVersion() == null ? null : currBean;
    }
    @SuppressWarnings("unchecked")
	public LibImageVersionBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibImageVersionBean> bi = (BeanItem<LibImageVersionBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached stuff
		resetCaches();
		
    	if (newDataSource != null) {
    		LibImageVersionBean bean = getBean(newDataSource);
    		
    		super.setItemDataSource(newDataSource, Arrays.asList("useDataUri"));
    		
    		setupForm(bean);
    		
    		setReadOnly( ! view.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	

    @Override
    public void setReadOnly(boolean readOnly) {   	
    	LibImageVersionBean bean = getCurrentBean();
    	
    	boolean isTestVersion = bean != null && view.isTestVersion(bean.imageVerInfo());
    	
    	testToProdButton.setVisible(bean != null && !readOnly && isTestVersion && bean.imageVersion().doUpdate() && view.getLibImageBean().isEnabled());
    	createTestFromProdButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.imageVerInfo()) && !view.hasTestVersion());
    	revertProdToTestButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.imageVerInfo()) && !view.hasTestVersion());
    	
    	uploadFile.setVisible(bean != null && isTestVersion && !readOnly);
    	
    	// Show this only if the object is created and is the Test version
    	if ( deleteButton != null ) {
    		deleteButton.setVisible(bean != null && bean.imageVersion().doUpdate() && isTestVersion); 
    	}

    	// If the request is to be not-readonly, but this is not the test version, we convert it back to readonly
    	if ( !readOnly && bean != null && !view.isTestVersion(bean.imageVerInfo())) {
    		readOnly = true;
    	}
    	super.setReadOnly(readOnly);
    }
    
    void setupForm(LibImageVersionBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("LibImageVersionForm.label.id",bean.getId()) );
		
		versionInfo.setValue( vaadinUi.getMsg("LibImageVersionForm.label.version",bean.getVersion(),view.getVersionLabel(bean.imageVerInfo())) );
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.imageVerInfo().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("LibImageVersionForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );
		
		if ( bean.imageVersion().doInsert() ) {
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibImageVersionForm.label.lastUpdated.whenNew") );
			lastUpdatedByInfo.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.imageVerInfo().getLastUpdatedByUserId());
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibImageVersionForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedByInfo.setIcon(null);
		}
		
		if ( bean.hasImageFileName() ) {
			imageInfo.setValue(bean.getImageFileName());
			imageInfo.removeStyleName("imageInfoError");
			uploadFile.setButtonCaption(vaadinUi.getMsg("LibImageVersionForm.button.uploadImage.hasimage.label"));
			if ( bean.hasThumbnailData() ) {
				thumbImage.setVisible(true);
				thumbImage.setSource(new ExternalResource(bean.getThumbnailByIdUrl()));
			} else {
				thumbImage.setVisible(false);
			}
			viewImageLink.setVisible(true);
			viewImageLink.setResource(new ExternalResource(bean.getImageByIdUrl()));
		} else {
			imageInfo.setValue(vaadinUi.getMsg("LibImageVersionForm.label.imageInfo.noimage"));
			imageInfo.addStyleName("imageInfoError");
			uploadFile.setButtonCaption(vaadinUi.getMsg("LibImageVersionForm.button.uploadImage.noimage.label"));
			thumbImage.setVisible(false);
			viewImageLink.setVisible(false);
		}
    }

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}
	
	@Override
	public boolean isValid() {
		if ( ! super.isValid() )
			return false;
		LibImageVersionBean bean = getCurrentBean();
		return bean.isImageVersionReady();
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
    	if ( propertyId.equals("useDataUri") ) {
    		layout.addComponent(field,1,0);
    	}
    }


	@Override
	public String checkDirty() { // OBSOLETE SINCE VERSION FORM HAS NO BUTTONS FOR SAVING, CANCELING
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibImageAndVersionsMainView.Image.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		LibImageVersionBean bean = getCurrentBean();
		return isModified() || (bean != null && bean.hasChanged()); // handles uploaded images that are not part of the form, but have changed our underlying object
	}
	
	public void discard() {
		super.discard();
		LibImageVersionBean bean = getCurrentBean();
		if ( bean != null ) {
			bean.discard();
			if ( bean.imageVersion() != null ) {
				BeanItem<LibImageVersionBean> bi = new BeanItem<LibImageVersionBean>(bean);
				setItemDataSource(bi);
			}
		}
	}
}