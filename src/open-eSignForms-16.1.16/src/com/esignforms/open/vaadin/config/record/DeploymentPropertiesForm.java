// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.record;

import java.util.Arrays;
import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.data.Record;
import com.esignforms.open.prog.ButtonMessage;
import com.esignforms.open.prog.ButtonMessageVersion;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentStyle;
import com.esignforms.open.prog.DocumentStyleVersion;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.EmailTemplateVersion;
import com.esignforms.open.prog.File;
import com.esignforms.open.prog.FileVersion;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PropertySet;
import com.esignforms.open.prog.PropertySetVersion;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.Serial;
import com.esignforms.open.prog.SerialVersion;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.Permission;
import com.esignforms.open.user.UI;
import com.esignforms.open.user.User;
import com.esignforms.open.util.RandomKey;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.EsfIntegerRangeValidator;
import com.esignforms.open.vaadin.validator.EsfStringRegexpValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.NativeSelectDropDown;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The DeploymentPropertiesForm is a single record (no need for a table to drive it), of type Record, hard-coded to have a specific view
 * for users to manipulate it.
 * @author Yozons Inc.
 */
public class DeploymentPropertiesForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 4941925313143803217L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DeploymentPropertiesForm.class);
	
	static List<String> _orderedProperties;

	Button saveButton;
	Button cancelButton;
	Button editButton;
	Button clearCacheButton;
	
    GridLayout layout;
    
    HorizontalLayout prodTestTransactionLayout;
    HorizontalLayout smtpExtrasLayout;
    HorizontalLayout imapExtrasLayout;
    HorizontalLayout retainSystemDaysLayout;
    HorizontalLayout retainUserDaysLayout;

    public DeploymentPropertiesForm() {
    	setStyleName("DeploymentPropertiesForm");
    }
    
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
    			
    		// If we're saving a new group bean
			Errors errors = new Errors();
			DeploymentPropertiesBean currBean = getCurrentBean();
			if ( currBean.save(errors) ) {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",currBean.getEsfName()) );
    			setReadOnly(true);
    			vaadinUi.getEsfapp().loadDeploymentProperties();
    			showOrHideDbSize();
    		} else {
    			vaadinUi.show(errors);
    		}
        } else if ( source == cancelButton ) {
    		discard();
    		vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
        	setReadOnly(true);
        } else if ( source == editButton ) {
        	setReadOnly(false);
        } else if ( source == clearCacheButton ) {
        	clearCaches();
        }
    }
	
	void clearCaches() {
    	// Load our User and Group caches in that order; Group cache load will also load the Permission cache,
    	// and Group will load itself into the various users who are members of it.
		Permission.Manager.reloadCache();
    	User.Manager.reloadCache();
    	Group.Manager.reloadCache();
    	Permission.Manager.refreshGroups();

		// Keep in sync with the cache clearing in the Backgrounder.do5minutesCleanup()
		Transaction.Manager.clearCache();
		
		TransactionTemplate.Manager.clearCache();

        Package.Manager.clearCache();
        PackageVersion.Manager.clearCache();
        
        ReportTemplate.Manager.clearCache();
        ReportFieldTemplate.Manager.clearCache();
        
        ButtonMessage.Manager.clearCache();
        ButtonMessageVersion.Manager.clearCache();
        
        Document.Manager.clearCache();
        DocumentVersion.Manager.clearCache();
        
        DocumentStyle.Manager.clearCache();
        DocumentStyleVersion.Manager.clearCache();
        
        DropDown.Manager.clearCache();
        DropDownVersion.Manager.clearCache();
        
        EmailTemplate.Manager.clearCache();
        EmailTemplateVersion.Manager.clearCache();
        
        File.Manager.clearCache();
        FileVersion.Manager.clearCache();
        
        Image.Manager.clearCache();
        ImageVersion.Manager.clearCache();
        
        PropertySet.Manager.clearCache();
        PropertySetVersion.Manager.clearCache();
        
        Serial.Manager.clearCache();
        SerialVersion.Manager.clearCache();
        
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		// For added random security measure, we'll also eat up some PRNG bytes
		RandomKey randomKey = vaadinUi.getEsfapp().getRandomKey();
		randomKey.getBytes(randomKey.getIntBetween(47, 311)); // now get a random number of bytes from our PRNG and discard.
        
		vaadinUi.showStatus( vaadinUi.getMsg("DeploymentPropertiesForm.button.clearCache.success.message") );
		
		User user = vaadinUi.getLoggedInUser();
		String msg = "User " + user.getFullDisplayName() + " cleared caches via the deployment properties form.";
		user.logConfigChange(msg);
		vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(msg);
	}
	
	DeploymentPropertiesBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public DeploymentPropertiesBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<DeploymentPropertiesBean> bi = (BeanItem<DeploymentPropertiesBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item dataSource) {
		if ( _orderedProperties == null ) {
			_orderedProperties = Arrays.asList("id","esfName","deployId","defaultUrlProtocolAddress","installYear","installDate","contextPath",
					"defaultLocale","defaultTimeZone","defaultCountryCode","defaultDateFormat","defaultTimeFormat","defaultLogDateFormat","defaultLogTimeFormat",
					"smtpServer","smtpPort","smtpAuthUser","smtpAuthPassword","smtpStartTLS",
					"imapServer","imapPort","imapUser","imapPassword","imapSSL",
					"defaultEmailFrom","smtpReturnPathHostName",
					"allowProductionTransactions","allowTestTransactions",
					"licenseType", "licenseSize", "dbSize",
					"defaultSessionTimeoutMinutes","loggedInSessionTimeoutMinutes","retainStatsDays",
					"retainSystemStartStopDays","retainSystemConfigChangeDays","retainSystemUserActivityDays","retainUserSecurityDays","retainUserConfigChangeDays","retainUserLoginLogoffDays"
					);
    	}
		super.setItemDataSource(dataSource, _orderedProperties);
    		
		setReadOnly(true);
		//getFooter().setVisible(true);

		showOrHideDbSize();
    }
	
	void showOrHideDbSize() {
		// Suppress the dbSize when it's not a commerical DB size license.
		Field<?> f = getField("dbSize");
		if ( f != null ) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			f.setVisible(vaadinUi.getEsfapp().isLicenseTypeCommercialDbSize());
		}
	}
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	// We have these only while editing
    	if ( saveButton != null ) {
    		/*
    		if ( ! readOnly ) {
    	    	editButton.removeClickShortcut();
    	    	saveButton.setClickShortcut(KeyCode.ENTER);
    		}
    		*/
    		saveButton.setVisible(!readOnly);
    	}
    	cancelButton.setVisible(!readOnly);

    	if ( editButton != null ) {
    		/*
    		if ( readOnly ) {
        		saveButton.removeClickShortcut();
        		editButton.setClickShortcut(KeyCode.ENTER);
    		}
    		*/
    		// We have this only while in view mode
    		editButton.setVisible(readOnly);
    		// We have this only while in edit mode
    		clearCacheButton.setVisible(!readOnly);
    		clearCacheButton.setEnabled(true);
    	}
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Record deploymentProperties = Record.Manager.getById(vaadinUi.getEsfapp().getDeploymentPropertiesId());
		DeploymentPropertiesBean bean = new DeploymentPropertiesBean(deploymentProperties);
		BeanItem<DeploymentPropertiesBean> dataSource = new BeanItem<DeploymentPropertiesBean>(bean);
		setItemDataSource(dataSource);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void initView() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	layout = new GridLayout(3,12);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
    	prodTestTransactionLayout = new HorizontalLayout();
    	prodTestTransactionLayout.setSpacing(true);
    	layout.addComponent(prodTestTransactionLayout, 2, 0);
    	
    	smtpExtrasLayout = new HorizontalLayout();
    	smtpExtrasLayout.setSpacing(true);
    	layout.addComponent(smtpExtrasLayout, 2, 4);
    	
    	imapExtrasLayout = new HorizontalLayout();
    	imapExtrasLayout.setSpacing(true);
    	layout.addComponent(imapExtrasLayout, 2, 5);
    	
    	retainSystemDaysLayout = new HorizontalLayout();
    	retainSystemDaysLayout.setSpacing(true);
    	layout.addComponent(retainSystemDaysLayout, 0, 10, 2, 10);
    	
    	retainUserDaysLayout = new HorizontalLayout();
    	retainUserDaysLayout.setSpacing(true);
    	layout.addComponent(retainUserDaysLayout, 0, 11, 2, 11);
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	//footer.setVisible(false);

    	User loggedInUser = vaadinUi.getUser();
		UI ui = new UI();

		if ( ui.showSaveButton(loggedInUser, UI.UI_DEPLOYMENT_PROPERTIES_VIEW) ) {
			saveButton = new Button(vaadinUi.getMsg("button.save.label"), (ClickListener)this);
			saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
			saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
			saveButton.setDescription(vaadinUi.getMsg("button.save.tooltip"));
	    	footer.addComponent(saveButton);
		}

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	if ( ui.canUpdate(loggedInUser, UI.UI_DEPLOYMENT_PROPERTIES_VIEW) ) {
    		editButton = new Button(vaadinUi.getMsg("button.edit.label"), (ClickListener)this);
    		editButton.setStyleName(Reindeer.BUTTON_DEFAULT);
        	editButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.edit.icon")));
        	editButton.setDescription(vaadinUi.getMsg("button.edit.tooltip"));
        	footer.addComponent(editButton);

        	clearCacheButton = new Button(vaadinUi.getMsg("DeploymentPropertiesForm.button.clearCache.label"), (ClickListener)this);
        	clearCacheButton.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.button.clearCache.tooltip"));
        	clearCacheButton.setDisableOnClick(true);
        	clearCacheButton.addStyleName("clearCacheButton");
        	clearCacheButton.addStyleName("caution");
        	footer.addComponent(clearCacheButton);
    	}

    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -5106851956249485330L;

			@Override
			public Field<?> createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				if ( propertyId.equals("allowProductionTransactions") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("DeploymentPropertiesForm.allowProductionTransactions.label"));
	            	cb.setImmediate(false);
	                cb.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.allowProductionTransactions.tooltip"));
	                return cb;
				} 
				if ( propertyId.equals("allowTestTransactions") ) {
					CheckBox cb = new CheckBox(vaadinUi.getMsg("DeploymentPropertiesForm.allowTestTransactions.label"));
					cb.setImmediate(false);
					cb.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.allowTestTransactions.tooltip"));
					return cb;
				}
				if ( propertyId.equals("defaultCountryCode") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("DeploymentPropertiesForm.defaultCountryCode.label"), vaadinUi.getEsfapp().getDropDownCountryEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.defaultCountryCode.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("defaultTimeZone") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("DeploymentPropertiesForm.defaultTimeZone.label"), vaadinUi.getEsfapp().getDropDownTimeZoneEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.defaultTimeZone.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("defaultLocale") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("DeploymentPropertiesForm.defaultLocale.label"), vaadinUi.getEsfapp().getDropDownLocaleEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.defaultLocale.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}
				if ( propertyId.equals("defaultDateFormat") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("DeploymentPropertiesForm.defaultDateFormat.label"), vaadinUi.getEsfapp().getDropDownDateFormatEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.defaultDateFormat.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}
				if ( propertyId.equals("defaultTimeFormat") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("DeploymentPropertiesForm.defaultTimeFormat.label"), vaadinUi.getEsfapp().getDropDownTimeFormatEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.defaultTimeFormat.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}
				if ( propertyId.equals("defaultLogDateFormat") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("DeploymentPropertiesForm.defaultLogDateFormat.label"), vaadinUi.getEsfapp().getDropDownDateFormatEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.defaultLogDateFormat.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}
				if ( propertyId.equals("defaultLogTimeFormat") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("DeploymentPropertiesForm.defaultLogTimeFormat.label"), vaadinUi.getEsfapp().getDropDownTimeFormatEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.defaultLogTimeFormat.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}
				if ( propertyId.equals("smtpStartTLS") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("DeploymentPropertiesForm.smtpStartTLS.label"));
	            	cb.setImmediate(false);
	                cb.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.smtpStartTLS.tooltip"));
	                return cb;
				} 
				if ( propertyId.equals("imapSSL") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("DeploymentPropertiesForm.imapSSL.label"));
	            	cb.setImmediate(false);
	                cb.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.imapSSL.tooltip"));
	                return cb;
				} 
				if ( propertyId.equals("licenseType") ) {
					NativeSelect select = new NativeSelect(vaadinUi.getMsg("DeploymentPropertiesForm.licenseType.label"));
	                select.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.licenseType.tooltip"));
	                select.setRequired(true);
	                select.setNullSelectionAllowed(false);
	                select.addValidator(new SelectValidator(select));
	                select.addItem(Application.LICENSE_TYPE_DEFAULT);
	                select.addItem(Application.LICENSE_TYPE_COMMERCIAL_DBSIZE);
	                select.addItem(Application.LICENSE_TYPE_COMMERCIAL_NUM_USERS);
	                return select;
				}

                Field<?> field = super.createField(item, propertyId, uiContext);
                field.setWidth("40ex");
				
                if (propertyId.equals("id")) {
                	TextField tf = (TextField)field;
                	tf.setReadOnly(true);
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.id.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.id.tooltip"));
                    tf.setStyleName("smallInfo");
                } else if (propertyId.equals("esfName")) {
                	TextField tf = (TextField)field;
                	tf.setReadOnly(true);
                    tf.setCaption(vaadinUi.getMsg("caption.esfName"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.esfName"));
                } else if (propertyId.equals("deployId")) {
                	TextField tf = (TextField)field;
                	tf.setReadOnly(true);
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.deployId.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.deployId.tooltip"));
                } else if (propertyId.equals("defaultUrlProtocolAddress")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.addValidator(new EsfStringRegexpValidator("https?://.+", vaadinUi.getMsg("DeploymentPropertiesForm.defaultUrlProtocolAddress.validator.message")));
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.defaultUrlProtocolAddress.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.defaultUrlProtocolAddress.tooltip"));
                } else if (propertyId.equals("smtpServer")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.smtpServer.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.smtpServer.tooltip"));
                } else if (propertyId.equals("smtpPort")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.addValidator(new EsfIntegerRangeValidator(1,9999));
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.smtpPort.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.smtpPort.tooltip"));
                } else if (propertyId.equals("smtpAuthUser")) {
                	TextField tf = (TextField)field;
                	tf.setWidth("20ex");
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.smtpAuthUser.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.smtpAuthUser.tooltip"));
                } else if (propertyId.equals("smtpAuthPassword")) {
                	TextField tf = (TextField)field;
                	tf.setWidth("20ex");
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.smtpAuthPassword.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.smtpAuthPassword.tooltip"));
                } else if (propertyId.equals("imapServer")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.imapServer.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.imapServer.tooltip"));
                } else if (propertyId.equals("imapPort")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.addValidator(new EsfIntegerRangeValidator(1,9999));
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.imapPort.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.imapPort.tooltip"));
                } else if (propertyId.equals("imapUser")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setWidth("20ex");
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.imapUser.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.imapUser.tooltip"));
                } else if (propertyId.equals("imapPassword")) {
                	TextField tf = (TextField)field;
                	tf.setWidth("20ex");
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.imapPassword.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.imapPassword.tooltip"));
                } else if (propertyId.equals("defaultEmailFrom")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.defaultEmailFrom.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.defaultEmailFrom.tooltip"));
                } else if (propertyId.equals("smtpReturnPathHostName")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.smtpReturnPathHostName.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.smtpReturnPathHostName.tooltip"));
                } else if (propertyId.equals("installYear")) {
                	TextField tf = (TextField)field;
                	tf.setReadOnly(true);
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.installYear.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.installYear.tooltip"));
                } else if (propertyId.equals("installDate")) {
                	TextField tf = (TextField)field;
                	tf.setReadOnly(true);
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.installDate.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.installYear.tooltip"));
                } else if (propertyId.equals("contextPath")) {
                	TextField tf = (TextField)field;
                	tf.setReadOnly(true);
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.contextPath.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.contextPath.tooltip"));
                } else if (propertyId.equals("licenseSize")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("0");
                	tf.addValidator(new EsfIntegerRangeValidator(0,999999999));
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.licenseSize.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.licenseSize.tooltip"));
                } else if (propertyId.equals("dbSize")) {
                	TextField tf = (TextField)field;
                	tf.setReadOnly(true);
                    tf.setCaption(vaadinUi.getMsg("DeploymentStatsForm.dbSize.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentStatsForm.dbSize.tooltip"));
                } else if (propertyId.equals("defaultSessionTimeoutMinutes")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("30");
                	tf.addValidator(new EsfIntegerRangeValidator(1,9999));
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.defaultSessionTimeoutMinutes.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.defaultSessionTimeoutMinutes.tooltip"));
                } else if (propertyId.equals("loggedInSessionTimeoutMinutes")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("30");
                	tf.addValidator(new EsfIntegerRangeValidator(1,9999));
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.loggedInSessionTimeoutMinutes.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.loggedInSessionTimeoutMinutes.tooltip"));
                } else if (propertyId.equals("retainSystemStartStopDays")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("365");
                	tf.addValidator(new EsfIntegerRangeValidator(1,99999));
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.retainSystemStartStopDays.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.retainSystemStartStopDays.tooltip"));
                } else if (propertyId.equals("retainSystemConfigChangeDays")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("365");
                	tf.addValidator(new EsfIntegerRangeValidator(1,99999));
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.retainSystemConfigChangeDays.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.retainSystemConfigChangeDays.tooltip"));
                } else if (propertyId.equals("retainSystemUserActivityDays")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("365");
                	tf.addValidator(new EsfIntegerRangeValidator(1,99999));
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.retainSystemUserActivityDays.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.retainSystemUserActivityDays.tooltip"));
                } else if (propertyId.equals("retainStatsDays")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("365");
                	tf.addValidator(new EsfIntegerRangeValidator(1,99999));
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.retainStatsDays.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.retainStatsDays.tooltip"));
                } else if (propertyId.equals("retainUserSecurityDays")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("365");
                	tf.addValidator(new EsfIntegerRangeValidator(1,99999));
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.retainUserSecurityDays.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.retainUserSecurityDays.tooltip"));
                } else if (propertyId.equals("retainUserConfigChangeDays")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("365");
                	tf.addValidator(new EsfIntegerRangeValidator(1,99999));
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.retainUserConfigChangeDays.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.retainUserConfigChangeDays.tooltip"));
                } else if (propertyId.equals("retainUserLoginLogoffDays")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("365");
                	tf.addValidator(new EsfIntegerRangeValidator(1,99999));
                    tf.setCaption(vaadinUi.getMsg("DeploymentPropertiesForm.retainUserLoginLogoffDays.label"));
                    tf.setDescription(vaadinUi.getMsg("DeploymentPropertiesForm.retainUserLoginLogoffDays.tooltip"));
                }

                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
	}
	
    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("id")) {
            layout.addComponent(field, 1, 0);
        } else if (propertyId.equals("esfName")) {
        	layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("deployId")) {
            layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("defaultUrlProtocolAddress")) {
            layout.addComponent(field, 1, 1);
        } else if (propertyId.equals("defaultLocale")) {
        	layout.addComponent(field, 0, 2);
        } else if (propertyId.equals("defaultTimeZone")) {
        	layout.addComponent(field, 0, 3);
        } else if (propertyId.equals("defaultDateFormat")) {
        	layout.addComponent(field, 1, 2);
        } else if (propertyId.equals("defaultTimeFormat")) {
        	layout.addComponent(field, 1, 3);
        } else if (propertyId.equals("defaultLogDateFormat")) {
        	layout.addComponent(field, 2, 2);
        } else if (propertyId.equals("defaultLogTimeFormat")) {
        	layout.addComponent(field, 2, 3);
        } else if (propertyId.equals("smtpServer")) {
        	layout.addComponent(field, 0, 4);
        } else if (propertyId.equals("smtpPort")) {
        	layout.addComponent(field, 1, 4);
        } else if (propertyId.equals("smtpAuthUser")) {
        	smtpExtrasLayout.removeAllComponents();
        	smtpExtrasLayout.addComponent(field);
        } else if (propertyId.equals("smtpAuthPassword")) {
        	smtpExtrasLayout.addComponent(field);
        } else if (propertyId.equals("smtpStartTLS")) {
        	smtpExtrasLayout.addComponent(field);
        } else if (propertyId.equals("imapServer")) {
        	layout.addComponent(field, 0, 5);
        } else if (propertyId.equals("imapPort")) {
        	layout.addComponent(field, 1, 5);
        } else if (propertyId.equals("imapUser")) {
        	imapExtrasLayout.removeAllComponents();
        	imapExtrasLayout.addComponent(field);
        } else if (propertyId.equals("imapPassword")) {
        	imapExtrasLayout.addComponent(field);
        } else if (propertyId.equals("imapSSL")) {
        	imapExtrasLayout.addComponent(field);
        } else if (propertyId.equals("defaultEmailFrom")) {
        	layout.addComponent(field, 0, 6);
        } else if (propertyId.equals("smtpReturnPathHostName")) {
        	layout.addComponent(field, 1, 6);
        } else if (propertyId.equals("installYear")) {
        	layout.addComponent(field, 0, 7);
        } else if (propertyId.equals("installDate")) {
        	layout.addComponent(field, 1, 7);
        } else if (propertyId.equals("contextPath")) {
        	layout.addComponent(field, 2, 7);
        } else if (propertyId.equals("allowProductionTransactions")) {
        	prodTestTransactionLayout.removeAllComponents();
        	prodTestTransactionLayout.addComponent(field);
        } else if (propertyId.equals("allowTestTransactions")) {
        	prodTestTransactionLayout.addComponent(field);
        } else if (propertyId.equals("defaultCountryCode")) {
        	layout.addComponent(field, 2, 1);
        } else if (propertyId.equals("licenseType")) {
        	layout.addComponent(field, 0, 8);
        } else if (propertyId.equals("licenseSize")) {
        	layout.addComponent(field, 1, 8);
        } else if (propertyId.equals("dbSize")) {
        	layout.addComponent(field, 2, 8);
        } else if (propertyId.equals("defaultSessionTimeoutMinutes")) {
        	layout.addComponent(field, 0, 9);
        } else if (propertyId.equals("loggedInSessionTimeoutMinutes")) {
        	layout.addComponent(field, 1, 9);
        } else if (propertyId.equals("retainStatsDays")) {
        	layout.addComponent(field, 2, 9);
        } else if (propertyId.equals("retainSystemStartStopDays")) {
        	retainSystemDaysLayout.removeAllComponents();
        	retainSystemDaysLayout.addComponent(field);
        } else if (propertyId.equals("retainSystemConfigChangeDays")) {
        	retainSystemDaysLayout.addComponent(field);
        } else if (propertyId.equals("retainSystemUserActivityDays")) {
        	retainSystemDaysLayout.addComponent(field);
        } else if (propertyId.equals("retainUserSecurityDays")) {
        	retainUserDaysLayout.removeAllComponents();
        	retainUserDaysLayout.addComponent(field);
        } else if (propertyId.equals("retainUserConfigChangeDays")) {
        	retainUserDaysLayout.addComponent(field);
        } else if (propertyId.equals("retainUserLoginLogoffDays")) {
        	retainUserDaysLayout.addComponent(field);
        }
    }

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("DeploymentPropertiesForm.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}