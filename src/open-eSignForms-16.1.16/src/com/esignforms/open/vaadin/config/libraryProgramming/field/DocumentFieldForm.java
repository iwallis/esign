// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.field;

import java.util.Map;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.libraryProgramming.document.LibDocumentVersionBean;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.validator.LibraryFieldEsfNameValidator;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Field;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * DocumentFieldForm is a LibFormField but in a different set of containers/views so it has some different setup and button logic.
 * @author Yozons Inc.
 */
public class DocumentFieldForm extends LibFieldForm {
	private static final long serialVersionUID = 5796291752276266178L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DocumentFieldForm.class);
	
	ConfirmDiscardFormChangesWindow parentWindow;
	final LibDocumentVersionBean docVerBean;
	final DocumentVersion duplicateDocumentVersion;
	
	EsfName lastDocumentFieldWorkedOn = null;
	
	public DocumentFieldForm(LibDocumentVersionBean docVerBeanParam, final Map<EsfName,FieldTemplate> libraryFieldTemplateMap) {
		super(null,null,libraryFieldTemplateMap,docVerBeanParam.duplicateDocumentVersion().getFieldTemplateMap());
		this.docVerBean = docVerBeanParam;
		this.duplicateDocumentVersion = docVerBean.duplicateDocumentVersion();
		this.library = docVerBean.duplicateDocumentVersion().getDocument().getLibrary();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		// For a document, we use different buttons
		HorizontalLayout footer = (HorizontalLayout)super.getFooter();
		
		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	// We have no edit button in documents

    	createLikeButton = new Button(vaadinUi.getMsg("LibDocumentVersionPageView.FieldForm.button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.FieldForm.button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("LibDocumentVersionPageView.FieldForm.button.createLike.tooltip"));
		footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
		footer.addComponent(deleteButton);

		_logger.debug("Form created");
    }
    
    @Override
	public void buttonClick(ClickEvent event) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
        	
			commit();
			
        	LibFieldBean bean = getCurrentBean();
        	
        	// Let make sure no radio buttons are listed for a party to edit (only radio button groups can be)
        	if ( bean.fieldTemplate().isTypeRadioButton() ) {
        		for( PartyTemplate pt : duplicateDocumentVersion._getPartyTemplateListInternal() ) {
        			pt.removePartyTemplateFieldTemplate(bean.fieldTemplate());
        		}
        	}
			
			if ( bean.fieldTemplate().doInsert() ) {
				duplicateDocumentVersion.setFieldTemplate(bean.fieldTemplate());
			} else {
	        	// Handles the case where the field has been renamed
	        	if ( ! bean.getOriginalEsfName().equals(bean.getEsfName()) ) {
	        		duplicateDocumentVersion._getFieldTemplateMapInternal().remove(bean.getOriginalEsfName());
	        		bean.fixupEsfName();
	        		duplicateDocumentVersion._getFieldTemplateMapInternal().put(bean.getEsfName(),bean.fieldTemplate());
	        		if ( bean.fieldTemplate().getContainerReferenceCount() > 0 ) {
	            		vaadinUi.showWarning(vaadinUi.getMsg("LibDocumentVersionPageView.FieldForm.renameWarning.caption"), vaadinUi.getMsg("LibDocumentVersionPageView.FieldForm.renameWarning.message",bean.getOriginalEsfName(),bean.getEsfName()));
	        		}
	            	if ( bean.fieldTemplate().isTypeRadioButtonGroup() ) {
	            		vaadinUi.showWarning(vaadinUi.getMsg("LibDocumentVersionPageView.FieldForm.renameRadioButtonGroupWarning.caption"), vaadinUi.getMsg("LibDocumentVersionPageView.FieldForm.renameRadioButtonGroupWarning.message",bean.getOriginalEsfName(),bean.getEsfName()));
	            	}
	        	}
			}
			lastDocumentFieldWorkedOn = bean.fieldTemplate().getEsfName();

			parentWindow.close();
        } else if ( source == cancelButton ) {
    		discard();
			parentWindow.close();
        } else if ( source == deleteButton ) {
        	discard();
			LibFieldBean bean = getCurrentBean();
			duplicateDocumentVersion.removeFieldTemplate(bean.fieldTemplate());
			lastDocumentFieldWorkedOn = bean.fieldTemplate().getEsfName();
        	parentWindow.close();
        } else if ( source == createLikeButton ) {
        	LibFieldBean bean = getCurrentBean();
        	
        	FieldTemplate newField = FieldTemplate.Manager.createLike(duplicateDocumentVersion.getId(), bean.fieldTemplate());
    		LibFieldBean newBean = new LibFieldBean(newField);
    		BeanItem<LibFieldBean> newBeanItem = new BeanItem<LibFieldBean>(newBean);
    		setItemDataSource(newBeanItem);
        }
    }
    
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	if ( createLikeButton != null ) {
    		createLikeButton.setVisible(!readOnly);
    	}
    	if ( deleteButton != null ) {
    		deleteButton.setVisible(!readOnly);
    	}
    }
    
    @Override
    protected void setupForm(LibFieldBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	layout.setCaption(vaadinUi.getMsg("LibDocumentVersionPageView.FieldForm.window.caption",bean.getEsfName()));

		id.setValue( vaadinUi.getMsg("LibDocumentVersionPageView.label.id",bean.getId(),bean.getContainerReferenceCount()) );
		
		Field esfname = getField("esfName");
		if ( esfnameValidator != null ) esfname.removeValidator(esfnameValidator);
		esfnameValidator = new LibraryFieldEsfNameValidator(duplicateDocumentVersion.getFieldTemplateMap(),bean.fieldTemplate(),this);
		esfname.addValidator(esfnameValidator);
		
		setupFieldsByType(bean.getType());
		
		lastDocumentFieldWorkedOn = bean.fieldTemplate().getEsfName();
    }
    
	public void setParentWindow(ConfirmDiscardFormChangesWindow parentWindow) {
		this.parentWindow = parentWindow;
	}

	
	public EsfName getLastDocumentFieldWorkedOn() {
		return lastDocumentFieldWorkedOn;
	}
}