// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.image;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.LibraryImageEsfNameValidator;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The LibImageForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class LibImageForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -8550925620263543667L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibImageForm.class);
	
	Button saveButton;
	Button createLikeButton;
	Button cancelButton;
	Button editButton;

	LibImageAndVersionsMainView view;
	LibImageBeanContainer container;
    GridLayout layout;
	
    LibraryImageEsfNameValidator esfnameValidator;
    
	Label id;
	
	// We store the prevBean in case we abandon working on the newBean when we 'create like'
	LibImageBean prevBean;
    LibImageBean newBean;
    
	
	public LibImageForm(LibImageAndVersionsMainView view, LibImageBeanContainer container) {
    	setStyleName("LibImageForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.view = view;
    	this.container = container;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
    	layout = new GridLayout(2,5);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.5f);
    	layout.setColumnExpandRatio(1, 0.5f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
		
		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id,0,4,1,4);

		HorizontalLayout footer = new HorizontalLayout();
		footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);

		if ( view.hasPermCreateLike() || view.hasPermUpdate() ) {
	    	saveButton = new Button(vaadinUi.getMsg("button.save.label"), (ClickListener)this);
	    	//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
			//saveButton.setClickShortcut(KeyCode.ENTER);
	    	saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
	    	saveButton.setDescription(vaadinUi.getMsg("button.save.tooltip"));
    		footer.addComponent(saveButton);
		}

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	if ( view.hasPermUpdate() ) {
	    	editButton = new Button(vaadinUi.getMsg("button.edit.label"), (ClickListener)this);
	    	editButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibImageForm.button.edit.icon")));
	    	editButton.setDescription(vaadinUi.getMsg("button.edit.tooltip"));
    		footer.addComponent(editButton);
    	}

    	if ( view.hasPermCreateLike() ) {
	    	createLikeButton = new Button(vaadinUi.getMsg("LibImageForm.button.createLike.label"), (ClickListener)this);
	    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibImageForm.button.createLike.icon")));
	    	createLikeButton.setDescription(vaadinUi.getMsg("LibImageForm.button.createLike.tooltip"));
    		footer.addComponent(createLikeButton);
		}

    	setFooter(footer);
    	setReadOnly(true);

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 1918577243917287088L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				_logger.debug("createField: " + propertyId);
				
				// Status will be an option group (radio buttons and not a text field)
				if ( propertyId.equals("status") ) {
					// Create the radio buttons to have the two possible values.
					OptionGroup statusOptionGroup = new OptionGroup();
					statusOptionGroup.addItem(Literals.STATUS_ENABLED);
					statusOptionGroup.addItem(Literals.STATUS_DISABLED);
					// Associate different labels/captions to those values
					statusOptionGroup.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					statusOptionGroup.setItemCaption(Literals.STATUS_ENABLED, vaadinUi.getPrettyCode().status(Literals.STATUS_ENABLED));
					statusOptionGroup.setItemCaption(Literals.STATUS_DISABLED, vaadinUi.getPrettyCode().status(Literals.STATUS_DISABLED));
					statusOptionGroup.setStyleName("inline");
					statusOptionGroup.setDescription(vaadinUi.getMsg("LibImageForm.status.tooltip")); // Doesn't appear to work without a caption on the OptionGroup, and we don't want one.
					return statusOptionGroup;
				}
				
				if ( propertyId.equals("comments") ) {
	            	TextArea ta = new TextArea();
	            	ta.setWidth(100, Unit.PERCENTAGE);
	            	ta.setNullRepresentation("");
	            	ta.setRows(3);
	            	ta.setInputPrompt(vaadinUi.getMsg("inputPrompt.comments"));
	            	ta.setCaption(vaadinUi.getMsg("caption.comments"));
	            	ta.setDescription(vaadinUi.getMsg("tooltip.comments"));
	            	/*
	            	ta.addFocusListener( new FieldEvents.FocusListener() {
						private static final long serialVersionUID = 2085962724895943432L;

						@Override
						public void focus(FocusEvent event) {
							if ( saveButton != null )
								saveButton.removeClickShortcut();
						}
	            	});
	            	ta.addBlurListener( new FieldEvents.BlurListener() {
						private static final long serialVersionUID = -1730957199398102134L;

						@Override
						public void blur(BlurEvent event) {
							if ( saveButton != null )
								saveButton.setClickShortcut(KeyCode.ENTER);
						}
					});
					*/
	            	return ta;
				}
					
				Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("esfName")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setCaption(vaadinUi.getMsg("caption.esfName"));
                	tf.setDescription(vaadinUi.getMsg("LibImageForm.esfname.tooltip"));
                	tf.setConversionError(vaadinUi.getMsg("validator.esfname.message"));
                } else if ( propertyId.equals("description") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("caption.description"));
                	tf.setInputPrompt(vaadinUi.getMsg("inputPrompt.description"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.description"));
                }
                
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
    }
    
    @Override
	public void buttonClick(ClickEvent event) {
    	Button source = event.getButton();
    	if ( source == saveButton ) {
     		view.save();
        } else if ( source == cancelButton ) {
        	view.cancelChanges();
        	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
        	if ( newBean != null ) {
        		view.discardForms();
        		view.unselectAllImageVersions();
        		view.selectImage(prevBean);
        		prevBean = newBean = null; // we're not doing a new bean anymore
        	}
        } else if ( source == editButton ) {
        	view.enterEditMode();
        } else if ( source == createLikeButton ) {
        	LibImageBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		createLike(currBean);
        	}	
        }
    }
	
    LibImageBean save(Connection con) throws SQLException {
		Errors errors = new Errors();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( newBean != null ) {
    		if ( newBean.save(con,errors) ) {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",newBean.getEsfName()) );

    			LibImageBean saved = newBean;
        		prevBean = newBean = null; // and we're not longer working on a new bean
        		return saved;
    		} 
    		vaadinUi.show(errors);
    		return null;
		}
		
		LibImageBean currBean = getCurrentBean();
		if ( currBean.save(con,errors) ) {
			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",currBean.getEsfName()) );
			return currBean;
   		}
		
		vaadinUi.show(errors);
		return null;
    }

	public String getCurrentBeanName() {
		LibImageBean currBean = getCurrentBean();
		return currBean == null ? "(None)" : currBean.getEsfName().toString();
	}
    
	LibImageBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public LibImageBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibImageBean> bi = (BeanItem<LibImageBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached stuff
		resetCaches();
		
    	if (newDataSource != null) {
    		LibImageBean bean = getBean(newDataSource);
    		
    		super.setItemDataSource(newDataSource, Arrays.asList("status","esfName","description","comments"));
    		
    		setupForm(bean);
    		
    		setReadOnly( ! view.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		newBean = bean.imageInfo().doInsert() ? bean : null;
    		if ( newBean != null ) {
    			TextField tf = (TextField)getField("esfName");
    			tf.selectAll();
    			tf.focus();
    		}
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		setReadOnly(true);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	LibImageBean bean = getCurrentBean();
    	
    	// We have these only while editing/creating new
    	if ( saveButton != null ) {
    		saveButton.setVisible(!readOnly);
    	}
    	cancelButton.setVisible(!readOnly);

    	// We have this only while in view mode
    	if ( editButton != null ) {
    		editButton.setVisible(readOnly && bean != null && view.hasPermUpdate());
    	}
    	
    	// This one we show this in all modes if has permission
    	if ( createLikeButton != null ) {
    		createLikeButton.setVisible(bean != null && bean.image().doUpdate()); // only want create like on an existing document
    	}    	

    	if ( bean != null && bean.image().hasProductionVersion() ) {
    		getField("esfName").setReadOnly(true);
    	}
    }
    
    public void createLike(LibImageBean likeBean) {
    	view.createLikeImage();
    	prevBean = likeBean;
     }
    
    public void createNew() {
    	view.createNewImage();
    	prevBean = null;
    }
    
    void setupForm(LibImageBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("LibImageForm.label.id",bean.getId()) );
		
		Field esfname = getField("esfName");
		if ( esfnameValidator != null ) esfname.removeValidator(esfnameValidator);
		esfnameValidator = new LibraryImageEsfNameValidator(view.getLibrary().getId(),bean.image());
		esfname.addValidator(esfnameValidator);		
    }
    
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("status")) {
            layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("esfName")) {
        	layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("description")) {
        	layout.addComponent(field, 0, 2, 1, 2);
        } else if (propertyId.equals("comments")) {
        	layout.addComponent(field, 0, 3, 1, 3);
        }
    }

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibImageAndVersionsMainView.Document.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}