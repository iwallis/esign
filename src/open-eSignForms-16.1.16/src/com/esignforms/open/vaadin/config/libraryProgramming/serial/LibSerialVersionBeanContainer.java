// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.serial;

import java.io.Serializable;
import java.util.Collection;

import com.esignforms.open.prog.Serial;
import com.esignforms.open.prog.SerialVersionInfo;
import com.vaadin.data.util.BeanItemContainer;


public class LibSerialVersionBeanContainer extends BeanItemContainer<LibSerialVersionBean> implements Serializable {
	private static final long serialVersionUID = -1351156198368683714L;

	final LibSerialAndVersionsMainView view;
	
	public LibSerialVersionBeanContainer(LibSerialAndVersionsMainView view) throws InstantiationException, IllegalAccessException {
		super(LibSerialVersionBean.class);
		this.view = view;

		// No items exist until a serial is selected and set to this re-loadable container.
	}
	
	public void reload(Serial serial) {
		removeAllItems();
		
		if ( serial != null ) {
			
			Collection<SerialVersionInfo> serialVers = SerialVersionInfo.Manager.getAll(serial);
			for( SerialVersionInfo d : serialVers ) {
				addItem( new LibSerialVersionBean(d,view.getVersionLabel(d)) );			
			}
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}