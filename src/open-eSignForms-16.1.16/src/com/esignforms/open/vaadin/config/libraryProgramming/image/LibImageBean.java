// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.image;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.ImageInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibImageBean implements Serializable, Comparable<LibImageBean> {
	private static final long serialVersionUID = 5204904987426256401L;

	private ImageInfo imageInfo;

	private Image image;
	
	public LibImageBean(ImageInfo imageInfo) {
		this.imageInfo = imageInfo;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibImageBean )
			return imageInfo.equals(((LibImageBean)o).imageInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return imageInfo.hashCode();
    }
	public int compareTo(LibImageBean d) {
		return imageInfo.compareTo(d.imageInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public ImageInfo imageInfo() {
		return imageInfo;
	}

	public Image image() {
		if ( image == null )
			image = Image.Manager.getById(imageInfo.getId());
		return image;
	}

	public LibImageBean createLike() {
		ImageInfo newImageInfo = ImageInfo.Manager.createLike(image, image.getEsfName());	    
	    return new LibImageBean(newImageInfo);
	}
	
	
	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( image().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			imageInfo = ImageInfo.Manager.createFromSource(image);
			return true;
		} 
		
		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}

	
	// Now for the JavaBeans methods
	public EsfName getEsfName() {
		return imageInfo.getEsfName();
	}
	public void setEsfName(EsfName v) {
		image.setEsfName(v);
	}

	public EsfUUID getId() {
		return imageInfo.getId();
	}
	
	public String getDescription() {
		return imageInfo.getDescription();
	}
	public void setDescription(String v) {
		image().setDescription(v);
	}

	public boolean isEnabled() {
		return imageInfo.isEnabled();
	}
	public String getStatus() {
		return imageInfo.getStatus();
	}
	public void setStatus(String v) {
		image().setStatus(v);
	}	
	
	public Integer getProductionVersion() {
		return imageInfo.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		return imageInfo.getTestVersion();
	}
	
	public void bumpTestVersion() {
		image().bumpTestVersion();
	}
	public void dropTestVersion() {
		image().dropTestVersion();
	}
	
	public String getComments() {
		return image().getComments();
	}
	public void setComments(String v) {
		image().setComments(v);
	}
}