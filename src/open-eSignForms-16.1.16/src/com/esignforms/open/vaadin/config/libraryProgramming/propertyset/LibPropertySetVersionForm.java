// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.propertyset;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.vaadin.dialogs.ConfirmDialog;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.NameValue;
import com.esignforms.open.data.Record;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.prog.PropertySetVersion;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.EsfNameValidator;
import com.vaadin.data.Buffered;
import com.vaadin.data.Item;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.Reindeer;

/**
 * The LibPropertySetVersionForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class LibPropertySetVersionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 8737276402713698668L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibPropertySetVersionForm.class);
	
	final LibPropertySetVersionForm thisForm;
	final LibPropertySetAndVersionsMainView view;
	LibPropertySetVersionBeanContainer container;
	GridLayout layout;
	
	Label id;
	Label createdByInfo;
	Label lastUpdatedByInfo;
	Label versionInfo;
	
	Button testToProdButton;
	Button createTestFromProdButton;
	Button revertProdToTestButton;
	Button deleteButton;

	Map<Object,List<Field>> propertiesListFields = new TreeMap<Object,List<Field>>();
	Table propertiesList;
	boolean propertiesListChanged;
	HorizontalLayout propertiesButtonLayout;
	Button propertiesListAddButton;
	Button propertiesListRemoveButton;

	public LibPropertySetVersionForm(LibPropertySetAndVersionsMainView viewParam, LibPropertySetVersionBeanContainer container) {
    	setStyleName("LibPropertySetVersionForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.view = viewParam;
    	this.container = container;
    	this.thisForm = this;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);

    	layout = new GridLayout(1,6);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
		
		versionInfo = new Label();
		versionInfo.setContentMode(ContentMode.TEXT);	
		layout.addComponent(versionInfo,0,0);
		
		propertiesList = new Table();
		propertiesList.setWidth(100, Unit.PERCENTAGE);
		propertiesList.addContainerProperty("name", TextField.class, null);
		propertiesList.addContainerProperty("value", TextArea.class, null);
		propertiesList.addContainerProperty("comment", TextArea.class, null);
		propertiesList.setColumnHeaders( vaadinUi.getStringArray("LibPropertySetVersionForm.propertiesList.columnHeaders") );
		propertiesList.setColumnExpandRatio("name", 0.2f);
		propertiesList.setColumnExpandRatio("value", 0.6f);
		propertiesList.setColumnExpandRatio("comment", 0.2f);
		propertiesList.setColumnCollapsingAllowed(true);
		propertiesList.setColumnReorderingAllowed(true);
		propertiesList.setSortDisabled(false);
		propertiesList.setNullSelectionAllowed(true);
		propertiesList.setSelectable(true);
		propertiesList.setMultiSelect(true);
		propertiesList.setImmediate(true);
		layout.addComponent(propertiesList,0,1);
		propertiesList.setVisible(false);
		
		propertiesButtonLayout = new HorizontalLayout();
		propertiesButtonLayout.setStyleName("propertiesButtons");
		propertiesButtonLayout.setMargin(false);
		propertiesButtonLayout.setSpacing(true);
		propertiesListAddButton = new Button(vaadinUi.getMsg("LibPropertySetVersionForm.button.addProperty.label"), (ClickListener)this);
		propertiesListAddButton.setStyleName(Reindeer.BUTTON_SMALL);
		propertiesButtonLayout.addComponent(propertiesListAddButton);
		propertiesListRemoveButton = new Button(vaadinUi.getMsg("LibPropertySetVersionForm.button.removeProperty.label"), (ClickListener)this);
		propertiesListRemoveButton.setStyleName(Reindeer.BUTTON_SMALL);
		propertiesButtonLayout.addComponent(propertiesListRemoveButton);
		layout.addComponent(propertiesButtonLayout,0,2);
		propertiesButtonLayout.setVisible(false);
		
		lastUpdatedByInfo = new Label();
		lastUpdatedByInfo.setContentMode(ContentMode.TEXT);
		lastUpdatedByInfo.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedByInfo,0,3);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,4);		

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id,0,5);

		HorizontalLayout footer = new HorizontalLayout();
		footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);
    	
    	testToProdButton = new Button(vaadinUi.getMsg("LibPropertySetVersionForm.button.testToProd.label"), (ClickListener)this);
    	testToProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibPropertySetVersionForm.button.testToProd.icon")));
    	testToProdButton.setDescription(vaadinUi.getMsg("LibPropertySetVersionForm.button.testToProd.tooltip"));
    	footer.addComponent(testToProdButton);

    	createTestFromProdButton = new Button(vaadinUi.getMsg("LibPropertySetVersionForm.button.createTestFromProd.label"), (ClickListener)this);
    	createTestFromProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibPropertySetVersionForm.button.createTestFromProd.icon")));
    	createTestFromProdButton.setDescription(vaadinUi.getMsg("LibPropertySetVersionForm.button.createTestFromProd.tooltip"));
    	footer.addComponent(createTestFromProdButton);

    	revertProdToTestButton = new Button(vaadinUi.getMsg("LibPropertySetVersionForm.button.revertProdToTest.label"), (ClickListener)this);
    	revertProdToTestButton.addStyleName("caution");
    	revertProdToTestButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibPropertySetVersionForm.button.revertProdToTest.icon")));
    	revertProdToTestButton.setDescription(vaadinUi.getMsg("LibPropertySetVersionForm.button.revertProdToTest.tooltip"));
    	footer.addComponent(revertProdToTestButton);

    	if ( view.hasPermDelete() ) {
    		deleteButton = new Button(vaadinUi.getMsg("LibPropertySetVersionForm.button.delete.label"), (ClickListener)this);
    		deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibPropertySetVersionForm.button.delete.icon")));
    		deleteButton.setDescription(vaadinUi.getMsg("LibPropertySetVersionForm.button.delete.tooltip"));
	    	deleteButton.addStyleName("deleteButton");
	    	deleteButton.addStyleName("caution");
	    	footer.addComponent(deleteButton);
		}
    	
    	setFooter(footer);
    	setReadOnly(true);   	

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 2469731265082301197L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				Field field = super.createField(item, propertyId, uiContext);
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
    	propertiesListFields.clear();
    }
    
    private TextField createPropertiesListTextField() {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		TextField tf = new TextField();
		tf.setWidth(100,Unit.PERCENTAGE);
		tf.addValidator(new EsfNameValidator());
		tf.setConversionError(vaadinUi.getMsg("validator.esfname.message"));
		tf.setImmediate(true); // for the validator to work
		tf.setBuffered(true);
		return tf;
    	
    }
    private TextArea createPropertiesListTextArea() {
		TextArea ta = new TextArea();
		ta.setWidth(100,Unit.PERCENTAGE);
		ta.setRows(2);
		ta.setBuffered(true);
		/*
    	ta.addFocusListener( new FieldEvents.FocusListener() {
			private static final long serialVersionUID = -2500440255481352460L;

			@Override
			public void focus(FocusEvent event) {
				if ( view.propertysetForm.saveButton != null )
					view.propertysetForm.saveButton.removeClickShortcut();
			}
    	});
    	ta.addBlurListener( new FieldEvents.BlurListener() {
			private static final long serialVersionUID = -2172351046066737111L;

			@Override
			public void blur(BlurEvent event) {
				if ( view.propertysetForm.saveButton != null )
					view.propertysetForm.saveButton.setClickShortcut(KeyCode.ENTER);
			}
		});
		*/
		return ta;
    }
    private void registerPropertiesListFields(Object itemId, Field... fields) {
        List<Field> fieldList = propertiesListFields.get(itemId);
        if ( fieldList == null ) fieldList = new LinkedList<Field>();
        for( Field f : fields )
        	fieldList.add(f);
		propertiesListFields.put(itemId,fieldList);
    }
    private void deregisterPropertiesListFields(Object itemId) {
    	propertiesListFields.remove(itemId);
    }
    
    @Override
	public void buttonClick(ClickEvent event) {
    	LibPropertySetVersionBean currBean = getCurrentBean();
    	if ( currBean == null ) return;
    	
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == testToProdButton ) {
    		view.promoteTestToProductionVersion();
    	} else if ( source == createTestFromProdButton ) {
    		view.createNextVersion();
    	} else if ( source == revertProdToTestButton ) {
    		view.revertProductionVersionBackToTest();
    	} else if ( source == propertiesListAddButton ) {
    		if ( ! commitFieldsToTableOnly() ) 
    			return;
    		TextField nameTextField = createPropertiesListTextField();
    		nameTextField.setValue((new EsfPathName("EsfName")).toString());
    		TextArea valueTextArea = createPropertiesListTextArea();
    		valueTextArea.setValue((new EsfString("Value")).toString());
    		TextArea commentTextArea = createPropertiesListTextArea();
    		commentTextArea.setValue("");
    		Object item[] = { nameTextField, valueTextArea, commentTextArea };
    	    Object newItemId = propertiesList.addItem(item,null);
    	    registerPropertiesListFields(newItemId,nameTextField,valueTextArea,commentTextArea);
    	    setPropertiesListPageLength();
    	    propertiesList.setCurrentPageFirstItemId(newItemId);
    	    propertiesList.select(newItemId);
    	    propertiesListChanged = true;
    	} else if ( source == propertiesListRemoveButton ) {
    		if ( ! commitFieldsToTableOnly() ) 
    			return;
    		LinkedList<Object> itemIdsToRemove = new LinkedList<Object>();
        	for( Object itemId : propertiesList.getItemIds() ) {
        		if ( propertiesList.isSelected(itemId) ) {
        			itemIdsToRemove.add(itemId);
        		}
        	}
        	for( Object itemId: itemIdsToRemove ) {
    			deregisterPropertiesListFields(itemId);
        		propertiesList.removeItem(itemId);
        	}
        	setPropertiesListPageLength();
    	    propertiesListChanged = true;
        	vaadinUi.showStatus(vaadinUi.getMsg("LibPropertySetVersionForm.button.removeProperty.message",itemIdsToRemove.size()));
    	} else if ( source == deleteButton ) {
	    	ConfirmDialog.show(UI.getCurrent(), 
	    			vaadinUi.getMsg("LibPropertySetVersionForm.button.delete.ConfirmDialog.caption"),
	    			vaadinUi.getMsg("LibPropertySetVersionForm.button.delete.ConfirmDialog.message",propertiesList.size()),
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
	    	        new ConfirmDialog.Listener() {
						private static final long serialVersionUID = 4017486073770533676L;

						public void onClose(ConfirmDialog dialog) {
	    	                if (dialog.isConfirmed()) {
	    	                    view.delete();
	    	                }
	    	            }
	    	        });
    	}
    }
    
    void setPropertiesListPageLength() {
		propertiesList.setPageLength( Math.min(5, propertiesList.size()) );
    }
	
    LibPropertySetVersionBean save(Connection con) throws SQLException {
    	LibPropertySetVersionBean currBean = getCurrentBean();
    	if ( currBean != null ) {
    		if ( currBean.save(con,null) ) {
    			return currBean;
    		}
    	}
    	return null;
    }
    	
    public String getCurrentBeanName() {
    	LibPropertySetVersionBean currBean = getCurrentBean();
		return currBean == null ? "(None)" : view.getEsfNameWithVersion();
	}
    
	LibPropertySetVersionBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public LibPropertySetVersionBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibPropertySetVersionBean> bi = (BeanItem<LibPropertySetVersionBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached stuff
		resetCaches();
		
    	if (newDataSource != null) {
    		LibPropertySetVersionBean bean = getBean(newDataSource);
    		
    		super.setItemDataSource(newDataSource, Arrays.asList());
    		
    		setupForm(bean);
    		
    		setReadOnly( ! view.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	

    @Override
    public void setReadOnly(boolean readOnly) {   	
    	LibPropertySetVersionBean bean = getCurrentBean();
    	
    	boolean isTestVersion = bean != null && view.isTestVersion(bean.propertysetVerInfo());
    	
    	testToProdButton.setVisible(bean != null && !readOnly && isTestVersion && bean.propertysetVersion().doUpdate() && view.getLibPropertySetBean().isEnabled());
    	createTestFromProdButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.propertysetVerInfo()) && !view.hasTestVersion());
    	revertProdToTestButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.propertysetVerInfo()) && !view.hasTestVersion());
    	
    	// Show this only if the object is created and is the Test version
    	if ( deleteButton != null ) {
    		deleteButton.setVisible(bean != null && bean.propertysetVersion().doUpdate() && isTestVersion); 
    	}

    	// If the request is to be not-readonly, but this is not the test version, we convert it back to readonly
    	if ( !readOnly && bean != null && !view.isTestVersion(bean.propertysetVerInfo())) {
    		readOnly = true;
    	}
    	super.setReadOnly(readOnly);
    	
    	//propertiesList.setReadOnly(readOnly);
		propertiesList.setEditable(!readOnly);
		propertiesButtonLayout.setVisible(!readOnly);
		if ( readOnly && propertiesList.size() > 0 ) {
    	    propertiesList.setCurrentPageFirstItemIndex(0); // correcting what seems is that it would scroll to the bottom of the Table
		}
		for( List<Field> fList : propertiesListFields.values() ) {
			for( Field f : fList ) {
				f.setReadOnly(readOnly);
			}
		}
    }
    
    void setupForm(LibPropertySetVersionBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("LibPropertySetVersionForm.label.id",bean.getId()) );
		
		versionInfo.setValue( vaadinUi.getMsg("LibPropertySetVersionForm.label.version",bean.getVersion(),view.getVersionLabel(bean.propertysetVerInfo())) );
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.propertysetVerInfo().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("LibPropertySetVersionForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );
		
		if ( bean.propertysetVersion().doInsert() ) {
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibPropertySetVersionForm.label.lastUpdated.whenNew") );
			lastUpdatedByInfo.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.propertysetVerInfo().getLastUpdatedByUserId());
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibPropertySetVersionForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedByInfo.setIcon(null);
		}

		populatePropertiesList(bean.propertysetVersion());
		setPropertiesListPageLength();
		propertiesList.setCaption(vaadinUi.getMsg("LibPropertySetVersionForm.propertiesList.label"));
		propertiesList.setVisible(true);
		propertiesButtonLayout.setVisible(true);
    }

    void populatePropertiesList(PropertySetVersion propertysetVersion) {
    	propertiesListFields.clear();
    	propertiesList.removeAllItems();
    	
    	for (NameValue nv : propertysetVersion.getProperties().getSortedNameValues()) {
    		TextField nameTextField = createPropertiesListTextField();
    		nameTextField.setPropertyDataSource(new ObjectProperty<EsfPathName>(nv.getPathName()) );
    		TextArea valueTextArea = createPropertiesListTextArea();
    		EsfString value = (EsfString)nv.getValue();
    		valueTextArea.setPropertyDataSource(new ObjectProperty<EsfString>(value) );
    		TextArea commentTextArea = createPropertiesListTextArea();
    		commentTextArea.setPropertyDataSource(new ObjectProperty<EsfString>(nv.getNonNullComment()) );
    	    Object item[] = { nameTextField, valueTextArea, commentTextArea };
    	    Object newItemId = propertiesList.addItem(item,null);
    	    registerPropertiesListFields(newItemId,nameTextField,valueTextArea,commentTextArea);
    	}
    }

    void savePropertiesToBean()
    {
    	LibPropertySetVersionBean currBean = getCurrentBean();
    	if ( currBean == null )
    		return;
    	
    	Record newProperties = new Record(new EsfName("newProps"),BlobDb.CompressOption.ENABLE,BlobDb.EncryptOption.ENABLE);
    	for( Object itemId : propertiesList.getItemIds() ) {
    		Item item = propertiesList.getItem(itemId);
    		
    		TextField nameTextField = (TextField)item.getItemProperty("name").getValue();
    		TextArea valueTextArea = (TextArea)item.getItemProperty("value").getValue();
    		TextArea commentTextArea = (TextArea)item.getItemProperty("comment").getValue();
    		
    		Object nameValue = nameTextField.getValue();
    		EsfPathName name = nameValue instanceof EsfPathName ? (EsfPathName)nameValue : new EsfPathName((String)nameValue);
    		Object valueValue = valueTextArea.getValue();
    		EsfString value = valueValue instanceof EsfString ? (EsfString)valueValue : new EsfString((String)valueValue);
    		Object commentValue = commentTextArea.getValue();
    		EsfString comment = commentValue instanceof EsfString ? (EsfString)commentValue : new EsfString((String)commentValue);

    		NameValue nv = new NameValue(name,value);
    		nv.setComment(comment);
    		
    		newProperties.addUpdate(nv);
    	}
    	
    	currBean.setProperties(newProperties);
    }
    
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
    }

    private boolean commitFieldsToTableOnly() {
    	boolean isValid = true;
    	
    	// Only do this if we have something in our table since emptying a table will not have cleared our fields list.
    	if ( propertiesList.size() > 0 ) {
    		for( List<Field> fList : propertiesListFields.values() ) {
    			for( Field f : fList ) {
    				if ( f.isValid() )
    					f.commit();
    				else {
    					isValid = false;
    				}
    			}
    		}
    	}
    	
    	if ( ! isValid )
    	{
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    	}

    	return isValid;
    }
    @Override
    public void commit() throws Buffered.SourceException {
    	super.commit();
    	
    	commitFieldsToTableOnly();
    	
    	// Let's save whatever we have now for our name-values
    	savePropertiesToBean();
    	propertiesListChanged = false;
    }
    
    @Override
    public void discard() throws Buffered.SourceException {
    	/* 1/3/2011 - Found this wasn't enough. It did reset changed fields, but it didn't restore removed/added rows that have not been saved
		for( Field f : propertiesListFields ) {
			f.discard();
		}
		*/
    	LibPropertySetVersionBean bean = getCurrentBean();
    	if ( bean != null ) {
    		populatePropertiesList(bean.propertysetVersion());
    	}
    	propertiesListChanged = false;
    	super.discard(); // no real form fields (yet)
    }
    
    @Override
    public void validate() throws InvalidValueException {
    	super.validate();
    	
    	if ( propertiesList.size() > 0 ) {
    		for( List<Field> fList : propertiesListFields.values() ) {
    			for( Field f : fList ) {
    				f.validate();
    			}
    		}
    	}
    }
    
    @Override
    public boolean isValid() {
    	if ( ! super.isValid() ) {
    		return false;
    	}

    	if ( propertiesList.size() > 0 ) {
    		for( List<Field> fList : propertiesListFields.values() ) {
    			for( Field f : fList ) {
        			if ( ! f.isValid() ) return false;
    			}
    		}
    	}

		return true;
    }

	@Override
	public String checkDirty() { // OBSOLETE SINCE VERSION FORM HAS NO BUTTONS FOR SAVING, CANCELING
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibPropertySetAndVersionsMainView.PropertySet.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		if ( isModified() || propertiesListChanged ) {
			return true;
		}
		
		for( List<Field> fList : propertiesListFields.values() ) {
			for( Field f : fList ) {
    			if ( f.isModified() ) return true;
			}
		}
		
		return false;
	}
}