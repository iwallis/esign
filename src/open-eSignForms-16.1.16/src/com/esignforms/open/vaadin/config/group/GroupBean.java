// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.group;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;

public class GroupBean implements Serializable, Comparable<GroupBean> {
	private static final long serialVersionUID = 6349756905570225305L;

	private Group group;
	
	// This contains all of the groups that the user can list, so these are the only ones that
	// can be used to add/remove groups (any groups that the Group object has that are not included
	// cannot be touched)
	private EsfUUID[] allGroupListIds;
	private EsfPathName[] allGroupListNames;

	// This contains all of the member users that the user can update, so these are the only ones that
	// can be used to add/remove member users (any users that the particular "updating user" can't update are not included and
	// cannot be touched)
	private EsfUUID[] allMemberUsersUpdateIds;
	private String[] allMemberUsersUpdateFullDisplayNames;
	
	private Set<EsfUUID> permListIds;
	private Set<EsfUUID> permViewDetailsIds;
	private Set<EsfUUID> permCreateLikeIds;
	private Set<EsfUUID> permUpdateIds;
	private Set<EsfUUID> permDeleteIds;
	private Set<EsfUUID> permMemberUsersListIds;
	private Set<EsfUUID> permMemberUsersViewDetailsIds;
	private Set<EsfUUID> permMemberUsersCreateLikeIds;
	private Set<EsfUUID> permMemberUsersUpdateIds;
	private Set<EsfUUID> permMemberUsersDeleteIds;
	private Set<EsfUUID> permMemberUsersManageToDoIds;
	private Set<EsfUUID> memberUsersIds;
	
	public GroupBean(Group group) {
		this.group = group;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o !=null && o instanceof GroupBean )
			return group.equals(((GroupBean)o).group);
		return false;
	}
	@Override
    public int hashCode() {
    	return group.hashCode();
    }
	public int compareTo(GroupBean g) {
		return group.compareTo(g.group);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	protected User user() {
		return EsfVaadinUI.getInstance().getUser();
	}
	public Group group() {
		return group;
	}

	public GroupBean createLike() {
	    Group newGroup = Group.Manager.createLike(group, group.getPathName(), user());	    
	    return new GroupBean(newGroup);
	}
	
	public boolean save(Errors errors) {
		boolean ret = group.save(user());
		if ( ret ) {
			resetCaches();
		} else {
			if ( errors != null ) {
				errors.addError(EsfVaadinUI.getInstance().getMsg("form.save.failed.message"));
			}
		}
		return ret;
	}
	public boolean save() {
		return save(null);
	}
	
	public boolean delete(Errors errors) {
		if ( group.isReserved() )
			return false;
		return group.delete(errors,user());
	}

	protected HashSet<EsfUUID> createIdHashSet( Collection<Group> list ) {
		HashSet<EsfUUID> set = new HashSet<EsfUUID>(list.size());
		for( Group g : list ) {
			set.add(g.getId());
		}
		return set;
	}
	
	public synchronized void resetCaches() {
		allGroupListIds = null;
		allGroupListNames = null;
		allMemberUsersUpdateIds = null;
		allMemberUsersUpdateFullDisplayNames = null;
		permListIds = null;
		permViewDetailsIds = null;
		permCreateLikeIds = null;
		permUpdateIds = null;
		permDeleteIds = null;
		permMemberUsersListIds = null;
		permMemberUsersViewDetailsIds = null;
		permMemberUsersCreateLikeIds = null;
		permMemberUsersUpdateIds = null;
		permMemberUsersDeleteIds = null;
		permMemberUsersManageToDoIds = null;
		memberUsersIds = null;
	}

	protected synchronized void createAllGroupLists() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Collection<Group> list = Group.Manager.getForUserWithListPermission(user(),Group.INCLUDE.BOTH_ENABLED_AND_DISABLED);
		
		// If this is a create new before being inserted, we need to add it as a possible candidate too
		int size = list.size();
		if ( group.doInsert() ) {
			++size;
		}
		
		// We will not include the "External Users" pseudo-group.
		allGroupListIds = new EsfUUID[size-1];
		allGroupListNames = new EsfPathName[size-1];
		int i = 0;
		for( Group g : list ) {
			if ( g.isExternalUsersGroup() )
				continue;
			allGroupListIds[i] = g.getId();
			allGroupListNames[i] = g.isDisabled() ? new EsfPathName(g.getPathName() + EsfPathName.PATH_SEPARATOR + vaadinUi.getMsg("PrettyCode.status.disabled")) : g.getPathName();
			++i;
		}
		if ( group.doInsert() ) {
			allGroupListIds[i] = group.getId();
			allGroupListNames[i] = group.isDisabled() ? new EsfPathName(group.getPathName() + EsfPathName.PATH_SEPARATOR + vaadinUi.getMsg("PrettyCode.status.disabled")) : group.getPathName();
		}
	}
	public EsfUUID[] allGroupListIds() {
		if ( allGroupListIds == null ) {
			createAllGroupLists();
		}
		return allGroupListIds;
	}
	public EsfPathName[] allGroupListNames() {
		if ( allGroupListNames == null ) {
			createAllGroupLists();
		}
		return allGroupListNames;
	}
	
	protected HashSet<EsfUUID> createUserIdHashSet( Collection<User> list ) {
		HashSet<EsfUUID> set = new HashSet<EsfUUID>(list.size());
		for( User u : list ) {
			set.add(u.getId());
		}
		return set;
	}
	protected synchronized void createAllMemberUsersUpdateLists() {
		Collection<User> list = Group.Manager.getAllMemberUsersForUserWithUpdateMemberUsersPermission(user());
		
		int size = list.size();
		
		allMemberUsersUpdateIds = new EsfUUID[size];
		allMemberUsersUpdateFullDisplayNames = new String[size];
		int i = 0;
		for( User u : list ) {
			allMemberUsersUpdateIds[i] = u.getId();
			allMemberUsersUpdateFullDisplayNames[i] = u.getFullDisplayName();
			++i;
		}
	}
	public EsfUUID[] allMemberUsersUpdateIds() {
		if ( allMemberUsersUpdateIds == null ) {
			createAllMemberUsersUpdateLists();
		}
		return allMemberUsersUpdateIds;
	}
	public String[] allMemberUsersUpdateFullDisplayNames() {
		if ( allMemberUsersUpdateFullDisplayNames == null ) {
			createAllMemberUsersUpdateLists();
		}
		return allMemberUsersUpdateFullDisplayNames;
	}
	

	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return group.getId();
	}
	
	public EsfPathName getPathName() {
		return group.getPathName();
	}
	public void setPathName(EsfPathName v) {
		if ( ! group.isReserved() ) {
			group.setPathName( v );
		}
	}
	
	public boolean isEnabled() {
		return group.isEnabled();
	}
	public String getStatus() {
		return group.getStatus();
	}
	public void setStatus( String v ) {
		if ( ! group.isReserved() ) {
			group.setStatus(v);
		}
	}
	
	public String getDescription() {
		return group.getDescription();
	}
	public void setDescription(String v) {
		group.setDescription(v);
	}
	
	public EsfString getComments() {
		return group.getComments();
	}
	public void setComments(EsfString v) {
		group.setComments( v );
	}

	protected void setPermIds(Set<EsfUUID> v, EsfName permOption) {
		// First, let's remove all groups this user can list
		for( Group g : Group.Manager.getForUserWithListPermission(user()) ) {
			group.removePermissionAllowedGroups(permOption, g);
		}
		// Then if we're doing a create-like, remove our new group too
		if ( group.doInsert() ) {
			group.removePermissionAllowedGroups(permOption, group);
		}
		// Then let's add back in the selected set
		for( EsfUUID gid : v ) {
			Group g = gid.equals(group.getId()) ? group : Group.Manager.getById( gid );
			if ( g != null ) {
				group.addPermissionAllowedGroups(permOption, g);
			}
		}
	}

	public Set<EsfUUID> getPermListIds() {
		if ( permListIds == null ) {
			permListIds = createIdHashSet( group.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_LIST) );
		}
		return permListIds;
	}
		
	public void setPermListIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_LIST);
	}

	public Set<EsfUUID> getPermViewDetailsIds() {
		if ( permViewDetailsIds == null ) {
			permViewDetailsIds = createIdHashSet( group.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_VIEWDETAILS) );
		}
		return permViewDetailsIds;
	}
	
	public void setPermViewDetailsIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public Set<EsfUUID> getPermCreateLikeIds() {
		if ( permCreateLikeIds == null ) {
			permCreateLikeIds = createIdHashSet( group.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_CREATELIKE) );
		}
		return permCreateLikeIds;
	}
	
	public void setPermCreateLikeIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public Set<EsfUUID> getPermUpdateIds() {
		if ( permUpdateIds == null ) {
			permUpdateIds = createIdHashSet( group.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_UPDATE) );
		}
		return permUpdateIds;
	}
	
	public void setPermUpdateIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_UPDATE);
	}

	public Set<EsfUUID> getPermDeleteIds() {
		if ( permDeleteIds == null ) {
			permDeleteIds = createIdHashSet( group.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_DELETE) );
		}
		return permDeleteIds;
	}
	
	public void setPermDeleteIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_DELETE);
	}

	protected void setPermMemberUsersIds(Set<EsfUUID> v, EsfName permOption) {
		// First, let's remove all groups this user can list
		for( Group g : Group.Manager.getForUserWithListPermission(user()) ) {
			group.removeMemberUsersPermissionAllowedGroups(permOption, g);
		}
		// Then if we're doing a create-like, remove our new group too
		if ( group.doInsert() ) {
			group.removeMemberUsersPermissionAllowedGroups(permOption, group);
		}
		// Then let's add back in the selected set
		for( EsfUUID gid : v ) {
			Group g = gid.equals(group.getId()) ? group : Group.Manager.getById( gid );
			if ( g != null ) {
				group.addMemberUsersPermissionAllowedGroups(permOption, g);
			}
		}
	}

	public Set<EsfUUID> getPermMemberUsersListIds() {
		if ( permMemberUsersListIds == null ) {
			permMemberUsersListIds = createIdHashSet( group.getMemberUsersPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_LIST) );
		}
		return permMemberUsersListIds;
	}
	
	public void setPermMemberUsersListIds(Set<EsfUUID> v) {
		setPermMemberUsersIds(v, PermissionOption.PERM_OPTION_LIST);
	}

	public Set<EsfUUID> getPermMemberUsersViewDetailsIds() {
		if ( permMemberUsersViewDetailsIds == null ) {
			permMemberUsersViewDetailsIds = createIdHashSet( group.getMemberUsersPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_VIEWDETAILS) );
		}
		return permMemberUsersViewDetailsIds;
	}
	
	public void setPermMemberUsersViewDetailsIds(Set<EsfUUID> v) {
		setPermMemberUsersIds(v, PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public Set<EsfUUID> getPermMemberUsersCreateLikeIds() {
		if ( permMemberUsersCreateLikeIds == null ) {
			permMemberUsersCreateLikeIds = createIdHashSet( group.getMemberUsersPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_CREATELIKE) );
		}
		return permMemberUsersCreateLikeIds;
	}
	
	public void setPermMemberUsersCreateLikeIds(Set<EsfUUID> v) {
		setPermMemberUsersIds(v, PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public Set<EsfUUID> getPermMemberUsersUpdateIds() {
		if ( permMemberUsersUpdateIds == null ) {
			permMemberUsersUpdateIds = createIdHashSet( group.getMemberUsersPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_UPDATE) );
		}
		return permMemberUsersUpdateIds;
	}
	
	public void setPermMemberUsersUpdateIds(Set<EsfUUID> v) {
		setPermMemberUsersIds(v, PermissionOption.PERM_OPTION_UPDATE);
	}

	public Set<EsfUUID> getPermMemberUsersDeleteIds() {
		if ( permMemberUsersDeleteIds == null ) {
			permMemberUsersDeleteIds = createIdHashSet( group.getMemberUsersPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_DELETE) );
		}
		return permMemberUsersDeleteIds;
	}
	
	public void setPermMemberUsersDeleteIds(Set<EsfUUID> v) {
		setPermMemberUsersIds(v, PermissionOption.PERM_OPTION_DELETE);
	}

	public Set<EsfUUID> getPermMemberUsersManageToDoIds() {
		if ( permMemberUsersManageToDoIds == null ) {
			permMemberUsersManageToDoIds = createIdHashSet( group.getMemberUsersPermissionAllowedGroups(user(), PermissionOption.GROUP_MEMBER_USER_PERM_OPTION_MANAGE_TO_DO) );
		}
		return permMemberUsersManageToDoIds;
	}
	
	public void setPermMemberUsersManageToDoIds(Set<EsfUUID> v) {
		setPermMemberUsersIds(v, PermissionOption.GROUP_MEMBER_USER_PERM_OPTION_MANAGE_TO_DO);
	}

	public Set<EsfUUID> getMemberUsersIds() {
		if ( memberUsersIds == null ) {
			memberUsersIds = createUserIdHashSet( group.getAllMemberUsers(user()) );
		}
		return memberUsersIds;
	}
	
	public void setMemberUsersIds(Set<EsfUUID> v) {
		// First, let's remove all users this user can update
		for( User u : Group.Manager.getAllMemberUsersForUserWithUpdateMemberUsersPermission(user()) )
			group.removeMemberUser(u);
		
		// Then let's add back in the selected set
		for( EsfUUID uid : v ) {
			User u = User.Manager.getById( uid );
			if ( u != null ) {
				group.addMemberUser(u);
			}
		}
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return group.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		return group.getLastUpdatedTimestamp().toString(user());
	}
	public String formatLogLastUpdatedTimestamp() {
		return group.getLastUpdatedTimestamp().toLogString(user());
	}

	public String formatCreatedTimestamp() {
		return group.getCreatedTimestamp().toString(user());
	}
	public String formatLogCreatedTimestamp() {
		return group.getCreatedTimestamp().toLogString(user());
	}

	public boolean hasPermViewDetails() {
		return group.hasPermission(user(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		return group.hasPermission(user(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		return group.hasPermission(user(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		return group.isReserved() ? false : group.hasPermission(user(), PermissionOption.PERM_OPTION_DELETE);
	}
	
	public boolean hasPermMemberUsersList() {
		return group.hasMemberUsersPermission(user(), PermissionOption.PERM_OPTION_LIST);
	}
	
	// To update member users from the group, both the group and the member users are being updated, so you need permission on both
	// to change to the membership of the group.
	public boolean hasPermAndMemberUsersUpdate() {
		return hasPermUpdate() && group.hasMemberUsersPermission(user(), PermissionOption.PERM_OPTION_UPDATE);
	}
	

}