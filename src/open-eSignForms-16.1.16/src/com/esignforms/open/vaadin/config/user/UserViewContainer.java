// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.user;

import java.io.Serializable;
import java.util.Collection;

import com.esignforms.open.user.Group;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
//import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;

public class UserViewContainer extends BeanItemContainer<UserBean> implements Serializable {
	private static final long serialVersionUID = 8958808420452192490L;

	public UserViewContainer() throws InstantiationException, IllegalAccessException {
		super(UserBean.class);
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		// In case a user is accidentally removed from all groups, the super group user should still be able to list that user.
		Collection<User> users;
		if ( vaadinUi.getUser().isMemberOfGroup(Group.Manager.getSuperGroup()) )
			users = User.Manager.getAll();
		else
			users = Group.Manager.getAllMemberUsersForUserWithViewDetailsMemberUsersPermission(vaadinUi.getUser());
		
		for( User u : users ) {
			addItem( new UserBean(u) );
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}