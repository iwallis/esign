// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule;

import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.vaadin.hene.popupbutton.PopupButton;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.PackageProgrammingRule;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.runtime.action.Action;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.main.EsfViewWithConfirmDiscardFormChangesWindowParent;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.ListSelectValid;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The PackageProgrammingRuleForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class PackageProgrammingRuleForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 6049851067132222709L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackageProgrammingRuleForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;

	GridLayout layout;
	
	final PackageVersion duplicatedPackageVersion;
	final PackageProgrammingRuleView view;
	final PackageProgrammingRuleContainer container;
	
	final ActionList actionList;
	boolean actionListModified;
	PopupButton addActionButton;
	HorizontalLayout actionListButtonsLayout;
	
	List<EsfUUID> allDocumentIds;
	List<EsfName> allDocumentNames;
	List<EsfUUID> allPartyIds;
	List<EsfName> allPartyNames;
	
	PackageProgrammingRule newRule; // when set, we're working on a new rule.
	
	
	public PackageProgrammingRuleForm(PackageProgrammingRuleView view, final PackageProgrammingRuleContainer container, PackageVersion duplicatedPackageVersionParam)	{
		setStyleName("PackageProgrammingRuleForm");
		this.view = view;
		this.container = container;
		this.duplicatedPackageVersion = duplicatedPackageVersionParam;
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
    	allDocumentIds = new LinkedList<EsfUUID>();
    	allDocumentNames = new LinkedList<EsfName>();
    	allPartyIds = new LinkedList<EsfUUID>();
    	allPartyNames = new LinkedList<EsfName>();
    	
    	for( EsfUUID docId : duplicatedPackageVersion.getDocumentIdList() ) {
    		Document doc = Document.Manager.getById(docId);
    		if ( doc == null ) {
    			_logger.warn("PackageProgrammingRuleForm() - Could not find document with id: " + docId + "; not including in our list");
    			continue;
    		}
    		allDocumentIds.add(docId);
    		allDocumentNames.add(doc.getEsfName());
    	}
	
    	for( PackageVersionPartyTemplate pvpt : duplicatedPackageVersion.getPackageVersionPartyTemplateList() ) {
    		allPartyIds.add(pvpt.getId());
    		allPartyNames.add(pvpt.getEsfName());
    	}
	
    	// Setup layout
    	layout = new GridLayout(4,2);
		layout.setColumnExpandRatio(0, 0.05f);
		layout.setColumnExpandRatio(1, 0.15f);
		layout.setColumnExpandRatio(2, 0.1f);
		layout.setColumnExpandRatio(3, 0.7f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setWidth(100, Unit.PERCENTAGE);
    	setLayout(layout);

    	actionList = new ActionList(vaadinUi.getMsg("PackageProgrammingRuleForm.ActionList.label"));
    	actionList.setWidth(100, Unit.PERCENTAGE);
    	actionList.initializeDND();
    	actionList.setVisible(false);
		layout.addComponent(actionList,3,0);
		
		actionListButtonsLayout = new HorizontalLayout();
		actionListButtonsLayout.setSpacing(true);
		actionListButtonsLayout.setMargin(new MarginInfo(false,true,false,true));
		actionListButtonsLayout.setVisible(false);
		
		VerticalLayout addActionButtonLayout = new VerticalLayout();
		addActionButtonLayout.setSizeUndefined();
		addActionButtonLayout.setMargin(false);
		addActionButtonLayout.setSpacing(true);
		addActionButton = new PopupButton(vaadinUi.getMsg("PackageProgrammingRuleForm.button.addAction.label"));
		addActionButton.setStyleName(Reindeer.BUTTON_SMALL);
		addActionButton.setContent(addActionButtonLayout);
		actionListButtonsLayout.addComponent(addActionButton);

		for( final String actionName : vaadinUi.getStringArray("PrettyCode.package.action.list") ) {
			Button addActionSubButton = new Button(vaadinUi.getMsg("PrettyCode.action."+actionName)+"...", new ClickListener() {
				private static final long serialVersionUID = 1697218755102161856L;

				@Override
				public void buttonClick(ClickEvent event) {
					((PopupButton)event.getButton().getParent().getParent()).setPopupVisible(false);
					processAddActionButton(actionName);
				}
			});
			addActionSubButton.setStyleName(Reindeer.BUTTON_LINK);
			addActionButtonLayout.addComponent(addActionSubButton);
		}
		
		layout.addComponent(actionListButtonsLayout,3,1);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
		setBuffered(true);
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
		closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    	footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 7852039263380202849L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

				if ( propertyId.equals("onEventEsfNames") ) {
					ListSelect select = new ListSelectValid(vaadinUi.getMsg("PackageProgrammingRuleForm.onEventEsfNames.select.label"));
					select.setDescription(vaadinUi.getMsg("PackageProgrammingRuleForm.onEventEsfNames.select.tooltip"));
					select.setRequired(true);
					select.setMultiSelect(true);
					select.setNullSelectionAllowed(false);
					select.setRows(16);
					select.setImmediate(true);
	                select.addValidator(new SelectValidator(select));
					
					for( String eventName : vaadinUi.getStringArray("PrettyCode.package.event.list") ) {
						EsfName name = new EsfName(eventName);
						select.addItem(name);
						select.setItemCaption(name, vaadinUi.getPrettyCode().eventName(eventName));
					}
					
					return select;
				}
				
				if ( propertyId.equals("onDocumentIds") ) {
					ListSelect select = new ListSelectValid(vaadinUi.getMsg("PackageProgrammingRuleForm.onDocumentIds.select.label"));
					select.setDescription(vaadinUi.getMsg("PackageProgrammingRuleForm.onDocumentIds.select.tooltip"));
					select.setRequired(false);
					select.setMultiSelect(true);
					select.setNullSelectionAllowed(true);
					select.setRows(16);
					select.setImmediate(true);
	                select.addValidator(new SelectValidator(select));
					
					ListIterator<EsfName> docNameIter = allDocumentNames.listIterator();
					for( EsfUUID docId : allDocumentIds ) {
						select.addItem(docId);
						select.setItemCaption(docId, docNameIter.next().toString());
					}
					
					return select;
				}
				
				if ( propertyId.equals("onPartyIds") ) {
					ListSelect select = new ListSelectValid(vaadinUi.getMsg("PackageProgrammingRuleForm.onPartyIds.select.label"));
					select.setDescription(vaadinUi.getMsg("PackageProgrammingRuleForm.onPartyIds.select.tooltip"));
					select.setRequired(false);
					select.setMultiSelect(true);
					select.setNullSelectionAllowed(true);
					select.setRows(16);
					select.setImmediate(true);
	                select.addValidator(new SelectValidator(select));
					
					ListIterator<EsfName> partyNameIter = allPartyNames.listIterator();
					for( EsfUUID partyId : allPartyIds ) {
						select.addItem(partyId);
						select.setItemCaption(partyId, partyNameIter.next().toString());
					}
					
					return select;
				}
				
				return null;
    	    }
    	 });

    	_logger.debug("Form created");

	}

	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		PackageProgrammingRule currBean = getCurrentBean();
    		if ( currBean == null ) {
    			saveRules();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
        	
        	if ( actionList.size() < 1 ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("PackageProgrammingRuleForm.ActionList.error.noactions"));
        		return;
        	}
        	
			commit();
			
			currBean.setActions(actionList.getActions());
			actionListModified = false;
			
    		if ( newRule != null ) {
    			container.addItem(newRule);
    			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",vaadinUi.getMsg("PackageProgrammingRuleForm.object.name")) );
    			newRule = null;
    		} else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("PackageProgrammingRuleForm.object.name")) );
    		}
    		
			saveRules();
    		
    		view.select(currBean); // reselect our bean so all updates cleanly
        } else if ( source == cancelButton ) {
    		discard();
    		if ( getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			setNewRuleAsDataSource(null);
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	//discard();
        	view.closeParentWindow();
        } else if ( source == createLikeButton ) {
        	discard();
        	PackageProgrammingRule rule = getCurrentBean();
        	if ( rule != null ) {
    			view.unselectAll();
    			setNewRuleAsDataSource(rule.duplicate());
        	}
        } else if ( source == deleteButton ) {
        	discard();
        	
        	if ( newRule != null ) {
        		newRule = null;
        	} else {
            	PackageProgrammingRule rule = getCurrentBean();
            	if ( rule != null ) {
            		container.removeItem(rule);
        			saveRules();
            	}
        	}
    		view.unselectAll();
			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("PackageProgrammingRuleForm.object.name")) );
        }
    }
	
	void openActionWindow(String actionName, Class<?> actionClass, Action action) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		String actionViewClassName = "com.esignforms.open.vaadin.config.pkg.rule." + actionName + ".ActionView";
		try {
			Class<?> actionViewClass = Class.forName(actionViewClassName); 
			Constructor<?> actionViewConstructor = actionViewClass.getConstructor(new Class[]{PackageVersion.class,actionClass});
			EsfViewWithConfirmDiscardFormChangesWindowParent view = (EsfViewWithConfirmDiscardFormChangesWindowParent)actionViewConstructor.newInstance(new Object[]{duplicatedPackageVersion,action});
			view.initView();
            
			ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("PackageProgrammingRule."+actionName+".view.window.caption"), view);
            w.center();
        	w.setWidth(vaadinUi.getMsg("PackageProgrammingRule."+actionName+".view.window.width"));
        	w.setHeight(vaadinUi.getMsg("PackageProgrammingRule."+actionName+".view.window.height"));
        	w.setModal(true);
			w.addCloseListener( new CloseListener() {
				private static final long serialVersionUID = 2380918874519059801L;

				@Override
				public void windowClose(CloseEvent e) {
					for ( Action action : actionList.getActions() ) {
						if ( action.isChanged() ) {
							actionListModified = true;
							action.clearObjectChanged();
						}
					}
					actionList.select(null);
				}
			});

        	view.activateView(EsfView.OpenMode.WINDOW, "");
            view.setReadOnly(isReadOnly());
        	view.setParentWindow(w);
        	vaadinUi.addWindowToUI(w);	
        } catch( Exception e ) {
			vaadinUi.showError("Unknown action view","Could not create the configuration view for action: " + actionName);
			_logger.error("openActionWindow() with unexpected actionName for view: " + actionName,e);
			return;
		}	
	}
	
	void processAddActionButton(String actionName) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		String actionClassName = "com.esignforms.open.runtime.action." + actionName;
		Class<?> actionClass = null;
		Action action = null;
		try {
			actionClass = Class.forName(actionClassName);
			action = (Action)actionClass.newInstance();
		} catch( Exception e ) {
			vaadinUi.showError("Unknown action","Could not create the action: " + actionName);
			_logger.error("processAddActionButton with unexpected actionName: " + actionName,e);
			return;
		}
		openActionWindow(actionName,actionClass,action);
    	actionList.addAction(action);
	}
	
	@SuppressWarnings("unchecked")
	void saveRules() {
		// Get all of the rules in the container and save them back to the package version
		LinkedList<PackageProgrammingRule> list = new LinkedList<PackageProgrammingRule>();
		
		int order = 1;
		for( PackageProgrammingRule rule : container.getItemIds() ) {
        	Item item = container.getItem(rule);
        	item.getItemProperty("order").setValue(order); // suppress unchecked warning
			rule.setOrder(order++);
			list.add(rule);
		}
		
		duplicatedPackageVersion.getPackageProgramming().setRules(list);
	}
	
	PackageProgrammingRule getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public PackageProgrammingRule getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<PackageProgrammingRule> bi = (BeanItem<PackageProgrammingRule>)dataSource;
		return bi.getBean();
    }
    
    public void setNewRuleAsDataSource(PackageProgrammingRule rule) {
    	newRule = rule;
    	if ( newRule == null )
    		setItemDataSource(null);
    	else
    		setItemDataSource(new BeanItem<PackageProgrammingRule>(newRule));
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		actionListModified = false;
		if ( newDataSource != null ) {
    		super.setItemDataSource(newDataSource, Arrays.asList("onEventEsfNames","onDocumentIds","onPartyIds"));
    		actionList.setupActionList(getCurrentBean());
    		actionList.setVisible(true);
    		actionListButtonsLayout.setVisible(true);
    		layout.setVisible(true);
		} else {
    		super.setItemDataSource(null);
    		actionList.setVisible(false);
    		actionListButtonsLayout.setVisible(false);
    		layout.setVisible(false);
		}
		setReadOnly(isReadOnly());
    }

	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	PackageProgrammingRule bean = getCurrentBean();
    	
    	boolean makeVisible = !readOnly && bean != null;
    	
    	saveButton.setVisible(makeVisible);
    	cancelButton.setVisible(makeVisible);
    	closeButton.setVisible(true);
    	createLikeButton.setVisible(makeVisible && newRule == null);
    	deleteButton.setVisible(makeVisible);

    	addActionButton.setVisible(makeVisible);
    	actionList.setButtonsVisible(makeVisible);
    	actionList.setReadOnly(readOnly);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("onEventEsfNames")) {
            layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("onDocumentIds")) {
        	layout.addComponent(field, 1, 0);
        } else if (propertyId.equals("onPartyIds")) {
        	layout.addComponent(field, 2, 0);
        }
    }
    
    @Override
	public String checkDirty() {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageProgrammingRuleView.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified() || newRule != null || actionListModified;
	}

	
	//************  ActionList is our table of actions for this rule ****************************
	
	public class ActionBean implements java.io.Serializable {
		private static final long serialVersionUID = -6597666519995387406L;

		final ActionBean thisBean;
		int order;
		Action action;
		Button createLikeButton;
		Button deleteButton;
		
		public ActionBean( int order, Action action ) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			this.thisBean = this;
			this.order = order;
			this.action = action;
			this.createLikeButton = new Button(vaadinUi.getMsg("PackageProgrammingRuleForm.ActionList.button.createLike.label"), new ClickListener() {
				private static final long serialVersionUID = 7345046107934440680L;

				@Override
				public void buttonClick(ClickEvent event) {
					actionList.createLikeActionBean(thisBean);
				}
	        });
			this.createLikeButton.setStyleName(Reindeer.BUTTON_SMALL);
			this.createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
			this.deleteButton = new Button(vaadinUi.getMsg("PackageProgrammingRuleForm.ActionList.button.delete.label"), new ClickListener() {
				private static final long serialVersionUID = -4018079677630315976L;

				@Override
				public void buttonClick(ClickEvent event) {
					actionList.removeActionBean(thisBean);
				}
	        });
			this.deleteButton.setStyleName(Reindeer.BUTTON_SMALL);
			this.deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
		}
		
		public int getOrder() {
			return order;
		}
		public void setOrder(int v) {
			order = v;
		}
		public Action getAction() {
			return action;
		}
		public EsfName getActionName() {
			return action.getEsfName();
		}
		
		public String getSpecsToString() {
			return action.specsToString();
		}
		
		public Button getCreateLikeButton() {
			return createLikeButton;
		}
		
		public Button getDeleteButton() {
			return deleteButton;
		}
	} // ActionBean
	
	class ActionList extends Table {
		private static final long serialVersionUID = -3584128410107349323L;

		final BeanItemContainer<ActionBean> container;
		final ActionList thisList;
		
		public ActionList(String caption) {
			super(caption);
			thisList = this;
			container = new BeanItemContainer<ActionBean>(ActionBean.class);
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	        setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("PackageProgrammingRuleForm.ActionList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("PackageProgrammingRuleForm.ActionList.showColumnHeaders"));
			setSortEnabled(false);
			setColumnAlignment("order", Align.CENTER);
			setColumnExpandRatio("specsToString", 0.8f);
			setNullSelectionAllowed(true);
			setSelectable(true);
			setImmediate(true);
			setPageLength(9);
			addValueChangeListener( new Property.ValueChangeListener() {
				private static final long serialVersionUID = 7110498474660409381L;

				@Override
				public void valueChange(Property.ValueChangeEvent event) {
					ActionBean bean = (ActionBean)getValue();
					if ( bean == null) return; // unselected
					
		            openActionWindow(bean.getActionName().toString(), bean.getAction().getClass(), bean.getAction());
				}
			});
		}
		
		@SuppressWarnings("unchecked")
		public void reorder() {
            // Fix up the order now
			LinkedList<Action> orderList = new LinkedList<Action>();
            int order = 1;
            for( ActionBean bi : container.getItemIds() ) {
            	Item item = container.getItem(bi);
            	item.getItemProperty("order").setValue(order); // suppress unchecked
            	bi.setOrder(order++);
            	orderList.add((Action)item.getItemProperty("action").getValue());
            }
            getCurrentBean().setActions(orderList);
    		actionListModified = true;
		}
		
		public void setButtonsVisible(boolean makeVisible) {
            for( ActionBean bi : container.getItemIds() ) {
            	BeanItem<ActionBean> beanItem = container.getItem(bi);
            	ActionBean bean = beanItem.getBean();
            	bean.getCreateLikeButton().setVisible(makeVisible);
            	bean.getDeleteButton().setVisible(makeVisible);
            }
		}
		
	    @Override
	    public void setReadOnly(boolean readOnly) {
	        //super.setReadOnly(false); // we want to keep the Table active so we can still click on entries to view their details
	    	setDragMode( readOnly ? TableDragMode.NONE : TableDragMode.ROW);
	    }
		
		public void initializeDND() {
			setDropHandler(new DropHandler() {
				private static final long serialVersionUID = 4301506268692295910L;

				public void drop(DragAndDropEvent dropEvent) {
	                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
	                
	                Container sourceContainer = t.getSourceContainer();
	                if ( sourceContainer == container ) {
	                	// reordering within the table rows
	                	ActionBean sourceItemId = (ActionBean)t.getItemId();

		                AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails)dropEvent.getTargetDetails());
		                ActionBean targetItemId = (ActionBean)dropData.getItemIdOver();
		                
		                // No move if source and target are the same
		                if ( sourceItemId == targetItemId )
		                	return;
		                
		                // Let's remove the source of the drag so we can add it back where requested...
		                container.removeItem(sourceItemId);
		                if ( targetItemId == null ) {
		                	container.addItem(sourceItemId);
		                } else if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
		                	container.addItemAfter(targetItemId,sourceItemId);
		            	} else {
		                    Object prevItemId = container.prevItemId(targetItemId);
		                    container.addItemAfter(prevItemId, sourceItemId);
		            	}
		                
		                reorder();
	                }
	            }

	            public AcceptCriterion getAcceptCriterion() {
	                return new And(SourceIsTarget.get(), new Not(AbstractSelect.VerticalLocationIs.MIDDLE)); // dragging within the table
	            }
	        });
			
		}
		
		public void setupActionList(PackageProgrammingRule rule) {
			container.removeAllItems();
			
			int order = 1;
			for( Action action : rule.getActions() ) {
				ActionBean actionBean = new ActionBean(order++, action);
				container.addBean(actionBean);
			}
		}
		
		public void addAction(Action action) {
			ActionBean actionBean = new ActionBean(container.size()+1,action);
			container.addBean(actionBean);
			actionListModified = true;
		}
		
		public void createLikeActionBean(ActionBean actionBean) {
			Action dupAction = actionBean.getAction().duplicate();
			ActionBean dupBean = new ActionBean(container.size()+1,dupAction);
			container.addBean(dupBean);
			actionListModified = true;
		}
		
		public void removeActionBean(ActionBean actionBean) {
			container.removeItem(actionBean);
			actionListModified = true;
		}
		
		public List<Action> getActions() {
			LinkedList<Action> actionList = new LinkedList<Action>();
			for( ActionBean bean : container.getItemIds() ) {
				actionList.add(bean.getAction());
			}
			return actionList;
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			if ( "specsToString".equals(colId) ) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				return vaadinUi.getPrettyCode().actionName(((ActionBean)rowId).getActionName()) + ": " + ((ActionBean)rowId).getSpecsToString();
			}
			return super.formatPropertyValue(rowId,colId,property);
		}
	} // ActionList

}