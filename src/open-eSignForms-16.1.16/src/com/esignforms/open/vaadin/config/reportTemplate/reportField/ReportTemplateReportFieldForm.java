// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.reportTemplate.reportField;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.ReportTemplateReportField;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.NativeSelectDropDown;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * The ReportTemplateReportFieldForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class ReportTemplateReportFieldForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -9037980272694295092L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ReportTemplateReportFieldForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;

	GridLayout layout;
	
	final ReportTemplateReportFieldForm thisForm;
	final ReportTemplate duplicatedReportTemplate;
	final ReportTemplateReportFieldView view;
	final ReportTemplateReportFieldContainer container;
	boolean containerOrderHasChanged = false;
	
	List<EsfUUID> allReportFieldTemplateIds;
	List<String> allReportFieldTemplateNames;
	
	ReportTemplateReportField newReportTemplateReportField; // when set, we're working on a new report field.
	
	
	@SuppressWarnings("deprecation")
	public ReportTemplateReportFieldForm(ReportTemplateReportFieldView viewParam, final ReportTemplateReportFieldContainer containerParam, ReportTemplate duplicatedReportTemplateParam)	{
		setStyleName("ReportTemplateReportFieldForm");
		this.thisForm = this;
		this.view = viewParam;
		this.container = containerParam;
		this.duplicatedReportTemplate = duplicatedReportTemplateParam;
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
    	allReportFieldTemplateIds = new LinkedList<EsfUUID>();
    	allReportFieldTemplateNames = new LinkedList<String>();
    	setupAllReportFieldTemplates();
    	
    	// Setup layout
    	layout = new GridLayout(4,1);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // want validators to run
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    	footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -5638520799988990292L;

			@Override
			public Field<?> createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			
				if (propertyId.equals("reportFieldTemplateId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("ReportTemplateReportFieldForm.reportFieldTemplateId.label"));
                	select.setDescription(vaadinUi.getMsg("ReportTemplateReportFieldForm.reportFieldTemplateId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	setupReportFieldTemplateSelect(select);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 5193367380108071516L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newReportFieldTemplateId = (EsfUUID)event.getProperty().getValue();
							if ( newReportFieldTemplateId != null && ! newReportFieldTemplateId.isNull() ) {
								setupAllFieldNames(newReportFieldTemplateId);
							}
						}
                	});
            		return select;
                }
                
				if ( propertyId.equals("allowSearch") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("ReportTemplateReportFieldForm.allowSearch.label"));
	            	cb.setImmediate(false);
	                cb.setDescription(vaadinUi.getMsg("ReportTemplateReportFieldForm.allowSearch.tooltip"));
	                return cb;
				}
				
				Field<?> field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("fieldLabel")) {
                	TextField tf = (TextField)field;
                	tf.setWidth(30, Unit.EX);
                    tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    tf.setCaption(vaadinUi.getMsg("ReportTemplateReportFieldForm.fieldLabel.label"));
                    tf.setDescription(vaadinUi.getMsg("ReportTemplateReportFieldForm.fieldLabel.tooltip"));
                } else if ( propertyId.equals("outputFormatSpec") ) {
                	// We rebuild this field based on type of report field template
                }

                return field;
    	    }
    	 });

    	_logger.debug("Form created");

	}

	void setContainerOrderHasChanged() {
		containerOrderHasChanged = true;
	}
	
	void setupAllFieldNames(EsfUUID reportFieldTemplateId) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		ReportFieldTemplate reportFieldTemplate = ReportFieldTemplate.Manager.getById(reportFieldTemplateId);
		if ( reportFieldTemplate == null )
			return;
		
		TextField fieldLabel = (TextField)getField("fieldLabel");
		if ( fieldLabel == null )
			return;
		String fieldLabelValue = (String)fieldLabel.getValue();
		if ( EsfString.isBlank(fieldLabelValue) ) {
			fieldLabel.setValue(reportFieldTemplate.getFieldLabel()); // setup default column header to be the label as defined in the report field template
		}
		
		// Determine which fields can be searched or not
		CheckBox allowSearch = (CheckBox)getField("allowSearch");
		allowSearch.setVisible(false);

		if ( reportFieldTemplate.isFieldTypeStringOrRelated() || reportFieldTemplate.isFieldTypeDate() ) {
			allowSearch.setVisible(true);
		} else if ( reportFieldTemplate.isFieldTypeBuiltIn() ) {
			if ( reportFieldTemplate.getFieldName().equals("esf_status_text") ) {
				allowSearch.setVisible(true);
			}
		}
		
		// Determine which fields have special output format specs. If the field is a File type, there is no output spec at all.
		Field outputFormatSpec = (Field)getField("outputFormatSpec");
		if ( outputFormatSpec != null ) {
			outputFormatSpec.discard();
			vaadinUi.removeAllValidators(outputFormatSpec);
			detachField(outputFormatSpec);
		}

		if ( reportFieldTemplate.isFieldTypeStringOrRelated() ) {
			NativeSelect select = new NativeSelect(vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.label"));
			select.setRequired(true);
        	select.setNullSelectionAllowed(false);
            select.setDescription(vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
        	select.addItem("standard");
        	select.setItemCaption("standard",vaadinUi.getMsg("LibFieldForm.TypeSubForm.general.outputFormatSpec.standard"));
        	select.addItem("left4mask");
        	select.setItemCaption("left4mask",vaadinUi.getMsg("LibFieldForm.TypeSubForm.general.outputFormatSpec.left4mask"));
        	select.addItem("right4mask");
        	select.setItemCaption("right4mask",vaadinUi.getMsg("LibFieldForm.TypeSubForm.general.outputFormatSpec.right4mask"));
    		select.setPropertyDataSource(getItemDataSource().getItemProperty("outputFormatSpec"));
        	addField("outputFormatSpec",select);
		} else if ( reportFieldTemplate.isFieldTypeDate() ) {
			NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("LibFieldForm.TypeSubForm.date.outputFormatSpec.label"), vaadinUi.getEsfapp().getDropDownDateFormatEsfName());
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.date.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
			select.setPropertyDataSource(getItemDataSource().getItemProperty("outputFormatSpec"));
			addField("outputFormatSpec",select);
		} else if ( reportFieldTemplate.isFieldTypeDecimal() ) {
			NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.decimal.label"), 
							vaadinUi.getEsfapp().getDropDownMoneyFormatEsfName(), vaadinUi.getEsfapp().getDropDownDecimalFormatEsfName());
			select.setRequired(true);
            select.setDescription(vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.decimal.tooltip"));
            select.addValidator(new SelectValidator(select));
			select.setPropertyDataSource(getItemDataSource().getItemProperty("outputFormatSpec"));
			addField("outputFormatSpec",select);
		} else if ( reportFieldTemplate.isFieldTypeInteger() ) {
			NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("LibFieldForm.TypeSubForm.integer.outputFormatSpec.label"), vaadinUi.getEsfapp().getDropDownIntegerFormatEsfName());
			select.setRequired(true);
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.integer.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
			select.setPropertyDataSource(getItemDataSource().getItemProperty("outputFormatSpec"));
			addField("outputFormatSpec",select);
		} else if ( reportFieldTemplate.isFieldTypeFile() ) {
			// We don't have any output format spec for a file report field as it's rendered as popup button
		} else if ( reportFieldTemplate.isFieldTypeBuiltIn() ) {
			if ( reportFieldTemplate.getFieldName().equals("esf_literal") ) {
				outputFormatSpec = new TextField();
				((TextField)outputFormatSpec).setNullRepresentation("");
				outputFormatSpec.setCaption(vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.esf_literal.label"));
				((TextField)outputFormatSpec).setDescription(vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.esf_literal.tooltip"));
				outputFormatSpec.setPropertyDataSource(getItemDataSource().getItemProperty("outputFormatSpec"));
				addField("outputFormatSpec",outputFormatSpec);
			} else if ( reportFieldTemplate.getFieldName().equals("esf_last_updated_by_user") || reportFieldTemplate.getFieldName().equals("esf_created_by_user") ) {
				NativeSelect select = new NativeSelect(vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.label"));
				select.setRequired(true);
	        	select.setNullSelectionAllowed(false);
	            select.setDescription(vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.tooltip"));
	            select.addValidator(new SelectValidator(select));
	        	select.addItem("email");
	        	select.setItemCaption("email",vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.builtin.user.email"));
	        	select.addItem("displayname");
	        	select.setItemCaption("displayname",vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.builtin.user.displayname"));
	        	select.addItem("fulldisplayname");
	        	select.setItemCaption("fulldisplayname",vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.builtin.user.fulldisplayname"));
	        	select.addItem("personalname");
	        	select.setItemCaption("personalname",vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.builtin.user.personalname"));
	        	select.addItem("familyname");
	        	select.setItemCaption("familyname",vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.builtin.user.familyname"));
	    		select.setPropertyDataSource(getItemDataSource().getItemProperty("outputFormatSpec"));
	        	addField("outputFormatSpec",select);
			} else if ( reportFieldTemplate.getFieldName().equals("esf_start_timestamp") || 
					    reportFieldTemplate.getFieldName().equals("esf_last_updated_timestamp") ||
					    reportFieldTemplate.getFieldName().equals("esf_expire_timestamp") ||
					    reportFieldTemplate.getFieldName().equals("esf_stall_timestamp") ||
					    reportFieldTemplate.getFieldName().equals("esf_cancel_timestamp") 
					   ) {
				NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.datetime.label"), vaadinUi.getEsfapp().getDropDownDateTimeFormatEsfName());
	            select.setDescription(vaadinUi.getMsg("ReportTemplateReportFieldForm.outputFormatSpec.datetime.tooltip"));
	            select.addValidator(new SelectValidator(select));
				select.setPropertyDataSource(getItemDataSource().getItemProperty("outputFormatSpec"));
				addField("outputFormatSpec",select);
			}
		}
	}

	void setupAllReportFieldTemplates() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		allReportFieldTemplateIds.clear();
		allReportFieldTemplateNames.clear();
		
    	List<ReportFieldTemplate> reportFieldTemplateList = ReportFieldTemplate.Manager.getAll();
    	for( ReportFieldTemplate rft : reportFieldTemplateList ) {
    		allReportFieldTemplateIds.add(rft.getId());
    		allReportFieldTemplateNames.add(rft.getFieldName() + " (" + vaadinUi.getPrettyCode().reportFieldTemplateType(rft.getFieldType()) + ")");
    	}
    	
    	NativeSelect select = (NativeSelect)getField("reportFieldTemplateId");
    	if ( select != null ) {
    		setupReportFieldTemplateSelect(select);
    	}
	}
	
	void setupReportFieldTemplateSelect(NativeSelect select) {
		EsfUUID targetFieldValue = (EsfUUID)select.getValue(); // save value before we empty the list
		select.removeAllItems();

		ListIterator<String> nameIter = allReportFieldTemplateNames.listIterator();
		for( EsfUUID id : allReportFieldTemplateIds ) {
			select.addItem(id);
			select.setItemCaption(id, nameIter.next());
		}

		if ( targetFieldValue != null && select.containsId(targetFieldValue) ) {
			try {
				select.setValue(targetFieldValue); // reset value back
				select.commit(); // don't let this cause the field to be marked as modified
			} catch( Exception e ) {}
		}
	}


    public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		ReportTemplateReportField currBean = getCurrentBean();
    		if ( currBean == null ) {
    			discard();
    			saveReportFields();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
        	
			commit();

			if ( newReportTemplateReportField != null ) {
    			container.addItem(newReportTemplateReportField);
    			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",vaadinUi.getMsg("ReportTemplateReportFieldForm.object.name")) );
    			newReportTemplateReportField = null;
    		} else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("ReportTemplateReportFieldForm.object.name")) );
    		}
    		
    		saveReportFields();
    		
    		view.select(currBean); // reselect our bean so all updates cleanly
        } else if ( source == cancelButton ) {
    		discard();
    		containerOrderHasChanged = false;
    		if ( getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			setNewReportFieldAsDataSource(null);
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	discard();
        	view.closeParentWindow();
        } else if ( source == createLikeButton ) {
        	discard();
        	ReportTemplateReportField reportField = getCurrentBean();
        	if ( reportField != null ) {
    			view.unselectAll();
    			ReportTemplateReportField newReportField = ReportTemplateReportField.Manager.createLike(reportField.getReportTemplateId(), reportField);
    			newReportField.setFieldOrder((short)(container.size()+1));
    			setNewReportFieldAsDataSource(newReportField);
        	}
        } else if ( source == deleteButton ) {
        	discard();
        	
        	if ( newReportTemplateReportField != null ) {
        		newReportTemplateReportField = null;
        	} else {
        		ReportTemplateReportField reportField = getCurrentBean();
            	if ( reportField != null ) {
            		container.removeItem(reportField);
        			saveReportFields();
            	}
        	}
    		view.unselectAll();
			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("ReportTemplateReportFieldForm.object.name")) );
        }
    }
	
	void saveReportFields() {
		// Get all of the fields in the container and save them back to the package version
		LinkedList<ReportTemplateReportField> list = new LinkedList<ReportTemplateReportField>();
		
		for( ReportTemplateReportField reportField : container.getItemIds() )
			list.add(reportField);
		view.reorder();
		duplicatedReportTemplate.setReportTemplateReportFieldList(list);
		containerOrderHasChanged = false;
		newReportTemplateReportField = null;
		container.refresh(duplicatedReportTemplate.getReportTemplateReportFieldList());
	}
	
	public ReportTemplateReportField getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public ReportTemplateReportField getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<ReportTemplateReportField> bi = (BeanItem<ReportTemplateReportField>)dataSource;
		return bi.getBean();
    }
    
    public void setNewReportFieldAsDataSource(ReportTemplateReportField reportField) {
    	newReportTemplateReportField = reportField;
    	if ( newReportTemplateReportField == null )
    		setItemDataSource(null);
    	else
    		setItemDataSource(new BeanItem<ReportTemplateReportField>(newReportTemplateReportField));
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		if ( newDataSource != null ) {
    		super.setItemDataSource(newDataSource, Arrays.asList("reportFieldTemplateId","fieldLabel","outputFormatSpec","allowSearch"));
    		setupAllFieldNames((EsfUUID)newDataSource.getItemProperty("reportFieldTemplateId").getValue());
    		layout.setVisible(true);
		} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
		}
		setReadOnly(isReadOnly());
    }

	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	ReportTemplateReportField bean = getCurrentBean();
    	
    	saveButton.setVisible(!readOnly && (bean != null || containerOrderHasChanged));
    	cancelButton.setVisible(!readOnly && (bean != null || containerOrderHasChanged));
    	closeButton.setVisible(true);
    	createLikeButton.setVisible(!readOnly && bean != null && newReportTemplateReportField == null);
    	deleteButton.setVisible(!readOnly && bean != null);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("reportFieldTemplateId")) {
        	layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("fieldLabel")) {
        	layout.addComponent(field, 1, 0);
        } else if (propertyId.equals("outputFormatSpec")) {
        	layout.addComponent(field, 2, 0);
        } else if (propertyId.equals("allowSearch")) {
        	layout.addComponent(field, 3, 0);
        }
    }
    
    @Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("ReportTemplateReportFieldView.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified() || newReportTemplateReportField != null || containerOrderHasChanged;
	}
}