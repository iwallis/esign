// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.reportFieldTemplate;

import java.util.Arrays;

import com.esignforms.open.Errors;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.ReportFieldTemplateEsfNameValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * The ReportFieldTemplateForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class ReportFieldTemplateForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 1509536257651647402L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ReportFieldTemplateForm.class);
	
	protected Button saveButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;

	GridLayout layout;
	
	Label id;
	ReportFieldTemplateEsfNameValidator fieldNameValidator;
	
	ReportTemplateBeanTable usedByReportTemplateTable;
	ReportTemplateBeanContainer usedByReportTemplateContainer;
	
	PackageVersionBeanTable feedsReportTemplateTable;
	PackageVersionBeanContainer feedsReportTemplateContainer;
	
	final ReportFieldTemplateForm thisForm;
	final ReportFieldTemplateView view;
	final ReportFieldTemplateContainer container;
	
	
	@SuppressWarnings("deprecation")
	public ReportFieldTemplateForm(ReportFieldTemplateView viewParam, final ReportFieldTemplateContainer containerParam)	{
		setStyleName("PackageReportFieldForm");
		this.thisForm = this;
		this.view = viewParam;
		this.container = containerParam;
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
    	// Setup layout
    	layout = new GridLayout(2,4);
		layout.setColumnExpandRatio(0, 0.5f);
		layout.setColumnExpandRatio(1, 0.5f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setWidth(100, Unit.PERCENTAGE);
    	setLayout(layout);
    	
    	id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id,0,0,1,0);
		
		usedByReportTemplateContainer = new ReportTemplateBeanContainer();
		usedByReportTemplateTable = new ReportTemplateBeanTable(vaadinUi.getMsg("ReportFieldTemplateForm.ReportTemplateBeanTable.label"), usedByReportTemplateContainer);
		usedByReportTemplateTable.setVisible(false);
		layout.addComponent(usedByReportTemplateTable,0,3);

		feedsReportTemplateContainer = new PackageVersionBeanContainer();
		feedsReportTemplateTable = new PackageVersionBeanTable(vaadinUi.getMsg("ReportFieldTemplateForm.PackageVersionBeanTable.label"), feedsReportTemplateContainer);
		feedsReportTemplateTable.setVisible(false);
		layout.addComponent(feedsReportTemplateTable,1,3);

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
		setBuffered(true);
    	setImmediate(true); // want validators to run
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);

		saveButton = new Button(vaadinUi.getMsg("button.save.label"), (ClickListener)this);
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.save.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 7064222825825939238L;

			@Override
			public Field<?> createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				
				if (propertyId.equals("fieldType")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("ReportFieldTemplateForm.fieldType.label"));
                	select.setDescription(vaadinUi.getMsg("ReportFieldTemplateForm.fieldType.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
	                select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	return select;
                }
                
				Field<?> field = super.createField(item, propertyId, uiContext);
            	field.setWidth(100, Unit.PERCENTAGE);

            	if (propertyId.equals("fieldName")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setCaption(vaadinUi.getMsg("caption.esfName"));
                	tf.setDescription(vaadinUi.getMsg("ReportFieldTemplateForm.fieldName.tooltip"));
                	tf.setConversionError(vaadinUi.getMsg("validator.esfname.message"));
                } else if (propertyId.equals("fieldLabel")) {
                	TextField tf = (TextField)field;
                	tf.setCaption(vaadinUi.getMsg("ReportFieldTemplateForm.fieldLabel.label"));
                	tf.setDescription(vaadinUi.getMsg("ReportFieldTemplateForm.fieldLabel.tooltip"));
                	tf.setNullRepresentation("");
                } else if (propertyId.equals("fieldTooltip")) {
                	TextField tf = (TextField)field;
                	tf.setCaption(vaadinUi.getMsg("ReportFieldTemplateForm.fieldTooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("ReportFieldTemplateForm.fieldTooltip.tooltip"));
                	tf.setNullRepresentation("");
                }
                
                return field;
    	    }
    	 });

    	_logger.debug("Form created");

	}

	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		ReportFieldTemplate currBean = getCurrentBean();
    		if ( currBean == null ) {
    			discard();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
        	        	
			commit();
			
        	currBean.save();

			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("ReportFieldTemplateForm.form.duplicatekey.message.param0")) );
    		
    		view.select(currBean); // reselect our bean so all updates cleanly
        } else if ( source == cancelButton ) {
    		discard();
    		if ( getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	discard();
        	view.closeParentWindow();
        } else if ( source == deleteButton ) {
        	discard();
        	ReportFieldTemplate bean = getCurrentBean();
    		setItemDataSource(null);
    		view.unselectAll(); // we'll unselect it and remove it from our container
        	if ( bean != null ) {
        		Errors errors = new Errors();
        		if ( bean.delete(errors) ) {
        			container.removeItem(bean);
        			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("ReportFieldTemplateForm.form.duplicatekey.message.param0")) );
        		} else {
        			vaadinUi.show(errors);
        		}
        	}
        }
    }
	
	public ReportFieldTemplate getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public ReportFieldTemplate getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<ReportFieldTemplate> bi = (BeanItem<ReportFieldTemplate>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		if ( newDataSource != null ) {
    		super.setItemDataSource(newDataSource, Arrays.asList("fieldType","fieldName","fieldLabel","fieldTooltip"));
    		setReadOnly(isReadOnly());
    		setupForm(getCurrentBean());
    		layout.setVisible(true);
		} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		setReadOnly(isReadOnly());
		}
    }

    protected void setupForm(ReportFieldTemplate bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("ReportFieldTemplateForm.label.id.label",bean.getId()) );
		
		Field fieldNameField = getField("fieldName");
		if ( fieldNameValidator != null ) fieldNameField.removeValidator(fieldNameValidator);
		fieldNameValidator = new ReportFieldTemplateEsfNameValidator(bean);
		fieldNameField.addValidator(fieldNameValidator);
		fieldNameField.setReadOnly(isReadOnly() || bean.isFieldTypeBuiltIn());
		
		NativeSelect fieldTypeSelect = (NativeSelect)getField("fieldType");
		fieldTypeSelect.removeAllItems();
		
		if ( bean.isFieldTypeStringOrRelated() ) {
			fieldTypeSelect.addItem(ReportFieldTemplate.FIELD_TYPE_STRING);
			fieldTypeSelect.addItem(ReportFieldTemplate.FIELD_TYPE_NUMERIC_ONLY);
			fieldTypeSelect.addItem(ReportFieldTemplate.FIELD_TYPE_ALPHANUMERIC_ONLY);
			fieldTypeSelect.setItemCaption(ReportFieldTemplate.FIELD_TYPE_STRING, vaadinUi.getPrettyCode().reportFieldTemplateType(ReportFieldTemplate.FIELD_TYPE_STRING));
			fieldTypeSelect.setItemCaption(ReportFieldTemplate.FIELD_TYPE_NUMERIC_ONLY, vaadinUi.getPrettyCode().reportFieldTemplateType(ReportFieldTemplate.FIELD_TYPE_NUMERIC_ONLY));
			fieldTypeSelect.setItemCaption(ReportFieldTemplate.FIELD_TYPE_ALPHANUMERIC_ONLY, vaadinUi.getPrettyCode().reportFieldTemplateType(ReportFieldTemplate.FIELD_TYPE_ALPHANUMERIC_ONLY));
			fieldTypeSelect.setReadOnly(false); // set so we can update it no matter how we end up
			fieldTypeSelect.setValue(bean.getFieldType());
			try {
				fieldTypeSelect.commit(); // don't let this cause the field to be marked as modified
			} catch( Exception e ) {}
			fieldTypeSelect.setReadOnly(isReadOnly());
		} else { // can't change the type
			fieldTypeSelect.addItem(bean.getFieldType());
			fieldTypeSelect.setItemCaption(bean.getFieldType(), vaadinUi.getPrettyCode().reportFieldTemplateType(bean.getFieldType()));
			fieldTypeSelect.setReadOnly(false); // set so we can update it no matter how we end up
			fieldTypeSelect.setValue(bean.getFieldType());
			try {
				fieldTypeSelect.commit(); // don't let this cause the field to be marked as modified
			} catch( Exception e ) {}
			fieldTypeSelect.setReadOnly(true);
		}
		
		usedByReportTemplateContainer.refresh(bean, vaadinUi.getUser());
		feedsReportTemplateContainer.refresh(bean, vaadinUi.getUser());
    }
    
	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	ReportFieldTemplate bean = getCurrentBean();
    	
    	saveButton.setVisible(!readOnly && (bean != null));
    	cancelButton.setVisible(!readOnly && (bean != null));
    	closeButton.setVisible(true);
    	//createLikeButton.setVisible(!readOnly && bean != null && newReportFieldTemplate == null);
    	deleteButton.setVisible(!readOnly && bean != null && ! bean.isFieldTypeBuiltIn() && bean.getNumReportsReferenced() == 0);
    	
    	usedByReportTemplateTable.setVisible(bean != null);
    	feedsReportTemplateTable.setVisible(bean != null && ! bean.isFieldTypeBuiltIn());
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("fieldType")) {
            layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("fieldName")) {
        	layout.addComponent(field, 1, 1);
        } else if (propertyId.equals("fieldLabel")) {
        	layout.addComponent(field, 0, 2);
        } else if (propertyId.equals("fieldTooltip")) {
        	layout.addComponent(field, 1, 2);
        }
    }
    
    @Override
	public String checkDirty() {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("ReportFieldTemplateView.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified() /*|| newReportFieldTemplate != null*/;
	}
	
	class ReportTemplateBeanTable extends Table {
		private static final long serialVersionUID = 1996412523755646209L;

		public ReportTemplateBeanTable(String caption, ReportTemplateBeanContainer container) {
			super(caption);
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			this.alwaysRecalculateColumnWidths = true;
			setVisibleColumns((Object[])vaadinUi.getStringArray("ReportFieldTemplateForm.ReportTemplateBeanTable.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("ReportFieldTemplateForm.ReportTemplateBeanTable.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setPageLength(5);

	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(false);
	        setImmediate(false);
			setSizeFull();
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			return super.formatPropertyValue(rowId,colId,property);
		}

	} // ReportTemplateTable

	class PackageVersionBeanTable extends Table {
		private static final long serialVersionUID = -5936188152982109062L;

		public PackageVersionBeanTable(String caption, PackageVersionBeanContainer container) {
			super(caption);
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			this.alwaysRecalculateColumnWidths = true;
			setVisibleColumns((Object[])vaadinUi.getStringArray("ReportFieldTemplateForm.PackageVersionBeanTable.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("ReportFieldTemplateForm.PackageVersionBeanTable.showColumnHeaders"));
			setColumnAlignment("version", Align.CENTER);
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setPageLength(5);

	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(false);
	        setImmediate(false);
			setSizeFull();
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			return super.formatPropertyValue(rowId,colId,property);
		}

	} // ReportTemplateTable

}