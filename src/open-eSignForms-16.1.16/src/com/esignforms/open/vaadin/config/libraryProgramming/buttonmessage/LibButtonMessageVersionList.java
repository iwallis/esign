// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.buttonmessage;

import java.text.DecimalFormat;

import com.esignforms.open.prog.ButtonMessageInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class LibButtonMessageVersionList extends Panel {
	private static final long serialVersionUID = -6710450971978037240L;

	final LibButtonMessageAndVersionsMainView view;
	LibButtonMessageVersionTable table;
	
	Button createNewButton;
	
	DecimalFormat versionFormat = new DecimalFormat("#,###,###,###");
	

	public LibButtonMessageVersionList(final LibButtonMessageAndVersionsMainView view, final LibButtonMessageVersionBeanContainer container) {
		super();
		this.view = view;
		setStyleName("LibButtonMessageVersionList");
		setSizeFull();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
        VerticalLayout layout = new VerticalLayout();
        setContent(layout);
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
    	
		HorizontalLayout searchBar = new HorizontalLayout();
    	searchBar.addStyleName("searchBar");
    	searchBar.setSpacing(true);
    	searchBar.setMargin(false);
    	if ( view.hasPermCreateLike() ) {
	    	createNewButton = new Button(vaadinUi.getMsg("LibButtonMessageVersionList.searchBar.createNewButton.label"));
	    	createNewButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibButtonMessageVersionList.searchBar.createNewButton.icon")));
	    	createNewButton.setDescription(vaadinUi.getMsg("LibButtonMessageVersionList.searchBar.createNewButton.tooltip"));
	    	createNewButton.setStyleName("createNewButton");
	    	createNewButton.addStyleName(Reindeer.BUTTON_SMALL);
	    	createNewButton.addClickListener( new Button.ClickListener() {			
				private static final long serialVersionUID = -1839074625862471719L;

				@Override
				public void buttonClick(ClickEvent event) {
					view.createNewButtonMessage();
				}
			});
	    	searchBar.addComponent(createNewButton);
    	}
    	layout.addComponent(searchBar);
    	
    	table = new LibButtonMessageVersionTable(view, container);
    	
    	layout.addComponent(table);
    	layout.setExpandRatio(table, 1);
	}
	
	public Table getTable() {
		return table;
	}
	
	protected BeanItem<LibButtonMessageBean> createNewBeanItem()
	{
		ButtonMessageInfo buttonmessage = ButtonMessageInfo.Manager.createNew(view.getLibrary());
		LibButtonMessageBean bean = new LibButtonMessageBean(buttonmessage);
		return new BeanItem<LibButtonMessageBean>(bean);
	}
	
	class LibButtonMessageVersionTable extends Table {
		private static final long serialVersionUID = -1353183062717688094L;

		public LibButtonMessageVersionTable(final LibButtonMessageAndVersionsMainView view, LibButtonMessageVersionBeanContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns(vaadinUi.getStringArray("LibButtonMessageVersionList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("LibButtonMessageVersionList.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setColumnAlignment("version",Align.CENTER);
			setCellStyleGenerator(new Table.CellStyleGenerator( ) {
				private static final long serialVersionUID = 1347547512341191271L;

				@Override
				public String getStyle(Table table, Object itemId, Object propertyId) {
					LibButtonMessageBean buttonmessageBean = view.getLibButtonMessageBean();
					return buttonmessageBean == null || buttonmessageBean.buttonmessage().isEnabled() ? null : "disabledText";
				}
			});
	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener((Property.ValueChangeListener)view);
	        /* We don't want to allow users to de-select a row */
	        setNullSelectionAllowed(false);
			setSizeFull();
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			if ( "lastUpdatedTimestamp".equals(colId) ) {
				return ((LibButtonMessageVersionBean)rowId).formatLastUpdatedTimestamp();
			} else if ( "version".equals(colId) ) {
				int version = ((LibButtonMessageVersionBean)rowId).buttonmessageVerInfo().getVersion();
				return versionFormat.format(version);
			}
			
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // LibButtonMessageVersionTable

} // LibButtonMessageVersionList
