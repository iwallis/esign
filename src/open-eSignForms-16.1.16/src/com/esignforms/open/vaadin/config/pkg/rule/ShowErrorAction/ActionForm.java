// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.ShowErrorAction;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.runtime.action.ShowErrorAction;
import com.esignforms.open.runtime.action.ShowErrorAction.Spec;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.FieldSpecTextField;
import com.esignforms.open.vaadin.widget.ListSelectValid;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Yozons Inc.
 */
public class ActionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 3006757364820684744L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ActionForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;

	GridLayout layout;
	
	final PackageVersion duplicatedPackageVersion;
	final DocumentVersion duplicatedDocumentVersion;
	final ShowErrorAction duplicatedAction;
	final ActionView view;
	final ActionContainer container;
	
	List<EsfUUID> allDocumentIds;
	List<String> allFieldNames;
	List<String> allFieldLabels;
	
	Spec newSpec; // when set, we're working on a new spec.
	
	
	public ActionForm(ActionView view, final ActionContainer container, PackageVersion duplicatedPackageVersionParam, ShowErrorAction duplicatedActionParam)	{
		this.view = view;
		this.container = container;
		this.duplicatedPackageVersion = duplicatedPackageVersionParam;
		this.duplicatedDocumentVersion = null;
		this.duplicatedAction = duplicatedActionParam;
		allDocumentIds = new LinkedList<EsfUUID>(duplicatedPackageVersion.getDocumentIdList());
		setupForm();
	}
	
	public ActionForm(ActionView view, final ActionContainer container, DocumentVersion duplicatedDocumentVersionParam, ShowErrorAction duplicatedActionParam)	{
		this.view = view;
		this.container = container;
		this.duplicatedPackageVersion = null;
		this.duplicatedDocumentVersion = duplicatedDocumentVersionParam;
		this.duplicatedAction = duplicatedActionParam;
		allDocumentIds = new LinkedList<EsfUUID>();
		allDocumentIds.add(duplicatedDocumentVersion.getDocumentId());
		setupForm();
	}	
	
	private void setupForm() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		setStyleName("ShowErrorActionForm");
		
		boolean isProduction;
		if ( duplicatedPackageVersion != null ) {
			com.esignforms.open.prog.Package pkg = duplicatedPackageVersion.getPackage();
			isProduction = pkg.getProductionVersion() == duplicatedPackageVersion.getVersion();
		} else {
			com.esignforms.open.prog.Document doc = duplicatedDocumentVersion.getDocument();
			isProduction = doc.getProductionVersion() == duplicatedDocumentVersion.getVersion();
		}
		
		allFieldLabels = new LinkedList<String>();
		allFieldNames = new LinkedList<String>();
		for ( EsfUUID docId : allDocumentIds ) {
    		Document doc = Document.Manager.getById(docId);
    		if ( doc == null ) {
    			_logger.warn("ActionForm() - Could not find document with id: " + docId);
    			continue;
    		}
    		DocumentVersion docVersion;
    		if ( isProduction )
    			docVersion = doc.getProductionDocumentVersion();
    		else
    			docVersion = doc.getTestDocumentVersion();
    		Map<EsfName,FieldTemplate> fieldTemplateMap = docVersion.getFieldTemplateMap();
    		for( EsfName fieldName : fieldTemplateMap.keySet() ) {
    			FieldTemplate fieldTemplate = fieldTemplateMap.get(fieldName);
    			if ( fieldTemplate.getContainerReferenceCount() > 0 && ! fieldTemplate.isTypeRadioButtonGroup() ) {
    				allFieldNames.add( fieldName.toString() );
    				allFieldLabels.add( fieldName + " (" + doc.getEsfName() + ")" );
    			}
    		}
		}
	
    	// Setup layout
    	layout = new GridLayout(3,2);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setWidth(100, Unit.PERCENTAGE);
    	setLayout(layout);

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	
    	saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    	footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 743796976664021278L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

				if (propertyId.equals("highlightFieldSet")) {
                	ListSelect select = new ListSelectValid(vaadinUi.getMsg("PackageProgrammingRule.ShowErrorAction.form.highlightFieldSet.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ShowErrorAction.form.highlightFieldSet.tooltip"));
                	select.setNullSelectionAllowed(true);
                	select.setRequired(false);
                	select.setImmediate(true);
                	select.setMultiSelect(true);
                	select.setRows( Math.min(5, allFieldNames.size()) );
	                select.addValidator(new SelectValidator(select));
	                select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
	                ListIterator<String> iter = allFieldLabels.listIterator();
                	for( String fieldName : allFieldNames ) {
                		select.addItem(fieldName);
                		select.setItemCaption(fieldName, iter.next().toString());
                	}
                	return select;
                }
                
				if (propertyId.equals("htmlMessage")) {
                	CheckBox cb = new CheckBox(vaadinUi.getMsg("PackageProgrammingRule.ShowErrorAction.form.htmlMessage.label"));
                	cb.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ShowErrorAction.form.htmlMessage.tooltip"));
                	return cb;
                }
                
				if ( propertyId.equals("type") ) {
					// Create the radio buttons to have the four possible values.
					OptionGroup optionGroup = new OptionGroup();
					// This is required, but no need the asterisk since they should be forced to choose something as its a radio button that should have a valid default value
					optionGroup.addItem(Errors.TYPE_ERROR);
					optionGroup.addItem(Errors.TYPE_WARNING);
					optionGroup.addItem(Errors.TYPE_SUCCESS);
					optionGroup.addItem(Errors.TYPE_INFO);
					// Associate different labels/captions to those values
					optionGroup.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					optionGroup.setItemCaption(Errors.TYPE_ERROR, vaadinUi.getPrettyCode().errorsType(Errors.TYPE_ERROR));
					optionGroup.setItemCaption(Errors.TYPE_WARNING, vaadinUi.getPrettyCode().errorsType(Errors.TYPE_WARNING));
					optionGroup.setItemCaption(Errors.TYPE_SUCCESS, vaadinUi.getPrettyCode().errorsType(Errors.TYPE_SUCCESS));
					optionGroup.setItemCaption(Errors.TYPE_INFO, vaadinUi.getPrettyCode().errorsType(Errors.TYPE_INFO));
					optionGroup.setImmediate(true);
					optionGroup.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 2685870370146992274L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							String selectedOption = (String)event.getProperty().getValue();
							Field highlightFieldSet = getField("highlightFieldSet");
							if ( highlightFieldSet != null )
								highlightFieldSet.setEnabled(Errors.TYPE_ERROR.equals(selectedOption) || Errors.TYPE_WARNING.equals(selectedOption));
						}
						
					});
					return optionGroup;
				}
					
				FieldSpecTextField tf = new FieldSpecTextField();
                tf.setWidth(100, Unit.PERCENTAGE);
    			
                if ( propertyId.equals("message") ) {
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.ShowErrorAction.form.message.label"));
                    tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ShowErrorAction.form.message.tooltip"));
                }
                
                return tf;
    	    }
    	 });

    	_logger.debug("Form created");

	}
	
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		if ( getItemDataSource() == null ) {
    			saveSpecs();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
    		if ( newSpec != null ) {
    			container.addItem(newSpec);
    			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",vaadinUi.getMsg("PackageProgrammingRule.ShowErrorAction.form.message.spec.name")) );
    			newSpec = null;
    		} else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("PackageProgrammingRule.ShowErrorAction.form.message.spec.name")) );
    		}
    		
			saveSpecs();
    		
    		view.select(getCurrentBean()); // reselect our bean so all updates cleanly
        } else if ( source == cancelButton ) {
    		discard();
    		if ( getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			setNewSpecAsDataSource(null);
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	view.closeParentWindow();
        } else if ( source == createLikeButton ) {
        	discard();
        	Spec spec = getCurrentBean();
        	if ( spec != null ) {
    			view.unselectAll();
    			setNewSpecAsDataSource(spec.duplicate());
        	}
        } else if ( source == deleteButton ) {
        	discard();
        	
        	if ( newSpec != null ) {
        		newSpec = null;
        	} else {
            	Spec spec = getCurrentBean();
            	if ( spec != null ) {
            		container.removeItem(spec);
        			saveSpecs();
            	}
        	}
    		view.unselectAll();
			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("PackageProgrammingRule.ShowErrorAction.form.message.spec.name")) );
        }
    }
	
	void saveSpecs() {
		view.specListUpdated();
	}
	
	Spec getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public Spec getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<Spec> bi = (BeanItem<Spec>)dataSource;
		return bi.getBean();
    }
    
    public void setNewSpecAsDataSource(Spec spec) {
    	newSpec = spec;
    	if ( newSpec == null )
    		setItemDataSource(null);
    	else
    		setItemDataSource(new BeanItem<Spec>(newSpec));
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		if ( newDataSource != null ) {
			super.setItemDataSource(newDataSource, Arrays.asList("message","htmlMessage","highlightFieldSet","type")); // put type after highlight fields to trigger setting of type to set state of highlight fields
    		layout.setVisible(true);
		} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
		}
		setReadOnly(isReadOnly());
    }

	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	Spec bean = getCurrentBean();
    	
    	saveButton.setVisible(!readOnly && bean != null);
    	cancelButton.setVisible(!readOnly && bean != null);
    	closeButton.setVisible(true);
    	createLikeButton.setVisible(!readOnly && bean != null && newSpec == null);
    	deleteButton.setVisible(!readOnly && bean != null);
    }
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("message")) {
            layout.addComponent(field, 0, 0, 2, 0);
        } else if (propertyId.equals("highlightFieldSet")) {
        	layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("type")) {
        	layout.addComponent(field, 1, 1);
        	layout.setComponentAlignment(field, Alignment.MIDDLE_LEFT);
        } else if (propertyId.equals("htmlMessage")) {
        	layout.addComponent(field, 2, 1);
        	layout.setComponentAlignment(field, Alignment.MIDDLE_LEFT);
        }
    }
	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageProgrammingRule.ShowErrorAction.view.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified() || newSpec != null;
	}
	
}