// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.image;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.prog.ImageVersionInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibImageVersionBean implements Serializable, Comparable<LibImageVersionBean> {
	private static final long serialVersionUID = -2563231320712075770L;

	private ImageVersionInfo imageVerInfo;
	private String versionLabel;
	
	private ImageVersion imageVersion;
	
	public LibImageVersionBean(ImageVersionInfo imageVerInfo, String versionLabel) {
		this.imageVerInfo = imageVerInfo;
		this.versionLabel = versionLabel;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibImageVersionBean )
			return imageVerInfo.equals(((LibImageVersionBean)o).imageVerInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return imageVerInfo.hashCode();
    }
	public int compareTo(LibImageVersionBean d) {
		return imageVerInfo.compareTo(d.imageVerInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public ImageVersionInfo imageVerInfo() {
		return imageVerInfo;
	}

	public ImageVersion imageVersion() {
		if ( imageVersion == null )
			imageVersion = ImageVersion.Manager.getById(imageVerInfo.getId());
		return imageVersion;
	}

	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( imageVersion().save(con,vaadinUi.getUser()) ) {
			imageVerInfo = ImageVersionInfo.Manager.createFromSource(imageVersion());
			resetCaches();
			return true;
		} 

		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}
	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return imageVerInfo.getId();
	}

	public EsfUUID getImageId() {
		return imageVerInfo.getImageId();
	}

	public int getVersion() {
		return imageVerInfo.getVersion();
	}
	
	public String getVersionLabel() {
		return versionLabel;
	}

	public EsfDateTime getCreatedTimestamp() {
		return imageVerInfo.getCreatedTimestamp();
	}
	public String formatCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return imageVerInfo.getCreatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return imageVerInfo.getCreatedTimestamp().toLogString(vaadinUi.getUser());
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return imageVerInfo.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return imageVerInfo.getLastUpdatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return imageVerInfo.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser());
	}
	
	public String getImageFileName() {
		return imageVerInfo.getImageFileName();
	}
	public boolean hasImageFileName() {
		return EsfString.isNonBlank(getImageFileName());
	}
	public void setImageFileName(String v) {
		imageVersion().setImageFileName(v);
	}	
	public String getImageByIdUrl() {
		return imageVersion().getImageByIdUrl();
	}
	public String getThumbnailByIdUrl() {
		return imageVersion().getThumbnailByIdUrl();
	}
	
	public void setImageData(byte[] v) {
		imageVersion().setImageData(v);
	}
	public void setImageMimeType(String v) {
		imageVersion().setImageMimeType(v);
	}
	public void setThumbnailData(byte[] v) {
		imageVersion().setThumbnailData(v);
	}
	public boolean hasThumbnailData() {
		byte[] thumbData = imageVersion().getThumbnailData();
		return thumbData != null && thumbData.length > 0;
	}
	
	public boolean getUseDataUri() {
		ImageVersion iv = imageVersion();
		return iv == null ? false : iv.isUseDataUri();
	}
	public void setUseDataUri(boolean v) {
		imageVersion().setUseDataUri( v ? Literals.Y : Literals.N );
	}
	
	public boolean hasChanged() {
		return imageVersion().hasChanged();
	}
	public void discard() {
		if ( imageVersion != null ) {
			EsfUUID id = imageVersion.getId();
			ImageVersion.Manager.dropFromCache(imageVersion);
			imageVersion = ImageVersion.Manager.getById(id); // get fresh if we have one
		}
		resetCaches();
	}
	
	public boolean isImageVersionReady() {
		return EsfString.isNonBlank(imageVersion().getImageFileName()) && imageVersion().getImageData() != null /* && imageVersion().getThumbnailData() != null*/ ;
	}
}