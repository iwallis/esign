// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.field;

import java.io.Serializable;

import com.esignforms.open.prog.LabelTemplate;


public class LibLabelBean implements Serializable, Comparable<LibLabelBean> {
	private static final long serialVersionUID = 1530771325061998462L;

	private LabelTemplate labelTemplate;
	
	public LibLabelBean(LabelTemplate labelTemplate) {
		this.labelTemplate = labelTemplate;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibLabelBean )
			return labelTemplate.equals(((LibLabelBean)o).labelTemplate);
		return false;
	}
	@Override
    public int hashCode() {
    	return labelTemplate.hashCode();
    }
	@Override
	public int compareTo(LibLabelBean o) {
		return labelTemplate.compareTo(o.labelTemplate);
	}

	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public LabelTemplate labelTemplate() {
		return labelTemplate;
	}
	
	public String getLabel() {
		return labelTemplate.getLabel();
	}
	public void setLabel(String v) {
		labelTemplate.setLabel(v);
	}
	
    public String getPosition()
    {
    	return labelTemplate.getPosition();
    }
    public void setPosition(String v)
    {
    	labelTemplate.setPosition(v);
    }

    public String getSeparator()
    {
    	return labelTemplate.getSeparator();
    }
    public void setSeparator(String v)
    {
    	labelTemplate.setSeparator(v);
    }

    public String getSize()
    {
    	return labelTemplate.getSize();
    }
    public void setSize(String v)
    {
    	labelTemplate.setSize(v);
    }

    public boolean isSuppressNonInput()
    {
    	return labelTemplate.isSuppressNonInput();
    }
    public void setSuppressNonInput(boolean v)
    {
    	labelTemplate.setSuppressNonInput(v ? LabelTemplate.SUPPRESS_NON_INPUT : LabelTemplate.SHOW_NON_INPUT);
    }

}