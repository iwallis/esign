// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.party;

import java.util.List;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

/**
 * LibPartyView is a splitpanel that contains the party list on the left and the party form on the right.
 * 
 * @author Yozons
 *
 */
public class LibPartyView extends HorizontalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = -5709635979592725708L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibPartyView.class);

    EsfUUID libraryId;
    Library library;
    final LibPartyView thisView;
    EsfUUID partyContainerId;
    List<PartyTemplate> partyTemplateList;
    
    LibPartyBeanContainer container;
    LibPartyList list;
    LibPartyForm form;
	
	public LibPartyView() {
		super(); 
		thisView = this;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			library = Library.Manager.getById(libraryId, vaadinUi.getUser());
			partyContainerId = library.getId();
			partyTemplateList = PartyTemplate.Manager.getAll(partyContainerId);
			
			container = new LibPartyBeanContainer( partyTemplateList);
			
			list = new LibPartyList(this,container);
		    form = new LibPartyForm(this,container);
		    
		    setFirstComponent(list);
			setSecondComponent(form);
			setSplitPosition(450,Unit.PIXELS);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Field view exception", e.getMessage());
		}
	}
	
	public java.util.List<PartyTemplate> getPartyTemplateList() {
		return partyTemplateList;
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		if ( list != null) list.setReadOnly(v);
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our list or not
        if (property == list.getTable()) {
        	final Item item = list.getTable().getItem(list.getTable().getValue());
        	
    		if ( form.isDirty() ) {
    			if ( form.getCurrentBean() == form.getBean(item) ) {
    				// We've selected the one that's been modified, so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibPartyView.ConfirmDiscardChangesDialog.message", form.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						form.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						list.getTable().select(form.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibPartyView.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
                form.setItemDataSource(item);
    		}
        }
        else
        	_logger.debug("valueChange() - no list event: " + event.toString());
	}

	@Override
	public void detach() {
		super.detach();
	}

	public void select(LibPartyBean bean) {
		unselectAll();
		list.getTable().select(bean);
		list.getTable().setCurrentPageFirstItemId(bean);
	}
	
	public void unselectAll() {
		list.getTable().setValue(null);
	}
	
	public boolean createLike(LibPartyBean likeBean) {
		PartyTemplate partyTemplate = PartyTemplate.Manager.createLike(partyContainerId, likeBean.partyTemplate());
		
		LibPartyBean newBean = new LibPartyBean(partyTemplate);
		BeanItem<LibPartyBean> newItem = new BeanItem<LibPartyBean>(newBean);
        form.setItemDataSource(newItem);
        setReadOnly(false);
		return true;
	}
	
	public boolean createNew() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		PartyTemplate partyTemplate = PartyTemplate.Manager.createNew(partyContainerId, new EsfName(vaadinUi.getMsg("createNew.esfname")));
		LibPartyBean newBean = new LibPartyBean(partyTemplate);
		
		BeanItem<LibPartyBean> newItem = new BeanItem<LibPartyBean>(newBean);
        form.setItemDataSource(newItem);
        setReadOnly(false);
		return true;
	}
	
	public Library getLibrary() {
		return library;
	}
	
	public boolean hasPermViewDetails() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return ! isReadOnly() && getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return ! isReadOnly() && getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return ! isReadOnly() && getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_DELETE);
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		_logger.debug("activateView() - params: " + params);
		libraryId = new EsfUUID(params);
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return form.checkDirty();
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return form.isDirty();
	}
}