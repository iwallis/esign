// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Property;
import com.vaadin.server.ExternalResource;
import com.vaadin.ui.BrowserFrame;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class LibDocumentVersionTester extends Panel implements EsfView {
	private static final long serialVersionUID = -1728191862912850740L;

	final LibDocumentVersionTester thisTester;
	DocumentVersion docVer;
	int pageNumber;
	
	BrowserFrame jspDocument;
	
	public LibDocumentVersionTester(DocumentVersion docVer, int pageNumber) {
		this.thisTester = this;
		this.docVer = docVer;
		this.pageNumber = pageNumber;
        setStyleName("LibDocumentVersionTester");
        setSizeFull();
        setStyleName(Reindeer.PANEL_LIGHT);
        VerticalLayout panelLayout = new VerticalLayout();
        panelLayout.setMargin(false);
        panelLayout.setSpacing(false);
        panelLayout.setSizeFull();
        setContent(panelLayout);
    }
    
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
        VerticalLayout panelLayout = (VerticalLayout)getContent();
    	
		Button closeButton = new Button(vaadinUi.getMsg("LibDocumentVersionTester.button.closeWindow.label"));
		closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
		closeButton.addClickListener( new Button.ClickListener() {
			private static final long serialVersionUID = -2900872753206195535L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance(); 
				ConfirmDiscardFormChangesWindow w = vaadinUi.getConfirmDiscardFormChangesWindow(thisTester);
				if ( w != null )
					w.close();
			}
		});
		
		NativeSelect partySelect = new NativeSelect(vaadinUi.getMsg("LibDocumentVersionTester.select.party.label"));
		partySelect.setDescription(vaadinUi.getMsg("LibDocumentVersionTester.select.party.tooltip"));
		partySelect.setNullSelectionAllowed(false); 
		partySelect.setImmediate(true);
		partySelect.addValidator(new SelectValidator(partySelect));
		PartyTemplate firstParty = null;
		for( PartyTemplate party : docVer.getPartyTemplateList() ) {
			if ( firstParty == null ) firstParty = party;
			partySelect.addItem(party.getEsfName());
		}
		// It's possible they are testing without defining any parties yet...for view only access.
		if ( firstParty != null ) {
			partySelect.setValue(firstParty.getEsfName());
		}
		partySelect.addValueChangeListener( new Property.ValueChangeListener() {
			private static final long serialVersionUID = -4072570626276370739L;

			@Override
			public void valueChange(Property.ValueChangeEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				EsfName partyName = (EsfName)event.getProperty().getValue();
				jspDocument.setSource(new ExternalResource(vaadinUi.getRequestExternalContextPath()+"/TestDocument/"+docVer.getDocumentId()+"?page="+pageNumber+"&party="+partyName));
			}
		});

		
    	HorizontalLayout header = new HorizontalLayout();
    	header.setWidth(100, Unit.PERCENTAGE);
    	header.setMargin(false);
    	header.setSpacing(true);
    	header.setStyleName("header");
    	header.addComponent(closeButton);
    	FormLayout partySelectLayout = new FormLayout();
    	partySelectLayout.setMargin(false);
    	partySelectLayout.setSpacing(false);
    	partySelectLayout.addComponent(partySelect);
    	header.addComponent(partySelectLayout);
    	panelLayout.addComponent(header);
    	
        jspDocument = new BrowserFrame();
        jspDocument.setWidth(100, Unit.PERCENTAGE);
        jspDocument.setHeight(98, Unit.PERCENTAGE); // avoids double scrollbars on IE, FF (panel and BrowserFrame)
        jspDocument.setSource(new ExternalResource(vaadinUi.getRequestExternalContextPath()+"/TestDocument/"+docVer.getDocumentId()+"?page="+pageNumber));
    	panelLayout.addComponent(jspDocument);
    	panelLayout.setExpandRatio(jspDocument, 1.0f);
	}

	@Override
	public void activateView(OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return null;
	}

	@Override
	public void initView() {
    	buildLayout();
	}
	
	@Override
	public boolean isDirty() {
		return false;
	}
	
}