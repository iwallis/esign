// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.ui;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.Permission;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.UI;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;

public class UIBean implements Serializable, Comparable<UIBean> {
	private static final long serialVersionUID = 1208100205408697160L;

	private UI ui;
	private EsfName viewName;
	private Permission permission;
	
	// This contains all of the groups that the user can list, so these are the only ones that
	// can be used to add/remove groups (any groups that the Group object has that are not included
	// cannot be touched)
	private EsfUUID[] allGroupListIds;
	private EsfPathName[] allGroupListNames;
	
	private Set<EsfUUID> permListIds;
	private Set<EsfUUID> permViewDetailsIds;
	private Set<EsfUUID> permCreateLikeIds;
	private Set<EsfUUID> permUpdateIds;
	private Set<EsfUUID> permDeleteIds;

	
	public UIBean(EsfName viewName) {
		this.viewName = viewName;
		ui = new UI();
		permission = ui.getPermission(viewName);
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof UIBean )
			return viewName.equals(((UIBean)o).viewName);
		return false;
	}
	@Override
    public int hashCode() {
    	return viewName.hashCode();
    }
	@Override
	public int compareTo(UIBean uibean) {
		return viewName.compareTo(uibean.viewName);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public User user() {
		return EsfVaadinUI.getInstance().getUser();
	}
	public Permission permission() {
		return permission;
	}

	public UIBean createLike() {
		EsfName[] fromPathNames = permission.getPathName().getNames();
		EsfName fromViewName = fromPathNames[fromPathNames.length-1];
		
	    Permission newPerm = Permission.Manager.createLike(permission.getPathName(), permission);
	    if ( newPerm == null )
	    	return null;
	    
	    UIBean newBean = new UIBean(fromViewName);
	    newBean.permission = newPerm;
	    return newBean;
	}
	
	public boolean save(Errors errors) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
        ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
    		boolean ret = permission.save(con);
    		if ( ret ) {
                con.commit();
    			resetCaches();
                return true;
     		} 
    		
			if ( errors != null ) {
				errors.addError(vaadinUi.getMsg("form.save.failed.message"));
			}
            con.rollback();
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
        	vaadinUi.getEsfapp().cleanupPool(pool,con,null);
        }
        return false;
	}
	public boolean save() {
		return save(null);
	}
	
	protected HashSet<EsfUUID> createIdHashSet( Collection<Group> list ) {
		HashSet<EsfUUID> set = new HashSet<EsfUUID>(list.size());
		for( Group g : list ) {
			set.add(g.getId());
		}
		return set;
	}
	
	private synchronized void resetCaches() {
		allGroupListIds = null;
		allGroupListNames = null;
		permListIds = null;
		permViewDetailsIds = null;
		permCreateLikeIds = null;
		permUpdateIds = null;
		permDeleteIds = null;
	}

	protected synchronized void createAllGroupLists() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Collection<Group> list = Group.Manager.getForUserWithListPermission(user());
		
		int size = list.size();
		
		// We will not include the "External Users" pseudo-group.
		allGroupListIds = new EsfUUID[size-1];
		allGroupListNames = new EsfPathName[size-1];
		int i = 0;
		for( Group g : list ) {
			if ( g.isExternalUsersGroup() )
				continue;
			allGroupListIds[i] = g.getId();
			allGroupListNames[i] = g.isDisabled() ? new EsfPathName(g.getPathName() + EsfPathName.PATH_SEPARATOR + vaadinUi.getMsg("PrettyCode.status.disabled")) : g.getPathName();
			++i;
		}
	}
	public EsfUUID[] allGroupListIds() {
		if ( allGroupListIds == null ) {
			createAllGroupLists();
		}
		return allGroupListIds;
	}
	public EsfPathName[] allGroupListNames() {
		if ( allGroupListNames == null ) {
			createAllGroupLists();
		}
		return allGroupListNames;
	}

	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return permission.getId();
	}
	
	public EsfPathName getPathName() {
		return permission.getPathName();
	}
	
	public EsfName getViewName() {
		return viewName;
	}
	/* Turn off the ability to set/change a view name
	public void setViewName(EsfName v) {
		viewName = v;
		permission.setPathName( ui.getPathNameForView(viewName) );
	}
	*/
	public String getDescription() {
		return permission.getDescription();
	}
	public void setDescription(String v) {
		permission.setDescription(v);
	}
	
    protected final Collection<Group> getPermissionAllowedGroups(EsfName permOptionName)
    {
    	return permission.getAllowedGroups(user(), permOptionName);
    }

	protected void setPermIds(Set<EsfUUID> v, EsfName permOption) {
		// First, let's remove all groups this user can list
		for( Group g : Group.Manager.getForUserWithListPermission(user()) ) {
			permission.removeGroupFromOptions(g,permOption);
		}

		// Then let's add back in the selected set
		for( EsfUUID gid : v ) {
			Group g = Group.Manager.getById( gid );
			if ( g != null ) {
				permission.addGroupToOptions(g, permOption);
			}
		}
	}

    public Set<EsfUUID> getPermListIds() {
		if ( permListIds == null ) {
			permListIds = createIdHashSet( getPermissionAllowedGroups(PermissionOption.PERM_OPTION_LIST) );
		}
		return permListIds;
	}
    
	
	public void setPermListIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_LIST);
	}

	public Set<EsfUUID> getPermViewDetailsIds() {
		if ( permViewDetailsIds == null ) {
			permViewDetailsIds = createIdHashSet( getPermissionAllowedGroups(PermissionOption.PERM_OPTION_VIEWDETAILS) );
		}
		return permViewDetailsIds;
	}
	
	public void setPermViewDetailsIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public Set<EsfUUID> getPermCreateLikeIds() {
		if ( permCreateLikeIds == null ) {
			permCreateLikeIds = createIdHashSet( getPermissionAllowedGroups(PermissionOption.PERM_OPTION_CREATELIKE) );
		}
		return permCreateLikeIds;
	}
	
	public void setPermCreateLikeIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public Set<EsfUUID> getPermUpdateIds() {
		if ( permUpdateIds == null ) {
			permUpdateIds = createIdHashSet( getPermissionAllowedGroups(PermissionOption.PERM_OPTION_UPDATE) );
		}
		return permUpdateIds;
	}
	
	public void setPermUpdateIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_UPDATE);
	}

	public Set<EsfUUID> getPermDeleteIds() {
		if ( permDeleteIds == null ) {
			permDeleteIds = createIdHashSet( getPermissionAllowedGroups(PermissionOption.PERM_OPTION_DELETE) );
		}
		return permDeleteIds;
	}
	
	public void setPermDeleteIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_DELETE);
	}

	public boolean hasPermViewDetails() {
		return permission.hasPermission(user(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		return permission.hasPermission(user(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		return permission.hasPermission(user(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		return false;
	}
}