// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.SomeButNotAllBlankCondition;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.runtime.condition.SomeButNotAllBlankCondition;
import com.esignforms.open.runtime.condition.SomeButNotAllBlankCondition.Spec;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.main.EsfViewWithConfirmDiscardFormChangesWindowParent;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

public class ConditionView extends VerticalSplitPanel implements EsfViewWithConfirmDiscardFormChangesWindowParent, Property.ValueChangeListener {
	private static final long serialVersionUID = -8066052280092396266L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ConditionView.class);
	
    final PackageVersion packageVersion;
    final DocumentVersion documentVersion;
    final SomeButNotAllBlankCondition duplicatedCondition;
    final ConditionView thisView;
    ConfirmDiscardFormChangesWindow parentWindow;
    ConditionContainer container;
    ConditionList list;
    ConditionForm form;
	
	public ConditionView(PackageVersion packageVersionParam, SomeButNotAllBlankCondition duplicatedConditionParam) {
		super();
		this.packageVersion = packageVersionParam;
		this.documentVersion = null;
		this.duplicatedCondition = duplicatedConditionParam;
		thisView = this;
	}
	
	public ConditionView(DocumentVersion documentVersionParam, SomeButNotAllBlankCondition duplicatedConditionParam) {
		super();
		this.packageVersion = null;
		this.documentVersion = documentVersionParam;
		this.duplicatedCondition = duplicatedConditionParam;
		thisView = this;
	}
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			setStyleName(Reindeer.SPLITPANEL_SMALL);
			container = new ConditionContainer(duplicatedCondition.getSpecList());
			list = new ConditionList(this,container,duplicatedCondition);
			form = packageVersion != null ? new ConditionForm(this,container,packageVersion,duplicatedCondition) : new ConditionForm(this,container,documentVersion,duplicatedCondition);
			
			setFirstComponent(list);
			setSecondComponent(form);
			setSplitPosition(50,Unit.PERCENTAGE);

			// If we're not in read-only made and the container is empty, auto start the first
			if ( ! isReadOnly() && container.size() == 0 )
				createNew();
			
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("ConditionView view exception", e.getMessage());
		}
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		if ( list != null) list.setReadOnly(v);
		if ( form != null) form.setReadOnly(v);
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our list or not
        if (property == list.getTable()) {
        	final Item item = list.getTable().getItem(list.getTable().getValue());
        	if ( isDirty() ) {
    			if ( form.getItemDataSource() == item ) {
    				// We've selected the same item so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("ConditionTree.SomeButNotAllBlankCondition.view.ConfirmDiscardChangesDialog.message"), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						form.discard();
						form.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						list.getTable().select(item); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("ConditionTree.SomeButNotAllBlankCondition.view.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    		} else {
            	form.setItemDataSource(item);
    		}
        }
        else
        	_logger.debug("valueChange() - no list event: " + event.toString());
	}

	public void createNew() {
		unselectAll();
		Spec newSpec = new Spec(form.getNewSpecDocumentId(),new EsfName(""));
		form.setNewSpecAsDataSource(newSpec);
	}
	
	public void specListUpdated() {
		//list.getTable().reorder();
	}
	
	@Override
	public void setParentWindow(ConfirmDiscardFormChangesWindow w) {
		parentWindow = w;
	}
	
	public void closeParentWindow() {
		parentWindow.close();
	}
	
	public void unselectAll() {
		list.getTable().setValue(null);
	}
	public void select(Spec spec) {
		unselectAll();
		list.getTable().select(spec);
		list.getTable().setCurrentPageFirstItemId(spec);
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return form.checkDirty();
	}
	
	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return form.isDirty();
	}
}