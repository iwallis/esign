// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.prog.PartyTemplateFieldTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.LibraryPartyEsfNameValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.themes.Reindeer;

/**
 * The PartyForm is used to set the values on the party of a document version.
 * @author Yozons Inc.
 */
public class PartyForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 968472383765265289L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PartyForm.class);
	
	Button saveButton;
	Button cancelButton;
	Button createLikeButton;
	Button deleteButton;
	
	final PartyForm thisForm;
	LibDocumentVersionBean docVerBean;
	PartyTemplate partyTemplate;
	
	ConfirmDiscardFormChangesWindow parentWindow;
    GridLayout layout;
    
    final DocumentVersion duplicateDocumentVersion;
	final List<PartyTemplate> libraryPartyTemplateList; // all party templates in our library
	List<FieldTemplate> allRequiredFieldsPartyCanUpdate;

	LibraryPartyEsfNameValidator esfnameValidator;
    
	Label id;
    

    public PartyForm(Library library, LibDocumentVersionBean docVerBean, PartyTemplate partyTemplate) {
    	setStyleName("LibDocumentVersionPageViewPartyForm");
    	this.thisForm = this;
    	this.docVerBean = docVerBean;
    	this.partyTemplate = partyTemplate; 
		this.duplicateDocumentVersion = docVerBean.duplicateDocumentVersion();
		this.libraryPartyTemplateList = PartyTemplate.Manager.getAll(library.getId());
     }
    
	@SuppressWarnings("unchecked")
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
			EsfName esfName = new EsfName((String)getField("esfName").getValue());
			partyTemplate.setEsfName(esfName);
			
			String displayName = (String)getField("displayName").getValue();
			partyTemplate.setDisplayName(displayName);
			
			// We need a new set since the one returned is non-modifiable...
			HashSet<EsfUUID> updatedSelectedFieldTemplateIds = new HashSet<EsfUUID>( (java.util.Set<EsfUUID>)getField("selectedFieldTemplateIds").getValue() );

			// We won't update this list, but we'll use it to set the required override for each of the updatable fields
			Set<EsfUUID> updatedSelectedNotRequiredFieldTemplateIds = (java.util.Set<EsfUUID>)getField("selectedNotRequiredFieldTemplateIds").getValue();
			
			// Make a copy of the original party template field templates into our map.
			List<PartyTemplateFieldTemplate> partyTemplateFieldTemplateList = partyTemplate.getPartyTemplateFieldTemplates();
			HashMap<EsfUUID,PartyTemplateFieldTemplate> originalPartyTemplateFieldTemplates = new HashMap<EsfUUID,PartyTemplateFieldTemplate>();
			for( PartyTemplateFieldTemplate ptft : partyTemplateFieldTemplateList ) {
				originalPartyTemplateFieldTemplates.put(ptft.getFieldTemplateId(), ptft);
			}
			
			// Remove all entries
			partyTemplate.clearPartyTemplateFieldTemplates();
			
			// Scan each of the selected fields.  If it was in our original list, we'll add it back in.  If not, we'll create a new one and put it in our list.
			for( EsfUUID fieldTemplateId : updatedSelectedFieldTemplateIds ) {
				PartyTemplateFieldTemplate ptft = originalPartyTemplateFieldTemplates.remove(fieldTemplateId);
				if ( ptft == null )	{
					ptft = PartyTemplateFieldTemplate.Manager.createNew(partyTemplate.getId(),fieldTemplateId);
				}
				ptft.setKeepRequired( updatedSelectedNotRequiredFieldTemplateIds.contains(fieldTemplateId) ? FieldTemplate.OPTIONAL : FieldTemplate.REQUIRED );
				partyTemplate.addPartyTemplateFieldTemplates(ptft);
			}
			
			// Whatever remains in our original map are no longer desired, so we'll mark them for deletion.
			for( EsfUUID fieldTemplateId : originalPartyTemplateFieldTemplates.keySet() ) {
				PartyTemplateFieldTemplate ptft = originalPartyTemplateFieldTemplates.get(fieldTemplateId);
				partyTemplate.removePartyTemplateFieldTemplate(ptft);
			}
			
			if ( partyTemplate.doInsert() && duplicateDocumentVersion.getPartyTemplate(partyTemplate.getEsfName()) == null )
				duplicateDocumentVersion.addPartyTemplate(partyTemplate);
						
			parentWindow.close();
        } else if ( source == cancelButton ) {
    		discard();
			parentWindow.close();
        } else if ( source == deleteButton ) {
        	discard();
        	duplicateDocumentVersion.removePartyTemplate(partyTemplate);
        	parentWindow.close();
        } else if ( source == createLikeButton ) {
        	partyTemplate = PartyTemplate.Manager.createLike(duplicateDocumentVersion.getId(), partyTemplate);
    		setupForm();
        }
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	saveButton.setVisible(!readOnly);
    	createLikeButton.setVisible(!readOnly);
    	deleteButton.setVisible(!readOnly);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
		setupForm();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	void setupForm() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		PropertysetItem item = new PropertysetItem();
		item.addItemProperty("esfName", new ObjectProperty<EsfName>(partyTemplate.getEsfName()));
		item.addItemProperty("displayName", new ObjectProperty<String>(partyTemplate.getDisplayName(),String.class));
		item.addItemProperty("libraryPartyTemplateId", new ObjectProperty<EsfUUID>(null,EsfUUID.class));
		
		HashSet<EsfUUID> fieldTemplateIdList = new HashSet<EsfUUID>();
		for( PartyTemplateFieldTemplate ptft : partyTemplate.getPartyTemplateFieldTemplates() )	{
			FieldTemplate ft = duplicateDocumentVersion.getFieldTemplate(ptft.getFieldTemplateId());
			if ( ft != null ) {
				fieldTemplateIdList.add(ptft.getFieldTemplateId());
			}
		}
		item.addItemProperty("selectedFieldTemplateIds", new ObjectProperty(fieldTemplateIdList,java.util.Set.class));

		if ( allRequiredFieldsPartyCanUpdate == null )
			allRequiredFieldsPartyCanUpdate = new LinkedList<FieldTemplate>();
		else
			allRequiredFieldsPartyCanUpdate.clear();
		HashSet<EsfUUID> notRequiredFieldTemplateIdList = new HashSet<EsfUUID>();
		for( PartyTemplateFieldTemplate ptft : partyTemplate.getPartyTemplateFieldTemplates() )	{
			FieldTemplate ft = duplicateDocumentVersion.getFieldTemplate(ptft.getFieldTemplateId());
			if ( ft != null && ft.isRequired() ) {
				allRequiredFieldsPartyCanUpdate.add(ft);
				if ( ptft.isOverrideAsOptional() ) { // only put those who have turned required off in our list
					notRequiredFieldTemplateIdList.add(ptft.getFieldTemplateId());
				}
			}
		}
		item.addItemProperty("selectedNotRequiredFieldTemplateIds", new ObjectProperty(notRequiredFieldTemplateIdList,java.util.Set.class));

		setItemDataSource(item);
		
		id.setValue( vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.label.id",partyTemplate.getId()) );
		
		TextField esfname = (TextField)getField("esfName");
		if ( esfnameValidator != null ) esfname.removeValidator(esfnameValidator);
		esfnameValidator = new LibraryPartyEsfNameValidator(duplicateDocumentVersion.getPartyTemplateList(),partyTemplate);
		esfname.addValidator(esfnameValidator);
		if ( partyTemplate.doInsert() ) {
			esfname.selectAll();
			esfname.focus();
		}
	}
	
	public void setParentWindow(ConfirmDiscardFormChangesWindow parentWindow) {
		this.parentWindow = parentWindow;
	}

	@Override
	public void initView() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	
    	// Setup layout
    	layout = new GridLayout(2,5);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.5f);
    	layout.setColumnExpandRatio(1, 0.5f);
    	setLayout(layout);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id,0,4,1,4); // last row

    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(true);
    	footer.setMargin(true);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	createLikeButton = new Button(vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.button.createLike.tooltip"));
		footer.addComponent(createLikeButton);

    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
		footer.addComponent(deleteButton);

		setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 8643182387654652179L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				
                if (propertyId.equals("libraryPartyTemplateId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.libraryPartyTemplateId.label"));
                	select.setStyleName("caution");
                	select.setNullSelectionAllowed(true);
                	select.addValidator(new SelectValidator(select));
                	select.setImmediate(true);
                	select.setRequired(false);
                	select.setMultiSelect(false);
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	for( PartyTemplate libParty : libraryPartyTemplateList ) {
                		select.addItem(libParty.getId());
                		select.setItemCaption( libParty.getId(), libParty.getEsfName().toString() );
                	}
                	select.addValueChangeListener( new Property.ValueChangeListener() {
						private static final long serialVersionUID = 37460594001719323L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							String selectedLibraryPartyTemplateId = event.getProperty().getValue().toString();
							if ( selectedLibraryPartyTemplateId != null ) {
								EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
								EsfUUID id = new EsfUUID(selectedLibraryPartyTemplateId);
				               	for( PartyTemplate libParty : libraryPartyTemplateList ) {
			                		if ( libParty.getId().equals(id) ) {
			                			thisForm.getField("esfName").setValue(libParty.getEsfName().toString());
			                			thisForm.getField("displayName").setValue(libParty.getDisplayName().toString());
										vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.libraryPartyTemplateId.message",libParty.getEsfName()));
			                			break;
			                		}
			                	}
							}
						}
                	});
                	return select;
                }
                
                if (propertyId.equals("selectedFieldTemplateIds")) {
                	TwinColSelect select = new TwinColSelect();
                	select.setLeftColumnCaption(vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.selectedFieldTemplateIds.label.left"));
                	select.setRightColumnCaption(vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.selectedFieldTemplateIds.label.right"));
                	select.setWidth(100, Unit.PERCENTAGE);

                	select.setNullSelectionAllowed(true); 
                	select.addValidator(new SelectValidator(select));
                	select.setImmediate(true);
                	select.setMultiSelect(true);
                	select.setRows(Math.min(15,duplicateDocumentVersion._getFieldTemplateMapInternal().size()));
                	select.setRequired(false);
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	for( EsfName fieldName : duplicateDocumentVersion._getFieldTemplateMapInternal().keySet() ) {
                		FieldTemplate ft = duplicateDocumentVersion._getFieldTemplateMapInternal().get(fieldName);
                		if ( ft.isTypeRadioButton() ) // permission to update is on the radio button group, not the individual radio buttons
                			continue;
                		select.addItem(ft.getId());
                		select.setItemCaption( ft.getId(), ft.getEsfName().toString() );
                	}
                	return select;
                }
                
                if (propertyId.equals("selectedNotRequiredFieldTemplateIds")) {
                	TwinColSelect select = new TwinColSelect();
                	select.setLeftColumnCaption(vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.selectedNotRequiredFieldTemplateIds.label.left"));
                	select.setRightColumnCaption(vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.selectedNotRequiredFieldTemplateIds.label.right"));
                	select.setWidth(100, Unit.PERCENTAGE);

                	select.setNullSelectionAllowed(true); 
                	select.addValidator(new SelectValidator(select));
                	select.setImmediate(true);
                	select.setMultiSelect(true);
                	select.setRows(Math.min(15,allRequiredFieldsPartyCanUpdate.size()));
                	select.setRequired(false);
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	
                	for( FieldTemplate ft : allRequiredFieldsPartyCanUpdate ) {
                		select.addItem(ft.getId());
                		select.setItemCaption( ft.getId(), ft.getEsfName().toString() );
                	}
                	return select;
                }
                
                Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("esfName")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setCaption(vaadinUi.getMsg("caption.esfName"));
                	tf.setDescription(vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.esfname.tooltip"));
                	tf.setConversionError(vaadinUi.getMsg("validator.esfname.message"));
                	return tf;
                }
                
                if (propertyId.equals("displayName")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setCaption(vaadinUi.getMsg("caption.displayName"));
                	tf.setDescription(vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.displayName.tooltip"));
                	return tf;
                }
                
                return field;
    	    }
    	 });

    	_logger.debug("Form created");
	}

	@Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("esfName")) {
        	layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("libraryPartyTemplateId")) {
        	layout.addComponent(field, 1, 0);
        } else if (propertyId.equals("displayName")) {
        	layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("selectedFieldTemplateIds")) {
        	layout.addComponent(field, 0, 2, 1, 2);
        } else if (propertyId.equals("selectedNotRequiredFieldTemplateIds")) {
        	layout.addComponent(field, 0, 3, 1, 3);
        }
    }


	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}