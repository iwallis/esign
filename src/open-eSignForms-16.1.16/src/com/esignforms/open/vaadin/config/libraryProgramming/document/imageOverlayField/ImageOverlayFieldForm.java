// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document.imageOverlayField;

import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.ImageVersionOverlayField;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.validator.ShortRangeValidator;
import com.esignforms.open.vaadin.widget.NativeSelectDropDown;
import com.vaadin.data.Buffered;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.VerticalLayout;

/**
 * @author Yozons Inc.
 */
public class ImageOverlayFieldForm extends CustomComponent implements EsfView, ClickListener {
	private static final long serialVersionUID = -4493532580400497953L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ImageOverlayFieldForm.class);
	
	final ImageOverlayFieldView view;
	final ImageOverlayFieldContainer container;
	
	final protected FieldGroup fieldGroup;
	VerticalLayout layout;

	HorizontalLayout buttonLayout;
	protected Button okButton;
	protected Button cancelButton;
	protected Button closeButton;

	protected TextField fieldTemplateName;
	
	public ImageOverlayFieldForm(ImageOverlayFieldView view, final ImageOverlayFieldContainer container) {
		setStyleName("ImageOverlayFieldForm");
		this.view = view;
		this.container = container;
		this.fieldGroup = new FieldGroup();
		initView();
	}
	
	@Override
	public void buttonClick(ClickEvent event) {
		ImageVersionOverlayField currBean = getCurrentBean();
    	if ( currBean == null ) return;
    	
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	Button source = event.getButton();
    	if ( source == okButton ) {
    		if ( fieldGroup.getItemDataSource() == null ) {
    			view.closeParentWindow();
    			return;
    		} 
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
        	
        	try {
    			commit();
    			view.select(getCurrentBean()); // reselect our bean so all updates cleanly
        	} catch (Exception e) {
        		_logger.error("buttonClick() - ok", e);
        		vaadinUi.showPlatformError(e.getMessage());
        	}
    	} else if ( source == cancelButton ) {
    		fieldGroup.discard();
    		if ( fieldGroup.getItemDataSource() == null ) {
    			view.closeParentWindow();
    		} else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			//setItemDataSource(null);
    			view.unselectAll();
    		}
    	} else if ( source == closeButton ) {
    		discard();
    		view.closeParentWindow();
    	}
    }
	    
    public String getCurrentBeanName() {
    	ImageVersionOverlayField currBean = getCurrentBean();
    	if ( currBean == null )
    		return "(None)";
    	FieldTemplate ft = view.documentVersion.getFieldTemplate(currBean.getFieldTemplateId());
		return ft == null ? "(None)" : ft.getEsfName().toString();
	}
    
    ImageVersionOverlayField getCurrentBean() {
    	return getBean(fieldGroup.getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public ImageVersionOverlayField getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<ImageVersionOverlayField> bi = (BeanItem<ImageVersionOverlayField>)dataSource;
		return bi.getBean();
    }
    
    public void setItemDataSource(Item newDataSource) {
    	if (newDataSource != null) {
    		ImageVersionOverlayField bean = getBean(newDataSource);
    		
    		fieldGroup.setItemDataSource(newDataSource);
    		
    		setupForm(bean);
    		setReadOnly(isReadOnly());
    		
    		layout.setVisible(true);
    		buttonLayout.setVisible(true);
    	} else {
    		fieldGroup.setItemDataSource(null);
    		layout.setVisible(false);
    		buttonLayout.setVisible(false);
    	}
    }
	

    @Override
    public void setReadOnly(boolean readOnly) {	
    	super.setReadOnly(readOnly);
    	
    	if ( getCurrentBean() != null )
    		fieldGroup.setReadOnly(readOnly);
    	
    	okButton.setVisible(!readOnly);
    	cancelButton.setVisible(!readOnly);
    	closeButton.setVisible(true);
    }
    
    void setupForm(ImageVersionOverlayField bean) {
    	FieldTemplate ft = view.documentVersion.getFieldTemplate(bean.getFieldTemplateId());
    	
    	fieldTemplateName.setReadOnly(false);
    	fieldTemplateName.setValue( ft==null ? "???" : ft.getEsfName().toString() );
    	fieldTemplateName.setReadOnly(true);
    	
    	// There's no width/height setting for radio buttons and checkboxes
    	boolean showWidthHeight = ! ft.isTypeRadioButton() && ! ft.isTypeCheckbox();
		Field<?> f = fieldGroup.getField("positionWidth");
		if ( f != null ) 
			f.setVisible(showWidthHeight);
		f = fieldGroup.getField("positionHeight");
		if ( f != null ) 
			f.setVisible(showWidthHeight);
    }

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	fieldGroup.setBuffered(true);

    	layout = new VerticalLayout();
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setVisible(false);
    	setCompositionRoot(layout);
		
    	HorizontalLayout line1 = new HorizontalLayout();
    	line1.setMargin(false);
    	line1.setSpacing(true);
    	layout.addComponent(line1);
    	
    	fieldTemplateName = new TextField(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldForm.fieldTemplateName.label"));
    	line1.addComponent(fieldTemplateName);

    	NativeSelect displayMode = new NativeSelect(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldForm.displayMode.label"));
    	displayMode.setImmediate(false);
    	displayMode.setDescription(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldForm.displayMode.tooltip"));
    	displayMode.setNullSelectionAllowed(false); 
    	displayMode.setRequired(true);
    	displayMode.setMultiSelect(false);
    	displayMode.addValidator(new SelectValidator(displayMode));
    	displayMode.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	displayMode.addItem(ImageVersionOverlayField.DISPLAY_MODE_FIELD);
    	displayMode.setItemCaption(ImageVersionOverlayField.DISPLAY_MODE_FIELD, vaadinUi.getPrettyCode().imageOverlayDisplayMode(ImageVersionOverlayField.DISPLAY_MODE_FIELD));
    	displayMode.addItem(ImageVersionOverlayField.DISPLAY_MODE_FIELD_LABEL);
    	displayMode.setItemCaption(ImageVersionOverlayField.DISPLAY_MODE_FIELD_LABEL, vaadinUi.getPrettyCode().imageOverlayDisplayMode(ImageVersionOverlayField.DISPLAY_MODE_FIELD_LABEL));
    	displayMode.addItem(ImageVersionOverlayField.DISPLAY_MODE_OUT);
    	displayMode.setItemCaption(ImageVersionOverlayField.DISPLAY_MODE_OUT, vaadinUi.getPrettyCode().imageOverlayDisplayMode(ImageVersionOverlayField.DISPLAY_MODE_OUT));
		line1.addComponent(displayMode);
		fieldGroup.bind(displayMode, "displayMode");
		
		NativeSelectDropDown backgroundColor = new NativeSelectDropDown(vaadinUi.getMsg("style.backgroundcolor.label"), vaadinUi.getEsfapp().getDropDownBackgroundColorEsfName(), false);
		backgroundColor.setDescription(vaadinUi.getMsg("style.backgroundcolor.tooltip"));
		backgroundColor.addValidator(new SelectValidator(backgroundColor));
		backgroundColor.setImmediate(true);
		backgroundColor.setRequired(true);
		line1.addComponent(backgroundColor);
		fieldGroup.bind(backgroundColor, "backgroundColor");

    	HorizontalLayout line2 = new HorizontalLayout();
    	line2.setMargin(false);
    	line2.setSpacing(true);
    	layout.addComponent(line2);

		TextField positionLeft = new TextField(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldForm.positionLeft.label"));
		positionLeft.setDescription(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldForm.positionLeft.tooltip"));
		positionLeft.setRequired(true);
		positionLeft.setRequiredError(vaadinUi.getMsg("tooltip.required"));
		positionLeft.setColumns(4);
		positionLeft.setMaxLength(4);
		positionLeft.setNullRepresentation("0");
		positionLeft.setConversionError(vaadinUi.getMsg("tooltip.invalid.integer.range","0-9999"));
   		positionLeft.addValidator(new ShortRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","0-9999"),(short)0,(short)9999));
		positionLeft.setImmediate(true);
    	line2.addComponent(positionLeft);
    	fieldGroup.bind(positionLeft, "positionLeft");
    	
		TextField positionTop = new TextField(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldForm.positionTop.label"));
		positionTop.setDescription(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldForm.positionTop.tooltip"));
		positionTop.setRequired(true);
		positionTop.setRequiredError(vaadinUi.getMsg("tooltip.required"));
		positionTop.setColumns(4);
		positionTop.setMaxLength(4);
		positionTop.setNullRepresentation("0");
		positionTop.setConversionError(vaadinUi.getMsg("tooltip.invalid.integer.range","0-9999"));
		positionTop.addValidator(new ShortRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","0-9999"),(short)0,(short)9999));
		positionTop.setImmediate(true);
    	line2.addComponent(positionTop);
    	fieldGroup.bind(positionTop, "positionTop");
    	
		TextField positionWidth = new TextField(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldForm.positionWidth.label"));
		positionWidth.setDescription(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldForm.positionWidth.tooltip"));
		positionWidth.setRequired(true);
		positionWidth.setRequiredError(vaadinUi.getMsg("tooltip.required"));
		positionWidth.setColumns(4);
		positionWidth.setMaxLength(4);
		positionWidth.setNullRepresentation("0");
		positionWidth.setConversionError(vaadinUi.getMsg("tooltip.invalid.integer.range","1-9999"));
		positionWidth.addValidator(new ShortRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","1-9999"),(short)1,(short)9999));
		positionWidth.setImmediate(true);
    	line2.addComponent(positionWidth);
    	fieldGroup.bind(positionWidth, "positionWidth");
    	
		TextField positionHeight = new TextField(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldForm.positionHeight.label"));
		positionHeight.setDescription(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldForm.positionHeight.tooltip"));
		positionHeight.setRequired(true);
		positionHeight.setRequiredError(vaadinUi.getMsg("tooltip.required"));
		positionHeight.setColumns(4);
		positionHeight.setMaxLength(4);
		positionHeight.setNullRepresentation("0");
		positionHeight.setConversionError(vaadinUi.getMsg("tooltip.invalid.integer.range","1-9999"));
		positionHeight.addValidator(new ShortRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","1-9999"),(short)1,(short)9999));
		positionHeight.setImmediate(true);
    	line2.addComponent(positionHeight);
    	fieldGroup.bind(positionHeight, "positionHeight");
    	
		buttonLayout = new HorizontalLayout();
		buttonLayout.setStyleName("footer");
		buttonLayout.setSpacing(false);
		buttonLayout.setMargin(false);
		layout.addComponent(buttonLayout);
    	
    	okButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
    	okButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
    	okButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	buttonLayout.addComponent(okButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	buttonLayout.addComponent(cancelButton);

    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	buttonLayout.addComponent(closeButton);

    	setReadOnly(true);   	
    	
    	_logger.debug("initView() - Form created");
	}

    
    boolean isValid() {
    	return fieldGroup.isValid();
    }
    
    public void commit() throws FieldGroup.CommitException {
    	fieldGroup.commit();
    }

    public void discard() throws Buffered.SourceException {
    	fieldGroup.discard();
    }

	@Override
	public String checkDirty() { // OBSOLETE SINCE VERSION FORM HAS NO BUTTONS FOR SAVING, CANCELING
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() { 
		return fieldGroup.isModified();
	}
}