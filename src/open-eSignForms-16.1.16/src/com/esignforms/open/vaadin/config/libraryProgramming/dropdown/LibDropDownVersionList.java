// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.dropdown;

import java.text.DecimalFormat;

import com.esignforms.open.prog.DropDownInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class LibDropDownVersionList extends Panel {
	private static final long serialVersionUID = -5890529624398234890L;

	final LibDropDownAndVersionsMainView view;
	LibDropDownVersionTable table;
	
	Button createNewButton;
	
	DecimalFormat versionFormat = new DecimalFormat("#,###,###,###");
	

	public LibDropDownVersionList(final LibDropDownAndVersionsMainView view, final LibDropDownVersionBeanContainer container) {
		super();
		this.view = view;
		setStyleName("LibDropDownVersionList");
		setSizeFull();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
    	
		HorizontalLayout searchBar = new HorizontalLayout();
    	searchBar.addStyleName("searchBar");
    	searchBar.setSpacing(true);
    	searchBar.setMargin(false);
    	if ( view.hasPermCreateLike() ) {
	    	createNewButton = new Button(vaadinUi.getMsg("LibDropDownVersionList.searchBar.createNewButton.label"));
	    	createNewButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDropDownVersionList.searchBar.createNewButton.icon")));
	    	createNewButton.setDescription(vaadinUi.getMsg("LibDropDownVersionList.searchBar.createNewButton.tooltip"));
	    	createNewButton.setStyleName("createNewButton");
	    	createNewButton.addStyleName(Reindeer.BUTTON_SMALL);
	    	createNewButton.addClickListener( new Button.ClickListener() {			
				private static final long serialVersionUID = -1839074625862471719L;

				@Override
				public void buttonClick(ClickEvent event) {
					view.createNewDropDown();
				}
			});
	    	searchBar.addComponent(createNewButton);
    	}
    	layout.addComponent(searchBar);
    	
    	table = new LibDropDownVersionTable(view, container);
    	
    	layout.addComponent(table);
    	layout.setExpandRatio(table, 1);
	}
	
	public Table getTable() {
		return table;
	}
	
	protected BeanItem<LibDropDownBean> createNewBeanItem()
	{
		DropDownInfo dropdown = DropDownInfo.Manager.createNew(view.getLibrary().getId());
		LibDropDownBean bean = new LibDropDownBean(dropdown);
		return new BeanItem<LibDropDownBean>(bean);
	}
	
	class LibDropDownVersionTable extends Table {
		private static final long serialVersionUID = 1522798336761065475L;

		public LibDropDownVersionTable(final LibDropDownAndVersionsMainView view, LibDropDownVersionBeanContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns(vaadinUi.getStringArray("LibDropDownVersionList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("LibDropDownVersionList.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setColumnAlignment("version",Align.CENTER);
			setCellStyleGenerator(new Table.CellStyleGenerator( ) {
				private static final long serialVersionUID = 1347547512341191271L;

				@Override
				public String getStyle(Table table, Object itemId, Object propertyId) {
					LibDropDownBean dropdownBean = view.getLibDropDownBean();
					return dropdownBean == null || dropdownBean.dropdown().isEnabled() ? null : "disabledText";
				}
			});
	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener((Property.ValueChangeListener)view);
	        /* We don't want to allow users to de-select a row */
	        setNullSelectionAllowed(false);
			setSizeFull();
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			if ( "lastUpdatedTimestamp".equals(colId) ) {
				return ((LibDropDownVersionBean)rowId).formatLastUpdatedTimestamp();
			} else if ( "version".equals(colId) ) {
				int version = ((LibDropDownVersionBean)rowId).dropdownVerInfo().getVersion();
				return versionFormat.format(version);
			}
			
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // LibDropDownVersionTable

} // LibDropDownVersionList
