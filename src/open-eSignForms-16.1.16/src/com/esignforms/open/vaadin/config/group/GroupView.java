// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.group;

import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.event.Action;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

public class GroupView extends VerticalSplitPanel implements EsfView, Property.ValueChangeListener, Action.Handler {
	private static final long serialVersionUID = 8878189456721223345L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(GroupView.class);

    final GroupView thisView;
	GroupViewContainer container;
	GroupList list;
	GroupForm form;
	
    private Action actionOpenWindow;
    private Action[] viewActions;


	public GroupView() {
		super();
		thisView = this;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			container = new GroupViewContainer();
			
			list = new GroupList(this,container);
		    actionOpenWindow = new Action(vaadinUi.getMsg("GroupList.action.view"));
		    viewActions = new Action[] { actionOpenWindow };
		    list.addActionHandler(this);

			form = new GroupForm(this,container);

			setFirstComponent(list);
			setSecondComponent(form);
			setSplitPosition(40);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Group view exception", e.getMessage());
		}
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our group list or not
        if (property == list) {
        	final Item item = list.getItem(list.getValue());
        	
    		if ( form.isDirty() ) { 
    			if ( form.getCurrentBean() == form.getBean(item) ) {
    				// We've selected the one that's been modified, so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("GroupView.ConfirmDiscardChangesDialog.message", form.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						form.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						list.select(form.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("GroupView.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
                form.setItemDataSource(item);
    		}
        }
        else
        	_logger.debug("valueChange() - no list event: " + event.toString());
	}
	
	@Override
	public Action[] getActions(Object target, Object sender) {
		return viewActions;
	}

	@Override
	public void handleAction(Action action, Object sender, Object target) {
   	 	if ( action == actionOpenWindow ) {
   			_logger.debug("handleAction ACTION_OPEN_WINDOW - item: " + ((GroupBean)target).getPathName());

            final GroupForm form = new GroupForm(this,container);
            
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(((GroupBean)target).getPathName().toPlainString(), form);
   	 		Item item = list.getItem(target);
            form.setItemDataSource(item);
            vaadinUi.addWindowToUI(w);
   	 	}
	}

	@Override
	public void detach() {
		for( Object o : list.getVisibleColumns() ) {
			_logger.debug("detach() - groupList visible columns[]: " + o); // These are string property ids of the column names like esfName, description, status, lastUpdatedTimestamp
		}
		super.detach();
	}
	
	public void select(GroupBean bean) {
		unselectAll();
		list.select(bean);
		list.setCurrentPageFirstItemId(bean);
	}
	
	public void unselectAll() {
		list.setValue(null);
	}
	
	public GroupForm getForm() {
		return form;
	}

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return form.checkDirty();
	}

	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return form.isDirty();
	}
}