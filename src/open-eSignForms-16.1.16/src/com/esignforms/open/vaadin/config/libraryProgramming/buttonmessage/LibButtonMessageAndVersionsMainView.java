// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.buttonmessage;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.ButtonMessage;
import com.esignforms.open.prog.ButtonMessageInfo;
import com.esignforms.open.prog.ButtonMessageVersion;
import com.esignforms.open.prog.ButtonMessageVersionInfo;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

/**
 * LibButtonMessageAndVersionsMainView is a splitpanel that contains the buttonmessage view in the left and the buttonmessage version view on the right.
 * 
 * Each of those views is itself a splitpanel, containing a table/list on the top to select items, with a form on the bottom to view/edit.
 * 
 * When a new buttonmessage is selected in the left view, we propagate that to the right view so it can sync the versions with the selected buttonmessage.
 * 
 * @author Yozons
 *
 */
public class LibButtonMessageAndVersionsMainView extends HorizontalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = -7319824513782547126L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibButtonMessageAndVersionsMainView.class);

    EsfUUID libraryId;
    Library library;
    final LibButtonMessageAndVersionsMainView thisView;
	
    VerticalSplitPanel buttonmessageSplitPanel;
    LibButtonMessageBeanContainer buttonmessageContainer;
    LibButtonMessageList buttonmessageList;
    LibButtonMessageForm buttonmessageForm;
	
    VerticalSplitPanel buttonmessageVerSplitPanel;
    LibButtonMessageVersionBeanContainer buttonmessageVerContainer;
	LibButtonMessageVersionList buttonmessageVerList;
	LibButtonMessageVersionForm buttonmessageVerForm;

	public LibButtonMessageAndVersionsMainView() {
		super(); 
		thisView = this;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			library = Library.Manager.getById(libraryId, vaadinUi.getUser());
			
			buttonmessageContainer = new LibButtonMessageBeanContainer(library);
			buttonmessageVerContainer = new LibButtonMessageVersionBeanContainer(this); // Load contents when a buttonmessage is selected from our buttonmessageContainer
			
			buttonmessageList = new LibButtonMessageList(this,buttonmessageContainer);
		    buttonmessageForm = new LibButtonMessageForm(this,buttonmessageContainer);

			buttonmessageSplitPanel = new VerticalSplitPanel();
			buttonmessageSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			buttonmessageSplitPanel.setSplitPosition(30);
			buttonmessageSplitPanel.setFirstComponent(buttonmessageList);
			buttonmessageSplitPanel.setSecondComponent(buttonmessageForm);
		    
			buttonmessageVerList = new LibButtonMessageVersionList(this,buttonmessageVerContainer);
			buttonmessageVerForm = new LibButtonMessageVersionForm(this,buttonmessageVerContainer);
			
			buttonmessageVerSplitPanel = new VerticalSplitPanel();
			buttonmessageVerSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			buttonmessageVerSplitPanel.setSplitPosition(30);
			buttonmessageVerSplitPanel.setFirstComponent(buttonmessageVerList);
			buttonmessageVerSplitPanel.setSecondComponent(buttonmessageVerForm);

			setFirstComponent(buttonmessageSplitPanel);
			setSecondComponent(buttonmessageVerSplitPanel);
			setSplitPosition(45);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("ButtonMessage and versions main view exception", e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	private LibButtonMessageBean getLibButtonMessageBean(Item buttonmessageListItem) {
    	if ( buttonmessageListItem == null )
    		return null;
		BeanItem<LibButtonMessageBean> bi = (BeanItem<LibButtonMessageBean>)buttonmessageListItem;
		return bi.getBean();
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our buttonmessage list or not
        if (property == buttonmessageList.getTable()) {
        	final Item item = buttonmessageList.getTable().getItem(buttonmessageList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( buttonmessageForm.getCurrentBean() == buttonmessageForm.getBean(item) ) {
    				// We've selected the same bean so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibButtonMessageAndVersionsMainView.ButtonMessage.ConfirmDiscardChangesDialog.message", buttonmessageForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						thisView.discardForms();
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						buttonmessageList.getTable().select(buttonmessageForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibButtonMessageAndVersionsMainView.ButtonMessage.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	LibButtonMessageBean bean = getLibButtonMessageBean(item);
                buttonmessageForm.setItemDataSource(item);
            	if ( bean != null ) {
                	buttonmessageVerContainer.reload(bean.buttonmessage());
                	selectFirstButtonMessageVersion();
            	} else {
            		buttonmessageVerForm.setItemDataSource(null);
            		buttonmessageVerContainer.removeAllItems();
            	}
    		}
        } else if (property == buttonmessageVerList.getTable()) {
        	final Item item = buttonmessageVerList.getTable().getItem(buttonmessageVerList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( buttonmessageVerForm.getCurrentBean() == buttonmessageVerForm.getBean(item) ) {
    				// We've selected the same bean (version) so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibButtonMessageAndVersionsMainView.ButtonMessage.ConfirmDiscardChangesDialog.message", buttonmessageForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						buttonmessageVerForm.discard();
						buttonmessageVerForm.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						buttonmessageVerList.getTable().select(buttonmessageVerForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibButtonMessageAndVersionsMainView.ButtonMessage.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	buttonmessageVerForm.setItemDataSource(item);
    		}
        }
        else {
        	_logger.debug("valueChange() - no list event: " + event.toString());
        }
	}
	

	@Override
	public void detach() {
		/*
		for( Object o : list.getTable().getVisibleColumns() ) {
			_logger.debug("detach() - list visible columns[]: " + o); // These are string property ids of the column names like email, status, lastLoginTimestamp
		}
		*/
		super.detach();
	}
	
	public void selectButtonMessage(LibButtonMessageBean bean) {
		unselectAllButtonMessages();
		buttonmessageList.getTable().select(bean);
		buttonmessageList.getTable().setCurrentPageFirstItemId(bean);
		if ( bean == null ) {
			buttonmessageVerForm.setItemDataSource(null);
		}
	}
	
	public void unselectAllButtonMessages() {
		buttonmessageList.getTable().setValue(null);
		buttonmessageForm.setItemDataSource(null);
	}
	
	public void selectButtonMessageVersion(LibButtonMessageVersionBean bean) {
		unselectAllButtonMessageVersions();
		buttonmessageVerList.getTable().select(bean);
		buttonmessageVerList.getTable().setCurrentPageFirstItemId(bean);
	}

	public void discardForms() {
		buttonmessageForm.discard();
		buttonmessageVerForm.discard();
		buttonmessageForm.setItemDataSource(null);
		buttonmessageVerForm.setItemDataSource(null);
	}

	
	public void selectFirstButtonMessageVersion() {
		if ( buttonmessageVerContainer.size() > 0 ) {
			selectButtonMessageVersion(buttonmessageVerContainer.getIdByIndex(0));
		} else {
			unselectAllButtonMessageVersions(); 
		}
	}
	
	public void unselectAllButtonMessageVersions() {
		buttonmessageVerList.getTable().setValue(null);
	}

	public Library getLibrary() {
		return library;
	}
	
	public LibButtonMessageBean getLibButtonMessageBean() {
		return buttonmessageForm == null ? null : buttonmessageForm.getCurrentBean();
	}
	
	public LibButtonMessageVersionBean getLibButtonMessageVersionBean() {
		return buttonmessageVerForm == null ? null : buttonmessageVerForm.getCurrentBean();
	}
	
	public String getEsfNameWithVersion() {
		LibButtonMessageBean buttonmessageBean = getLibButtonMessageBean();
		LibButtonMessageVersionBean buttonmessageVerBean = getLibButtonMessageVersionBean();
		return buttonmessageVerBean == null ? "?? [?]" : buttonmessageBean.getEsfName() + " [" + buttonmessageVerBean.getVersion() + "]";
	}
	
	public Integer getProductionVersion() {
		LibButtonMessageBean buttonmessageBean = getLibButtonMessageBean();
		return buttonmessageBean == null ? 0 : buttonmessageBean.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		LibButtonMessageBean buttonmessageBean = getLibButtonMessageBean();
		return buttonmessageBean == null ? 0 : buttonmessageBean.getTestVersion();
	}

	public boolean hasTestVersion() {
		LibButtonMessageBean buttonmessageBean = getLibButtonMessageBean();
		return buttonmessageBean == null ? false : buttonmessageBean.buttonmessage().hasTestVersion();
	}
	
    public boolean isProductionVersion(ButtonMessageVersionInfo buttonmessageVer)
    {
    	LibButtonMessageBean buttonmessageBean = getLibButtonMessageBean();
    	return buttonmessageBean != null && buttonmessageVer != null && buttonmessageVer.getVersion() == buttonmessageBean.getProductionVersion();
    }

    public boolean isTestVersion(ButtonMessageVersionInfo buttonmessageVer)
    {
    	LibButtonMessageBean buttonmessageBean = getLibButtonMessageBean();
    	return buttonmessageBean != null && buttonmessageVer != null && buttonmessageVer.getVersion() > buttonmessageBean.getProductionVersion();
    }

    // "Current" here means latest production or test version.
    public boolean isCurrentVersion(ButtonMessageVersionInfo buttonmessageVer)
    {
    	LibButtonMessageBean buttonmessageBean = getLibButtonMessageBean();
    	return buttonmessageBean != null && buttonmessageVer != null && buttonmessageVer.getVersion() >= buttonmessageBean.getProductionVersion();
    }

    public boolean isOldVersion(ButtonMessageVersionInfo buttonmessageVer)
    {
    	LibButtonMessageBean buttonmessageBean = getLibButtonMessageBean();
    	return buttonmessageBean != null && buttonmessageVer != null && buttonmessageVer.getVersion() < buttonmessageBean.getProductionVersion();
    }
	
	public String getVersionLabel(ButtonMessageVersionInfo buttonmessageVer) {
		if ( isTestVersion(buttonmessageVer) )
			return Literals.VERSION_LABEL_TEST;
		if ( isProductionVersion(buttonmessageVer) )
			return Literals.VERSION_LABEL_PRODUCTION;
		return Literals.VERSION_LABEL_NOT_CURRENT;
	}
	
	public boolean hasPermViewDetails() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_DELETE);
	}
	
	public boolean cancelChanges() {
		buttonmessageForm.discard();
		buttonmessageVerForm.discard();
		buttonmessageForm.setReadOnly(true);
		buttonmessageVerForm.setReadOnly(true);
		return true;
	}
	
	public boolean enterEditMode() {
		buttonmessageForm.setReadOnly(false);
		buttonmessageVerForm.setReadOnly(false);
		return true;
	}
	
	public boolean save() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
		if ( ! buttonmessageForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		if ( ! buttonmessageVerForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		buttonmessageForm.commit();
		buttonmessageVerForm.commit();
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			// If we're saving a new test version, we'll want to bump our buttonmessage to show this as the new test version
			LibButtonMessageBean currButtonMessage = getLibButtonMessageBean();
			LibButtonMessageVersionBean currButtonMessageVer = getLibButtonMessageVersionBean();
			if ( currButtonMessageVer.buttonmessageVersion().getVersion() > currButtonMessage.buttonmessage().getTestVersion() )
				currButtonMessage.buttonmessage().bumpTestVersion();
			
			LibButtonMessageBean savedButtonMessage = buttonmessageForm.save(con);
			if ( savedButtonMessage == null )  {
				con.rollback();
				return false;
			}
			
			LibButtonMessageVersionBean savedDocVer = buttonmessageVerForm.save(con);
			if ( savedDocVer == null )  {
				con.rollback();
				return false;
			}
			
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Saved buttonmessage " + savedButtonMessage.getEsfName() + " version " + savedDocVer.getVersion() + ". ButtonMessageVersionId: " + savedDocVer.getId() + "; status: " + savedButtonMessage.getStatus());
			}

			con.commit();
	    		
			buttonmessageContainer.refresh();  // refresh our list after a change
			selectButtonMessage(savedButtonMessage);
			selectButtonMessageVersion(savedDocVer);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean createNewButtonMessage() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ButtonMessageInfo buttonmessageInfo = ButtonMessageInfo.Manager.createNew(getLibrary());
		LibButtonMessageBean buttonmessageBean = new LibButtonMessageBean(buttonmessageInfo);
		BeanItem<LibButtonMessageBean> buttonmessageItem = new BeanItem<LibButtonMessageBean>(buttonmessageBean);
        buttonmessageForm.setItemDataSource(buttonmessageItem);
        
        // Create new like the default button message set so users have some data to start with. These should always be present.
        ButtonMessage defaultBM = ButtonMessage.Manager.getDefault();
        ButtonMessageVersion defaultBMV = defaultBM == null ? null : defaultBM.getProductionButtonMessageVersion();
		
		ButtonMessageVersionInfo buttonmessageVerInfo = defaultBMV == null ? ButtonMessageVersionInfo.Manager.createNew(ButtonMessage.Manager.getById(buttonmessageInfo.getId()),vaadinUi.getUser()) : ButtonMessageVersionInfo.Manager.createLike(ButtonMessage.Manager.getById(buttonmessageInfo.getId()),defaultBMV,vaadinUi.getUser());
		LibButtonMessageVersionBean buttonmessageVerBean = new LibButtonMessageVersionBean(buttonmessageVerInfo,getVersionLabel(buttonmessageVerInfo));
		BeanItem<LibButtonMessageVersionBean> buttonmessageVerItem = new BeanItem<LibButtonMessageVersionBean>(buttonmessageVerBean);
        buttonmessageVerForm.setItemDataSource(buttonmessageVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean createLikeButtonMessage() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibButtonMessageBean likeDocBean = getLibButtonMessageBean();
		LibButtonMessageVersionBean likeDocVerBean = getLibButtonMessageVersionBean();
		
    	LibButtonMessageBean createdDocBean = likeDocBean.createLike();
		BeanItem<LibButtonMessageBean> createdDocItem = new BeanItem<LibButtonMessageBean>(createdDocBean);
        buttonmessageForm.setItemDataSource(createdDocItem);

    	ButtonMessageVersionInfo createdButtonMessageVerInfo = ButtonMessageVersionInfo.Manager.createLike(ButtonMessage.Manager.getById(createdDocBean.buttonmessageInfo().getId()), likeDocVerBean.buttonmessageVersion(), vaadinUi.getUser());
    	LibButtonMessageVersionBean createdButtonMessageVerBean = new LibButtonMessageVersionBean(createdButtonMessageVerInfo,Literals.VERSION_LABEL_TEST); // The new buttonmessage and version will always be Test.
		BeanItem<LibButtonMessageVersionBean> createdButtonMessageVerItem = new BeanItem<LibButtonMessageVersionBean>(createdButtonMessageVerBean);       
        buttonmessageVerForm.setItemDataSource(createdButtonMessageVerItem);
        
        setReadOnly(false);
		return true;
	}

	public boolean createNextVersion() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibButtonMessageBean currButtonMessageBean = getLibButtonMessageBean();
		LibButtonMessageVersionBean currButtonMessageVerBean = getLibButtonMessageVersionBean();
		
        buttonmessageForm.setReadOnly(!hasPermUpdate()); // should have permission since doing a create next version...

    	ButtonMessageVersionInfo nextButtonMessageVerInfo = ButtonMessageVersionInfo.Manager.createLike(currButtonMessageBean.buttonmessage(), currButtonMessageVerBean.buttonmessageVersion(), vaadinUi.getUser());
    	LibButtonMessageVersionBean nextButtonMessageVerBean = new LibButtonMessageVersionBean(nextButtonMessageVerInfo,Literals.VERSION_LABEL_TEST); // The new version will always be Test.
		BeanItem<LibButtonMessageVersionBean> nextButtonMessageVerItem = new BeanItem<LibButtonMessageVersionBean>(nextButtonMessageVerBean);
        buttonmessageVerForm.setItemDataSource(nextButtonMessageVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean promoteTestToProductionVersion() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibButtonMessageVersionForm.versionChange.TestToProd.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibButtonMessageBean buttonmessageBean = getLibButtonMessageBean();
			LibButtonMessageVersionBean buttonmessageVerBean = getLibButtonMessageVersionBean();
			
			buttonmessageBean.buttonmessage().promoteTestVersionToProduction();
			
			if ( ! buttonmessageBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved buttonmessage " + buttonmessageBean.getEsfName() + " version " + buttonmessageVerBean.getVersion() + " into Production status. ButtonMessageVersionId: " + buttonmessageVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved buttonmessage " + buttonmessageBean.getEsfName() + " version " + buttonmessageVerBean.getVersion() + " into Production status. ButtonMessageVersionId: " + buttonmessageVerBean.getId());
			}
			
			con.commit();
			buttonmessageContainer.refresh();  // refresh our list after a change
			selectButtonMessage(buttonmessageBean);
			selectButtonMessageVersion(buttonmessageVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibButtonMessageVersionForm.versionChange.TestToProd.success.message",buttonmessageBean.getEsfName(),buttonmessageVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean revertProductionVersionBackToTest() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibButtonMessageVersionForm.versionChange.revertProdToTest.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibButtonMessageBean buttonmessageBean = getLibButtonMessageBean();
			LibButtonMessageVersionBean buttonmessageVerBean = getLibButtonMessageVersionBean();
			
			buttonmessageBean.buttonmessage().revertProductionVersionBackToTest();
			
			if ( ! buttonmessageBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved Production buttonmessage " + buttonmessageBean.getEsfName() + " version " + buttonmessageVerBean.getVersion() + " back into Test status. ButtonMessageVersionId: " + buttonmessageVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved Production buttonmessage " + buttonmessageBean.getEsfName() + " version " + buttonmessageVerBean.getVersion() + " back into Test status. ButtonMessageVersionId: " + buttonmessageVerBean.getId());
			}
			
			con.commit();
			buttonmessageContainer.refresh();  // refresh our list after a change
			selectButtonMessage(buttonmessageBean);
			selectButtonMessageVersion(buttonmessageVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibButtonMessageVersionForm.versionChange.revertProdToTest.success.message",buttonmessageBean.getEsfName(),buttonmessageVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean delete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibButtonMessageBean currButtonMessage = getLibButtonMessageBean();
			LibButtonMessageVersionBean currButtonMessageVer = getLibButtonMessageVersionBean();
			
			// If there is no Production version, we'll get rid of the button message entirely, otherwise just the test version
			Errors errors = new Errors();
			if ( currButtonMessage.getProductionVersion() == 0 )
				currButtonMessage.buttonmessage().delete(con,errors,vaadinUi.getUser());
			else {
				currButtonMessageVer.buttonmessageVersion().delete(con,errors,vaadinUi.getUser());
				currButtonMessage.dropTestVersion();
				currButtonMessage.save(con, errors);
			}
			
			if ( errors.hasError() ) {
				vaadinUi.show(errors);
				con.rollback();
				return false;
			}
			
			con.commit();
    		
			vaadinUi.showStatus(vaadinUi.getMsg("LibButtonMessageVersionForm.button.delete.successMessage",currButtonMessage.getEsfName(),currButtonMessageVer.getVersion()));
			
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Deleted buttonmessage " + currButtonMessage.getEsfName() + " version " + currButtonMessageVer.getVersion() + ". ButtonMessageVersionId: " + currButtonMessageVer.getId() + "; status: " + currButtonMessage.getStatus());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted buttonmessage " + currButtonMessage.getEsfName() + " version " + currButtonMessageVer.getVersion() + ". ButtonMessageVersionId: " + currButtonMessageVer.getId() + "; status: " + currButtonMessage.getStatus());
			}

			buttonmessageContainer.refresh();  // refresh our list after a change
			selectButtonMessage(null);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		_logger.debug("activateView() - params: " + params);
		libraryId = new EsfUUID(params);
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return buttonmessageForm.isDirty() ? buttonmessageForm.checkDirty() : buttonmessageVerForm.checkDirty();
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return buttonmessageForm.isDirty() || buttonmessageVerForm.isDirty();
	}
}