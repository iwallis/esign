// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.field;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.vaadin.hene.popupbutton.PopupButton;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.Library;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.LibraryFieldEsfNameValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.validator.ShortRangeValidator;
import com.esignforms.open.vaadin.widget.NativeSelectDropDown;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The LibFieldForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class LibFieldForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 2773886349839364024L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibFieldForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button editButton; // Not used for document versions

	LibFieldView view;
	Library library;
	final LibFieldForm thisForm;
	final Map<EsfName,FieldTemplate> libraryFieldTemplateMap; // fields defined in the library
	final Map<EsfName,FieldTemplate> fieldTemplateMap; // fields defined in our "container" 
	String currentType = ""; // let's us keep track of type changes so we can redo our forms to be type-specific 
	LibFieldBeanContainer container;
    GridLayout layout;
    
    int tabIndex = 1;
    int typeBeanTabIndex;
    HorizontalLayout alignRequiredSecurityButtonLayout;
    PopupButton securityButton;
    VerticalLayout securityButtonLayout;
    HorizontalLayout widthLayout;
    HorizontalLayout borderLayout;
    LibraryFieldEsfNameValidator esfnameValidator;
    
	Label id;
	
	// We store the prevBean in case we abandon working on the newBean when we 'create like'
	LibFieldBean prevBean;
    LibFieldBean newBean;
    
	@SuppressWarnings("deprecation")
	public LibFieldForm(final LibFieldView view, LibFieldBeanContainer container, 
						final Map<EsfName,FieldTemplate> libraryFieldTemplateMap, final Map<EsfName,FieldTemplate> fieldTemplateMap) {
		this.thisForm = this;
		setStyleName("LibFieldForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.view = view; // may be null if not a standard field editor (as done with the document version subclass)
    	this.container = container; // may be null if not a standard field editor (as done with the document version subclass)
    	this.libraryFieldTemplateMap = libraryFieldTemplateMap;
    	this.fieldTemplateMap = fieldTemplateMap;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
    	layout = new GridLayout(3,6);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.33f);
    	layout.setColumnExpandRatio(1, 0.34f);
    	layout.setColumnExpandRatio(2, 0.33f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
    	
    	alignRequiredSecurityButtonLayout = new HorizontalLayout();
    	alignRequiredSecurityButtonLayout.setMargin(false);
    	alignRequiredSecurityButtonLayout.setSpacing(true);
    	layout.addComponent(alignRequiredSecurityButtonLayout, 0, 1);
    	
        widthLayout = new HorizontalLayout();
        widthLayout.setMargin(false);
        widthLayout.setSpacing(true);
        layout.addComponent(widthLayout, 0, 2);
        
        borderLayout = new HorizontalLayout();
        borderLayout.setMargin(false);
        borderLayout.setSpacing(true);
    	layout.addComponent(borderLayout, 1, 2, 2, 2);
    	layout.setComponentAlignment(borderLayout, Alignment.BOTTOM_CENTER);

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id,0,5,2,5); // last row

		HorizontalLayout footer = new HorizontalLayout();
		footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);

    	// view == null when subclasses use this form in other ways
    	if ( view != null ) {
    		library = view.getLibrary();
    		if ( view.hasPermCreateLike() || view.hasPermUpdate() ) {
    	    	saveButton = new Button(vaadinUi.getMsg("button.save.label"), (ClickListener)this);
    	    	//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
    			//saveButton.setClickShortcut(KeyCode.ENTER);
    	    	saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
    	    	saveButton.setDescription(vaadinUi.getMsg("button.save.tooltip"));
        		footer.addComponent(saveButton);
    		}

        	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
        	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
        	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
        	footer.addComponent(cancelButton);

        	if ( view.hasPermUpdate() ) {
    	    	editButton = new Button(vaadinUi.getMsg("button.edit.label"), (ClickListener)this);
    	    	editButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibFieldForm.button.edit.icon")));
    	    	editButton.setDescription(vaadinUi.getMsg("button.edit.tooltip"));
        		footer.addComponent(editButton);
        	}

    		if ( view.hasPermCreateLike() ) {
    	    	createLikeButton = new Button(vaadinUi.getMsg("LibFieldForm.button.createLike.label"), (ClickListener)this);
    	    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibFieldForm.button.createLike.icon")));
    	    	createLikeButton.setDescription(vaadinUi.getMsg("LibFieldForm.button.createLike.tooltip"));
        		footer.addComponent(createLikeButton);
    		}
        	
        	if ( view.hasPermDelete() ) {
            	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
            	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibFieldForm.button.delete.icon")));
            	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
            	deleteButton.addStyleName("deleteButton");
            	deleteButton.addStyleName("caution");
        		footer.addComponent(deleteButton);
        	}
    	}

    	setFooter(footer);
    	setReadOnly(true);

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -6698715293466239449L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				if (propertyId.equals("typeBean")) {
					typeBeanTabIndex = tabIndex;
					TypeSubForm form = new TypeSubForm(library,thisForm,fieldTemplateMap,tabIndex++);
					return form;
				}

				if (propertyId.equals("libLabelBean")) {
					tabIndex += 4; // enough to skip by all typeBean indexes
					LibLabelForm form = new LibLabelForm(thisForm,tabIndex++);
					return form;
				}

                if (propertyId.equals("libraryFieldTemplateId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.libraryFieldTemplateId.label"));
                	select.setTabIndex(tabIndex++);
                	select.setStyleName("caution");
                	select.setNullSelectionAllowed(true); 
                	select.setImmediate(true);
                	select.setRequired(false);
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	for( EsfName fieldName : libraryFieldTemplateMap.keySet() ) {
                		FieldTemplate libField = libraryFieldTemplateMap.get(fieldName);
                		select.addItem(libField.getId());
                		select.setItemCaption( libField.getId(), libField.getEsfName().toString() );
                	}
                	select.addValueChangeListener( new Property.ValueChangeListener() {
						private static final long serialVersionUID = -5238041965372759221L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
							Object eventPropertyValue = event.getProperty().getValue();
							String selectedLibraryFieldTemplateId = eventPropertyValue == null ? "" : eventPropertyValue.toString();
							if ( selectedLibraryFieldTemplateId != null ) {
								EsfUUID id = new EsfUUID(selectedLibraryFieldTemplateId);
								for( EsfName fieldName : libraryFieldTemplateMap.keySet() ) {
			                		FieldTemplate libField = libraryFieldTemplateMap.get(fieldName);
			                		if ( libField.getId().equals(id) ) {
			                			thisForm.getField("esfName").setValue(libField.getEsfName().toString());
			                			thisForm.getField("type").setValue(libField.getType());
			                			thisForm.getField("required").setValue(libField.getRequired());
			                			thisForm.getField("autoComplete").setValue(libField.isAutoCompleteAllowed());
			                			thisForm.getField("maskInDataSnapshot").setValue(libField.isMaskInDataSnapshot());
			                			thisForm.getField("maskInput").setValue(libField.isMaskInput());
			                			thisForm.getField("cloneAllowed").setValue(libField.isCloneAllowed());
			                			thisForm.getField("minLength").setValue(Integer.toString(libField.getMinLength()));
			                			thisForm.getField("maxLength").setValue(Integer.toString(libField.getMaxLength()));
			                			thisForm.getField("width").setValue(Short.toString(libField.getWidth()));
			                			thisForm.getField("widthUnit").setValue(libField.getWidthUnit());
			                			thisForm.getField("ignoreWidthOnOutput").setValue(libField.isIgnoreWidthOnOutput());
			                			thisForm.getField("borderTop").setValue(libField.isBorderTop());
			                			thisForm.getField("borderBottom").setValue(libField.isBorderBottom());
			                			thisForm.getField("borderLeft").setValue(libField.isBorderLeft());
			                			thisForm.getField("borderRight").setValue(libField.isBorderRight());
			                			thisForm.getField("typeBean").setValue(new LibFieldBean.TypeBean(libField));
			                			thisForm.getField("libLabelBean").setValue(new LibLabelBean(libField.getLabelTemplate()));
										vaadinUi.showStatus(vaadinUi.getMsg("LibFieldForm.libraryFieldTemplateId.message",libField.getEsfName()));
										Field typeField = getField("type");
										String newType = (String)typeField.getValue();
										rebuildTypeForm(newType);
			                			break;
			                		}
			                	}
							}
						}
                		
                	});
                	return select;
                }
                
                if (propertyId.equals("type")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.type.label"));
                	select.setTabIndex(tabIndex++);
                	select.setNullSelectionAllowed(false); 
                	select.setImmediate(true);
                	select.setRequired(true);
                    select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                    
                    for( String fieldType : FieldTemplate.SELECT_LIST_OF_ALL_TYPES ) {
                    	select.addItem(fieldType);
                    	select.setItemCaption(fieldType, vaadinUi.getPrettyCode().fieldTemplateType(fieldType));
                    }
                    
                	select.addValueChangeListener( new Property.ValueChangeListener() {
						private static final long serialVersionUID = 7079644068957746261L;

						@Override
						public void valueChange(Property.ValueChangeEvent event) {
							String newType = (String)event.getProperty().getValue();
							Field<?> typeField = getField("type");
							if ( typeField == null ) return;
							rebuildTypeForm(newType);
						}
					});
                	return select;
                }
                
				if (propertyId.equals("textAlignCss")) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.textalign.label"), vaadinUi.getEsfapp().getDropDownTextAlignEsfName());
                	select.setTabIndex(tabIndex++);
	                select.setDescription(vaadinUi.getMsg("style.textalign.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}
				
				if ( propertyId.equals("required") ) {
					NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.required.label"));
					select.setNullSelectionAllowed(false);
					select.addItem(FieldTemplate.REQUIRED);
					select.setItemCaption(FieldTemplate.REQUIRED, vaadinUi.getMsg("LibFieldForm.required.select.required"));
					select.addItem(FieldTemplate.OPTIONAL);
					select.setItemCaption(FieldTemplate.OPTIONAL, vaadinUi.getMsg("LibFieldForm.required.select.optional"));
					select.addItem(FieldTemplate.OPTIONAL_STYLE_REQUIRED);
					select.setItemCaption(FieldTemplate.OPTIONAL_STYLE_REQUIRED, vaadinUi.getMsg("LibFieldForm.required.select.optionalStyleRequired"));
                	select.setTabIndex(tabIndex++);
	                select.setDescription(vaadinUi.getMsg("LibFieldForm.required.tooltip"));
	                return select;
				} 

				if ( propertyId.equals("autoComplete") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("LibFieldForm.autoComplete.label"));
                	cb.setTabIndex(tabIndex++);
	            	cb.setImmediate(false);
	                cb.setDescription(vaadinUi.getMsg("LibFieldForm.autoComplete.tooltip"));
	                return cb;
				} 

				if ( propertyId.equals("maskInDataSnapshot") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("LibFieldForm.maskInDataSnapshot.label"));
                	cb.setTabIndex(tabIndex++);
	            	cb.setImmediate(false);
	                cb.setDescription(vaadinUi.getMsg("LibFieldForm.maskInDataSnapshot.tooltip"));
	                return cb;
				} 

				if ( propertyId.equals("maskInput") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("LibFieldForm.maskInput.label"));
                	cb.setTabIndex(tabIndex++);
	            	cb.setImmediate(false);
	                cb.setDescription(vaadinUi.getMsg("LibFieldForm.maskInput.tooltip"));
	                return cb;
				} 

				if ( propertyId.equals("cloneAllowed") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("LibFieldForm.cloneAllowed.label"));
                	cb.setTabIndex(tabIndex++);
	            	cb.setImmediate(false);
	                cb.setDescription(vaadinUi.getMsg("LibFieldForm.cloneAllowed.tooltip"));
	                return cb;
				} 

                if (propertyId.equals("widthUnit")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.widthUnit.label"));
                	select.setTabIndex(tabIndex++);
                	select.setNullSelectionAllowed(false); 
                	select.setImmediate(false);
	                select.addValidator(new SelectValidator(select));
                	select.setRequired(true);
                    select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	select.addItem(FieldTemplate.WIDTH_UNIT_PERCENT);
                	select.addItem(FieldTemplate.WIDTH_UNIT_PIXELS);
                	select.addItem(FieldTemplate.WIDTH_UNIT_EM);
                	select.addItem(FieldTemplate.WIDTH_UNIT_EX);
                	select.addItem(FieldTemplate.WIDTH_UNIT_AUTO);
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	select.setItemCaption(FieldTemplate.WIDTH_UNIT_PERCENT, vaadinUi.getPrettyCode().fieldTemplateWidth(FieldTemplate.WIDTH_UNIT_PERCENT));
                	select.setItemCaption(FieldTemplate.WIDTH_UNIT_PIXELS, vaadinUi.getPrettyCode().fieldTemplateWidth(FieldTemplate.WIDTH_UNIT_PIXELS));
                	select.setItemCaption(FieldTemplate.WIDTH_UNIT_EM, vaadinUi.getPrettyCode().fieldTemplateWidth(FieldTemplate.WIDTH_UNIT_EM));
                	select.setItemCaption(FieldTemplate.WIDTH_UNIT_EX, vaadinUi.getPrettyCode().fieldTemplateWidth(FieldTemplate.WIDTH_UNIT_EX));
                	select.setItemCaption(FieldTemplate.WIDTH_UNIT_AUTO, vaadinUi.getPrettyCode().fieldTemplateWidth(FieldTemplate.WIDTH_UNIT_AUTO));
			        select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -1826058076733325193L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							setupWidthAndWidthUnits((String)event.getProperty().getValue());
						}
			        	
			        });
                	return select;
                }
                
				if ( propertyId.equals("ignoreWidthOnOutput") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("LibFieldForm.ignoreWidthOnOutput.label"));
                	cb.setTabIndex(tabIndex++);
	            	cb.setImmediate(false);
	                cb.setDescription(vaadinUi.getMsg("LibFieldForm.ignoreWidthOnOutput.tooltip"));
	                return cb;
				} 
                
				if ( propertyId.equals("borderTop") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("LibFieldForm.border.top.label"));
                	cb.setTabIndex(tabIndex++);
	            	cb.setImmediate(false);
	                return cb;
				} 
				if ( propertyId.equals("borderBottom") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("LibFieldForm.border.bottom.label"));
                	cb.setTabIndex(tabIndex++);
	            	cb.setImmediate(false);
	                return cb;
				} 
				if ( propertyId.equals("borderLeft") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("LibFieldForm.border.left.label"));
                	cb.setTabIndex(tabIndex++);
	            	cb.setImmediate(false);
	                return cb;
				} 
				if ( propertyId.equals("borderRight") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("LibFieldForm.border.right.label"));
                	cb.setTabIndex(tabIndex++);
	            	cb.setImmediate(false);
	                return cb;
				} 
				
				Field<?> field = super.createField(item, propertyId, uiContext);
            	field.setTabIndex(tabIndex++);
    			
                if (propertyId.equals("esfName")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setCaption(vaadinUi.getMsg("caption.esfName"));
                	tf.setDescription(vaadinUi.getMsg("LibFieldForm.esfname.tooltip"));
                	tf.setWidth(100, Unit.PERCENTAGE);
                	tf.setConversionError(vaadinUi.getMsg("validator.esfname.message"));
                } else if (propertyId.equals("minLength")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    tf.setConversionError(vaadinUi.getMsg("tooltip.invalid.integer")); // validators added later if needed
                	tf.setColumns(4);
                } else if (propertyId.equals("maxLength")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    tf.setConversionError(vaadinUi.getMsg("tooltip.invalid.integer")); // validators added later if needed
                	tf.setColumns(6);
                } else if (propertyId.equals("width")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setColumns(4);
                	tf.setMaxLength(4);
                	tf.setNullRepresentation("100");
                	tf.setCaption(vaadinUi.getMsg("LibFieldForm.width.label"));
                	tf.setDescription(vaadinUi.getMsg("LibFieldForm.width.tooltip"));
               		tf.setConversionError(vaadinUi.getMsg("tooltip.invalid.integer.range","1-9999"));
               		tf.addValidator(new ShortRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","1-9999"),(short)1,(short)9999));
				}
                
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    @Override
	public void buttonClick(ClickEvent event) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
			LibFieldBean currBean = getCurrentBean();
			
    		// If we're saving a new bean
			Errors errors = new Errors();
    		if ( newBean != null ) {
        		if ( newBean.save(errors) ) {
            		view.getFieldTemplateMap().put(newBean.getEsfName(), newBean.fieldTemplate());
        			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",newBean.getEsfName()) );
            		// We need to add the new bean to the container
            		container.addItem(newBean);
            		view.select(newBean); // reselect our bean so all updates cleanly
            		prevBean = newBean = null; // and we're not longer working on a new bean
        		} else {
        			vaadinUi.show(errors);
        		}
    		} else {
    			view.getFieldTemplateMap().remove(currBean.getOriginalEsfName());  // Handles the case where it's renamed...remove original name before it's saved
    			if ( currBean.save(errors) ) {
        			view.getFieldTemplateMap().put(currBean.getEsfName(), currBean.fieldTemplate());
        			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",currBean.getEsfName()) );
            		view.select(currBean); // reselect our bean so all updates cleanly
        		} else {
        			vaadinUi.show(errors);
        		}
    		}
        } else if ( source == cancelButton ) {
    		discard();
    		vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
        	if ( newBean != null ) {
        		setItemDataSource(null);
        		view.unselectAll(); // we'll unselect and the reselect our prevBean
        		view.select(prevBean);
        		prevBean = newBean = null; // we're not doing a new bean anymore
        	}
        	setReadOnly(true);
        } else if ( source == editButton ) {
        	setReadOnly(false);
        } else if ( source == deleteButton ) {
        	discard();
        	LibFieldBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		setItemDataSource(null);
        		view.unselectAll(); // we'll unselect it and remove it from our container
        		if ( newBean == null ) {
        			Errors errors = new Errors();
        			if ( currBean.delete(errors) ) {
                		view.getFieldTemplateMap().remove(currBean.getEsfName());
                		container.removeItem(currBean);
                		vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",currBean.getEsfName()) );
        			} else {
        				vaadinUi.show(errors);
        			}
        		} else {
        			newBean.delete(null);
            		view.select(prevBean);
        			prevBean = newBean = null;
        		}
        	}	
        } else if ( source == createLikeButton ) {
        	LibFieldBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		createLike(currBean);
        	}	
        }
    }
	
	public String getCurrentBeanName() {
		LibFieldBean currBean = getCurrentBean();
		return currBean == null ? "(None)" : currBean.getEsfName().toString();
	}
    
	LibFieldBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public LibFieldBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibFieldBean> bi = (BeanItem<LibFieldBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		if ( newDataSource != null ) {
    		LibFieldBean bean = getBean(newDataSource);
    		
    		List<String> orderedProperties = new LinkedList<String>();
    		orderedProperties.addAll(Arrays.asList("esfName","libraryFieldTemplateId","type","textAlignCss","required","autoComplete","maskInput","maskInDataSnapshot","cloneAllowed",
    												"minLength","maxLength","width","widthUnit","ignoreWidthOnOutput","borderTop","borderBottom","borderLeft","borderRight",
    												"typeBean","libLabelBean"));
    		if ( libraryFieldTemplateMap == null || libraryFieldTemplateMap.size() == 0 ) {
    			orderedProperties.remove("libraryFieldTemplateId");
    		}
    		
    		super.setItemDataSource(newDataSource, orderedProperties);
    		
    		setupForm(bean);
    		
    		setReadOnly( view != null && ! view.hasPermUpdate() ); // if they can edit, we put them in edit mode right away

    		newBean = bean.fieldTemplate().doInsert() ? bean : null;
    		if ( newBean != null ) {
    			TextField tf = (TextField)getField("esfName");
    			tf.selectAll();
    			tf.focus();
    		}
    		layout.setVisible(true);
    		getFooter().setVisible(true);
		} else {
    		super.setItemDataSource(null);
    		setReadOnly(true);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
		}
    	
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	LibFieldBean bean = getCurrentBean();
    	
    	// We have these only while editing/creating new
    	if ( saveButton != null ) {
    		saveButton.setVisible(!readOnly);
    	}
    	
    	if ( cancelButton != null ) {
    		cancelButton.setVisible(!readOnly);
    	}

    	// We have this only while in view mode
    	if ( editButton != null ) {
    		editButton.setVisible(readOnly && bean != null && view != null && view.hasPermUpdate());
    	}
    	
    	// This one we show this in all modes if has permission
    	if ( createLikeButton != null ) {
    		createLikeButton.setVisible(bean != null && bean.fieldTemplate().doUpdate() && view != null && view.hasPermCreateLike()); // only want create like on an existing field
    	}
    	
    	if ( deleteButton != null ) {
        	deleteButton.setVisible(bean != null && view != null && view.hasPermDelete() && ! bean.getOriginalEsfName().hasReservedPrefix() );
    	}
    	
    	if ( bean != null && bean.getOriginalEsfName().hasReservedPrefix() ) {
    		getField("esfName").setReadOnly(true);
    	}
    	
    	alignRequiredSecurityButtonLayout.setReadOnly(readOnly);
    	widthLayout.setReadOnly(readOnly);
    	borderLayout.setReadOnly(readOnly);
    }
    
    public void createLike(LibFieldBean likeBean) {
    	view.createLike(likeBean);
    	prevBean = likeBean;
     }
    
    protected void setupForm(LibFieldBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("LibFieldForm.label.id",bean.getId()) );
		
		Field esfname = getField("esfName");
		if ( esfnameValidator != null ) esfname.removeValidator(esfnameValidator);
		esfnameValidator = new LibraryFieldEsfNameValidator(view.getFieldTemplateMap(),bean.fieldTemplate(),this);
		esfname.addValidator(esfnameValidator);
		
		setupFieldsByType(bean.getType());
    }
    
    protected void rebuildTypeForm(String type) {
    	if ( currentType.equals(type) )
    		return;
    	
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	TextField esfNameField = (TextField)getField("esfName");

    	// Kill the previous form field by removing its data source and all validators
    	Form typeBeanFormField = (Form)getField("typeBean");
       	typeBeanFormField.setPropertyDataSource(null);
		// when validators are set for input and output format specs, they are in the TypeSubForm class
       	vaadinUi.removeAllValidators(typeBeanFormField);
       	vaadinUi.removeAllValidators(typeBeanFormField.getField("initialValue")); 
       	vaadinUi.removeAllValidators(typeBeanFormField.getField("autoPost")); 
       	vaadinUi.removeAllValidators(typeBeanFormField.getField("tooltip")); 
       	vaadinUi.removeAllValidators(typeBeanFormField.getField("inputFormatSpec"));
       	vaadinUi.removeAllValidators(typeBeanFormField.getField("outputFormatSpec"));
       	vaadinUi.removeAllValidators(typeBeanFormField.getField("extraOptions"));
       	
    	thisForm.detachField(typeBeanFormField);
    	
    	// Create a new one using this type
    	TypeSubForm typeSubFormField = new TypeSubForm(library,thisForm,fieldTemplateMap,typeBeanTabIndex);
    	typeSubFormField.setPropertyDataSource(this.getItemDataSource().getItemProperty("typeBean"));
    	thisForm.addField("typeBean",typeSubFormField);
    	try {
    		esfNameField.validate();
    		esfNameField.setComponentError(null);
    	} catch( Validator.InvalidValueException e ) {
    		esfNameField.requestRepaint();
    	}
    	setupFieldsByType(type);
    }
    
    protected void setupFieldsByType(String type) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
 		TextField minLengthField = (TextField)getField("minLength");
    	if ( minLengthField == null ) return;  // If we don't have our fields setup yet, ignore
    	vaadinUi.removeAllValidators(minLengthField);
    	if ( FieldTemplate.isTypeDecimal(type) ) {
    		minLengthField.setCaption(vaadinUi.getMsg("LibFieldForm.minLength.decimal.label"));
    		minLengthField.setDescription(vaadinUi.getMsg("LibFieldForm.minLength.decimal.tooltip"));
    		minLengthField.setNullRepresentation("0");
    		minLengthField.addValidator(new IntegerRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","-999999999 to 999999999"),-999999999,999999999));
    	} else if ( FieldTemplate.isTypeInteger(type) ) {
    		minLengthField.setCaption(vaadinUi.getMsg("LibFieldForm.minLength.integer.label"));
    		minLengthField.setDescription(vaadinUi.getMsg("LibFieldForm.minLength.integer.tooltip"));
    		minLengthField.setNullRepresentation("0");
    		minLengthField.addValidator(new IntegerRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","-999999999 to 999999999"),-999999999,999999999));
    	} else if ( FieldTemplate.isTypeMoney(type) ) {
    		minLengthField.setCaption(vaadinUi.getMsg("LibFieldForm.minLength.money.label"));
    		minLengthField.setDescription(vaadinUi.getMsg("LibFieldForm.minLength.money.tooltip"));
    		minLengthField.setNullRepresentation("0");
    		minLengthField.addValidator(new IntegerRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","-999999999 to 999999999"),-999999999,999999999));
    	} else {
        	minLengthField.setCaption(vaadinUi.getMsg("LibFieldForm.minLength.label"));
    		minLengthField.setDescription(vaadinUi.getMsg("LibFieldForm.minLength.tooltip"));
    		minLengthField.setNullRepresentation("0");
    		minLengthField.addValidator(new IntegerRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","0-9999"),0,9999));
    	}
		minLengthField.setVisible(true);
    	
    	TextField maxLengthField = (TextField)getField("maxLength");
		vaadinUi.removeAllValidators(maxLengthField);
	   	if ( FieldTemplate.isTypeDecimal(type) ) {
    		maxLengthField.setCaption(vaadinUi.getMsg("LibFieldForm.maxLength.decimal.label"));
    		maxLengthField.setDescription(vaadinUi.getMsg("LibFieldForm.maxLength.decimal.tooltip"));
    		maxLengthField.setNullRepresentation("999999999");
    		maxLengthField.addValidator(new IntegerRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","-999999999 to 999999999"),-999999999,999999999));
    	} else if ( FieldTemplate.isTypeInteger(type) ) {
    		maxLengthField.setCaption(vaadinUi.getMsg("LibFieldForm.maxLength.integer.label"));
    		maxLengthField.setDescription(vaadinUi.getMsg("LibFieldForm.maxLength.integer.tooltip"));
    		maxLengthField.setNullRepresentation("999999999");
    		maxLengthField.addValidator(new IntegerRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","-999999999 to 999999999"),-999999999,999999999));
    	} else if ( FieldTemplate.isTypeMoney(type) ) {
    		maxLengthField.setCaption(vaadinUi.getMsg("LibFieldForm.maxLength.money.label"));
    		maxLengthField.setDescription(vaadinUi.getMsg("LibFieldForm.maxLength.money.tooltip"));
    		maxLengthField.setNullRepresentation("999999999");
    		maxLengthField.addValidator(new IntegerRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","-999999999 to 999999999"),-999999999,999999999));
     	} else {
    		maxLengthField.setCaption(vaadinUi.getMsg("LibFieldForm.maxLength.label"));
    		maxLengthField.setDescription(vaadinUi.getMsg("LibFieldForm.maxLength.tooltip"));
    		maxLengthField.setNullRepresentation("100");
    		maxLengthField.addValidator(new IntegerRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","1-999999"),1,999999));
    	}
		maxLengthField.setVisible(true);			

		Field<?> requiredField = getField("required");
		requiredField.setVisible(true);
		
		Field<?> autoCompleteField = getField("autoComplete");
		autoCompleteField.setVisible(true);
		
		Field<?> autoPostField = getField("autoPost");
		if ( autoPostField != null )
			autoPostField.setVisible(true);
		
		Field<?> maskInDataSnapshotField = getField("maskInDataSnapshot");
		maskInDataSnapshotField.setVisible(true);
		
		Field<?> maskInputField = getField("maskInput");
		maskInputField.setVisible(true);
		
		Field<?> cloneAllowedField = getField("cloneAllowed");
		cloneAllowedField.setVisible(true);
		
		Field<?> textAlignCss = getField("textAlignCss");
		textAlignCss.setVisible(true);

		Field<?> width = getField("width");
		Field<?> widthUnit = getField("widthUnit");
		setupWidthAndWidthUnits((String)widthUnit.getValue());
		
		Field<?> ignoreWidthOnOutput = getField("ignoreWidthOnOutput");
		ignoreWidthOnOutput.setVisible(true);
		
		borderLayout.setVisible(true);
		
		Field<?> libLabelBean = getField("libLabelBean");
		libLabelBean.setVisible(true);

		if ( FieldTemplate.isTypeCheckbox(type) ) {
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
			autoCompleteField.setVisible(false);
			maskInDataSnapshotField.setVisible(false);
			maskInputField.setVisible(false);
		} else if ( FieldTemplate.isTypeCreditCard(type) ) {
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
		} else if ( FieldTemplate.isTypeDate(type) ) {
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
			maskInputField.setVisible(false);
		} else if ( FieldTemplate.isTypeDateTime(type) ) {
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
			maskInputField.setVisible(false);
		} else if ( FieldTemplate.isTypeEmailAddress(type) ) {
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
		} else if ( FieldTemplate.isTypeFile(type) ) {
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
			maskInputField.setVisible(false);
		} else if ( FieldTemplate.isTypeFileConfirmClick(type) ) {
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
			maskInputField.setVisible(false);
		} else if ( FieldTemplate.isTypePhone(type) ) {
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
		} else if ( FieldTemplate.isTypeRadioButton(type) ) {
			requiredField.setVisible(false);
			autoCompleteField.setVisible(false);
			maskInDataSnapshotField.setVisible(false);
			maskInputField.setVisible(false);
			cloneAllowedField.setVisible(false);
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
		} else if ( FieldTemplate.isTypeRadioButtonGroup(type) ) {
			autoCompleteField.setVisible(false);
			maskInDataSnapshotField.setVisible(false);
			maskInputField.setVisible(false);
			textAlignCss.setVisible(false);
			width.setVisible(false);
			widthUnit.setVisible(false);
			ignoreWidthOnOutput.setVisible(false);
			borderLayout.setVisible(false);
			libLabelBean.setVisible(false);
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
		} else if ( FieldTemplate.isTypeRichTextarea(type) ) {
			maskInputField.setVisible(false);
		} else if ( FieldTemplate.isTypeSignature(type) ) {
			maskInputField.setVisible(false);
		} else if ( FieldTemplate.isTypeSignDate(type) ) {
    		requiredField.setVisible(false);
			autoCompleteField.setVisible(false);
			maskInDataSnapshotField.setVisible(false);
			maskInputField.setVisible(false);
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
    	} else if ( FieldTemplate.isTypeSelection(type) ) {
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
			maskInputField.setVisible(false);
		} else if ( FieldTemplate.isTypeSsnEin(type) ) {
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
		} else if ( FieldTemplate.isTypeTextarea(type) ) {
			maskInputField.setVisible(false);
    	} else if ( FieldTemplate.isTypeZipCode(type) ) {
			minLengthField.setVisible(false);
			maxLengthField.setVisible(false);
		}
		
		currentType = type;
    }
    
    void setupWidthAndWidthUnits(String widthUnitValue) {
		Field<String> f = getField("width");
		if ( f != null ) {
			boolean isAuto = FieldTemplate.WIDTH_UNIT_AUTO.equals(widthUnitValue);
			f.setVisible(! isAuto);
			EsfInteger v = new EsfInteger((String)f.getValue());
			if ( v.isNull() || v.isLessThan(1) || v.isGreaterThan(9999) )
				f.setValue("100");
		}
    }
    
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("esfName")) {
        	layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("libraryFieldTemplateId")) {
        	layout.addComponent(field, 1, 0);
        } else if (propertyId.equals("type")) {
        	layout.addComponent(field, 2, 0);
        } else if (propertyId.equals("textAlignCss")) {
        	alignRequiredSecurityButtonLayout.removeAllComponents();
        	alignRequiredSecurityButtonLayout.addComponent(field);
        } else if (propertyId.equals("required")) {
        	alignRequiredSecurityButtonLayout.addComponent(field);
        	alignRequiredSecurityButtonLayout.setComponentAlignment(field, Alignment.BOTTOM_CENTER);
        } else if (propertyId.equals("autoComplete")) {
        	securityButtonLayout = new VerticalLayout();
        	securityButtonLayout.setSizeUndefined();
        	securityButtonLayout.setMargin(false);
        	securityButtonLayout.setSpacing(true);

        	securityButton = new PopupButton("Security options");
        	securityButton.setStyleName(Reindeer.BUTTON_LINK);
        	securityButton.setContent(securityButtonLayout);
        	alignRequiredSecurityButtonLayout.addComponent(securityButton);
        	alignRequiredSecurityButtonLayout.setComponentAlignment(securityButton, Alignment.BOTTOM_CENTER);
        	
        	securityButtonLayout.addComponent(field);
        } else if (propertyId.equals("maskInDataSnapshot")) {
        	securityButtonLayout.addComponent(field);
        } else if (propertyId.equals("maskInput")) {
        	securityButtonLayout.addComponent(field);
        } else if (propertyId.equals("cloneAllowed")) {
        	securityButtonLayout.addComponent(field);
        } else if (propertyId.equals("minLength")) {
        	layout.addComponent(field, 1, 1);
        } else if (propertyId.equals("maxLength")) {
        	layout.addComponent(field, 2, 1);
        } else if (propertyId.equals("width")) {
        	widthLayout.removeAllComponents();
        	widthLayout.addComponent(field);
        } else if (propertyId.equals("widthUnit")) {
        	widthLayout.addComponent(field);
        } else if (propertyId.equals("ignoreWidthOnOutput")) {
        	widthLayout.addComponent(field);
        	widthLayout.setComponentAlignment(field, Alignment.BOTTOM_LEFT);
        } else if (propertyId.equals("borderTop")) {
        	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
        	borderLayout.removeAllComponents();
        	borderLayout.addComponent(new Label(vaadinUi.getMsg("LibFieldForm.border.label")));
        	borderLayout.addComponent(field);
        } else if (propertyId.equals("borderBottom")) {
        	borderLayout.addComponent(field);
        } else if (propertyId.equals("borderLeft")) {
        	borderLayout.addComponent(field);
        } else if (propertyId.equals("borderRight")) {
        	borderLayout.addComponent(field);
        } else if (propertyId.equals("typeBean")) {
        	Component prev = layout.getComponent(0, 3);
        	if ( prev == null )
        		layout.addComponent(field, 0, 3, 2, 3);
        	else
        		layout.replaceComponent(prev, field);
        } else if (propertyId.equals("libLabelBean")) {
        	layout.addComponent(field, 0, 4, 2, 4);
        }
    }
    
	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibFieldView.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}