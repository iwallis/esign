// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.file;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.FileVersion;
import com.esignforms.open.prog.FileVersionInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibFileVersionBean implements Serializable, Comparable<LibFileVersionBean> {
	private static final long serialVersionUID = 3075201993382513152L;

	private FileVersionInfo fileVerInfo;
	private String versionLabel;
	
	private FileVersion fileVersion;
	
	public LibFileVersionBean(FileVersionInfo fileVerInfo, String versionLabel) {
		this.fileVerInfo = fileVerInfo;
		this.versionLabel = versionLabel;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibFileVersionBean )
			return fileVerInfo.equals(((LibFileVersionBean)o).fileVerInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return fileVerInfo.hashCode();
    }
	public int compareTo(LibFileVersionBean d) {
		return fileVerInfo.compareTo(d.fileVerInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public FileVersionInfo fileVerInfo() {
		return fileVerInfo;
	}

	public FileVersion fileVersion() {
		if ( fileVersion == null )
			fileVersion = FileVersion.Manager.getById(fileVerInfo.getId());
		return fileVersion;
	}

	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( fileVersion().save(con,vaadinUi.getUser()) ) {
			fileVerInfo = FileVersionInfo.Manager.createFromSource(fileVersion());
			resetCaches();
			return true;
		} 

		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}
	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return fileVerInfo.getId();
	}

	public EsfUUID getFileId() {
		return fileVerInfo.getFileId();
	}

	public int getVersion() {
		return fileVerInfo.getVersion();
	}
	
	public String getVersionLabel() {
		return versionLabel;
	}

	public EsfDateTime getCreatedTimestamp() {
		return fileVerInfo.getCreatedTimestamp();
	}
	public String formatCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return fileVerInfo.getCreatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return fileVerInfo.getCreatedTimestamp().toLogString(vaadinUi.getUser());
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return fileVerInfo.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return fileVerInfo.getLastUpdatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return fileVerInfo.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser());
	}
	
	public String getFileFileName() {
		return fileVerInfo.getFileFileName();
	}
	public boolean hasFileFileName() {
		return EsfString.isNonBlank(getFileFileName());
	}
	public void setFileFileName(String v) {
		fileVersion().setFileFileName(v);
	}	
	public String getFileByIdUrl() {
		return fileVersion().getFileByIdUrl();
	}
	
	public void setFileData(byte[] v) {
		fileVersion().setFileData(v);
	}
	public void setFileMimeType(String v) {
		fileVersion().setFileMimeType(v);
	}
	
	public int getFileSize() {
		return fileVersion().getFileSize();
	}
	
	public boolean hasChanged() {
		return fileVersion().hasChanged();
	}
	public void discard() {
		if ( fileVersion != null ) {
			EsfUUID id = fileVersion.getId();
			FileVersion.Manager.dropFromCache(fileVersion);
			fileVersion = FileVersion.Manager.getById(id); // get fresh if we have one
		}
		resetCaches();
	}
	
	public boolean isFileVersionReady() {
		return EsfString.isNonBlank(fileVersion().getFileFileName()) && fileVersion().getFileData() != null;
	}
}