// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.SetFieldValueAction;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.runtime.action.SetFieldValueAction;
import com.esignforms.open.runtime.action.SetFieldValueAction.Spec;
import com.esignforms.open.runtime.condition.CompoundCondition;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.main.EsfViewWithConfirmDiscardFormChangesWindowParent;
import com.esignforms.open.vaadin.widget.ConditionTree;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

public class ActionView extends HorizontalSplitPanel implements EsfViewWithConfirmDiscardFormChangesWindowParent, Property.ValueChangeListener {
	private static final long serialVersionUID = -3064844079065463847L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ActionView.class);
	
    final PackageVersion duplicatedPackageVersion;
    final DocumentVersion duplicatedDocumentVersion;
    final SetFieldValueAction duplicatedAction;
    final ActionView thisView;
    ConfirmDiscardFormChangesWindow parentWindow;
    ConditionTree conditionTree;
    ActionContainer container;
    ActionList list;
    ActionForm form;
	
	public ActionView(PackageVersion duplicatedPackageVersionParam, SetFieldValueAction duplicatedActionParam) {
		super();
		this.duplicatedDocumentVersion = null;
		this.duplicatedPackageVersion = duplicatedPackageVersionParam;
		this.duplicatedAction = duplicatedActionParam;
		thisView = this;
	}
	
	public ActionView(DocumentVersion duplicatedDocumentVersionParam, SetFieldValueAction duplicatedActionParam) {
		super();
		this.duplicatedDocumentVersion = duplicatedDocumentVersionParam;
		this.duplicatedPackageVersion = null;
		this.duplicatedAction = duplicatedActionParam;
		thisView = this;
	}

	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			setStyleName(Reindeer.SPLITPANEL_SMALL);
			
			container = new ActionContainer(duplicatedAction.getSpecList());
			
			Panel conditionPanel = new Panel(vaadinUi.getMsg("ConditionTree.panel.caption")); 
			conditionTree = new ConditionTree();
			VerticalLayout panelLayout = new VerticalLayout();
			panelLayout.setSpacing(true);
			panelLayout.setMargin(true);
			conditionPanel.setContent(panelLayout);
			panelLayout.addComponent(conditionTree);
			if ( duplicatedPackageVersion != null )
				conditionTree.setPackageVersion(duplicatedPackageVersion); 
			else
				conditionTree.setDocumentVersion(duplicatedDocumentVersion); 
	        if ( duplicatedAction.hasCondition() )
	        	conditionTree.setRootCondition((CompoundCondition)duplicatedAction.getCondition());
			
			list = new ActionList(this,container);
			form = duplicatedPackageVersion != null ? new ActionForm(this,container,duplicatedPackageVersion,duplicatedAction): new ActionForm(this,container,duplicatedDocumentVersion,duplicatedAction);
			
			VerticalSplitPanel listFormPanel = new VerticalSplitPanel();
			listFormPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			listFormPanel.setFirstComponent(list);
			listFormPanel.setSecondComponent(form);
			listFormPanel.setSplitPosition(30,Unit.PERCENTAGE);

			setFirstComponent(conditionPanel);
			setSecondComponent(listFormPanel);
			setSplitPosition(300,Unit.PIXELS);

			// If we're not in read-only made and the container is empty, auto start the first
			if ( ! isReadOnly() && container.size() == 0 )
				createNew();
			
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("ActionView view exception", e.getMessage());
		}
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		if ( conditionTree != null ) conditionTree.setReadOnly(v);
		if ( list != null) list.setReadOnly(v);
		if ( form != null) form.setReadOnly(v);
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our list or not
        if (property == list.getTable()) {
        	final Item item = list.getTable().getItem(list.getTable().getValue());
        	if ( isDirty() ) {
    			if ( form.getItemDataSource() == item ) {
    				// We've selected the same item so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("PackageProgrammingRule.SetFieldValueAction.view.ConfirmDiscardChangesDialog.message"), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						form.discard();
						form.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						list.getTable().select(item); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("PackageProgrammingRule.SetFieldValueAction.view.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    		} else {
            	form.setItemDataSource(item);
    		}
        }
        else
        	_logger.debug("valueChange() - no list event: " + event.toString());
	}

	public void createNew() {
		unselectAll();
		Spec newSpec = new Spec(container.size()+1,form.getNewSpecDocumentId(),new EsfName(""),"",false,null,-1,-1);
		form.setNewSpecAsDataSource(newSpec);
	}
	
	public void specListUpdated() {
		list.getTable().reorder();
	}
	
	@Override
	public void setParentWindow(ConfirmDiscardFormChangesWindow w) {
		parentWindow = w;
	}
	
	public void closeParentWindow() {
		parentWindow.close();
	}
	
	public void unselectAll() {
		list.getTable().setValue(null);
	}
	public void select(Spec spec) {
		unselectAll();
		list.getTable().select(spec);
		list.getTable().setCurrentPageFirstItemId(spec);
	}
	
	@Override
	public void detach() {
		// When our view goes out, we'll save any changes that were made.
	    if ( ! isReadOnly() ) {
	    	if ( conditionTree.isDirty() ) {
	    		duplicatedAction.setCondition(conditionTree.getRootCondition());
	    		conditionTree.clearDirty();
	    	}
	    }
	    
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return form.checkDirty();
	}
	
	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return form.isDirty();
	}
}