// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.propertyset;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.Record;
import com.esignforms.open.prog.PropertySetVersion;
import com.esignforms.open.prog.PropertySetVersionInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibPropertySetVersionBean implements Serializable, Comparable<LibPropertySetVersionBean> {
	private static final long serialVersionUID = 2525725561900649744L;

	private PropertySetVersionInfo propertysetVerInfo;
	private String versionLabel;
	
	private PropertySetVersion propertysetVersion;
	
	public LibPropertySetVersionBean(PropertySetVersionInfo propertysetVerInfo, String versionLabel) {
		this.propertysetVerInfo = propertysetVerInfo;
		this.versionLabel = versionLabel;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibPropertySetVersionBean )
			return propertysetVerInfo.equals(((LibPropertySetVersionBean)o).propertysetVerInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return propertysetVerInfo.hashCode();
    }
	public int compareTo(LibPropertySetVersionBean d) {
		return propertysetVerInfo.compareTo(d.propertysetVerInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public PropertySetVersionInfo propertysetVerInfo() {
		return propertysetVerInfo;
	}

	public PropertySetVersion propertysetVersion() {
		if ( propertysetVersion == null )
			propertysetVersion = PropertySetVersion.Manager.getById(propertysetVerInfo.getId());
		return propertysetVersion;
	}

	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( propertysetVersion().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			return true;
		} 

		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}
	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return propertysetVerInfo.getId();
	}

	public int getVersion() {
		return propertysetVerInfo.getVersion();
	}
	
	public String getVersionLabel() {
		return versionLabel;
	}

	public EsfDateTime getCreatedTimestamp() {
		return propertysetVerInfo.getCreatedTimestamp();
	}
	public String formatCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return propertysetVerInfo.getCreatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return propertysetVerInfo.getCreatedTimestamp().toLogString(vaadinUi.getUser());
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return propertysetVerInfo.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return propertysetVerInfo.getLastUpdatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return propertysetVerInfo.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser());
	}
	
	public int getNumProperties() {
		return propertysetVerInfo.getNumProperties();
	}
	
	public void setProperties(Record newProps) {
		propertysetVersion().setProperties(newProps);
	}
}