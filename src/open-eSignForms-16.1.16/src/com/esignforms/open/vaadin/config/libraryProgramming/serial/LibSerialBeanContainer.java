// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.serial;

import java.io.Serializable;
import java.util.Collection;

import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.SerialInfo;
import com.vaadin.data.util.BeanItemContainer;

public class LibSerialBeanContainer extends BeanItemContainer<LibSerialBean> implements Serializable {
	private static final long serialVersionUID = -3686980735995273088L;

	final EsfUUID containerId;
	
	public LibSerialBeanContainer(EsfUUID containerId) throws InstantiationException, IllegalAccessException {
		super(LibSerialBean.class);
		this.containerId = containerId;
		
		refresh();
	}
	
	public void refresh() {
		removeAllItems();
		
		if ( containerId != null ) {
			
			Collection<SerialInfo> serials = SerialInfo.Manager.getAll(containerId);
			for( SerialInfo s : serials ) {
				addItem( new LibSerialBean(s) );					
			}
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}