// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.propertyset;

import java.io.Serializable;
import java.util.Collection;

import com.esignforms.open.prog.PropertySetInfo;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.util.BeanItemContainer;


public class LibPropertySetBeanContainer extends BeanItemContainer<LibPropertySetBean> implements Serializable {
	private static final long serialVersionUID = 682011475088388554L;

	final Library library;
	final User user;
	
	public LibPropertySetBeanContainer(Library library) throws InstantiationException, IllegalAccessException {
		super(LibPropertySetBean.class);
		this.library = library;
		this.user = null;
		refresh();
	}
	
	public LibPropertySetBeanContainer(User user) throws InstantiationException, IllegalAccessException {
		super(LibPropertySetBean.class);
		this.library = null;
		this.user = user;
		refresh();
	}
	
	public void refresh() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		removeAllItems();
		
		// Users need view details permission to see its contents
		if ( library != null && library.hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS) ) {			
			Collection<PropertySetInfo> propertySets = PropertySetInfo.Manager.getAll(library.getId());
			for( PropertySetInfo i : propertySets ) {
				addItem( new LibPropertySetBean(i) );					
			}
		} else if ( user != null && (user.canUserList(vaadinUi.getUser()) || user.canUserUpdate(vaadinUi.getUser())) ) {
			Collection<PropertySetInfo> propertySets = PropertySetInfo.Manager.getAll(user.getId());
			for( PropertySetInfo i : propertySets ) {
				addItem( new LibPropertySetBean(i) );					
			}
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}