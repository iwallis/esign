// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.documentstyle;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.DocumentStyle;
import com.esignforms.open.prog.DocumentStyleInfo;
import com.esignforms.open.prog.DocumentStyleVersionInfo;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

/**
 * LibDocumentStyleAndVersionsMainView is a splitpanel that contains the documentstyle view in the left and the documentstyle version view on the right.
 * 
 * Each of those views is itself a splitpanel, containing a table/list on the top to select items, with a form on the bottom to view/edit.
 * 
 * When a new documentstyle is selected in the left view, we propagate that to the right view so it can sync the versions with the selected documentstyle.
 * 
 * @author Yozons
 *
 */
public class LibDocumentStyleAndVersionsMainView extends HorizontalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = 8266509502746617624L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibDocumentStyleAndVersionsMainView.class);

    EsfUUID libraryId;
    Library library;
    final LibDocumentStyleAndVersionsMainView thisView;
	
    VerticalSplitPanel documentstyleSplitPanel;
    LibDocumentStyleBeanContainer documentstyleContainer;
    LibDocumentStyleList documentstyleList;
    LibDocumentStyleForm documentstyleForm;
	
    VerticalSplitPanel documentstyleVerSplitPanel;
    LibDocumentStyleVersionBeanContainer documentstyleVerContainer;
	LibDocumentStyleVersionList documentstyleVerList;
	LibDocumentStyleVersionForm documentstyleVerForm;

	public LibDocumentStyleAndVersionsMainView() {
		super(); 
		thisView = this;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			library = Library.Manager.getById(libraryId, vaadinUi.getUser());
			
			documentstyleContainer = new LibDocumentStyleBeanContainer(library);
			documentstyleVerContainer = new LibDocumentStyleVersionBeanContainer(this); // Load contents when a documentstyle is selected from our documentstyleContainer
			
			documentstyleList = new LibDocumentStyleList(this,documentstyleContainer);
		    documentstyleForm = new LibDocumentStyleForm(this,documentstyleContainer);

			documentstyleSplitPanel = new VerticalSplitPanel();
			documentstyleSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			documentstyleSplitPanel.setSplitPosition(30);
			documentstyleSplitPanel.setFirstComponent(documentstyleList);
			documentstyleSplitPanel.setSecondComponent(documentstyleForm);
		    
			documentstyleVerList = new LibDocumentStyleVersionList(this,documentstyleVerContainer);
			documentstyleVerForm = new LibDocumentStyleVersionForm(this,documentstyleVerContainer);
			
			documentstyleVerSplitPanel = new VerticalSplitPanel();
			documentstyleVerSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			documentstyleVerSplitPanel.setSplitPosition(30);
			documentstyleVerSplitPanel.setFirstComponent(documentstyleVerList);
			documentstyleVerSplitPanel.setSecondComponent(documentstyleVerForm);

			setFirstComponent(documentstyleSplitPanel);
			setSecondComponent(documentstyleVerSplitPanel);
			setSplitPosition(35);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("DocumentStyle and versions main view exception", e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	private LibDocumentStyleBean getLibDocumentStyleBean(Item documentstyleListItem) {
    	if ( documentstyleListItem == null )
    		return null;
		BeanItem<LibDocumentStyleBean> bi = (BeanItem<LibDocumentStyleBean>)documentstyleListItem;
		return bi.getBean();
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our documentstyle list or not
        if (property == documentstyleList.getTable()) {
        	final Item item = documentstyleList.getTable().getItem(documentstyleList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( documentstyleForm.getCurrentBean() == documentstyleForm.getBean(item) ) {
    				// We've selected the same bean so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibDocumentStyleAndVersionsMainView.DocumentStyle.ConfirmDiscardChangesDialog.message", documentstyleForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						thisView.discardForms();
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						documentstyleList.getTable().select(documentstyleForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentStyleAndVersionsMainView.DocumentStyle.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	LibDocumentStyleBean bean = getLibDocumentStyleBean(item);
                documentstyleForm.setItemDataSource(item);
            	if ( bean != null ) {
                	documentstyleVerContainer.reload(bean.documentstyle());
                	selectFirstDocumentStyleVersion();
            	} else {
            		documentstyleVerForm.setItemDataSource(null);
            		documentstyleVerContainer.removeAllItems();
            	}
    		}
        } else if (property == documentstyleVerList.getTable()) {
        	final Item item = documentstyleVerList.getTable().getItem(documentstyleVerList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( documentstyleVerForm.getCurrentBean() == documentstyleVerForm.getBean(item) ) {
    				// We've selected the same bean (version) so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibDocumentStyleAndVersionsMainView.DocumentStyle.ConfirmDiscardChangesDialog.message", documentstyleForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						documentstyleVerForm.discard();
						documentstyleVerForm.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						documentstyleVerList.getTable().select(documentstyleVerForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentStyleAndVersionsMainView.DocumentStyle.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	documentstyleVerForm.setItemDataSource(item);
    		}
        }
        else {
        	_logger.debug("valueChange() - no list event: " + event.toString());
        }
	}
	

	@Override
	public void detach() {
		/*
		for( Object o : list.getTable().getVisibleColumns() ) {
			_logger.debug("detach() - list visible columns[]: " + o); // These are string property ids of the column names like email, status, lastLoginTimestamp
		}
		*/
		super.detach();
	}
	
	public void selectDocumentStyle(LibDocumentStyleBean bean) {
		unselectAllDocumentStyles();
		documentstyleList.getTable().select(bean);
		documentstyleList.getTable().setCurrentPageFirstItemId(bean);
		if ( bean == null ) {
			documentstyleVerForm.setItemDataSource(null);
		}
	}
	
	public void unselectAllDocumentStyles() {
		documentstyleList.getTable().setValue(null);
		documentstyleForm.setItemDataSource(null);
	}
	
	public void selectDocumentStyleVersion(LibDocumentStyleVersionBean bean) {
		unselectAllDocumentStyleVersions();
		documentstyleVerList.getTable().select(bean);
		documentstyleVerList.getTable().setCurrentPageFirstItemId(bean);
	}

	public void discardForms() {
		documentstyleForm.discard();
		documentstyleVerForm.discard();
		documentstyleForm.setItemDataSource(null);
		documentstyleVerForm.setItemDataSource(null);
	}

	
	public void selectFirstDocumentStyleVersion() {
		if ( documentstyleVerContainer.size() > 0 ) {
			selectDocumentStyleVersion(documentstyleVerContainer.getIdByIndex(0));
		} else {
			unselectAllDocumentStyleVersions(); 
		}
	}
	
	public void unselectAllDocumentStyleVersions() {
		documentstyleVerList.getTable().setValue(null);
	}

	public Library getLibrary() {
		return library;
	}
	
	public LibDocumentStyleBean getLibDocumentStyleBean() {
		return documentstyleForm == null ? null : documentstyleForm.getCurrentBean();
	}
	
	public LibDocumentStyleVersionBean getLibDocumentStyleVersionBean() {
		return documentstyleVerForm == null ? null : documentstyleVerForm.getCurrentBean();
	}
	
	public String getEsfNameWithVersion() {
		LibDocumentStyleBean documentstyleBean = getLibDocumentStyleBean();
		LibDocumentStyleVersionBean documentstyleVerBean = getLibDocumentStyleVersionBean();
		return documentstyleVerBean == null ? "?? [?]" : documentstyleBean.getEsfName() + " [" + documentstyleVerBean.getVersion() + "]";
	}
	
	public Integer getProductionVersion() {
		LibDocumentStyleBean documentstyleBean = getLibDocumentStyleBean();
		return documentstyleBean == null ? 0 : documentstyleBean.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		LibDocumentStyleBean documentstyleBean = getLibDocumentStyleBean();
		return documentstyleBean == null ? 0 : documentstyleBean.getTestVersion();
	}

	public boolean hasTestVersion() {
		LibDocumentStyleBean documentstyleBean = getLibDocumentStyleBean();
		return documentstyleBean == null ? false : documentstyleBean.documentstyle().hasTestVersion();
	}
	
    public boolean isProductionVersion(DocumentStyleVersionInfo documentstyleVer)
    {
    	LibDocumentStyleBean documentstyleBean = getLibDocumentStyleBean();
    	return documentstyleBean != null && documentstyleVer != null && documentstyleVer.getVersion() == documentstyleBean.getProductionVersion();
    }

    public boolean isTestVersion(DocumentStyleVersionInfo documentstyleVer)
    {
    	LibDocumentStyleBean documentstyleBean = getLibDocumentStyleBean();
    	return documentstyleBean != null && documentstyleVer != null && documentstyleVer.getVersion() > documentstyleBean.getProductionVersion();
    }

    // "Current" here means latest production or test version.
    public boolean isCurrentVersion(DocumentStyleVersionInfo documentstyleVer)
    {
    	LibDocumentStyleBean documentstyleBean = getLibDocumentStyleBean();
    	return documentstyleBean != null && documentstyleVer != null && documentstyleVer.getVersion() >= documentstyleBean.getProductionVersion();
    }

    public boolean isOldVersion(DocumentStyleVersionInfo documentstyleVer)
    {
    	LibDocumentStyleBean documentstyleBean = getLibDocumentStyleBean();
    	return documentstyleBean != null && documentstyleVer != null && documentstyleVer.getVersion() < documentstyleBean.getProductionVersion();
    }
	
	public String getVersionLabel(DocumentStyleVersionInfo documentstyleVer) {
		if ( isTestVersion(documentstyleVer) )
			return Literals.VERSION_LABEL_TEST;
		if ( isProductionVersion(documentstyleVer) )
			return Literals.VERSION_LABEL_PRODUCTION;
		return Literals.VERSION_LABEL_NOT_CURRENT;
	}
	
	public boolean hasPermViewDetails() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_DELETE);
	}
	
	public boolean cancelChanges() {
		documentstyleForm.discard();
		documentstyleVerForm.discard();
		documentstyleForm.setReadOnly(true);
		documentstyleVerForm.setReadOnly(true);
		return true;
	}
	
	public boolean enterEditMode() {
		documentstyleForm.setReadOnly(false);
		documentstyleVerForm.setReadOnly(false);
		return true;
	}
	
	public boolean save() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
		if ( ! documentstyleForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		if ( ! documentstyleVerForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		documentstyleForm.commit();
		documentstyleVerForm.commit();
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			// If we're saving a new test version, we'll want to bump our documentstyle to show this as the new test version
			LibDocumentStyleBean currDocumentStyle = getLibDocumentStyleBean();
			LibDocumentStyleVersionBean currDocumentStyleVer = getLibDocumentStyleVersionBean();
			if ( currDocumentStyleVer.documentstyleVersion().getVersion() > currDocumentStyle.documentstyle().getTestVersion() )
				currDocumentStyle.documentstyle().bumpTestVersion();
			
			LibDocumentStyleBean savedDocumentStyle = documentstyleForm.save(con);
			if ( savedDocumentStyle == null )  {
				con.rollback();
				return false;
			}
			
			LibDocumentStyleVersionBean savedDocVer = documentstyleVerForm.save(con);
			if ( savedDocVer == null )  {
				con.rollback();
				return false;
			}
			
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Saved documentstyle " + savedDocumentStyle.getEsfName() + " version " + savedDocVer.getVersion() + ". DocumentStyleVersionId: " + savedDocVer.getId() + "; status: " + savedDocumentStyle.getStatus());
			}

			con.commit();
	    		
			documentstyleContainer.refresh();  // refresh our list after a change
			selectDocumentStyle(savedDocumentStyle);
			selectDocumentStyleVersion(savedDocVer);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean createNewDocumentStyle() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		DocumentStyleInfo documentstyleInfo = DocumentStyleInfo.Manager.createNew(getLibrary());
		LibDocumentStyleBean documentstyleBean = new LibDocumentStyleBean(documentstyleInfo);
		BeanItem<LibDocumentStyleBean> documentstyleItem = new BeanItem<LibDocumentStyleBean>(documentstyleBean);
        documentstyleForm.setItemDataSource(documentstyleItem);
		
		DocumentStyleVersionInfo documentstyleVerInfo = DocumentStyleVersionInfo.Manager.createNew(DocumentStyle.Manager.getById(documentstyleInfo.getId()),vaadinUi.getUser());
		LibDocumentStyleVersionBean documentstyleVerBean = new LibDocumentStyleVersionBean(documentstyleVerInfo,getVersionLabel(documentstyleVerInfo));
		BeanItem<LibDocumentStyleVersionBean> documentstyleVerItem = new BeanItem<LibDocumentStyleVersionBean>(documentstyleVerBean);
        documentstyleVerForm.setItemDataSource(documentstyleVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean createLikeDocumentStyle() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibDocumentStyleBean likeDocBean = getLibDocumentStyleBean();
		LibDocumentStyleVersionBean likeDocVerBean = getLibDocumentStyleVersionBean();
		
    	LibDocumentStyleBean createdDocBean = likeDocBean.createLike();
		BeanItem<LibDocumentStyleBean> createdDocItem = new BeanItem<LibDocumentStyleBean>(createdDocBean);
        documentstyleForm.setItemDataSource(createdDocItem);

    	DocumentStyleVersionInfo createdDocumentStyleVerInfo = DocumentStyleVersionInfo.Manager.createLike(DocumentStyle.Manager.getById(createdDocBean.documentstyleInfo().getId()), likeDocVerBean.documentstyleVersion(), vaadinUi.getUser());
    	LibDocumentStyleVersionBean createdDocumentStyleVerBean = new LibDocumentStyleVersionBean(createdDocumentStyleVerInfo,Literals.VERSION_LABEL_TEST); // The new documentstyle and version will always be Test.
		BeanItem<LibDocumentStyleVersionBean> createdDocumentStyleVerItem = new BeanItem<LibDocumentStyleVersionBean>(createdDocumentStyleVerBean);       
        documentstyleVerForm.setItemDataSource(createdDocumentStyleVerItem);
        
        setReadOnly(false);
		return true;
	}

	public boolean createNextVersion() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibDocumentStyleBean currDocumentStyleBean = getLibDocumentStyleBean();
		LibDocumentStyleVersionBean currDocumentStyleVerBean = getLibDocumentStyleVersionBean();
		
        documentstyleForm.setReadOnly(!hasPermUpdate()); // should have permission since doing a create next version...

    	DocumentStyleVersionInfo nextDocumentStyleVerInfo = DocumentStyleVersionInfo.Manager.createLike(currDocumentStyleBean.documentstyle(), currDocumentStyleVerBean.documentstyleVersion(), vaadinUi.getUser());
    	LibDocumentStyleVersionBean nextDocumentStyleVerBean = new LibDocumentStyleVersionBean(nextDocumentStyleVerInfo,Literals.VERSION_LABEL_TEST); // The new version will always be Test.
		BeanItem<LibDocumentStyleVersionBean> nextDocumentStyleVerItem = new BeanItem<LibDocumentStyleVersionBean>(nextDocumentStyleVerBean);
        documentstyleVerForm.setItemDataSource(nextDocumentStyleVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean promoteTestToProductionVersion() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibDocumentStyleVersionForm.versionChange.TestToProd.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibDocumentStyleBean documentstyleBean = getLibDocumentStyleBean();
			LibDocumentStyleVersionBean documentstyleVerBean = getLibDocumentStyleVersionBean();
			
			documentstyleBean.documentstyle().promoteTestVersionToProduction();
			
			if ( ! documentstyleBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved documentstyle " + documentstyleBean.getEsfName() + " version " + documentstyleVerBean.getVersion() + " into Production status. DocumentStyleVersionId: " + documentstyleVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved documentstyle " + documentstyleBean.getEsfName() + " version " + documentstyleVerBean.getVersion() + " into Production status. DocumentStyleVersionId: " + documentstyleVerBean.getId());
			}
			
			con.commit();
			documentstyleContainer.refresh();  // refresh our list after a change
			selectDocumentStyle(documentstyleBean);
			selectDocumentStyleVersion(documentstyleVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentStyleVersionForm.versionChange.TestToProd.success.message",documentstyleBean.getEsfName(),documentstyleVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean revertProductionVersionBackToTest() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibDocumentStyleVersionForm.versionChange.revertProdToTest.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibDocumentStyleBean documentstyleBean = getLibDocumentStyleBean();
			LibDocumentStyleVersionBean documentstyleVerBean = getLibDocumentStyleVersionBean();
			
			documentstyleBean.documentstyle().revertProductionVersionBackToTest();
			
			if ( ! documentstyleBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved Production documentstyle " + documentstyleBean.getEsfName() + " version " + documentstyleVerBean.getVersion() + " back into Test status. DocumentStyleVersionId: " + documentstyleVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved Production documentstyle " + documentstyleBean.getEsfName() + " version " + documentstyleVerBean.getVersion() + " back into Test status. DocumentStyleVersionId: " + documentstyleVerBean.getId());
			}
			
			con.commit();
			documentstyleContainer.refresh();  // refresh our list after a change
			selectDocumentStyle(documentstyleBean);
			selectDocumentStyleVersion(documentstyleVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentStyleVersionForm.versionChange.revertProdToTest.success.message",documentstyleBean.getEsfName(),documentstyleVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean delete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibDocumentStyleBean currDocumentStyle = getLibDocumentStyleBean();
			LibDocumentStyleVersionBean currDocumentStyleVer = getLibDocumentStyleVersionBean();
			
			// If there is no Production version, we'll get rid of the document style entirely, otherwise just the test version
			Errors errors = new Errors();
			if ( currDocumentStyle.getProductionVersion() == 0 )
				currDocumentStyle.documentstyle().delete(con,errors,vaadinUi.getUser());
			else {
				currDocumentStyleVer.documentstyleVersion().delete(con,errors,vaadinUi.getUser());
				currDocumentStyle.dropTestVersion();
				currDocumentStyle.save(con, errors);
			}
			
			if ( errors.hasError() ) {
				vaadinUi.show(errors);
				con.rollback();
				return false;
			}
			
			con.commit();
    		
			vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentStyleVersionForm.button.delete.successMessage",currDocumentStyle.getEsfName(),currDocumentStyleVer.getVersion()));
	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Deleted documentstyle " + currDocumentStyle.getEsfName() + " version " + currDocumentStyleVer.getVersion() + ". DocumentStyleVersionId: " + currDocumentStyleVer.getId() + "; status: " + currDocumentStyle.getStatus());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted documentstyle " + currDocumentStyle.getEsfName() + " version " + currDocumentStyleVer.getVersion() + ". DocumentStyleVersionId: " + currDocumentStyleVer.getId() + "; status: " + currDocumentStyle.getStatus());
			}

			documentstyleContainer.refresh();  // refresh our list after a change
			selectDocumentStyle(null);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		_logger.debug("activateView() - params: " + params);
		libraryId = new EsfUUID(params);
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return documentstyleForm.isDirty() ? documentstyleForm.checkDirty() : documentstyleVerForm.checkDirty();
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return documentstyleForm.isDirty() || documentstyleVerForm.isDirty();
	}
}