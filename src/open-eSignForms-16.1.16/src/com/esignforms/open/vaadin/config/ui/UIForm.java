// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.ui;

import java.util.Arrays;

import org.vaadin.jouni.animator.Disclosure;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.UI;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.EsfNameValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The UIForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class UIForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -1529735101134976209L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(UIForm.class);
	
	Button saveButton;
	Button createLikeButton;
	Button deleteButton;
	Button cancelButton;
	Button editButton;

	UIView view;
	UIViewContainer container;
    VerticalLayout layout;
	
	// We store the prevBean in case we abandon working on the newBean when we 'create like'
	UIBean prevBean;
    UIBean newBean;
    
    // We use this for all our select lists, so we only reload once per Bean being set as our datasource
	EsfUUID[] allGroupListIds;
	EsfPathName[] allGroupListNames;
	
	Label id;

	Disclosure permDisclosure;
	
	
	
	public UIForm(UIView view, UIViewContainer container) {
    	setStyleName("UIForm");
       	setWidth(100, Unit.PERCENTAGE); // hack because seems to have "horizontal scrollbar" issues if the form is shown with vertical scrollbars in place
    	this.view = view;
    	this.container = container;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
    	layout = new VerticalLayout();
       	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
    	
		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id);

		permDisclosure = new Disclosure(""); // we change the caption per each item being set into the form
		permDisclosure.setWidth(100, Unit.PERCENTAGE);
		permDisclosure.setVisible(false);
		layout.addComponent(permDisclosure);
		
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);

		User loggedInUser = vaadinUi.getUser();
		UI ui = new UI();
		if ( ui.showSaveButton(loggedInUser, UI.UI_UI_VIEW) ) {
			saveButton = new Button(vaadinUi.getMsg("button.save.label"), (ClickListener)this);
			//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
	    	//saveButton.setClickShortcut(KeyCode.ENTER);
	    	saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
	    	saveButton.setDescription(vaadinUi.getMsg("button.save.tooltip"));
	    	footer.addComponent(saveButton);
		}

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	if ( ui.canUpdate(loggedInUser, UI.UI_UI_VIEW) ) {
        	editButton = new Button(vaadinUi.getMsg("button.edit.label"), (ClickListener)this);
        	editButton.setIcon(new ThemeResource(vaadinUi.getMsg("UIForm.button.edit.icon")));
        	editButton.setDescription(vaadinUi.getMsg("button.edit.tooltip"));
        	footer.addComponent(editButton);
    	}

    	if ( ui.canCreateLike(loggedInUser, UI.UI_UI_VIEW) ) {
	    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
	    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("UIForm.button.createLike.icon")));
	    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
	    	footer.addComponent(createLikeButton);
		}

    	if ( ui.canDelete(loggedInUser, UI.UI_UI_VIEW) ) {
        	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
        	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("UIForm.button.delete.icon")));
        	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
        	deleteButton.addStyleName("deleteButton");
        	deleteButton.addStyleName("caution");
        	footer.addComponent(deleteButton);
    	}
    	         
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -3270866697262959370L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				// Load our cache if we don't have it yet since we'll need it for all our permission selection boxes
				if ( allGroupListIds == null ) {
					UIBean bean = getCurrentBean();
					allGroupListIds = bean.allGroupListIds();
					allGroupListNames = bean.allGroupListNames();
				}

				if ( propertyId.equals("permListIds") ) {
					return createPermissionSelectBox("UIForm.permList.label");
				} 
				
				if ( propertyId.equals("permViewDetailsIds") ) {
					return createPermissionSelectBox("UIForm.permViewDetails.label");
				}
				
				if ( propertyId.equals("permCreateLikeIds") ) {
					return createPermissionSelectBox("UIForm.permCreateLike.label");
				}
				
				if ( propertyId.equals("permUpdateIds") ) {
					return createPermissionSelectBox("UIForm.permUpdate.label");
				}
				
				if ( propertyId.equals("permDeleteIds") ) {
					return createPermissionSelectBox("UIForm.permDelete.label");
				}
				
				Field field = super.createField(item, propertyId, uiContext);
				field.setWidth(100, Unit.PERCENTAGE);
    			
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
                if (propertyId.equals("viewName")) {
                	TextField tf = (TextField)field;
                	tf.setCaption(vaadinUi.getMsg("UIForm.viewName.label"));
                	tf.setDescription(vaadinUi.getMsg("tooltip.esfName"));
                    tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    tf.addValidator(new EsfNameValidator());
                    tf.setConversionError(vaadinUi.getMsg("validator.esfname.message"));
                } else if ( propertyId.equals("description") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("caption.description"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.description"));
                }

                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
		allGroupListIds = null;
		allGroupListNames = null;
    }
    
    private TwinColSelect createPermissionSelectBox(String vaadinMessageCaption) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	TwinColSelect selectList = new TwinColSelect();
    	selectList.setLeftColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".left"));
    	selectList.setRightColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".right"));
    	selectList.setWidth(100, Unit.PERCENTAGE);
        selectList.addValidator(new SelectValidator(selectList));
		
		// Set all the possible values
		for( int i=0; i < allGroupListIds.length; ++i ) {
			selectList.addItem(allGroupListIds[i]);
		}
		// Associate labels with our ID values
		selectList.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		for( int i=0; i < allGroupListIds.length; ++i ) {
			selectList.setItemCaption(allGroupListIds[i], allGroupListNames[i].toPlainString());
		}
		
		selectList.setRows( Math.min(allGroupListIds.length,5) );
		selectList.setNullSelectionAllowed(true);
		selectList.setMultiSelect(true);
		return selectList;
    }
    
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
    			
    		// If we're saving a new bean
			Errors errors = new Errors();
    		if ( newBean != null ) {
        		if ( newBean.save(errors) ) {
            		UI.Manager.addView(newBean.getViewName());
        			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",newBean.getViewName()) );
            		// We need to add the new bean to the container
            		container.addItem(newBean);
            		view.select(newBean); // reselect our bean so all updates cleanly
            		prevBean = newBean = null; // and we're not longer working on a new bean
        		} else {
        			vaadinUi.show(errors);
        		}
    		} else {
    			UIBean currBean = getCurrentBean();
    			if ( currBean.save(errors) ) {
        			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",currBean.getViewName()) );
            		view.select(currBean); // reselect our bean so all updates cleanly
        		} else {
        			vaadinUi.show(errors);
        		}
    		}
        } else if ( source == cancelButton ) {
    		discard();
    		vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
        	if ( newBean != null ) {
        		setItemDataSource(null);
        		view.unselectAll(); // we'll unselect and the reselect our prevBean
        		view.select(prevBean);
        		prevBean = newBean = null; // we're not doing a new bean anymore
        	}
        	setReadOnly(true);
        } else if ( source == editButton ) {
        	setReadOnly(false);
        } else if ( source == deleteButton ) {
        	discard();
        	UIBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		setItemDataSource(null);
        		view.unselectAll(); // we'll unselect it and remove it from our container
        		if ( newBean == null ) {
            		container.removeItem(currBean);
        			//if ( currBean.delete() ) {
                	//	vaadinUi.showStatus( this, vaadinUi.getMsg("form.delete.success.message",currBean.getPathName()) );
        			//}
        		} else {
            		view.select(prevBean);
        			prevBean = newBean = null;
        		}
        	}	
        } else if ( source == createLikeButton ) {
        	UIBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		createLike(currBean);
        	}	
        }
    }
	
	public String getCurrentBeanName() {
		UIBean bean = getCurrentBean();
		return bean == null ? "(None)" : bean.getViewName().toPlainString();
	}
    
	UIBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public UIBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<UIBean> bi = (BeanItem<UIBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached list
		resetCaches();
		
    	if (newDataSource != null) {
    		UIBean bean = getBean(newDataSource);

    		super.setItemDataSource(newDataSource, Arrays.asList("viewName","description","permListIds","permViewDetailsIds","permCreateLikeIds","permUpdateIds","permDeleteIds"));
    		
    		setupForm(bean);
    		
    		setReadOnly( ! bean.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		newBean = bean.permission().doInsert() ? bean : null;
    		/*
    		if ( newBean != null ) {
    			TextField tf = (TextField)getField("viewName");
    			tf.selectAll();
    			tf.focus();
    		}
    		*/
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	UIBean bean = getCurrentBean();
    	
    	// We have these only while editing/creating new
    	if ( saveButton != null ) {
    		saveButton.setVisible(!readOnly);
    	}
    	cancelButton.setVisible(!readOnly);

    	// We have this only while in view mode
    	if ( editButton != null ) {
        	editButton.setVisible(readOnly && bean != null && bean.hasPermUpdate());
    	}
    	
    	// This one we show this in all modes if has permission
    	if ( createLikeButton != null ) {
        	createLikeButton.setVisible(bean != null && bean.hasPermCreateLike() && bean.permission().doUpdate()); // only want create like on an existing bean
    	}
    	
    	if ( deleteButton != null ) {
        	deleteButton.setVisible(bean != null && bean.hasPermDelete());
    	}	
    }
    
    public void createLike(UIBean likeBean) {
        // Create a temporary item for the form
    	UIBean createdBean = likeBean.createLike();
        
        BeanItem<UIBean> bi = new BeanItem<UIBean>( createdBean );
        setItemDataSource(bi);
    	prevBean = likeBean;
        setReadOnly(false);
    }
    
    void setupForm(UIBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("UIForm.label.id",bean.getId(),bean.getPathName()) );

		createPermDisclosure(bean);
    }
    
    void createPermDisclosure(UIBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	VerticalLayout permLayout = new VerticalLayout();
    	permLayout.setMargin(false);
    	permLayout.setSpacing(true);
    	permLayout.setWidth(100, Unit.PERCENTAGE);
    	
		permLayout.addComponent(getField("permListIds"));
		permLayout.addComponent(getField("permViewDetailsIds"));
		permLayout.addComponent(getField("permCreateLikeIds"));
		permLayout.addComponent(getField("permUpdateIds"));
		permLayout.addComponent(getField("permDeleteIds"));
				
		permDisclosure.setContent(permLayout);
		permDisclosure.setDisclosureCaption(vaadinUi.getMsg("UIForm.permDisclosure.label",bean.getDescription()));
		permDisclosure.setVisible(true);
		permDisclosure.open();
	}

    
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("viewName")) {
        	layout.addComponent(field, 1); // after our id label
        } else if (propertyId.equals("description")) {
            layout.addComponent(field, 2);
        }
    }

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("UIView.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}