// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.reportField;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionReportField;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.PackageVersionReportFieldReportFieldNameValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Validator;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.AbstractSelect.NewItemHandler;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.VerticalLayout;

/**
 * The PackageReportFieldForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class PackageReportFieldForm extends CustomComponent implements EsfView, ClickListener {
	private static final long serialVersionUID = 8085447718845950397L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackageReportFieldForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;

	HorizontalLayout fieldLayout;
	FieldGroup fieldGroup;
	
	final PackageReportFieldForm thisForm;
	final PackageVersion duplicatedPackageVersion;
	final PackageReportFieldView view;
	final PackageReportFieldContainer container;
	boolean containerOrderHasChanged = false;
	
	List<EsfUUID> allDocumentIds;
	List<EsfName> allDocumentNames;

	List<EsfName> allReportFieldTemplateNames;
	List<String> allReportFieldTemplateLabels;
	
	PackageVersionReportField newPackageVersionReportField; // when set, we're working on a new report field.
	
	
	public PackageReportFieldForm(PackageReportFieldView viewParam, final PackageReportFieldContainer containerParam, PackageVersion duplicatedPackageVersionParam)	{
		setStyleName("PackageReportFieldForm");
		this.thisForm = this;
		this.view = viewParam;
		this.container = containerParam;
		this.duplicatedPackageVersion = duplicatedPackageVersionParam;
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		fieldGroup = new FieldGroup();
    	fieldGroup.setBuffered(true);

    	allDocumentIds = new LinkedList<EsfUUID>(duplicatedPackageVersion.getDocumentIdList());
    	allDocumentNames = new LinkedList<EsfName>();
    	for( EsfUUID docId : allDocumentIds ) {
    		Document doc = Document.Manager.getById(docId);
    		if ( doc == null ) {
    			_logger.warn("PackageReportFieldForm() - Could not find document with id: " + docId);
    			allDocumentNames.add(docId.toEsfName());
    		} else {
    			allDocumentNames.add(doc.getEsfName());
    		}
    	}

    	allReportFieldTemplateNames = new LinkedList<EsfName>();
    	allReportFieldTemplateLabels = new LinkedList<String>();
    	
    	// Setup layout
    	VerticalLayout layout = new VerticalLayout();
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setWidth(100, Unit.PERCENTAGE);
    	setCompositionRoot(layout);

    	fieldLayout = new HorizontalLayout();
    	fieldLayout.setMargin(false);
    	fieldLayout.setSpacing(true);
    	fieldLayout.setWidth(100,  Unit.PERCENTAGE);
    	fieldLayout.setVisible(false);
    	layout.addComponent(fieldLayout);
    	
    	NativeSelect documentId = new NativeSelect(vaadinUi.getMsg("PackageReportFieldForm.documentId.label"));
    	documentId.setDescription(vaadinUi.getMsg("PackageReportFieldForm.documentId.tooltip"));
    	documentId.setNullSelectionAllowed(false);
    	documentId.setRequired(true);
    	documentId.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	documentId.setImmediate(true);
    	documentId.addValidator(new SelectValidator(documentId));
    	documentId.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
    	for( EsfUUID docId : allDocumentIds ) {
    		documentId.addItem(docId);
    		documentId.setItemCaption(docId, nameIter.next().toString());
    	}
    	documentId.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = -5157819134939772836L;

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
				if ( newDocId != null && ! newDocId.isNull() ) {
					setupAllFieldNames(newDocId);
				}
			}
    	});
    	fieldLayout.addComponent(documentId);
    	fieldGroup.bind(documentId, "documentId");
				
    	NativeSelect fieldName = new NativeSelect(vaadinUi.getMsg("PackageReportFieldForm.fieldName.label"));
    	fieldName.setDescription(vaadinUi.getMsg("PackageReportFieldForm.fieldName.tooltip"));
    	fieldName.setNullSelectionAllowed(false);
    	fieldName.setRequired(true);
    	fieldName.setImmediate(true);
    	fieldName.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	fieldName.addValidator(new SelectValidator(fieldName));
    	fieldName.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = -7076177861786930376L;

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				EsfName newFieldName = (EsfName)event.getProperty().getValue();
				if ( newFieldName != null && newFieldName.isValid() ) {
					setupReportFieldTemplateName(newFieldName);
				}
			}
    	});
    	fieldLayout.addComponent(fieldName);
    	fieldGroup.bind(fieldName, "fieldName");
                
    	final ComboBox reportFieldTemplateName = new ComboBox(vaadinUi.getMsg("PackageReportFieldForm.reportFieldTemplateName.label"));
    	reportFieldTemplateName.setDescription(vaadinUi.getMsg("PackageReportFieldForm.reportFieldTemplateName.tooltip"));
    	reportFieldTemplateName.setNullSelectionAllowed(false);
    	reportFieldTemplateName.setRequired(true);
    	reportFieldTemplateName.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	reportFieldTemplateName.setImmediate(true);
    	reportFieldTemplateName.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	reportFieldTemplateName.setFilteringMode(FilteringMode.STARTSWITH);
    	reportFieldTemplateName.setNewItemsAllowed(true);
    	reportFieldTemplateName.setNewItemHandler( new NewItemHandler() {
			private static final long serialVersionUID = -2766891516983926653L;

			@Override
			public void addNewItem(String newItemCaption) {
				setOrAddNewItemToComboBox(reportFieldTemplateName,newItemCaption);
			}
    	});
    	reportFieldTemplateName.addValidator(new PackageVersionReportFieldReportFieldNameValidator(thisForm,container));
    	fieldLayout.addComponent(reportFieldTemplateName);
    	fieldGroup.bind(reportFieldTemplateName,"reportFieldTemplateName");
                
    	CheckBox showToDo = new CheckBox(vaadinUi.getMsg("PackageReportFieldForm.showToDo.label"));
    	showToDo.setImmediate(false);
    	showToDo.setDescription(vaadinUi.getMsg("PackageReportFieldForm.showToDo.tooltip"));
    	fieldLayout.addComponent(showToDo);
    	fieldGroup.bind(showToDo,"showToDo");

    	Label notice = new Label(vaadinUi.getMsg("PackageReportFieldForm.notice"));
    	layout.addComponent(notice);
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    	footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	layout.addComponent(footer);
    	
    	_logger.debug("Form created");

	}

	FieldTemplate getFieldTemplate(PackageVersionReportField pvrf) {
		Document document = Document.Manager.getById(pvrf.getDocumentId());
		if ( document == null )
			return null;
		DocumentVersion docVersion;
		com.esignforms.open.prog.Package pkg = duplicatedPackageVersion.getPackage();
		if ( pkg.getProductionVersion() == duplicatedPackageVersion.getVersion() )
			docVersion = document.getProductionDocumentVersion();
		else
			docVersion = document.getTestDocumentVersion();
		return docVersion.getFieldTemplate(pvrf.getFieldName());
	}
	
	void setContainerOrderHasChanged() {
		containerOrderHasChanged = true;
	}
	
	void setupAllFieldNames(EsfUUID newDocId) {
		NativeSelect targetField = (NativeSelect)fieldGroup.getField("fieldName");
		if ( targetField == null )
			return;
		EsfName targetFieldValue = (EsfName)targetField.getValue(); // save value before we empty the list
		targetField.removeAllItems();
		
		Document document = Document.Manager.getById(newDocId);
		if ( document == null )
			return; 
		DocumentVersion docVersion;
		com.esignforms.open.prog.Package pkg = duplicatedPackageVersion.getPackage();
		if ( pkg.getProductionVersion() == duplicatedPackageVersion.getVersion() )
			docVersion = document.getProductionDocumentVersion();
		else
			docVersion = document.getTestDocumentVersion();
		for( EsfName fieldName : docVersion.getFieldTemplateMap().keySet() )
			targetField.addItem(fieldName);
		if ( targetFieldValue != null && targetFieldValue.isValid() && targetField.containsId(targetFieldValue) ) {
			try {
				targetField.setValue(targetFieldValue); // reset value back
				targetField.commit(); // don't let this cause the field to be marked as modified
			} catch( Exception e ) {}
		}
	}
	
	void setupReportFieldTemplateName(EsfName newDocFieldName) {
		ComboBox targetField = (ComboBox)fieldGroup.getField("reportFieldTemplateName");
		if ( targetField == null || newDocFieldName == null || ! newDocFieldName.isValid() )
			return;
		EsfName targetFieldValue = (EsfName)targetField.getValue(); // save value before we empty the list
		if ( newDocFieldName.equals(targetFieldValue) )
			return;
		
		setOrAddNewItemToComboBox(targetField,newDocFieldName.toString());
	}
	
	void setOrAddNewItemToComboBox(ComboBox combo,String newItemCaption) {
	    // Checks for readonly
	    if (isReadOnly()) {
	        throw new Property.ReadOnlyException();
	    }

		if ( EsfString.isNonBlank(newItemCaption ) ) {
			if ( newItemCaption.startsWith("+") )
				newItemCaption = newItemCaption.substring(1);
			EsfName newReportFieldName = new EsfName(newItemCaption);
			if ( newReportFieldName.isValid() ) {
				if ( combo.containsId(newReportFieldName) ) {
					combo.setValue(newReportFieldName);
				} else {
					combo.addItem(newReportFieldName);
					combo.setItemCaption(newReportFieldName, "+"+newReportFieldName);
					combo.setValue(newReportFieldName);
					combo.setPageLength(Math.min(20, combo.size()));
				}
			}
		}
	}

	
	void setupAllReportFieldTemplates() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		PackageVersionReportField currBean = getCurrentBean();
		
		allReportFieldTemplateNames.clear();
		allReportFieldTemplateLabels.clear();
		
    	List<ReportFieldTemplate> reportFieldTemplateList = ReportFieldTemplate.Manager.getAllNonBuiltIn();
    	for( ReportFieldTemplate rft : reportFieldTemplateList ) {
    		short existingOrder = container.getOrderOfReportFieldTemplateId(rft.getId());
    		if ( existingOrder < 0 || (currBean != null && currBean.getFieldOrder() == existingOrder) )
    		{
        		allReportFieldTemplateNames.add(rft.getFieldName());
        		allReportFieldTemplateLabels.add(rft.getFieldName() + " (" + vaadinUi.getPrettyCode().reportFieldTemplateType(rft.getFieldType()) + ")");
    		}
    	}
    	
    	ComboBox combo = (ComboBox)fieldGroup.getField("reportFieldTemplateName");
    	if ( combo != null ) {
    		setupReportFieldTemplateComboBox(combo);
    	}
	}
	
	void setupReportFieldTemplateComboBox(ComboBox combo) {
		EsfName targetFieldValue = (EsfName)combo.getValue(); // save value before we empty the list
		combo.removeAllItems();
		
		ListIterator<String> labelIter = allReportFieldTemplateLabels.listIterator();
		for( EsfName name : allReportFieldTemplateNames ) {
			combo.addItem(name);
			combo.setItemCaption(name, labelIter.next());
		}
		
    	combo.setPageLength(Math.min(20, combo.size()));

		if ( targetFieldValue != null && combo.containsId(targetFieldValue) ) {
			try {
				combo.setValue(targetFieldValue); // reset value back
				combo.commit(); // don't let this cause the field to be marked as modified
			} catch( Exception e ) {}
		}
	}


	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		PackageVersionReportField currBean = getCurrentBean();
    		if ( currBean == null ) {
    			boolean saveOnReorder = containerOrderHasChanged;
    			fieldGroup.discard();
    			saveReportFields();
    			if ( ! saveOnReorder )
    				view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! fieldGroup.isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
        	
			try {
				fieldGroup.commit();

				// If we are saving without an ID, we assume we're creating a new report field template with the specified name
				if ( currBean.getReportFieldTemplateId() == null ) {
					try {
						setupNewReportFieldTemplate(currBean);
					} catch( Validator.InvalidValueException e ) {
						return;
					}
				}

				if ( newPackageVersionReportField != null ) {
	    			container.addItem(currBean);
	    			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",vaadinUi.getMsg("PackageReportFieldForm.object.name")) );
	    			newPackageVersionReportField = null;
	    		} else {
	    			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("PackageReportFieldForm.object.name")) );
	    		}
	    		
	    		saveReportFields();
	    		
	    		view.select(currBean); // reselect our bean so all updates cleanly
			} catch (CommitException e) {
        		_logger.error("fieldGroup.commit()",e);
			}
        } else if ( source == cancelButton ) {
    		fieldGroup.discard();
    		containerOrderHasChanged = false;
    		if ( fieldGroup.getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			setNewReportFieldAsDataSource(null);
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	fieldGroup.discard();
        	view.closeParentWindow();
        } else if ( source == createLikeButton ) {
        	fieldGroup.discard();
        	PackageVersionReportField reportField = getCurrentBean();
        	if ( reportField != null ) {
    			view.unselectAll();
    			PackageVersionReportField newReportField = PackageVersionReportField.Manager.createLike(reportField.getPackageVersionId(), reportField);
    			newReportField.setFieldOrder((short)(container.size()+1));
    			setNewReportFieldAsDataSource(newReportField);
        	}
        } else if ( source == deleteButton ) {
        	fieldGroup.discard();
        	
        	if ( newPackageVersionReportField != null ) {
        		newPackageVersionReportField = null;
        	} else {
            	PackageVersionReportField reportField = getCurrentBean();
            	if ( reportField != null ) {
            		container.removeItem(reportField);
        			saveReportFields();
            	}
        	}
    		view.unselectAll();
			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("PackageReportFieldForm.object.name")) );
        }
    }
	
	void setupNewReportFieldTemplate(PackageVersionReportField pvrf) throws Validator.InvalidValueException {
		EsfName newReportFieldTemplateName = pvrf.getReportFieldTemplateName();
		// Let's see if we already have a report template with this name. If not, let's create one.
		ReportFieldTemplate reportFieldTemplate = ReportFieldTemplate.Manager.getByName(newReportFieldTemplateName);
		if ( reportFieldTemplate == null ) {
			reportFieldTemplate = ReportFieldTemplate.Manager.createNew(getFieldTemplate(pvrf));
			reportFieldTemplate.setFieldName(newReportFieldTemplateName); // but use our new name
			if ( reportFieldTemplate.save() ) {
				ComboBox reportFieldTemplateNameField = (ComboBox)fieldGroup.getField("reportFieldTemplateName");
				reportFieldTemplateNameField.setValue(reportFieldTemplate.getFieldName());
				reportFieldTemplateNameField.validate();
			}
		}
		pvrf.setReportFieldTemplateId(reportFieldTemplate.getId());
	}
	
	void saveReportFields() {
		// Get all of the fields in the container and save them back to the package version
		LinkedList<PackageVersionReportField> list = new LinkedList<PackageVersionReportField>();
		
		for( PackageVersionReportField reportField : container.getItemIds() )
			list.add(reportField);
		
		duplicatedPackageVersion.setReportFieldList(list);
		containerOrderHasChanged = false;
		newPackageVersionReportField = null;
		container.refresh(duplicatedPackageVersion.getPackageVersionReportFieldList());
	}
	
    // Used when a new report field is created. We'll default the document id to the first document id our list, or a "fake" id if we have no documents.
    public EsfUUID getNewReportFieldDocumentId()
    {
    	return ( allDocumentIds == null || allDocumentIds.size() == 0 ) ? new EsfUUID() : allDocumentIds.get(0);
    }

	public PackageVersionReportField getCurrentBean() {
    	return getBean(fieldGroup.getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public PackageVersionReportField getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<PackageVersionReportField> bi = (BeanItem<PackageVersionReportField>)dataSource;
		return bi.getBean();
    }
    
    public void setNewReportFieldAsDataSource(PackageVersionReportField reportField) {
    	newPackageVersionReportField = reportField;
    	if ( newPackageVersionReportField == null )
    		setItemDataSource(null);
    	else
    		setItemDataSource(new BeanItem<PackageVersionReportField>(newPackageVersionReportField));
    }
    
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		if ( newDataSource != null ) {
			fieldGroup.setItemDataSource(newDataSource);
			setupAllReportFieldTemplates();
    		fieldLayout.setVisible(true);
		} else {
			fieldGroup.setItemDataSource(null);
			fieldLayout.setVisible(false);
		}
		setReadOnly(isReadOnly());
    }

	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	PackageVersionReportField bean = getCurrentBean();
    	if ( bean != null )
    		fieldGroup.setReadOnly(readOnly);
    	
    	saveButton.setVisible(!readOnly && (bean != null || containerOrderHasChanged));
    	cancelButton.setVisible(!readOnly && (bean != null || containerOrderHasChanged));
    	closeButton.setVisible(true);
    	createLikeButton.setVisible(!readOnly && bean != null && newPackageVersionReportField == null);
    	deleteButton.setVisible(!readOnly && bean != null);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

    @Override
	public String checkDirty() {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageReportFieldView.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return fieldGroup.isModified() || newPackageVersionReportField != null || containerOrderHasChanged;
	}
}