// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.emailtemplate;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.EmailTemplateInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibEmailTemplateBean implements Serializable, Comparable<LibEmailTemplateBean> {
	private static final long serialVersionUID = 5355828495077654070L;

	private EmailTemplateInfo emailtemplateInfo;

	private EmailTemplate emailtemplate;
	
	public LibEmailTemplateBean(EmailTemplateInfo emailtemplateInfo) {
		this.emailtemplateInfo = emailtemplateInfo;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibEmailTemplateBean )
			return emailtemplateInfo.equals(((LibEmailTemplateBean)o).emailtemplateInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return emailtemplateInfo.hashCode();
    }
	public int compareTo(LibEmailTemplateBean d) {
		return emailtemplateInfo.compareTo(d.emailtemplateInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public EmailTemplateInfo emailtemplateInfo() {
		return emailtemplateInfo;
	}

	public EmailTemplate emailtemplate() {
		if ( emailtemplate == null )
			emailtemplate = EmailTemplate.Manager.getById(emailtemplateInfo.getId());
		return emailtemplate;
	}

	public LibEmailTemplateBean createLike() {
		EmailTemplateInfo newEmailTemplateInfo = EmailTemplateInfo.Manager.createLike(emailtemplate, emailtemplate.getEsfName());	    
	    return new LibEmailTemplateBean(newEmailTemplateInfo);
	}
	
	
	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( emailtemplate().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			emailtemplateInfo = EmailTemplateInfo.Manager.createFromSource(emailtemplate);
			return true;
		} 
		
		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}

	
	// Now for the JavaBeans methods
	public EsfName getEsfName() {
		return emailtemplateInfo.getEsfName();
	}
	public void setEsfName(EsfName v) {
		emailtemplate.setEsfName(v);
	}

	public EsfUUID getId() {
		return emailtemplateInfo.getId();
	}
	
	public String getDescription() {
		return emailtemplateInfo.getDescription();
	}
	public void setDescription(String v) {
		emailtemplate().setDescription(v);
	}

	public boolean isEnabled() {
		return emailtemplateInfo.isEnabled();
	}
	public String getStatus() {
		return emailtemplateInfo.getStatus();
	}
	public void setStatus(String v) {
		emailtemplate().setStatus(v);
	}	
	
	public Integer getProductionVersion() {
		return emailtemplateInfo.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		return emailtemplateInfo.getTestVersion();
	}
	
	public void bumpTestVersion() {
		emailtemplate().bumpTestVersion();
	}
	public void dropTestVersion() {
		emailtemplate().dropTestVersion();
	}
	
	public String getComments() {
		return emailtemplate().getComments();
	}
	public void setComments(String v) {
		emailtemplate().setComments(v);
	}
}