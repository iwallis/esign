// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.dropdown;

import java.io.Serializable;
import java.util.Collection;

import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.DropDownVersionInfo;
import com.vaadin.data.util.BeanItemContainer;


public class LibDropDownVersionBeanContainer extends BeanItemContainer<LibDropDownVersionBean> implements Serializable {
	private static final long serialVersionUID = -8301802608016430496L;

	final LibDropDownAndVersionsMainView view;
	
	public LibDropDownVersionBeanContainer(LibDropDownAndVersionsMainView view) throws InstantiationException, IllegalAccessException {
		super(LibDropDownVersionBean.class);
		this.view = view;

		// No items exist until a drop down is selected and set to this re-loadable container.
	}
	
	public void reload(DropDown dropDown) {
		removeAllItems();
		
		// Users need update permission to program its contents
		if ( dropDown != null ) {
			
			Collection<DropDownVersionInfo> dropDownVers = DropDownVersionInfo.Manager.getAll(dropDown);
			for( DropDownVersionInfo d : dropDownVers ) {
				addItem( new LibDropDownVersionBean(d,view.getVersionLabel(d)) );			
			}
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}