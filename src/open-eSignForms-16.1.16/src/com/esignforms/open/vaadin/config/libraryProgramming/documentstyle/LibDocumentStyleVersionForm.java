// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.documentstyle;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.vaadin.dialogs.ConfirmDialog;

import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.NativeSelectDropDown;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * The LibDocumentStyleVersionForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class LibDocumentStyleVersionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 458441018671643428L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibDocumentStyleVersionForm.class);
	
	static List<String> _orderedProperties;
	
	final LibDocumentStyleVersionForm thisForm;
	final LibDocumentStyleAndVersionsMainView view;
	LibDocumentStyleVersionBeanContainer container;
	GridLayout layout;
	
	Label id;
	Label createdByInfo;
	Label lastUpdatedByInfo;
	Label versionInfo;
	
	Label documentSection;
	Label signatureSection;
	Label fieldDataSection;
	Label inputFieldRequiredDataSection;
	Label inputFieldOptionalDataSection;
	Label inputFieldErrorDataSection;
	Label normalLabelSection;
	Label normalLabelErrorSection;
	Label smallLabelSection;
	Label smallLabelErrorSection;
	
	Button testToProdButton;
	Button createTestFromProdButton;
	Button revertProdToTestButton;
	Button deleteButton;

	public LibDocumentStyleVersionForm(LibDocumentStyleAndVersionsMainView view, LibDocumentStyleVersionBeanContainer container) {
    	setStyleName("LibDocumentStyleVersionForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.view = view;
    	this.container = container;
    	this.thisForm = this;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);

    	layout = new GridLayout(6,24);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
		
		versionInfo = new Label();
		versionInfo.setContentMode(ContentMode.TEXT);	
		layout.addComponent(versionInfo,0,0,5,0);
		
		lastUpdatedByInfo = new Label();
		lastUpdatedByInfo.setContentMode(ContentMode.TEXT);
		lastUpdatedByInfo.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedByInfo,0,21,5,21);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,22,5,22);		

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id,0,23,5,23);
		
		documentSection = new Label(vaadinUi.getMsg("LibDocumentStyleVersionForm.documentSection.label.html"));
		documentSection.setContentMode(Label.CONTENT_XHTML);
		documentSection.setVisible(false);
		layout.addComponent(documentSection,0,1,2,1);

		signatureSection = new Label(vaadinUi.getMsg("LibDocumentStyleVersionForm.signatureSection.label.html"));
		signatureSection.setContentMode(Label.CONTENT_XHTML);
		signatureSection.setVisible(false);
		layout.addComponent(signatureSection,0,3,2,3);

		fieldDataSection = new Label(vaadinUi.getMsg("LibDocumentStyleVersionForm.fieldDataSection.label.html"));
		fieldDataSection.setContentMode(Label.CONTENT_XHTML);
		fieldDataSection.setVisible(false);
		layout.addComponent(fieldDataSection,0,5,2,5);

		inputFieldRequiredDataSection = new Label(vaadinUi.getMsg("LibDocumentStyleVersionForm.inputFieldRequiredDataSection.label.html"));
		inputFieldRequiredDataSection.setContentMode(Label.CONTENT_XHTML);
		inputFieldRequiredDataSection.setVisible(false);
		layout.addComponent(inputFieldRequiredDataSection,0,7,2,7);

		inputFieldOptionalDataSection = new Label(vaadinUi.getMsg("LibDocumentStyleVersionForm.inputFieldOptionalDataSection.label.html"));
		inputFieldOptionalDataSection.setContentMode(Label.CONTENT_XHTML);
		inputFieldOptionalDataSection.setVisible(false);
		layout.addComponent(inputFieldOptionalDataSection,0,9,2,9);

		inputFieldErrorDataSection = new Label(vaadinUi.getMsg("LibDocumentStyleVersionForm.inputFieldErrorDataSection.label.html"));
		inputFieldErrorDataSection.setContentMode(Label.CONTENT_XHTML);
		inputFieldErrorDataSection.setVisible(false);
		layout.addComponent(inputFieldErrorDataSection,0,11,2,11);

		normalLabelSection = new Label(vaadinUi.getMsg("LibDocumentStyleVersionForm.normalLabelSection.label.html"));
		normalLabelSection.setContentMode(Label.CONTENT_XHTML);
		normalLabelSection.setVisible(false);
		layout.addComponent(normalLabelSection,0,13,2,13);

		normalLabelErrorSection = new Label(vaadinUi.getMsg("LibDocumentStyleVersionForm.normalLabelErrorSection.label.html"));
		normalLabelErrorSection.setContentMode(Label.CONTENT_XHTML);
		normalLabelErrorSection.setVisible(false);
		layout.addComponent(normalLabelErrorSection,0,15,2,15);

		smallLabelSection = new Label(vaadinUi.getMsg("LibDocumentStyleVersionForm.smallLabelSection.label.html"));
		smallLabelSection.setContentMode(Label.CONTENT_XHTML);
		smallLabelSection.setVisible(false);
		layout.addComponent(smallLabelSection,0,17,2,17);

		smallLabelErrorSection = new Label(vaadinUi.getMsg("LibDocumentStyleVersionForm.smallLabelErrorSection.label.html"));
		smallLabelErrorSection.setContentMode(Label.CONTENT_XHTML);
		smallLabelErrorSection.setVisible(false);
		layout.addComponent(smallLabelErrorSection,0,19,2,19);

		HorizontalLayout footer = new HorizontalLayout();
		footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);
    	
    	testToProdButton = new Button(vaadinUi.getMsg("LibDocumentStyleVersionForm.button.testToProd.label"), (ClickListener)this);
    	testToProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentStyleVersionForm.button.testToProd.icon")));
    	testToProdButton.setDescription(vaadinUi.getMsg("LibDocumentStyleVersionForm.button.testToProd.tooltip"));
    	footer.addComponent(testToProdButton);

    	createTestFromProdButton = new Button(vaadinUi.getMsg("LibDocumentStyleVersionForm.button.createTestFromProd.label"), (ClickListener)this);
    	createTestFromProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentStyleVersionForm.button.createTestFromProd.icon")));
    	createTestFromProdButton.setDescription(vaadinUi.getMsg("LibDocumentStyleVersionForm.button.createTestFromProd.tooltip"));
    	footer.addComponent(createTestFromProdButton);

    	revertProdToTestButton = new Button(vaadinUi.getMsg("LibDocumentStyleVersionForm.button.revertProdToTest.label"), (ClickListener)this);
    	revertProdToTestButton.addStyleName("caution");
    	revertProdToTestButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentStyleVersionForm.button.revertProdToTest.icon")));
    	revertProdToTestButton.setDescription(vaadinUi.getMsg("LibDocumentStyleVersionForm.button.revertProdToTest.tooltip"));
    	footer.addComponent(revertProdToTestButton);

    	if ( view.hasPermDelete() ) {
    		deleteButton = new Button(vaadinUi.getMsg("LibDocumentStyleVersionForm.button.delete.label"), (ClickListener)this);
    		deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentStyleVersionForm.button.delete.icon")));
    		deleteButton.setDescription(vaadinUi.getMsg("LibDocumentStyleVersionForm.button.delete.tooltip"));
	    	deleteButton.addStyleName("deleteButton");
	    	deleteButton.addStyleName("caution");
	    	footer.addComponent(deleteButton);
		}
    	
    	setFooter(footer);
    	setReadOnly(true);   	

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -5968472163263210014L;			
			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				if ( propertyId.equals("documentFont") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.font.label"), vaadinUi.getEsfapp().getDropDownFontEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.font.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("documentFontSize") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontsize.label"), vaadinUi.getEsfapp().getDropDownFontSizeEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontsize.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("documentFontStyle") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontstyle.label"), vaadinUi.getEsfapp().getDropDownFontStyleEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontstyle.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("documentFontColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontcolor.label"), vaadinUi.getEsfapp().getDropDownFontColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("documentBackgroundColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.backgroundcolor.label"), vaadinUi.getEsfapp().getDropDownBackgroundColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.backgroundcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				

				if ( propertyId.equals("signatureFont") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.font.label"), vaadinUi.getEsfapp().getDropDownFontEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.font.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("signatureFontSize") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontsize.label"), vaadinUi.getEsfapp().getDropDownFontSizeEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontsize.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("signatureFontStyle") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontstyle.label"), vaadinUi.getEsfapp().getDropDownFontStyleEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontstyle.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("signatureFontColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontcolor.label"), vaadinUi.getEsfapp().getDropDownFontColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("signatureBackgroundColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.backgroundcolor.label"), vaadinUi.getEsfapp().getDropDownBackgroundColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.backgroundcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				

				if ( propertyId.equals("fieldDataFont") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.font.label"), vaadinUi.getEsfapp().getDropDownFontEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.font.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("fieldDataFontSize") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontsize.label"), vaadinUi.getEsfapp().getDropDownFontSizeEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontsize.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("fieldDataFontStyle") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontstyle.label"), vaadinUi.getEsfapp().getDropDownFontStyleEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontstyle.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("fieldDataFontColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontcolor.label"), vaadinUi.getEsfapp().getDropDownFontColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("fieldDataBackgroundColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.backgroundcolor.label"), vaadinUi.getEsfapp().getDropDownBackgroundColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.backgroundcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				

				if ( propertyId.equals("inputFieldRequiredFont") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.font.label"), vaadinUi.getEsfapp().getDropDownFontEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.font.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldRequiredFontSize") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontsize.label"), vaadinUi.getEsfapp().getDropDownFontSizeEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontsize.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldRequiredFontStyle") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontsize.label"), vaadinUi.getEsfapp().getDropDownFontStyleEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontstyle.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldRequiredFontColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontcolor.label"), vaadinUi.getEsfapp().getDropDownFontColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldRequiredBackgroundColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.backgroundcolor.label"), vaadinUi.getEsfapp().getDropDownBackgroundColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.backgroundcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldRequiredBorderTypes") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.bordertypes.label"), vaadinUi.getEsfapp().getDropDownBorderTypesEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.bordertypes.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				

				if ( propertyId.equals("inputFieldOptionalFont") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.font.label"), vaadinUi.getEsfapp().getDropDownFontEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.font.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldOptionalFontSize") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontsize.label"), vaadinUi.getEsfapp().getDropDownFontSizeEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontsize.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldOptionalFontStyle") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontsize.label"), vaadinUi.getEsfapp().getDropDownFontStyleEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontstyle.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldOptionalFontColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontcolor.label"), vaadinUi.getEsfapp().getDropDownFontColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldOptionalBackgroundColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.backgroundcolor.label"), vaadinUi.getEsfapp().getDropDownBackgroundColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.backgroundcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldOptionalBorderTypes") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.bordertypes.label"), vaadinUi.getEsfapp().getDropDownBorderTypesEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.bordertypes.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				

				if ( propertyId.equals("inputFieldErrorFont") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.font.label"), vaadinUi.getEsfapp().getDropDownFontEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.font.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldErrorFontSize") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontsize.label"), vaadinUi.getEsfapp().getDropDownFontSizeEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontsize.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldErrorFontStyle") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontsize.label"), vaadinUi.getEsfapp().getDropDownFontStyleEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontstyle.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldErrorFontColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontcolor.label"), vaadinUi.getEsfapp().getDropDownFontColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldErrorBackgroundColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.backgroundcolor.label"), vaadinUi.getEsfapp().getDropDownBackgroundColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.backgroundcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("inputFieldErrorBorderTypes") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.bordertypes.label"), vaadinUi.getEsfapp().getDropDownBorderTypesEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.bordertypes.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				

				if ( propertyId.equals("normalLabelFont") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.font.label"), vaadinUi.getEsfapp().getDropDownFontEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.font.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("normalLabelFontSize") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontsize.label"), vaadinUi.getEsfapp().getDropDownFontSizeEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontsize.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("normalLabelFontStyle") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontstyle.label"), vaadinUi.getEsfapp().getDropDownFontStyleEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontstyle.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("normalLabelFontColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontcolor.label"), vaadinUi.getEsfapp().getDropDownFontColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("normalLabelBackgroundColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.backgroundcolor.label"), vaadinUi.getEsfapp().getDropDownBackgroundColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.backgroundcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				

				if ( propertyId.equals("normalLabelErrorFont") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.font.label"), vaadinUi.getEsfapp().getDropDownFontEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.font.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("normalLabelErrorFontSize") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontsize.label"), vaadinUi.getEsfapp().getDropDownFontSizeEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontsize.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("normalLabelErrorFontStyle") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontstyle.label"), vaadinUi.getEsfapp().getDropDownFontStyleEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontstyle.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("normalLabelErrorFontColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontcolor.label"), vaadinUi.getEsfapp().getDropDownFontColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("normalLabelErrorBackgroundColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.backgroundcolor.label"), vaadinUi.getEsfapp().getDropDownBackgroundColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.backgroundcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				

				if ( propertyId.equals("smallLabelFont") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.font.label"), vaadinUi.getEsfapp().getDropDownFontEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.font.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("smallLabelFontSize") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontsize.label"), vaadinUi.getEsfapp().getDropDownFontSizeEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontsize.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("smallLabelFontStyle") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontstyle.label"), vaadinUi.getEsfapp().getDropDownFontStyleEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontstyle.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("smallLabelFontColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontcolor.label"), vaadinUi.getEsfapp().getDropDownFontColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("smallLabelBackgroundColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.backgroundcolor.label"), vaadinUi.getEsfapp().getDropDownBackgroundColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.backgroundcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				

				if ( propertyId.equals("smallLabelErrorFont") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.font.label"), vaadinUi.getEsfapp().getDropDownFontEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.font.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("smallLabelErrorFontSize") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontsize.label"), vaadinUi.getEsfapp().getDropDownFontSizeEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontsize.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("smallLabelErrorFontStyle") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontstyle.label"), vaadinUi.getEsfapp().getDropDownFontStyleEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontstyle.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("smallLabelErrorFontColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.fontcolor.label"), vaadinUi.getEsfapp().getDropDownFontColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.fontcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				
				if ( propertyId.equals("smallLabelErrorBackgroundColor") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("style.backgroundcolor.label"), vaadinUi.getEsfapp().getDropDownBackgroundColorEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("style.backgroundcolor.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}				

				Field field = super.createField(item, propertyId, uiContext);
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
    }
    
    @Override
	public void buttonClick(ClickEvent event) {
    	LibDocumentStyleVersionBean currBean = getCurrentBean();
    	if ( currBean == null ) return;
    	
    	Button source = event.getButton();
    	if ( source == testToProdButton ) {
    		view.promoteTestToProductionVersion();
    	} else if ( source == createTestFromProdButton ) {
    		view.createNextVersion();
    	} else if ( source == revertProdToTestButton ) {
    		view.revertProductionVersionBackToTest();
    	} else if ( source == deleteButton ) {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	ConfirmDialog.show(UI.getCurrent(), 
	    			vaadinUi.getMsg("LibDocumentStyleVersionForm.button.delete.ConfirmDialog.caption"),
	    			vaadinUi.getMsg("LibDocumentStyleVersionForm.button.delete.ConfirmDialog.message"),
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
	    	        new ConfirmDialog.Listener() {
						private static final long serialVersionUID = 7106676993822644695L;

						public void onClose(ConfirmDialog dialog) {
	    	                if (dialog.isConfirmed()) {
	    	                    view.delete();
	    	                }
	    	            }
	    	        });
    	}
     }
	
    LibDocumentStyleVersionBean save(Connection con) throws SQLException {
    	LibDocumentStyleVersionBean currBean = getCurrentBean();
    	if ( currBean != null ) {
    		if ( currBean.save(con,null) ) {
    			return currBean;
    		}
    	}
    	return null;
    }
    	
    public String getCurrentBeanName() {
    	LibDocumentStyleVersionBean currBean = getCurrentBean();
		return currBean == null ? "(None)" : view.getEsfNameWithVersion();
	}
    
	LibDocumentStyleVersionBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public LibDocumentStyleVersionBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibDocumentStyleVersionBean> bi = (BeanItem<LibDocumentStyleVersionBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached stuff
		resetCaches();
		
    	if (newDataSource != null) {
    		LibDocumentStyleVersionBean bean = getBean(newDataSource);
    		
    		if ( _orderedProperties == null ) {
    			_orderedProperties = Arrays.asList(
    					"documentFont","documentFontSize","documentFontStyle","documentFontColor","documentBackgroundColor",
    					"signatureFont","signatureFontSize","signatureFontStyle","signatureFontColor","signatureBackgroundColor",
    					"fieldDataFont","fieldDataFontSize","fieldDataFontStyle","fieldDataFontColor","fieldDataBackgroundColor",
    					"inputFieldRequiredFont","inputFieldRequiredFontSize","inputFieldRequiredFontStyle","inputFieldRequiredFontColor","inputFieldRequiredBackgroundColor","inputFieldRequiredBorderTypes",
    					"inputFieldOptionalFont","inputFieldOptionalFontSize","inputFieldOptionalFontStyle","inputFieldOptionalFontColor","inputFieldOptionalBackgroundColor","inputFieldOptionalBorderTypes",
    					"inputFieldErrorFont","inputFieldErrorFontSize","inputFieldErrorFontStyle","inputFieldErrorFontColor","inputFieldErrorBackgroundColor","inputFieldErrorBorderTypes",
    					"normalLabelFont","normalLabelFontSize","normalLabelFontStyle","normalLabelFontColor","normalLabelBackgroundColor",
    					"normalLabelErrorFont","normalLabelErrorFontSize","normalLabelErrorFontStyle","normalLabelErrorFontColor","normalLabelErrorBackgroundColor",
    					"smallLabelFont","smallLabelFontSize","smallLabelFontStyle","smallLabelFontColor","smallLabelBackgroundColor",
    					"smallLabelErrorFont","smallLabelErrorFontSize","smallLabelErrorFontStyle","smallLabelErrorFontColor","smallLabelErrorBackgroundColor"
    											 );
    		}
    		super.setItemDataSource(newDataSource, _orderedProperties);
    		
    		setupForm(bean);
    		
    		setReadOnly( ! view.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	

    @Override
    public void setReadOnly(boolean readOnly) {   	
    	LibDocumentStyleVersionBean bean = getCurrentBean();
    	
    	boolean isTestVersion = bean != null && view.isTestVersion(bean.documentstyleVerInfo());
    	
    	testToProdButton.setVisible(bean != null && !readOnly && isTestVersion && bean.documentstyleVersion().doUpdate() && view.getLibDocumentStyleBean().isEnabled());
    	createTestFromProdButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.documentstyleVerInfo()) && !view.hasTestVersion());
    	revertProdToTestButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.documentstyleVerInfo()) && !view.hasTestVersion());
    	
    	// Show this only if the object is created and is the Test version
    	if ( deleteButton != null ) {
    		deleteButton.setVisible(bean != null && bean.documentstyleVersion().doUpdate() && isTestVersion); 
    	}

    	// If the request is to be not-readonly, but this is not the test version, we convert it back to readonly
    	if ( !readOnly && bean != null && !view.isTestVersion(bean.documentstyleVerInfo())) {
    		readOnly = true;
    	}
    	super.setReadOnly(readOnly);
    }
    
    void setupForm(LibDocumentStyleVersionBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("LibDocumentStyleVersionForm.label.id",bean.getId()) );
		
		versionInfo.setValue( vaadinUi.getMsg("LibDocumentStyleVersionForm.label.version",bean.getVersion(),view.getVersionLabel(bean.documentstyleVerInfo())) );
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.documentstyleVerInfo().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("LibDocumentStyleVersionForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );
		
		if ( bean.documentstyleVersion().doInsert() ) {
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibDocumentStyleVersionForm.label.lastUpdated.whenNew") );
			lastUpdatedByInfo.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.documentstyleVerInfo().getLastUpdatedByUserId());
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibDocumentStyleVersionForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedByInfo.setIcon(null);
		}
		
		documentSection.setVisible(true);
		signatureSection.setVisible(true);
		fieldDataSection.setVisible(true);
		inputFieldRequiredDataSection.setVisible(true);
		inputFieldOptionalDataSection.setVisible(true);
		inputFieldErrorDataSection.setVisible(true);
		normalLabelSection.setVisible(true);
		normalLabelErrorSection.setVisible(true);
		smallLabelSection.setVisible(true);
		smallLabelErrorSection.setVisible(true);
    }

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
    	if (propertyId.equals("documentFont") ) {
			layout.addComponent(field,0,2);
    	} else if (propertyId.equals("documentFontSize") ) {
			layout.addComponent(field,1,2);
    	} else if (propertyId.equals("documentFontStyle") ) {
			layout.addComponent(field,2,2);
    	} else if (propertyId.equals("documentFontColor") ) {
			layout.addComponent(field,3,2);
    	} else if (propertyId.equals("documentBackgroundColor") ) {
			layout.addComponent(field,4,2);
    	} else if (propertyId.equals("signatureFont") ) {
			layout.addComponent(field,0,4);
    	} else if (propertyId.equals("signatureFontSize") ) {
			layout.addComponent(field,1,4);
    	} else if (propertyId.equals("signatureFontStyle") ) {
			layout.addComponent(field,2,4);
    	} else if (propertyId.equals("signatureFontColor") ) {
			layout.addComponent(field,3,4);
    	} else if (propertyId.equals("signatureBackgroundColor") ) {
			layout.addComponent(field,4,4);
    	} else if (propertyId.equals("fieldDataFont") ) {
			layout.addComponent(field,0,6);
    	} else if (propertyId.equals("fieldDataFontSize") ) {
			layout.addComponent(field,1,6);
    	} else if (propertyId.equals("fieldDataFontStyle") ) {
			layout.addComponent(field,2,6);
    	} else if (propertyId.equals("fieldDataFontColor") ) {
			layout.addComponent(field,3,6);
    	} else if (propertyId.equals("fieldDataBackgroundColor") ) {
			layout.addComponent(field,4,6);
    	} else if (propertyId.equals("inputFieldRequiredFont") ) {
			layout.addComponent(field,0,8);
    	} else if (propertyId.equals("inputFieldRequiredFontSize") ) {
			layout.addComponent(field,1,8);
    	} else if (propertyId.equals("inputFieldRequiredFontStyle") ) {
			layout.addComponent(field,2,8);
    	} else if (propertyId.equals("inputFieldRequiredFontColor") ) {
			layout.addComponent(field,3,8);
    	} else if (propertyId.equals("inputFieldRequiredBackgroundColor") ) {
			layout.addComponent(field,4,8);
    	} else if (propertyId.equals("inputFieldRequiredBorderTypes") ) {
			layout.addComponent(field,5,8);
    	} else if (propertyId.equals("inputFieldOptionalFont") ) {
			layout.addComponent(field,0,10);
    	} else if (propertyId.equals("inputFieldOptionalFontSize") ) {
			layout.addComponent(field,1,10);
    	} else if (propertyId.equals("inputFieldOptionalFontStyle") ) {
			layout.addComponent(field,2,10);
    	} else if (propertyId.equals("inputFieldOptionalFontColor") ) {
			layout.addComponent(field,3,10);
    	} else if (propertyId.equals("inputFieldOptionalBackgroundColor") ) {
			layout.addComponent(field,4,10);
    	} else if (propertyId.equals("inputFieldOptionalBorderTypes") ) {
			layout.addComponent(field,5,10);
    	} else if (propertyId.equals("inputFieldErrorFont") ) {
			layout.addComponent(field,0,12);
    	} else if (propertyId.equals("inputFieldErrorFontSize") ) {
			layout.addComponent(field,1,12);
    	} else if (propertyId.equals("inputFieldErrorFontStyle") ) {
			layout.addComponent(field,2,12);
    	} else if (propertyId.equals("inputFieldErrorFontColor") ) {
			layout.addComponent(field,3,12);
    	} else if (propertyId.equals("inputFieldErrorBackgroundColor") ) {
			layout.addComponent(field,4,12);
    	} else if (propertyId.equals("inputFieldErrorBorderTypes") ) {
			layout.addComponent(field,5,12);
    	} else if (propertyId.equals("normalLabelFont") ) {
			layout.addComponent(field,0,14);
    	} else if (propertyId.equals("normalLabelFontSize") ) {
			layout.addComponent(field,1,14);
    	} else if (propertyId.equals("normalLabelFontStyle") ) {
			layout.addComponent(field,2,14);
    	} else if (propertyId.equals("normalLabelFontColor") ) {
			layout.addComponent(field,3,14);
    	} else if (propertyId.equals("normalLabelBackgroundColor") ) {
			layout.addComponent(field,4,14);
    	} else if (propertyId.equals("normalLabelErrorFont") ) {
			layout.addComponent(field,0,16);
    	} else if (propertyId.equals("normalLabelErrorFontSize") ) {
			layout.addComponent(field,1,16);
    	} else if (propertyId.equals("normalLabelErrorFontStyle") ) {
			layout.addComponent(field,2,16);
    	} else if (propertyId.equals("normalLabelErrorFontColor") ) {
			layout.addComponent(field,3,16);
    	} else if (propertyId.equals("normalLabelErrorBackgroundColor") ) {
			layout.addComponent(field,4,16);
    	} else if (propertyId.equals("smallLabelFont") ) {
			layout.addComponent(field,0,18);
    	} else if (propertyId.equals("smallLabelFontSize") ) {
			layout.addComponent(field,1,18);
    	} else if (propertyId.equals("smallLabelFontStyle") ) {
			layout.addComponent(field,2,18);
    	} else if (propertyId.equals("smallLabelFontColor") ) {
			layout.addComponent(field,3,18);
    	} else if (propertyId.equals("smallLabelBackgroundColor") ) {
			layout.addComponent(field,4,18);
    	} else if (propertyId.equals("smallLabelErrorFont") ) {
			layout.addComponent(field,0,20);
    	} else if (propertyId.equals("smallLabelErrorFontSize") ) {
			layout.addComponent(field,1,20);
    	} else if (propertyId.equals("smallLabelErrorFontStyle") ) {
			layout.addComponent(field,2,20);
    	} else if (propertyId.equals("smallLabelErrorFontColor") ) {
			layout.addComponent(field,3,20);
    	} else if (propertyId.equals("smallLabelErrorBackgroundColor") ) {
			layout.addComponent(field,4,20);
    	}
    }


	@Override
	public String checkDirty() { // OBSOLETE SINCE VERSION FORM HAS NO BUTTONS FOR SAVING, CANCELING
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibDocumentStyleAndVersionsMainView.DocumentStyle.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}