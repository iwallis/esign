// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.ComparisonCondition;

import java.io.Serializable;

import com.esignforms.open.runtime.condition.ComparisonCondition.Spec;
import com.vaadin.data.util.BeanItemContainer;


public class ConditionContainer extends BeanItemContainer<Spec> implements Serializable {
	private static final long serialVersionUID = -4306253849468723022L;

	public ConditionContainer(Spec spec) throws InstantiationException, IllegalAccessException {
		super(Spec.class);
		refresh(spec);
	}
	
	public void refresh(Spec spec) {
		removeAllItems();
		if ( spec != null )
			addItem(spec);
	}
}