// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.documentstyle;

import java.io.Serializable;
import java.util.Collection;

import com.esignforms.open.prog.DocumentStyle;
import com.esignforms.open.prog.DocumentStyleVersionInfo;
import com.vaadin.data.util.BeanItemContainer;


public class LibDocumentStyleVersionBeanContainer extends BeanItemContainer<LibDocumentStyleVersionBean> implements Serializable {
	private static final long serialVersionUID = -8349277630227074658L;

	final LibDocumentStyleAndVersionsMainView view;
	
	public LibDocumentStyleVersionBeanContainer(LibDocumentStyleAndVersionsMainView view) throws InstantiationException, IllegalAccessException {
		super(LibDocumentStyleVersionBean.class);
		this.view = view;

		// No items exist until a document style is selected and set to this re-loadable container.
	}
	
	public void reload(DocumentStyle documentStyle) {
		removeAllItems();
		
		// Users need update permission to program its contents
		if ( documentStyle != null ) {
			
			Collection<DocumentStyleVersionInfo> documentStyleVers = DocumentStyleVersionInfo.Manager.getAll(documentStyle);
			for( DocumentStyleVersionInfo d : documentStyleVers ) {
				addItem( new LibDocumentStyleVersionBean(d,view.getVersionLabel(d)) );			
			}
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}