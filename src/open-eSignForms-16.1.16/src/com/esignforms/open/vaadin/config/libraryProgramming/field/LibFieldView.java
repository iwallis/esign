// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.field;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

/**
 * LibFieldView is a splitpanel that contains the field list on the left and the field form on the right.
 * 
 * @author Yozons
 *
 */
public class LibFieldView extends HorizontalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = 717453859740512083L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibFieldView.class);

    EsfUUID libraryId;
    Library library;
    final LibFieldView thisView;
    EsfUUID fieldContainerId;
    java.util.Map<EsfName,FieldTemplate> fieldTemplateMap;
    
    LibFieldBeanContainer container;
    LibFieldList list;
    LibFieldForm form;
	
	public LibFieldView() {
		super(); 
		thisView = this;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			library = Library.Manager.getById(libraryId, vaadinUi.getUser());
			fieldContainerId = library.getId();
			fieldTemplateMap = FieldTemplate.Manager.getAll(fieldContainerId);
			
			container = new LibFieldBeanContainer(fieldTemplateMap);
			
			list = new LibFieldList(this,container);
		    form = new LibFieldForm(this,container,null,fieldTemplateMap);
		    
			setFirstComponent(list);
			setSecondComponent(form);
			setSplitPosition(350,Unit.PIXELS);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Field view exception", e.getMessage());
		}
	}
	
	public java.util.Map<EsfName,FieldTemplate> getFieldTemplateMap() {
		return fieldTemplateMap;
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		if ( list != null) list.setReadOnly(v);
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our list or not
        if (property == list.getTable()) {
        	final Item item = list.getTable().getItem(list.getTable().getValue());
        	
    		if ( form.isDirty() ) {
    			if ( form.getCurrentBean() == form.getBean(item) ) {
    				// We've selected the one that's been modified, so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibFieldView.ConfirmDiscardChangesDialog.message", form.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						form.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						list.getTable().select(form.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibFieldView.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
                form.setItemDataSource(item);
    		}
        }
        else
        	_logger.debug("valueChange() - no list event: " + event.toString());
	}

	@Override
	public void detach() {
		/*
		for( Object o : list.getTable().getVisibleColumns() ) {
			_logger.debug("detach() - list visible columns[]: " + o); // These are string property ids of the column names like email, status, lastLoginTimestamp
		}
		*/
		super.detach();
	}

	public void select(LibFieldBean bean) {
		unselectAll();
		list.getTable().select(bean);
		list.getTable().setCurrentPageFirstItemId(bean);
	}
	
	public void unselectAll() {
		list.getTable().setValue(null);
	}
	
	public boolean createLike(LibFieldBean likeBean) {
		FieldTemplate fieldTemplate = FieldTemplate.Manager.createLike(fieldContainerId, likeBean.fieldTemplate());
		
		LibFieldBean newBean = new LibFieldBean(fieldTemplate);
		BeanItem<LibFieldBean> newItem = new BeanItem<LibFieldBean>(newBean);
        form.setItemDataSource(newItem);
        setReadOnly(false);
		return true;
	}
	
	public boolean createNew() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		FieldTemplate fieldTemplate = FieldTemplate.Manager.createNew(fieldContainerId, new EsfName(vaadinUi.getMsg("createNew.esfname")));
		LibFieldBean newBean = new LibFieldBean(fieldTemplate);
		
		BeanItem<LibFieldBean> newItem = new BeanItem<LibFieldBean>(newBean);
        form.setItemDataSource(newItem);
        setReadOnly(false);
		return true;
	}
	
	public Library getLibrary() {
		return library;
	}
	
	public boolean hasPermViewDetails() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return ! isReadOnly() && getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return ! isReadOnly() && getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return ! isReadOnly() && getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_DELETE);
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		_logger.debug("activateView() - params: " + params);
		libraryId = new EsfUUID(params);
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return form.checkDirty();
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return form.isDirty();
	}
}