// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.buttonmessage;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.ButtonMessage;
import com.esignforms.open.prog.ButtonMessageInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibButtonMessageBean implements Serializable, Comparable<LibButtonMessageBean> {
	private static final long serialVersionUID = -8297333111766581528L;

	private ButtonMessageInfo buttonmessageInfo;

	private ButtonMessage buttonmessage;
	
	public LibButtonMessageBean(ButtonMessageInfo buttonmessageInfo) {
		this.buttonmessageInfo = buttonmessageInfo;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibButtonMessageBean )
			return buttonmessageInfo.equals(((LibButtonMessageBean)o).buttonmessageInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return buttonmessageInfo.hashCode();
    }
	public int compareTo(LibButtonMessageBean d) {
		return buttonmessageInfo.compareTo(d.buttonmessageInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public ButtonMessageInfo buttonmessageInfo() {
		return buttonmessageInfo;
	}

	public ButtonMessage buttonmessage() {
		if ( buttonmessage == null )
			buttonmessage = ButtonMessage.Manager.getById(buttonmessageInfo.getId());
		return buttonmessage;
	}

	public LibButtonMessageBean createLike() {
		ButtonMessageInfo newButtonMessageInfo = ButtonMessageInfo.Manager.createLike(buttonmessage, buttonmessage.getEsfName());
	    return new LibButtonMessageBean(newButtonMessageInfo);
	}
	
	
	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( buttonmessage().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			buttonmessageInfo = ButtonMessageInfo.Manager.createFromSource(buttonmessage);
			return true;
		} 
		
		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}

	
	// Now for the JavaBeans methods
	public EsfName getEsfName() {
		return buttonmessageInfo.getEsfName();
	}
	public void setEsfName(EsfName v) {
		buttonmessage.setEsfName(v);
	}

	public EsfUUID getId() {
		return buttonmessageInfo.getId();
	}
	
	public String getDescription() {
		return buttonmessageInfo.getDescription();
	}
	public void setDescription(String v) {
		buttonmessage().setDescription(v);
	}

	public boolean isEnabled() {
		return buttonmessageInfo.isEnabled();
	}
	public String getStatus() {
		return buttonmessageInfo.getStatus();
	}
	public void setStatus(String v) {
		buttonmessage().setStatus(v);
	}	
	
	public Integer getProductionVersion() {
		return buttonmessageInfo.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		return buttonmessageInfo.getTestVersion();
	}
	
	public void bumpTestVersion() {
		buttonmessage().bumpTestVersion();
	}
	public void dropTestVersion() {
		buttonmessage().dropTestVersion();
	}
	
	public String getComments() {
		return buttonmessage().getComments();
	}
	public void setComments(String v) {
		buttonmessage().setComments(v);
	}
}