// Copyright (C) 2012-2016 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.reportFieldTemplate;

import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Property;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

public class ReportFieldTemplateList extends Panel {
	private static final long serialVersionUID = 6969989073285475870L;

	final ReportFieldTemplateView view;
	final ReportFieldTemplateContainer container;
	ReportFieldTemplateTable table;
	
	// For search bar
	//Button createNewButton;

	public ReportFieldTemplateList(final ReportFieldTemplateView view, final ReportFieldTemplateContainer containerParam) {
		super();
		this.view = view;
		setStyleName("ReportFieldTemplateList");
		setSizeFull();
		//EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
		
    	this.container = containerParam;
    	
		HorizontalLayout searchBar = new HorizontalLayout();
		searchBar.setWidth(100, Unit.PERCENTAGE);
    	searchBar.addStyleName("searchBar");
    	searchBar.setSpacing(true);
    	searchBar.setMargin(false);

    	layout.addComponent(searchBar);
    	
    	Label dummyLabel = new Label("");
    	searchBar.addComponent(dummyLabel);
    	searchBar.setExpandRatio(dummyLabel, 1.0f); // forces following buttons to right

    	/*
    	createNewButton = new Button(vaadinUi.getMsg("PackageReportFieldView.searchBar.createNewButton.label"));
    	createNewButton.setStyleName(Reindeer.BUTTON_SMALL);
    	createNewButton.addStyleName(Reindeer.BUTTON_DEFAULT);
    	createNewButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageReportFieldView.searchBar.createNewButton.icon")));
    	createNewButton.setDescription(vaadinUi.getMsg("PackageReportFieldView.searchBar.createNewButton.tooltip"));
    	createNewButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = -117413691392017403L;

			@Override
			public void buttonClick(ClickEvent event) {
				view.createNew();
			}
		});
    	searchBar.addComponent(createNewButton);
    	searchBar.setComponentAlignment(createNewButton, Alignment.MIDDLE_RIGHT);
    	*/

    	table = new ReportFieldTemplateTable(view, container);
    	
    	layout.addComponent(searchBar);
    	layout.addComponent(table);
    	layout.setExpandRatio(table, 1);
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		//createNewButton.setVisible(!v);
	}
	
	public Table getTable() {
		return table;
	}
	
	class ReportFieldTemplateTable extends Table {
		private static final long serialVersionUID = 2155113597219512130L;

		public ReportFieldTemplateTable(ReportFieldTemplateView view, ReportFieldTemplateContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("ReportFieldTemplateList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("ReportFieldTemplateList.showColumnHeaders"));
			setColumnAlignment("numReportsReferenced", Align.CENTER);
	        setNullSelectionAllowed(true);
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);

	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener((Property.ValueChangeListener)view);
			setSizeFull();
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			if ( "fieldType".equals(colId) ) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				return vaadinUi.getPrettyCode().reportFieldTemplateType((String)property.getValue());
			}
			return super.formatPropertyValue(rowId,colId,property);
		}

	} // ReportFieldTemplateTable

} // ReportFieldTemplateList
