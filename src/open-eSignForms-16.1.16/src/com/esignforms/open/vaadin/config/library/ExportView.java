// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.library;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.esignforms.open.Application;
import com.esignforms.open.Version;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.ButtonMessage;
import com.esignforms.open.prog.ButtonMessageInfo;
import com.esignforms.open.prog.ButtonMessageVersion;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentInfo;
import com.esignforms.open.prog.DocumentStyle;
import com.esignforms.open.prog.DocumentStyleInfo;
import com.esignforms.open.prog.DocumentStyleVersion;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.DropDownInfo;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.EmailTemplateInfo;
import com.esignforms.open.prog.EmailTemplateVersion;
import com.esignforms.open.prog.File;
import com.esignforms.open.prog.FileInfo;
import com.esignforms.open.prog.FileVersion;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.ImageInfo;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.PropertySet;
import com.esignforms.open.prog.PropertySetInfo;
import com.esignforms.open.prog.PropertySetVersion;
import com.esignforms.open.user.User;
import com.esignforms.open.util.XmlUtil;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.widget.ListSelectValid;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.WebBrowser;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class ExportView extends Panel implements EsfView {
	private static final long serialVersionUID = -8903217664581951235L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ExportView.class);
	
    final ExportView thisView;
    final Library library;
	
    String productionOnly;
    OptionGroup versionTypeOptionGroup;
	ListSelect documentSelect;
	ListSelect buttonMessageSelect;
	ListSelect documentStyleSelect;
	ListSelect dropDownSelect;
	ListSelect emailSelect;
	ListSelect fileSelect;
	ListSelect imageSelect;
	ListSelect propertySetSelect;
    
	public ExportView(Library library) {
		super();
		thisView = this;
		this.library = library;
		setStyleName("LibraryFormExportView");
		setSizeFull();
	}

	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
	        VerticalLayout layout = new VerticalLayout();
	        setContent(layout);
			layout.setSizeUndefined();
			layout.setSpacing(true);
			layout.setMargin(false);

			productionOnly = vaadinUi.getMsg("LibraryForm.ExportView.optiongroup.production.label");
	    	HorizontalLayout optionsLayout = new HorizontalLayout();
	    	optionsLayout.addStyleName("optionsLayout");
	    	optionsLayout.setSpacing(true);
	    	optionsLayout.setMargin(new MarginInfo(false,true,false,true));
	    	
			versionTypeOptionGroup = new OptionGroup();
			versionTypeOptionGroup.addItem(productionOnly);
			versionTypeOptionGroup.addItem(vaadinUi.getMsg("LibraryForm.ExportView.optiongroup.test.label"));
			versionTypeOptionGroup.setStyleName("inline");
			versionTypeOptionGroup.setValue(productionOnly);
			versionTypeOptionGroup.setImmediate(true);
			versionTypeOptionGroup.addValueChangeListener( new Property.ValueChangeListener() {
				private static final long serialVersionUID = -1700673810020810318L;

				@Override
				public void valueChange(ValueChangeEvent event) {
					refreshSelections();
				}
			});
			optionsLayout.addComponent(versionTypeOptionGroup);

	    	HorizontalLayout listSelectLayout = new HorizontalLayout();
	    	listSelectLayout.addStyleName("listSelectLayout");
	    	listSelectLayout.setSpacing(true);
	    	listSelectLayout.setMargin(new MarginInfo(false,true,false,true));
	    	
			documentSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ExportView.document.select.label"));
			documentSelect.setDescription(vaadinUi.getMsg("LibraryForm.ExportView.document.select.tooltip"));
			documentSelect.setRequired(false);
			documentSelect.setMultiSelect(true);
			documentSelect.setNullSelectionAllowed(true);
			documentSelect.setRows(20);
			listSelectLayout.addComponent(documentSelect);
			
			buttonMessageSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ExportView.buttonMessage.select.label"));
			buttonMessageSelect.setDescription(vaadinUi.getMsg("LibraryForm.ExportView.buttonMessage.select.tooltip"));
			buttonMessageSelect.setRequired(false);
			buttonMessageSelect.setMultiSelect(true);
			buttonMessageSelect.setNullSelectionAllowed(true);
			buttonMessageSelect.setRows(20);
			listSelectLayout.addComponent(buttonMessageSelect);
			
			documentStyleSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ExportView.documentStyle.select.label"));
			documentStyleSelect.setDescription(vaadinUi.getMsg("LibraryForm.ExportView.documentStyle.select.tooltip"));
			documentStyleSelect.setRequired(false);
			documentStyleSelect.setMultiSelect(true);
			documentStyleSelect.setNullSelectionAllowed(true);
			documentStyleSelect.setRows(20);
			listSelectLayout.addComponent(documentStyleSelect);
			
			dropDownSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ExportView.dropDown.select.label"));
			dropDownSelect.setDescription(vaadinUi.getMsg("LibraryForm.ExportView.dropDown.select.tooltip"));
			dropDownSelect.setRequired(false);
			dropDownSelect.setMultiSelect(true);
			dropDownSelect.setNullSelectionAllowed(true);
			dropDownSelect.setRows(20);
			listSelectLayout.addComponent(dropDownSelect);
			
			emailSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ExportView.email.select.label"));
			emailSelect.setDescription(vaadinUi.getMsg("LibraryForm.ExportView.email.select.tooltip"));
			emailSelect.setRequired(false);
			emailSelect.setMultiSelect(true);
			emailSelect.setNullSelectionAllowed(true);
			emailSelect.setRows(20);
			listSelectLayout.addComponent(emailSelect);

			fileSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ExportView.file.select.label"));
			fileSelect.setDescription(vaadinUi.getMsg("LibraryForm.ExportView.file.select.tooltip"));
			fileSelect.setRequired(false);
			fileSelect.setMultiSelect(true);
			fileSelect.setNullSelectionAllowed(true);
			fileSelect.setRows(20);
			listSelectLayout.addComponent(fileSelect);
			
			imageSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ExportView.image.select.label"));
			imageSelect.setDescription(vaadinUi.getMsg("LibraryForm.ExportView.image.select.tooltip"));
			imageSelect.setRequired(false);
			imageSelect.setMultiSelect(true);
			imageSelect.setNullSelectionAllowed(true);
			imageSelect.setRows(20);
			listSelectLayout.addComponent(imageSelect);
			
			propertySetSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ExportView.propertySet.select.label"));
			propertySetSelect.setDescription(vaadinUi.getMsg("LibraryForm.ExportView.propertySet.select.tooltip"));
			propertySetSelect.setRequired(false);
			propertySetSelect.setMultiSelect(true);
			propertySetSelect.setNullSelectionAllowed(true);
			propertySetSelect.setRows(20);
			listSelectLayout.addComponent(propertySetSelect);

	    	HorizontalLayout buttonLayout = new HorizontalLayout();
	    	buttonLayout.addStyleName("buttonLayout");
	    	buttonLayout.setSpacing(true);
	    	buttonLayout.setMargin(new MarginInfo(false,true,false,true));
	    	
			Button exportButton = new Button(vaadinUi.getMsg("LibraryForm.ExportView.button.export.label"));
			exportButton.setDescription(vaadinUi.getMsg("LibraryForm.ExportView.button.export.tooltip"));
			exportButton.setStyleName(Reindeer.BUTTON_SMALL);
			exportButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.download.icon")));
			exportButton.addClickListener( new Button.ClickListener() {			
				private static final long serialVersionUID = 3994296906118985838L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					doExport();
					vaadinUi.showStatus(vaadinUi.getMsg("LibraryForm.ExportView.exportOkay.message",library.getPathName()));
				}
			});
	    	buttonLayout.addComponent(exportButton);

	    	layout.addComponent(optionsLayout);
	    	layout.addComponent(listSelectLayout);
	    	layout.addComponent(buttonLayout);
	    	
	    	refreshSelections();
			
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Library ExportView view exception", e.getMessage());
		}
	}
	
	void refreshSelections() {
		boolean showOnlyProduction = productionOnly.equals(versionTypeOptionGroup.getValue().toString());
		
		documentSelect.removeAllItems();
		documentSelect.setValue( new HashSet<EsfUUID>() ); // select nothing
		Collection<DocumentInfo> docInfoList = DocumentInfo.Manager.getAll(library);
		for( DocumentInfo info : docInfoList ) {
    		Document doc = Document.Manager.getById(info.getId());
    		if ( doc == null ) {
    			_logger.warn("refreshSelections() - Could not find document with id: " + info.getId() + "; not including in our list");
    			continue;
    		} else if ( doc.isDisabled() ) {
    			continue; // we'll skip disabled for export
    		} else if ( showOnlyProduction && ! doc.hasProductionVersion() ) {
    			continue;
    		}
			documentSelect.addItem(doc.getId());
			documentSelect.setItemCaption(doc.getId(), doc.getEsfName().toString());
    	}
	
		buttonMessageSelect.removeAllItems();
		buttonMessageSelect.setValue( new HashSet<EsfUUID>() ); // select nothing
		Collection<ButtonMessageInfo> buttonMessageInfoList = ButtonMessageInfo.Manager.getAll(library);
		for( ButtonMessageInfo info : buttonMessageInfoList ) {
			ButtonMessage buttonMessage = ButtonMessage.Manager.getById(info.getId());
    		if ( buttonMessage == null ) {
    			_logger.warn("refreshSelections() - Could not find button message with id: " + info.getId() + "; not including in our list");
    			continue;
    		} else if ( buttonMessage.isDisabled() ) {
    			continue; // we'll skip disabled for export
    		} else if ( showOnlyProduction && ! buttonMessage.hasProductionVersion() ) {
    			continue;
    		}
    		buttonMessageSelect.addItem(buttonMessage.getId());
    		buttonMessageSelect.setItemCaption(buttonMessage.getId(), buttonMessage.getEsfName().toString());
    	}

		documentStyleSelect.removeAllItems();
		documentStyleSelect.setValue( new HashSet<EsfUUID>() ); // select nothing
		Collection<DocumentStyleInfo> docStyleInfoList = DocumentStyleInfo.Manager.getAll(library);
		for( DocumentStyleInfo info : docStyleInfoList ) {
    		DocumentStyle docStyle = DocumentStyle.Manager.getById(info.getId());
    		if ( docStyle == null ) {
    			_logger.warn("refreshSelections() - Could not find document style with id: " + info.getId() + "; not including in our list");
    			continue;
    		} else if ( docStyle.isDisabled() ) {
    			continue; // we'll skip disabled for export
    		} else if ( showOnlyProduction && ! docStyle.hasProductionVersion() ) {
    			continue;
    		}
			documentStyleSelect.addItem(docStyle.getId());
			documentStyleSelect.setItemCaption(docStyle.getId(), docStyle.getEsfName().toString());
    	}

		dropDownSelect.removeAllItems();
		dropDownSelect.setValue( new HashSet<EsfUUID>() ); // select nothing
		Collection<DropDownInfo> dropDownInfoList = DropDownInfo.Manager.getAll(library.getId());
		for( DropDownInfo info : dropDownInfoList ) {
    		DropDown dropDown = DropDown.Manager.getById(info.getId());
    		if ( dropDown == null ) {
    			_logger.warn("refreshSelections() - Could not find drop down with id: " + info.getId() + "; not including in our list");
    			continue;
    		} else if ( dropDown.isDisabled() ) {
    			continue; // we'll skip disabled for export
    		} else if ( showOnlyProduction && ! dropDown.hasProductionVersion() ) {
    			continue;
    		}
    		dropDownSelect.addItem(dropDown.getId());
    		dropDownSelect.setItemCaption(dropDown.getId(), dropDown.getEsfName().toString());
    	}

		emailSelect.removeAllItems();
		emailSelect.setValue( new HashSet<EsfUUID>() ); // select nothing
		Collection<EmailTemplateInfo> emailInfoList = EmailTemplateInfo.Manager.getAll(library.getId());
		for( EmailTemplateInfo info : emailInfoList ) {
    		EmailTemplate email = EmailTemplate.Manager.getById(info.getId());
    		if ( email == null ) {
    			_logger.warn("refreshSelections() - Could not find email template with id: " + info.getId() + "; not including in our list");
    			continue;
    		} else if ( email.isDisabled() ) {
    			continue; // we'll skip disabled for export
    		} else if ( showOnlyProduction && ! email.hasProductionVersion() ) {
    			continue;
    		}
    		emailSelect.addItem(email.getId());
    		emailSelect.setItemCaption(email.getId(), email.getEsfName().toString());
    	}

		fileSelect.removeAllItems();
		fileSelect.setValue( new HashSet<EsfUUID>() ); // select nothing
		Collection<FileInfo> fileInfoList = FileInfo.Manager.getAll(library.getId());
		for( FileInfo info : fileInfoList ) {
    		File file = File.Manager.getById(info.getId());
    		if ( file == null ) {
    			_logger.warn("refreshSelections() - Could not find file with id: " + info.getId() + "; not including in our list");
    			continue;
    		} else if ( file.isDisabled() ) {
    			continue; // we'll skip disabled for export
    		} else if ( showOnlyProduction && ! file.hasProductionVersion() ) {
    			continue;
    		}
    		fileSelect.addItem(file.getId());
    		fileSelect.setItemCaption(file.getId(), file.getEsfName().toString());
    	}

		imageSelect.removeAllItems();
		imageSelect.setValue( new HashSet<EsfUUID>() ); // select nothing
		Collection<ImageInfo> imageInfoList = ImageInfo.Manager.getAll(library.getId());
		for( ImageInfo info : imageInfoList ) {
    		Image image = Image.Manager.getById(info.getId());
    		if ( image == null ) {
    			_logger.warn("refreshSelections() - Could not find image with id: " + info.getId() + "; not including in our list");
    			continue;
    		} else if ( image.isDisabled() ) {
    			continue; // we'll skip disabled for export
    		} else if ( showOnlyProduction && ! image.hasProductionVersion() ) {
    			continue;
    		}
    		imageSelect.addItem(image.getId());
    		imageSelect.setItemCaption(image.getId(), image.getEsfName().toString());
    	}

		propertySetSelect.removeAllItems();
		propertySetSelect.setValue( new HashSet<EsfUUID>() ); // select nothing
		Collection<PropertySetInfo> propertySetInfoList = PropertySetInfo.Manager.getAll(library.getId());
		for( PropertySetInfo info : propertySetInfoList ) {
			PropertySet propertySet = PropertySet.Manager.getById(info.getId());
    		if ( propertySet == null ) {
    			_logger.warn("refreshSelections() - Could not find property set with id: " + info.getId() + "; not including in our list");
    			continue;
    		} else if ( propertySet.isDisabled() ) {
    			continue; // we'll skip disabled for export
    		} else if ( showOnlyProduction && ! propertySet.hasProductionVersion() ) {
    			continue;
    		}
    		propertySetSelect.addItem(propertySet.getId());
    		propertySetSelect.setItemCaption(propertySet.getId(), propertySet.getEsfName().toString());
    	}
	}
	
	void doExport() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		boolean showOnlyProduction = productionOnly.equals(versionTypeOptionGroup.getValue().toString());
		
		StringBuilder buf = new StringBuilder(4096*10);
        buf.append("<LibraryExport xmlns=\"").append(XmlUtil.getXmlNamespace2011()).append("\">\n");
        buf.append(" <deployId>").append(vaadinUi.getEsfapp().getDeployId().toXml()).append("</deployId>\n");
        buf.append(" <release>").append(XmlUtil.toEscapedXml(Version.getReleaseString())).append("</release>\n");
        buf.append(" <versionMajor>").append(Version.getMajor()).append("</versionMajor>\n");
        buf.append(" <versionMinor>").append(Version.getMinor()).append("</versionMinor>\n");
        buf.append(" <timestamp>").append((new EsfDateTime()).toXml()).append("</timestamp>\n");
        buf.append(" <exportedByUserId>").append(vaadinUi.getUser().getId().toXml()).append("</exportedByUserId>\n");
        buf.append(" <libraryId>").append(library.getId().toXml()).append("</libraryId>\n");
        buf.append(" <libraryPathName>").append(library.getPathName().toXml()).append("</libraryPathName>\n");
        exportDocuments(buf,showOnlyProduction);
        exportButtonMessages(buf,showOnlyProduction);
        exportDocumentStyles(buf,showOnlyProduction);
        exportDropDowns(buf,showOnlyProduction);
        exportEmails(buf,showOnlyProduction);
        exportFiles(buf,showOnlyProduction);
        exportImages(buf,showOnlyProduction);
        exportPropertySets(buf,showOnlyProduction);
        buf.append("</LibraryExport>\n");
        
		final byte[] data = EsfString.stringToBytes(buf.toString());
		
		StreamResource.StreamSource ss = new StreamResource.StreamSource() {
			private static final long serialVersionUID = 4945722953932073542L;
			
			InputStream is = new BufferedInputStream(new ByteArrayInputStream(data));
			@Override
			public InputStream getStream() {
				return is;
			}
		};
		
		WebBrowser wb = Page.getCurrent().getWebBrowser();
		
		String libraryPathName = library.getPathName().toString();
		libraryPathName = libraryPathName.replaceAll(EsfPathName.PATH_SEPARATOR, "_");
		String fileName = vaadinUi.getMsg("LibraryForm.ExportView.button.export.filename",libraryPathName);
		StreamResource sr = new StreamResource(ss, fileName);
		//sr.setMIMEType(Application.CONTENT_TYPE_XML+Application.CONTENT_TYPE_CHARSET_UTF_8);
		sr.setMIMEType(Application.CONTENT_TYPE_BINARY);
		sr.setCacheTime(0); // disable cache
		sr.getStream().setParameter("Content-Length", Integer.toString(data.length));
		sr.getStream().setParameter("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		Page.getCurrent().open(sr, wb.isChrome() || wb.isSafari() ? "_top" : "_blank", false);
		
		User user = vaadinUi.getUser();
		if ( user != null ) {
			user.logConfigChange("Exported contents from library: " + library.getPathName() + "; libraryId: " + library.getId());
			vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange("User: " + user.getFullDisplayName() + " exported contents from library: " + library.getPathName() + "; libraryId: " + library.getId());
		}
	}
	
	void exportDocuments(StringBuilder buf,boolean showOnlyProduction) {
		@SuppressWarnings("unchecked")
		Set<EsfUUID> idSet = (Set<EsfUUID>)documentSelect.getValue();
		
		for( EsfUUID id : idSet ) {
			Document obj = Document.Manager.getById(id);
			if ( obj == null )
				continue;
			obj.appendXml(buf);
			DocumentVersion ver = showOnlyProduction ? obj.getProductionDocumentVersion() : obj.getTestDocumentVersion();
			if ( ver != null ) {
				ver.appendXml(buf);
			}
		}
	}
	
	void exportButtonMessages(StringBuilder buf,boolean showOnlyProduction) {
		@SuppressWarnings("unchecked")
		Set<EsfUUID> idSet = (Set<EsfUUID>)buttonMessageSelect.getValue();
		
		for( EsfUUID id : idSet ) {
			ButtonMessage obj = ButtonMessage.Manager.getById(id);
			if ( obj == null )
				continue;
			obj.appendXml(buf);
			ButtonMessageVersion ver = showOnlyProduction ? obj.getProductionButtonMessageVersion() : obj.getTestButtonMessageVersion();
			if ( ver != null ) {
				ver.appendXml(buf);
			}
		}
	}
	
	void exportDocumentStyles(StringBuilder buf,boolean showOnlyProduction) {
		@SuppressWarnings("unchecked")
		Set<EsfUUID> idSet = (Set<EsfUUID>)documentStyleSelect.getValue();
		
		for( EsfUUID id : idSet ) {
			DocumentStyle obj = DocumentStyle.Manager.getById(id);
			if ( obj == null )
				continue;
			obj.appendXml(buf);
			DocumentStyleVersion ver = showOnlyProduction ? obj.getProductionDocumentStyleVersion() : obj.getTestDocumentStyleVersion();
			if ( ver != null ) {
				ver.appendXml(buf);
			}
		}
	}
	
	void exportDropDowns(StringBuilder buf,boolean showOnlyProduction) {
		@SuppressWarnings("unchecked")
		Set<EsfUUID> idSet = (Set<EsfUUID>)dropDownSelect.getValue();
		
		for( EsfUUID id : idSet ) {
			DropDown obj = DropDown.Manager.getById(id);
			if ( obj == null )
				continue;
			obj.appendXml(buf);
			DropDownVersion ver = showOnlyProduction ? obj.getProductionDropDownVersion() : obj.getTestDropDownVersion();
			if ( ver != null ) {
				ver.appendXml(buf);
			}
		}
	}
	
	void exportEmails(StringBuilder buf,boolean showOnlyProduction) {
		@SuppressWarnings("unchecked")
		Set<EsfUUID> idSet = (Set<EsfUUID>)emailSelect.getValue();
		
		for( EsfUUID id : idSet ) {
			EmailTemplate obj = EmailTemplate.Manager.getById(id);
			if ( obj == null )
				continue;
			obj.appendXml(buf);
			EmailTemplateVersion ver = showOnlyProduction ? obj.getProductionEmailTemplateVersion() : obj.getTestEmailTemplateVersion();
			if ( ver != null ) {
				ver.appendXml(buf);
			}
		}
	}
	
	void exportFiles(StringBuilder buf,boolean showOnlyProduction) {
		@SuppressWarnings("unchecked")
		Set<EsfUUID> idSet = (Set<EsfUUID>)fileSelect.getValue();
		
		for( EsfUUID id : idSet ) {
			File obj = File.Manager.getById(id);
			if ( obj == null )
				continue;
			obj.appendXml(buf);
			FileVersion ver = showOnlyProduction ? obj.getProductionFileVersion() : obj.getTestFileVersion();
			if ( ver != null ) {
				ver.appendXml(buf);
			}
		}
	}
	
	void exportImages(StringBuilder buf,boolean showOnlyProduction) {
		@SuppressWarnings("unchecked")
		Set<EsfUUID> idSet = (Set<EsfUUID>)imageSelect.getValue();
		
		for( EsfUUID id : idSet ) {
			Image obj = Image.Manager.getById(id);
			if ( obj == null )
				continue;
			obj.appendXml(buf);
			ImageVersion ver = showOnlyProduction ? obj.getProductionImageVersion() : obj.getTestImageVersion();
			if ( ver != null ) {
				ver.appendXml(buf);
			}
		}
	}
	
	void exportPropertySets(StringBuilder buf,boolean showOnlyProduction) {
		@SuppressWarnings("unchecked")
		Set<EsfUUID> idSet = (Set<EsfUUID>)propertySetSelect.getValue();
		
		for( EsfUUID id : idSet ) {
			PropertySet obj = PropertySet.Manager.getById(id);
			if ( obj == null )
				continue;
			obj.appendXml(buf);
			PropertySetVersion ver = showOnlyProduction ? obj.getProductionPropertySetVersion() : obj.getTestPropertySetVersion();
			if ( ver != null ) {
				ver.appendXml(buf);
			}
		}
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return "";
	}
	
	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
}