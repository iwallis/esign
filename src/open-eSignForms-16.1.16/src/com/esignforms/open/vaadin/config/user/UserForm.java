// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.user;

import java.util.ArrayList;
import java.util.Arrays;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.jouni.animator.Disclosure;
import org.vaadin.peter.buttongroup.ButtonGroup;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.email.OutboundEmailMessage;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.UI;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.libraryProgramming.propertyset.LibPropertySetAndVersionsMainView;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.log.email.EmailLogView;
import com.esignforms.open.vaadin.log.user.LogView;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.validator.UserEmailValidator;
import com.esignforms.open.vaadin.widget.NativeSelectDropDown;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The UserForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class UserForm extends Form implements EsfView, ClickListener {

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(UserForm.class);
	
	Button saveButton;
	Button createLikeButton;
	Button deleteButton;
	Button cancelButton;
	Button editButton;
	Button viewLogButton;
	Button viewEmailButton;
	Button sendPasswordResetButton;
	Button propertySetsButton;

	UserView view;
	UserViewContainer container;
    GridLayout layout;
	
	Label id;
	Label lastLoggedInTimestamp;
	Label lastUpdatedTimestamp;
	Label createdByInfo;
    
	// We store the prevBean in case we abandon working on the newBean when we 'create like'
	UserBean prevBean;
    UserBean newBean;
    
    Disclosure memberGroupsDisclosure;
	
    // We use these for all our member groups select list, so we only reload once per UserBean being set as our datasource
	EsfUUID[] allMemberGroupsUpdateIds;
	String[] allMemberGroupsUpdatePathNames;

	Disclosure memberGroupsUpdateDisclosure;

	
	public UserForm(UserView view, UserViewContainer container) {
    	setStyleName("UserForm");
       	setWidth(100, Unit.PERCENTAGE); // hack because seems to have "horizontal scrollbar" issues if the form is shown with vertical scrollbars in place
    	this.view = view;
    	this.container = container;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	
    	layout = new GridLayout(3,10);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.34f);
    	layout.setColumnExpandRatio(1, 0.33f);
    	layout.setColumnExpandRatio(2, 0.33f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id, 0, 0);

		lastLoggedInTimestamp = new Label();
		lastLoggedInTimestamp.setContentMode(ContentMode.TEXT);
		lastLoggedInTimestamp.setStyleName("smallInfo");
		layout.addComponent(lastLoggedInTimestamp, 1, 0, 2, 0);
		
		memberGroupsDisclosure = new Disclosure(vaadinUi.getMsg("UserForm.memberGroupsDisclosure.label"));
		memberGroupsDisclosure.setWidth(100, Unit.PERCENTAGE);
		memberGroupsDisclosure.setVisible(false);
		layout.addComponent(memberGroupsDisclosure,0,6,2,6);
		
		memberGroupsUpdateDisclosure = new Disclosure(vaadinUi.getMsg("UserForm.memberGroupsUpdateDisclosure.label"));
		memberGroupsUpdateDisclosure.setWidth(100, Unit.PERCENTAGE);
		memberGroupsUpdateDisclosure.setVisible(false);
		layout.addComponent(memberGroupsUpdateDisclosure,0,7,2,7);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,8,2,8);		
		
		lastUpdatedTimestamp = new Label();
		lastUpdatedTimestamp.setContentMode(ContentMode.TEXT);
		lastUpdatedTimestamp.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedTimestamp,0,9,2,9);

		VerticalLayout footer = new VerticalLayout();
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);

		HorizontalLayout footer1 = new HorizontalLayout();
		footer1.setStyleName("footer");
		footer1.setSpacing(false);
		footer1.setMargin(false);

		HorizontalLayout footer2 = new HorizontalLayout();
		footer2.setStyleName("footer2");
		footer2.setSpacing(false);
		footer2.setMargin(false);

    	footer.addComponent(footer1);
    	footer.addComponent(footer2);

    	User loggedInUser = vaadinUi.getUser();
		UI ui = new UI();
		if ( ui.showSaveButton(loggedInUser, UI.UI_USER_VIEW) ) {
	    	saveButton = new Button(vaadinUi.getMsg("button.save.label"), (ClickListener)this);
	    	//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
	    	//saveButton.setClickShortcut(KeyCode.ENTER);
	    	saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
	    	saveButton.setDescription(vaadinUi.getMsg("button.save.tooltip"));
    		footer2.addComponent(saveButton);
		}

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer2.addComponent(cancelButton);

    	if ( ui.canUpdate(loggedInUser, UI.UI_USER_VIEW) ) {
	    	editButton = new Button(vaadinUi.getMsg("button.edit.label"), (ClickListener)this);
	    	editButton.setIcon(new ThemeResource(vaadinUi.getMsg("UserForm.button.edit.icon")));
	    	editButton.setDescription(vaadinUi.getMsg("button.edit.tooltip"));
    		footer2.addComponent(editButton);
    	}

		if ( ui.canCreateLike(loggedInUser, UI.UI_USER_VIEW) ) {
	    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
	    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("UserForm.button.createLike.icon")));
	    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    		footer2.addComponent(createLikeButton);
		}
    	
    	if ( ui.canDelete(loggedInUser, UI.UI_USER_VIEW) ) {
	    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
	    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("UserForm.button.delete.icon")));
	    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
	    	deleteButton.setStyleName("deleteButton");
	    	deleteButton.addStyleName("caution");
	    	footer2.addComponent(deleteButton);
    	}
    	
    	ButtonGroup buttonGroup = new ButtonGroup();
    	buttonGroup.setStyleName(Reindeer.BUTTON_SMALL);
    	footer1.addComponent(buttonGroup);
    	
    	viewLogButton = new Button(vaadinUi.getMsg("UserForm.button.viewLog.label"), (ClickListener)this);
    	viewLogButton.setStyleName(Reindeer.BUTTON_SMALL);
    	viewLogButton.setIcon(new ThemeResource(vaadinUi.getMsg("UserForm.button.viewLog.icon")));
    	buttonGroup.addButton(viewLogButton);
    	         
    	viewEmailButton = new Button(vaadinUi.getMsg("UserForm.button.viewEmail.label"), (ClickListener)this);
    	viewEmailButton.setStyleName(Reindeer.BUTTON_SMALL);
    	viewEmailButton.setIcon(new ThemeResource(vaadinUi.getMsg("UserForm.button.viewEmail.icon")));
    	buttonGroup.addButton(viewEmailButton);
    	
		propertySetsButton = new Button(vaadinUi.getMsg("UserForm.button.propertySets.label"), (ClickListener)this);
		propertySetsButton.setStyleName("propertySetsButton");
		propertySetsButton.addStyleName(Reindeer.BUTTON_SMALL);
		propertySetsButton.setDescription(vaadinUi.getMsg("UserForm.button.propertySets.tooltip"));
		footer1.addComponent(propertySetsButton);   
    	
    	if ( ui.canUpdate(loggedInUser, UI.UI_USER_VIEW) ) {
    		sendPasswordResetButton = new Button(vaadinUi.getMsg("UserForm.button.sendPasswordReset.label"), (ClickListener)this);
    		sendPasswordResetButton.setStyleName("sendPasswordResetButton");
    		sendPasswordResetButton.addStyleName(Reindeer.BUTTON_SMALL);
    		sendPasswordResetButton.addStyleName("caution");
    		sendPasswordResetButton.setDescription(vaadinUi.getMsg("UserForm.button.sendPasswordReset.tooltip"));
    		footer1.addComponent(sendPasswordResetButton);   
    	}
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -4433302478473482621L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				// Load our cache if we don't have it yet since we'll need it for all our permission selection boxes
				if ( allMemberGroupsUpdateIds == null ) {
					UserBean bean = getCurrentBean();
					
					allMemberGroupsUpdateIds = bean.allMemberGroupsUpdateIds();
					allMemberGroupsUpdatePathNames = bean.allMemberGroupsUpdatePathNames();

					_logger.debug("createField() - created allMemberGroupsUpdateIds.size: " + allMemberGroupsUpdateIds.length);
				}

				// Status will be an option group (radio buttons and not a text field)
				if ( propertyId.equals("status") ) {
					// Create the radio buttons to have the two possible values.
					OptionGroup statusOptionGroup = new OptionGroup();
					statusOptionGroup.addItem(Literals.STATUS_ENABLED);
					statusOptionGroup.addItem(Literals.STATUS_DISABLED);
					// Associate different labels/captions to those values
					statusOptionGroup.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					statusOptionGroup.setItemCaption(Literals.STATUS_ENABLED, vaadinUi.getPrettyCode().status(Literals.STATUS_ENABLED));
					statusOptionGroup.setItemCaption(Literals.STATUS_DISABLED, vaadinUi.getPrettyCode().status(Literals.STATUS_DISABLED));
					statusOptionGroup.setStyleName("inline");
					statusOptionGroup.setDescription(vaadinUi.getMsg("UserForm.status.tooltip")); // Doesn't appear to work without a caption on the OptionGroup, and we don't want one.
					return statusOptionGroup;
				}
					
				if ( propertyId.equals("memberGroupsUpdateIds") ) {
					return createMemberGroupsUpdateSelectBox("UserForm.memberGroupsUpdate.label");
				} 
				
				if ( propertyId.equals("timeZone") ) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("UserForm.timeZone.label"), vaadinUi.getEsfapp().getDropDownTimeZoneEsfName(), true);
	                select.setDescription(vaadinUi.getMsg("UserForm.timeZone.tooltip"));
	                select.addValidator(new SelectValidator(select));
	                return select;
				}
				
				if ( propertyId.equals("comments") ) {
	            	TextArea ta = new TextArea();
	            	ta.setWidth(100, Unit.PERCENTAGE);
	            	ta.setNullRepresentation("");
	            	ta.setRows(3);
	            	ta.setInputPrompt(vaadinUi.getMsg("inputPrompt.comments"));
	            	ta.setCaption(vaadinUi.getMsg("caption.comments"));
	            	ta.setDescription(vaadinUi.getMsg("tooltip.comments"));
	            	/*
	            	ta.addFocusListener( new FieldEvents.FocusListener() {
						private static final long serialVersionUID = 8225972181117048314L;

						@Override
						public void focus(FocusEvent event) {
							if ( saveButton != null )
								saveButton.removeClickShortcut();
						}
	            	});
	            	ta.addBlurListener( new FieldEvents.BlurListener() {
						private static final long serialVersionUID = 667496726519278753L;

						@Override
						public void blur(BlurEvent event) {
							if ( saveButton != null )
								saveButton.setClickShortcut(KeyCode.ENTER);
						}
					});
					*/
	            	return ta;
				}
				
				Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("email")) {
                	TextField tf = (TextField)field;
                    tf.addValidator(new UserEmailValidator(getCurrentBean()));
                    tf.setImmediate(true);
                    tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    tf.setCaption(vaadinUi.getMsg("caption.email.address"));
                    tf.setDescription(vaadinUi.getMsg("UserForm.email.tooltip"));
                } else if ( propertyId.equals("displayName") ) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("caption.email.displayName"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.email.displayName"));
                } else if ( propertyId.equals("personalName") ) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("caption.personalName"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.personalName"));
                } else if ( propertyId.equals("familyName") ) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("caption.familyName"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.familyName"));
                } else if ( propertyId.equals("employeeId") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("UserForm.employeeId.label"));
                    tf.setDescription(vaadinUi.getMsg("UserForm.employeeId.tooltip"));
                } else if ( propertyId.equals("jobTitle") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("UserForm.jobTitle.label"));
                    tf.setDescription(vaadinUi.getMsg("UserForm.jobTitle.tooltip"));
                } else if ( propertyId.equals("location") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("UserForm.location.label"));
                    tf.setDescription(vaadinUi.getMsg("UserForm.location.tooltip"));
                } else if ( propertyId.equals("department") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("UserForm.department.label"));
                    tf.setDescription(vaadinUi.getMsg("UserForm.department.tooltip"));
                } else if ( propertyId.equals("phoneNumber") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("UserForm.phoneNumber.label"));
                    tf.setDescription(vaadinUi.getMsg("UserForm.phoneNumber.tooltip"));
                }
                
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
		allMemberGroupsUpdateIds = null;
		allMemberGroupsUpdatePathNames = null;
    }
    
    private TwinColSelect createMemberGroupsUpdateSelectBox(String vaadinMessageCaption) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	TwinColSelect selectList = new TwinColSelect();
    	selectList.setLeftColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".left"));
    	selectList.setRightColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".right"));
    	selectList.setWidth(100, Unit.PERCENTAGE);
        selectList.addValidator(new SelectValidator(selectList));
        selectList.setRequired(true);
		
		// Set all the possible values
		for( int i=0; i < allMemberGroupsUpdateIds.length; ++i ) {
			selectList.addItem(allMemberGroupsUpdateIds[i]);
		}
		// Associate labels with our ID values
		selectList.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		for( int i=0; i < allMemberGroupsUpdateIds.length; ++i ) {
			selectList.setItemCaption(allMemberGroupsUpdateIds[i], allMemberGroupsUpdatePathNames[i]);
		}
		
		selectList.setRows( Math.min(allMemberGroupsUpdateIds.length,5) );
		selectList.setNullSelectionAllowed(true);
		selectList.setMultiSelect(true);
		return selectList;
    }
    
    
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
    			
    		// If we're saving a new bean
			Errors errors = new Errors();
    		if ( newBean != null ) {
        		if ( newBean.save(errors) ) {
        			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",newBean.getFullDisplayName()) );
            		// We need to add the new bean to the container
            		container.addItem(newBean);
            		view.select(newBean); // reselect our bean so all updates cleanly
            		prevBean = newBean = null; // and we're not longer working on a new bean
        		} else {
        			vaadinUi.show(errors);
        		}
    		} else {
    			UserBean currUser = getCurrentBean();
    			if ( currUser.save(errors) ) {
        			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",currUser.getFullDisplayName()) );
            		view.select(currUser); // reselect our user so all updates cleanly
        		} else {
        			vaadinUi.show(errors);
        		}
    		}
        } else if ( source == cancelButton ) {
    		discard();
    		vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
        	if ( newBean != null ) {
        		setItemDataSource(null);
        		view.unselectAll(); // we'll unselect and the reselect our prevBean
        		view.select(prevBean);
        		prevBean = newBean = null; // we're not doing a new user anymore
        	}
        	setReadOnly(true);
        } else if ( source == editButton ) {
        	setReadOnly(false);
        } else if ( source == deleteButton ) {
	    	ConfirmDialog.show(com.vaadin.ui.UI.getCurrent(), 
	    			vaadinUi.getMsg("UserForm.button.delete.ConfirmDialog.caption"),
	    			vaadinUi.getMsg("UserForm.button.delete.ConfirmDialog.message"),
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
	    	        new ConfirmDialog.Listener() {
						private static final long serialVersionUID = 9099169081369828968L;

						public void onClose(ConfirmDialog dialog) {
	    	                if (dialog.isConfirmed()) {
	    	                	discard();
	    	                	UserBean currBean = getCurrentBean();
	    	                	if ( currBean != null ) {
	    	                		setItemDataSource(null);
	    	                		view.unselectAll(); // we'll unselect it and remove it from our container
	    	                		if ( newBean == null ) {
	    	                			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	                			Errors errors = new Errors();
	    	                			if ( currBean.delete(errors) ) {
	    	                        		container.removeItem(currBean);
	    	                        		vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",currBean.getFullDisplayName()) );
	    	                			} else {
	    	                				vaadinUi.show(errors);
	    	                			}
	    	                		} else {
	    	                			newBean.delete(null);
	    	                    		view.select(prevBean);
	    	                			prevBean = newBean = null;
	    	                		}
	    	                	}	
	    	                }
	    	            }
	    	        });
        } else if ( source == createLikeButton ) {
        	UserBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		createLike(currBean);
        	}	
    	} else if ( source == viewLogButton ) {
    		UserBean currBean = getCurrentBean();
            final LogView logView = new LogView(currBean.user());
            logView.initView();
    		
            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("UserLogView.window.caption",currBean.getFullDisplayName(),currBean.getId()), logView);
        	w.setWidth(80, Unit.PERCENTAGE);
        	w.setHeight(80, Unit.PERCENTAGE);

            logView.activateView(EsfView.OpenMode.WINDOW, "");
            vaadinUi.addWindowToUI(w); 	
    	} else if ( source == viewEmailButton ) {
    		UserBean currBean = getCurrentBean();
            final EmailLogView emailLogView = new EmailLogView(OutboundEmailMessage.LINK_TYPE_USER,currBean.user().getId());
            emailLogView.initView();
    		
            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("UserEmailLogView.window.caption",currBean.getFullDisplayName(),currBean.getId()), emailLogView);
        	w.setWidth(90, Unit.PERCENTAGE);
        	w.setHeight(80, Unit.PERCENTAGE);

        	emailLogView.activateView(EsfView.OpenMode.WINDOW, "");
            vaadinUi.addWindowToUI(w); 	
    	} else if ( source == propertySetsButton ) {
    		UserBean currBean = getCurrentBean();
            final LibPropertySetAndVersionsMainView propSetView = new LibPropertySetAndVersionsMainView(currBean.user());
            propSetView.initView();
            propSetView.setReadOnly(isReadOnly());
    		
            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("UserPropertySetsView.window.caption",currBean.getFullDisplayName(),currBean.getId()), propSetView);
        	w.setWidth(90, Unit.PERCENTAGE);
        	w.setHeight(80, Unit.PERCENTAGE);
        	w.center();

        	propSetView.activateView(EsfView.OpenMode.WINDOW, currBean.user().getId().toString());
            vaadinUi.addWindowToUI(w); 	
        } else if ( source == sendPasswordResetButton ) {
        	UserBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		sendPasswordReset(currBean);
        	}	
        }
    }
	
	public String getCurrentBeanName() {
		UserBean currBean = getCurrentBean();
		return currBean == null ? "(None)" : currBean.getFullDisplayName();
	}
    
	UserBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public UserBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<UserBean> bi = (BeanItem<UserBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached list
		resetCaches();
		
    	if (newDataSource != null) {
    		UserBean bean = getBean(newDataSource);
    		super.setItemDataSource(newDataSource, Arrays.asList("status","email","personalName","familyName","displayName","employeeId","jobTitle","location","department","phoneNumber","comments","timeZone","memberGroupsUpdateIds"));
    		
    		setupForm(bean);
    		
    		setReadOnly( ! bean.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		newBean = bean.user().doInsert() ? bean : null;
    		if ( newBean != null ) {
    			TextField tf = (TextField)getField("email");
    			tf.selectAll();
    			tf.focus();
    		}
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	UserBean bean = getCurrentBean();
    	
    	// We have these only while editing/creating new
    	if ( saveButton != null ) {
    		saveButton.setVisible(!readOnly && bean != null && bean.hasPermUpdate());
    	}
    	cancelButton.setVisible(!readOnly);

    	// We have this only while in view mode
    	if ( editButton != null ) {
    		editButton.setVisible(readOnly && bean != null && bean.hasPermUpdate());
    	}
    	
    	// This one we show this in all modes if has permission
    	if ( createLikeButton != null ) {
    		createLikeButton.setVisible(bean != null && bean.hasPermCreateLike() && bean.user().doUpdate()); // only want create like on an existing user
    	}
    	
    	if ( deleteButton != null ) {
    		deleteButton.setVisible(bean != null && bean.hasPermDelete());
    	}
    	
    	viewLogButton.setVisible(bean != null);
    	
    	if ( sendPasswordResetButton != null ) {
        	sendPasswordResetButton.setVisible(bean != null && bean.hasPermUpdate() && bean.user().doUpdate() && bean.user().isEnabled());
    	}
    }
    
    public void createLike(UserBean likeBean) {
        // Create a temporary item for the form
    	UserBean createdBean = likeBean.createLike();
        
        BeanItem<UserBean> bi = new BeanItem<UserBean>( createdBean );
        setItemDataSource(bi);
    	prevBean = likeBean;
        setReadOnly(false);
    }
    
    public void sendPasswordReset(UserBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	EmailTemplate emailTemplate = EmailTemplate.Manager.getByName(Library.Manager.getTemplate().getId(), vaadinUi.getEsfapp().getEmailTemplateSetPasswordEsfName());
    	if ( emailTemplate == null ) {
    		vaadinUi.showError(null, vaadinUi.getMsg("templateLibrary.emailTemplate.notFound",vaadinUi.getEsfapp().getEmailTemplateSetPasswordEsfName()));
    	} else {
    		try {
        		vaadinUi.getEsfapp().getPasswordManager().requestSetPassword(bean.user(), vaadinUi.getUser(), vaadinUi.getRequestExternalContextPath(), vaadinUi.getRequestIpAddress());
        		vaadinUi.showStatus(vaadinUi.getMsg("UserForm.sendPasswordReset.success"));
        		sendPasswordResetButton.setCaption(vaadinUi.getMsg("UserForm.button.sendPasswordSet.label"));
    		} catch( EsfException e ) {
        		vaadinUi.showError( null, vaadinUi.getMsg("UserForm.sendPasswordReset.failure",e.getMessage()));
    		}
    	}
    }
    
    void setupForm(UserBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("UserForm.label.id",bean.getId()) );
		
		if ( bean.user().doInsert() ) {
			lastUpdatedTimestamp.setCaption(vaadinUi.getMsg("UserForm.label.lastUpdated.whenNew"));
			lastUpdatedTimestamp.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.user().getLastUpdatedByUserId());
			lastUpdatedTimestamp.setCaption( vaadinUi.getMsg("UserForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedTimestamp.setIcon(null);
		}

		if ( bean.user().doInsert() ) {
			lastLoggedInTimestamp.setValue(vaadinUi.getMsg("UserForm.label.lastLoggedIn.whenNew"));
		} else if ( ! bean.user().hasLastLoginTimestamp() )	{
			EsfDateTime dt = bean.user().getLoginTimestamp();
			if ( dt != null ) {
				lastLoggedInTimestamp.setValue( vaadinUi.getMsg("UserForm.label.lastLoggedIn",bean.formatLogLastLoginTimestamp(),bean.getLastLoginIP()) );
			} else {
				lastLoggedInTimestamp.setValue(vaadinUi.getMsg("UserForm.label.lastLoggedIn.never"));
			}
		} else {
			lastLoggedInTimestamp.setValue( vaadinUi.getMsg("UserForm.label.lastLoggedIn",bean.formatLogLastLoginTimestamp(),bean.getLastLoginIP()) );
		}
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.user().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("UserForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );

		createMemberGroupsDisclosure(bean);
		createMemberGroupsUpdateDisclosure(bean);
		
    	if ( sendPasswordResetButton != null ) {
    		String captionKey = ( bean.user().hasPassword() ) ? "UserForm.button.sendPasswordReset.label" : "UserForm.button.sendPasswordSet.label";
    		sendPasswordResetButton.setCaption(vaadinUi.getMsg(captionKey));
    	}
    }
    
    void createMemberGroupsDisclosure(UserBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	GridLayout memberGroupsLayout;
    	
    	ArrayList<Group> memberGroupsList = bean.user().getMemberOfGroups(vaadinUi.getUser(),PermissionOption.PERM_OPTION_LIST);
    	
    	if ( memberGroupsList.size() == 0 ) {
    		memberGroupsLayout = new GridLayout();
    		memberGroupsLayout.addComponent( new Label(vaadinUi.getMsg("UserForm.memberGroupsDisclosure.noGroupsToShow")) );
    	} else {
    		memberGroupsLayout = new GridLayout(2,memberGroupsList.size());
        	
        	for( Group g : memberGroupsList ) {
        		memberGroupsLayout.addComponent( new Label(g.getPathName().toString()) );
        		memberGroupsLayout.addComponent( new Label("- " + g.getDescription().toString()) );
        	}
    	}

    	memberGroupsLayout.setMargin(true);
    	memberGroupsLayout.setSpacing(true);
    	memberGroupsLayout.setWidth(100, Unit.PERCENTAGE);

		memberGroupsDisclosure.setContent(memberGroupsLayout);
    	memberGroupsDisclosure.setVisible(true);
    }

    
    void createMemberGroupsUpdateDisclosure(UserBean bean) {
    	VerticalLayout memberGroupsUpdateLayout = new VerticalLayout();
    	memberGroupsUpdateLayout.setMargin(false);
    	memberGroupsUpdateLayout.setSpacing(true);
    	memberGroupsUpdateLayout.setWidth(100, Unit.PERCENTAGE);

    	memberGroupsUpdateLayout.addComponent(getField("memberGroupsUpdateIds"));
				
    	memberGroupsUpdateDisclosure.setContent(memberGroupsUpdateLayout);
    	memberGroupsUpdateDisclosure.setVisible(true);
    }


	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("status")) {
            layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("timeZone")) {
        	layout.addComponent(field, 1, 1, 2, 1);
        } else if (propertyId.equals("email")) {
        	layout.addComponent(field, 0, 2);
        } else if (propertyId.equals("displayName")) {
            layout.addComponent(field, 1, 2);
        } else if (propertyId.equals("phoneNumber")) {
        	layout.addComponent(field, 2, 2);    
        } else if (propertyId.equals("personalName")) {
        	layout.addComponent(field, 0, 3);
        } else if (propertyId.equals("familyName")) {
        	layout.addComponent(field, 1, 3);
        } else if (propertyId.equals("jobTitle")) {
        	layout.addComponent(field, 2, 3);
        } else if (propertyId.equals("employeeId")) {
        	layout.addComponent(field, 0, 4);
        } else if (propertyId.equals("location")) {
        	layout.addComponent(field, 1, 4);
        } else if (propertyId.equals("department")) {
        	layout.addComponent(field, 2, 4);
        } else if (propertyId.equals("comments")) {
        	layout.addComponent(field, 0, 5, 2, 5);
        }
    }

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("UserView.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}