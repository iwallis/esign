// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming;

import com.esignforms.open.data.EsfHtml;
import com.esignforms.open.data.Record;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

public class LibraryProgrammingTipsView extends Panel implements EsfView {
	private static final long serialVersionUID = 3417660969803481380L;

	Label messageAreaHtml;
	
	public LibraryProgrammingTipsView() {
        setIcon(null); // we just want the icon in the tab
        setStyleName("libraryProgrammingTipsView");
        setSizeFull();
        setStyleName(Reindeer.PANEL_LIGHT);
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
        layout.setMargin(true);
        layout.setSpacing(false);
    }
    
	protected void buildLayout() {
        messageAreaHtml = new Label();
        messageAreaHtml.setWidth(98, Unit.PERCENTAGE);
    	messageAreaHtml.addStyleName("messageArea");
    	messageAreaHtml.setContentMode(Label.CONTENT_XHTML);
    	
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
        layout.setMargin(true);
        layout.setSpacing(false);
    	layout.addComponent(messageAreaHtml);
	}

	@Override
	public void activateView(OpenMode mode, String params) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
        Record deploymentProperties = Record.Manager.getById(vaadinUi.getEsfapp().getDeploymentPropertiesId());
        EsfHtml html = deploymentProperties.getHtmlByName(vaadinUi.getEsfapp().getLibraryProgrammingTipsHtmlEsfName());
        String showHtml = ( html == null ) ? vaadinUi.getMsg("LibraryProgrammingTipsView.tipsAreaHtml.default") : html.toPlainString();
    	messageAreaHtml.setValue(showHtml);
	}

	@Override
	public String checkDirty() {
		return null;
	}

	@Override
	public void initView() {
    	buildLayout();
	}
	
	@Override
	public boolean isDirty() {
		return false;
	}
	
}