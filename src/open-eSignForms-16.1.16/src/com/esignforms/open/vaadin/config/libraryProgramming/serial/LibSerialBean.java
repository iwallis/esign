// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.serial;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Serial;
import com.esignforms.open.prog.SerialInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibSerialBean implements Serializable, Comparable<LibSerialBean> {
	private static final long serialVersionUID = 6085307794909510143L;

	private SerialInfo serialInfo;

	private Serial serial;
	
	public LibSerialBean(SerialInfo serialInfo) {
		this.serialInfo = serialInfo;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibSerialBean )
			return serialInfo.equals(((LibSerialBean)o).serialInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return serialInfo.hashCode();
    }
	public int compareTo(LibSerialBean d) {
		return serialInfo.compareTo(d.serialInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public SerialInfo serialInfo() {
		return serialInfo;
	}

	public Serial serial() {
		if ( serial == null )
			serial = Serial.Manager.getById(serialInfo.getId());
		return serial;
	}

	public LibSerialBean createLike() {
		SerialInfo newSerialInfo = SerialInfo.Manager.createLike(serial, serial.getEsfName());	    
	    return new LibSerialBean(newSerialInfo);
	}
	
	
	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( serial().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			serialInfo = SerialInfo.Manager.createFromSource(serial);
			return true;
		} 
		
		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}

	
	// Now for the JavaBeans methods
	public EsfName getEsfName() {
		return serialInfo.getEsfName();
	}
	public void setEsfName(EsfName v) {
		serial.setEsfName(v);
	}

	public EsfUUID getId() {
		return serialInfo.getId();
	}
	
	public String getComments() {
		return serialInfo.getComments();
	}
	public void setComments(String v) {
		serial().setComments(v);
	}
	
	public String getDescription() {
		return serialInfo.getDescription();
	}
	public void setDescription(String v) {
		serial().setDescription(v);
	}

	public boolean isEnabled() {
		return serialInfo.isEnabled();
	}
	public String getStatus() {
		return serialInfo.getStatus();
	}
	public void setStatus(String v) {
		serial().setStatus(v);
	}	
	
	public Integer getProductionVersion() {
		return serialInfo.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		return serialInfo.getTestVersion();
	}
	
	public void bumpTestVersion() {
		serial().bumpTestVersion();
	}
	public void dropTestVersion() {
		serial().dropTestVersion();
	}
	
    public long getNextProductionSerial() {
    	return serial().getNextProductionSerial().toLong();
    }
    public void setNextProductionSerial(long v) {
    	serial().setNextProductionSerial( new EsfInteger(v) );
    }

    public long getNextTestSerial() {
    	return serial().getNextTestSerial().toLong();
    }
    public void setNextTestSerial(long v) {
    	serial().setNextTestSerial( new EsfInteger(v) );
    }

}