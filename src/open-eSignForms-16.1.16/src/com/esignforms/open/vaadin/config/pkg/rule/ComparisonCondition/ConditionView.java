// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.ComparisonCondition;

import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.runtime.condition.ComparisonCondition;
import com.esignforms.open.runtime.condition.ComparisonCondition.Spec;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.main.EsfViewWithConfirmDiscardFormChangesWindowParent;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class ConditionView extends Panel implements EsfViewWithConfirmDiscardFormChangesWindowParent {
	private static final long serialVersionUID = -1227987577816849259L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ConditionView.class);
	
    final PackageVersion packageVersion;
    final DocumentVersion documentVersion;
    final ComparisonCondition duplicatedCondition;
    final ConditionView thisView;
    ConfirmDiscardFormChangesWindow parentWindow;
    ConditionContainer container;
    ConditionForm form;
	
	public ConditionView(PackageVersion packageVersionParam, ComparisonCondition duplicatedConditionParam) {
		super();
		VerticalLayout layout = new VerticalLayout();
		layout.setSpacing(true);
		layout.setMargin(true);
		setContent(layout);
		this.packageVersion = packageVersionParam;
		this.documentVersion = null;
		this.duplicatedCondition = duplicatedConditionParam;
		thisView = this;
	}
	
	public ConditionView(DocumentVersion documentVersionParam, ComparisonCondition duplicatedConditionParam) {
		super();
		VerticalLayout layout = new VerticalLayout();
		layout.setSpacing(true);
		layout.setMargin(true);
		setContent(layout);
		this.packageVersion = null;
		this.documentVersion = documentVersionParam;
		this.duplicatedCondition = duplicatedConditionParam;
		thisView = this;
	}
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			Spec spec = duplicatedCondition.getSpec();
			container = new ConditionContainer(spec);
			form = packageVersion != null ? new ConditionForm(this,container,packageVersion,duplicatedCondition) : new ConditionForm(this,container,documentVersion,duplicatedCondition);
			form.setSpecAsDataSource(spec);
			
			((VerticalLayout)getContent()).addComponent(form);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("ConditionView view exception", e.getMessage());
		}
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		if ( form != null) form.setReadOnly(v);
	}
	
	@Override
	public void setParentWindow(ConfirmDiscardFormChangesWindow w) {
		parentWindow = w;
	}
	
	public void closeParentWindow() {
		parentWindow.close();
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return form.checkDirty();
	}
	
	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return form.isDirty();
	}
}