// Copyright (C) 2013-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.ChangeFileFieldAction;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.runtime.action.ChangeFileFieldAction;
import com.esignforms.open.runtime.action.ChangeFileFieldAction.Spec;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Yozons Inc.
 */
public class ActionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -1412583209523892042L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ActionForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;

	GridLayout layout;
	VerticalLayout substringLayout;
	
	final PackageVersion duplicatedPackageVersion;
	final DocumentVersion duplicatedDocumentVersion;
	final ChangeFileFieldAction duplicatedAction;
	final ActionView view;
	final ActionContainer container;
	
	List<EsfUUID> allDocumentIds;
	List<EsfName> allDocumentNames;
	
	List<EsfUUID> allPartyIds;
	List<EsfName> allPartyNames;
	
	Spec newSpec; // when set, we're working on a new spec.
	
	public ActionForm(ActionView view, final ActionContainer container, PackageVersion duplicatedPackageVersionParam, ChangeFileFieldAction duplicatedActionParam) {
		this.view = view;
		this.container = container;
		this.duplicatedDocumentVersion = null;
		this.duplicatedPackageVersion = duplicatedPackageVersionParam;
		this.duplicatedAction = duplicatedActionParam;
		allDocumentIds = new LinkedList<EsfUUID>(duplicatedPackageVersion.getDocumentIdList());
		setupForm();
	}
	
	public ActionForm(ActionView view, final ActionContainer container, DocumentVersion duplicatedDocumentVersionParam, ChangeFileFieldAction duplicatedActionParam) {
		this.view = view;
		this.container = container;
		this.duplicatedDocumentVersion = duplicatedDocumentVersionParam;
		this.duplicatedPackageVersion = null;
		this.duplicatedAction = duplicatedActionParam;
		allDocumentIds = new LinkedList<EsfUUID>();
		allDocumentIds.add(duplicatedDocumentVersion.getDocumentId());
		setupForm();
	}
	
	private void setupForm() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		setStyleName("ChangeFileFieldActionForm");
    	allDocumentNames = new LinkedList<EsfName>();
    	
    	for( EsfUUID docId : allDocumentIds ) {
    		Document doc = Document.Manager.getById(docId);
    		if ( doc == null ) {
    			_logger.warn("ActionForm() - Could not find document with id: " + docId);
    			allDocumentNames.add(docId.toEsfName());
    		} else {
    			allDocumentNames.add(doc.getEsfName());
    		}
    	}
	
		allPartyIds = new LinkedList<EsfUUID>();
    	allPartyNames = new LinkedList<EsfName>();

    	if ( duplicatedPackageVersion != null ) {
			for( PackageVersionPartyTemplate pvpt : duplicatedPackageVersion.getPackageVersionPartyTemplateList() ) {
				allPartyIds.add(pvpt.getId());
				allPartyNames.add(pvpt.getEsfName());
			}
		} else if ( duplicatedDocumentVersion != null ) {
			for( PartyTemplate pt : duplicatedDocumentVersion.getPartyTemplateList() ) {
				allPartyIds.add(pt.getId());
				allPartyNames.add(pt.getEsfName());
			}
		}
    	
    	// Setup layout
    	layout = new GridLayout(4,1);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.25f);
    	layout.setColumnExpandRatio(1, 0.25f);
    	layout.setColumnExpandRatio(2, 0.25f);
    	layout.setColumnExpandRatio(3, 0.25f);
    	setLayout(layout);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    	footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -8353186314222011155L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

				if (propertyId.equals("targetDocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.ChangeFileFieldAction.form.targetDocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeFileFieldAction.form.targetDocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -7087783343224250950L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames(newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("targetField")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.ChangeFileFieldAction.form.targetField.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeFileFieldAction.form.targetField.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
				
				if (propertyId.equals("targetPartyId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.ChangeFileFieldAction.form.targetPartyId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeFileFieldAction.form.targetPartyId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allPartyNames.listIterator();
                	for( EsfUUID partyId : allPartyIds ) {
                		select.addItem(partyId);
                		select.setItemCaption(partyId, nameIter.next().toString());
                	}
                	select.setImmediate(false);
                	return select;
                }
                
				if (propertyId.equals("changeType")) {
					OptionGroup changeTypeOptionGroup = new OptionGroup();
					changeTypeOptionGroup.addItem(ChangeFileFieldAction.Spec.CHANGE_TYPE_VIEW_DOWNLOAD_OPTIONAL);
					changeTypeOptionGroup.addItem(ChangeFileFieldAction.Spec.CHANGE_TYPE_VIEW_DOWNLOAD_PROHIBITED);
					// Associate different labels/captions to those values
					changeTypeOptionGroup.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					changeTypeOptionGroup.setItemCaption(ChangeFileFieldAction.Spec.CHANGE_TYPE_VIEW_DOWNLOAD_OPTIONAL, vaadinUi.getPrettyCode().changeFileFieldActionChangeType(ChangeFileFieldAction.Spec.CHANGE_TYPE_VIEW_DOWNLOAD_OPTIONAL));
					changeTypeOptionGroup.setItemCaption(ChangeFileFieldAction.Spec.CHANGE_TYPE_VIEW_DOWNLOAD_PROHIBITED, vaadinUi.getPrettyCode().changeFileFieldActionChangeType(ChangeFileFieldAction.Spec.CHANGE_TYPE_VIEW_DOWNLOAD_PROHIBITED));
					return changeTypeOptionGroup;
				}				
                
				Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
                return field;
    	    }
    	 });

    	_logger.debug("Form created");

	}
	
	void setupAllFieldNames(EsfUUID newDocId) {
		NativeSelect targetField = (NativeSelect)getField("targetField");
		if ( targetField == null )
			return;
		EsfName targetFieldValue = (EsfName)targetField.getValue(); // save value before we empty the list
		targetField.removeAllItems();
		
		boolean isProduction;
		if ( duplicatedPackageVersion != null ) {
			com.esignforms.open.prog.Package pkg = duplicatedPackageVersion.getPackage();
			isProduction = pkg.getProductionVersion() == duplicatedPackageVersion.getVersion();
		} else {
			com.esignforms.open.prog.Document doc = duplicatedDocumentVersion.getDocument();
			isProduction = doc.getProductionVersion() == duplicatedDocumentVersion.getVersion();
		}
		
		Document document = Document.Manager.getById(newDocId);
		if ( document == null )
			return; 
		DocumentVersion docVersion;
		
		if ( isProduction )
			docVersion = document.getProductionDocumentVersion();
		else
			docVersion = document.getTestDocumentVersion();
		Map<EsfName,FieldTemplate> docFieldMap = docVersion.getFieldTemplateMap();
		for( EsfName fieldName : docFieldMap.keySet() ) {
			FieldTemplate ft = docFieldMap.get(fieldName);
			if ( ft != null && ft.isTypeFile() ) {
				targetField.addItem(fieldName);
			}
		}
		if ( targetFieldValue != null && targetFieldValue.isValid() ) {
			try {
				if ( targetField.containsId(targetFieldValue) )// reset value back if its valid for our new select list
					targetField.setValue(targetFieldValue); 
				else
					targetField.setValue(null);
				targetField.commit(); // don't let this cause the field to be marked as modified; ignore exceptions
			} catch( Exception e ) {}
		}
	}

	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		if ( getItemDataSource() == null ) {
    			saveSpecs();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
    		if ( newSpec != null ) {
    			container.addItem(newSpec);
    			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",vaadinUi.getMsg("PackageProgrammingRule.ChangeFileFieldAction.form.message.spec.name")) );
    			newSpec = null;
    		} else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("PackageProgrammingRule.ChangeFileFieldAction.form.message.spec.name")) );
    		}
    		
			saveSpecs();
    		
    		view.select(getCurrentBean()); // reselect our bean so all updates cleanly
        } else if ( source == cancelButton ) {
    		discard();
    		if ( getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			setNewSpecAsDataSource(null);
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	view.closeParentWindow();
        } else if ( source == createLikeButton ) {
        	discard();
        	Spec spec = getCurrentBean();
        	if ( spec != null ) {
    			view.unselectAll();
    			setNewSpecAsDataSource(spec.duplicate());
        	}
        } else if ( source == deleteButton ) {
        	discard();
        	
        	if ( newSpec != null ) {
        		newSpec = null;
        	} else {
            	Spec spec = getCurrentBean();
            	if ( spec != null ) {
            		container.removeItem(spec);
        			saveSpecs();
            	}
        	}
    		view.unselectAll();
			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("PackageProgrammingRule.ChangeFileFieldAction.form.message.spec.name")) );
        }
    }
	
	void saveSpecs() {
		view.specListUpdated();
	}
	
	Spec getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public Spec getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<Spec> bi = (BeanItem<Spec>)dataSource;
		return bi.getBean();
    }
    
    public void setNewSpecAsDataSource(Spec spec) {
    	newSpec = spec;
    	if ( newSpec == null )
    		setItemDataSource(null);
    	else
    		setItemDataSource(new BeanItem<Spec>(newSpec));
    }
    
    // Used when a new spec is created. We'll default the document id to the first document id our list, or a "fake" id if we have no documents.
    public EsfUUID getNewSpecDocumentId()
    {
    	if ( duplicatedPackageVersion != null )
    		return ( allDocumentIds == null || allDocumentIds.size() == 0 ) ? new EsfUUID() : allDocumentIds.get(0);
    	return duplicatedDocumentVersion.getDocumentId();
    }
    
    public EsfUUID getNewSpecPartyId()
    {
    	if ( duplicatedPackageVersion != null )
    		return ( allPartyIds == null || allPartyIds.size() == 0 ) ? new EsfUUID() : allPartyIds.get(0);
    	return duplicatedDocumentVersion.getPartyTemplateList().get(0).getId();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		if ( newDataSource != null ) {
    		super.setItemDataSource(newDataSource, Arrays.asList("targetField","targetDocumentId","targetPartyId","changeType"));
    		layout.setVisible(true);
		} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
		}
		setReadOnly(isReadOnly());
    }

	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	Spec bean = getCurrentBean();
    	
    	saveButton.setVisible(!readOnly && bean != null);
    	cancelButton.setVisible(!readOnly && bean != null);
    	closeButton.setVisible(true);
    	createLikeButton.setVisible(!readOnly && bean != null && newSpec == null);
    	deleteButton.setVisible(!readOnly && bean != null);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("targetDocumentId")) {
            layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("targetField")) {
        	layout.addComponent(field, 1, 0);
        } else if (propertyId.equals("targetPartyId")) {
        	layout.addComponent(field, 2, 0);
        } else if (propertyId.equals("changeType")) {
        	layout.addComponent(field, 3, 0);
        }
    }
	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageProgrammingRule.ChangeFileFieldAction.view.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified() || newSpec != null;
	}
	
}