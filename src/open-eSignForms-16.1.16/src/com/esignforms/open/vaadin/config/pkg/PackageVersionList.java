// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg;

import java.text.DecimalFormat;

import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Property;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;


public class PackageVersionList extends Panel {
	private static final long serialVersionUID = 5789728389608153629L;

	final PackageAndVersionsMainView view;
	PackageVersionTable table;
	
	DecimalFormat versionFormat = new DecimalFormat("#,###,###,###");
	

	public PackageVersionList(final PackageAndVersionsMainView view, final PackageVersionBeanContainer container) {
		super();
		this.view = view;
		setStyleName("PackageVersionList");
		setSizeFull();
		
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
		
		HorizontalLayout searchBar = new HorizontalLayout();
    	searchBar.addStyleName("searchBar");
    	searchBar.setSpacing(true);
    	searchBar.setMargin(false);
    	layout.addComponent(searchBar);
    	
    	table = new PackageVersionTable(view, container);
    	
    	layout.addComponent(table);
    	layout.setExpandRatio(table, 1);
	}
	
	public Table getTable() {
		return table;
	}
	
	class PackageVersionTable extends Table {
		private static final long serialVersionUID = 3717294962470597365L;

		public PackageVersionTable(final PackageAndVersionsMainView view, PackageVersionBeanContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("PackageVersionList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("PackageVersionList.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setColumnAlignment("version",Align.CENTER);
			setCellStyleGenerator(new Table.CellStyleGenerator( ) {
				private static final long serialVersionUID = -6015930536333051054L;

				@Override
				public String getStyle(Table table, Object itemId, Object propertyId) {
					PackageBean packageBean = view.getPackageBean();
					return packageBean == null || packageBean.pkg().isEnabled() ? null : "disabledText";
				}
			});
	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener((Property.ValueChangeListener)view);
	        /* We don't want to allow users to de-select a row */
	        setNullSelectionAllowed(false);
			setSizeFull();
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			if ( "lastUpdatedTimestamp".equals(colId) ) {
				return ((PackageVersionBean)rowId).formatLastUpdatedTimestamp();
			} else if ( "version".equals(colId) ) {
				int version = ((PackageVersionBean)rowId).packageVerInfo().getVersion();
				return versionFormat.format(version);
			}
			
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // PackageVersionTable

} // PackageVersionList
