// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.party;

import java.util.Arrays;
import java.util.ListIterator;

import com.esignforms.open.Errors;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.user.Group;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.LibraryPartyEsfNameValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * The LibPartyForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class LibPartyForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 3245742989152868764L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibPartyForm.class);
	
	Button saveButton;
	Button createLikeButton;
	Button deleteButton;
	Button cancelButton;
	Button editButton;

	LibPartyView view;
	LibPartyBeanContainer container;
	final LibPartyForm thisForm;
    GridLayout layout;
	
    LibraryPartyEsfNameValidator esfnameValidator;
    
	Label id;
	
	// We store the prevBean in case we abandon working on the newBean when we 'create like'
	LibPartyBean prevBean;
    LibPartyBean newBean;
    
	
	public LibPartyForm(LibPartyView view, LibPartyBeanContainer container) {
		this.thisForm = this;
    	setStyleName("LibPartyForm");
       	setWidth(100, Unit.PERCENTAGE);
       	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	this.view = view;
    	this.container = container;
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
    	layout = new GridLayout(3,3);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.80f);
    	layout.setColumnExpandRatio(1, 0.10f);
    	layout.setColumnExpandRatio(2, 0.10f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
		
		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id,0,2,2,2); // last row

		HorizontalLayout footer = new HorizontalLayout();
		footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);

		if ( view.hasPermCreateLike() || view.hasPermUpdate() ) {
	    	saveButton = new Button(vaadinUi.getMsg("button.save.label"), (ClickListener)this);
	    	saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
	    	saveButton.setDescription(vaadinUi.getMsg("button.save.tooltip"));
    		footer.addComponent(saveButton);
		}

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	if ( view.hasPermUpdate() ) {
	    	editButton = new Button(vaadinUi.getMsg("button.edit.label"), (ClickListener)this);
	    	editButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibPartyForm.button.edit.icon")));
	    	editButton.setDescription(vaadinUi.getMsg("button.edit.tooltip"));
    		footer.addComponent(editButton);
    	}

		if ( view.hasPermCreateLike() ) {
	    	createLikeButton = new Button(vaadinUi.getMsg("LibPartyForm.button.createLike.label"), (ClickListener)this);
	    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibPartyForm.button.createLike.icon")));
	    	createLikeButton.setDescription(vaadinUi.getMsg("LibPartyForm.button.createLike.tooltip"));
    		footer.addComponent(createLikeButton);
		}
    	
    	if ( view.hasPermDelete() ) {
        	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
        	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibPartyForm.button.delete.icon")));
        	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
        	deleteButton.addStyleName("deleteButton");
        	deleteButton.addStyleName("caution");
    		footer.addComponent(deleteButton);
    	}

    	setFooter(footer);
    	setReadOnly(true);

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 1918577243917287088L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				_logger.debug("createField: " + propertyId);

                if (propertyId.equals("todoGroupId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibPartyForm.todoGroupId.label"));
                	select.setNullSelectionAllowed(true); 
                	select.setImmediate(false);
                	select.setRequired(false);
                	select.setMultiSelect(false);
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	for( Group group : Group.Manager.getForUserWithListMemberUsersPermission(vaadinUi.getUser(), Group.INCLUDE.ONLY_ENABLED)) {
                		select.addItem(group.getId());
                		select.setItemCaption(group.getId(), group.getPathName().toString());
                	}
                	return select;
                }
                
                if (propertyId.equals("testTodoGroupId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibPartyForm.testTodoGroupId.label"));
                	select.setNullSelectionAllowed(true); 
                	select.setImmediate(false);
                	select.setRequired(false);
                	select.setMultiSelect(false);
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	for( Group group : Group.Manager.getForUserWithListMemberUsersPermission(vaadinUi.getUser(), Group.INCLUDE.ONLY_ENABLED)) {
                		select.addItem(group.getId());
                		select.setItemCaption(group.getId(), group.getPathName().toString());
                	}
                	return select;
                }
                
                Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("esfName")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setCaption(vaadinUi.getMsg("caption.esfName"));
                	tf.setDescription(vaadinUi.getMsg("LibPartyForm.esfname.tooltip"));
                	tf.setConversionError(vaadinUi.getMsg("validator.esfname.message"));
                	return tf;
                }
                
                if (propertyId.equals("displayName")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setCaption(vaadinUi.getMsg("caption.displayName"));
                	tf.setDescription(vaadinUi.getMsg("LibPartyForm.displayName.tooltip"));
                	return tf;
                }
                
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
    }
    
    @Override
	public void buttonClick(ClickEvent event) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
    		// If we're saving a new bean
			Errors errors = new Errors();
    		if ( newBean != null ) {
        		if ( newBean.save(errors) ) {
            		view.getPartyTemplateList().add(newBean.partyTemplate());
        			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",newBean.getEsfName()) );
            		// We need to add the new bean to the container
            		container.addItem(newBean);
            		view.select(newBean); // reselect our bean so all updates cleanly
            		prevBean = newBean = null; // and we're not longer working on a new bean
        		} else {
        			vaadinUi.show(errors);
        		}
    		} else {
    			LibPartyBean currBean = getCurrentBean();
    			if ( currBean.save(errors) ) {
    				ListIterator<PartyTemplate> partyListIter = view.getPartyTemplateList().listIterator();
    				while( partyListIter.hasNext() ) {
    					PartyTemplate party = partyListIter.next();
    					if ( party.equals(currBean.partyTemplate()) ) {
    						partyListIter.set(currBean.partyTemplate());
    						break;
    					}
    				}
        			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",currBean.getEsfName()) );
            		view.select(currBean); // reselect our bean so all updates cleanly
        		} else {
        			vaadinUi.show(errors);
        		}
    		}
        } else if ( source == cancelButton ) {
    		discard();
    		vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
        	if ( newBean != null ) {
        		setItemDataSource(null);
        		view.unselectAll(); // we'll unselect and the reselect our prevBean
        		view.select(prevBean);
        		prevBean = newBean = null; // we're not doing a new bean anymore
        	}
        	setReadOnly(true);
        } else if ( source == editButton ) {
        	setReadOnly(false);
        } else if ( source == deleteButton ) {
        	discard();
        	LibPartyBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		setItemDataSource(null);
        		view.unselectAll(); // we'll unselect it and remove it from our container if it's not new
        		if ( newBean == null ) {
        			Errors errors = new Errors();
        			if ( currBean.delete(errors) ) {
                		view.getPartyTemplateList().remove(currBean.partyTemplate());
                		container.removeItem(currBean);
                		vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",currBean.getEsfName()) );
        			} else {
        				vaadinUi.show(errors);
        				view.select(currBean);
        			}
        		} else {
        			newBean.delete(null);
            		view.select(prevBean);
        			prevBean = newBean = null;
        		}
        	}	
        } else if ( source == createLikeButton ) {
        	LibPartyBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		createLike(currBean);
        	}	
        }
    }
	
	public String getCurrentBeanName() {
		LibPartyBean currBean = getCurrentBean();
		return currBean == null ? "(None)" : currBean.getEsfName().toString();
	}
    
	LibPartyBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public LibPartyBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibPartyBean> bi = (BeanItem<LibPartyBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached stuff
		resetCaches();
		
    	if (newDataSource != null) {
    		LibPartyBean bean = getBean(newDataSource);
    		
    		super.setItemDataSource(newDataSource, Arrays.asList("esfName","displayName","todoGroupId","testTodoGroupId"));
    		
    		setupForm(bean);
    		
    		setReadOnly( ! view.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		newBean = bean.partyTemplate().doInsert() ? bean : null;
    		if ( newBean != null ) {
    			TextField tf = (TextField)getField("esfName");
    			tf.selectAll();
    			tf.focus();
    		}
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		setReadOnly(true);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	LibPartyBean bean = getCurrentBean();
    	
    	// We have these only while editing/creating new
    	if ( saveButton != null ) {
    		saveButton.setVisible(!readOnly);
    	}
    	cancelButton.setVisible(!readOnly);

    	// We have this only while in view mode
    	if ( editButton != null ) {
    		editButton.setVisible(readOnly && bean != null && view.hasPermUpdate());
    	}
    	
    	// This one we show this in all modes if has permission
    	if ( createLikeButton != null ) {
    		createLikeButton.setVisible(bean != null && bean.partyTemplate().doUpdate() && view.hasPermCreateLike()); // only want create like on an existing field
    	}
    	
    	if ( deleteButton != null ) {
        	deleteButton.setVisible(bean != null && ! bean.getOriginalEsfName().hasReservedPrefix() && view.hasPermDelete());
    	}
    	
    	if ( bean != null && bean.getOriginalEsfName().hasReservedPrefix() ) {
    		getField("esfName").setReadOnly(true);
    	}
    }
    
    public void createLike(LibPartyBean likeBean) {
    	view.createLike(likeBean);
    	prevBean = likeBean;
     }
    
    void setupForm(LibPartyBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("LibPartyForm.label.id",bean.getId()) );
		
		Field esfname = getField("esfName");
		if ( esfnameValidator != null ) esfname.removeValidator(esfnameValidator);
		esfnameValidator = new LibraryPartyEsfNameValidator(view.getPartyTemplateList(),bean.partyTemplate());
		esfname.addValidator(esfnameValidator);
    }
    
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("esfName")) {
        	layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("displayName")) {
        	layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("todoGroupId")) {
        	layout.addComponent(field, 1, 1);
        } else if (propertyId.equals("testTodoGroupId")) {
        	layout.addComponent(field, 2, 1);
        }
    }

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibPartyView.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}