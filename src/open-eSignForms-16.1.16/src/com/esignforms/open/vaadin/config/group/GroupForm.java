// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.group;

import java.util.Arrays;
import java.util.List;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.jouni.animator.Disclosure;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.UI;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.GroupEsfPathNameValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The GroupForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class GroupForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 425992406206314874L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(GroupForm.class);
	
	static List<String> _orderedProperties;
    
	Button saveButton;
	Button createLikeButton;
	Button deleteButton;
	Button cancelButton;
	Button editButton;

	Button showMemberUsersButton;

	GroupView view;
	GroupViewContainer container;
    GridLayout layout;
	
	Label id;
	Label lastUpdatedTimestamp;
	Label createdByInfo;
    
	// We store the prevBean in case we abandon working on the newBean when we 'create like'
	GroupBean prevBean;
    GroupBean newBean;
    
    // We use this for all our select lists, so we only reload once per Bean being set as our datasource
	EsfUUID[] allGroupListIds;
	EsfPathName[] allGroupListNames;
	
    // We use these for all our member users select list, so we only reload once per Bean being set as our datasource
	EsfUUID[] allMemberUsersUpdateIds;
	String[] allMemberUsersUpdateFullDisplayNames;

	Disclosure permDisclosure;
	
	Disclosure permMemberUsersDisclosure;
	
	Disclosure memberUsersDisclosure;

	
	@SuppressWarnings("deprecation")
	public GroupForm(GroupView view, GroupViewContainer container) {		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
    	setStyleName("GroupForm");
       	setWidth(100, Unit.PERCENTAGE); // hack because seems to have "horizontal scrollbar" issues if the form is shown with vertical scrollbars in place
    	this.view = view;
    	this.container = container;

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
    	layout = new GridLayout(2,8);
       	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.5f);
    	layout.setColumnExpandRatio(1, 0.5f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id, 0, 0);

		lastUpdatedTimestamp = new Label();
		lastUpdatedTimestamp.setContentMode(ContentMode.TEXT);
		lastUpdatedTimestamp.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedTimestamp, 1, 0);
		
		permDisclosure = new Disclosure(vaadinUi.getMsg("GroupForm.permDisclosure.label"));
		permDisclosure.setWidth(100, Unit.PERCENTAGE);
		permDisclosure.setVisible(false);
		layout.addComponent(permDisclosure,0,4,1,4);
		
		memberUsersDisclosure = new Disclosure(vaadinUi.getMsg("GroupForm.memberUsersDisclosure.label"));
		memberUsersDisclosure.setWidth(100, Unit.PERCENTAGE);
		memberUsersDisclosure.setVisible(false);
		layout.addComponent(memberUsersDisclosure,0,5,1,5);
		
		permMemberUsersDisclosure = new Disclosure(vaadinUi.getMsg("GroupForm.permMemberUsersDisclosure.label"));
		permMemberUsersDisclosure.setWidth(100, Unit.PERCENTAGE);
		permMemberUsersDisclosure.setVisible(false);
		layout.addComponent(permMemberUsersDisclosure,0,6,1,6);
		
		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,7,1,7);
		
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);

		User loggedInUser = vaadinUi.getUser();
		UI ui = new UI();
		if ( ui.showSaveButton(loggedInUser, UI.UI_GROUP_VIEW) ) {
			saveButton = new Button(vaadinUi.getMsg("button.save.label"), (ClickListener)this);
			//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
			//saveButton.setClickShortcut(KeyCode.ENTER);
			saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
	    	saveButton.setDescription(vaadinUi.getMsg("button.save.tooltip"));
        	footer.addComponent(saveButton);
		}

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	if ( ui.canUpdate(loggedInUser, UI.UI_GROUP_VIEW) ) {
        	editButton = new Button(vaadinUi.getMsg("button.edit.label"), (ClickListener)this);
        	editButton.setIcon(new ThemeResource(vaadinUi.getMsg("GroupForm.button.edit.icon")));
        	editButton.setDescription(vaadinUi.getMsg("button.edit.tooltip"));
    		footer.addComponent(editButton);
    	}

    	if ( ui.canCreateLike(loggedInUser, UI.UI_GROUP_VIEW) ) {
	    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
	    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("GroupForm.button.createLike.icon")));
	    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    		footer.addComponent(createLikeButton);
		}

    	if ( ui.canDelete(loggedInUser, UI.UI_GROUP_VIEW) ) {
        	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
        	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("GroupForm.button.delete.icon")));
        	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
        	deleteButton.addStyleName("deleteButton");
        	deleteButton.addStyleName("caution");
    		footer.addComponent(deleteButton);
    	}
    	
    	showMemberUsersButton = new Button(vaadinUi.getMsg("GroupForm.button.showMemberUsers.label"), (ClickListener)this);
    	showMemberUsersButton.setIcon(new ThemeResource(vaadinUi.getMsg("GroupForm.button.showMemberUsers.icon")));
    	showMemberUsersButton.setDescription(vaadinUi.getMsg("GroupForm.button.showMemberUsers.tooltip"));
    	showMemberUsersButton.setStyleName(Reindeer.BUTTON_SMALL);
    	showMemberUsersButton.addStyleName("showMemberUsersButton");
    	footer.addComponent(showMemberUsersButton);
         
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -4433302478473482621L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				
				// Status will be an option group (radio buttons and not a text field)
				if ( propertyId.equals("status") ) {
					// Create the radio buttons to have the two possible values.
					OptionGroup statusOptionGroup = new OptionGroup();
					statusOptionGroup.addItem(Literals.STATUS_ENABLED);
					statusOptionGroup.addItem(Literals.STATUS_DISABLED);
					// Associate different labels/captions to those values
					statusOptionGroup.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					statusOptionGroup.setItemCaption(Literals.STATUS_ENABLED, vaadinUi.getPrettyCode().status(Literals.STATUS_ENABLED));
					statusOptionGroup.setItemCaption(Literals.STATUS_DISABLED, vaadinUi.getPrettyCode().status(Literals.STATUS_DISABLED));
					statusOptionGroup.setStyleName("inline");
					statusOptionGroup.setDescription(vaadinUi.getMsg("GroupForm.status.tooltip")); // Doesn't appear to work without a caption on the OptionGroup, and we dont' want one.
					return statusOptionGroup;
				}
					
				if ( propertyId.equals("permListIds") ) {
					return createPermissionSelectBox("GroupForm.permList.label");
				} 
				
				if ( propertyId.equals("permViewDetailsIds") ) {
					return createPermissionSelectBox("GroupForm.permViewDetails.label");
				}
				
				if ( propertyId.equals("permCreateLikeIds") ) {
					return createPermissionSelectBox("GroupForm.permCreateLike.label");
				}
				
				if ( propertyId.equals("permUpdateIds") ) {
					return createPermissionSelectBox("GroupForm.permUpdate.label");
				}
				
				if ( propertyId.equals("permDeleteIds") ) {
					return createPermissionSelectBox("GroupForm.permDelete.label");
				}
				
				if ( propertyId.equals("permMemberUsersListIds") ) {
					return createPermissionSelectBox("GroupForm.permMemberUsersList.label");
				} 
				
				if ( propertyId.equals("permMemberUsersViewDetailsIds") ) {
					return createPermissionSelectBox("GroupForm.permMemberUsersViewDetails.label");
				}
				
				if ( propertyId.equals("permMemberUsersCreateLikeIds") ) {
					return createPermissionSelectBox("GroupForm.permMemberUsersCreateLike.label");
				}
				
				if ( propertyId.equals("permMemberUsersUpdateIds") ) {
					return createPermissionSelectBox("GroupForm.permMemberUsersUpdate.label");
				}
				
				if ( propertyId.equals("permMemberUsersDeleteIds") ) {
					return createPermissionSelectBox("GroupForm.permMemberUsersDelete.label");
				}
				
				if ( propertyId.equals("permMemberUsersManageToDoIds") ) {
					return createPermissionSelectBox("GroupForm.permMemberUsersManageToDo.label");
				}
				
				if ( propertyId.equals("memberUsersIds") ) {
					return createMemberUsersSelectBox("GroupForm.memberUsers.label");
				} 
				
				if ( propertyId.equals("comments") ) {
	            	TextArea ta = new TextArea();
	            	ta.setWidth(100, Unit.PERCENTAGE);
	            	ta.setNullRepresentation("");
	            	ta.setRows(3);
	            	ta.setInputPrompt(vaadinUi.getMsg("inputPrompt.comments"));
	            	ta.setCaption(vaadinUi.getMsg("caption.comments"));
	            	ta.setDescription(vaadinUi.getMsg("tooltip.comments"));
	            	/*
	            	ta.addFocusListener( new FieldEvents.FocusListener() {
						private static final long serialVersionUID = 1692505847582239282L;

						@Override
						public void focus(FocusEvent event) {
							if ( saveButton != null )
								saveButton.removeClickShortcut();
						}
	            	});
	            	ta.addBlurListener( new FieldEvents.BlurListener() {
						private static final long serialVersionUID = 8842453565968165325L;

						@Override
						public void blur(BlurEvent event) {
							if ( saveButton != null )
								saveButton.setClickShortcut(KeyCode.ENTER);
						}
					});
					*/
	            	return ta;
				}
				
				Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("pathName")) {
                	TextField tf = (TextField)field;
                    tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    tf.setCaption(vaadinUi.getMsg("caption.esfPathName"));
                    tf.setDescription(vaadinUi.getMsg("GroupForm.pathName.tooltip"));
                    tf.addValidator(new GroupEsfPathNameValidator(getCurrentBean()));
                    tf.setConversionError(vaadinUi.getMsg("validator.esfpathname.message"));
                } else if ( propertyId.equals("description") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("caption.description"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.description"));
                }

                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created.");
    }
    
    private void resetCaches() {
		allGroupListIds = null;
		allGroupListNames = null;
		
		allMemberUsersUpdateIds = null;
		allMemberUsersUpdateFullDisplayNames = null;
		
		GroupBean bean = getCurrentBean();
		if ( bean != null ) {
			bean.resetCaches();
			allGroupListIds = bean.allGroupListIds();
			allGroupListNames = bean.allGroupListNames();
			
			allMemberUsersUpdateIds = bean.allMemberUsersUpdateIds();
			allMemberUsersUpdateFullDisplayNames = bean.allMemberUsersUpdateFullDisplayNames();
			reloadPermissionSelectBoxes();
			reloadMemberUsersPermissionSelectBoxes();
		}
    }
    
    private void setupPermissionSelectBox(TwinColSelect selectList) {
    	if ( selectList == null )
    		return;
    	
    	selectList.removeAllItems();
    	
		// Set all the possible values
		for( int i=0; i < allGroupListIds.length; ++i ) {
			selectList.addItem(allGroupListIds[i]);
		}
		// Associate labels with our ID values
		selectList.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		for( int i=0; i < allGroupListIds.length; ++i ) {
			selectList.setItemCaption(allGroupListIds[i], allGroupListNames[i].toPlainString());
		}
		
		selectList.setRows( Math.min(allGroupListIds.length,5) );
		selectList.discard();
    }
    
    private void reloadPermissionSelectBoxes() {
    	setupPermissionSelectBox((TwinColSelect)getField("permListIds"));
    	setupPermissionSelectBox((TwinColSelect)getField("permViewDetailsIds"));
    	setupPermissionSelectBox((TwinColSelect)getField("permCreateLikeIds"));
    	setupPermissionSelectBox((TwinColSelect)getField("permUpdateIds"));
    	setupPermissionSelectBox((TwinColSelect)getField("permDeleteIds"));
    	setupPermissionSelectBox((TwinColSelect)getField("permMemberUsersListIds"));
    	setupPermissionSelectBox((TwinColSelect)getField("permMemberUsersViewDetailsIds"));
    	setupPermissionSelectBox((TwinColSelect)getField("permMemberUsersCreateLikeIds"));
    	setupPermissionSelectBox((TwinColSelect)getField("permMemberUsersUpdateIds"));
    	setupPermissionSelectBox((TwinColSelect)getField("permMemberUsersDeleteIds"));
    	setupPermissionSelectBox((TwinColSelect)getField("permMemberUsersManageToDoIds"));
    }
    
    private TwinColSelect createPermissionSelectBox(String vaadinMessageCaption) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		TwinColSelect selectList = new TwinColSelect();
    	selectList.setLeftColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".left"));
    	selectList.setRightColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".right"));
    	selectList.setWidth(100, Unit.PERCENTAGE);
        selectList.addValidator(new SelectValidator(selectList));
		selectList.setNullSelectionAllowed(true);
		selectList.setMultiSelect(true);
		return selectList;
    }
    
    private void reloadMemberUsersPermissionSelectBoxes() {
    	TwinColSelect selectList = (TwinColSelect)getField("memberUsersIds");
    	if ( selectList == null )
    		return;
    	
    	selectList.removeAllItems();
    	
		// Set all the possible values
		for( int i=0; i < allMemberUsersUpdateIds.length; ++i ) {
			selectList.addItem(allMemberUsersUpdateIds[i]);
		}
		// Associate labels with our ID values
		selectList.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		for( int i=0; i < allMemberUsersUpdateIds.length; ++i ) {
			selectList.setItemCaption(allMemberUsersUpdateIds[i], allMemberUsersUpdateFullDisplayNames[i]);
		}
		
		selectList.setRows( Math.min(allMemberUsersUpdateIds.length,5) );
		selectList.discard();
    }
    
    private TwinColSelect createMemberUsersSelectBox(String vaadinMessageCaption) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		TwinColSelect selectList = new TwinColSelect();
    	selectList.setLeftColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".left"));
    	selectList.setRightColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".right"));
    	selectList.setWidth(100, Unit.PERCENTAGE);
        selectList.addValidator(new SelectValidator(selectList));
		selectList.setNullSelectionAllowed(true);
		selectList.setMultiSelect(true);
		return selectList;
    }
    
    
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
    			
    		// If we're saving a new bean
			Errors errors = new Errors();
    		if ( newBean != null ) {
        		if ( newBean.save(errors) ) {
        			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",newBean.getPathName()) );
            		// We need to add the new bean to the container
            		container.addItem(newBean);
            		view.select(newBean); // reselect our bean so all updates cleanly
            		prevBean = newBean = null; // and we're not longer working on a new bean
        		} else {
        			vaadinUi.show(errors);
        		}
    		} else {
    			GroupBean currBean = getCurrentBean();
    			if ( currBean.save(errors) ) {
        			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",currBean.getPathName()) );
            		view.select(currBean); // reselect our bean so all updates cleanly
        		} else {
        			vaadinUi.show(errors);
        		}
    		}
        } else if ( source == cancelButton ) {
    		discard();
    		vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
        	if ( newBean != null ) {
        		setItemDataSource(null);
        		view.unselectAll(); // we'll unselect and the reselect our prevBean
        		view.select(prevBean);
        		prevBean = newBean = null; // we're not doing a new bean anymore
        	}
        	setReadOnly(true);
        } else if ( source == editButton ) {
        	setReadOnly(false);
        } else if ( source == deleteButton ) {
	    	ConfirmDialog.show(com.vaadin.ui.UI.getCurrent(), 
	    			vaadinUi.getMsg("GroupForm.button.delete.ConfirmDialog.caption"),
	    			vaadinUi.getMsg("GroupForm.button.delete.ConfirmDialog.message"),
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
	    	        new ConfirmDialog.Listener() {
						private static final long serialVersionUID = 5850076071269147044L;

						public void onClose(ConfirmDialog dialog) {
	    	                if (dialog.isConfirmed()) {
	    	                	discard();
	    	                	GroupBean currBean = getCurrentBean();
	    	                	if ( currBean != null ) {
	    	                		setItemDataSource(null);
	    	                		view.unselectAll(); // we'll unselect it and remove it from our container
	    	                		if ( newBean == null ) {
	    	            				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	                			Errors errors = new Errors();
	    	                			if ( currBean.delete(errors) ) {
	    	                        		container.removeItem(currBean);
	    	                        		vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",currBean.getPathName()) );
	    	                			} else {
	    	                				vaadinUi.show(errors);
	    	                			}
	    	                		} else {
	    	                			newBean.delete(null);
	    	                    		view.select(prevBean);
	    	                			prevBean = newBean = null;
	    	                		}
	    	                	}	
	    	                }
	    	            }
	    	        });
        } else if ( source == createLikeButton ) {
        	GroupBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		createLike(currBean);
        	}	
        } else if ( source == showMemberUsersButton ) {
        	GroupBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		GroupMemberUsersList list = new GroupMemberUsersList(currBean);
        		list.show();
        	}
        }
    }
	
	public String getCurrentBeanName() {
		GroupBean bean = getCurrentBean();
		return bean == null ? "(None)" : bean.getPathName().toString();
	}
    
	GroupBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public GroupBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<GroupBean> bi = (BeanItem<GroupBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
    	if (newDataSource != null) {
    		GroupBean bean = getBean(newDataSource);
    		if ( _orderedProperties == null ) {
    			_orderedProperties = Arrays.asList("status","pathName","description","comments","permListIds","permViewDetailsIds","permCreateLikeIds","permUpdateIds","permDeleteIds",
    					"memberUsersIds","permMemberUsersListIds","permMemberUsersViewDetailsIds","permMemberUsersCreateLikeIds","permMemberUsersUpdateIds","permMemberUsersDeleteIds","permMemberUsersManageToDoIds");
    		}
    		super.setItemDataSource(newDataSource, _orderedProperties);
    		
    		setupForm(bean);
    		
    		setReadOnly( ! bean.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		newBean = bean.group().doInsert() ? bean : null;
    		if ( newBean != null ) {
    			TextField tf = (TextField)getField("pathName");
    			tf.selectAll();
    			tf.focus();
    		}
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	GroupBean bean = getCurrentBean();
    	
    	// We have these only while editing/creating new
    	if ( saveButton != null ) {
    		saveButton.setVisible(!readOnly);
    	}
    	cancelButton.setVisible(!readOnly);

    	// We have this only while in view mode
    	if ( editButton != null ) {
        	editButton.setVisible(readOnly && bean != null && bean.hasPermUpdate());
    	}
    	
    	// This one we show this in all modes if has permission
    	if ( createLikeButton != null ) {
        	createLikeButton.setVisible(bean != null && bean.hasPermCreateLike() && bean.group().doUpdate() && ! bean.group().isReserved()); // only want create like on an existing bean (and not a reserved group)
    	}
    	
    	if ( deleteButton != null ) {
        	deleteButton.setVisible(bean != null && bean.hasPermDelete());
    	}
    	
    	showMemberUsersButton.setVisible( bean != null && bean.hasPermMemberUsersList() );
    	
    	// We don't want any updates to the status, name or permissions on these specialty groups
    	if ( bean != null && bean.group().isReserved() ) {
    		getField("pathName").setReadOnly(true);
    		getField("status").setReadOnly(true);
    		
   			_orderedProperties = Arrays.asList("status","pathName","description","comments","permListIds","permViewDetailsIds","permCreateLikeIds","permUpdateIds","permDeleteIds",
					"memberUsersIds","permMemberUsersListIds","permMemberUsersViewDetailsIds","permMemberUsersCreateLikeIds","permMemberUsersUpdateIds","permMemberUsersDeleteIds","permMemberUsersManageToDoIds");

    		
    		getField("permListIds").setReadOnly(true);
    		getField("permViewDetailsIds").setReadOnly(true);
    		getField("permCreateLikeIds").setReadOnly(true);
    		getField("permUpdateIds").setReadOnly(true);
    		getField("permDeleteIds").setReadOnly(true);
    		
    		getField("permMemberUsersListIds").setReadOnly(true);
    		getField("permMemberUsersViewDetailsIds").setReadOnly(true);
    		getField("permMemberUsersCreateLikeIds").setReadOnly(true);
    		getField("permMemberUsersUpdateIds").setReadOnly(true);
    		getField("permMemberUsersDeleteIds").setReadOnly(true);	
    		getField("permMemberUsersManageToDoIds").setReadOnly(true);	
    	}
    }
    
    public void createLike(GroupBean likeBean) {
		// Create a temporary item for the form
    	GroupBean createdBean = likeBean.createLike();
        
        BeanItem<GroupBean> bi = new BeanItem<GroupBean>( createdBean );
        setItemDataSource(bi);
    	prevBean = likeBean;
        setReadOnly(false);
    }
    
    void setupForm(GroupBean bean) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		// Clear our cached list
		resetCaches();

		layout.setCaption(vaadinUi.getMsg("GroupForm.caption",bean.getPathName()));

		id.setValue( vaadinUi.getMsg("GroupForm.label.id",bean.getId()) );
		
		if ( bean.group().doInsert() ) {
			lastUpdatedTimestamp.setCaption(vaadinUi.getMsg("GroupForm.label.lastUpdated.whenNew"));
			lastUpdatedTimestamp.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.group().getLastUpdatedByUserId());
			lastUpdatedTimestamp.setCaption( vaadinUi.getMsg("GroupForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedTimestamp.setIcon(null);
		}
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.group().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("GroupForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );

		createPermDisclosure();
		createPermMemberUsersDisclosure();
		createMemberUsersDisclosure();
    }
    
    void createPermDisclosure() {
    	VerticalLayout permLayout = new VerticalLayout();
    	permLayout.setMargin(false);
    	permLayout.setSpacing(true);
    	
		permLayout.addComponent(getField("permListIds"));
		permLayout.addComponent(getField("permViewDetailsIds"));
		permLayout.addComponent(getField("permCreateLikeIds"));
		permLayout.addComponent(getField("permUpdateIds"));
		permLayout.addComponent(getField("permDeleteIds"));
				
		permDisclosure.setContent(permLayout);
		permDisclosure.setVisible(true);
    }

    
    void createPermMemberUsersDisclosure() {
    	VerticalLayout permLayout = new VerticalLayout();
    	permLayout.setMargin(false);
    	permLayout.setSpacing(true);
    	
		permLayout.addComponent(getField("permMemberUsersListIds"));
		permLayout.addComponent(getField("permMemberUsersViewDetailsIds"));
		permLayout.addComponent(getField("permMemberUsersCreateLikeIds"));
		permLayout.addComponent(getField("permMemberUsersUpdateIds"));
		permLayout.addComponent(getField("permMemberUsersDeleteIds"));
		permLayout.addComponent(getField("permMemberUsersManageToDoIds"));
				
		permMemberUsersDisclosure.setContent(permLayout);
		permMemberUsersDisclosure.setVisible(true);
    }

    void createMemberUsersDisclosure() {
    	VerticalLayout memberUsersLayout = new VerticalLayout();
    	memberUsersLayout.setMargin(false);
    	memberUsersLayout.setSpacing(true);
    	
    	memberUsersLayout.addComponent(getField("memberUsersIds"));
				
    	memberUsersDisclosure.setContent(memberUsersLayout);
    	memberUsersDisclosure.setVisible(true);
    }

    
 	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("status")) {
            layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("pathName")) {
        	layout.addComponent(field, 0, 2);
        } else if (propertyId.equals("description")) {
            layout.addComponent(field, 1, 2);
        } else if (propertyId.equals("comments")) {
        	layout.addComponent(field, 0, 3, 1, 3);
        }
    }

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("GroupView.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}