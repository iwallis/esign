// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.library;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.vaadin.jouni.animator.Disclosure;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DocumentStyleInfo;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.UI;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.LibraryEsfPathNameValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The LibraryForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class LibraryForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 1418394903252713453L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibraryForm.class);
	
	static List<String> _orderedProperties;
    
	Button saveButton;
	Button createLikeButton; // TODO: Do we want someone to be able to duplicate a library? It may not be a good idea, but like delete, needs a lot of chaining to other objects to get right
	Button deleteButton; // TODO: Still trying to figure out what we mean by deleting a library.
	Button cancelButton;
	Button editButton;
	Button importButton;
	Button exportButton;

	LibraryView view;
	LibraryViewContainer container;
    GridLayout layout;
	
	Label id;
	Label lastUpdatedTimestamp;
	Label createdByInfo;
    
	// We store the prevBean in case we abandon working on the newBean when we 'create like'
	LibraryBean prevBean;
    LibraryBean newBean;
    
    // We use this for all our select lists, so we only reload once per Bean being set as our datasource
	EsfUUID[] allGroupListIds;
	EsfPathName[] allGroupListNames;
	
	Disclosure permDisclosure;
	
	@SuppressWarnings("deprecation")
	public LibraryForm(LibraryView view, LibraryViewContainer container) {
    	setStyleName("LibraryForm");
       	setWidth(100, Unit.PERCENTAGE); // hack because seems to have "horizontal scrollbar" issues if the form is shown with vertical scrollbars in place
    	this.view = view;
    	this.container = container;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
    	layout = new GridLayout(3,6);
       	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.34f);
    	layout.setColumnExpandRatio(1, 0.33f);
    	layout.setColumnExpandRatio(2, 0.33f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id, 0, 0);

		lastUpdatedTimestamp = new Label();
		lastUpdatedTimestamp.setContentMode(ContentMode.TEXT);
		lastUpdatedTimestamp.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedTimestamp, 1, 0, 2, 0);
		
		permDisclosure = new Disclosure(vaadinUi.getMsg("LibraryForm.permDisclosure.label"));
		permDisclosure.setWidth(100, Unit.PERCENTAGE);
		permDisclosure.setVisible(false);
		layout.addComponent(permDisclosure,0,4,2,4);
		
		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,5,2,5);
		
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);

		User loggedInUser = vaadinUi.getUser();
		UI ui = new UI();
		if ( ui.showSaveButton(loggedInUser, UI.UI_LIBRARY_VIEW) ) {
			saveButton = new Button(vaadinUi.getMsg("button.save.label"), (ClickListener)this);
			//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
			//saveButton.setClickShortcut(KeyCode.ENTER);
	    	saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
	    	saveButton.setDescription(vaadinUi.getMsg("button.save.tooltip"));
        	footer.addComponent(saveButton);
		}

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	if ( ui.canUpdate(loggedInUser, UI.UI_LIBRARY_VIEW) ) {
        	editButton = new Button(vaadinUi.getMsg("button.edit.label"), (ClickListener)this);
        	editButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibraryForm.button.edit.icon")));
        	editButton.setDescription(vaadinUi.getMsg("button.edit.tooltip"));
    		footer.addComponent(editButton);
    	}

    	if ( ui.canCreateLike(loggedInUser, UI.UI_LIBRARY_VIEW) ) {
	    	createLikeButton = new Button(vaadinUi.getMsg("LibraryForm.button.createLike.label"), (ClickListener)this);
	    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibraryForm.button.createLike.icon")));
	    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    		footer.addComponent(createLikeButton);
		}

    	/*
    	if ( ui.canDelete(loggedInUser, UI.UI_LIBRARY_VIEW) ) {
        	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
        	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibraryForm.button.delete.icon")));
        	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
        	deleteButton.addStyleName("deleteButton");
        	deleteButton.addStyleName("caution");
    		footer.addComponent(deleteButton);
    	}
    	*/
    	
		exportButton = new Button(vaadinUi.getMsg("LibraryForm.button.export.label"), (ClickListener)this);
		exportButton.setStyleName(Reindeer.BUTTON_SMALL);
    	exportButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.download.icon")));
    	exportButton.setDescription(vaadinUi.getMsg("LibraryForm.button.export.tooltip"));
    	exportButton.addStyleName("exportButton");
		footer.addComponent(exportButton);
    	
    	if ( ui.canUpdate(loggedInUser, UI.UI_LIBRARY_VIEW) ) {
	    	importButton = new Button(vaadinUi.getMsg("LibraryForm.button.import.label"), (ClickListener)this);
	    	importButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.upload.icon")));
	    	importButton.setStyleName(Reindeer.BUTTON_SMALL);
	    	importButton.setDescription(vaadinUi.getMsg("LibraryForm.button.import.tooltip"));
	    	importButton.addStyleName("importButton");
			footer.addComponent(importButton);
    	}
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 3442892898371681637L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				// Load our cache if we don't have it yet since we'll need it for all our permission selection boxes
				if ( allGroupListIds == null ) {
					LibraryBean bean = getCurrentBean();
					allGroupListIds = bean.allGroupListIds();
					allGroupListNames = bean.allGroupListNames();
				}

				// Status will be an option group (radio buttons and not a text field)
				if ( propertyId.equals("status") ) {
					// Create the radio buttons to have the two possible values.
					OptionGroup statusOptionGroup = new OptionGroup();
					statusOptionGroup.addItem(Literals.STATUS_ENABLED);
					statusOptionGroup.addItem(Literals.STATUS_DISABLED);
					// Associate different labels/captions to those values
					statusOptionGroup.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					statusOptionGroup.setItemCaption(Literals.STATUS_ENABLED, vaadinUi.getPrettyCode().status(Literals.STATUS_ENABLED));
					statusOptionGroup.setItemCaption(Literals.STATUS_DISABLED, vaadinUi.getPrettyCode().status(Literals.STATUS_DISABLED));
					statusOptionGroup.setStyleName("inline");
					statusOptionGroup.setDescription(vaadinUi.getMsg("LibraryForm.status.tooltip")); // Doesn't appear to work without a caption on the OptionGroup, and we dont' want one.
					return statusOptionGroup;
				}
					
				if ( propertyId.equals("permListIds") ) {
					return createPermissionSelectBox("LibraryForm.permList.label");
				} 
				
				if ( propertyId.equals("permViewDetailsIds") ) {
					return createPermissionSelectBox("LibraryForm.permViewDetails.label");
				}
				
				if ( propertyId.equals("permCreateLikeIds") ) {
					return createPermissionSelectBox("LibraryForm.permCreateLike.label");
				}
				
				if ( propertyId.equals("permUpdateIds") ) {
					return createPermissionSelectBox("LibraryForm.permUpdate.label");
				}
				
				if ( propertyId.equals("permDeleteIds") ) {
					return createPermissionSelectBox("LibraryForm.permDelete.label");
				}
				
				if ( propertyId.equals("defaultDocumentStyleId") ) {
					NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibraryForm.defaultDocumentStyleId.label"));
					select.setNullSelectionAllowed(false); 
					select.setImmediate(false);
					select.setRequired(true);
					select.setMultiSelect(false);
	                select.addValidator(new SelectValidator(select));
					select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		    		// Update our document styles to include both the current library and the template library
					Library templateLibrary = Library.Manager.getTemplate();
		        	Collection<DocumentStyleInfo> allDocumentStyles;
		    		LibraryBean bean = getCurrentBean();
		    		if ( bean != null ) {
			        	allDocumentStyles = DocumentStyleInfo.Manager.getAll(bean.library());
			    		if ( ! bean.library().equals(templateLibrary) ) {
				        	allDocumentStyles.addAll(DocumentStyleInfo.Manager.getAll(templateLibrary));
			    		}
		    		} else { // shouldn't be the case, but we'll at least have the template's document style
		    			allDocumentStyles = DocumentStyleInfo.Manager.getAll(templateLibrary);
		    		}
		        	for( DocumentStyleInfo dsi : allDocumentStyles ) {
		        		select.addItem(dsi.getId());
		        		select.setItemCaption( dsi.getId(), dsi.getEsfName().toString() );
		        	}
                	return select;
				}
				
				if ( propertyId.equals("comments") ) {
	            	TextArea ta = new TextArea();
	            	ta.setWidth(100, Unit.PERCENTAGE);
	            	ta.setNullRepresentation("");
	            	ta.setRows(3);
	            	ta.setInputPrompt(vaadinUi.getMsg("inputPrompt.comments"));
	            	ta.setCaption(vaadinUi.getMsg("caption.comments"));
	            	ta.setDescription(vaadinUi.getMsg("tooltip.comments"));
	            	/*
	            	ta.addFocusListener( new FieldEvents.FocusListener() {
						private static final long serialVersionUID = 1126972597950041328L;

						@Override
						public void focus(FocusEvent event) {
							if ( saveButton != null )
								saveButton.removeClickShortcut();
						}
	            	});
	            	ta.addBlurListener( new FieldEvents.BlurListener() {
						private static final long serialVersionUID = 4550619902667095797L;

						@Override
						public void blur(BlurEvent event) {
							if ( saveButton != null )
								saveButton.setClickShortcut(KeyCode.ENTER);
						}
					});
					*/
	            	return ta;
				}
				
				Field<?> field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("pathName")) {
                	TextField tf = (TextField)field;
                    tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    tf.setCaption(vaadinUi.getMsg("caption.esfPathName"));
                    tf.setDescription(vaadinUi.getMsg("LibraryForm.pathName.tooltip"));
                    tf.addValidator(new LibraryEsfPathNameValidator(getCurrentBean()));
                    tf.setConversionError(vaadinUi.getMsg("validator.esfpathname.message"));
                } else if ( propertyId.equals("description") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                	tf.setInputPrompt(vaadinUi.getMsg("inputPrompt.description"));
                    tf.setCaption(vaadinUi.getMsg("caption.description"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.description"));
                }

                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
		allGroupListIds = null;
		allGroupListNames = null;
    }
    
    private TwinColSelect createPermissionSelectBox(String vaadinMessageCaption) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	TwinColSelect selectList = new TwinColSelect();
    	selectList.setLeftColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".left"));
    	selectList.setRightColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".right"));
    	selectList.setWidth(100, Unit.PERCENTAGE);
        selectList.addValidator(new SelectValidator(selectList));
		
		// Set all the possible values
		for( int i=0; i < allGroupListIds.length; ++i ) {
			selectList.addItem(allGroupListIds[i]);
		}
		// Associate labels with our ID values
		selectList.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		for( int i=0; i < allGroupListIds.length; ++i ) {
			selectList.setItemCaption(allGroupListIds[i], allGroupListNames[i].toPlainString());
		}
		
		selectList.setRows( Math.min(allGroupListIds.length,5) );
		selectList.setNullSelectionAllowed(true);
		selectList.setMultiSelect(true);
		return selectList;
    }
    
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
    			
    		// If we're saving a new bean
			Errors errors = new Errors();
    		if ( newBean != null ) {
        		if ( newBean.save(errors) ) {
        			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",newBean.getPathName()) );
            		vaadinUi.notifyLibraryChangeListeners(newBean.library());
            		// We need to add the new bean to the container
            		container.addItem(newBean);
            		view.select(newBean); // reselect our bean so all updates cleanly
            		prevBean = newBean = null; // and we're not longer working on a new bean
        		} else {
        			vaadinUi.show(errors);
        		}
    		} else {
    			LibraryBean currBean = getCurrentBean();
    			if ( currBean.save(errors) ) {
        			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",currBean.getPathName()) );
            		vaadinUi.notifyLibraryChangeListeners(currBean.library());
            		view.select(currBean); // reselect our bean so all updates cleanly
        		} else {
        			vaadinUi.show(errors);
        		}
    		}
        } else if ( source == cancelButton ) {
    		discard();
    		vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
        	if ( newBean != null ) {
        		setItemDataSource(null);
        		view.unselectAll(); // we'll unselect and the reselect our prevBean
        		view.select(prevBean);
        		prevBean = newBean = null; // we're not doing a new bean anymore
        	}
        	setReadOnly(true);
        } else if ( source == editButton ) {
        	setReadOnly(false);
        } else if ( source == deleteButton ) {
        	discard();
        	LibraryBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		setItemDataSource(null);
        		view.unselectAll(); // we'll unselect it and remove it from our container
        		if ( newBean == null ) {
        			Errors errors = new Errors();
        			if ( currBean.delete(errors) ) {
                		vaadinUi.notifyLibraryChangeListeners(currBean.library());
                		container.removeItem(currBean);
                		vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",currBean.getPathName()) );
        			} else {
        				vaadinUi.show(errors);
        			}
        		} else {
        			newBean.delete(null);
            		view.select(prevBean);
        			prevBean = newBean = null;
        		}
        	}	
        } else if ( source == createLikeButton ) {
        	LibraryBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		createLike(currBean);
        	}	
        } else if ( source == exportButton ) {
        	LibraryBean currBean = getCurrentBean();
        	if ( currBean != null ) {
                ExportView exportView = new ExportView(currBean.library());
                exportView.initView();
        		
                ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("LibraryForm.ExportView.window.caption",currBean.library().getPathName()), exportView);
                w.center();
            	w.setWidth(90, Unit.PERCENTAGE);
            	w.setHeight(470, Unit.PIXELS);

            	exportView.activateView(EsfView.OpenMode.WINDOW, "");
            	vaadinUi.addWindowToUI(w);	
        	}	
        } else if ( source == importButton ) {
        	LibraryBean currBean = getCurrentBean();
        	if ( currBean != null ) {
                ImportView importView = new ImportView(currBean.library());
                importView.initView();
        		
                ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("LibraryForm.ImportView.window.caption",currBean.library().getPathName()), importView);
                w.center();
            	w.setWidth(90, Unit.PERCENTAGE);
            	w.setHeight(510, Unit.PIXELS);

            	importView.activateView(EsfView.OpenMode.WINDOW, "");
            	vaadinUi.addWindowToUI(w);	
        	}	
        }
    }
	
	public String getCurrentBeanName() {
		LibraryBean bean = getCurrentBean();
		return bean == null ? "(None)" : bean.getPathName().toString();
	}
    
	LibraryBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public LibraryBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibraryBean> bi = (BeanItem<LibraryBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached list
		resetCaches();
		
    	if (newDataSource != null) {
    		LibraryBean bean = getBean(newDataSource);
    		
    		if ( _orderedProperties == null ) {
    			_orderedProperties = Arrays.asList("status","pathName","description","defaultDocumentStyleId","comments","permListIds","permViewDetailsIds",
    					"permCreateLikeIds","permUpdateIds","permDeleteIds");
    		}
    		super.setItemDataSource(newDataSource, _orderedProperties);
    		
    		setupForm(bean);
    		
    		setReadOnly( ! bean.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		newBean = bean.library().doInsert() ? bean : null;
    		if ( newBean != null ) {
    			TextField tf = (TextField)getField("pathName");
    			tf.selectAll();
    			tf.focus();
    		}
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	LibraryBean bean = getCurrentBean();
    	
    	// We have these only while editing/creating new
    	if ( saveButton != null ) {
    		saveButton.setVisible(!readOnly && bean != null && bean.hasPermUpdate());
    	}
    	cancelButton.setVisible(!readOnly);

    	// We have this only while in view mode
    	if ( editButton != null ) {
        	editButton.setVisible(readOnly && bean != null && bean.hasPermUpdate());
    	}
    	
    	// This one we show this in all modes if has permission and it's not an ESF reserved library
    	if ( createLikeButton != null ) {
        	createLikeButton.setVisible(bean != null && ! bean.getPathName().hasReservedPathPrefix() && bean.hasPermCreateLike() && bean.library().doUpdate()); // only want create like on an existing bean
    	}
    	
    	if ( deleteButton != null ) {
        	deleteButton.setVisible(bean != null && bean.hasPermDelete());
    	}
    	
    	if ( importButton != null ) {
    		importButton.setVisible(!readOnly && bean != null && bean.hasPermUpdate());
    	}
    	
    	if ( exportButton != null ) {
    		exportButton.setVisible(bean != null && bean.hasPermViewDetails());
    	}
    	
    	// We don't want any updates to the status, name or delete permissions on these specialty libraries
    	if ( bean != null && bean.library().isReserved() ) {
    		getField("pathName").setReadOnly(true);
    		getField("status").setReadOnly(true);
    		getField("permDeleteIds").setReadOnly(true);
    	}
    }
    
    public void createLike(LibraryBean likeBean) {
        // Create a temporary item for the form
    	LibraryBean createdBean = likeBean.createLike();
    	if ( createdBean == null ) {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		vaadinUi.showError("Unexpected library create like exception", "Failed to create a new library like the current one.");
    		setItemDataSource(null);
    		return;
    	}
        
        BeanItem<LibraryBean> bi = new BeanItem<LibraryBean>( createdBean );
        setItemDataSource(bi);
    	prevBean = likeBean;
        setReadOnly(false);
    }
    
    void setupForm(LibraryBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	layout.setCaption(vaadinUi.getMsg("LibraryForm.caption",bean.getPathName()));

		id.setValue( vaadinUi.getMsg("LibraryForm.label.id",bean.getId()) );
		
		if ( bean.library().doInsert() ) {
			lastUpdatedTimestamp.setCaption(vaadinUi.getMsg("LibraryForm.label.lastUpdated.whenNew"));
			lastUpdatedTimestamp.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.library().getLastUpdatedByUserId());
			lastUpdatedTimestamp.setCaption( vaadinUi.getMsg("LibraryForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedTimestamp.setIcon(null);
		}
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.library().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("LibraryForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );

		createPermDisclosure();
    }
    
    void createPermDisclosure() {
    	VerticalLayout permLayout = new VerticalLayout();
    	permLayout.setMargin(false);
    	permLayout.setSpacing(true);
    	
		permLayout.addComponent(getField("permListIds"));
		permLayout.addComponent(getField("permViewDetailsIds"));
		permLayout.addComponent(getField("permCreateLikeIds"));
		permLayout.addComponent(getField("permUpdateIds"));
		permLayout.addComponent(getField("permDeleteIds"));
				
		permDisclosure.setContent(permLayout);
		permDisclosure.setVisible(true);
    }

    
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("status")) {
            layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("pathName")) {
        	layout.addComponent(field, 0, 2);
        } else if (propertyId.equals("description")) {
            layout.addComponent(field, 1, 2);
        } else if (propertyId.equals("defaultDocumentStyleId")) {
            layout.addComponent(field, 2, 2);
        } else if (propertyId.equals("comments")) {
        	layout.addComponent(field, 0, 3, 2, 3);
        }
    }

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibraryView.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}