// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.buttonmessage;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.ButtonMessageVersion;
import com.esignforms.open.prog.ButtonMessageVersionInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibButtonMessageVersionBean implements Serializable, Comparable<LibButtonMessageVersionBean> {
	private static final long serialVersionUID = -2524933067626374237L;

	private ButtonMessageVersionInfo buttonmessageVerInfo;
	private String versionLabel;
	
	private ButtonMessageVersion buttonmessageVersion;
	
	public LibButtonMessageVersionBean(ButtonMessageVersionInfo buttonmessageVerInfo, String versionLabel) {
		this.buttonmessageVerInfo = buttonmessageVerInfo;
		this.versionLabel = versionLabel;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibButtonMessageVersionBean )
			return buttonmessageVerInfo.equals(((LibButtonMessageVersionBean)o).buttonmessageVerInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return buttonmessageVerInfo.hashCode();
    }
	public int compareTo(LibButtonMessageVersionBean d) {
		return buttonmessageVerInfo.compareTo(d.buttonmessageVerInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public ButtonMessageVersionInfo buttonmessageVerInfo() {
		return buttonmessageVerInfo;
	}

	public ButtonMessageVersion buttonmessageVersion() {
		if ( buttonmessageVersion == null )
			buttonmessageVersion = ButtonMessageVersion.Manager.getById(buttonmessageVerInfo.getId());
		return buttonmessageVersion;
	}

	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( buttonmessageVersion().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			return true;
		} 

		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
		buttonmessageVersion = null;
	}
	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return buttonmessageVerInfo.getId();
	}

	public int getVersion() {
		return buttonmessageVerInfo.getVersion();
	}
	
	public String getVersionLabel() {
		return versionLabel;
	}

	public EsfDateTime getCreatedTimestamp() {
		return buttonmessageVerInfo.getCreatedTimestamp();
	}
	public String formatCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return buttonmessageVerInfo.getCreatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return buttonmessageVerInfo.getCreatedTimestamp().toLogString(vaadinUi.getUser());
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return buttonmessageVerInfo.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return buttonmessageVerInfo.getLastUpdatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return buttonmessageVerInfo.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser());
	}
	
    public EsfString getPackageFooterHtml()
    {
    	return buttonmessageVersion().getPackageFooterHtml();
    }
    public void setPackageFooterHtml(EsfString v)
    {
    	buttonmessageVersion().setPackageFooterHtml(v);
    }

    public EsfString getDocumentFooterHtml()
    {
    	return buttonmessageVersion().getDocumentFooterHtml();
    }
    public void setDocumentFooterHtml(EsfString v)
    {
    	buttonmessageVersion().setDocumentFooterHtml(v);
    }

    public EsfString getPackageButtonContinueToDo()
    {
    	return buttonmessageVersion().getPackageButtonContinueToDo();
    }
    public void setPackageButtonContinueToDo(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonContinueToDo(v);
    }

    public EsfString getPackageButtonContinueToDoTitle()
    {
    	return buttonmessageVersion().getPackageButtonContinueToDoTitle();
    }
    public void setPackageButtonContinueToDoTitle(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonContinueToDoTitle(v);
    }

    public EsfString getPackageButtonContinueDone()
    {
    	return buttonmessageVersion().getPackageButtonContinueDone();
    }
    public void setPackageButtonContinueDone(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonContinueDone(v);
    }

    public EsfString getPackageButtonContinueDoneTitle()
    {
    	return buttonmessageVersion().getPackageButtonContinueDoneTitle();
    }
    public void setPackageButtonContinueDoneTitle(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonContinueDoneTitle(v);
    }

    public EsfString getPackageButtonEditDocument()
    {
    	return buttonmessageVersion().getPackageButtonEditDocument();
    }
    public void setPackageButtonEditDocument(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonEditDocument(v);
    }

    public EsfString getPackageButtonEditDocumentTitle()
    {
    	return buttonmessageVersion().getPackageButtonEditDocumentTitle();
    }
    public void setPackageButtonEditDocumentTitle(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonEditDocumentTitle(v);
    }

    public EsfString getPackageButtonViewCompletedDocument()
    {
    	return buttonmessageVersion().getPackageButtonViewCompletedDocument();
    }
    public void setPackageButtonViewCompletedDocument(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonViewCompletedDocument(v);
    }

    public EsfString getPackageButtonViewCompletedDocumentTitle()
    {
    	return buttonmessageVersion().getPackageButtonViewCompletedDocumentTitle();
    }
    public void setPackageButtonViewCompletedDocumentTitle(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonViewCompletedDocumentTitle(v);
    }

    public EsfString getPackageButtonDelete()
    {
    	return buttonmessageVersion().getPackageButtonDelete();
    }
    public void setPackageButtonDelete(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonDelete(v);
    }

    public EsfString getPackageButtonDeleteTitle()
    {
    	return buttonmessageVersion().getPackageButtonDeleteTitle();
    }
    public void setPackageButtonDeleteTitle(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonDeleteTitle(v);
    }

    public EsfString getPackageButtonNotCompleting()
    {
    	return buttonmessageVersion().getPackageButtonNotCompleting();
    }
    public void setPackageButtonNotCompleting(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonNotCompleting(v);
    }

    public EsfString getPackageButtonNotCompletingTitle()
    {
    	return buttonmessageVersion().getPackageButtonNotCompletingTitle();
    }
    public void setPackageButtonNotCompletingTitle(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonNotCompletingTitle(v);
    }

    public EsfString getPackageButtonDownloadMyDocsAsPdf()
    {
    	return buttonmessageVersion().getPackageButtonDownloadMyDocsAsPdf();
    }
    public void setPackageButtonDownloadMyDocsAsPdf(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonDownloadMyDocsAsPdf(v);
    }

    public EsfString getPackageButtonDownloadMyDocsAsPdfTitle()
    {
    	return buttonmessageVersion().getPackageButtonDownloadMyDocsAsPdfTitle();
    }
    public void setPackageButtonDownloadMyDocsAsPdfTitle(EsfString v)
    {
    	buttonmessageVersion().setPackageButtonDownloadMyDocsAsPdfTitle(v);
    }

    public EsfString getDocumentButtonGoToPackage()
    {
    	return buttonmessageVersion().getDocumentButtonGoToPackage();
    }
    public void setDocumentButtonGoToPackage(EsfString v)
    {
    	buttonmessageVersion().setDocumentButtonGoToPackage(v);
    }

    public EsfString getDocumentButtonGoToPackageTitle()
    {
    	return buttonmessageVersion().getDocumentButtonGoToPackageTitle();
    }
    public void setDocumentButtonGoToPackageTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentButtonGoToPackageTitle(v);
    }

    public EsfString getDocumentButtonGoToPackageMessage()
    {
    	return buttonmessageVersion().getDocumentButtonGoToPackageMessage();
    }
    public void setDocumentButtonGoToPackageMessage(EsfString v)
    {
    	buttonmessageVersion().setDocumentButtonGoToPackageMessage(v);
    }

    public EsfString getDocumentEditButtonContinueNextPage()
    {
    	return buttonmessageVersion().getDocumentEditButtonContinueNextPage();
    }
    public void setDocumentEditButtonContinueNextPage(EsfString v)
    {
    	buttonmessageVersion().setDocumentEditButtonContinueNextPage(v);
    }

    public EsfString getDocumentEditButtonContinueNextPageTitle()
    {
    	return buttonmessageVersion().getDocumentEditButtonContinueNextPageTitle();
    }
    public void setDocumentEditButtonContinueNextPageTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentEditButtonContinueNextPageTitle(v);
    }

    public EsfString getDocumentEditButtonContinueToReview()
    {
    	return buttonmessageVersion().getDocumentEditButtonContinueToReview();
    }
    public void setDocumentEditButtonContinueToReview(EsfString v)
    {
    	buttonmessageVersion().setDocumentEditButtonContinueToReview(v);
    }

    public EsfString getDocumentEditButtonContinueToReviewTitle()
    {
    	return buttonmessageVersion().getDocumentEditButtonContinueToReviewTitle();
    }
    public void setDocumentEditButtonContinueToReviewTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentEditButtonContinueToReviewTitle(v);
    }

    public EsfString getDocumentEditButtonContinueToReviewMessage()
    {
    	return buttonmessageVersion().getDocumentEditButtonContinueToReviewMessage();
    }
    public void setDocumentEditButtonContinueToReviewMessage(EsfString v)
    {
    	buttonmessageVersion().setDocumentEditButtonContinueToReviewMessage(v);
    }

    public EsfString getDocumentEditButtonPreviousPage()
    {
    	return buttonmessageVersion().getDocumentEditButtonPreviousPage();
    }
    public void setDocumentEditButtonPreviousPage(EsfString v)
    {
    	buttonmessageVersion().setDocumentEditButtonPreviousPage(v);
    }

    public EsfString getDocumentEditButtonPreviousPageTitle()
    {
    	return buttonmessageVersion().getDocumentEditButtonPreviousPageTitle();
    }
    public void setDocumentEditButtonPreviousPageTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentEditButtonPreviousPageTitle(v);
    }

    public EsfString getDocumentEditButtonSave()
    {
    	return buttonmessageVersion().getDocumentEditButtonSave();
    }
    public void setDocumentEditButtonSave(EsfString v)
    {
    	buttonmessageVersion().setDocumentEditButtonSave(v);
    }

    public EsfString getDocumentEditButtonSaveTitle()
    {
    	return buttonmessageVersion().getDocumentEditButtonSaveTitle();
    }
    public void setDocumentEditButtonSaveTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentEditButtonSaveTitle(v);
    }

    public EsfString getDocumentReviewViewOnlyButtonContinueNextPage()
    {
    	return buttonmessageVersion().getDocumentReviewViewOnlyButtonContinueNextPage();
    }
    public void setDocumentReviewViewOnlyButtonContinueNextPage(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewViewOnlyButtonContinueNextPage(v);
    }

    public EsfString getDocumentReviewViewOnlyButtonContinueNextPageTitle()
    {
    	return buttonmessageVersion().getDocumentReviewViewOnlyButtonContinueNextPageTitle();
    }
    public void setDocumentReviewViewOnlyButtonContinueNextPageTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewViewOnlyButtonContinueNextPageTitle(v);
    }

    public EsfString getDocumentReviewViewOnlyButtonContinueNextPageMessage()
    {
    	return buttonmessageVersion().getDocumentReviewViewOnlyButtonContinueNextPageMessage();
    }
    public void setDocumentReviewViewOnlyButtonContinueNextPageMessage(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewViewOnlyButtonContinueNextPageMessage(v);
    }

    public EsfString getDocumentReviewViewOnlyButtonComplete()
    {
    	return buttonmessageVersion().getDocumentReviewViewOnlyButtonComplete();
    }
    public void setDocumentReviewViewOnlyButtonComplete(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewViewOnlyButtonComplete(v);
    }

    public EsfString getDocumentReviewViewOnlyButtonCompleteTitle()
    {
    	return buttonmessageVersion().getDocumentReviewViewOnlyButtonCompleteTitle();
    }
    public void setDocumentReviewViewOnlyButtonCompleteTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewViewOnlyButtonCompleteTitle(v);
    }

    public EsfString getDocumentReviewViewOnlyButtonCompleteMessage()
    {
    	return buttonmessageVersion().getDocumentReviewViewOnlyButtonCompleteMessage();
    }
    public void setDocumentReviewViewOnlyButtonCompleteMessage(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewViewOnlyButtonCompleteMessage(v);
    }

    public EsfString getDocumentReviewViewOnlyButtonPreviousPage()
    {
    	return buttonmessageVersion().getDocumentReviewViewOnlyButtonPreviousPage();
    }
    public void setDocumentReviewViewOnlyButtonPreviousPage(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewViewOnlyButtonPreviousPage(v);
    }

    public EsfString getDocumentReviewViewOnlyButtonPreviousPageTitle()
    {
    	return buttonmessageVersion().getDocumentReviewViewOnlyButtonPreviousPageTitle();
    }
    public void setDocumentReviewViewOnlyButtonPreviousPageTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewViewOnlyButtonPreviousPageTitle(v);
    }

    public EsfString getDocumentReviewButtonCompleteSigner()
    {
    	return buttonmessageVersion().getDocumentReviewButtonCompleteSigner();
    }
    public void setDocumentReviewButtonCompleteSigner(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewButtonCompleteSigner(v);
    }

    public EsfString getDocumentReviewButtonCompleteSignerTitle()
    {
    	return buttonmessageVersion().getDocumentReviewButtonCompleteSignerTitle();
    }
    public void setDocumentReviewButtonCompleteSignerTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewButtonCompleteSignerTitle(v);
    }

    public EsfString getDocumentReviewButtonCompleteSignerMessage()
    {
    	return buttonmessageVersion().getDocumentReviewButtonCompleteSignerMessage();
    }
    public void setDocumentReviewButtonCompleteSignerMessage(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewButtonCompleteSignerMessage(v);
    }

    public EsfString getDocumentReviewButtonCompleteNotSigner()
    {
    	return buttonmessageVersion().getDocumentReviewButtonCompleteNotSigner();
    }
    public void setDocumentReviewButtonCompleteNotSigner(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewButtonCompleteNotSigner(v);
    }

    public EsfString getDocumentReviewButtonCompleteNotSignerTitle()
    {
    	return buttonmessageVersion().getDocumentReviewButtonCompleteNotSignerTitle();
    }
    public void setDocumentReviewButtonCompleteNotSignerTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewButtonCompleteNotSignerTitle(v);
    }

    public EsfString getDocumentReviewButtonCompleteNotSignerMessage()
    {
    	return buttonmessageVersion().getDocumentReviewButtonCompleteNotSignerMessage();
    }
    public void setDocumentReviewButtonCompleteNotSignerMessage(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewButtonCompleteNotSignerMessage(v);
    }

    public EsfString getDocumentReviewButtonReturnToEdit()
    {
    	return buttonmessageVersion().getDocumentReviewButtonReturnToEdit();
    }
    public void setDocumentReviewButtonReturnToEdit(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewButtonReturnToEdit(v);
    }

    public EsfString getDocumentReviewButtonReturnToEditTitle()
    {
    	return buttonmessageVersion().getDocumentReviewButtonReturnToEditTitle();
    }
    public void setDocumentReviewButtonReturnToEditTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewButtonReturnToEditTitle(v);
    }

    public EsfString getDocumentReviewButtonReturnToEditMessage()
    {
    	return buttonmessageVersion().getDocumentReviewButtonReturnToEditMessage();
    }
    public void setDocumentReviewButtonReturnToEditMessage(EsfString v)
    {
    	buttonmessageVersion().setDocumentReviewButtonReturnToEditMessage(v);
    }

    public EsfString getDocumentViewOnlyButtonNextPage()
    {
    	return buttonmessageVersion().getDocumentViewOnlyButtonNextPage();
    }
    public void setDocumentViewOnlyButtonNextPage(EsfString v)
    {
    	buttonmessageVersion().setDocumentViewOnlyButtonNextPage(v);
    }

    public EsfString getDocumentViewOnlyButtonNextPageTitle()
    {
    	return buttonmessageVersion().getDocumentViewOnlyButtonNextPageTitle();
    }
    public void setDocumentViewOnlyButtonNextPageTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentViewOnlyButtonNextPageTitle(v);
    }

    public EsfString getDocumentViewOnlyButtonPreviousPage()
    {
    	return buttonmessageVersion().getDocumentViewOnlyButtonPreviousPage();
    }
    public void setDocumentViewOnlyButtonPreviousPage(EsfString v)
    {
    	buttonmessageVersion().setDocumentViewOnlyButtonPreviousPage(v);
    }

    public EsfString getDocumentViewOnlyButtonPreviousPageTitle()
    {
    	return buttonmessageVersion().getDocumentViewOnlyButtonPreviousPageTitle();
    }
    public void setDocumentViewOnlyButtonPreviousPageTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentViewOnlyButtonPreviousPageTitle(v);
    }

    public EsfString getDocumentViewOnlyButtonNextDocument()
    {
    	return buttonmessageVersion().getDocumentViewOnlyButtonNextDocument();
    }
    public void setDocumentViewOnlyButtonNextDocument(EsfString v)
    {
    	buttonmessageVersion().setDocumentViewOnlyButtonNextDocument(v);
    }

    public EsfString getDocumentViewOnlyButtonNextDocumentTitle()
    {
    	return buttonmessageVersion().getDocumentViewOnlyButtonNextDocumentTitle();
    }
    public void setDocumentViewOnlyButtonNextDocumentTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentViewOnlyButtonNextDocumentTitle(v);
    }

    public EsfString getDocumentViewOnlyButtonPreviousDocument()
    {
    	return buttonmessageVersion().getDocumentViewOnlyButtonPreviousDocument();
    }
    public void setDocumentViewOnlyButtonPreviousDocument(EsfString v)
    {
    	buttonmessageVersion().setDocumentViewOnlyButtonPreviousDocument(v);
    }

    public EsfString getDocumentViewOnlyButtonPreviousDocumentTitle()
    {
    	return buttonmessageVersion().getDocumentViewOnlyButtonPreviousDocumentTitle();
    }
    public void setDocumentViewOnlyButtonPreviousDocumentTitle(EsfString v)
    {
    	buttonmessageVersion().setDocumentViewOnlyButtonPreviousDocumentTitle(v);
    }
    
    /* The general messages not associated with a button */

    public EsfString getDocumentAlreadyCompletedMessage()
    {
    	return buttonmessageVersion().getDocumentAlreadyCompletedMessage();
    }
    public void setDocumentAlreadyCompletedMessage(EsfString v)
    {
    	buttonmessageVersion().setDocumentAlreadyCompletedMessage(v);
    }

    public EsfString getDocumentAlreadyCompletedCannotEditMessage()
    {
    	return buttonmessageVersion().getDocumentAlreadyCompletedCannotEditMessage();
    }
    public void setDocumentAlreadyCompletedCannotEditMessage(EsfString v)
    {
    	buttonmessageVersion().setDocumentAlreadyCompletedCannotEditMessage(v);
    }

    public EsfString getViewOnlyDocumentFYIMessage()
    {
    	return buttonmessageVersion().getViewOnlyDocumentFYIMessage();
    }
    public void setViewOnlyDocumentFYIMessage(EsfString v)
    {
    	buttonmessageVersion().setViewOnlyDocumentFYIMessage(v);
    }

    public EsfString getReviewDocumentNotSignerMessage()
    {
    	return buttonmessageVersion().getReviewDocumentNotSignerMessage();
    }
    public void setReviewDocumentNotSignerMessage(EsfString v)
    {
    	buttonmessageVersion().setReviewDocumentNotSignerMessage(v);
    }

    public EsfString getReviewDocumentSignerMessage()
    {
    	return buttonmessageVersion().getReviewDocumentSignerMessage();
    }
    public void setReviewDocumentSignerMessage(EsfString v)
    {
    	buttonmessageVersion().setReviewDocumentSignerMessage(v);
    }

    public EsfString getCompleteDocumentEditsMessage()
    {
    	return buttonmessageVersion().getCompleteDocumentEditsMessage();
    }
    public void setCompleteDocumentEditsMessage(EsfString v)
    {
    	buttonmessageVersion().setCompleteDocumentEditsMessage(v);
    }

    public EsfString getCompletedAllDocumentsMessage()
    {
    	return buttonmessageVersion().getCompletedAllDocumentsMessage();
    }
    public void setCompletedAllDocumentsMessage(EsfString v)
    {
    	buttonmessageVersion().setCompletedAllDocumentsMessage(v);
    }

    public EsfString getDeletedTransactionMessage()
    {
    	return buttonmessageVersion().getDeletedTransactionMessage();
    }
    public void setDeletedTransactionMessage(EsfString v)
    {
    	buttonmessageVersion().setDeletedTransactionMessage(v);
    }
    
}