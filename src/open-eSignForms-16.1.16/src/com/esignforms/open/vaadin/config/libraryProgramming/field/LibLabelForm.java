// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.field;

import java.util.Arrays;

import com.esignforms.open.prog.LabelTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * The LibLabelForm is generally used like a field in another form to set the various values of the LabelTemplate.
 * 
 * @author Yozons Inc.
 */
public class LibLabelForm extends Form {
	private static final long serialVersionUID = 680751228091328531L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibLabelForm.class);
	
	final LibLabelForm thisForm;
	GridLayout layout;
    int tabIndex;
    int positionTabIndex;
	
	public LibLabelForm(final Form parentForm, int startTabIndex) {
		this.thisForm = this;
		setStyleName("LibLabelForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.positionTabIndex = startTabIndex;
    	this.tabIndex = startTabIndex + 1;

    	setBuffered(parentForm.isBuffered());
    	setImmediate(parentForm.isImmediate());
    	
    	layout = new GridLayout(5,1);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.05f);
    	layout.setColumnExpandRatio(1, 0.05f);
    	layout.setColumnExpandRatio(2, 0.8f); // the label to show needs the most space
    	layout.setColumnExpandRatio(3, 0.05f);
    	layout.setColumnExpandRatio(4, 0.05f);
    	layout.setMargin(true); // we're in another form
    	layout.setSpacing(true);
		
    	Panel labelPanel = new Panel();
    	labelPanel.setStyleName("bubble");
    	labelPanel.setContent(layout);
    	VerticalLayout formLayout = new VerticalLayout();
    	formLayout.setMargin(new MarginInfo(true,false,true,false));
    	formLayout.setSpacing(false);
    	formLayout.addComponent(labelPanel);
    	setLayout(formLayout);
    	
    	setReadOnly(parentForm.isReadOnly());

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 2225393732365334230L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				_logger.debug("createField: " + propertyId);

                if (propertyId.equals("position")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibLabelForm.position.label"));
                	select.setTabIndex(positionTabIndex);
                	select.setNullSelectionAllowed(false); 
                	select.setImmediate(true);
                	select.setRequired(true);
                    select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.addItem(LabelTemplate.LABEL_POSITION_NO_AUTO_SHOW);
                	select.addItem(LabelTemplate.LABEL_POSITION_TOP_LEFT);
                	select.addItem(LabelTemplate.LABEL_POSITION_TOP_CENTER);
                	select.addItem(LabelTemplate.LABEL_POSITION_TOP_RIGHT);
                	select.addItem(LabelTemplate.LABEL_POSITION_BOTTOM_LEFT);
                	select.addItem(LabelTemplate.LABEL_POSITION_BOTTOM_CENTER);
                	select.addItem(LabelTemplate.LABEL_POSITION_BOTTOM_RIGHT);
                	select.addItem(LabelTemplate.LABEL_POSITION_LEFT);
                	select.addItem(LabelTemplate.LABEL_POSITION_RIGHT);
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	select.setItemCaption(LabelTemplate.LABEL_POSITION_NO_AUTO_SHOW, vaadinUi.getPrettyCode().labelTemplatePosition(LabelTemplate.LABEL_POSITION_NO_AUTO_SHOW));
                	select.setItemCaption(LabelTemplate.LABEL_POSITION_TOP_LEFT, vaadinUi.getPrettyCode().labelTemplatePosition(LabelTemplate.LABEL_POSITION_TOP_LEFT));
                	select.setItemCaption(LabelTemplate.LABEL_POSITION_TOP_CENTER, vaadinUi.getPrettyCode().labelTemplatePosition(LabelTemplate.LABEL_POSITION_TOP_CENTER));
                	select.setItemCaption(LabelTemplate.LABEL_POSITION_TOP_RIGHT, vaadinUi.getPrettyCode().labelTemplatePosition(LabelTemplate.LABEL_POSITION_TOP_RIGHT));
                	select.setItemCaption(LabelTemplate.LABEL_POSITION_BOTTOM_LEFT, vaadinUi.getPrettyCode().labelTemplatePosition(LabelTemplate.LABEL_POSITION_BOTTOM_LEFT));
                	select.setItemCaption(LabelTemplate.LABEL_POSITION_BOTTOM_CENTER, vaadinUi.getPrettyCode().labelTemplatePosition(LabelTemplate.LABEL_POSITION_BOTTOM_CENTER));
                	select.setItemCaption(LabelTemplate.LABEL_POSITION_BOTTOM_RIGHT, vaadinUi.getPrettyCode().labelTemplatePosition(LabelTemplate.LABEL_POSITION_BOTTOM_RIGHT));
                	select.setItemCaption(LabelTemplate.LABEL_POSITION_LEFT, vaadinUi.getPrettyCode().labelTemplatePosition(LabelTemplate.LABEL_POSITION_LEFT));
                	select.setItemCaption(LabelTemplate.LABEL_POSITION_RIGHT, vaadinUi.getPrettyCode().labelTemplatePosition(LabelTemplate.LABEL_POSITION_RIGHT));
                	select.addValueChangeListener( new Property.ValueChangeListener() {
						private static final long serialVersionUID = 6281140297737300263L;

						@Override
						public void valueChange(Property.ValueChangeEvent event) {
							boolean showFields = ! LabelTemplate.LABEL_POSITION_NO_AUTO_SHOW.equals((String)event.getProperty().getValue());
							Field f = thisForm.getField("label");
							if ( f != null ) f.setVisible(showFields);
							f = thisForm.getField("separator");
							if ( f != null ) f.setVisible(showFields);
							f = thisForm.getField("size");
							if ( f != null ) f.setVisible(showFields);
							f = thisForm.getField("style");
							if ( f != null ) f.setVisible(showFields);
							f = thisForm.getField("suppressNonInput");
							if ( f != null ) f.setVisible(showFields);
						}
					});
                	return select;
                }
                
                if (propertyId.equals("separator")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibLabelForm.separator.label"));
                	select.setTabIndex(tabIndex++);
                	select.setDescription(vaadinUi.getMsg("LibLabelForm.separator.tooltip"));
	                select.addValidator(new SelectValidator(select));
                	select.setNullSelectionAllowed(false); 
                	select.setImmediate(false);
                	select.setRequired(true);
                    select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	select.addItem(LabelTemplate.LABEL_SEPARATOR_NONE);
                	select.addItem(LabelTemplate.LABEL_SEPARATOR_SPACE);
                	select.addItem(LabelTemplate.LABEL_SEPARATOR_COLON_SPACE);
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	select.setItemCaption(LabelTemplate.LABEL_SEPARATOR_NONE, vaadinUi.getPrettyCode().labelTemplateSeparator(LabelTemplate.LABEL_SEPARATOR_NONE));
                	select.setItemCaption(LabelTemplate.LABEL_SEPARATOR_SPACE, vaadinUi.getPrettyCode().labelTemplateSeparator(LabelTemplate.LABEL_SEPARATOR_SPACE));
                	select.setItemCaption(LabelTemplate.LABEL_SEPARATOR_COLON_SPACE, vaadinUi.getPrettyCode().labelTemplateSeparator(LabelTemplate.LABEL_SEPARATOR_COLON_SPACE));
                	return select;
                }
                
                if (propertyId.equals("size")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibLabelForm.size.label"));
                	select.setTabIndex(tabIndex++);
	                select.addValidator(new SelectValidator(select));
                	select.setDescription(vaadinUi.getMsg("LibLabelForm.size.tooltip"));
                	select.setNullSelectionAllowed(false); 
                	select.setImmediate(false);
                	select.setRequired(true);
                    select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	select.addItem(LabelTemplate.LABEL_SIZE_NORMAL);
                	select.addItem(LabelTemplate.LABEL_SIZE_SMALL);
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	select.setItemCaption(LabelTemplate.LABEL_SIZE_NORMAL, vaadinUi.getPrettyCode().labelTemplateSize(LabelTemplate.LABEL_SIZE_NORMAL));
                	select.setItemCaption(LabelTemplate.LABEL_SIZE_SMALL, vaadinUi.getPrettyCode().labelTemplateSize(LabelTemplate.LABEL_SIZE_SMALL));
                	return select;
                }
                
                if (propertyId.equals("suppressNonInput")) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("LibLabelForm.suppressNonInput.caption"));
                	cb.setTabIndex(tabIndex++);
	            	cb.setImmediate(false);
	                cb.setDescription(vaadinUi.getMsg("LibLabelForm.suppressNonInput.tooltip"));
	                return cb;
                }
                
				Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
                field.setTabIndex(tabIndex++);
                
                if (propertyId.equals("label")) {
	            	TextField tf = (TextField)field;
	            	tf.setRequired(false);
	            	tf.setNullRepresentation("");
	            	tf.setCaption(vaadinUi.getMsg("LibLabelForm.label.label"));
	            	tf.setDescription(vaadinUi.getMsg("LibLabelForm.label.tooltip"));
	    	    }

                return field;
			}
    	 });
    	
    	_logger.debug("Form created");
    }
    
	LibLabelBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public LibLabelBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibLabelBean> bi = (BeanItem<LibLabelBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
    	if (newDataSource != null) {
     		// we put position last so that if created and set to NONE it makes the other fields invisible
    		super.setItemDataSource(newDataSource, Arrays.asList("label","separator","size","suppressNonInput","position"));
    	} else {
    		super.setItemDataSource(null);
    		setReadOnly(true);
    	}
    }
	
    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("position")) {
        	layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("suppressNonInput")) {
        	layout.addComponent(field, 1, 0);
        	layout.setComponentAlignment(field, Alignment.BOTTOM_CENTER);
        } else if (propertyId.equals("label")) {
        	layout.addComponent(field, 2, 0);
        } else if (propertyId.equals("separator")) {
        	layout.addComponent(field, 3, 0);
        } else if (propertyId.equals("size")) {
        	layout.addComponent(field, 4, 0);
        }
    }

}