// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.library;

import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Property;
import com.vaadin.ui.Table;

public class LibraryList extends Table {
	private static final long serialVersionUID = 2231616483502038683L;

	public LibraryList(LibraryView view, LibraryViewContainer container) {
		super();
		setSizeFull();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
		setContainerDataSource(container);
		setVisibleColumns(vaadinUi.getStringArray("LibraryList.showColumnProperties"));
		setColumnHeaders(vaadinUi.getStringArray("LibraryList.showColumnHeaders"));
		setColumnCollapsingAllowed(true);
		setColumnReorderingAllowed(true);
		setCellStyleGenerator(new Table.CellStyleGenerator( ) {
			private static final long serialVersionUID = 9176918357301535898L;

			@Override
			public String getStyle(Table table, Object itemId, Object propertyId) {
				LibraryBean bean = (LibraryBean)itemId;
				return bean.isEnabled() ? null : "disabledText";
			}
		});
        // Make table selectable, react immediately to user events, and pass events to the view
        setSelectable(true);
        setImmediate(true);
        addValueChangeListener((Property.ValueChangeListener)view);
        setNullSelectionAllowed(true);
	}
	
	@Override
	protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
		if ( "lastUpdatedTimestamp".equals(colId) ) {
			return ((LibraryBean)rowId).formatLastUpdatedTimestamp();
		}
		
		return super.formatPropertyValue(rowId,colId,property);
	}
}