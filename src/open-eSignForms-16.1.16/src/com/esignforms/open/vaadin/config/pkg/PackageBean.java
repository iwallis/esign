// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.PackageInfo;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class PackageBean implements Serializable, Comparable<PackageBean> {
	private static final long serialVersionUID = -2440477936584686159L;

	private PackageInfo packageInfo;

	private Package pkg;
	
	// This contains all of the groups that the user can list, so these are the only ones that
	// can be used to add/remove groups (any groups that the Group object has that are not included
	// cannot be touched)
	private EsfUUID[] allGroupListIds;
	private EsfPathName[] allGroupListNames;

	private Set<EsfUUID> permListIds;
	private Set<EsfUUID> permViewDetailsIds;
	private Set<EsfUUID> permCreateLikeIds;
	private Set<EsfUUID> permUpdateIds;
	private Set<EsfUUID> permDeleteIds;

	public PackageBean(PackageInfo packageInfo) {
		this.packageInfo = packageInfo;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof PackageBean )
			return packageInfo.equals(((PackageBean)o).packageInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return packageInfo.hashCode();
    }
	public int compareTo(PackageBean d) {
		return packageInfo.compareTo(d.packageInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	protected User user() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getUser();
	}

	public PackageInfo packageInfo() {
		return packageInfo;
	}

	public Package pkg() {
		if ( pkg == null )
			pkg = Package.Manager.getById(packageInfo.getId());
		return pkg;
	}

	public PackageBean createLike() {
		EsfPathName newPathName;
		String fromPathName = pkg.getPathName().toPlainString();
		if ( EsfPathName.isReservedPathPrefix(fromPathName) ) { // don't let them duplicate with the reserved path prefix
			fromPathName = fromPathName.substring(EsfPathName.ESF_RESERVED_PATH_PREFIX.length());
			newPathName = new EsfPathName(fromPathName);
		} else {
			newPathName = pkg.getPathName();
		}
		PackageInfo newPackageInfo = PackageInfo.Manager.createLike(pkg, newPathName, user());	    
	    return new PackageBean(newPackageInfo);
	}
	
	
	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( pkg().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			packageInfo = PackageInfo.Manager.createFromSource(pkg);
			return true;
		} 
		
		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	protected HashSet<EsfUUID> createIdHashSet( Collection<Group> list ) {
		HashSet<EsfUUID> set = new HashSet<EsfUUID>(list.size());
		for( Group g : list ) {
			set.add(g.getId());
		}
		return set;
	}
	
	private synchronized void resetCaches() {
		allGroupListIds = null;
		allGroupListNames = null;
		permListIds = null;
		permViewDetailsIds = null;
		permCreateLikeIds = null;
		permUpdateIds = null;
		permDeleteIds = null;
	}

	protected synchronized void createAllGroupLists() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Collection<Group> list = Group.Manager.getForUserWithListPermission(user(),Group.INCLUDE.BOTH_ENABLED_AND_DISABLED);
		
		int size = list.size();
		
		// We will not include the "External Users" pseudo-group.
		allGroupListIds = new EsfUUID[size-1];
		allGroupListNames = new EsfPathName[size-1];
		int i = 0;
		for( Group g : list ) {
			if ( g.isExternalUsersGroup() )
				continue;
			allGroupListIds[i] = g.getId();
			allGroupListNames[i] = g.isDisabled() ? new EsfPathName(g.getPathName() + EsfPathName.PATH_SEPARATOR + vaadinUi.getMsg("PrettyCode.status.disabled")) : g.getPathName();
			++i;
		}
	}
	public EsfUUID[] allGroupListIds() {
		if ( allGroupListIds == null ) {
			createAllGroupLists();
		}
		return allGroupListIds;
	}
	public EsfPathName[] allGroupListNames() {
		if ( allGroupListNames == null ) {
			createAllGroupLists();
		}
		return allGroupListNames;
	}
	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return packageInfo.getId();
	}
	
	public EsfPathName getPathName() {
		return packageInfo.getPathName();
	}
	public void setPathName(EsfPathName v) {
		if ( ! pkg.isReserved() ) {
			pkg.setPathName(v);
		}
	}

	public String getDownloadFileNameSpec() {
		return packageInfo.getDownloadFileNameSpec();
	}
	public void setDownloadFileNameSpec(String v) {
		pkg().setDownloadFileNameSpec(v);
	}

	public String getDescription() {
		return packageInfo.getDescription();
	}
	public void setDescription(String v) {
		pkg().setDescription(v);
	}

	public boolean isEnabled() {
		return packageInfo.isEnabled();
	}
	public String getStatus() {
		return packageInfo.getStatus();
	}
	public void setStatus(String v) {
		pkg().setStatus(v);
	}	
	
	public Integer getProductionVersion() {
		return packageInfo.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		return packageInfo.getTestVersion();
	}
	
	public void bumpTestVersion() {
		pkg().bumpTestVersion();
	}
	public void dropTestVersion() {
		pkg().dropTestVersion();
	}
	
	public String getComments() {
		return pkg().getComments();
	}
	public void setComments(String v) {
		pkg().setComments(v);
	}
	
	protected void setPermIds(Set<EsfUUID> v, EsfName permOption) {
		// First, let's remove all groups this user can list
		for( Group g : Group.Manager.getForUserWithListPermission(user()) ) {
			pkg().removePermissionAllowedGroups(permOption, g);
		}
		// Then let's add back in the selected set
		for( EsfUUID gid : v ) {
			Group g = Group.Manager.getById( gid );
			if ( g != null ) {
				pkg().addPermissionAllowedGroups(permOption, g);
			}
		}
	}

	public Set<EsfUUID> getPermListIds() {
		if ( permListIds == null ) {
			permListIds = createIdHashSet( pkg().getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_LIST) );
		}
		return permListIds;
	}
		
	public void setPermListIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_LIST);
	}

	public Set<EsfUUID> getPermViewDetailsIds() {
		if ( permViewDetailsIds == null ) {
			permViewDetailsIds = createIdHashSet( pkg().getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_VIEWDETAILS) );
		}
		return permViewDetailsIds;
	}
	
	public void setPermViewDetailsIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public Set<EsfUUID> getPermCreateLikeIds() {
		if ( permCreateLikeIds == null ) {
			permCreateLikeIds = createIdHashSet( pkg().getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_CREATELIKE) );
		}
		return permCreateLikeIds;
	}
	
	public void setPermCreateLikeIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public Set<EsfUUID> getPermUpdateIds() {
		if ( permUpdateIds == null ) {
			permUpdateIds = createIdHashSet( pkg().getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_UPDATE) );
		}
		return permUpdateIds;
	}
	
	public void setPermUpdateIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_UPDATE);
	}

	public Set<EsfUUID> getPermDeleteIds() {
		if ( permDeleteIds == null ) {
			permDeleteIds = createIdHashSet( pkg().getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_DELETE) );
		}
		return permDeleteIds;
	}
	
	public void setPermDeleteIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_DELETE);
	}

}