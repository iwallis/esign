// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.integration.httpsend.HttpSendResponse;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.LabelTemplate;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionPartyAssignment;
import com.esignforms.open.runtime.TransactionPartyDocument;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;

/**
 * A class for converting codes to a user-friendly form.
 */
public class PrettyCode implements java.io.Serializable
{
	private static final long serialVersionUID = -2368030872956852672L;

	final EsfVaadinUI vaadinUi;
	
	public PrettyCode(EsfVaadinUI vaadinUi) {
		this.vaadinUi = vaadinUi;
	}
	
	public String actionName(String actionEsfName) {
		return vaadinUi.getMsg("PrettyCode.action."+actionEsfName);
	}
	public String actionName(EsfName actionEsfName) {
		return actionName( actionEsfName.toString() );
	}
	
	public String conditionName(String conditionEsfName) {
		return vaadinUi.getMsg("PrettyCode.condition."+conditionEsfName);
	}
	public String conditionName(EsfName conditionEsfName) {
		return conditionName( conditionEsfName.toString() );
	}

	public String comparisonConditionOperator(String operator) {
		return vaadinUi.getMsg("PrettyCode.ComparisonCondition.operator."+operator);
	}

	public String builtinConditionName(String conditionEsfName) {
		return vaadinUi.getMsg("PrettyCode.condition.builtin."+conditionEsfName);
	}
	public String builtinConditionName(EsfName conditionEsfName) {
		return builtinConditionName( conditionEsfName.toString() );
	}
	
	public String changeFileFieldActionChangeType(String statusChangeType) {
		return vaadinUi.getMsg("PrettyCode.ChangeFileFieldAction.changeType."+statusChangeType);
	}
	
	public String changeStatusActionType(String statusChangeType) {
		return vaadinUi.getMsg("PrettyCode.ChangeStatusAction.type."+statusChangeType);
	}
	
	public String completedDocumentType(String docType) {
		return vaadinUi.getMsg("PrettyCode.CompletedDocument.type."+docType);
	}

	public String documentPageEditReviewType(String editReviewType) {
		return vaadinUi.getMsg("PrettyCode.DocumentVersionPage.editReviewMode."+editReviewType);
	}

	public String errorsType(String errorType) {
		return vaadinUi.getMsg("PrettyCode.Errors.type."+errorType);
	}

	public String eventName(String eventEsfName) {
		return vaadinUi.getMsg("PrettyCode.event."+eventEsfName);
	}
	public String eventName(EsfName eventEsfName) {
		return eventName( eventEsfName.toString() );
	}
	
	public String fieldTemplateType(String typeCode) {
		if ( FieldTemplate.isTypeGeneral(typeCode) ) { // out of order but this is the most common field type so we put its test first
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.general");
		} 
		if ( FieldTemplate.isTypeCheckbox(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.checkbox");
		} 
		if ( FieldTemplate.isTypeCreditCard(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.creditCard");
		} 
		if ( FieldTemplate.isTypeDate(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.date");
		} 
		if ( FieldTemplate.isTypeDateTime(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.datetime");
		} 
		if ( FieldTemplate.isTypeDecimal(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.decimal");
		} 
		if ( FieldTemplate.isTypeEmailAddress(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.emailAddress");
		} 
		if ( FieldTemplate.isTypeFile(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.file");
		} 
		if ( FieldTemplate.isTypeFileConfirmClick(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.fileconfirmclick");
		} 
		// General would go here, but we put at the top since it's the most common field type
		if ( FieldTemplate.isTypeInteger(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.integer");
		} 
		if ( FieldTemplate.isTypeMoney(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.money");
		} 
		if ( FieldTemplate.isTypePhone(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.phone");
		} 
		if ( FieldTemplate.isTypeRadioButton(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.radioButton");
		} 
		if ( FieldTemplate.isTypeRadioButtonGroup(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.radioButtonGroup");
		} 
		if ( FieldTemplate.isTypeRichTextarea(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.richTextarea");
		} 
		if ( FieldTemplate.isTypeSelection(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.select");
		} 
		if ( FieldTemplate.isTypeSignature(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.signature");
		} 
		if ( FieldTemplate.isTypeSignDate(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.signDate");
		} 
		if ( FieldTemplate.isTypeSsnEin(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.ssnein");
		} 
		if ( FieldTemplate.isTypeTextarea(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.textarea");
		} 
		if ( FieldTemplate.isTypeZipCode(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.type.zipCode");
		} 
		return "??" + typeCode + "??";
	}
	
	public String fieldTemplateWidth(String widthCode) {
		if ( FieldTemplate.WIDTH_UNIT_PERCENT.equals(widthCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.width.percent");
		} 
		if ( FieldTemplate.WIDTH_UNIT_PIXELS.equals(widthCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.width.pixels");
		} 
		if ( FieldTemplate.WIDTH_UNIT_EM.equals(widthCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.width.em");
		} 
		if ( FieldTemplate.WIDTH_UNIT_EX.equals(widthCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.width.ex");
		} 
		if ( FieldTemplate.WIDTH_UNIT_AUTO.equals(widthCode) ) {
			return vaadinUi.getMsg("PrettyCode.fieldTemplate.width.auto");
		} 
		return "??" + widthCode + "??";
	}
	
	public String httpSendActionRequestMethod(String requestMethod) {
		return vaadinUi.getMsg("PrettyCode.HttpSendAction.requestMethod."+requestMethod);
	}
	
	public String httpSendResponseStatus(String status) {
		if ( HttpSendResponse.STATUS_SUCCESS.equals(status) )
			return vaadinUi.getMsg("PrettyCode.HttpSendResponse.status."+HttpSendResponse.STATUS_SUCCESS);
		return vaadinUi.getMsg("PrettyCode.HttpSendResponse.status."+HttpSendResponse.STATUS_FAILURE);
	}
	
	public String httpSendResponseMatchType(String type) {
		return vaadinUi.getMsg("PrettyCode.HttpSendAction.responseMatchType."+type);
	}
	
	public String imageOverlayDisplayMode(String displayMode) {
		return vaadinUi.getMsg("PrettyCode.ImageOverlayDisplayMode."+displayMode);
	}
	
	public String labelTemplatePosition(String positionCode) {
		if ( LabelTemplate.LABEL_POSITION_NO_AUTO_SHOW.equals(positionCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.position.noautoshow");
		} 
		if ( LabelTemplate.LABEL_POSITION_TOP_LEFT.equals(positionCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.position.topLeft");
		} 
		if ( LabelTemplate.LABEL_POSITION_TOP_CENTER.equals(positionCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.position.topCenter");
		} 
		if ( LabelTemplate.LABEL_POSITION_TOP_RIGHT.equals(positionCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.position.topRight");
		} 
		if ( LabelTemplate.LABEL_POSITION_BOTTOM_LEFT.equals(positionCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.position.bottomLeft");
		} 
		if ( LabelTemplate.LABEL_POSITION_BOTTOM_CENTER.equals(positionCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.position.bottomCenter");
		} 
		if ( LabelTemplate.LABEL_POSITION_BOTTOM_RIGHT.equals(positionCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.position.bottomRight");
		} 
		if ( LabelTemplate.LABEL_POSITION_LEFT.equals(positionCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.position.left");
		} 
		if ( LabelTemplate.LABEL_POSITION_RIGHT.equals(positionCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.position.right");
		} 
		return "??" + positionCode + "??";
	}
	
	public String labelTemplateSeparator(String separatorCode) {
		if ( LabelTemplate.LABEL_SEPARATOR_NONE.equals(separatorCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.separator.none");
		} 
		if ( LabelTemplate.LABEL_SEPARATOR_SPACE.equals(separatorCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.separator.space");
		} 
		if ( LabelTemplate.LABEL_SEPARATOR_COLON_SPACE.equals(separatorCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.separator.colonSpace");
		} 
		return "??" + separatorCode + "??";
	}
	
	public String labelTemplateSize(String sizeCode) {
		if ( LabelTemplate.LABEL_SIZE_NORMAL.equals(sizeCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.size.normal");
		} 
		if ( LabelTemplate.LABEL_SIZE_SMALL.equals(sizeCode) ) {
			return vaadinUi.getMsg("PrettyCode.labelTemplate.size.small");
		} 
		return "??" + sizeCode + "??";
	}
	
	public String modeSelector(EsfVaadinUI.Mode mode) {
		return vaadinUi.getMsg("PrettyCode.modeSelector."+mode.toString());
	}
	
	public String packageVersionPartyTemplateLandingPage(String landingPageCode) {
		if ( PackageVersionPartyTemplate.LANDING_PAGE_PACKAGE.equals(landingPageCode) ) {
			return vaadinUi.getMsg("PrettyCode.packageVersionPartyTemplate.landingPage.package");
		} 
		if ( PackageVersionPartyTemplate.LANDING_PAGE_FIRST_DOC.equals(landingPageCode) ) {
			return vaadinUi.getMsg("PrettyCode.packageVersionPartyTemplate.landingPage.firstDoc");
		} 
		if ( PackageVersionPartyTemplate.LANDING_PAGE_FIRST_TODO_OR_FIRST_DOC.equals(landingPageCode) ) {
			return vaadinUi.getMsg("PrettyCode.packageVersionPartyTemplate.landingPage.firstToDoOrFirstDoc");
		} 
		if ( PackageVersionPartyTemplate.LANDING_PAGE_FIRST_TODO_OR_PACKAGE.equals(landingPageCode) ) {
			return vaadinUi.getMsg("PrettyCode.packageVersionPartyTemplate.landingPage.firstToDoOrPackage");
		} 
		return "??" + landingPageCode + "??";
	}
	
	public String pageOrientation(String pageOrientationCode) {
		if ( DocumentVersion.PAGE_ORIENTATION_PORTRAIT.equals(pageOrientationCode) ) {
			return vaadinUi.getMsg("PrettyCode.pageOrientation.portrait");
		}
		if ( DocumentVersion.PAGE_ORIENTATION_LANDSCAPE.equals(pageOrientationCode) ) {
			return vaadinUi.getMsg("PrettyCode.pageOrientation.landscape");
		}
		return "??" + pageOrientationCode + "??";
	}
	
	public String esignProcessRecordLocation(String esignProcessRecordLocation) {
		return vaadinUi.getMsg("PrettyCode.esignProcessRecordLocation." + esignProcessRecordLocation);
	}
	
	
	public String fieldSpecType(String type) {
		return vaadinUi.getMsg("PrettyCode.FieldSpec.type." + type);
	}

	
	public String reportFieldTemplateType(String typeCode) {
		if ( ReportFieldTemplate.isFieldTypeBuiltIn(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.reportFieldTemplate.type.builtin");
		} 
		if ( ReportFieldTemplate.isFieldTypeString(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.reportFieldTemplate.type.string");
		} 
		if ( ReportFieldTemplate.isFieldTypeInteger(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.reportFieldTemplate.type.integer");
		} 
		if ( ReportFieldTemplate.isFieldTypeDecimal(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.reportFieldTemplate.type.decimal");
		} 
		if ( ReportFieldTemplate.isFieldTypeDate(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.reportFieldTemplate.type.date");
		} 
		if ( ReportFieldTemplate.isFieldTypeFile(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.reportFieldTemplate.type.file");
		} 
		if ( ReportFieldTemplate.isFieldTypeNumericOnly(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.reportFieldTemplate.type.numericonly");
		} 
		if ( ReportFieldTemplate.isFieldTypeAlphaNumericOnly(typeCode) ) {
			return vaadinUi.getMsg("PrettyCode.reportFieldTemplate.type.alphanumericonly");
		} 
		return "??" + typeCode + "??";
	}
	
	public String transactionPartyAssignmentStatus(String statusCode) {
		if ( TransactionPartyAssignment.STATUS_ACTIVATED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.party.assignment.status.activated");
		} 
		if ( TransactionPartyAssignment.STATUS_RETRIEVED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.party.assignment.status.retrieved");
		} 
		if ( TransactionPartyAssignment.STATUS_COMPLETED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.party.assignment.status.completed");
		} 
		if ( TransactionPartyAssignment.STATUS_REJECTED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.party.assignment.status.rejected");
		} 
		if ( TransactionPartyAssignment.STATUS_TRANSFERRED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.party.assignment.status.transferred");
		} 
		if ( TransactionPartyAssignment.STATUS_UNDEFINED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.party.assignment.status.undefined");
		} 
		if ( TransactionPartyAssignment.STATUS_SKIPPED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.party.assignment.status.skipped");
		} 
		return "??" + statusCode + "??";
	}

	
	public String transactionPartyStatus(String statusCode) {
		if ( TransactionParty.STATUS_ACTIVE.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.party.status.active");
		} 
		if ( TransactionParty.STATUS_COMPLETED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.party.status.completed");
		} 
		if ( TransactionParty.STATUS_SKIPPED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.party.status.skipped");
		} 
		if ( TransactionParty.STATUS_CREATED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.party.status.created");
		} 
		if ( TransactionParty.STATUS_REPORTS.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.party.status.reports");
		} 
		if ( TransactionParty.STATUS_UNDEFINED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.party.status.undefined");
		} 
		return "??" + statusCode + "??";
	}

	
	public String transactionPartyDocumentStatus(String statusCode) {
		if ( TransactionPartyDocument.STATUS_COMPLETED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.partyDocument.status.completed");
		} 
		if ( TransactionPartyDocument.STATUS_FIXED_REQUESTED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.partyDocument.status.fixRequested");
		} 
		if ( TransactionPartyDocument.STATUS_NOT_YET_RETRIEVED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.partyDocument.status.netYetRetrieved");
		} 
		if ( TransactionPartyDocument.STATUS_VIEW_OPTIONAL.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.partyDocument.status.viewOptional");
		} 
		if ( TransactionPartyDocument.STATUS_VIEW_OPTIONAL_VIEWED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.partyDocument.status.viewOptionalViewed");
		} 
		if ( TransactionPartyDocument.STATUS_REJECTED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.partyDocument.status.rejected");
		} 
		if ( TransactionPartyDocument.STATUS_RETRIEVED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.partyDocument.status.retrieved");
		} 
		if ( TransactionPartyDocument.STATUS_SKIPPED.equals(statusCode) ) {
			return vaadinUi.getMsg("transaction.partyDocument.status.skipped");
		} 
		return "??" + statusCode + "??";
	}

	
	public String status(String statusCode) {
		if ( Literals.STATUS_ENABLED.equals(statusCode) ) {
			return vaadinUi.getMsg("PrettyCode.status.enabled");
		} 
		if ( Literals.STATUS_DISABLED.equals(statusCode) ) {
			return vaadinUi.getMsg("PrettyCode.status.disabled");
		}
		return "??" + statusCode + "??";
	}
	
	public String timerActionType(String timerType) {
		return vaadinUi.getMsg("PrettyCode.TimerAction.type."+timerType);
	}
	
	public String tranSnapshotsDownloadFormat(String formatType) {
		return vaadinUi.getMsg("PrettyCode.TranSnapshotsDownloadFormat." + formatType);
	}
	
	public String userDisplayName(EsfUUID userId)
	{
		String displayName;
		if ( vaadinUi.getEsfapp().getDeployId().equals(userId) )
			displayName = vaadinUi.getMsg("PrettyCode.userDisplayName.system");
		else {
			User u = User.Manager.getById(userId);
			displayName = u == null ? vaadinUi.getMsg("PrettyCode.userDisplayName.unknown",userId) : u.getFullDisplayName();
		}
		return displayName;
	}

}
