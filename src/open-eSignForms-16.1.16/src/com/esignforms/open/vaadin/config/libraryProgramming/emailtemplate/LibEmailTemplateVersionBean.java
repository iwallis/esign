// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.emailtemplate;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.EmailTemplateVersion;
import com.esignforms.open.prog.EmailTemplateVersionInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibEmailTemplateVersionBean implements Serializable, Comparable<LibEmailTemplateVersionBean> {
	private static final long serialVersionUID = 1336283918335111924L;

	private EmailTemplateVersionInfo emailtemplateVerInfo;
	private String versionLabel;
	
	private EmailTemplateVersion emailtemplateVersion;
	
	public LibEmailTemplateVersionBean(EmailTemplateVersionInfo emailtemplateVerInfo, String versionLabel) {
		this.emailtemplateVerInfo = emailtemplateVerInfo;
		this.versionLabel = versionLabel;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibEmailTemplateVersionBean )
			return emailtemplateVerInfo.equals(((LibEmailTemplateVersionBean)o).emailtemplateVerInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return emailtemplateVerInfo.hashCode();
    }
	public int compareTo(LibEmailTemplateVersionBean d) {
		return emailtemplateVerInfo.compareTo(d.emailtemplateVerInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public EmailTemplateVersionInfo emailtemplateVerInfo() {
		return emailtemplateVerInfo;
	}

	public EmailTemplateVersion emailtemplateVersion() {
		if ( emailtemplateVersion == null )
			emailtemplateVersion = EmailTemplateVersion.Manager.getById(emailtemplateVerInfo.getId());
		return emailtemplateVersion;
	}

	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( emailtemplateVersion().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			return true;
		} 

		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}
	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return emailtemplateVerInfo.getId();
	}

	public int getVersion() {
		return emailtemplateVerInfo.getVersion();
	}
	
	public String getVersionLabel() {
		return versionLabel;
	}

	public EsfDateTime getCreatedTimestamp() {
		return emailtemplateVerInfo.getCreatedTimestamp();
	}
	public String formatCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return emailtemplateVerInfo.getCreatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return emailtemplateVerInfo.getCreatedTimestamp().toLogString(vaadinUi.getUser());
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return emailtemplateVerInfo.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return emailtemplateVerInfo.getLastUpdatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return emailtemplateVerInfo.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser());
	}
	
	public String getEmailFrom() {
		return emailtemplateVersion().getEmailFrom();
	}
	public void setEmailFrom(String v) {
		emailtemplateVersion().setEmailFrom(v);
	}
	
	public String getEmailTo() {
		return emailtemplateVersion().getEmailTo();
	}
	public void setEmailTo(String v) {
		emailtemplateVersion().setEmailTo(v);
	}
	
	public String getEmailCc() {
		return emailtemplateVersion().getEmailCc();
	}
	public void setEmailCc(String v) {
		emailtemplateVersion().setEmailCc(v);
	}
	
	public String getEmailBcc() {
		return emailtemplateVersion().getEmailBcc();
	}
	public void setEmailBcc(String v) {
		emailtemplateVersion().setEmailBcc(v);
	}
	
	public String getEmailSubject() {
		return emailtemplateVerInfo.getEmailSubject();
	}
	public void setEmailSubject(String v) {
		emailtemplateVersion().setEmailSubject(v);
	}

	public String getEmailText() {
		return emailtemplateVersion().getEmailText();
	}
	public void setEmailText(String v) {
		emailtemplateVersion().setEmailText(v);
	}

	public String getEmailHtml() {
		return emailtemplateVersion().getEmailHtml();
	}
	public void setEmailHtml(String v) {
		emailtemplateVersion().setEmailHtml(v);
	}
	
}