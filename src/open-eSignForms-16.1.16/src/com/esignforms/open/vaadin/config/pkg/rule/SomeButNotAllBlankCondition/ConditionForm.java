// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.SomeButNotAllBlankCondition;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.runtime.condition.SomeButNotAllBlankCondition.Spec;
import com.esignforms.open.runtime.condition.SomeButNotAllBlankCondition;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Yozons Inc.
 */
public class ConditionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 1396725392155868519L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ConditionForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;

	GridLayout layout;
	
	final PackageVersion packageVersion;
	final DocumentVersion documentVersion;
	final SomeButNotAllBlankCondition duplicatedCondition;
	final ConditionView view;
	final ConditionContainer container;
	
	List<EsfUUID> allDocumentIds;
	List<EsfName> allDocumentNames;
	
	Spec newSpec; // when set, we're working on a new spec.
	
	public ConditionForm( ConditionView view, final ConditionContainer container, PackageVersion packageVersionParam, SomeButNotAllBlankCondition duplicatedConditionParam)	{
		this.view = view;
		this.container = container;
		this.packageVersion = packageVersionParam;
		this.documentVersion = null;
		this.duplicatedCondition = duplicatedConditionParam;
		allDocumentIds = new LinkedList<EsfUUID>(packageVersion.getDocumentIdList());
		setupForm();
	}
	
	public ConditionForm(ConditionView view, final ConditionContainer container, DocumentVersion documentVersionParam, SomeButNotAllBlankCondition duplicatedConditionParam)	{
		this.view = view;
		this.container = container;
		this.packageVersion = null;
		this.documentVersion = documentVersionParam;
		this.duplicatedCondition = duplicatedConditionParam;
		allDocumentIds = new LinkedList<EsfUUID>();
		allDocumentIds.add(documentVersion.getDocumentId());
		setupForm();
	}
	
	private void setupForm() {	
		setStyleName("SomeButNotAllBlankConditionForm");
    	allDocumentNames = new LinkedList<EsfName>();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
   	
    	for( EsfUUID docId : allDocumentIds ) {
    		Document doc = Document.Manager.getById(docId);
    		if ( doc == null ) {
    			_logger.warn("ActionForm() - Could not find document with id: " + docId);
    			allDocumentNames.add(docId.toEsfName());
    		} else {
    			allDocumentNames.add(doc.getEsfName());
    		}
    	}
	
    	// Setup layout
    	layout = new GridLayout(2,1);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.5f);
    	layout.setColumnExpandRatio(1, 0.5f);
    	setLayout(layout);

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    	footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 8488907026003066357L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

				if (propertyId.equals("documentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("ConditionTree.SomeButNotAllBlankCondition.form.documentId.label"));
                	select.setDescription(vaadinUi.getMsg("ConditionTree.SomeButNotAllBlankCondition.form.documentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -2947929602264802744L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames(newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("field")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("ConditionTree.SomeButNotAllBlankCondition.form.field.label"));
                	select.setDescription(vaadinUi.getMsg("ConditionTree.SomeButNotAllBlankCondition.form.field.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
                
				Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
                return field;
    	    }
    	 });

    	_logger.debug("Form created");

	}
	
	void setupAllFieldNames(EsfUUID newDocId) {
		NativeSelect field = (NativeSelect)getField("field");
		if ( field == null )
			return;
		EsfName fieldValue = (EsfName)field.getValue(); // save value before we empty the list
		field.removeAllItems();
		
		boolean isProduction;
		if ( packageVersion != null ) {
			com.esignforms.open.prog.Package pkg = packageVersion.getPackage();
			isProduction = pkg.getProductionVersion() == packageVersion.getVersion();
		} else {
			com.esignforms.open.prog.Document doc = documentVersion.getDocument();
			isProduction = doc.getProductionVersion() == documentVersion.getVersion();
		}

		Document document = Document.Manager.getById(newDocId);
		if ( document == null )
			return; 
		DocumentVersion docVersion;

		if ( isProduction )
			docVersion = document.getProductionDocumentVersion();
		else
			docVersion = document.getTestDocumentVersion();
		Map<EsfName,FieldTemplate> docFieldMap = docVersion.getFieldTemplateMap();
		for( EsfName fieldName : docFieldMap.keySet() ) {
			FieldTemplate t = docFieldMap.get(fieldName);
			if ( ! t.isTypeRadioButton() ) { // we skip radio buttons since conditions work on radio button groups
				field.addItem(fieldName);
			}
		}
		if ( fieldValue != null && fieldValue.isValid() ) {
			try {
				if ( field.containsId(fieldValue) )// reset value back if its valid for our new select list
					field.setValue(fieldValue); 
				else
					field.setValue(null);
				field.commit(); // don't let this cause the field to be marked as modified; ignore exceptions
			} catch( Exception e ) {}
		}
	}

	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		if ( getItemDataSource() == null ) {
    			saveSpecs();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
    		if ( newSpec != null ) {
    			container.addItem(newSpec);
    			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",vaadinUi.getMsg("ConditionTree.SomeButNotAllBlankCondition.form.message.spec.name")) );
    			newSpec = null;
    		} else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("ConditionTree.SomeButNotAllBlankCondition.form.message.spec.name")) );
    		}
    		
			saveSpecs();
    		
    		view.select(getCurrentBean()); // reselect our bean so all updates cleanly
        } else if ( source == cancelButton ) {
    		discard();
    		if ( getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			setNewSpecAsDataSource(null);
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	view.closeParentWindow();
        } else if ( source == createLikeButton ) {
        	discard();
        	Spec spec = getCurrentBean();
        	if ( spec != null ) {
    			view.unselectAll();
    			setNewSpecAsDataSource(spec.duplicate());
        	}
        } else if ( source == deleteButton ) {
        	discard();
        	
        	if ( newSpec != null ) {
        		newSpec = null;
        	} else {
            	Spec spec = getCurrentBean();
            	if ( spec != null ) {
            		container.removeItem(spec);
        			saveSpecs();
            	}
        	}
    		view.unselectAll();
			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("ConditionTree.SomeButNotAllBlankCondition.form.message.spec.name")) );
        }
    }
	
	void saveSpecs() {
		// Get all of the specs in the container and save them back to the action
		LinkedList<Spec> list = new LinkedList<Spec>();
		
		for( Spec spec : container.getItemIds() )
			list.add(spec);
		
		duplicatedCondition.setSpecList(list);
		view.specListUpdated();
	}
	
	Spec getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public Spec getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<Spec> bi = (BeanItem<Spec>)dataSource;
		return bi.getBean();
    }
    
    public void setNewSpecAsDataSource(Spec spec) {
    	newSpec = spec;
    	if ( newSpec == null )
    		setItemDataSource(null);
    	else
    		setItemDataSource(new BeanItem<Spec>(newSpec));
    }
    
    // Used when a new spec is created. We'll default the document id to the first document id our list, or a "fake" id if we have no documents.
    public EsfUUID getNewSpecDocumentId()
    {
    	if ( packageVersion != null )
    		return ( allDocumentIds == null || allDocumentIds.size() == 0 ) ? new EsfUUID() : allDocumentIds.get(0);
    	return documentVersion.getDocumentId();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		if ( newDataSource != null ) {
    		super.setItemDataSource(newDataSource, Arrays.asList("field","documentId"));
    		layout.setVisible(true);
		} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
		}
		setReadOnly(isReadOnly());
    }

	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	Spec bean = getCurrentBean();
    	
    	saveButton.setVisible(!readOnly && bean != null);
    	cancelButton.setVisible(!readOnly && bean != null);
    	closeButton.setVisible(true);
    	createLikeButton.setVisible(!readOnly && bean != null && newSpec == null);
    	deleteButton.setVisible(!readOnly && bean != null);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("documentId")) {
            layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("field")) {
        	layout.addComponent(field, 1, 0);
        }
    }
	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("ConditionTree.SomeButNotAllBlankCondition.view.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified() || newSpec != null;
	}
	
}