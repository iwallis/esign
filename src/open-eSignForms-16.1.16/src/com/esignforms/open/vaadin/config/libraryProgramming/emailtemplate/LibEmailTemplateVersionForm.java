// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.emailtemplate;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.openesignforms.ckeditor.CKEditorConfig;
import org.vaadin.openesignforms.ckeditor.CKEditorTextField;

import com.esignforms.open.config.CKEditorContext;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.widget.FieldSpecTextField;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
//import com.vaadin.event.FieldEvents;
//import com.vaadin.event.FieldEvents.BlurEvent;
//import com.vaadin.event.FieldEvents.FocusEvent;
//import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * The LibEmailTemplateVersionForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class LibEmailTemplateVersionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 4878423175847966800L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibEmailTemplateVersionForm.class);
	
	final LibEmailTemplateVersionForm thisForm;
	final LibEmailTemplateAndVersionsMainView view;
	LibEmailTemplateVersionBeanContainer container;
	GridLayout layout;
	
	Label id;
	Label createdByInfo;
	Label lastUpdatedByInfo;
	Label versionInfo;
	
	Button testToProdButton;
	Button createTestFromProdButton;
	Button revertProdToTestButton;
	Button deleteButton;

    String editorContextId;

    public LibEmailTemplateVersionForm(final LibEmailTemplateAndVersionsMainView view, LibEmailTemplateVersionBeanContainer container) {
    	setStyleName("LibEmailTemplateVersionForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.view = view;
    	this.container = container;
    	this.thisForm = this;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);

    	layout = new GridLayout(3,9);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
		
		versionInfo = new Label();
		versionInfo.setContentMode(ContentMode.TEXT);	
		layout.addComponent(versionInfo,0,0,2,0);
		
		lastUpdatedByInfo = new Label();
		lastUpdatedByInfo.setContentMode(ContentMode.TEXT);
		lastUpdatedByInfo.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedByInfo,0,6,2,6);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,7,2,7);		

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id,0,8,2,8);
		
		HorizontalLayout footer = new HorizontalLayout();
		footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);
    	
    	testToProdButton = new Button(vaadinUi.getMsg("LibEmailTemplateVersionForm.button.testToProd.label"), (ClickListener)this);
    	testToProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibEmailTemplateVersionForm.button.testToProd.icon")));
    	testToProdButton.setDescription(vaadinUi.getMsg("LibEmailTemplateVersionForm.button.testToProd.tooltip"));
    	footer.addComponent(testToProdButton);

    	createTestFromProdButton = new Button(vaadinUi.getMsg("LibEmailTemplateVersionForm.button.createTestFromProd.label"), (ClickListener)this);
    	createTestFromProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibEmailTemplateVersionForm.button.createTestFromProd.icon")));
    	createTestFromProdButton.setDescription(vaadinUi.getMsg("LibEmailTemplateVersionForm.button.createTestFromProd.tooltip"));
    	footer.addComponent(createTestFromProdButton);

    	revertProdToTestButton = new Button(vaadinUi.getMsg("LibEmailTemplateVersionForm.button.revertProdToTest.label"), (ClickListener)this);
    	revertProdToTestButton.addStyleName("caution");
    	revertProdToTestButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibEmailTemplateVersionForm.button.revertProdToTest.icon")));
    	revertProdToTestButton.setDescription(vaadinUi.getMsg("LibEmailTemplateVersionForm.button.revertProdToTest.tooltip"));
    	footer.addComponent(revertProdToTestButton);

    	if ( view.hasPermDelete() ) {
    		deleteButton = new Button(vaadinUi.getMsg("LibEmailTemplateVersionForm.button.delete.label"), (ClickListener)this);
    		deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibEmailTemplateVersionForm.button.delete.icon")));
    		deleteButton.setDescription(vaadinUi.getMsg("LibEmailTemplateVersionForm.button.delete.tooltip"));
	    	deleteButton.addStyleName("deleteButton");
	    	deleteButton.addStyleName("caution");
	    	footer.addComponent(deleteButton);
		}
    	
    	setFooter(footer);
    	setReadOnly(true);   	

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 1473881893527599294L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

				if ( propertyId.equals("emailText") ) {
	            	TextArea ta = new TextArea();
	            	ta.setWidth(100, Unit.PERCENTAGE);
	            	ta.setNullRepresentation("");
	            	ta.setRows(5);
	            	ta.setRequired(true);
	            	ta.setInputPrompt(vaadinUi.getMsg("LibEmailTemplateVersionForm.emailText.inputPrompt"));
	            	ta.setCaption(vaadinUi.getMsg("LibEmailTemplateVersionForm.emailText.label"));
	            	ta.setDescription(vaadinUi.getMsg("LibEmailTemplateVersionForm.emailText.tooltip"));
                	// Got rid since seems to conflict with CKEditor FOCUS/BLUR
	            	/*
	            	ta.addFocusListener( new FieldEvents.FocusListener() {
						private static final long serialVersionUID = 2104058633169395499L;

						@Override
						public void focus(FocusEvent event) {
							if ( view.emailtemplateForm.saveButton != null )
								view.emailtemplateForm.saveButton.removeClickShortcut();
						}
	            	});
	            	ta.addBlurListener( new FieldEvents.BlurListener() {
						private static final long serialVersionUID = 6125248001832086686L;

						@Override
						public void blur(BlurEvent event) {
							if ( view.emailtemplateForm.saveButton != null )
								view.emailtemplateForm.saveButton.setClickShortcut(KeyCode.ENTER);
						}
					});
					*/
	            	return ta;
				}
            
                if (propertyId.equals("emailHtml")) {
                	vaadinUi.getUser().releaseCKEditorContext(vaadinUi.getHttpSession(),editorContextId);
                	editorContextId = vaadinUi.getUser().createCKEditorContext(vaadinUi.getHttpSession());
                	
            		CKEditorContext editorContext = vaadinUi.getUser().getCKEditorContext(vaadinUi.getHttpSession(),editorContextId);
            		if ( editorContext != null ) { // it should always be there since we just created it above
            			editorContext.setLibrary(view.getLibrary());
            		} else {
            			_logger.error("Failed to find the CKEditorContext for user " + vaadinUi.getUser().getFullDisplayName() + "; with context id: " + editorContextId);
            		}

                	CKEditorConfig config = new CKEditorConfig();
                	config.setupForOpenESignForms(vaadinUi.getRequestContextPath(),editorContextId,null);
                	CKEditorTextField editor = new CKEditorTextField(config);
                	editor.setCaption(vaadinUi.getMsg("LibEmailTemplateVersionForm.emailHtml.label"));
                	editor.setImmediate(true);
                	editor.setHeight(410,Unit.PIXELS); // bigger than our config.setHeight to accommodate the toolbar and footer
                	editor.setReadOnly(false);
                	// Got rid since seems to conflict with CKEditor FOCUS/BLUR
                	/*
                	editor.addFocusListener( new FieldEvents.FocusListener() {
						private static final long serialVersionUID = 2104058633169395499L;

						@Override
						public void focus(FocusEvent event) {
							if ( view.emailtemplateForm.saveButton != null )
								view.emailtemplateForm.saveButton.removeClickShortcut();
						}
	            	});
                	editor.addBlurListener( new FieldEvents.BlurListener() {
						private static final long serialVersionUID = 6125248001832086686L;

						@Override
						public void blur(BlurEvent event) {
							if ( view.emailtemplateForm.saveButton != null )
								view.emailtemplateForm.saveButton.setClickShortcut(KeyCode.ENTER);
						}
					});
					*/
                    return editor;
                }

                FieldSpecTextField field = new FieldSpecTextField();
                field.setWidth(100, Unit.PERCENTAGE);
                
                if (propertyId.equals("emailFrom")) {
                	field.setCaption(vaadinUi.getMsg("LibEmailTemplateVersionForm.emailFrom.label"));
                	field.setDescription(vaadinUi.getMsg("LibEmailTemplateVersionForm.emailFrom.tooltip"));
                } else if (propertyId.equals("emailTo")) {
                	field.setCaption(vaadinUi.getMsg("LibEmailTemplateVersionForm.emailTo.label"));
                	field.setDescription(vaadinUi.getMsg("LibEmailTemplateVersionForm.emailTo.tooltip"));
                } else if (propertyId.equals("emailCc")) {
                	field.setCaption(vaadinUi.getMsg("LibEmailTemplateVersionForm.emailCc.label"));
                	field.setDescription(vaadinUi.getMsg("LibEmailTemplateVersionForm.emailCc.tooltip"));
                } else if (propertyId.equals("emailBcc")) {
                	field.setCaption(vaadinUi.getMsg("LibEmailTemplateVersionForm.emailBcc.label"));
                	field.setDescription(vaadinUi.getMsg("LibEmailTemplateVersionForm.emailBcc.tooltip"));
                } else if (propertyId.equals("emailSubject")) {
                	field.setCaption(vaadinUi.getMsg("LibEmailTemplateVersionForm.emailSubject.label"));
                	field.setRequired(true);
                }
                
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
    }
    
    @Override
	public void buttonClick(ClickEvent event) {
    	LibEmailTemplateVersionBean currBean = getCurrentBean();
    	if ( currBean == null ) return;
    	
    	Button source = event.getButton();
    	if ( source == testToProdButton ) {
    		view.promoteTestToProductionVersion();
    	} else if ( source == createTestFromProdButton ) {
    		view.createNextVersion();
    	} else if ( source == revertProdToTestButton ) {
    		view.revertProductionVersionBackToTest();
    	} else if ( source == deleteButton ) {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	ConfirmDialog.show(UI.getCurrent(), 
	    			vaadinUi.getMsg("LibEmailTemplateVersionForm.button.delete.ConfirmDialog.caption"),
	    			vaadinUi.getMsg("LibEmailTemplateVersionForm.button.delete.ConfirmDialog.message"),
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
	    	        new ConfirmDialog.Listener() {
						private static final long serialVersionUID = 3109791064128956353L;

						public void onClose(ConfirmDialog dialog) {
	    	                if (dialog.isConfirmed()) {
	    	                    view.delete();
	    	                }
	    	            }
	    	        });
    	}
     }
	
    LibEmailTemplateVersionBean save(Connection con) throws SQLException {
    	LibEmailTemplateVersionBean currBean = getCurrentBean();
    	if ( currBean != null ) {
    		if ( currBean.save(con,null) ) {
    			return currBean;
    		}
    	}
    	return null;
    }
    	
    public String getCurrentBeanName() {
    	LibEmailTemplateVersionBean currBean = getCurrentBean();
		return currBean == null ? "(None)" : view.getEsfNameWithVersion();
	}
    
	LibEmailTemplateVersionBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public LibEmailTemplateVersionBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibEmailTemplateVersionBean> bi = (BeanItem<LibEmailTemplateVersionBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached stuff
		resetCaches();
		
    	if (newDataSource != null) {
    		LibEmailTemplateVersionBean bean = getBean(newDataSource);
    		
    		super.setItemDataSource(newDataSource, Arrays.asList("emailFrom","emailTo","emailCc","emailBcc","emailSubject","emailText","emailHtml"));
    		
    		setupForm(bean);
    		
    		setReadOnly( ! view.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	

    @Override
    public void setReadOnly(boolean readOnly) {   	
    	LibEmailTemplateVersionBean bean = getCurrentBean();
    	
    	boolean isTestVersion = bean != null && view.isTestVersion(bean.emailtemplateVerInfo());
    	
    	testToProdButton.setVisible(bean != null && !readOnly && isTestVersion && bean.emailtemplateVersion().doUpdate() && view.getLibEmailTemplateBean().isEnabled());
    	createTestFromProdButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.emailtemplateVerInfo()) && !view.hasTestVersion());
    	revertProdToTestButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.emailtemplateVerInfo()) && !view.hasTestVersion());
    	
    	// Show this only if the object is created and is the Test version
    	if ( deleteButton != null ) {
    		deleteButton.setVisible(bean != null && bean.emailtemplateVersion().doUpdate() && isTestVersion); 
    	}

    	// If the request is to be not-readonly, but this is not the test version, we convert it back to readonly
    	if ( !readOnly && bean != null && !view.isTestVersion(bean.emailtemplateVerInfo())) {
    		readOnly = true;
    	}
    	super.setReadOnly(readOnly);
    }
    
    void setupForm(LibEmailTemplateVersionBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("LibEmailTemplateVersionForm.label.id",bean.getId()) );
		
		versionInfo.setValue( vaadinUi.getMsg("LibEmailTemplateVersionForm.label.version",bean.getVersion(),view.getVersionLabel(bean.emailtemplateVerInfo())) );
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.emailtemplateVerInfo().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("LibEmailTemplateVersionForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );
		
		if ( bean.emailtemplateVersion().doInsert() ) {
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibEmailTemplateVersionForm.label.lastUpdated.whenNew") );
			lastUpdatedByInfo.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.emailtemplateVerInfo().getLastUpdatedByUserId());
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibEmailTemplateVersionForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedByInfo.setIcon(null);
		}
    }

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
    	if (propertyId.equals("emailFrom") ) {
			layout.addComponent(field,0,1,1,1);
    	} else if (propertyId.equals("emailTo") ) {
			layout.addComponent(field,0,2);
    	} else if (propertyId.equals("emailCc") ) {
			layout.addComponent(field,1,2);
    	} else if (propertyId.equals("emailBcc") ) {
			layout.addComponent(field,2,2);
    	} else if (propertyId.equals("emailSubject") ) {
			layout.addComponent(field,0,3,2,3);
    	} else if (propertyId.equals("emailText") ) {
			layout.addComponent(field,0,4,2,4);
    	} else if (propertyId.equals("emailHtml") ) {
			layout.addComponent(field,0,5,2,5);
    	}
    }


	@Override
	public String checkDirty() { // OBSOLETE SINCE VERSION FORM HAS NO BUTTONS FOR SAVING, CANCELING
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibEmailTemplateAndVersionsMainView.EmailTemplate.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
	
	@Override
	public void detach() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		User user = vaadinUi.getUser();
		if ( user != null ) {
	    	user.releaseCKEditorContext(vaadinUi.getHttpSession(),editorContextId);
		}
		super.detach();
	}
}