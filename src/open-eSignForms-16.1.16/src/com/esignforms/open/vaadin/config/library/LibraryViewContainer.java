// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.library;

import java.io.Serializable;
import java.util.Collection;

import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.util.BeanItemContainer;

public class LibraryViewContainer extends BeanItemContainer<LibraryBean> implements Serializable {
	private static final long serialVersionUID = -8639792978836181427L;

	public LibraryViewContainer() throws InstantiationException, IllegalAccessException {
		super(LibraryBean.class);

		Collection<Library> libraries = Library.Manager.getForUserWithPermission(EsfVaadinUI.getInstance().getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS);
		for( Library l : libraries ) {
			addItem( new LibraryBean(l) );
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}