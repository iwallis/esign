// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.ComparisonCondition;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.runtime.condition.ComparisonCondition.Spec;
import com.esignforms.open.runtime.condition.ComparisonCondition;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.FieldSpecTextField;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author Yozons Inc.
 */
public class ConditionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -4212668735295784281L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ConditionForm.class);
	
	Button saveButton;
	Button cancelButton;
	Button closeButton;
	CheckBox cbConditionNegated;

	GridLayout layout;
	
	final PackageVersion packageVersion;
	final DocumentVersion documentVersion;
	final ComparisonCondition duplicatedCondition;
	
	final ConditionForm thisConditionForm;
	final ConditionView view;
	final ConditionContainer container;
	
	List<EsfUUID> allDocumentIds;
	List<EsfName> allDocumentNames;
	
	
	public ConditionForm(ConditionView view, final ConditionContainer container, PackageVersion packageVersionParam, ComparisonCondition duplicatedConditionParam) {
		this.thisConditionForm = this;
		this.view = view;
		this.container = container;
		this.packageVersion = packageVersionParam;
		this.documentVersion = null;
		this.duplicatedCondition = duplicatedConditionParam;
		allDocumentIds = new LinkedList<EsfUUID>(packageVersion.getDocumentIdList());
		setupForm();
	}
	
	public ConditionForm(ConditionView view, final ConditionContainer container, DocumentVersion documentVersionParam, ComparisonCondition duplicatedConditionParam) {
		this.thisConditionForm = this;
		this.view = view;
		this.container = container;
		this.packageVersion = null;
		this.documentVersion = documentVersionParam;
		this.duplicatedCondition = duplicatedConditionParam;
		allDocumentIds = new LinkedList<EsfUUID>();
		allDocumentIds.add(documentVersion.getDocumentId());
		setupForm();
	}
	
	private void setupForm() {	
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		setStyleName("ComparisonConditionForm");
    	allDocumentNames = new LinkedList<EsfName>();
    	
    	for( EsfUUID docId : allDocumentIds ) {
    		Document doc = Document.Manager.getById(docId);
    		if ( doc == null ) {
    			_logger.warn("ActionForm() - Could not find document with id: " + docId);
    			allDocumentNames.add(docId.toEsfName());
    		} else {
    			allDocumentNames.add(doc.getEsfName());
    		}
    	}
	
    	// Setup layout
    	layout = new GridLayout(5,2);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.0f);
    	layout.setColumnExpandRatio(1, 0.0f);
    	layout.setColumnExpandRatio(2, 0.0f);
    	layout.setColumnExpandRatio(3, 1.0f);
    	layout.setColumnExpandRatio(4, 0.0f);
    	setLayout(layout);

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	
    	cbConditionNegated = new CheckBox(vaadinUi.getMsg("ConditionTree.negated.checkbox.label"));
    	cbConditionNegated.setDescription(vaadinUi.getMsg("ConditionTree.negated.checkbox.tooltip"));
    	cbConditionNegated.setImmediate(true);
    	cbConditionNegated.setValue(duplicatedCondition.isNegated());
    	cbConditionNegated.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = -667108167862261996L;

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				boolean isChecked = (Boolean)event.getProperty().getValue();
				duplicatedCondition.setNegated(isChecked);
				// Toggle the window caption
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				Window w = vaadinUi.getWindow(thisConditionForm);
				if ( w != null ) {
					w.setCaption( isChecked ? vaadinUi.getMsg("ConditionTree.ComparisonCondition.view.negated.window.caption") : vaadinUi.getMsg("ConditionTree.ComparisonCondition.view.window.caption"));
				}
			}
    	});
    	layout.addComponent(cbConditionNegated,0,0,4,0);
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 5320549434163192414L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

				if (propertyId.equals("documentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("ConditionTree.ComparisonCondition.form.documentId.label"));
                	select.setDescription(vaadinUi.getMsg("ConditionTree.ComparisonCondition.form.documentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 4589701468758165201L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames(newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("field")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("ConditionTree.ComparisonCondition.form.field.label"));
                	select.setDescription(vaadinUi.getMsg("ConditionTree.ComparisonCondition.form.field.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfName newFieldName = (EsfName)event.getProperty().getValue();
							if ( newFieldName != null && ! newFieldName.isNull() ) {
								setupOperatorsForField(newFieldName);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("operator")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("ConditionTree.ComparisonCondition.form.operator.label"));
                	select.setDescription(vaadinUi.getMsg("ConditionTree.ComparisonCondition.form.operator.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
	                select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
	                for( String operator : vaadinUi.getStringArray("PrettyCode.ComparisonCondition.operators.list") ) {
		                select.addItem(operator);
		                select.setItemCaption(operator, vaadinUi.getPrettyCode().comparisonConditionOperator(operator));
	                }
                	return select;
                }
				
				if (propertyId.equals("caseSensitive")) {
					CheckBox cb = new CheckBox(vaadinUi.getMsg("ConditionTree.ComparisonCondition.form.caseSensitive.label"));
					cb.setDescription(vaadinUi.getMsg("ConditionTree.ComparisonCondition.form.caseSensitive.tooltip"));
					return cb;
				}
                
                if ( propertyId.equals("fieldValueSpec") ) {
                	FieldSpecTextField tf = new FieldSpecTextField();
                	tf.setWidth(100, Unit.PERCENTAGE);
                	tf.setCaption(vaadinUi.getMsg("ConditionTree.ComparisonCondition.form.fieldValueSpec.label"));
                	tf.setDescription(vaadinUi.getMsg("ConditionTree.ComparisonCondition.form.fieldValueSpec.tooltip"));
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	return tf;
                }
                
				Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
                
                return field;
    	    }
    	 });

    	_logger.debug("Form created");

	}
	
	void setupAllFieldNames(EsfUUID newDocId) {
		NativeSelect field = (NativeSelect)getField("field");
		if ( field == null )
			return;
		EsfName fieldValue = (EsfName)field.getValue(); // save value before we empty the list
		field.removeAllItems();
		
		boolean isProduction;
		if ( packageVersion != null ) {
			com.esignforms.open.prog.Package pkg = packageVersion.getPackage();
			isProduction = pkg.getProductionVersion() == packageVersion.getVersion();
		} else {
			com.esignforms.open.prog.Document doc = documentVersion.getDocument();
			isProduction = doc.getProductionVersion() == documentVersion.getVersion();
		}

		Document document = Document.Manager.getById(newDocId);
		if ( document == null )
			return; 
		DocumentVersion docVersion;

		if ( isProduction )
			docVersion = document.getProductionDocumentVersion();
		else
			docVersion = document.getTestDocumentVersion();
		Map<EsfName,FieldTemplate> docFieldMap = docVersion.getFieldTemplateMap();
		for( EsfName fieldName : docFieldMap.keySet() ) {
			FieldTemplate t = docFieldMap.get(fieldName);
			if ( ! t.isTypeRadioButton() ) { // we skip radio buttons since conditions work on radio button groups
				field.addItem(fieldName);
			}
		}
		if ( fieldValue != null && fieldValue.isValid() ) {
			try {
				if ( field.containsId(fieldValue) )// reset value back if its valid for our new select list
					field.setValue(fieldValue); 
				else
					field.setValue(null);
				field.commit(); // don't let this cause the field to be marked as modified; ignore exceptions
			} catch( Exception e ) {}
		}
	}
	
	void setupOperatorsForField(EsfName fieldName) {
		NativeSelect operatorNativeSelect = (NativeSelect)getField("operator");
		if ( operatorNativeSelect == null )
			return;
		
		Field<?> caseSensitiveField = getField("caseSensitive");
		
		String operatorValue = (String)operatorNativeSelect.getValue(); // save value before we empty the operator list
		operatorNativeSelect.removeAllItems();
		
		Field<?> documentIdField = getField("documentId");
		EsfUUID documentId = (EsfUUID)documentIdField.getValue();
		
		boolean isProduction;
		if ( packageVersion != null ) {
			com.esignforms.open.prog.Package pkg = packageVersion.getPackage();
			isProduction = pkg.getProductionVersion() == packageVersion.getVersion();
		} else {
			com.esignforms.open.prog.Document doc = documentVersion.getDocument();
			isProduction = doc.getProductionVersion() == documentVersion.getVersion();
		}

		Document document = Document.Manager.getById(documentId);
		if ( document == null )
			return; 
		DocumentVersion docVersion;

		if ( isProduction )
			docVersion = document.getProductionDocumentVersion();
		else
			docVersion = document.getTestDocumentVersion();
		FieldTemplate fieldTemplate = docVersion.getFieldTemplate(fieldName);
		if ( fieldTemplate == null )
			return;
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		String[] operatorList;
		if ( fieldTemplate.isTypeDate() || fieldTemplate.isTypeDateTime() || 
			 fieldTemplate.isTypeDecimal() || fieldTemplate.isTypeInteger() || fieldTemplate.isTypeMoney() ) {
			operatorList = vaadinUi.getStringArray("PrettyCode.ComparisonCondition.operators.listForIntegerMoneyDecimalDateDateTime");
			if ( caseSensitiveField != null )
				caseSensitiveField.setVisible(false);
		} else {
			operatorList = vaadinUi.getStringArray("PrettyCode.ComparisonCondition.operators.list");
			if ( caseSensitiveField != null )
				caseSensitiveField.setVisible(true);
		}
		
		boolean operatorValueIsValid = false;
        for( String operator : operatorList ) {
        	operatorNativeSelect.addItem(operator);
        	operatorNativeSelect.setItemCaption(operator, vaadinUi.getPrettyCode().comparisonConditionOperator(operator));
        	if ( operator.equals(operatorValue) )
        		operatorValueIsValid = true;
        }

		if ( operatorValueIsValid ) {
			operatorNativeSelect.setValue(operatorValue);
		} else {
			operatorNativeSelect.setValue(null);
		}
		try {
			operatorNativeSelect.commit(); // don't let this cause the field to be marked as modified; ignore exceptions
		} catch( Exception e ) {}

	}

	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		if ( getItemDataSource() == null ) {
    			saveSpec();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("ConditionTree.ComparisonCondition.form.message.spec.name")) );
    		
			saveSpec();
        } else if ( source == cancelButton ) {
    		discard();
    		setSpecAsDataSource(container.firstItemId());
    		vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
        } else if ( source == closeButton ) {
        	view.closeParentWindow();
        }
    }
	
	void saveSpec() {
		// Get the spec in the container and save it back to the action
		LinkedList<Spec> list = new LinkedList<Spec>();
		
		for( Spec spec : container.getItemIds() )
			list.add(spec);
		
		duplicatedCondition.setSpec(list.getFirst());
	}
	
	Spec getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public Spec getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<Spec> bi = (BeanItem<Spec>)dataSource;
		return bi.getBean();
    }
    
    public EsfUUID getNewSpecDocumentId()
    {
    	if ( packageVersion != null )
    		return ( allDocumentIds == null || allDocumentIds.size() == 0 ) ? new EsfUUID() : allDocumentIds.get(0);
    	return documentVersion.getDocumentId();
    }

    public void setSpecAsDataSource(Spec spec) {
    	if ( spec == null ) {
    		spec = new Spec(getNewSpecDocumentId(), new EsfName(""), ComparisonCondition.OPERATOR_EQUALS, "", new EsfBoolean(false));
    		container.refresh(spec);
    	}
    	setItemDataSource(new BeanItem<Spec>(spec));
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		if ( newDataSource != null ) {
    		super.setItemDataSource(newDataSource, Arrays.asList("field","documentId","operator","fieldValueSpec","caseSensitive"));
    		layout.setVisible(true);
		} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
		}
		setReadOnly(isReadOnly());
    }

	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	cbConditionNegated.setReadOnly(readOnly);
    	
    	Spec bean = getCurrentBean();
    	
    	saveButton.setVisible(!readOnly && bean != null);
    	cancelButton.setVisible(!readOnly && bean != null);
    	closeButton.setVisible(true);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("documentId")) {
            layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("field")) {
        	layout.addComponent(field, 1, 1);
        } else if (propertyId.equals("operator")) {
        	layout.addComponent(field, 2, 1);
        } else if (propertyId.equals("fieldValueSpec")) {
        	layout.addComponent(field, 3, 1);
        } else if (propertyId.equals("caseSensitive")) {
        	layout.addComponent(field, 4, 1);
        	layout.setComponentAlignment(field, Alignment.BOTTOM_CENTER);
        }
    }
	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("ConditionTree.ComparisonCondition.view.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
	
}