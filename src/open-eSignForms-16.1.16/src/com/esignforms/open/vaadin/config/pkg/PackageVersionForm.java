// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.peter.buttongroup.ButtonGroup;

import com.esignforms.open.Application;
import com.esignforms.open.Version;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.ButtonMessageInfo;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentInfo;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.user.UI;
import com.esignforms.open.user.User;
import com.esignforms.open.util.XmlUtil;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.pkg.ImportView;
import com.esignforms.open.vaadin.config.pkg.reportField.PackageReportFieldView;
import com.esignforms.open.vaadin.config.pkg.rule.PackageProgrammingRuleView;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.vaadin.data.Buffered;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.event.dd.acceptcriteria.Or;
import com.vaadin.event.dd.acceptcriteria.SourceIs;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.server.Page;
import com.vaadin.server.StreamResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.WebBrowser;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.Tree;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Tree.TreeDragMode;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The PackageVersionForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class PackageVersionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -5496476792980958582L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackageVersionForm.class);
	
	final PackageVersionForm thisForm;
	final PackageAndVersionsMainView view;
	PackageVersionBeanContainer container;
	VerticalLayout layout;
	
	HorizontalLayout documentSelectionLayout;
	final LibraryDocumentTree libraryDocumentTree;
	final DocumentList documentList;
	final PartyList partyList;
	int partyListNumUnused = 0;
	Button addViewOnlyPartyButton;
	Button partyListRemoveUnusedButton;
	HorizontalLayout packageDisclosureButtonMessageSetLayout;
	
	Label id;
	Label createdByInfo;
	Label lastUpdatedByInfo;
	Label versionInfo;
	
	Button testToProdButton;
	Button createTestFromProdButton;
	Button revertProdToTestButton;
	Button deleteButton;
	Button packageProgrammingRuleButton;
	Button packageReportFieldsButton;
	Button exportButton;
	Button importButton;

	@SuppressWarnings("deprecation")
	public PackageVersionForm(PackageAndVersionsMainView view, PackageVersionBeanContainer container) {
    	setStyleName("PackageVersionForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.view = view;
    	this.container = container;
    	this.thisForm = this;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);

    	layout = new VerticalLayout();
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
		
		versionInfo = new Label();
		versionInfo.setContentMode(ContentMode.TEXT);	
		layout.addComponent(versionInfo);
		
		documentSelectionLayout = new HorizontalLayout();
		documentSelectionLayout.setWidth(100, Unit.PERCENTAGE);
		documentSelectionLayout.setSpacing(true);
		documentSelectionLayout.setMargin(false);
		libraryDocumentTree = new LibraryDocumentTree(vaadinUi.getMsg("PackageVersionForm.LibraryDocumentTree.label"));
		Panel libraryDocumentTreePanel = new Panel();
		libraryDocumentTreePanel.setStyleName(Reindeer.PANEL_LIGHT);
		libraryDocumentTreePanel.setHeight(170, Unit.PIXELS);
		libraryDocumentTreePanel.setContent(libraryDocumentTree);
		documentList = new DocumentList(vaadinUi.getMsg("PackageVersionForm.DocumentList.label"));
		documentList.setWidth(100, Unit.PERCENTAGE);
		libraryDocumentTree.initializeDND();
		documentList.initializeDND();
		documentSelectionLayout.addComponent(libraryDocumentTreePanel);
		documentSelectionLayout.addComponent(documentList);
		documentSelectionLayout.setExpandRatio(libraryDocumentTreePanel, 0.3f);
		documentSelectionLayout.setExpandRatio(documentList, 0.7f);
		documentSelectionLayout.setVisible(false);
		layout.addComponent(documentSelectionLayout);
		
		partyList = new PartyList(vaadinUi.getMsg("PackageVersionForm.PartyList.label"));
		partyList.setWidth(100, Unit.PERCENTAGE);
		partyList.initializeDND();
		partyList.setVisible(false);
		layout.addComponent(partyList);
		
		HorizontalLayout partyListButtonLayout = new HorizontalLayout();
		partyListButtonLayout.setMargin(false);
		partyListButtonLayout.setSpacing(true);
		layout.addComponent(partyListButtonLayout);
		layout.setComponentAlignment(partyListButtonLayout, Alignment.MIDDLE_RIGHT);
		
		addViewOnlyPartyButton = new Button(vaadinUi.getMsg("PackageVersionForm.PartyList.button.addViewOnly.label",0), (ClickListener)this);
		addViewOnlyPartyButton.addStyleName(Reindeer.BUTTON_SMALL);
		addViewOnlyPartyButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageVersionForm.PartyList.button.addViewOnly.icon")));
		addViewOnlyPartyButton.setDescription(vaadinUi.getMsg("PackageVersionForm.PartyList.button.addViewOnly.tooltip"));
		partyListButtonLayout.addComponent(addViewOnlyPartyButton);
		
		partyListRemoveUnusedButton = new Button(vaadinUi.getMsg("PackageVersionForm.PartyList.button.removeUnused.label",0), (ClickListener)this);
		partyListRemoveUnusedButton.addStyleName(Reindeer.BUTTON_SMALL);
		partyListRemoveUnusedButton.setDescription(vaadinUi.getMsg("PackageVersionForm.PartyList.button.removeUnused.tooltip"));
		partyListRemoveUnusedButton.setVisible(false); 
		partyListButtonLayout.addComponent(partyListRemoveUnusedButton);
		
		packageDisclosureButtonMessageSetLayout = new HorizontalLayout();
		packageDisclosureButtonMessageSetLayout.setWidth(100, Unit.PERCENTAGE);
		packageDisclosureButtonMessageSetLayout.setSpacing(true);
		packageDisclosureButtonMessageSetLayout.setMargin(false);
		layout.addComponent(packageDisclosureButtonMessageSetLayout);
		
		lastUpdatedByInfo = new Label();
		lastUpdatedByInfo.setContentMode(ContentMode.TEXT);
		lastUpdatedByInfo.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedByInfo);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo);		

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id);
		
		VerticalLayout footer = new VerticalLayout();
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);

    	HorizontalLayout footer1 = new HorizontalLayout();
    	footer1.setStyleName("footer1");
    	footer1.setSpacing(true);
    	footer1.setMargin(false);

    	HorizontalLayout footer2 = new HorizontalLayout();
    	footer2.setStyleName("footer2");
    	footer2.setSpacing(false);
    	footer2.setMargin(false);

    	footer.addComponent(footer1);
    	footer.addComponent(footer2);
    	
    	packageProgrammingRuleButton = new Button(vaadinUi.getMsg("PackageVersionForm.button.packageProgrammingRuleButton.label"), (ClickListener)this);
    	packageProgrammingRuleButton.setStyleName(Reindeer.BUTTON_SMALL);
    	packageProgrammingRuleButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageVersionForm.button.packageProgrammingRuleButton.icon")));
    	packageProgrammingRuleButton.setDescription(vaadinUi.getMsg("PackageVersionForm.button.packageProgrammingRuleButton.tooltip"));
    	footer1.addComponent(packageProgrammingRuleButton);
    	
    	packageReportFieldsButton = new Button(vaadinUi.getMsg("PackageVersionForm.button.packageReportFieldsButton.label"), (ClickListener)this);
    	packageReportFieldsButton.setStyleName(Reindeer.BUTTON_SMALL);
    	packageReportFieldsButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageVersionForm.button.packageReportFieldsButton.icon")));
    	packageReportFieldsButton.setDescription(vaadinUi.getMsg("PackageVersionForm.button.packageReportFieldsButton.tooltip"));
    	footer1.addComponent(packageReportFieldsButton);

    	ButtonGroup buttonGroup = new ButtonGroup();
    	buttonGroup.setStyleName(Reindeer.BUTTON_SMALL);
    	footer1.addComponent(buttonGroup);
    	
		exportButton = new Button(vaadinUi.getMsg("PackageVersionForm.button.export.label"));
		exportButton.setDescription(vaadinUi.getMsg("PackageVersionForm.button.export.tooltip"));
		exportButton.setStyleName(Reindeer.BUTTON_SMALL);
		exportButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.download.icon")));
		exportButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = -8024660317461884116L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				PackageVersionBean bean = getCurrentBean();
				doExport(bean.duplicatedPackageVersion());
				vaadinUi.showStatus(vaadinUi.getMsg("PackageVersionForm.exportOkay.message",bean.duplicatedPackageVersion().getPackagePathNameVersionWithLabel()));
			}
		});
		buttonGroup.addButton(exportButton);

		importButton = new Button(vaadinUi.getMsg("PackageVersionForm.button.import.label"));
		importButton.setDescription(vaadinUi.getMsg("PackageVersionForm.button.import.tooltip"));
		importButton.setStyleName(Reindeer.BUTTON_SMALL);
		importButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.upload.icon")));
		importButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = -8699179475449095150L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				final PackageVersionBean currBean = getCurrentBean();
				
			    ImportView importView = new ImportView(currBean);
			    importView.initView();
				
			    ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("PackageVersionForm.ImportView.window.caption",currBean.duplicatedPackageVersion().getPackage().getPathName()), importView);
			    w.center();
				w.setWidth(600, Unit.PIXELS);
				w.setHeight(300, Unit.PIXELS);
				w.setModal(true);
				importView.activateView(EsfView.OpenMode.WINDOW, "");
				vaadinUi.addWindowToUI(w);	
				w.addCloseListener( new CloseListener() {
					private static final long serialVersionUID = -3377897139529537823L;

					@Override
					public void windowClose(CloseEvent e) {
						BeanItem<PackageVersionBean> updatedPackageVerItem = new BeanItem<PackageVersionBean>(currBean); 
						setItemDataSource(updatedPackageVerItem);
					}
				});
			}
		});
		buttonGroup.addButton(importButton);

     	testToProdButton = new Button(vaadinUi.getMsg("PackageVersionForm.button.testToProd.label"), (ClickListener)this);
     	testToProdButton.setStyleName(Reindeer.BUTTON_SMALL);
    	testToProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageVersionForm.button.testToProd.icon")));
    	testToProdButton.setDescription(vaadinUi.getMsg("PackageVersionForm.button.testToProd.tooltip"));
    	footer2.addComponent(testToProdButton);

    	createTestFromProdButton = new Button(vaadinUi.getMsg("PackageVersionForm.button.createTestFromProd.label"), (ClickListener)this);
    	createTestFromProdButton.setStyleName(Reindeer.BUTTON_SMALL);
    	createTestFromProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageVersionForm.button.createTestFromProd.icon")));
    	createTestFromProdButton.setDescription(vaadinUi.getMsg("PackageVersionForm.button.createTestFromProd.tooltip"));
    	footer2.addComponent(createTestFromProdButton);

    	revertProdToTestButton = new Button(vaadinUi.getMsg("PackageVersionForm.button.revertProdToTest.label"), (ClickListener)this);
    	revertProdToTestButton.setStyleName(Reindeer.BUTTON_SMALL);
    	revertProdToTestButton.addStyleName("caution");
    	revertProdToTestButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageVersionForm.button.revertProdToTest.icon")));
    	revertProdToTestButton.setDescription(vaadinUi.getMsg("PackageVersionForm.button.revertProdToTest.tooltip"));
    	footer2.addComponent(revertProdToTestButton);
    	
		User loggedInUser = vaadinUi.getUser();
		UI ui = new UI();
		if ( ui.canDelete(loggedInUser, UI.UI_PACKAGE_VIEW) ) {
			deleteButton = new Button(vaadinUi.getMsg("PackageVersionForm.button.delete.label"), (ClickListener)this);
			deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageVersionForm.button.delete.icon")));
			deleteButton.setDescription(vaadinUi.getMsg("PackageVersionForm.button.delete.tooltip"));
	    	deleteButton.addStyleName("deleteButton");
	    	deleteButton.addStyleName("caution");
	    	footer2.addComponent(deleteButton);
		}
    	
    	setFooter(footer);
    	setReadOnly(true);   	

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 3556741147314250220L;

			@Override
			public Field<?> createField(Item item, Object propertyId, Component uiContext) {
				
				if ( propertyId.equals("packageDocumentId") ) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageVersionForm.packageDocumentId.label"));
					select.setDescription(vaadinUi.getMsg("PackageVersionForm.packageDocumentId.tooltip"));
					select.setNullSelectionAllowed(false);
					select.setImmediate(false);
					select.setRequired(true);
					select.setMultiSelect(false);
	                select.addValidator(new SelectValidator(select));
					select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					
					Collection<Library> libraries = Library.Manager.getForUserWithViewDetailsPermission(vaadinUi.getUser(), Library.INCLUDE.ONLY_ENABLED);
					for( Library library : libraries ) {
						Collection<DocumentInfo> documents = DocumentInfo.Manager.getAll(library,DocumentInfo.INCLUDE.ONLY_ENABLED);
						for( DocumentInfo docInfo : documents ) {
							select.addItem(docInfo.getId());
							select.setItemCaption(docInfo.getId(), library.getPathName().toString() + "/" + docInfo.getEsfName().toString());
						}
					}

                	return select;
				}
				
				if ( propertyId.equals("buttonMessageId") ) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageVersionForm.buttonMessageId.label"));
					select.setDescription(vaadinUi.getMsg("PackageVersionForm.buttonMessageId.tooltip"));
					select.setNullSelectionAllowed(false);
					select.setImmediate(false);
					select.setRequired(true);
					select.setMultiSelect(false);
	                select.addValidator(new SelectValidator(select));
					select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					
					Collection<Library> libraries = Library.Manager.getForUserWithViewDetailsPermission(vaadinUi.getUser(), Library.INCLUDE.ONLY_ENABLED);
					for( Library library : libraries ) {
						Collection<ButtonMessageInfo> buttonMessageSets = ButtonMessageInfo.Manager.getAll(library,ButtonMessageInfo.INCLUDE.ONLY_ENABLED);
						for( ButtonMessageInfo bm : buttonMessageSets ) {
							select.addItem(bm.getId());
							select.setItemCaption(bm.getId(), library.getPathName().toString() + "/" + bm.getEsfName().toString());
						}
					}

                	return select;
				}
				
				Field<?> field = super.createField(item, propertyId, uiContext);
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
    }
    
    @Override
	public void buttonClick(ClickEvent event) {
    	PackageVersionBean currBean = getCurrentBean();
    	if ( currBean == null ) return;
    	
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == testToProdButton ) {
    		view.promoteTestToProductionVersion();
    	} else if ( source == createTestFromProdButton ) {
    		view.createNextVersion();
    	} else if ( source == revertProdToTestButton ) {
    		view.revertProductionVersionBackToTest();
    	} else if ( source == deleteButton ) {
	    	ConfirmDialog.show(com.vaadin.ui.UI.getCurrent(), 
	    			vaadinUi.getMsg("PackageVersionForm.button.delete.ConfirmDialog.caption"),
	    			vaadinUi.getMsg("PackageVersionForm.button.delete.ConfirmDialog.message"),
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
	    	        new ConfirmDialog.Listener() {
						private static final long serialVersionUID = 7135417985411282340L;

						public void onClose(ConfirmDialog dialog) {
	    	                if (dialog.isConfirmed()) {
	    	                    view.delete();
	    	                }
	    	            }
	    	        });
    	} else if ( source == partyListRemoveUnusedButton ) {
    		partyList.removeUnusedFromPartyList(currBean);
    	} else if ( source == addViewOnlyPartyButton ) {
    		partyList.addViewOnlyPartyToListList(currBean);
    	} else if ( source == packageProgrammingRuleButton ) {
            PackageProgrammingRuleView packageRuleView = new PackageProgrammingRuleView(currBean.duplicatedPackageVersion());
            packageRuleView.initView();
    		
            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("PackageProgrammingRuleView.window.caption",currBean.duplicatedPackageVersion().getPackagePathNameVersionWithLabel()), packageRuleView);
            w.center();
        	w.setWidth(90, Unit.PERCENTAGE);
        	w.setHeight(95, Unit.PERCENTAGE);

        	packageRuleView.activateView(EsfView.OpenMode.WINDOW, "");
            packageRuleView.setReadOnly(thisForm.isReadOnly() || currBean.isProductionVersion()); // if we're read-only or this is a production package, no modifications to custom logic is allowed
        	packageRuleView.setParentWindow(w);
        	vaadinUi.addWindowToUI(w);	
    	} else if ( source == packageReportFieldsButton ) {
    		PackageReportFieldView packageReportFieldView = new PackageReportFieldView(currBean.duplicatedPackageVersion());
    		packageReportFieldView.initView();
    		
            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("PackageReportFieldView.window.caption",currBean.duplicatedPackageVersion().getPackagePathNameVersionWithLabel()), packageReportFieldView);
            w.center();
        	w.setWidth(90, Unit.PERCENTAGE);
        	w.setHeight(95, Unit.PERCENTAGE);

        	packageReportFieldView.activateView(EsfView.OpenMode.WINDOW, "");
        	packageReportFieldView.setReadOnly(!view.hasPermUpdate()); // Even if production, we do allow report field changes any time so long as they have update permission
        	packageReportFieldView.setParentWindow(w);
        	vaadinUi.addWindowToUI(w);	
    	}
     }
	
    PackageVersionBean save(Connection con) throws SQLException {
    	PackageVersionBean currBean = getCurrentBean();
    	if ( currBean != null ) {
    		if ( currBean.save(con,null) ) {
    			return currBean;
    		}
    	}
    	return null;
    }
    	
    public String getCurrentBeanName() {
    	PackageVersionBean currBean = getCurrentBean();
		return currBean == null ? "(None)" : view.getPathNameWithVersion();
	}
    
	PackageVersionBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public PackageVersionBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<PackageVersionBean> bi = (BeanItem<PackageVersionBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached stuff
		resetCaches();
		
    	if (newDataSource != null) {
    		PackageVersionBean bean = getBean(newDataSource);
    		
    		super.setItemDataSource(newDataSource, Arrays.asList("packageDocumentId","buttonMessageId"));
    		
    		setupForm(bean);
    		
    		setReadOnly( ! view.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	

    @Override
    public void setReadOnly(boolean readOnly) {   	
    	PackageVersionBean bean = getCurrentBean();
    	
    	boolean isTestVersion = bean != null && view.isTestVersion(bean.packageVerInfo());
    	boolean isReserved = bean != null &&  bean.duplicatedPackageVersion().getPackage().isReserved();
    	
    	testToProdButton.setVisible(bean != null && !readOnly && isTestVersion && bean.duplicatedPackageVersion().doUpdate() && view.getPackageBean().isEnabled() && !isReserved);
    	createTestFromProdButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.packageVerInfo()) && !view.hasTestVersion() && !isReserved);
    	revertProdToTestButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.packageVerInfo()) && !view.hasTestVersion() && !isReserved);
    	
    	packageProgrammingRuleButton.setVisible(bean != null);
    	
    	// Show this only if the object is created and is the Test version
    	if ( deleteButton != null ) {
    		deleteButton.setVisible(bean != null && bean.duplicatedPackageVersion().doUpdate() && isTestVersion && view.hasPermDelete() && ! isReserved ); 
    	}

    	exportButton.setVisible(bean != null);
    	
    	importButton.setVisible(bean != null && !readOnly && isTestVersion && bean.duplicatedPackageVersion().doUpdate() && !isReserved);

    	// If the request is to be not-readonly, but this is not the test version, we convert it back to readonly
    	if ( !readOnly && bean != null && !view.isTestVersion(bean.packageVerInfo())) {
    		readOnly = true;
    	}
    	super.setReadOnly(readOnly);
    	
    	libraryDocumentTree.setReadOnly(readOnly);
    	libraryDocumentTree.setDragMode( readOnly ? TreeDragMode.NONE : TreeDragMode.NODE);
    	//documentList.setReadOnly(readOnly);
    	documentList.setDragMode( readOnly ? TableDragMode.NONE : TableDragMode.ROW);
    	//partyList.setReadOnly(readOnly);
    	partyList.setDragMode( readOnly ? TableDragMode.NONE : TableDragMode.ROW);
    	partyList.setupPartyListRemoveButton();
		
		addViewOnlyPartyButton.setVisible( ! readOnly );
    }
    
    void setupForm(PackageVersionBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("PackageVersionForm.label.id",bean.getId()) );
		
		versionInfo.setValue( vaadinUi.getMsg("PackageVersionForm.label.version",bean.getVersion(),view.getVersionLabel(bean.packageVerInfo())) );
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.packageVerInfo().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("PackageVersionForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );
		
		if ( bean.duplicatedPackageVersion().doInsert() ) {
			lastUpdatedByInfo.setValue( vaadinUi.getMsg("PackageVersionForm.label.lastUpdated.whenNew") );
			lastUpdatedByInfo.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.packageVerInfo().getLastUpdatedByUserId());
			lastUpdatedByInfo.setValue( vaadinUi.getMsg("PackageVersionForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedByInfo.setIcon(null);
		}
		
		libraryDocumentTree.refresh();
		updateListsFromBean(bean);
		documentSelectionLayout.setVisible(true);
		partyList.setVisible(true);
    }
    
    void updateListsFromBean(PackageVersionBean bean) {
		documentList.setupDocumentList(bean);
		partyList.setupPartyList(bean);
    }

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("packageDocumentId")) {
        	packageDisclosureButtonMessageSetLayout.addComponent(field);
        } else if (propertyId.equals("buttonMessageId")) {
        	packageDisclosureButtonMessageSetLayout.addComponent(field);
        }
    }

	@Override
	public String checkDirty() { // OBSOLETE SINCE VERSION FORM HAS NO BUTTONS FOR SAVING, CANCELING
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageAndVersionsMainView.Package.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		PackageVersionBean bean = getCurrentBean();
		return isModified() || (bean != null && bean.isModified());
	}
	
    @Override
    public void commit() throws Buffered.SourceException {
    	super.commit();
    }
    
    @Override
    public void discard() throws Buffered.SourceException {
    	PackageVersionBean bean = getCurrentBean();
    	if ( bean != null ) {
        	bean.resetCaches();
        	libraryDocumentTree.refresh(); // refresh our tree
        	documentList.setupDocumentList(bean); // refresh our table
        	partyList.setupPartyList(bean); // refresh our table
    	}
    	super.discard();
    }
	
	void doExport(PackageVersion pkgVer) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		Package pkg = pkgVer.getPackage();
		
		StringBuilder buf = new StringBuilder(4096*10 + (int)pkg.getEstimatedLengthXml() + (int)pkgVer.getEstimatedLengthXml());
        buf.append("<PackageExport xmlns=\"").append(XmlUtil.getXmlNamespace2013()).append("\">\n");
        buf.append(" <deployId>").append(vaadinUi.getEsfapp().getDeployId().toXml()).append("</deployId>\n");
        buf.append(" <release>").append(XmlUtil.toEscapedXml(Version.getReleaseString())).append("</release>\n");
        buf.append(" <versionMajor>").append(Version.getMajor()).append("</versionMajor>\n");
        buf.append(" <versionMinor>").append(Version.getMinor()).append("</versionMinor>\n");
        buf.append(" <timestamp>").append((new EsfDateTime()).toXml()).append("</timestamp>\n");
        buf.append(" <exportedByUserId>").append(vaadinUi.getUser().getId().toXml()).append("</exportedByUserId>\n");
        
        pkg.appendXml(buf);
        pkgVer.appendXml(buf);
        buf.append("</PackageExport>\n");
        
		final byte[] data = EsfString.stringToBytes(buf.toString());
		
		StreamResource.StreamSource ss = new StreamResource.StreamSource() {
			private static final long serialVersionUID = -7158355998475946390L;

			InputStream is = new BufferedInputStream(new ByteArrayInputStream(data));
			@Override
			public InputStream getStream() {
				return is;
			}
		};
		
		WebBrowser wb = Page.getCurrent().getWebBrowser();
		
		String packagePathName = pkg.getPathName().toString();
		packagePathName = packagePathName.replaceAll(EsfPathName.PATH_SEPARATOR, "_");
		String fileName = vaadinUi.getMsg("PackageVersionForm.button.export.filename",packagePathName);
		StreamResource sr = new StreamResource(ss, fileName);
		//sr.setMIMEType(Application.CONTENT_TYPE_XML+Application.CONTENT_TYPE_CHARSET_UTF_8);
		sr.setMIMEType(Application.CONTENT_TYPE_BINARY);
		sr.setCacheTime(0); // Disable cache
		sr.getStream().setParameter("Content-Length", Integer.toString(data.length));
		sr.getStream().setParameter("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		Page.getCurrent().open(sr, wb.isChrome() || wb.isSafari() ? "_top" : "_blank", false);
		
		User user = vaadinUi.getUser();
		if ( user != null ) {
			user.logConfigChange("Exported package: " + pkg.getPathName() + "; libraryId: " + pkg.getId() + "; version: " + pkgVer.getVersion() + "; versionId: " + pkgVer.getId());
			vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange("User: " + user.getFullDisplayName() + " exported package: " + pkg.getPathName() + "; libraryId: " + pkg.getId() + "; version: " + pkgVer.getVersion() + "; versionId: " + pkgVer.getId());
		}
	}


    
	class LibraryDocumentTree extends Tree {
		private static final long serialVersionUID = -2895986668192699304L;

		final HierarchicalContainer container;
		
		public LibraryDocumentTree(String caption) {
			super(caption);
			container = new HierarchicalContainer();
			container.addContainerProperty("label", String.class, null);
			container.addContainerProperty("libraryId", EsfUUID.class, null);
			container.addContainerProperty("documentId", EsfUUID.class, null);
			setItemCaptionPropertyId("label");
	        setContainerDataSource(container);
		}
		
		public HierarchicalContainer getContainer() {
			return container;
		}
		
		public void initializeDND() {
			setDropHandler(new DropHandler() {
				private static final long serialVersionUID = -2292969943086717305L;

				public void drop(DragAndDropEvent dropEvent) {
					PackageVersionBean packageVersionBean = getCurrentBean();
					
	                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
	                DocumentBean sourceItemId = (DocumentBean)t.getItemId();
	                if ( sourceItemId == null )
	                	return;
	                Container sourceContainer = t.getSourceContainer();

	                // Put back in the tree. First we need to find the node for the library the document is in.
					EsfUUID libraryItemId = sourceItemId.getLibraryId();
	                Item libraryItem = container.getItem(libraryItemId);
	                if ( libraryItem == null )
	                	return;
	                libraryDocumentTree.expandItem(libraryItemId); // make it visible
	                
	                // Put the document back in with the library as the parent
	                EsfUUID docItemId = sourceItemId.getDocumentId();
	                Item docItem = container.addItem(docItemId);
	                docItem.getItemProperty("label").setValue(sourceItemId.getDocumentName().toString());
	                docItem.getItemProperty("libraryId").setValue(libraryItemId);
	                docItem.getItemProperty("documentId").setValue(docItemId);
					container.setParent(docItemId, libraryItemId);
					container.setChildrenAllowed(docItemId, false);
	                
	                // Finally, remove the document from the table and from our bean
	                sourceContainer.removeItem(sourceItemId);
	                
	                packageVersionBean.removeDocumentId(docItemId);
	                
	                documentList.reorder();
	                thisForm.updateListsFromBean(packageVersionBean);
	            }

	            public AcceptCriterion getAcceptCriterion() {
	                return new And(new SourceIs(documentList), AcceptItem.ALL);
	            }
	        });
		}
		
		public void refresh() {
			container.removeAllItems();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			
	        Item item = null;
	        EsfUUID itemId;
			Collection<Library> libraries = Library.Manager.getForUserWithViewDetailsPermission(vaadinUi.getUser(), Library.INCLUDE.ONLY_ENABLED);
			for( Library library : libraries ) {
				Collection<DocumentInfo> documents = DocumentInfo.Manager.getAll(library,DocumentInfo.INCLUDE.ONLY_ENABLED);
				if ( documents.size() == 0 ) // we'll leave libraries that have no documents available off the list
					continue;

				// Put the library in the tree as a parent that can have children
				itemId = library.getId();
				item = container.addItem(itemId);
				item.getItemProperty("label").setValue(library.getPathName().toString());
				item.getItemProperty("libraryId").setValue(library.getId());
				container.setChildrenAllowed(itemId, true);
				
				EsfUUID libraryItemId = itemId;
				
				for( DocumentInfo docInfo : documents ) {
					itemId = docInfo.getId();
					item = container.addItem(itemId);
					item.getItemProperty("label").setValue(docInfo.getEsfName().toString());
					item.getItemProperty("libraryId").setValue(library.getId());
					item.getItemProperty("documentId").setValue(docInfo.getId());
					container.setParent(itemId, libraryItemId);
					container.setChildrenAllowed(itemId, false);
				}
			}
		}
		
		void removeDocumentId(EsfUUID documentId) {
			for( Object itemId : container.getItemIds() ) {
				Item item = container.getItem(itemId);
				EsfUUID treeDocumentId = (EsfUUID)item.getItemProperty("documentId").getValue();
				if ( documentId.equals(treeDocumentId) ) {
					container.removeItem(itemId);
					break;
				}
			}
		}
	} // LibraryDocumentTree
	
	
	public class DocumentBean implements java.io.Serializable {
		private static final long serialVersionUID = 7824300795668016474L;

		int order;
		EsfUUID libraryId;
		EsfPathName libraryPathName;
		EsfUUID documentId;
		EsfName documentName;
		int numParties;
		
		public DocumentBean( int order, Document document, boolean isProductionVersion ) {
			this( order, 
				  document.getLibraryId(), 
				  document.getLibrary().getPathName(), 
				  document.getId(), 
				  document.getEsfName(), 
				  isProductionVersion ? document.getProductionDocumentVersion().getNumPartyTemplates() : document.getTestDocumentVersion().getNumPartyTemplates() 
				);
		}
		public DocumentBean( int order, EsfUUID libraryId, EsfPathName libraryPathName, EsfUUID documentId, EsfName documentName, int numParties ) {
			this.order = order;
			this.libraryId = libraryId;
			this.libraryPathName = libraryPathName;
			this.documentId = documentId;
			this.documentName = documentName;
			this.numParties = numParties;
		}
		
		public int getOrder() {
			return order;
		}
		public void setOrder(int v) {
			order = v;
		}
		public EsfUUID getLibraryId() {
			return libraryId;
		}
		public EsfPathName getLibraryPathName() {
			return libraryPathName;
		}
		public EsfUUID getDocumentId() {
			return documentId;
		}
		public EsfName getDocumentName() {
			return documentName;
		}
		public int getNumParties() {
			return numParties;
		}
	} // DocumentBean
	
	class DocumentList extends Table {
		private static final long serialVersionUID = -2735604859103373867L;

		final DocumentList thisList;
		final BeanItemContainer<DocumentBean> container;
		
		public DocumentList(String caption) {
			super(caption);
			thisList = this;
			this.alwaysRecalculateColumnWidths = true;
			container = new BeanItemContainer<DocumentBean>(DocumentBean.class);
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	        setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("PackageVersionForm.DocumentList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("PackageVersionForm.DocumentList.showColumnHeaders"));
			setSortEnabled(false);
			setColumnAlignment("order", Align.CENTER);
			setColumnAlignment("numParties", Align.CENTER);
			setNullSelectionAllowed(true);
			setSelectable(true);
			setImmediate(true);
			setPageLength(6);
			addValueChangeListener( new Property.ValueChangeListener() {
				private static final long serialVersionUID = 290752585649134905L;

				@Override
				public void valueChange(Property.ValueChangeEvent event) {
					DocumentBean bean = (DocumentBean)getValue();
					if ( bean == null) return; // unselected

					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		            PackageVersionBean packageVersionBean = getCurrentBean();
		            
		            final DocumentPartyToPackagePartyForm mappingForm = new DocumentPartyToPackagePartyForm(packageVersionBean,bean.getDocumentId());
		            mappingForm.initView();

		            String pageTitle = vaadinUi.getMsg("DocumentPartyToPackagePartyForm.window.caption");
		            
		            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(pageTitle, mappingForm, false);
		            w.center();
		        	w.setWidth(400, Unit.PIXELS);
		        	w.setHeight(400, Unit.PIXELS);
					w.setModal(false);
					w.addCloseListener( new CloseListener() {
						private static final long serialVersionUID = -3052995689129950485L;

						@Override
						public void windowClose(CloseEvent e) {
							thisList.select(null);
							partyList.setupPartyList(getCurrentBean());
						}
						
					});
		        	mappingForm.setParentWindow(w);
		        	mappingForm.activateView(EsfView.OpenMode.WINDOW, "");
		        	mappingForm.setReadOnly(thisForm.isReadOnly() || packageVersionBean.isProductionVersion());
					vaadinUi.addWindowToUI(w);
				}
			});
			
		}
		
		public void reorder() {
            // Fix up the order now
			LinkedList<EsfUUID> orderList = new LinkedList<EsfUUID>();
            int order = 1;
            for( DocumentBean bi : container.getItemIds() ) {
            	Item item = container.getItem(bi);
            	item.getItemProperty("order").setValue(order);
            	bi.setOrder(order++);
            	orderList.add((EsfUUID)item.getItemProperty("documentId").getValue());
            }
            getCurrentBean().setDocumentIdOrder(orderList);
		}
		
		public void initializeDND() {
			setDropHandler(new DropHandler() {
				private static final long serialVersionUID = -8813692102061122329L;

				public void drop(DragAndDropEvent dropEvent) {
	                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
	                
	                PackageVersionBean packageVersionBean = getCurrentBean();

	                Container sourceContainer = t.getSourceContainer();
	                if ( sourceContainer == container ) { // reordering within the table rows
		                DocumentBean sourceItemId = (DocumentBean)t.getItemId();

		                AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails)dropEvent.getTargetDetails());
		                DocumentBean targetItemId = (DocumentBean)dropData.getItemIdOver();
		                
		                // No move if source and target are the same
		                if ( sourceItemId == targetItemId )
		                	return;
		                
		                // Let's remove the source of the drag so we can add it back where requested...
		                container.removeItem(sourceItemId);
		                if ( targetItemId == null ) {
		                	container.addItem(sourceItemId);
		                } else if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
		                	container.addItemAfter(targetItemId,sourceItemId);
		            	} else {
		                    Object prevItemId = container.prevItemId(targetItemId);
		                    container.addItemAfter(prevItemId, sourceItemId);
		            	}
	                } else if ( sourceContainer == libraryDocumentTree.getContainer() ) { 
	                	// add from tree
		                Object sourceItemId = t.getItemId();
		                Item sourceItem = sourceContainer.getItem(sourceItemId);
		                EsfUUID sourceLibraryId = (EsfUUID)sourceItem.getItemProperty("libraryId").getValue();
		                EsfUUID sourceDocumentId = (EsfUUID)sourceItem.getItemProperty("documentId").getValue();
		                
		                if ( sourceLibraryId == null ) return;
		                
		                AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails)dropEvent.getTargetDetails());
		                DocumentBean targetItemId = (DocumentBean)dropData.getItemIdOver();
		                
		                // If there's no document id, they dragged the library over, so do those
		                if ( sourceDocumentId == null ) {
		                	// For the children of the library node, add them to our table and remove them from the tree. We make our own list to avoid concurrent modifications to tree
		                	Collection<?> libraryChildren = libraryDocumentTree.getChildren(sourceLibraryId);
		                	if ( libraryChildren == null || libraryChildren.size() == 0 ) {
		                		return;
		                	}
		                	List<Object> childrenItemIds = new LinkedList<Object>(libraryChildren);
		                	for( Object docItemId : childrenItemIds ) {
				                Document document = Document.Manager.getById((EsfUUID)docItemId);
				                if ( document == null ) return;
				                
				                DocumentBean newDocumentBean = new DocumentBean(container.size()+1, document, getCurrentBean().isProductionVersion());
				                
				                // Let's remove the document from the tree...
				                sourceContainer.removeItem(docItemId);

				                if ( targetItemId == null ) {
				                	container.addItem(newDocumentBean);
				                } else if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
				                	container.addItemAfter(targetItemId, newDocumentBean);
				            	} else {
				                    Object prevItemId = container.prevItemId(targetItemId);
				                    container.addItemAfter(prevItemId, newDocumentBean);
				            	}
				                
				                packageVersionBean.addDocument(document);
		                	}
		                }
		                else {
			                Document document = Document.Manager.getById(sourceDocumentId);
			                if ( document == null ) return;
			                
			                DocumentBean newDocumentBean = new DocumentBean(container.size()+1, document, getCurrentBean().isProductionVersion());
			                
			                // Let's remove the source of the drag from the tree...
			                sourceContainer.removeItem(sourceItemId);

			                if ( targetItemId == null ) {
			                	container.addItem(newDocumentBean);
			                } else if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
			                	container.addItemAfter(targetItemId, newDocumentBean);
			            	} else {
			                    Object prevItemId = container.prevItemId(targetItemId);
			                    container.addItemAfter(prevItemId, newDocumentBean);
			            	}
			                
			                packageVersionBean.addDocument(document);
		                }
	                }
	                
	                reorder();
	                thisForm.updateListsFromBean(packageVersionBean);
	            }

	            public AcceptCriterion getAcceptCriterion() {
	                return new Or( new And(SourceIsTarget.get(),new Not(AbstractSelect.VerticalLocationIs.MIDDLE)),
	                		       new And(new SourceIs(libraryDocumentTree),new Not(AbstractSelect.VerticalLocationIs.MIDDLE))
	                             ); // dragging within the table or from the tree
	            }
	        });
			
		}
		
		public void setupDocumentList(PackageVersionBean bean) {
			container.removeAllItems();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			
			int order = 1;
			for( EsfUUID docId : bean.getDocumentIdList() ) {
				Document document = Document.Manager.getById(docId);
				if ( document == null ) {
					vaadinUi.showWarning(vaadinUi.getMsg("PackageVersionForm.DocumentList.document.missing.caption"), vaadinUi.getMsg("PackageVersionForm.DocumentList.document.missing.message",docId));
				} else {
					DocumentVersion docVer = bean.isProductionVersion() ? document.getProductionDocumentVersion() : document.getTestDocumentVersion();
					if ( docVer == null ) {
						String prodOrTest = bean.isProductionVersion() ? vaadinUi.getMsg("transaction.production") : vaadinUi.getMsg("transaction.test");
						vaadinUi.showWarning(vaadinUi.getMsg("PackageVersionForm.DocumentList.documentVersion.missing.caption"), vaadinUi.getMsg("PackageVersionForm.DocumentList.documentVersion.missing.message",prodOrTest,document.getEsfName()));
					} else {
						DocumentBean documentBean = new DocumentBean(order++, document, bean.isProductionVersion());
						container.addBean(documentBean);
						// We remove any documents we already have from the tree. It's possible it's not in the tree (if subsequently disabled or permissions changed) and if so and
						// the user removes that document from the package, it won't be allowed to be re-selected since only those in the tree can be added.
						libraryDocumentTree.removeDocumentId(document.getId());
					}
				}
			}
			
			setPageLength( Math.min(6, container.size()) );
		}
		
	} // DocumentList
	
	
	public class PackageVersionPartyTemplateBean implements java.io.Serializable {
		private static final long serialVersionUID = 2194839362034619506L;

		int order;
		EsfUUID packageVersionPartyTemplateId;
		EsfName packageVersionPartyTemplateName;
		String packageVersionPartyTemplateDisplayName;
		int numDocuments;
		
		public PackageVersionPartyTemplateBean( int order, PackageVersionPartyTemplate packageVersionPartyTemplate ) {
			this( order, packageVersionPartyTemplate.getId(), packageVersionPartyTemplate.getEsfName(), packageVersionPartyTemplate.getDisplayName(), packageVersionPartyTemplate.getNumPackageVersionPartyTemplateDocumentParties() );
		}
		public PackageVersionPartyTemplateBean( int order, EsfUUID packageVersionPartyTemplateId, EsfName packageVersionPartyTemplateName, String packageVersionPartyTemplateDisplayName, int numDocuments ) {
			this.order = order;
			this.packageVersionPartyTemplateId = packageVersionPartyTemplateId;
			this.packageVersionPartyTemplateName = packageVersionPartyTemplateName;
			this.packageVersionPartyTemplateDisplayName = packageVersionPartyTemplateDisplayName;
			this.numDocuments = numDocuments;
		}
		
		public int getOrder() {
			return order;
		}
		public void setOrder(int v) {
			order = v;
		}
		public EsfUUID getPackageVersionPartyTemplateId() {
			return packageVersionPartyTemplateId;
		}
		public EsfName getPackageVersionPartyTemplateName() {
			return packageVersionPartyTemplateName;
		}
		public String getPackageVersionPartyTemplateDisplayName() {
			return packageVersionPartyTemplateDisplayName;
		}
		public int getNumDocuments() {
			return numDocuments;
		}
	} // PackageVersionPartyBean
	
	class PartyList extends Table {
		private static final long serialVersionUID = -5062461752668475187L;

		final BeanItemContainer<PackageVersionPartyTemplateBean> container;
		final PartyList thisList;
		
		public PartyList(String caption) {
			super(caption);
			thisList = this;
			this.alwaysRecalculateColumnWidths = true;
			container = new BeanItemContainer<PackageVersionPartyTemplateBean>(PackageVersionPartyTemplateBean.class);
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	        setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("PackageVersionForm.PartyList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("PackageVersionForm.PartyList.showColumnHeaders"));
			setSortEnabled(false);
			setColumnAlignment("order", Align.CENTER);
			setColumnAlignment("numDocuments", Align.CENTER);
			setNullSelectionAllowed(true);
			setSelectable(true);
			setImmediate(true);
			setPageLength(6);
			addValueChangeListener( new Property.ValueChangeListener() {
				private static final long serialVersionUID = 8070099823223839160L;

				@Override
				public void valueChange(Property.ValueChangeEvent event) {
					PackageVersionPartyTemplateBean bean = (PackageVersionPartyTemplateBean)getValue();
					if ( bean == null) return; // unselected
					
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		            PackageVersionBean packageVersionBean = getCurrentBean();
		            
		            final PackagePartyToDocumentPartyForm mappingForm = new PackagePartyToDocumentPartyForm(packageVersionBean,bean.getPackageVersionPartyTemplateId());
		            mappingForm.initView(); 
		            // safe to use the packageVersionPartyTemplate after initView() sets it up in the mappingForm
		            String name = mappingForm.packageVersionPartyTemplate.getEsfName().toString();
		            String pageTitle = vaadinUi.getMsg("PackagePartyToDocumentPartyForm.window.caption",name);
		            
		            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(pageTitle,  mappingForm, false);
		            w.center();
		        	w.setWidth(900, Unit.PIXELS);
		        	w.setHeight(550, Unit.PIXELS);
					w.setModal(false);
					w.addCloseListener( new CloseListener() {
						private static final long serialVersionUID = -856892450718421895L;

						@Override
						public void windowClose(CloseEvent e) {
							thisList.select(null);
							thisForm.updateListsFromBean(getCurrentBean());
						}
						
					});
		        	mappingForm.setParentWindow(w);
		        	mappingForm.activateView(EsfView.OpenMode.WINDOW, "");
		        	
		        	// If the package is not being edited, no changes are allowed.
		        	// If we're on a read-only version or production, but it's the reports access party, we still let them edit it.
		        	mappingForm.setReadOnly( view.packageForm.isReadOnly() || ((thisForm.isReadOnly() || packageVersionBean.isProductionVersion()) && ! mappingForm.packageVersionPartyTemplate.isEsfReportsAccess()) );
					vaadinUi.addWindowToUI(w);
				}
			});
		}
		
		public void reorder() {
            // Fix up the order now
			LinkedList<EsfUUID> orderList = new LinkedList<EsfUUID>();
            int order = 1;
            for( PackageVersionPartyTemplateBean bi : container.getItemIds() ) {
            	Item item = container.getItem(bi);
            	item.getItemProperty("order").setValue(order);
            	bi.setOrder(order++);
            	orderList.add((EsfUUID)item.getItemProperty("packageVersionPartyTemplateId").getValue());
            }
            getCurrentBean().setPackageVersionPartyTemplateIdOrder(orderList);
		}
		
		public void initializeDND() {
			setDropHandler(new DropHandler() {
				private static final long serialVersionUID = -544326946804992479L;

				public void drop(DragAndDropEvent dropEvent) {
	                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
	                
	                Container sourceContainer = t.getSourceContainer();
	                if ( sourceContainer == container ) {
	                	// reordering within the table rows
	                	PackageVersionPartyTemplateBean sourceItemId = (PackageVersionPartyTemplateBean)t.getItemId();

		                AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails)dropEvent.getTargetDetails());
		                PackageVersionPartyTemplateBean targetItemId = (PackageVersionPartyTemplateBean)dropData.getItemIdOver();
		                
		                // No move if source and target are the same
		                if ( sourceItemId == targetItemId )
		                	return;
		                
		                // Let's remove the source of the drag so we can add it back where requested...
		                container.removeItem(sourceItemId);
		                if ( targetItemId == null ) {
		                	container.addItem(sourceItemId);
		                } else if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
		                	container.addItemAfter(targetItemId,sourceItemId);
		            	} else {
		                    Object prevItemId = container.prevItemId(targetItemId);
		                    container.addItemAfter(prevItemId, sourceItemId);
		            	}
		                
		                reorder();
		                //thisForm.updateListsFromBean(getCurrentBean()); right now, because just moving, no need to reload anything since doesn't affect other lists
	                }
	            }

	            public AcceptCriterion getAcceptCriterion() {
	                return new And(SourceIsTarget.get(),new Not(AbstractSelect.VerticalLocationIs.MIDDLE)); // dragging within the table 
	            }
	        });
			
		}
		
		public void setupPartyList(PackageVersionBean bean) {
			container.removeAllItems();
			
			partyListNumUnused = 0;
			int order = 1;
			for( PackageVersionPartyTemplate partyTemplate : bean.getPackageVersionPartyTemplateList() ) {
				PackageVersionPartyTemplateBean partyBean = new PackageVersionPartyTemplateBean(order++, partyTemplate);
				if ( partyBean.getNumDocuments() == 0 )
					++partyListNumUnused;
				container.addBean(partyBean);
			}
			
			setPageLength( Math.min(6, container.size()) );
			
			setupPartyListRemoveButton();
		}
		
		public void setupPartyListRemoveButton() {
			if ( ! thisForm.isReadOnly() && partyListNumUnused > 0 ) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				partyListRemoveUnusedButton.setCaption(vaadinUi.getMsg("PackageVersionForm.PartyList.button.removeUnused.label",partyListNumUnused));
				partyListRemoveUnusedButton.setVisible(true);
			} else {
				partyListRemoveUnusedButton.setVisible(false);
			}
		}
		
		public void addViewOnlyPartyToListList(PackageVersionBean bean) {
			bean.addViewOnlyParty();
			setupPartyList(bean);
		}

		public void removeUnusedFromPartyList(PackageVersionBean bean) {
			for( PackageVersionPartyTemplate partyTemplate : bean.getPackageVersionPartyTemplateList() ) {
				if ( partyTemplate.getNumPackageVersionPartyTemplateDocumentParties() == 0 ) {
					bean.removePackageVersionPartyTemplate(partyTemplate);
				}
			}
			setupPartyList(bean);
		}

	} // PartyList

}