// Copyright (C) 2013-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg;

import java.io.StringReader;
import java.util.Collection;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;

import com.esignforms.open.Errors;
import com.esignforms.open.Version;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.util.XmlUtil;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.UploadFileWithProgress;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

public class ImportView extends Panel implements EsfView {
	private static final long serialVersionUID = 4288159210588583731L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ImportView.class);
	
    final ImportView thisView;
    final PackageVersion duplicatedPackageVersion;
    
    Library defaultLibrary;
    Collection<Library> allLibraries;
    
    NativeSelect librarySelect;
    UploadFileWithProgress uploadFile;
	Namespace ns = Namespace.getNamespace(XmlUtil.getXmlNamespace2013());
    // Uploaded data
	EsfPathName packagePathName;
    EsfUUID deployId;
    String release;
    int versionMajor;
    int versionMinor;
    EsfDateTime timestamp;
    Element packageVersionElement;
    
	public ImportView(PackageVersionBean currBean) {
		super();
		thisView = this;
		this.duplicatedPackageVersion = currBean.duplicatedPackageVersion();
		setStyleName("PackageVersionFormImportView");
		setSizeFull();
	}
	
	protected void buildUploadLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			VerticalLayout layout = new VerticalLayout();
			setContent(layout);
			layout.setWidth(100,Unit.PERCENTAGE);
			layout.setSpacing(true);
			layout.setMargin(true);
			
			layout.addComponent( new Label("") );
			
			librarySelect = new NativeSelect(vaadinUi.getMsg("PackageVersionForm.ImportView.select.library.label"));
			librarySelect.setDescription(vaadinUi.getMsg("PackageVersionForm.ImportView.select.library.tooltip"));
			librarySelect.setNullSelectionAllowed(false); 
			librarySelect.setImmediate(false);
			librarySelect.setRequired(true);
			librarySelect.setMultiSelect(false);
			librarySelect.addValidator(new SelectValidator(librarySelect));
			librarySelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
        	
        	allLibraries = Library.Manager.getForUserWithViewDetailsPermission(vaadinUi.getUser(),Library.INCLUDE.ONLY_ENABLED);
        	for( Library library : allLibraries ) {
        		librarySelect.addItem(library.getId());
        		librarySelect.setItemCaption( library.getId(), library.getPathName().toString() );
        	}
        	librarySelect.setValue(Library.Manager.getTemplate().getId());
        	
			layout.addComponent(librarySelect);

			layout.addComponent( new Label("") );
			
			uploadFile = new UploadFileWithProgress(null,vaadinUi.getMsg("PackageVersionForm.ImportView.button.upload.label"),(int)vaadinUi.getEsfapp().getUploadConfigMaxBytes()) {
				private static final long serialVersionUID = -6589054028537342896L;
				
				@Override
				public void afterUploadSucceeded() {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					String xmlData = EsfString.bytesToString(uploadFile.getFileData());
					Errors errors = processImportedXml(xmlData);
					if ( errors.hasError() ) {
						vaadinUi.show(errors);
					}  else {
						vaadinUi.show(errors);
						vaadinUi.showStatus(vaadinUi.getMsg("PackageVersionForm.ImportView.upload.successful",uploadFile.getFileName(),EsfInteger.byteSizeInUnits(uploadFile.getFileData().length) ));
					}
				}
				@Override
				public void afterUploadFailed(String fileName, boolean wasCanceled, boolean wasInvalidMimeType, boolean wasTooBig, int contentLength, int maxSize) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					if ( wasTooBig ) {
						vaadinUi.showError(
								vaadinUi.getMsg("PackageVersionForm.ImportView.fileTooLarge.caption"), 
								vaadinUi.getMsg("PackageVersionForm.ImportView.fileTooLarge.message",EsfInteger.byteSizeInUnits(contentLength),EsfInteger.byteSizeInUnits(maxSize))
								);
					} else if ( wasCanceled ) {
						vaadinUi.showStatus(vaadinUi.getMsg("PackageVersionForm.ImportView.upload.canceled"));
					} else if ( wasInvalidMimeType ) {
						vaadinUi.showError(
								vaadinUi.getMsg("PackageVersionForm.ImportView.invalidType.caption"), 
								vaadinUi.getMsg("PackageVersionForm.ImportView.invalidType.message",EsfString.isBlank(fileName)?"(unknown)":fileName)
								);
					}
				}
			}; 
			uploadFile.setDescription(vaadinUi.getMsg("PackageVersionForm.ImportView.button.upload.tooltip"));
			uploadFile.setAllowedMimeTypesForXml();
			layout.addComponent(uploadFile);

			layout.addComponent( new Label(vaadinUi.getMsg("PackageVersionForm.ImportView.notice.label")) );
			
	    	_logger.debug("buildUploadLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build upload view", e);
			vaadinUi.showError("Package ImportView view exception", e.getMessage());
		}
	}
	
	Errors processImportedXml(String xmlData) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Errors errors = new Errors();
		
		StringReader sr = null;

		SAXBuilder saxBuilder = new SAXBuilder(XMLReaders.NONVALIDATING);
		saxBuilder.setIgnoringElementContentWhitespace(true);

		try {
			sr = new StringReader(xmlData);

			org.jdom2.Document doc = saxBuilder.build(sr);
			Element rootElement = doc.getRootElement();
			
			loadXmlFromJdom(rootElement, xmlData);
			
			doImport(errors);
		} catch (EsfException e) {
			errors.addError(e.getMessage());
		} catch (java.io.IOException e) {
			_logger.error("processImportedXml() - could not read data from the XML string: " + xmlData,e);
			errors.addError(vaadinUi.getMsg("PackageVersionForm.ImportView.xmlError.ioexception"));
		} catch (JDOMException e) {
			_logger.error("processImportedXml() - could not XML parse data from the XML string: " + xmlData, e);
			errors.addError(vaadinUi.getMsg("PackageVersionForm.ImportView.xmlError.jdomexception"));
		} finally {
			if (sr != null) {
				try { sr.close(); } catch (Exception e) {}
			}
		}
		
		return errors;
	}
	
	protected void loadXmlFromJdom(Element rootElement, String dataXml) throws EsfException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		String rootName = rootElement.getName();
		if (!rootName.equals("PackageExport")) {
			_logger.error("loadXmlFromJdom(): Root element is not PackageExport.  Found instead: " + rootName + "; in XML: "
					+ dataXml);
			throw new EsfException(vaadinUi.getMsg("PackageVersionForm.ImportView.xmlError.invalidRoot"));
		}

		deployId = new EsfUUID(rootElement.getChildText("deployId", ns));
		if (deployId.isNull()) {
			_logger.error("loadXmlFromJdom(): required deployId element is missing in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("PackageVersionForm.ImportView.xmlError.missingElement","deployId"));
		}

		release = rootElement.getChildText("release", ns);
		if (EsfString.isBlank(release)) {
			_logger.error("loadXmlFromJdom(): required release element is missing in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("PackageVersionForm.ImportView.xmlError.missingElement","release"));
		}

		versionMajor = vaadinUi.getEsfapp().stringToInt(rootElement.getChildText("versionMajor", ns),0);
		if (versionMajor < 1) {
			_logger.error("loadXmlFromJdom(): required versionMajor element is missing in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("PackageVersionForm.ImportView.xmlError.missingElement","versionMajor"));
		}
		versionMinor = vaadinUi.getEsfapp().stringToInt(rootElement.getChildText("versionMinor", ns),0);
		if (versionMinor < 1) {
			_logger.error("loadXmlFromJdom(): required versionMinor element is missing in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("PackageVersionForm.ImportView.xmlError.missingElement","versionMinor"));
		}
		if ( versionMajor > Version.getMajor() || (versionMajor == Version.getMajor() && versionMinor > Version.getMinor()) ) {
			_logger.error("loadXmlFromJdom(): versionMajor " + versionMajor + "and versionMinor " + versionMinor + " is greater than our own version: " + Version.getMajor() + "." + Version.getMinor() + "; in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("PackageVersionForm.ImportView.xmlError.majorVersionMismatch",versionMajor,versionMinor,Version.getMajor(),Version.getMinor()));
		}
		
		timestamp =  EsfDateTime.createFromToXml(rootElement.getChildText("timestamp", ns));
		if ( timestamp.isNull() ) {
			_logger.error("loadXmlFromJdom(): required timestamp element is missing in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("PackageVersionForm.ImportView.xmlError.missingElement","timestamp"));
		}

		Element packageElement = rootElement.getChild("Package", ns);
		if ( packageElement == null ) {
			_logger.error("loadXmlFromJdom(): required Package element is missing in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("PackageVersionForm.ImportView.xmlError.missingElement","Package"));
		}
		
		packagePathName = new EsfPathName(packageElement.getChildText("pathName",ns));
		
		packageVersionElement = rootElement.getChild("PackageVersion",ns);
		if ( packageVersionElement == null ) {
			_logger.error("loadXmlFromJdom(): required PackageVersion element is missing in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("PackageVersionForm.ImportView.xmlError.missingElement","PackageVersion"));			
		}
	}

	void doImport(Errors errors) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		errors.addInfo(vaadinUi.getMsg("PackageVersionForm.ImportView.info.importedFrom",packagePathName,deployId,release,timestamp));
		
		EsfUUID selectedLibraryId = (EsfUUID)librarySelect.getValue();
		defaultLibrary = Library.Manager.getById(selectedLibraryId);
		if ( defaultLibrary == null ) {
			defaultLibrary = Library.Manager.getTemplate();
			errors.addWarning(vaadinUi.getMsg("PackageVersionForm.ImportView.warning.noDefaultLibrary",defaultLibrary.getPathName()));
		}
		
		duplicatedPackageVersion.importFromJDOM(packageVersionElement,errors,defaultLibrary,allLibraries);
		
		if ( ! errors.hasError() )
			errors.addSuccess(vaadinUi.getMsg("PackageVersionForm.ImportView.info.completed"));
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return "";
	}
	
	@Override
	public void initView() {
    	buildUploadLayout();
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
}