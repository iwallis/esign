// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.DropDownVersionOption;
import com.esignforms.open.prog.Library;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.LibraryDropDownEsfNameValidator;
import com.vaadin.data.Buffered;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.themes.Reindeer;

/**
 * The DocumentDowndownForm is used to set the values on the drop down of a document version.
 * @author Yozons Inc.
 */
public class DocumentDropdownForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -6050121659626899571L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DocumentDropdownForm.class);
	
	Button saveButton;
	Button cancelButton;
	Button deleteButton;
	
	final DocumentDropdownForm thisForm;
	LibDocumentVersionBean docVerBean;
	DocumentVersion.DocumentDropdown documentDropdown;
	
	ConfirmDiscardFormChangesWindow parentWindow;
    GridLayout layout;
    
    final DocumentVersion duplicateDocumentVersion;

    LibraryDropDownEsfNameValidator esfnameValidator;
    
	Label dropdownId;
	Label dropdownVersionId;
	Label createdByInfo;
	Label lastUpdatedByInfo;
	Label dropdownInfo;
	
	Map<Object,List<Field<?>>> optionInfoListFields = new TreeMap<Object,List<Field<?>>>();
	OptionInfoList optionInfoList;
	boolean optionInfoListChanged;
	HorizontalLayout optionInfoButtonLayout;
	Button optionInfoListAddButton;
	Button optionInfoListRemoveButton;

	// we use this to rollback since we can't just rely on commit/discard as we need to see the newly uploaded image
	// on upload, not just after clicking OK/Save.
	DropDownVersion origDropdownVersion;
	                              
    

    public DocumentDropdownForm(Library library, LibDocumentVersionBean docVerBean, DocumentVersion.DocumentDropdown documentDropdown) {
    	setStyleName("LibDocumentVersionPageViewDocumentDropdownForm");
    	this.thisForm = this;
    	this.docVerBean = docVerBean;
    	this.documentDropdown = documentDropdown; 
		this.duplicateDocumentVersion = docVerBean.duplicateDocumentVersion();
		this.origDropdownVersion = documentDropdown.dropdownVersion.duplicate();
     }
    
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
			EsfName esfName = new EsfName((String)getField("esfName").getValue());
			documentDropdown.dropdown.setEsfName(esfName);
			
			String description = (String)getField("description").getValue();
			documentDropdown.dropdown.setDescription(description);
			
			String comments = (String)getField("comments").getValue();
			documentDropdown.dropdown.setComments(comments);
			
			boolean allowMultiSelection = (Boolean)getField("allowMultiSelection").getValue();
			documentDropdown.dropdownVersion.setAllowMultiSelection(allowMultiSelection);
			
			if ( documentDropdown.dropdown.doInsert() )
				duplicateDocumentVersion.addDocumentDropdown(documentDropdown);
						
			parentWindow.close();
    	} else if ( source == optionInfoListAddButton ) {
        	commitFieldsToTableOnly();
        	optionInfoList.addNewOptionInfo();
        	optionInfoList.setPageLength();
    	} else if ( source == optionInfoListRemoveButton ) {
        	commitFieldsToTableOnly();
        	optionInfoList.removeSelectedOptionInfo();
        	optionInfoList.setPageLength();
        } else if ( source == cancelButton ) {
    		discard();
    		documentDropdown.dropdownVersion = origDropdownVersion;
    		DropDownVersion.Manager.replaceInCache(origDropdownVersion);
			parentWindow.close();
        } else if ( source == deleteButton ) {
        	discard();
        	duplicateDocumentVersion.removeDocumentDropdown(documentDropdown);
        	parentWindow.close();
        }
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	saveButton.setVisible(!readOnly);
    	deleteButton.setVisible(!readOnly);
    	
		optionInfoList.setDragMode( readOnly ? TableDragMode.NONE : TableDragMode.ROW);
    	optionInfoList.setReadOnly(readOnly);
    	optionInfoList.setEditable(!readOnly);
		optionInfoButtonLayout.setVisible(!readOnly);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
		setupForm();
	}
	
	void setupForm() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		PropertysetItem item = new PropertysetItem();
		item.addItemProperty("esfName", new ObjectProperty<EsfName>(documentDropdown.dropdown.getEsfName()));
		item.addItemProperty("description", new ObjectProperty<String>(documentDropdown.dropdown.getDescription(),String.class));
		item.addItemProperty("comments", new ObjectProperty<String>(documentDropdown.dropdown.getComments(),String.class));
		item.addItemProperty("allowMultiSelection", new ObjectProperty<Boolean>(documentDropdown.dropdownVersion.isAllowMultiSelection(),Boolean.class));
		setItemDataSource(item);
		
		dropdownId.setValue( vaadinUi.getMsg("LibDocumentVersionPageView.DropdownForm.label.dropdownId",documentDropdown.dropdown.getId()) );
		dropdownVersionId.setValue( vaadinUi.getMsg("LibDocumentVersionPageView.DropdownForm.label.id",documentDropdown.dropdownVersion.getId()) );		

		String createdBy = vaadinUi.getPrettyCode().userDisplayName(documentDropdown.dropdownVersion.getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("LibDocumentVersionPageView.DropdownForm.label.createdBy",documentDropdown.dropdownVersion.getCreatedTimestamp().toLogString(vaadinUi.getUser()),createdBy) );
		
		if ( documentDropdown.dropdownVersion.doInsert() ) {
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibDocumentVersionPageView.DropdownForm.label.lastUpdated.whenNew") );
			lastUpdatedByInfo.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(documentDropdown.dropdownVersion.getLastUpdatedByUserId());
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibDocumentVersionPageView.DropdownForm.label.lastUpdated",documentDropdown.dropdownVersion.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser()),lastUpdatedBy) );
			lastUpdatedByInfo.setIcon(null);
		}
		
		TextField esfname = (TextField)getField("esfName");
		if ( esfnameValidator != null ) esfname.removeValidator(esfnameValidator);
		esfnameValidator = new LibraryDropDownEsfNameValidator(duplicateDocumentVersion,documentDropdown.dropdown);
		esfname.addValidator(esfnameValidator);
		if ( documentDropdown.dropdownVersion.doInsert() ) {
			esfname.selectAll();
			esfname.focus();
		}

		optionInfoList.setupOptionList(documentDropdown.dropdownVersion);
		optionInfoList.setPageLength();
		optionInfoList.setCaption(vaadinUi.getMsg("LibDropDownVersionForm.optionList.label"));
		optionInfoList.setVisible(true);
		optionInfoButtonLayout.setVisible(true);
	}
	
	public void setParentWindow(ConfirmDiscardFormChangesWindow parentWindow) {
		this.parentWindow = parentWindow;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void initView() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	
    	// Setup layout
    	layout = new GridLayout(2,11);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.5f);
    	layout.setColumnExpandRatio(1, 0.5f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
		optionInfoList = new OptionInfoList();
		optionInfoList.initializeDND();
		layout.addComponent(optionInfoList,0,4,1,4);
		
		optionInfoButtonLayout = new HorizontalLayout();
		optionInfoButtonLayout.setStyleName("optionButtons");
		optionInfoButtonLayout.setMargin(false);
		optionInfoButtonLayout.setSpacing(true);
		optionInfoListAddButton = new Button(vaadinUi.getMsg("LibDropDownVersionForm.button.addOption.label"), (ClickListener)this);
		optionInfoListAddButton.setStyleName(Reindeer.BUTTON_SMALL);
		optionInfoButtonLayout.addComponent(optionInfoListAddButton);
		optionInfoListRemoveButton = new Button(vaadinUi.getMsg("LibDropDownVersionForm.button.removeOption.label"), (ClickListener)this);
		optionInfoListRemoveButton.setStyleName(Reindeer.BUTTON_SMALL);
		optionInfoButtonLayout.addComponent(optionInfoListRemoveButton);
		layout.addComponent(optionInfoButtonLayout,0,5,1,5);
				
		dropdownInfo = new Label();
		dropdownInfo.setStyleName("dropdownInfo");
		dropdownInfo.setContentMode(ContentMode.TEXT);
		layout.addComponent(dropdownInfo,0,6,1,6);
		
		lastUpdatedByInfo = new Label();
		lastUpdatedByInfo.setContentMode(ContentMode.TEXT);
		lastUpdatedByInfo.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedByInfo,0,7,1,7);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,8,1,8);		

		dropdownId = new Label();
		dropdownId.setContentMode(ContentMode.TEXT);
		dropdownId.setStyleName("smallInfo");
		layout.addComponent(dropdownId,0,9,1,9);

		dropdownVersionId = new Label();
		dropdownVersionId.setContentMode(ContentMode.TEXT);
		dropdownVersionId.setStyleName("smallInfo");
		layout.addComponent(dropdownVersionId,0,10,1,10);

    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(true);
    	footer.setMargin(true);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
		footer.addComponent(deleteButton);

		setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -3117057794007558086L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				
				if (propertyId.equals("allowMultiSelection")) {
					CheckBox cb = new CheckBox(vaadinUi.getMsg("LibDropDownVersionForm.allowMultiSelection.label"));
					cb.setImmediate(false);
					cb.setDescription(vaadinUi.getMsg("LibDropDownVersionForm.allowMultiSelection.tooltip"));
					return cb;
				}

				if ( propertyId.equals("comments") ) {
	            	TextArea ta = new TextArea();
	            	ta.setWidth(100, Unit.PERCENTAGE);
	            	ta.setNullRepresentation("");
	            	ta.setRows(3);
	            	ta.setInputPrompt(vaadinUi.getMsg("inputPrompt.comments"));
	            	ta.setCaption(vaadinUi.getMsg("caption.comments"));
	            	ta.setDescription(vaadinUi.getMsg("tooltip.comments"));
	            	return ta;
				}
				
				Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("esfName")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setCaption(vaadinUi.getMsg("caption.esfName"));
                	tf.setDescription(vaadinUi.getMsg("LibDocumentVersionPageView.DropdownForm.esfname.tooltip"));
                	tf.setConversionError(vaadinUi.getMsg("validator.esfname.message"));
                } else if ( propertyId.equals("description") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("caption.description"));
                    tf.setInputPrompt(vaadinUi.getMsg("inputPrompt.description"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.description"));
                }
                
                return field;
    	    }
    	 });

    	_logger.debug("Form created");
	}

	@Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("esfName")) {
        	layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("description")) {
        	layout.addComponent(field, 0, 1, 1, 1);
        } else if (propertyId.equals("comments")) {
        	layout.addComponent(field, 0, 2, 1, 2);
        } else if (propertyId.equals("allowMultiSelection") ) {
        	layout.addComponent(field, 0, 3, 1, 3);
        }
    }

    private void commitFieldsToTableOnly() {
    	// Only do this if we have something in our table since emptying a table will not have cleared our fields list.
    	try {
        	if ( optionInfoList.getNumOptionInfo() > 0 ) {
        		for( List<Field<?>> fList : optionInfoListFields.values() ) {
        			for( Field<?> f : fList ) {
        				f.commit();
        			}
        		}
        	}
    	} catch( Exception e ) {
    		_logger.error("commitFieldsToTableOnly()",e);
    	}
    }
    
    @Override
    public void commit() throws Buffered.SourceException {
    	super.commit();
    	
    	commitFieldsToTableOnly();
    	
    	// Let's save whatever we have now for our options
    	optionInfoList.saveOptionsListToBean(documentDropdown.dropdownVersion);
    	optionInfoListChanged = false;
    }

    @Override
    public void discard() throws Buffered.SourceException {
    	super.discard();
    	optionInfoList.setupOptionList(documentDropdown.dropdownVersion);
    	optionInfoListChanged = false;
    }

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		if ( isModified() || optionInfoListChanged ) {
			return true;
		}
		
		if ( optionInfoList != null && optionInfoList.getNumOptionInfo() > 0 ) {
    		for( List<Field<?>> fList : optionInfoListFields.values() ) {
    			for( Field<?> f : fList ) {
    				if ( f.isModified() ) return true;
    			}
    		}
		}

		return false;
	}
	
	
	   //*************************  OptionInfoList is our table of option-values ******************************
		public class OptionInfoContainer extends BeanItemContainer<OptionInfo> implements Serializable {
			private static final long serialVersionUID = 637145975048177369L;

			public OptionInfoContainer() {
				super(OptionInfo.class);
			}

			public void setOptions(DropDownVersion dropdownVersion) {
			   	optionInfoListFields.clear();
				super.removeAllItems();
				
		    	for (DropDownVersionOption option : dropdownVersion.getOptions()) {
		    		OptionInfo oi = new OptionInfo(option.getListOrder(), option.getOption(), option.getValue());
		    	    addItem(oi);
		    	}
			}
		}
		
		
		public class OptionInfo implements java.io.Serializable, Comparable<OptionInfo> {
			private static final long serialVersionUID = -7707882607734729114L;

			int order;
			String label;
			String value;
			
			public OptionInfo(int order, String label, String value) {
				this.order = order;
				this.label = label;
				this.value = value;
			}
			
			public int getOrder() {
				return order;
			}
			public void setOrder(int v) {
				order = v;
			}

			public String getLabel() {
				return label;
			}
			public void setLabel(String v) {
				label = v;
			}
			
			public String getValue() {
				return value;
			}
			public void setValue(String v) {
				value = v;
			}
			
			public int hashCode() {
				return order;
			}

			@Override
			public int compareTo(OptionInfo o) {
				if ( o == null )
					return -1;
				return order - o.order;
			}
		}
	    
		class OptionInfoList extends Table {
			private static final long serialVersionUID = 4250553640840294380L;

			private OptionInfoContainer optionInfoContainer;
			
			public OptionInfoList() {
				super();
				addStyleName("optionInfoList");
				setWidth(100, Unit.PERCENTAGE);
				
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				optionInfoContainer = new OptionInfoContainer();
				setContainerDataSource(optionInfoContainer);
				setVisibleColumns( (Object[])vaadinUi.getStringArray("LibDropDownVersionForm.optionInfoList.columnProperties") );
				setColumnHeaders( vaadinUi.getStringArray("LibDropDownVersionForm.optionInfoList.columnHeaders") );
				setColumnAlignment("order", Align.CENTER);
				setColumnExpandRatio("order", 0f);
				setColumnExpandRatio("label", 0.25f);
				setColumnExpandRatio("value", 0.75f);
				setSortEnabled(false);
				setNullSelectionAllowed(true);
				setSelectable(true);
				setImmediate(true);
				
				setTableFieldFactory(new DefaultFieldFactory() {
					private static final long serialVersionUID = -6890191656526545684L;

					public Field<?> createField(Container container, Object itemId, Object propertyId, Component uiContext) {
						
				        if ( propertyId.equals("order") ) {
							if ( optionInfoListFields.containsKey(itemId) ) { // If I already have the specified itemId in my map and this is the first property in our table, let's clear it and assume we're building it again
								optionInfoListFields.clear(); 
							}
							return null;
				        }

				        List<Field<?>> fieldList = optionInfoListFields.get(itemId);
				        if ( fieldList == null ) fieldList = new LinkedList<Field<?>>();

				        Field<?> f = super.createField(container, itemId, propertyId, uiContext);
				        f.setBuffered(true);
				        f.setWidth(100,Unit.PERCENTAGE);
				        fieldList.add(f);
				        optionInfoListFields.put(itemId,fieldList);
				        return f;
				    }
				});
			}
			
		    @Override
		    public void setReadOnly(boolean readOnly) {
		        super.setReadOnly(readOnly); 
		    	setDragMode( readOnly ? TableDragMode.NONE : TableDragMode.ROW);
		    	setEditable(!readOnly);
		    }
			
			public void initializeDND() {
				setDropHandler(new DropHandler() {
					private static final long serialVersionUID = -6196788173493117633L;

					public void drop(DragAndDropEvent dropEvent) {
		                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
		                
		                OptionInfo sourceItemId = (OptionInfo)t.getItemId();

		                AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails)dropEvent.getTargetDetails());
		                OptionInfo targetItemId = (OptionInfo)dropData.getItemIdOver();
			                
		                // No move if source and target are the same, or the drop is not around a given target (like above/below the list)
		                if ( sourceItemId == targetItemId || targetItemId == null )
		                	return;
			                
		                commitFieldsToTableOnly();

		                // Let's remove the source of the drag so we can add it back where requested...
			            optionInfoContainer.removeItem(sourceItemId);
			            if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
			            	optionInfoContainer.addItemAfter(targetItemId,sourceItemId);
			            } else {
			            	Object prevItemId = optionInfoContainer.prevItemId(targetItemId);
			            	optionInfoContainer.addItemAfter(prevItemId, sourceItemId);
			            }
			                
			            optionInfoListChanged = true;
		            }

		            public AcceptCriterion getAcceptCriterion() {
		                return new And(SourceIsTarget.get(), new Not(AbstractSelect.VerticalLocationIs.MIDDLE)); // dragging within the table
		            }
		        });
				
			} // initializeDND
			
		    void setupOptionList(DropDownVersion dropdownVersion) {
		    	optionInfoContainer.setOptions(dropdownVersion);
		    }
		    
		    void saveOptionsListToBean(DropDownVersion dropDownVersion) {
		    	if ( dropDownVersion == null ) return;
		    	
		    	LinkedList<DropDownVersionOption> list = new LinkedList<DropDownVersionOption>();
		    	
		    	for( OptionInfo oi : optionInfoContainer.getItemIds() ) {
		    		DropDownVersionOption option = DropDownVersionOption.Manager.createNew(dropDownVersion, (short)oi.getOrder());
		    		option.setOption(oi.getLabel());
		    		option.setValue(oi.getValue());
		    		list.add(option);
		    	}
		    	
		    	if ( list.size() == 0 ) {
		    		list.add( DropDownVersionOption.Manager.createNew(dropDownVersion, (short)1));
		    	}
		    	
		    	dropDownVersion.setOptions(list);
		    }
		    
		    void addNewOptionInfo() {
		    	int maxOrder = 1;
		    	for( OptionInfo oi : optionInfoContainer.getItemIds() ) {
		    		if ( oi.getOrder() > maxOrder )
		    			maxOrder = oi.getOrder();
		    	}
		    	++maxOrder;
		    	
		    	OptionInfo oi = new OptionInfo(maxOrder, "Label", "Value");
		    	optionInfoContainer.addItem(oi);
		    	optionInfoListChanged = true;
		    }
		    
			public void removeSelectedOptionInfo() {
				List<OptionInfo> optionsToRemove = new LinkedList<OptionInfo>();
				List<OptionInfo> optionInfos = optionInfoContainer.getItemIds();
		    	for( OptionInfo oi : optionInfos ) {
		    		if ( optionInfoList.isSelected(oi) ) {
		    			optionsToRemove.add(oi);
		    		}
		    	}
		    	for( OptionInfo oi : optionsToRemove ) {
		    		optionInfoContainer.removeItem(oi);
		    		optionInfoListChanged = true;
		    	}
			}
			
		    int getNumOptionInfo() {
		    	return optionInfoContainer.size();
		    }
		    
		    void setPageLength() {
				setPageLength( getNumOptionInfo() );    	    
		    }
			
		} // OptionInfoList

}