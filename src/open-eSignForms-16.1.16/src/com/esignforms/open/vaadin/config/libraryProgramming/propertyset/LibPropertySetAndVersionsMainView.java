// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.propertyset;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.PropertySet;
import com.esignforms.open.prog.PropertySetInfo;
import com.esignforms.open.prog.PropertySetVersionInfo;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

/**
 * LibPropertySetAndVersionsMainView is a splitpanel that contains the propertyset view in the left and the propertyset version view on the right.
 * 
 * Each of those views is itself a splitpanel, containing a table/list on the top to select items, with a form on the bottom to view/edit.
 * 
 * When a new propertyset is selected in the left view, we propagate that to the right view so it can sync the versions with the selected propertyset.
 * 
 * @author Yozons
 *
 */
public class LibPropertySetAndVersionsMainView extends HorizontalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = -3296878800009309949L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibPropertySetAndVersionsMainView.class);

	// 10/26/2015 - Property sets can belong to a Library or a User, but not both.
	EsfUUID containerId;
	User user;
	
    Library library;
    final LibPropertySetAndVersionsMainView thisView;
	
    VerticalSplitPanel propertysetSplitPanel;
    LibPropertySetBeanContainer propertysetContainer;
    LibPropertySetList propertysetList;
    LibPropertySetForm propertysetForm;
	
    VerticalSplitPanel propertysetVerSplitPanel;
    LibPropertySetVersionBeanContainer propertysetVerContainer;
	LibPropertySetVersionList propertysetVerList;
	LibPropertySetVersionForm propertysetVerForm;

	public LibPropertySetAndVersionsMainView() { // traditional property sets for a library
		super(); 
		thisView = this;
        user = null;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	public LibPropertySetAndVersionsMainView(User forUser) { // property sets for a user
		super(); 
		thisView = this;
		user = forUser;
		library = null;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			if ( ! isUserPropertySet() ) // If it's not for a pre-created User property sets view, we assume it's for the library.
				library = Library.Manager.getById(containerId, vaadinUi.getUser());
			
			propertysetContainer = isLibraryPropertySet() ? new LibPropertySetBeanContainer(library) : new LibPropertySetBeanContainer(user);
			propertysetVerContainer = new LibPropertySetVersionBeanContainer(this); // Load contents when a propertyset is selected from our propertysetContainer
			
			propertysetList = new LibPropertySetList(this,propertysetContainer);
		    propertysetForm = new LibPropertySetForm(this,propertysetContainer);

			propertysetSplitPanel = new VerticalSplitPanel();
			propertysetSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			propertysetSplitPanel.setSplitPosition(25);
			propertysetSplitPanel.setFirstComponent(propertysetList);
			propertysetSplitPanel.setSecondComponent(propertysetForm);
		    
			propertysetVerList = new LibPropertySetVersionList(this,propertysetVerContainer);
			propertysetVerForm = new LibPropertySetVersionForm(this,propertysetVerContainer);
			
			propertysetVerSplitPanel = new VerticalSplitPanel();
			propertysetVerSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			propertysetVerSplitPanel.setSplitPosition(25);
			propertysetVerSplitPanel.setFirstComponent(propertysetVerList);
			propertysetVerSplitPanel.setSecondComponent(propertysetVerForm);

			setFirstComponent(propertysetSplitPanel);
			setSecondComponent(propertysetVerSplitPanel);
			setSplitPosition(35);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("PropertySet and versions main view exception", e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	private LibPropertySetBean getLibPropertySetBean(Item propertysetListItem) {
    	if ( propertysetListItem == null )
    		return null;
		BeanItem<LibPropertySetBean> bi = (BeanItem<LibPropertySetBean>)propertysetListItem;
		return bi.getBean();
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our propertyset list or not
        if (property == propertysetList.getTable()) {
        	final Item item = propertysetList.getTable().getItem(propertysetList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( propertysetForm.getCurrentBean() == propertysetForm.getBean(item) ) {
    				// We've selected the same bean so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibPropertySetAndVersionsMainView.PropertySet.ConfirmDiscardChangesDialog.message", propertysetForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						thisView.discardForms();
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						propertysetList.getTable().select(propertysetForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibPropertySetAndVersionsMainView.PropertySet.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	LibPropertySetBean bean = getLibPropertySetBean(item);
                propertysetForm.setItemDataSource(item);
            	if ( bean != null ) {
                	propertysetVerContainer.reload(bean.propertyset());
                	selectFirstPropertySetVersion();
            	} else {
            		propertysetVerForm.setItemDataSource(null);
            		propertysetVerContainer.removeAllItems();
            	}
    		}
        } else if (property == propertysetVerList.getTable()) {
        	final Item item = propertysetVerList.getTable().getItem(propertysetVerList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( propertysetVerForm.getCurrentBean() == propertysetVerForm.getBean(item) ) {
    				// We've selected the same bean (version) so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibPropertySetAndVersionsMainView.PropertySet.ConfirmDiscardChangesDialog.message", propertysetForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						propertysetVerForm.discard();
						propertysetVerForm.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						propertysetVerList.getTable().select(propertysetVerForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibPropertySetAndVersionsMainView.PropertySet.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	propertysetVerForm.setItemDataSource(item);
    		}
        }
        else {
        	_logger.debug("valueChange() - no list event: " + event.toString());
        }
	}
	

	@Override
	public void detach() {
		/*
		for( Object o : list.getTable().getVisibleColumns() ) {
			_logger.debug("detach() - list visible columns[]: " + o); // These are string property ids of the column names like email, status, lastLoginTimestamp
		}
		*/
		super.detach();
	}
	
	public void selectPropertySet(LibPropertySetBean bean) {
		unselectAllPropertySets();
		propertysetList.getTable().select(bean);
		propertysetList.getTable().setCurrentPageFirstItemId(bean);
		if ( bean == null ) {
			propertysetVerForm.setItemDataSource(null);
		}
	}
	
	public void unselectAllPropertySets() {
		propertysetList.getTable().setValue(null);
		propertysetForm.setItemDataSource(null);
	}
	
	public void selectPropertySetVersion(LibPropertySetVersionBean bean) {
		unselectAllPropertySetVersions();
		propertysetVerList.getTable().select(bean);
		propertysetVerList.getTable().setCurrentPageFirstItemId(bean);
	}

	public void discardForms() {
		propertysetForm.discard();
		propertysetVerForm.discard();
		propertysetForm.setItemDataSource(null);
		propertysetVerForm.setItemDataSource(null);
	}

	
	public void selectFirstPropertySetVersion() {
		if ( propertysetVerContainer.size() > 0 ) {
			selectPropertySetVersion(propertysetVerContainer.getIdByIndex(0));
		} else {
			unselectAllPropertySetVersions(); 
		}
	}
	
	public void unselectAllPropertySetVersions() {
		propertysetVerList.getTable().setValue(null);
	}
	
	public EsfUUID getContainerId() {
		return containerId;
	}
	
	public User getUser() {
		return user;
	}
	public boolean isUserPropertySet() {
		return user != null;
	}

	public Library getLibrary() {
		return library;
	}
	public boolean isLibraryPropertySet() {
		return library != null;
	}
	
	public LibPropertySetBean getLibPropertySetBean() {
		return propertysetForm == null ? null : propertysetForm.getCurrentBean();
	}
	
	public LibPropertySetVersionBean getLibPropertySetVersionBean() {
		return propertysetVerForm == null ? null : propertysetVerForm.getCurrentBean();
	}
	
	public String getEsfNameWithVersion() {
		LibPropertySetBean propertysetBean = getLibPropertySetBean();
		LibPropertySetVersionBean propertysetVerBean = getLibPropertySetVersionBean();
		return propertysetVerBean == null ? "?? [?]" : propertysetBean.getEsfName() + " [" + propertysetVerBean.getVersion() + "]";
	}
	
	public Integer getProductionVersion() {
		LibPropertySetBean propertysetBean = getLibPropertySetBean();
		return propertysetBean == null ? 0 : propertysetBean.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		LibPropertySetBean propertysetBean = getLibPropertySetBean();
		return propertysetBean == null ? 0 : propertysetBean.getTestVersion();
	}

	public boolean hasTestVersion() {
		LibPropertySetBean propertysetBean = getLibPropertySetBean();
		return propertysetBean == null ? false : propertysetBean.propertyset().hasTestVersion();
	}
	
    public boolean isProductionVersion(PropertySetVersionInfo propertysetVer)
    {
    	LibPropertySetBean propertysetBean = getLibPropertySetBean();
    	return propertysetBean != null && propertysetVer != null && propertysetVer.getVersion() == propertysetBean.getProductionVersion();
    }

    public boolean isTestVersion(PropertySetVersionInfo propertysetVer)
    {
    	LibPropertySetBean propertysetBean = getLibPropertySetBean();
    	return propertysetBean != null && propertysetVer != null && propertysetVer.getVersion() > propertysetBean.getProductionVersion();
    }

    // "Current" here means latest production or test version.
    public boolean isCurrentVersion(PropertySetVersionInfo propertysetVer)
    {
    	LibPropertySetBean propertysetBean = getLibPropertySetBean();
    	return propertysetBean != null && propertysetVer != null && propertysetVer.getVersion() >= propertysetBean.getProductionVersion();
    }

    public boolean isOldVersion(PropertySetVersionInfo propertysetVer)
    {
    	LibPropertySetBean propertysetBean = getLibPropertySetBean();
    	return propertysetBean != null && propertysetVer != null && propertysetVer.getVersion() < propertysetBean.getProductionVersion();
    }
	
	public String getVersionLabel(PropertySetVersionInfo propertysetVer) {
		if ( isTestVersion(propertysetVer) )
			return Literals.VERSION_LABEL_TEST;
		if ( isProductionVersion(propertysetVer) )
			return Literals.VERSION_LABEL_PRODUCTION;
		return Literals.VERSION_LABEL_NOT_CURRENT;
	}
	
	public boolean hasPermViewDetails() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return isLibraryPropertySet() ? getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS) : user.canUserList(vaadinUi.getUser());
	}

	public boolean hasPermUpdate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return isLibraryPropertySet() ? getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE) : ! isReadOnly() && user.canUserUpdate(vaadinUi.getUser());
	}

	public boolean hasPermCreateLike() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return isLibraryPropertySet() ? getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_CREATELIKE) : ! isReadOnly() && user.canUserUpdate(vaadinUi.getUser());
	}

	public boolean hasPermDelete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return isLibraryPropertySet() ? getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_DELETE) : ! isReadOnly() && user.canUserUpdate(vaadinUi.getUser());
	}
	
	public boolean cancelChanges() {
		propertysetForm.discard();
		propertysetVerForm.discard();
		propertysetForm.setReadOnly(true);
		propertysetVerForm.setReadOnly(true);
		return true;
	}
	
	public boolean enterEditMode() {
		propertysetForm.setReadOnly(false);
		propertysetVerForm.setReadOnly(false);
		return true;
	}
	
	public boolean save() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
		if ( ! propertysetForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		if ( ! propertysetVerForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		propertysetForm.commit();
		propertysetVerForm.commit();
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			// If we're saving a new test version, we'll want to bump our propertyset to show this as the new test version
			LibPropertySetBean currPropertySet = getLibPropertySetBean();
			LibPropertySetVersionBean currPropertySetVer = getLibPropertySetVersionBean();
			if ( currPropertySetVer.propertysetVersion().getVersion() > currPropertySet.propertyset().getTestVersion() )
				currPropertySet.propertyset().bumpTestVersion();
			
			LibPropertySetBean savedPropertySet = propertysetForm.save(con);
			if ( savedPropertySet == null )  {
				con.rollback();
				return false;
			}
			
			LibPropertySetVersionBean savedDocVer = propertysetVerForm.save(con);
			if ( savedDocVer == null )  {
				con.rollback();
				return false;
			}
			
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Saved property set " + savedPropertySet.getEsfName() + " version " + savedDocVer.getVersion() + ". PropertySetVersionId: " + savedDocVer.getId() + "; status: " + savedPropertySet.getStatus());
			}

			con.commit();
	    		
			propertysetContainer.refresh();  // refresh our list after a change
			selectPropertySet(savedPropertySet);
			selectPropertySetVersion(savedDocVer);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean createNewPropertySet() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		PropertySetInfo propertysetInfo = PropertySetInfo.Manager.createNew(getContainerId());
		LibPropertySetBean propertysetBean = new LibPropertySetBean(propertysetInfo);
		BeanItem<LibPropertySetBean> propertysetItem = new BeanItem<LibPropertySetBean>(propertysetBean);
        propertysetForm.setItemDataSource(propertysetItem);
		
		PropertySetVersionInfo propertysetVerInfo = PropertySetVersionInfo.Manager.createNew(PropertySet.Manager.getById(propertysetInfo.getId()),vaadinUi.getUser());
		LibPropertySetVersionBean propertysetVerBean = new LibPropertySetVersionBean(propertysetVerInfo,getVersionLabel(propertysetVerInfo));
		BeanItem<LibPropertySetVersionBean> propertysetVerItem = new BeanItem<LibPropertySetVersionBean>(propertysetVerBean);
        propertysetVerForm.setItemDataSource(propertysetVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean createLikePropertySet() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibPropertySetBean likeDocBean = getLibPropertySetBean();
		LibPropertySetVersionBean likeDocVerBean = getLibPropertySetVersionBean();
		
    	LibPropertySetBean createdDocBean = likeDocBean.createLike();
		BeanItem<LibPropertySetBean> createdDocItem = new BeanItem<LibPropertySetBean>(createdDocBean);
        propertysetForm.setItemDataSource(createdDocItem);

    	PropertySetVersionInfo createdPropertySetVerInfo = PropertySetVersionInfo.Manager.createLike(PropertySet.Manager.getById(createdDocBean.propertysetInfo().getId()), likeDocVerBean.propertysetVersion(), vaadinUi.getUser());
    	LibPropertySetVersionBean createdPropertySetVerBean = new LibPropertySetVersionBean(createdPropertySetVerInfo,Literals.VERSION_LABEL_TEST); // The new propertyset and version will always be Test.
		BeanItem<LibPropertySetVersionBean> createdPropertySetVerItem = new BeanItem<LibPropertySetVersionBean>(createdPropertySetVerBean);       
        propertysetVerForm.setItemDataSource(createdPropertySetVerItem);
        
        setReadOnly(false);
		return true;
	}

	public boolean createNextVersion() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibPropertySetBean currPropertySetBean = getLibPropertySetBean();
		LibPropertySetVersionBean currPropertySetVerBean = getLibPropertySetVersionBean();
		
        propertysetForm.setReadOnly(!hasPermUpdate()); // should have permission since doing a create next version...

    	PropertySetVersionInfo nextPropertySetVerInfo = PropertySetVersionInfo.Manager.createLike(currPropertySetBean.propertyset(), currPropertySetVerBean.propertysetVersion(), vaadinUi.getUser());
    	LibPropertySetVersionBean nextPropertySetVerBean = new LibPropertySetVersionBean(nextPropertySetVerInfo,Literals.VERSION_LABEL_TEST); // The new version will always be Test.
		BeanItem<LibPropertySetVersionBean> nextPropertySetVerItem = new BeanItem<LibPropertySetVersionBean>(nextPropertySetVerBean);
        propertysetVerForm.setItemDataSource(nextPropertySetVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean promoteTestToProductionVersion() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibPropertySetVersionForm.versionChange.TestToProd.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibPropertySetBean propertysetBean = getLibPropertySetBean();
			LibPropertySetVersionBean propertysetVerBean = getLibPropertySetVersionBean();
			
			propertysetBean.propertyset().promoteTestVersionToProduction();
			
			if ( ! propertysetBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				vaadinUi.getUser().logConfigChange(con, "Moved propertyset " + propertysetBean.getEsfName() + " version " + propertysetVerBean.getVersion() + " into Production status. PropertySetVersionId: " + propertysetVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + vaadinUi.getUser().getFullDisplayName() + " moved propertyset " + propertysetBean.getEsfName() + " version " + propertysetVerBean.getVersion() + " into Production status. PropertySetVersionId: " + propertysetVerBean.getId());
			}
			
			con.commit();
			propertysetContainer.refresh();  // refresh our list after a change
			selectPropertySet(propertysetBean);
			selectPropertySetVersion(propertysetVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibPropertySetVersionForm.versionChange.TestToProd.success.message",propertysetBean.getEsfName(),propertysetVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean revertProductionVersionBackToTest() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibPropertySetVersionForm.versionChange.revertProdToTest.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibPropertySetBean propertysetBean = getLibPropertySetBean();
			LibPropertySetVersionBean propertysetVerBean = getLibPropertySetVersionBean();
			
			propertysetBean.propertyset().revertProductionVersionBackToTest();
			
			if ( ! propertysetBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved Production propertyset " + propertysetBean.getEsfName() + " version " + propertysetVerBean.getVersion() + " back into Test status. PropertySetVersionId: " + propertysetVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved Production propertyset " + propertysetBean.getEsfName() + " version " + propertysetVerBean.getVersion() + " back into Test status. PropertySetVersionId: " + propertysetVerBean.getId());
			}
			
			con.commit();
			propertysetContainer.refresh();  // refresh our list after a change
			selectPropertySet(propertysetBean);
			selectPropertySetVersion(propertysetVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibPropertySetVersionForm.versionChange.revertProdToTest.success.message",propertysetBean.getEsfName(),propertysetVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean delete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibPropertySetBean currPropertySet = getLibPropertySetBean();
			LibPropertySetVersionBean currPropertySetVer = getLibPropertySetVersionBean();
			
			// If there is no Production version, we'll get rid of the property set entirely, otherwise just the test version
			Errors errors = new Errors();
			if ( currPropertySet.getProductionVersion() == 0 )
				currPropertySet.propertyset().delete(con,errors,vaadinUi.getUser());
			else {
				currPropertySetVer.propertysetVersion().delete(con,errors,vaadinUi.getUser());
				currPropertySet.dropTestVersion();
				currPropertySet.save(con, errors);
			}
			
			if ( errors.hasError() ) {
				vaadinUi.show(errors);
				con.rollback();
				return false;
			}
			
			con.commit();
    		
			vaadinUi.showStatus(vaadinUi.getMsg("LibPropertySetVersionForm.button.delete.successMessage",currPropertySet.getEsfName(),currPropertySetVer.getVersion()));

			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Deleted propertyset " + currPropertySet.getEsfName() + " version " + currPropertySetVer.getVersion() + ". PropertySetVersionId: " + currPropertySetVer.getId() + "; status: " + currPropertySet.getStatus());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted propertyset " + currPropertySet.getEsfName() + " version " + currPropertySetVer.getVersion() + ". PropertySetVersionId: " + currPropertySetVer.getId() + "; status: " + currPropertySet.getStatus());
			}

			propertysetContainer.refresh();  // refresh our list after a change
			selectPropertySet(null);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		_logger.debug("activateView() - params: " + params);
		containerId = new EsfUUID(params);
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return propertysetForm.isDirty() ? propertysetForm.checkDirty() : propertysetVerForm.checkDirty();
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return propertysetForm.isDirty() || propertysetVerForm.isDirty();
	}
}