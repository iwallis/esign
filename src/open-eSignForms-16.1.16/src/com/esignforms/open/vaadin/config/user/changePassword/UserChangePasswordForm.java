// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.user.changePassword;

import java.util.Arrays;

import com.esignforms.open.Errors;
import com.esignforms.open.admin.PasswordManager;
import com.esignforms.open.config.Literals;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.user.User;
import com.esignforms.open.user.UserLoginInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.TextField;

/**
 * The UserChangePasswordForm is a simple Form used to change a logged-in user's password. The user must first enter their existing
 * password before they are offered the change to change it.
 * @author Yozons Inc.
 */
public class UserChangePasswordForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -4731321649686691952L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(UserChangePasswordForm.class);
	
	String question = "";
	
	enum SHOW_VIEW { CURRENT_PASSWORD, NEW_PASSWORD, DONE };
	
	Button checkPasswordButton;
	Button changePasswordButton;
	Button cancelButton;
	SHOW_VIEW currentView;
	
    GridLayout layout;
    
    public UserChangePasswordForm() {
    	setStyleName("UserChangePasswordForm");
    }
    
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == checkPasswordButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
    			
			Field currentPasswordField = this.getField("currentPassword");
			if ( currentPasswordField == null )
				return;
			
    		// Let's see if the password entered is valid.
			Errors errors = new Errors();
			
			PasswordManager pm = vaadinUi.getEsfapp().getPasswordManager();
			
			try {
				UserLoginInfo loginInfo = pm.login(vaadinUi.getUser(), (String)currentPasswordField.getValue(), vaadinUi.getEsfapp().getExternalContextPath(), vaadinUi.getRequestHostIp());
				question = loginInfo.getForgottenQuestion();
				vaadinUi.showStatus( vaadinUi.getMsg("UserChangePasswordForm.currentPassword.validated") );
	    		currentView = SHOW_VIEW.NEW_PASSWORD;
	    		showCorrectView();
			} catch ( EsfException e ) {
				errors.addError(e.getMessage());
				vaadinUi.show(errors);
			}
        } else if ( source == changePasswordButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
    			
			Field newPasswordField = this.getField("newPassword");
			Field newPasswordRepeatField = this.getField("newPasswordRepeat");
			Field newQuestionField = this.getField("newQuestion");
			Field newAnswerField = this.getField("newAnswer");
			if ( newPasswordField == null || newPasswordRepeatField == null || newQuestionField == null || newAnswerField == null )
				return;
			
    		// Let's see if the password entered is valid.
			Errors errors = new Errors();
			
			String newPassword = (String)newPasswordField.getValue();
			String newPasswordRepeat = (String)newPasswordRepeatField.getValue();
			if ( ! newPassword.equals(newPasswordRepeat) ) {
				errors.addError(vaadinUi.getMsg("UserChangePasswordForm.newPassword.donotmatch"));
				vaadinUi.show(errors);
				return;
			}
			
			String newQuestion = (String)newQuestionField.getValue();
			String newAnswer = (String)newAnswerField.getValue();
			
			PasswordManager pm = vaadinUi.getEsfapp().getPasswordManager();
			
			try {
				User user = vaadinUi.getUser();
				if ( user != null ) {
					pm.checkAllowedPassword(user.getId(), newPassword);
					pm.checkAllowedForgotPasswordAnswer(user.getId(), newAnswer);
					pm.setPassword(user, newPassword, newQuestion, newAnswer, vaadinUi.getEsfapp().getExternalContextPath(), vaadinUi.getRequestHostIp());
		    		currentView = SHOW_VIEW.DONE;
		    		showCorrectView();
				}
			} catch ( EsfException e ) {
				errors.addError(e.getMessage());
				vaadinUi.show(errors);
			}
        } else if ( source == cancelButton ) {
    		discard();
    		vaadinUi.showStatus( vaadinUi.getMsg("UserChangePasswordForm.button.cancel.success.message") );
    		currentView = SHOW_VIEW.CURRENT_PASSWORD;
    		showCorrectView();
        }
    }
	
	@Override
    public void setItemDataSource(Item dataSource) {
		if ( currentView == SHOW_VIEW.CURRENT_PASSWORD ) {
			super.setItemDataSource(dataSource, Arrays.asList("currentPassword"));
			checkPasswordButton.setVisible(true);
			changePasswordButton.setVisible(false);
			Field<?> currentPasswordField = getField("currentPassword");
			if ( currentPasswordField != null )
				currentPasswordField.focus();
		} else if ( currentView == SHOW_VIEW.NEW_PASSWORD ) {
			super.setItemDataSource(dataSource, Arrays.asList("newPassword","newPasswordRepeat","newQuestion","newAnswer"));
			checkPasswordButton.setVisible(false);
			changePasswordButton.setVisible(true);
			Field<?> newPasswordField = getField("newPassword");
			if ( newPasswordField != null )
				newPasswordField.focus();
		} else {
			super.setItemDataSource(dataSource, Arrays.asList("currentPassword"));
			checkPasswordButton.setVisible(false);
			changePasswordButton.setVisible(false);
			cancelButton.setVisible(false);
		}
    }
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		currentView = SHOW_VIEW.CURRENT_PASSWORD;
		showCorrectView();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	void showCorrectView() {
		layout.removeAllComponents();
		
		if ( currentView == SHOW_VIEW.CURRENT_PASSWORD ) {
			question = "";
			PropertysetItem item = new PropertysetItem();
			item.addItemProperty("currentPassword", new ObjectProperty("",String.class));
			setItemDataSource(item);
		} else if ( currentView == SHOW_VIEW.NEW_PASSWORD ) {
			PropertysetItem item = new PropertysetItem();
			item.addItemProperty("newPassword", new ObjectProperty("",String.class));
			item.addItemProperty("newPasswordRepeat", new ObjectProperty("",String.class));
			item.addItemProperty("newQuestion", new ObjectProperty(question,String.class));
			item.addItemProperty("newAnswer", new ObjectProperty("",String.class));
			setItemDataSource(item);
		} else {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			setItemDataSource(null);
			Label notice = new Label(vaadinUi.getMsg("UserChangePasswordForm.newPassword.success"));
			layout.addComponent(notice, 0, 0, 1, 0);
		}
	}

	@Override
	public void initView() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	layout = new GridLayout(2,2);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);

    	checkPasswordButton = new Button(vaadinUi.getMsg("UserChangePasswordForm.button.checkPassword.label"), (ClickListener)this);
    	checkPasswordButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
    	checkPasswordButton.setDescription(vaadinUi.getMsg("UserChangePasswordForm.button.checkPassword.tooltip"));
    	footer.addComponent(checkPasswordButton);

    	changePasswordButton = new Button(vaadinUi.getMsg("UserChangePasswordForm.button.changePassword.label"), (ClickListener)this);
    	changePasswordButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
    	changePasswordButton.setDescription(vaadinUi.getMsg("UserChangePasswordForm.button.changePassword.tooltip"));
    	footer.addComponent(changePasswordButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 5703169888364716131L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

                if (propertyId.equals("currentPassword")) {
                	PasswordField pf = new PasswordField(vaadinUi.getMsg("UserChangePasswordForm.currentPassword.label"));
                    pf.setDescription(vaadinUi.getMsg("UserChangePasswordForm.currentPassword.tooltip"));
                    pf.setMaxLength(vaadinUi.getEsfapp().getPasswordManager().getMaxPasswordLength());
                    pf.setWidth("50ex");
                    pf.setRequired(true);
                    pf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    return pf;
                }
				
                if (propertyId.equals("newPassword")) {
                	PasswordField pf = new PasswordField(vaadinUi.getMsg("UserChangePasswordForm.newPassword.label"));
                    pf.setDescription(vaadinUi.getMsg("UserChangePasswordForm.newPassword.tooltip"));
                    pf.setMaxLength(vaadinUi.getEsfapp().getPasswordManager().getMaxPasswordLength());
                    pf.setWidth("50ex");
                    pf.setRequired(true);
                    pf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    return pf;
                }
				
                if (propertyId.equals("newPasswordRepeat")) {
                	PasswordField pf = new PasswordField(vaadinUi.getMsg("UserChangePasswordForm.newPasswordRepeat.label"));
                    pf.setDescription(vaadinUi.getMsg("UserChangePasswordForm.newPasswordRepeat.tooltip"));
                    pf.setMaxLength(vaadinUi.getEsfapp().getPasswordManager().getMaxPasswordLength());
                    pf.setWidth("50ex");
                    pf.setRequired(true);
                    pf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    return pf;
                }
				
                if (propertyId.equals("newQuestion")) {
                	TextField tf = new TextField(vaadinUi.getMsg("UserChangePasswordForm.newQuestion.label"));
                    tf.setDescription(vaadinUi.getMsg("UserChangePasswordForm.newQuestion.tooltip"));
                    tf.setMaxLength(Literals.FORGOTTEN_PASSWORD_QUESTION_MAX_LENGTH);
                    tf.setWidth("50ex");
                    tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    return tf;
                }
				
                if (propertyId.equals("newAnswer")) {
                	TextField tf = new TextField(vaadinUi.getMsg("UserChangePasswordForm.newAnswer.label"));
                    tf.setDescription(vaadinUi.getMsg("UserChangePasswordForm.newAnswer.tooltip"));
                    tf.setMaxLength(vaadinUi.getEsfapp().getPasswordManager().getMaxForgotPasswordAnswerLength());
                    tf.setWidth("50ex");
                    tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    return tf;
                }
				
				Field field = super.createField(item, propertyId, uiContext);
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
	}
	
    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("currentPassword")) {
            layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("newPassword")) {
            layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("newPasswordRepeat")) {
            layout.addComponent(field, 1, 0);
        } else if (propertyId.equals("newQuestion")) {
            layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("newAnswer")) {
            layout.addComponent(field, 1, 1);
        }
    }

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("UserChangePasswordForm.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}