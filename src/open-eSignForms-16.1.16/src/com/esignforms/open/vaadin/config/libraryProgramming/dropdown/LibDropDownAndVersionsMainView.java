// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.dropdown;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.DropDownInfo;
import com.esignforms.open.prog.DropDownVersionInfo;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

/**
 * LibDropDownAndVersionsMainView is a splitpanel that contains the dropdown view in the left and the dropdown version view on the right.
 * 
 * Each of those views is itself a splitpanel, containing a table/list on the top to select items, with a form on the bottom to view/edit.
 * 
 * When a new dropdown is selected in the left view, we propagate that to the right view so it can sync the versions with the selected dropdown.
 * 
 * @author Yozons
 *
 */
public class LibDropDownAndVersionsMainView extends HorizontalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = 3458451925389683508L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibDropDownAndVersionsMainView.class);

    EsfUUID libraryId;
    Library library;
    final LibDropDownAndVersionsMainView thisView;
	
    VerticalSplitPanel dropdownSplitPanel;
    LibDropDownBeanContainer dropdownContainer;
    LibDropDownList dropdownList;
    LibDropDownForm dropdownForm;
	
    VerticalSplitPanel dropdownVerSplitPanel;
    LibDropDownVersionBeanContainer dropdownVerContainer;
	LibDropDownVersionList dropdownVerList;
	LibDropDownVersionForm dropdownVerForm;

	public LibDropDownAndVersionsMainView() {
		super(); 
		thisView = this;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			library = Library.Manager.getById(libraryId, vaadinUi.getUser());
			
			dropdownContainer = new LibDropDownBeanContainer(library);
			dropdownVerContainer = new LibDropDownVersionBeanContainer(this); // Load contents when a dropdown is selected from our dropdownContainer
			
			dropdownList = new LibDropDownList(this,dropdownContainer);
		    dropdownForm = new LibDropDownForm(this,dropdownContainer);

			dropdownSplitPanel = new VerticalSplitPanel();
			dropdownSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			dropdownSplitPanel.setSplitPosition(40);
			dropdownSplitPanel.setFirstComponent(dropdownList);
			dropdownSplitPanel.setSecondComponent(dropdownForm);
		    
			dropdownVerList = new LibDropDownVersionList(this,dropdownVerContainer);
			dropdownVerForm = new LibDropDownVersionForm(this);
			dropdownVerForm.initView();
			
			dropdownVerSplitPanel = new VerticalSplitPanel();
			dropdownVerSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			dropdownVerSplitPanel.setSplitPosition(40);
			dropdownVerSplitPanel.setFirstComponent(dropdownVerList);
			dropdownVerSplitPanel.setSecondComponent(dropdownVerForm);

			setFirstComponent(dropdownSplitPanel);
			setSecondComponent(dropdownVerSplitPanel);
			setSplitPosition(35);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Dropdown and versions main view exception", e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	private LibDropDownBean getLibDropDownBean(Item dropdownListItem) {
    	if ( dropdownListItem == null )
    		return null;
		BeanItem<LibDropDownBean> bi = (BeanItem<LibDropDownBean>)dropdownListItem;
		return bi.getBean();
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our dropdown list or not
        if (property == dropdownList.getTable()) {
        	final Item item = dropdownList.getTable().getItem(dropdownList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( dropdownForm.getCurrentBean() == dropdownForm.getBean(item) ) {
    				// We've selected the same bean so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibDropDownAndVersionsMainView.DropDown.ConfirmDiscardChangesDialog.message", dropdownForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						thisView.discardForms();
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						dropdownList.getTable().select(dropdownForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibDropDownAndVersionsMainView.DropDown.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	LibDropDownBean bean = getLibDropDownBean(item);
                dropdownForm.setItemDataSource(item);
            	if ( bean != null ) {
                	dropdownVerContainer.reload(bean.dropdown());
                	selectFirstDropDownVersion();
            	} else {
            		dropdownVerForm.setItemDataSource(null);
            		dropdownVerContainer.removeAllItems();
            	}
    		}
        } else if (property == dropdownVerList.getTable()) {
        	final Item item = dropdownVerList.getTable().getItem(dropdownVerList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( dropdownVerForm.getCurrentBean() == dropdownVerForm.getBean(item) ) {
    				// We've selected the same bean (version) so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibDropDownAndVersionsMainView.DropDown.ConfirmDiscardChangesDialog.message", dropdownForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						dropdownVerForm.discard();
						dropdownVerForm.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						dropdownVerList.getTable().select(dropdownVerForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibDropDownAndVersionsMainView.DropDown.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	dropdownVerForm.setItemDataSource(item);
    		}
        }
        else {
        	_logger.debug("valueChange() - no list event: " + event.toString());
        }
	}
	

	@Override
	public void detach() {
		/*
		for( Object o : list.getTable().getVisibleColumns() ) {
			_logger.debug("detach() - list visible columns[]: " + o); // These are string property ids of the column names like email, status, lastLoginTimestamp
		}
		*/
		super.detach();
	}
	
	public void selectDropDown(LibDropDownBean bean) {
		unselectAllDropDowns();
		dropdownList.getTable().select(bean);
		dropdownList.getTable().setCurrentPageFirstItemId(bean);
		if ( bean == null ) {
			dropdownVerForm.setItemDataSource(null);
		}
	}
	
	public void unselectAllDropDowns() {
		dropdownList.getTable().setValue(null);
		dropdownForm.setItemDataSource(null);
	}
	
	public void selectDropDownVersion(LibDropDownVersionBean bean) {
		unselectAllDropDownVersions();
		dropdownVerList.getTable().select(bean);
		dropdownVerList.getTable().setCurrentPageFirstItemId(bean);
	}

	public void discardForms() {
		dropdownForm.discard();
		dropdownVerForm.discard();
		dropdownForm.setItemDataSource(null);
		dropdownVerForm.setItemDataSource(null);
	}

	
	public void selectFirstDropDownVersion() {
		if ( dropdownVerContainer.size() > 0 ) {
			selectDropDownVersion(dropdownVerContainer.getIdByIndex(0));
		} else {
			unselectAllDropDownVersions(); 
		}
	}
	
	public void unselectAllDropDownVersions() {
		dropdownVerList.getTable().setValue(null);
	}

	public Library getLibrary() {
		return library;
	}
	
	public LibDropDownBean getLibDropDownBean() {
		return dropdownForm == null ? null : dropdownForm.getCurrentBean();
	}
	
	public LibDropDownVersionBean getLibDropDownVersionBean() {
		return dropdownVerForm == null ? null : dropdownVerForm.getCurrentBean();
	}
	
	public String getEsfNameWithVersion() {
		LibDropDownBean dropdownBean = getLibDropDownBean();
		LibDropDownVersionBean dropdownVerBean = getLibDropDownVersionBean();
		return dropdownVerBean == null ? "?? [?]" : dropdownBean.getEsfName() + " [" + dropdownVerBean.getVersion() + "]";
	}
	
	public Integer getProductionVersion() {
		LibDropDownBean dropdownBean = getLibDropDownBean();
		return dropdownBean == null ? 0 : dropdownBean.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		LibDropDownBean dropdownBean = getLibDropDownBean();
		return dropdownBean == null ? 0 : dropdownBean.getTestVersion();
	}

	public boolean hasTestVersion() {
		LibDropDownBean dropdownBean = getLibDropDownBean();
		return dropdownBean == null ? false : dropdownBean.dropdown().hasTestVersion();
	}
	
    public boolean isProductionVersion(DropDownVersionInfo dropdownVer)
    {
    	LibDropDownBean dropdownBean = getLibDropDownBean();
    	return dropdownBean != null && dropdownVer != null && dropdownVer.getVersion() == dropdownBean.getProductionVersion();
    }

    public boolean isTestVersion(DropDownVersionInfo dropdownVer)
    {
    	LibDropDownBean dropdownBean = getLibDropDownBean();
    	return dropdownBean != null && dropdownVer != null && dropdownVer.getVersion() > dropdownBean.getProductionVersion();
    }

    // "Current" here means latest production or test version.
    public boolean isCurrentVersion(DropDownVersionInfo dropdownVer)
    {
    	LibDropDownBean dropdownBean = getLibDropDownBean();
    	return dropdownBean != null && dropdownVer != null && dropdownVer.getVersion() >= dropdownBean.getProductionVersion();
    }

    public boolean isOldVersion(DropDownVersionInfo dropdownVer)
    {
    	LibDropDownBean dropdownBean = getLibDropDownBean();
    	return dropdownBean != null && dropdownVer != null && dropdownVer.getVersion() < dropdownBean.getProductionVersion();
    }
	
	public String getVersionLabel(DropDownVersionInfo dropdownVer) {
		if ( isTestVersion(dropdownVer) )
			return Literals.VERSION_LABEL_TEST;
		if ( isProductionVersion(dropdownVer) )
			return Literals.VERSION_LABEL_PRODUCTION;
		return Literals.VERSION_LABEL_NOT_CURRENT;
	}
	
	public boolean hasPermViewDetails() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_DELETE);
	}
	
	public boolean cancelChanges() {
		dropdownForm.discard();
		dropdownVerForm.discard();
		dropdownForm.setReadOnly(true);
		dropdownVerForm.setReadOnly(true);
		return true;
	}
	
	public boolean enterEditMode() {
		dropdownForm.setReadOnly(false);
		dropdownVerForm.setReadOnly(false);
		return true;
	}
	
	public boolean save() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
		if ( ! dropdownForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		if ( ! dropdownVerForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		dropdownForm.commit();
		try {
			dropdownVerForm.commit();
		} catch (CommitException e) {
			_logger.debug("save() - dropDownVerForm.commit()",e);
		}
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			// If we're saving a new test version, we'll want to bump our dropdown to show this as the new test version
			LibDropDownBean currDropDown = getLibDropDownBean();
			LibDropDownVersionBean currDropDownVer = getLibDropDownVersionBean();
			if ( currDropDownVer.dropdownVersion().getVersion() > currDropDown.dropdown().getTestVersion() )
				currDropDown.dropdown().bumpTestVersion();
			
			LibDropDownBean savedDropDown = dropdownForm.save(con);
			if ( savedDropDown == null )  {
				con.rollback();
				return false;
			}
			
			LibDropDownVersionBean savedDocVer = dropdownVerForm.save(con);
			if ( savedDocVer == null )  {
				con.rollback();
				return false;
			}
			
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Saved dropdown " + savedDropDown.getEsfName() + " version " + savedDocVer.getVersion() + ". DropDownVersionId: " + savedDocVer.getId() + "; status: " + savedDropDown.getStatus());
			}

			con.commit();
	    		
			dropdownContainer.refresh();  // refresh our list after a change
			selectDropDown(savedDropDown);
			selectDropDownVersion(savedDocVer);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean createNewDropDown() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		DropDownInfo dropdownInfo = DropDownInfo.Manager.createNew(getLibrary().getId());
		LibDropDownBean dropdownBean = new LibDropDownBean(dropdownInfo);
		BeanItem<LibDropDownBean> dropdownItem = new BeanItem<LibDropDownBean>(dropdownBean);
        dropdownForm.setItemDataSource(dropdownItem);
		
		DropDownVersionInfo dropdownVerInfo = DropDownVersionInfo.Manager.createNew(DropDown.Manager.getById(dropdownInfo.getId()),vaadinUi.getUser());
		LibDropDownVersionBean dropdownVerBean = new LibDropDownVersionBean(dropdownVerInfo,getVersionLabel(dropdownVerInfo));
		BeanItem<LibDropDownVersionBean> dropdownVerItem = new BeanItem<LibDropDownVersionBean>(dropdownVerBean);
        dropdownVerForm.setItemDataSource(dropdownVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean createLikeDropDown() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibDropDownBean likeDocBean = getLibDropDownBean();
		LibDropDownVersionBean likeDocVerBean = getLibDropDownVersionBean();
		
    	LibDropDownBean createdDocBean = likeDocBean.createLike();
		BeanItem<LibDropDownBean> createdDocItem = new BeanItem<LibDropDownBean>(createdDocBean);
        dropdownForm.setItemDataSource(createdDocItem);

    	DropDownVersionInfo createdDropDownVerInfo = DropDownVersionInfo.Manager.createLike(DropDown.Manager.getById(createdDocBean.dropdownInfo().getId()), likeDocVerBean.dropdownVersion(), vaadinUi.getUser());
    	LibDropDownVersionBean createdDropDownVerBean = new LibDropDownVersionBean(createdDropDownVerInfo,Literals.VERSION_LABEL_TEST); // The new dropdown and version will always be Test.
		BeanItem<LibDropDownVersionBean> createdDropDownVerItem = new BeanItem<LibDropDownVersionBean>(createdDropDownVerBean);       
        dropdownVerForm.setItemDataSource(createdDropDownVerItem);
        
        setReadOnly(false);
		return true;
	}

	public boolean createNextVersion() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibDropDownBean currDropDownBean = getLibDropDownBean();
		LibDropDownVersionBean currDropDownVerBean = getLibDropDownVersionBean();
		
        dropdownForm.setReadOnly(!hasPermUpdate()); // should have permission since doing a create next version...

    	DropDownVersionInfo nextDropDownVerInfo = DropDownVersionInfo.Manager.createLike(currDropDownBean.dropdown(), currDropDownVerBean.dropdownVersion(), vaadinUi.getUser());
    	LibDropDownVersionBean nextDropDownVerBean = new LibDropDownVersionBean(nextDropDownVerInfo,Literals.VERSION_LABEL_TEST); // The new version will always be Test.
		BeanItem<LibDropDownVersionBean> nextDropDownVerItem = new BeanItem<LibDropDownVersionBean>(nextDropDownVerBean);
        dropdownVerForm.setItemDataSource(nextDropDownVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean promoteTestToProductionVersion() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibDropDownVersionForm.versionChange.TestToProd.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibDropDownBean dropdownBean = getLibDropDownBean();
			LibDropDownVersionBean dropdownVerBean = getLibDropDownVersionBean();
			
			dropdownBean.dropdown().promoteTestVersionToProduction();
			
			if ( ! dropdownBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved dropdown " + dropdownBean.getEsfName() + " version " + dropdownVerBean.getVersion() + " into Production status. DropDownVersionId: " + dropdownVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved dropdown " + dropdownBean.getEsfName() + " version " + dropdownVerBean.getVersion() + " into Production status. DropDownVersionId: " + dropdownVerBean.getId());
			}
			
			con.commit();
			dropdownContainer.refresh();  // refresh our list after a change
			selectDropDown(dropdownBean);
			selectDropDownVersion(dropdownVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibDropDownVersionForm.versionChange.TestToProd.success.message",dropdownBean.getEsfName(),dropdownVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean revertProductionVersionBackToTest() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibDropDownVersionForm.versionChange.revertProdToTest.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibDropDownBean dropdownBean = getLibDropDownBean();
			LibDropDownVersionBean dropdownVerBean = getLibDropDownVersionBean();
			
			dropdownBean.dropdown().revertProductionVersionBackToTest();
			
			if ( ! dropdownBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved Production dropdown " + dropdownBean.getEsfName() + " version " + dropdownVerBean.getVersion() + " back into Test status. DropDownVersionId: " + dropdownVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved Production dropdown " + dropdownBean.getEsfName() + " version " + dropdownVerBean.getVersion() + " back into Test status. DropDownVersionId: " + dropdownVerBean.getId());
			}
			
			con.commit();
			dropdownContainer.refresh();  // refresh our list after a change
			selectDropDown(dropdownBean);
			selectDropDownVersion(dropdownVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibDropDownVersionForm.versionChange.revertProdToTest.success.message",dropdownBean.getEsfName(),dropdownVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean delete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibDropDownBean currDropDown = getLibDropDownBean();
			LibDropDownVersionBean currDropDownVer = getLibDropDownVersionBean();
			
			// If there is no Production version, we'll get rid of the drop down entirely, otherwise just the test version
			Errors errors = new Errors();
			if ( currDropDown.getProductionVersion() == 0 )
				currDropDown.dropdown().delete(con,errors,vaadinUi.getUser());
			else {
				currDropDownVer.dropdownVersion().delete(con,errors,vaadinUi.getUser());
				currDropDown.dropTestVersion();
				currDropDown.save(con, errors);
			}
			
			if ( errors.hasError() ) {
				vaadinUi.show(errors);
				con.rollback();
				return false;
			}
			
			con.commit();
    		
			vaadinUi.showStatus(vaadinUi.getMsg("LibDropDownVersionForm.button.delete.successMessage",currDropDown.getEsfName(),currDropDownVer.getVersion()));

			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Deleted dropdown " + currDropDown.getEsfName() + " version " + currDropDownVer.getVersion() + ". DropDownVersionId: " + currDropDownVer.getId() + "; status: " + currDropDown.getStatus());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted dropdown " + currDropDown.getEsfName() + " version " + currDropDownVer.getVersion() + ". DropDownVersionId: " + currDropDownVer.getId() + "; status: " + currDropDown.getStatus());
			}

			dropdownContainer.refresh();  // refresh our list after a change
			selectDropDown(null);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		_logger.debug("activateView() - params: " + params);
		libraryId = new EsfUUID(params);
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return dropdownForm.isDirty() ? dropdownForm.checkDirty() : dropdownVerForm.checkDirty();
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return dropdownForm.isDirty() || dropdownVerForm.isDirty();
	}
}