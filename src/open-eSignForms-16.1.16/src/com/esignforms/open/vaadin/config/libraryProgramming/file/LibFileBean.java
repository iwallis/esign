// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.file;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.File;
import com.esignforms.open.prog.FileInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibFileBean implements Serializable, Comparable<LibFileBean> {
	private static final long serialVersionUID = 3196342209896925343L;

	private FileInfo fileInfo;

	private File file;
	
	public LibFileBean(FileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibFileBean )
			return fileInfo.equals(((LibFileBean)o).fileInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return fileInfo.hashCode();
    }
	public int compareTo(LibFileBean d) {
		return fileInfo.compareTo(d.fileInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public FileInfo fileInfo() {
		return fileInfo;
	}

	public File file() {
		if ( file == null )
			file = File.Manager.getById(fileInfo.getId());
		return file;
	}

	public LibFileBean createLike() {
		FileInfo newFileInfo = FileInfo.Manager.createLike(file, file.getEsfName());	    
	    return new LibFileBean(newFileInfo);
	}
	
	
	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( file().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			fileInfo = FileInfo.Manager.createFromSource(file);
			return true;
		} 
		
		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}

	
	// Now for the JavaBeans methods
	public EsfName getEsfName() {
		return fileInfo.getEsfName();
	}
	public void setEsfName(EsfName v) {
		file().setEsfName(v);
	}

	public EsfUUID getId() {
		return fileInfo.getId();
	}
	
	public String getDisplayName() {
		return fileInfo.getDisplayName();
	}
	public void setDisplayName(String v) {
		file().setDisplayName(v);
	}

	public String getDescription() {
		return fileInfo.getDescription();
	}
	public void setDescription(String v) {
		file().setDescription(v);
	}

	public boolean isEnabled() {
		return fileInfo.isEnabled();
	}
	public String getStatus() {
		return fileInfo.getStatus();
	}
	public void setStatus(String v) {
		file().setStatus(v);
	}	
	
	public Integer getProductionVersion() {
		return fileInfo.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		return fileInfo.getTestVersion();
	}
	
	public void bumpTestVersion() {
		file().bumpTestVersion();
	}
	public void dropTestVersion() {
		file().dropTestVersion();
	}
	
	public String getComments() {
		return file().getComments();
	}
	public void setComments(String v) {
		file().setComments(v);
	}
}