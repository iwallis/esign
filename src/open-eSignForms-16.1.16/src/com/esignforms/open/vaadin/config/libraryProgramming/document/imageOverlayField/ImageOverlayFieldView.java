// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document.imageOverlayField;

import java.util.List;

import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.ImageVersionOverlayField;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.main.EsfViewWithConfirmDiscardFormChangesWindowParent;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

public class ImageOverlayFieldView extends VerticalSplitPanel implements EsfViewWithConfirmDiscardFormChangesWindowParent, Property.ValueChangeListener {
	private static final long serialVersionUID = 734347238493689890L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ImageOverlayFieldView.class);
	
    final ImageOverlayFieldView thisView;
    final List<ImageVersionOverlayField> overlayFieldList;
    final DocumentVersion documentVersion;
    ConfirmDiscardFormChangesWindow parentWindow;
    ImageOverlayFieldContainer container;
    ImageOverlayFieldList list;
    ImageOverlayFieldForm form;
	
	public ImageOverlayFieldView(DocumentVersion documentVersionParam, List<ImageVersionOverlayField> overlayFieldListParam) {
		super();
		this.documentVersion = documentVersionParam;
		this.overlayFieldList = overlayFieldListParam;
		if ( overlayFieldList != null ) {
			for ( ImageVersionOverlayField ivof : overlayFieldList ) { // we do this so we can resolve field template ids back to the field template name
				ivof._setDocumentVersionToResolveFieldNames(documentVersion);
			}
		}
		thisView = this;
	}
	
	// There's no document version action for changing the tran/party status
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    
		try {
			setStyleName(Reindeer.SPLITPANEL_SMALL);

			container = new ImageOverlayFieldContainer(overlayFieldList);
	        list = new ImageOverlayFieldList(this,container);
			form = new ImageOverlayFieldForm(this,container);
			
			setStyleName(Reindeer.SPLITPANEL_SMALL);
			setFirstComponent(list);
			setSecondComponent(form);
			setSplitPosition(30,Unit.PERCENTAGE);

	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("buildLayout() - Failed to build view", e);
			vaadinUi.showError("ImageOverlayFieldView view exception", e.getMessage());
		}
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		if ( list != null) list.setReadOnly(v);
		if ( form != null) form.setReadOnly(v);
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our list or not
        if (property == list.getTable()) {
        	final Item item = list.getTable().getItem(list.getTable().getValue());
        	if ( isDirty() ) {
    			if ( form.fieldGroup.getItemDataSource() == item ) {
    				// We've selected the same item so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldView.ConfirmDiscardChangesDialog.message"), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						form.fieldGroup.discard();
						form.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						list.getTable().select(item); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldView.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    		} else {
            	form.setItemDataSource(item);
    		}
        }
        else
        	_logger.debug("valueChange() - no list event: " + event.toString());
	}

	@Override
	public void setParentWindow(ConfirmDiscardFormChangesWindow w) {
		parentWindow = w;
	}
	
	public void closeParentWindow() {
		parentWindow.close();
	}
	
	public void unselectAll() {
		list.getTable().setValue(null);
	}
	public void select(ImageVersionOverlayField overlayField) {
		unselectAll();
		list.getTable().select(overlayField);
		list.getTable().setCurrentPageFirstItemId(overlayField);
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return form.checkDirty();
	}
	
	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return form.isDirty();
	}
}