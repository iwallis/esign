// Copyright (C) 2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.TimerAction;

import java.util.Arrays;
import java.util.LinkedList;

import com.esignforms.open.config.Literals;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.runtime.action.TimerAction;
import com.esignforms.open.runtime.action.TimerAction.Spec;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.FieldSpecTextField;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author Yozons Inc.
 */
public class ActionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -6439824691495044158L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ActionForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;

	GridLayout layout;
	
	final PackageVersion duplicatedPackageVersion;
	final TimerAction duplicatedAction;
	final ActionView view;
	final ActionContainer container;
	
	Spec newSpec; // when set, we're working on a new spec.
	
	
	public ActionForm(ActionView view, final ActionContainer container, PackageVersion duplicatedPackageVersionParam, TimerAction duplicatedActionParam) {
		setStyleName("TimerActionForm");
		this.view = view;
		this.container = container;
		this.duplicatedPackageVersion = duplicatedPackageVersionParam;
		this.duplicatedAction = duplicatedActionParam;
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	// Setup layout
    	layout = new GridLayout(3,2);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.1f);
    	layout.setColumnExpandRatio(1, 0.4f);
    	layout.setColumnExpandRatio(2, 0.5f);
    	setLayout(layout);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
        setBuffered(true);
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    	footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

				if (propertyId.equals("timerType")) { // this field is always present and determines which of the other fields are visible and active
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.TimerAction.form.timerType.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.TimerAction.form.timerType.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);

                	select.addItem(TimerAction.SET_TIMER_TYPE);
                	select.setItemCaption(TimerAction.SET_TIMER_TYPE, 
                			vaadinUi.getPrettyCode().timerActionType(TimerAction.SET_TIMER_TYPE));
                	select.addItem(TimerAction.CANCEL_TIMER_TYPE);
                	select.setItemCaption(TimerAction.CANCEL_TIMER_TYPE, 
                			vaadinUi.getPrettyCode().timerActionType(TimerAction.CANCEL_TIMER_TYPE));
                	select.addItem(TimerAction.CANCEL_ALL_TIMERS_TYPE);
                	select.setItemCaption(TimerAction.CANCEL_ALL_TIMERS_TYPE, 
                			vaadinUi.getPrettyCode().timerActionType(TimerAction.CANCEL_ALL_TIMERS_TYPE));
              
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							String type = (String)event.getProperty().getValue();
							setupVariableFields(type);
						}
                	});
                	return select;
                }
                
				if ( propertyId.equals("expireTimestampSpec") ) { // present for SET_TIMER
					FieldSpecTextField tf = new FieldSpecTextField();
                	tf.setWidth(100, Unit.PERCENTAGE);
                	//tf.setRequired(true); -- we set this depending on which type is chosen
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setImmediate(true);
                    tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.TimerAction.form.expireTimestampSpec.label"));
                    tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.TimerAction.form.expireTimestampSpec.tooltip"));
                    return tf;
                }
                
				if ( propertyId.equals("reasonSpec") ) { // this field is always present
					FieldSpecTextField tf = new FieldSpecTextField();
                	tf.setWidth(100, Unit.PERCENTAGE);
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setImmediate(true);
                    tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.TimerAction.form.reasonSpec.label"));
                    tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.TimerAction.form.reasonSpec.tooltip"));
                    return tf;
                }
                
				Field field = super.createField(item, propertyId, uiContext);
				field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("timerName")) { // present on SET_TIMER and CANCEL_TIMER types
	            	TextField tf = (TextField)field;
	            	//tf.setRequired(true); -- we set this depending on which type is chosen
	                tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setImmediate(true);
	            	tf.setMaxLength(Literals.TIMER_NAME_MAX_LENGTH);
	            	tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.TimerAction.form.timerName.label"));
	            	tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.TimerAction.form.timerName.tooltip"));
	    	    }
                return field;
			}
    	 });

    	_logger.debug("Form created");

	}
	
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		if ( getItemDataSource() == null ) {
    			saveSpecs();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
    		if ( newSpec != null ) {
    			container.addItem(newSpec);
    			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",vaadinUi.getMsg("PackageProgrammingRule.TimerAction.form.message.spec.name")) );
    			newSpec = null;
    		} else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("PackageProgrammingRule.TimerAction.form.message.spec.name")) );
    		}
    		
			saveSpecs();
    		
    		view.select(getCurrentBean()); // reselect our bean so all updates cleanly
        } else if ( source == cancelButton ) {
    		discard();
    		if ( getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			setNewSpecAsDataSource(null);
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	view.closeParentWindow();
        } else if ( source == createLikeButton ) {
        	discard();
        	Spec spec = getCurrentBean();
        	if ( spec != null ) {
    			view.unselectAll();
    			setNewSpecAsDataSource(spec.duplicate());
        	}
        } else if ( source == deleteButton ) {
        	discard();
        	
        	if ( newSpec != null ) {
        		newSpec = null;
        	} else {
            	Spec spec = getCurrentBean();
            	if ( spec != null ) {
            		container.removeItem(spec);
        			saveSpecs();
            	}
        	}
    		view.unselectAll();
			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("PackageProgrammingRule.TimerAction.form.message.spec.name")) );
        }
    }
	
	void saveSpecs() {
		view.specListUpdated();
	}
	
	Spec getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public Spec getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<Spec> bi = (BeanItem<Spec>)dataSource;
		return bi.getBean();
    }
    
    public void setNewSpecAsDataSource(Spec spec) {
    	newSpec = spec;
    	if ( newSpec == null )
    		setItemDataSource(null);
    	else
    		setItemDataSource(new BeanItem<Spec>(newSpec));
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		if ( newDataSource != null ) {
    		super.setItemDataSource(newDataSource, Arrays.asList("timerType","timerName","expireTimestampSpec","reasonSpec"));
    		layout.setVisible(true);
    		setupVariableFields(getCurrentBean().getTimerType());
		} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
		}
		setReadOnly(isReadOnly());
    }

	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	Spec bean = getCurrentBean();
    	
    	saveButton.setVisible(!readOnly && bean != null);
    	cancelButton.setVisible(!readOnly && bean != null);
    	closeButton.setVisible(true);
    	createLikeButton.setVisible(!readOnly && bean != null && newSpec == null);
    	deleteButton.setVisible(!readOnly && bean != null);
    }
    
	void setupVariableFields(String timerType) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		boolean isCancelAllTimers = TimerAction.CANCEL_ALL_TIMERS_TYPE.equals(timerType);
		boolean isSetTimer = TimerAction.SET_TIMER_TYPE.equals(timerType);

		Field<?> f = getField("timerName");
		if ( f != null ) {
			f.setRequired(!isCancelAllTimers);
			f.setEnabled(!isCancelAllTimers);
		}

		f = getField("expireTimestampSpec");
		if ( f != null ) {
			f.setRequired(isSetTimer);
			f.setEnabled(isSetTimer);
		}
	}
	
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("timerType")) {
            layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("timerName")) {
        	layout.addComponent(field, 1, 0); 
        } else if (propertyId.equals("expireTimestampSpec")) {
        	layout.addComponent(field, 2, 0); 
        } else if (propertyId.equals("reasonSpec")) {
        	layout.addComponent(field, 0, 1, 2, 1);
        }
    }
	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageProgrammingRule.TimerAction.view.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		Field<?> f = getField("timerType");
		if ( f != null ) {
			setupVariableFields((String)f.getValue()); // we do this to discard any fields that may be updated but don't matter for our type
		}
		return isModified() || newSpec != null;
	}
	
}