// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.SendEmailAction;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentIdFieldName;
import com.esignforms.open.prog.DocumentIdPartyId;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.prog.PackageVersionPartyTemplateDocumentParty;
import com.esignforms.open.runtime.action.SendEmailAction;
import com.esignforms.open.runtime.action.SendEmailAction.Spec;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.FieldSpecTextField;
import com.esignforms.open.vaadin.widget.ListSelectValid;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.VerticalLayout;

/**
 * @author Yozons Inc.
 */
public class ActionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -8835826763203633630L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ActionForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;

	GridLayout layout;
	VerticalLayout pdfInfoLayout;
	
	final PackageVersion duplicatedPackageVersion;
	final SendEmailAction duplicatedAction;
	final ActionView view;
	final ActionContainer container;
	
	List<EsfUUID> allPartyTemplateIds;
	List<EsfName> allPartyTemplateEsfNames;
	
	List<EsfName> allEmailTemplateEsfNames; // don't use ids since we search libraries by name
	
	List<DocumentIdFieldName> allDocAndFileFields; 

	List<DocumentIdPartyId> allDocAndParty;
	
	Spec newSpec; // when set, we're working on a new spec.
	
	
	public ActionForm(ActionView view, final ActionContainer container, PackageVersion duplicatedPackageVersionParam, SendEmailAction duplicatedActionParam)	{
		setStyleName("SendEmailActionForm");
		this.view = view;
		this.container = container;
		this.duplicatedPackageVersion = duplicatedPackageVersionParam;
		this.duplicatedAction = duplicatedActionParam;
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		Collection<Library> libraries = Library.Manager.getForUserWithViewDetailsPermission(vaadinUi.getUser(), Library.INCLUDE.ONLY_ENABLED);
		
		allPartyTemplateIds = new LinkedList<EsfUUID>();
		allPartyTemplateEsfNames = new LinkedList<EsfName>();
    	for( PackageVersionPartyTemplate pvpt : duplicatedPackageVersion.getPackageVersionPartyTemplateList() ) {
    		if ( ! pvpt.isEsfReportsAccess() ) { // cannot send emails related to the built-in reports access party
        		allPartyTemplateIds.add(pvpt.getId());
        		allPartyTemplateEsfNames.add(pvpt.getEsfName());
    		}
    	}

		allEmailTemplateEsfNames = new LinkedList<EsfName>();
		List<EsfName> passwordRelatedEmailTemplateNames = vaadinUi.getEsfapp().getAllPasswordRelatedEmailTemplateEsfNames();
		for( Library library : libraries ) {
			List<EmailTemplate> list = EmailTemplate.Manager.getAll(library.getId());
			for( EmailTemplate emailTemplate : list ) {
				// We'll skip some well known email templates
				if ( passwordRelatedEmailTemplateNames.contains(emailTemplate.getEsfName()) )
					continue;
				// We'll skip any names we already have (defined in multiple libraries)
				if ( allEmailTemplateEsfNames.contains(emailTemplate.getEsfName()) )
					continue;
				allEmailTemplateEsfNames.add(emailTemplate.getEsfName());
			}
		}
		
		allDocAndParty = new LinkedList<DocumentIdPartyId>();
		for ( EsfUUID docId : duplicatedPackageVersion.getDocumentIdList() ) {
    		Document doc = Document.Manager.getById(docId);
    		if ( doc == null ) {
    			_logger.warn("ActionForm() - Could not find document with id: " + docId + "; not including in our list");
    			continue;
    		}
    		DocumentIdPartyId dp = new DocumentIdPartyId(docId,null);
			dp.documentName = doc.getEsfName();
			allDocAndParty.add(dp); // this is the "latest document" entry, not tied to a particular party.
			for( PackageVersionPartyTemplate pvpt : duplicatedPackageVersion.getPackageVersionPartyTemplateList() ) {
				for( PackageVersionPartyTemplateDocumentParty mapping : pvpt.getPackageVersionPartyTemplateDocumentParties() ) {
					if ( mapping.isDocumentPartyTemplateView() || mapping.isDocumentPartyTemplateEsfReportsAccess() )
						continue; // this document mapping is not to a particular party, so we ignore for our list
					if ( mapping.getDocumentId().equals(docId) ) {
						PackageVersionPartyTemplate matchingParty = duplicatedPackageVersion.getPackageVersionPartyTemplateById(mapping.getPackageVersionPartyTemplateId());
						if ( matchingParty == null ) {
			    			_logger.warn("ActionForm() - Could not find matching party for document id: " + docId + "; packageVersionPartyTemplateId: " + mapping.getPackageVersionPartyTemplateId() + "; not including in our list");
			    			continue;
						} else if ( matchingParty.isEsfReportsAccess() ) {
							continue;
						}
						dp = new DocumentIdPartyId(docId,doc.getEsfName(),mapping.getPackageVersionPartyTemplateId(),matchingParty.getEsfName());
						allDocAndParty.add(dp);
					}
				}
			}
		}
		
		allDocAndFileFields = new LinkedList<DocumentIdFieldName>();
		for ( EsfUUID docId : duplicatedPackageVersion.getDocumentIdList() ) {
    		Document doc = Document.Manager.getById(docId);
    		if ( doc == null ) {
    			_logger.warn("ActionForm() - Could not find document with id: " + docId + "; not including any file attachment fields in our list");
    			continue;
    		}
    		DocumentVersion docVersion;
    		com.esignforms.open.prog.Package pkg = duplicatedPackageVersion.getPackage();
    		if ( pkg.getProductionVersion() == duplicatedPackageVersion.getVersion() )
    			docVersion = doc.getProductionDocumentVersion();
    		else
    			docVersion = doc.getTestDocumentVersion();
    		Map<EsfName,FieldTemplate> fieldTemplateMap = docVersion.getFieldTemplateMap();
    		for( EsfName fieldName : fieldTemplateMap.keySet() ) {
    			FieldTemplate fieldTemplate = fieldTemplateMap.get(fieldName);
    			if ( fieldTemplate.isTypeFile() )
    				allDocAndFileFields.add( new DocumentIdFieldName(docId,fieldName,doc.getEsfName()) );
    		}
		}
	
    	// Setup layout
    	layout = new GridLayout(3,2);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.1f);
    	layout.setColumnExpandRatio(1, 0.2f);
    	layout.setColumnExpandRatio(2, 0.7f);
    	setLayout(layout);
    	
    	// Holds our PDF checkbox and optional pdf file name spec (only visible when checked)
    	pdfInfoLayout = new VerticalLayout();
    	pdfInfoLayout.setMargin(false);
    	pdfInfoLayout.setSpacing(true);
    	layout.addComponent(pdfInfoLayout, 1, 1);
    	layout.setComponentAlignment(pdfInfoLayout, Alignment.TOP_CENTER);

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true);
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	
    	saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    	footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 1632213076068756835L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

				if (propertyId.equals("emailTemplateName")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.emailTemplateName.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.emailTemplateName.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	select.setImmediate(true);
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	for( EsfName emailTemplateName : allEmailTemplateEsfNames ) {
                		select.addItem(emailTemplateName);
                		select.setItemCaption(emailTemplateName, emailTemplateName.toString());
                	}
                	return select;
                }
                
				if (propertyId.equals("partyId")) {
					EsfUUID noPartyId = new EsfUUID("");
					
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.partyId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.partyId.tooltip"));
                	select.setNullSelectionAllowed(true);
                	select.setNullSelectionItemId(noPartyId);
                	select.setRequired(false);
                	select.setImmediate(true);
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);

            		ListIterator<EsfName> partyNameIter = allPartyTemplateEsfNames.listIterator();
            		for( EsfUUID partyId : allPartyTemplateIds ) {
                		select.addItem(partyId);
                		select.setItemCaption(partyId, partyNameIter.next().toString());
                	}
                	return select;
                }
                
				if (propertyId.equals("documentIdSet")) {
                	String partyLatest = vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.documentIdSet.latestParty");

                	ListSelect select = new ListSelectValid(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.documentIdSet.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.documentIdSet.tooltip"));
                	select.setNullSelectionAllowed(true);
                	select.setRequired(false);
                	select.setImmediate(true);
                	select.setMultiSelect(true);
                	select.setRows( Math.min(5, allDocAndParty.size()));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	
	                select.addValueChangeListener( new ValueChangeListener() {

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							java.util.Set<DocumentIdPartyId> selectedDocuments = (java.util.Set<DocumentIdPartyId>)event.getProperty().getValue();
							CheckBox attachAsPdf = (CheckBox)getField("attachAsPdf");
							if ( attachAsPdf != null ) {
								boolean canAttachAsPdf = selectedDocuments != null && selectedDocuments.size() > 0;
								attachAsPdf.setVisible(canAttachAsPdf);
								Field pdfFileNameField = getField("pdfFileNameSpec");
								if ( pdfFileNameField != null )
									pdfFileNameField.setVisible(canAttachAsPdf && attachAsPdf.getValue());
							}
						}
	                });
                	
                	for( DocumentIdPartyId docAndParty : allDocAndParty ) {
                		select.addItem(docAndParty);
                		String partyName = docAndParty.partyName == null ? partyLatest : docAndParty.partyName.toString();
                		select.setItemCaption(docAndParty, docAndParty.documentName.toString() + " (" + partyName + ")");
                	}
                	return select;
                }
                
				if (propertyId.equals("attachAsPdf")) {
                	CheckBox cb = new CheckBox(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.attachAsPdf.label"));
                	cb.setDescription(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.attachAsPdf.tooltip"));
                	cb.setStyleName("attachAsPdf");
                	cb.setVisible(false);
                	cb.setImmediate(true);
                	cb.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 3973747577266479973L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							boolean checked = (Boolean)event.getProperty().getValue();
							Field pdfFileNameField = getField("pdfFileNameSpec");
							if ( pdfFileNameField != null )
								pdfFileNameField.setVisible(checked);
						}
                		
                	});
                	return cb;
                }
                
				if (propertyId.equals("fileFieldSet")) {
                	ListSelect select = new ListSelectValid(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.fileFieldSet.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.fileFieldSet.tooltip"));
                	select.setNullSelectionAllowed(true);
                	select.setRequired(false);
                	select.setImmediate(true);
                	select.setMultiSelect(true);
                	select.setRows( Math.min(5, allDocAndFileFields.size()));
	                select.addValidator(new SelectValidator(select));
                	for( DocumentIdFieldName docAndField : allDocAndFileFields ) {
                		select.addItem(docAndField);
                		select.setItemCaption(docAndField, docAndField.documentName + " (" + docAndField.fieldName + ")");
                	}
                	return select;
                }
                
				FieldSpecTextField tf = new FieldSpecTextField();
                tf.setWidth(100, Unit.PERCENTAGE);
    			
                if ( propertyId.equals("emailAddressFieldExpression") ) {
                	tf.setRequired(false);
                    tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.emailAddressFieldExpression.label"));
                    tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.emailAddressFieldExpression.tooltip"));
                } else if ( propertyId.equals("pdfFileNameSpec") ) {
                	tf.setRequired(false);
                	tf.setVisible(false);
                	tf.setInputPrompt(vaadinUi.getServerMsg("Package.defaultDownloadFileName"));
                    tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.pdfFileNameSpec.label"));
                    tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.pdfFileNameSpec.tooltip"));
                }
                
                return tf;
    	    }
    	 });

    	_logger.debug("Form created");

	}
	
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		if ( getItemDataSource() == null ) {
    			saveSpecs();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
    		if ( newSpec != null ) {
    			container.addItem(newSpec);
    			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.message.spec.name")) );
    			newSpec = null;
    		} else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.message.spec.name")) );
    		}
    		
			saveSpecs();
    		
    		view.select(getCurrentBean()); // reselect our bean so all updates cleanly
        } else if ( source == cancelButton ) {
    		discard();
    		if ( getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			setNewSpecAsDataSource(null);
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	view.closeParentWindow();
        } else if ( source == createLikeButton ) {
        	discard();
        	Spec spec = getCurrentBean();
        	if ( spec != null ) {
    			view.unselectAll();
    			setNewSpecAsDataSource(spec.duplicate());
        	}
        } else if ( source == deleteButton ) {
        	discard();
        	
        	if ( newSpec != null ) {
        		newSpec = null;
        	} else {
            	Spec spec = getCurrentBean();
            	if ( spec != null ) {
            		container.removeItem(spec);
        			saveSpecs();
            	}
        	}
    		view.unselectAll();
			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.form.message.spec.name")) );
        }
    }
	
	void saveSpecs() {
		view.specListUpdated();
	}
	
	Spec getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public Spec getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<Spec> bi = (BeanItem<Spec>)dataSource;
		return bi.getBean();
    }
    
    public void setNewSpecAsDataSource(Spec spec) {
    	newSpec = spec;
    	if ( newSpec == null )
    		setItemDataSource(null);
    	else
    		setItemDataSource(new BeanItem<Spec>(newSpec));
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		if ( newDataSource != null ) {
			if ( allDocAndFileFields.size() > 0 ) // only include file field set if this package has documents with file fields defined
				super.setItemDataSource(newDataSource, Arrays.asList("emailTemplateName","partyId","emailAddressFieldExpression","attachAsPdf","pdfFileNameSpec","documentIdSet","fileFieldSet"));
			else
				super.setItemDataSource(newDataSource, Arrays.asList("emailTemplateName","partyId","emailAddressFieldExpression","attachAsPdf","pdfFileNameSpec","documentIdSet"));
    		layout.setVisible(true);
    		
    		Field pdfFileNameSpec = getField("pdfFileNameSpec");
    		if ( pdfFileNameSpec != null ) {
        		Spec spec = getCurrentBean();
        		pdfFileNameSpec.setVisible(spec.isAttachAsPdf());
    		}
		} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
		}
		setReadOnly(isReadOnly());
    }

	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	Spec bean = getCurrentBean();
    	
    	saveButton.setVisible(!readOnly && bean != null);
    	cancelButton.setVisible(!readOnly && bean != null);
    	closeButton.setVisible(true);
    	createLikeButton.setVisible(!readOnly && bean != null && newSpec == null);
    	deleteButton.setVisible(!readOnly && bean != null);
    }
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("emailTemplateName")) {
            layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("partyId")) {
        	layout.addComponent(field, 1, 0);
        } else if (propertyId.equals("emailAddressFieldExpression")) {
        	layout.addComponent(field, 2, 0);
        } else if (propertyId.equals("documentIdSet")) {
        	layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("attachAsPdf")) {
        	pdfInfoLayout.removeAllComponents();
        	pdfInfoLayout.addComponent(field);
        } else if (propertyId.equals("pdfFileNameSpec")) {
        	pdfInfoLayout.addComponent(field);
        } else if (propertyId.equals("fileFieldSet")) { 
        	layout.addComponent(field, 2, 1);
        }
    }
	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageProgrammingRule.SendEmailAction.view.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified() || newSpec != null;
	}
	
}