// Copyright (C) 2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document.imageOverlayField;

import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.ImageVersionOverlayField;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

public class ImageOverlayFieldList extends Panel {
	private static final long serialVersionUID = 802633678053419781L;

	final ImageOverlayFieldView view;
	ImageOverlayFieldContainer container;
	OverlayList table;
	
	public ImageOverlayFieldList(final ImageOverlayFieldView viewParam, final ImageOverlayFieldContainer containerParam) {
		super();
		this.view = viewParam;
		this.container = containerParam;
		setStyleName("ImageOverlayFieldList");
		setSizeFull();
		
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
    	
    	table = new OverlayList(view);
    	
    	layout.addComponent(table);
    	layout.setExpandRatio(table, 1);
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
	}
	
	public OverlayList getTable() {
		return table;
	}
	
	class OverlayList extends Table {
		private static final long serialVersionUID = -2740053600175999472L;

		final OverlayList thisList;
		
		public OverlayList(ImageOverlayFieldView view) {
			super();
			thisList = this;
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldView.list.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldView.list.showColumnHeaders"));
			setSortEnabled(true);
			setColumnAlignment("positionLeft", Align.CENTER);
			setColumnAlignment("positionTop", Align.CENTER);
			setColumnAlignment("positionWidth", Align.CENTER);
			setColumnAlignment("positionHeight", Align.CENTER);
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);

	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener((Property.ValueChangeListener)view);
	        setNullSelectionAllowed(true);
			setSizeFull();
		}
		
	    @SuppressWarnings("unchecked")
		public ImageVersionOverlayField getBean(Item item) {
	    	if ( item == null )
	    		return null;
			BeanItem<ImageVersionOverlayField> bi = (BeanItem<ImageVersionOverlayField>)item;
			return bi.getBean();
	    }

		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			if ( "positionWidth".equals(colId) || "positionHeight".equals(colId) ) {
				ImageVersionOverlayField ivof = (ImageVersionOverlayField)rowId;
				FieldTemplate ft = view.documentVersion.getFieldTemplate(ivof.getFieldTemplateId());
				return ft == null || ft.isTypeCheckbox() || ft.isTypeRadioButton() ? "n/a" : property.getValue().toString();
			}
			if ( "displayMode".equals(colId) ) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				return vaadinUi.getPrettyCode().imageOverlayDisplayMode((String)property.getValue());
			}
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // OverlayList

} // ImageFieldOverlayList
