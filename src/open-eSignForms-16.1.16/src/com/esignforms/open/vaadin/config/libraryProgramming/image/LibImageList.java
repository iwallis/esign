// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.image;

import java.text.DecimalFormat;

import org.vaadin.peter.buttongroup.ButtonGroup;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.prog.ImageInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class LibImageList extends Panel {
	private static final long serialVersionUID = -482007578454587091L;

	final LibImageAndVersionsMainView view;
	LibImageTable table;
	
	// For search bar
	TextField searchEsfName;
	CheckBox searchEnabled;
	CheckBox searchDisabled;
	Button filterButton;
	Button showAllButton;
	
	DecimalFormat versionFormat = new DecimalFormat("#,###,###,###");
	

	public LibImageList(final LibImageAndVersionsMainView view, final LibImageBeanContainer container) {
		super();
		this.view = view;
		setStyleName("LibImageList");
		setSizeFull();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
    	
    	HorizontalLayout searchBar = new HorizontalLayout();
    	searchBar.addStyleName("searchBar");
    	searchBar.setSpacing(true);
    	searchBar.setMargin(false);
    	
    	String tooltipSearchString = vaadinUi.getMsg("tooltip.search.string");
    	
    	searchEsfName = new TextField();
    	searchEsfName.setStyleName("searchEsfName");
    	searchEsfName.setInputPrompt(vaadinUi.getMsg("LibImageList.searchBar.esfname.label"));
    	searchEsfName.setDescription(tooltipSearchString);
    	searchBar.addComponent(searchEsfName);
    	
    	searchEnabled = new CheckBox(vaadinUi.getPrettyCode().status(Literals.STATUS_ENABLED));
    	searchBar.addComponent(searchEnabled);

    	searchDisabled = new CheckBox(vaadinUi.getPrettyCode().status(Literals.STATUS_DISABLED));
    	searchBar.addComponent(searchDisabled);

    	ButtonGroup buttonGroup = new ButtonGroup();
    	buttonGroup.setStyleName(Reindeer.BUTTON_SMALL);
    	searchBar.addComponent(buttonGroup);
    	
    	filterButton = new Button(vaadinUi.getMsg("LibImageList.searchBar.filteredButton.label"));
    	filterButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.filter.icon")));
    	filterButton.setDescription(vaadinUi.getMsg("button.filter.tooltip"));
    	filterButton.setStyleName(Reindeer.BUTTON_SMALL);
    	filterButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = 6029328202093038791L;

			@Override
			public void buttonClick(ClickEvent event) {
				view.unselectAllImages();
				container.removeAllContainerFilters();
				String limitEsfName = (String)searchEsfName.getValue();
				if ( EsfString.isNonBlank(limitEsfName) ) {
					boolean searchStartsWith = limitEsfName.charAt(0) == Literals.SEARCH_STARTS_WITH_PREFIX;
					if ( searchStartsWith ) {
						limitEsfName = limitEsfName.substring(1);
					}
					container.addContainerFilter("esfName", limitEsfName, true, searchStartsWith);
				}
				boolean isStatusEnabled = searchEnabled.getValue();
				boolean isStatusDisabled = searchDisabled.getValue();
				if ( (isStatusEnabled && isStatusDisabled) || (! isStatusEnabled && ! isStatusDisabled) ) {
					; // don't filter on status, both set or neither set
				} else if ( isStatusEnabled ){
					container.addContainerFilter("status", Literals.STATUS_ENABLED, false, false);
				} else {
					container.addContainerFilter("status", Literals.STATUS_DISABLED, false, false);
				}
				
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				vaadinUi.showStatus(vaadinUi.getMsg("list.search.filterButton.message",container.size()));
			}
		});
    	buttonGroup.addButton(filterButton);
    	
    	showAllButton = new Button(vaadinUi.getMsg("LibImageList.searchBar.showAllButton.label"));
    	showAllButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.showAll.icon")));
    	showAllButton.setDescription(vaadinUi.getMsg("button.showAll.tooltip"));
    	showAllButton.setStyleName(Reindeer.BUTTON_SMALL);
    	showAllButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = -4589933537839050473L;

			@Override
			public void buttonClick(ClickEvent event) {
				view.unselectAllImages();
				container.removeAllContainerFilters();
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				vaadinUi.showStatus(vaadinUi.getMsg("list.search.showAllButton.message",container.size()));
			}
		});
    	buttonGroup.addButton(showAllButton);
    	
    	table = new LibImageTable(view, container);
    	
    	layout.addComponent(searchBar);
    	layout.addComponent(table);
    	layout.setExpandRatio(table, 1);
	}
	
	public Table getTable() {
		return table;
	}
	
	protected BeanItem<LibImageBean> createNewBeanItem()
	{
		ImageInfo image = ImageInfo.Manager.createNew(view.getLibrary().getId());
		LibImageBean bean = new LibImageBean(image);
		return new BeanItem<LibImageBean>(bean);
	}
	
	class LibImageTable extends Table {
		private static final long serialVersionUID = 3082740381440904254L;

		public LibImageTable(LibImageAndVersionsMainView view, LibImageBeanContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("LibImageList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("LibImageList.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setColumnExpandRatio("esfName", 1.0f);
			setColumnAlignment("productionVersion",Align.CENTER);
			setColumnAlignment("testVersion",Align.CENTER);
			setCellStyleGenerator(new Table.CellStyleGenerator( ) {
				private static final long serialVersionUID = -7573281313388361138L;

				@Override
				public String getStyle(Table table, Object itemId, Object propertyId) {
					LibImageBean bean = (LibImageBean)itemId;
					return bean.isEnabled() ? null : "disabledText";
				}
			});
	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener((Property.ValueChangeListener)view);
	        setNullSelectionAllowed(true);
			setSizeFull();
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			if ( "productionVersion".equals(colId) ) {
				int productionVersion = ((LibImageBean)rowId).imageInfo().getProductionVersion();
				return productionVersion > 0 ? versionFormat.format(productionVersion) : "";
			}
			if ( "testVersion".equals(colId) ) {
				int productionVersion = ((LibImageBean)rowId).imageInfo().getProductionVersion();
				int testVersion = ((LibImageBean)rowId).imageInfo().getTestVersion();
				return testVersion > productionVersion ? versionFormat.format(testVersion) : "";
			}
			
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // LibImageTable

} // LibImageList
