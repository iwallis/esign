// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.file;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;

import org.vaadin.dialogs.ConfirmDialog;

import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.widget.UploadFileWithProgress;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.WebBrowser;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * The LibFileVersionForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class LibFileVersionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 5514080598651887561L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibFileVersionForm.class);
	
	final LibFileVersionForm thisForm;
	final LibFileAndVersionsMainView view;
	LibFileVersionBeanContainer container;
	GridLayout layout;
	
	Label id;
	Label createdByInfo;
	Label lastUpdatedByInfo;
	Label versionInfo;
	Label fileInfo;
	Link downloadFileLink;
	UploadFileWithProgress uploadFile;
	
	Button testToProdButton;
	Button createTestFromProdButton;
	Button revertProdToTestButton;
	Button deleteButton;

	public LibFileVersionForm(LibFileAndVersionsMainView view, LibFileVersionBeanContainer container) {
    	setStyleName("LibFileVersionForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.view = view;
    	this.container = container;
    	this.thisForm = this;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);

    	layout = new GridLayout(2,6);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	//layout.setColumnExpandRatio(0, 0.8f);
    	//layout.setColumnExpandRatio(1, 0.2f);
    	setLayout(layout);
		
		versionInfo = new Label();
		versionInfo.setContentMode(ContentMode.TEXT);	
		layout.addComponent(versionInfo,0,0,1,0);
		
		lastUpdatedByInfo = new Label();
		lastUpdatedByInfo.setContentMode(ContentMode.TEXT);
		lastUpdatedByInfo.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedByInfo,0,1,1,1);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,2,1,2);		

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id,0,3,1,3);
		
		fileInfo = new Label();
		fileInfo.setStyleName("fileInfo");
		fileInfo.setContentMode(ContentMode.TEXT);
		layout.addComponent(fileInfo,0,4,1,4);
		
		// TODO: 100MB max for a file in our library, make tunable
		uploadFile = new UploadFileWithProgress(null,vaadinUi.getMsg("LibFileVersionForm.button.uploadFile.nofile.label"),(int)vaadinUi.getEsfapp().getUploadFileMaxBytes()) {
			private static final long serialVersionUID = 3213151045057111105L;

			@Override
			public void afterUploadSucceeded() {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				LibFileVersionBean currBean = getCurrentBean();
				byte[] fileData = uploadFile.getFileData();
				currBean.setFileData(fileData);
				currBean.setFileFileName(uploadFile.getFileName());
				currBean.setFileMimeType(uploadFile.getMimeType());
				String sizeWithUnits = EsfInteger.byteSizeInUnits(uploadFile.getFileData().length);
				vaadinUi.showStatus(vaadinUi.getMsg("LibFileVersionForm.upload.successful",uploadFile.getFileName(),sizeWithUnits ));
				fileInfo.setValue(uploadFile.getFileName() + " (" + sizeWithUnits + ")");
				fileInfo.removeStyleName("fileInfoError");
				uploadFile.setButtonCaption(vaadinUi.getMsg("LibFileVersionForm.button.uploadFile.hasfile.label"));
				downloadFileLink.setResource(new com.vaadin.server.ExternalResource(currBean.getFileByIdUrl()));
				downloadFileLink.setVisible(true);
			}
			@Override
			public void afterUploadFailed(String fileName, boolean wasCanceled, boolean wasInvalidMimeType, boolean wasTooBig, int contentLength, int maxSize) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				if ( wasTooBig ) {
					vaadinUi.showError(
							vaadinUi.getMsg("LibFileVersionForm.fileTooLarge.caption"), 
							vaadinUi.getMsg("LibFileVersionForm.fileTooLarge.message",EsfInteger.byteSizeInUnits(contentLength),EsfInteger.byteSizeInUnits(maxSize))
							);
				} else if ( wasCanceled ) {
					vaadinUi.showStatus(vaadinUi.getMsg("LibFileVersionForm.upload.canceled"));
				} else if ( wasInvalidMimeType ) {
					vaadinUi.showError(
							vaadinUi.getMsg("LibFileVersionForm.invalidType.caption"), 
							vaadinUi.getMsg("LibFileVersionForm.invalidType.message",EsfString.isBlank(fileName)?"(unknown)":fileName)
							);
				}
			}
		}; 
		uploadFile.setVisible(false);
		layout.addComponent(uploadFile,0,5);
		
		WebBrowser wb = Page.getCurrent().getWebBrowser();
		downloadFileLink = new Link();
		downloadFileLink.setCaption(vaadinUi.getMsg("LibFileVersionForm.link.downloadFile.label"));
		downloadFileLink.setTargetName(wb.isChrome() || wb.isSafari() ? "_top" : "_blank");
		downloadFileLink.setTargetWidth(300);
		downloadFileLink.setTargetHeight(300);
		downloadFileLink.setTargetBorder(Link.TARGET_BORDER_NONE);
		layout.addComponent(downloadFileLink,1,5);
		downloadFileLink.setVisible(false);
		
		HorizontalLayout footer = new HorizontalLayout();
		footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);
    	
    	testToProdButton = new Button(vaadinUi.getMsg("LibFileVersionForm.button.testToProd.label"), (ClickListener)this);
    	testToProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibFileVersionForm.button.testToProd.icon")));
    	testToProdButton.setDescription(vaadinUi.getMsg("LibFileVersionForm.button.testToProd.tooltip"));
    	footer.addComponent(testToProdButton);

    	createTestFromProdButton = new Button(vaadinUi.getMsg("LibFileVersionForm.button.createTestFromProd.label"), (ClickListener)this);
    	createTestFromProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibFileVersionForm.button.createTestFromProd.icon")));
    	createTestFromProdButton.setDescription(vaadinUi.getMsg("LibFileVersionForm.button.createTestFromProd.tooltip"));
    	footer.addComponent(createTestFromProdButton);

    	revertProdToTestButton = new Button(vaadinUi.getMsg("LibFileVersionForm.button.revertProdToTest.label"), (ClickListener)this);
    	revertProdToTestButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibFileVersionForm.button.revertProdToTest.icon")));
    	revertProdToTestButton.setDescription(vaadinUi.getMsg("LibFileVersionForm.button.revertProdToTest.tooltip"));
    	footer.addComponent(revertProdToTestButton);

    	if ( view.hasPermDelete() ) {
    		deleteButton = new Button(vaadinUi.getMsg("LibFileVersionForm.button.delete.label"), (ClickListener)this);
    		deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibFileVersionForm.button.delete.icon")));
    		deleteButton.setDescription(vaadinUi.getMsg("LibFileVersionForm.button.delete.tooltip"));
	    	deleteButton.addStyleName("deleteButton");
	    	deleteButton.addStyleName("caution");
	    	footer.addComponent(deleteButton);
		}
    	
    	setFooter(footer);
    	setReadOnly(true);   	

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -7772021673770906797L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				_logger.debug("createField: " + propertyId);
				
				Field field = super.createField(item, propertyId, uiContext);
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
    }
    
    @Override
	public void buttonClick(ClickEvent event) {
    	LibFileVersionBean currBean = getCurrentBean();
    	if ( currBean == null ) return;
    	
    	Button source = event.getButton();
    	if ( source == testToProdButton ) {
    		view.promoteTestToProductionVersion();
    	} else if ( source == createTestFromProdButton ) {
    		view.createNextVersion();
    	} else if ( source == revertProdToTestButton ) {
    		view.revertProductionVersionBackToTest();
    	} else if ( source == deleteButton ) {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	ConfirmDialog.show(UI.getCurrent(), 
	    			vaadinUi.getMsg("LibFileVersionForm.button.delete.ConfirmDialog.caption"),
	    			vaadinUi.getMsg("LibFileVersionForm.button.delete.ConfirmDialog.message"),
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
	    	        new ConfirmDialog.Listener() {
						private static final long serialVersionUID = 4482216810408894439L;

						public void onClose(ConfirmDialog dialog) {
	    	                if (dialog.isConfirmed()) {
	    	                    view.delete();
	    	                }
	    	            }
	    	        });
    	}
     }
	
    LibFileVersionBean save(Connection con) throws SQLException {
    	LibFileVersionBean currBean = getCurrentBean();
    	if ( currBean != null ) {
    		if ( currBean.save(con,null) ) {
    			return currBean;
    		}
    	}
    	return null;
    }
    	
    public String getCurrentBeanName() {
    	LibFileVersionBean currBean = getCurrentBean();
		return currBean == null || currBean.fileVersion() == null ? "(None)" : view.getEsfNameWithVersion();
	}
    
	LibFileVersionBean getCurrentBean() {
		LibFileVersionBean currBean = getBean(getItemDataSource());
    	return currBean == null || currBean.fileVersion() == null ? null : currBean;
    }
    @SuppressWarnings("unchecked")
	public LibFileVersionBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibFileVersionBean> bi = (BeanItem<LibFileVersionBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached stuff
		resetCaches();
		
    	if (newDataSource != null) {
    		LibFileVersionBean bean = getBean(newDataSource);
    		
    		super.setItemDataSource(newDataSource, Arrays.asList());
    		
    		setupForm(bean);
    		
    		setReadOnly( ! view.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	

    @Override
    public void setReadOnly(boolean readOnly) {   	
    	LibFileVersionBean bean = getCurrentBean();
    	
    	boolean isTestVersion = bean != null && view.isTestVersion(bean.fileVerInfo());
    	
    	testToProdButton.setVisible(bean != null && !readOnly && isTestVersion && bean.fileVersion().doUpdate() && view.getLibFileBean().isEnabled());
    	createTestFromProdButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.fileVerInfo()) && !view.hasTestVersion());
    	revertProdToTestButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.fileVerInfo()) && !view.hasTestVersion());
    	
    	uploadFile.setVisible(bean != null && isTestVersion && !readOnly);
    	
    	// Show this only if the object is created and is the Test version
    	if ( deleteButton != null ) {
    		deleteButton.setVisible(bean != null && bean.fileVersion().doUpdate() && isTestVersion); 
    	}

    	// If the request is to be not-readonly, but this is not the test version, we convert it back to readonly
    	if ( !readOnly && bean != null && !view.isTestVersion(bean.fileVerInfo())) {
    		readOnly = true;
    	}
    	super.setReadOnly(readOnly);
    }
    
    void setupForm(LibFileVersionBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("LibFileVersionForm.label.id",bean.getId()) );
		
		versionInfo.setValue( vaadinUi.getMsg("LibFileVersionForm.label.version",bean.getVersion(),view.getVersionLabel(bean.fileVerInfo())) );
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.fileVerInfo().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("LibFileVersionForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );
		
		if ( bean.fileVersion().doInsert() ) {
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibFileVersionForm.label.lastUpdated.whenNew") );
			lastUpdatedByInfo.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.fileVerInfo().getLastUpdatedByUserId());
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibFileVersionForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedByInfo.setIcon(null);
		}
		
		if ( bean.hasFileFileName() ) {
			fileInfo.setValue(bean.getFileFileName() + " (" + EsfInteger.byteSizeInUnits(bean.getFileSize()) + ")");
			fileInfo.removeStyleName("fileInfoError");
			uploadFile.setButtonCaption(vaadinUi.getMsg("LibFileVersionForm.button.uploadFile.hasfile.label"));
			downloadFileLink.setVisible(true);
			downloadFileLink.setResource(new ExternalResource(bean.getFileByIdUrl()));
		} else {
			fileInfo.setValue(vaadinUi.getMsg("LibFileVersionForm.label.fileInfo.nofile"));
			fileInfo.addStyleName("fileInfoError");
			uploadFile.setButtonCaption(vaadinUi.getMsg("LibFileVersionForm.button.uploadFile.nofile.label"));
			downloadFileLink.setVisible(false);
		}
    }

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}
	
	@Override
	public boolean isValid() {
		if ( ! super.isValid() )
			return false;
		LibFileVersionBean bean = getCurrentBean();
		return bean.isFileVersionReady();
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
    }


	@Override
	public String checkDirty() { // OBSOLETE SINCE VERSION FORM HAS NO BUTTONS FOR SAVING, CANCELING
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibFileAndVersionsMainView.Image.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		LibFileVersionBean bean = getCurrentBean();
		return isModified() || (bean != null && bean.hasChanged()); // handles uploaded images that are not part of the form, but have changed our underlying object
	}
	
	public void discard() {
		super.discard();
		LibFileVersionBean bean = getCurrentBean();
		if ( bean != null ) {
			bean.discard();
			if ( bean.fileVersion() != null ) {
				BeanItem<LibFileVersionBean> bi = new BeanItem<LibFileVersionBean>(bean);
				setItemDataSource(bi);
			}
		}
	}
}