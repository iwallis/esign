// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.esignforms.open.data.EsfHtml;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DocumentVersionPage;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.libraryProgramming.field.DocumentFieldForm;
import com.esignforms.open.vaadin.config.libraryProgramming.field.LibFieldBean;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * LibDocumentVersionPageView is a splitpanel that contains the document version page form in the left and various options on the right.
 * 
 * @author Yozons
 *
 */
public class LibDocumentVersionPageView extends HorizontalSplitPanel implements EsfView {
	private static final long serialVersionUID = 7846905523621949472L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibDocumentVersionPageView.class);

    final LibDocumentAndVersionsMainView mainView;
    final LibDocumentVersionPageView thisView;
    final LibDocumentVersionForm documentVersionForm;
    final LibDocumentVersionBean docVerBean;
    public final static int TABSHEET_TABLE_PAGELENGTH = 25;
    FieldList fieldList;
    PartyList partyList;
    FileList fileList;
    ImageList imageList;
    DropdownList dropdownList;
    DocumentVersionPage docVerPage;
    EsfName pageName;
    int pageNumber;
    
    // On the left side
    LibDocumentVersionPageForm pageForm;
	
    // On the right side
    TabSheet optionsTabSheet;
    


	public LibDocumentVersionPageView(LibDocumentAndVersionsMainView mainViewParam, LibDocumentVersionForm documentVersionForm, LibDocumentVersionBean docVerBean, EsfName pageName, int pageNumber) {
		super(); 
		setSizeFull();
		this.setStyleName("LibDocumentVersionPageView");
		this.mainView = mainViewParam;
		thisView = this;
		this.documentVersionForm = documentVersionForm;
		this.docVerBean = docVerBean;
		this.pageName = pageName;
		this.pageNumber = pageNumber;
		refreshDocumentVersionPage();
	}
	
	protected void refreshDocumentVersionPage() {
		docVerPage = docVerBean.pageByName(pageName);
		if ( docVerPage == null ) {
			docVerPage = docVerBean.pageNumber(pageNumber);
			pageName = docVerPage.getEsfName();
   	 	}
	}
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
   	 		PropertysetItem pageItem = new PropertysetItem();
   	 		pageItem.addItemProperty("id", new ObjectProperty<EsfUUID>(docVerPage.getId(),EsfUUID.class));
   	 		pageItem.addItemProperty("esfName", new ObjectProperty<EsfName>(docVerPage.getEsfName(),EsfName.class));
   	 		pageItem.addItemProperty("editReviewType", new ObjectProperty<String>(docVerPage.getEditReviewType(),String.class));
   	 		pageItem.addItemProperty("defaultNewFieldParty", new ObjectProperty<EsfName>(null,EsfName.class));
   	 		pageItem.addItemProperty("html", new ObjectProperty<EsfHtml>(docVerPage.getHtml(),EsfHtml.class));

   	 		pageForm = new LibDocumentVersionPageForm(this);
            pageForm.initView();
   	 		pageForm.setItemDataSource(pageItem);
            pageForm.setReadOnly(thisView.isReadOnly());
            pageForm.activateView(EsfView.OpenMode.WINDOW, "");
			
            optionsTabSheet = new TabSheet();
            optionsTabSheet.setStyleName(Reindeer.TABSHEET_SMALL);
            optionsTabSheet.addStyleName(Reindeer.TABSHEET_BORDERLESS);
            optionsTabSheet.setSizeFull();

            VerticalLayout tabSheetLayout = new VerticalLayout();
            tabSheetLayout.setSizeFull();
            tabSheetLayout.addComponent(optionsTabSheet);
            
            fieldList = new FieldList();
            optionsTabSheet.addTab(fieldList, vaadinUi.getMsg("LibDocumentVersionPageView.FieldList.label",0), new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.FieldList.icon")));
            
            partyList = new PartyList();
            optionsTabSheet.addTab(partyList, vaadinUi.getMsg("LibDocumentVersionPageView.PartyList.label",0), new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.PartyList.icon")));
            
            fileList = new FileList();
            optionsTabSheet.addTab(fileList, vaadinUi.getMsg("LibDocumentVersionPageView.FileList.label",0), new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.FileList.icon")));

            imageList = new ImageList();
            optionsTabSheet.addTab(imageList, vaadinUi.getMsg("LibDocumentVersionPageView.ImageList.label",0), new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.ImageList.icon")));
            
            dropdownList = new DropdownList();
            optionsTabSheet.addTab(dropdownList, vaadinUi.getMsg("LibDocumentVersionPageView.DropdownList.label",0), new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.DropdownList.icon")));
            
            refreshAllOptionLists();
            
            setFirstComponent(pageForm);
			setSecondComponent(tabSheetLayout);
			setSplitPosition(490,Unit.PIXELS,true); // want all tabs to be visible
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Document and versions main view exception", e.getMessage());
		}
	}
	
	@Override
	public void detach() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance(); 
		ConfirmDiscardFormChangesWindow w = vaadinUi.getConfirmDiscardFormChangesWindow(this);
 		documentVersionForm.pageFormClosed(w);
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		_logger.debug("activateView() - params: " + params);
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return pageForm.checkDirty();
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return pageForm.isDirty() || docVerBean.isModified();
	}
	
	public void commit() {
		try {
			pageForm.fieldGroup.commit();
			Item currItem = pageForm.fieldGroup.getItemDataSource();
			docVerPage.setEsfName( (EsfName)currItem.getItemProperty("esfName").getValue() );
			docVerPage.setEditReviewType( (String)currItem.getItemProperty("editReviewType").getValue() );
			docVerPage.setDefaultNewFieldParty( (EsfName)currItem.getItemProperty("defaultNewFieldParty").getValue() );
			docVerPage.setHtml( (EsfHtml)currItem.getItemProperty("html").getValue() );
		} catch( Exception e ) {
			_logger.error("commit()",e);
		}
	}
	
    public void save() {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try	{
			documentVersionForm.commit();
			documentVersionForm.save(con);
			con.commit();
			
	    	LibDocumentVersionBean bean = documentVersionForm.getCurrentBean();
	    	if ( bean != null ) {
	    		bean.loadDuplicateDocumentVersion();
	    	}
			
			refreshDocumentVersionPage();
			refreshAllOptionLists();
		} catch(SQLException e) {
			vaadinUi.getEsfapp().sqlerr(e, "Save document page");
			pool.rollbackIgnoreException(con,e);
		} catch (CommitException e) {
			_logger.error("save(commit failed)",e);
			pool.rollbackIgnoreException(con);
		} catch( RuntimeException e ) {
			_logger.error("save()",e);
			throw e;
		} finally {
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
    }
    
    void refreshAllOptionLists() {
    	if ( fieldList != null )
    		fieldList.refresh();
    	if ( partyList != null )
    		partyList.refresh();
    	if ( fileList != null )
    		fileList.refresh();
    	if ( imageList != null )
    		imageList.refresh();
    	if ( dropdownList != null )
    		dropdownList.refresh();
    }

    public void close() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance(); 
		ConfirmDiscardFormChangesWindow w = vaadinUi.getConfirmDiscardFormChangesWindow(this);
		if ( w != null )
			w.close();
    }
    
    public void discardAndClose() {
    	pageForm.fieldGroup.discard();
    	documentVersionForm.discard();
		close();
    }
    
    
    // THIS IS THE FIELD TABLE that lists all fields in the document
    class FieldList extends Table {
		private static final long serialVersionUID = 6991354789388876034L;

		final FieldList thisList;
    	final IndexedContainer container;
    	
    	public FieldList() {
    		thisList = this;
    		this.setStyleName("fieldList");
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		setCaption(vaadinUi.getMsg("LibDocumentVersionPageView.FieldList.label"));
    		setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.FieldList.icon")));
    		setWidth(100,Unit.PERCENTAGE);
			setPageLength(TABSHEET_TABLE_PAGELENGTH);
    		
    		container = new IndexedContainer();
    		container.addContainerProperty("esfName", EsfName.class, null);
    		container.addContainerProperty("type", String.class, null);
    		container.addContainerProperty("containerReferenceCount", Short.class, null);
    		setContainerDataSource(container);
    		
			setVisibleColumns((Object[])vaadinUi.getStringArray("LibDocumentVersionPageView.FieldList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("LibDocumentVersionPageView.FieldList.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setColumnAlignment("containerReferenceCount", Align.CENTER);
			//this.setColumnWidth("esfName", 120);
	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener( new Property.ValueChangeListener() {
				private static final long serialVersionUID = 5591863115481543448L;

				@Override
				public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
					EsfName fieldName = (EsfName)getValue();
					if ( fieldName == null ) return; // unselected
					openWindow(docVerBean.duplicateDocumentVersion().getFieldTemplate(fieldName));
				}
	        	
	        });
	        setNullSelectionAllowed(true);
    	}
    	
     	public void openWindow(FieldTemplate fieldTemplate) {
    		if ( fieldTemplate == null ) return;
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		LibFieldBean fieldBean = new LibFieldBean(fieldTemplate);
    		BeanItem<LibFieldBean> fieldBeanItem = new BeanItem<LibFieldBean>(fieldBean);
    		
    		final DocumentFieldForm form = new DocumentFieldForm(docVerBean,FieldTemplate.Manager.getAll(mainView.getLibrary().getId()));
            form.initView(); 
            String pageTitle = vaadinUi.getMsg("LibDocumentVersionPageView.FieldForm.window.caption",fieldTemplate.getEsfName());
            
            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(pageTitle, form, false);
            w.center();
        	w.setWidth(700, Unit.PIXELS);
        	w.setHeight(600, Unit.PIXELS);
			w.setModal(true);
			w.addCloseListener( new CloseListener() {
				private static final long serialVersionUID = -8772298723925020359L;

				@Override
				public void windowClose(CloseEvent e) {
					thisList.select(null);
					thisList.refresh();
					
					// Try to get us positioned on so we can work on the field we did, or if deleted, the previous one.
					Object lastFieldId = form.getLastDocumentFieldWorkedOn();
					int lastFieldItemIndex = getItemIndex(lastFieldId);
					if ( lastFieldItemIndex != -1 ) {
						thisList.setCurrentPageFirstItemIndex(lastFieldItemIndex);
					} else {
						Object prevFieldId = getPreviousItemId(lastFieldId);
						if ( prevFieldId != null ) {
							int prevFieldItemIndex = getItemIndex(prevFieldId);
							if ( prevFieldItemIndex != -1 ) {
								thisList.setCurrentPageFirstItemIndex(prevFieldItemIndex);
							}
						}
					}
				}
			});
        	form.setParentWindow(w);
        	form.activateView(EsfView.OpenMode.WINDOW, "");
        	form.setItemDataSource(fieldBeanItem);
        	form.setReadOnly(thisView.isReadOnly());
			vaadinUi.addWindowToUI(w);
    	}

    	public void refresh() {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		removeAllItems();
    		
    		Map<EsfName,FieldTemplate> fieldTemplateMap = docVerBean.duplicateDocumentVersion().getFieldTemplateMap();
    		
    		for( EsfName name : fieldTemplateMap.keySet() ) {
    			FieldTemplate ft = fieldTemplateMap.get(name);
    			if ( ft != null ) {
        			Item item = addItem(ft.getEsfName());
        			if ( item != null ) {
            			item.getItemProperty("esfName").setValue(ft.getEsfName());
            			item.getItemProperty("type").setValue(ft.getType());
            			item.getItemProperty("containerReferenceCount").setValue(new Short(ft.getContainerReferenceCount()));
        			} else {
        				_logger.warn("FieldList.refresh() - Couldn't addItem to table for fieldTemplate name: " + ft.getEsfName() + "; field template id: " + ft.getId());
        			}
    			} else {
    				_logger.warn("FieldList.refresh() - Couldn't retrieve field from map with name: " + name);
    			}
    		}
    		
			Tab tab = optionsTabSheet.getTab(this);
			if ( tab != null ) {
				tab.setCaption(vaadinUi.getMsg("LibDocumentVersionPageView.FieldList.label",container.size()));
			}
    	}
    	
    	int getItemIndex(Object itemId) {
    		for( int i=0; i < size(); ++i ) {
    			Object id = getIdByIndex(i);
    			if ( itemId.equals(id) )
    				return i;
    		}
    		return -1;
    	}
    	Object getPreviousItemId(Object itemId) {
    		EsfName itemIdAsName = (EsfName)itemId;
    		Object prev = null;
    		for( int i=0; i < size(); ++i ) {
    			Object id = getIdByIndex(i);
    			EsfName idAsName = (EsfName)id;
    			if ( idAsName.compareTo(itemIdAsName) >= 0 )
    				break;
    			prev = id;
    		}
    		return prev;
    	}
    	
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			if ( "type".equals(colId) ) {
				Object value = property.getValue();
				// not sure why we get these, but we do on refreshing the table it seems
				if ( value != null ) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					return vaadinUi.getPrettyCode().fieldTemplateType(value.toString());
				}
			}
			return super.formatPropertyValue(rowId,colId,property);
		}
    }

  
    
    // THIS IS THE PARTY TABLE that lists all parties in the document ******************************************************************************
    class PartyList extends Table {
 		private static final long serialVersionUID = 1740396366166417261L;
		
 		final PartyList thisList;
    	final IndexedContainer container;
    	
    	public PartyList() {
    		thisList = this;
    		this.setStyleName("partyList");
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		setCaption(vaadinUi.getMsg("LibDocumentVersionPageView.PartyList.label"));
    		setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.PartyList.icon")));
    		setWidth(100,Unit.PERCENTAGE);
			setPageLength(TABSHEET_TABLE_PAGELENGTH);
    		
    		container = new IndexedContainer();
    		container.addContainerProperty("esfName", EsfName.class, null);
    		container.addContainerProperty("processOrder", Short.class, null);
    		setContainerDataSource(container);
    		
			setVisibleColumns((Object[])vaadinUi.getStringArray("LibDocumentVersionPageView.PartyList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("LibDocumentVersionPageView.PartyList.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setColumnAlignment("processOrder", Align.CENTER);
			//setColumnWidth("esfName", 120);
	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener( new Property.ValueChangeListener() {
				private static final long serialVersionUID = -3249738227316971538L;

				@Override
				public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
					EsfName partyName = (EsfName)getValue();
					if ( partyName == null ) return; // unselected
					openWindow(docVerBean.duplicateDocumentVersion().getPartyTemplate(partyName));
				}
	        	
	        });
	        setNullSelectionAllowed(true);
	        
	        // We let them drag to reorder
	        setSortEnabled(false);
	        setDragMode( thisView.isReadOnly() ? TableDragMode.NONE : TableDragMode.ROW );
			setDropHandler(new DropHandler() {
				private static final long serialVersionUID = 3000589397066360704L;

				@SuppressWarnings("unchecked")
				public void drop(DragAndDropEvent dropEvent) {
	                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
	                EsfName sourceItemId = (EsfName)t.getItemId();

	                AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails) dropEvent.getTargetDetails());
	                EsfName targetItemId = (EsfName)dropData.getItemIdOver();
	                
	                // No move if source and target are the same
	                if ( sourceItemId == targetItemId || targetItemId == null)
	                	return;
	                
	                // Let's remove the source of the drag so we can add it back where requested...
	                removeItem(sourceItemId);
	            	if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
	            		addItemAfter(targetItemId,sourceItemId);
	            	} else {
	                    Object prevItemId = container.prevItemId(targetItemId);
	                    addItemAfter(prevItemId, sourceItemId);
	            	}
	            	
	            	thisList.savePartyOrderChanged( (Collection<EsfName>)getItemIds() );
	            }

	            public AcceptCriterion getAcceptCriterion() {
	                return new And(SourceIsTarget.get(), new Not(AbstractSelect.VerticalLocationIs.MIDDLE));
	            }
	        });
    	}
    	
    	public void refresh() {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		removeAllItems();
    		
    		List<PartyTemplate> partyTemplateList = docVerBean.duplicateDocumentVersion().getPartyTemplateList();
    		
    		for( PartyTemplate pt : partyTemplateList ) {
    			Item item = addItem(pt.getEsfName());
    			if ( item != null ) {
        			item.getItemProperty("esfName").setValue(pt.getEsfName());
        			item.getItemProperty("processOrder").setValue(new Short(pt.getProcessOrder()));
    			} else {
    				_logger.warn("PartyList.refresh() - Couldn't addItem to table for partyTemplate name: " + pt.getEsfName() + "; party template id: " + pt.getId());
    			}
    		}
    		
    		pageForm.loadDefaultNewFieldPartySelect();
			Tab tab = optionsTabSheet.getTab(this);
			if ( tab != null ) {
				tab.setCaption(vaadinUi.getMsg("LibDocumentVersionPageView.PartyList.label",container.size()));
			}
    	}
    	
    	public void savePartyOrderChanged(java.util.Collection<EsfName> newNameOrder) {
			LinkedList<EsfName> partyOrderNames = new LinkedList<EsfName>();
			for( EsfName name : newNameOrder ) {
				partyOrderNames.add( name );
			}
			docVerBean.duplicateDocumentVersion().setPartyTemplateOrder(partyOrderNames);
			refresh();
    	}
    	
    	public void openWindow(PartyTemplate partyTemplate) {
    		if ( partyTemplate == null ) return;
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			final PartyForm form = new PartyForm(mainView.getLibrary(), docVerBean, partyTemplate);
            form.initView(); 
            String pageTitle = vaadinUi.getMsg("LibDocumentVersionPageView.PartyForm.window.caption",partyTemplate.getEsfName());
            
            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(pageTitle, form, false);
            w.center();
        	w.setWidth(600, Unit.PIXELS);
        	w.setHeight(685, Unit.PIXELS);
			w.setModal(true);
			w.addCloseListener( new CloseListener() {
				private static final long serialVersionUID = 1807490179425910340L;

				@Override
				public void windowClose(CloseEvent e) {
					thisList.select(null);
					thisList.refresh();
				}
				
			});
        	form.setParentWindow(w);
        	form.activateView(EsfView.OpenMode.WINDOW, "");
        	form.setReadOnly(thisView.isReadOnly());
			vaadinUi.addWindowToUI(w);
    	}
    }

    
    // THIS IS THE FILE TABLE that lists all files in the document ***********************************************************************************
    class FileList extends Table {
		private static final long serialVersionUID = -2474090285220746700L;
 
		final FileList thisList;
    	final IndexedContainer container;
    	
    	public FileList() {
    		thisList = this;
    		this.setStyleName("fileList");
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		setCaption(vaadinUi.getMsg("LibDocumentVersionPageView.FileList.label"));
    		setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.FileList.icon")));
    		setWidth(100,Unit.PERCENTAGE);
			setPageLength(TABSHEET_TABLE_PAGELENGTH);
    		
    		container = new IndexedContainer();
    		container.addContainerProperty("esfName", EsfName.class, null);
    		container.addContainerProperty("fileFileName", String.class, null);
    		setContainerDataSource(container);
    		
			setVisibleColumns((Object[])vaadinUi.getStringArray("LibDocumentVersionPageView.FileList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("LibDocumentVersionPageView.FileList.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			//this.setColumnWidth("esfName", 120);
	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener( new Property.ValueChangeListener() {
				private static final long serialVersionUID = 3214095379417270298L;

				@Override
				public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
					EsfName fileName = (EsfName)getValue();
					if ( fileName == null ) return; // unselected
					openWindow(docVerBean.duplicateDocumentVersion().getDocumentFile(fileName));
				}
	        	
	        });
	        setNullSelectionAllowed(true);
    	}
    	
    	public void refresh() {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		removeAllItems();
    		
    		List<DocumentVersion.DocumentFile> list = docVerBean.duplicateDocumentVersion().getDocumentFileList();
    		
    		for( DocumentVersion.DocumentFile df : list ) {
    			Item item = addItem(df.file.getEsfName());
    			if ( item != null ) {
        			item.getItemProperty("esfName").setValue(df.file.getEsfName());
        			item.getItemProperty("fileFileName").setValue(df.fileVersion.getFileFileName());
    			} else {
    				_logger.warn("FileList.refresh() - Couldn't addItem to table for documentFile name: " + df.file.getEsfName() + "; document file id: " + df.file.getId());
    			}
    		}
    		
			Tab tab = optionsTabSheet.getTab(this);
			if ( tab != null ) {
				tab.setCaption(vaadinUi.getMsg("LibDocumentVersionPageView.FileList.label",container.size()));
			}
    	}
        
    	public void openWindow(DocumentVersion.DocumentFile df) {
    		if ( df == null ) return;
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		final DocumentFileForm form = new DocumentFileForm(mainView.getLibrary(), docVerBean, df);
            form.initView(); 
            String pageTitle = vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.window.caption",df.file.getEsfName());
            
            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(pageTitle, form, false);
            w.center();
        	w.setWidth(600, Unit.PIXELS);
        	w.setHeight(550, Unit.PIXELS);
    		w.setModal(true);
    		w.addCloseListener( new CloseListener() {
				private static final long serialVersionUID = 1265218039737308369L;

				@Override
    			public void windowClose(CloseEvent e) {
    				thisList.select(null);
    				thisList.refresh();
    			}
    			
    		});
        	form.setParentWindow(w);
        	form.activateView(EsfView.OpenMode.WINDOW, "");
        	form.setReadOnly(thisView.isReadOnly());
    		vaadinUi.addWindowToUI(w);
    	}
    }
    
    // THIS IS THE IMAGE TABLE that lists all images in the document ***********************************************************************************
    class ImageList extends Table {
		private static final long serialVersionUID = 8094493704155632637L;

		final ImageList thisList;
    	final IndexedContainer container;
    	
    	public ImageList() {
    		thisList = this;
    		this.setStyleName("imageList");
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		setCaption(vaadinUi.getMsg("LibDocumentVersionPageView.ImageList.label"));
    		setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.ImageList.icon")));
    		setWidth(100,Unit.PERCENTAGE);
    		setPageLength(TABSHEET_TABLE_PAGELENGTH);
    		
    		container = new IndexedContainer();
    		container.addContainerProperty("esfName", EsfName.class, null);
    		container.addContainerProperty("imageFileName", String.class, null);
    		setContainerDataSource(container);
    		
			setVisibleColumns((Object[])vaadinUi.getStringArray("LibDocumentVersionPageView.ImageList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("LibDocumentVersionPageView.ImageList.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			//this.setColumnWidth("esfName", 120);
	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener( new Property.ValueChangeListener() {
				private static final long serialVersionUID = 2859805552559447909L;

				@Override
				public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
					EsfName imageName = (EsfName)getValue();
					if ( imageName == null ) return; // unselected
					openWindow(docVerBean.duplicateDocumentVersion().getDocumentImage(imageName));
				}
	        	
	        });
	        setNullSelectionAllowed(true);
    	}
    	
    	public void refresh() {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		removeAllItems();
    		
    		List<DocumentVersion.DocumentImage> list = docVerBean.duplicateDocumentVersion().getDocumentImageList();
    		
    		for( DocumentVersion.DocumentImage di : list ) {
    			Item item = addItem(di.image.getEsfName());
    			if ( item != null ) {
        			item.getItemProperty("esfName").setValue(di.image.getEsfName());
        			item.getItemProperty("imageFileName").setValue(di.imageVersion.getImageFileName());
    			} else {
    				_logger.warn("ImageList.refresh() - Couldn't addItem to table for documentImage name: " + di.image.getEsfName() + "; document file id: " + di.image.getId());
    			}
    		}
    		
			Tab tab = optionsTabSheet.getTab(this);
			if ( tab != null ) {
				tab.setCaption(vaadinUi.getMsg("LibDocumentVersionPageView.ImageList.label",container.size()));
			}
    	}
        
    	public void openWindow(DocumentVersion.DocumentImage di) {
    		if ( di == null ) return;
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		final DocumentImageForm form = new DocumentImageForm(mainView.getLibrary(), docVerBean, di);
            form.initView(); 
            String pageTitle = vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.window.caption",di.image.getEsfName());
            
            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(pageTitle, form, false);
            w.center();
        	w.setWidth(600, Unit.PIXELS);
        	w.setHeight(600, Unit.PIXELS);
    		w.setModal(true);
    		w.addCloseListener( new CloseListener() {
				private static final long serialVersionUID = 7379804296498556440L;

				@Override
    			public void windowClose(CloseEvent e) {
    				thisList.select(null);
    				thisList.refresh();
    			}
    			
    		});
        	form.setParentWindow(w);
        	form.activateView(EsfView.OpenMode.WINDOW, "");
        	form.setReadOnly(thisView.isReadOnly());
    		vaadinUi.addWindowToUI(w);
    	}
    }
    
    
    // THIS IS THE DROPDOWN TABLE that lists all dropdowns in the document ***********************************************************************************
    class DropdownList extends Table {
		private static final long serialVersionUID = -6637752571852161763L;

		final DropdownList thisList;
    	final IndexedContainer container;
    	
    	public DropdownList() {
    		thisList = this;
    		this.setStyleName("dropdownList");
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		setCaption(vaadinUi.getMsg("LibDocumentVersionPageView.DropdownList.label"));
    		setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.DropdownList.icon")));
    		setWidth(100,Unit.PERCENTAGE);
    		setPageLength(TABSHEET_TABLE_PAGELENGTH);
    		
    		container = new IndexedContainer();
    		container.addContainerProperty("esfName", EsfName.class, null);
    		container.addContainerProperty("numOptions", Integer.class, null);
    		setContainerDataSource(container);
    		
			setVisibleColumns((Object[])vaadinUi.getStringArray("LibDocumentVersionPageView.DropdownList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("LibDocumentVersionPageView.DropdownList.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setColumnAlignment("numOptions", Align.CENTER);
			//this.setColumnWidth("esfName", 120);
	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener( new Property.ValueChangeListener() {
				private static final long serialVersionUID = -7199175228570793229L;

				@Override
				public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
					EsfName dropdownName = (EsfName)getValue();
					if ( dropdownName == null ) return; // unselected
					openWindow(docVerBean.duplicateDocumentVersion().getDocumentDropdown(dropdownName));
				}
	        	
	        });
	        setNullSelectionAllowed(true);
    	}
    	
    	public void refresh() {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		removeAllItems();
    		
    		List<DocumentVersion.DocumentDropdown> list = docVerBean.duplicateDocumentVersion().getDocumentDropdownList();
    		
    		for( DocumentVersion.DocumentDropdown dd : list ) {
    			Item item = addItem(dd.dropdown.getEsfName());
    			if ( item != null ) {
        			item.getItemProperty("esfName").setValue(dd.dropdown.getEsfName());
        			item.getItemProperty("numOptions").setValue(new Integer(dd.dropdownVersion.getNumOptions()));
    			} else {
    				_logger.warn("DropdownList.refresh() - Couldn't addItem to table for dropdown name: " + dd.dropdown.getEsfName() + "; dropdown id: " + dd.dropdown.getId());
    			}
    		}
    		
			Tab tab = optionsTabSheet.getTab(this);
			if ( tab != null ) {
				tab.setCaption(vaadinUi.getMsg("LibDocumentVersionPageView.DropdownList.label",container.size()));
			}
    	}
        
    	public void openWindow(DocumentVersion.DocumentDropdown dd) {
    		if ( dd == null ) return;
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		final DocumentDropdownForm form = new DocumentDropdownForm(mainView.getLibrary(), docVerBean, dd);
            form.initView(); 
            String pageTitle = vaadinUi.getMsg("LibDocumentVersionPageView.DropdownForm.window.caption",dd.dropdown.getEsfName());
            
            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(pageTitle, form, false);
            w.center();
        	w.setWidth(600, Unit.PIXELS);
        	w.setHeight(600, Unit.PIXELS);
    		w.setModal(true);
    		w.addCloseListener( new CloseListener() {
				private static final long serialVersionUID = -7898768097836369960L;

				@Override
    			public void windowClose(CloseEvent e) {
    				thisList.select(null);
    				thisList.refresh();
    			}
    			
    		});
        	form.setParentWindow(w);
        	form.activateView(EsfView.OpenMode.WINDOW, "");
        	form.setReadOnly(thisView.isReadOnly());
    		vaadinUi.addWindowToUI(w);
    	}
    }

}