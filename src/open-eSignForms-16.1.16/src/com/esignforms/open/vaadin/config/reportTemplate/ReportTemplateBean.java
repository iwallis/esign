// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.reportTemplate;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.TreeMap;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;

public class ReportTemplateBean implements Serializable, Comparable<ReportTemplateBean> {
	private static final long serialVersionUID = 5152659573108375613L;

	private EsfUUID reportTemplateId;
	
	private ReportTemplate duplicatedReportTemplate; // we work on a duplicate copy so if we have to abandon our changes, nothing is saved or stored in the cache
	
	// This contains all of the groups that the user can list, so these are the only ones that
	// can be used to add/remove groups (any groups that the Group object has that are not included
	// cannot be touched)
	private EsfUUID[] allGroupListIds;
	private EsfPathName[] allGroupListNames;

	private Set<EsfUUID> permListIds;
	private Set<EsfUUID> permViewDetailsIds;
	private Set<EsfUUID> permCreateLikeIds;
	private Set<EsfUUID> permUpdateIds;
	private Set<EsfUUID> permDeleteIds;
	
	private Set<EsfUUID> permRunReport;
	private Set<EsfUUID> permViewStartedByExternalUsers;
	private Set<EsfUUID> permViewStartedByAnyUser;
	private Set<EsfUUID> permViewAnyUserPartyTo;
	private Set<EsfUUID> permViewProduction;
	private Set<EsfUUID> permDownloadCsv;
	private Set<EsfUUID> permDownloadArchive;
	private Set<EsfUUID> permViewActivityLog;
	private Set<EsfUUID> permViewEmailLog;
	private Set<EsfUUID> permViewSnapshotData;
	private Set<EsfUUID> permViewSnapshotDocument;
	private Set<EsfUUID> permEsfReportsAccessView;
	private Set<EsfUUID> permEsfReportsAccessUpdate;
	
	private EsfUUID[] allTransactionTemplateListIds;
	private EsfPathName[] allTransactionTemplateListNames;

	private Set<EsfUUID> transactionTemplateIds;

	private EsfUUID[] allDocumentListIds;
	private EsfPathName[] allDocumentListNames;

	private Set<EsfUUID> limitDocumentIds;

	private boolean allowSaveToObject = true;
	
	public ReportTemplateBean(ReportTemplate reportTemplate) {
		this.reportTemplateId = reportTemplate.getId();
		this.resetCaches();
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o !=null && o instanceof ReportTemplateBean )
			return duplicatedReportTemplate.equals(((ReportTemplateBean)o).duplicatedReportTemplate);
		return false;
	}
	@Override
    public int hashCode() {
    	return duplicatedReportTemplate.hashCode();
    }
	public int compareTo(ReportTemplateBean o) {
		return duplicatedReportTemplate.compareTo(o.duplicatedReportTemplate);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	protected User user() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getUser();
	}
	public ReportTemplate duplicatedReportTemplate() {
		return duplicatedReportTemplate;
	}

	public ReportTemplateBean createLike() {
		EsfPathName newPathName;
		String fromPathName = duplicatedReportTemplate.getPathName().toPlainString();
		if ( EsfPathName.isReservedPathPrefix(fromPathName) ) { // don't let them duplicate with the reserved path prefix
			fromPathName = fromPathName.substring(EsfPathName.ESF_RESERVED_PATH_PREFIX.length());
			newPathName = new EsfPathName(fromPathName);
		} else {
			newPathName = duplicatedReportTemplate.getPathName();
		}
	    ReportTemplate newReportTemplate = ReportTemplate.Manager.createLike(duplicatedReportTemplate, newPathName, user());	    
	    return new ReportTemplateBean(newReportTemplate);
	}
	
	public boolean save(Errors errors) {
		boolean ret = duplicatedReportTemplate.save(user());
		if ( ret ) {
			resetCaches();
		} else {
			if ( errors != null ) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				errors.addError(vaadinUi.getMsg("form.save.failed.message"));
			}
		}
		return ret;
	}
	public boolean save() {
		return save(null);
	}
	
	public boolean delete(Errors errors) {
		if ( duplicatedReportTemplate.isReserved() )
			return false;
		return duplicatedReportTemplate.delete(errors,user());
	}

	protected HashSet<EsfUUID> createIdHashSet( Collection<Group> list ) {
		HashSet<EsfUUID> set = new HashSet<EsfUUID>(list.size());
		for( Group g : list ) {
			set.add(g.getId());
		}
		return set;
	}
	
	public synchronized void resetCaches() {
		allGroupListIds = null;
		allGroupListNames = null;
		permListIds = null;
		permViewDetailsIds = null;
		permCreateLikeIds = null;
		permUpdateIds = null;
		permDeleteIds = null;
		permRunReport = null;
		permViewStartedByExternalUsers = null;
		permViewStartedByAnyUser = null;
		permViewAnyUserPartyTo = null;
		permViewProduction = null;
		permDownloadCsv = null;
		permDownloadArchive = null;
		permViewActivityLog = null;
		permViewEmailLog = null;
		permViewSnapshotData = null;
		permViewSnapshotDocument = null;
		permEsfReportsAccessView = null;
		permEsfReportsAccessUpdate = null;
		
		allTransactionTemplateListIds = null;
		allTransactionTemplateListNames = null;
		transactionTemplateIds = null;
		
		allDocumentListIds = null;
		allDocumentListNames = null;
		limitDocumentIds = null;
		
		this.duplicatedReportTemplate = ReportTemplate.Manager.getById(reportTemplateId).duplicate();
	}


	protected synchronized void createAllGroupLists() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Collection<Group> list = Group.Manager.getForUserWithListPermission(user(),Group.INCLUDE.BOTH_ENABLED_AND_DISABLED);
		
		int size = list.size();
		
		// We will not include the "External Users" pseudo-group.
		allGroupListIds = new EsfUUID[size-1];
		allGroupListNames = new EsfPathName[size-1];
		int i = 0;
		for( Group g : list ) {
			if ( g.isExternalUsersGroup() )
				continue;
			allGroupListIds[i] = g.getId();
			allGroupListNames[i] = g.isDisabled() ? new EsfPathName(g.getPathName() + EsfPathName.PATH_SEPARATOR + vaadinUi.getMsg("PrettyCode.status.disabled")) : g.getPathName();
			++i;
		}
	}
	public EsfUUID[] allGroupListIds() {
		if ( allGroupListIds == null ) {
			createAllGroupLists();
		}
		return allGroupListIds;
	}
	public EsfPathName[] allGroupListNames() {
		if ( allGroupListNames == null ) {
			createAllGroupLists();
		}
		return allGroupListNames;
	}
	
	protected synchronized void createAllTransactionTemplateLists() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Collection<TransactionTemplate> list = TransactionTemplate.Manager.getForUserWithPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_LIST);

		// Remove the standard template if it's in the list
		TransactionTemplate esfTemplate = TransactionTemplate.Manager.getTemplate();
		Iterator<TransactionTemplate> listIter = list.iterator();
		while( listIter.hasNext() ) {
			TransactionTemplate t = listIter.next();
			if ( t.equals(esfTemplate) ) {
				listIter.remove();
				break;
			}
		}
		
		int size = list.size();
		allTransactionTemplateListIds = new EsfUUID[size];
		allTransactionTemplateListNames = new EsfPathName[size];
		int i = 0;
		for( TransactionTemplate tt : list ) {
			allTransactionTemplateListIds[i] = tt.getId();
			allTransactionTemplateListNames[i] = tt.getPathName();
			++i;
		}
	}
	public EsfUUID[] allTransactionTemplateListIds() {
		if ( allTransactionTemplateListIds == null ) {
			createAllTransactionTemplateLists();
		}
		return allTransactionTemplateListIds;
	}
	public EsfPathName[] allTransactionTemplateListNames() {
		if ( allTransactionTemplateListNames == null ) {
			createAllTransactionTemplateLists();
		}
		return allTransactionTemplateListNames;
	}
	
	// This has "all documents" from the selected transaction templates only.
	public synchronized void createAllDocumentLists( Set<EsfUUID> transactionIdSet ) {
		HashSet<EsfUUID> documentIdSet = new HashSet<EsfUUID>();
		
		for ( EsfUUID tranTemplateId : transactionIdSet ) {
			TransactionTemplate tt = TransactionTemplate.Manager.getById(tranTemplateId);
			if ( tt != null ) {
				Package pkg = tt.getPackage();
				if ( pkg != null ) {
					List<PackageVersion> pkgVersionList = PackageVersion.Manager.getAllByPackageId(pkg.getId());
					if ( pkgVersionList != null ) {
						for ( PackageVersion pkgVersion : pkgVersionList ) {
							documentIdSet.addAll(pkgVersion.getDocumentIdList());
						}
					}
				}
			}
		}
		
		TreeMap<EsfPathName,Document> documentMap = new TreeMap<EsfPathName,Document>();
		for( EsfUUID docId : documentIdSet ) {
			Document document = Document.Manager.getById(docId);
			if ( document != null ) {
				EsfPathName fullDocName = new EsfPathName(document.getLibrary().getPathName() + EsfPathName.PATH_SEPARATOR + document.getEsfName());
				documentMap.put(fullDocName, document);
			}
		}
		
		int size = documentMap.size();
		allDocumentListIds = new EsfUUID[size];
		allDocumentListNames = new EsfPathName[size];
		int i = 0;
		Iterator<EsfPathName> docIter = documentMap.keySet().iterator();
		while( docIter.hasNext() ) {
			EsfPathName docPathName = docIter.next();
			Document document = documentMap.get(docPathName);
			allDocumentListIds[i] = document.getId();
			allDocumentListNames[i] = docPathName;
			++i;
		}
	}
	public EsfUUID[] allDocumentListIds() {
		if ( allDocumentListIds == null ) {
			createAllDocumentLists(getTransactionTemplateIds());
		}
		return allDocumentListIds;
	}
	public EsfPathName[] allDocumentListNames() {
		if ( allDocumentListNames == null ) {
			createAllDocumentLists(getTransactionTemplateIds());
		}
		return allDocumentListNames;
	}
	
	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return duplicatedReportTemplate.getId();
	}
	
	public EsfPathName getPathName() {
		return duplicatedReportTemplate.getPathName();
	}
	public void setPathName(EsfPathName v) {
		if ( ! duplicatedReportTemplate.isReserved() ) {
			duplicatedReportTemplate.setPathName( v );
		}
	}
	
	public boolean isEnabled() {
		return duplicatedReportTemplate.isEnabled();
	}
	public String getStatus() {
		return duplicatedReportTemplate.getStatus();
	}
	public void setStatus( String v ) {
		if ( ! duplicatedReportTemplate.isReserved() ) {
			duplicatedReportTemplate.setStatus(v);
		}
	}
	
	public String getDisplayName() {
		return duplicatedReportTemplate.getDisplayName();
	}
	public void setDisplayName(String v) {
		duplicatedReportTemplate.setDisplayName(v);
	}
	
	public String getDescription() {
		return duplicatedReportTemplate.getDescription();
	}
	public void setDescription(String v) {
		duplicatedReportTemplate.setDescription(v);
	}
	
	public String getComments() {
		return duplicatedReportTemplate.getComments();
	}
	public void setComments(String v) {
		duplicatedReportTemplate.setComments( v );
	}
	
	public Set<EsfUUID> getTransactionTemplateIds() {
		if ( transactionTemplateIds == null ) {
			List<EsfUUID> idList = duplicatedReportTemplate.getTransactionTemplateIdList();
			transactionTemplateIds = new HashSet<EsfUUID>(idList);
		}
		return transactionTemplateIds;
	}
	public void setTransactionTemplateIds(Set<EsfUUID> v) {
		LinkedList<EsfUUID> idList = new LinkedList<EsfUUID>(v);
		ListIterator<EsfUUID> idListIter = idList.listIterator();
		while( idListIter.hasNext() ) {
			EsfUUID id = idListIter.next();
			boolean found = false;
			for( int i=0; i < allTransactionTemplateListIds().length; ++i ) {
				if ( allTransactionTemplateListIds[i].equals(id) ) {
					found = true;
					break;
				}
			}
			if ( ! found ) {
				idListIter.remove();
			}
		}
		duplicatedReportTemplate.setTransactionTemplateIdList(idList);
	}
	
	public Set<EsfUUID> getLimitDocumentIds() {
		if ( limitDocumentIds == null ) {
			List<EsfUUID> idList = duplicatedReportTemplate.getLimitDocumentIdList();
			limitDocumentIds = new HashSet<EsfUUID>(idList);
		}
		return limitDocumentIds;
	}
	public void setLimitDocumentIds(Set<EsfUUID> v) {
		LinkedList<EsfUUID> idList = new LinkedList<EsfUUID>(v);
		ListIterator<EsfUUID> idListIter = idList.listIterator();
		while( idListIter.hasNext() ) {
			EsfUUID id = idListIter.next();
			boolean found = false;
			for( int i=0; i < allDocumentListIds().length; ++i ) {
				if ( allDocumentListIds[i].equals(id) ) {
					found = true;
					break;
				}
			}
			if ( ! found ) {
				idListIter.remove();
			}
		}
		if ( allowSaveToObject )
			duplicatedReportTemplate.setLimitDocumentIdList(idList);
	}
	// Because the limit document ids changes values when the tran templates changes, this is a hack to not change the bean when we are doing
	// our hack commit
	public void _suppressSaveToObject() {
		allowSaveToObject = false;
	}
	public void _allowSaveToObject() {
		allowSaveToObject = true;
	}
	
	protected void setPermIds(Set<EsfUUID> v, EsfName permOption) {
		// First, let's remove all groups this user can list
		for( Group g : Group.Manager.getForUserWithListPermission(user()) ) {
			duplicatedReportTemplate.removePermissionAllowedGroups(permOption, g);
		}
		// Then let's add back in the selected set
		for( EsfUUID gid : v ) {
			Group g = Group.Manager.getById( gid );
			if ( g != null ) {
				duplicatedReportTemplate.addPermissionAllowedGroups(permOption, g);
			}
		}
	}

	public Set<EsfUUID> getPermListIds() {
		if ( permListIds == null ) {
			permListIds = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_LIST) );
		}
		return permListIds;
	}
	public void setPermListIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_LIST);
	}

	public Set<EsfUUID> getPermViewDetailsIds() {
		if ( permViewDetailsIds == null ) {
			permViewDetailsIds = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_VIEWDETAILS) );
		}
		return permViewDetailsIds;
	}
	public void setPermViewDetailsIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public Set<EsfUUID> getPermCreateLikeIds() {
		if ( permCreateLikeIds == null ) {
			permCreateLikeIds = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_CREATELIKE) );
		}
		return permCreateLikeIds;
	}
	public void setPermCreateLikeIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public Set<EsfUUID> getPermUpdateIds() {
		if ( permUpdateIds == null ) {
			permUpdateIds = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_UPDATE) );
		}
		return permUpdateIds;
	}
	public void setPermUpdateIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_UPDATE);
	}

	public Set<EsfUUID> getPermDeleteIds() {
		if ( permDeleteIds == null ) {
			permDeleteIds = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_DELETE) );
		}
		return permDeleteIds;
	}
	public void setPermDeleteIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_DELETE);
	}

	public Set<EsfUUID> getPermRunReportIds() {
		if ( permRunReport == null ) {
			permRunReport = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.REPORT_PERM_OPTION_RUN_REPORT) );
		}
		return permRunReport;
	}
	public void setPermRunReportIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.REPORT_PERM_OPTION_RUN_REPORT);
	}

	public Set<EsfUUID> getPermViewStartedByExternalUsersIds() {
		if ( permViewStartedByExternalUsers == null ) {
			permViewStartedByExternalUsers = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.REPORT_PERM_OPTION_VIEW_STARTED_BY_EXTERNAL_USERS) );
		}
		return permViewStartedByExternalUsers;
	}
	public void setPermViewStartedByExternalUsersIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.REPORT_PERM_OPTION_VIEW_STARTED_BY_EXTERNAL_USERS);
	}

	public Set<EsfUUID> getPermViewStartedByAnyUserIds() {
		if ( permViewStartedByAnyUser == null ) {
			permViewStartedByAnyUser = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.REPORT_PERM_OPTION_VIEW_STARTED_BY_ANY_USER) );
		}
		return permViewStartedByAnyUser;
	}
	public void setPermViewStartedByAnyUserIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.REPORT_PERM_OPTION_VIEW_STARTED_BY_ANY_USER);
	}

	public Set<EsfUUID> getPermViewAnyUserPartyToIds() {
		if ( permViewAnyUserPartyTo == null ) {
			permViewAnyUserPartyTo = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO) );
		}
		return permViewAnyUserPartyTo;
	}
	public void setPermViewAnyUserPartyToIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.REPORT_PERM_OPTION_VIEW_ANY_USER_PARTY_TO);
	}

	public Set<EsfUUID> getPermViewProductionIds() {
		if ( permViewProduction == null ) {
			permViewProduction = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.REPORT_PERM_OPTION_VIEW_PRODUCTION) );
		}
		return permViewProduction;
	}
	public void setPermViewProductionIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.REPORT_PERM_OPTION_VIEW_PRODUCTION);
	}

	public Set<EsfUUID> getPermDownloadCsvIds() {
		if ( permDownloadCsv == null ) {
			permDownloadCsv = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.REPORT_PERM_OPTION_DOWNLOAD_CSV) );
		}
		return permDownloadCsv;
	}
	public void setPermDownloadCsvIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.REPORT_PERM_OPTION_DOWNLOAD_CSV);
	}

	public Set<EsfUUID> getPermDownloadArchiveIds() {
		if ( permDownloadArchive == null ) {
			permDownloadArchive = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.REPORT_PERM_OPTION_DOWNLOAD_ARCHIVE) );
		}
		return permDownloadArchive;
	}
	public void setPermDownloadArchiveIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.REPORT_PERM_OPTION_DOWNLOAD_ARCHIVE);
	}

	public Set<EsfUUID> getPermViewActivityLogIds() {
		if ( permViewActivityLog == null ) {
			permViewActivityLog = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.REPORT_PERM_OPTION_VIEW_ACTIVITY_LOG) );
		}
		return permViewActivityLog;
	}
	public void setPermViewActivityLogIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.REPORT_PERM_OPTION_VIEW_ACTIVITY_LOG);
	}

	public Set<EsfUUID> getPermViewEmailLogIds() {
		if ( permViewEmailLog == null ) {
			permViewEmailLog = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.REPORT_PERM_OPTION_VIEW_EMAIL_LOG) );
		}
		return permViewEmailLog;
	}
	public void setPermViewEmailLogIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.REPORT_PERM_OPTION_VIEW_EMAIL_LOG);
	}

	public Set<EsfUUID> getPermViewSnapshotDataIds() {
		if ( permViewSnapshotData == null ) {
			permViewSnapshotData = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.REPORT_PERM_OPTION_VIEW_SNAPSHOT_DATA) );
		}
		return permViewSnapshotData;
	}
	public void setPermViewSnapshotDataIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.REPORT_PERM_OPTION_VIEW_SNAPSHOT_DATA);
	}

	public Set<EsfUUID> getPermViewSnapshotDocumentIds() {
		if ( permViewSnapshotDocument == null ) {
			permViewSnapshotDocument = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.REPORT_PERM_OPTION_VIEW_SNAPSHOT_DOCUMENT) );
		}
		return permViewSnapshotDocument;
	}
	public void setPermViewSnapshotDocumentIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.REPORT_PERM_OPTION_VIEW_SNAPSHOT_DOCUMENT);
	}

	public Set<EsfUUID> getPermEsfReportsAccessViewIds() {
		if ( permEsfReportsAccessView == null ) {
			permEsfReportsAccessView = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_VIEW) );
		}
		return permEsfReportsAccessView;
	}
	public void setPermEsfReportsAccessViewIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_VIEW);
	}
	
	public Set<EsfUUID> getPermEsfReportsAccessUpdateIds() {
		if ( permEsfReportsAccessUpdate == null ) {
			permEsfReportsAccessUpdate = createIdHashSet( duplicatedReportTemplate.getPermissionAllowedGroups(user(), PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE) );
		}
		return permEsfReportsAccessUpdate;
	}
	public void setPermEsfReportsAccessUpdateIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.REPORT_PERM_OPTION_ESF_REPORTS_ACCESS_UPDATE);
	}
	
	public EsfDateTime getLastUpdatedTimestamp() {
		return duplicatedReportTemplate.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		return duplicatedReportTemplate.getLastUpdatedTimestamp().toString(user());
	}
	public String formatLogLastUpdatedTimestamp() {
		return duplicatedReportTemplate.getLastUpdatedTimestamp().toLogString(user());
	}

	public String formatCreatedTimestamp() {
		return duplicatedReportTemplate.getCreatedTimestamp().toString(user());
	}
	public String formatLogCreatedTimestamp() {
		return duplicatedReportTemplate.getCreatedTimestamp().toLogString(user());
	}

	public boolean hasPermViewDetails() {
		return duplicatedReportTemplate.hasPermission(user(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		return duplicatedReportTemplate.hasPermission(user(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		return duplicatedReportTemplate.hasPermission(user(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		return duplicatedReportTemplate.isReserved() ? false : duplicatedReportTemplate.hasPermission(user(), PermissionOption.PERM_OPTION_DELETE);
	}
	
}