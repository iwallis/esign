// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2010 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.group;

import java.io.Serializable;
import java.util.Collection;

import com.esignforms.open.user.Group;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.util.BeanItemContainer;

public class GroupViewContainer extends BeanItemContainer<GroupBean> implements Serializable {
	private static final long serialVersionUID = -2477155660290024320L;

	public GroupViewContainer() throws InstantiationException, IllegalAccessException {
		super(GroupBean.class);

		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Collection<Group> groups = Group.Manager.getForUserWithPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS);
		for( Group g : groups ) {
			// We don't do updates on our pseudo groups
			if ( g.isAllUsersGroup() || g.isExternalUsersGroup() )
				continue;
			addItem( new GroupBean(g) );
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}