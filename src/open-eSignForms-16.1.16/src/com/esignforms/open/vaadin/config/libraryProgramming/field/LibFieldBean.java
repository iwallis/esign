// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.field;

import java.io.Serializable;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibFieldBean implements Serializable, Comparable<LibFieldBean> {
	private static final long serialVersionUID = 7354662524360981890L;

	private FieldTemplate fieldTemplate;
	private EsfName originalEsfName;
	
	private EsfUUID setValuesUsingLibraryFieldTemplateId;
	private LibLabelBean libLabelBean;
	private TypeBean typeBean;
	
	public LibFieldBean(FieldTemplate fieldTemplate) {
		this.fieldTemplate = fieldTemplate;
		this.originalEsfName = fieldTemplate.getEsfName();
		this.libLabelBean = new LibLabelBean(fieldTemplate.getLabelTemplate());
		this.typeBean = new TypeBean(fieldTemplate);
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibFieldBean )
			return fieldTemplate.equals(((LibFieldBean)o).fieldTemplate);
		return false;
	}
	@Override
    public int hashCode() {
    	return fieldTemplate.hashCode();
    }
	@Override
	public int compareTo(LibFieldBean o) {
		return fieldTemplate.compareTo(o.fieldTemplate);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public FieldTemplate fieldTemplate() {
		return fieldTemplate;
	}
	
	public boolean save(Errors errors) {
		boolean ret = fieldTemplate.save();
		if ( ret ) {
			resetCaches();
		} else {
			if ( errors != null ) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				errors.addError(vaadinUi.getMsg("form.save.failed.message"));
			}
		}
		return ret;
	}
	
	public boolean delete(Errors errors) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return fieldTemplate.delete(errors,vaadinUi.getUser());
	}

	
	private synchronized void resetCaches() {
		originalEsfName = getEsfName();
	}

	
	// Now for the JavaBeans methods
	public EsfName getEsfName() {
		return fieldTemplate.getEsfName();
	}
	public void setEsfName(EsfName v) {
		if ( ! originalEsfName.hasReservedPrefix() && v != null && ! v.hasReservedPrefix() ) {
			fieldTemplate.setEsfName(v);
		}
	}
	public EsfName getOriginalEsfName() {
		return originalEsfName;
	}
	
	public void fixupEsfName() {
		resetCaches();
	}

	public EsfUUID getId() {
		return fieldTemplate.getId();
	}
	
    public short getContainerReferenceCount()
    {
    	return fieldTemplate.getContainerReferenceCount();
    }

	public String getType() {
		return fieldTemplate.getType();
	}
	public void setType(String v) {
		fieldTemplate.setType(v);
	}
	
	public short getWidth() {
		return fieldTemplate.getWidth();
	}
	public void setWidth(short v) {
		fieldTemplate.setWidth(v);
	}
	
	public String getWidthUnit() {
		return fieldTemplate.getWidthUnit();
	}
	public void setWidthUnit(String v) {
		fieldTemplate.setWidthUnit(v);
	}
    
	public boolean getIgnoreWidthOnOutput() {
		return fieldTemplate.isIgnoreWidthOnOutput();
	}
	public void setIgnoreWidthOnOutput(boolean v) {
		fieldTemplate.setIgnoreWidthOnOutput(v ? FieldTemplate.WIDTH_IGNORE_ON_OUTPUT : FieldTemplate.WIDTH_KEEP_ON_OUTPUT);
	}
	
	public boolean getBorderTop() {
		return fieldTemplate.isBorderTop();
	}
	public void setBorderTop(boolean v) {
		fieldTemplate.setBorderTop(v);
	}
	
	public boolean getBorderRight() {
		return fieldTemplate.isBorderRight();
	}
	public void setBorderRight(boolean v) {
		fieldTemplate.setBorderRight(v);
	}
	
	public boolean getBorderBottom() {
		return fieldTemplate.isBorderBottom();
	}
	public void setBorderBottom(boolean v) {
		fieldTemplate.setBorderBottom(v);
	}
	
	public boolean getBorderLeft() {
		return fieldTemplate.isBorderLeft();
	}
	public void setBorderLeft(boolean v) {
		fieldTemplate.setBorderLeft(v);
	}
	
	// Bridge routines for DropDown ESF_TextAlign
	private String textAlignCss;
	public String getTextAlignCss() {
		if ( textAlignCss == null )
			textAlignCss = fieldTemplate.getTextAlignCss();
		return textAlignCss;
	}
	public void setTextAlignCss(String v) {
		textAlignCss = v;
		fieldTemplate.setAlignFromTextAlignCss(v);
	}
	
	public String getRequired() {
		return fieldTemplate.getRequired();
	}
	public void setRequired(String v) {
		fieldTemplate.setRequired(v);
	}
	
	public boolean getAutoComplete() {
		return fieldTemplate.isAutoCompleteAllowed();
	}
	public void setAutoComplete(boolean v) {
		fieldTemplate.setAutoComplete(v ? FieldTemplate.AUTO_COMPLETE_ALLOWED : FieldTemplate.AUTO_COMPLETE_NOT_ALLOWED);
	}
	
	public boolean getMaskInDataSnapshot() {
		return fieldTemplate.isMaskInDataSnapshot();
	}
	public void setMaskInDataSnapshot(boolean v) {
		fieldTemplate.setMaskInDataSnapshot(v ? FieldTemplate.MASK_IN_DATA_SNAPSHOT_ALLOWED : FieldTemplate.MASK_IN_DATA_SNAPSHOT_NOT_ALLOWED);
	}
	
	public boolean getMaskInput() {
		return fieldTemplate.isMaskInput();
	}
	public void setMaskInput(boolean v) {
		fieldTemplate.setMaskInput(v ? FieldTemplate.MASK_INPUT_ALLOWED : FieldTemplate.MASK_INPUT_NOT_ALLOWED);
	}
	
	public boolean getCloneAllowed() {
		return fieldTemplate.isCloneAllowed();
	}
	public void setCloneAllowed(boolean v) {
		if ( fieldTemplate.isTypeSignature() || fieldTemplate.isTypeSignDate() )
			fieldTemplate.setCloneAllowed(FieldTemplate.CLONE_NOT_ALLOWED); // we don't let signatures be cloned ever
		else
			fieldTemplate.setCloneAllowed(v ? FieldTemplate.CLONE_ALLOWED : FieldTemplate.CLONE_NOT_ALLOWED);
	}
	
    public int getMinLength()
    {
    	return fieldTemplate.getMinLength();
    }
    public void setMinLength(int v)
    {
    	fieldTemplate.setMinLength(v);
    }
    
    public int getMaxLength()
    {
    	return fieldTemplate.getMaxLength();
    }
    public void setMaxLength(int v)
    {
    	fieldTemplate.setMaxLength(v);
    }
	
    public static class TypeBean implements java.io.Serializable {
		private static final long serialVersionUID = 4876749034797088576L;

		FieldTemplate fieldTemplate;
    	public TypeBean(FieldTemplate fieldTemplate) {
    		this.fieldTemplate = fieldTemplate;
    	}
    	
    	public FieldTemplate fieldTemplate() { return fieldTemplate; }
    	
        public String getInitialValue()
        {
        	return fieldTemplate.getInitialValue();
        }
        public void setInitialValue(String v)
        {
        	fieldTemplate.setInitialValue(v);
        }
        
    	public boolean getDisplayEmptyValueEnabled() {
    		return fieldTemplate.isDisplayEmptyValueEnabled();
    	}
    	public void setDisplayEmptyValueEnabled(boolean v) {
    		fieldTemplate.setDisplayEmptyValueEnabled(v ? FieldTemplate.DISPLAY_EMPTY_VALUE_ENABLED : FieldTemplate.DISPLAY_EMPTY_VALUE_DISABLED);
    	}

    	public String getDisplayEmptyValue()
        {
        	return fieldTemplate.getDisplayEmptyValue();
        }
        public void setDisplayEmptyValue(String v)
        {
        	fieldTemplate.setDisplayEmptyValue(v);
        }
        
    	public boolean getAutoPost() {
    		return fieldTemplate.isAutoPostAllowed();
    	}
    	public void setAutoPost(boolean v) {
    		fieldTemplate.setAutoPost(v ? FieldTemplate.AUTO_POST_ALLOWED : FieldTemplate.AUTO_POST_NOT_ALLOWED);
    	}
    	
        public String getInputFormatSpec()
        {
        	if ( ! fieldTemplate.hasInputFormatSpec() && fieldTemplate.isTypePhone() )
        		return Application.getInstance().getDefaultCountryCode();
        	return fieldTemplate.getInputFormatSpec();
        }
        public void setInputFormatSpec(String v)
        {
        	fieldTemplate.setInputFormatSpec(v);
        }
        
        public String getOutputFormatSpec()
        {
        	return fieldTemplate.getOutputFormatSpec();
        }
        public void setOutputFormatSpec(String v)
        {
        	fieldTemplate.setOutputFormatSpec(v);
        }
        
        public String getExtraOptions()
        {
        	return fieldTemplate.getExtraOptions();
        }
        public void setExtraOptions(String v)
        {
        	fieldTemplate.setExtraOptions(v);
        }
        
        public String getTooltip()
        {
        	return fieldTemplate.getTooltip();
        }
        public void setTooltip(String v)
        {
        	fieldTemplate.setTooltip(v);
        }
        
        public String getInputPrompt()
        {
        	return fieldTemplate.getInputPrompt();
        }
        public void setInputPrompt(String v)
        {
        	fieldTemplate.setInputPrompt(v);
        }
    }
    
    public TypeBean getTypeBean() {
    	return typeBean;
    }
    public void setTypeBean(TypeBean ignoreTypeBean) {
    	typeBean = ignoreTypeBean; // just a placeholder so that the form thinks that the label is not just read-only
    }
    
	public LibLabelBean getLibLabelBean() {
		return libLabelBean;
	}
	public void setLibLabelBean(LibLabelBean ignoreLibLabelBean) {
		libLabelBean = ignoreLibLabelBean; // just a placeholder so that the form thinks that the label is not just read-only
	}
	
	public EsfUUID getLibraryFieldTemplateId() {
		return setValuesUsingLibraryFieldTemplateId;
	}
	public void setLibraryFieldTemplateId(EsfUUID v) {
		setValuesUsingLibraryFieldTemplateId = null; // we ignore this since this is not remembered
	}

}