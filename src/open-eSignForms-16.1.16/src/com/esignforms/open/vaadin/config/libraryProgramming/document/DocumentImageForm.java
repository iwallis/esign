// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.image.Thumbnail;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.prog.ImageVersionOverlayField;
import com.esignforms.open.prog.Library;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.libraryProgramming.document.imageOverlayField.ImageOverlayFieldView;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.LibraryImageEsfNameValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.UploadFileWithProgress;
import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.BorderStyle;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The DocumentImageForm is used to set the values on the image of a document version.
 * It has the added ability to operate as an image overlay by various fields.
 * @author Yozons Inc.
 */
public class DocumentImageForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 2918242767202508997L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DocumentImageForm.class);
	
	Button saveButton;
	Button cancelButton;
	Button deleteButton;
	
	Button positionOverlayFieldsButton;
	
	final DocumentImageForm thisForm;
	LibDocumentVersionBean docVerBean;
	DocumentVersion.DocumentImage documentImage;
	
	ConfirmDiscardFormChangesWindow parentWindow;
    GridLayout layout;
    
    final DocumentVersion duplicateDocumentVersion;

    LibraryImageEsfNameValidator esfnameValidator;
    
	Label imageId;
	Label imageVersionId;
	Label createdByInfo;
	Label lastUpdatedByInfo;
	Label imageInfo;
	Image thumbImage;
	Link viewImageLink;
	UploadFileWithProgress uploadFile;
	
	// we use this to rollback since we can't just rely on commit/discard as we need to see the newly uploaded image
	// on upload, not just after clicking OK/Save.
	ImageVersion origImageVersion;
	                              
    

    public DocumentImageForm(Library library, LibDocumentVersionBean docVerBean, DocumentVersion.DocumentImage documentImage) {
    	setStyleName("LibDocumentVersionPageViewDocumentImageForm");
    	this.thisForm = this;
    	this.docVerBean = docVerBean;
    	this.documentImage = documentImage; 
		this.duplicateDocumentVersion = docVerBean.duplicateDocumentVersion();
		this.origImageVersion = documentImage.imageVersion.duplicate();
     }
    
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
			EsfName esfName = new EsfName((String)getField("esfName").getValue());
			documentImage.image.setEsfName(esfName);
			
			String description = (String)getField("description").getValue();
			documentImage.image.setDescription(description);
			
			String comments = (String)getField("comments").getValue();
			documentImage.image.setComments(comments);
			
			boolean useDataUri = (Boolean)getField("useDataUri").getValue();
			documentImage.imageVersion.setUseDataUri(useDataUri ? Literals.Y : Literals.N);
			
			updateOverlayFieldTemplates();

			if ( documentImage.image.doInsert() )
				duplicateDocumentVersion.addDocumentImage(documentImage);
						
			parentWindow.close();
        } else if ( source == cancelButton ) {
    		discard();
    		documentImage.imageVersion = origImageVersion;
    		ImageVersion.Manager.replaceInCache(origImageVersion);
			parentWindow.close();
        } else if ( source == deleteButton ) {
        	discard();
        	duplicateDocumentVersion.removeDocumentImage(documentImage);
        	parentWindow.close();
        }
    }
	
	protected void updateOverlayFieldTemplates() {
		Field<?> select = getField("selectedOverlayFieldTemplateIds");
		if ( select == null ) return;
		
		try { select.commit(); } catch(Exception e) {};
		
		List<ImageVersionOverlayField> newOverlayFields = new LinkedList<ImageVersionOverlayField>();
		
		// We need a new set since the one returned is non-modifiable...
		HashSet<EsfUUID> selectedOverlayFieldTemplateIds = new HashSet<EsfUUID>( (java.util.Set<EsfUUID>)select.getValue() );

		// Scan each of the selected fields.  If it was in our original list, we'll add it back in.  If not, we'll create a new one and put it in our list.
		for( EsfUUID fieldTemplateId : selectedOverlayFieldTemplateIds ) {
			ImageVersionOverlayField ivof = getOverlayFieldTemplateById(fieldTemplateId, documentImage.imageVersion.getOverlayFields());
			if ( ivof == null )
				ivof = new ImageVersionOverlayField(documentImage.imageVersion.getId(), fieldTemplateId);
			newOverlayFields.add(ivof);
		}
		
		documentImage.imageVersion.setOverlayFields(newOverlayFields);
	}
	protected ImageVersionOverlayField getOverlayFieldTemplateById(EsfUUID fieldTemplateId, List<ImageVersionOverlayField> list) {
		if ( list == null || fieldTemplateId == null || fieldTemplateId.isNull() )
			return null;
		for( ImageVersionOverlayField ivof : list ) {
			if ( ivof.getFieldTemplateId().equals(fieldTemplateId) ) {
				return ivof;
			}
		}
		return null;
	}
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	saveButton.setVisible(!readOnly && documentImage.imageVersion.hasImageFileName());
    	deleteButton.setVisible(!readOnly && documentImage.imageVersion.hasImageFileName());
    	uploadFile.setVisible(!readOnly);
    	
    	positionOverlayFieldsButton.setVisible(documentImage.imageVersion.hasOverlayFields());
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
		setupForm();
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	void setupForm() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		PropertysetItem item = new PropertysetItem();
		item.addItemProperty("esfName", new ObjectProperty<EsfName>(documentImage.image.getEsfName()));
		item.addItemProperty("description", new ObjectProperty<String>(documentImage.image.getDescription(),String.class));
		item.addItemProperty("comments", new ObjectProperty<String>(documentImage.image.getComments(),String.class));
		item.addItemProperty("useDataUri", new ObjectProperty<Boolean>(documentImage.imageVersion.isUseDataUri(),Boolean.class));
		
		HashSet<EsfUUID> selectedOverlayFieldTemplateIds = new HashSet<EsfUUID>();
		if ( documentImage.imageVersion.hasOverlayFields() ) {
			for( ImageVersionOverlayField ivof : documentImage.imageVersion.getOverlayFields() ) {
				selectedOverlayFieldTemplateIds.add(ivof.getFieldTemplateId());
			}
		}
		item.addItemProperty("selectedOverlayFieldTemplateIds", new ObjectProperty(selectedOverlayFieldTemplateIds,java.util.Set.class));
		
		setItemDataSource(item);
		
		imageId.setValue( vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.label.imageId",documentImage.image.getId()) );
		imageVersionId.setValue( vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.label.id",documentImage.imageVersion.getId()) );		

		String createdBy = vaadinUi.getPrettyCode().userDisplayName(documentImage.imageVersion.getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.label.createdBy",documentImage.imageVersion.getCreatedTimestamp().toLogString(vaadinUi.getUser()),createdBy) );
		
		if ( documentImage.imageVersion.doInsert() ) {
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.label.lastUpdated.whenNew") );
			lastUpdatedByInfo.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(documentImage.imageVersion.getLastUpdatedByUserId());
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.label.lastUpdated",documentImage.imageVersion.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser()),lastUpdatedBy) );
			lastUpdatedByInfo.setIcon(null);
		}
		
		if ( documentImage.imageVersion.hasImageFileName() ) {
			imageInfo.setValue(documentImage.imageVersion.getImageFileName());
			imageInfo.removeStyleName("imageInfoError");
			uploadFile.setButtonCaption(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.button.uploadImage.hasimage.label"));
			thumbImage.setVisible(true);
			thumbImage.setSource(new ExternalResource(documentImage.imageVersion.getThumbnailByIdUrl()));
			viewImageLink.setVisible(true);
			viewImageLink.setResource(new ExternalResource(documentImage.imageVersion.getImageByIdUrl()));
			
			ImageVersion.Manager.replaceInCache(documentImage.imageVersion); // so we can see the new images
			
		} else {
			imageInfo.setValue(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.label.imageInfo.noimage"));
			imageInfo.addStyleName("imageInfoError");
			uploadFile.setButtonCaption(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.button.uploadImage.noimage.label"));
			thumbImage.setVisible(false);
			viewImageLink.setVisible(false);
		}

		TextField esfname = (TextField)getField("esfName");
		if ( esfnameValidator != null ) esfname.removeValidator(esfnameValidator);
		esfnameValidator = new LibraryImageEsfNameValidator(duplicateDocumentVersion,documentImage.image);
		esfname.addValidator(esfnameValidator);
		if ( documentImage.imageVersion.doInsert() ) {
			esfname.selectAll();
			esfname.focus();
		}
	}
	
	void openImageFieldOverlayWindow() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			ImageOverlayFieldView view = new ImageOverlayFieldView(duplicateDocumentVersion, documentImage.imageVersion.getOverlayFields());
			view.initView();
            
			ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldView.window.caption"), view);
            w.center();
        	w.setWidth(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldView.window.width"));
        	w.setHeight(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ImageOverlayFieldView.window.height"));
        	w.setModal(true);
        	view.activateView(EsfView.OpenMode.WINDOW, "");
            view.setReadOnly(isReadOnly());
        	view.setParentWindow(w);
        	vaadinUi.addWindowToUI(w);	
        } catch( Exception e ) {
			vaadinUi.showError(null,"Could not create the image overlay field view.");
			_logger.error("openImageOverlayFieldWindow()",e);
			return;
		}	
	}
	
	public void setParentWindow(ConfirmDiscardFormChangesWindow parentWindow) {
		this.parentWindow = parentWindow;
	}

	@Override
	public void initView() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	
    	// Setup layout
    	layout = new GridLayout(2,11);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.5f);
    	layout.setColumnExpandRatio(1, 0.5f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
		imageInfo = new Label();
		imageInfo.setStyleName("imageInfo");
		imageInfo.setContentMode(ContentMode.TEXT);
		layout.addComponent(imageInfo,0,3);
		
		thumbImage = new Image(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.embedded.thumbnail.label"));
        layout.addComponent(thumbImage,1,3);
        thumbImage.setVisible(false);
		
		// TODO: 5MB max for an image in our document, make tunable
		uploadFile = new UploadFileWithProgress(null,vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.button.uploadImage.noimage.label"),(int)vaadinUi.getEsfapp().getUploadImageMaxBytes()) {
			private static final long serialVersionUID = 832945848591490271L;

			@Override
			public void afterUploadSucceeded() {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				byte[] imageData = uploadFile.getFileData();
				try {
					Thumbnail thumb = new Thumbnail(imageData);
					if ( thumb.createThumbnail(100) ) {
						byte[] thumbData = thumb.saveThumbnailToBytesByMimeType(uploadFile.getMimeType());
						documentImage.imageVersion.setThumbnailData(thumbData);
						if ( thumbData == null ) {
							vaadinUi.showError(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.thumbnail.failure.caption"), vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.thumbnail.failure.message"));
						}
					} else {
						documentImage.imageVersion.setThumbnailData(null);
						vaadinUi.showError(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.thumbnail.failure.caption"), vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.thumbnail.failure.message"));
					}
				} catch( EsfException e ) {
					_logger.error("Failed to create thumbnail in afterUploadSucceeded()",e);
				}
				documentImage.imageVersion.setImageData(imageData);
				documentImage.imageVersion.setImageFileName(uploadFile.getFileName());
				documentImage.imageVersion.setImageMimeType(uploadFile.getMimeType());
				vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.upload.successful",uploadFile.getFileName(),EsfInteger.byteSizeInUnits(uploadFile.getFileData().length) ));
				imageInfo.setValue(uploadFile.getFileName());
				imageInfo.removeStyleName("imageInfoError");
				uploadFile.setButtonCaption(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.button.uploadImage.hasimage.label"));
				thumbImage.setSource(new ExternalResource(documentImage.imageVersion.getThumbnailByIdUrl()));
				thumbImage.setVisible(true);
				viewImageLink.setResource(new ExternalResource(documentImage.imageVersion.getImageByIdUrl()));
				viewImageLink.setVisible(true);
				thisForm.setReadOnly(isReadOnly());
			}
			@Override
			public void afterUploadFailed(String fileName, boolean wasCanceled, boolean wasInvalidMimeType, boolean wasTooBig, int contentLength, int maxSize) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				if ( wasTooBig ) {
					vaadinUi.showError(
							vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.fileTooLarge.caption"), 
							vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.fileTooLarge.message",EsfInteger.byteSizeInUnits(contentLength),EsfInteger.byteSizeInUnits(maxSize))
							);
				} else if ( wasCanceled ) {
					vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.upload.canceled"));
				} else if ( wasInvalidMimeType ) {
					vaadinUi.showError(
							vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.invalidType.caption"), 
							vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.invalidType.message",EsfString.isBlank(fileName)?"(unknown)":fileName)
							);
				}
			}
		}; 
		uploadFile.setAllowedMimeTypesForImages();
		uploadFile.setVisible(false);
		layout.addComponent(uploadFile,0,4);
		
		viewImageLink = new Link();
		viewImageLink.setCaption(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.link.viewImage.label"));
		viewImageLink.setTargetName("_blank");
		viewImageLink.setTargetWidth(300);
		viewImageLink.setTargetHeight(300);
		viewImageLink.setTargetBorder(BorderStyle.NONE);
		layout.addComponent(viewImageLink,1,4);
		viewImageLink.setVisible(false);
		
		positionOverlayFieldsButton = new Button(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.button.positionOverlayFields.label"), (ClickListener)this);
		positionOverlayFieldsButton.setDescription(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.button.positionOverlayFields.tooltip"));
		positionOverlayFieldsButton.addStyleName(Reindeer.BUTTON_SMALL);
		positionOverlayFieldsButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = 5660958759056573050L;

			@Override
			public void buttonClick(ClickEvent event) {
				updateOverlayFieldTemplates();
				if ( documentImage.imageVersion.hasOverlayFields() ) {
					openImageFieldOverlayWindow();
				}
			}
		});
    	layout.addComponent(positionOverlayFieldsButton,1,6);
		positionOverlayFieldsButton.setVisible(false);

		lastUpdatedByInfo = new Label();
		lastUpdatedByInfo.setContentMode(ContentMode.TEXT);
		lastUpdatedByInfo.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedByInfo,0,7,1,7);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,8,1,8);		

		imageId = new Label();
		imageId.setContentMode(ContentMode.TEXT);
		imageId.setStyleName("smallInfo");
		layout.addComponent(imageId,0,9,1,9);

		imageVersionId = new Label();
		imageVersionId.setContentMode(ContentMode.TEXT);
		imageVersionId.setStyleName("smallInfo");
		layout.addComponent(imageVersionId,0,10,1,10);

    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(true);
    	footer.setMargin(true);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
		footer.addComponent(deleteButton);

		setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 1555447630270230809L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				
				if ( propertyId.equals("comments") ) {
	            	TextArea ta = new TextArea();
	            	ta.setWidth(100, Unit.PERCENTAGE);
	            	ta.setNullRepresentation("");
	            	ta.setRows(3);
	            	ta.setInputPrompt(vaadinUi.getMsg("inputPrompt.comments"));
	            	ta.setCaption(vaadinUi.getMsg("caption.comments"));
	            	ta.setDescription(vaadinUi.getMsg("tooltip.comments"));
	            	return ta;
				} 
				if ( propertyId.equals("useDataUri") ) {
					CheckBox cb = new CheckBox(vaadinUi.getMsg("LibImageVersionForm.useDataUri.label"));
					cb.setImmediate(false);
					cb.setDescription(vaadinUi.getMsg("LibImageVersionForm.useDataUri.tooltip"));
					return cb;
				}
                if (propertyId.equals("selectedOverlayFieldTemplateIds")) {
                	TwinColSelect select = new TwinColSelect();
                	select.setLeftColumnCaption(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.selectedOverlayFieldTemplateIds.label.left"));
                	select.setRightColumnCaption(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.selectedOverlayFieldTemplateIds.label.right"));
                	select.setWidth(100, Unit.PERCENTAGE);

                	select.setNullSelectionAllowed(true); 
                	select.addValidator(new SelectValidator(select));
                	select.setImmediate(true);
                	select.setMultiSelect(true);
                	select.setRows(Math.min(15,duplicateDocumentVersion._getFieldTemplateMapInternal().size()));
                	select.setRequired(false);
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	for( EsfName fieldName : duplicateDocumentVersion._getFieldTemplateMapInternal().keySet() ) {
                		FieldTemplate ft = duplicateDocumentVersion._getFieldTemplateMapInternal().get(fieldName);
                		if ( ft.isTypeRadioButtonGroup() ) // Cannot position a radio button group since it's not a field
                			continue;
                		select.addItem(ft.getId());
                		select.setItemCaption( ft.getId(), ft.getEsfName().toString() );
                	}
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 8518382702899079337L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							HashSet<EsfUUID> selectedOverlayFieldTemplateIds = new HashSet<EsfUUID>( (java.util.Set<EsfUUID>)event.getProperty().getValue() );
							positionOverlayFieldsButton.setVisible(selectedOverlayFieldTemplateIds.size() > 0);
						}
                		
                	});
                	return select;
                }
				
				Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("esfName")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setCaption(vaadinUi.getMsg("caption.esfName"));
                	tf.setDescription(vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.esfname.tooltip"));
                	tf.setConversionError(vaadinUi.getMsg("validator.esfname.message"));
                } else if ( propertyId.equals("description") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("caption.description"));
                    tf.setInputPrompt(vaadinUi.getMsg("inputPrompt.description"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.description"));
                }
                
                return field;
    	    }
    	 });

    	_logger.debug("Form created");
	}

	@Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("esfName")) {
        	layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("useDataUri")) {
        	layout.addComponent(field, 1, 0);
        } else if (propertyId.equals("description")) {
        	layout.addComponent(field, 0, 1, 1, 1);
        } else if (propertyId.equals("comments")) {
        	layout.addComponent(field, 0, 2, 1, 2);
        } else if (propertyId.equals("selectedOverlayFieldTemplateIds")) {
        	layout.addComponent(field, 0, 5, 1, 5);
        }
    }


	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibDocumentVersionPageView.ImageForm.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}