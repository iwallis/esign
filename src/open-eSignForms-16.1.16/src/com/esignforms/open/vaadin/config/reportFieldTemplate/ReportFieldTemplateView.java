// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.reportFieldTemplate;

import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

public class ReportFieldTemplateView extends VerticalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = -5529973850020574551L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ReportFieldTemplateView.class);
	
    final ReportFieldTemplateView thisView;
    ConfirmDiscardFormChangesWindow parentWindow;
    ReportFieldTemplateContainer container;
    ReportFieldTemplateList list;
    ReportFieldTemplateForm form;
	
	public ReportFieldTemplateView() {
		super();
		thisView = this;
		setStyleName(Reindeer.SPLITPANEL_SMALL);
	}
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			container = new ReportFieldTemplateContainer();
			list = new ReportFieldTemplateList(this,container);
			form = new ReportFieldTemplateForm(this,container);
			
			setFirstComponent(list);
			setSecondComponent(form);
			setSplitPosition(320,Unit.PIXELS);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("ReportFieldTemplateView view exception", e.getMessage());
		}
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		if ( list != null) list.setReadOnly(v);
		if ( form != null) form.setReadOnly(v);
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our list or not
        if (property == list.getTable()) {
        	final Item item = list.getTable().getItem(list.getTable().getValue());
        	if ( isDirty() ) {
    			if ( form.getItemDataSource() == item ) {
    				// We've selected the same item so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("ReportFieldTemplateView.ConfirmDiscardChangesDialog.message"), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						form.discard();
						form.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						list.getTable().select(form.getItemDataSource()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("ReportFieldTemplateView.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	form.setItemDataSource(item);
    		}
        }
        else
        	_logger.debug("valueChange() - no list event: " + event.toString());
	}

	/*
    public void createNew() {
		unselectAll();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ReportFieldTemplate newField = ReportFieldTemplate.Manager.createNew();
		form.setNewReportFieldAsDataSource(newField);
	}
	*/
	
	public void setParentWindow(ConfirmDiscardFormChangesWindow w) {
		parentWindow = w;
	}
	
	public void closeParentWindow() {
		parentWindow.close();
	}
	
	public void unselectAll() {
		list.getTable().setValue(null);
	}
	public void select(ReportFieldTemplate rft) {
		unselectAll();
		list.getTable().select(rft);
		list.getTable().setCurrentPageFirstItemId(rft);
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return "";//form.checkDirty();
	}
	
	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return false;//form.isDirty();
	}
}