// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.ui;

import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Property;
import com.vaadin.ui.Table;

public class UIList extends Table {
	private static final long serialVersionUID = 5744578797654469413L;

	public UIList(UIView uiView, UIViewContainer uiContainer) {
		super();
		setSizeFull();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
		setContainerDataSource(uiContainer);
		setVisibleColumns(vaadinUi.getStringArray("UIList.showColumnProperties"));
		setColumnHeaders(vaadinUi.getStringArray("UIList.showColumnHeaders"));
		setColumnCollapsingAllowed(false);
		setColumnReorderingAllowed(false);
        // Make table selectable, react immediately to user events, and pass events to the GroupView
        setSelectable(true);
        setImmediate(true);
        addValueChangeListener((Property.ValueChangeListener)uiView);
        setNullSelectionAllowed(true);
	}
}