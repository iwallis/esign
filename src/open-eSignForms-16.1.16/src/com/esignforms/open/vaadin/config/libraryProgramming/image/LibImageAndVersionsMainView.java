// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.image;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.ImageInfo;
import com.esignforms.open.prog.ImageVersionInfo;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

/**
 * LibImageAndVersionsMainView is a splitpanel that contains the image view in the left and the image version view on the right.
 * 
 * Each of those views is itself a splitpanel, containing a table/list on the top to select items, with a form on the bottom to view/edit.
 * 
 * When a new image is selected in the left view, we propagate that to the right view so it can sync the versions with the selected image.
 * 
 * @author Yozons
 *
 */
public class LibImageAndVersionsMainView extends HorizontalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = -1702894724935295471L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibImageAndVersionsMainView.class);

    EsfUUID libraryId;
    Library library;
    final LibImageAndVersionsMainView thisView;
	
    VerticalSplitPanel imageSplitPanel;
    LibImageBeanContainer imageContainer;
    LibImageList imageList;
    LibImageForm imageForm;
	
    VerticalSplitPanel imageVerSplitPanel;
    LibImageVersionBeanContainer imageVerContainer;
	LibImageVersionList imageVerList;
	LibImageVersionForm imageVerForm;

	public LibImageAndVersionsMainView() {
		super(); 
		thisView = this;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			library = Library.Manager.getById(libraryId, vaadinUi.getUser());
			
			imageContainer = new LibImageBeanContainer( library);
			imageVerContainer = new LibImageVersionBeanContainer(this); // Load contents when a image is selected from our imageContainer
			
			imageList = new LibImageList(this,imageContainer);
		    imageForm = new LibImageForm(this,imageContainer);

			imageSplitPanel = new VerticalSplitPanel();
			imageSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			imageSplitPanel.setSplitPosition(30);
			imageSplitPanel.setFirstComponent(imageList);
			imageSplitPanel.setSecondComponent(imageForm);
		    
			imageVerList = new LibImageVersionList(this,imageVerContainer);
			imageVerForm = new LibImageVersionForm(this,imageVerContainer);
			
			imageVerSplitPanel = new VerticalSplitPanel();
			imageVerSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			imageVerSplitPanel.setSplitPosition(30);
			imageVerSplitPanel.setFirstComponent(imageVerList);
			imageVerSplitPanel.setSecondComponent(imageVerForm);

			setFirstComponent(imageSplitPanel);
			setSecondComponent(imageVerSplitPanel);
			setSplitPosition(35);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Image and versions main view exception", e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	private LibImageBean getLibImageBean(Item imageListItem) {
    	if ( imageListItem == null )
    		return null;
		BeanItem<LibImageBean> bi = (BeanItem<LibImageBean>)imageListItem;
		return bi.getBean();
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our image list or not
        if (property == imageList.getTable()) {
        	final Item item = imageList.getTable().getItem(imageList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( imageForm.getCurrentBean() == imageForm.getBean(item) ) {
    				// We've selected the same bean so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibImageAndVersionsMainView.Image.ConfirmDiscardChangesDialog.message", imageForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						thisView.discardForms();
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						imageList.getTable().select(imageForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibImageAndVersionsMainView.Image.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	LibImageBean bean = getLibImageBean(item);
                imageForm.setItemDataSource(item);
            	if ( bean != null ) {
                	imageVerContainer.reload(bean.image());
                	selectFirstImageVersion();
            	} else {
            		imageVerForm.setItemDataSource(null);
            		imageVerContainer.removeAllItems();
            	}
    		}
        } else if (property == imageVerList.getTable()) {
        	final Item item = imageVerList.getTable().getItem(imageVerList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( imageVerForm.getCurrentBean() == imageVerForm.getBean(item) ) {
    				// We've selected the same bean (version) so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibImageAndVersionsMainView.Image.ConfirmDiscardChangesDialog.message", imageForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						imageVerForm.discard();
						imageVerForm.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						imageVerList.getTable().select(imageVerForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibImageAndVersionsMainView.Image.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	imageVerForm.setItemDataSource(item);
    		}
        }
        else {
        	_logger.debug("valueChange() - no list event: " + event.toString());
        }
	}
	

	@Override
	public void detach() {
		/*
		for( Object o : list.getTable().getVisibleColumns() ) {
			_logger.debug("detach() - list visible columns[]: " + o); // These are string property ids of the column names like email, status, lastLoginTimestamp
		}
		*/
		super.detach();
	}
	
	public void selectImage(LibImageBean bean) {
		unselectAllImages();
		imageList.getTable().select(bean);
		imageList.getTable().setCurrentPageFirstItemId(bean);
		if ( bean == null ) {
			imageVerForm.setItemDataSource(null);
		}
	}
	
	public void unselectAllImages() {
		imageList.getTable().setValue(null);
		imageForm.setItemDataSource(null);
	}
	
	public void selectImageVersion(LibImageVersionBean bean) {
		unselectAllImageVersions();
		imageVerList.getTable().select(bean);
		imageVerList.getTable().setCurrentPageFirstItemId(bean);
	}

	public void discardForms() {
		imageForm.discard();
		imageVerForm.discard();
		imageForm.setItemDataSource(null);
		imageVerForm.setItemDataSource(null);
	}

	
	public void selectFirstImageVersion() {
		if ( imageVerContainer.size() > 0 ) {
			selectImageVersion(imageVerContainer.getIdByIndex(0));
		} else {
			unselectAllImageVersions(); 
		}
	}
	
	public void unselectAllImageVersions() {
		imageVerList.getTable().setValue(null);
	}

	public Library getLibrary() {
		return library;
	}
	
	public LibImageBean getLibImageBean() {
		return imageForm == null ? null : imageForm.getCurrentBean();
	}
	
	public LibImageVersionBean getLibImageVersionBean() {
		return imageVerForm == null ? null : imageVerForm.getCurrentBean();
	}
	
	public String getEsfNameWithVersion() {
		LibImageBean imageBean = getLibImageBean();
		LibImageVersionBean imageVerBean = getLibImageVersionBean();
		return imageVerBean == null ? "?? [?]" : imageBean.getEsfName() + " [" + imageVerBean.getVersion() + "]";
	}
	
	public Integer getProductionVersion() {
		LibImageBean imageBean = getLibImageBean();
		return imageBean == null ? 0 : imageBean.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		LibImageBean imageBean = getLibImageBean();
		return imageBean == null ? 0 : imageBean.getTestVersion();
	}

	public boolean hasTestVersion() {
		LibImageBean imageBean = getLibImageBean();
		return imageBean == null ? false : imageBean.image().hasTestVersion();
	}
	
    public boolean isProductionVersion(ImageVersionInfo imageVer)
    {
    	LibImageBean imageBean = getLibImageBean();
    	return imageBean != null && imageVer != null && imageVer.getVersion() == imageBean.getProductionVersion();
    }

    public boolean isTestVersion(ImageVersionInfo imageVer)
    {
    	LibImageBean imageBean = getLibImageBean();
    	return imageBean != null && imageVer != null && imageVer.getVersion() > imageBean.getProductionVersion();
    }

    // "Current" here means latest production or test version.
    public boolean isCurrentVersion(ImageVersionInfo imageVer)
    {
    	LibImageBean imageBean = getLibImageBean();
    	return imageBean != null && imageVer != null && imageVer.getVersion() >= imageBean.getProductionVersion();
    }

    public boolean isOldVersion(ImageVersionInfo imageVer)
    {
    	LibImageBean imageBean = getLibImageBean();
    	return imageBean != null && imageVer != null && imageVer.getVersion() < imageBean.getProductionVersion();
    }
	
	public String getVersionLabel(ImageVersionInfo imageVer) {
		if ( isTestVersion(imageVer) )
			return Literals.VERSION_LABEL_TEST;
		if ( isProductionVersion(imageVer) )
			return Literals.VERSION_LABEL_PRODUCTION;
		return Literals.VERSION_LABEL_NOT_CURRENT;
	}
	
	public boolean hasPermViewDetails() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_DELETE);
	}
	
	public boolean cancelChanges() {
		imageForm.discard();
		imageVerForm.discard();
		imageForm.setReadOnly(true);
		imageVerForm.setReadOnly(true);
		return true;
	}
	
	public boolean enterEditMode() {
		imageForm.setReadOnly(false);
		imageVerForm.setReadOnly(false);
		return true;
	}
	
	public boolean save() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
		if ( ! imageForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		if ( ! imageVerForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		imageForm.commit();
		imageVerForm.commit();
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			// If we're saving a new test version, we'll want to bump our image to show this as the new test version
			LibImageBean currImage = getLibImageBean();
			LibImageVersionBean currImageVer = getLibImageVersionBean();
			if ( currImageVer.imageVersion().getVersion() > currImage.image().getTestVersion() )
				currImage.image().bumpTestVersion();
			
			LibImageBean savedImage = imageForm.save(con);
			if ( savedImage == null )  {
				con.rollback();
				return false;
			}
			
			LibImageVersionBean savedImageVer = imageVerForm.save(con);
			if ( savedImageVer == null )  {
				con.rollback();
				return false;
			}
			
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Saved image " + savedImage.getEsfName() + " version " + savedImageVer.getVersion() + ". ImageVersionId: " + savedImageVer.getId() + "; status: " + savedImage.getStatus());
			}

			con.commit();
	    		
			imageContainer.refresh();  // refresh our list after a change
			selectImage(savedImage);
			selectImageVersion(savedImageVer);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean createNewImage() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ImageInfo imageInfo = ImageInfo.Manager.createNew(getLibrary().getId());
		LibImageBean imageBean = new LibImageBean(imageInfo);
		BeanItem<LibImageBean> imageItem = new BeanItem<LibImageBean>(imageBean);
        imageForm.setItemDataSource(imageItem);
		
		ImageVersionInfo imageVerInfo = ImageVersionInfo.Manager.createNew(Image.Manager.getById(imageInfo.getId()),vaadinUi.getUser());
		LibImageVersionBean imageVerBean = new LibImageVersionBean(imageVerInfo,getVersionLabel(imageVerInfo));
		BeanItem<LibImageVersionBean> imageVerItem = new BeanItem<LibImageVersionBean>(imageVerBean);
        imageVerForm.setItemDataSource(imageVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean createLikeImage() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibImageBean likeImageBean = getLibImageBean();
		LibImageVersionBean likeImageVerBean = getLibImageVersionBean();
		
    	LibImageBean createdImageBean = likeImageBean.createLike();
		BeanItem<LibImageBean> createdImageItem = new BeanItem<LibImageBean>(createdImageBean);
        imageForm.setItemDataSource(createdImageItem);

    	ImageVersionInfo createdImageVerInfo = ImageVersionInfo.Manager.createLike(Image.Manager.getById(createdImageBean.imageInfo().getId()), likeImageVerBean.imageVersion(), vaadinUi.getUser());
    	LibImageVersionBean createdImageVerBean = new LibImageVersionBean(createdImageVerInfo,Literals.VERSION_LABEL_TEST); // The new image and version will always be Test.
		BeanItem<LibImageVersionBean> createdImageVerItem = new BeanItem<LibImageVersionBean>(createdImageVerBean);       
        imageVerForm.setItemDataSource(createdImageVerItem);
        
        setReadOnly(false);
		return true;
	}

	public boolean createNextVersion() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibImageBean currImageBean = getLibImageBean();
		LibImageVersionBean currImageVerBean = getLibImageVersionBean();
		
        imageForm.setReadOnly(!hasPermUpdate()); // should have permission since doing a create next version...

    	ImageVersionInfo nextImageVerInfo = ImageVersionInfo.Manager.createLike(currImageBean.image(), currImageVerBean.imageVersion(), vaadinUi.getUser());
    	LibImageVersionBean nextImageVerBean = new LibImageVersionBean(nextImageVerInfo,Literals.VERSION_LABEL_TEST); // The new version will always be Test.
		BeanItem<LibImageVersionBean> nextImageVerItem = new BeanItem<LibImageVersionBean>(nextImageVerBean);
        imageVerForm.setItemDataSource(nextImageVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean promoteTestToProductionVersion() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibImageVersionForm.versionChange.TestToProd.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibImageBean imageBean = getLibImageBean();
			LibImageVersionBean imageVerBean = getLibImageVersionBean();
			
			imageBean.image().promoteTestVersionToProduction();
			
			if ( ! imageBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved image " + imageBean.getEsfName() + " version " + imageVerBean.getVersion() + " into Production status. ImageVersionId: " + imageVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved image " + imageBean.getEsfName() + " version " + imageVerBean.getVersion() + " into Production status. ImageVersionId: " + imageVerBean.getId());
			}
			
			con.commit();
			imageContainer.refresh();  // refresh our list after a change
			selectImage(imageBean);
			selectImageVersion(imageVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibImageVersionForm.versionChange.TestToProd.success.message",imageBean.getEsfName(),imageVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean revertProductionVersionBackToTest() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibImageVersionForm.versionChange.revertProdToTest.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibImageBean imageBean = getLibImageBean();
			LibImageVersionBean imageVerBean = getLibImageVersionBean();
			
			imageBean.image().revertProductionVersionBackToTest();
			
			if ( ! imageBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved Production image " + imageBean.getEsfName() + " version " + imageVerBean.getVersion() + " back into Test status. ImageVersionId: " + imageVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved Production image " + imageBean.getEsfName() + " version " + imageVerBean.getVersion() + " back into Test status. ImageVersionId: " + imageVerBean.getId());
			}
			
			con.commit();
			imageContainer.refresh();  // refresh our list after a change
			selectImage(imageBean);
			selectImageVersion(imageVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibImageVersionForm.versionChange.revertProdToTest.success.message",imageBean.getEsfName(),imageVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean delete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibImageBean currImage = getLibImageBean();
			LibImageVersionBean currImageVer = getLibImageVersionBean();
			
			// If there is no Production version, we'll get rid of the image entirely, otherwise just the test version
			Errors errors = new Errors();
			if ( currImage.getProductionVersion() == 0 )
				currImage.image().delete(con,errors,vaadinUi.getUser());
			else {
				currImageVer.imageVersion().delete(con,errors,vaadinUi.getUser());
				currImage.dropTestVersion();
				currImage.save(con, errors);
			}
			
			if ( errors.hasError() ) {
				vaadinUi.show(errors);
				con.rollback();
				return false;
			}
			
			con.commit();
    		
			vaadinUi.showStatus(vaadinUi.getMsg("LibImageVersionForm.button.delete.successMessage",currImage.getEsfName(),currImageVer.getVersion()));
			
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Deleted image " + currImage.getEsfName() + " version " + currImageVer.getVersion() + ". ImageVersionId: " + currImageVer.getId() + "; status: " + currImage.getStatus());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted image " + currImage.getEsfName() + " version " + currImageVer.getVersion() + ". ImageVersionId: " + currImageVer.getId() + "; status: " + currImage.getStatus());
			}

			imageContainer.refresh();  // refresh our list after a change
			selectImage(null);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		_logger.debug("activateView() - params: " + params);
		libraryId = new EsfUUID(params);
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return imageForm.isDirty() ? imageForm.checkDirty() : imageVerForm.checkDirty();
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return imageForm.isDirty() || imageVerForm.isDirty();
	}
}