// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document;

import java.io.Serializable;
import java.util.Collection;

import com.esignforms.open.prog.DocumentInfo;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.util.BeanItemContainer;


public class LibDocumentBeanContainer extends BeanItemContainer<LibDocumentBean> implements Serializable {
	private static final long serialVersionUID = -6255409079956195058L;

	final Library library;
	
	public LibDocumentBeanContainer(Library library) throws InstantiationException, IllegalAccessException {
		super(LibDocumentBean.class);
		this.library = library;
		
		refresh();
	}
	
	public void refresh() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		removeAllItems();
		
		// Users need view details permission to see its contents
		if ( library != null && library.hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS) ) {
			
			Collection<DocumentInfo> docs = DocumentInfo.Manager.getAll(library);
			for( DocumentInfo d : docs ) {
				addItem( new LibDocumentBean(d) );					
			}
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}