// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.transactionTemplate;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;

public class TransactionTemplateBean implements Serializable, Comparable<TransactionTemplateBean> {
	private static final long serialVersionUID = 1556907659878179831L;

	private TransactionTemplate transactionTemplate;
	
	// This contains all of the groups that the user can list, so these are the only ones that
	// can be used to add/remove groups (any groups that the Group object has that are not included
	// cannot be touched)
	private EsfUUID[] allGroupListIds;
	private EsfPathName[] allGroupListNames;

	private EsfUUID[] allGroupWithExternalListIds;
	private EsfPathName[] allGroupWithExternalListNames;

	private Set<EsfUUID> permListIds;
	private Set<EsfUUID> permViewDetailsIds;
	private Set<EsfUUID> permCreateLikeIds;
	private Set<EsfUUID> permUpdateIds;
	private Set<EsfUUID> permDeleteIds;
	
	private Set<EsfUUID> permTransactionStart;
	private Set<EsfUUID> permTransactionCancel;
	private Set<EsfUUID> permTransactionReactivate;
	private Set<EsfUUID> permTransactionSuspend;
	private Set<EsfUUID> permTransactionResume;
	private Set<EsfUUID> permTransactionUseUpdateApi;
	private Set<EsfUUID> permTransactionUseAdminApi;
	
	
	public TransactionTemplateBean(TransactionTemplate transactionTemplate) {
		this.transactionTemplate = transactionTemplate;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o !=null && o instanceof TransactionTemplateBean )
			return transactionTemplate.equals(((TransactionTemplateBean)o).transactionTemplate);
		return false;
	}
	@Override
    public int hashCode() {
    	return transactionTemplate.hashCode();
    }
	public int compareTo(TransactionTemplateBean o) {
		return transactionTemplate.compareTo(o.transactionTemplate);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	protected User user() {
		return EsfVaadinUI.getInstance().getUser();
	}
	public TransactionTemplate transactionTemplate() {
		return transactionTemplate;
	}

	public TransactionTemplateBean createLike() {
		EsfPathName newPathName;
		String fromPathName = transactionTemplate.getPathName().toPlainString();
		if ( EsfPathName.isReservedPathPrefix(fromPathName) ) { // don't let them duplicate with the reserved path prefix
			fromPathName = fromPathName.substring(EsfPathName.ESF_RESERVED_PATH_PREFIX.length());
			newPathName = new EsfPathName(fromPathName);
		} else {
			newPathName = transactionTemplate.getPathName();
		}
	    TransactionTemplate newTransactionTemplate = TransactionTemplate.Manager.createLike(transactionTemplate, newPathName, user());	    
	    return new TransactionTemplateBean(newTransactionTemplate);
	}
	
	public boolean save(Errors errors) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		// If production is enabled, we want to make sure there's a production package. 
		if ( transactionTemplate.isProductionEnabled() ) {
			Package pkg = transactionTemplate.getPackage();
			if ( ! pkg.hasProductionVersion() ) {
				if ( errors != null ) {
					errors.addError(vaadinUi.getMsg("TransactionTemplateForm.production.status.error.noProductionPackage"));
				}
				transactionTemplate.makeProductionDisabled();
				return false;
			} else if ( pkg.hasTestVersion() ) {
				if ( errors != null ) {
					errors.addWarning(vaadinUi.getMsg("TransactionTemplateForm.production.status.warning.hasTestPackage"));
				}
			}
		}
		
		boolean ret = transactionTemplate.save(user());
		if ( ret ) {
			resetCaches();
		} else {
			if ( errors != null ) {
				errors.addError(vaadinUi.getMsg("form.save.failed.message"));
			}
		}
		return ret;
	}
	public boolean save() {
		return save(null);
	}
	
	public boolean delete(Errors errors) {
		if ( transactionTemplate.isReserved() )
			return false;
		return transactionTemplate.delete(errors,user());
	}

	protected HashSet<EsfUUID> createIdHashSet( Collection<Group> list ) {
		HashSet<EsfUUID> set = new HashSet<EsfUUID>(list.size());
		for( Group g : list ) {
			set.add(g.getId());
		}
		return set;
	}
	
	private synchronized void resetCaches() {
		allGroupListIds = null;
		allGroupListNames = null;
		allGroupWithExternalListIds = null;
		allGroupWithExternalListNames = null;
		permListIds = null;
		permViewDetailsIds = null;
		permCreateLikeIds = null;
		permUpdateIds = null;
		permDeleteIds = null;
		permTransactionStart = null;
		permTransactionCancel = null;
		permTransactionReactivate = null;
		permTransactionSuspend = null;
		permTransactionResume = null;
		permTransactionUseUpdateApi = null;
		permTransactionUseAdminApi = null;
	}

	protected synchronized void createAllGroupLists() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Collection<Group> list = Group.Manager.getForUserWithListPermission(user(),Group.INCLUDE.BOTH_ENABLED_AND_DISABLED);
		
		int size = list.size();
		
		// We will not include the "External Users" pseudo-group.
		allGroupListIds = new EsfUUID[size-1];
		allGroupListNames = new EsfPathName[size-1];
		int i = 0;
		for( Group g : list ) {
			if ( g.isExternalUsersGroup() )
				continue;
			allGroupListIds[i] = g.getId();
			allGroupListNames[i] = g.isDisabled() ? new EsfPathName(g.getPathName() + EsfPathName.PATH_SEPARATOR + vaadinUi.getMsg("PrettyCode.status.disabled")) : g.getPathName();
			++i;
		}
	}
	public EsfUUID[] allGroupListIds() {
		if ( allGroupListIds == null ) {
			createAllGroupLists();
		}
		return allGroupListIds;
	}
	public EsfPathName[] allGroupListNames() {
		if ( allGroupListNames == null ) {
			createAllGroupLists();
		}
		return allGroupListNames;
	}
	
	protected synchronized void createAllGroupWithExternalLists() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Collection<Group> list = Group.Manager.getForUserWithListPermission(user(),Group.INCLUDE.BOTH_ENABLED_AND_DISABLED);
		
		int size = list.size();
		
		allGroupWithExternalListIds = new EsfUUID[size];
		allGroupWithExternalListNames = new EsfPathName[size];
		int i = 0;
		for( Group g : list ) {
			allGroupWithExternalListIds[i] = g.getId();
			allGroupWithExternalListNames[i] = g.isDisabled() ? new EsfPathName(g.getPathName() + EsfPathName.PATH_SEPARATOR + vaadinUi.getMsg("PrettyCode.status.disabled")) : g.getPathName();
			++i;
		}
	}
	public EsfUUID[] allGroupWithExternalListIds() {
		if ( allGroupWithExternalListIds == null ) {
			createAllGroupWithExternalLists();
		}
		return allGroupWithExternalListIds;
	}
	public EsfPathName[] allGroupWithExternalListNames() {
		if ( allGroupWithExternalListNames == null ) {
			createAllGroupWithExternalLists();
		}
		return allGroupWithExternalListNames;
	}
	

	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return transactionTemplate.getId();
	}
	
	public EsfPathName getPathName() {
		return transactionTemplate.getPathName();
	}
	public void setPathName(EsfPathName v) {
		if ( ! transactionTemplate.isReserved() ) {
			transactionTemplate.setPathName( v );
		}
	}
	
	public boolean isProductionEnabled() {
		return transactionTemplate.isProductionEnabled();
	}
	public String getProductionStatus() {
		return transactionTemplate.getProductionStatus();
	}
	public void setProductionStatus( String v ) {
		if ( ! transactionTemplate.isReserved() ) {
			transactionTemplate.setProductionStatus(v);
		}
	}
	
	public boolean isTestEnabled() {
		return transactionTemplate.isTestEnabled();
	}
	public String getTestStatus() {
		return transactionTemplate.getTestStatus();
	}
	public void setTestStatus( String v ) {
		if ( ! transactionTemplate.isReserved() ) {
			transactionTemplate.setTestStatus(v);
		}
	}
	
	public String getDisplayName() {
		return transactionTemplate.getDisplayName();
	}
	public void setDisplayName(String v) {
		transactionTemplate.setDisplayName(v);
	}
	
	public String getDescription() {
		return transactionTemplate.getDescription();
	}
	public void setDescription(String v) {
		transactionTemplate.setDescription(v);
	}
	
	public String getComments() {
		return transactionTemplate.getComments();
	}
	public void setComments(String v) {
		transactionTemplate.setComments( v );
	}
	
    public EsfUUID getPackageId() {
    	return transactionTemplate.getPackageId();
    }
    public void setPackageId(EsfUUID v) {
    	transactionTemplate.setPackageId(v);
    }
    public String getPackagePathName() {
    	return Package.Manager.getById(getPackageId()).getPathName().toString();
    }

    public EsfUUID getBrandLibraryId() {
    	return transactionTemplate.getBrandLibraryId();
    }
    public boolean hasBrandLibraryId() {
    	return transactionTemplate.hasBrandLibraryId();
    }
    public String getBrandLibraryPathName() {
    	return hasBrandLibraryId() ? Library.Manager.getById(getBrandLibraryId()).getPathName().toString() : "";
    }
    public void setBrandLibraryId(EsfUUID v) {
    	transactionTemplate.setBrandLibraryId(v);
    }

    public int getProductionRetentionNumTimeIntervalUnits() {
    	return transactionTemplate.getProductionRetentionNumTimeIntervalUnits();
    }
    public void setProductionRetentionNumTimeIntervalUnits(int v) {
    	transactionTemplate.setProductionRetentionSpec(v + " " + transactionTemplate.getProductionRetentionTimeIntervalUnits());
    }

    public String getProductionRetentionTimeIntervalUnits() {
    	return transactionTemplate.getProductionRetentionTimeIntervalUnits();
    }
    public void setProductionRetentionTimeIntervalUnits(String v) {
    	transactionTemplate.setProductionRetentionSpec(transactionTemplate.getProductionRetentionNumTimeIntervalUnits() + " " + v);
    }

    public int getTestRetentionNumTimeIntervalUnits() {
    	return transactionTemplate.getTestRetentionNumTimeIntervalUnits();
    }
    public void setTestRetentionNumTimeIntervalUnits(int v) {
    	transactionTemplate.setTestRetentionSpec(v + " " + transactionTemplate.getTestRetentionTimeIntervalUnits());
    }

    public String getTestRetentionTimeIntervalUnits() {
    	return transactionTemplate.getTestRetentionTimeIntervalUnits();
    }
    public void setTestRetentionTimeIntervalUnits(String v) {
    	transactionTemplate.setTestRetentionSpec(transactionTemplate.getTestRetentionNumTimeIntervalUnits() + " " + v);
    }
    
    public String getProductionRetentionLabel() {
    	if ( transactionTemplate.isProductionRetentionForever() )
    		return transactionTemplate.getTimeIntervalUnitLabel(Literals.TIME_INTERVAL_UNIT_FOREVER);
    	if ( transactionTemplate.isProductionRetentionNow() )
    		return transactionTemplate.getTimeIntervalUnitLabel(Literals.TIME_INTERVAL_UNIT_NOW);
    	return transactionTemplate.getProductionRetentionNumTimeIntervalUnits() + " " + transactionTemplate.getTimeIntervalUnitLabel(transactionTemplate.getProductionRetentionTimeIntervalUnits());
    }

	protected void setPermIds(Set<EsfUUID> v, EsfName permOption) {
		// First, let's remove all groups this user can list
		for( Group g : Group.Manager.getForUserWithListPermission(user()) ) {
			transactionTemplate.removePermissionAllowedGroups(permOption, g);
		}
		// Then let's add back in the selected set
		for( EsfUUID gid : v ) {
			Group g = Group.Manager.getById( gid );
			if ( g != null ) {
				transactionTemplate.addPermissionAllowedGroups(permOption, g);
			}
		}
	}

	public Set<EsfUUID> getPermListIds() {
		if ( permListIds == null ) {
			permListIds = createIdHashSet( transactionTemplate.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_LIST) );
		}
		return permListIds;
	}	
	public void setPermListIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_LIST);
	}

	public Set<EsfUUID> getPermViewDetailsIds() {
		if ( permViewDetailsIds == null ) {
			permViewDetailsIds = createIdHashSet( transactionTemplate.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_VIEWDETAILS) );
		}
		return permViewDetailsIds;
	}
	public void setPermViewDetailsIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public Set<EsfUUID> getPermCreateLikeIds() {
		if ( permCreateLikeIds == null ) {
			permCreateLikeIds = createIdHashSet( transactionTemplate.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_CREATELIKE) );
		}
		return permCreateLikeIds;
	}
	public void setPermCreateLikeIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public Set<EsfUUID> getPermUpdateIds() {
		if ( permUpdateIds == null ) {
			permUpdateIds = createIdHashSet( transactionTemplate.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_UPDATE) );
		}
		return permUpdateIds;
	}
	public void setPermUpdateIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_UPDATE);
	}

	public Set<EsfUUID> getPermDeleteIds() {
		if ( permDeleteIds == null ) {
			permDeleteIds = createIdHashSet( transactionTemplate.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_DELETE) );
		}
		return permDeleteIds;
	}
	public void setPermDeleteIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_DELETE);
	}

	public Set<EsfUUID> getPermTransactionStartIds() {
		if ( permTransactionStart == null ) {
			permTransactionStart = createIdHashSet( transactionTemplate.getPermissionAllowedGroups(user(), PermissionOption.TRAN_PERM_OPTION_START) );
		}
		return permTransactionStart;
	}
	public void setPermTransactionStartIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.TRAN_PERM_OPTION_START);
	}

	public Set<EsfUUID> getPermTransactionCancelIds() {
		if ( permTransactionCancel == null ) {
			permTransactionCancel = createIdHashSet( transactionTemplate.getPermissionAllowedGroups(user(), PermissionOption.TRAN_PERM_OPTION_CANCEL) );
		}
		return permTransactionCancel;
	}
	public void setPermTransactionCancelIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.TRAN_PERM_OPTION_CANCEL);
	}

	public Set<EsfUUID> getPermTransactionReactivateIds() {
		if ( permTransactionReactivate == null ) {
			permTransactionReactivate = createIdHashSet( transactionTemplate.getPermissionAllowedGroups(user(), PermissionOption.TRAN_PERM_OPTION_REACTIVATE) );
		}
		return permTransactionReactivate;
	}
	public void setPermTransactionReactivateIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.TRAN_PERM_OPTION_REACTIVATE);
	}

	public Set<EsfUUID> getPermTransactionSuspendIds() {
		if ( permTransactionSuspend == null ) {
			permTransactionSuspend = createIdHashSet( transactionTemplate.getPermissionAllowedGroups(user(), PermissionOption.TRAN_PERM_OPTION_SUSPEND) );
		}
		return permTransactionSuspend;
	}
	public void setPermTransactionSuspendIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.TRAN_PERM_OPTION_SUSPEND);
	}

	public Set<EsfUUID> getPermTransactionResumeIds() {
		if ( permTransactionResume == null ) {
			permTransactionResume = createIdHashSet( transactionTemplate.getPermissionAllowedGroups(user(), PermissionOption.TRAN_PERM_OPTION_RESUME) );
		}
		return permTransactionResume;
	}
	public void setPermTransactionResumeIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.TRAN_PERM_OPTION_RESUME);
	}

	public Set<EsfUUID> getPermTransactionUseUpdateApiIds() {
		if ( permTransactionUseUpdateApi == null ) {
			permTransactionUseUpdateApi = createIdHashSet( transactionTemplate.getPermissionAllowedGroups(user(), PermissionOption.TRAN_PERM_OPTION_USE_UPDATE_API) );
		}
		return permTransactionUseUpdateApi;
	}
	public void setPermTransactionUseUpdateApiIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.TRAN_PERM_OPTION_USE_UPDATE_API);
	}

	public Set<EsfUUID> getPermTransactionUseAdminApiIds() {
		if ( permTransactionUseAdminApi == null ) {
			permTransactionUseAdminApi = createIdHashSet( transactionTemplate.getPermissionAllowedGroups(user(), PermissionOption.TRAN_PERM_OPTION_USE_ADMIN_API) );
		}
		return permTransactionUseAdminApi;
	}
	public void setPermTransactionUseAdminApiIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.TRAN_PERM_OPTION_USE_ADMIN_API);
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return transactionTemplate.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		return transactionTemplate.getLastUpdatedTimestamp().toString(user());
	}
	public String formatLogLastUpdatedTimestamp() {
		return transactionTemplate.getLastUpdatedTimestamp().toLogString(user());
	}

	public String formatCreatedTimestamp() {
		return transactionTemplate.getCreatedTimestamp().toString(user());
	}
	public String formatLogCreatedTimestamp() {
		return transactionTemplate.getCreatedTimestamp().toLogString(user());
	}

	public boolean hasPermViewDetails() {
		return transactionTemplate.hasPermission(user(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		return transactionTemplate.hasPermission(user(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		return transactionTemplate.hasPermission(user(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		return transactionTemplate.isReserved() ? false : transactionTemplate.hasPermission(user(), PermissionOption.PERM_OPTION_DELETE);
	}
	
}