// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.library;

import java.io.StringReader;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;

import com.esignforms.open.Errors;
import com.esignforms.open.Version;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.prog.ButtonMessage;
import com.esignforms.open.prog.ButtonMessageVersion;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentStyle;
import com.esignforms.open.prog.DocumentStyleVersion;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.EmailTemplateVersion;
import com.esignforms.open.prog.File;
import com.esignforms.open.prog.FileVersion;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.PropertySet;
import com.esignforms.open.prog.PropertySetVersion;
import com.esignforms.open.util.XmlUtil;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.widget.ListSelectValid;
import com.esignforms.open.vaadin.widget.UploadFileWithProgress;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class ImportView extends Panel implements EsfView {
	private static final long serialVersionUID = 3078706842124430088L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ImportView.class);
	
    final ImportView thisView;
    final Library library;
    
    UploadFileWithProgress uploadFile;
	Namespace ns = Namespace.getNamespace(XmlUtil.getXmlNamespace2011());
	Namespace ns2012 = Namespace.getNamespace(XmlUtil.getXmlNamespace2012());
    // Uploaded data
    EsfUUID deployId;
    String release;
    int versionMajor;
    int versionMinor;
    EsfDateTime timestamp;
    EsfPathName libraryPathName;
	
    CheckBox overwriteTestVersion;
    
    List<Element> documentElements;
    List<Element> documentVersionElements;
	ListSelect documentSelect;

    List<Element> buttonMessageElements;
    List<Element> buttonMessageVersionElements;
	ListSelect buttonMessageSelect;
	
    List<Element> documentStyleElements;
    List<Element> documentStyleVersionElements;
	ListSelect documentStyleSelect;
	
    List<Element> dropDownElements;
    List<Element> dropDownVersionElements;
	ListSelect dropDownSelect;
	
    List<Element> emailElements;
    List<Element> emailVersionElements;
	ListSelect emailSelect;
	
    List<Element> fileElements;
    List<Element> fileVersionElements;
	ListSelect fileSelect;
	
    List<Element> imageElements;
    List<Element> imageVersionElements;
	ListSelect imageSelect;
	
    List<Element> propertySetElements;
    List<Element> propertySetVersionElements;
	ListSelect propertySetSelect;
    
	public ImportView(Library library) {
		super();
		thisView = this;
		this.library = library;
		setStyleName("LibraryFormImportView");
		setSizeFull();
	}

	protected void buildUploadLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
	        VerticalLayout layout = new VerticalLayout();
	        setContent(layout);
			layout.setSizeUndefined();
			layout.setSpacing(true);
			layout.setMargin(true);
			
			layout.addComponent( new Label(vaadinUi.getMsg("")) );

			uploadFile = new UploadFileWithProgress(null,vaadinUi.getMsg("LibraryForm.ImportView.button.upload.label"),(int)vaadinUi.getEsfapp().getUploadConfigMaxBytes()) {
				private static final long serialVersionUID = -7472228644172610548L;

				@Override
				public void afterUploadSucceeded() {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					String xmlData = EsfString.bytesToString(uploadFile.getFileData());
					Errors errors = processImportedXml(xmlData);
					if ( errors.hasError() ) {
						vaadinUi.show(errors);
					}  else {
						vaadinUi.showStatus(vaadinUi.getMsg("LibraryForm.ImportView.upload.successful",uploadFile.getFileName(),EsfInteger.byteSizeInUnits(uploadFile.getFileData().length) ));
					}
				}
				@Override
				public void afterUploadFailed(String fileName, boolean wasCanceled, boolean wasInvalidMimeType, boolean wasTooBig, int contentLength, int maxSize) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					if ( wasTooBig ) {
						vaadinUi.showError( 
								vaadinUi.getMsg("LibraryForm.ImportView.fileTooLarge.caption"), 
								vaadinUi.getMsg("LibraryForm.ImportView.fileTooLarge.message",EsfInteger.byteSizeInUnits(contentLength),EsfInteger.byteSizeInUnits(maxSize))
								);
					} else if ( wasCanceled ) {
						vaadinUi.showStatus(vaadinUi.getMsg("LibraryForm.ImportView.upload.canceled"));
					} else if ( wasInvalidMimeType ) {
						vaadinUi.showError(
								vaadinUi.getMsg("LibraryForm.ImportView.invalidType.caption"), 
								vaadinUi.getMsg("LibraryForm.ImportView.invalidType.message",EsfString.isBlank(fileName)?"(unknown)":fileName)
								);
					}
				}
			}; 
			uploadFile.setDescription(vaadinUi.getMsg("LibraryForm.ImportView.button.upload.tooltip"));
			uploadFile.setAllowedMimeTypesForXml();
			layout.addComponent(uploadFile);

	    	_logger.debug("buildUploadLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build upload view", e);
			vaadinUi.showError("Library ImportView view exception", e.getMessage());
		}
	}
	
	protected void buildImportLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
	        VerticalLayout layout = new VerticalLayout();
	        setContent(layout);
			layout.setSizeUndefined();
			layout.setSpacing(true);
			layout.setMargin(false);

	    	VerticalLayout optionsLayout = new VerticalLayout();
	    	optionsLayout.addStyleName("optionsLayout");
	    	optionsLayout.setSpacing(true);
	    	optionsLayout.setMargin(new MarginInfo(false,true,false,true));
	    	
	    	Label info = new Label(vaadinUi.getMsg("LibraryForm.ImportView.info.label",libraryPathName,deployId,release,timestamp));
	    	info.setWidth(100, Unit.PERCENTAGE);
	    	optionsLayout.addComponent(info);

	    	overwriteTestVersion = new CheckBox(vaadinUi.getMsg("LibraryForm.ImportView.overwriteTestVersion.checkbox.label"));
	    	overwriteTestVersion.setValue(true);
	    	optionsLayout.addComponent(overwriteTestVersion);
	    	
	    	HorizontalLayout listSelectLayout = new HorizontalLayout();
	    	listSelectLayout.addStyleName("listSelectLayout");
	    	listSelectLayout.setSpacing(true);
	    	listSelectLayout.setMargin(new MarginInfo(false,true,false,true));
	    	
	    	if ( documentElements != null && documentElements.size() > 0 ) {
				documentSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ImportView.document.select.label"));
				documentSelect.setDescription(vaadinUi.getMsg("LibraryForm.ImportView.document.select.tooltip"));
				documentSelect.setRequired(false);
				documentSelect.setMultiSelect(true);
				documentSelect.setNullSelectionAllowed(true);
				documentSelect.setRows(20);
				listSelectLayout.addComponent(documentSelect);
	    	}
			
	    	if ( buttonMessageElements != null && buttonMessageElements.size() > 0 ) {
	    		buttonMessageSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ImportView.buttonMessage.select.label"));
	    		buttonMessageSelect.setDescription(vaadinUi.getMsg("LibraryForm.ImportView.buttonMessage.select.tooltip"));
	    		buttonMessageSelect.setRequired(false);
	    		buttonMessageSelect.setMultiSelect(true);
	    		buttonMessageSelect.setNullSelectionAllowed(true);
	    		buttonMessageSelect.setRows(20);
				listSelectLayout.addComponent(buttonMessageSelect);
	    	}
			
	    	if ( documentStyleElements != null && documentStyleElements.size() > 0 ) {
				documentStyleSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ImportView.documentStyle.select.label"));
				documentStyleSelect.setDescription(vaadinUi.getMsg("LibraryForm.ImportView.documentStyle.select.tooltip"));
				documentStyleSelect.setRequired(false);
				documentStyleSelect.setMultiSelect(true);
				documentStyleSelect.setNullSelectionAllowed(true);
				documentStyleSelect.setRows(20);
				listSelectLayout.addComponent(documentStyleSelect);
	    	}
			
	    	if ( dropDownElements != null && dropDownElements.size() > 0 ) {
				dropDownSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ImportView.dropDown.select.label"));
				dropDownSelect.setDescription(vaadinUi.getMsg("LibraryForm.ImportView.dropDown.select.tooltip"));
				dropDownSelect.setRequired(false);
				dropDownSelect.setMultiSelect(true);
				dropDownSelect.setNullSelectionAllowed(true);
				dropDownSelect.setRows(20);
				listSelectLayout.addComponent(dropDownSelect);
	    	}
			
	    	if ( emailElements != null && emailElements.size() > 0 ) {
				emailSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ImportView.email.select.label"));
				emailSelect.setDescription(vaadinUi.getMsg("LibraryForm.ImportView.email.select.tooltip"));
				emailSelect.setRequired(false);
				emailSelect.setMultiSelect(true);
				emailSelect.setNullSelectionAllowed(true);
				emailSelect.setRows(20);
				listSelectLayout.addComponent(emailSelect);
	    	}

	    	if ( fileElements != null && fileElements.size() > 0 ) {
				fileSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ImportView.file.select.label"));
				fileSelect.setDescription(vaadinUi.getMsg("LibraryForm.ImportView.file.select.tooltip"));
				fileSelect.setRequired(false);
				fileSelect.setMultiSelect(true);
				fileSelect.setNullSelectionAllowed(true);
				fileSelect.setRows(20);
				listSelectLayout.addComponent(fileSelect);
	    	}

	    	if ( imageElements != null && imageElements.size() > 0 ) {
				imageSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ImportView.image.select.label"));
				imageSelect.setDescription(vaadinUi.getMsg("LibraryForm.ImportView.image.select.tooltip"));
				imageSelect.setRequired(false);
				imageSelect.setMultiSelect(true);
				imageSelect.setNullSelectionAllowed(true);
				imageSelect.setRows(20);
				listSelectLayout.addComponent(imageSelect);
	    	}
			
	    	if ( propertySetElements != null && propertySetElements.size() > 0 ) {
				propertySetSelect = new ListSelectValid(vaadinUi.getMsg("LibraryForm.ImportView.propertySet.select.label"));
				propertySetSelect.setDescription(vaadinUi.getMsg("LibraryForm.ImportView.propertySet.select.tooltip"));
				propertySetSelect.setRequired(false);
				propertySetSelect.setMultiSelect(true);
				propertySetSelect.setNullSelectionAllowed(true);
				propertySetSelect.setRows(20);
				listSelectLayout.addComponent(propertySetSelect);
	    	}
	    	
	    	// If we have few selects, we'll set the width to help our 'info' element not be too narrow.
	    	if ( listSelectLayout.getComponentCount() < 4 )
	    		layout.setWidth(850, Unit.PIXELS);

	    	HorizontalLayout buttonLayout = new HorizontalLayout();
	    	buttonLayout.addStyleName("buttonLayout");
	    	buttonLayout.setSpacing(true);
	    	buttonLayout.setMargin(new MarginInfo(false,true,false,true));
	    	
			Button importButton = new Button(vaadinUi.getMsg("LibraryForm.ImportView.button.import.label"));
			importButton.setDescription(vaadinUi.getMsg("LibraryForm.ImportView.button.import.tooltip"));
			importButton.setStyleName(Reindeer.BUTTON_SMALL);
			importButton.addClickListener( new Button.ClickListener() {			
				private static final long serialVersionUID = 3255748479872276488L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					Errors errors = new Errors();
					doImport(errors);
					if ( errors.getNumErrorEntries() == 0 )
						errors.addWarning(vaadinUi.getMsg("LibraryForm.ImportView.importNoElements.message"));
					vaadinUi.show(errors);
					refreshSelections();
				}
			});
	    	buttonLayout.addComponent(importButton);

	    	layout.addComponent(optionsLayout);
	    	layout.addComponent(listSelectLayout);
	    	layout.addComponent(buttonLayout);
	    	
	    	refreshSelections();
			
	    	_logger.debug("buildImportLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Library ImportView view exception", e.getMessage());
		}
	}
	
	void refreshSelections() {
		if ( documentSelect != null ) {
			documentSelect.removeAllItems();
			if ( documentElements != null ) {
				HashSet<Element> selectedValues = new HashSet<Element>();
				ListIterator<Element> iter = documentElements.listIterator();
				while( iter.hasNext() ) {
					Element e = iter.next();
					EsfName esfname = EsfName.createFromToXml(e.getChildText("esfname",ns));
					if ( esfname.isValid() ) {
						selectedValues.add(e);
						documentSelect.addItem(e);
						documentSelect.setItemCaption(e, esfname.toString());
					}
				}
				documentSelect.setValue( selectedValues ); // select all
			}
		}

		if ( buttonMessageSelect != null ) {
			buttonMessageSelect.removeAllItems();
			if ( buttonMessageElements != null ) {
				HashSet<Element> selectedValues = new HashSet<Element>();
				ListIterator<Element> iter = buttonMessageElements.listIterator();
				while( iter.hasNext() ) {
					Element e = iter.next();
					EsfName esfname = EsfName.createFromToXml(e.getChildText("esfname",ns2012));
					if ( esfname.isValid() ) {
						selectedValues.add(e);
						buttonMessageSelect.addItem(e);
						buttonMessageSelect.setItemCaption(e, esfname.toString());
					}
				}
				buttonMessageSelect.setValue( selectedValues ); // select all
			}
		}
		
		if ( documentStyleSelect != null ) {
			documentStyleSelect.removeAllItems();
			if ( documentStyleElements != null ) {
				HashSet<Element> selectedValues = new HashSet<Element>();
				ListIterator<Element> iter = documentStyleElements.listIterator();
				while( iter.hasNext() ) {
					Element e = iter.next();
					EsfName esfname = EsfName.createFromToXml(e.getChildText("esfname",ns));
					if ( esfname.isValid() ) {
						selectedValues.add(e);
						documentStyleSelect.addItem(e);
						documentStyleSelect.setItemCaption(e, esfname.toString());
					}
				}
				documentStyleSelect.setValue( selectedValues ); // select all
			}
		}
		
		if ( dropDownSelect != null ) {
			dropDownSelect.removeAllItems();
			if ( dropDownElements != null ) {
				HashSet<Element> selectedValues = new HashSet<Element>();
				ListIterator<Element> iter = dropDownElements.listIterator();
				while( iter.hasNext() ) {
					Element e = iter.next();
					EsfName esfname = EsfName.createFromToXml(e.getChildText("esfname",ns));
					if ( esfname.isValid() ) {
						selectedValues.add(e);
			    		dropDownSelect.addItem(e);
			    		dropDownSelect.setItemCaption(e, esfname.toString());
					}
				}
				dropDownSelect.setValue( selectedValues ); // select all
			}
		}

		if ( emailSelect != null ) {
			emailSelect.removeAllItems();
			if ( emailElements != null ) {
				HashSet<Element> selectedValues = new HashSet<Element>();
				ListIterator<Element> iter = emailElements.listIterator();
				while( iter.hasNext() ) {
					Element e = iter.next();
					EsfName esfname = EsfName.createFromToXml(e.getChildText("esfname",ns));
					if ( esfname.isValid() ) {
						selectedValues.add(e);
						emailSelect.addItem(e);
						emailSelect.setItemCaption(e, esfname.toString());
					}
				}
				emailSelect.setValue( selectedValues ); // select all
			}
		}

		if ( fileSelect != null ) {
			fileSelect.removeAllItems();
			if ( fileElements != null ) {
				HashSet<Element> selectedValues = new HashSet<Element>();
				ListIterator<Element> iter = fileElements.listIterator();
				while( iter.hasNext() ) {
					Element e = iter.next();
					EsfName esfname = EsfName.createFromToXml(e.getChildText("esfname",ns));
					if ( esfname.isValid() ) {
						selectedValues.add(e);
						fileSelect.addItem(e);
						fileSelect.setItemCaption(e, esfname.toString());
					}
				}
				fileSelect.setValue( selectedValues ); // select all
			}
		}

		if ( imageSelect != null ) {
			imageSelect.removeAllItems();
			if ( imageElements != null ) {
				HashSet<Element> selectedValues = new HashSet<Element>();
				ListIterator<Element> iter = imageElements.listIterator();
				while( iter.hasNext() ) {
					Element e = iter.next();
					EsfName esfname = EsfName.createFromToXml(e.getChildText("esfname",ns));
					if ( esfname.isValid() ) {
						selectedValues.add(e);
						imageSelect.addItem(e);
						imageSelect.setItemCaption(e, esfname.toString());
					}
				}
				imageSelect.setValue( selectedValues ); // select all
			}
		}

		if ( propertySetSelect != null ) {
			propertySetSelect.removeAllItems();
			if ( propertySetElements != null ) {
				HashSet<Element> selectedValues = new HashSet<Element>();
				ListIterator<Element> iter = propertySetElements.listIterator();
				while( iter.hasNext() ) {
					Element e = iter.next();
					EsfName esfname = EsfName.createFromToXml(e.getChildText("esfname",ns));
					if ( esfname.isValid() ) {
						selectedValues.add(e);
						propertySetSelect.addItem(e);
						propertySetSelect.setItemCaption(e, esfname.toString());
					}
				}
				propertySetSelect.setValue( selectedValues ); // select all
			}
		}
	}
	
	Errors processImportedXml(String xmlData) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Errors errors = new Errors();
		
		StringReader sr = null;

		SAXBuilder saxBuilder = new SAXBuilder(XMLReaders.NONVALIDATING);
		saxBuilder.setIgnoringElementContentWhitespace(true);

		try {
			sr = new StringReader(xmlData);

			org.jdom2.Document doc = saxBuilder.build(sr);
			Element rootElement = doc.getRootElement();
			
			loadXmlFromJdom(rootElement, xmlData);
			
			buildImportLayout();
		} catch (EsfException e) {
			errors.addError(e.getMessage());
		} catch (java.io.IOException e) {
			_logger.error("processImportedXml() - could not read data from the XML string: " + xmlData,e);
			errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.ioexception"));
		} catch (JDOMException e) {
			_logger.error("processImportedXml() - could not XML parse data from the XML string: " + xmlData, e);
			errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.jdomexception"));
		} finally {
			if (sr != null) {
				try { sr.close(); } catch (Exception e) {}
			}
		}
		
		return errors;
	}
	
	protected void loadXmlFromJdom(Element rootElement, String dataXml) throws EsfException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		String rootName = rootElement.getName();
		if (!rootName.equals("LibraryExport")) {
			_logger.error("loadXmlFromJdom(): Root element is not LibraryExport.  Found instead: " + rootName + "; in XML: "
					+ dataXml);
			throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.invalidRoot"));
		}

		deployId = new EsfUUID(rootElement.getChildText("deployId", ns));
		if (deployId.isNull()) {
			_logger.error("loadXmlFromJdom(): required deployId element is missing in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.missingElement","deployId"));
		}

		release = rootElement.getChildText("release", ns);
		if (EsfString.isBlank(release)) {
			_logger.error("loadXmlFromJdom(): required release element is missing in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.missingElement","release"));
		}

		versionMajor = vaadinUi.getEsfapp().stringToInt(rootElement.getChildText("versionMajor", ns),0);
		if (versionMajor < 1) {
			_logger.error("loadXmlFromJdom(): required versionMajor element is missing in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.missingElement","versionMajor"));
		}
		versionMinor = vaadinUi.getEsfapp().stringToInt(rootElement.getChildText("versionMinor", ns),0);
		if (versionMinor < 1) {
			_logger.error("loadXmlFromJdom(): required versionMinor element is missing in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.missingElement","versionMinor"));
		}
		if ( versionMajor > Version.getMajor() || (versionMajor == Version.getMajor() && versionMinor > Version.getMinor()) ) {
			_logger.error("loadXmlFromJdom(): versionMajor " + versionMajor + "and versionMinor " + versionMinor + " is greater than our own version: " + Version.getMajor() + "." + Version.getMinor() + "; in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.majorVersionMismatch",versionMajor,versionMinor,Version.getMajor(),Version.getMinor()));
		}

		libraryPathName = new EsfPathName(rootElement.getChildText("libraryPathName", ns));
		if (! libraryPathName.isValid()) {
			_logger.error("loadXmlFromJdom(): required libraryPathName element is missing in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.missingElement","libraryPathName"));
		}
		
		timestamp =  EsfDateTime.createFromToXml(rootElement.getChildText("timestamp", ns));
		if ( timestamp.isNull() ) {
			_logger.error("loadXmlFromJdom(): required timestamp element is missing in XML: " + dataXml);
			throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.missingElement","timestamp"));
		}
		
		boolean hasSomethingToImport = false;
		
		documentElements = new LinkedList<Element>(rootElement.getChildren("Document", ns));
		documentVersionElements = new LinkedList<Element>(rootElement.getChildren("DocumentVersion", ns));
		if ( documentElements != null || documentVersionElements != null ) {
			int count = documentElements == null ? 0 : documentElements.size();
			int countVersion = documentVersionElements == null ? 0 : documentVersionElements.size();
			if ( 
				(documentElements == null && documentVersionElements != null)  ||
				(documentElements != null && documentVersionElements == null)  ||
				(documentElements != null && documentVersionElements != null && documentElements.size() != documentVersionElements.size())
			   ) {
				_logger.error("loadXmlFromJdom(): Mismatched number of Document/DocumentVersion elements in XML: " + dataXml);
				throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.objectCountsMismatch",count,"Document",countVersion,"DocumentVersion"));
			}
			hasSomethingToImport = hasSomethingToImport || count > 0;
		}
		
		buttonMessageElements = new LinkedList<Element>(rootElement.getChildren("ButtonMessage", ns2012));
		buttonMessageVersionElements = new LinkedList<Element>(rootElement.getChildren("ButtonMessageVersion", ns2012));
		if ( buttonMessageElements != null || buttonMessageVersionElements != null ) {
			int count = buttonMessageElements == null ? 0 : buttonMessageElements.size();
			int countVersion = buttonMessageVersionElements == null ? 0 : buttonMessageVersionElements.size();
			if ( 
				(buttonMessageElements == null && buttonMessageVersionElements != null)  ||
				(buttonMessageElements != null && buttonMessageVersionElements == null)  ||
				(buttonMessageElements != null && buttonMessageVersionElements != null && buttonMessageElements.size() != buttonMessageVersionElements.size())
			   ) {
				_logger.error("loadXmlFromJdom(): Mismatched number of ButtonMessage/ButtonMessageVersion elements in XML: " + dataXml);
				throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.objectCountsMismatch",count,"ButtonMessage",countVersion,"ButtonMessageVersion"));
			}
			hasSomethingToImport = hasSomethingToImport || count > 0;
		}
		
		documentStyleElements = new LinkedList<Element>(rootElement.getChildren("DocumentStyle", ns));
		documentStyleVersionElements = new LinkedList<Element>(rootElement.getChildren("DocumentStyleVersion", ns));
		if ( documentStyleElements != null || documentStyleVersionElements != null ) {
			int count = documentStyleElements == null ? 0 : documentStyleElements.size();
			int countVersion = documentStyleVersionElements == null ? 0 : documentStyleVersionElements.size();
			if ( 
				(documentStyleElements == null && documentStyleVersionElements != null)  ||
				(documentStyleElements != null && documentStyleVersionElements == null)  ||
				(documentStyleElements != null && documentStyleVersionElements != null && documentStyleElements.size() != documentStyleVersionElements.size())
			   ) {
				_logger.error("loadXmlFromJdom(): Mismatched number of DocumentStyle/DocumentStyleVersion elements in XML: " + dataXml);
				throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.objectCountsMismatch",count,"DocumentStyle",countVersion,"DocumentStyleVersion"));
			}
			hasSomethingToImport = hasSomethingToImport || count > 0;
		}
		
		dropDownElements = new LinkedList<Element>(rootElement.getChildren("DropDown", ns));
		dropDownVersionElements = new LinkedList<Element>(rootElement.getChildren("DropDownVersion", ns));
		if ( dropDownElements != null || dropDownVersionElements != null ) {
			int count = dropDownElements == null ? 0 : dropDownElements.size();
			int countVersion = dropDownVersionElements == null ? 0 : dropDownVersionElements.size();
			if ( 
				(dropDownElements == null && dropDownVersionElements != null)  ||
				(dropDownElements != null && dropDownVersionElements == null)  ||
				(dropDownElements != null && dropDownVersionElements != null && dropDownElements.size() != dropDownVersionElements.size())
			   ) {
				_logger.error("loadXmlFromJdom(): Mismatched number of DropDown/DropDownVersion elements in XML: " + dataXml);
				throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.objectCountsMismatch",count,"DropDown",countVersion,"DropDownVersion"));
			}
			hasSomethingToImport = hasSomethingToImport || count > 0;
		}
		
		emailElements = new LinkedList<Element>(rootElement.getChildren("EmailTemplate", ns));
		emailVersionElements = new LinkedList<Element>(rootElement.getChildren("EmailTemplateVersion", ns));
		if ( emailElements != null || emailVersionElements != null ) {
			int count = emailElements == null ? 0 : emailElements.size();
			int countVersion = emailVersionElements == null ? 0 : emailVersionElements.size();
			if ( 
				(emailElements == null && emailVersionElements != null)  ||
				(emailElements != null && emailVersionElements == null)  ||
				(emailElements != null && emailVersionElements != null && emailElements.size() != emailVersionElements.size())
			   ) {
				_logger.error("loadXmlFromJdom(): Mismatched number of EmailTemplate/EmailTemplateVersion elements in XML: " + dataXml);
				throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.objectCountsMismatch",count,"EmailTemplate",countVersion,"EmailTemplateVersion"));
			}
			hasSomethingToImport = hasSomethingToImport || count > 0;
		}
		
		fileElements = new LinkedList<Element>(rootElement.getChildren("File", ns));
		fileVersionElements = new LinkedList<Element>(rootElement.getChildren("FileVersion", ns));
		if ( fileElements != null || fileVersionElements != null ) {
			int count = fileElements == null ? 0 : fileElements.size();
			int countVersion = fileVersionElements == null ? 0 : fileVersionElements.size();
			if ( 
				(fileElements == null && fileVersionElements != null)  ||
				(fileElements != null && fileVersionElements == null)  ||
				(fileElements != null && fileVersionElements != null && fileElements.size() != fileVersionElements.size())
			   ) {
				_logger.error("loadXmlFromJdom(): Mismatched number of File/FileVersion elements in XML: " + dataXml);
				throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.objectCountsMismatch",count,"File",countVersion,"FileVersion"));
			}
			hasSomethingToImport = hasSomethingToImport || count > 0;
		}
		
		imageElements = new LinkedList<Element>(rootElement.getChildren("Image", ns));
		imageVersionElements = new LinkedList<Element>(rootElement.getChildren("ImageVersion", ns));
		if ( imageElements != null || imageVersionElements != null ) {
			int count = imageElements == null ? 0 : imageElements.size();
			int countVersion = imageVersionElements == null ? 0 : imageVersionElements.size();
			if ( 
				(imageElements == null && imageVersionElements != null)  ||
				(imageElements != null && imageVersionElements == null)  ||
				(imageElements != null && imageVersionElements != null && imageElements.size() != imageVersionElements.size())
			   ) {
				_logger.error("loadXmlFromJdom(): Mismatched number of Image/ImageVersion elements in XML: " + dataXml);
				throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.objectCountsMismatch",count,"Image",countVersion,"ImageVersion"));
			}
			hasSomethingToImport = hasSomethingToImport || count > 0;
		}
		
		propertySetElements = new LinkedList<Element>(rootElement.getChildren("PropertySet", ns));
		propertySetVersionElements = new LinkedList<Element>(rootElement.getChildren("PropertySetVersion", ns));
		if ( propertySetElements != null || propertySetVersionElements != null ) {
			int count = propertySetElements == null ? 0 : propertySetElements.size();
			int countVersion = propertySetVersionElements == null ? 0 : propertySetVersionElements.size();
			if ( 
				(propertySetElements == null && propertySetVersionElements != null)  ||
				(propertySetElements != null && propertySetVersionElements == null)  ||
				(propertySetElements != null && propertySetVersionElements != null && propertySetElements.size() != propertySetVersionElements.size())
			   ) {
				_logger.error("loadXmlFromJdom(): Mismatched number of PropertySet/PropertySetVersion elements in XML: " + dataXml);
				throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.objectCountsMismatch",count,"PropertySet",countVersion,"PropertySetVersion"));
			}
			hasSomethingToImport = hasSomethingToImport || count > 0;
		}
		
		if ( ! hasSomethingToImport ) {
			throw new EsfException(vaadinUi.getMsg("LibraryForm.ImportView.xmlError.nothingtodo"));
		}
	}

	void doImport(Errors errors) {
		Boolean doOverwriteTestVersion = overwriteTestVersion.getValue();
		importButtonMessages(doOverwriteTestVersion,errors);
		importDocumentStyles(doOverwriteTestVersion,errors);
		importDropDowns(doOverwriteTestVersion,errors);
		importEmails(doOverwriteTestVersion,errors);
		importFiles(doOverwriteTestVersion,errors);
		importImages(doOverwriteTestVersion,errors);
		importPropertySets(doOverwriteTestVersion,errors);
		importDocuments(doOverwriteTestVersion,errors); // we do this last since it may reference imported document styles, for example
	}
	
	void importDocuments(boolean doOverwriteTestVersion,Errors errors) {
		if ( documentSelect == null ) return;
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
    		@SuppressWarnings("unchecked")
    		Set<Element> elementSet = (Set<Element>)documentSelect.getValue();
    		
    		// Import each of the selected objects
    		for( Element e : elementSet ) {
    			EsfUUID id = new EsfUUID(e.getChildText("id", ns));
    			if ( id.isNull() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"id"));
    				continue;
    			}
    			EsfName esfname = new EsfName(e.getChildText("esfname", ns));
    			if ( ! esfname.isValid() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"esfname"));
    				continue;
    			}
    			
    			// Check if we know this object already
    			Document document = Document.Manager.getByName(library, esfname);
    			
				// Find the imported version object that goes with this by matching on the embedded ids
				Element ev = null;
				ListIterator<Element> iter = documentVersionElements.listIterator();
				while( iter.hasNext() ) {
					Element checkEv = iter.next();
					EsfUUID evParentId = new EsfUUID(checkEv.getChildText("documentId", ns));
					if ( evParentId.equals(id) ) {
						ev = checkEv;
						break;
					}
				}
				if ( ev == null ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingVersionElement",e.getName(),esfname.toString()));
					continue;
				} 
				
				// If we have the document already, and it has a test version already, but we're not allowed to overwrite it, we'll have to skip this import
				if ( document != null && document.hasTestVersion() && ! doOverwriteTestVersion ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.hasTestVersionButCannotOverwrite",e.getName(),esfname.toString()));
					continue;
				}
				
				// If we don't know this document, let's create one.
				// Otherwise, if the existing one has a test version, let's delete that test version since we'll be using our imported instead.
				if ( document == null )
					document = Document.Manager.createFromJDOM(library.getId(), e);
				else if ( document.hasTestVersion() ) {
					DocumentVersion prevTestVersion = document.getTestDocumentVersion();
					if ( ! prevTestVersion.delete(con, errors, vaadinUi.getUser()) )
						continue;
				} else {
					document.bumpTestVersion();
				}
				
				DocumentVersion documentVersion = DocumentVersion.Manager.createFromJDOM(document, ev);
				
				if ( document.save(con, vaadinUi.getUser()) && documentVersion.save(con, vaadinUi.getUser()) )
				{
					errors.addSuccess(vaadinUi.getMsg("LibraryForm.ImportView.importOkay.message",e.getName(),esfname.toString()));

					documentElements.remove(e); // don't want to process this object again.
					documentVersionElements.remove(ev);
				} 
				else
				{
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.saveFailed",e.getName(),esfname.toString()));
				}
    		}
            con.commit();
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"importDocuments()");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            vaadinUi.getEsfapp().cleanupPool(pool,con,null);
        }
	}
	
	void importButtonMessages(boolean doOverwriteTestVersion,Errors errors) {
		if ( buttonMessageSelect == null ) return;
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
    		@SuppressWarnings("unchecked")
    		Set<Element> elementSet = (Set<Element>)buttonMessageSelect.getValue();
    		
    		// Import each of the selected objects
    		for( Element e : elementSet ) {
    			EsfUUID id = new EsfUUID(e.getChildText("id", ns2012));
    			if ( id.isNull() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"id"));
    				continue;
    			}
    			EsfName esfname = new EsfName(e.getChildText("esfname", ns2012));
    			if ( ! esfname.isValid() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"esfname"));
    				continue;
    			}
    			
    			// Check if we know this object already
    			ButtonMessage buttonMessage = ButtonMessage.Manager.getByName(library, esfname);
    			
				// Find the imported version object that goes with this by matching on the embedded ids
				Element ev = null;
				ListIterator<Element> iter = buttonMessageVersionElements.listIterator();
				while( iter.hasNext() ) {
					Element checkEv = iter.next();
					EsfUUID evParentId = new EsfUUID(checkEv.getChildText("buttonMessageId", ns2012));
					if ( evParentId.equals(id) ) {
						ev = checkEv;
						break;
					}
				}
				if ( ev == null ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingVersionElement",e.getName(),esfname.toString()));
					continue;
				} 
				
				// If we have the drop down already, and it has a test version already, but we're not allowed to overwrite it, we'll have to skip this import
				if ( buttonMessage != null && buttonMessage.hasTestVersion() && ! doOverwriteTestVersion ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.hasTestVersionButCannotOverwrite",e.getName(),esfname.toString()));
					continue;
				}
				
				// If we don't know this drop down, let's create one.
				// Otherwise, if the existing one has a test version, let's delete that test version since we'll be using our imported instead.
				if ( buttonMessage == null )
					buttonMessage = ButtonMessage.Manager.createFromJDOM(library.getId(), e);
				else if ( buttonMessage.hasTestVersion() ) {
					ButtonMessageVersion prevTestVersion = buttonMessage.getTestButtonMessageVersion();
					if ( ! prevTestVersion.delete(con, errors, vaadinUi.getUser()) )
						continue;
				} else {
					buttonMessage.bumpTestVersion();
				}
				
				ButtonMessageVersion buttonMessageVersion = ButtonMessageVersion.Manager.createFromJDOM(buttonMessage, ev);
				
				if ( buttonMessage.save(con, vaadinUi.getUser()) && buttonMessageVersion.save(con, vaadinUi.getUser()) )
				{
					errors.addSuccess(vaadinUi.getMsg("LibraryForm.ImportView.importOkay.message",e.getName(),esfname.toString()));

					buttonMessageElements.remove(e); // don't want to process this object again.
					buttonMessageVersionElements.remove(ev);
				} 
				else
				{
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.saveFailed",e.getName(),esfname.toString()));
				}
    		}
            con.commit();
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"importButtonMessages()");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            vaadinUi.getEsfapp().cleanupPool(pool,con,null);
        }
	}
	
	void importDocumentStyles(boolean doOverwriteTestVersion,Errors errors) {
		if ( documentStyleSelect == null ) return;
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
    		@SuppressWarnings("unchecked")
    		Set<Element> elementSet = (Set<Element>)documentStyleSelect.getValue();
    		
    		// Import each of the selected objects
    		for( Element e : elementSet ) {
    			EsfUUID id = new EsfUUID(e.getChildText("id", ns));
    			if ( id.isNull() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"id"));
    				continue;
    			}
    			EsfName esfname = new EsfName(e.getChildText("esfname", ns));
    			if ( ! esfname.isValid() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"esfname"));
    				continue;
    			}
    			
    			// Check if we know this object already
    			DocumentStyle documentStyle = DocumentStyle.Manager.getByName(library, esfname);
    			
				// Find the imported version object that goes with this by matching on the embedded ids
				Element ev = null;
				ListIterator<Element> iter = documentStyleVersionElements.listIterator();
				while( iter.hasNext() ) {
					Element checkEv = iter.next();
					EsfUUID evParentId = new EsfUUID(checkEv.getChildText("documentStyleId", ns));
					if ( evParentId.equals(id) ) {
						ev = checkEv;
						break;
					}
				}
				if ( ev == null ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingVersionElement",e.getName(),esfname.toString()));
					continue;
				} 
				
				// If we have the drop down already, and it has a test version already, but we're not allowed to overwrite it, we'll have to skip this import
				if ( documentStyle != null && documentStyle.hasTestVersion() && ! doOverwriteTestVersion ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.hasTestVersionButCannotOverwrite",e.getName(),esfname.toString()));
					continue;
				}
				
				// If we don't know this drop down, let's create one.
				// Otherwise, if the existing one has a test version, let's delete that test version since we'll be using our imported instead.
				if ( documentStyle == null )
					documentStyle = DocumentStyle.Manager.createFromJDOM(library.getId(), e);
				else if ( documentStyle.hasTestVersion() ) {
					DocumentStyleVersion prevTestVersion = documentStyle.getTestDocumentStyleVersion();
					if ( ! prevTestVersion.delete(con, errors, vaadinUi.getUser()) )
						continue;
				} else {
					documentStyle.bumpTestVersion();
				}
				
				DocumentStyleVersion documentStyleVersion = DocumentStyleVersion.Manager.createFromJDOM(documentStyle, ev);
				
				if ( documentStyle.save(con, vaadinUi.getUser()) && documentStyleVersion.save(con, vaadinUi.getUser()) )
				{
					errors.addSuccess(vaadinUi.getMsg("LibraryForm.ImportView.importOkay.message",e.getName(),esfname.toString()));

					documentStyleElements.remove(e); // don't want to process this object again.
					documentStyleVersionElements.remove(ev);
				} 
				else
				{
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.saveFailed",e.getName(),esfname.toString()));
				}
    		}
            con.commit();
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"importDocumentStyles()");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            vaadinUi.getEsfapp().cleanupPool(pool,con,null);
        }
	}
	
	void importDropDowns(boolean doOverwriteTestVersion,Errors errors) {
		if ( dropDownSelect == null ) return;
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
    		@SuppressWarnings("unchecked")
    		Set<Element> elementSet = (Set<Element>)dropDownSelect.getValue();
    		
    		// Import each of the selected objects
    		for( Element e : elementSet ) {
    			EsfUUID id = new EsfUUID(e.getChildText("id", ns));
    			if ( id.isNull() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"id"));
    				continue;
    			}
    			EsfName esfname = new EsfName(e.getChildText("esfname", ns));
    			if ( ! esfname.isValid() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"esfname"));
    				continue;
    			}
    			
    			// Check if we know this object already
    			DropDown dropDown = DropDown.Manager.getByName(library.getId(), esfname);
    			
				// Find the imported version object that goes with this by matching on the embedded ids
				Element ev = null;
				ListIterator<Element> iter = dropDownVersionElements.listIterator();
				while( iter.hasNext() ) {
					Element checkEv = iter.next();
					EsfUUID evParentId = new EsfUUID(checkEv.getChildText("dropDownId", ns));
					if ( evParentId.equals(id) ) {
						ev = checkEv;
						break;
					}
				}
				if ( ev == null ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingVersionElement",e.getName(),esfname.toString()));
					continue;
				} 
				
				// If we have the drop down already, and it has a test version already, but we're not allowed to overwrite it, we'll have to skip this import
				if ( dropDown != null && dropDown.hasTestVersion() && ! doOverwriteTestVersion ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.hasTestVersionButCannotOverwrite",e.getName(),esfname.toString()));
					continue;
				}
				
				// If we don't know this drop down, let's create one.
				// Otherwise, if the existing one has a test version, let's delete that test version since we'll be using our imported instead.
				if ( dropDown == null )
					dropDown = DropDown.Manager.createFromJDOM(library.getId(), e);
				else if ( dropDown.hasTestVersion() ) {
					DropDownVersion prevTestVersion = dropDown.getTestDropDownVersion();
					if ( ! prevTestVersion.delete(con, errors, vaadinUi.getUser()) )
						continue;
				} else {
					dropDown.bumpTestVersion();
				}
				
				DropDownVersion dropDownVersion = DropDownVersion.Manager.createFromJDOM(dropDown, ev);
				
				if ( dropDown.save(con, vaadinUi.getUser()) && dropDownVersion.save(con, vaadinUi.getUser()) )
				{
					errors.addSuccess(vaadinUi.getMsg("LibraryForm.ImportView.importOkay.message",e.getName(),esfname.toString()));

					dropDownElements.remove(e); // don't want to process this object again.
					dropDownVersionElements.remove(ev);
				} 
				else
				{
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.saveFailed",e.getName(),esfname.toString()));
				}
    		}
            con.commit();
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"importDropDowns()");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            vaadinUi.getEsfapp().cleanupPool(pool,con,null);
        }
	}
	
	void importEmails(boolean doOverwriteTestVersion,Errors errors) {
		if ( emailSelect == null ) return;
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
    		@SuppressWarnings("unchecked")
    		Set<Element> elementSet = (Set<Element>)emailSelect.getValue();
    		
    		// Import each of the selected objects
    		for( Element e : elementSet ) {
    			EsfUUID id = new EsfUUID(e.getChildText("id", ns));
    			if ( id.isNull() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"id"));
    				continue;
    			}
    			EsfName esfname = new EsfName(e.getChildText("esfname", ns));
    			if ( ! esfname.isValid() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"esfname"));
    				continue;
    			}
    			
    			// Check if we know this object already
    			EmailTemplate email = EmailTemplate.Manager.getByName(library.getId(), esfname);
    			
				// Find the imported version object that goes with this by matching on the embedded ids
				Element ev = null;
				ListIterator<Element> iter = emailVersionElements.listIterator();
				while( iter.hasNext() ) {
					Element checkEv = iter.next();
					EsfUUID evParentId = new EsfUUID(checkEv.getChildText("emailTemplateId", ns));
					if ( evParentId.equals(id) ) {
						ev = checkEv;
						break;
					}
				}
				if ( ev == null ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingVersionElement",e.getName(),esfname.toString()));
					continue;
				} 
				
				// If we have the drop down already, and it has a test version already, but we're not allowed to overwrite it, we'll have to skip this import
				if ( email != null && email.hasTestVersion() && ! doOverwriteTestVersion ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.hasTestVersionButCannotOverwrite",e.getName(),esfname.toString()));
					continue;
				}
				
				// If we don't know this drop down, let's create one.
				// Otherwise, if the existing one has a test version, let's delete that test version since we'll be using our imported instead.
				if ( email == null )
					email = EmailTemplate.Manager.createFromJDOM(library.getId(), e);
				else if ( email.hasTestVersion() ) {
					EmailTemplateVersion prevTestVersion = email.getTestEmailTemplateVersion();
					if ( ! prevTestVersion.delete(con, errors, vaadinUi.getUser()) )
						continue;
				} else {
					email.bumpTestVersion();
				}
				
				EmailTemplateVersion emailVersion = EmailTemplateVersion.Manager.createFromJDOM(email, ev);
				
				if ( email.save(con, vaadinUi.getUser()) && emailVersion.save(con, vaadinUi.getUser()) )
				{
					errors.addSuccess(vaadinUi.getMsg("LibraryForm.ImportView.importOkay.message",e.getName(),esfname.toString()));

					emailElements.remove(e); // don't want to process this object again.
					emailVersionElements.remove(ev);
				} 
				else
				{
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.saveFailed",e.getName(),esfname.toString()));
				}
    		}
            con.commit();
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"importEmails()");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            vaadinUi.getEsfapp().cleanupPool(pool,con,null);
        }
	}
	
	void importFiles(boolean doOverwriteTestVersion,Errors errors) {
		if ( fileSelect == null ) return;
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
    		@SuppressWarnings("unchecked")
    		Set<Element> elementSet = (Set<Element>)fileSelect.getValue();
    		
    		// Import each of the selected objects
    		for( Element e : elementSet ) {
    			EsfUUID id = new EsfUUID(e.getChildText("id", ns));
    			if ( id.isNull() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"id"));
    				continue;
    			}
    			EsfName esfname = new EsfName(e.getChildText("esfname", ns));
    			if ( ! esfname.isValid() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"esfname"));
    				continue;
    			}
    			
    			// Check if we know this object already
    			File file = File.Manager.getByName(library.getId(), esfname);
    			
				// Find the imported version object that goes with this by matching on the embedded ids
				Element ev = null;
				ListIterator<Element> iter = fileVersionElements.listIterator();
				while( iter.hasNext() ) {
					Element checkEv = iter.next();
					EsfUUID evParentId = new EsfUUID(checkEv.getChildText("fileId", ns));
					if ( evParentId.equals(id) ) {
						ev = checkEv;
						break;
					}
				}
				if ( ev == null ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingVersionElement",e.getName(),esfname.toString()));
					continue;
				} 
				
				// If we have the object already, and it has a test version already, but we're not allowed to overwrite it, we'll have to skip this import
				if ( file != null && file.hasTestVersion() && ! doOverwriteTestVersion ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.hasTestVersionButCannotOverwrite",e.getName(),esfname.toString()));
					continue;
				}
				
				// If we don't know this object, let's create one.
				// Otherwise, if the existing one has a test version, let's delete that test version since we'll be using our imported instead.
				if ( file == null )
					file = File.Manager.createFromJDOM(library.getId(), e);
				else if ( file.hasTestVersion() ) {
					FileVersion prevTestVersion = file.getTestFileVersion();
					if ( ! prevTestVersion.delete(con, errors, vaadinUi.getUser()) )
						continue;
				} else {
					file.bumpTestVersion();
				}
				
				FileVersion fileVersion = FileVersion.Manager.createFromJDOM(file, ev);
				
				if ( file.save(con, vaadinUi.getUser()) && fileVersion.save(con, vaadinUi.getUser()) )
				{
					errors.addSuccess(vaadinUi.getMsg("LibraryForm.ImportView.importOkay.message",e.getName(),esfname.toString()));

					fileElements.remove(e); // don't want to process this object again.
					fileVersionElements.remove(ev);
				} 
				else
				{
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.saveFailed",e.getName(),esfname.toString()));
				}
    		}
            con.commit();
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"importFiles()");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            vaadinUi.getEsfapp().cleanupPool(pool,con,null);
        }
	}
	
	
	void importImages(boolean doOverwriteTestVersion,Errors errors) {
		if ( imageSelect == null ) return;
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
    		@SuppressWarnings("unchecked")
    		Set<Element> elementSet = (Set<Element>)imageSelect.getValue();
    		
    		// Import each of the selected objects
    		for( Element e : elementSet ) {
    			EsfUUID id = new EsfUUID(e.getChildText("id", ns));
    			if ( id.isNull() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"id"));
    				continue;
    			}
    			EsfName esfname = new EsfName(e.getChildText("esfname", ns));
    			if ( ! esfname.isValid() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"esfname"));
    				continue;
    			}
    			
    			// Check if we know this object already
    			Image image = Image.Manager.getByName(library.getId(), esfname);
    			
				// Find the imported version object that goes with this by matching on the embedded ids
				Element ev = null;
				ListIterator<Element> iter = imageVersionElements.listIterator();
				while( iter.hasNext() ) {
					Element checkEv = iter.next();
					EsfUUID evParentId = new EsfUUID(checkEv.getChildText("imageId", ns));
					if ( evParentId.equals(id) ) {
						ev = checkEv;
						break;
					}
				}
				if ( ev == null ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingVersionElement",e.getName(),esfname.toString()));
					continue;
				} 
				
				// If we have the object already, and it has a test version already, but we're not allowed to overwrite it, we'll have to skip this import
				if ( image != null && image.hasTestVersion() && ! doOverwriteTestVersion ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.hasTestVersionButCannotOverwrite",e.getName(),esfname.toString()));
					continue;
				}
				
				// If we don't know this object, let's create one.
				// Otherwise, if the existing one has a test version, let's delete that test version since we'll be using our imported instead.
				if ( image == null )
					image = Image.Manager.createFromJDOM(library.getId(), e);
				else if ( image.hasTestVersion() ) {
					ImageVersion prevTestVersion = image.getTestImageVersion();
					if ( ! prevTestVersion.delete(con, errors, vaadinUi.getUser()) )
						continue;
				} else {
					image.bumpTestVersion();
				}
				
				ImageVersion imageVersion = ImageVersion.Manager.createFromJDOM(null, image, ev);
				
				if ( image.save(con, vaadinUi.getUser()) && imageVersion.save(con, vaadinUi.getUser()) )
				{
					errors.addSuccess(vaadinUi.getMsg("LibraryForm.ImportView.importOkay.message",e.getName(),esfname.toString()));

					imageElements.remove(e); // don't want to process this object again.
					imageVersionElements.remove(ev);
				} 
				else
				{
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.saveFailed",e.getName(),esfname.toString()));
				}
    		}
            con.commit();
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"importImages()");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            vaadinUi.getEsfapp().cleanupPool(pool,con,null);
        }
	}
	
	void importPropertySets(boolean doOverwriteTestVersion,Errors errors) {
		if ( propertySetSelect == null ) return;
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
    		@SuppressWarnings("unchecked")
    		Set<Element> elementSet = (Set<Element>)propertySetSelect.getValue();
    		
    		// Import each of the selected objects
    		for( Element e : elementSet ) {
    			EsfUUID id = new EsfUUID(e.getChildText("id", ns));
    			if ( id.isNull() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"id"));
    				continue;
    			}
    			EsfName esfname = new EsfName(e.getChildText("esfname", ns));
    			if ( ! esfname.isValid() ) {
    				errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingElement",e.getName(),"esfname"));
    				continue;
    			}
    			
    			// Check if we know this object already
    			PropertySet propertySet = PropertySet.Manager.getByName(library.getId(), esfname);
    			
				// Find the imported version object that goes with this by matching on the embedded ids
				Element ev = null;
				ListIterator<Element> iter = propertySetVersionElements.listIterator();
				while( iter.hasNext() ) {
					Element checkEv = iter.next();
					EsfUUID evParentId = new EsfUUID(checkEv.getChildText("propertySetId", ns));
					if ( evParentId.equals(id) ) {
						ev = checkEv;
						break;
					}
				}
				if ( ev == null ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.missingVersionElement",e.getName(),esfname.toString()));
					continue;
				} 
				
				// If we have the drop down already, and it has a test version already, but we're not allowed to overwrite it, we'll have to skip this import
				if ( propertySet != null && propertySet.hasTestVersion() && ! doOverwriteTestVersion ) {
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.hasTestVersionButCannotOverwrite",e.getName(),esfname.toString()));
					continue;
				}
				
				// If we don't know this drop down, let's create one.
				// Otherwise, if the existing one has a test version, let's delete that test version since we'll be using our imported instead.
				if ( propertySet == null )
					propertySet = PropertySet.Manager.createFromJDOM(library.getId(), e);
				else if ( propertySet.hasTestVersion() ) {
					PropertySetVersion prevTestVersion = propertySet.getTestPropertySetVersion();
					if ( ! prevTestVersion.delete(con, errors, vaadinUi.getUser()) )
						continue;
				} else {
					propertySet.bumpTestVersion();
				}
				
				PropertySetVersion propertySetVersion = PropertySetVersion.Manager.createFromJDOM(propertySet, ev);
				
				if ( propertySet.save(con, vaadinUi.getUser()) && propertySetVersion.save(con, vaadinUi.getUser()) )
				{
					errors.addSuccess(vaadinUi.getMsg("LibraryForm.ImportView.importOkay.message",e.getName(),esfname.toString()));

					propertySetElements.remove(e); // don't want to process this object again.
					propertySetVersionElements.remove(ev);
				} 
				else
				{
					errors.addError(vaadinUi.getMsg("LibraryForm.ImportView.importError.saveFailed",e.getName(),esfname.toString()));
				}
    		}
            con.commit();
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"importPropertySets()");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            vaadinUi.getEsfapp().cleanupPool(pool,con,null);
        }
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return "";
	}
	
	@Override
	public void initView() {
    	buildUploadLayout();
	}

	@Override
	public boolean isDirty() {
		return false;
	}
	
}