// Copyright (C) 2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.UpdateApiCondition;

import java.io.Serializable;
import java.util.List;

import com.esignforms.open.runtime.condition.UpdateApiCondition.Spec;
import com.vaadin.data.util.BeanItemContainer;


public class ConditionContainer extends BeanItemContainer<Spec> implements Serializable {
	private static final long serialVersionUID = -7903725351424804952L;

	public ConditionContainer(List<Spec> specList) throws InstantiationException, IllegalAccessException {
		super(Spec.class);
		refresh(specList);
	}
	
	public void refresh(List<Spec> specList) {
		removeAllItems();
		for( Spec spec : specList ) {
			addItem(spec);
		}
	}
}