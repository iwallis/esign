// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.documentstyle;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DocumentStyle;
import com.esignforms.open.prog.DocumentStyleInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibDocumentStyleBean implements Serializable, Comparable<LibDocumentStyleBean> {
	private static final long serialVersionUID = -8297333111766581528L;

	private DocumentStyleInfo documentstyleInfo;

	private DocumentStyle documentstyle;
	
	public LibDocumentStyleBean(DocumentStyleInfo documentstyleInfo) {
		this.documentstyleInfo = documentstyleInfo;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibDocumentStyleBean )
			return documentstyleInfo.equals(((LibDocumentStyleBean)o).documentstyleInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return documentstyleInfo.hashCode();
    }
	public int compareTo(LibDocumentStyleBean d) {
		return documentstyleInfo.compareTo(d.documentstyleInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public DocumentStyleInfo documentstyleInfo() {
		return documentstyleInfo;
	}

	public DocumentStyle documentstyle() {
		if ( documentstyle == null )
			documentstyle = DocumentStyle.Manager.getById(documentstyleInfo.getId());
		return documentstyle;
	}

	public LibDocumentStyleBean createLike() {
		DocumentStyleInfo newDocumentStyleInfo = DocumentStyleInfo.Manager.createLike(documentstyle, documentstyle.getEsfName());	    
	    return new LibDocumentStyleBean(newDocumentStyleInfo);
	}
	
	
	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( documentstyle().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			documentstyleInfo = DocumentStyleInfo.Manager.createFromSource(documentstyle);
			return true;
		} 
		
		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}

	
	// Now for the JavaBeans methods
	public EsfName getEsfName() {
		return documentstyleInfo.getEsfName();
	}
	public void setEsfName(EsfName v) {
		documentstyle.setEsfName(v);
	}

	public EsfUUID getId() {
		return documentstyleInfo.getId();
	}
	
	public String getDescription() {
		return documentstyleInfo.getDescription();
	}
	public void setDescription(String v) {
		documentstyle().setDescription(v);
	}

	public boolean isEnabled() {
		return documentstyleInfo.isEnabled();
	}
	public String getStatus() {
		return documentstyleInfo.getStatus();
	}
	public void setStatus(String v) {
		documentstyle().setStatus(v);
	}	
	
	public Integer getProductionVersion() {
		return documentstyleInfo.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		return documentstyleInfo.getTestVersion();
	}
	
	public void bumpTestVersion() {
		documentstyle().bumpTestVersion();
	}
	public void dropTestVersion() {
		documentstyle().dropTestVersion();
	}
	
	public String getComments() {
		return documentstyle().getComments();
	}
	public void setComments(String v) {
		documentstyle().setComments(v);
	}
}