// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.emailtemplate;

import java.io.Serializable;
import java.util.Collection;

import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.EmailTemplateVersionInfo;
import com.vaadin.data.util.BeanItemContainer;


public class LibEmailTemplateVersionBeanContainer extends BeanItemContainer<LibEmailTemplateVersionBean> implements Serializable {
	private static final long serialVersionUID = 6301825643221370661L;

	final LibEmailTemplateAndVersionsMainView view;
	
	public LibEmailTemplateVersionBeanContainer(LibEmailTemplateAndVersionsMainView view) throws InstantiationException, IllegalAccessException {
		super(LibEmailTemplateVersionBean.class);
		this.view = view;

		// No items exist until a email template is selected and set to this re-loadable container.
	}
	
	public void reload(EmailTemplate emailTemplate) {
		removeAllItems();
		
		// Users need update permission to program its contents
		if ( emailTemplate != null ) {
			
			Collection<EmailTemplateVersionInfo> emailTemplateVers = EmailTemplateVersionInfo.Manager.getAll(emailTemplate);
			for( EmailTemplateVersionInfo d : emailTemplateVers ) {
				addItem( new LibEmailTemplateVersionBean(d,view.getVersionLabel(d)) );			
			}
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}