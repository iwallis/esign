// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.dropdown;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.DropDownVersionInfo;
import com.esignforms.open.prog.DropDownVersionOption;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibDropDownVersionBean implements Serializable, Comparable<LibDropDownVersionBean> {
	private static final long serialVersionUID = 5839598384849093751L;

	private DropDownVersionInfo dropdownVerInfo;
	private String versionLabel;
	
	private DropDownVersion dropdownVersion;
	
	public LibDropDownVersionBean(DropDownVersionInfo dropdownVerInfo, String versionLabel) {
		this.dropdownVerInfo = dropdownVerInfo;
		this.versionLabel = versionLabel;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibDropDownVersionBean )
			return dropdownVerInfo.equals(((LibDropDownVersionBean)o).dropdownVerInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return dropdownVerInfo.hashCode();
    }
	public int compareTo(LibDropDownVersionBean d) {
		return dropdownVerInfo.compareTo(d.dropdownVerInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public DropDownVersionInfo dropdownVerInfo() {
		return dropdownVerInfo;
	}

	public DropDownVersion dropdownVersion() {
		if ( dropdownVersion == null )
			dropdownVersion = DropDownVersion.Manager.getById(dropdownVerInfo.getId());
		return dropdownVersion;
	}

	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( dropdownVersion().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			return true;
		} 

		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}
	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return dropdownVerInfo.getId();
	}

	public int getVersion() {
		return dropdownVerInfo.getVersion();
	}
	
	public String getVersionLabel() {
		return versionLabel;
	}

	public EsfDateTime getCreatedTimestamp() {
		return dropdownVerInfo.getCreatedTimestamp();
	}
	public String formatCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return dropdownVerInfo.getCreatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return dropdownVerInfo.getCreatedTimestamp().toLogString(vaadinUi.getUser());
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return dropdownVerInfo.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return dropdownVerInfo.getLastUpdatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return dropdownVerInfo.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser());
	}
	
	public boolean getAllowMultiSelection() {
		return dropdownVersion().isAllowMultiSelection();
	}
	public void setAllowMultiSelection(boolean v) {
		dropdownVersion().setAllowMultiSelection( v );
	}
	
	public void setOptions(List<DropDownVersionOption> newList) {
		dropdownVersion().setOptions(newList);
	}
}