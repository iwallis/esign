// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.reportField;

import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionReportField;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.reportFieldTemplate.ReportFieldTemplateView;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.reloadTransactionData.ReloadTransactionDataView;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;
import com.vaadin.ui.themes.Reindeer;

public class PackageReportFieldList extends Panel {
	private static final long serialVersionUID = -5664626986878039626L;

	final PackageVersion duplicatedPackageVersion;
	final PackageReportFieldView view;
	final PackageReportFieldContainer container;
	PackageReportFieldTable table;
	
	// For search bar
	Button reportFieldTemplatesButton;
	Button reloadTransactionDataButton;
	Button createNewButton;

	public PackageReportFieldList(final PackageReportFieldView view, final PackageReportFieldContainer containerParam, PackageVersion duplicatedPackageVersionParam) {
		super();
		this.view = view;
		setStyleName("PackageReportFieldList");
		setSizeFull();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
		
    	this.container = containerParam;
    	this.duplicatedPackageVersion = duplicatedPackageVersionParam;
    	
		HorizontalLayout searchBar = new HorizontalLayout();
		searchBar.setWidth(100, Unit.PERCENTAGE);
    	searchBar.addStyleName("searchBar");
    	searchBar.setSpacing(true);
    	searchBar.setMargin(false);

    	layout.addComponent(searchBar);
    	
    	Label dummyLabel = new Label("");
    	searchBar.addComponent(dummyLabel);
    	searchBar.setExpandRatio(dummyLabel, 1.0f); // forces following buttons to right
    	
    	reloadTransactionDataButton = new Button(vaadinUi.getMsg("PackageReportFieldView.searchBar.reloadTransactionDataButton.label"));
    	reloadTransactionDataButton.setStyleName(Reindeer.BUTTON_SMALL);
    	reloadTransactionDataButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageReportFieldView.searchBar.reloadTransactionDataButton.icon")));
    	reloadTransactionDataButton.setDescription(vaadinUi.getMsg("PackageReportFieldView.searchBar.reloadTransactionDataButton.tooltip"));
    	reloadTransactionDataButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = 920301269082960921L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	            final ReloadTransactionDataView reloadView = new ReloadTransactionDataView(duplicatedPackageVersion);
	            reloadView.initView();
	    		
	            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("ReloadTransactionDataView.window.caption",duplicatedPackageVersion.getPackage().getPathName()), reloadView);
	            w.center();
	            w.setModal(true); // don't let them do anything else while on the tran data reload request view
	        	w.setWidth(vaadinUi.getMsg("ReloadTransactionDataView.view.window.width"));
	        	w.setHeight(vaadinUi.getMsg("ReloadTransactionDataView.view.window.height"));

	        	reloadView.activateView(EsfView.OpenMode.WINDOW, "");
	        	vaadinUi.addWindowToUI(w);
			}
		});
    	searchBar.addComponent(reloadTransactionDataButton);
    	searchBar.setComponentAlignment(reloadTransactionDataButton, Alignment.MIDDLE_RIGHT);

    	reportFieldTemplatesButton = new Button(vaadinUi.getMsg("PackageReportFieldView.searchBar.reportFieldTemplatesButton.label"));
    	reportFieldTemplatesButton.setStyleName(Reindeer.BUTTON_SMALL);
    	reportFieldTemplatesButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageReportFieldView.searchBar.reportFieldTemplatesButton.icon")));
    	reportFieldTemplatesButton.setDescription(vaadinUi.getMsg("PackageReportFieldView.searchBar.reportFieldTemplatesButton.tooltip"));
    	reportFieldTemplatesButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = 920301269082960921L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	            final ReportFieldTemplateView rftView = new ReportFieldTemplateView();
	            rftView.initView();
	    		
	            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("ReportFieldTemplateView.window.caption"), rftView);
	            w.center();
	            w.setModal(true); // don't let them do anything else while on they work on report field templates
	            w.setWidth(90, Unit.PERCENTAGE);
	            w.setHeight(90, Unit.PERCENTAGE);
				w.addCloseListener( new CloseListener() {
					private static final long serialVersionUID = 4435616258491733108L;

					@Override
					public void windowClose(CloseEvent e) {
						if ( ! view.isReadOnly() )
							view.refreshReportTemplates();
					}
				});

				rftView.activateView(EsfView.OpenMode.WINDOW, "");
				rftView.setParentWindow(w);
				rftView.setReadOnly(view.isReadOnly());
	        	vaadinUi.addWindowToUI(w);
			}
		});
    	searchBar.addComponent(reportFieldTemplatesButton);
    	searchBar.setComponentAlignment(reportFieldTemplatesButton, Alignment.MIDDLE_RIGHT);

    	createNewButton = new Button(vaadinUi.getMsg("PackageReportFieldView.searchBar.createNewButton.label"));
    	createNewButton.setStyleName(Reindeer.BUTTON_SMALL);
    	createNewButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageReportFieldView.searchBar.createNewButton.icon")));
    	createNewButton.setDescription(vaadinUi.getMsg("PackageReportFieldView.searchBar.createNewButton.tooltip"));
    	createNewButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = -117413691392017403L;

			@Override
			public void buttonClick(ClickEvent event) {
				view.createNew();
			}
		});
    	searchBar.addComponent(createNewButton);
    	searchBar.setComponentAlignment(createNewButton, Alignment.MIDDLE_RIGHT);

    	table = new PackageReportFieldTable(view, container);
    	
    	layout.addComponent(searchBar);
    	layout.addComponent(table);
    	layout.setExpandRatio(table, 1);
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		createNewButton.setVisible(!v);
		table.setDragMode( v ? TableDragMode.NONE : TableDragMode.ROW);
	}
	
	public Table getTable() {
		return table;
	}
	
	class PackageReportFieldTable extends Table {
		private static final long serialVersionUID = -2600029447817738504L;

		public PackageReportFieldTable(PackageReportFieldView view, PackageReportFieldContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("PackageReportFieldList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("PackageReportFieldList.showColumnHeaders"));
			setSortEnabled(false);
			setColumnAlignment("fieldOrder", Align.CENTER);
			setColumnAlignment("showToDo", Align.CENTER);
	        setNullSelectionAllowed(true);
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);

	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener((Property.ValueChangeListener)view);
			setSizeFull();
	    	initializeDND(); // allow reorder via DND
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			if ( "documentId".equals(colId) ) {
				EsfUUID docId = (EsfUUID)property.getValue();
				Document document = Document.Manager.getById(docId);
				return document == null ? "???" : document.getEsfName().toString();
			}
			if ( "reportFieldTemplateId".equals(colId) ) {
				EsfUUID reportFieldTemplateId = (EsfUUID)property.getValue();
				ReportFieldTemplate reportFieldTemplate = ReportFieldTemplate.Manager.getById(reportFieldTemplateId);
				return reportFieldTemplate == null ? "???" : reportFieldTemplate.getFieldName().toString();
			}
			if ( "showToDo".equals(colId) ) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				String showToDo = property.getValue().toString();
				return vaadinUi.getMsg("PackageReportFieldList.showToDo."+showToDo);
			}
			return super.formatPropertyValue(rowId,colId,property);
		}

		@SuppressWarnings("unchecked")
		public void reorder() {
            // Fix up the order now
            short order = 1;
            for( PackageVersionReportField rf : container.getItemIds() ) {
            	BeanItem<PackageVersionReportField> item = container.getItem(rf);
            	item.getItemProperty("fieldOrder").setValue(order); // suppress unchecked
            	rf.setFieldOrder(order++);
            }
            view.form.setContainerOrderHasChanged();
            view.form.setReadOnly(isReadOnly());
		}
		
		public void initializeDND() {
			setDropHandler(new DropHandler() {
				private static final long serialVersionUID = 9193381415779608450L;

				public void drop(DragAndDropEvent dropEvent) {
	                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
	                
	                Container sourceContainer = t.getSourceContainer();
	                if ( sourceContainer == container ) { 
	                	// reordering within the table rows
		                PackageVersionReportField sourceItemId = (PackageVersionReportField)t.getItemId();

		                AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails)dropEvent.getTargetDetails());
		                PackageVersionReportField targetItemId = (PackageVersionReportField)dropData.getItemIdOver();
		                
		                // No move if source and target are the same
		                if ( sourceItemId == targetItemId )
		                	return;
		                
		                // Let's remove the source of the drag so we can add it back where requested...
		                container.removeItem(sourceItemId);
		                if ( targetItemId == null ) {
		                	container.addItem(sourceItemId);
		                } else if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
		                	container.addItemAfter(targetItemId,sourceItemId);
		            	} else {
		                    Object prevItemId = container.prevItemId(targetItemId);
		                    container.addItemAfter(prevItemId, sourceItemId);
		            	}
		                
		                reorder();
	                }
	            }

	            public AcceptCriterion getAcceptCriterion() {
	                return new And(SourceIsTarget.get(), new Not(AbstractSelect.VerticalLocationIs.MIDDLE)); // dragging within the table
	            }
	        });
			
		}
	
	} // PackageReportFieldTable

} // PackageReportFieldList
