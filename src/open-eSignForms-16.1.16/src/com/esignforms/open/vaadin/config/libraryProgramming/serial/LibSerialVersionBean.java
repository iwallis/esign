// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.serial;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.SerialVersion;
import com.esignforms.open.prog.SerialVersionInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibSerialVersionBean implements Serializable, Comparable<LibSerialVersionBean> {
	private static final long serialVersionUID = 2204549673787123886L;

	private SerialVersionInfo serialVerInfo;
	private String versionLabel;
	
	private SerialVersion serialVersion;
	
	public LibSerialVersionBean(SerialVersionInfo serialVerInfo, String versionLabel) {
		this.serialVerInfo = serialVerInfo;
		this.versionLabel = versionLabel;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibSerialVersionBean )
			return serialVerInfo.equals(((LibSerialVersionBean)o).serialVerInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return serialVerInfo.hashCode();
    }
	public int compareTo(LibSerialVersionBean d) {
		return serialVerInfo.compareTo(d.serialVerInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public SerialVersionInfo serialVerInfo() {
		return serialVerInfo;
	}

	public SerialVersion serialVersion() {
		if ( serialVersion == null )
			serialVersion = SerialVersion.Manager.getById(serialVerInfo.getId());
		return serialVersion;
	}

	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( serialVersion().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			return true;
		} 

		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}
	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return serialVerInfo.getId();
	}

	public int getVersion() {
		return serialVerInfo.getVersion();
	}
	
	public String getVersionLabel() {
		return versionLabel;
	}

	public EsfDateTime getCreatedTimestamp() {
		return serialVerInfo.getCreatedTimestamp();
	}
	public String formatCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return serialVerInfo.getCreatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return serialVerInfo.getCreatedTimestamp().toLogString(vaadinUi.getUser());
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return serialVerInfo.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return serialVerInfo.getLastUpdatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return serialVerInfo.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser());
	}
	
	public String getDecimalFormat() {
		return serialVersion().getDecimalFormat();
	}
	public void setDecimalFormat(String v) {
		serialVersion().setDecimalFormat(v);
	}
}