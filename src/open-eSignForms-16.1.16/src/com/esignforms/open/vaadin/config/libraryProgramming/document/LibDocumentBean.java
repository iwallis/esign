// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibDocumentBean implements Serializable, Comparable<LibDocumentBean> {
	private static final long serialVersionUID = -5547230883996972930L;

	private DocumentInfo docInfo;
	
	private Document document;
	
	public LibDocumentBean(DocumentInfo docInfo) {
		this.docInfo = docInfo;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibDocumentBean )
			return docInfo.equals(((LibDocumentBean)o).docInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return docInfo.hashCode();
    }
	public int compareTo(LibDocumentBean d) {
		return docInfo.compareTo(d.docInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public DocumentInfo docInfo() {
		return docInfo;
	}
	
	public Document document() {
		if ( document == null )
			document = Document.Manager.getById(docInfo.getId());
		return document;
	}

	public LibDocumentBean createLike() {
		DocumentInfo docInfo = DocumentInfo.Manager.createLike(document, document.getEsfName());
	    return new LibDocumentBean(docInfo);
	}
	
	
	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( document().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			docInfo = DocumentInfo.Manager.createFromSource(document);
			return true;
		} 
		
		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}

	
	// Now for the JavaBeans methods
	public EsfName getEsfName() {
		return docInfo.getEsfName();
	}
	public void setEsfName(EsfName v) {
		document.setEsfName(v);
	}

	public EsfUUID getId() {
		return docInfo.getId();
	}
	
	public String getDisplayName() {
		return docInfo.getDisplayName();
	}
	public void setDisplayName(String v) {
		document().setDisplayName(v);
	}

	public String getDescription() {
		return docInfo.getDescription();
	}
	public void setDescription(String v) {
		document().setDescription(v);
	}

	public boolean isEnabled() {
		return docInfo.isEnabled();
	}
	public String getStatus() {
		return docInfo.getStatus();
	}
	public void setStatus(String v) {
		document().setStatus(v);
	}	
	
	public Integer getProductionVersion() {
		return docInfo.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		return docInfo.getTestVersion();
	}
	
	public void bumpTestVersion() {
		document().bumpTestVersion();
	}
	public void dropTestVersion() {
		document().dropTestVersion();
	}
	
	public String getFileNameSpec() {
		return document().getFileNameSpec();
	}
	public void setFileNameSpec(String v) {
		document().setFileNameSpec(v);
	}
	
	public String getComments() {
		return document().getComments();
	}
	public void setComments(String v) {
		document().setComments(v);
	}
}