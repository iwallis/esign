// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming;

import java.util.Iterator;

import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;

public class LibraryProgrammingTabSheet extends TabSheet implements TabSheet.SelectedTabChangeListener, TabSheet.CloseHandler {
	private static final long serialVersionUID = -5198793594092389497L;

	LibraryProgrammingView libraryProgrammingView;
	
	public LibraryProgrammingTabSheet(LibraryProgrammingView libraryProgrammingView) {
    	super();
    	this.libraryProgrammingView = libraryProgrammingView;
    	setCloseHandler(this);
    	addSelectedTabChangeListener((TabSheet.SelectedTabChangeListener)this);
	}
	
    public void setTabState() {
		// If we have more than 1 tab, make them all closable, else make the last tab non-closable
		int numTabs = 0;
		Component lastComponent = null;
		Iterator<Component> iter = getComponentIterator();
		while( iter.hasNext() ) {
			++numTabs;
			lastComponent = iter.next();
		}
		if ( numTabs == 1 ) {
			Tab tab = getTab(lastComponent);
			tab.setClosable(false);
		} else if ( numTabs > 1 ) {
			iter = getComponentIterator();
			while( iter.hasNext() ) {
				Tab tab = getTab(iter.next());
				tab.setClosable(true);
			}
		}
    }
 
	public void superClose(TabSheet tabsheet, Component tabContent) {
		tabsheet.removeComponent(tabContent);
		setTabState();
	}
	
	@Override
	public void onTabClose(final TabSheet tabsheet, final Component tabContent) {
		EsfView view = (EsfView)tabContent;
		if ( view != null && view.isDirty() ) {
			new ConfirmDiscardChangesDialog(view.checkDirty(), new ConfirmDiscardChangesListener() {

				@Override
				public void doContinueKeepChanges() {
				}

				@Override
				public void doDiscardChanges() {
					superClose(tabsheet,tabContent);
				}
				
			});
		} else {
			superClose(tabsheet, tabContent);
		}
		
	}

	@Override
	public void selectedTabChange(SelectedTabChangeEvent event) {
	    //TabSheet tabsheet = event.getTabSheet();
	    //EsfView view = (EsfView)tabsheet.getSelectedTab();
	}
}