// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.image;

import java.io.Serializable;
import java.util.Collection;

import com.esignforms.open.prog.ImageInfo;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.util.BeanItemContainer;


public class LibImageBeanContainer extends BeanItemContainer<LibImageBean> implements Serializable {
	private static final long serialVersionUID = -529854531395975293L;

	final Library library;
	
	public LibImageBeanContainer(Library library) throws InstantiationException, IllegalAccessException {
		super(LibImageBean.class);
		this.library = library;
		
		refresh();
	}
	
	public void refresh() {
		removeAllItems();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		// Users need view details permission to see its contents
		if ( library != null && library.hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS) ) {
			
			Collection<ImageInfo> images = ImageInfo.Manager.getAll(library.getId());
			for( ImageInfo i : images ) {
				addItem( new LibImageBean(i) );					
			}
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}