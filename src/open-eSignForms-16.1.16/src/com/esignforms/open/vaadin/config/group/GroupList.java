// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.group;

import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Property;
import com.vaadin.ui.Table;

public class GroupList extends Table {
	private static final long serialVersionUID = -3169293176449189096L;

	public GroupList(GroupView view, GroupViewContainer container) {
		super();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		setSizeFull();
    	
 		setContainerDataSource(container);
		setVisibleColumns(vaadinUi.getStringArray("GroupList.showColumnProperties"));
		setColumnHeaders(vaadinUi.getStringArray("GroupList.showColumnHeaders"));
		setColumnCollapsingAllowed(true);
		setColumnReorderingAllowed(true);
		setCellStyleGenerator(new Table.CellStyleGenerator( ) {
			private static final long serialVersionUID = -7573281313388361138L;

			@Override
			public String getStyle(Table table, Object itemId, Object propertyId) {
				GroupBean bean = (GroupBean)itemId;
				return bean.isEnabled() ? null : "disabledText";
			}
		});
        // Make table selectable, react immediately to user events, and pass events to the view
        setSelectable(true);
        setImmediate(true);
        addValueChangeListener((Property.ValueChangeListener)view);
        // We don't want to allow users to de-select a row 
        setNullSelectionAllowed(true);
	}
	
	@Override
	protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
		if ( "lastUpdatedTimestamp".equals(colId) ) {
			return ((GroupBean)rowId).formatLastUpdatedTimestamp();
		}
		
		return super.formatPropertyValue(rowId,colId,property);
	}
}