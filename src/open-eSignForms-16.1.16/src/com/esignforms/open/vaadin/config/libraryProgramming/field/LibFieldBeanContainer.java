// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.field;

import java.io.Serializable;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.prog.FieldTemplate;
import com.vaadin.data.util.BeanItemContainer;


public class LibFieldBeanContainer extends BeanItemContainer<LibFieldBean> implements Serializable {
	private static final long serialVersionUID = 994218733248660925L;

	public LibFieldBeanContainer(java.util.Map<EsfName,FieldTemplate> fieldTemplateMap) throws InstantiationException, IllegalAccessException {
		super(LibFieldBean.class);
		refresh(fieldTemplateMap);
	}
	
	public void refresh(java.util.Map<EsfName,FieldTemplate> fieldTemplateMap) {
		removeAllItems();
		for( EsfName name : fieldTemplateMap.keySet() ) {
			addItem(new LibFieldBean(fieldTemplateMap.get(name)));
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}