// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.AllBlankCondition;

import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.runtime.condition.Condition;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

public class ConditionList extends Panel {
	private static final long serialVersionUID = 2698135326853293057L;

	final ConditionList thisConditionList;
	final ConditionView view;
	final Condition duplicatedCondition;
	ConditionContainer container;
	SpecList table;
	
	// For search bar
	Button createNewButton;
	CheckBox cbConditionNegated;

	public ConditionList(final ConditionView view, final ConditionContainer containerParam, Condition duplicatedConditionParam) {
		super();
		this.thisConditionList = this;
		this.view = view;
		this.container = containerParam;
		this.duplicatedCondition = duplicatedConditionParam;
		setStyleName("ConditionList");
		setSizeFull();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
    	
		HorizontalLayout searchBar = new HorizontalLayout();
		searchBar.setWidth(100, Unit.PERCENTAGE);
    	searchBar.addStyleName("searchBar");
    	searchBar.setSpacing(true);
    	searchBar.setMargin(false);

    	layout.addComponent(searchBar);
    	Label caption = new Label(vaadinUi.getMsg("ConditionTree.AllBlankCondition.list.label"));
    	searchBar.addComponent(caption);
    	searchBar.setExpandRatio(caption, 0.7f);
    	
    	cbConditionNegated = new CheckBox(vaadinUi.getMsg("ConditionTree.negated.checkbox.label"));
    	cbConditionNegated.setDescription(vaadinUi.getMsg("ConditionTree.negated.checkbox.tooltip"));
    	cbConditionNegated.setImmediate(true);
    	cbConditionNegated.setValue(duplicatedCondition.isNegated());
    	cbConditionNegated.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = 4343373258507347222L;

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				boolean isChecked = (Boolean)event.getProperty().getValue();
				duplicatedCondition.setNegated(isChecked);
				// Toggle the window caption
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				Window w = vaadinUi.getWindow(thisConditionList);
				if ( w != null ) {
					w.setCaption( isChecked ? vaadinUi.getMsg("ConditionTree.AllBlankCondition.view.negated.window.caption") : vaadinUi.getMsg("ConditionTree.AllBlankCondition.view.window.caption"));
				}
			}
    	});
    	searchBar.addComponent(cbConditionNegated);

    	createNewButton = new Button(vaadinUi.getMsg("ConditionTree.AllBlankCondition.view.searchBar.createNewButton.label"));
    	createNewButton.setStyleName(Reindeer.BUTTON_SMALL);
    	createNewButton.setIcon(new ThemeResource(vaadinUi.getMsg("ConditionTree.AllBlankCondition.view.searchBar.createNewButton.icon")));
    	createNewButton.setDescription(vaadinUi.getMsg("ConditionTree.AllBlankCondition.view.searchBar.createNewButton.tooltip"));
    	createNewButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = 7637910150809709208L;

			@Override
			public void buttonClick(ClickEvent event) {
				view.createNew();
			}
		});
    	searchBar.addComponent(createNewButton);
    	searchBar.setComponentAlignment(createNewButton, Alignment.MIDDLE_RIGHT);

    	table = new SpecList(view);
    	//table.initializeDND();
    	
    	layout.addComponent(searchBar);
    	layout.addComponent(table);
    	layout.setExpandRatio(table, 1);
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		createNewButton.setVisible(!v);
	}
	
	public SpecList getTable() {
		return table;
	}
	
	class SpecList extends Table {
		private static final long serialVersionUID = -5988899866456461355L;

		final SpecList thisList;
		
		public SpecList(ConditionView view) {
			super();
			thisList = this;
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("ConditionTree.AllBlankCondition.list.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("ConditionTree.AllBlankCondition.list.showColumnHeaders"));
			setSortEnabled(false);
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);

	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener((Property.ValueChangeListener)view);
	        setNullSelectionAllowed(true);
			setSizeFull();
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			if ( "documentId".equals(colId) ) {
				EsfUUID id = (EsfUUID)property.getValue();
				if ( id == null || id.isNull() )
					return "";
				return Document.Manager.getById(id).getEsfName().toString();
			}
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // SpecList

} // ConditionList
