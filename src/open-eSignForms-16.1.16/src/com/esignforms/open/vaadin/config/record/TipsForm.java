// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.record;

import org.vaadin.openesignforms.ckeditor.CKEditorConfig;
import org.vaadin.openesignforms.ckeditor.CKEditorTextField;
import org.vaadin.openesignforms.ckeditor.CKEditorTextField.VaadinSaveListener;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.Record;
import com.esignforms.open.user.Permission;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.UI;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * The TipsForm is used to configure just a single, configurable deployment property that is then used
 * as tips throughout the app.
 * It is always in EDIT mode.
 * @author Yozons Inc.
 */
public class TipsForm extends CustomComponent implements EsfView {
	private static final long serialVersionUID = -2120717043890175734L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TipsForm.class);
	
	Button saveButton;
	Button cancelButton;
	Button restoreToDefaultButton;
	Permission tipsPermission;
	
    String tipsPrefix;
    VerticalLayout layout;
    HorizontalLayout buttonLayout;
    
    CKEditorTextField editor;
    String editorContextId;
    
    FieldGroup fieldGroup;

    public TipsForm() {
    	setStyleName("TipsForm");
    	setWidth(100,Unit.PERCENTAGE);
    	setHeight(98, Unit.PERCENTAGE);
    	fieldGroup = new FieldGroup();
    }
    
	void setDataSourceFromDeploymentProperties() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Record deploymentProperties = Record.Manager.getById(vaadinUi.getEsfapp().getDeploymentPropertiesId());
		DeploymentPropertiesBean bean = new DeploymentPropertiesBean(deploymentProperties);
		bean.setPropertyPrefix(tipsPrefix);
		BeanItem<DeploymentPropertiesBean> dataSource = new BeanItem<DeploymentPropertiesBean>(bean);
		setItemDataSource(dataSource);
	}
	
	DeploymentPropertiesBean getCurrentBean() {
    	return getBean(fieldGroup.getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public DeploymentPropertiesBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<DeploymentPropertiesBean> bi = (BeanItem<DeploymentPropertiesBean>)dataSource;
		return bi.getBean();
    }
    
    void setItemDataSource(Item dataSource) {
		fieldGroup.setItemDataSource(dataSource);
		setReadOnly(false); // we're always in edit mode 
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	if ( getCurrentBean() != null )
    		fieldGroup.setReadOnly(readOnly);
    	
    	// We have these only while editing
    	if ( saveButton != null )
    		saveButton.setVisible(!readOnly);
    	if ( cancelButton != null )
    		cancelButton.setVisible(!readOnly);
    	if ( restoreToDefaultButton != null ) {
    		restoreToDefaultButton.setVisible(!readOnly && getCurrentBean().hasTipsHtml());
    	}
    	editor.setReadOnly(readOnly);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		tipsPrefix = EsfString.isBlank(params) ? "" : params;

    	setCaption(vaadinUi.getMsg(tipsPrefix+"TipsForm.caption"));
    	
    	tipsPermission = Permission.Manager.getByPathName(new EsfPathName(UI.UI_PERM_PATH_PREFIX+tipsPrefix+"TipsEditView"));
    	if ( tipsPermission == null || ! tipsPermission.hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE) ) {
    		buttonLayout.removeComponent(saveButton);
    		buttonLayout.removeComponent(cancelButton);
    		buttonLayout.removeComponent(restoreToDefaultButton);
    		saveButton = restoreToDefaultButton = cancelButton = null;
    		if ( tipsPermission == null ) {
    			_logger.warn("activateView() - Failed to find permission: " + UI.UI_PERM_PATH_PREFIX+tipsPrefix+"TipsEditView");
    		}
    	}
    	
    	setDataSourceFromDeploymentProperties();
	}

	@Override
	public void initView() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	layout = new VerticalLayout();
    	layout.setSizeFull();
    	layout.setMargin(false);
    	layout.setSpacing(true);
    	setCompositionRoot(layout);
    	
    	editorContextId = vaadinUi.getUser().createCKEditorContext(vaadinUi.getHttpSession());
    	CKEditorConfig config = new CKEditorConfig();
    	config.setupForOpenESignForms(vaadinUi.getRequestContextPath(),editorContextId,null);
    	editor = new CKEditorTextField(config);
    	editor.setImmediate(false);
    	editor.setSizeFull();
    	editor.setReadOnly(false);
    	editor.addVaadinSaveListener( new VaadinSaveListener() {
			private static final long serialVersionUID = 7841762904901899860L;

			@Override
			public void vaadinSave(CKEditorTextField editor) {
				doSave();
			}
    	});
        fieldGroup.bind(editor,"tipsHtml");

        layout.addComponent(editor);
        layout.setExpandRatio(editor, 1.0f);
    	
    	saveButton = new Button(vaadinUi.getMsg("button.save.label"));
    	saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
    	saveButton.setDescription(vaadinUi.getMsg("button.save.tooltip"));
    	saveButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = -8688761513885955431L;

			@Override
			public void buttonClick(ClickEvent event) {
				doSave();
			}
    	});

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"));
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	cancelButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = -8688761513885955431L;

			@Override
			public void buttonClick(ClickEvent event) {
				fieldGroup.discard();
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
			}
    		
    	});

    	restoreToDefaultButton = new Button(vaadinUi.getMsg("button.restoreToDefault.label"));
    	restoreToDefaultButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.restoreToDefault.icon")));
    	restoreToDefaultButton.setDescription(vaadinUi.getMsg("button.restoreToDefault.tooltip"));
    	restoreToDefaultButton.addStyleName("restoreToDefaultButton");
    	restoreToDefaultButton.addClickListener( new ClickListener() {
 			private static final long serialVersionUID = 8018977886689785294L;

				@Override
    			public void buttonClick(ClickEvent event) {
    				fieldGroup.discard();
    				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    				DeploymentPropertiesBean currBean = getCurrentBean();
    	    		currBean.resetTipsHtmlToDefaults();
    	    		vaadinUi.showStatus( vaadinUi.getMsg("form.restoreToDefault.success.message") );
    	    		setDataSourceFromDeploymentProperties();
    			}
        		
        	});

        buttonLayout = new HorizontalLayout();
        buttonLayout.setStyleName("footer");
        buttonLayout.setSpacing(false);
        buttonLayout.setMargin(false);
        buttonLayout.addComponent(saveButton);
        buttonLayout.addComponent(cancelButton);
        buttonLayout.addComponent(restoreToDefaultButton);
        layout.addComponent(buttonLayout);
    	
    	_logger.debug("Form created");
	}
	
	void doSave() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
    	if ( ! fieldGroup.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return;
    	}
    	
    	try {
    		fieldGroup.commit();
    		// If we're saving a new group bean
			Errors errors = new Errors();
			DeploymentPropertiesBean currBean = getCurrentBean();
			if ( currBean.save(errors) ) {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",currBean.getEsfName()) );
    			setDataSourceFromDeploymentProperties();
    		} else {
    			vaadinUi.show(errors);
    		}
    	} catch( FieldGroup.CommitException e ) {
    		_logger.error("fieldGroup.commit()",e);
    	}
	}
 
	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg(tipsPrefix+"TipsForm.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return fieldGroup.isModified();
	}
	
	@Override
	public void detach() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		User user = vaadinUi.getUser();
		if ( user != null ) {
	    	user.releaseCKEditorContext(vaadinUi.getHttpSession(),editorContextId);
		}
		super.detach();
	}
}