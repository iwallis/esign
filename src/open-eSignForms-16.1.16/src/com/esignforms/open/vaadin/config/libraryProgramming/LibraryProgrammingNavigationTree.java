// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming;

import java.util.HashMap;

import com.esignforms.open.config.MessageFormatFile;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.user.UI;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.event.Action;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.MouseEventDetails.MouseButton;
import com.vaadin.ui.Tree;


public class LibraryProgrammingNavigationTree extends Tree implements ItemClickListener, Action.Handler {
	private static final long serialVersionUID = -153359084073924761L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibraryProgrammingNavigationTree.class);

    public static final Object NAV_PROPERTY_LABEL		= "label";		// label of the item, displayed in tree
	public static final Object NAV_PROPERTY_VIEW_FRAGMENT = "viewFragment"; // view fragment to fire when clicked; if blank, no action will take place when clicked
	public static final Object NAV_PROPERTY_ITEM_ID		= "itemId";		// so we can get back the item id for a given item
	public static final Object NAV_PROPERTY_ICON		= "icon";		// icon; if blank, no icon
	
	HashMap<String,NavigationItem> nameToItemMap = new HashMap<String,NavigationItem>();
	HashMap<EsfView,NavigationItem> viewToItemMap = new HashMap<EsfView,NavigationItem>();
	Item currItem = null;
	
    // Actions for the context menu
    private Action actionNothing;
    private Action[] nonViewActions;
    
    private Action actionOpenTab;
    private Action[] viewActions;

    UI ui;
	LibraryProgrammingView libraryProgrammingView;
	final LibraryProgrammingNavigationTree thisTree;
	MessageFormatFile navigationSetup;

    public LibraryProgrammingNavigationTree(final LibraryProgrammingView libraryProgrammingView) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	this.ui = vaadinUi.getEsfapp().getUI();
    	this.libraryProgrammingView = libraryProgrammingView;
    	this.thisTree = this;
    	addStyleName("LibraryProgrammingNavigationTree");
    	
    	navigationSetup = new MessageFormatFile("com.esignforms.open.vaadin.config.libraryProgramming.navigation", getLocale());
    	libraryProgrammingView.setDefaultViewName(navigationSetup.getString("defaultView"));
    	
    	setContainerDataSource(getNavigationContainer(navigationSetup));
    	
        // Set tree to show the 'label' property as caption for items
        setItemCaptionPropertyId(NAV_PROPERTY_LABEL);
        setItemCaptionMode(ItemCaptionMode.PROPERTY);
        setItemIconPropertyId(NAV_PROPERTY_ICON);

        // We want items to be selectable but do not want the user to be able to de-select an item.
        setSelectable(true);
        setNullSelectionAllowed(false);
        
        // Cause valueChange immediately when the user selects
        setImmediate(true);

        // We'll handle item click events
        addItemClickListener( (ItemClickListener)this );
        
        // Add our right-click action handler
        actionNothing = new Action(vaadinUi.getMsg("LibraryProgrammingNavigationTree.action.view.none"));
        nonViewActions = new Action[] { actionNothing };
        
        actionOpenTab = new Action(vaadinUi.getMsg("LibraryProgrammingNavigationTree.action.view.openTab"));
        viewActions = new Action[] { actionOpenTab };

        addActionHandler(this);
    }
    
	public HierarchicalContainer getNavigationContainer(MessageFormatFile navigationSetup) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
        
        // Create new container
        HierarchicalContainer navContainer = new HierarchicalContainer();

        // Create containerproperty for name, permission and viewName so when clicked we know which view to switch to
        navContainer.addContainerProperty(NAV_PROPERTY_LABEL, String.class, null);
        navContainer.addContainerProperty(NAV_PROPERTY_VIEW_FRAGMENT, String.class, null);
        navContainer.addContainerProperty(NAV_PROPERTY_ITEM_ID, Integer.class, null);
        navContainer.addContainerProperty(NAV_PROPERTY_ICON, ThemeResource.class, null);
        
        if ( vaadinUi.isUserLoggedIn() ) {
	        String topLevelPrefix = "nav.";
	        int itemId = 1;
	        int topLevelParentId = 0; // no parent on top level
	        
	        itemId = loadChildren(navContainer,topLevelPrefix,itemId,topLevelParentId);
        }
        
        return navContainer;
    }
	
	private int loadChildren(HierarchicalContainer navContainer, String prefix, int itemId, int parentId) {
        // Load our tree based on the configuration
		int navIndex = 1;

    	while( true ) {
    		String itemPrefix = prefix+navIndex+".";
    		
        	try {
            	Item item = createNavItem(itemId, navigationSetup, navContainer, itemPrefix);
            	if ( item == null ) {
            		if ( navIndex == 1 && parentId > 0 ) { // If 1 when no child found, then no children at all
            			navContainer.setChildrenAllowed(parentId, false);
            		}
            		break;
            	} else if ( parentId > 0 ){
               		if ( navIndex == 1 ) { // We found our first child, so children are allowed on this parent
            			navContainer.setChildrenAllowed(parentId, true);
            		}
            		navContainer.setParent(itemId, parentId);
            	}

            	// Now load the children of this itemId (which will be their parents) starting with the next item id
           		itemId = loadChildren(navContainer, itemPrefix, (itemId+1), itemId);       		
        	} catch( Exception e ) {
        		// no permission, so try next 
        	}
        	
        	++navIndex;
        }       
    	
    	return itemId;
	}
	
	// We only throw an exception if the user doesn't have permission; null means no item to be created
    @SuppressWarnings("unchecked")
	Item createNavItem(int itemId, MessageFormatFile navigationSetup, HierarchicalContainer navContainer, String prefix) 
    	throws Exception {
    	String label = navigationSetup.getString(prefix+"label");
    	
    	// If we have no label, then we're done
    	if ( EsfString.isBlank(label) ) {
    		return null;
    	}

    	String viewName = navigationSetup.getString(prefix+"view.name");
    	
    	String icon = navigationSetup.getString(prefix+"icon");
    	Class<EsfView> viewClass = null;
    	boolean viewDoCache = false;
    	String tabTitle = null;
    	String windowTitle = null;
    	if ( EsfString.isNonBlank(viewName) ) {
    		String viewClassName = navigationSetup.getString(viewName+".class");
    		try {
    			Class<?> checkViewClass = Class.forName(viewClassName);
    			if ( EsfView.class.isAssignableFrom(checkViewClass) ) {
    				viewClass = (Class<EsfView>)checkViewClass;
    			}
    		} catch( ClassNotFoundException e ) {
    			_logger.error("createNavItem() - Could not find view class for view name: " + viewName + "; view class: " + viewClassName + "; key: " + prefix+"view.name");
    		}
    		String doCacheValue = navigationSetup.getString(viewName+".doCache");
    		viewDoCache = EsfBoolean.toBoolean(doCacheValue);
    		// If we don't have an icon on the navigation element, see if there's a view icon to use
    		if ( EsfString.isBlank(icon) ) {
        		icon = navigationSetup.getString(viewName+".icon");
    		}
    		tabTitle = navigationSetup.getString(viewName+".tabTitle");
    		windowTitle = navigationSetup.getString(viewName+".windowTitle");
    	}
     	
    	ThemeResource iconThemeResource = EsfString.isNonBlank(icon) ? new ThemeResource(icon) : null;
    	
        // Add new item and then store its properties with it
        Item item = navContainer.addItem(itemId);
        item.getItemProperty(NAV_PROPERTY_LABEL).setValue(label);
        item.getItemProperty(NAV_PROPERTY_VIEW_FRAGMENT).setValue(viewName);
        item.getItemProperty(NAV_PROPERTY_ITEM_ID).setValue(itemId);
        if ( EsfString.isNonBlank(icon) ) {
            item.getItemProperty(NAV_PROPERTY_ICON).setValue(iconThemeResource);
        }
        
        if ( EsfString.isNonBlank(viewName) && ! nameToItemMap.containsKey(viewName) ) {
        	NavigationItem navItem = new NavigationItem(viewName,null,viewClass,iconThemeResource,tabTitle,windowTitle,viewDoCache);
        	nameToItemMap.put(viewName, navItem);
        }
 
        return item;
    }
    
	@Override
	public void itemClick(final ItemClickEvent event) {
		final String label = (String)event.getItem().getItemProperty(NAV_PROPERTY_LABEL).getValue();
		final String viewFragment = (String)event.getItem().getItemProperty(NAV_PROPERTY_VIEW_FRAGMENT).getValue();
		
		if (event.getButton() == MouseButton.RIGHT) {
			_logger.debug("IGNORING right click on library programming tree item label: " + label + "; viewFragment: " + viewFragment);
		}
		else {
			_logger.debug("itemClicked - label: " + label + "; viewFragment: " + viewFragment);
			
			// Do we have a currently selected navigation item.  If so, check that it's not dirty before we move forward.
			if ( currItem != null && libraryProgrammingView.isDirty() ) {
    			new ConfirmDiscardChangesDialog(libraryProgrammingView.checkDirty(), new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						currItem = event.getItem();
						libraryProgrammingView.showView(viewFragment,"");
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						thisTree.select(currItem.getItemProperty(NAV_PROPERTY_ITEM_ID).getValue());
						vaadinUi.showStatus(vaadinUi.getMsg("ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});	
			} else if ( EsfString.isNonBlank(viewFragment) ) { // only fire an event if a view is configured for this item
				currItem = event.getItem();
				libraryProgrammingView.showView(viewFragment,"");
			}
		}
	}
	
	@Override
	public Action[] getActions(Object target, Object sender) {
		if ( target == null ) // this was introduced in the update to Vaadin 6.6.0 -- not sure why it is calling us with a null 'target' param.
			return null;
		// Add our right-click action handler if there's a view name set
		Item item = getItem(target);
		String viewFragment = (String)item.getItemProperty(NAV_PROPERTY_VIEW_FRAGMENT).getValue();
		return EsfString.isBlank(viewFragment) ? nonViewActions : viewActions;
	}

	@Override
	public void handleAction(Action action, Object sender, Object target) {
		if ( action == actionOpenTab ) {
   	 		Item item = getItem(target);
   	 		
   			String label = (String)item.getItemProperty(NAV_PROPERTY_LABEL).getValue();
   			String viewFragment = (String)item.getItemProperty(NAV_PROPERTY_VIEW_FRAGMENT).getValue();
   			_logger.debug("handleAction ACTION_OPEN_TAB - label: " + label + "; viewFragment: " + viewFragment);

   			if ( viewFragment != null ) {
   				NavigationItem navItem = getNavigationItem(viewFragment);
   				if ( navItem != null ) {
   					libraryProgrammingView.showViewInNewTab(navItem);
   				}
   			}
   	 	}
	}
	
	public NavigationItem getNavigationItem(String viewName) {
		return nameToItemMap.get(viewName);
	}
	public NavigationItem getNavigationItem(EsfView view) {
		return viewToItemMap.get(view);
	}
	// Called by our MainWindow as views are created so we can map back existing views to the appropriate navigation item
	void setNavigationItem(EsfView view, NavigationItem navItem) {
		if ( ! viewToItemMap.containsKey(view) ) {
			viewToItemMap.put(view,navItem);
		}
	}
	
	public class NavigationItem implements java.io.Serializable {
		private static final long serialVersionUID = 8597684244285800236L;

		private String viewName;
		private String viewParams;
		private Class<EsfView> viewClass;
		private boolean doCache;
		private ThemeResource icon;
		private String tabTitle;
		private String windowTitle;
		
		public NavigationItem(String viewName, String viewParams, Class<EsfView> viewClass, ThemeResource icon, String tabTitle, String windowTitle, boolean doCache) {
			this.viewName = viewName;
			this.viewParams = EsfString.isBlank(viewParams) ? null : viewParams;
			this.viewClass = viewClass;
			this.icon = icon;
			this.tabTitle = tabTitle;
			this.windowTitle = windowTitle;
			this.doCache = doCache;
		}
		
		public String getViewName() {
			return viewName;
		}
		public boolean doLogout() {
			return "#logout".equalsIgnoreCase(viewName);
		}

		public String getViewParams() {
			return viewParams;
		}
		public boolean hasViewParams() {
			return viewParams != null;
		}
		
		public String getViewFragment() {
			return hasViewParams() ? getViewName() + "/" + getViewParams() : getViewName();
		}
		
		public Class<EsfView> getViewClass() {
			return viewClass;
		}
		public boolean hasViewClass() {
			return viewClass != null;
		}
		
		public ThemeResource getIcon() {
			return icon;
		}
		
		public String getTabTitle() {
			return tabTitle;
		}
		
		public String getWindowTitle() {
			return windowTitle;
		}

		public boolean doCache() {
			return doCache;
		}
	}
}