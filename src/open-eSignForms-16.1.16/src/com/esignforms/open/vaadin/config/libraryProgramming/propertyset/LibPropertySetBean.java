// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.propertyset;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.PropertySet;
import com.esignforms.open.prog.PropertySetInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibPropertySetBean implements Serializable, Comparable<LibPropertySetBean> {
	private static final long serialVersionUID = 6764725876287639464L;

	private PropertySetInfo propertysetInfo;

	private PropertySet propertyset;
	
	public LibPropertySetBean(PropertySetInfo propertysetInfo) {
		this.propertysetInfo = propertysetInfo;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibPropertySetBean )
			return propertysetInfo.equals(((LibPropertySetBean)o).propertysetInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return propertysetInfo.hashCode();
    }
	public int compareTo(LibPropertySetBean d) {
		return propertysetInfo.compareTo(d.propertysetInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public PropertySetInfo propertysetInfo() {
		return propertysetInfo;
	}

	public PropertySet propertyset() {
		if ( propertyset == null )
			propertyset = PropertySet.Manager.getById(propertysetInfo.getId());
		return propertyset;
	}

	public LibPropertySetBean createLike() {
		PropertySetInfo newPropertySetInfo = PropertySetInfo.Manager.createLike(propertyset, propertyset.getEsfName());
	    return new LibPropertySetBean(newPropertySetInfo);
	}
	
	
	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( propertyset().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			propertysetInfo = PropertySetInfo.Manager.createFromSource(propertyset);
			return true;
		} 
		
		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}

	
	// Now for the JavaBeans methods
	public EsfName getEsfName() {
		return propertysetInfo.getEsfName();
	}
	public void setEsfName(EsfName v) {
		propertyset.setEsfName(v);
	}

	public EsfUUID getId() {
		return propertysetInfo.getId();
	}
	
	public String getDescription() {
		return propertysetInfo.getDescription();
	}
	public void setDescription(String v) {
		propertyset().setDescription(v);
	}

	public boolean isEnabled() {
		return propertysetInfo.isEnabled();
	}
	public String getStatus() {
		return propertysetInfo.getStatus();
	}
	public void setStatus(String v) {
		propertyset().setStatus(v);
	}	
	
	public Integer getProductionVersion() {
		return propertysetInfo.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		return propertysetInfo.getTestVersion();
	}
	
	public void bumpTestVersion() {
		propertyset().bumpTestVersion();
	}
	public void dropTestVersion() {
		propertyset().dropTestVersion();
	}
	
	public String getComments() {
		return propertyset().getComments();
	}
	public void setComments(String v) {
		propertyset().setComments(v);
	}
}