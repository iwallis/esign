// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.dropdown;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.DropDownInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibDropDownBean implements Serializable, Comparable<LibDropDownBean> {
	private static final long serialVersionUID = 1335563529407535054L;

	private DropDownInfo dropdownInfo;

	private DropDown dropdown;
	
	public LibDropDownBean(DropDownInfo dropdownInfo) {
		this.dropdownInfo = dropdownInfo;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibDropDownBean )
			return dropdownInfo.equals(((LibDropDownBean)o).dropdownInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return dropdownInfo.hashCode();
    }
	public int compareTo(LibDropDownBean d) {
		return dropdownInfo.compareTo(d.dropdownInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public DropDownInfo dropdownInfo() {
		return dropdownInfo;
	}

	public DropDown dropdown() {
		if ( dropdown == null )
			dropdown = DropDown.Manager.getById(dropdownInfo.getId());
		return dropdown;
	}

	public LibDropDownBean createLike() {
		DropDownInfo newDropDownInfo = DropDownInfo.Manager.createLike(dropdown, dropdown.getEsfName());	    
	    return new LibDropDownBean(newDropDownInfo);
	}
	
	
	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( dropdown().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			dropdownInfo = DropDownInfo.Manager.createFromSource(dropdown);
			return true;
		} 
		
		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}

	
	// Now for the JavaBeans methods
	public EsfName getEsfName() {
		return dropdownInfo.getEsfName();
	}
	public void setEsfName(EsfName v) {
		dropdown.setEsfName(v);
	}

	public EsfUUID getId() {
		return dropdownInfo.getId();
	}
	
	public String getDescription() {
		return dropdownInfo.getDescription();
	}
	public void setDescription(String v) {
		dropdown().setDescription(v);
	}

	public boolean isEnabled() {
		return dropdownInfo.isEnabled();
	}
	public String getStatus() {
		return dropdownInfo.getStatus();
	}
	public void setStatus(String v) {
		dropdown().setStatus(v);
	}	
	
	public Integer getProductionVersion() {
		return dropdownInfo.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		return dropdownInfo.getTestVersion();
	}
	
	public void bumpTestVersion() {
		dropdown().bumpTestVersion();
	}
	public void dropTestVersion() {
		dropdown().dropTestVersion();
	}
	
	public String getComments() {
		return dropdown().getComments();
	}
	public void setComments(String v) {
		dropdown().setComments(v);
	}
}