// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.record;

import java.io.Serializable;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfHtml;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.Record;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;

public class DeploymentPropertiesBean implements Serializable {
	private static final long serialVersionUID = -6876225777086424194L;

	private Record record;
	private String propertyPrefix;
	
	public DeploymentPropertiesBean(Record record) {
		this.record = record;
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	protected User user() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getUser();
	}

	public boolean save(Errors errors) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		boolean ret = record.save();
		if ( ! ret ) {
			if ( errors != null ) {
				errors.addError(vaadinUi.getMsg("form.save.failed.message"));
			} 
		} else {
			// Let's update our application runtime as necessary
			Application app = vaadinUi.getEsfapp();
			app.setAllowProductionTransactions(getAllowProductionTransactions());
			app.setAllowTestTransactions(getAllowTestTransactions());
			app.setDefaultUrlProtocolAddress(getDefaultUrlProtocolAddress().toString());
			app.setDefaultTimezone(getDefaultTimeZone().toString());
			app.setDefaultLocale(getDefaultLocale().toString());
			app.setDefaultCountryCode(getDefaultCountryCode().toString());
			app.setDefaultDateFormat(getDefaultDateFormat().toString());
			app.setDefaultTimeFormat(getDefaultTimeFormat().toString());
			app.setDefaultLogDateFormat(getDefaultLogDateFormat().toString());
			app.setDefaultLogTimeFormat(getDefaultLogTimeFormat().toString());
			
			app.setLicenseType(getLicenseType());
			app.setLicenseSize(getLicenseSize());
			
			app.setDefaultSessionTimeoutMinutes(getDefaultSessionTimeoutMinutes());
			app.setLoggedInSessionTimeoutMinutes(getLoggedInSessionTimeoutMinutes());

			app.setRetainSystemConfigChangeDays(getRetainSystemConfigChangeDays());
			app.setRetainSystemUserActivityDays(getRetainSystemUserActivityDays());
			app.setRetainSystemStartStopDays(getRetainSystemStartStopDays());
			app.setRetainStatsDays(getRetainStatsDays());
			app.setRetainUserConfigChangeDays(getRetainUserConfigChangeDays());
			app.setRetainUserSecurityDays(getRetainUserSecurityDays());
			app.setRetainUserLoginLogoffDays(getRetainUserLoginLogoffDays());
		}
		return ret;
	}
	public boolean save() {
		return save(null);
	}
	
	public boolean delete() {
		return record.delete();
	}

	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return record.getId();
	}
	
	public EsfName getEsfName() {
		return record.getEsfName();
	}
	
	public EsfUUID getDeployId() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getDeployId();
	}
	
	private EsfName getInstallYearEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getInstallYearEsfName();
	}
	public String getInstallYear() {
		return record.getIntegerByName(getInstallYearEsfName()).toPlainString();
	}
	
	private EsfName getInstallDateEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getInstallDateEsfName();
	}
	public String getInstallDate() { // using String instead of EsfDate to avoid form commit trying to convert the string back to a date
		EsfDate d = record.getDateByName(getInstallDateEsfName());
		return d == null ? "" : d.toString();
	}
	
	public String getContextPath() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getContextPath();
	}
	
	private EsfName getDefaultLocaleEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getDefaultLocaleEsfName();
	}
	public EsfString getDefaultLocale() {
		return record.getStringByName(getDefaultLocaleEsfName());
	}
	public void setDefaultLocale(EsfString v) {
		record.addUpdate(getDefaultLocaleEsfName(), v);
	}
	
	private EsfName getDefaultUrlProtocolAddressEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getDefaultUrlProtocolAddressEsfName();
	}
	public EsfString getDefaultUrlProtocolAddress() {
		return record.getStringByName(getDefaultUrlProtocolAddressEsfName());
	}
	public void setDefaultUrlProtocolAddress(EsfString v) {
		record.addUpdate(getDefaultUrlProtocolAddressEsfName(), v);
	}
	
	private EsfName getDefaultTimeZoneEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getDefaultTimeZoneEsfName();
	}
	public EsfString getDefaultTimeZone() {
		return record.getStringByName(getDefaultTimeZoneEsfName());
	}
	public void setDefaultTimeZone(EsfString v) {
		record.addUpdate(getDefaultTimeZoneEsfName(), v);
	}
	
	private EsfName getDefaultCountryCodeEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getDefaultCountryCodeEsfName();
	}
	public EsfString getDefaultCountryCode() {
		return record.getStringByName(getDefaultCountryCodeEsfName());
	}
	public void setDefaultCountryCode(EsfString v) {
		record.addUpdate(getDefaultCountryCodeEsfName(), v);
	}
	
	private EsfName getDefaultDateFormatEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getDefaultDateFormatEsfName();
	}
	public EsfString getDefaultDateFormat() {
		return record.getStringByName(getDefaultDateFormatEsfName());
	}
	public void setDefaultDateFormat(EsfString v) {
		record.addUpdate(getDefaultDateFormatEsfName(), v);
	}
	
	private EsfName getDefaultTimeFormatEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getDefaultTimeFormatEsfName();
	}
	public EsfString getDefaultTimeFormat() {
		return record.getStringByName(getDefaultTimeFormatEsfName());
	}
	public void setDefaultTimeFormat(EsfString v) {
		record.addUpdate(getDefaultTimeFormatEsfName(), v);
	}
	
	private EsfName getDefaultLogDateFormatEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getDefaultLogDateFormatEsfName();
	}
	public EsfString getDefaultLogDateFormat() {
		return record.getStringByName(getDefaultLogDateFormatEsfName());
	}
	public void setDefaultLogDateFormat(EsfString v) {
		record.addUpdate(getDefaultLogDateFormatEsfName(), v);
	}
	
	private EsfName getDefaultLogTimeFormatEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getDefaultLogTimeFormatEsfName();
	}
	public EsfString getDefaultLogTimeFormat() {
		return record.getStringByName(getDefaultLogTimeFormatEsfName());
	}
	public void setDefaultLogTimeFormat(EsfString v) {
		record.addUpdate(getDefaultLogTimeFormatEsfName(), v);
	}
	
	private EsfName getSmtpServerEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getSmtpServerEsfName();
	}
	public EsfString getSmtpServer() {
		return record.getStringByName(getSmtpServerEsfName());
	}
	public void setSmtpServer(EsfString v) {
		record.addUpdate(getSmtpServerEsfName(), v);
	}
	
	private EsfName getSmtpPortEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getSmtpPortEsfName();
	}
	public EsfInteger getSmtpPort() {
		return record.getIntegerByName(getSmtpPortEsfName());
	}
	public void setSmtpPort(EsfInteger v) {
		record.addUpdate(getSmtpPortEsfName(), v);
	}
	
	private EsfName getSmtpAuthUserEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getSmtpAuthUserEsfName();
	}
	public EsfString getSmtpAuthUser() {
		return record.getStringByName(getSmtpAuthUserEsfName());
	}
	public void setSmtpAuthUser(EsfString v) {
		record.addUpdate(getSmtpAuthUserEsfName(), v);
	}
	
	private EsfName getSmtpAuthPasswordEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getSmtpAuthPasswordEsfName();
	}
	public EsfString getSmtpAuthPassword() {
		return record.getStringByName(getSmtpAuthPasswordEsfName());
	}
	public void setSmtpAuthPassword(EsfString v) {
		record.addUpdate(getSmtpAuthPasswordEsfName(), v);
	}
	
	private EsfName getSmtpStartTLSEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getSmtpStartTLSEsfName();
	}
	public boolean getSmtpStartTLS() {
		return record.getBooleanByName(getSmtpStartTLSEsfName()).isTrue();
	}
	public void setSmtpStartTLS(boolean v) {
		record.addUpdate(getSmtpStartTLSEsfName(), new EsfBoolean(v));
	}

	private EsfName getImapServerEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getImapServerEsfName();
	}
	public EsfString getImapServer() {
		return record.getStringByName(getImapServerEsfName());
	}
	public void setImapServer(EsfString v) {
		record.addUpdate(getImapServerEsfName(), v);
	}
	
	private EsfName getImapPortEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getImapPortEsfName();
	}
	public EsfInteger getImapPort() {
		return record.getIntegerByName(getImapPortEsfName());
	}
	public void setImapPort(EsfInteger v) {
		record.addUpdate(getImapPortEsfName(), v);
	}
	
	private EsfName getImapUserEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getImapUserEsfName();
	}
	public EsfString getImapUser() {
		return record.getStringByName(getImapUserEsfName());
	}
	public void setImapUser(EsfString v) {
		record.addUpdate(getImapUserEsfName(), v);
	}
	
	private EsfName getImapPasswordEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getImapPasswordEsfName();
	}
	public EsfString getImapPassword() {
		return record.getStringByName(getImapPasswordEsfName());
	}
	public void setImapPassword(EsfString v) {
		record.addUpdate(getImapPasswordEsfName(), v);
	}
	
	private EsfName getImapSSLEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getImapSSLEsfName();
	}
	public boolean getImapSSL() {
		return record.getBooleanByName(getImapSSLEsfName()).isTrue();
	}
	public void setImapSSL(boolean v) {
		record.addUpdate(getImapSSLEsfName(), new EsfBoolean(v));
	}

	private EsfName getDefaultEmailFromEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getDefaultEmailFromEsfName();
	}
	public EsfString getDefaultEmailFrom() {
		return record.getStringByName(getDefaultEmailFromEsfName());
	}
	public void setDefaultEmailFrom(EsfString v) {
		record.addUpdate(getDefaultEmailFromEsfName(), v);
	}
	
	private EsfName getSmtpReturnPathHostNameEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getSmtpReturnPathHostNameEsfName();
	}
	public EsfString getSmtpReturnPathHostName() {
		return record.getStringByName(getSmtpReturnPathHostNameEsfName());
	}
	public void setSmtpReturnPathHostName(EsfString v) {
		record.addUpdate(getSmtpReturnPathHostNameEsfName(), v);
	}
	
	public void setPropertyPrefix(String prefix) {
		propertyPrefix = prefix;
	}
	private EsfName getTipsHtmlEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getTipsHtmlEsfName(propertyPrefix);
	}
	public EsfHtml getTipsHtml() {
		EsfHtml html = record.getHtmlByName(getTipsHtmlEsfName());
		if ( html == null || html.isBlank() ) {
			html = getDefaultTipsHtml(propertyPrefix);
		}
		return html;
	}
	public boolean hasTipsHtml() {
		EsfHtml html = record.getHtmlByName(getTipsHtmlEsfName());
		return html != null && html.isNonBlank();
	}
	public void setTipsHtml(EsfHtml v) {
		record.addUpdate(getTipsHtmlEsfName(), v);
	}
	public EsfHtml getDefaultTipsHtml(String tipsPrefix) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return new EsfHtml(vaadinUi.getMsg(tipsPrefix+"TipsView.tipsAreaHtml.default"));
	}
	public void resetTipsHtmlToDefaults() {
		record.remove(getTipsHtmlEsfName());
		record.save();
	}
	
	private EsfName getAllowProductionTransactionsEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getAllowProductionTransactionsEsfName();
	}
	public boolean getAllowProductionTransactions() {
		return record.getBooleanByName(getAllowProductionTransactionsEsfName()).isTrue();
	}
	public void setAllowProductionTransactions(boolean v) {
		record.addUpdate(getAllowProductionTransactionsEsfName(), new EsfBoolean(v));
	}

	private EsfName getAllowTestTransactionsEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getAllowTestTransactionsEsfName();
	}
	public boolean getAllowTestTransactions() {
		return record.getBooleanByName(getAllowTestTransactionsEsfName()).isTrue();
	}
	public void setAllowTestTransactions(boolean v) {
		record.addUpdate(getAllowTestTransactionsEsfName(), new EsfBoolean(v));
	}
	
	
	private EsfName getLicenseTypeEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getLicenseTypeEsfName();
	}
	public EsfString getLicenseType() {
		return record.getStringByName(getLicenseTypeEsfName());
	}
	public void setLicenseType(EsfString v) {
		record.addUpdate(getLicenseTypeEsfName(), v);
	}
	
	private EsfName getLicenseSizeEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getLicenseSizeEsfName();
	}
	public EsfInteger getLicenseSize() {
		return record.getIntegerByName(getLicenseSizeEsfName());
	}
	public void setLicenseSize(EsfInteger v) {
		record.addUpdate(getLicenseSizeEsfName(), v);
	}
	
	public String getDbSize() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return EsfInteger.byteSizeInUnits(vaadinUi.getEsfapp().getDatabaseSize());
	}

	private EsfName getDefaultSessionTimeoutMinutesEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getDefaultSessionTimeoutMinutesEsfName();
	}
	public EsfInteger getDefaultSessionTimeoutMinutes() {
		return record.getIntegerByName(getDefaultSessionTimeoutMinutesEsfName());
	}
	public void setDefaultSessionTimeoutMinutes(EsfInteger v) {
		record.addUpdate(getDefaultSessionTimeoutMinutesEsfName(), v);
	}
	
	private EsfName getLoggedInSessionTimeoutMinutesEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getLoggedInSessionTimeoutMinutesEsfName();
	}
	public EsfInteger getLoggedInSessionTimeoutMinutes() {
		return record.getIntegerByName(getLoggedInSessionTimeoutMinutesEsfName());
	}
	public void setLoggedInSessionTimeoutMinutes(EsfInteger v) {
		record.addUpdate(getLoggedInSessionTimeoutMinutesEsfName(), v);
	}
	
	private EsfName getRetainSystemConfigChangeDaysEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getRetainSystemConfigChangeDaysEsfName();
	}
	public EsfInteger getRetainSystemConfigChangeDays() {
		return record.getIntegerByName(getRetainSystemConfigChangeDaysEsfName());
	}
	public void setRetainSystemConfigChangeDays(EsfInteger v) {
		record.addUpdate(getRetainSystemConfigChangeDaysEsfName(), v);
	}
	
	private EsfName getRetainSystemUserActivityDaysEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getRetainSystemUserActivityDaysEsfName();
	}
	public EsfInteger getRetainSystemUserActivityDays() {
		return record.getIntegerByName(getRetainSystemUserActivityDaysEsfName());
	}
	public void setRetainSystemUserActivityDays(EsfInteger v) {
		record.addUpdate(getRetainSystemUserActivityDaysEsfName(), v);
	}
	
	private EsfName getRetainSystemStartStopDaysEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getRetainSystemStartStopDaysEsfName();
	}
	public EsfInteger getRetainSystemStartStopDays() {
		return record.getIntegerByName(getRetainSystemStartStopDaysEsfName());
	}
	public void setRetainSystemStartStopDays(EsfInteger v) {
		record.addUpdate(getRetainSystemStartStopDaysEsfName(), v);
	}
	
	private EsfName getRetainStatsDaysEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getRetainStatsDaysEsfName();
	}
	public EsfInteger getRetainStatsDays() {
		return record.getIntegerByName(getRetainStatsDaysEsfName());
	}
	public void setRetainStatsDays(EsfInteger v) {
		record.addUpdate(getRetainStatsDaysEsfName(), v);
	}
	
	private EsfName getRetainUserConfigChangeDaysEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getRetainUserConfigChangeDaysEsfName();
	}
	public EsfInteger getRetainUserConfigChangeDays() {
		return record.getIntegerByName(getRetainUserConfigChangeDaysEsfName());
	}
	public void setRetainUserConfigChangeDays(EsfInteger v) {
		record.addUpdate(getRetainUserConfigChangeDaysEsfName(), v);
	}
	
	private EsfName getRetainUserSecurityDaysEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getRetainUserSecurityDaysEsfName();
	}
	public EsfInteger getRetainUserSecurityDays() {
		return record.getIntegerByName(getRetainUserSecurityDaysEsfName());
	}
	public void setRetainUserSecurityDays(EsfInteger v) {
		record.addUpdate(getRetainUserSecurityDaysEsfName(), v);
	}
	
	private EsfName getRetainUserLoginLogoffDaysEsfName() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getRetainUserLoginLogoffDaysEsfName();
	}
	public EsfInteger getRetainUserLoginLogoffDays() {
		return record.getIntegerByName(getRetainUserLoginLogoffDaysEsfName());
	}
	public void setRetainUserLoginLogoffDays(EsfInteger v) {
		record.addUpdate(getRetainUserLoginLogoffDaysEsfName(), v);
	}
}