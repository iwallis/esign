// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.dropdown;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.vaadin.dialogs.ConfirmDialog;

import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.DropDownVersionOption;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Buffered;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * The LibDropDownVersionForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class LibDropDownVersionForm extends CustomComponent implements EsfView, ClickListener {
	private static final long serialVersionUID = 4508696464716526983L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibDropDownVersionForm.class);
	
	final LibDropDownVersionForm thisForm;
	final LibDropDownAndVersionsMainView view;
	VerticalLayout layout;
	FieldGroup fieldGroup;
	HorizontalLayout buttonLayout;
	
	Label id;
	Label createdByInfo;
	Label lastUpdatedByInfo;
	Label versionInfo;
	
	Button testToProdButton;
	Button createTestFromProdButton;
	Button revertProdToTestButton;
	Button deleteButton;

	Map<Object,List<Field<?>>> optionInfoListFields = new TreeMap<Object,List<Field<?>>>();
	OptionInfoList optionInfoList;
	boolean optionInfoListChanged;
	HorizontalLayout optionInfoButtonLayout;
	Button optionInfoListAddButton;
	Button optionInfoListRemoveButton;

	public LibDropDownVersionForm(LibDropDownAndVersionsMainView view) {
    	setStyleName("LibDropDownVersionForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.view = view;
    	this.thisForm = this;
    	this.fieldGroup = new FieldGroup();
    }
    
    private void resetCaches() {
    	optionInfoListFields.clear();
    }
    
	@Override
	public void buttonClick(ClickEvent event) {
    	LibDropDownVersionBean currBean = getCurrentBean();
    	if ( currBean == null ) return;
    	
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	Button source = event.getButton();
    	if ( source == testToProdButton ) {
    		view.promoteTestToProductionVersion();
    	} else if ( source == createTestFromProdButton ) {
    		view.createNextVersion();
    	} else if ( source == revertProdToTestButton ) {
    		view.revertProductionVersionBackToTest();
    	} else if ( source == optionInfoListAddButton ) {
        	commitFieldsToTableOnly();
        	optionInfoList.addNewOptionInfo();
        	optionInfoList.setPageLength();
    	} else if ( source == optionInfoListRemoveButton ) {
        	commitFieldsToTableOnly();
        	optionInfoList.removeSelectedOptionInfo();
        	optionInfoList.setPageLength();
    	} else if ( source == deleteButton ) {
	    	ConfirmDialog.show(UI.getCurrent(), 
	    			vaadinUi.getMsg("LibDropDownVersionForm.button.delete.ConfirmDialog.caption"),
	    			vaadinUi.getMsg("LibDropDownVersionForm.button.delete.ConfirmDialog.message",optionInfoList.size()),
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
	    	        new ConfirmDialog.Listener() {
						private static final long serialVersionUID = 4333333321762635386L;

						public void onClose(ConfirmDialog dialog) {
	    	                if (dialog.isConfirmed()) {
	    	                    view.delete();
	    	                }
	    	            }
	    	        });
    	}
    }
    
    LibDropDownVersionBean save(Connection con) throws SQLException {
    	LibDropDownVersionBean currBean = getCurrentBean();
    	if ( currBean != null ) {
    		if ( currBean.save(con,null) ) {
    			return currBean;
    		}
    	}
    	return null;
    }
    	
    public String getCurrentBeanName() {
    	LibDropDownVersionBean currBean = getCurrentBean();
		return currBean == null ? "(None)" : view.getEsfNameWithVersion();
	}
    
	LibDropDownVersionBean getCurrentBean() {
    	return getBean(fieldGroup.getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public LibDropDownVersionBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibDropDownVersionBean> bi = (BeanItem<LibDropDownVersionBean>)dataSource;
		return bi.getBean();
    }
    
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached stuff
		resetCaches();
		
    	if (newDataSource != null) {
    		LibDropDownVersionBean bean = getBean(newDataSource);
    		
    		fieldGroup.setItemDataSource(newDataSource);
    		
    		setupForm(bean);
    		
    		setReadOnly( ! view.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		layout.setVisible(true);
    		buttonLayout.setVisible(true);
    	} else {
    		fieldGroup.setItemDataSource(null);
    		layout.setVisible(false);
    		buttonLayout.setVisible(false);
    	}
    }
	

    @Override
    public void setReadOnly(boolean readOnly) {   	
    	LibDropDownVersionBean bean = getCurrentBean();
    	
    	boolean isTestVersion = bean != null && view.isTestVersion(bean.dropdownVerInfo());
    	
    	testToProdButton.setVisible(bean != null && !readOnly && isTestVersion && bean.dropdownVersion().doUpdate() && view.getLibDropDownBean().isEnabled());
    	createTestFromProdButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.dropdownVerInfo()) && !view.hasTestVersion());
    	revertProdToTestButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.dropdownVerInfo()) && !view.hasTestVersion());
    	
    	// Show this only if the object is created and is the Test version
    	if ( deleteButton != null ) {
    		deleteButton.setVisible(bean != null && bean.dropdownVersion().doUpdate() && isTestVersion); 
    	}

    	// If the request is to be not-readonly, but this is not the test version, we convert it back to readonly
    	if ( !readOnly && bean != null && !view.isTestVersion(bean.dropdownVerInfo())) {
    		readOnly = true;
    	}
    	super.setReadOnly(readOnly);
    	
    	if ( bean != null )
    		fieldGroup.setReadOnly(readOnly);
    	
		optionInfoList.setReadOnly(readOnly);
		optionInfoButtonLayout.setVisible(!readOnly);
    }
    
    void setupForm(LibDropDownVersionBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("LibDropDownVersionForm.label.id",bean.getId()) );
		
		versionInfo.setValue( vaadinUi.getMsg("LibDropDownVersionForm.label.version",bean.getVersion(),view.getVersionLabel(bean.dropdownVerInfo())) );
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.dropdownVerInfo().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("LibDropDownVersionForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );
		
		if ( bean.dropdownVersion().doInsert() ) {
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibDropDownVersionForm.label.lastUpdated.whenNew") );
			lastUpdatedByInfo.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.dropdownVerInfo().getLastUpdatedByUserId());
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibDropDownVersionForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedByInfo.setIcon(null);
		}

		optionInfoList.setupOptionList(bean.dropdownVersion());
		optionInfoList.setPageLength();
		optionInfoList.setCaption(vaadinUi.getMsg("LibDropDownVersionForm.optionList.label"));
		optionInfoList.setVisible(true);
		optionInfoButtonLayout.setVisible(true);
    }

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	fieldGroup.setBuffered(true);

    	layout = new VerticalLayout();
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setVisible(false);
    	setCompositionRoot(layout);
		
		versionInfo = new Label();
		versionInfo.setContentMode(ContentMode.TEXT);	
		layout.addComponent(versionInfo);
		
		CheckBox cb = new CheckBox(vaadinUi.getMsg("LibDropDownVersionForm.allowMultiSelection.label"));
		cb.setImmediate(false);
		cb.setDescription(vaadinUi.getMsg("LibDropDownVersionForm.allowMultiSelection.tooltip"));
		layout.addComponent(cb);
		fieldGroup.bind(cb, "allowMultiSelection");
		
		optionInfoList = new OptionInfoList();
		optionInfoList.initializeDND();
		layout.addComponent(optionInfoList);
		
		optionInfoButtonLayout = new HorizontalLayout();
		optionInfoButtonLayout.setStyleName("optionButtons");
		optionInfoButtonLayout.setMargin(false);
		optionInfoButtonLayout.setSpacing(true);
		optionInfoListAddButton = new Button(vaadinUi.getMsg("LibDropDownVersionForm.button.addOption.label"), (ClickListener)this);
		optionInfoListAddButton.setStyleName(Reindeer.BUTTON_SMALL);
		optionInfoButtonLayout.addComponent(optionInfoListAddButton);
		optionInfoListRemoveButton = new Button(vaadinUi.getMsg("LibDropDownVersionForm.button.removeOption.label"), (ClickListener)this);
		optionInfoListRemoveButton.setStyleName(Reindeer.BUTTON_SMALL);
		optionInfoButtonLayout.addComponent(optionInfoListRemoveButton);
		layout.addComponent(optionInfoButtonLayout);
		
		lastUpdatedByInfo = new Label();
		lastUpdatedByInfo.setContentMode(ContentMode.TEXT);
		lastUpdatedByInfo.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedByInfo);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo);		

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id);

		buttonLayout = new HorizontalLayout();
		buttonLayout.setStyleName("footer");
		buttonLayout.setSpacing(false);
		buttonLayout.setMargin(false);
		layout.addComponent(buttonLayout);
    	
    	testToProdButton = new Button(vaadinUi.getMsg("LibDropDownVersionForm.button.testToProd.label"), (ClickListener)this);
    	testToProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDropDownVersionForm.button.testToProd.icon")));
    	testToProdButton.setDescription(vaadinUi.getMsg("LibDropDownVersionForm.button.testToProd.tooltip"));
    	buttonLayout.addComponent(testToProdButton);

    	createTestFromProdButton = new Button(vaadinUi.getMsg("LibDropDownVersionForm.button.createTestFromProd.label"), (ClickListener)this);
    	createTestFromProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDropDownVersionForm.button.createTestFromProd.icon")));
    	createTestFromProdButton.setDescription(vaadinUi.getMsg("LibDropDownVersionForm.button.createTestFromProd.tooltip"));
    	buttonLayout.addComponent(createTestFromProdButton);

    	revertProdToTestButton = new Button(vaadinUi.getMsg("LibDropDownVersionForm.button.revertProdToTest.label"), (ClickListener)this);
    	revertProdToTestButton.addStyleName("caution");
    	revertProdToTestButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDropDownVersionForm.button.revertProdToTest.icon")));
    	revertProdToTestButton.setDescription(vaadinUi.getMsg("LibDropDownVersionForm.button.revertProdToTest.tooltip"));
    	buttonLayout.addComponent(revertProdToTestButton);

    	if ( view.hasPermDelete() ) {
    		deleteButton = new Button(vaadinUi.getMsg("LibDropDownVersionForm.button.delete.label"), (ClickListener)this);
    		deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDropDownVersionForm.button.delete.icon")));
    		deleteButton.setDescription(vaadinUi.getMsg("LibDropDownVersionForm.button.delete.tooltip"));
	    	deleteButton.addStyleName("deleteButton");
	    	deleteButton.addStyleName("caution");
	    	buttonLayout.addComponent(deleteButton);
		}
    	
    	setReadOnly(true);   	
    	
    	_logger.debug("initView() - Form created");
	}

    
    private void commitFieldsToTableOnly() {
    	// Only do this if we have something in our table since emptying a table will not have cleared our fields list.
    	try {
        	if ( optionInfoList.getNumOptionInfo() > 0 ) {
        		for( List<Field<?>> fList : optionInfoListFields.values() ) {
        			for( Field<?> f : fList ) {
        				f.commit();
        			}
        		}
        	}
    	} catch( Exception e ) {
    		_logger.error("commitFieldsToTableOnly()",e);
    	}
    }
    
    boolean isValid() {
    	return fieldGroup.isValid();
    }
    
    public void commit() throws FieldGroup.CommitException {
    	fieldGroup.commit();
    	
    	commitFieldsToTableOnly();
    	
    	// Let's save whatever we have now for our options
    	optionInfoList.saveOptionsListToBean(getCurrentBean());
    	optionInfoListChanged = false;
    }

    public void discard() throws Buffered.SourceException {
    	/* 1/3/2011 - Found this wasn't enough. It did reset changed fields, but it didn't restore removed/added rows that have not been saved
		for( Field<?> f : optionListFields ) {
			f.discard();
		}
		*/
    	fieldGroup.discard();
    	LibDropDownVersionBean bean = getCurrentBean();
    	if ( bean != null ) {
    		optionInfoList.setupOptionList(bean.dropdownVersion());
    	}
    	optionInfoListChanged = false;
    }

	@Override
	public String checkDirty() { // OBSOLETE SINCE VERSION FORM HAS NO BUTTONS FOR SAVING, CANCELING
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibDropDownAndVersionsMainView.DropDown.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean isDirty() { 
		if ( fieldGroup.isModified() || optionInfoListChanged ) {
			return true;
		}
		
		if ( optionInfoList != null && optionInfoList.getNumOptionInfo() > 0 ) {
    		for( List<Field<?>> fList : optionInfoListFields.values() ) {
    			for( Field f : fList ) {
    				if ( f.isModified() ) return true;
    			}
    		}
		}

		return false;
	}
	
	
    //*************************  OptionInfoList is our table of option-values ******************************
	public class OptionInfoContainer extends BeanItemContainer<OptionInfo> implements Serializable {
		private static final long serialVersionUID = -5333353861185532977L;

		public OptionInfoContainer() {
			super(OptionInfo.class);
		}

		public void setOptions(DropDownVersion dropdownVersion) {
		   	optionInfoListFields.clear();
			super.removeAllItems();
			
	    	for (DropDownVersionOption option : dropdownVersion.getOptions()) {
	    		OptionInfo oi = new OptionInfo(option.getListOrder(), option.getOption(), option.getValue());
	    	    addItem(oi);
	    	}
		}
	}
	
	
	public class OptionInfo implements java.io.Serializable, Comparable<OptionInfo> {
		private static final long serialVersionUID = -317231033215716624L;

		int order;
		String label;
		String value;
		
		public OptionInfo(int order, String label, String value) {
			this.order = order;
			this.label = label;
			this.value = value;
		}
		
		public int getOrder() {
			return order;
		}
		public void setOrder(int v) {
			order = v;
		}

		public String getLabel() {
			return label;
		}
		public void setLabel(String v) {
			label = v;
		}
		
		public String getValue() {
			return value;
		}
		public void setValue(String v) {
			value = v;
		}
		
		public int hashCode() {
			return order;
		}

		@Override
		public int compareTo(OptionInfo o) {
			if ( o == null )
				return -1;
			return order - o.order;
		}
	}
    
	class OptionInfoList extends Table {
		private static final long serialVersionUID = -6374455915518163883L;

		private OptionInfoContainer optionInfoContainer;
		
		public OptionInfoList() {
			super();
			addStyleName("optionInfoList");
			setWidth(100, Unit.PERCENTAGE);
			
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			optionInfoContainer = new OptionInfoContainer();
			setContainerDataSource(optionInfoContainer);
			setVisibleColumns( (Object[])vaadinUi.getStringArray("LibDropDownVersionForm.optionInfoList.columnProperties") );
			setColumnHeaders( vaadinUi.getStringArray("LibDropDownVersionForm.optionInfoList.columnHeaders") );
			setColumnAlignment("order", Align.CENTER);
			setColumnExpandRatio("order", 0f);
			setColumnExpandRatio("label", 0.25f);
			setColumnExpandRatio("value", 0.75f);
			setSortEnabled(false);
			setNullSelectionAllowed(true);
			setSelectable(true);
			setImmediate(true);
			
			setTableFieldFactory(new DefaultFieldFactory() {
				private static final long serialVersionUID = -6402679188849138525L;

				public Field<?> createField(Container container, Object itemId, Object propertyId, Component uiContext) {
					
			        if ( propertyId.equals("order") ) {
						if ( optionInfoListFields.containsKey(itemId) ) { // If I already have the specified itemId in my map and this is the first property in our table, let's clear it and assume we're building it again
							optionInfoListFields.clear(); 
						}
						return null;
			        }

			        List<Field<?>> fieldList = optionInfoListFields.get(itemId);
			        if ( fieldList == null ) fieldList = new LinkedList<Field<?>>();

			        Field<?> f = super.createField(container, itemId, propertyId, uiContext);
			        f.setBuffered(true);
			        f.setWidth(100,Unit.PERCENTAGE);
			        fieldList.add(f);
			        optionInfoListFields.put(itemId,fieldList);
			        return f;
			    }
			});
		}
		
	    @Override
	    public void setReadOnly(boolean readOnly) {
	        super.setReadOnly(readOnly); 
	    	setDragMode( readOnly ? TableDragMode.NONE : TableDragMode.ROW);
	    	setEditable(!readOnly);
	    }
		
		public void initializeDND() {
			setDropHandler(new DropHandler() {
				private static final long serialVersionUID = -6196788173493117633L;

				public void drop(DragAndDropEvent dropEvent) {
	                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
	                
	                OptionInfo sourceItemId = (OptionInfo)t.getItemId();

	                AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails)dropEvent.getTargetDetails());
	                OptionInfo targetItemId = (OptionInfo)dropData.getItemIdOver();
		                
	                // No move if source and target are the same, or the drop is not around a given target (like above/below the list)
	                if ( sourceItemId == targetItemId || targetItemId == null )
	                	return;
		                
	                commitFieldsToTableOnly();

	                // Let's remove the source of the drag so we can add it back where requested...
		            optionInfoContainer.removeItem(sourceItemId);
		            if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
		            	optionInfoContainer.addItemAfter(targetItemId,sourceItemId);
		            } else {
		            	Object prevItemId = optionInfoContainer.prevItemId(targetItemId);
		            	optionInfoContainer.addItemAfter(prevItemId, sourceItemId);
		            }
		                
		            optionInfoListChanged = true;
	            }

	            public AcceptCriterion getAcceptCriterion() {
	                return new And(SourceIsTarget.get(), new Not(AbstractSelect.VerticalLocationIs.MIDDLE)); // dragging within the table
	            }
	        });
			
		} // initializeDND
		
	    void setupOptionList(DropDownVersion dropdownVersion) {
	    	optionInfoContainer.setOptions(dropdownVersion);
	    }
	    
	    void saveOptionsListToBean(LibDropDownVersionBean bean) {
	    	if ( bean == null ) return;
	    	
	    	LinkedList<DropDownVersionOption> list = new LinkedList<DropDownVersionOption>();
	    	
	    	for( OptionInfo oi : optionInfoContainer.getItemIds() ) {
	    		DropDownVersionOption option = DropDownVersionOption.Manager.createNew(bean.dropdownVersion(), (short)oi.getOrder());
	    		option.setOption(oi.getLabel());
	    		option.setValue(oi.getValue());
	    		list.add(option);
	    	}
	    	
	    	if ( list.size() == 0 ) {
	    		list.add( DropDownVersionOption.Manager.createNew(bean.dropdownVersion(), (short)1));
	    	}
	    	
	    	bean.setOptions(list);
	    	
	    }
	    
	    void addNewOptionInfo() {
	    	int maxOrder = 1;
	    	for( OptionInfo oi : optionInfoContainer.getItemIds() ) {
	    		if ( oi.getOrder() > maxOrder )
	    			maxOrder = oi.getOrder();
	    	}
	    	++maxOrder;
	    	
	    	OptionInfo oi = new OptionInfo(maxOrder, "Label", "Value");
	    	optionInfoContainer.addItem(oi);
	    	optionInfoListChanged = true;
	    }
	    
		public void removeSelectedOptionInfo() {
			List<OptionInfo> optionsToRemove = new LinkedList<OptionInfo>();
			List<OptionInfo> optionInfos = optionInfoContainer.getItemIds();
	    	for( OptionInfo oi : optionInfos ) {
	    		if ( optionInfoList.isSelected(oi) ) {
	    			optionsToRemove.add(oi);
	    		}
	    	}
	    	for( OptionInfo oi : optionsToRemove ) {
	    		optionInfoContainer.removeItem(oi);
	    		optionInfoListChanged = true;
	    	}
		}
		
	    int getNumOptionInfo() {
	    	return optionInfoContainer.size();
	    }
	    
	    void setPageLength() {
			setPageLength( getNumOptionInfo() );    	    
	    }
		
	} // OptionInfoList

}