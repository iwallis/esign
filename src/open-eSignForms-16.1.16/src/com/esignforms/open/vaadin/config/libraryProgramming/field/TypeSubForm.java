// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.field;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.jsp.libdocsgen.DateSelectorCalendar;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.File;
import com.esignforms.open.prog.Library;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.libraryProgramming.field.LibFieldBean.TypeBean;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.FieldSpecTextField;
import com.esignforms.open.vaadin.widget.NativeSelectDropDown;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

/**
 * The TypeSubForm is used like a field in the LibFieldForm form to set type-specific fields.
 * 
 * @author Yozons Inc.
 */
public class TypeSubForm extends Form {
	private static final long serialVersionUID = -626105667486082945L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TypeSubForm.class);
	
	private static final String DATE_INPUT_REGEX = "([-+][0-9]{1,4})|(now)|([12][0-9]{3}-[01][0-9]-[0123][0-9])|(\\$\\{[A-Za-z][A-Za-z0-9_.]*\\})";
	private static final String DATETIME_INPUT_REGEX = "([-+][0-9]{1,4})|(now)|([12][0-9]{3}-[01][0-9]-[0123][0-9] [012][1-9]:[0-5][0-9]:[0-5][0-9] ?[a-zA-Z]*)|(\\$\\{[A-Za-z][A-Za-z0-9_.]*\\})";
	
	final TypeSubForm thisForm;
	final Library library;
	final Form parentForm;
	final Map<EsfName,FieldTemplate> fieldTemplateMap;
	VerticalLayout layout;
	HorizontalLayout line1Layout;
	HorizontalLayout line2Layout;
	HorizontalLayout line3Layout; // introduced with the drop down list having an extraOptions that sets a dynamic drop down name spec
    HorizontalLayout displayEmptyValueLayout;
    int tabIndex;
    
    String targetType;

	public TypeSubForm(final Library library, final Form parentForm, final Map<EsfName,FieldTemplate> fieldTemplateMap, int startTabIndex) {
		this.thisForm = this;
		this.parentForm = parentForm;
		this.library = library;
		this.fieldTemplateMap = fieldTemplateMap;
		setStyleName("TypeSubForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.tabIndex = startTabIndex;

    	setBuffered(parentForm.isBuffered());
    	setImmediate(parentForm.isImmediate());
    	
    	displayEmptyValueLayout = new HorizontalLayout();
    	displayEmptyValueLayout.setMargin(false);
    	displayEmptyValueLayout.setSpacing(true);
    	
    	layout = new VerticalLayout();
    	layout.setMargin(false); // we're in another form
    	layout.setSpacing(false);
    	setLayout(layout);
    	
    	setReadOnly(parentForm.isReadOnly());

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 2225393732365334230L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				
				// Special input formats...
				if ( propertyId.equals("inputFormatSpec") ) {
					if ( FieldTemplate.isTypeDate(targetType) ) {
						NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.TypeSubForm.date.inputFormatSpec.label"));
	                	select.setTabIndex(tabIndex++);
						select.setRequired(true);
	                	select.setNullSelectionAllowed(false);
		                select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.date.inputFormatSpec.tooltip"));
		                select.addValidator(new SelectValidator(select));
		                for( int i=0; i < DateSelectorCalendar.SELECTION_FORMATS.length; ++i ) 
		                {
		                	select.addItem(DateSelectorCalendar.SELECTION_FORMATS[i]);
		                	select.setItemCaption(DateSelectorCalendar.SELECTION_FORMATS[i],DateSelectorCalendar.SELECTION_FORMAT_CAPTIONS[i]);
		                }
		                return select;
					}

					if ( FieldTemplate.isTypeDateTime(targetType) ) {
						NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.TypeSubForm.datetime.inputFormatSpec.label"));
	                	select.setTabIndex(tabIndex++);
						select.setRequired(true);
	                	select.setNullSelectionAllowed(false);
		                select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.datetime.inputFormatSpec.tooltip"));
		                select.addValidator(new SelectValidator(select));
		                for( int i=0; i < DateSelectorCalendar.SELECTION_FORMATS.length; ++i ) 
		                {
		                	select.addItem(DateSelectorCalendar.SELECTION_FORMATS[i]);
		                	select.setItemCaption(DateSelectorCalendar.SELECTION_FORMATS[i],DateSelectorCalendar.SELECTION_FORMAT_CAPTIONS[i]);
		                }
		                return select;
					}

					if ( FieldTemplate.isTypeFileConfirmClick(targetType) ) {
						NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.TypeSubForm.fileconfirmclick.inputFormatSpec.label"));
	                	select.setTabIndex(tabIndex++);
						select.setRequired(true);
	                	select.setNullSelectionAllowed(false);
		                select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.fileconfirmclick.inputFormatSpec.tooltip"));
		                select.addValidator(new SelectValidator(select));
		                
		                // If we're embedded inside a document field form, we also need to allow them to select files defined inside the document version itself
		                if ( parentForm instanceof DocumentFieldForm ) {
		                	DocumentFieldForm docFieldForm = (DocumentFieldForm)parentForm;
		                	for( DocumentVersion.DocumentFile docFile : docFieldForm.duplicateDocumentVersion.getDocumentFileList() ) {
		                		select.addItem(docFile.file.getEsfName().toString());
		                	}
		                }
		                
		                Collection<File> fileList = File.Manager.getAll(library.getId());
		                for( File file : fileList ) {
		                	String name = file.getEsfName().toString();
		                	if ( ! select.containsId(name))
		                		select.addItem(name);
		                }
		                return select;
					}
	                
					if ( FieldTemplate.isTypePhone(targetType) ) {
						NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("LibFieldForm.TypeSubForm.phone.inputFormatSpec.label"),vaadinUi.getEsfapp().getDropDownCountryEsfName());
	                	select.setTabIndex(tabIndex++);
						select.setRequired(true);
	                	select.setNullSelectionAllowed(false);
		                select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.date.inputFormatSpec.tooltip"));
		                select.addValidator(new SelectValidator(select));
		                return select;
					}

					if ( FieldTemplate.isTypeSelection(targetType) ) {
						NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.TypeSubForm.selection.inputFormatSpec.label"));
	                	select.setTabIndex(tabIndex++);
						select.setRequired(true);
	                	select.setNullSelectionAllowed(false);
		                select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.selection.inputFormatSpec.tooltip"));
		                select.addValidator(new SelectValidator(select));
		                
		                // If we're embedded inside a document field form, we also need to allow them to select drop downs defined inside the document version itself
		                if ( parentForm instanceof DocumentFieldForm ) {
		                	DocumentFieldForm docFieldForm = (DocumentFieldForm)parentForm;
		                	for( DocumentVersion.DocumentDropdown docDropdown : docFieldForm.duplicateDocumentVersion.getDocumentDropdownList() ) {
		                		select.addItem(docDropdown.dropdown.getEsfName().toString());
		                	}
		                }
		                
		                List<DropDown> dropdownList = DropDown.Manager.getAllIncludingTemplateLibrary(library.getId());
		                for( DropDown dropdown : dropdownList ) {
		                	String name = dropdown.getEsfName().toString();
		                	if ( ! select.containsId(name))
		                		select.addItem(name);
		                }
		                return select;
					}
	                
					if ( FieldTemplate.isTypeZipCode(targetType) ) {
						NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.TypeSubForm.zipCode.inputFormatSpec.label"));
	                	select.setTabIndex(tabIndex++);
						select.setRequired(true);
	                	select.setNullSelectionAllowed(false);
		                select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.zipCode.inputFormatSpec.tooltip"));
		                select.addValidator(new SelectValidator(select));
	                	select.addItem("5");
	                	select.setItemCaption("5",vaadinUi.getMsg("LibFieldForm.TypeSubForm.zipCode.inputFormatSpec.zipOnly"));
	                	select.addItem("9");
	                	select.setItemCaption("9",vaadinUi.getMsg("LibFieldForm.TypeSubForm.zipCode.inputFormatSpec.zipPlus4Only"));
	                	select.addItem("");
	                	select.setItemCaption("",vaadinUi.getMsg("LibFieldForm.TypeSubForm.zipCode.inputFormatSpec.zipOrZipPlus4"));
		                return select;
					}
				}
				
				// Special output formats
				if ( propertyId.equals("outputFormatSpec") ) {
					// First let's see if it's a standard one that we use
					NativeSelect outputFormatSpecField = createOutputFormatFieldNativeSelect(targetType);
					if ( outputFormatSpecField != null ) {
						outputFormatSpecField.setTabIndex(tabIndex++);
						return outputFormatSpecField;
					}
					
					// Okay, it wasn't, so let's check if this does it!
					if ( FieldTemplate.isTypeRadioButton(targetType) ) {
						NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.TypeSubForm.radioButton.outputFormatSpec.label"));
						select.setRequired(true);
			        	select.setNullSelectionAllowed(false);
			            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.radioButton.outputFormatSpec.tooltip"));
			            select.addValidator(new SelectValidator(select));
			            for( EsfName fieldName : fieldTemplateMap.keySet() ) {
			            	FieldTemplate t = fieldTemplateMap.get(fieldName);
			            	if ( t.isTypeRadioButtonGroup() ) {
			            		select.addItem(t.getEsfName().toPlainString());
			            	}
			            }
			            return select;
					}
				}
				
				// Extra options, when necessary
				if ( propertyId.equals("extraOptions") ) {
					if ( FieldTemplate.isTypeInteger(targetType) ) {
						NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("LibFieldForm.TypeSubForm.integer.extraOptions.label"), vaadinUi.getEsfapp().getDropDownIntegerExtraOptionsEsfName());
	                	select.setTabIndex(tabIndex++);
						select.setRequired(true);
		                select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.integer.extraOptions.tooltip"));
		                select.addValidator(new SelectValidator(select));
		                return select;
					}
				}
				
				if ( propertyId.equals("displayEmptyValueEnabled") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("LibFieldForm.TypeSubForm.displayEmptyValueEnabled.label"));
                	cb.setTabIndex(tabIndex++);
	            	cb.setImmediate(true);
	                cb.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.displayEmptyValueEnabled.tooltip"));
	                cb.addValueChangeListener(new ValueChangeListener(){
						private static final long serialVersionUID = 4078921880320182966L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							setupDisplayEmptyValueField();
						}
	                });
	                return cb;
				} 

				if ( propertyId.equals("autoPost") ) {
	            	CheckBox cb = new CheckBox(vaadinUi.getMsg("LibFieldForm.TypeSubForm.autoPost.label"));
                	cb.setTabIndex(tabIndex++);
	            	cb.setImmediate(false);
	                cb.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.autoPost.tooltip"));
	                return cb;
				} 

				if (propertyId.equals("initialValue")) {
					FieldSpecTextField tf = new FieldSpecTextField();
					tf.setRequired(false);
					tf.setNullRepresentation("");
					if ( FieldTemplate.isTypeDate(targetType) ) {
						tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.date.initialValue.tooltip"));
						tf.addValidator(new RegexpValidator(DATE_INPUT_REGEX, vaadinUi.getMsg("LibFieldForm.TypeSubForm.date.initialValue.tooltip")));
					} else if ( FieldTemplate.isTypeDateTime(targetType) ) {
						tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.datetime.initialValue.tooltip"));
						tf.addValidator(new RegexpValidator(DATETIME_INPUT_REGEX, vaadinUi.getMsg("LibFieldForm.TypeSubForm.datetime.initialValue.tooltip")));
					} else {
						tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.initialValue.tooltip"));
					}
					tf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.initialValue.label"));
					return tf;
				}

				// Now for text type fields
				Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
                field.setTabIndex(tabIndex++);
                
                if (propertyId.equals("outputFormatSpec")) { 
	            	TextField tf = (TextField)field;
	            	tf.setNullRepresentation("");
	            	tf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.outputFormatSpec.label"));
	            	tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.outputFormatSpec.tooltip"));
	            	tf.setRequired(false);
	            } else if (propertyId.equals("displayEmptyValue")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setEnabled(false);
                	tf.setNullRepresentation("");
                	tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.displayEmptyValue.tooltip"));
                	tf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.displayEmptyValue.label"));
                } else if (propertyId.equals("inputFormatSpec")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	if ( FieldTemplate.isTypeCheckbox(targetType) ) {
                    	tf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.checkbox.inputFormatSpec.label"));
                    	tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.checkbox.inputFormatSpec.tooltip"));          		
                	} else if ( FieldTemplate.isTypeFile(targetType) ) {
                    	tf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.file.inputFormatSpec.label"));
                    	tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.file.inputFormatSpec.tooltip"));
                    	tf.setRequired(false);
                        tf.setWidth(4,Unit.EM);
                    	tf.setColumns(3);
                    	tf.setMaxLength(3);
                    	tf.addValidator(new RegexpValidator("[1-9][0-9]{0,2}", vaadinUi.getMsg("tooltip.invalid.integer.range","1-999")));
                	} else if ( FieldTemplate.isTypeGeneral(targetType) ) {
                    	tf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.general.inputFormatSpec.label"));
                    	tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.general.inputFormatSpec.tooltip"));  
                	} else if ( FieldTemplate.isTypeRadioButton(targetType) ) {
                    	tf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.radioButton.inputFormatSpec.label"));
                    	tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.radioButton.inputFormatSpec.tooltip"));  
                	} else if ( FieldTemplate.isTypeRichTextarea(targetType) ) {
                    	tf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.richTextarea.inputFormatSpec.label"));
                    	tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.richTextarea.inputFormatSpec.tooltip"));
                    	tf.setRequired(true);
                        tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                        tf.setWidth(4,Unit.EM);
                    	tf.setColumns(3);
                    	tf.setMaxLength(3);
                    	tf.addValidator(new RegexpValidator("[1-9][0-9]{0,2}", vaadinUi.getMsg("tooltip.invalid.integer.range","1-999")));
                	} else if ( FieldTemplate.isTypeSignature(targetType) ) {
                    	FieldSpecTextField fstf = new FieldSpecTextField();
                    	fstf.setWidth(100, Unit.PERCENTAGE);
                    	fstf.setTabIndex(tabIndex++);
                    	fstf.setRequired(false);
                    	fstf.setNullRepresentation("");
                    	fstf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.signature.inputFormatSpec.label"));
                    	fstf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.signature.inputFormatSpec.tooltip"));
                    	return fstf;
                	} else if ( FieldTemplate.isTypeTextarea(targetType) ) {
                    	tf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.textarea.inputFormatSpec.label"));
                    	tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.textarea.inputFormatSpec.tooltip"));
                    	tf.setRequired(true);
                        tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                        tf.setWidth(4,Unit.EM);
                    	tf.setColumns(3);
                    	tf.setMaxLength(3);
                    	tf.addValidator(new RegexpValidator("[1-9][0-9]{0,2}", vaadinUi.getMsg("tooltip.invalid.integer.range","1-999")));
                	} else {
                    	tf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.inputFormatSpec.label"));
                    	tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.inputFormatSpec.tooltip"));
                	}
                } else if (propertyId.equals("tooltip")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.tooltip.tooltip"));
                } else if (propertyId.equals("inputPrompt")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.inputPrompt.label"));
                	tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.inputPrompt.tooltip"));
                } else if ( propertyId.equals("extraOptions") ) {
					if ( FieldTemplate.isTypeSelection(targetType) ) {
	                	TextField tf = (TextField)field;
	                	tf.setRequired(false);
	                	tf.setNullRepresentation("");
	                	tf.setCaption(vaadinUi.getMsg("LibFieldForm.TypeSubForm.selection.extraOptions.label"));
	                	tf.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.selection.extraOptions.tooltip"));
					}
                }
                
                return field;
			}
    	 });
    	
    	_logger.debug("Form created");
    }
	
	public static NativeSelect createOutputFormatFieldNativeSelect(String fieldTemplateType)
	{
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		if ( FieldTemplate.isTypeCreditCard(fieldTemplateType) ) {
			NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.TypeSubForm.creditCard.outputFormatSpec.label"));
			select.setRequired(true);
        	select.setNullSelectionAllowed(false);
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.creditCard.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
        	select.addItem("normal");
        	select.setItemCaption("normal",vaadinUi.getMsg("LibFieldForm.TypeSubForm.creditCard.outputFormatSpec.normal"));
        	select.addItem("masked");
        	select.setItemCaption("masked",vaadinUi.getMsg("LibFieldForm.TypeSubForm.creditCard.outputFormatSpec.masked"));
        	select.addItem("last4");
        	select.setItemCaption("last4",vaadinUi.getMsg("LibFieldForm.TypeSubForm.creditCard.outputFormatSpec.last4"));
        	select.addItem("digits");
        	select.setItemCaption("digits",vaadinUi.getMsg("LibFieldForm.TypeSubForm.creditCard.outputFormatSpec.digits"));
            return select;
		}

		if ( FieldTemplate.isTypeDate(fieldTemplateType) ) {
			NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("LibFieldForm.TypeSubForm.date.outputFormatSpec.label"), vaadinUi.getEsfapp().getDropDownDateFormatEsfName());
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.date.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
            return select;
		}
                        
		if ( FieldTemplate.isTypeDateTime(fieldTemplateType) ) {
			NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("LibFieldForm.TypeSubForm.datetime.outputFormatSpec.label"), vaadinUi.getEsfapp().getDropDownDateTimeFormatEsfName());
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.datetime.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
            return select;
		}
                        
		if ( FieldTemplate.isTypeDecimal(fieldTemplateType) ) {
			NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("LibFieldForm.TypeSubForm.decimal.outputFormatSpec.label"), vaadinUi.getEsfapp().getDropDownDecimalFormatEsfName());
			select.setRequired(true);
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.decimal.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
            return select;
		}

		if ( FieldTemplate.isTypeFile(fieldTemplateType) ) {
			NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.TypeSubForm.file.outputFormatSpec.label"));
			select.setRequired(true);
        	select.setNullSelectionAllowed(false);
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.file.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
        	select.addItem("standard");
        	select.setItemCaption("standard",vaadinUi.getMsg("LibFieldForm.TypeSubForm.file.outputFormatSpec.standard"));
        	select.addItem("image200");
        	select.setItemCaption("image200",vaadinUi.getMsg("LibFieldForm.TypeSubForm.file.outputFormatSpec.image200"));
        	select.addItem("image400");
        	select.setItemCaption("image400",vaadinUi.getMsg("LibFieldForm.TypeSubForm.file.outputFormatSpec.image400"));
        	select.addItem("image600");
        	select.setItemCaption("image600",vaadinUi.getMsg("LibFieldForm.TypeSubForm.file.outputFormatSpec.image600"));
        	select.addItem("image800");
        	select.setItemCaption("image800",vaadinUi.getMsg("LibFieldForm.TypeSubForm.file.outputFormatSpec.image800"));
        	select.addItem("imageOnly200");
        	select.setItemCaption("imageOnly200",vaadinUi.getMsg("LibFieldForm.TypeSubForm.file.outputFormatSpec.imageOnly200"));
        	select.addItem("imageOnly400");
        	select.setItemCaption("imageOnly400",vaadinUi.getMsg("LibFieldForm.TypeSubForm.file.outputFormatSpec.imageOnly400"));
        	select.addItem("imageOnly600");
        	select.setItemCaption("imageOnly600",vaadinUi.getMsg("LibFieldForm.TypeSubForm.file.outputFormatSpec.imageOnly600"));
        	select.addItem("imageOnly800");
        	select.setItemCaption("imageOnly800",vaadinUi.getMsg("LibFieldForm.TypeSubForm.file.outputFormatSpec.imageOnly800"));
            return select;
		}

		if ( FieldTemplate.isTypeGeneral(fieldTemplateType) ) {
			NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.TypeSubForm.general.outputFormatSpec.label"));
			select.setRequired(true);
        	select.setNullSelectionAllowed(false);
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.general.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
        	select.addItem("standard");
        	select.setItemCaption("standard",vaadinUi.getMsg("LibFieldForm.TypeSubForm.general.outputFormatSpec.standard"));
        	select.addItem("fullmask");
        	select.setItemCaption("fullmask",vaadinUi.getMsg("LibFieldForm.TypeSubForm.general.outputFormatSpec.fullmask"));
        	select.addItem("left4mask");
        	select.setItemCaption("left4mask",vaadinUi.getMsg("LibFieldForm.TypeSubForm.general.outputFormatSpec.left4mask"));
        	select.addItem("right4mask");
        	select.setItemCaption("right4mask",vaadinUi.getMsg("LibFieldForm.TypeSubForm.general.outputFormatSpec.right4mask"));
            return select;
		}

		if ( FieldTemplate.isTypeInteger(fieldTemplateType) ) {
			NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("LibFieldForm.TypeSubForm.integer.outputFormatSpec.label"), vaadinUi.getEsfapp().getDropDownIntegerFormatEsfName());
			select.setRequired(true);
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.integer.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
            return select;
		}

		if ( FieldTemplate.isTypeMoney(fieldTemplateType) ) {
			NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("LibFieldForm.TypeSubForm.money.outputFormatSpec.label"), vaadinUi.getEsfapp().getDropDownMoneyFormatEsfName());
			select.setRequired(true);
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.money.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
            return select;
		}

		if ( FieldTemplate.isTypePhone(fieldTemplateType) ) {
			NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.TypeSubForm.phone.outputFormatSpec.label"));
			select.setRequired(true);
        	select.setNullSelectionAllowed(false);
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.phone.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
        	select.addItem("standard");
        	select.setItemCaption("standard",vaadinUi.getMsg("LibFieldForm.TypeSubForm.phone.outputFormatSpec.standard"));
        	select.addItem("international");
        	select.setItemCaption("international",vaadinUi.getMsg("LibFieldForm.TypeSubForm.phone.outputFormatSpec.international"));
        	select.addItem("e164");
        	select.setItemCaption("e164",vaadinUi.getMsg("LibFieldForm.TypeSubForm.phone.outputFormatSpec.e164"));
        	select.addItem("rfc3966");
        	select.setItemCaption("rfc3966",vaadinUi.getMsg("LibFieldForm.TypeSubForm.phone.outputFormatSpec.rfc3966"));
        	select.addItem("digits");
        	select.setItemCaption("digits",vaadinUi.getMsg("LibFieldForm.TypeSubForm.phone.outputFormatSpec.digits"));
        	select.addItem("input");
        	select.setItemCaption("input",vaadinUi.getMsg("LibFieldForm.TypeSubForm.phone.outputFormatSpec.input"));
            return select;
		}

		if ( FieldTemplate.isTypeSelection(fieldTemplateType) ) {
			NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.TypeSubForm.selection.outputFormatSpec.label"));
			select.setRequired(true);
        	select.setNullSelectionAllowed(false);
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.selection.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
            select.addItem("value");
            select.setItemCaption("value", vaadinUi.getMsg("LibFieldForm.TypeSubForm.selection.outputFormatSpec.value"));
            select.addItem("option");
            select.setItemCaption("option", vaadinUi.getMsg("LibFieldForm.TypeSubForm.selection.outputFormatSpec.option"));
            return select;
		}
        
		if ( FieldTemplate.isTypeSignDate(fieldTemplateType) ) {
			NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("LibFieldForm.TypeSubForm.signatureDate.outputFormatSpec.label"), vaadinUi.getEsfapp().getDropDownDateFormatEsfName());
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.signatureDate.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
            return select;
		}
        
		if ( FieldTemplate.isTypeSsnEin(fieldTemplateType) ) {
			NativeSelect select = new NativeSelect(vaadinUi.getMsg("LibFieldForm.TypeSubForm.ssnein.outputFormatSpec.label"));
			select.setRequired(true);
        	select.setNullSelectionAllowed(false);
            select.setDescription(vaadinUi.getMsg("LibFieldForm.TypeSubForm.ssnein.outputFormatSpec.tooltip"));
            select.addValidator(new SelectValidator(select));
        	select.addItem("normal");
        	select.setItemCaption("normal",vaadinUi.getMsg("LibFieldForm.TypeSubForm.ssnein.outputFormatSpec.normal"));
        	select.addItem("normalEIN");
        	select.setItemCaption("normalEIN",vaadinUi.getMsg("LibFieldForm.TypeSubForm.ssnein.outputFormatSpec.normalEIN"));
        	select.addItem("masked");
        	select.setItemCaption("masked",vaadinUi.getMsg("LibFieldForm.TypeSubForm.ssnein.outputFormatSpec.masked"));
        	select.addItem("maskedEIN");
        	select.setItemCaption("maskedEIN",vaadinUi.getMsg("LibFieldForm.TypeSubForm.ssnein.outputFormatSpec.maskedEIN"));
        	select.addItem("last4");
        	select.setItemCaption("last4",vaadinUi.getMsg("LibFieldForm.TypeSubForm.ssnein.outputFormatSpec.last4"));
        	select.addItem("digits");
        	select.setItemCaption("digits",vaadinUi.getMsg("LibFieldForm.TypeSubForm.ssnein.outputFormatSpec.digits"));
            return select;
		}
		
		return null;
	}
    
	TypeBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public TypeBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<TypeBean> bi = (BeanItem<TypeBean>)dataSource;
		return bi.getBean();
    }
    
	void setupDisplayEmptyValueField() {
		CheckBox displayEmptyValueEnabledField = (CheckBox)thisForm.getField("displayEmptyValueEnabled");
		if ( displayEmptyValueEnabledField != null ) {
			boolean isChecked = (Boolean)displayEmptyValueEnabledField.getValue();
			Field<?> displayEmptyValueField = thisForm.getField("displayEmptyValue");
			if ( displayEmptyValueField != null ) {
				displayEmptyValueField.setEnabled(isChecked);
			}
		}
	}

    
	@Override
    public void setItemDataSource(Item newDataSource) {
    	if (newDataSource != null) {
    		targetType = (String)parentForm.getField("type").getValue();
    		
    		List<String> orderedProperties;
    		
    		if ( FieldTemplate.isTypeCheckbox(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","tooltip","inputFormatSpec"); // input format is the checkbox's value when checked
    		} else if ( FieldTemplate.isTypeCreditCard(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputPrompt","outputFormatSpec"); // output format is either credit card normal, masked, last-4 digits or just digits.
    		} else if ( FieldTemplate.isTypeDate(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputPrompt","inputFormatSpec","outputFormatSpec"); // input format is a select box of supported calendar popup formats; output format is the date format to use
    		} else if ( FieldTemplate.isTypeDateTime(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputPrompt","inputFormatSpec","outputFormatSpec"); // input format is a select box of supported calendar popup formats with drop downs for the HH:MM:SS TIMEZONE fields; output format is the date format to use
    		} else if ( FieldTemplate.isTypeDecimal(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputPrompt","outputFormatSpec"); // output format is either digits, or with thousand separators
    		} else if ( FieldTemplate.isTypeEmailAddress(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputPrompt"); 
    		} else if ( FieldTemplate.isTypeFile(targetType) ) {
    			orderedProperties = Arrays.asList("tooltip","inputFormatSpec","outputFormatSpec"); // input format can limit number of files to upload; output format specifies whether to show images or not
    		} else if ( FieldTemplate.isTypeFileConfirmClick(targetType) ) {
    			orderedProperties = Arrays.asList("tooltip","inputFormatSpec"); 
    		} else if ( FieldTemplate.isTypeGeneral(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputPrompt","inputFormatSpec","outputFormatSpec"); // input format is a regex; output format allows for masking
    		} else if ( FieldTemplate.isTypeInteger(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputPrompt","outputFormatSpec","extraOptions"); // output format is either digits, or with thousand separators
    		} else if ( FieldTemplate.isTypeMoney(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputPrompt","outputFormatSpec"); // output format is either digits, or with thousand separators
    		} else if ( FieldTemplate.isTypePhone(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputPrompt","inputFormatSpec","outputFormatSpec"); // input format is a dropdown for country codes to assume as the default
    		} else if ( FieldTemplate.isTypeRadioButton(targetType) ) {
    			orderedProperties = Arrays.asList("tooltip","inputFormatSpec","outputFormatSpec"); // input format is the radio button's default value; output format is the radio button group's field name
    		} else if ( FieldTemplate.isTypeRadioButtonGroup(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue"); // initial value to set the radio button group to
    		} else if ( FieldTemplate.isTypeRichTextarea(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputFormatSpec"); // input format is the number of rows to show
    		} else if ( FieldTemplate.isTypeSelection(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","autoPost","tooltip","inputFormatSpec","outputFormatSpec","extraOptions"); // input format is a dropdown list of drop downs; output format determines if value or option is shown on display
    		} else if ( FieldTemplate.isTypeSignature(targetType) ) {
    			orderedProperties = Arrays.asList("inputFormatSpec","tooltip","inputPrompt"); // input format is field expression that must match for signing
    		} else if ( FieldTemplate.isTypeSignDate(targetType) ) {
    			orderedProperties = Arrays.asList("outputFormatSpec","tooltip"); // output format is the date format to use
    		} else if ( FieldTemplate.isTypeSsnEin(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputPrompt","outputFormatSpec"); // output format is either SSN/EIN normal, masked, last-4 digits or just digits
    		} else if ( FieldTemplate.isTypeTextarea(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputPrompt","inputFormatSpec"); // input format is the number of rows to show
    		} else if ( FieldTemplate.isTypeZipCode(targetType) ) {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputPrompt","inputFormatSpec"); // input format is a dropdown for the Zip Only; Zip or Zip+4; or Zip+4 only
    		} else {
    			orderedProperties = Arrays.asList("initialValue","displayEmptyValueEnabled","displayEmptyValue","tooltip","inputPrompt","inputFormatSpec","outputFormatSpec"); 
    		}
    		
    		line1Layout = line2Layout = line3Layout = null;
    		layout.removeAllComponents();
        	line1Layout = new HorizontalLayout();
        	line1Layout.setWidth(100, Unit.PERCENTAGE);
        	line1Layout.setMargin(false);
        	line1Layout.setSpacing(true);
        	layout.addComponent(line1Layout);
    		
    		if ( orderedProperties.size() > 3 ) {
            	line2Layout = new HorizontalLayout();
            	line2Layout.setWidth(100, Unit.PERCENTAGE);
            	line2Layout.setMargin(false);
            	line2Layout.setSpacing(true);
            	layout.addComponent(line2Layout);
    		}
    		
    		if ( FieldTemplate.isTypeSelection(targetType) ) {
            	line3Layout = new HorizontalLayout();
            	line3Layout.setWidth(100, Unit.PERCENTAGE);
            	line3Layout.setMargin(false);
            	line3Layout.setSpacing(true);
            	layout.addComponent(line3Layout);
    		}

    		super.setItemDataSource(newDataSource,orderedProperties);

    		setupDisplayEmptyValueField();
    	} else {
    		super.setItemDataSource(null);
    		setReadOnly(true);
    	}
    }
	
	
    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
    	if ( "initialValue".equals(propertyId) ) {
    		line1Layout.addComponent(field);
    	} else if ( "displayEmptyValueEnabled".equals(propertyId) ) {
        	displayEmptyValueLayout.removeAllComponents();
        	displayEmptyValueLayout.addComponent(field);
        	displayEmptyValueLayout.setComponentAlignment(field, Alignment.BOTTOM_CENTER);
        	displayEmptyValueLayout.setExpandRatio(field, 1.0f);
        } else if ( "displayEmptyValue".equals(propertyId) ) {
        	displayEmptyValueLayout.addComponent(field);
        	displayEmptyValueLayout.setExpandRatio(field, 2.0f);
        	line1Layout.addComponent(displayEmptyValueLayout);
        } else if ( "autoPost".equals(propertyId) ) {
    		line1Layout.addComponent(field);
    		line1Layout.setComponentAlignment(field, Alignment.BOTTOM_CENTER);
        } else if ( "tooltip".equals(propertyId) ) {
    		line1Layout.addComponent(field);
        } else if ( "inputPrompt".equals(propertyId) ) {
    		line1Layout.addComponent(field);
        } else {
        	if ( "extraOptions".equals(propertyId) && line3Layout != null ) {
        		line3Layout.addComponent(field);
        	} else {
            	if ( line2Layout != null )
            		line2Layout.addComponent(field);
            	else
            		line1Layout.addComponent(field);
        	}
        }
    }
}