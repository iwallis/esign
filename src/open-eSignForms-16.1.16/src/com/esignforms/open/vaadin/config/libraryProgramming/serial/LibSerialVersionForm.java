// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.serial;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;

import org.vaadin.dialogs.ConfirmDialog;

import com.esignforms.open.config.Literals;
import com.esignforms.open.prog.Serial;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.TextField;

/**
 * The LibSerialVersionForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class LibSerialVersionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -7006127161463720176L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibSerialVersionForm.class);
	
	final LibSerialVersionForm thisForm;
	final LibSerialAndVersionsMainView view;
	LibSerialVersionBeanContainer container;
	GridLayout layout;
	
	Label id;
	Label createdByInfo;
	Label lastUpdatedByInfo;
	Label versionInfo;
	Label nextFormattedSerialNumbers;
	
	Button testToProdButton;
	Button createTestFromProdButton;
	Button revertProdToTestButton;
	Button deleteButton;

    public LibSerialVersionForm(final LibSerialAndVersionsMainView view, LibSerialVersionBeanContainer container) {
    	setStyleName("LibSerialVersionForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.view = view;
    	this.container = container;
    	this.thisForm = this;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);

    	layout = new GridLayout(1,6);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
		
		versionInfo = new Label();
		versionInfo.setContentMode(ContentMode.TEXT);	
		layout.addComponent(versionInfo,0,0);

		nextFormattedSerialNumbers = new Label();
		nextFormattedSerialNumbers.setContentMode(ContentMode.TEXT);
		layout.addComponent(nextFormattedSerialNumbers,0,2);

		lastUpdatedByInfo = new Label();
		lastUpdatedByInfo.setContentMode(ContentMode.TEXT);
		lastUpdatedByInfo.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedByInfo,0,3);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,4);		

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id,0,5);
		
		HorizontalLayout footer = new HorizontalLayout();
		footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);
    	
    	testToProdButton = new Button(vaadinUi.getMsg("LibSerialVersionForm.button.testToProd.label"), (ClickListener)this);
    	testToProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibSerialVersionForm.button.testToProd.icon")));
    	testToProdButton.setDescription(vaadinUi.getMsg("LibSerialVersionForm.button.testToProd.tooltip"));
    	footer.addComponent(testToProdButton);

    	createTestFromProdButton = new Button(vaadinUi.getMsg("LibSerialVersionForm.button.createTestFromProd.label"), (ClickListener)this);
    	createTestFromProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibSerialVersionForm.button.createTestFromProd.icon")));
    	createTestFromProdButton.setDescription(vaadinUi.getMsg("LibSerialVersionForm.button.createTestFromProd.tooltip"));
    	footer.addComponent(createTestFromProdButton);

    	revertProdToTestButton = new Button(vaadinUi.getMsg("LibSerialVersionForm.button.revertProdToTest.label"), (ClickListener)this);
    	revertProdToTestButton.addStyleName("caution");
    	revertProdToTestButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibSerialVersionForm.button.revertProdToTest.icon")));
    	revertProdToTestButton.setDescription(vaadinUi.getMsg("LibSerialVersionForm.button.revertProdToTest.tooltip"));
    	footer.addComponent(revertProdToTestButton);

    	if ( view.hasPermDelete() ) {
    		deleteButton = new Button(vaadinUi.getMsg("LibSerialVersionForm.button.delete.label"), (ClickListener)this);
    		deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibSerialVersionForm.button.delete.icon")));
    		deleteButton.setDescription(vaadinUi.getMsg("LibSerialVersionForm.button.delete.tooltip"));
	    	deleteButton.addStyleName("deleteButton");
	    	deleteButton.addStyleName("caution");
	    	footer.addComponent(deleteButton);
		}
    	
    	setFooter(footer);
    	setReadOnly(true);   	

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 5618293905356025947L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

                Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
                
                if (propertyId.equals("decimalFormat")) {
                	TextField tf = (TextField)field;
                	tf.setCaption(vaadinUi.getMsg("LibSerialVersionForm.decimalFormat.label"));
                	tf.setDescription(vaadinUi.getMsg("LibSerialVersionForm.decimalFormat.tooltip"));
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setMaxLength(Literals.SERIAL_FORMAT_MAX_LENGTH);
                	// Allow an optional text prefix, 0 or more '#' followed by 0 or more '0' followed by an optional text suffix.
                	tf.addValidator(new RegexpValidator("[A-Za-z1-9]*#*0*[A-Za-z1-9]*", vaadinUi.getMsg("LibSerialVersionForm.decimalFormat.tooltip")));
                }
                
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
    }
    
    @Override
	public void buttonClick(ClickEvent event) {
    	LibSerialVersionBean currBean = getCurrentBean();
    	if ( currBean == null ) return;
    	
    	Button source = event.getButton();
    	if ( source == testToProdButton ) {
    		view.promoteTestToProductionVersion();
    	} else if ( source == createTestFromProdButton ) {
    		view.createNextVersion();
    	} else if ( source == revertProdToTestButton ) {
    		view.revertProductionVersionBackToTest();
    	} else if ( source == deleteButton ) {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	ConfirmDialog.show(UI.getCurrent(), 
	    			vaadinUi.getMsg("LibSerialVersionForm.button.delete.ConfirmDialog.caption"),
	    			vaadinUi.getMsg("LibSerialVersionForm.button.delete.ConfirmDialog.message"),
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
	    	        new ConfirmDialog.Listener() {
						private static final long serialVersionUID = 7652492371349603264L;

						public void onClose(ConfirmDialog dialog) {
	    	                if (dialog.isConfirmed()) {
	    	                    view.delete();
	    	                }
	    	            }
	    	        });
    	}
     }
	
    LibSerialVersionBean save(Connection con) throws SQLException {
    	LibSerialVersionBean currBean = getCurrentBean();
    	if ( currBean != null ) {
    		if ( currBean.save(con,null) ) {
    			return currBean;
    		}
    	}
    	return null;
    }
    	
    public String getCurrentBeanName() {
    	LibSerialVersionBean currBean = getCurrentBean();
		return currBean == null ? "(None)" : view.getEsfNameWithVersion();
	}
    
	LibSerialVersionBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public LibSerialVersionBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibSerialVersionBean> bi = (BeanItem<LibSerialVersionBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached stuff
		resetCaches();
		
    	if (newDataSource != null) {
    		LibSerialVersionBean bean = getBean(newDataSource);
    		
    		super.setItemDataSource(newDataSource, Arrays.asList("decimalFormat"));
    		
    		setupForm(bean);
    		
    		setReadOnly( ! view.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	

    @Override
    public void setReadOnly(boolean readOnly) {   	
    	LibSerialVersionBean bean = getCurrentBean();
    	
    	boolean isTestVersion = bean != null && view.isTestVersion(bean.serialVerInfo());
    	
    	testToProdButton.setVisible(bean != null && !readOnly && isTestVersion && bean.serialVersion().doUpdate() && view.getLibSerialBean().isEnabled());
    	createTestFromProdButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.serialVerInfo()) && !view.hasTestVersion());
    	revertProdToTestButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.serialVerInfo()) && !view.hasTestVersion());
    	
    	// Show this only if the object is created and is the Test version
    	if ( deleteButton != null ) {
    		deleteButton.setVisible(bean != null && bean.serialVersion().doUpdate() && isTestVersion); 
    	}

    	// If the request is to be not-readonly, but this is not the test version, we convert it back to readonly
    	if ( !readOnly && bean != null && !view.isTestVersion(bean.serialVerInfo())) {
    		readOnly = true;
    	}
    	super.setReadOnly(readOnly);
    }
    
    void setupForm(LibSerialVersionBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	Serial serial = bean.serialVersion().getSerial();
    	String nextProd = serial.getNextProductionSerial().format(bean.getDecimalFormat());
    	String nextTest = serial.getNextTestSerial().format(bean.getDecimalFormat());
    	nextFormattedSerialNumbers.setValue( vaadinUi.getMsg("LibSerialVersionForm.label.nextFormattedSerialNumbers",nextProd,nextTest) );

		id.setValue( vaadinUi.getMsg("LibSerialVersionForm.label.id",bean.getId()) );
		
		versionInfo.setValue( vaadinUi.getMsg("LibSerialVersionForm.label.version",bean.getVersion(),view.getVersionLabel(bean.serialVerInfo())) );
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.serialVerInfo().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("LibSerialVersionForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );
		
		if ( bean.serialVersion().doInsert() ) {
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibSerialVersionForm.label.lastUpdated.whenNew") );
			lastUpdatedByInfo.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.serialVerInfo().getLastUpdatedByUserId());
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibSerialVersionForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedByInfo.setIcon(null);
		}
    }

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
    	if (propertyId.equals("decimalFormat") ) {
			layout.addComponent(field,0,1);
    	}
    }


	@Override
	public String checkDirty() { // OBSOLETE SINCE VERSION FORM HAS NO BUTTONS FOR SAVING, CANCELING
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibSerialAndVersionsMainView.Serial.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
	
	@Override
	public void detach() {
		super.detach();
	}
}