// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.group;

import java.io.Serializable;

import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;

public class MemberUserBean implements Serializable, Comparable<MemberUserBean> {
	private static final long serialVersionUID = 8329436307154434559L;

	private User user;
	
	
	public MemberUserBean(User user) {
		this.user = user;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o !=null && o instanceof MemberUserBean )
			return user.equals(((MemberUserBean)o).user);
		return false;
	}
	@Override
    public int hashCode() {
    	return user.hashCode();
    }
	public int compareTo(MemberUserBean g) {
		return user.compareTo(g.user);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	protected User loggedInUser() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getUser();
	}
	public User user() {
		return user;
	}

	// Now for the JavaBeans methods
	public String getPersonalName() {
		return user.getPersonalName();
	}
	
	public String getFamilyName() {
		return user.getFamilyName();
	}
	
	public String getEmail() {
		return user.getEmail();
	}
	
	public String getStatus() {
		return user.getStatus();
	}
	
}