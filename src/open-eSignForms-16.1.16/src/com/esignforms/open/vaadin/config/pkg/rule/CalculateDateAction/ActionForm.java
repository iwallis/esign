// Copyright (C) 2014-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.CalculateDateAction;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.runtime.action.CalculateDateAction;
import com.esignforms.open.runtime.action.CalculateDateAction.Spec;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.NativeSelectDropDown;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author Yozons Inc.
 */
public class ActionForm extends CustomComponent implements EsfView, ClickListener {
	private static final long serialVersionUID = -5287035118195811378L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ActionForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;
	protected Label offsetByLabel;

	VerticalLayout layout;
	GridLayout formLayout;
	FieldGroup fieldGroup;
	
	final PackageVersion duplicatedPackageVersion;
	final DocumentVersion duplicatedDocumentVersion;
	final CalculateDateAction duplicatedAction;
	final ActionView view;
	final ActionContainer container;
	
	List<EsfUUID> allDocumentIds;
	List<EsfName> allDocumentNames;
	
	Spec newSpec; // when set, we're working on a new spec.
	
	public ActionForm(ActionView view, final ActionContainer container, PackageVersion duplicatedPackageVersionParam, CalculateDateAction duplicatedActionParam) {
		this.view = view;
		this.container = container;
		this.duplicatedPackageVersion = duplicatedPackageVersionParam;
		this.duplicatedDocumentVersion = null;
		this.duplicatedAction = duplicatedActionParam;
		allDocumentIds = new LinkedList<EsfUUID>(duplicatedPackageVersion.getDocumentIdList());
		setupForm();
	}
	
	public ActionForm(ActionView view, final ActionContainer container, DocumentVersion duplicatedDocumentVersionParam, CalculateDateAction duplicatedActionParam)	{
		this.view = view;
		this.container = container;
		this.duplicatedPackageVersion = null;
		this.duplicatedDocumentVersion = duplicatedDocumentVersionParam;
		this.duplicatedAction = duplicatedActionParam;
		allDocumentIds = new LinkedList<EsfUUID>();
		allDocumentIds.add(duplicatedDocumentVersion.getDocumentId());
		setupForm();
	}
	
	private void setupForm() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		setStyleName("CalculateDateActionForm");
		this.fieldGroup = new FieldGroup();

		allDocumentNames = new LinkedList<EsfName>();
    	
    	for( EsfUUID docId : allDocumentIds ) {
    		Document doc = Document.Manager.getById(docId);
    		if ( doc == null ) {
    			_logger.warn("ActionForm() - Could not find document with id: " + docId);
    			allDocumentNames.add(docId.toEsfName());
    		} else {
    			allDocumentNames.add(doc.getEsfName());
    		}
    	}
	
    	// Setup layout
    	layout = new VerticalLayout();
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setCompositionRoot(layout);

    	// Setup form layout
    	formLayout = new GridLayout(5,2);
    	formLayout.setMargin(false);
    	formLayout.setSpacing(true);
    	formLayout.setVisible(false);
    	formLayout.setWidth(100, Unit.PERCENTAGE);
    	layout.addComponent(formLayout);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	fieldGroup.setBuffered(true);
    	
    	NativeSelect targetDocumentId = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.targetDocumentId.label"));
    	targetDocumentId.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.targetDocumentId.tooltip"));
    	targetDocumentId.setNullSelectionAllowed(false);
    	targetDocumentId.setRequired(true);
    	targetDocumentId.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	targetDocumentId.addValidator(new SelectValidator(targetDocumentId));
    	targetDocumentId.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
    	for( EsfUUID docId : allDocumentIds ) {
    		targetDocumentId.addItem(docId);
    		targetDocumentId.setItemCaption(docId, nameIter.next().toString());
    	}
    	targetDocumentId.setImmediate(true);
    	targetDocumentId.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = 326905197498636417L;

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
				if ( newDocId != null && ! newDocId.isNull() ) {
					setupAllFieldNames("targetField",newDocId);
				}
			}
    	});
    	formLayout.addComponent(targetDocumentId, 0, 0);
    	fieldGroup.bind(targetDocumentId, "targetDocumentId");
    	
    	NativeSelect targetField = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.targetField.label"));
    	targetField.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.targetField.tooltip"));
    	targetField.setNullSelectionAllowed(false);
    	targetField.setRequired(true);
    	targetField.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	targetField.addValidator(new SelectValidator(targetField));
    	formLayout.addComponent(targetField, 1, 0);
    	fieldGroup.bind(targetField, "targetField");
    	
    	NativeSelect leftSideDocumentId = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.leftSideDocumentId.label"));
    	leftSideDocumentId.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.leftSideDocumentId.tooltip"));
    	leftSideDocumentId.setNullSelectionAllowed(false);
    	leftSideDocumentId.setRequired(true);
    	leftSideDocumentId.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	leftSideDocumentId.addValidator(new SelectValidator(leftSideDocumentId));
    	leftSideDocumentId.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	nameIter = allDocumentNames.listIterator();
    	for( EsfUUID docId : allDocumentIds ) {
    		leftSideDocumentId.addItem(docId);
    		leftSideDocumentId.setItemCaption(docId, nameIter.next().toString());
    	}
    	leftSideDocumentId.setImmediate(true);
    	leftSideDocumentId.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = 4749365377673959925L;

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
				if ( newDocId != null && ! newDocId.isNull() ) {
					setupAllFieldNames("leftSideField",newDocId);
				}
			}
    	});
    	formLayout.addComponent(leftSideDocumentId, 0, 1);
    	fieldGroup.bind(leftSideDocumentId, "leftSideDocumentId");
    	
    	NativeSelect leftSideField = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.leftSideField.label"));
    	leftSideField.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.leftSideField.tooltip"));
    	leftSideField.setNullSelectionAllowed(false);
    	leftSideField.setRequired(true);
    	leftSideField.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	leftSideField.addValidator(new SelectValidator(leftSideField));
    	formLayout.addComponent(leftSideField, 1, 1);
    	fieldGroup.bind(leftSideField, "leftSideField");
    	
    	offsetByLabel = new Label(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.offsetBy.label"));
    	offsetByLabel.setStyleName("offsetByLabel");
    	offsetByLabel.setWidthUndefined();
    	formLayout.addComponent(offsetByLabel, 2, 1);

    	TextField numOffsetUnits = new TextField();
    	numOffsetUnits.setRequired(true);
    	numOffsetUnits.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	numOffsetUnits.setColumns(3);
    	numOffsetUnits.setMaxLength(5);
    	numOffsetUnits.setNullRepresentation("1");
    	numOffsetUnits.setCaption(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.numOffsetUnits.label"));
    	numOffsetUnits.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.numOffsetUnits.tooltip"));
    	numOffsetUnits.setImmediate(true);
    	numOffsetUnits.addValidator(new RegexpValidator("[-+]?[1-9][0-9]{0,3}", vaadinUi.getMsg("tooltip.invalid.integer.range","-9999 to -9999 except 0")));
    	formLayout.addComponent(numOffsetUnits, 3, 1);
    	fieldGroup.bind(numOffsetUnits, "numOffsetUnits");
    	
		NativeSelectDropDown offsetUnit = new NativeSelectDropDown(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.offsetUnit.label"), vaadinUi.getEsfapp().getDropDownTimeIntervalUnitsEsfName());
		offsetUnit.removeItem(Literals.TIME_INTERVAL_UNIT_FOREVER);
		offsetUnit.removeItem(Literals.TIME_INTERVAL_UNIT_NOW);
		offsetUnit.setRequired(true);
		offsetUnit.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.offsetUnit.tooltip"));
		offsetUnit.setImmediate(true);
		offsetUnit.addValidator(new SelectValidator(offsetUnit));
    	formLayout.addComponent(offsetUnit, 4, 1);
    	fieldGroup.bind(offsetUnit, "offsetUnit");

    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	layout.addComponent(footer);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    	footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	_logger.debug("Form created");
	}
	
	void setupAllFieldNames(String forFieldName, EsfUUID newDocId) {
		NativeSelect targetField = (NativeSelect)fieldGroup.getField(forFieldName);
		if ( targetField == null )
			return;
		EsfName targetFieldValue = (EsfName)targetField.getValue(); // save value before we empty the list
		targetField.removeAllItems();
		
		boolean isProduction;
		if ( duplicatedPackageVersion != null ) {
			com.esignforms.open.prog.Package pkg = duplicatedPackageVersion.getPackage();
			isProduction = pkg.getProductionVersion() == duplicatedPackageVersion.getVersion();
		} else {
			com.esignforms.open.prog.Document doc = duplicatedDocumentVersion.getDocument();
			isProduction = doc.getProductionVersion() == duplicatedDocumentVersion.getVersion();
		}

		Document document = Document.Manager.getById(newDocId);
		if ( document == null )
			return; 
		DocumentVersion docVersion;

		if ( isProduction )
			docVersion = document.getProductionDocumentVersion();
		else
			docVersion = document.getTestDocumentVersion();
		Map<EsfName,FieldTemplate> fieldTemplateMap = docVersion.getFieldTemplateMap();
		for( EsfName fieldName : fieldTemplateMap.keySet() ) {
			FieldTemplate fieldTemplate = fieldTemplateMap.get(fieldName);
			if ( fieldTemplate.isTypeDate() || fieldTemplate.isTypeDateTime() )
				targetField.addItem(fieldName);
		}
		if ( targetFieldValue != null && targetFieldValue.isValid() && targetField.containsId(targetFieldValue) ) {
			try {
				targetField.setValue(targetFieldValue); // reset value back
				targetField.commit(); // don't let this cause the field to be marked as modified
			} catch( Exception e ) {}
		}
	}

	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		if ( fieldGroup.getItemDataSource() == null ) {
    			saveSpecs();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! fieldGroup.isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
        	
        	try {
    			fieldGroup.commit();
    			
        		if ( newSpec != null ) {
        			container.addItem(newSpec);
        			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.message.spec.name")) );
        			newSpec = null;
        		} else {
        			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.message.spec.name")) );
        		}
        		
    			saveSpecs();
        		
        		view.select(getCurrentBean()); // reselect our bean so all updates cleanly
	    	} catch( FieldGroup.CommitException e ) {
	    		_logger.error("buttonClick(saveButton) fieldGroup.commit()",e);
	    	}
        } else if ( source == cancelButton ) {
    		fieldGroup.discard();
    		if ( fieldGroup.getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			setNewSpecAsDataSource(null);
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	view.closeParentWindow();
        } else if ( source == createLikeButton ) {
        	fieldGroup.discard();
        	Spec spec = getCurrentBean();
        	if ( spec != null ) {
    			view.unselectAll();
    			setNewSpecAsDataSource(spec.duplicate());
        	}
        } else if ( source == deleteButton ) {
        	fieldGroup.discard();
        	
        	if ( newSpec != null ) {
        		newSpec = null;
        	} else {
            	Spec spec = getCurrentBean();
            	if ( spec != null ) {
            		container.removeItem(spec);
        			saveSpecs();
            	}
        	}
    		view.unselectAll();
			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.form.message.spec.name")) );
        }
    }
	
	void saveSpecs() {
		view.specListUpdated();
	}
	
	Spec getCurrentBean() {
    	return getBean(fieldGroup.getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public Spec getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<Spec> bi = (BeanItem<Spec>)dataSource;
		return bi.getBean();
    }
    
    public void setNewSpecAsDataSource(Spec spec) {
    	newSpec = spec;
    	if ( newSpec == null )
    		setItemDataSource(null);
    	else
    		setItemDataSource(new BeanItem<Spec>(newSpec));
    }
    
    // Used when a new spec is created. We'll default the document id to the first document id our list, or a "fake" id if we have no documents.
    public EsfUUID getNewSpecDocumentId()
    {
    	if ( duplicatedPackageVersion != null )
    		return ( allDocumentIds == null || allDocumentIds.size() == 0 ) ? new EsfUUID() : allDocumentIds.get(0);
    	return duplicatedDocumentVersion.getDocumentId();
    }
    
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
    	fieldGroup.setItemDataSource(newDataSource);
    	formLayout.setVisible(newDataSource != null);
		setReadOnly(isReadOnly());
    }

	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	Spec bean = getCurrentBean();
    	
		offsetByLabel.setVisible(bean != null);
		
    	saveButton.setVisible(!readOnly && bean != null);
    	cancelButton.setVisible(!readOnly && bean != null);
    	closeButton.setVisible(true);
    	createLikeButton.setVisible(!readOnly && bean != null && newSpec == null);
    	deleteButton.setVisible(!readOnly && bean != null);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageProgrammingRule.CalculateDateAction.view.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return fieldGroup.isModified() || newSpec != null;
	}
	
}