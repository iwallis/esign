// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.CalculateValueAction;

import java.util.LinkedList;

import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.runtime.action.CalculateValueAction.Spec;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.themes.Reindeer;

public class ActionList extends Panel {
	private static final long serialVersionUID = 8257181374502918508L;

	final ActionView view;
	ActionContainer container;
	SpecList table;
	
	// For search bar
	Button createNewButton;

	public ActionList(final ActionView view, final ActionContainer containerParam) {
		super();
		this.view = view;
		this.container = containerParam;
		setStyleName("CalculateValueActionList");
		setSizeFull();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
    	
		HorizontalLayout searchBar = new HorizontalLayout();
		searchBar.setWidth(100, Unit.PERCENTAGE);
    	searchBar.addStyleName("searchBar");
    	searchBar.setSpacing(true);
    	searchBar.setMargin(false);

    	layout.addComponent(searchBar);
    	Label caption = new Label(vaadinUi.getMsg("PackageProgrammingRule.CalculateValueAction.list.label"));
    	searchBar.addComponent(caption);
    	searchBar.setExpandRatio(caption, 0.7f);
    	
    	createNewButton = new Button(vaadinUi.getMsg("PackageProgrammingRule.CalculateValueAction.view.searchBar.createNewButton.label"));
    	createNewButton.setStyleName(Reindeer.BUTTON_SMALL);
    	createNewButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageProgrammingRule.CalculateValueAction.view.searchBar.createNewButton.icon")));
    	createNewButton.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateValueAction.view.searchBar.createNewButton.tooltip"));
    	createNewButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = 7655187241044149368L;

			@Override
			public void buttonClick(ClickEvent event) {
				view.createNew();
			}
		});
    	searchBar.addComponent(createNewButton);
    	searchBar.setComponentAlignment(createNewButton, Alignment.MIDDLE_RIGHT);

    	table = new SpecList(view);
    	table.initializeDND();
    	
    	layout.addComponent(searchBar);
    	layout.addComponent(table);
    	layout.setExpandRatio(table, 1);
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		createNewButton.setVisible(!v);
		table.setDragMode( v ? TableDragMode.NONE : TableDragMode.ROW);
	}
	
	public SpecList getTable() {
		return table;
	}
	
	class SpecList extends Table {
		private static final long serialVersionUID = -8348016448396355297L;

		final SpecList thisList;
		
		public SpecList(ActionView view) {
			super();
			thisList = this;
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns(vaadinUi.getStringArray("PackageProgrammingRule.CalculateValueAction.list.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("PackageProgrammingRule.CalculateValueAction.list.showColumnHeaders"));
			setSortDisabled(true);
			setColumnAlignment("order", ALIGN_CENTER);
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);

	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener((Property.ValueChangeListener)view);
	        setNullSelectionAllowed(true);
			setSizeFull();
		}
		
	    public void reorder() {
            // Fix up the order now
			LinkedList<Spec> orderList = new LinkedList<Spec>();
            int order = 1;
            for( Spec spec : container.getItemIds() ) {
            	Item item = container.getItem(spec);
            	item.getItemProperty("order").setValue(order);
            	spec.setOrder(order++);
            	orderList.add(getBean(item));
            }
            view.duplicatedAction.setSpecList(orderList);
		}
		
	    @SuppressWarnings("unchecked")
		public Spec getBean(Item item) {
	    	if ( item == null )
	    		return null;
			BeanItem<Spec> bi = (BeanItem<Spec>)item;
			return bi.getBean();
	    }

	    public void initializeDND() {
			setDropHandler(new DropHandler() {
				private static final long serialVersionUID = 3120438461100141485L;

				public void drop(DragAndDropEvent dropEvent) {
	                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
	                
	                Container sourceContainer = t.getSourceContainer();
	                if ( sourceContainer == container ) {
	                	// reordering within the table rows
	                	Spec sourceItemId = (Spec)t.getItemId();

		                AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails)dropEvent.getTargetDetails());
		                Spec targetItemId = (Spec)dropData.getItemIdOver();
		                
		                // No move if source and target are the same
		                if ( sourceItemId == targetItemId )
		                	return;
		                
		                // Let's remove the source of the drag so we can add it back where requested...
		                container.removeItem(sourceItemId);
		                if ( targetItemId == null ) {
		                	container.addItem(sourceItemId);
		                } else if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
		                	container.addItemAfter(targetItemId,sourceItemId);
		            	} else {
		                    Object prevItemId = container.prevItemId(targetItemId);
		                    container.addItemAfter(prevItemId, sourceItemId);
		            	}
		                
		                reorder();
	                }
	            }

	            public AcceptCriterion getAcceptCriterion() {
	                return new And(SourceIsTarget.get(), new Not(AbstractSelect.VerticalLocationIs.MIDDLE)); // dragging within the table
	            }
	        });	
		}

		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property property) {
			if ( "targetDocumentId".equals(colId) ) {
				EsfUUID id = (EsfUUID)property.getValue();
				if ( id == null || id.isNull() )
					return "";
				return Document.Manager.getById(id).getEsfName().toString();
			}
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // SpecList

} // ActionList
