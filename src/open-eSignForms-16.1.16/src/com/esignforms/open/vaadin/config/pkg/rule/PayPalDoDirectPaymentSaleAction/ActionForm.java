// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.PayPalDoDirectPaymentSaleAction;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.runtime.action.PayPalDoDirectPaymentSaleAction;
import com.esignforms.open.runtime.action.PayPalDoDirectPaymentSaleAction.Spec;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.FieldSpecTextField;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * @author Yozons Inc.
 */
public class ActionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 1767272072619483362L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ActionForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;

	GridLayout layout;
	
	final PackageVersion duplicatedPackageVersion;
	final DocumentVersion duplicatedDocumentVersion;
	final PayPalDoDirectPaymentSaleAction duplicatedAction;
	final ActionView view;
	final ActionContainer container;
	
	List<EsfUUID> allDocumentIds;
	List<EsfName> allDocumentNames;
	
	HorizontalLayout maskCheckboxLayout;
	
	Spec newSpec; // when set, we're working on a new spec.
	
	public ActionForm(ActionView view, final ActionContainer container, PackageVersion duplicatedPackageVersionParam, PayPalDoDirectPaymentSaleAction duplicatedActionParam)	{
		this.view = view;
		this.container = container;
		this.duplicatedPackageVersion = duplicatedPackageVersionParam;
		this.duplicatedDocumentVersion = null;
		this.duplicatedAction = duplicatedActionParam;
		allDocumentIds = new LinkedList<EsfUUID>(duplicatedPackageVersion.getDocumentIdList());
		setupForm();
	}
	
	public ActionForm(ActionView view, final ActionContainer container, DocumentVersion duplicatedDocumentVersionParam, PayPalDoDirectPaymentSaleAction duplicatedActionParam)	{
		this.view = view;
		this.container = container;
		this.duplicatedPackageVersion = null;
		this.duplicatedDocumentVersion = duplicatedDocumentVersionParam;
		this.duplicatedAction = duplicatedActionParam;
		allDocumentIds = new LinkedList<EsfUUID>();
		allDocumentIds.add(duplicatedDocumentVersion.getDocumentId());
		setupForm();
	}
	
	private void setupForm() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		setStyleName("PayPalDoDirectPaymentSaleActionForm");
    	allDocumentNames = new LinkedList<EsfName>();
    	
    	for( EsfUUID docId : allDocumentIds ) {
    		Document doc = Document.Manager.getById(docId);
    		if ( doc == null ) {
    			_logger.warn("ActionForm() - Could not find document with id: " + docId);
    			allDocumentNames.add(docId.toEsfName());
    		} else {
    			allDocumentNames.add(doc.getEsfName());
    		}
    	}
	
    	// Setup layout
    	layout = new GridLayout(5,10);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setWidth(100, Unit.PERCENTAGE);
    	setLayout(layout);
    	
    	Label spacer = new Label("&nbsp;");
    	spacer.setContentMode(Label.CONTENT_XHTML);
    	layout.addComponent(spacer, 2, 0, 2, 6);
    	
    	maskCheckboxLayout = new HorizontalLayout();
    	maskCheckboxLayout.setSpacing(true);
    	maskCheckboxLayout.setMargin(false);
    	layout.addComponent(maskCheckboxLayout, 2, 7, 4, 7);

    	Label notice = new Label(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.notice"));
    	notice.setStyleName(Reindeer.LABEL_SMALL);
    	layout.addComponent(notice, 0, 9, 4, 9);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    	footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 8221025967269201869L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

				if (propertyId.equals("ccBillingFirstNameDocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingFirstNameDocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingFirstNameDocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -1757377279292191016L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames("ccBillingFirstNameField",newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("ccBillingFirstNameField")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingFirstNameField.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingFirstNameField.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
                
				if (propertyId.equals("ccBillingLastNameDocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingLastNameDocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingLastNameDocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 5838159413664102985L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames("ccBillingLastNameField",newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("ccBillingLastNameField")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingLastNameField.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingLastNameField.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
                
				if (propertyId.equals("ccBillingAddressDocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingAddressDocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingAddressDocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 990316198301547827L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames("ccBillingAddressField",newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("ccBillingAddressField")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingAddressField.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingAddressField.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
                
				if (propertyId.equals("ccBillingAddress2DocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingAddress2DocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingAddress2DocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(false);
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -1142547641463157233L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames("ccBillingAddress2Field",newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("ccBillingAddress2Field")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingAddress2Field.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingAddress2Field.tooltip"));
                	select.setNullSelectionAllowed(true);
                	select.setRequired(false);
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
                
				if (propertyId.equals("ccBillingCityDocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingCityDocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingCityDocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -6412645729019280424L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames("ccBillingCityField",newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("ccBillingCityField")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingCityField.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingCityField.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
                
				if (propertyId.equals("ccBillingStateDocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingStateDocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingStateDocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 5476934790409678480L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames("ccBillingStateField",newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("ccBillingStateField")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingStateField.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingStateField.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
                
				if (propertyId.equals("ccBillingZipDocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingZipDocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingZipDocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 3905006699110107503L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames("ccBillingZipField",newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("ccBillingZipField")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingZipField.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingZipField.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
                
				if (propertyId.equals("ccBillingCountryCodeDocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingCountryCodeDocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingCountryCodeDocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(false);
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -3300480886375992164L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames("ccBillingCountryCodeField",newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("ccBillingCountryCodeField")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingCountryCodeField.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccBillingCountryCodeField.tooltip"));
                	select.setNullSelectionAllowed(true);
                	select.setRequired(false);
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
                
				if (propertyId.equals("ccTypeDocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccTypeDocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccTypeDocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(false);
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -6981031039626268295L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames("ccTypeField",newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("ccTypeField")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccTypeField.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccTypeField.tooltip"));
                	select.setNullSelectionAllowed(true);
                	select.setRequired(false);
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
                
				if (propertyId.equals("ccAccountNumberDocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccAccountNumberDocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccAccountNumberDocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -3557694313367974153L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames("ccAccountNumberField",newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("ccAccountNumberField")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccAccountNumberField.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccAccountNumberField.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
                
				if (propertyId.equals("ccExpirationDocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccExpirationDocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccExpirationDocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 2572515649891463459L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames("ccExpirationField",newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("ccExpirationField")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccExpirationField.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccExpirationField.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
                
				if (propertyId.equals("ccSecurityIdDocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccSecurityIdDocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccSecurityIdDocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(false);
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 6645731603535741184L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames("ccSecurityIdField",newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("ccSecurityIdField")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccSecurityIdField.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.ccSecurityIdField.tooltip"));
                	select.setNullSelectionAllowed(true);
                	select.setRequired(false);
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
                
				if (propertyId.equals("amountDocumentId")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.amountDocumentId.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.amountDocumentId.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		select.addItem(docId);
                		select.setItemCaption(docId, nameIter.next().toString());
                	}
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 7508226748070673151L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
							if ( newDocId != null && ! newDocId.isNull() ) {
								setupAllFieldNames("amountField",newDocId);
							}
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("amountField")) {
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.amountField.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.amountField.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	return select;
                }
				
				if (propertyId.equals("maskAccountNumberOnSuccess")) {
					CheckBox cb = new CheckBox(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.maskAccountNumberOnSuccess.label"));
					cb.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.maskAccountNumberOnSuccess.tooltip"));
					return cb;
				}
                
				if (propertyId.equals("maskSecurityIdOnSuccess")) {
					CheckBox cb = new CheckBox(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.maskSecurityIdOnSuccess.label"));
					cb.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.maskSecurityIdOnSuccess.tooltip"));
					return cb;
				}
                
				FieldSpecTextField tf = new FieldSpecTextField();
                tf.setWidth(100, Unit.PERCENTAGE);
    			
                if ( propertyId.equals("customerNameFieldSpec") ) { 
                	tf.setRequired(false);
                    tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.customerNameFieldSpec.label"));
                    tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.customerNameFieldSpec.tooltip"));
                } else if ( propertyId.equals("descriptionFieldSpec") ) {
                	tf.setRequired(false);
                    tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.descriptionFieldSpec.label"));
                    tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.descriptionFieldSpec.tooltip"));
                } else if ( propertyId.equals("emailFieldSpec") ) {
                	tf.setRequired(false);
                    tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.emailFieldSpec.label"));
                    tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.emailFieldSpec.tooltip"));
                } else if ( propertyId.equals("invoiceNumberFieldSpec") ) {
                	tf.setRequired(false);
                    tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.invoiceNumberFieldSpec.label"));
                    tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.invoiceNumberFieldSpec.tooltip"));
                } else if ( propertyId.equals("onSuccessMessageSpec") ) {
                	tf.setRequired(false);
                    tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.onSuccessMessageSpec.label"));
                    tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.onSuccessMessageSpec.tooltip"));
                } else if ( propertyId.equals("onFailureMessageSpec") ) {
                	tf.setRequired(false);
                    tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.onFailureMessageSpec.label"));
                    tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.onFailureMessageSpec.tooltip"));
                }
                
                return tf;
    	    }
    	 });

    	_logger.debug("Form created");

	}
	
	void setupAllFieldNames(String forFieldName, EsfUUID newDocId) {
		NativeSelect targetField = (NativeSelect)getField(forFieldName);
		if ( targetField == null )
			return;
		EsfName targetFieldValue = (EsfName)targetField.getValue(); // save value before we empty the list
		targetField.removeAllItems();
		
		boolean isProduction;
		if ( duplicatedPackageVersion != null ) {
			com.esignforms.open.prog.Package pkg = duplicatedPackageVersion.getPackage();
			isProduction = pkg.getProductionVersion() == duplicatedPackageVersion.getVersion();
		} else {
			com.esignforms.open.prog.Document doc = duplicatedDocumentVersion.getDocument();
			isProduction = doc.getProductionVersion() == duplicatedDocumentVersion.getVersion();
		}

		Document document = Document.Manager.getById(newDocId);
		if ( document == null )
			return; 
		DocumentVersion docVersion;

		if ( isProduction )
			docVersion = document.getProductionDocumentVersion();
		else
			docVersion = document.getTestDocumentVersion();
		Map<EsfName,FieldTemplate> fieldTemplateMap = docVersion.getFieldTemplateMap();
		for( EsfName fieldName : fieldTemplateMap.keySet() ) {
			FieldTemplate fieldTemplate = fieldTemplateMap.get(fieldName);
			if ( "amountField".equals(forFieldName) ) {
				if ( fieldTemplate.isTypeMoney() )
					targetField.addItem(fieldName);
			} else if ( "ccExpirationField".equals(forFieldName) ) {
				if ( fieldTemplate.isTypeDate() )
					targetField.addItem(fieldName);
			} else if ( "ccAccountNumberField".equals(forFieldName) ) {
				if ( fieldTemplate.isTypeCreditCard() )
					targetField.addItem(fieldName);
			} else if ( "ccBillingZipField".equals(forFieldName) ) {
				if ( fieldTemplate.isTypeZipCode() || fieldTemplate.isTypeGeneral() ) // allow General for non-US postal codes since we don't have a field type for other areas
					targetField.addItem(fieldName);
			} else if ( "ccSecurityIdField".equals(forFieldName) ) {
				if ( fieldTemplate.isTypeGeneral() || fieldTemplate.isTypeInteger() )
					targetField.addItem(fieldName);
			} else if ( fieldTemplate.isTypeGeneral() || fieldTemplate.isTypeSelection() || fieldTemplate.isTypeRadioButtonGroup() || fieldTemplate.isTypeCheckbox() ) { // everything else should be a string field like checkbox, general, radio button group field
				targetField.addItem(fieldName);
			}
		}
		if ( targetFieldValue != null && targetFieldValue.isValid() && targetField.containsId(targetFieldValue) ) {
			try {
				targetField.setValue(targetFieldValue); // reset value back
				targetField.commit(); // don't let this cause the field to be marked as modified
			} catch( Exception e ) {}
		}
	}

	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		if ( getItemDataSource() == null ) {
    			saveSpecs();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
    		if ( newSpec != null ) {
    			container.addItem(newSpec);
    			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.message.spec.name")) );
    			newSpec = null;
    		} else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.message.spec.name")) );
    		}
    		
			saveSpecs();
    		
    		view.select(getCurrentBean()); // reselect our bean so all updates cleanly
        } else if ( source == cancelButton ) {
    		discard();
    		if ( getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			setNewSpecAsDataSource(null);
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	view.closeParentWindow();
        } else if ( source == createLikeButton ) {
        	discard();
        	Spec spec = getCurrentBean();
        	if ( spec != null ) {
    			view.unselectAll();
    			setNewSpecAsDataSource(spec.duplicate());
        	}
        } else if ( source == deleteButton ) {
        	discard();
        	
        	if ( newSpec != null ) {
        		newSpec = null;
        	} else {
            	Spec spec = getCurrentBean();
            	if ( spec != null ) {
            		container.removeItem(spec);
        			saveSpecs();
            	}
        	}
    		view.unselectAll();
			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.message.spec.name")) );
        }
    }
	
	void saveSpecs() {
		view.specListUpdated();
	}
	
	Spec getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public Spec getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<Spec> bi = (BeanItem<Spec>)dataSource;
		return bi.getBean();
    }
    
    public void setNewSpecAsDataSource(Spec spec) {
    	newSpec = spec;
    	if ( newSpec == null )
    		setItemDataSource(null);
    	else
    		setItemDataSource(new BeanItem<Spec>(newSpec));
    }
    
    // Used when a new spec is created. We'll default the document id to the first document id our list, or a "fake" id if we have no documents.
    public EsfUUID getNewSpecDocumentId()
    {
    	if ( duplicatedPackageVersion != null )
    		return ( allDocumentIds == null || allDocumentIds.size() == 0 ) ? new EsfUUID() : allDocumentIds.get(0);
    	return duplicatedDocumentVersion.getDocumentId();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		if ( newDataSource != null ) {
    		super.setItemDataSource(newDataSource, Arrays.asList("customerNameFieldSpec","descriptionFieldSpec","emailFieldSpec","invoiceNumberFieldSpec",  
    				"ccBillingFirstNameField", "ccBillingFirstNameDocumentId", // create the field before the document id
    				"ccBillingLastNameField", "ccBillingLastNameDocumentId",
    				"ccBillingAddressField", "ccBillingAddressDocumentId",
    				"ccBillingAddress2Field", "ccBillingAddress2DocumentId",
    				"ccBillingCityField", "ccBillingCityDocumentId",
    				"ccBillingStateField", "ccBillingStateDocumentId",
    				"ccBillingZipField", "ccBillingZipDocumentId",
    				"ccBillingCountryCodeField", "ccBillingCountryCodeDocumentId",
    				"ccTypeField", "ccTypeDocumentId",
    				"ccAccountNumberField", "ccAccountNumberDocumentId",
    				"ccExpirationField", "ccExpirationDocumentId",
    				"ccSecurityIdField", "ccSecurityIdDocumentId",
    				"amountField", "amountDocumentId",
    				"maskAccountNumberOnSuccess", "maskSecurityIdOnSuccess",
    				"onSuccessMessageSpec", "onFailureMessageSpec")
    								);
    		layout.setVisible(true);
		} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
		}
		setReadOnly(isReadOnly());
    }

	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	Spec bean = getCurrentBean();
    	
    	saveButton.setVisible(!readOnly && bean != null);
    	cancelButton.setVisible(!readOnly && bean != null);
    	closeButton.setVisible(true);
    	createLikeButton.setVisible(!readOnly && bean != null && newSpec == null);
    	deleteButton.setVisible(!readOnly && bean != null);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("customerNameFieldSpec")) {
            layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("descriptionFieldSpec")) {
        	layout.addComponent(field, 1, 0);
        } else if (propertyId.equals("emailFieldSpec")) {
        	layout.addComponent(field, 3, 0);
        } else if (propertyId.equals("invoiceNumberFieldSpec")) {
        	layout.addComponent(field, 4, 0);
        } else if (propertyId.equals("ccBillingFirstNameDocumentId")) {
        	layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("ccBillingFirstNameField")) {
        	layout.addComponent(field, 1, 1);
        } else if (propertyId.equals("ccBillingLastNameDocumentId")) {
        	layout.addComponent(field, 3, 1);
        } else if (propertyId.equals("ccBillingLastNameField")) {
        	layout.addComponent(field, 4, 1);
        } else if (propertyId.equals("ccBillingAddressDocumentId")) {
        	layout.addComponent(field, 0, 2);
        } else if (propertyId.equals("ccBillingAddressField")) {
        	layout.addComponent(field, 1, 2);
        } else if (propertyId.equals("ccBillingAddress2DocumentId")) {
        	layout.addComponent(field, 3, 2);
        } else if (propertyId.equals("ccBillingAddress2Field")) {
        	layout.addComponent(field, 4, 2);
        } else if (propertyId.equals("ccBillingCityDocumentId")) {
        	layout.addComponent(field, 0, 3);
        } else if (propertyId.equals("ccBillingCityField")) {
        	layout.addComponent(field, 1, 3);
        } else if (propertyId.equals("ccBillingStateDocumentId")) {
        	layout.addComponent(field, 3, 3);
        } else if (propertyId.equals("ccBillingStateField")) {
        	layout.addComponent(field, 4, 3);
        } else if (propertyId.equals("ccBillingZipDocumentId")) {
        	layout.addComponent(field, 0, 4);
        } else if (propertyId.equals("ccBillingZipField")) {
        	layout.addComponent(field, 1, 4);
        } else if (propertyId.equals("ccBillingCountryCodeDocumentId")) {
        	layout.addComponent(field, 3, 4);
        } else if (propertyId.equals("ccBillingCountryCodeField")) {
        	layout.addComponent(field, 4, 4);
        } else if (propertyId.equals("ccTypeDocumentId")) {
        	layout.addComponent(field, 0, 5);
        } else if (propertyId.equals("ccTypeField")) {
        	layout.addComponent(field, 1, 5);
        } else if (propertyId.equals("ccAccountNumberDocumentId")) {
        	layout.addComponent(field, 3, 5);
        } else if (propertyId.equals("ccAccountNumberField")) {
        	layout.addComponent(field, 4, 5);
        } else if (propertyId.equals("ccExpirationDocumentId")) {
        	layout.addComponent(field, 0, 6);
        } else if (propertyId.equals("ccExpirationField")) {
        	layout.addComponent(field, 1, 6);
        } else if (propertyId.equals("ccSecurityIdDocumentId")) {
        	layout.addComponent(field, 3, 6);
        } else if (propertyId.equals("ccSecurityIdField")) {
        	layout.addComponent(field, 4, 6);
        } else if (propertyId.equals("amountDocumentId")) {
        	layout.addComponent(field, 0, 7);
        } else if (propertyId.equals("amountField")) {
        	layout.addComponent(field, 1, 7);
        } else if (propertyId.equals("maskAccountNumberOnSuccess")) {
        	maskCheckboxLayout.removeAllComponents();
        	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
        	Label label = new Label(vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.form.maskCheckboxLayout.prefix.label"));
        	label.setSizeUndefined();
        	maskCheckboxLayout.addComponent(label);
        	maskCheckboxLayout.addComponent(field);
        } else if (propertyId.equals("maskSecurityIdOnSuccess")) {
        	maskCheckboxLayout.addComponent(field);
        	layout.setComponentAlignment(maskCheckboxLayout, Alignment.MIDDLE_CENTER);
        } else if (propertyId.equals("onSuccessMessageSpec")) {
        	layout.addComponent(field, 0, 8, 1, 8);
        } else if (propertyId.equals("onFailureMessageSpec")) {
        	layout.addComponent(field, 3, 8, 4, 8);
        }
    }
	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageProgrammingRule.PayPalDoDirectPaymentSaleAction.view.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified() || newSpec != null;
	}
	
}