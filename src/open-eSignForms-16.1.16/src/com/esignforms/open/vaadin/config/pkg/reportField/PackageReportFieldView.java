// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.reportField;

import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionReportField;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

public class PackageReportFieldView extends VerticalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = 7184799450440821948L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackageReportFieldView.class);
	
    final PackageVersion duplicatedPackageVersion;
    final PackageReportFieldView thisView;
    ConfirmDiscardFormChangesWindow parentWindow;
    PackageReportFieldContainer container;
    PackageReportFieldList list;
    PackageReportFieldForm form;
	
	public PackageReportFieldView(PackageVersion duplicatedPackageVersion) {
		super();
		this.duplicatedPackageVersion = duplicatedPackageVersion;
		thisView = this;
		setStyleName(Reindeer.SPLITPANEL_SMALL);
	}
	
	public void refreshReportTemplates() {
		container.refresh(duplicatedPackageVersion.getPackageVersionReportFieldList());
		form.setupAllReportFieldTemplates();
	}
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			container = new PackageReportFieldContainer(duplicatedPackageVersion.getPackageVersionReportFieldList());
			list = new PackageReportFieldList(this,container,duplicatedPackageVersion);
			form = new PackageReportFieldForm(this,container,duplicatedPackageVersion);
			
			setFirstComponent(list);
			setSecondComponent(form);
			setSplitPosition(320,Unit.PIXELS);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("PackageReportFieldView view exception", e.getMessage());
		}
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		if ( list != null) list.setReadOnly(v);
		if ( form != null) form.setReadOnly(v);
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our list or not
        if (property == list.getTable()) {
        	final Item item = list.getTable().getItem(list.getTable().getValue());
        	if ( isDirty() ) {
    			if ( form.fieldGroup.getItemDataSource() == item ) {
    				// We've selected the same item so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("PackageReportFieldView.ConfirmDiscardChangesDialog.message"), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						form.fieldGroup.discard();
						form.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						list.getTable().select(form.fieldGroup.getItemDataSource()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("PackageReportFieldView.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	form.setItemDataSource(item);
    		}
        }
        else
        	_logger.debug("valueChange() - no list event: " + event.toString());
	}

    public void createNew() {
		unselectAll();
		PackageVersionReportField newField = PackageVersionReportField.Manager.createNew(duplicatedPackageVersion.getId(), 
				form.getNewReportFieldDocumentId(), 
				null,
				(short)(container.size()+1)
		);
		form.setNewReportFieldAsDataSource(newField);
	}
	
	public void setParentWindow(ConfirmDiscardFormChangesWindow w) {
		parentWindow = w;
	}
	
	public void closeParentWindow() {
		parentWindow.close();
	}
	
	public void unselectAll() {
		list.getTable().setValue(null);
	}
	public void select(PackageVersionReportField rf) {
		unselectAll();
		list.getTable().select(rf);
		list.getTable().setCurrentPageFirstItemId(rf);
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return form.checkDirty();
	}
	
	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return form.isDirty();
	}
}