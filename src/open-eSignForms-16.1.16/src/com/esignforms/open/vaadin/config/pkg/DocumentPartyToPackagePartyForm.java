// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.ListSelectValid;
import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The DocumentPartyToPackagePartyForm is used to map the parties defined in a give Document to the parties defined in 
 * the package.
 * @author Yozons Inc.
 */
public class DocumentPartyToPackagePartyForm extends Form implements EsfView, ClickListener {

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DocumentPartyToPackagePartyForm.class);
	
	Button saveButton;
	Button cancelButton;
	
	PackageVersionBean packageVersionBean;
	PackageVersion packageVersion;
	EsfUUID documentId;
	Document document;
	DocumentVersion documentVersion;
	List<EsfUUID> allDocumentPartyTemplateIds;
	List<EsfName> allDocumentPartyTemplateNames;
	List<EsfUUID> allPackagePartyTemplateIds;
	List<EsfName> allPackagePartyTemplateNames;
	Map<EsfUUID,HashSet<EsfUUID>> selectedDocumentPartyIdToPackagePartyIds; // contains the selected package parties that map to a document party
	
	ConfirmDiscardFormChangesWindow parentWindow;
    VerticalLayout layout;
    

    public DocumentPartyToPackagePartyForm(PackageVersionBean packageVersionBean, EsfUUID documentId) {
    	setStyleName("DocumentPartyToPackagePartyForm");
    	this.packageVersionBean = packageVersionBean;
    	this.documentId = documentId;
    }
    
	@SuppressWarnings("unchecked")
	public void buttonClick(ClickEvent event) {
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
	    	for( EsfUUID docPartyId : allDocumentPartyTemplateIds ) {
	    		java.util.Set<EsfUUID> packagePartyIds = (java.util.Set<EsfUUID>)getField(docPartyId).getValue();
	    		packageVersion.mapDocumentPartyToPackageParties(documentId,getDocPartyName(docPartyId),packagePartyIds);
	    	}
			parentWindow.close();
        } else if ( source == cancelButton ) {
    		discard();
			parentWindow.close();
        }
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	saveButton.setVisible(!readOnly);
    }
    
    
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void activateView(EsfView.OpenMode mode, String param) {
		PropertysetItem item = new PropertysetItem();
    	for( EsfUUID docPartyId : allDocumentPartyTemplateIds ) {
    		HashSet<EsfUUID> packagePartyIds = selectedDocumentPartyIdToPackagePartyIds.get(docPartyId);
    		item.addItemProperty(docPartyId, new ObjectProperty(packagePartyIds,java.util.Set.class));
    	}
		setItemDataSource(item);
	}
	
	public void setParentWindow(ConfirmDiscardFormChangesWindow parentWindow) {
		this.parentWindow = parentWindow;
	}

	@Override
	public void initView() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		// Set up our data structures.
    	packageVersion = packageVersionBean.duplicatedPackageVersion();
    	document = Document.Manager.getById(documentId);
    	documentVersion = packageVersionBean.isProductionVersion() ? document.getProductionDocumentVersion() : document.getTestDocumentVersion();

    	allPackagePartyTemplateIds = new LinkedList<EsfUUID>();
    	allPackagePartyTemplateNames = new LinkedList<EsfName>();
    	for( PackageVersionPartyTemplate packageParty : packageVersion._getPackageVersionPartyTemplateListInternal() ) {
    		allPackagePartyTemplateIds.add(packageParty.getId());
    		allPackagePartyTemplateNames.add(packageParty.getEsfName());
    	}
    	
    	allDocumentPartyTemplateIds = new LinkedList<EsfUUID>();
    	allDocumentPartyTemplateNames = new LinkedList<EsfName>();
    	for( PartyTemplate party : documentVersion.getPartyTemplateList() ) {
			allDocumentPartyTemplateIds.add(party.getId());
			allDocumentPartyTemplateNames.add(party.getEsfName());
    	}
    	
    	selectedDocumentPartyIdToPackagePartyIds = new HashMap<EsfUUID,HashSet<EsfUUID>>();
    	ListIterator<EsfUUID> docPartyIdIter = allDocumentPartyTemplateIds.listIterator();
    	ListIterator<EsfName> docPartyNameIter = allDocumentPartyTemplateNames.listIterator();
    	while( docPartyIdIter.hasNext() && docPartyNameIter.hasNext() ) {
    		EsfUUID docPartyId = docPartyIdIter.next();
    		EsfName docPartyName = docPartyNameIter.next();
    		HashSet<EsfUUID> mappedToPackagePartyIds = new HashSet<EsfUUID>(packageVersion.getPackageVersionPartyTemplateIds(documentId,docPartyName));
    		selectedDocumentPartyIdToPackagePartyIds.put(docPartyId, mappedToPackagePartyIds);
    	}

    	// Set up layout
    	setCaption(vaadinUi.getMsg("DocumentPartyToPackagePartyForm.caption",documentVersion.getDocumentNameVersionWithLabel()));

    	layout = new VerticalLayout();
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -7294953618125270580L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				
				EsfUUID docPartyId = (EsfUUID)propertyId;
				
	    		ListSelect select = new ListSelectValid(vaadinUi.getMsg("DocumentPartyToPackagePartyForm.ListSelect.label",getDocPartyName(docPartyId).toString()));
	    		select.setRequired(true);
				select.setMultiSelect(true); // While not common, a given document party can be played by multiple package parties (multiple package parties -- real processing parties -- access the same document with the same capabilities)
				select.setNullSelectionAllowed(false);
				select.setWidth(100, Unit.PERCENTAGE);
				select.setImmediate(true);
                select.addValidator(new SelectValidator(select));
		    	ListIterator<EsfUUID> pkgPartyIdIter = allPackagePartyTemplateIds.listIterator();
		    	ListIterator<EsfName> pkgPartyNameIter = allPackagePartyTemplateNames.listIterator();
		    	while( pkgPartyIdIter.hasNext() && pkgPartyNameIter.hasNext() ) {
		    		EsfUUID pkgPartyId = pkgPartyIdIter.next();
		    		EsfName pkgPartyName = pkgPartyNameIter.next();
		    		select.addItem(pkgPartyId);
		    		select.setItemCaption(pkgPartyId, pkgPartyName.toString());
		    	}
		    	select.setRows(Math.min(4, allPackagePartyTemplateIds.size()));
	    		return select;
    	    }
    	 });
    	
    	_logger.debug("Form created");
	}
	
    private EsfName getDocPartyName(EsfUUID searchDocPartyId) {
    	ListIterator<EsfUUID> docPartyIdIter = allDocumentPartyTemplateIds.listIterator();
    	ListIterator<EsfName> docPartyNameIter = allDocumentPartyTemplateNames.listIterator();
    	while( docPartyIdIter.hasNext() && docPartyNameIter.hasNext() ) {
    		EsfUUID docPartyId = docPartyIdIter.next();
    		EsfName docPartyName = docPartyNameIter.next();
    		if ( docPartyId.equals(searchDocPartyId) ) {
    			return docPartyName;
    		}
    	}
    	return null;
    }

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageAndVersionsMainView.Package.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}