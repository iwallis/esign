// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.reportTemplate.reportField;

import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.ReportTemplateReportField;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

public class ReportTemplateReportFieldView extends VerticalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = 5845860158167255853L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ReportTemplateReportFieldView.class);
	
    final ReportTemplate duplicatedReportTemplate;
    final ReportTemplateReportFieldView thisView;
    ConfirmDiscardFormChangesWindow parentWindow;
    ReportTemplateReportFieldContainer container;
    ReportTemplateReportFieldList list;
    ReportTemplateReportFieldForm form;
	
	public ReportTemplateReportFieldView(ReportTemplate duplicatedReportTemplate) {
		super();
		this.duplicatedReportTemplate = duplicatedReportTemplate;
		thisView = this;
		setStyleName(Reindeer.SPLITPANEL_SMALL);
	}
	
	protected void buildLayout() {
		try {
			container = new ReportTemplateReportFieldContainer(duplicatedReportTemplate.getReportTemplateReportFieldList());
			list = new ReportTemplateReportFieldList(this,container,duplicatedReportTemplate);
			form = new ReportTemplateReportFieldForm(this,container,duplicatedReportTemplate);
			
			setFirstComponent(list);
			setSecondComponent(form);
			setSplitPosition(320,Unit.PIXELS);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			vaadinUi.showError("ReportTemplateReportFieldView view exception", e.getMessage());
		}
	}
	
	public void refreshReportTemplates() {
		container.refresh(duplicatedReportTemplate.getReportTemplateReportFieldList());
		form.setupAllReportFieldTemplates();
	}

	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		if ( list != null) list.setReadOnly(v);
		if ( form != null) form.setReadOnly(v);
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our list or not
        if (property == list.getTable()) {
        	final Item item = list.getTable().getItem(list.getTable().getValue());
        	if ( isDirty() ) {
    			if ( form.getItemDataSource() == item ) {
    				// We've selected the same item so don't need to warn (or reset the data source)
    				return;
    			}
    			new ConfirmDiscardChangesDialog(EsfVaadinUI.getInstance().getMsg("ReportTemplateReportFieldView.ConfirmDiscardChangesDialog.message"), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						form.discard();
						form.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						list.getTable().select(form.getItemDataSource()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("ReportTemplateReportFieldView.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	form.setItemDataSource(item);
    		}
        }
        else
        	_logger.debug("valueChange() - no list event: " + event.toString());
	}

    public void createNew() {
		unselectAll();
		
		ReportTemplateReportField newField = ReportTemplateReportField.Manager.createNew(duplicatedReportTemplate.getId(),(short)(container.size()+1));
		form.setNewReportFieldAsDataSource(newField);
	}
    
    public void reorder() {
    	list.getTable().reorder();
    }
	
	public void setParentWindow(ConfirmDiscardFormChangesWindow w) {
		parentWindow = w;
	}
	
	public void closeParentWindow() {
		parentWindow.close();
	}
	
	public void unselectAll() {
		list.getTable().setValue(null);
	}
	public void select(ReportTemplateReportField rtrf) {
		unselectAll();
		list.getTable().select(rtrf);
		list.getTable().setCurrentPageFirstItemId(rtrf);
	}
	
	@Override
	public void detach() {
		super.detach();
	}
	
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public String checkDirty() {
		return form.checkDirty();
	}
	
	@Override
	public void initView() {
    	buildLayout();
	}

	@Override
	public boolean isDirty() {
		return form.isDirty();
	}
}