// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.reportFieldTemplate;

import java.io.Serializable;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.user.User;


public class ReportTemplateBean implements Serializable {
	private static final long serialVersionUID = 9174681496503641728L;

	EsfPathName pathName;
	String displayName;
	
	public ReportTemplateBean(ReportTemplate reportTemplate, User user) {
		boolean userCanList = reportTemplate.canUserList(user);
		Application app = Application.getInstance();
		this.pathName = userCanList ? reportTemplate.getPathName() : new EsfPathName(app.getServerMessages().getString("permission.userCannotList.esfName"));
		this.displayName = userCanList ? reportTemplate.getDisplayName() : app.getServerMessages().getString("permission.userCannotList.displayName");
	}
	
	public EsfPathName getPathName() { return pathName; }
	public String getDisplayName() { return displayName; }
}