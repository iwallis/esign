// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.dropdown;

import java.io.Serializable;

import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.DropDownVersionOption;


public class LibDropDownVersionOptionBean implements Serializable, Comparable<LibDropDownVersionOptionBean> {
	private static final long serialVersionUID = 2458445124835440359L;

	private LibDropDownVersionBean dropDownVersionBean;
	
	private DropDownVersionOption dropDownVersionOption;
	
	
	public LibDropDownVersionOptionBean(LibDropDownVersionBean dropDownVersionBean, DropDownVersionOption dropDownVersionOption) {
		this.dropDownVersionBean = dropDownVersionBean;
		this.dropDownVersionOption = dropDownVersionOption;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibDropDownVersionOptionBean )
			return dropDownVersionOption.equals(((LibDropDownVersionOptionBean)o).dropDownVersionOption);
		return false;
	}
	@Override
    public int hashCode() {
    	return dropDownVersionOption.hashCode();
    }
	public int compareTo(LibDropDownVersionOptionBean o) {
		return dropDownVersionOption.getOption().compareTo(o.dropDownVersionOption.getOption());
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public LibDropDownVersionBean dropDownVersionBean() {
		return dropDownVersionBean;
	}
	
	public DropDownVersion dropdownVersion() {
		return dropDownVersionBean.dropdownVersion();
	}

	public DropDownVersionOption dropdownVersionOption() {
		return dropDownVersionOption;
	}

	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return dropDownVersionOption.getId();
	}
	
	public short getListOrder() {
		return dropDownVersionOption.getListOrder();
	}

	public String getOption() {
		return dropDownVersionOption.getOption();
	}
	public void setOption(String v) {
		dropDownVersionOption.setOption(v);
	}

	public String getValue() {
		return dropDownVersionOption.getValue();
	}
	public void setValue(String v) {
		dropDownVersionOption.setValue(v);
	}
}