// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.image;

import java.io.Serializable;
import java.util.Collection;

import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.ImageVersionInfo;
import com.vaadin.data.util.BeanItemContainer;


public class LibImageVersionBeanContainer extends BeanItemContainer<LibImageVersionBean> implements Serializable {
	private static final long serialVersionUID = 385089927902726828L;

	final LibImageAndVersionsMainView view;
	
	public LibImageVersionBeanContainer(LibImageAndVersionsMainView view) throws InstantiationException, IllegalAccessException {
		super(LibImageVersionBean.class);
		this.view = view;

		// No items exist until a image is selected and set to this re-loadable container.
	}
	
	public void reload(Image image) {
		removeAllItems();
		
		// Users need update permission to program its contents
		if ( image != null ) {
			
			Collection<ImageVersionInfo> imageVers = ImageVersionInfo.Manager.getAll(image);
			for( ImageVersionInfo i : imageVers ) {
				addItem( new LibImageVersionBean(i,view.getVersionLabel(i)) );			
			}
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}