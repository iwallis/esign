// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document.rule;

import java.util.HashSet;
import java.util.List;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DocumentProgrammingRule;
import com.esignforms.open.prog.DocumentVersionPage;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.runtime.action.Action;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.themes.Reindeer;

/**
 * Based on PackageProgrammingRuleList as of 5/23/2012.
 * @author Yozons, Inc.
 */
public class DocumentProgrammingRuleList extends Panel {
	private static final long serialVersionUID = -655086715422030755L;

	final DocumentProgrammingRuleView view;
	DocumentProgrammingRuleContainer container;
	DocumentProgrammingRuleTable table;
	
	// For search bar
	Button createNewButton;

	public DocumentProgrammingRuleList(final DocumentProgrammingRuleView view, final DocumentProgrammingRuleContainer container) {
		super();
		this.view = view;
		setStyleName("LibDocumentProgrammingRuleList");
		setSizeFull();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
        VerticalLayout layout = new VerticalLayout();
        setContent(layout);
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
    	
		HorizontalLayout searchBar = new HorizontalLayout();
		searchBar.setWidth(100, Unit.PERCENTAGE);
    	searchBar.addStyleName("searchBar");
    	searchBar.setSpacing(true);
    	searchBar.setMargin(false);

    	layout.addComponent(searchBar);

    	createNewButton = new Button(vaadinUi.getMsg("LibDocumentProgrammingRuleView.searchBar.createNewButton.label"));
    	createNewButton.setStyleName(Reindeer.BUTTON_SMALL);
    	createNewButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentProgrammingRuleView.searchBar.createNewButton.icon")));
    	createNewButton.setDescription(vaadinUi.getMsg("LibDocumentProgrammingRuleView.searchBar.createNewButton.tooltip"));
    	createNewButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = -3500031033881641011L;

			@Override
			public void buttonClick(ClickEvent event) {
				view.createNew();
			}
		});
    	searchBar.addComponent(createNewButton);
    	searchBar.setComponentAlignment(createNewButton, Alignment.MIDDLE_RIGHT);

    	table = new DocumentProgrammingRuleTable(view, container);
    	table.initializeDND();
    	
    	layout.addComponent(searchBar);
    	layout.addComponent(table);
    	layout.setExpandRatio(table, 1);
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		createNewButton.setVisible(!v);
		table.setDragMode( v ? TableDragMode.NONE : TableDragMode.ROW);
	}
	
	public Table getTable() {
		return table;
	}
	
	class DocumentProgrammingRuleTable extends Table {
		private static final long serialVersionUID = 3367462499675559471L;

		DocumentProgrammingRuleContainer container;
		
		public DocumentProgrammingRuleTable(DocumentProgrammingRuleView view, DocumentProgrammingRuleContainer container) {
			super();
			this.container = container;
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("LibDocumentProgrammingRuleList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("LibDocumentProgrammingRuleList.showColumnHeaders"));
			setSortEnabled(false);
			setColumnAlignment("order", Align.CENTER);
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);

	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener((Property.ValueChangeListener)view);
	        setNullSelectionAllowed(true);
			setSizeFull();
		}
		
		public void initializeDND() {
			setDropHandler(new DropHandler() {
				private static final long serialVersionUID = 6373537408112597329L;

				public void drop(DragAndDropEvent dropEvent) {
	                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
	                
	                Container sourceContainer = t.getSourceContainer();
	                if ( sourceContainer == container ) {
	                	// reordering within the table rows
	                	DocumentProgrammingRule sourceItemId = (DocumentProgrammingRule)t.getItemId();

		                AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails)dropEvent.getTargetDetails());
		                DocumentProgrammingRule targetItemId = (DocumentProgrammingRule)dropData.getItemIdOver();
		                
		                // No move if source and target are the same
		                if ( sourceItemId == targetItemId )
		                	return;
		                
		                // Let's remove the source of the drag so we can add it back where requested...
		                container.removeItem(sourceItemId);
		                if ( targetItemId == null ) {
		                	container.addItem(sourceItemId);
		                } else if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
		                	container.addItemAfter(targetItemId,sourceItemId);
		            	} else {
		                    Object prevItemId = container.prevItemId(targetItemId);
		                    container.addItemAfter(prevItemId, sourceItemId);
		            	}
		                
		                view.form.saveRules();
	                }
	            }

	            public AcceptCriterion getAcceptCriterion() {
	                return new And(SourceIsTarget.get(), new Not(AbstractSelect.VerticalLocationIs.MIDDLE)); // dragging within the table
	            }
	        });
			
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			if ( "onEventEsfNames".equals(colId) ) {
				@SuppressWarnings("unchecked")
				HashSet<EsfName> set = (HashSet<EsfName>)property.getValue();
				StringBuilder buf = new StringBuilder(256);
				for( EsfName name : set ) {
					if ( buf.length() > 0 )
						buf.append(", ");
					buf.append(vaadinUi.getPrettyCode().eventName(name));
				}
				return buf.toString();
			}
			if ( "onPageIds".equals(colId) ) {
				@SuppressWarnings("unchecked")
				HashSet<EsfUUID> set = (HashSet<EsfUUID>)property.getValue();
				StringBuilder buf = new StringBuilder(256);
				for( EsfUUID id : set ) {
					DocumentVersionPage page = view.duplicatedDocumentVersion.getPageById(id);
					if ( page == null ) continue;
					if ( buf.length() > 0 )
						buf.append(", ");
					buf.append(page.getEsfName());
				}
				return buf.toString();
			}
			if ( "onPartyIds".equals(colId) ) {
				@SuppressWarnings("unchecked")
				HashSet<EsfUUID> set = (HashSet<EsfUUID>)property.getValue();
				StringBuilder buf = new StringBuilder(256);
				for( EsfUUID id : set ) {
					PartyTemplate partyTemplate = view.duplicatedDocumentVersion.getPartyTemplate(id);
					if ( partyTemplate == null ) continue;
					if ( buf.length() > 0 )
						buf.append(", ");
					buf.append(partyTemplate.getEsfName());
				}
				return buf.toString();
			}
			if ( "actions".equals(colId) ) {
				@SuppressWarnings("unchecked")
				List<Action> list = (List<Action>)property.getValue();
				StringBuilder buf = new StringBuilder(256);
				for( Action action : list ) {
					if ( buf.length() > 0 )
						buf.append(", ");
					buf.append(vaadinUi.getPrettyCode().actionName(action.getEsfName()));
				}
				return buf.toString();
			}
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // DocumentProgrammingRuleTable

} // DocumentProgrammingRuleList
