// Copyright (C) 2012-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.reportFieldTemplate;

import java.io.Serializable;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.user.User;


public class PackageVersionBean implements Serializable {
	private static final long serialVersionUID = -2753280574412841504L;

	EsfPathName pathName;
	int version;
	String versionLabel;
	
	public PackageVersionBean(PackageVersion pkgVersion, User user) {
		Application app = Application.getInstance();
		Package pkg = pkgVersion.getPackage();
		boolean userCanList = pkg.canUserList(user);
		this.pathName = userCanList ? pkg.getPathName() : new EsfPathName(app.getServerMessages().getString("permission.userCannotList.esfName"));
		this.version = pkgVersion.getVersion();
		if ( pkg.getProductionVersion() == version )
			this.versionLabel = app.getServerMessages().getString("version.production.label");
		else if ( pkg.getTestVersion() == version )
			this.versionLabel = app.getServerMessages().getString("version.test.label");
		else
			this.versionLabel = app.getServerMessages().getString("version.notcurrent.label");		
	}
	
	public EsfPathName getPathName() { return pathName; }
	public int getVersion() { return version; }
	public String getVersionLabel() { return versionLabel; }
}