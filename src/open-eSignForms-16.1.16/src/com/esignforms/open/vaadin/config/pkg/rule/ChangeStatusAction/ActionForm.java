// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.ChangeStatusAction;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.runtime.action.ChangeStatusAction;
import com.esignforms.open.runtime.action.ChangeStatusAction.Spec;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.FieldSpecTextField;
import com.esignforms.open.vaadin.widget.NativeSelectDropDown;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.RegexpValidator;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author Yozons Inc.
 */
public class ActionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 3823525558629288269L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ActionForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;

	GridLayout layout;
	
	final PackageVersion duplicatedPackageVersion;
	final ChangeStatusAction duplicatedAction;
	final ActionView view;
	final ActionContainer container;
	
	List<EsfUUID> allDocumentIds;
	List<EsfName> allDocumentEsfNames;
	
	List<EsfUUID> allPartyTemplateIds;
	List<EsfName> allPartyTemplateEsfNames;
	
	HorizontalLayout cancelTransactionTimeLayout;
	HorizontalLayout cancelTransactionRetentionLayout;
	
	Spec newSpec; // when set, we're working on a new spec.
	
	
	public ActionForm(ActionView view, final ActionContainer container, PackageVersion duplicatedPackageVersionParam, ChangeStatusAction duplicatedActionParam)	{
		setStyleName("ChangeStatusActionForm");
		this.view = view;
		this.container = container;
		this.duplicatedPackageVersion = duplicatedPackageVersionParam;
		this.duplicatedAction = duplicatedActionParam;
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		allDocumentIds = duplicatedPackageVersion.getDocumentIdList();
		allDocumentEsfNames = duplicatedPackageVersion.getDocumentNameList();
    	
		allPartyTemplateIds = new LinkedList<EsfUUID>();
		allPartyTemplateEsfNames = new LinkedList<EsfName>();
    	for( PackageVersionPartyTemplate pvpt : duplicatedPackageVersion.getPackageVersionPartyTemplateList() ) {
    		if ( ! pvpt.isEsfReportsAccess() ) { // cannot activate/skip or the like our built-in reports access party
        		allPartyTemplateIds.add(pvpt.getId());
        		allPartyTemplateEsfNames.add(pvpt.getEsfName());
    		}
    	}
    	
    	// Setup layout
    	layout = new GridLayout(5,2);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.0f);
    	layout.setColumnExpandRatio(1, 0.2f);
    	layout.setColumnExpandRatio(2, 0.8f);
    	layout.setColumnExpandRatio(3, 0f);
    	layout.setColumnExpandRatio(4, 0f);
    	setLayout(layout);
    	
    	cancelTransactionTimeLayout = new HorizontalLayout();
    	cancelTransactionTimeLayout.setMargin(false);
    	cancelTransactionTimeLayout.setSpacing(true);
        layout.addComponent(cancelTransactionTimeLayout, 1, 0);
        
        cancelTransactionRetentionLayout = new HorizontalLayout();
        cancelTransactionRetentionLayout.setMargin(false);
        cancelTransactionRetentionLayout.setSpacing(true);
        layout.addComponent(cancelTransactionRetentionLayout, 0, 1, 3, 1);

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
        setBuffered(true);
    	
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    	footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 8442050151425866626L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

				if (propertyId.equals("statusChangeType")) { // this field is always present and determines which of the other fields are visible and active
                	NativeSelect select = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.statusChangeType.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.statusChangeType.tooltip"));
                	select.setNullSelectionAllowed(false);
                	select.setRequired(true);
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	                select.addValidator(new SelectValidator(select));
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);

                	select.addItem(ChangeStatusAction.CANCEL_TRANSACTION_TYPE);
                	select.setItemCaption(ChangeStatusAction.CANCEL_TRANSACTION_TYPE, 
                			vaadinUi.getPrettyCode().changeStatusActionType(ChangeStatusAction.CANCEL_TRANSACTION_TYPE));
                	select.addItem(ChangeStatusAction.CLEAR_CANCEL_TRANSACTION_TYPE);
                	select.setItemCaption(ChangeStatusAction.CLEAR_CANCEL_TRANSACTION_TYPE, 
                			vaadinUi.getPrettyCode().changeStatusActionType(ChangeStatusAction.CLEAR_CANCEL_TRANSACTION_TYPE));
                	select.addItem(ChangeStatusAction.SUSPEND_TRANSACTION_TYPE);
                	select.setItemCaption(ChangeStatusAction.SUSPEND_TRANSACTION_TYPE, 
                			vaadinUi.getPrettyCode().changeStatusActionType(ChangeStatusAction.SUSPEND_TRANSACTION_TYPE));
                	select.addItem(ChangeStatusAction.RESUME_TRANSACTION_TYPE);
                	select.setItemCaption(ChangeStatusAction.RESUME_TRANSACTION_TYPE, 
                			vaadinUi.getPrettyCode().changeStatusActionType(ChangeStatusAction.RESUME_TRANSACTION_TYPE));
                	select.addItem(ChangeStatusAction.SKIP_DOCUMENT_TYPE);
                	select.setItemCaption(ChangeStatusAction.SKIP_DOCUMENT_TYPE, 
                			vaadinUi.getPrettyCode().changeStatusActionType(ChangeStatusAction.SKIP_DOCUMENT_TYPE));
                	select.addItem(ChangeStatusAction.SKIP_DOCUMENT_PARTY_TYPE);
                	select.setItemCaption(ChangeStatusAction.SKIP_DOCUMENT_PARTY_TYPE, 
                			vaadinUi.getPrettyCode().changeStatusActionType(ChangeStatusAction.SKIP_DOCUMENT_PARTY_TYPE));
                	select.addItem(ChangeStatusAction.SKIP_PARTY_TYPE);
                	select.setItemCaption(ChangeStatusAction.SKIP_PARTY_TYPE, 
                			vaadinUi.getPrettyCode().changeStatusActionType(ChangeStatusAction.SKIP_PARTY_TYPE));
                	select.addItem(ChangeStatusAction.ACTIVATE_PARTY_TYPE);
                	select.setItemCaption(ChangeStatusAction.ACTIVATE_PARTY_TYPE, 
                			vaadinUi.getPrettyCode().changeStatusActionType(ChangeStatusAction.ACTIVATE_PARTY_TYPE));
            
                	select.setImmediate(true);
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 7492300340803897980L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							String type = (String)event.getProperty().getValue();
							setupVariableFields(type);
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("cancelUnit")) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.cancelUnit.label"), vaadinUi.getEsfapp().getDropDownTimeIntervalUnitsEsfName());
					//select.setRequired(true); -- we set this depending on which type is chosen
					select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.cancelUnit.tooltip"));
                	select.setImmediate(true);
			        //select.addValidator(new SelectValidator(select)); -- we set this depending on which type is chosen
			        select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 8846037917842917407L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							Field f = getField("cancelNumUnits");
							if ( f != null ) {
								String value = (String)event.getProperty().getValue();
								boolean isNowOrForever = Literals.TIME_INTERVAL_UNIT_FOREVER.equals(value) || Literals.TIME_INTERVAL_UNIT_NOW.equals(value);
								f.setVisible(! isNowOrForever);
								EsfInteger v = new EsfInteger((String)f.getValue());
								if ( v.isNull() || v.isLessThan(1) || v.isGreaterThan(9999) )
									f.setValue("1");
							}
						}
			        });
	                return select;
				}
				
				if (propertyId.equals("cancelRetentionChange")) {
			    	CheckBox cb = new CheckBox(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.cancelRetentionChange.label"));
			    	cb.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.cancelRetentionChange.tooltip"));
			    	cb.setImmediate(true);
			    	cb.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -5955917284154667744L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							boolean isChecked = (Boolean)event.getProperty().getValue();
							
							Field f = getField("cancelRetentionNumUnits");
							if ( f != null ) {
								f.setEnabled(isChecked);
								f.setRequired(isChecked);
								if ( ! isChecked ) {
									EsfVaadinUI.getInstance().removeAllValidators(f);
								}
							}
							f = getField("cancelRetentionUnit");
							if ( f != null ) {
								f.setEnabled(isChecked);
								f.setRequired(isChecked);
								if ( ! isChecked ) {
									EsfVaadinUI.getInstance().removeAllValidators(f);
								}
							}
						}
			    	});
			    	return cb;
				}
				
				if (propertyId.equals("cancelRetentionUnit")) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.cancelRetentionUnit.label"), vaadinUi.getEsfapp().getDropDownTimeIntervalUnitsEsfName());
					//select.setRequired(true); -- we set this depending on which type is chosen
					select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
					select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.cancelRetentionUnit.tooltip"));
                	select.setImmediate(true);
			        //select.addValidator(new SelectValidator(select)); -- we set this depending on which type is chosen
			        select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -5955917284154667744L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							Field f = getField("cancelRetentionNumUnits");
							if ( f != null ) {
								String value = (String)event.getProperty().getValue();
								boolean isNowOrForever = Literals.TIME_INTERVAL_UNIT_FOREVER.equals(value) || Literals.TIME_INTERVAL_UNIT_NOW.equals(value);
								f.setVisible(! isNowOrForever);
								EsfInteger v = new EsfInteger((String)f.getValue());
								if ( v.isNull() || v.isLessThan(1) || v.isGreaterThan(9999) )
									f.setValue("1");
							}
						}
			        });
	                return select;
				}
				
				if (propertyId.equals("documentIdSet")) {
                	final ListSelect select = new ListSelect(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.skipDocumentIdSet.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.skipDocumentIdSet.tooltip"));
                	//select.setNullSelectionAllowed(false); -- we set this depending on which type is chosen
					//select.setRequired(true); -- we set this depending on which type is chosen
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	select.setMultiSelect(true);
                	select.setRows( Math.min(10, allDocumentIds.size()));
                	select.setImmediate(true);
	                //select.addValidator(new SelectValidator(select)); -- we set this depending on which type is chosen
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	
                	ListIterator<EsfName> nameIter = allDocumentEsfNames.listIterator();
                	for( EsfUUID docId : allDocumentIds ) {
                		EsfName docName = nameIter.next();
                		select.addItem(docId);
                		select.setItemCaption(docId, docName.toString());
                	}
                	
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = 1131516150583083038L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							// When our select changes, we check for any values that shouldn't be there. This is typically because a document was removed after the action was setup.
							boolean valuesRemoved = false;
							@SuppressWarnings("unchecked")
							HashSet<EsfUUID> currValues = new HashSet<EsfUUID>((Collection<? extends EsfUUID>) event.getProperty().getValue());
							Iterator<EsfUUID> currValuesIter = currValues.iterator();
							while( currValuesIter.hasNext() ) {
								EsfUUID id = currValuesIter.next();
								if ( ! select.containsId(id) ) {
									currValuesIter.remove();
									valuesRemoved = true;
								}
							}
							if ( valuesRemoved )
								select.setValue(currValues);
						}
                	});
                	return select;
                }
                
				if (propertyId.equals("partyIdSet")) {
                	final ListSelect select = new ListSelect(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.skipPartyIdSet.label"));
                	select.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.skipPartyIdSet.tooltip"));
                	//select.setNullSelectionAllowed(false); -- we set this depending on which type is chosen
					//select.setRequired(true); -- we set this depending on which type is chosen
                	select.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	select.setMultiSelect(true);
                	select.setRows( Math.min(10, allPartyTemplateIds.size()));
                	select.setImmediate(true);
	                //select.addValidator(new SelectValidator(select)); -- we set this depending on which type is chosen
                	select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
                	
                	ListIterator<EsfName> nameIter = allPartyTemplateEsfNames.listIterator();
                	for( EsfUUID partyId : allPartyTemplateIds ) {
                		EsfName partyName = nameIter.next();
                		select.addItem(partyId);
                		select.setItemCaption(partyId, partyName.toString());
                	}
                	select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -290912920579875777L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							// When our select changes, we check for any values that shouldn't be there. This is typically because a party was removed after the action was setup.
							boolean valuesRemoved = false;
							@SuppressWarnings("unchecked")
							HashSet<EsfUUID> currValues = new HashSet<EsfUUID>((Collection<? extends EsfUUID>) event.getProperty().getValue());
							Iterator<EsfUUID> currValuesIter = currValues.iterator();
							while( currValuesIter.hasNext() ) {
								EsfUUID id = currValuesIter.next();
								if ( ! select.containsId(id) ) {
									currValuesIter.remove();
									valuesRemoved = true;
								}
							}
							if ( valuesRemoved )
								select.setValue(currValues);
						}
                	});
                	return select;
                }
				
				if ( propertyId.equals("reasonSpec") ) { // this field is always present
					FieldSpecTextField tf = new FieldSpecTextField();
                	tf.setWidth(100, Unit.PERCENTAGE);
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.reasonSpec.label"));
                    tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.reasonSpec.tooltip"));
                    return tf;
                }
                
				Field field = super.createField(item, propertyId, uiContext);
    			
                if (propertyId.equals("cancelNumUnits")) {
	            	TextField tf = (TextField)field;
	            	//tf.setRequired(true); -- we set this depending on which type is chosen
	                tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	            	tf.setColumns(2);
	            	tf.setMaxLength(4);
	            	tf.setNullRepresentation("1");
	            	tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.cancelNumUnits.label"));
	            	tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.cancelUnit.tooltip"));
	            	tf.setImmediate(true);
	            	//tf.addValidator(new RegexpValidator("[1-9][0-9]{0,3}", vaadinUi.getMsg("tooltip.invalid.integer.range","1-9999"))); -- we set this depending on which type is chosen
                } else if (propertyId.equals("cancelRetentionNumUnits")) {
	            	TextField tf = (TextField)field;
	            	//tf.setRequired(true); -- we set this depending on which type is chosen
	                tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	            	tf.setColumns(2);
	            	tf.setMaxLength(4);
	            	tf.setNullRepresentation("1");
	            	tf.setCaption(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.cancelRetentionNumUnits.label"));
	            	tf.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.cancelRetentionUnit.tooltip"));
	            	tf.setImmediate(true);
	            	//tf.addValidator(new RegexpValidator("[1-9][0-9]{0,3}", vaadinUi.getMsg("tooltip.invalid.integer.range","1-9999"))); -- we set this depending on which type is chosen
	    	    }
                return field;
			}
    	 });

    	_logger.debug("Form created");

	}
	
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		if ( getItemDataSource() == null ) {
    			saveSpecs();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
			Spec spec = getCurrentBean();
			if ( spec.isCancelTransactionType() && Literals.TIME_INTERVAL_UNIT_FOREVER.equals(spec.getCancelUnit()) )
				vaadinUi.showWarning(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.cancelUnit.label"), vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.cancelUnit.forever.warning"));
			
    		if ( newSpec != null ) {
    			container.addItem(newSpec);
    			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.message.spec.name")) );
    			newSpec = null;
    		} else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.message.spec.name")) );
    		}
    		
			saveSpecs();
    		
    		view.select(getCurrentBean()); // reselect our bean so all updates cleanly
        } else if ( source == cancelButton ) {
    		discard();
    		if ( getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			setNewSpecAsDataSource(null);
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	view.closeParentWindow();
        } else if ( source == createLikeButton ) {
        	discard();
        	Spec spec = getCurrentBean();
        	if ( spec != null ) {
    			view.unselectAll();
    			setNewSpecAsDataSource(spec.duplicate());
        	}
        } else if ( source == deleteButton ) {
        	discard();
        	
        	if ( newSpec != null ) {
        		newSpec = null;
        	} else {
            	Spec spec = getCurrentBean();
            	if ( spec != null ) {
            		container.removeItem(spec);
        			saveSpecs();
            	}
        	}
    		view.unselectAll();
			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.message.spec.name")) );
        }
    }
	
	void saveSpecs() {
		view.specListUpdated();
	}
	
	Spec getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public Spec getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<Spec> bi = (BeanItem<Spec>)dataSource;
		return bi.getBean();
    }
    
    public void setNewSpecAsDataSource(Spec spec) {
    	newSpec = spec;
    	if ( newSpec == null )
    		setItemDataSource(null);
    	else
    		setItemDataSource(new BeanItem<Spec>(newSpec));
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		if ( newDataSource != null ) {
    		super.setItemDataSource(newDataSource, Arrays.asList("statusChangeType","cancelNumUnits","cancelUnit","reasonSpec","documentIdSet","partyIdSet",
    				"cancelRetentionChange","cancelRetentionNumUnits","cancelRetentionUnit"));
    		layout.setVisible(true);
    		setupVariableFields(getCurrentBean().getStatusChangeType());
		} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
		}
		setReadOnly(isReadOnly());
    }

	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	Spec bean = getCurrentBean();
    	
    	saveButton.setVisible(!readOnly && bean != null);
    	cancelButton.setVisible(!readOnly && bean != null);
    	closeButton.setVisible(true);
    	createLikeButton.setVisible(!readOnly && bean != null && newSpec == null);
    	deleteButton.setVisible(!readOnly && bean != null);
    }
    
	void setupVariableFields(String statusChangeType) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		Field<?> f = getField("partyIdSet");
		if ( f != null ) {
			ListSelect s = (ListSelect)f;
			vaadinUi.removeAllValidators(s);

			boolean isSkip = ChangeStatusAction.SKIP_PARTY_TYPE.equals(statusChangeType);
			boolean isSkipDocumentParty = ChangeStatusAction.SKIP_DOCUMENT_PARTY_TYPE.equals(statusChangeType);
			boolean isActivate = ChangeStatusAction.ACTIVATE_PARTY_TYPE.equals(statusChangeType);
			s.setVisible(isSkip || isSkipDocumentParty || isActivate);
			s.setRequired(isSkip || isSkipDocumentParty || isActivate);
			s.setNullSelectionAllowed(!isSkip && !isSkipDocumentParty && !isActivate);
			
			if ( isSkip ) {
				s.setCaption(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.skipPartyIdSet.label"));
				s.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.skipPartyIdSet.tooltip"));
			} else if ( isSkipDocumentParty ) {
				s.setCaption(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.skipDocumentPartyPartyIdSet.label"));
				s.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.skipDocumentPartyPartyIdSet.tooltip"));
			} else if ( isActivate ) {
				s.setCaption(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.activatePartyIdSet.label"));
				s.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.activatePartyIdSet.tooltip"));
			}
			
			if ( isSkip || isSkipDocumentParty || isActivate ) 
				s.addValidator(new SelectValidator(s));
		}

		f = getField("documentIdSet");
		if ( f != null ) {
			ListSelect s = (ListSelect)f;
			vaadinUi.removeAllValidators(s);
			boolean isSkip = ChangeStatusAction.SKIP_DOCUMENT_TYPE.equals(statusChangeType);
			boolean isSkipDocumentParty = ChangeStatusAction.SKIP_DOCUMENT_PARTY_TYPE.equals(statusChangeType);
			s.setVisible(isSkip || isSkipDocumentParty);
			s.setRequired(isSkip || isSkipDocumentParty);
			s.setNullSelectionAllowed(!isSkip && !isSkipDocumentParty);
			s.setImmediate(isSkip || isSkipDocumentParty);
			
			if ( isSkip ) {
				s.setCaption(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.skipDocumentIdSet.label"));
				s.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.skipDocumentIdSet.tooltip"));
			} else if ( isSkipDocumentParty ) {
				s.setCaption(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.skipDocumentPartyDocumentIdSet.label"));
				s.setDescription(vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.form.skipDocumentPartyDocumentIdSet.tooltip"));
			}
			
			if ( isSkip || isSkipDocumentParty ) 
				s.addValidator(new SelectValidator(s));
		}

		boolean isCancel = ChangeStatusAction.CANCEL_TRANSACTION_TYPE.equals(statusChangeType);
		cancelTransactionTimeLayout.setVisible(isCancel);
		cancelTransactionRetentionLayout.setVisible(isCancel);
		f = getField("cancelNumUnits");
		if ( f != null ) {
			vaadinUi.removeAllValidators(f);
			f.setRequired(isCancel);
			if ( isCancel ) {
            	f.addValidator(new RegexpValidator("[1-9][0-9]{0,3}", vaadinUi.getMsg("tooltip.invalid.integer.range","1-9999")));
			}
		}
		f = getField("cancelUnit");
		if ( f != null ) {
			vaadinUi.removeAllValidators(f);
			f.setRequired(isCancel);
			if ( isCancel ) {
				NativeSelectDropDown select = (NativeSelectDropDown)f;
				select.addValidator(new SelectValidator(select));
			}
		}
		
		boolean isCancelRetentionChange = false;
		f = getField("cancelRetentionChange");
		if ( f != null )
			isCancelRetentionChange = isCancel && (Boolean)f.getValue();
		
		f = getField("cancelRetentionNumUnits");
		if ( f != null ) {
			vaadinUi.removeAllValidators(f);
			f.setRequired(isCancelRetentionChange);
			f.setEnabled(isCancelRetentionChange);
			if ( isCancelRetentionChange ) {
            	f.addValidator(new RegexpValidator("[1-9][0-9]{0,3}", vaadinUi.getMsg("tooltip.invalid.integer.range","1-9999")));
			}
		}
		f = getField("cancelRetentionUnit");
		if ( f != null ) {
			vaadinUi.removeAllValidators(f);
			f.setRequired(isCancelRetentionChange);
			f.setEnabled(isCancelRetentionChange);
			if ( isCancelRetentionChange ) {
				NativeSelectDropDown select = (NativeSelectDropDown)f;
				select.addValidator(new SelectValidator(select));
			}
		}
	}
	
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("statusChangeType")) {
            layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("cancelNumUnits")) {
        	cancelTransactionTimeLayout.removeAllComponents();
        	cancelTransactionTimeLayout.addComponent(field);
        } else if (propertyId.equals("cancelUnit")) {
        	cancelTransactionTimeLayout.addComponent(field);
        	cancelTransactionTimeLayout.setComponentAlignment(field, Alignment.BOTTOM_LEFT);
        } else if (propertyId.equals("cancelRetentionChange")) {
        	cancelTransactionRetentionLayout.removeAllComponents();
        	cancelTransactionRetentionLayout.addComponent(field);
        	cancelTransactionRetentionLayout.setComponentAlignment(field, Alignment.MIDDLE_CENTER);
        } else if (propertyId.equals("cancelRetentionNumUnits")) {
        	cancelTransactionRetentionLayout.addComponent(field);
        	cancelTransactionRetentionLayout.setComponentAlignment(field, Alignment.BOTTOM_CENTER);
        } else if (propertyId.equals("cancelRetentionUnit")) {
        	cancelTransactionRetentionLayout.addComponent(field);
        	cancelTransactionRetentionLayout.setComponentAlignment(field, Alignment.BOTTOM_CENTER);
        } else if (propertyId.equals("reasonSpec")) {
        	layout.addComponent(field, 2, 0);
        } else if (propertyId.equals("documentIdSet")) {
        	layout.addComponent(field, 3, 0); 
        } else if (propertyId.equals("partyIdSet")) {
        	layout.addComponent(field, 4, 0); 
        }
    }
	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageProgrammingRule.ChangeStatusAction.view.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		Field<?> f = getField("statusChangeType");
		if ( f != null ) {
			setupVariableFields((String)f.getValue()); // we do this to discard any fields that may be updated but don't matter for our type
		}
		return isModified() || newSpec != null;
	}
	
}