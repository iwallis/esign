// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.reportField;

import java.io.Serializable;
import java.util.List;

import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.PackageVersionReportField;
import com.vaadin.data.util.BeanItemContainer;


public class PackageReportFieldContainer extends BeanItemContainer<PackageVersionReportField> implements Serializable {
	private static final long serialVersionUID = 3052563774742695715L;

	public PackageReportFieldContainer(List<PackageVersionReportField> fieldsList) throws InstantiationException, IllegalAccessException {
		super(PackageVersionReportField.class);
		refresh(fieldsList);
	}
	
	public void refresh(List<PackageVersionReportField> fieldsList) {
		removeAllItems();
		for( PackageVersionReportField field : fieldsList ) {
			addItem(field);
		}
	}
	
	public short getOrderOfReportFieldTemplateId(EsfUUID reportFieldTemplateId) {
		for( PackageVersionReportField field : getItemIds() ) {
			if ( field.getReportFieldTemplateId().equals(reportFieldTemplateId) )
				return field.getFieldOrder();
		}
		return -1;
	}
	
	public short getOrderOfReportFieldTemplateName(EsfName reportFieldTemplateName) {
		for( PackageVersionReportField field : getItemIds() ) {
			if ( field.getReportFieldTemplate().getFieldName().equals(reportFieldTemplateName) )
				return field.getFieldOrder();
		}
		return -1;
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}