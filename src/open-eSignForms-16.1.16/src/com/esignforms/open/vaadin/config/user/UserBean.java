// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.user;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;

public class UserBean implements Serializable, Comparable<UserBean> {
	private static final long serialVersionUID = 8933114336966477015L;

	private User user;
	
	// This contains all of the member groups that the user can update, so these are the only ones that
	// can be used to add/remove its user record to be a member of these groups
	private EsfUUID[] allMemberGroupsUpdateIds;
	private String[] allMemberGroupsUpdatePathNames;
	private Set<EsfUUID> memberGroupsUpdateIds;

	LinkedList<Group> groupsToSave = new LinkedList<Group>();
	
	public UserBean(User user) {
		this.user = user;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof UserBean )
			return user.equals(((UserBean)o).user);
		return false;
	}
	@Override
    public int hashCode() {
    	return user.hashCode();
    }
	public int compareTo(UserBean u) {
		return user.compareTo(u.user);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public User user() {
		return user;
	}

	public UserBean createLike() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		User newUser = User.Manager.createNewUserLike(user, user.getEmail(), user.getPersonalName(), user.getFamilyName(), vaadinUi.getUser());
	    return new UserBean(newUser);
	}
	
	public boolean save(Errors errors) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
        ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
        	boolean userIsNew = user.doInsert();
        	
    		boolean ret = user.save(con,vaadinUi.getUser());
    		if ( ret ) {
    			if ( userIsNew ) {
           			for( Group g : user.getMemberOfGroups() ) {
        				g.save(con,vaadinUi.getUser());
        			}
    			}
    			else {
           			for( Group g : groupsToSave ) {
        				g.save(con,vaadinUi.getUser());
        			}
    			}
    			
                con.commit();

    			resetCaches();
                return true;
     		} 
    		
			if ( errors != null ) {
				errors.addError(vaadinUi.getMsg("form.save.failed.message"));
			}
            con.rollback();
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
        	vaadinUi.getEsfapp().cleanupPool(pool,con,null);
        }
        return false;
	}
	public boolean save() {
		return save(null);
	}
	
	public boolean delete(Errors errors) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return user.delete(errors,vaadinUi.getUser());
	}

	protected HashSet<EsfUUID> createGroupIdHashSet( Collection<Group> list ) {
		HashSet<EsfUUID> set = new HashSet<EsfUUID>(list.size());
		for( Group g : list ) {
			set.add(g.getId());
		}
		return set;
	}
	
	private synchronized void resetCaches() {
		allMemberGroupsUpdateIds = null;
		allMemberGroupsUpdatePathNames = null;
		memberGroupsUpdateIds = null;
	}

	protected synchronized void createAllMemberGroupsUpdateLists() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Collection<Group> list = Group.Manager.getForUserWithUpdateMemberUsersPermission(vaadinUi.getUser(),Group.INCLUDE.BOTH_ENABLED_AND_DISABLED);
		
		int size = list.size();
		
		allMemberGroupsUpdateIds = new EsfUUID[size];
		allMemberGroupsUpdatePathNames = new String[size];
		int i = 0;
		for( Group g : list ) {
			allMemberGroupsUpdateIds[i] = g.getId();
			allMemberGroupsUpdatePathNames[i] = g.isDisabled() ? (g.getPathName() + EsfPathName.PATH_SEPARATOR + vaadinUi.getMsg("PrettyCode.status.disabled")) : g.getPathName().toString();
			++i;
		}
	}
	public EsfUUID[] allMemberGroupsUpdateIds() {
		if ( allMemberGroupsUpdateIds == null ) {
			createAllMemberGroupsUpdateLists();
		}
		return allMemberGroupsUpdateIds;
	}
	public String[] allMemberGroupsUpdatePathNames() {
		if ( allMemberGroupsUpdatePathNames == null ) {
			createAllMemberGroupsUpdateLists();
		}
		return allMemberGroupsUpdatePathNames;
	}

	public Set<EsfUUID> getMemberGroupsUpdateIds() {
		if ( memberGroupsUpdateIds == null ) {
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			memberGroupsUpdateIds = createGroupIdHashSet( user.getMemberOfGroups(vaadinUi.getUser(),PermissionOption.PERM_OPTION_UPDATE) );
		}
		return memberGroupsUpdateIds;
	}
	
	public void setMemberGroupsUpdateIds(Set<EsfUUID> v) {
		// If this is a newly created user, he's not really a member of any group yet, so we'll just manipulate the memberOfGroups list
		// directly as that list will be saved on insert and he'll be added to the groups then.
		if ( user.doInsert() ) {
			// First, let's remove all groups 
			user._clearMemberOfGroupsDuringCreateLikeOnly();
			// Then let's add back in the selected set
			for( EsfUUID gid : v ) {
				Group g = Group.Manager.getById( gid );
				if ( g != null ) {
					user._addToMemberOfGroupsDuringCreateLikeOnly(g);
				}
			}
			return;
		}
		
		
		// For updates, we'll actually remove him from the groups and remember the modified groups so they can be saved
		// along with the user object itself so they stay in sync.
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();		
		
		// First, let's remove all groups this user can update
		for( Group g : user.getMemberOfGroups(vaadinUi.getUser(),PermissionOption.PERM_OPTION_UPDATE) ) {
			g.removeMemberUser(user);
			if ( ! groupsToSave.contains(g) ) {
				groupsToSave.add(g);
			}
		}
		// Then let's add back in the selected set
		for( EsfUUID gid : v ) {
			Group g = Group.Manager.getById( gid );
			if ( g != null ) {
				g.addMemberUser(user);
				if ( ! groupsToSave.contains(g) ) {
					groupsToSave.add(g);
				}
			}
		}
	}

	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return user.getId();
	}
	
	public boolean isEnabled() { 
		return user.isEnabled();
	}
	public String getStatus() {
		return user.getStatus();
	}
	public void setStatus( String v ) {
		user.setStatus(v);
	}
	
	public EsfString getComments() {
		return user.getComments();
	}
	public void setComments(EsfString v) {
		user.setComments( v );
	}

	public EsfString getTimeZone() {
		return user.getTimezone();
	}
	public void setTimeZone(EsfString v) {
		user.setTimezone(v);
	}

	
	public String getDisplayName() {
		return user.getDisplayName();
	}
	public void setDisplayName(String v) {
		user.setDisplayName(v);
	}
	
	public String getEmail() {
		return user.getEmail();
	}
	public void setEmail(String v) {
		user.setEmail(v);
	}
	
	public String getPersonalName() {
		return user.getPersonalName();
	}
	public void setPersonalName(String v) {
		user.setPersonalName(v);
	}
	
	public String getFamilyName() {
		return user.getFamilyName();
	}
	public void setFamilyName(String v) {
		user.setFamilyName(v);
	}
	
	public String getEmployeeId() {
		return user.getEmployeeId();
	}
	public void setEmployeeId(String v) {
		user.setEmployeeId(v);
	}
	
	public String getJobTitle() {
		return user.getJobTitle();
	}
	public void setJobTitle(String v) {
		user.setJobTitle(v);
	}
	
	public String getLocation() {
		return user.getLocation();
	}
	public void setLocation(String v) {
		user.setLocation(v);
	}
	
	public String getDepartment() {
		return user.getDepartment();
	}
	public void setDepartment(String v) {
		user.setDepartment(v);
	}
	
	public String getPhoneNumber() {
		return user.getPhoneNumber();
	}
	public void setPhoneNumber(String v) {
		user.setPhoneNumber(v);
	}
	
	public String getFullDisplayName() {
		return user.getFullDisplayName();
	}
	
	public EsfDateTime getCreatedTimestamp() {
		return user.getCreatedTimestamp();
	}
	public String formatCreatedTimestamp() {
		return user.getCreatedTimestamp().toString(user());
	}
	public String formatLogCreatedTimestamp() {
		return user.getCreatedTimestamp().toLogString(user());
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return user.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		return user.getLastUpdatedTimestamp().toString(user());
	}
	public String formatLogLastUpdatedTimestamp() {
		return user.getLastUpdatedTimestamp().toLogString(user());
	}
	
	public EsfDateTime getLastLoginTimestamp() {
		EsfDateTime dt = user.getLoginTimestamp();
		if ( dt != null )
			return dt;
		if ( user.hasLastLoginTimestamp() )
			return user.getLastLoginTimestamp();
		return null;
	}
	public String formatLastLoginTimestamp() {
		EsfDateTime dt = user.getLoginTimestamp();
		if ( dt != null ) {
			return user.getLoginTimestamp().toString(user());
		}
		if ( user.hasLastLoginTimestamp() ) {
			return user.getLastLoginTimestamp().toString(user());
		} 
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("userList.neverLoggedIn");
	}
	public String formatLogLastLoginTimestamp() {
		EsfDateTime dt = user.getLoginTimestamp();
		if ( dt != null ) {
			return user.getLoginTimestamp().toLogString(user());
		}
		if ( user.hasLastLoginTimestamp() ) {
			return user.getLastLoginTimestamp().toLogString(user());
		} 
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("userList.neverLoggedIn");
	}

	public String getLastLoginIP() {
		String ip = user.getLoginIP();
		return EsfString.isNonBlank(ip) ? ip : user.getLastLoginIP();
	}

	public EsfString getTimezone() {
		return user.getTimezone();
	}
	public void setTimezone(EsfString v) {
		user.setTimezone(v);
	}
	
	public boolean hasPermViewDetails() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getUser().hasMemberUsersPerm(PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return user.canUserUpdate(vaadinUi.getUser());
	}

	public boolean hasPermCreateLike() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return user.canUserCreateLike(vaadinUi.getUser());
	}

	public boolean hasPermDelete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return user.canUserDelete(vaadinUi.getUser());
	}
}