// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.serial;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.Serial;
import com.esignforms.open.prog.SerialInfo;
import com.esignforms.open.prog.SerialVersionInfo;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

/**
 * LibSerialAndVersionsMainView is a splitpanel that contains the serial view in the left and the serial version view on the right.
 * 
 * Each of those views is itself a splitpanel, containing a table/list on the top to select items, with a form on the bottom to view/edit.
 * 
 * When a new serial is selected in the left view, we propagate that to the right view so it can sync the versions with the selected serial.
 * 
 * @author Yozons
 *
 */
public class LibSerialAndVersionsMainView extends HorizontalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = -7912554210166422484L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibSerialAndVersionsMainView.class);

    EsfUUID libraryId;
    Library library;
    final LibSerialAndVersionsMainView thisView;
	
    VerticalSplitPanel serialSplitPanel;
    LibSerialBeanContainer serialContainer;
    LibSerialList serialList;
    LibSerialForm serialForm;
	
    VerticalSplitPanel serialVerSplitPanel;
    LibSerialVersionBeanContainer serialVerContainer;
	LibSerialVersionList serialVerList;
	LibSerialVersionForm serialVerForm;

	public LibSerialAndVersionsMainView() {
		super(); 
		thisView = this;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		try {
			library = Library.Manager.getById(libraryId, vaadinUi.getUser());
			
			serialContainer = new LibSerialBeanContainer( library.getId());
			serialVerContainer = new LibSerialVersionBeanContainer(this); // Load contents when a serial is selected from our serialContainer
			
			serialList = new LibSerialList(this,serialContainer);
		    serialForm = new LibSerialForm(this,serialContainer);

			serialSplitPanel = new VerticalSplitPanel();
			serialSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			serialSplitPanel.setSplitPosition(30);
			serialSplitPanel.setFirstComponent(serialList);
			serialSplitPanel.setSecondComponent(serialForm);
		    
			serialVerList = new LibSerialVersionList(this,serialVerContainer);
			serialVerForm = new LibSerialVersionForm(this,serialVerContainer);
			
			serialVerSplitPanel = new VerticalSplitPanel();
			serialVerSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			serialVerSplitPanel.setSplitPosition(30);
			serialVerSplitPanel.setFirstComponent(serialVerList);
			serialVerSplitPanel.setSecondComponent(serialVerForm);

			setFirstComponent(serialSplitPanel);
			setSecondComponent(serialVerSplitPanel);
			setSplitPosition(50);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Serial and versions main view exception", e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	private LibSerialBean getLibSerialBean(Item serialListItem) {
    	if ( serialListItem == null )
    		return null;
		BeanItem<LibSerialBean> bi = (BeanItem<LibSerialBean>)serialListItem;
		return bi.getBean();
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our serial list or not
        if (property == serialList.getTable()) {
        	final Item item = serialList.getTable().getItem(serialList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( serialForm.getCurrentBean() == serialForm.getBean(item) ) {
    				// We've selected the same bean so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibSerialAndVersionsMainView.Serial.ConfirmDiscardChangesDialog.message", serialForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						thisView.discardForms();
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						serialList.getTable().select(serialForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibSerialAndVersionsMainView.Serial.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	LibSerialBean bean = getLibSerialBean(item);
                serialForm.setItemDataSource(item);
            	if ( bean != null ) {
                	serialVerContainer.reload(bean.serial());
                	selectFirstSerialVersion();
            	} else {
            		serialVerForm.setItemDataSource(null);
            		serialVerContainer.removeAllItems();
            	}
    		}
        } else if (property == serialVerList.getTable()) {
        	final Item item = serialVerList.getTable().getItem(serialVerList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( serialVerForm.getCurrentBean() == serialVerForm.getBean(item) ) {
    				// We've selected the same bean (version) so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibSerialAndVersionsMainView.Serial.ConfirmDiscardChangesDialog.message", serialForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						serialVerForm.discard();
						serialVerForm.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						serialVerList.getTable().select(serialVerForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibSerialAndVersionsMainView.Serial.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	serialVerForm.setItemDataSource(item);
    		}
        }
        else {
        	_logger.debug("valueChange() - no list event: " + event.toString());
        }
	}
	

	@Override
	public void detach() {
		super.detach();
	}
	
	public void selectSerial(LibSerialBean bean) {
		unselectAllSerials();
		serialList.getTable().select(bean);
		serialList.getTable().setCurrentPageFirstItemId(bean);
		if ( bean == null ) {
			serialVerForm.setItemDataSource(null);
		}
	}
	
	public void unselectAllSerials() {
		serialList.getTable().setValue(null);
		serialForm.setItemDataSource(null);
	}
	
	public void selectSerialVersion(LibSerialVersionBean bean) {
		unselectAllSerialVersions();
		serialVerList.getTable().select(bean);
		serialVerList.getTable().setCurrentPageFirstItemId(bean);
	}

	public void discardForms() {
		serialForm.discard();
		serialVerForm.discard();
		serialForm.setItemDataSource(null);
		serialVerForm.setItemDataSource(null);
	}

	
	public void selectFirstSerialVersion() {
		if ( serialVerContainer.size() > 0 ) {
			selectSerialVersion(serialVerContainer.getIdByIndex(0));
		} else {
			unselectAllSerialVersions(); 
		}
	}
	
	public void unselectAllSerialVersions() {
		serialVerList.getTable().setValue(null);
	}

	public Library getLibrary() {
		return library;
	}
	
	public LibSerialBean getLibSerialBean() {
		return serialForm == null ? null : serialForm.getCurrentBean();
	}
	
	public LibSerialVersionBean getLibSerialVersionBean() {
		return serialVerForm == null ? null : serialVerForm.getCurrentBean();
	}
	
	public String getEsfNameWithVersion() {
		LibSerialBean serialBean = getLibSerialBean();
		LibSerialVersionBean serialVerBean = getLibSerialVersionBean();
		return serialVerBean == null ? "?? [?]" : serialBean.getEsfName() + " [" + serialVerBean.getVersion() + "]";
	}
	
	public Integer getProductionVersion() {
		LibSerialBean serialBean = getLibSerialBean();
		return serialBean == null ? 0 : serialBean.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		LibSerialBean serialBean = getLibSerialBean();
		return serialBean == null ? 0 : serialBean.getTestVersion();
	}

	public boolean hasTestVersion() {
		LibSerialBean serialBean = getLibSerialBean();
		return serialBean == null ? false : serialBean.serial().hasTestVersion();
	}
	
    public boolean isProductionVersion(SerialVersionInfo serialVer)
    {
    	LibSerialBean serialBean = getLibSerialBean();
    	return serialBean != null && serialVer != null && serialVer.getVersion() == serialBean.getProductionVersion();
    }

    public boolean isTestVersion(SerialVersionInfo serialVer)
    {
    	LibSerialBean serialBean = getLibSerialBean();
    	return serialBean != null && serialVer != null && serialVer.getVersion() > serialBean.getProductionVersion();
    }

    // "Current" here means latest production or test version.
    public boolean isCurrentVersion(SerialVersionInfo serialVer)
    {
    	LibSerialBean serialBean = getLibSerialBean();
    	return serialBean != null && serialVer != null && serialVer.getVersion() >= serialBean.getProductionVersion();
    }

    public boolean isOldVersion(SerialVersionInfo serialVer)
    {
    	LibSerialBean serialBean = getLibSerialBean();
    	return serialBean != null && serialVer != null && serialVer.getVersion() < serialBean.getProductionVersion();
    }
	
	public String getVersionLabel(SerialVersionInfo serialVer) {
		if ( isTestVersion(serialVer) )
			return Literals.VERSION_LABEL_TEST;
		if ( isProductionVersion(serialVer) )
			return Literals.VERSION_LABEL_PRODUCTION;
		return Literals.VERSION_LABEL_NOT_CURRENT;
	}
	
	public boolean hasPermViewDetails() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_DELETE);
	}
	
	public boolean cancelChanges() {
		serialForm.discard();
		serialVerForm.discard();
		serialForm.setReadOnly(true);
		serialVerForm.setReadOnly(true);
		return true;
	}
	
	public boolean enterEditMode() {
		serialForm.setReadOnly(false);
		serialVerForm.setReadOnly(false);
		return true;
	}
	
	public boolean save() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
		if ( ! serialForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		if ( ! serialVerForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		serialForm.commit();
		serialVerForm.commit();
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			// If we're saving a new test version, we'll want to bump our serial to show this as the new test version
			LibSerialBean currSerial = getLibSerialBean();
			LibSerialVersionBean currSerialVer = getLibSerialVersionBean();
			if ( currSerialVer.serialVersion().getVersion() > currSerial.serial().getTestVersion() )
				currSerial.serial().bumpTestVersion();
			
			LibSerialBean savedSerial = serialForm.save(con);
			if ( savedSerial == null )  {
				con.rollback();
				return false;
			}
			
			LibSerialVersionBean savedDocVer = serialVerForm.save(con);
			if ( savedDocVer == null )  {
				con.rollback();
				return false;
			}
			
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Saved serial " + savedSerial.getEsfName() + " version " + savedDocVer.getVersion() + ". SerialVersionId: " + savedDocVer.getId() + "; status: " + savedSerial.getStatus());
			}

			con.commit();
	    		
			serialContainer.refresh();  // refresh our list after a change
			selectSerial(savedSerial);
			selectSerialVersion(savedDocVer);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean createNewSerial() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		SerialInfo serialInfo = SerialInfo.Manager.createNew(getLibrary().getId());
		LibSerialBean serialBean = new LibSerialBean(serialInfo);
		BeanItem<LibSerialBean> serialItem = new BeanItem<LibSerialBean>(serialBean);
        serialForm.setItemDataSource(serialItem);
		
		SerialVersionInfo serialVerInfo = SerialVersionInfo.Manager.createNew(Serial.Manager.getById(serialInfo.getId()),vaadinUi.getUser());
		LibSerialVersionBean serialVerBean = new LibSerialVersionBean(serialVerInfo,getVersionLabel(serialVerInfo));
		BeanItem<LibSerialVersionBean> serialVerItem = new BeanItem<LibSerialVersionBean>(serialVerBean);
        serialVerForm.setItemDataSource(serialVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean createLikeSerial() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibSerialBean likeDocBean = getLibSerialBean();
		LibSerialVersionBean likeDocVerBean = getLibSerialVersionBean();
		
    	LibSerialBean createdDocBean = likeDocBean.createLike();
		BeanItem<LibSerialBean> createdDocItem = new BeanItem<LibSerialBean>(createdDocBean);
        serialForm.setItemDataSource(createdDocItem);

    	SerialVersionInfo createdSerialVerInfo = SerialVersionInfo.Manager.createLike(Serial.Manager.getById(createdDocBean.serialInfo().getId()), likeDocVerBean.serialVersion(), vaadinUi.getUser());
    	LibSerialVersionBean createdSerialVerBean = new LibSerialVersionBean(createdSerialVerInfo,Literals.VERSION_LABEL_TEST); // The new serial and version will always be Test.
		BeanItem<LibSerialVersionBean> createdSerialVerItem = new BeanItem<LibSerialVersionBean>(createdSerialVerBean);       
        serialVerForm.setItemDataSource(createdSerialVerItem);
        
        setReadOnly(false);
		return true;
	}

	public boolean createNextVersion() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibSerialBean currSerialBean = getLibSerialBean();
		LibSerialVersionBean currSerialVerBean = getLibSerialVersionBean();
		
        serialForm.setReadOnly(!hasPermUpdate()); // should have permission since doing a create next version...

    	SerialVersionInfo nextSerialVerInfo = SerialVersionInfo.Manager.createLike(currSerialBean.serial(), currSerialVerBean.serialVersion(), vaadinUi.getUser());
    	LibSerialVersionBean nextSerialVerBean = new LibSerialVersionBean(nextSerialVerInfo,Literals.VERSION_LABEL_TEST); // The new version will always be Test.
		BeanItem<LibSerialVersionBean> nextSerialVerItem = new BeanItem<LibSerialVersionBean>(nextSerialVerBean);
        serialVerForm.setItemDataSource(nextSerialVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean promoteTestToProductionVersion() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibSerialVersionForm.versionChange.TestToProd.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibSerialBean serialBean = getLibSerialBean();
			LibSerialVersionBean serialVerBean = getLibSerialVersionBean();
			
			serialBean.serial().promoteTestVersionToProduction();
			
			if ( ! serialBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved serial " + serialBean.getEsfName() + " version " + serialVerBean.getVersion() + " into Production status. SerialVersionId: " + serialVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved serial " + serialBean.getEsfName() + " version " + serialVerBean.getVersion() + " into Production status. SerialVersionId: " + serialVerBean.getId());
			}
			
			con.commit();
			serialContainer.refresh();  // refresh our list after a change
			selectSerial(serialBean);
			selectSerialVersion(serialVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibSerialVersionForm.versionChange.TestToProd.success.message",serialBean.getEsfName(),serialVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean revertProductionVersionBackToTest() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibSerialVersionForm.versionChange.revertProdToTest.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibSerialBean serialBean = getLibSerialBean();
			LibSerialVersionBean serialVerBean = getLibSerialVersionBean();
			
			serialBean.serial().revertProductionVersionBackToTest();
			
			if ( ! serialBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved Production serial " + serialBean.getEsfName() + " version " + serialVerBean.getVersion() + " back into Test status. SerialVersionId: " + serialVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved Production serial " + serialBean.getEsfName() + " version " + serialVerBean.getVersion() + " back into Test status. SerialVersionId: " + serialVerBean.getId());
			}
			
			con.commit();
			serialContainer.refresh();  // refresh our list after a change
			selectSerial(serialBean);
			selectSerialVersion(serialVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibSerialVersionForm.versionChange.revertProdToTest.success.message",serialBean.getEsfName(),serialVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean delete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibSerialBean currSerial = getLibSerialBean();
			LibSerialVersionBean currSerialVer = getLibSerialVersionBean();
			
			// If there is no Production version, we'll get rid of the serial entirely, otherwise just the test version
			Errors errors = new Errors();
			if ( currSerial.getProductionVersion() == 0 )
				currSerial.serial().delete(con,errors,vaadinUi.getUser());
			else {
				currSerialVer.serialVersion().delete(con,errors,vaadinUi.getUser());
				currSerial.dropTestVersion();
				currSerial.save(con, errors);
			}
			
			if ( errors.hasError() ) {
				vaadinUi.show(errors);
				con.rollback();
				return false;
			}
			
			con.commit();
    		
			vaadinUi.showStatus(vaadinUi.getMsg("LibSerialVersionForm.button.delete.successMessage",currSerial.getEsfName(),currSerialVer.getVersion()));

			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Deleted serial " + currSerial.getEsfName() + " version " + currSerialVer.getVersion() + ". SerialVersionId: " + currSerialVer.getId() + "; status: " + currSerial.getStatus());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted serial " + currSerial.getEsfName() + " version " + currSerialVer.getVersion() + ". SerialVersionId: " + currSerialVer.getId() + "; status: " + currSerial.getStatus());
			}

			serialContainer.refresh();  // refresh our list after a change
			selectSerial(null);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		_logger.debug("activateView() - params: " + params);
		libraryId = new EsfUUID(params);
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return serialForm.isDirty() ? serialForm.checkDirty() : serialVerForm.checkDirty();
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return serialForm.isDirty() || serialVerForm.isDirty();
	}
}