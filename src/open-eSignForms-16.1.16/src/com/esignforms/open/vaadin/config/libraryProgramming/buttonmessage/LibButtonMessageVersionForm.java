// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.buttonmessage;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.openesignforms.ckeditor.CKEditorConfig;
import org.vaadin.openesignforms.ckeditor.CKEditorTextField;

import com.esignforms.open.config.CKEditorContext;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.UI;

/**
 * The LibButtonMessageVersionForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class LibButtonMessageVersionForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -1931467633284079489L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibButtonMessageVersionForm.class);
	
	static List<String> _orderedProperties;
	
	final LibButtonMessageVersionForm thisForm;
	final LibButtonMessageAndVersionsMainView view;
	LibButtonMessageVersionBeanContainer container;
	GridLayout layout;
	
	Label id;
	Label createdByInfo;
	Label lastUpdatedByInfo;
	Label versionInfo;
	
	Label packageSection;
	Label documentGeneralSection;
	Label documentEditSection;
	Label documentReviewSection;
	Label documentReadOnlySection;
	Label documentViewOnlySection;
	Label messagesSection;
	
	Button testToProdButton;
	Button createTestFromProdButton;
	Button revertProdToTestButton;
	Button deleteButton;
	
    String editorPackageFooterHtmlContextId;
    String editorDocumentFooterHtmlContextId;

	public LibButtonMessageVersionForm(final LibButtonMessageAndVersionsMainView view, LibButtonMessageVersionBeanContainer container) {
    	setStyleName("LibButtonMessageVersionForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.view = view;
    	this.container = container;
    	this.thisForm = this;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);

    	layout = new GridLayout(2,50);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
		
		versionInfo = new Label();
		versionInfo.setContentMode(ContentMode.TEXT);	
		layout.addComponent(versionInfo,0,0,1,0);
		
		lastUpdatedByInfo = new Label();
		lastUpdatedByInfo.setContentMode(ContentMode.TEXT);
		lastUpdatedByInfo.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedByInfo,0,47,1,47);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,48,1,48);	

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id,0,49,1,49);
		
		packageSection = new Label(vaadinUi.getMsg("LibButtonMessageVersionForm.packageSection.label.html"));
		packageSection.setContentMode(ContentMode.HTML);
		packageSection.setVisible(false);
		layout.addComponent(packageSection,0,3,1,3);

		documentGeneralSection = new Label(vaadinUi.getMsg("LibButtonMessageVersionForm.documentGeneralSection.label.html"));
		documentGeneralSection.setContentMode(ContentMode.HTML);
		documentGeneralSection.setVisible(false);
		layout.addComponent(documentGeneralSection,0,11,1,11);

		documentEditSection = new Label(vaadinUi.getMsg("LibButtonMessageVersionForm.documentEditSection.label.html"));
		documentEditSection.setContentMode(ContentMode.HTML);
		documentEditSection.setVisible(false);
		layout.addComponent(documentEditSection,0,14,1,14);

		documentReviewSection = new Label(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReviewSection.label.html"));
		documentReviewSection.setContentMode(ContentMode.HTML);
		documentReviewSection.setVisible(false);
		layout.addComponent(documentReviewSection,0,20,1,20);

		documentReadOnlySection = new Label(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReadOnlySection.label.html"));
		documentReadOnlySection.setContentMode(ContentMode.HTML);
		documentReadOnlySection.setVisible(false);
		layout.addComponent(documentReadOnlySection,0,27,1,27);

		documentViewOnlySection = new Label(vaadinUi.getMsg("LibButtonMessageVersionForm.documentViewOnlySection.label.html"));
		documentViewOnlySection.setContentMode(ContentMode.HTML);
		documentViewOnlySection.setVisible(false);
		layout.addComponent(documentViewOnlySection,0,33,1,33);

		messagesSection = new Label(vaadinUi.getMsg("LibButtonMessageVersionForm.messagesSection.label.html"));
		messagesSection.setContentMode(ContentMode.HTML);
		messagesSection.setVisible(false);
		layout.addComponent(messagesSection,0,38,1,38);
		
		HorizontalLayout footer = new HorizontalLayout();
		footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);
    	
    	testToProdButton = new Button(vaadinUi.getMsg("LibButtonMessageVersionForm.button.testToProd.label"), (ClickListener)this);
    	testToProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibButtonMessageVersionForm.button.testToProd.icon")));
    	testToProdButton.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.testToProd.tooltip"));
    	footer.addComponent(testToProdButton);

    	createTestFromProdButton = new Button(vaadinUi.getMsg("LibButtonMessageVersionForm.button.createTestFromProd.label"), (ClickListener)this);
    	createTestFromProdButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibButtonMessageVersionForm.button.createTestFromProd.icon")));
    	createTestFromProdButton.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.createTestFromProd.tooltip"));
    	footer.addComponent(createTestFromProdButton);

    	revertProdToTestButton = new Button(vaadinUi.getMsg("LibButtonMessageVersionForm.button.revertProdToTest.label"), (ClickListener)this);
    	revertProdToTestButton.addStyleName("caution");
    	revertProdToTestButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibButtonMessageVersionForm.button.revertProdToTest.icon")));
    	revertProdToTestButton.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.revertProdToTest.tooltip"));
    	footer.addComponent(revertProdToTestButton);

    	if ( view.hasPermDelete() ) {
    		deleteButton = new Button(vaadinUi.getMsg("LibButtonMessageVersionForm.button.delete.label"), (ClickListener)this);
    		deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibButtonMessageVersionForm.button.delete.icon")));
    		deleteButton.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.delete.tooltip"));
	    	deleteButton.addStyleName("deleteButton");
	    	deleteButton.addStyleName("caution");
	    	footer.addComponent(deleteButton);
		}
    	
    	setFooter(footer);
    	setReadOnly(true);   	

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = -6555797148394769786L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				
                if (propertyId.equals("packageFooterHtml")) {
                	vaadinUi.getUser().releaseCKEditorContext(vaadinUi.getHttpSession(),editorPackageFooterHtmlContextId);
                	editorPackageFooterHtmlContextId = vaadinUi.getUser().createCKEditorContext(vaadinUi.getHttpSession());
                	
            		CKEditorContext editorContext = vaadinUi.getUser().getCKEditorContext(vaadinUi.getHttpSession(),editorPackageFooterHtmlContextId);
            		if ( editorContext != null ) { // it should always be there since we just created it above
            			editorContext.setLibrary(view.getLibrary());
            		} else {
            			_logger.error("Failed to find the CKEditorContext for user " + vaadinUi.getUser().getFullDisplayName() + "; with context id: " + editorPackageFooterHtmlContextId);
            		}

                	CKEditorConfig config = new CKEditorConfig();
                	config.setupForOpenESignForms(vaadinUi.getRequestContextPath(),editorPackageFooterHtmlContextId,null);
                	config.setHeight("70px");
                	CKEditorTextField editor= new CKEditorTextField(config);
                	editor.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.package.footer.html.label"));
                	editor.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.package.footer.html.tooltip"));
                	editor.setImmediate(true);
                	editor.setRequired(false);
                	editor.setHeight(180,Unit.PIXELS); // bigger than our config.setHeight to accommodate the toolbar and footer
                	editor.setReadOnly(false);
                	/*
                	editor.addFocusListener( new FieldEvents.FocusListener() {
						private static final long serialVersionUID = -3612693745471985954L;

						@Override
						public void focus(FocusEvent event) {
							if ( view.buttonmessageForm.saveButton != null )
								view.buttonmessageForm.saveButton.removeClickShortcut();
						}
	            	});
                	editor.addBlurListener( new FieldEvents.BlurListener() {
						private static final long serialVersionUID = 1531661693061949179L;

						@Override
						public void blur(BlurEvent event) {
							if ( view.buttonmessageForm.saveButton != null )
								view.buttonmessageForm.saveButton.setClickShortcut(KeyCode.ENTER);
						}
					});
					*/
                    return editor;
                } 
                
                if (propertyId.equals("documentFooterHtml")) {
                	vaadinUi.getUser().releaseCKEditorContext(vaadinUi.getHttpSession(),editorDocumentFooterHtmlContextId);
                	editorDocumentFooterHtmlContextId = vaadinUi.getUser().createCKEditorContext(vaadinUi.getHttpSession());
                	
            		CKEditorContext editorContext = vaadinUi.getUser().getCKEditorContext(vaadinUi.getHttpSession(),editorDocumentFooterHtmlContextId);
            		if ( editorContext != null ) { // it should always be there since we just created it above
            			editorContext.setLibrary(view.getLibrary());
            		} else {
            			_logger.error("Failed to find the CKEditorContext for user " + vaadinUi.getUser().getFullDisplayName() + "; with context id: " + editorDocumentFooterHtmlContextId);
            		}

                	CKEditorConfig config = new CKEditorConfig();
                	config.setupForOpenESignForms(vaadinUi.getRequestContextPath(),editorDocumentFooterHtmlContextId,null);
                	config.setHeight("70px");
                	CKEditorTextField editor= new CKEditorTextField(config);
                	editor.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.document.footer.html.label"));
                	editor.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.document.footer.html.tooltip"));
                	editor.setImmediate(true);
                	editor.setRequired(false);
                	editor.setHeight(180,Unit.PIXELS); // bigger than our config.setHeight to accommodate the toolbar and footer
                	editor.setReadOnly(false);
                	/*
                	editor.addFocusListener( new FieldEvents.FocusListener() {
						private static final long serialVersionUID = -868876792675255040L;

						@Override
						public void focus(FocusEvent event) {
							if ( view.buttonmessageForm.saveButton != null )
								view.buttonmessageForm.saveButton.removeClickShortcut();
						}
	            	});
                	editor.addBlurListener( new FieldEvents.BlurListener() {
						private static final long serialVersionUID = 7979193641328618657L;

						@Override
						public void blur(BlurEvent event) {
							if ( view.buttonmessageForm.saveButton != null )
								view.buttonmessageForm.saveButton.setClickShortcut(KeyCode.ENTER);
						}
					});
					*/
                    return editor;
                }
				
				Field field = super.createField(item, propertyId, uiContext);
				field.setWidth(100, Unit.PERCENTAGE);
				
                if (propertyId.equals("packageButtonContinueToDo")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonContinueToDo.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonContinueToDo.tooltip"));
                } else if (propertyId.equals("packageButtonContinueToDoTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("packageButtonContinueDone")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonContinueDone.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonContinueDone.tooltip"));
                } else if (propertyId.equals("packageButtonContinueDoneTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("packageButtonEditDocument")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonEditDocument.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonEditDocument.tooltip"));
                } else if (propertyId.equals("packageButtonEditDocumentTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("packageButtonViewCompletedDocument")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonViewCompletedDocument.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonViewCompletedDocument.tooltip"));
                } else if (propertyId.equals("packageButtonViewCompletedDocumentTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("packageButtonDelete")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonDelete.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonDelete.tooltip"));
                } else if (propertyId.equals("packageButtonDeleteTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("packageButtonNotCompleting")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonNotCompleting.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonNotCompleting.tooltip"));
                } else if (propertyId.equals("packageButtonNotCompletingTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("packageButtonDownloadMyDocsAsPdf")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonDownloadMyDocsAsPdf.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.packageButtonDownloadMyDocsAsPdf.tooltip"));
                } else if (propertyId.equals("packageButtonDownloadMyDocsAsPdfTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("documentButtonGoToPackage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentButtonGoToPackage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentButtonGoToPackage.tooltip"));
                } else if (propertyId.equals("documentButtonGoToPackageTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("documentButtonGoToPackageMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.tooltip"));
                } else if (propertyId.equals("documentEditButtonContinueNextPage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentEditButtonContinueNextPage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentEditButtonContinueNextPage.tooltip"));
                } else if (propertyId.equals("documentEditButtonContinueNextPageTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("documentEditButtonPreviousPage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentEditButtonPreviousPage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentEditButtonPreviousPage.tooltip"));
                } else if (propertyId.equals("documentEditButtonPreviousPageTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("documentEditButtonSave")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentEditButtonSave.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentEditButtonSave.tooltip"));
                } else if (propertyId.equals("documentEditButtonSaveTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("documentEditButtonContinueToReview")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentEditButtonContinueToReview.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentEditButtonContinueToReview.tooltip"));
                } else if (propertyId.equals("documentEditButtonContinueToReviewTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("documentEditButtonContinueToReviewMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.tooltip"));
                } else if (propertyId.equals("documentReviewButtonCompleteSigner")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReviewButtonCompleteSigner.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReviewButtonCompleteSigner.tooltip"));
                } else if (propertyId.equals("documentReviewButtonCompleteSignerTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("documentReviewButtonCompleteSignerMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.tooltip"));
                } else if (propertyId.equals("documentReviewButtonCompleteNotSigner")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReviewButtonCompleteNotSigner.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReviewButtonCompleteNotSigner.tooltip"));
                } else if (propertyId.equals("documentReviewButtonCompleteNotSignerTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("documentReviewButtonCompleteNotSignerMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.tooltip"));
                } else if (propertyId.equals("documentReviewButtonReturnToEdit")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReviewButtonReturnToEdit.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReviewButtonReturnToEdit.tooltip"));
                } else if (propertyId.equals("documentReviewButtonReturnToEditTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("documentReviewButtonReturnToEditMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.tooltip"));
                } else if (propertyId.equals("documentReviewViewOnlyButtonContinueNextPage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReviewViewOnlyButtonContinueNextPage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReviewViewOnlyButtonContinueNextPage.tooltip"));
                } else if (propertyId.equals("documentReviewViewOnlyButtonContinueNextPageTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("documentReviewViewOnlyButtonContinueNextPageMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.tooltip"));
                } else if (propertyId.equals("documentReviewViewOnlyButtonPreviousPage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReviewViewOnlyButtonPreviousPage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReviewViewOnlyButtonPreviousPage.tooltip"));
                } else if (propertyId.equals("documentReviewViewOnlyButtonPreviousPageTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));
                } else if (propertyId.equals("documentReviewViewOnlyButtonComplete")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setNullRepresentation("");
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReviewViewOnlyButtonComplete.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentReviewViewOnlyButtonComplete.tooltip"));
                } else if (propertyId.equals("documentReviewViewOnlyButtonCompleteTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));                
                } else if (propertyId.equals("documentReviewViewOnlyButtonCompleteMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.message.tooltip"));
                } else if (propertyId.equals("documentViewOnlyButtonNextPage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentViewOnlyButtonNextPage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentViewOnlyButtonNextPage.tooltip"));
                } else if (propertyId.equals("documentViewOnlyButtonNextPageTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));                
                } else if (propertyId.equals("documentViewOnlyButtonPreviousPage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentViewOnlyButtonPreviousPage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentViewOnlyButtonPreviousPage.tooltip"));
                } else if (propertyId.equals("documentViewOnlyButtonPreviousPageTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));                
                } else if (propertyId.equals("documentViewOnlyButtonNextDocument")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentViewOnlyButtonNextDocument.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentViewOnlyButtonNextDocument.tooltip"));
                } else if (propertyId.equals("documentViewOnlyButtonNextDocumentTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));                
                } else if (propertyId.equals("documentViewOnlyButtonPreviousDocument")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.documentViewOnlyButtonPreviousDocument.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.documentViewOnlyButtonPreviousDocument.tooltip"));
                } else if (propertyId.equals("documentViewOnlyButtonPreviousDocumentTitle")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.button.tooltip.tooltip"));                
                } else if (propertyId.equals("documentAlreadyCompletedMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.message.documentAlreadyCompletedMessage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.message.documentAlreadyCompletedMessage.tooltip"));                
                } else if (propertyId.equals("documentAlreadyCompletedCannotEditMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.message.documentAlreadyCompletedCannotEditMessage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.message.documentAlreadyCompletedCannotEditMessage.tooltip"));                
                } else if (propertyId.equals("viewOnlyDocumentFYIMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.message.viewOnlyDocumentFYIMessage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.message.viewOnlyDocumentFYIMessage.tooltip"));                
                } else if (propertyId.equals("reviewDocumentNotSignerMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.message.reviewDocumentNotSignerMessage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.message.reviewDocumentNotSignerMessage.tooltip"));                
                } else if (propertyId.equals("reviewDocumentSignerMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.message.reviewDocumentSignerMessage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.message.reviewDocumentSignerMessage.tooltip"));                
                } else if (propertyId.equals("completeDocumentEditsMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.message.completeDocumentEditsMessage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.message.completeDocumentEditsMessage.tooltip"));                
                } else if (propertyId.equals("completedAllDocumentsMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.message.completedAllDocumentsMessage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.message.completedAllDocumentsMessage.tooltip"));                
                } else if (propertyId.equals("deletedTransactionMessage")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(false);
                	tf.setNullRepresentation("");
                	tf.setCaption(vaadinUi.getMsg("LibButtonMessageVersionForm.message.deletedTransactionMessage.label"));
                	tf.setDescription(vaadinUi.getMsg("LibButtonMessageVersionForm.message.deletedTransactionMessage.tooltip"));                                
                }
                
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
    }
    
    @Override
	public void buttonClick(ClickEvent event) {
    	LibButtonMessageVersionBean currBean = getCurrentBean();
    	if ( currBean == null ) return;
    	
    	Button source = event.getButton();
    	if ( source == testToProdButton ) {
    		view.promoteTestToProductionVersion();
    	} else if ( source == createTestFromProdButton ) {
    		view.createNextVersion();
    	} else if ( source == revertProdToTestButton ) {
    		view.revertProductionVersionBackToTest();
    	} else if ( source == deleteButton ) {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	ConfirmDialog.show(UI.getCurrent(),
	    			vaadinUi.getMsg("LibButtonMessageVersionForm.button.delete.ConfirmDialog.caption"),
	    			vaadinUi.getMsg("LibButtonMessageVersionForm.button.delete.ConfirmDialog.message"),
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
	    	        new ConfirmDialog.Listener() {
						private static final long serialVersionUID = -9013700462115624885L;

						public void onClose(ConfirmDialog dialog) {
	    	                if (dialog.isConfirmed()) {
	    	                    view.delete();
	    	                }
	    	            }
	    	        });
    	}
     }
	
    LibButtonMessageVersionBean save(Connection con) throws SQLException {
    	LibButtonMessageVersionBean currBean = getCurrentBean();
    	if ( currBean != null ) {
    		if ( currBean.save(con,null) ) {
    			return currBean;
    		}
    	}
    	return null;
    }
    	
    public String getCurrentBeanName() {
    	LibButtonMessageVersionBean currBean = getCurrentBean();
		return currBean == null ? "(None)" : view.getEsfNameWithVersion();
	}
    
	LibButtonMessageVersionBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public LibButtonMessageVersionBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<LibButtonMessageVersionBean> bi = (BeanItem<LibButtonMessageVersionBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached stuff
		resetCaches();
		
    	if (newDataSource != null) {
    		LibButtonMessageVersionBean bean = getBean(newDataSource);
    		
    		if ( _orderedProperties == null ) {
    			_orderedProperties = Arrays.asList(
    					"packageFooterHtml","documentFooterHtml",
    					"packageButtonContinueToDo","packageButtonContinueToDoTitle","packageButtonContinueDone","packageButtonContinueDoneTitle","packageButtonEditDocument","packageButtonEditDocumentTitle","packageButtonViewCompletedDocument","packageButtonViewCompletedDocumentTitle","packageButtonDelete","packageButtonDeleteTitle","packageButtonNotCompleting","packageButtonNotCompletingTitle","packageButtonDownloadMyDocsAsPdf","packageButtonDownloadMyDocsAsPdfTitle",
    					"documentButtonGoToPackage","documentButtonGoToPackageTitle","documentButtonGoToPackageMessage",
    					"documentEditButtonContinueNextPage","documentEditButtonContinueNextPageTitle","documentEditButtonPreviousPage","documentEditButtonPreviousPageTitle","documentEditButtonSave","documentEditButtonSaveTitle","documentEditButtonContinueToReview","documentEditButtonContinueToReviewTitle","documentEditButtonContinueToReviewMessage",
    					"documentReviewButtonCompleteSigner","documentReviewButtonCompleteSignerTitle","documentReviewButtonCompleteSignerMessage","documentReviewButtonCompleteNotSigner","documentReviewButtonCompleteNotSignerTitle","documentReviewButtonCompleteNotSignerMessage","documentReviewButtonReturnToEdit","documentReviewButtonReturnToEditTitle","documentReviewButtonReturnToEditMessage",
    					"documentReviewViewOnlyButtonContinueNextPage","documentReviewViewOnlyButtonContinueNextPageTitle","documentReviewViewOnlyButtonContinueNextPageMessage","documentReviewViewOnlyButtonPreviousPage","documentReviewViewOnlyButtonPreviousPageTitle","documentReviewViewOnlyButtonComplete","documentReviewViewOnlyButtonCompleteTitle","documentReviewViewOnlyButtonCompleteMessage",
    					"documentViewOnlyButtonNextPage","documentViewOnlyButtonNextPageTitle","documentViewOnlyButtonPreviousPage","documentViewOnlyButtonPreviousPageTitle","documentViewOnlyButtonNextDocument","documentViewOnlyButtonNextDocumentTitle","documentViewOnlyButtonPreviousDocument","documentViewOnlyButtonPreviousDocumentTitle",
    					"documentAlreadyCompletedMessage","documentAlreadyCompletedCannotEditMessage","viewOnlyDocumentFYIMessage","reviewDocumentNotSignerMessage","reviewDocumentSignerMessage","completeDocumentEditsMessage","completedAllDocumentsMessage","deletedTransactionMessage"
    											 );
    		}
    		super.setItemDataSource(newDataSource, _orderedProperties);
    		
    		setupForm(bean);
    		
    		Field f = getField("packageFooterHtml");
    		f.commit();
    		f = getField("documentFooterHtml");
    		f.commit();
    				
    		setReadOnly( ! view.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	

    @Override
    public void setReadOnly(boolean readOnly) {   	
    	LibButtonMessageVersionBean bean = getCurrentBean();
    	
    	boolean isTestVersion = bean != null && view.isTestVersion(bean.buttonmessageVerInfo());
    	
    	testToProdButton.setVisible(bean != null && !readOnly && isTestVersion && bean.buttonmessageVersion().doUpdate() && view.getLibButtonMessageBean().isEnabled());
    	createTestFromProdButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.buttonmessageVerInfo()) && !view.hasTestVersion());
    	revertProdToTestButton.setVisible(bean != null && !readOnly && view.isProductionVersion(bean.buttonmessageVerInfo()) && !view.hasTestVersion());
    	
    	// Show this only if the object is created and is the Test version
    	if ( deleteButton != null ) {
    		deleteButton.setVisible(bean != null && bean.buttonmessageVersion().doUpdate() && isTestVersion); 
    	}

    	// If the request is to be not-readonly, but this is not the test version, we convert it back to readonly
    	if ( !readOnly && bean != null && !view.isTestVersion(bean.buttonmessageVerInfo())) {
    		readOnly = true;
    	}
    	super.setReadOnly(readOnly);
    }
    
    void setupForm(LibButtonMessageVersionBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("LibButtonMessageVersionForm.label.id",bean.getId()) );
		
		versionInfo.setValue( vaadinUi.getMsg("LibButtonMessageVersionForm.label.version",bean.getVersion(),view.getVersionLabel(bean.buttonmessageVerInfo())) );
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.buttonmessageVerInfo().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("LibButtonMessageVersionForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );
		
		if ( bean.buttonmessageVersion().doInsert() ) {
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibButtonMessageVersionForm.label.lastUpdated.whenNew") );
			lastUpdatedByInfo.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.buttonmessageVerInfo().getLastUpdatedByUserId());
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibButtonMessageVersionForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedByInfo.setIcon(null);
		}
		
		packageSection.setVisible(true);
		documentGeneralSection.setVisible(true);
		documentEditSection.setVisible(true);
		documentReviewSection.setVisible(true);
		documentReadOnlySection.setVisible(true);
		documentViewOnlySection.setVisible(true);
		messagesSection.setVisible(true);
    }

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
    	if (propertyId.equals("packageFooterHtml") ) {
			layout.addComponent(field,0,1,1,1);
    	} else if (propertyId.equals("documentFooterHtml") ) {
			layout.addComponent(field,0,2,1,2);
    	} else if (propertyId.equals("packageButtonContinueToDo") ) {
			layout.addComponent(field,0,4);
    	} else if (propertyId.equals("packageButtonContinueToDoTitle") ) {
			layout.addComponent(field,1,4);
    	} else if (propertyId.equals("packageButtonContinueDone") ) {
			layout.addComponent(field,0,5);
    	} else if (propertyId.equals("packageButtonContinueDoneTitle") ) {
			layout.addComponent(field,1,5);
    	} else if (propertyId.equals("packageButtonEditDocument") ) {
			layout.addComponent(field,0,6);
    	} else if (propertyId.equals("packageButtonEditDocumentTitle") ) {
			layout.addComponent(field,1,6);

    	} else if (propertyId.equals("packageButtonViewCompletedDocument") ) {
			layout.addComponent(field,0,7);
    	} else if (propertyId.equals("packageButtonViewCompletedDocumentTitle") ) {
			layout.addComponent(field,1,7);
    	} else if (propertyId.equals("packageButtonDelete") ) {
			layout.addComponent(field,0,8);
    	} else if (propertyId.equals("packageButtonDeleteTitle") ) {
			layout.addComponent(field,1,8);
    	} else if (propertyId.equals("packageButtonNotCompleting") ) {
			layout.addComponent(field,0,9);
    	} else if (propertyId.equals("packageButtonNotCompletingTitle") ) {
			layout.addComponent(field,1,9);
    	} else if (propertyId.equals("packageButtonDownloadMyDocsAsPdf") ) {
			layout.addComponent(field,0,10);
    	} else if (propertyId.equals("packageButtonDownloadMyDocsAsPdfTitle") ) {
			layout.addComponent(field,1,10);
    	} else if (propertyId.equals("documentButtonGoToPackage") ) {
			layout.addComponent(field,0,12);
    	} else if (propertyId.equals("documentButtonGoToPackageTitle") ) {
			layout.addComponent(field,1,12);
    	} else if (propertyId.equals("documentButtonGoToPackageMessage") ) {
			layout.addComponent(field,0,13,1,13);
    	} else if (propertyId.equals("documentEditButtonContinueNextPage") ) {
			layout.addComponent(field,0,15);
    	} else if (propertyId.equals("documentEditButtonContinueNextPageTitle") ) {
			layout.addComponent(field,1,15);
    	} else if (propertyId.equals("documentEditButtonPreviousPage") ) {
			layout.addComponent(field,0,16);
    	} else if (propertyId.equals("documentEditButtonPreviousPageTitle") ) {
			layout.addComponent(field,1,16);
    	} else if (propertyId.equals("documentEditButtonSave") ) {
			layout.addComponent(field,0,17);
    	} else if (propertyId.equals("documentEditButtonSaveTitle") ) {
			layout.addComponent(field,1,17);  
    	} else if (propertyId.equals("documentEditButtonContinueToReview") ) {
			layout.addComponent(field,0,18);
    	} else if (propertyId.equals("documentEditButtonContinueToReviewTitle") ) {
			layout.addComponent(field,1,18);
    	} else if (propertyId.equals("documentEditButtonContinueToReviewMessage") ) {
			layout.addComponent(field,0,19,1,19);
    	} else if (propertyId.equals("documentReviewButtonCompleteSigner") ) {
			layout.addComponent(field,0,21);
    	} else if (propertyId.equals("documentReviewButtonCompleteSignerTitle") ) {
			layout.addComponent(field,1,21);
    	} else if (propertyId.equals("documentReviewButtonCompleteSignerMessage") ) {
			layout.addComponent(field,0,22,1,22);
    	} else if (propertyId.equals("documentReviewButtonCompleteNotSigner") ) {
			layout.addComponent(field,0,23);
    	} else if (propertyId.equals("documentReviewButtonCompleteNotSignerTitle") ) {
			layout.addComponent(field,1,23);
    	} else if (propertyId.equals("documentReviewButtonCompleteNotSignerMessage") ) {
			layout.addComponent(field,0,24,1,24);
    	} else if (propertyId.equals("documentReviewButtonReturnToEdit") ) {
			layout.addComponent(field,0,25);
    	} else if (propertyId.equals("documentReviewButtonReturnToEditTitle") ) {
			layout.addComponent(field,1,25);
    	} else if (propertyId.equals("documentReviewButtonReturnToEditMessage") ) {
			layout.addComponent(field,0,26,1,26);
    	} else if (propertyId.equals("documentReviewViewOnlyButtonContinueNextPage") ) {
			layout.addComponent(field,0,28);
    	} else if (propertyId.equals("documentReviewViewOnlyButtonContinueNextPageTitle") ) {
			layout.addComponent(field,1,28);
    	} else if (propertyId.equals("documentReviewViewOnlyButtonContinueNextPageMessage") ) {
			layout.addComponent(field,0,29,1,29);
    	} else if (propertyId.equals("documentReviewViewOnlyButtonPreviousPage") ) {
			layout.addComponent(field,0,30);
    	} else if (propertyId.equals("documentReviewViewOnlyButtonPreviousPageTitle") ) {
			layout.addComponent(field,1,30);
    	} else if (propertyId.equals("documentReviewViewOnlyButtonComplete") ) {
			layout.addComponent(field,0,31);
    	} else if (propertyId.equals("documentReviewViewOnlyButtonCompleteTitle") ) {
			layout.addComponent(field,1,31);
    	} else if (propertyId.equals("documentReviewViewOnlyButtonCompleteMessage") ) {
			layout.addComponent(field,0,32,1,32);
    	} else if (propertyId.equals("documentViewOnlyButtonNextPage") ) {
			layout.addComponent(field,0,34);
    	} else if (propertyId.equals("documentViewOnlyButtonNextPageTitle") ) {
			layout.addComponent(field,1,34);
    	} else if (propertyId.equals("documentViewOnlyButtonPreviousPage") ) {
			layout.addComponent(field,0,35);
    	} else if (propertyId.equals("documentViewOnlyButtonPreviousPageTitle") ) {
			layout.addComponent(field,1,35);
    	} else if (propertyId.equals("documentViewOnlyButtonNextDocument") ) {
			layout.addComponent(field,0,36);
    	} else if (propertyId.equals("documentViewOnlyButtonNextDocumentTitle") ) {
			layout.addComponent(field,1,36);
    	} else if (propertyId.equals("documentViewOnlyButtonPreviousDocument") ) {
			layout.addComponent(field,0,37);
    	} else if (propertyId.equals("documentViewOnlyButtonPreviousDocumentTitle") ) {
			layout.addComponent(field,1,37);
    	} else if (propertyId.equals("documentAlreadyCompletedMessage") ) {
			layout.addComponent(field,0,39,1,39);
    	} else if (propertyId.equals("documentAlreadyCompletedCannotEditMessage") ) {
			layout.addComponent(field,0,40,1,40);
    	} else if (propertyId.equals("viewOnlyDocumentFYIMessage") ) {
			layout.addComponent(field,0,41,1,41);
    	} else if (propertyId.equals("reviewDocumentNotSignerMessage") ) {
			layout.addComponent(field,0,42,1,42);
    	} else if (propertyId.equals("reviewDocumentSignerMessage") ) {
			layout.addComponent(field,0,43,1,43);
    	} else if (propertyId.equals("completeDocumentEditsMessage") ) {
			layout.addComponent(field,0,44,1,44);
    	} else if (propertyId.equals("completedAllDocumentsMessage") ) {
			layout.addComponent(field,0,45,1,45);
    	} else if (propertyId.equals("deletedTransactionMessage") ) {
			layout.addComponent(field,0,46,1,46);
    	}
    }


	@Override
	public String checkDirty() { // OBSOLETE SINCE VERSION FORM HAS NO BUTTONS FOR SAVING, CANCELING
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibButtonMessageAndVersionsMainView.ButtonMessage.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
	
	@Override
	public void detach() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		User user = vaadinUi.getUser();
		if ( user != null ) {
	    	user.releaseCKEditorContext(vaadinUi.getHttpSession(),editorPackageFooterHtmlContextId);
	    	user.releaseCKEditorContext(vaadinUi.getHttpSession(),editorDocumentFooterHtmlContextId);
		}
		super.detach();
	}

}