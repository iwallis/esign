// Copyright (C) 2012-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document;

import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FileVersion;
import com.esignforms.open.prog.Library;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.LibraryFileEsfNameValidator;
import com.esignforms.open.vaadin.widget.UploadFileWithProgress;
import com.vaadin.data.Item;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.WebBrowser;
import com.vaadin.shared.ui.BorderStyle;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The DocumentFileForm is used to set the values on the file of a document version.
 * @author Yozons Inc.
 */
public class DocumentFileForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = 4996162901095502994L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DocumentFileForm.class);
	
	Button saveButton;
	Button cancelButton;
	Button deleteButton;
	
	final DocumentFileForm thisForm;
	LibDocumentVersionBean docVerBean;
	DocumentVersion.DocumentFile documentFile;
	
	ConfirmDiscardFormChangesWindow parentWindow;
    GridLayout layout;
    
    final DocumentVersion duplicateDocumentVersion;

    LibraryFileEsfNameValidator esfnameValidator;
    
	Label fileId;
	Label fileVersionId;
	Label createdByInfo;
	Label lastUpdatedByInfo;
	Label fileInfo;
	Link downloadFileLink;
	UploadFileWithProgress uploadFile;
	
	// we use this to rollback since we can't just rely on commit/discard as we need to see the newly uploaded image
	// on upload, not just after clicking OK/Save.
	FileVersion origFileVersion;
	                              
    

    public DocumentFileForm(Library library, LibDocumentVersionBean docVerBean, DocumentVersion.DocumentFile documentFile) {
    	setStyleName("LibDocumentVersionPageViewDocumentFileForm");
    	this.thisForm = this;
    	this.docVerBean = docVerBean;
    	this.documentFile = documentFile; 
		this.duplicateDocumentVersion = docVerBean.duplicateDocumentVersion();
		this.origFileVersion = documentFile.fileVersion.duplicate();
     }
    
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
			EsfName esfName = new EsfName((String)getField("esfName").getValue());
			documentFile.file.setEsfName(esfName);
			
			String displayName = (String)getField("displayName").getValue();
			documentFile.file.setDisplayName(displayName);
			
			String description = (String)getField("description").getValue();
			documentFile.file.setDescription(description);
			
			String comments = (String)getField("comments").getValue();
			documentFile.file.setComments(comments);
			
			if ( documentFile.file.doInsert() )
				duplicateDocumentVersion.addDocumentFile(documentFile);
						
			parentWindow.close();
        } else if ( source == cancelButton ) {
    		discard();
    		documentFile.fileVersion = origFileVersion;
    		FileVersion.Manager.replaceInCache(origFileVersion);
			parentWindow.close();
        } else if ( source == deleteButton ) {
        	discard();
        	duplicateDocumentVersion.removeDocumentFile(documentFile);
        	parentWindow.close();
        }
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	saveButton.setVisible(!readOnly && documentFile.fileVersion.hasFileFileName());
    	deleteButton.setVisible(!readOnly && documentFile.fileVersion.hasFileFileName());
    	uploadFile.setVisible(!readOnly);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
		setupForm();
	}
	
	void setupForm() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		PropertysetItem item = new PropertysetItem();
		item.addItemProperty("esfName", new ObjectProperty<EsfName>(documentFile.file.getEsfName()));
		item.addItemProperty("displayName", new ObjectProperty<String>(documentFile.file.getDisplayName(),String.class));
		item.addItemProperty("description", new ObjectProperty<String>(documentFile.file.getDescription(),String.class));
		item.addItemProperty("comments", new ObjectProperty<String>(documentFile.file.getComments(),String.class));
		setItemDataSource(item);
		
		fileId.setValue( vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.label.fileId",documentFile.file.getId()) );
		fileVersionId.setValue( vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.label.id",documentFile.fileVersion.getId()) );		

		String createdBy = vaadinUi.getPrettyCode().userDisplayName(documentFile.fileVersion.getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.label.createdBy",documentFile.fileVersion.getCreatedTimestamp().toLogString(vaadinUi.getUser()),createdBy) );
		
		if ( documentFile.fileVersion.doInsert() ) {
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.label.lastUpdated.whenNew") );
			lastUpdatedByInfo.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(documentFile.fileVersion.getLastUpdatedByUserId());
			lastUpdatedByInfo.setCaption( vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.label.lastUpdated",documentFile.fileVersion.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser()),lastUpdatedBy) );
			lastUpdatedByInfo.setIcon(null);
		}
		
		if ( documentFile.fileVersion.hasFileFileName() ) {
			fileInfo.setValue(documentFile.fileVersion.getFileFileName());
			fileInfo.removeStyleName("fileInfoError");
			uploadFile.setButtonCaption(vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.button.uploadFile.hasfile.label"));
			downloadFileLink.setVisible(true);
			downloadFileLink.setResource(new ExternalResource(documentFile.fileVersion.getFileByIdUrl()));
			
			FileVersion.Manager.replaceInCache(documentFile.fileVersion); // so we can see the new files
			
		} else {
			fileInfo.setValue(vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.label.fileInfo.nofile"));
			fileInfo.addStyleName("fileInfoError");
			uploadFile.setButtonCaption(vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.button.uploadFile.nofile.label"));
			downloadFileLink.setVisible(false);
		}

		TextField esfname = (TextField)getField("esfName");
		if ( esfnameValidator != null ) esfname.removeValidator(esfnameValidator);
		esfnameValidator = new LibraryFileEsfNameValidator(duplicateDocumentVersion,documentFile.file);
		esfname.addValidator(esfnameValidator);
		if ( documentFile.fileVersion.doInsert() ) {
			esfname.selectAll();
			esfname.focus();
		}
	}
	
	public void setParentWindow(ConfirmDiscardFormChangesWindow parentWindow) {
		this.parentWindow = parentWindow;
	}

	@Override
	public void initView() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	
    	// Setup layout
    	layout = new GridLayout(2,9);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.5f);
    	layout.setColumnExpandRatio(1, 0.5f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
		fileInfo = new Label();
		fileInfo.setStyleName("fileInfo");
		fileInfo.setContentMode(ContentMode.TEXT);
		layout.addComponent(fileInfo,0,3,1,3);
		
		// TODO: 100MB max for a file in our document, make tunable
		uploadFile = new UploadFileWithProgress(null,vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.button.uploadFile.nofile.label"),(int)vaadinUi.getEsfapp().getUploadFileMaxBytes()) {
			private static final long serialVersionUID = -1023380584133621045L;

			@Override
			public void afterUploadSucceeded() {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				byte[] fileData = uploadFile.getFileData();
				documentFile.fileVersion.setFileData(fileData);
				documentFile.fileVersion.setFileFileName(uploadFile.getFileName());
				documentFile.fileVersion.setFileMimeType(uploadFile.getMimeType());
				String sizeWithUnits = EsfInteger.byteSizeInUnits(uploadFile.getFileData().length);
				vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.upload.successful",uploadFile.getFileName(),sizeWithUnits ));
				fileInfo.setValue(uploadFile.getFileName() + " (" + sizeWithUnits + ")");
				fileInfo.removeStyleName("fileInfoError");
				uploadFile.setButtonCaption(vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.button.uploadFile.hasfile.label"));
				downloadFileLink.setResource(new ExternalResource(documentFile.fileVersion.getFileByIdUrl()));
				downloadFileLink.setVisible(true);
				thisForm.setReadOnly(isReadOnly());
			}
			@Override
			public void afterUploadFailed(String fileName, boolean wasCanceled, boolean wasInvalidMimeType, boolean wasTooBig, int contentLength, int maxSize) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				if ( wasTooBig ) {
					vaadinUi.showError(
							vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.fileTooLarge.caption"), 
							vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.fileTooLarge.message",EsfInteger.byteSizeInUnits(contentLength),EsfInteger.byteSizeInUnits(maxSize))
							);
				} else if ( wasCanceled ) {
					vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.upload.canceled"));
				} else if ( wasInvalidMimeType ) {
					vaadinUi.showError(
							vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.invalidType.caption"), 
							vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.invalidType.message",EsfString.isBlank(fileName)?"(unknown)":fileName)
							);
				}
			}
		}; 
		uploadFile.setVisible(false);
		layout.addComponent(uploadFile,0,4);
		
		WebBrowser wb = Page.getCurrent().getWebBrowser();
		downloadFileLink = new Link();
		downloadFileLink.setCaption(vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.link.downloadFile.label"));
		downloadFileLink.setTargetName(wb.isChrome() || wb.isSafari() ? "_top" : "_blank");
		downloadFileLink.setTargetWidth(300);
		downloadFileLink.setTargetHeight(300);
		downloadFileLink.setTargetBorder(BorderStyle.NONE);
		layout.addComponent(downloadFileLink,1,4);
		downloadFileLink.setVisible(false);

		lastUpdatedByInfo = new Label();
		lastUpdatedByInfo.setContentMode(ContentMode.TEXT);
		lastUpdatedByInfo.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedByInfo,0,5,1,5);

		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,6,1,6);		

		fileId = new Label();
		fileId.setContentMode(ContentMode.TEXT);
		fileId.setStyleName("smallInfo");
		layout.addComponent(fileId,0,7,1,7);

		fileVersionId = new Label();
		fileVersionId.setContentMode(ContentMode.TEXT);
		fileVersionId.setStyleName("smallInfo");
		layout.addComponent(fileVersionId,0,8,1,8);

    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(true);
    	footer.setMargin(true);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
		footer.addComponent(deleteButton);

		setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 7952046021088686320L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				
				if ( propertyId.equals("comments") ) {
	            	TextArea ta = new TextArea();
	            	ta.setWidth(100, Unit.PERCENTAGE);
	            	ta.setNullRepresentation("");
	            	ta.setRows(3);
	            	ta.setInputPrompt(vaadinUi.getMsg("inputPrompt.comments"));
	            	ta.setCaption(vaadinUi.getMsg("caption.comments"));
	            	ta.setDescription(vaadinUi.getMsg("tooltip.comments"));
	            	return ta;
				}
				
				Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("esfName")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setCaption(vaadinUi.getMsg("caption.esfName"));
                	tf.setDescription(vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.esfname.tooltip"));
                	tf.setConversionError(vaadinUi.getMsg("validator.esfname.message"));
                } else if ( propertyId.equals("displayName") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("caption.displayName"));
                    tf.setInputPrompt(vaadinUi.getMsg("inputPrompt.displayName"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.displayName"));
                } else if ( propertyId.equals("description") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("caption.description"));
                    tf.setInputPrompt(vaadinUi.getMsg("inputPrompt.description"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.description"));
                }
                
                return field;
    	    }
    	 });

    	_logger.debug("Form created");
	}

	@Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("esfName")) {
        	layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("displayName")) {
        	layout.addComponent(field, 1, 0);
        } else if (propertyId.equals("description")) {
        	layout.addComponent(field, 0, 1, 1, 1);
        } else if (propertyId.equals("comments")) {
        	layout.addComponent(field, 0, 2, 1, 2);
        }
    }


	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibDocumentVersionPageView.FileForm.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}