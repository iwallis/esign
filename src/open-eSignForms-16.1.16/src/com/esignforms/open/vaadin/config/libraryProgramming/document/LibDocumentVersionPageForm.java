// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document;

import org.vaadin.hene.popupbutton.PopupButton;
import org.vaadin.openesignforms.ckeditor.CKEditorConfig;
import org.vaadin.openesignforms.ckeditor.CKEditorTextField;




import com.esignforms.open.config.CKEditorContext;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.prog.DocumentStyleVersion;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DocumentVersionPage;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.File;
import com.esignforms.open.prog.FileVersion;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.libraryProgramming.document.LibDocumentVersionTester;
import com.esignforms.open.vaadin.config.libraryProgramming.document.rule.DocumentProgrammingRuleView;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.main.TipsView;
import com.esignforms.open.vaadin.validator.LibraryDocumentVersionPageEsfNameValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.FieldSpecPopupView;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
//import com.vaadin.event.FieldEvents;
//import com.vaadin.event.FieldEvents.BlurEvent;
//import com.vaadin.event.FieldEvents.FocusEvent;
//import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.themes.Reindeer;

/**
 * Allows a user to view/edit the HTML and name of a given document version page.  Only document versions in 'Test' mode can be modified.
 * @author Yozons Inc.
 */
public class LibDocumentVersionPageForm extends CustomComponent implements EsfView {
	private static final long serialVersionUID = 2543512780158699254L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibDocumentVersionPageForm.class);
	
	Button saveButton;
	Button closeButton;
	Button saveAndTestButton;
	Button documentProgrammingRuleButton;
	
 	PopupButton createNewButton;
	Button createNewFieldButton;
	Button createNewPartyButton;
	Button createNewFileButton;
	Button createNewImageButton;
	Button createNewDropdownButton;

    final LibDocumentVersionPageView documentVersionPageView;
    VerticalLayout layout;
    HorizontalLayout buttonLayout;
    FieldGroup fieldGroup;
    
    TextField esfname;
    LibraryDocumentVersionPageEsfNameValidator esfnameValidator;
    String editorContextId;
    
    Label id;
    Label documentNameVersion;

    public LibDocumentVersionPageForm(LibDocumentVersionPageView documentVersionPageView) {
    	layout = new VerticalLayout();
    	layout.setSizeFull();
    	layout.setMargin(false);
    	layout.setSpacing(true);
    	setCompositionRoot(layout);
    	fieldGroup = new FieldGroup();
    	setStyleName("LibDocumentVersionPageForm");
    	setWidth(100,Unit.PERCENTAGE);
    	setHeight(98, Unit.PERCENTAGE);
    	this.documentVersionPageView = documentVersionPageView;
    }
    
	public String getCurrentBeanName() {
		Item currItem = fieldGroup.getItemDataSource();
		return currItem == null ? "(None)" : documentVersionPageView.documentVersionForm.mainView.getEsfNameWithVersion();
	}

    public void setItemDataSource(Item dataSource) {
    	fieldGroup.setItemDataSource(dataSource);
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		if ( esfnameValidator != null ) esfname.removeValidator(esfnameValidator);
		esfnameValidator = new LibraryDocumentVersionPageEsfNameValidator(documentVersionPageView.docVerBean.duplicateDocumentVersion(),documentVersionPageView.docVerPage);
		esfname.addValidator(esfnameValidator);
		
		id.setValue( vaadinUi.getMsg("LibDocumentVersionPageForm.label.id",dataSource.getItemProperty("id").getValue().toString()) );
		documentNameVersion.setValue( vaadinUi.getMsg("LibDocumentVersionPageForm.label.documentVersion",
				documentVersionPageView.documentVersionForm.mainView.getLibDocumentBean().getEsfName(), documentVersionPageView.docVerBean.duplicateDocumentVersion().getVersion()) );
		
		CKEditorContext editorContext = vaadinUi.getUser().getCKEditorContext(vaadinUi.getHttpSession(),editorContextId);
		if ( editorContext != null ) { // it should always be there 
			editorContext.setDocumentVersion(documentVersionPageView.docVerBean.duplicateDocumentVersion());
		} else {
			_logger.error("Failed to find the CKEditorContext for user " + vaadinUi.getUser().getFullDisplayName() + "; with context id: " + editorContextId);
		}
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);

    	Item currItem = fieldGroup.getItemDataSource();
    	
    	if ( currItem != null )
    		fieldGroup.setReadOnly(readOnly);
    	
    	// We have these only while editing
    	saveButton.setVisible(!readOnly && currItem != null);
    	
    	saveAndTestButton.setVisible(currItem != null && documentVersionPageView.documentVersionForm.testButton.isVisible());
    	
    	documentProgrammingRuleButton.setVisible(currItem != null && documentVersionPageView.docVerBean.duplicateDocumentVersion().doUpdate());
    	
    	// This always is visible
    	closeButton.setVisible(true);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		if ( mode != EsfView.OpenMode.WINDOW && closeButton != null ) {
			buttonLayout.removeComponent(closeButton);
			closeButton = null;
		}
	}

	@Override
	public void initView() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( vaadinUi.getUser() == null ) {
			_logger.error("Could not create initialize the view as no current user could be found.");
			vaadinUi.logoffUser();
			return;
		}
		
		HorizontalLayout row1 = new HorizontalLayout();
		row1.setMargin(false);
		row1.setSpacing(true);
		row1.setWidth(100, Unit.PERCENTAGE);
		layout.addComponent(row1);

		documentNameVersion = new Label();
		documentNameVersion.setSizeUndefined();
		documentNameVersion.setContentMode(ContentMode.TEXT);
		row1.addComponent(documentNameVersion);

        id = new Label();
		id.setSizeUndefined();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		row1.addComponent(id);

		HorizontalLayout row1ButtonLayout = new HorizontalLayout();
		row1ButtonLayout.setMargin(false);
		row1ButtonLayout.setSpacing(true);
		row1.addComponent(row1ButtonLayout);
		row1.setComponentAlignment(row1ButtonLayout, Alignment.MIDDLE_RIGHT);
		
		Button esfcssButton = new Button(vaadinUi.getMsg("LibDocumentVersionPageForm.button.esfcss.label"));
		esfcssButton.setStyleName(Reindeer.BUTTON_LINK);
		esfcssButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = 300105332856342521L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    		Window w = new Window(vaadinUi.getMsg("LibDocumentVersionPageForm.window.esfcss.caption"));
	    		w.center();
	    		w.setWidth(800, Unit.PIXELS);
	            w.setHeight(600, Unit.PIXELS);
	            w.setModal(true); // we find they get lost behind this document page editor window, so perhaps one-at-a-time will help
	            Label label = new Label();
	            label.setContentMode(ContentMode.PREFORMATTED);
	            label.setValue( vaadinUi.getEsfapp().getServletResourceAsString("/static/esf/esf.css") );

	            VerticalLayout wLayout = new VerticalLayout();
	            wLayout.setWidth(1800, Unit.PIXELS);
	            wLayout.addComponent(label);
	            w.setContent(wLayout);
	            vaadinUi.addWindowToUI(w);
			}
			
		});
		row1ButtonLayout.addComponent(esfcssButton);
		
		FieldSpecPopupView fieldSpecPopupView = new FieldSpecPopupView();
		row1ButtonLayout.addComponent(fieldSpecPopupView);
		
		Button tipsButton = new Button();
		tipsButton.setStyleName(Reindeer.BUTTON_LINK);
		tipsButton.addStyleName("tipsButton");
		tipsButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageForm.button.tips.icon")));
		tipsButton.setDescription(vaadinUi.getMsg("LibDocumentVersionPageForm.button.tips.tooltip"));
		tipsButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = 367033365657945833L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	        	final TipsView tipsView = new TipsView();
	            tipsView.initView();
	            tipsView.activateView(OpenMode.WINDOW,"LibraryDocumentProgramming");
	    		ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("LibraryDocumentProgrammingTipsView.caption"));
	    		w.center();
	    		w.setWidth(80, Unit.PERCENTAGE);
	            w.setHeight(90, Unit.PERCENTAGE);
	            w.setModal(true); // we find they get lost behind this document page editor window, so perhaps one-at-a-time will help
	            w.setView(tipsView); // so we can detect if our form has changes when closing
	            w.getLayout().addComponent(tipsView);
	            vaadinUi.addWindowToUI(w);
			}
			
		});
		row1ButtonLayout.addComponent(tipsButton);
		
        if ( ! documentVersionPageView.isReadOnly() ) {
        	VerticalLayout createNewButtonLayout = new VerticalLayout();
        	createNewButtonLayout.setSizeUndefined();
        	createNewButtonLayout.setMargin(false);
        	createNewButtonLayout.setSpacing(true);
        	createNewFieldButton = new Button(vaadinUi.getMsg("LibDocumentVersionPageForm.button.createNew.field.button.label"), new ClickListener() {
				private static final long serialVersionUID = 8145031665659304250L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		        	FieldTemplate newField = FieldTemplate.Manager.createNew(documentVersionPageView.docVerBean.duplicateDocumentVersion().getId(), new EsfName(vaadinUi.getMsg("createNew.esfname")));
		        	documentVersionPageView.fieldList.openWindow(newField);
		        	createNewButton.setPopupVisible(false);
				}
        		
        	});
        	createNewFieldButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.FieldList.icon")));
        	createNewFieldButton.setStyleName(Reindeer.BUTTON_LINK);
        	createNewButtonLayout.addComponent(createNewFieldButton);
        	
        	createNewPartyButton = new Button(vaadinUi.getMsg("LibDocumentVersionPageForm.button.createNew.party.button.label"), new ClickListener() {
				private static final long serialVersionUID = 8145031665659304250L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		        	PartyTemplate newParty = PartyTemplate.Manager.createNew(documentVersionPageView.docVerBean.duplicateDocumentVersion().getId(), new EsfName(vaadinUi.getMsg("createNew.esfname")));
		        	documentVersionPageView.partyList.openWindow(newParty);
		        	createNewButton.setPopupVisible(false);
				}
        		
        	});
        	createNewPartyButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.PartyList.icon")));
        	createNewPartyButton.setStyleName(Reindeer.BUTTON_LINK);
        	createNewButtonLayout.addComponent(createNewPartyButton);
        	
        	createNewFileButton = new Button(vaadinUi.getMsg("LibDocumentVersionPageForm.button.createNew.file.button.label"), new ClickListener() {
				private static final long serialVersionUID = 1L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					DocumentVersion.DocumentFile df = new DocumentVersion.DocumentFile();
		   			df.file = File.Manager.createNew(documentVersionPageView.docVerBean.duplicateDocumentVersion().getId());
		   			df.fileVersion = FileVersion.Manager.createTest(df.file, vaadinUi.getUser());
		   			df.file.promoteTestVersionToProduction(); // for single version files, test==production
		   			documentVersionPageView.fileList.openWindow(df);
		        	createNewButton.setPopupVisible(false);
				}
        		
        	});
        	createNewFileButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.FileList.icon")));
        	createNewFileButton.setStyleName(Reindeer.BUTTON_LINK);
        	createNewButtonLayout.addComponent(createNewFileButton);
        	
        	createNewImageButton = new Button(vaadinUi.getMsg("LibDocumentVersionPageForm.button.createNew.image.button.label"), new ClickListener() {
				private static final long serialVersionUID = -1053468016208440964L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					DocumentVersion.DocumentImage di = new DocumentVersion.DocumentImage();
		   			di.image = Image.Manager.createNew(documentVersionPageView.docVerBean.duplicateDocumentVersion().getId());
		   			di.imageVersion = ImageVersion.Manager.createTest(di.image, vaadinUi.getUser());
		   			di.image.promoteTestVersionToProduction(); // for single version images, test==production
		   			documentVersionPageView.imageList.openWindow(di);
		        	createNewButton.setPopupVisible(false);
				}
        		
        	});
        	createNewImageButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.DropdownList.icon")));
        	createNewImageButton.setStyleName(Reindeer.BUTTON_LINK);
        	createNewButtonLayout.addComponent(createNewImageButton);
        	
        	createNewDropdownButton = new Button(vaadinUi.getMsg("LibDocumentVersionPageForm.button.createNew.dropdown.button.label"), new ClickListener() {
				private static final long serialVersionUID = 3756201284908527494L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					DocumentVersion.DocumentDropdown dd = new DocumentVersion.DocumentDropdown();
		   			dd.dropdown = DropDown.Manager.createNew(documentVersionPageView.docVerBean.duplicateDocumentVersion().getId());
		   			dd.dropdownVersion = DropDownVersion.Manager.createTest(dd.dropdown, vaadinUi.getUser());
		   			dd.dropdown.promoteTestVersionToProduction(); // for single version drop downs, test==production
		   			documentVersionPageView.dropdownList.openWindow(dd);
		        	createNewButton.setPopupVisible(false);
				}
        		
        	});
        	createNewDropdownButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageView.DropdownList.icon")));
        	createNewDropdownButton.setStyleName(Reindeer.BUTTON_LINK);
        	createNewButtonLayout.addComponent(createNewDropdownButton);
        	
        	createNewButton = new PopupButton(vaadinUi.getMsg("LibDocumentVersionPageForm.button.createNew.popup.label"));
        	createNewButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageForm.button.createNew.popup.icon")));
        	createNewButton.setStyleName(Reindeer.BUTTON_SMALL);
        	createNewButton.setContent(createNewButtonLayout);
        	row1ButtonLayout.addComponent(createNewButton);
        }
        
		HorizontalLayout row2 = new HorizontalLayout();
		row2.setWidth(100,  Unit.PERCENTAGE);
		row2.setMargin(false);
		row2.setSpacing(true);
		layout.addComponent(row2);
		
    	esfname = new TextField();
    	esfname.setWidth(100, Unit.PERCENTAGE);
    	esfname.setRequired(true);
    	esfname.setImmediate(true); // so the validator can run
        esfname.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	esfname.setCaption(vaadinUi.getMsg("LibDocumentVersionPageForm.esfname.label"));
    	esfname.setDescription(vaadinUi.getMsg("LibDocumentVersionPageForm.esfname.tooltip"));
    	esfname.setConversionError(vaadinUi.getMsg("validator.esfname.message"));
    	fieldGroup.bind(esfname, "esfName");
    	row2.addComponent(esfname);
		
		NativeSelect editReviewType = new NativeSelect(vaadinUi.getMsg("LibDocumentVersionPageForm.editReviewType.label"));
		editReviewType.setDescription(vaadinUi.getMsg("LibDocumentVersionPageForm.editReviewType.tooltip"));
		editReviewType.setNullSelectionAllowed(false); 
		editReviewType.setImmediate(true);
		editReviewType.setRequired(true);
		editReviewType.setMultiSelect(false);
		editReviewType.addValidator(new SelectValidator(editReviewType));
		editReviewType.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		editReviewType.addItem(DocumentVersionPage.EDIT_REVIEW_TYPE_EDIT_AND_REVIEW);
		editReviewType.addItem(DocumentVersionPage.EDIT_REVIEW_TYPE_EDIT_ONLY);
		editReviewType.addItem(DocumentVersionPage.EDIT_REVIEW_TYPE_REVIEW_ONLY);
		editReviewType.setItemCaption(DocumentVersionPage.EDIT_REVIEW_TYPE_EDIT_AND_REVIEW, 
				vaadinUi.getPrettyCode().documentPageEditReviewType(DocumentVersionPage.EDIT_REVIEW_TYPE_EDIT_AND_REVIEW));
		editReviewType.setItemCaption(DocumentVersionPage.EDIT_REVIEW_TYPE_EDIT_ONLY, 
				vaadinUi.getPrettyCode().documentPageEditReviewType(DocumentVersionPage.EDIT_REVIEW_TYPE_EDIT_ONLY));
		editReviewType.setItemCaption(DocumentVersionPage.EDIT_REVIEW_TYPE_REVIEW_ONLY, 
				vaadinUi.getPrettyCode().documentPageEditReviewType(DocumentVersionPage.EDIT_REVIEW_TYPE_REVIEW_ONLY));
    	fieldGroup.bind(editReviewType, "editReviewType");
    	row2.addComponent(editReviewType);

		NativeSelect defaultNewFieldParty = new NativeSelect(vaadinUi.getMsg("LibDocumentVersionPageForm.defaultNewFieldParty.label"));
		defaultNewFieldParty.setDescription(vaadinUi.getMsg("LibDocumentVersionPageForm.defaultNewFieldParty.tooltip"));
		defaultNewFieldParty.setImmediate(false);
		defaultNewFieldParty.setNullSelectionAllowed(true);
		defaultNewFieldParty.addValidator(new SelectValidator(defaultNewFieldParty));
        loadDefaultNewFieldPartySelect(defaultNewFieldParty);
    	fieldGroup.bind(defaultNewFieldParty, "defaultNewFieldParty");
    	row2.addComponent(defaultNewFieldParty);
		
    	vaadinUi.getUser().releaseCKEditorContext(vaadinUi.getHttpSession(),editorContextId);
    	editorContextId = vaadinUi.getUser().createCKEditorContext(vaadinUi.getHttpSession());
    	CKEditorConfig config = new CKEditorConfig();
    	DocumentStyleVersion docStyleVer = documentVersionPageView.documentVersionForm.getCurrentBean().duplicateDocumentVersion().getDocumentStyle().getTestDocumentStyleVersion();
    	config.setupForOpenESignForms(vaadinUi.getRequestContextPath(),editorContextId,docStyleVer.getId().toNormalizedEsfNameString(),docStyleVer.getCssFileName());
    	config.setStartupFocus(true);
    	CKEditorTextField editor = new CKEditorTextField(config);
    	editor.setCaption(vaadinUi.getMsg("LibDocumentVersionPageForm.editor.label"));
    	editor.setImmediate(true);
    	editor.setSizeFull();
    	editor.setReadOnly(false);
    	// Got rid since seems to conflict with CKEditor FOCUS/BLUR
    	/*
    	editor.addFocusListener( new FieldEvents.FocusListener() {
			private static final long serialVersionUID = 8013321441028530858L;

			@Override
			public void focus(FocusEvent event) {
				System.out.println("TRACE on Server focus() - " + System.currentTimeMillis());
				saveButton.removeClickShortcut();
			}
    	});
    	editor.addBlurListener( new FieldEvents.BlurListener() {
			private static final long serialVersionUID = -5667961159450017959L;

			@Override
			public void blur(BlurEvent event) {
				System.out.println("TRACE on Server blur() - " + System.currentTimeMillis());
				saveButton.setClickShortcut(KeyCode.ENTER);
			}
		});
		*/
    	
    	editor.addVaadinSaveListener( new CKEditorTextField.VaadinSaveListener() {
			private static final long serialVersionUID = 5314219857424440568L;

			@Override
			public void vaadinSave(CKEditorTextField editor) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
	        	if ( ! fieldGroup.isValid() ) {
	    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
	        		return;
	        	}
				documentVersionPageView.save();
				vaadinUi.showStatus(vaadinUi.getMsg("form.save.success.message",getCurrentBeanName()));
			}
		});
    	
    	fieldGroup.bind(editor, "html");
    	layout.addComponent(editor);
    	layout.setExpandRatio(editor, 1.0f);

		buttonLayout = new HorizontalLayout();
		buttonLayout.setStyleName("footer");
		buttonLayout.setSpacing(false);
		buttonLayout.setMargin(false);
		layout.addComponent(buttonLayout);

		saveButton = new Button(vaadinUi.getMsg("LibDocumentVersionPageForm.button.save.label"));
    	// Got rid since seems to conflict with CKEditor FOCUS/BLUR
		//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
		//saveButton.setClickShortcut(KeyCode.ENTER);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
		saveButton.setDescription(vaadinUi.getMsg("LibDocumentVersionPageForm.button.save.tooltip"));
		saveButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = 5683498091963600219L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
	        	if ( ! fieldGroup.isValid() ) {
	    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
	        		return;
	        	}
				documentVersionPageView.save();
				vaadinUi.showStatus(vaadinUi.getMsg("form.save.success.message",getCurrentBeanName()));
				CKEditorTextField editor = (CKEditorTextField)fieldGroup.getField("html");
				editor.focus();
			}
			
		});
		buttonLayout.addComponent(saveButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"));
    	closeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	closeButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = -420066261357723326L;

			@Override
			public void buttonClick(ClickEvent event) {
	    		documentVersionPageView.close();
			}
		});
    	buttonLayout.addComponent(closeButton);
    	
		saveAndTestButton = new Button(vaadinUi.getMsg("LibDocumentVersionPageForm.button.saveAndTest.label"));
		saveAndTestButton.setStyleName("saveAndTestButton");
		saveAndTestButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageForm.button.saveAndTest.icon")));
		saveAndTestButton.setDescription(vaadinUi.getMsg("LibDocumentVersionPageForm.button.saveAndTest.tooltip"));
		saveAndTestButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = -6350865466851734948L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				if ( ! documentVersionPageView.isReadOnly() ) {
		    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
		        	if ( ! fieldGroup.isValid() ) {
		    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
		        		return;
		        	}
					documentVersionPageView.save();
					vaadinUi.showStatus(vaadinUi.getMsg("form.save.success.message",getCurrentBeanName()));
				}
				
	            final LibDocumentVersionTester docTester = new LibDocumentVersionTester(documentVersionPageView.docVerBean.duplicateDocumentVersion(), documentVersionPageView.docVerPage.getPageNumber());
	            docTester.initView();
	            docTester.activateView(OpenMode.WINDOW, "");
	    		ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow("Testing document: " + getCurrentBeanName());
	    		w.addStyleName("LibDocumentVersionTester");
	    		w.center();
	    		w.setWidth(800, Unit.PIXELS);
	            w.setHeight(95, Unit.PERCENTAGE);
	            w.setModal(true); // we find they get lost behind this document page editor window, so perhaps one-at-a-time will help
	            w.setView(docTester); // so we can detect if our form has changes when closing
	            w.getLayout().addComponent(docTester);
	            vaadinUi.addWindowToUI(w);
			}
    	});
		buttonLayout.addComponent(saveAndTestButton);
    	
    	documentProgrammingRuleButton = new Button(vaadinUi.getMsg("LibDocumentVersionPageForm.documentProgrammingRuleButton.label"));
    	documentProgrammingRuleButton.setStyleName("documentProgrammingRuleButton");
    	documentProgrammingRuleButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibDocumentVersionPageForm.documentProgrammingRuleButton.icon")));
    	documentProgrammingRuleButton.setDescription(vaadinUi.getMsg("LibDocumentVersionPageForm.documentProgrammingRuleButton.tooltip"));
    	documentProgrammingRuleButton.addClickListener( new ClickListener() {
			private static final long serialVersionUID = -3080161089197114281L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    		DocumentVersion duplicateDocumentVersion = documentVersionPageView.docVerBean.duplicateDocumentVersion();
	            DocumentProgrammingRuleView documentRuleView = new DocumentProgrammingRuleView(duplicateDocumentVersion);
	            documentRuleView.initView();
	    		
	            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("LibDocumentProgrammingRuleView.window.caption",duplicateDocumentVersion.getDocumentNameVersionWithLabel()), documentRuleView);
	            w.center();
	        	w.setWidth(90, Unit.PERCENTAGE);
	        	w.setHeight(95, Unit.PERCENTAGE);

	        	documentRuleView.activateView(EsfView.OpenMode.WINDOW, "");
	        	documentRuleView.setReadOnly(isReadOnly() || documentVersionPageView.docVerBean.isProductionVersion()); // if we're read-only or this is a production document, no modifications to custom logic is allowed
	        	documentRuleView.setParentWindow(w);
	        	vaadinUi.addWindowToUI(w);	
			}
			
		});
    	buttonLayout.addComponent(documentProgrammingRuleButton);
    	
    	_logger.debug("Form created");
	}
	
	// This is also called when we update the party list
	public void loadDefaultNewFieldPartySelect()
	{
		NativeSelect select = (NativeSelect)fieldGroup.getField("defaultNewFieldParty");
		loadDefaultNewFieldPartySelect(select);
	}
	public void loadDefaultNewFieldPartySelect(NativeSelect select)
	{
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		String nullParty = vaadinUi.getMsg("LibDocumentVersionPageForm.defaultNewFieldParty.select.none");
		
		EsfName selectedValue = (EsfName)select.getValue();
		boolean selectedValueFoundInNewList = false;
		
		select.removeAllItems();
		
		select.setNullSelectionItemId(nullParty);
		select.addItem(nullParty);
		for( PartyTemplate party : documentVersionPageView.documentVersionForm.getCurrentBean().duplicateDocumentVersion().getPartyTemplateList() ) {
			if ( party.getEsfName().equals(selectedValue) )
				selectedValueFoundInNewList = true;
			select.addItem(party.getEsfName());
		}
		
		if ( selectedValueFoundInNewList )
			select.setValue(selectedValue);
	}
	
	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("LibDocumentAndVersionsMainView.Document.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		Field<?> defaultNewFieldParty = fieldGroup.getField("defaultNewFieldParty");
		if ( defaultNewFieldParty != null ) // changes to this field should not trigger the page as being dirty
			defaultNewFieldParty.commit();
		return fieldGroup.isModified();
	}
	
	@Override
	public void detach() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		User user = vaadinUi.getUser();
		if ( user != null ) {
			user.releaseCKEditorContext(vaadinUi.getHttpSession(),editorContextId);
		}
    	fieldGroup.discard();
		super.detach();
	}

}