// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DocumentVersionInfo;
import com.esignforms.open.prog.DocumentVersionPage;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibDocumentVersionBean implements Serializable, Comparable<LibDocumentVersionBean> {
	private static final long serialVersionUID = -7052450580377682860L;

	private DocumentVersionInfo docVerInfo;
	private String versionLabel;
	
	private DocumentVersion duplicateDocumentVersion; // we work on a duplicate copy so if we have to abandon our changes, nothing is saved or stored in the cache
	
	public LibDocumentVersionBean(DocumentVersionInfo docVerInfo, String versionLabel) {
		this.docVerInfo = docVerInfo;
		this.versionLabel = versionLabel;
		loadDuplicateDocumentVersion();
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibDocumentVersionBean )
			return docVerInfo.equals(((LibDocumentVersionBean)o).docVerInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return docVerInfo.hashCode();
    }
	public int compareTo(LibDocumentVersionBean d) {
		return docVerInfo.compareTo(d.docVerInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public DocumentVersionInfo docVerInfo() {
		return docVerInfo;
	}
	
	public DocumentVersion duplicateDocumentVersion() {
		return duplicateDocumentVersion;
	}

	public DocumentVersionPage pageByName(EsfName pageName) {
		return duplicateDocumentVersion.getPageByName(pageName);
	}
	
	public DocumentVersionPage pageNumber(int pageNum) {
		return duplicateDocumentVersion.getPageNumber(pageNum);
	}
	
	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( duplicateDocumentVersion.save(con,vaadinUi.getUser()) ) {
			resetCaches();
			return true;
		} 

		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private void resetCaches() {
	}
	
	public synchronized void loadDuplicateDocumentVersion() {
		DocumentVersion docVer = DocumentVersion.Manager.getById(docVerInfo.getId());
		this.duplicateDocumentVersion = ( docVer != null ) ? docVer.duplicate() : null;
	}
	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return docVerInfo.getId();
	}

	public int getVersion() {
		return docVerInfo.getVersion();
	}
	
	public String getVersionLabel() {
		return versionLabel;
	}

	public EsfDateTime getCreatedTimestamp() {
		return docVerInfo.getCreatedTimestamp();
	}
	public String formatCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return docVerInfo.getCreatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return docVerInfo.getCreatedTimestamp().toLogString(vaadinUi.getUser());
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return docVerInfo.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return docVerInfo.getLastUpdatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return docVerInfo.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser());
	}

	public void setPageOrder(LinkedList<EsfName> pageOrderNames) {
		duplicateDocumentVersion.setPageOrder(pageOrderNames);
	}
	
	public EsfUUID getDocumentStyleId() {
		return duplicateDocumentVersion.getDocumentStyleId();
	}
	public void setDocumentStyleId(EsfUUID v) {
		duplicateDocumentVersion.setDocumentStyleId(v);
	}
	
	public String getPageOrientation() {
		return duplicateDocumentVersion.getPageOrientation();
	}
	public void setPageOrientation(String v) {
		duplicateDocumentVersion.setPageOrientation(v);
	}
	
	public String getEsignProcessRecordLocation() {
		return duplicateDocumentVersion.getEsignProcessRecordLocation();
	}
	public void setEsignProcessRecordLocation(String v) {
		duplicateDocumentVersion.setEsignProcessRecordLocation(v);
	}
	
	public  boolean isProductionVersion() {
		return duplicateDocumentVersion.getVersion() == duplicateDocumentVersion.getDocument().getProductionVersion();
	}

	public boolean isModified() {
		return duplicateDocumentVersion != null && duplicateDocumentVersion.hasChanged();
	}
}