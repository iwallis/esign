// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.user;

import org.vaadin.peter.buttongroup.ButtonGroup;
import org.vaadin.resetbuttonfortextfield.ResetButtonForTextField;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Property;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.Reindeer;

public class UserList extends Panel {
	private static final long serialVersionUID = 966422319025669634L;

	final UserView view;
	UserTable table;
	
	// For search bar
	TextField searchEmail;
	TextField searchFamilyName;
	TextField searchPersonalName;
	CheckBox searchEnabled;
	CheckBox searchDisabled;
	Button filterButton;
	Button showAllButton;

	public UserList(final UserView view, final UserViewContainer container) {
		super();
		this.view = view;
		setStyleName("UserList");
		setSizeFull();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
		
    	HorizontalLayout searchBar = new HorizontalLayout();
    	searchBar.addStyleName("searchBar");
    	searchBar.setSpacing(true);
    	searchBar.setMargin(false);
    	
    	String tooltipSearchString = vaadinUi.getMsg("tooltip.search.string");
    	
    	searchEmail = new TextField();
    	ResetButtonForTextField.extend(searchEmail);
    	searchEmail.setStyleName(Reindeer.TEXTFIELD_SMALL);
    	searchEmail.setInputPrompt(vaadinUi.getMsg("UserList.searchBar.email.label"));
    	searchEmail.setDescription(tooltipSearchString);
    	searchBar.addComponent(searchEmail);
    	
    	searchPersonalName = new TextField();
    	ResetButtonForTextField.extend(searchPersonalName);
    	searchPersonalName.setStyleName(Reindeer.TEXTFIELD_SMALL);
    	searchPersonalName.setInputPrompt(vaadinUi.getMsg("UserList.searchBar.personalName.label"));
    	searchPersonalName.setDescription(tooltipSearchString);   	
    	searchBar.addComponent(searchPersonalName);

    	searchFamilyName = new TextField();
    	ResetButtonForTextField.extend(searchFamilyName);
    	searchFamilyName.setStyleName(Reindeer.TEXTFIELD_SMALL);
    	searchFamilyName.setInputPrompt(vaadinUi.getMsg("UserList.searchBar.familyName.label"));
    	searchFamilyName.setDescription(tooltipSearchString);
    	searchBar.addComponent(searchFamilyName);

    	searchEnabled = new CheckBox(vaadinUi.getPrettyCode().status(Literals.STATUS_ENABLED));
    	searchBar.addComponent(searchEnabled);

    	searchDisabled = new CheckBox(vaadinUi.getPrettyCode().status(Literals.STATUS_DISABLED));
    	searchBar.addComponent(searchDisabled);

    	ButtonGroup buttonGroup = new ButtonGroup();
    	buttonGroup.setStyleName(Reindeer.BUTTON_SMALL);
    	searchBar.addComponent(buttonGroup);
    	
    	filterButton = new Button(vaadinUi.getMsg("UserList.searchBar.filteredButton.label"));
    	filterButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.filter.icon")));
    	filterButton.setDescription(vaadinUi.getMsg("button.filter.tooltip"));
    	filterButton.setStyleName(Reindeer.BUTTON_SMALL);
    	filterButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = -944051561492466764L;

			@Override
			public void buttonClick(ClickEvent event) {
				container.removeAllContainerFilters();
				String limitEmail = (String)searchEmail.getValue();
				if ( EsfString.isNonBlank(limitEmail) ) {
					boolean searchStartsWith = limitEmail.charAt(0) == Literals.SEARCH_STARTS_WITH_PREFIX;
					if ( searchStartsWith ) {
						limitEmail = limitEmail.substring(1);
					}
					container.addContainerFilter("email", limitEmail, true, searchStartsWith);
				}
				String limitPersonalName = (String)searchPersonalName.getValue();
				if ( EsfString.isNonBlank(limitPersonalName) ) {
					boolean searchStartsWith = limitPersonalName.charAt(0) == Literals.SEARCH_STARTS_WITH_PREFIX;
					if ( searchStartsWith ) {
						limitPersonalName = limitPersonalName.substring(1);
					}
					container.addContainerFilter("personalName", limitPersonalName, true, searchStartsWith);
				}
				String limitFamilyName = (String)searchFamilyName.getValue();
				if ( EsfString.isNonBlank(limitFamilyName) ) {
					boolean searchStartsWith = limitFamilyName.charAt(0) == Literals.SEARCH_STARTS_WITH_PREFIX;
					if ( searchStartsWith ) {
						limitFamilyName = limitFamilyName.substring(1);
					}
					container.addContainerFilter("familyName", limitFamilyName, true, searchStartsWith);
				}
				boolean isStatusEnabled = searchEnabled.getValue();
				boolean isStatusDisabled = searchDisabled.getValue();
				if ( (isStatusEnabled && isStatusDisabled) || (! isStatusEnabled && ! isStatusDisabled) ) {
					; // don't filter on status, both set or neither set
				} else if ( isStatusEnabled ){
					container.addContainerFilter("status", Literals.STATUS_ENABLED, false, false);
				} else {
					container.addContainerFilter("status", Literals.STATUS_DISABLED, false, false);
				}
				
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				vaadinUi.showStatus(vaadinUi.getMsg("list.search.filterButton.message",container.size()));
			}
		});
    	buttonGroup.addButton(filterButton);

    	showAllButton = new Button(vaadinUi.getMsg("UserList.searchBar.showAllButton.label"));
    	showAllButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.showAll.icon")));
    	showAllButton.setDescription(vaadinUi.getMsg("button.showAll.tooltip"));
    	showAllButton.setStyleName(Reindeer.BUTTON_SMALL);
    	showAllButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = -944051561492466764L;

			@Override
			public void buttonClick(ClickEvent event) {
				container.removeAllContainerFilters();
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				vaadinUi.showStatus(vaadinUi.getMsg("list.search.showAllButton.message",container.size()));
			}
		});
    	buttonGroup.addButton(showAllButton);
    	
    	table = new UserTable(view, container);
    	
    	layout.addComponent(searchBar);
    	layout.addComponent(table);
    	layout.setExpandRatio(table, 1);
	}
	
	public Table getTable() {
		return table;
	}
	
	class UserTable extends Table {
		private static final long serialVersionUID = 8498483887939262343L;

		public UserTable(UserView view, UserViewContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("UserList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("UserList.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);
			setCellStyleGenerator(new Table.CellStyleGenerator( ) {
				private static final long serialVersionUID = -5943122706048314047L;

				@Override
				public String getStyle(Table table, Object itemId, Object propertyId) {
					UserBean bean = (UserBean)itemId;
					return bean.isEnabled() ? null : "disabledText";
				}
			});
	        // Make table selectable, react immediately to user events, and pass events to the GroupView
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener((Property.ValueChangeListener)view);
	        setNullSelectionAllowed(true);
			setSizeFull();
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			if ( "lastUpdatedTimestamp".equals(colId) ) {
				return ((UserBean)rowId).formatLastUpdatedTimestamp();
			} else if ( "lastLoginTimestamp".equals(colId) ) {
				return ((UserBean)rowId).formatLastLoginTimestamp();
			}
			
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // UserTable

} // UserList