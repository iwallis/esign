// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.documentstyle;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.DocumentStyleVersion;
import com.esignforms.open.prog.DocumentStyleVersionInfo;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibDocumentStyleVersionBean implements Serializable, Comparable<LibDocumentStyleVersionBean> {
	private static final long serialVersionUID = -5105502209380556627L;

	private DocumentStyleVersionInfo documentstyleVerInfo;
	private String versionLabel;
	
	private DocumentStyleVersion documentstyleVersion;
	
	public LibDocumentStyleVersionBean(DocumentStyleVersionInfo documentstyleVerInfo, String versionLabel) {
		this.documentstyleVerInfo = documentstyleVerInfo;
		this.versionLabel = versionLabel;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibDocumentStyleVersionBean )
			return documentstyleVerInfo.equals(((LibDocumentStyleVersionBean)o).documentstyleVerInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return documentstyleVerInfo.hashCode();
    }
	public int compareTo(LibDocumentStyleVersionBean d) {
		return documentstyleVerInfo.compareTo(d.documentstyleVerInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public DocumentStyleVersionInfo documentstyleVerInfo() {
		return documentstyleVerInfo;
	}

	public DocumentStyleVersion documentstyleVersion() {
		if ( documentstyleVersion == null )
			documentstyleVersion = DocumentStyleVersion.Manager.getById(documentstyleVerInfo.getId());
		return documentstyleVersion;
	}

	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( documentstyleVersion().save(con,vaadinUi.getUser()) ) {
			resetCaches();
			return true;
		} 

		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	private synchronized void resetCaches() {
	}
	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return documentstyleVerInfo.getId();
	}

	public int getVersion() {
		return documentstyleVerInfo.getVersion();
	}
	
	public String getVersionLabel() {
		return versionLabel;
	}

	public EsfDateTime getCreatedTimestamp() {
		return documentstyleVerInfo.getCreatedTimestamp();
	}
	public String formatCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return documentstyleVerInfo.getCreatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return documentstyleVerInfo.getCreatedTimestamp().toLogString(vaadinUi.getUser());
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return documentstyleVerInfo.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return documentstyleVerInfo.getLastUpdatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return documentstyleVerInfo.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser());
	}
	
	private EsfString documentFont;
    public EsfString getDocumentFont()
    {
    	if ( documentFont == null )
    		documentFont = documentstyleVersion().getDocumentFont();
    	return documentFont;
    }
    public void setDocumentFont(EsfString v)
    {
    	documentFont = v;
    	documentstyleVersion().setDocumentFont(v);
    }

	private EsfString documentFontSize;
    public EsfString getDocumentFontSize()
    {
    	if ( documentFontSize == null )
    		documentFontSize = documentstyleVersion().getDocumentFontSize();
    	return documentFontSize;
    }
    public void setDocumentFontSize(EsfString v)
    {
    	documentFontSize = v;
    	documentstyleVersion().setDocumentFontSize(v);
    }

    private EsfString documentFontStyle;
    public EsfString getDocumentFontStyle()
    {
    	if ( documentFontStyle == null )
    		documentFontStyle = documentstyleVersion().getDocumentFontStyle();
    	return documentFontStyle;
    }
    public void setDocumentFontStyle(EsfString v)
    {
    	documentFontStyle = v;
    	documentstyleVersion().setDocumentFontStyle(v);
    }

    private EsfString documentFontColor;
    public EsfString getDocumentFontColor()
    {
    	if ( documentFontColor == null )
    		documentFontColor = documentstyleVersion().getDocumentFontColor();
    	return documentFontColor;
    }
    public void setDocumentFontColor(EsfString v)
    {
    	documentFontColor = v;
    	documentstyleVersion().setDocumentFontColor(v);
    }

    private EsfString documentBackgroundColor;
    public EsfString getDocumentBackgroundColor()
    {
    	if ( documentBackgroundColor == null )
    		documentBackgroundColor = documentstyleVersion().getDocumentBackgroundColor();
    	return documentBackgroundColor;
    }
    public void setDocumentBackgroundColor(EsfString v)
    {
    	documentBackgroundColor = v;
    	documentstyleVersion().setDocumentBackgroundColor(v);
    }

    private EsfString signatureFont;
    public EsfString getSignatureFont()
    {
    	if ( signatureFont == null )
    		signatureFont = documentstyleVersion().getSignatureFont();
    	return signatureFont;
    }
    public void setSignatureFont(EsfString v)
    {
    	signatureFont = v;
    	documentstyleVersion().setSignatureFont(v);
    }

    private EsfString signatureFontSize;
    public EsfString getSignatureFontSize()
    {
    	if ( signatureFontSize == null )
    		signatureFontSize = documentstyleVersion().getSignatureFontSize();
    	return signatureFontSize;
    }
    public void setSignatureFontSize(EsfString v)
    {
    	signatureFontSize = v;
    	documentstyleVersion().setSignatureFontSize(v);
    }

    private EsfString signatureFontStyle;
    public EsfString getSignatureFontStyle()
    {
    	if ( signatureFontStyle == null )
    		signatureFontStyle = documentstyleVersion().getSignatureFontStyle();
    	return signatureFontStyle;
    }
    public void setSignatureFontStyle(EsfString v)
    {
    	signatureFontStyle = v;
    	documentstyleVersion().setSignatureFontStyle(v);
    }

    private EsfString signatureFontColor;
    public EsfString getSignatureFontColor()
    {
    	if ( signatureFontColor == null )
    		signatureFontColor = documentstyleVersion().getSignatureFontColor();
    	return signatureFontColor;
    }
    public void setSignatureFontColor(EsfString v)
    {
    	signatureFontColor = v;
    	documentstyleVersion().setSignatureFontColor(v);
    }

    private EsfString signatureBackgroundColor;
    public EsfString getSignatureBackgroundColor()
    {
    	if ( signatureBackgroundColor == null )
    		signatureBackgroundColor = documentstyleVersion().getSignatureBackgroundColor();
    	return signatureBackgroundColor;
    }
    public void setSignatureBackgroundColor(EsfString v)
    {
    	signatureBackgroundColor = v;
    	documentstyleVersion().setSignatureBackgroundColor(v);
    }

    private EsfString fieldDataFont;
    public EsfString getFieldDataFont()
    {
    	if ( fieldDataFont == null )
    		fieldDataFont = documentstyleVersion().getFieldDataFont();
    	return fieldDataFont;
    }
    public void setFieldDataFont(EsfString v)
    {
    	fieldDataFont = v;
    	documentstyleVersion().setFieldDataFont(v);
    }

    private EsfString fieldDataFontSize;
    public EsfString getFieldDataFontSize()
    {
    	if ( fieldDataFontSize == null )
    		fieldDataFontSize = documentstyleVersion().getFieldDataFontSize();
    	return fieldDataFontSize;
    }
    public void setFieldDataFontSize(EsfString v)
    {
    	fieldDataFontSize = v;
    	documentstyleVersion().setFieldDataFontSize(v);
    }

    private EsfString fieldDataFontStyle;
    public EsfString getFieldDataFontStyle()
    {
    	if ( fieldDataFontStyle == null )
    		fieldDataFontStyle = documentstyleVersion().getFieldDataFontStyle();
    	return fieldDataFontStyle;
    }
    public void setFieldDataFontStyle(EsfString v)
    {
    	fieldDataFontStyle = v;
    	documentstyleVersion().setFieldDataFontStyle(v);
    }

    private EsfString fieldDataFontColor;
    public EsfString getFieldDataFontColor()
    {
    	if ( fieldDataFontColor == null )
    		fieldDataFontColor = documentstyleVersion().getFieldDataFontColor();
    	return fieldDataFontColor;
    }
    public void setFieldDataFontColor(EsfString v)
    {
    	fieldDataFontColor = v;
    	documentstyleVersion().setFieldDataFontColor(v);
    }

    private EsfString fieldDataBackgroundColor;
    public EsfString getFieldDataBackgroundColor()
    {
    	if ( fieldDataBackgroundColor == null )
    		fieldDataBackgroundColor = documentstyleVersion().getFieldDataBackgroundColor();
    	return fieldDataBackgroundColor;
    }
    public void setFieldDataBackgroundColor(EsfString v)
    {
    	fieldDataBackgroundColor = v;
    	documentstyleVersion().setFieldDataBackgroundColor(v);
    }

    private EsfString inputFieldRequiredFont;
    public EsfString getInputFieldRequiredFont()
    {
    	if ( inputFieldRequiredFont == null )
    		inputFieldRequiredFont = documentstyleVersion().getInputFieldRequiredFont();
    	return inputFieldRequiredFont;
    }
    public void setInputFieldRequiredFont(EsfString v)
    {
    	inputFieldRequiredFont = null;
    	documentstyleVersion().setInputFieldRequiredFont(v);
    }

    private EsfString inputFieldRequiredFontSize;
    public EsfString getInputFieldRequiredFontSize()
    {
    	if ( inputFieldRequiredFontSize == null )
    		inputFieldRequiredFontSize = documentstyleVersion().getInputFieldRequiredFontSize();
    	return inputFieldRequiredFontSize;
    }
    public void setInputFieldRequiredFontSize(EsfString v)
    {
    	inputFieldRequiredFontSize = v;
    	documentstyleVersion().setInputFieldRequiredFontSize(v);
    }

    private EsfString inputFieldRequiredFontStyle;
    public EsfString getInputFieldRequiredFontStyle()
    {
    	if ( inputFieldRequiredFontStyle == null )
    		inputFieldRequiredFontStyle = documentstyleVersion().getInputFieldRequiredFontStyle();
    	return inputFieldRequiredFontStyle;
    }
    public void setInputFieldRequiredFontStyle(EsfString v)
    {
    	inputFieldRequiredFontStyle = v;
    	documentstyleVersion().setInputFieldRequiredFontStyle(v);
    }

    private EsfString inputFieldRequiredFontColor;
    public EsfString getInputFieldRequiredFontColor()
    {
    	if ( inputFieldRequiredFontColor == null )
    		inputFieldRequiredFontColor = documentstyleVersion().getInputFieldRequiredFontColor();
    	return inputFieldRequiredFontColor;
    }
    public void setInputFieldRequiredFontColor(EsfString v)
    {
    	inputFieldRequiredFontColor = v;
    	documentstyleVersion().setInputFieldRequiredFontColor(v);
    }

    private EsfString inputFieldRequiredBackgroundColor;
    public EsfString getInputFieldRequiredBackgroundColor()
    {
    	if ( inputFieldRequiredBackgroundColor == null )
    		inputFieldRequiredBackgroundColor = documentstyleVersion().getInputFieldRequiredBackgroundColor();
    	return inputFieldRequiredBackgroundColor;
    }
    public void setInputFieldRequiredBackgroundColor(EsfString v)
    {
    	inputFieldRequiredBackgroundColor = v;
    	documentstyleVersion().setInputFieldRequiredBackgroundColor(v);
    }

    private EsfString inputFieldRequiredBorderTypes;
    public EsfString getInputFieldRequiredBorderTypes()
    {
    	if ( inputFieldRequiredBorderTypes == null )
    		inputFieldRequiredBorderTypes = documentstyleVersion().getInputFieldRequiredBorderTypes();
    	return inputFieldRequiredBorderTypes;
    }
    public void setInputFieldRequiredBorderTypes(EsfString v)
    {
    	inputFieldRequiredBorderTypes = v;
    	documentstyleVersion().setInputFieldRequiredBorderTypes(v);
    }

    private EsfString inputFieldOptionalFont;
    public EsfString getInputFieldOptionalFont()
    {
    	if ( inputFieldOptionalFont == null )
    		inputFieldOptionalFont = documentstyleVersion().getInputFieldOptionalFont();
    	return inputFieldOptionalFont;
    }
    public void setInputFieldOptionalFont(EsfString v)
    {
    	inputFieldOptionalFont = v;
    	documentstyleVersion().setInputFieldOptionalFont(v);
    }

    private EsfString inputFieldOptionalFontSize;
    public EsfString getInputFieldOptionalFontSize()
    {
    	if ( inputFieldOptionalFontSize == null )
    		inputFieldOptionalFontSize = documentstyleVersion().getInputFieldOptionalFontSize();
    	return inputFieldOptionalFontSize;
    }
    public void setInputFieldOptionalFontSize(EsfString v)
    {
    	inputFieldOptionalFontSize = v;
    	documentstyleVersion().setInputFieldOptionalFontSize(v);
    }

    private EsfString inputFieldOptionalFontStyle;
    public EsfString getInputFieldOptionalFontStyle()
    {
    	if ( inputFieldOptionalFontStyle == null )
    		inputFieldOptionalFontStyle = documentstyleVersion().getInputFieldOptionalFontStyle();
    	return inputFieldOptionalFontStyle;
    }
    public void setInputFieldOptionalFontStyle(EsfString v)
    {
    	inputFieldOptionalFontStyle = v;
    	documentstyleVersion().setInputFieldOptionalFontStyle(v);
    }

    private EsfString inputFieldOptionalFontColor;
    public EsfString getInputFieldOptionalFontColor()
    {
    	if ( inputFieldOptionalFontColor == null )
    		inputFieldOptionalFontColor = documentstyleVersion().getInputFieldOptionalFontColor();
    	return inputFieldOptionalFontColor;
    }
    public void setInputFieldOptionalFontColor(EsfString v)
    {
    	inputFieldOptionalFontColor = v;
    	documentstyleVersion().setInputFieldOptionalFontColor(v);
    }

    private EsfString inputFieldOptionalBackgroundColor;
    public EsfString getInputFieldOptionalBackgroundColor()
    {
    	if ( inputFieldOptionalBackgroundColor == null )
    		inputFieldOptionalBackgroundColor = documentstyleVersion().getInputFieldOptionalBackgroundColor();
    	return inputFieldOptionalBackgroundColor;
    }
    public void setInputFieldOptionalBackgroundColor(EsfString v)
    {
    	inputFieldOptionalBackgroundColor = v;
    	documentstyleVersion().setInputFieldOptionalBackgroundColor(v);
    }

    private EsfString inputFieldOptionalBorderTypes;
    public EsfString getInputFieldOptionalBorderTypes()
    {
    	if ( inputFieldOptionalBorderTypes == null )
    		inputFieldOptionalBorderTypes = documentstyleVersion().getInputFieldOptionalBorderTypes();
    	return inputFieldOptionalBorderTypes;
    }
    public void setInputFieldOptionalBorderTypes(EsfString v)
    {
    	inputFieldOptionalBorderTypes = v;
    	documentstyleVersion().setInputFieldOptionalBorderTypes(v);
    }

    private EsfString inputFieldErrorFont;
    public EsfString getInputFieldErrorFont()
    {
    	if ( inputFieldErrorFont == null )
    		inputFieldErrorFont = documentstyleVersion().getInputFieldErrorFont();
    	return inputFieldErrorFont;
    }
    public void setInputFieldErrorFont(EsfString v)
    {
    	inputFieldErrorFont = v;
    	documentstyleVersion().setInputFieldErrorFont(v);
    }

    private EsfString inputFieldErrorFontSize;
    public EsfString getInputFieldErrorFontSize()
    {
    	if ( inputFieldErrorFontSize == null )
    		inputFieldErrorFontSize = documentstyleVersion().getInputFieldErrorFontSize();
    	return inputFieldErrorFontSize;
    }
    public void setInputFieldErrorFontSize(EsfString v)
    {
    	inputFieldErrorFontSize = v;
    	documentstyleVersion().setInputFieldErrorFontSize(v);
    }

    private EsfString inputFieldErrorFontStyle;
    public EsfString getInputFieldErrorFontStyle()
    {
    	if ( inputFieldErrorFontStyle == null )
    		inputFieldErrorFontStyle = documentstyleVersion().getInputFieldErrorFontStyle();
    	return inputFieldErrorFontStyle;
    }
    public void setInputFieldErrorFontStyle(EsfString v)
    {
    	inputFieldErrorFontStyle = v;
    	documentstyleVersion().setInputFieldErrorFontStyle(v);
    }

    private EsfString inputFieldErrorFontColor;
    public EsfString getInputFieldErrorFontColor()
    {
	   	if ( inputFieldErrorFontColor == null )
	   		inputFieldErrorFontColor = documentstyleVersion().getInputFieldErrorFontColor();
    	return inputFieldErrorFontColor;
    }
    public void setInputFieldErrorFontColor(EsfString v)
    {
    	inputFieldErrorFontColor = v;
    	documentstyleVersion().setInputFieldErrorFontColor(v);
    }

    private EsfString inputFieldErrorBackgroundColor;
    public EsfString getInputFieldErrorBackgroundColor()
    {
    	if ( inputFieldErrorBackgroundColor == null )
    		inputFieldErrorBackgroundColor = documentstyleVersion().getInputFieldErrorBackgroundColor();
    	return inputFieldErrorBackgroundColor;
    }
    public void setInputFieldErrorBackgroundColor(EsfString v)
    {
    	inputFieldErrorBackgroundColor = v;
    	documentstyleVersion().setInputFieldErrorBackgroundColor(v);
    }

    private EsfString inputFieldErrorBorderTypes;
    public EsfString getInputFieldErrorBorderTypes()
    {
    	if ( inputFieldErrorBorderTypes == null )
    		inputFieldErrorBorderTypes = documentstyleVersion().getInputFieldErrorBorderTypes();
    	return inputFieldErrorBorderTypes;
    }
    public void setInputFieldErrorBorderTypes(EsfString v)
    {
    	inputFieldErrorBorderTypes = v;
    	documentstyleVersion().setInputFieldErrorBorderTypes(v);
    }
    
    private EsfString normalLabelFont;
    public EsfString getNormalLabelFont()
    {
    	if ( normalLabelFont == null )
    		normalLabelFont = documentstyleVersion().getNormalLabelFont();
    	return normalLabelFont;
    }
    public void setNormalLabelFont(EsfString v)
    {
    	normalLabelFont = v;
    	documentstyleVersion().setNormalLabelFont(v);
    }

    private EsfString normalLabelFontSize;
    public EsfString getNormalLabelFontSize()
    {
    	if ( normalLabelFontSize == null )
    		normalLabelFontSize = documentstyleVersion().getNormalLabelFontSize();
    	return normalLabelFontSize;
    }
    public void setNormalLabelFontSize(EsfString v)
    {
    	normalLabelFontSize = v;
    	documentstyleVersion().setNormalLabelFontSize(v);
    }

    private EsfString normalLabelFontStyle;
    public EsfString getNormalLabelFontStyle()
    {
    	if ( normalLabelFontStyle == null )
    		normalLabelFontStyle = documentstyleVersion().getNormalLabelFontStyle();
    	return normalLabelFontStyle;
    }
    public void setNormalLabelFontStyle(EsfString v)
    {
    	normalLabelFontStyle = v;
    	documentstyleVersion().setNormalLabelFontStyle(v);
    }

    private EsfString normalLabelFontColor;
    public EsfString getNormalLabelFontColor()
    {
    	if ( normalLabelFontColor == null )
    		normalLabelFontColor = documentstyleVersion().getNormalLabelFontColor();
    	return normalLabelFontColor;
    }
    public void setNormalLabelFontColor(EsfString v)
    {
    	normalLabelFontColor = v;
    	documentstyleVersion().setNormalLabelFontColor(v);
    }

    private EsfString normalLabelBackgroundColor;
    public EsfString getNormalLabelBackgroundColor()
    {
    	if ( normalLabelBackgroundColor == null )
    		normalLabelBackgroundColor = documentstyleVersion().getNormalLabelBackgroundColor();
    	return normalLabelBackgroundColor;
    }
    public void setNormalLabelBackgroundColor(EsfString v)
    {
    	normalLabelBackgroundColor = v;
    	documentstyleVersion().setNormalLabelBackgroundColor(v);
    }

    private EsfString normalLabelErrorFont;
    public EsfString getNormalLabelErrorFont()
    {
    	if ( normalLabelErrorFont == null )
    		normalLabelErrorFont = documentstyleVersion().getNormalLabelErrorFont();
    	return normalLabelErrorFont;
    }
    public void setNormalLabelErrorFont(EsfString v)
    {
    	normalLabelErrorFont = v;
    	documentstyleVersion().setNormalLabelErrorFont(v);
    }

    private EsfString normalLabelErrorFontSize;
    public EsfString getNormalLabelErrorFontSize()
    {
    	if ( normalLabelErrorFontSize == null )
    		normalLabelErrorFontSize = documentstyleVersion().getNormalLabelErrorFontSize();
    	return normalLabelErrorFontSize;
    }
    public void setNormalLabelErrorFontSize(EsfString v)
    {
    	normalLabelErrorFontSize = v;
    	documentstyleVersion().setNormalLabelErrorFontSize(v);
    }

    private EsfString normalLabelErrorFontStyle;
    public EsfString getNormalLabelErrorFontStyle()
    {
    	if ( normalLabelErrorFontStyle == null )
    		normalLabelErrorFontStyle = documentstyleVersion().getNormalLabelErrorFontStyle();
    	return normalLabelErrorFontStyle;
    }
    public void setNormalLabelErrorFontStyle(EsfString v)
    {
    	normalLabelErrorFontStyle = v;
    	documentstyleVersion().setNormalLabelErrorFontStyle(v);
    }

    private EsfString normalLabelErrorFontColor;
    public EsfString getNormalLabelErrorFontColor()
    {
    	if ( normalLabelErrorFontColor == null )
    		normalLabelErrorFontColor = documentstyleVersion().getNormalLabelErrorFontColor();
    	return normalLabelErrorFontColor;
    }
    public void setNormalLabelErrorFontColor(EsfString v)
    {
    	normalLabelErrorFontColor = v;
    	documentstyleVersion().setNormalLabelErrorFontColor(v);
    }

    private EsfString normalLabelErrorBackgroundColor;
    public EsfString getNormalLabelErrorBackgroundColor()
    {
    	if ( normalLabelErrorBackgroundColor == null )
    		normalLabelErrorBackgroundColor = documentstyleVersion().getNormalLabelErrorBackgroundColor();
    	return normalLabelErrorBackgroundColor;
    }
    public void setNormalLabelErrorBackgroundColor(EsfString v)
    {
    	normalLabelErrorBackgroundColor = v;
    	documentstyleVersion().setNormalLabelErrorBackgroundColor(v);
    }

    private EsfString smallLabelFont;
    public EsfString getSmallLabelFont()
    {
    	if ( smallLabelFont == null )
    		smallLabelFont = documentstyleVersion().getSmallLabelFont();
    	return smallLabelFont;
    }
    public void setSmallLabelFont(EsfString v)
    {
    	smallLabelFont = v;
    	documentstyleVersion().setSmallLabelFont(v);
    }

    private EsfString smallLabelFontSize;
    public EsfString getSmallLabelFontSize()
    {
    	if ( smallLabelFontSize == null )
    		smallLabelFontSize = documentstyleVersion().getSmallLabelFontSize();
    	return smallLabelFontSize;
    }
    public void setSmallLabelFontSize(EsfString v)
    {
    	smallLabelFontSize = v;
    	documentstyleVersion().setSmallLabelFontSize(v);
    }

    private EsfString smallLabelFontStyle;
    public EsfString getSmallLabelFontStyle()
    {
    	if ( smallLabelFontStyle == null )
    		smallLabelFontStyle = documentstyleVersion().getSmallLabelFontStyle();
    	return smallLabelFontStyle;
    }
    public void setSmallLabelFontStyle(EsfString v)
    {
    	smallLabelFontStyle = v;
    	documentstyleVersion().setSmallLabelFontStyle(v);
    }

    private EsfString smallLabelFontColor;
    public EsfString getSmallLabelFontColor()
    {
    	if ( smallLabelFontColor == null )
    		smallLabelFontColor = documentstyleVersion().getSmallLabelFontColor();
    	return smallLabelFontColor;
    }
    public void setSmallLabelFontColor(EsfString v)
    {
    	smallLabelFontColor = v;
    	documentstyleVersion().setSmallLabelFontColor(v);
    }

    private EsfString smallLabelBackgroundColor;
    public EsfString getSmallLabelBackgroundColor()
    {
    	if ( smallLabelBackgroundColor == null )
    		smallLabelBackgroundColor = documentstyleVersion().getSmallLabelBackgroundColor();
    	return smallLabelBackgroundColor;
    }
    public void setSmallLabelBackgroundColor(EsfString v)
    {
    	smallLabelBackgroundColor = v;
    	documentstyleVersion().setSmallLabelBackgroundColor(v);
    }

    private EsfString smallLabelErrorFont;
    public EsfString getSmallLabelErrorFont()
    {
    	if ( smallLabelErrorFont == null )
    		smallLabelErrorFont = documentstyleVersion().getSmallLabelErrorFont();
    	return smallLabelErrorFont;
    }
    public void setSmallLabelErrorFont(EsfString v)
    {
    	smallLabelErrorFont = v;
    	documentstyleVersion().setSmallLabelErrorFont(v);
    }

    private EsfString smallLabelErrorFontSize;
    public EsfString getSmallLabelErrorFontSize()
    {
    	if ( smallLabelErrorFontSize == null )
    		smallLabelErrorFontSize = documentstyleVersion().getSmallLabelErrorFontSize();
    	return smallLabelErrorFontSize;
    }
    public void setSmallLabelErrorFontSize(EsfString v)
    {
    	smallLabelErrorFontSize = v;
    	documentstyleVersion().setSmallLabelErrorFontSize(v);
    }

    private EsfString smallLabelErrorFontStyle;
    public EsfString getSmallLabelErrorFontStyle()
    {
    	if ( smallLabelErrorFontStyle == null )
    		smallLabelErrorFontStyle = documentstyleVersion().getSmallLabelErrorFontStyle();
    	return smallLabelErrorFontStyle;
    }
    public void setSmallLabelErrorFontStyle(EsfString v)
    {
    	smallLabelErrorFontStyle = v;
    	documentstyleVersion().setSmallLabelErrorFontStyle(v);
    }

    private EsfString smallLabelErrorFontColor;
    public EsfString getSmallLabelErrorFontColor()
    {
    	if ( smallLabelErrorFontColor == null )
    		smallLabelErrorFontColor = documentstyleVersion().getSmallLabelErrorFontColor();
    	return smallLabelErrorFontColor;
    }
    public void setSmallLabelErrorFontColor(EsfString v)
    {
    	smallLabelErrorFontColor = v;
    	documentstyleVersion().setSmallLabelErrorFontColor(v);
    }

    private EsfString smallLabelErrorBackgroundColor;
    public EsfString getSmallLabelErrorBackgroundColor()
    {
    	if ( smallLabelErrorBackgroundColor == null )
    		smallLabelErrorBackgroundColor = documentstyleVersion().getSmallLabelErrorBackgroundColor();
    	return smallLabelErrorBackgroundColor;
    }
    public void setSmallLabelErrorBackgroundColor(EsfString v)
    {
    	smallLabelErrorBackgroundColor = v;
    	documentstyleVersion().setSmallLabelErrorBackgroundColor(v);
    }

}