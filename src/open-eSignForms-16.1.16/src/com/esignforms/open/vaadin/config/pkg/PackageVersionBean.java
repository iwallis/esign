// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.PackageProgramming;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionInfo;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class PackageVersionBean implements Serializable, Comparable<PackageVersionBean> {
	private static final long serialVersionUID = 41851288438426873L;

	private PackageVersionInfo packageVerInfo;
	private String versionLabel;
	
	private PackageVersion duplicatedPackageVersion; // we work on a duplicate copy so if we have to abandon our changes, nothing is saved or stored in the cache
	
	public PackageVersionBean(PackageVersionInfo packageVerInfo, String versionLabel) {
		this.packageVerInfo = packageVerInfo;
		this.versionLabel = versionLabel;
		resetCaches();
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof PackageVersionBean )
			return packageVerInfo.equals(((PackageVersionBean)o).packageVerInfo);
		return false;
	}
	@Override
    public int hashCode() {
    	return packageVerInfo.hashCode();
    }
	public int compareTo(PackageVersionBean d) {
		return packageVerInfo.compareTo(d.packageVerInfo);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public PackageVersionInfo packageVerInfo() {
		return packageVerInfo;
	}

	public PackageVersion duplicatedPackageVersion() {
		return duplicatedPackageVersion;
	}
	
	public PackageVersionBean createLike(Package pkg) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		PackageVersion newPackageVer = PackageVersion.Manager.createLike(pkg, duplicatedPackageVersion(), vaadinUi.getUser());
		PackageVersionInfo newPackageVerInfo = new PackageVersionInfo(newPackageVer);
		
		PackageVersionBean newBean = new PackageVersionBean(newPackageVerInfo,Literals.VERSION_LABEL_TEST); // The new pkg version will always be Test.
		newBean.duplicatedPackageVersion = newPackageVer; // since this is a new package version, we'll use it rather than any duplicate that will be missing the programming rules (since the blob hasn't been saved yet, it's ID won't find a DB match)
		return newBean;
	}

	public boolean save(Connection con, Errors errors) throws SQLException {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( duplicatedPackageVersion.save(con,vaadinUi.getUser()) ) {
			resetCaches();
			return true;
		} 

		if ( errors != null ) {
			errors.addError(vaadinUi.getMsg("form.save.failed.message"));
		}
		return false;
	}
	
	public synchronized void resetCaches() {
		this.duplicatedPackageVersion = PackageVersion.Manager.getById(packageVerInfo.getId()).duplicate();
	}
	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return packageVerInfo.getId();
	}

	public int getVersion() {
		return packageVerInfo.getVersion();
	}
	
	public String getVersionLabel() {
		return versionLabel;
	}

	public int getNumDocuments() {
		return packageVerInfo.getNumDocuments();
	}
	
	public int getNumParties() {
		return packageVerInfo.getNumParties();
	}
	
	public EsfDateTime getCreatedTimestamp() {
		return packageVerInfo.getCreatedTimestamp();
	}
	public String formatCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return packageVerInfo.getCreatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogCreatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return packageVerInfo.getCreatedTimestamp().toLogString(vaadinUi.getUser());
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return packageVerInfo.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return packageVerInfo.getLastUpdatedTimestamp().toString(vaadinUi.getUser());
	}
	public String formatLogLastUpdatedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return packageVerInfo.getLastUpdatedTimestamp().toLogString(vaadinUi.getUser());
	}
	
	public  boolean isProductionVersion() {
		return duplicatedPackageVersion.getVersion() == duplicatedPackageVersion.getPackage().getProductionVersion();
	}
	
	public boolean isModified() {
		if ( duplicatedPackageVersion.hasPackageProgramming() ) {
			PackageProgramming prog = duplicatedPackageVersion.getPackageProgramming();
			if ( prog != null && prog.hasChanged() ) {
				return true;
			}
		}
		return duplicatedPackageVersion.hasChanged();
	}
	
	public EsfUUID getPackageDocumentId()
	{
		return duplicatedPackageVersion.getPackageDocumentId();
	}
	public void setPackageDocumentId(EsfUUID v)
	{
		duplicatedPackageVersion.setPackageDocumentId(v);
	}
	
	public EsfUUID getButtonMessageId()
	{
		return duplicatedPackageVersion.getButtonMessageId();
	}
	public void setButtonMessageId(EsfUUID v)
	{
		duplicatedPackageVersion.setButtonMessageId(v);
	}
	
	public List<EsfUUID> getDocumentIdList() {
		return duplicatedPackageVersion._getDocumentIdListInternal();
	}
	public void addDocument(Document document) {
		duplicatedPackageVersion.addDocumentAndPartiesToTestPackage(document);
	}
	public void removeDocumentId(EsfUUID docId) {
		duplicatedPackageVersion.removeDocumentId(docId);
	}
	public void setDocumentIdOrder(List<EsfUUID> orderList) {
		duplicatedPackageVersion.setDocumentIdOrder(orderList);
	}
	
	public PackageVersionPartyTemplate addViewOnlyParty() {
		return duplicatedPackageVersion.addViewOnlyParty();
	}
	
	public List<PackageVersionPartyTemplate> getPackageVersionPartyTemplateList() {
		return duplicatedPackageVersion._getPackageVersionPartyTemplateListInternal();
	}
	public void setPackageVersionPartyTemplateIdOrder(List<EsfUUID> orderList) {
		duplicatedPackageVersion.setPackageVersionPartyTemplateIdOrder(orderList);
	}
	public void removePackageVersionPartyTemplate(PackageVersionPartyTemplate packagePartyTemplate) {
		duplicatedPackageVersion.removePackageVersionPartyTemplate(packagePartyTemplate);
	}
}