// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.party;

import java.io.Serializable;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.PartyTemplate;
import com.esignforms.open.user.Group;
import com.esignforms.open.vaadin.EsfVaadinUI;


public class LibPartyBean implements Serializable, Comparable<LibPartyBean> {
	private static final long serialVersionUID = -2299947125267338754L;

	private PartyTemplate partyTemplate;
	private EsfName originalEsfName;
	
	public LibPartyBean(PartyTemplate partyTemplate) {
		this.partyTemplate = partyTemplate;
		this.originalEsfName = partyTemplate.getEsfName();
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o != null && o instanceof LibPartyBean )
			return partyTemplate.equals(((LibPartyBean)o).partyTemplate);
		return false;
	}
	@Override
    public int hashCode() {
    	return partyTemplate.hashCode();
    }
	public int compareTo(LibPartyBean p) {
		return partyTemplate.compareTo(p.partyTemplate);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	public PartyTemplate partyTemplate() {
		return partyTemplate;
	}
	
	public boolean save(Errors errors) {
		boolean ret = partyTemplate.save();
		if ( ret ) {
			resetCaches();
		} else {
			if ( errors != null ) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				errors.addError(vaadinUi.getMsg("form.save.failed.message"));
			}
		}
		return ret;
	}
	
	public boolean delete(Errors errors) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return partyTemplate.delete(errors,vaadinUi.getUser());
	}

	
	private synchronized void resetCaches() {
		originalEsfName = getEsfName();
	}

	
	// Now for the JavaBeans methods
	public EsfName getEsfName() {
		return partyTemplate.getEsfName();
	}
	public void setEsfName(EsfName v) {
    	if ( v == null || ! v.isValid() )
    		return;
    	if ( v.hasReservedPrefix() && ! PartyTemplate.ESF_PARTY_NAME_REPORTS_ACCESS.equals(v) ) // the only reserved name allowed is the reports access name
    		return;
		partyTemplate.setEsfName(v);
	}
	public EsfName getOriginalEsfName() {
		return originalEsfName;
	}

	public EsfUUID getId() {
		return partyTemplate.getId();
	}
	
	public short getProcessOrder() {
		return partyTemplate.getProcessOrder();
	}
	
	public String getDisplayName() {
		return partyTemplate.getDisplayName();
	}
	public void setDisplayName(String v) {
		if ( EsfString.isNonBlank(v) ) {
			partyTemplate.setDisplayName(v);
		}
	}
	public EsfUUID getTodoGroupId() {
		return partyTemplate.getTodoGroupId();
	}
	public String getTodoGroupPathName() {
		return ( partyTemplate.hasTodoGroupId() ) ? Group.Manager.getById(partyTemplate.getTodoGroupId()).getPathName().toString() : "";
	}
	public void setTodoGroupId(EsfUUID v) {
		partyTemplate.setTodoGroupId(v);
	}
	
	public EsfUUID getTestTodoGroupId() {
		return partyTemplate.getTestTodoGroupId();
	}
	public String getTestTodoGroupPathName() {
		return ( partyTemplate.hasTestTodoGroupId() ) ? Group.Manager.getById(partyTemplate.getTestTodoGroupId()).getPathName().toString() : "";
	}
	public void setTestTodoGroupId(EsfUUID v) {
		partyTemplate.setTestTodoGroupId(v);
	}
}