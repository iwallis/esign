// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming;

import java.util.HashMap;

import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

public class LibraryProgrammingView extends HorizontalSplitPanel implements EsfView {
	private static final long serialVersionUID = 2821592671490451246L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibraryProgrammingView.class);
    
	String libraryId;
	LibraryProgrammingNavigationTree navTree; // left side of split panel
	LibraryProgrammingTabSheet viewTabs; // right side of split panel
	HashMap<Class<EsfView>, EsfView> viewClassToInstanceCache = new HashMap<Class<EsfView>, EsfView>();
	
	String defaultViewName; // set by the LibraryProgrammingNavigationTree once it processes its configuration
	
	public LibraryProgrammingView() {
		super();
        setSplitPosition(160, Unit.PIXELS);
		setStyleName(Reindeer.SPLITPANEL_SMALL);
	}
	
	protected void buildLayout() {
		_logger.debug("buildLayout()");
		
		VerticalLayout navLayout = new VerticalLayout();
		navLayout.setStyleName("LibraryProgrammingViewNavigation");
		navLayout.setMargin(false);
		navLayout.setSpacing(false);
		navLayout.setHeight(100,  Unit.PERCENTAGE);
        navTree = new LibraryProgrammingNavigationTree(this);
        navLayout.addComponent(navTree);
        setFirstComponent(navLayout);
        
        // Put the tabsheet into the right side
        viewTabs = new LibraryProgrammingTabSheet(this);
        viewTabs.setSizeFull();
        //viewTabs.setStyleName("open-only-closable"); // Chameleon theme option to make tab close buttons disappear except when active or hovered
        viewTabs.setStyleName(Reindeer.TABSHEET_MINIMAL);
        viewTabs.addStyleName(Reindeer.TABSHEET_HOVER_CLOSABLE);
        viewTabs.addStyleName(Reindeer.TABSHEET_SELECTED_CLOSABLE);
        setSecondComponent(viewTabs);

        _logger.debug("buildLayout() completed");
	}
	
    public void setDefaultViewName( String viewName ) {
    	defaultViewName = viewName;
    }
    
    LibraryProgrammingNavigationTree.NavigationItem getNavigationItem(String viewName, String viewParam)
    {
    	// First, let's see if we can fine a param-specific view defined...
    	LibraryProgrammingNavigationTree.NavigationItem navItem = navTree.getNavigationItem(viewName + "/" + viewParam); 
    	if ( navItem != null ) {
    		return navItem;
    	}
    	return navTree.getNavigationItem(viewName);
    }
    
    public void showView(final String viewName, final String viewParam) {
    	_logger.debug("showView() - viewName: " + viewName + "; viewParam: " + viewParam);
		EsfVaadinUI.getInstance().ensureLoggedIn();

    	
    	final LibraryProgrammingNavigationTree.NavigationItem navItem = getNavigationItem(viewName,viewParam);
    	if ( navItem != null ) {
	    	// If it's cached and we have it in a tab, show that tab and be done with it.
	    	if ( navItem.doCache() ) {
	    		EsfView toView = viewClassToInstanceCache.get(navItem.getViewClass());
	    		if ( toView != null ) {
	    			Tab viewTab = viewTabs.getTab(toView);
	    			if ( viewTab != null ) {
	    				viewTabs.setSelectedTab(toView);
	    				return;
	    			}
	    		}
	    	}

			final EsfView toView = getOrCreateView(navItem);
			EsfView currentView = (EsfView)viewTabs.getSelectedTab();
			// If we don't have any tabs, create one.
			if ( currentView == null ) {
				viewTabs.addTab(toView);
			} else if ( toView == currentView ) {
				return;
			}
			toView.activateView(EsfView.OpenMode.NORMAL, libraryId);
			setCurrentTabView(toView,navItem);
    	} else {
    		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		User user = vaadinUi.getUser();
    		if ( user != null ) {
        		user.logSecurity("Unexpected library programming view requested: " + viewName +  " (if a valid view name, user does not have permission)");
    		}
    		vaadinUi.showStatus(vaadinUi.getMsg("caption.showStatus.error"), vaadinUi.getMsg("LibraryProgrammingView.showView.unknownView",viewName));    		
    	}
    }
    
    protected void setCurrentTabView(EsfView toView, LibraryProgrammingNavigationTree.NavigationItem navItem) {
		EsfView currentView = (EsfView)viewTabs.getSelectedTab();
		if ( currentView != toView ) {
			viewTabs.replaceComponent(currentView, toView);
		}
		Tab currentTab = viewTabs.getTab(toView);
		currentTab.setCaption(navItem.getTabTitle());
		ThemeResource icon = navItem.getIcon();
		currentTab.setIcon( icon ); // even if null, want to clear any that may be there 
		viewTabs.setTabState();
    }
    
    public void showViewInNewTab(LibraryProgrammingNavigationTree.NavigationItem navItem) {
    	_logger.debug("showViewInNewTab() - viewName: " + navItem.getViewName());
    	
    	// If it's cached and we have it in a tab, show that tab and be done with it.
    	if ( navItem.doCache() ) {
    		EsfView toView = viewClassToInstanceCache.get(navItem.getViewClass());
    		if ( toView != null ) {
    			Tab viewTab = viewTabs.getTab(toView);
    			if ( viewTab != null ) {
    				viewTabs.setSelectedTab(toView);
    				return;
    			}
    		}
    	}
    	
        EsfView toView = getOrCreateView(navItem);
        toView.activateView(EsfView.OpenMode.WINDOW, libraryId);

        Tab currentTab = viewTabs.addTab(toView);
        viewTabs.setSelectedTab(toView);
		currentTab.setCaption(navItem.getTabTitle());
		ThemeResource icon = navItem.getIcon();
		currentTab.setIcon( icon ); // even if null, want to clear any that may be there 
		viewTabs.setTabState();
    }
    
    public void showViewInWindow(LibraryProgrammingNavigationTree.NavigationItem navItem) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	_logger.debug("showViewInWindow() - viewName: " + navItem.getViewName());
    	
		ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(navItem.getWindowTitle());
        w.setWidth(80, Unit.PERCENTAGE);
        w.setHeight(80, Unit.PERCENTAGE);
        EsfView toView = createView(navItem);
        toView.activateView(EsfView.OpenMode.WINDOW, libraryId);
        w.setView(toView); // so we can detect if our form has changes when closing
        w.getLayout().addComponent(toView);
        vaadinUi.addWindowToUI(w);
    }
    
	public EsfView getOrCreateView(LibraryProgrammingNavigationTree.NavigationItem navItem) {
		EsfView toView = null;
		
		if ( navItem.doCache() ) {
			toView = viewClassToInstanceCache.get(navItem.getViewClass());
			if ( toView != null ) {
				return toView;
			}
		}
		
		toView = createView(navItem);
		
		if ( toView != null && navItem.doCache() ) {
			viewClassToInstanceCache.put(navItem.getViewClass(), toView);
		}

		return toView;
	}

	public EsfView createView(LibraryProgrammingNavigationTree.NavigationItem navItem) {
		EsfView toView = null;
		
		try {
			toView = navItem.getViewClass().newInstance();
			//toView.setIcon(navItem.getIcon()); // 12/11/2012 removed on switch to Reindeer theme where icon appeared in tab and in top of view
			toView.initView();
			navTree.setNavigationItem(toView,navItem);
		} catch( InstantiationException e ) {
			_logger.error("createView() - could not instantiate instance of class: " + navItem.getViewClass().getName());
		} catch( IllegalAccessException e ) {
			_logger.error("createView() - illegal access: could not instantiate instance of class: " + navItem.getViewClass().getName());
		}
		
		return toView;
	}

	@Override
    public boolean isDirty() {
    	EsfView currentView = (EsfView)viewTabs.getSelectedTab();
    	return currentView != null && currentView.isDirty();
    }
    
    public String checkDirty() {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	EsfView currentView = (EsfView)viewTabs.getSelectedTab();
    	return currentView == null ? vaadinUi.getMsg("ConfirmDiscardChangesDialog.defaultMessage") : currentView.checkDirty();
    }
    
	@Override
	public void activateView(OpenMode mode, String params) {
		libraryId = params;
    	showView(defaultViewName,libraryId);
	}

	@Override
	public void initView() {
    	buildLayout();
	}
}
