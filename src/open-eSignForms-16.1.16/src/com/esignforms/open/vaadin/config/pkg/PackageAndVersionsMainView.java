// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.PackageVersionInfo;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

/**
 * PackageAndVersionsMainView is a splitpanel that contains the package view in the left and the package version view on the right.
 * 
 * Each of those views is itself a splitpanel, containing a table/list on the top to select items, with a form on the bottom to view/edit.
 * 
 * When a new package is selected in the left view, we propagate that to the right view so it can sync the versions with the selected package.
 * 
 * @author Yozons
 *
 */
public class PackageAndVersionsMainView extends HorizontalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = 1537357339774560810L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackageAndVersionsMainView.class);

    final PackageAndVersionsMainView thisView;
	
    VerticalSplitPanel packageSplitPanel;
    PackageBeanContainer packageContainer;
    PackageList packageList;
    PackageForm packageForm;
	
    VerticalSplitPanel packageVerSplitPanel;
    PackageVersionBeanContainer packageVerContainer;
	PackageVersionList packageVerList;
	PackageVersionForm packageVerForm;

	public PackageAndVersionsMainView() {
		super(); 
		thisView = this;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			packageContainer = new PackageBeanContainer();
			packageVerContainer = new PackageVersionBeanContainer(this); // Load contents when a pkg is selected from our packageContainer
			
			packageList = new PackageList(this,packageContainer);
		    packageForm = new PackageForm(this,packageContainer);

			packageSplitPanel = new VerticalSplitPanel();
			packageSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			packageSplitPanel.setSplitPosition(30);
			packageSplitPanel.setFirstComponent(packageList);
			packageSplitPanel.setSecondComponent(packageForm);
		    
			packageVerList = new PackageVersionList(this,packageVerContainer);
			packageVerForm = new PackageVersionForm(this,packageVerContainer);
			
			packageVerSplitPanel = new VerticalSplitPanel();
			packageVerSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			packageVerSplitPanel.setSplitPosition(30);
			packageVerSplitPanel.setFirstComponent(packageVerList);
			packageVerSplitPanel.setSecondComponent(packageVerForm);

			setFirstComponent(packageSplitPanel);
			setSecondComponent(packageVerSplitPanel);
			setSplitPosition(50);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Package and versions main view exception", e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	private PackageBean getPackageBean(Item packageListItem) {
    	if ( packageListItem == null )
    		return null;
		BeanItem<PackageBean> bi = (BeanItem<PackageBean>)packageListItem;
		return bi.getBean();
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our pkg list or not
        if (property == packageList.getTable()) {
        	final Item item = packageList.getTable().getItem(packageList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( packageForm.getCurrentBean() == packageForm.getBean(item) ) {
    				// We've selected the same bean so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("PackageAndVersionsMainView.Package.ConfirmDiscardChangesDialog.message", packageForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						thisView.discardForms();
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						packageList.getTable().select(packageForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("PackageAndVersionsMainView.Package.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	PackageBean bean = getPackageBean(item);
                packageForm.setItemDataSource(item);
            	if ( bean != null ) {
                	packageVerContainer.reload(bean.pkg());
                	selectFirstPackageVersion();
            	} else {
            		packageVerForm.setItemDataSource(null);
            		packageVerContainer.removeAllItems();
            	}
    		}
        } else if (property == packageVerList.getTable()) {
        	final Item item = packageVerList.getTable().getItem(packageVerList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( packageVerForm.getCurrentBean() == packageVerForm.getBean(item) ) {
    				// We've selected the same bean (version) so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("PackageAndVersionsMainView.Package.ConfirmDiscardChangesDialog.message", packageForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						packageVerForm.discard();
						packageVerForm.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						packageVerList.getTable().select(packageVerForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("PackageAndVersionsMainView.Package.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	packageVerForm.setItemDataSource(item);
    		}
        }
        else {
        	_logger.debug("valueChange() - no list event: " + event.toString());
        }
	}
	

	@Override
	public void detach() {
		/*
		for( Object o : list.getTable().getVisibleColumns() ) {
			_logger.debug("detach() - list visible columns[]: " + o); // These are string property ids of the column names like email, status, lastLoginTimestamp
		}
		*/
		super.detach();
	}
	
	public void selectPackage(PackageBean bean) {
		unselectAllPackages();
		packageList.getTable().select(bean);
		packageList.getTable().setCurrentPageFirstItemId(bean);
		if ( bean == null ) {
			packageVerForm.setItemDataSource(null);
		}
	}
	
	public void unselectAllPackages() {
		packageList.getTable().setValue(null);
		packageForm.setItemDataSource(null);
		packageVerForm.setItemDataSource(null);
	}
	
	public void selectPackageVersion(PackageVersionBean bean) {
		unselectAllPackageVersions();
		packageVerList.getTable().select(bean);
		packageVerList.getTable().setCurrentPageFirstItemId(bean);
	}

	public void discardForms() {
		packageForm.discard();
		packageVerForm.discard();
		packageForm.setItemDataSource(null);
		packageVerForm.setItemDataSource(null);
	}

	
	public void selectFirstPackageVersion() {
		if ( packageVerContainer.size() > 0 ) {
			selectPackageVersion(packageVerContainer.getIdByIndex(0));
		} else {
			unselectAllPackageVersions(); 
		}
	}
	
	public void unselectAllPackageVersions() {
		packageVerList.getTable().setValue(null);
	}

	public PackageBean getPackageBean() {
		return packageForm == null ? null : packageForm.getCurrentBean();
	}
	
	public PackageVersionBean getPackageVersionBean() {
		return packageVerForm == null ? null : packageVerForm.getCurrentBean();
	}
	
	public String getPathNameWithVersion() {
		PackageBean packageBean = getPackageBean();
		PackageVersionBean packageVerBean = getPackageVersionBean();
		return packageVerBean == null ? "?? [?]" : packageBean.getPathName() + " [" + packageVerBean.getVersion() + "]";
	}
	
	public Integer getProductionVersion() {
		PackageBean packageBean = getPackageBean();
		return packageBean == null ? 0 : packageBean.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		PackageBean packageBean = getPackageBean();
		return packageBean == null ? 0 : packageBean.getTestVersion();
	}

	public boolean hasTestVersion() {
		PackageBean packageBean = getPackageBean();
		return packageBean == null ? false : packageBean.pkg().hasTestVersion();
	}
	
    public boolean isProductionVersion(PackageVersionInfo packageVer)
    {
    	PackageBean packageBean = getPackageBean();
    	return packageBean != null && packageVer != null && packageVer.getVersion() == packageBean.getProductionVersion();
    }

    public boolean isTestVersion(PackageVersionInfo packageVer)
    {
    	PackageBean packageBean = getPackageBean();
    	return packageBean != null && packageVer != null && packageVer.getVersion() > packageBean.getProductionVersion();
    }

    // "Current" here means latest production or test version.
    public boolean isCurrentVersion(PackageVersionInfo packageVer)
    {
    	PackageBean packageBean = getPackageBean();
    	return packageBean != null && packageVer != null && packageVer.getVersion() >= packageBean.getProductionVersion();
    }

    public boolean isOldVersion(PackageVersionInfo packageVer)
    {
    	PackageBean packageBean = getPackageBean();
    	return packageBean != null && packageVer != null && packageVer.getVersion() < packageBean.getProductionVersion();
    }
	
	public String getVersionLabel(PackageVersionInfo packageVer) {
		if ( isTestVersion(packageVer) )
			return Literals.VERSION_LABEL_TEST;
		if ( isProductionVersion(packageVer) )
			return Literals.VERSION_LABEL_PRODUCTION;
		return Literals.VERSION_LABEL_NOT_CURRENT;
	}
	
	public boolean hasPermViewDetails() {
		PackageBean packageBean = getPackageBean();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return packageBean != null && packageBean.pkg().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		PackageBean packageBean = getPackageBean();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return packageBean != null && packageBean.pkg().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLikeOrUpdate() {
		PackageBean packageBean = getPackageBean();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return packageBean != null && 
		       (packageBean.pkg().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_CREATELIKE) ||
			   packageBean.pkg().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE)) ;
	}

	public boolean hasPermCreateLike() {
		PackageBean packageBean = getPackageBean();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return packageBean != null && packageBean.pkg().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		PackageBean packageBean = getPackageBean();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return packageBean != null && packageBean.pkg().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_DELETE);
	}
	
	public boolean cancelChanges() {
		packageForm.discard();
		packageVerForm.discard();
		packageForm.setReadOnly(true);
		packageVerForm.setReadOnly(true);
		return true;
	}
	
	public boolean enterEditMode() {
		packageForm.setReadOnly(false);
		packageVerForm.setReadOnly(false);
		return true;
	}
	
	public boolean save() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
		if ( ! packageForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		if ( ! packageVerForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		packageForm.commit();
		packageVerForm.commit();
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			// If we're saving a new test version, we'll want to bump our pkg to show this as the new test version
			PackageBean currPackage = getPackageBean();
			PackageVersionBean currPackageVer = getPackageVersionBean();
			if ( currPackageVer.duplicatedPackageVersion().getVersion() > currPackage.pkg().getTestVersion() )
				currPackage.pkg().bumpTestVersion();
			
			PackageBean savedPackage = packageForm.save(con);
			if ( savedPackage == null )  {
				con.rollback();
				return false;
			}
			
			PackageVersionBean savedPackageVersion = packageVerForm.save(con);
			if ( savedPackageVersion == null )  {
				con.rollback();
				return false;
			}
			
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Saved package " + savedPackage.getPathName() + " version " + savedPackageVersion.getVersion() + ". PackageVersionId: " + savedPackageVersion.getId() + "; status: " + savedPackage.getStatus());
			}

			con.commit();
	    		
			packageContainer.refresh();  // refresh our list after a change
			selectPackage(savedPackage);
			selectPackageVersion(savedPackageVersion);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean createLikePackage() {
		PackageBean likePackageBean = getPackageBean();
		PackageVersionBean likePackageVerBean = getPackageVersionBean();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
    	PackageBean createdPackageBean = likePackageBean.createLike();
		BeanItem<PackageBean> createdPackageItem = new BeanItem<PackageBean>(createdPackageBean);
        packageForm.setItemDataSource(createdPackageItem);

    	PackageVersionBean createdPackageVerBean = likePackageVerBean.createLike(createdPackageBean.pkg());
		BeanItem<PackageVersionBean> createdPackageVerItem = new BeanItem<PackageVersionBean>(createdPackageVerBean);       
        packageVerForm.setItemDataSource(createdPackageVerItem);
        
        setReadOnly(false);
		return true;
	}

	public boolean createNextVersion() {
		PackageBean currPackageBean = getPackageBean();
		PackageVersionBean currPackageVerBean = getPackageVersionBean();
		
        packageForm.setReadOnly(!hasPermUpdate()); // should have permission since doing a create next version...

    	PackageVersionBean nextPackageVerBean = currPackageVerBean.createLike(currPackageBean.pkg());
		BeanItem<PackageVersionBean> nextPackageVerItem = new BeanItem<PackageVersionBean>(nextPackageVerBean);
        packageVerForm.setItemDataSource(nextPackageVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean promoteTestToProductionVersion() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("PackageVersionForm.versionChange.TestToProd.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		PackageBean packageBean = getPackageBean();
		PackageVersionBean packageVerBean = getPackageVersionBean();
		
		// If moving the package to production, all documents and the package document must also have a production version
		Document packageDocument = Document.Manager.getById(packageVerBean.duplicatedPackageVersion().getPackageDocumentId());
		if ( ! packageDocument.hasProductionVersion() ) {
			errors.addError(vaadinUi.getMsg("PackageVersionForm.versionChange.TestToProd.error.noProductionPackageDocument",packageDocument.getEsfName()));
		} else if ( packageDocument.hasTestVersion() ) {
			errors.addWarning(vaadinUi.getMsg("PackageVersionForm.versionChange.TestToProd.warning.hasTestPackageDocument",packageDocument.getEsfName()));
		}
		
		for( EsfUUID docId : packageVerBean.duplicatedPackageVersion().getDocumentIdList() ) {
			Document document = Document.Manager.getById(docId);
			if ( ! document.hasProductionVersion() ) {
				errors.addError(vaadinUi.getMsg("PackageVersionForm.versionChange.TestToProd.error.noProductionDocument",document.getEsfName()));
			} else if ( document.hasTestVersion() ) {
				errors.addWarning(vaadinUi.getMsg("PackageVersionForm.versionChange.TestToProd.warning.hasTestDocument",document.getEsfName()));
			}
		}
		
		if ( errors.hasError() ) {
    		vaadinUi.show(errors);
    		return false;
		} 
			
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			
			packageBean.pkg().promoteTestVersionToProduction();
			
			if ( ! packageBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved package " + packageBean.getPathName() + " version " + packageVerBean.getVersion() + " into Production status. PackageVersionId: " + packageVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved package " + packageBean.getPathName() + " version " + packageVerBean.getVersion() + " into Production status. PackageVersionId: " + packageVerBean.getId());
			}
			
			con.commit();
			packageContainer.refresh();  // refresh our list after a change
			selectPackage(packageBean);
			selectPackageVersion(packageVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("PackageVersionForm.versionChange.TestToProd.success.message",packageBean.getPathName(),packageVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean revertProductionVersionBackToTest() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("PackageVersionForm.versionChange.revertProdToTest.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			PackageBean packageBean = getPackageBean();
			PackageVersionBean packageVerBean = getPackageVersionBean();
			
			packageBean.pkg().revertProductionVersionBackToTest();
			
			if ( ! packageBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved Production package " + packageBean.getPathName() + " version " + packageVerBean.getVersion() + " back into Test status. PackageVersionId: " + packageVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved Production package " + packageBean.getPathName() + " version " + packageVerBean.getVersion() + " back into Test status. PackageVersionId: " + packageVerBean.getId());
			}
			
			con.commit();
			packageContainer.refresh();  // refresh our list after a change
			selectPackage(packageBean);
			selectPackageVersion(packageVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("PackageVersionForm.versionChange.revertProdToTest.success.message",packageBean.getPathName(),packageVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean delete() {
		packageForm.discard();
		packageVerForm.discard();
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			PackageBean currPackage = getPackageBean();
			PackageVersionBean currPackageVer = getPackageVersionBean();
			
			// If there is no Production version, we'll get rid of the package entirely, otherwise just the test version
			Errors errors = new Errors();
			if ( currPackage.getProductionVersion() == 0 )
				currPackage.pkg().delete(con,errors,vaadinUi.getUser());
			else {
				currPackageVer.duplicatedPackageVersion().delete(con,errors,vaadinUi.getUser());
				currPackage.dropTestVersion();
				currPackage.save(con, errors);
			}
			
			if ( errors.hasError() ) {
				vaadinUi.show(errors);
				con.rollback();
				return false;
			}
			
			con.commit();
    		
			vaadinUi.showStatus(vaadinUi.getMsg("PackageVersionForm.button.delete.successMessage",currPackage.getPathName(),currPackageVer.getVersion()));

			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Deleted package " + currPackage.getPathName() + " version " + currPackageVer.getVersion() + ". PackageVersionId: " + currPackageVer.getId() + "; status: " + currPackage.getStatus());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted package " + currPackage.getPathName() + " version " + currPackageVer.getVersion() + ". PackageVersionId: " + currPackageVer.getId() + "; status: " + currPackage.getStatus());
			}

			packageContainer.refresh();  // refresh our list after a change
			selectPackage(null);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		_logger.debug("activateView() - params: " + params);
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return packageForm.isDirty() ? packageForm.checkDirty() : packageVerForm.checkDirty();
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return packageForm.isDirty() || packageVerForm.isDirty();
	}
}