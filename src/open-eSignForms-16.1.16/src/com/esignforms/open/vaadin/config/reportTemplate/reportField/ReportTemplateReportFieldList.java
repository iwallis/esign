// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.reportTemplate.reportField;

import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.ReportTemplateReportField;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.reportFieldTemplate.ReportFieldTemplateView;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardFormChangesWindow;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;
import com.vaadin.ui.themes.Reindeer;

public class ReportTemplateReportFieldList extends Panel {
	private static final long serialVersionUID = -2569480012087967734L;

	final ReportTemplate duplicatedReportTemplate;
	final ReportTemplateReportFieldView view;
	final ReportTemplateReportFieldContainer container;
	ReportTemplateReportFieldTable table;
	
	// For search bar
	Button reportFieldTemplatesButton;
	Button createNewButton;

	public ReportTemplateReportFieldList(final ReportTemplateReportFieldView view, final ReportTemplateReportFieldContainer containerParam, ReportTemplate duplicatedReportTemplateParam) {
		super();
		this.view = view;
		setStyleName("ReportTemplateReportFieldList");
		setSizeFull();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
		
    	this.container = containerParam;
    	this.duplicatedReportTemplate = duplicatedReportTemplateParam;
    	
		HorizontalLayout searchBar = new HorizontalLayout();
		searchBar.setWidth(100, Unit.PERCENTAGE);
    	searchBar.addStyleName("searchBar");
    	searchBar.setSpacing(true);
    	searchBar.setMargin(false);

    	layout.addComponent(searchBar);
    	
    	Label dummyLabel = new Label("");
    	searchBar.addComponent(dummyLabel);
    	searchBar.setExpandRatio(dummyLabel, 1.0f); // forces following buttons to right
    	
    	reportFieldTemplatesButton = new Button(vaadinUi.getMsg("ReportTemplateReportFieldView.searchBar.reportFieldTemplatesButton.label"));
    	reportFieldTemplatesButton.setStyleName(Reindeer.BUTTON_SMALL);
    	reportFieldTemplatesButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportTemplateReportFieldView.searchBar.reportFieldTemplatesButton.icon")));
    	reportFieldTemplatesButton.setDescription(vaadinUi.getMsg("ReportTemplateReportFieldView.searchBar.reportFieldTemplatesButton.tooltip"));
    	reportFieldTemplatesButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = 1356429175746277266L;

			@Override
			public void buttonClick(ClickEvent event) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	            final ReportFieldTemplateView rftView = new ReportFieldTemplateView();
	            rftView.initView();
	    		
	            ConfirmDiscardFormChangesWindow w = new ConfirmDiscardFormChangesWindow(vaadinUi.getMsg("ReportFieldTemplateView.window.caption"), rftView);
	            w.center();
	            w.setModal(true); // don't let them do anything else while on they work on report field templates
	            w.setWidth(90, Unit.PERCENTAGE);
	            w.setHeight(90, Unit.PERCENTAGE);
				w.addCloseListener( new CloseListener() {
					private static final long serialVersionUID = 1192836914575731458L;

					@Override
					public void windowClose(CloseEvent e) {
						if ( ! view.isReadOnly() )
							view.refreshReportTemplates();
					}
				});

				rftView.activateView(EsfView.OpenMode.WINDOW, "");
				rftView.setParentWindow(w);
				rftView.setReadOnly(view.isReadOnly());
	        	vaadinUi.addWindowToUI(w);
			}
		});
    	searchBar.addComponent(reportFieldTemplatesButton);
    	searchBar.setComponentAlignment(reportFieldTemplatesButton, Alignment.MIDDLE_RIGHT);

    	createNewButton = new Button(vaadinUi.getMsg("ReportTemplateReportFieldView.searchBar.createNewButton.label"));
    	createNewButton.setStyleName(Reindeer.BUTTON_SMALL);
    	createNewButton.setIcon(new ThemeResource(vaadinUi.getMsg("ReportTemplateReportFieldView.searchBar.createNewButton.icon")));
    	createNewButton.setDescription(vaadinUi.getMsg("ReportTemplateReportFieldView.searchBar.createNewButton.tooltip"));
    	createNewButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = -7347643996424139504L;

			@Override
			public void buttonClick(ClickEvent event) {
				view.createNew();
			}
		});
    	searchBar.addComponent(createNewButton);
    	searchBar.setComponentAlignment(createNewButton, Alignment.MIDDLE_RIGHT);

    	table = new ReportTemplateReportFieldTable(view, container);
    	
    	layout.addComponent(searchBar);
    	layout.addComponent(table);
    	layout.setExpandRatio(table, 1);
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		createNewButton.setVisible(!v);
		table.setDragMode( v ? TableDragMode.NONE : TableDragMode.ROW);
	}
	
	public ReportTemplateReportFieldTable getTable() {
		return table;
	}
	
	class ReportTemplateReportFieldTable extends Table {
		private static final long serialVersionUID = -7289064258802962958L;

		public ReportTemplateReportFieldTable(ReportTemplateReportFieldView view, ReportTemplateReportFieldContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("ReportTemplateReportFieldList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("ReportTemplateReportFieldList.showColumnHeaders"));
			setSortEnabled(false);
			setColumnAlignment("fieldOrder", Align.CENTER);
			setColumnAlignment("allowSearch", Align.CENTER);
	        setNullSelectionAllowed(true);
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);

	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener((Property.ValueChangeListener)view);
			setSizeFull();
	    	initializeDND(); // allow reorder via DND
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			if ( "reportFieldTemplateId".equals(colId) ) {
				EsfUUID reportFieldTemplateId = (EsfUUID)property.getValue();
				ReportFieldTemplate reportFieldTemplate = ReportFieldTemplate.Manager.getById(reportFieldTemplateId);
				return reportFieldTemplate == null ? "???" : reportFieldTemplate.getFieldName().toString();
			}
			if ( "allowSearch".equals(colId) ) {
				String allowSearch = property.getValue().toString();
				return EsfVaadinUI.getInstance().getMsg("ReportTemplateReportFieldList.allowSearch."+allowSearch);
			}
			return super.formatPropertyValue(rowId,colId,property);
		}

		public void reorder() {
            // Fix up the order now
            short order = 1;
            for( ReportTemplateReportField rtrf : container.getItemIds() ) {
            	BeanItem<ReportTemplateReportField> item = container.getItem(rtrf);
            	item.getItemProperty("fieldOrder").setValue(order);
            	rtrf.setFieldOrder(order++);
            }
            view.form.setContainerOrderHasChanged();
            view.form.setReadOnly(isReadOnly());
		}
		
		public void initializeDND() {
			setDropHandler(new DropHandler() {
				private static final long serialVersionUID = 113785907594507229L;

				public void drop(DragAndDropEvent dropEvent) {
	                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
	                
	                Container sourceContainer = t.getSourceContainer();
	                if ( sourceContainer == container ) { 
	                	// reordering within the table rows
	                	ReportTemplateReportField sourceItemId = (ReportTemplateReportField)t.getItemId();

		                AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails)dropEvent.getTargetDetails());
		                ReportTemplateReportField targetItemId = (ReportTemplateReportField)dropData.getItemIdOver();
		                
		                // No move if source and target are the same
		                if ( sourceItemId == targetItemId )
		                	return;
		                
		                // Let's remove the source of the drag so we can add it back where requested...
		                container.removeItem(sourceItemId);
		                if ( targetItemId == null ) {
		                	container.addItem(sourceItemId);
		                } else if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
		                	container.addItemAfter(targetItemId,sourceItemId);
		            	} else {
		                    Object prevItemId = container.prevItemId(targetItemId);
		                    container.addItemAfter(prevItemId, sourceItemId);
		            	}
		                
		                reorder();
	                }
	            }

	            public AcceptCriterion getAcceptCriterion() {
	                return new And(SourceIsTarget.get(),new Not(AbstractSelect.VerticalLocationIs.MIDDLE)); // dragging within the table
	            }
	        });
			
		}
	
	} // ReportTemplateReportFieldTable

} // ReportTemplateReportFieldList
