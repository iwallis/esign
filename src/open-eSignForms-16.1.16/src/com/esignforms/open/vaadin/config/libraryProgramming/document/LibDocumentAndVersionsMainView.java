// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.document;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentInfo;
import com.esignforms.open.prog.DocumentVersionInfo;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

/**
 * LibDocumentAndVersionsMainView is a splitpanel that contains the document view in the left and the document version view on the right.
 * 
 * Each of those views is itself a splitpanel, containing a table/list on the top to select items, with a form on the bottom to view/edit.
 * 
 * When a new document is selected in the left view, we propagate that to the right view so it can sync the versions with the selected document.
 * 
 * @author Yozons
 *
 */
public class LibDocumentAndVersionsMainView extends HorizontalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = -1860117736053008933L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibDocumentAndVersionsMainView.class);

    EsfUUID libraryId;
    Library library;
    final LibDocumentAndVersionsMainView thisView;
	
    VerticalSplitPanel docSplitPanel;
    LibDocumentBeanContainer docContainer;
    LibDocumentList docList;
    LibDocumentForm docForm;
	
    VerticalSplitPanel docVerSplitPanel;
    LibDocumentVersionBeanContainer docVerContainer;
	LibDocumentVersionList docVerList;
	LibDocumentVersionForm docVerForm;

	public LibDocumentAndVersionsMainView() {
		super(); 
		thisView = this;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			library = Library.Manager.getById(libraryId, vaadinUi.getUser());
			
			docContainer = new LibDocumentBeanContainer(library);
			docVerContainer = new LibDocumentVersionBeanContainer(this); // Load contents when a document is selected from our docContainer
			
			docList = new LibDocumentList(this,docContainer);
		    docForm = new LibDocumentForm(this,docContainer);

			docSplitPanel = new VerticalSplitPanel();
			docSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			docSplitPanel.setSplitPosition(40);
			docSplitPanel.setFirstComponent(docList);
			docSplitPanel.setSecondComponent(docForm);
		    
			docVerList = new LibDocumentVersionList(this,docVerContainer);
			docVerForm = new LibDocumentVersionForm(this,docVerContainer);
			
			docVerSplitPanel = new VerticalSplitPanel();
			docVerSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			docVerSplitPanel.setSplitPosition(40);
			docVerSplitPanel.setFirstComponent(docVerList);
			docVerSplitPanel.setSecondComponent(docVerForm);

			setFirstComponent(docSplitPanel);
			setSecondComponent(docVerSplitPanel);
			setSplitPosition(50);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("Document and versions main view exception", e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	private LibDocumentBean getLibDocumentBean(Item docListItem) {
    	if ( docListItem == null )
    		return null;
		BeanItem<LibDocumentBean> bi = (BeanItem<LibDocumentBean>)docListItem;
		return bi.getBean();
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
        
        // Determine if the click is on our document list or not
        if (property == docList.getTable()) {
        	final Item item = docList.getTable().getItem(docList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( docForm.getCurrentBean() == docForm.getBean(item) ) {
    				// We've selected the same bean so don't need to warn (or reset the data source)
    				return;
    			}
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibDocumentAndVersionsMainView.Document.ConfirmDiscardChangesDialog.message", docForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						thisView.discardForms();
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						docList.getTable().select(docForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentAndVersionsMainView.Document.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	LibDocumentBean bean = getLibDocumentBean(item);
                docForm.setItemDataSource(item);
            	if ( bean != null ) {
                	docVerContainer.reload(bean.document());
                	selectFirstDocumentVersion();
            	} else {
            		docVerForm.setItemDataSource(null);
            		docVerContainer.removeAllItems();
            	}
    		}
        } else if (property == docVerList.getTable()) {
        	final Item item = docVerList.getTable().getItem(docVerList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( docVerForm.getCurrentBean() == docVerForm.getBean(item) ) {
    				// We've selected the same bean (version) so don't need to warn (or reset the data source)
    				return;
    			}
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibDocumentAndVersionsMainView.Document.ConfirmDiscardChangesDialog.message", docForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						docVerForm.discard();
						docVerForm.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						docVerList.getTable().select(docVerForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentAndVersionsMainView.Document.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	docVerForm.setItemDataSource(item);
    		}
        }
        else {
        	_logger.debug("valueChange() - no list event: " + event.toString());
        }
	}
	

	@Override
	public void detach() {
		/*
		for( Object o : list.getTable().getVisibleColumns() ) {
			_logger.debug("detach() - list visible columns[]: " + o); // These are string property ids of the column names like email, status, lastLoginTimestamp
		}
		*/
		super.detach();
	}
	
	public void selectDocument(LibDocumentBean bean) {
		unselectAllDocuments();
		docList.getTable().select(bean);
		docList.getTable().setCurrentPageFirstItemId(bean);
		if ( bean == null ) {
			docVerForm.setItemDataSource(null);
		}
	}
	
	public void unselectAllDocuments() {
		docList.getTable().setValue(null);
		docForm.setItemDataSource(null);
	}
	
	public void selectDocumentVersion(LibDocumentVersionBean bean) {
		unselectAllDocumentVersions();
		docVerList.getTable().select(bean);
		docVerList.getTable().setCurrentPageFirstItemId(bean);
	}

	public void discardForms() {
		docForm.discard();
		docVerForm.discard();
		docForm.setItemDataSource(null);
		docVerForm.setItemDataSource(null);
	}

	
	public void selectFirstDocumentVersion() {
		if ( docVerContainer.size() > 0 ) {
			selectDocumentVersion(docVerContainer.getIdByIndex(0));
		} else {
			unselectAllDocumentVersions(); 
		}
	}
	
	public void unselectAllDocumentVersions() {
		docVerList.getTable().setValue(null);
	}

	public Library getLibrary() {
		return library;
	}
	
	public LibDocumentBean getLibDocumentBean() {
		return docForm.getCurrentBean();
	}
	
	public LibDocumentVersionBean getLibDocumentVersionBean() {
		return docVerForm.getCurrentBean();
	}
	
	public String getEsfNameWithVersion() {
		LibDocumentBean docBean = getLibDocumentBean();
		LibDocumentVersionBean docVerBean = getLibDocumentVersionBean();
		return docVerBean == null ? "?? [?]" : docBean.getEsfName() + " [" + docVerBean.getVersion() + "]";
	}
	
	public Integer getProductionVersion() {
		LibDocumentBean docBean = getLibDocumentBean();
		return docBean == null ? 0 : docBean.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		LibDocumentBean docBean = getLibDocumentBean();
		return docBean == null ? 0 : docBean.getTestVersion();
	}

	public boolean hasTestVersion() {
		LibDocumentBean docBean = getLibDocumentBean();
		return docBean == null ? false : docBean.document().hasTestVersion();
	}
	
    public boolean isProductionVersion(DocumentVersionInfo docVer)
    {
    	LibDocumentBean docBean = getLibDocumentBean();
    	return docBean != null && docVer != null && docVer.getVersion() == docBean.getProductionVersion();
    }

    public boolean isTestVersion(DocumentVersionInfo docVer)
    {
    	LibDocumentBean docBean = getLibDocumentBean();
    	return docBean != null && docVer != null && docVer.getVersion() > docBean.getProductionVersion();
    }

    // "Current" here means latest production or test version.
    public boolean isCurrentVersion(DocumentVersionInfo docVer)
    {
    	LibDocumentBean docBean = getLibDocumentBean();
    	return docBean != null && docVer != null && docVer.getVersion() >= docBean.getProductionVersion();
    }

    public boolean isOldVersion(DocumentVersionInfo docVer)
    {
    	LibDocumentBean docBean = getLibDocumentBean();
    	return docBean != null && docVer != null && docVer.getVersion() < docBean.getProductionVersion();
    }
	
	public String getVersionLabel(DocumentVersionInfo docVer) {
		if ( isTestVersion(docVer) )
			return Literals.VERSION_LABEL_TEST;
		if ( isProductionVersion(docVer) )
			return Literals.VERSION_LABEL_PRODUCTION;
		return Literals.VERSION_LABEL_NOT_CURRENT;
	}
	
	public boolean hasPermViewDetails() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_DELETE);
	}
	
	public boolean cancelChanges() {
		docForm.discard();
		docVerForm.discard();
		docVerForm.closeAllPageForms();
		docForm.setReadOnly(true);
		docVerForm.setReadOnly(true);
		return true;
	}
	
	public boolean enterEditMode() {
		docForm.setReadOnly(false);
		docVerForm.setReadOnly(false);
		return true;
	}
	
	public boolean save() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
		if ( ! docForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		if ( ! docVerForm.isValid() ) {
			_logger.debug("DEBUG: docVerForm.isValid() failed");
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		
		LibDocumentVersionBean currDocVer = getLibDocumentVersionBean();
		if ( currDocVer.duplicateDocumentVersion().getNumEditPages() < 1 )
		{
			vaadinUi.showWarning(null, vaadinUi.getMsg("LibDocumentVersionForm.pageInfo.error.noEditModePages"));
			return false;
		}
		if ( currDocVer.duplicateDocumentVersion().getNumReviewPages() < 1 )
		{
			vaadinUi.showWarning(null, vaadinUi.getMsg("LibDocumentVersionForm.pageInfo.error.noReviewModePages"));
			return false;
		}
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			docForm.commit();
			docVerForm.commit();
			
			// If we're saving a new test version, we'll want to bump our document to show this as the new test version
			LibDocumentBean currDoc = getLibDocumentBean();
			currDocVer = getLibDocumentVersionBean();
			if ( currDocVer.duplicateDocumentVersion().getVersion() > currDoc.document().getTestVersion() )
				currDoc.document().bumpTestVersion();
			
			LibDocumentBean savedDoc = docForm.save(con);
			if ( savedDoc == null )  {
				con.rollback();
				return false;
			}
			
			LibDocumentVersionBean savedDocVer = docVerForm.save(con);
			if ( savedDocVer == null )  {
				con.rollback();
				return false;
			}
			
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Saved document " + savedDoc.getEsfName() + " version " + savedDocVer.getVersion() + ". DocumentVersionId: " + savedDocVer.getId() + "; status: " + savedDoc.getStatus());
			}

			con.commit();
	    		
			docContainer.refresh();  // refresh our list after a change
			selectDocument(savedDoc);
			selectDocumentVersion(savedDocVer);
			return true;
		}
		catch(SQLException e) 
		{
			_logger.error("save()", e);
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		catch(CommitException e) 
		{
			_logger.error("save(commit-failed)", e);
			pool.rollbackIgnoreException(con);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean createNewDocument() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		DocumentInfo docInfo = DocumentInfo.Manager.createNew(getLibrary());
		LibDocumentBean docBean = new LibDocumentBean(docInfo);
		BeanItem<LibDocumentBean> docItem = new BeanItem<LibDocumentBean>(docBean);
        docForm.setItemDataSource(docItem);
		
		DocumentVersionInfo docVerInfo = DocumentVersionInfo.Manager.createNew(Document.Manager.getById(docInfo.getId()),vaadinUi.getUser());
		LibDocumentVersionBean docVerBean = new LibDocumentVersionBean(docVerInfo,getVersionLabel(docVerInfo));
		BeanItem<LibDocumentVersionBean> docVerItem = new BeanItem<LibDocumentVersionBean>(docVerBean);
        docVerForm.setItemDataSource(docVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean createLikeDocument() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibDocumentBean likeDocBean = getLibDocumentBean();
		LibDocumentVersionBean likeDocVerBean = getLibDocumentVersionBean();
		
    	LibDocumentBean createdDocBean = likeDocBean.createLike();
		BeanItem<LibDocumentBean> createdDocItem = new BeanItem<LibDocumentBean>(createdDocBean);
        docForm.setItemDataSource(createdDocItem);

    	DocumentVersionInfo createdDocVerInfo = DocumentVersionInfo.Manager.createLike(Document.Manager.getById(createdDocBean.docInfo().getId()), likeDocVerBean.duplicateDocumentVersion(), vaadinUi.getUser());
    	LibDocumentVersionBean createdDocVerBean = new LibDocumentVersionBean(createdDocVerInfo,Literals.VERSION_LABEL_TEST); // The new document and version will always be Test.
		BeanItem<LibDocumentVersionBean> createdDocVerItem = new BeanItem<LibDocumentVersionBean>(createdDocVerBean);       
        docVerForm.setItemDataSource(createdDocVerItem);
        
        setReadOnly(false);
		return true;
	}

	public boolean createNextVersion() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibDocumentBean currDocBean = getLibDocumentBean();
		LibDocumentVersionBean currDocVerBean = getLibDocumentVersionBean();
		
        docForm.setReadOnly(!hasPermUpdate()); // should have permission since doing a create next version...

    	DocumentVersionInfo nextDocVerInfo = DocumentVersionInfo.Manager.createLike(currDocBean.document(), currDocVerBean.duplicateDocumentVersion(), vaadinUi.getUser());
    	LibDocumentVersionBean nextDocVerBean = new LibDocumentVersionBean(nextDocVerInfo,Literals.VERSION_LABEL_TEST); // The new version will always be Test.
		BeanItem<LibDocumentVersionBean> nextDocVerItem = new BeanItem<LibDocumentVersionBean>(nextDocVerBean);
        docVerForm.setItemDataSource(nextDocVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean promoteTestToProductionVersion() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibDocumentVersionForm.versionChange.TestToProd.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibDocumentBean docBean = getLibDocumentBean();
			LibDocumentVersionBean docVerBean = getLibDocumentVersionBean();
			
			docBean.document().promoteTestVersionToProduction();
			
			if ( ! docBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved document " + docBean.getEsfName() + " version " + docVerBean.getVersion() + " into Production status. DocumentVersionId: " + docVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved document " + docBean.getEsfName() + " version " + docVerBean.getVersion() + " into Production status. DocumentVersionId: " + docVerBean.getId());
			}
			
			con.commit();
			docContainer.refresh();  // refresh our list after a change
			selectDocument(docBean);
			selectDocumentVersion(docVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentVersionForm.versionChange.TestToProd.success.message",docBean.getEsfName(),docVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean revertProductionVersionBackToTest() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibDocumentVersionForm.versionChange.revertProdToTest.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibDocumentBean docBean = getLibDocumentBean();
			LibDocumentVersionBean docVerBean = getLibDocumentVersionBean();
			
			docBean.document().revertProductionVersionBackToTest();
			
			if ( ! docBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved Production document " + docBean.getEsfName() + " version " + docVerBean.getVersion() + " back into Test status. DocumentVersionId: " + docVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved Production document " + docBean.getEsfName() + " version " + docVerBean.getVersion() + " back into Test status. DocumentVersionId: " + docVerBean.getId());
			}
			
			con.commit();
			docContainer.refresh();  // refresh our list after a change
			selectDocument(docBean);
			selectDocumentVersion(docVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentVersionForm.versionChange.revertProdToTest.success.message",docBean.getEsfName(),docVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean delete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibDocumentBean currDocument = getLibDocumentBean();
			LibDocumentVersionBean currDocumentVer = getLibDocumentVersionBean();
			
			// If there is no Production version, we'll get rid of the document entirely, otherwise just the test version
			Errors errors = new Errors();
			if ( currDocument.getProductionVersion() == 0 )
				currDocument.document().delete(con,errors,vaadinUi.getUser());
			else {
				currDocumentVer.duplicateDocumentVersion().delete(con,errors,vaadinUi.getUser());
				currDocument.dropTestVersion();
				currDocument.save(con, errors);
			}
			
			if ( errors.hasError() ) {
				vaadinUi.show(errors);
				con.rollback();
				return false;
			}
			
			con.commit();
    		
			vaadinUi.showStatus(vaadinUi.getMsg("LibDocumentVersionForm.button.delete.successMessage",currDocument.getEsfName(),currDocumentVer.getVersion()));
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Deleted document " + currDocument.getEsfName() + " version " + currDocumentVer.getVersion() + ". DropDownVersionId: " + currDocumentVer.getId() + "; status: " + currDocument.getStatus());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted document " + currDocument.getEsfName() + " version " + currDocumentVer.getVersion() + ". DropDownVersionId: " + currDocumentVer.getId() + "; status: " + currDocument.getStatus());
			}

			docContainer.refresh();  // refresh our list after a change
			selectDocument(null);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		_logger.debug("activateView() - params: " + params);
		libraryId = new EsfUUID(params);
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return (docForm != null && docForm.isDirty()) ? docForm.checkDirty() : docVerForm.checkDirty();
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return (docForm != null && docForm.isDirty()) || (docVerForm != null && docVerForm.isDirty());
	}
}