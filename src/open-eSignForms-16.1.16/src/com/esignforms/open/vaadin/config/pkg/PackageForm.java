// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import org.vaadin.jouni.animator.Disclosure;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.UI;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.PackageEsfPathNameValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.FieldSpecTextField;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.event.FieldEvents;
import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.Reindeer;

/**
 * The PackageForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class PackageForm extends Form implements EsfView, ClickListener {

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PackageForm.class);
	
	static List<String> _orderedProperties;
    
	Button saveButton;
	Button createLikeButton;
	Button cancelButton;
	Button editButton;

	PackageAndVersionsMainView view;
	PackageBeanContainer container;
    GridLayout layout;
	
	Label id;
	
	// We store the prevBean in case we abandon working on the newBean when we 'create like'
	PackageBean prevBean;
    PackageBean newBean;
    
    // We use this for all our select lists, so we only reload once per Bean being set as our datasource
	EsfUUID[] allGroupListIds;
	EsfPathName[] allGroupListNames;
	
	Disclosure permDisclosure;
	
	public PackageForm(PackageAndVersionsMainView view, PackageBeanContainer container) {
    	setStyleName("PackageForm");
       	setWidth(100, Unit.PERCENTAGE);
    	this.view = view;
    	this.container = container;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
    	layout = new GridLayout(2,6);
        layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.5f);
    	layout.setColumnExpandRatio(1, 0.5f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);
		
		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id,0,4,1,4);

		permDisclosure = new Disclosure(vaadinUi.getMsg("PackageForm.permDisclosure.label"));
		permDisclosure.setWidth(100, Unit.PERCENTAGE);
		permDisclosure.setVisible(false);
		layout.addComponent(permDisclosure,0,5,1,5);

		HorizontalLayout footer = new HorizontalLayout();
		footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);

		User loggedInUser = vaadinUi.getUser();
		UI ui = new UI();
		if ( ui.showSaveButton(loggedInUser, UI.UI_PACKAGE_VIEW) ) {
	    	saveButton = new Button(vaadinUi.getMsg("button.save.label"), (ClickListener)this);
	    	//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
	    	//saveButton.setClickShortcut(KeyCode.ENTER);
	    	saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
	    	saveButton.setDescription(vaadinUi.getMsg("button.save.tooltip"));
	    	footer.addComponent(saveButton);
		}

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

		if ( ui.canUpdate(loggedInUser, UI.UI_PACKAGE_VIEW) ) {
	    	editButton = new Button(vaadinUi.getMsg("button.edit.label"), (ClickListener)this);
	    	editButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageForm.button.edit.icon")));
	    	editButton.setDescription(vaadinUi.getMsg("button.edit.tooltip"));
			footer.addComponent(editButton);
		}

    	if ( ui.canCreateLike(loggedInUser, UI.UI_PACKAGE_VIEW) ) {
	    	createLikeButton = new Button(vaadinUi.getMsg("PackageForm.button.createLike.label"), (ClickListener)this);
	    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("PackageForm.button.createLike.icon")));
	    	createLikeButton.setDescription(vaadinUi.getMsg("PackageForm.button.createLike.tooltip"));
			footer.addComponent(createLikeButton);
    	}

    	setFooter(footer);
    	setReadOnly(true);

    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 119337154536003138L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				_logger.debug("createField: " + propertyId);
				
				// Load our cache if we don't have it yet since we'll need it for all our permission selection boxes
				if ( allGroupListIds == null ) {
					PackageBean bean = getCurrentBean();
					allGroupListIds = bean.allGroupListIds();
					allGroupListNames = bean.allGroupListNames();
					
					_logger.debug("createField() - created allGroupList.size: " + allGroupListIds.length);
				}

				// Status will be an option group (radio buttons and not a text field)
				if ( propertyId.equals("status") ) {
					// Create the radio buttons to have the two possible values.
					OptionGroup statusOptionGroup = new OptionGroup();
					statusOptionGroup.addItem(Literals.STATUS_ENABLED);
					statusOptionGroup.addItem(Literals.STATUS_DISABLED);
					// Associate different labels/captions to those values
					statusOptionGroup.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					statusOptionGroup.setItemCaption(Literals.STATUS_ENABLED, vaadinUi.getPrettyCode().status(Literals.STATUS_ENABLED));
					statusOptionGroup.setItemCaption(Literals.STATUS_DISABLED, vaadinUi.getPrettyCode().status(Literals.STATUS_DISABLED));
					statusOptionGroup.setStyleName("inline");
					statusOptionGroup.setDescription(vaadinUi.getMsg("PackageForm.status.tooltip")); // Doesn't appear to work without a caption on the OptionGroup, and we don't want one.
					return statusOptionGroup;
				}
					
				if ( propertyId.equals("permListIds") ) {
					return createPermissionSelectBox("PackageForm.permList.label");
				} 
				
				if ( propertyId.equals("permViewDetailsIds") ) {
					return createPermissionSelectBox("PackageForm.permViewDetails.label");
				}
				
				if ( propertyId.equals("permCreateLikeIds") ) {
					return createPermissionSelectBox("PackageForm.permCreateLike.label");
				}
				
				if ( propertyId.equals("permUpdateIds") ) {
					return createPermissionSelectBox("PackageForm.permUpdate.label");
				}
				
				if ( propertyId.equals("permDeleteIds") ) {
					return createPermissionSelectBox("PackageForm.permDelete.label");
				}

				if ( propertyId.equals("comments") ) {
	            	TextArea ta = new TextArea();
	            	ta.setWidth(100, Unit.PERCENTAGE);
	            	ta.setNullRepresentation("");
	            	ta.setRows(3);
	            	ta.setInputPrompt(vaadinUi.getMsg("inputPrompt.comments"));
	            	ta.setCaption(vaadinUi.getMsg("caption.comments"));
	            	ta.setDescription(vaadinUi.getMsg("tooltip.comments"));
	            	/*
	            	ta.addFocusListener( new FieldEvents.FocusListener() {
						private static final long serialVersionUID = -8132582890163358782L;

						@Override
						public void focus(FocusEvent event) {
							if ( saveButton != null )
								saveButton.removeClickShortcut();
						}
	            	});
	            	ta.addBlurListener( new FieldEvents.BlurListener() {
						private static final long serialVersionUID = 6989982256198005033L;

						@Override
						public void blur(BlurEvent event) {
							if ( saveButton != null )
								saveButton.setClickShortcut(KeyCode.ENTER);
						}
					});
					*/
	            	return ta;
				}
				
            	if ( propertyId.equals("downloadFileNameSpec") ) {
            		FieldSpecTextField tf = new FieldSpecTextField();
            		tf.setWidth(100, Unit.PERCENTAGE);
            		tf.setNullRepresentation("");
            		tf.setCaption(vaadinUi.getMsg("PackageForm.downloadFileNameSpec.label"));
            		tf.setInputPrompt(vaadinUi.getMsg("PackageForm.downloadFileNameSpec.inputPrompt"));
            		tf.setDescription(vaadinUi.getMsg("PackageForm.downloadFileNameSpec.tooltip"));
            		return tf;
            	}
				
				Field<?> field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("pathName")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                	tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setCaption(vaadinUi.getMsg("caption.esfPathName"));
                	tf.setDescription(vaadinUi.getMsg("PackageForm.pathname.tooltip"));
                	tf.addValidator(new PackageEsfPathNameValidator(getCurrentBean()));
                	tf.setConversionError(vaadinUi.getMsg("validator.esfpathname.message"));
                } else if ( propertyId.equals("description") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                    tf.setCaption(vaadinUi.getMsg("caption.description"));
                	tf.setInputPrompt(vaadinUi.getMsg("inputPrompt.description"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.description"));
                }
                
                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
		allGroupListIds = null;
		allGroupListNames = null;
    }
    
    private TwinColSelect createPermissionSelectBox(String vaadinMessageCaption) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	TwinColSelect selectList = new TwinColSelect();
    	selectList.setLeftColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".left"));
    	selectList.setRightColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".right"));
    	selectList.setWidth(100, Unit.PERCENTAGE);
        selectList.addValidator(new SelectValidator(selectList));
		
		// Set all the possible values
		for( int i=0; i < allGroupListIds.length; ++i ) {
			selectList.addItem(allGroupListIds[i]);
		}
		// Associate labels with our ID values
		selectList.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		for( int i=0; i < allGroupListIds.length; ++i ) {
			selectList.setItemCaption(allGroupListIds[i], allGroupListNames[i].toPlainString());
		}
		
		selectList.setRows( Math.min(allGroupListIds.length,5) );
		selectList.setNullSelectionAllowed(true);
		selectList.setMultiSelect(true);
		return selectList;
    }
    
    @Override
	public void buttonClick(ClickEvent event) {
    	Button source = event.getButton();
    	if ( source == saveButton ) {
     		view.save();
        } else if ( source == cancelButton ) {
        	view.cancelChanges();
        	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    		vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
        	if ( newBean != null ) {
        		view.discardForms();
        		view.unselectAllPackageVersions();
        		view.selectPackage(prevBean);
        		prevBean = newBean = null; // we're not doing a new bean anymore
        	}
        } else if ( source == editButton ) {
        	view.enterEditMode();
        } else if ( source == createLikeButton ) {
        	PackageBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		createLike(currBean);
        	}	
        }
    }
	
    PackageBean save(Connection con) throws SQLException {
		Errors errors = new Errors();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( newBean != null ) {
    		if ( newBean.save(con,errors) ) {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",newBean.getPathName()) );

    			PackageBean savedDoc = newBean;
        		prevBean = newBean = null; // and we're not longer working on a new bean
        		return savedDoc;
    		} 
    		vaadinUi.show(errors);
    		return null;
		}
		
		PackageBean currBean = getCurrentBean();
		if ( currBean.save(con,errors) ) {
			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",currBean.getPathName()) );
			return currBean;
   		}
		
		vaadinUi.show(errors);
		return null;
    }

	public String getCurrentBeanName() {
		PackageBean currBean = getCurrentBean();
		return currBean == null ? "(None)" : currBean.getPathName().toString();
	}
    
	PackageBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public PackageBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<PackageBean> bi = (BeanItem<PackageBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached stuff
		resetCaches();
		
    	if (newDataSource != null) {
    		PackageBean bean = getBean(newDataSource);
    		
    		if ( _orderedProperties == null ) {
    			_orderedProperties = Arrays.asList("status","pathName","downloadFileNameSpec","description","comments","permListIds","permViewDetailsIds","permCreateLikeIds","permUpdateIds","permDeleteIds");
    		}
    		super.setItemDataSource(newDataSource, _orderedProperties);
    		
    		setupForm(bean);
    		
    		setReadOnly( ! view.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		newBean = bean.packageInfo().doInsert() ? bean : null;
    		if ( newBean != null ) {
    			TextField tf = (TextField)getField("pathName");
    			tf.selectAll();
    			tf.focus();
    		}
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		setReadOnly(true);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	PackageBean bean = getCurrentBean();
    	
    	// We have these only while editing/creating new
    	if ( saveButton != null ) {
    		saveButton.setVisible(!readOnly && bean != null && view.hasPermCreateLikeOrUpdate());
    	}

		cancelButton.setVisible(!readOnly && bean != null);

    	// We have this only while in view mode
		if ( editButton != null ) {
			editButton.setVisible(readOnly && bean != null && view.hasPermUpdate());
		}
    	
    	// This one we show this in all modes if has permission
		if ( createLikeButton != null ) {
			createLikeButton.setVisible(bean != null && bean.pkg().doUpdate() && view.hasPermCreateLike()); // only want create like on an existing document
		}

		// We don't want any updates to the status, name or delete permissions on these specialty libraries
		if ( bean != null ) {
	    	if ( bean.pkg().isReserved() ) {
	    		getField("pathName").setReadOnly(true);
	    		getField("status").setReadOnly(true);
	    		getField("permDeleteIds").setReadOnly(true);
	    	} else if ( bean.pkg().hasProductionVersion() ) {
	    		getField("pathName").setReadOnly(true);
	    	}
		}
    }
    
    public void createLike(PackageBean likeBean) {
    	view.createLikePackage();
    	prevBean = likeBean;
     }
    
    void setupForm(PackageBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("PackageForm.label.id",bean.getId()) );

		createPermDisclosure();
    }
    
    void createPermDisclosure() {
    	VerticalLayout permLayout = new VerticalLayout();
    	permLayout.setMargin(false);
    	permLayout.setSpacing(true);

		permLayout.addComponent(getField("permListIds"));
		permLayout.addComponent(getField("permViewDetailsIds"));
		permLayout.addComponent(getField("permCreateLikeIds"));
		permLayout.addComponent(getField("permUpdateIds"));
		permLayout.addComponent(getField("permDeleteIds"));
				
		permDisclosure.setContent(permLayout);
		permDisclosure.setVisible(true);
    }

    
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("status")) {
            layout.addComponent(field, 0, 0);
        } else if (propertyId.equals("pathName")) {
        	layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("downloadFileNameSpec")) {
        	layout.addComponent(field, 1, 1);
        } else if (propertyId.equals("description")) {
        	layout.addComponent(field, 0, 2, 1, 2);
        } else if (propertyId.equals("comments")) {
        	layout.addComponent(field, 0, 3, 1, 3);
        }
    }

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageAndVersionsMainView.Document.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}