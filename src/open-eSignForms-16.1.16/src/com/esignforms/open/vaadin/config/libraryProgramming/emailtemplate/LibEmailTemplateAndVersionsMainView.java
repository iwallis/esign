// Copyright (C) 2011-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.emailtemplate;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.EmailTemplateInfo;
import com.esignforms.open.prog.EmailTemplateVersionInfo;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

/**
 * LibEmailTemplateAndVersionsMainView is a splitpanel that contains the emailtemplate view in the left and the emailtemplate version view on the right.
 * 
 * Each of those views is itself a splitpanel, containing a table/list on the top to select items, with a form on the bottom to view/edit.
 * 
 * When a new emailtemplate is selected in the left view, we propagate that to the right view so it can sync the versions with the selected emailtemplate.
 * 
 * @author Yozons
 *
 */
public class LibEmailTemplateAndVersionsMainView extends HorizontalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = -9177896859848320206L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibEmailTemplateAndVersionsMainView.class);

    EsfUUID libraryId;
    Library library;
    final LibEmailTemplateAndVersionsMainView thisView;
	
    VerticalSplitPanel emailtemplateSplitPanel;
    LibEmailTemplateBeanContainer emailtemplateContainer;
    LibEmailTemplateList emailtemplateList;
    LibEmailTemplateForm emailtemplateForm;
	
    VerticalSplitPanel emailtemplateVerSplitPanel;
    LibEmailTemplateVersionBeanContainer emailtemplateVerContainer;
	LibEmailTemplateVersionList emailtemplateVerList;
	LibEmailTemplateVersionForm emailtemplateVerForm;

	public LibEmailTemplateAndVersionsMainView() {
		super(); 
		thisView = this;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		try {
			library = Library.Manager.getById(libraryId, vaadinUi.getUser());
			
			emailtemplateContainer = new LibEmailTemplateBeanContainer( library.getId());
			emailtemplateVerContainer = new LibEmailTemplateVersionBeanContainer(this); // Load contents when a emailtemplate is selected from our emailtemplateContainer
			
			emailtemplateList = new LibEmailTemplateList(this,emailtemplateContainer);
		    emailtemplateForm = new LibEmailTemplateForm(this,emailtemplateContainer);

			emailtemplateSplitPanel = new VerticalSplitPanel();
			emailtemplateSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			emailtemplateSplitPanel.setSplitPosition(30);
			emailtemplateSplitPanel.setFirstComponent(emailtemplateList);
			emailtemplateSplitPanel.setSecondComponent(emailtemplateForm);
		    
			emailtemplateVerList = new LibEmailTemplateVersionList(this,emailtemplateVerContainer);
			emailtemplateVerForm = new LibEmailTemplateVersionForm(this,emailtemplateVerContainer);
			
			emailtemplateVerSplitPanel = new VerticalSplitPanel();
			emailtemplateVerSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			emailtemplateVerSplitPanel.setSplitPosition(30);
			emailtemplateVerSplitPanel.setFirstComponent(emailtemplateVerList);
			emailtemplateVerSplitPanel.setSecondComponent(emailtemplateVerForm);

			setFirstComponent(emailtemplateSplitPanel);
			setSecondComponent(emailtemplateVerSplitPanel);
			setSplitPosition(35);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("EmailTemplate and versions main view exception", e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	private LibEmailTemplateBean getLibEmailTemplateBean(Item emailtemplateListItem) {
    	if ( emailtemplateListItem == null )
    		return null;
		BeanItem<LibEmailTemplateBean> bi = (BeanItem<LibEmailTemplateBean>)emailtemplateListItem;
		return bi.getBean();
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our emailtemplate list or not
        if (property == emailtemplateList.getTable()) {
        	final Item item = emailtemplateList.getTable().getItem(emailtemplateList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( emailtemplateForm.getCurrentBean() == emailtemplateForm.getBean(item) ) {
    				// We've selected the same bean so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibEmailTemplateAndVersionsMainView.EmailTemplate.ConfirmDiscardChangesDialog.message", emailtemplateForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						thisView.discardForms();
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						emailtemplateList.getTable().select(emailtemplateForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibEmailTemplateAndVersionsMainView.EmailTemplate.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	LibEmailTemplateBean bean = getLibEmailTemplateBean(item);
                emailtemplateForm.setItemDataSource(item);
            	if ( bean != null ) {
                	emailtemplateVerContainer.reload(bean.emailtemplate());
                	selectFirstEmailTemplateVersion();
            	} else {
            		emailtemplateVerForm.setItemDataSource(null);
            		emailtemplateVerContainer.removeAllItems();
            	}
    		}
        } else if (property == emailtemplateVerList.getTable()) {
        	final Item item = emailtemplateVerList.getTable().getItem(emailtemplateVerList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( emailtemplateVerForm.getCurrentBean() == emailtemplateVerForm.getBean(item) ) {
    				// We've selected the same bean (version) so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibEmailTemplateAndVersionsMainView.EmailTemplate.ConfirmDiscardChangesDialog.message", emailtemplateForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						emailtemplateVerForm.discard();
						emailtemplateVerForm.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						emailtemplateVerList.getTable().select(emailtemplateVerForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibEmailTemplateAndVersionsMainView.EmailTemplate.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	emailtemplateVerForm.setItemDataSource(item);
    		}
        }
        else {
        	_logger.debug("valueChange() - no list event: " + event.toString());
        }
	}
	

	@Override
	public void detach() {
		/*
		for( Object o : list.getTable().getVisibleColumns() ) {
			_logger.debug("detach() - list visible columns[]: " + o); // These are string property ids of the column names like email, status, lastLoginTimestamp
		}
		*/
		super.detach();
	}
	
	public void selectEmailTemplate(LibEmailTemplateBean bean) {
		unselectAllEmailTemplates();
		emailtemplateList.getTable().select(bean);
		emailtemplateList.getTable().setCurrentPageFirstItemId(bean);
		if ( bean == null ) {
			emailtemplateVerForm.setItemDataSource(null);
		}
	}
	
	public void unselectAllEmailTemplates() {
		emailtemplateList.getTable().setValue(null);
		emailtemplateForm.setItemDataSource(null);
	}
	
	public void selectEmailTemplateVersion(LibEmailTemplateVersionBean bean) {
		unselectAllEmailTemplateVersions();
		emailtemplateVerList.getTable().select(bean);
		emailtemplateVerList.getTable().setCurrentPageFirstItemId(bean);
	}

	public void discardForms() {
		emailtemplateForm.discard();
		emailtemplateVerForm.discard();
		emailtemplateForm.setItemDataSource(null);
		emailtemplateVerForm.setItemDataSource(null);
	}

	
	public void selectFirstEmailTemplateVersion() {
		if ( emailtemplateVerContainer.size() > 0 ) {
			selectEmailTemplateVersion(emailtemplateVerContainer.getIdByIndex(0));
		} else {
			unselectAllEmailTemplateVersions(); 
		}
	}
	
	public void unselectAllEmailTemplateVersions() {
		emailtemplateVerList.getTable().setValue(null);
	}

	public Library getLibrary() {
		return library;
	}
	
	public LibEmailTemplateBean getLibEmailTemplateBean() {
		return emailtemplateForm == null ? null : emailtemplateForm.getCurrentBean();
	}
	
	public LibEmailTemplateVersionBean getLibEmailTemplateVersionBean() {
		return emailtemplateVerForm == null ? null : emailtemplateVerForm.getCurrentBean();
	}
	
	public String getEsfNameWithVersion() {
		LibEmailTemplateBean emailtemplateBean = getLibEmailTemplateBean();
		LibEmailTemplateVersionBean emailtemplateVerBean = getLibEmailTemplateVersionBean();
		return emailtemplateVerBean == null ? "?? [?]" : emailtemplateBean.getEsfName() + " [" + emailtemplateVerBean.getVersion() + "]";
	}
	
	public Integer getProductionVersion() {
		LibEmailTemplateBean emailtemplateBean = getLibEmailTemplateBean();
		return emailtemplateBean == null ? 0 : emailtemplateBean.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		LibEmailTemplateBean emailtemplateBean = getLibEmailTemplateBean();
		return emailtemplateBean == null ? 0 : emailtemplateBean.getTestVersion();
	}

	public boolean hasTestVersion() {
		LibEmailTemplateBean emailtemplateBean = getLibEmailTemplateBean();
		return emailtemplateBean == null ? false : emailtemplateBean.emailtemplate().hasTestVersion();
	}
	
    public boolean isProductionVersion(EmailTemplateVersionInfo emailtemplateVer)
    {
    	LibEmailTemplateBean emailtemplateBean = getLibEmailTemplateBean();
    	return emailtemplateBean != null && emailtemplateVer != null && emailtemplateVer.getVersion() == emailtemplateBean.getProductionVersion();
    }

    public boolean isTestVersion(EmailTemplateVersionInfo emailtemplateVer)
    {
    	LibEmailTemplateBean emailtemplateBean = getLibEmailTemplateBean();
    	return emailtemplateBean != null && emailtemplateVer != null && emailtemplateVer.getVersion() > emailtemplateBean.getProductionVersion();
    }

    // "Current" here means latest production or test version.
    public boolean isCurrentVersion(EmailTemplateVersionInfo emailtemplateVer)
    {
    	LibEmailTemplateBean emailtemplateBean = getLibEmailTemplateBean();
    	return emailtemplateBean != null && emailtemplateVer != null && emailtemplateVer.getVersion() >= emailtemplateBean.getProductionVersion();
    }

    public boolean isOldVersion(EmailTemplateVersionInfo emailtemplateVer)
    {
    	LibEmailTemplateBean emailtemplateBean = getLibEmailTemplateBean();
    	return emailtemplateBean != null && emailtemplateVer != null && emailtemplateVer.getVersion() < emailtemplateBean.getProductionVersion();
    }
	
	public String getVersionLabel(EmailTemplateVersionInfo emailtemplateVer) {
		if ( isTestVersion(emailtemplateVer) )
			return Literals.VERSION_LABEL_TEST;
		if ( isProductionVersion(emailtemplateVer) )
			return Literals.VERSION_LABEL_PRODUCTION;
		return Literals.VERSION_LABEL_NOT_CURRENT;
	}
	
	public boolean hasPermViewDetails() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_DELETE);
	}
	
	public boolean cancelChanges() {
		emailtemplateForm.discard();
		emailtemplateVerForm.discard();
		emailtemplateForm.setReadOnly(true);
		emailtemplateVerForm.setReadOnly(true);
		return true;
	}
	
	public boolean enterEditMode() {
		emailtemplateForm.setReadOnly(false);
		emailtemplateVerForm.setReadOnly(false);
		return true;
	}
	
	public boolean save() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
		if ( ! emailtemplateForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		if ( ! emailtemplateVerForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		emailtemplateForm.commit();
		emailtemplateVerForm.commit();
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			// If we're saving a new test version, we'll want to bump our emailtemplate to show this as the new test version
			LibEmailTemplateBean currEmailTemplate = getLibEmailTemplateBean();
			LibEmailTemplateVersionBean currEmailTemplateVer = getLibEmailTemplateVersionBean();
			if ( currEmailTemplateVer.emailtemplateVersion().getVersion() > currEmailTemplate.emailtemplate().getTestVersion() )
				currEmailTemplate.emailtemplate().bumpTestVersion();
			
			LibEmailTemplateBean savedEmailTemplate = emailtemplateForm.save(con);
			if ( savedEmailTemplate == null )  {
				con.rollback();
				return false;
			}
			
			LibEmailTemplateVersionBean savedDocVer = emailtemplateVerForm.save(con);
			if ( savedDocVer == null )  {
				con.rollback();
				return false;
			}
			
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Saved emailtemplate " + savedEmailTemplate.getEsfName() + " version " + savedDocVer.getVersion() + ". EmailTemplateVersionId: " + savedDocVer.getId() + "; status: " + savedEmailTemplate.getStatus());
			}

			con.commit();
	    		
			emailtemplateContainer.refresh();  // refresh our list after a change
			selectEmailTemplate(savedEmailTemplate);
			selectEmailTemplateVersion(savedDocVer);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean createNewEmailTemplate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		EmailTemplateInfo emailtemplateInfo = EmailTemplateInfo.Manager.createNew(getLibrary().getId());
		LibEmailTemplateBean emailtemplateBean = new LibEmailTemplateBean(emailtemplateInfo);
		BeanItem<LibEmailTemplateBean> emailtemplateItem = new BeanItem<LibEmailTemplateBean>(emailtemplateBean);
        emailtemplateForm.setItemDataSource(emailtemplateItem);
		
		EmailTemplateVersionInfo emailtemplateVerInfo = EmailTemplateVersionInfo.Manager.createNew(EmailTemplate.Manager.getById(emailtemplateInfo.getId()),vaadinUi.getUser());
		LibEmailTemplateVersionBean emailtemplateVerBean = new LibEmailTemplateVersionBean(emailtemplateVerInfo,getVersionLabel(emailtemplateVerInfo));
		BeanItem<LibEmailTemplateVersionBean> emailtemplateVerItem = new BeanItem<LibEmailTemplateVersionBean>(emailtemplateVerBean);
        emailtemplateVerForm.setItemDataSource(emailtemplateVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean createLikeEmailTemplate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibEmailTemplateBean likeDocBean = getLibEmailTemplateBean();
		LibEmailTemplateVersionBean likeDocVerBean = getLibEmailTemplateVersionBean();
		
    	LibEmailTemplateBean createdDocBean = likeDocBean.createLike();
		BeanItem<LibEmailTemplateBean> createdDocItem = new BeanItem<LibEmailTemplateBean>(createdDocBean);
        emailtemplateForm.setItemDataSource(createdDocItem);

    	EmailTemplateVersionInfo createdEmailTemplateVerInfo = EmailTemplateVersionInfo.Manager.createLike(EmailTemplate.Manager.getById(createdDocBean.emailtemplateInfo().getId()), likeDocVerBean.emailtemplateVersion(), vaadinUi.getUser());
    	LibEmailTemplateVersionBean createdEmailTemplateVerBean = new LibEmailTemplateVersionBean(createdEmailTemplateVerInfo,Literals.VERSION_LABEL_TEST); // The new emailtemplate and version will always be Test.
		BeanItem<LibEmailTemplateVersionBean> createdEmailTemplateVerItem = new BeanItem<LibEmailTemplateVersionBean>(createdEmailTemplateVerBean);       
        emailtemplateVerForm.setItemDataSource(createdEmailTemplateVerItem);
        
        setReadOnly(false);
		return true;
	}

	public boolean createNextVersion() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibEmailTemplateBean currEmailTemplateBean = getLibEmailTemplateBean();
		LibEmailTemplateVersionBean currEmailTemplateVerBean = getLibEmailTemplateVersionBean();
		
        emailtemplateForm.setReadOnly(!hasPermUpdate()); // should have permission since doing a create next version...

    	EmailTemplateVersionInfo nextEmailTemplateVerInfo = EmailTemplateVersionInfo.Manager.createLike(currEmailTemplateBean.emailtemplate(), currEmailTemplateVerBean.emailtemplateVersion(), vaadinUi.getUser());
    	LibEmailTemplateVersionBean nextEmailTemplateVerBean = new LibEmailTemplateVersionBean(nextEmailTemplateVerInfo,Literals.VERSION_LABEL_TEST); // The new version will always be Test.
		BeanItem<LibEmailTemplateVersionBean> nextEmailTemplateVerItem = new BeanItem<LibEmailTemplateVersionBean>(nextEmailTemplateVerBean);
        emailtemplateVerForm.setItemDataSource(nextEmailTemplateVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean promoteTestToProductionVersion() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibEmailTemplateVersionForm.versionChange.TestToProd.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibEmailTemplateBean emailtemplateBean = getLibEmailTemplateBean();
			LibEmailTemplateVersionBean emailtemplateVerBean = getLibEmailTemplateVersionBean();
			
			emailtemplateBean.emailtemplate().promoteTestVersionToProduction();
			
			if ( ! emailtemplateBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved emailtemplate " + emailtemplateBean.getEsfName() + " version " + emailtemplateVerBean.getVersion() + " into Production status. EmailTemplateVersionId: " + emailtemplateVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved emailtemplate " + emailtemplateBean.getEsfName() + " version " + emailtemplateVerBean.getVersion() + " into Production status. EmailTemplateVersionId: " + emailtemplateVerBean.getId());
			}
			
			con.commit();
			emailtemplateContainer.refresh();  // refresh our list after a change
			selectEmailTemplate(emailtemplateBean);
			selectEmailTemplateVersion(emailtemplateVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibEmailTemplateVersionForm.versionChange.TestToProd.success.message",emailtemplateBean.getEsfName(),emailtemplateVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean revertProductionVersionBackToTest() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibEmailTemplateVersionForm.versionChange.revertProdToTest.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibEmailTemplateBean emailtemplateBean = getLibEmailTemplateBean();
			LibEmailTemplateVersionBean emailtemplateVerBean = getLibEmailTemplateVersionBean();
			
			emailtemplateBean.emailtemplate().revertProductionVersionBackToTest();
			
			if ( ! emailtemplateBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved Production emailtemplate " + emailtemplateBean.getEsfName() + " version " + emailtemplateVerBean.getVersion() + " back into Test status. EmailTemplateVersionId: " + emailtemplateVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved Production emailtemplate " + emailtemplateBean.getEsfName() + " version " + emailtemplateVerBean.getVersion() + " back into Test status. EmailTemplateVersionId: " + emailtemplateVerBean.getId());
			}
			
			con.commit();
			emailtemplateContainer.refresh();  // refresh our list after a change
			selectEmailTemplate(emailtemplateBean);
			selectEmailTemplateVersion(emailtemplateVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibEmailTemplateVersionForm.versionChange.revertProdToTest.success.message",emailtemplateBean.getEsfName(),emailtemplateVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean delete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibEmailTemplateBean currEmailTemplate = getLibEmailTemplateBean();
			LibEmailTemplateVersionBean currEmailTemplateVer = getLibEmailTemplateVersionBean();
			
			// If there is no Production version, we'll get rid of the email template entirely, otherwise just the test version
			Errors errors = new Errors();
			if ( currEmailTemplate.getProductionVersion() == 0 )
				currEmailTemplate.emailtemplate().delete(con,errors,vaadinUi.getUser());
			else {
				currEmailTemplateVer.emailtemplateVersion().delete(con,errors,vaadinUi.getUser());
				currEmailTemplate.dropTestVersion();
				currEmailTemplate.save(con, errors);
			}
			
			if ( errors.hasError() ) {
				vaadinUi.show(errors);
				con.rollback();
				return false;
			}
			
			con.commit();
    		
			vaadinUi.showStatus(vaadinUi.getMsg("LibEmailTemplateVersionForm.button.delete.successMessage",currEmailTemplate.getEsfName(),currEmailTemplateVer.getVersion()));

			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Deleted emailtemplate " + currEmailTemplate.getEsfName() + " version " + currEmailTemplateVer.getVersion() + ". EmailTemplateVersionId: " + currEmailTemplateVer.getId() + "; status: " + currEmailTemplate.getStatus());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted emailtemplate " + currEmailTemplate.getEsfName() + " version " + currEmailTemplateVer.getVersion() + ". EmailTemplateVersionId: " + currEmailTemplateVer.getId() + "; status: " + currEmailTemplate.getStatus());
			}

			emailtemplateContainer.refresh();  // refresh our list after a change
			selectEmailTemplate(null);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		_logger.debug("activateView() - params: " + params);
		libraryId = new EsfUUID(params);
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return emailtemplateForm.isDirty() ? emailtemplateForm.checkDirty() : emailtemplateVerForm.checkDirty();
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return emailtemplateForm.isDirty() || emailtemplateVerForm.isDirty();
	}
}