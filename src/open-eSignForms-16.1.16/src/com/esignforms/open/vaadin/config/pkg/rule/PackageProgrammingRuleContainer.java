// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule;

import java.io.Serializable;
import java.util.List;

import com.esignforms.open.prog.PackageProgrammingRule;
import com.vaadin.data.util.BeanItemContainer;


public class PackageProgrammingRuleContainer extends BeanItemContainer<PackageProgrammingRule> implements Serializable {
	private static final long serialVersionUID = -2866421724588685633L;

	public PackageProgrammingRuleContainer(List<PackageProgrammingRule> rulesList) throws InstantiationException, IllegalAccessException {
		super(PackageProgrammingRule.class);
		refresh(rulesList);
	}
	
	public void refresh(List<PackageProgrammingRule> rulesList) {
		removeAllItems();
		int order = 1;
		for( PackageProgrammingRule rule : rulesList ) {
			rule.setOrder(order++);
			addItem(rule);
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}