// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.file;

import java.sql.Connection;
import java.sql.SQLException;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.File;
import com.esignforms.open.prog.FileInfo;
import com.esignforms.open.prog.FileVersionInfo;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.dialog.ConfirmDiscardChangesDialog;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalSplitPanel;
import com.vaadin.ui.themes.Reindeer;

/**
 * LibFileAndVersionsMainView is a splitpanel that contains the file view in the left and the file version view on the right.
 * 
 * Each of those views is itself a splitpanel, containing a table/list on the top to select items, with a form on the bottom to view/edit.
 * 
 * When a new file is selected in the left view, we propagate that to the right view so it can sync the versions with the selected file.
 * 
 * @author Yozons
 *
 */
public class LibFileAndVersionsMainView extends HorizontalSplitPanel implements EsfView, Property.ValueChangeListener {
	private static final long serialVersionUID = 9201218128872829665L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(LibFileAndVersionsMainView.class);

    EsfUUID libraryId;
    Library library;
    final LibFileAndVersionsMainView thisView;
	
    VerticalSplitPanel fileSplitPanel;
    LibFileBeanContainer fileContainer;
    LibFileList fileList;
    LibFileForm fileForm;
	
    VerticalSplitPanel fileVerSplitPanel;
    LibFileVersionBeanContainer fileVerContainer;
	LibFileVersionList fileVerList;
	LibFileVersionForm fileVerForm;

	public LibFileAndVersionsMainView() {
		super(); 
		thisView = this;
        setStyleName(Reindeer.SPLITPANEL_SMALL);
     }
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {
			library = Library.Manager.getById(libraryId, vaadinUi.getUser());
			
			fileContainer = new LibFileBeanContainer( library);
			fileVerContainer = new LibFileVersionBeanContainer(this); // Load contents when a file is selected from our fileContainer
			
			fileList = new LibFileList(this,fileContainer);
		    fileForm = new LibFileForm(this,fileContainer);

			fileSplitPanel = new VerticalSplitPanel();
			fileSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			fileSplitPanel.setSplitPosition(30);
			fileSplitPanel.setFirstComponent(fileList);
			fileSplitPanel.setSecondComponent(fileForm);
		    
			fileVerList = new LibFileVersionList(this,fileVerContainer);
			fileVerForm = new LibFileVersionForm(this,fileVerContainer);
			
			fileVerSplitPanel = new VerticalSplitPanel();
			fileVerSplitPanel.setStyleName(Reindeer.SPLITPANEL_SMALL);
			fileVerSplitPanel.setSplitPosition(30);
			fileVerSplitPanel.setFirstComponent(fileVerList);
			fileVerSplitPanel.setSecondComponent(fileVerForm);

			setFirstComponent(fileSplitPanel);
			setSecondComponent(fileVerSplitPanel);
			setSplitPosition(50);
	    	_logger.debug("buildLayout() completed");
		} catch( Exception e ) {
			_logger.debug("Failed to build view", e);
			vaadinUi.showError("File and versions main view exception", e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	private LibFileBean getLibFileBean(Item fileListItem) {
    	if ( fileListItem == null )
    		return null;
		BeanItem<LibFileBean> bi = (BeanItem<LibFileBean>)fileListItem;
		return bi.getBean();
	}
	
	@Override
	public void valueChange(ValueChangeEvent event) {
        Property<?> property = event.getProperty();
        
        // Determine if the click is on our file list or not
        if (property == fileList.getTable()) {
        	final Item item = fileList.getTable().getItem(fileList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( fileForm.getCurrentBean() == fileForm.getBean(item) ) {
    				// We've selected the same bean so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibFileAndVersionsMainView.File.ConfirmDiscardChangesDialog.message", fileForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						thisView.discardForms();
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						fileList.getTable().select(fileForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibFileAndVersionsMainView.File.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	LibFileBean bean = getLibFileBean(item);
                fileForm.setItemDataSource(item);
            	if ( bean != null ) {
                	fileVerContainer.reload(bean.file());
                	selectFirstFileVersion();
            	} else {
            		fileVerForm.setItemDataSource(null);
            		fileVerContainer.removeAllItems();
            	}
    		}
        } else if (property == fileVerList.getTable()) {
        	final Item item = fileVerList.getTable().getItem(fileVerList.getTable().getValue());
        	
        	if ( isDirty() ) {
    			if ( fileVerForm.getCurrentBean() == fileVerForm.getBean(item) ) {
    				// We've selected the same bean (version) so don't need to warn (or reset the data source)
    				return;
    			}
    			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    			new ConfirmDiscardChangesDialog(vaadinUi.getMsg("LibFileAndVersionsMainView.File.ConfirmDiscardChangesDialog.message", fileForm.getCurrentBeanName()), 
    					new ConfirmDiscardChangesDialog.ConfirmDiscardChangesListener() {
					
					@Override
					public void doDiscardChanges() {
						fileVerForm.discard();
						fileVerForm.setItemDataSource(item);
					}
					
					@Override
					public void doContinueKeepChanges() {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						fileVerList.getTable().select(fileVerForm.getCurrentBean()); // reselect the original
						vaadinUi.showStatus(vaadinUi.getMsg("LibFileAndVersionsMainView.File.ConfirmDiscardChangesDialog.continueKeepStatus"));
					}
				});
    			
    		} else {
            	fileVerForm.setItemDataSource(item);
    		}
        }
        else {
        	_logger.debug("valueChange() - no list event: " + event.toString());
        }
	}
	

	@Override
	public void detach() {
		/*
		for( Object o : list.getTable().getVisibleColumns() ) {
			_logger.debug("detach() - list visible columns[]: " + o); // These are string property ids of the column names like email, status, lastLoginTimestamp
		}
		*/
		super.detach();
	}
	
	public void selectFile(LibFileBean bean) {
		unselectAllFiles();
		fileList.getTable().select(bean);
		fileList.getTable().setCurrentPageFirstItemId(bean);
		if ( bean == null ) {
			fileVerForm.setItemDataSource(null);
		}
	}
	
	public void unselectAllFiles() {
		fileList.getTable().setValue(null);
		fileForm.setItemDataSource(null);
	}
	
	public void selectFileVersion(LibFileVersionBean bean) {
		unselectAllFileVersions();
		fileVerList.getTable().select(bean);
		fileVerList.getTable().setCurrentPageFirstItemId(bean);
	}

	public void discardForms() {
		fileForm.discard();
		fileVerForm.discard();
		fileForm.setItemDataSource(null);
		fileVerForm.setItemDataSource(null);
	}

	
	public void selectFirstFileVersion() {
		if ( fileVerContainer.size() > 0 ) {
			selectFileVersion(fileVerContainer.getIdByIndex(0));
		} else {
			unselectAllFileVersions(); 
		}
	}
	
	public void unselectAllFileVersions() {
		fileVerList.getTable().setValue(null);
	}

	public Library getLibrary() {
		return library;
	}
	
	public LibFileBean getLibFileBean() {
		return fileForm == null ? null : fileForm.getCurrentBean();
	}
	
	public LibFileVersionBean getLibFileVersionBean() {
		return fileVerForm == null ? null : fileVerForm.getCurrentBean();
	}
	
	public String getEsfNameWithVersion() {
		LibFileBean fileBean = getLibFileBean();
		LibFileVersionBean fileVerBean = getLibFileVersionBean();
		return fileVerBean == null ? "?? [?]" : fileBean.getEsfName() + " [" + fileVerBean.getVersion() + "]";
	}
	
	public Integer getProductionVersion() {
		LibFileBean fileBean = getLibFileBean();
		return fileBean == null ? 0 : fileBean.getProductionVersion();
	}
	
	public Integer getTestVersion() {
		LibFileBean fileBean = getLibFileBean();
		return fileBean == null ? 0 : fileBean.getTestVersion();
	}

	public boolean hasTestVersion() {
		LibFileBean fileBean = getLibFileBean();
		return fileBean == null ? false : fileBean.file().hasTestVersion();
	}
	
    public boolean isProductionVersion(FileVersionInfo fileVer) {
    	LibFileBean fileBean = getLibFileBean();
    	return fileBean != null && fileVer != null && fileVer.getVersion() == fileBean.getProductionVersion();
    }

    public boolean isTestVersion(FileVersionInfo fileVer) {
    	LibFileBean fileBean = getLibFileBean();
    	return fileBean != null && fileVer != null && fileVer.getVersion() > fileBean.getProductionVersion();
    }

    // "Current" here means latest production or test version.
    public boolean isCurrentVersion(FileVersionInfo fileVer) {
    	LibFileBean fileBean = getLibFileBean();
    	return fileBean != null && fileVer != null && fileVer.getVersion() >= fileBean.getProductionVersion();
    }

    public boolean isOldVersion(FileVersionInfo fileVer) {
    	LibFileBean fileBean = getLibFileBean();
    	return fileBean != null && fileVer != null && fileVer.getVersion() < fileBean.getProductionVersion();
    }
	
	public String getVersionLabel(FileVersionInfo fileVer) {
		if ( isTestVersion(fileVer) )
			return Literals.VERSION_LABEL_TEST;
		if ( isProductionVersion(fileVer) )
			return Literals.VERSION_LABEL_PRODUCTION;
		return Literals.VERSION_LABEL_NOT_CURRENT;
	}
	
	public boolean hasPermViewDetails() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return getLibrary().hasPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_DELETE);
	}
	
	public boolean cancelChanges() {
		fileForm.discard();
		fileVerForm.discard();
		fileForm.setReadOnly(true);
		fileVerForm.setReadOnly(true);
		return true;
	}
	
	public boolean enterEditMode() {
		fileForm.setReadOnly(false);
		fileVerForm.setReadOnly(false);
		return true;
	}
	
	public boolean save() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
		if ( ! fileForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		if ( ! fileVerForm.isValid() ) {
			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
    		return false;
		}
		fileForm.commit();
		fileVerForm.commit();
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			// If we're saving a new test version, we'll want to bump our file to show this as the new test version
			LibFileBean currFile = getLibFileBean();
			LibFileVersionBean currFileVer = getLibFileVersionBean();
			if ( currFileVer.fileVersion().getVersion() > currFile.file().getTestVersion() )
				currFile.file().bumpTestVersion();
			
			LibFileBean savedFile = fileForm.save(con);
			if ( savedFile == null )  {
				con.rollback();
				return false;
			}
			
			LibFileVersionBean savedFileVer = fileVerForm.save(con);
			if ( savedFileVer == null )  {
				con.rollback();
				return false;
			}
			
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Saved file " + savedFile.getEsfName() + " version " + savedFileVer.getVersion() + ". FileVersionId: " + savedFileVer.getId() + "; status: " + savedFile.getStatus());
			}

			con.commit();
	    		
			fileContainer.refresh();  // refresh our list after a change
			selectFile(savedFile);
			selectFileVersion(savedFileVer);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean createNewFile() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		FileInfo fileInfo = FileInfo.Manager.createNew(getLibrary().getId());
		LibFileBean fileBean = new LibFileBean(fileInfo);
		BeanItem<LibFileBean> fileItem = new BeanItem<LibFileBean>(fileBean);
        fileForm.setItemDataSource(fileItem);
		
		FileVersionInfo fileVerInfo = FileVersionInfo.Manager.createNew(File.Manager.getById(fileInfo.getId()),vaadinUi.getUser());
		LibFileVersionBean fileVerBean = new LibFileVersionBean(fileVerInfo,getVersionLabel(fileVerInfo));
		BeanItem<LibFileVersionBean> fileVerItem = new BeanItem<LibFileVersionBean>(fileVerBean);
        fileVerForm.setItemDataSource(fileVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean createLikeFile() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibFileBean likeFileBean = getLibFileBean();
		LibFileVersionBean likeFileVerBean = getLibFileVersionBean();
		
    	LibFileBean createdFileBean = likeFileBean.createLike();
		BeanItem<LibFileBean> createdFileItem = new BeanItem<LibFileBean>(createdFileBean);
        fileForm.setItemDataSource(createdFileItem);

    	FileVersionInfo createdFileVerInfo = FileVersionInfo.Manager.createLike(File.Manager.getById(createdFileBean.fileInfo().getId()), likeFileVerBean.fileVersion(), vaadinUi.getUser());
    	LibFileVersionBean createdFileVerBean = new LibFileVersionBean(createdFileVerInfo,Literals.VERSION_LABEL_TEST); // The new file and version will always be Test.
		BeanItem<LibFileVersionBean> createdFileVerItem = new BeanItem<LibFileVersionBean>(createdFileVerBean);       
        fileVerForm.setItemDataSource(createdFileVerItem);
        
        setReadOnly(false);
		return true;
	}

	public boolean createNextVersion() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		LibFileBean currFileBean = getLibFileBean();
		LibFileVersionBean currFileVerBean = getLibFileVersionBean();
		
        fileForm.setReadOnly(!hasPermUpdate()); // should have permission since doing a create next version...

    	FileVersionInfo nextFileVerInfo = FileVersionInfo.Manager.createLike(currFileBean.file(), currFileVerBean.fileVersion(), vaadinUi.getUser());
    	LibFileVersionBean nextFileVerBean = new LibFileVersionBean(nextFileVerInfo,Literals.VERSION_LABEL_TEST); // The new version will always be Test.
		BeanItem<LibFileVersionBean> nextFileVerItem = new BeanItem<LibFileVersionBean>(nextFileVerBean);
        fileVerForm.setItemDataSource(nextFileVerItem);

        setReadOnly(false);
		return true;
	}

	public boolean promoteTestToProductionVersion() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibFileVersionForm.versionChange.TestToProd.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibFileBean fileBean = getLibFileBean();
			LibFileVersionBean fileVerBean = getLibFileVersionBean();
			
			fileBean.file().promoteTestVersionToProduction();
			
			if ( ! fileBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved file " + fileBean.getEsfName() + " version " + fileVerBean.getVersion() + " into Production status. FileVersionId: " + fileVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved file " + fileBean.getEsfName() + " version " + fileVerBean.getVersion() + " into Production status. FileVersionId: " + fileVerBean.getId());
			}
			
			con.commit();
			fileContainer.refresh();  // refresh our list after a change
			selectFile(fileBean);
			selectFileVersion(fileVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibFileVersionForm.versionChange.TestToProd.success.message",fileBean.getEsfName(),fileVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean revertProductionVersionBackToTest() {
    	Errors errors = new Errors();
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		if ( isDirty() ) {
    		errors.addWarning(vaadinUi.getMsg("LibFileVersionForm.versionChange.revertProdToTest.dirtyform.message"));
    		vaadinUi.show(errors);
    		return false;
		} 
		
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibFileBean fileBean = getLibFileBean();
			LibFileVersionBean fileVerBean = getLibFileVersionBean();
			
			fileBean.file().revertProductionVersionBackToTest();
			
			if ( ! fileBean.save(con, errors) ) {
				con.rollback();
				vaadinUi.show(errors);
				return false;
			}
	    	
			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Moved Production file " + fileBean.getEsfName() + " version " + fileVerBean.getVersion() + " back into Test status. FileVersionId: " + fileVerBean.getId());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " moved Production file " + fileBean.getEsfName() + " version " + fileVerBean.getVersion() + " back into Test status. FileVersionId: " + fileVerBean.getId());
			}
			
			con.commit();
			fileContainer.refresh();  // refresh our list after a change
			selectFile(fileBean);
			selectFileVersion(fileVerBean);
			vaadinUi.showStatus(vaadinUi.getMsg("LibFileVersionForm.versionChange.revertProdToTest.success.message",fileBean.getEsfName(),fileVerBean.getVersion()));
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	public boolean delete() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		ConnectionPool    pool = vaadinUi.getEsfapp().getConnectionPool();
		Connection        con  = pool.getConnection();
		try
		{
			LibFileBean currFile = getLibFileBean();
			LibFileVersionBean currFileVer = getLibFileVersionBean();
			
			// If there is no Production version, we'll get rid of the file entirely, otherwise just the test version
			Errors errors = new Errors();
			if ( currFile.getProductionVersion() == 0 )
				currFile.file().delete(con,errors,vaadinUi.getUser());
			else {
				currFileVer.fileVersion().delete(con,errors,vaadinUi.getUser());
				currFile.dropTestVersion();
				currFile.save(con, errors);
			}
			
			if ( errors.hasError() ) {
				vaadinUi.show(errors);
				con.rollback();
				return false;
			}
			
			con.commit();
    		
			vaadinUi.showStatus(vaadinUi.getMsg("LibFileVersionForm.button.delete.successMessage",currFile.getEsfName(),currFileVer.getVersion()));

			User user = vaadinUi.getUser();
			if ( user != null ) {
				user.logConfigChange(con, "Deleted file " + currFile.getEsfName() + " version " + currFileVer.getVersion() + ". FileVersionId: " + currFileVer.getId() + "; status: " + currFile.getStatus());
				vaadinUi.getEsfapp().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " deleted file " + currFile.getEsfName() + " version " + currFileVer.getVersion() + ". FileVersionId: " + currFileVer.getId() + "; status: " + currFile.getStatus());
			}

			fileContainer.refresh();  // refresh our list after a change
			selectFile(null);
			return true;
		}
		catch(SQLException e) 
		{
			pool.rollbackIgnoreException(con,e);
			return false;
		}
		finally
		{
			vaadinUi.getEsfapp().cleanupPool(pool,con,null);
		}
	}

	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		_logger.debug("activateView() - params: " + params);
		libraryId = new EsfUUID(params);
    	buildLayout();
	}

	@Override
	public String checkDirty() {
		return fileForm.isDirty() ? fileForm.checkDirty() : fileVerForm.checkDirty();
	}

	@Override
	public void initView() {
	}

	@Override
	public boolean isDirty() {
		return fileForm.isDirty() || fileVerForm.isDirty();
	}
}