// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.record;

import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.vaadin.data.Item;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;

/**
 * The DeploymentStatsForm shows various information about versions, etc. that are running.  It's a view-only form now.
 * @author Yozons Inc.
 */
public class DeploymentStatsForm extends CustomComponent implements EsfView {
	private static final long serialVersionUID = 6387130379743082363L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(DeploymentStatsForm.class);
	
    GridLayout layout;
    FieldGroup fieldGroup;
    
    Label systemSection;
    Label webSection;
    Label databaseSection;
    
    public DeploymentStatsForm() {
    	setStyleName("DeploymentStatsForm");
    	fieldGroup = new FieldGroup();
    }
    
	DeploymentStatsBean getCurrentBean() {
    	return getBean(fieldGroup.getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public DeploymentStatsBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<DeploymentStatsBean> bi = (BeanItem<DeploymentStatsBean>)dataSource;
		return bi.getBean();
    }
    
    void setItemDataSource(Item dataSource) {
		fieldGroup.setItemDataSource(dataSource);
		setReadOnly(true);
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	if ( getCurrentBean() != null )
    		fieldGroup.setReadOnly(readOnly);    	
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
		DeploymentStatsBean bean = new DeploymentStatsBean();
		BeanItem<DeploymentStatsBean> dataSource = new BeanItem<DeploymentStatsBean>(bean);
		setItemDataSource(dataSource);
	}

	@Override
	public void initView() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	layout = new GridLayout(3,10);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setCompositionRoot(layout);
    	
		systemSection = new Label(vaadinUi.getMsg("DeploymentStatsForm.columnHeader.system.label.captionHTML"));
		systemSection.setContentMode(ContentMode.HTML);
		layout.addComponent(systemSection,0,0);
    	
		createBindTextField("systemVersion",0,1);
		createBindTextField("systemStartedTimestamp",0,2);
		createBindTextField("javaVersion",0,3);
		createBindTextField("javaMemory",0,4);
		createBindTextField("totalThreads",0,5);
		
		webSection = new Label(vaadinUi.getMsg("DeploymentStatsForm.columnHeader.web.label.captionHTML"));
		webSection.setContentMode(ContentMode.HTML);
		layout.addComponent(webSection,1,0);

		createBindTextField("webServerVersion",1,1);
		createBindTextField("vaadinVersion",1,2);
		createBindTextField("vaadinPushmode",1,3);
		createBindTextField("httpThreads",1,4);
		createBindTextField("httpsThreads",1,5);
		createBindTextField("backgrounderThread",1,6);
		createBindTextField("outboundEmailThread",1,7);
		createBindTextField("inboundEmailThread",1,8);
		createBindTextField("httpSendThread",1,9);
		
		databaseSection = new Label(vaadinUi.getMsg("DeploymentStatsForm.columnHeader.database.label.captionHTML"));
		databaseSection.setContentMode(ContentMode.HTML);
		layout.addComponent(databaseSection,2,0);
		
		createBindTextField("dbVersion",2,1);
		createBindTextField("dbMaxConnections",2,2);
		createBindTextField("dbConnections",2,3);
		createBindTextField("dbTransactionCount",2,4);
		createBindTextField("dbUserCount",2,5);
		createBindTextField("dbSize",2,6);
		createBindTextField("dbArchiveSize",2,7);

     	_logger.debug("Form created");
	}
	
	TextField createBindTextField(String propertyName, int col, int row) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		TextField tf = new TextField(vaadinUi.getMsg("DeploymentStatsForm."+propertyName+".label"));
		tf.setDescription(vaadinUi.getMsg("DeploymentStatsForm."+propertyName+".tooltip"));
		tf.setReadOnly(true);
		tf.setWidth("40ex");
		layout.addComponent(tf, col, row);
		fieldGroup.bind(tf, propertyName);
		return tf;
	}

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("DeploymentPropertiesForm.ConfirmDiscardChangesDialog.message"); // Don't really care about this now since page has no update capability (yet?)
	}

	@Override
	public boolean isDirty() {
		return fieldGroup.isModified();
	}
}