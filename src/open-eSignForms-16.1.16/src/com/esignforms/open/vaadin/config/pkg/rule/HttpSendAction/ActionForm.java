// Copyright (C) 2013-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.HttpSendAction;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.integration.httpsend.HttpSendRequest;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentIdPartyId;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PackageVersionPartyTemplate;
import com.esignforms.open.prog.PackageVersionPartyTemplateDocumentParty;
import com.esignforms.open.runtime.action.HttpSendAction;
import com.esignforms.open.runtime.action.HttpSendAction.Spec;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.config.libraryProgramming.field.TypeSubForm;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.HttpUrlValidator;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.FieldSpecTextField;
import com.esignforms.open.vaadin.widget.ListSelectValid;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.Not;
import com.vaadin.event.dd.acceptcriteria.SourceIsTarget;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.dd.VerticalDropLocation;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Table;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table.TableDragMode;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;

/**
 * Handles the configuration of the HttpSendAction.
 * @author Yozons Inc.
 */
public class ActionForm extends CustomComponent implements EsfView, ClickListener {
	private static final long serialVersionUID = 2244803925667368994L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ActionForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;

	VerticalLayout layout;
	VerticalLayout formLayout;
	FieldGroup fieldGroup;
	final FieldView fieldView;
	
	final PackageVersion duplicatedPackageVersion;
	final HttpSendAction duplicatedAction;
	final ActionView view;
	final ActionContainer container;
	
	Spec newSpec; // when set, we're working on a new spec.
	
	
	public ActionForm(ActionView view, final ActionContainer container, PackageVersion duplicatedPackageVersionParam, HttpSendAction duplicatedActionParam)	{
		setStyleName("HttpSendActionForm");
		this.view = view;
		this.container = container;
		this.duplicatedPackageVersion = duplicatedPackageVersionParam;
		this.duplicatedAction = duplicatedActionParam;
		this.fieldGroup = new FieldGroup();
		
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
    	// Setup layout
    	layout = new VerticalLayout();
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setCompositionRoot(layout);
    	
    	formLayout = new VerticalLayout();
    	formLayout.setMargin(false);
    	formLayout.setSpacing(true);
    	formLayout.setVisible(false);
    	layout.addComponent(formLayout);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	fieldGroup.setBuffered(true);
    	
    	// Line 1
    	HorizontalLayout line1 = new HorizontalLayout();
    	line1.setWidth(100, Unit.PERCENTAGE);
    	line1.setMargin(false);
    	line1.setSpacing(true);
    	formLayout.addComponent(line1);

    	NativeSelect requestMethod = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.requestMethod.label"));
    	requestMethod.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.requestMethod.tooltip"));
    	requestMethod.setNullSelectionAllowed(false);
    	requestMethod.setRequired(true);
    	requestMethod.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	requestMethod.setImmediate(true);
    	requestMethod.addValidator(new SelectValidator(requestMethod));
    	requestMethod.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	requestMethod.addItem(HttpSendRequest.REQUEST_METHOD_POST);
    	requestMethod.setItemCaption(HttpSendRequest.REQUEST_METHOD_POST, vaadinUi.getPrettyCode().httpSendActionRequestMethod(HttpSendRequest.REQUEST_METHOD_POST));
    	requestMethod.addItem(HttpSendRequest.REQUEST_METHOD_GET);
    	requestMethod.setItemCaption(HttpSendRequest.REQUEST_METHOD_GET, vaadinUi.getPrettyCode().httpSendActionRequestMethod(HttpSendRequest.REQUEST_METHOD_GET));
    	line1.addComponent(requestMethod);
    	fieldGroup.bind(requestMethod, "requestMethod");
    	line1.setExpandRatio(requestMethod, 0f);
    	
    	FieldSpecTextField url = new FieldSpecTextField(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.url.label"));
    	url.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.url.tooltip"));
    	url.setInputPrompt(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.url.inputPrompt"));
    	url.setWidth(95, Unit.PERCENTAGE);
    	url.setRequired(true);
    	url.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	url.addValidator(new HttpUrlValidator());
    	url.setImmediate(true);
    	url.setNullSettingAllowed(false);
    	url.setNullRepresentation("");
    	line1.addComponent(url);
    	fieldGroup.bind(url, "url");
    	line1.setExpandRatio(url, 0.8f);
    	
    	NativeSelect maxAttempts = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.maxAttempts.label"));
    	maxAttempts.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.maxAttempts.tooltip"));
    	maxAttempts.setNullSelectionAllowed(false);
    	maxAttempts.setRequired(true);
    	maxAttempts.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	maxAttempts.setImmediate(true);
    	maxAttempts.addValidator(new SelectValidator(maxAttempts));
    	maxAttempts.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	for( int i=1; i <= 10; ++i ) {
    		maxAttempts.addItem(i);
    		maxAttempts.setItemCaption(i, Integer.toString(i));
    	}
    	line1.addComponent(maxAttempts);
    	line1.setExpandRatio(maxAttempts, 0f);
    	maxAttempts.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = 2312034318183075739L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				NativeSelect retryDelayMinutes = (NativeSelect)fieldGroup.getField("retryDelayMinutes");
				if ( retryDelayMinutes != null ) {
					Object valueObj = event.getProperty().getValue();
					if ( valueObj != null ) {
						int value = (int)valueObj;
						retryDelayMinutes.setEnabled(value > 1);
					}
				}
			}
    	});
    	
    	NativeSelect retryDelayMinutes = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.retryDelayMinutesSelect.label"));
    	retryDelayMinutes.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.retryDelayMinutesSelect.tooltip"));
    	retryDelayMinutes.setNullSelectionAllowed(false);
    	retryDelayMinutes.setRequired(true);
    	retryDelayMinutes.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	retryDelayMinutes.setImmediate(true);
    	retryDelayMinutes.addValidator(new SelectValidator(retryDelayMinutes));
    	retryDelayMinutes.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	for( int i=1; i <= 60; ++i ) {
    		retryDelayMinutes.addItem(i);
    		retryDelayMinutes.setItemCaption(i, Integer.toString(i));
    	}
    	line1.addComponent(retryDelayMinutes);
    	fieldGroup.bind(retryDelayMinutes, "retryDelayMinutes");
    	fieldGroup.bind(maxAttempts, "maxAttempts"); // bind after retry delay so changes can enable/disable
    	line1.setExpandRatio(retryDelayMinutes, 0f);
    	
    	// Line 2
    	HorizontalLayout line2 = new HorizontalLayout();
    	line2.setWidth(100, Unit.PERCENTAGE);
    	line2.setMargin(false);
    	line2.setSpacing(true);
    	formLayout.addComponent(line2);
    	
    	TextField limitSuccessfulHttpStatusCodes = new TextField(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.limitSuccessfulHttpStatusCodes.label"));
    	limitSuccessfulHttpStatusCodes.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.limitSuccessfulHttpStatusCodes.tooltip"));
    	limitSuccessfulHttpStatusCodes.setRequired(false);
    	limitSuccessfulHttpStatusCodes.setWidth(95, Unit.PERCENTAGE);
    	limitSuccessfulHttpStatusCodes.setNullSettingAllowed(true);
    	limitSuccessfulHttpStatusCodes.setNullRepresentation("");
    	limitSuccessfulHttpStatusCodes.setMaxLength(50); // some small constraint on valid status codes
    	limitSuccessfulHttpStatusCodes.setImmediate(true);
    	line2.addComponent(limitSuccessfulHttpStatusCodes);
    	line2.setExpandRatio(limitSuccessfulHttpStatusCodes, 0.4f);
    	fieldGroup.bind(limitSuccessfulHttpStatusCodes, "limitSuccessfulHttpStatusCodes");
    	
    	FieldSpecTextField reasonSpec = new FieldSpecTextField(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.reasonSpec.label"));
    	reasonSpec.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.reasonSpec.tooltip"));
    	reasonSpec.setRequired(true);
    	reasonSpec.setWidth(95, Unit.PERCENTAGE);
    	reasonSpec.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	reasonSpec.setNullSettingAllowed(false);
    	reasonSpec.setNullRepresentation("");
    	reasonSpec.setImmediate(true);
    	line2.addComponent(reasonSpec);
    	fieldGroup.bind(reasonSpec, "reasonSpec");
    	line2.setExpandRatio(reasonSpec, 0.6f);
    	
    	// Line 3
    	HorizontalLayout line3 = new HorizontalLayout();
    	line3.setWidth(100, Unit.PERCENTAGE);
    	line3.setMargin(false);
    	line3.setSpacing(true);
    	formLayout.addComponent(line3);

    	NativeSelect responseMatchType = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.responseMatchType.label"));
    	responseMatchType.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.responseMatchType.tooltip"));
    	responseMatchType.setNullSelectionAllowed(false);
    	responseMatchType.setRequired(true);
    	responseMatchType.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	responseMatchType.setImmediate(true);
    	responseMatchType.addValidator(new SelectValidator(responseMatchType));
    	responseMatchType.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	responseMatchType.addItem(HttpSendRequest.RESPONSE_MATCH_TYPE_ANY);
    	responseMatchType.setItemCaption(HttpSendRequest.RESPONSE_MATCH_TYPE_ANY, vaadinUi.getPrettyCode().httpSendResponseMatchType(HttpSendRequest.RESPONSE_MATCH_TYPE_ANY));
    	responseMatchType.addItem(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_CONTAIN);
    	responseMatchType.setItemCaption(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_CONTAIN, vaadinUi.getPrettyCode().httpSendResponseMatchType(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_CONTAIN));
    	responseMatchType.addItem(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_NOT_CONTAIN);
    	responseMatchType.setItemCaption(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_NOT_CONTAIN, vaadinUi.getPrettyCode().httpSendResponseMatchType(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_NOT_CONTAIN));
    	responseMatchType.addItem(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_START_WITH);
    	responseMatchType.setItemCaption(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_START_WITH, vaadinUi.getPrettyCode().httpSendResponseMatchType(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_START_WITH));
    	responseMatchType.addItem(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_NOT_START_WITH);
    	responseMatchType.setItemCaption(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_NOT_START_WITH, vaadinUi.getPrettyCode().httpSendResponseMatchType(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_NOT_START_WITH));
    	responseMatchType.addItem(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_MATCH_REGEX);
    	responseMatchType.setItemCaption(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_MATCH_REGEX, vaadinUi.getPrettyCode().httpSendResponseMatchType(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_MATCH_REGEX));
    	responseMatchType.addItem(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_NOT_MATCH_REGEX);
    	responseMatchType.setItemCaption(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_NOT_MATCH_REGEX, vaadinUi.getPrettyCode().httpSendResponseMatchType(HttpSendRequest.RESPONSE_MATCH_TYPE_MUST_NOT_MATCH_REGEX));
    	line3.addComponent(responseMatchType);
    	line3.setExpandRatio(responseMatchType, 0f);
    	responseMatchType.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = 2312034318183075739L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				TextField responseMatchValue = (TextField)fieldGroup.getField("responseMatchValue");
				if ( responseMatchValue != null )
				{
					String value = (String)event.getProperty().getValue();
					responseMatchValue.setEnabled(! HttpSendRequest.RESPONSE_MATCH_TYPE_ANY.equals(value));
				}
			}
    	});
    	
    	TextField responseMatchValue = new TextField(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.responseMatchValue.label"));
    	responseMatchValue.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.responseMatchValue.tooltip"));
    	responseMatchValue.setRequired(false);
    	responseMatchValue.setWidth(95, Unit.PERCENTAGE);
    	responseMatchValue.setNullSettingAllowed(true);
    	responseMatchValue.setNullRepresentation("");
    	line3.addComponent(responseMatchValue);
    	fieldGroup.bind(responseMatchValue, "responseMatchValue");
    	fieldGroup.bind(responseMatchType, "responseMatchType"); // bind after so value field exists before type
    	line3.setExpandRatio(responseMatchValue, 1.0f);
    	
    	fieldView = new FieldView(duplicatedPackageVersion);
    	formLayout.addComponent( fieldView );

    	// Footer buttons
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setMargin(false);
    	footer.setSpacing(true);
    	layout.addComponent(footer);
   	
    	saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    	footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	_logger.debug("Form created");

	}
	
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		if ( fieldGroup.getItemDataSource() == null ) {
    			saveSpecs();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! fieldGroup.isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
        	
        	try {
        		fieldGroup.commit();
    			fieldView.fieldSpecList.reorder(); // ensure we're in order as we save

    			if ( newSpec != null ) {
        			newSpec.setFieldSpecs( fieldView.getFieldSpecs() );
        			container.addItem(newSpec);
        			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.message.spec.name")) );
        			newSpec = null;
        		} else {
        			getCurrentBean().setFieldSpecs(fieldView.getFieldSpecs());
        			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.message.spec.name")) );
        		}

        		saveSpecs();
        		
        		view.select(getCurrentBean()); // reselect our bean so all updates cleanly
        	} catch( FieldGroup.CommitException e ) {
        		_logger.error("buttonClick() fieldGroup.commit()",e);
        	}
        } else if ( source == cancelButton ) {
    		fieldGroup.discard();
    		if ( fieldGroup.getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			setNewSpecAsDataSource(null);
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	view.closeParentWindow();
        } else if ( source == createLikeButton ) {
        	fieldGroup.discard();
        	Spec spec = getCurrentBean();
        	if ( spec != null ) {
    			view.unselectAll();
    			setNewSpecAsDataSource(spec.duplicate());
        	}
        } else if ( source == deleteButton ) {
        	fieldGroup.discard();
        	
        	if ( newSpec != null ) {
        		newSpec = null;
        	} else {
            	Spec spec = getCurrentBean();
            	if ( spec != null ) {
            		container.removeItem(spec);
        			saveSpecs();
            	}
        	}
    		view.unselectAll();
			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.form.message.spec.name")) );
        }
    }
	
	void saveSpecs() {
		view.specListUpdated();
		fieldView.setFieldSpecListChanged(false);
	}
	
	Spec getCurrentBean() {
    	return getBean(fieldGroup.getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public Spec getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<Spec> bi = (BeanItem<Spec>)dataSource;
		return bi.getBean();
    }
    
    public void setNewSpecAsDataSource(Spec spec) {
    	newSpec = spec;
    	if ( newSpec == null ) {
    		setItemDataSource(null);
    	} else {
    		setItemDataSource(new BeanItem<Spec>(newSpec));
    	}
    }
    
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
		fieldGroup.setItemDataSource(newDataSource);
		if ( newDataSource != null ) {
			formLayout.setVisible(true);
			fieldView.setFieldSpecs(getCurrentBean().getFieldSpecs());
		} else {
			formLayout.setVisible(false);
			fieldView.clearFieldSpecs();
		}
		setReadOnly(isReadOnly());
		fieldView.setFieldSpecListChanged(false);
    }

    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	Spec bean = getCurrentBean();
    	
    	if ( bean != null ) {
    		fieldGroup.setReadOnly(readOnly);
    		fieldView.setReadOnly(readOnly);
    	}
    	
    	saveButton.setVisible(!readOnly && bean != null);
    	cancelButton.setVisible(!readOnly && bean != null);
    	closeButton.setVisible(true);
    	createLikeButton.setVisible(!readOnly && bean != null && newSpec == null);
    	deleteButton.setVisible(!readOnly && bean != null);
    }
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.view.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return fieldGroup.isModified() || newSpec != null || fieldView.isFieldSpecListChanged();
	}
	
	
	//////// FieldView has the buttons and Table for setting up fields
	
	static class FieldView extends Panel {
		private static final long serialVersionUID = 3079066351973627711L;

		final FieldView thisFieldView;
		final PackageVersion duplicatedPackageVersion;
		FieldSpecList fieldSpecList;
		HorizontalLayout buttonLayout;
		boolean fieldSpecsChanged;
		
		public FieldView(PackageVersion duplicatedPackageVersionParam) {
			super();
			this.thisFieldView = this;
			this.duplicatedPackageVersion = duplicatedPackageVersionParam;
			fieldSpecList = new FieldSpecList(this);
			setStyleName("HttpSendActionFieldView");
			setWidth(100, Unit.PERCENTAGE);
			
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			
			VerticalLayout layout = new VerticalLayout();
			setContent(layout);
			layout.setWidth(100, Unit.PERCENTAGE);
			layout.setSpacing(true);
			layout.setMargin(false);
			
			buttonLayout = new HorizontalLayout();
			buttonLayout.setWidth(-1, Unit.PIXELS);
			buttonLayout.setStyleName("buttonLayout");
			buttonLayout.setMargin(false);
			buttonLayout.setSpacing(true);

			layout.addComponent(buttonLayout);
			layout.setComponentAlignment(buttonLayout, Alignment.MIDDLE_RIGHT);
			layout.addComponent(fieldSpecList);
			
			Button addPartyLinkButton = new Button(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldView.button.addPartyLinks.label"), new ClickListener() {
				private static final long serialVersionUID = 4881582642035643997L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					PartyLinksSelector pls = new PartyLinksSelector(duplicatedPackageVersion,thisFieldView);
					Window w = new Window(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.PartyLinksSelector.window.caption"));
					w.center();
					w.setWidth(400, Unit.PIXELS);
					w.setHeight(300, Unit.PIXELS);
					w.setModal(true);
					w.setContent(pls);
					vaadinUi.addWindowToUI(w);
				}
			});
			addPartyLinkButton.addStyleName(Reindeer.BUTTON_SMALL);
			buttonLayout.addComponent(addPartyLinkButton);
			
			Button addCompletedDocumentsButton = new Button(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldView.button.addCompletedDocuments.label"), new ClickListener() {
				private static final long serialVersionUID = 4881582642035643997L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					CompletedDocumentsSelector cds = new CompletedDocumentsSelector(duplicatedPackageVersion,thisFieldView);
					Window w = new Window(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.CompletedDocumentsSelector.window.caption"));
					w.center();
					w.setWidth(600, Unit.PIXELS);
					w.setHeight(400, Unit.PIXELS);
					w.setModal(true);
					w.setContent(cds);
					vaadinUi.addWindowToUI(w);
				}
			});
			addCompletedDocumentsButton.addStyleName(Reindeer.BUTTON_SMALL);
			buttonLayout.addComponent(addCompletedDocumentsButton);
			
			Button addDocumentFieldsButton = new Button(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldView.button.addDocumentFields.label"), new ClickListener() {
				private static final long serialVersionUID = 4881582642035643997L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					DocumentFieldsSelector dfs = new DocumentFieldsSelector(duplicatedPackageVersion,thisFieldView);
					Window w = new Window(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.DocumentFieldsSelector.window.caption"));
					w.center();
					w.setWidth(600, Unit.PIXELS);
					w.setHeight(600, Unit.PIXELS);
					w.setModal(true);
					w.setContent(dfs);
					vaadinUi.addWindowToUI(w);
				}
			});
			addDocumentFieldsButton.addStyleName(Reindeer.BUTTON_SMALL);
			buttonLayout.addComponent(addDocumentFieldsButton);
			
			Button addLiteralButton = new Button(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldView.button.addLiteral.label"), new ClickListener() {
				private static final long serialVersionUID = 4881582642035643997L;

				@Override
				public void buttonClick(ClickEvent event) {
					int order = fieldSpecList.getNumFieldSpecs()+1;
					Spec.FieldSpec fs = new Spec.FieldSpec(order, Spec.FieldSpec.TYPE_LITERAL, "param"+order, "", 
							                               null, null, 
							                               null, null, null, null,
							                               "value"+order);
					fieldSpecList.addFieldSpec(fs);
					fieldSpecList.setPageLength();
				}
			});
			addLiteralButton.addStyleName(Reindeer.BUTTON_SMALL);
			buttonLayout.addComponent(addLiteralButton);
		}
		
		public boolean isReadOnly() {
			return fieldSpecList.getDragMode() == TableDragMode.NONE;
		}
		public void setReadOnly(boolean readOnly) {
			// We want our table to be responsive to clicks, but no buttons to control entries.
			buttonLayout.setVisible(! readOnly);
			fieldSpecList.setDragMode( readOnly ? TableDragMode.NONE : TableDragMode.ROW);
		}
		
		public List<Spec.FieldSpec> getFieldSpecs() {
			return fieldSpecList.getFieldSpecs();
		}
		public void setFieldSpecs(List<Spec.FieldSpec> list) {
			fieldSpecList.setFieldSpecs(list);
		}
		public void clearFieldSpecs() {
			fieldSpecList.removeAllItems();
		}
		public boolean isFieldSpecListChanged() { return fieldSpecsChanged; }
		public void setFieldSpecListChanged(boolean v) { fieldSpecsChanged = v; }
	}
	
	
	//////// FieldSpecList is the table of fields in a FieldView

	static class FieldSpecList extends Table {
		private static final long serialVersionUID = 757874180935129908L;

		final FieldSpecList thisList;
		final FieldView fieldView;
		final FieldListContainer fieldListContainer;
		
		public FieldSpecList(final FieldView fieldView) {
			super();
			thisList = this;
			this.fieldView = fieldView;
			this.fieldListContainer = new FieldListContainer();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(fieldListContainer);
			setPageLength(); // initial size
			setVisibleColumns((Object[])vaadinUi.getStringArray("PackageProgrammingRule.HttpSendAction.FieldSpecList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("PackageProgrammingRule.HttpSendAction.FieldSpecList.showColumnHeaders"));
			setSortEnabled(false);
			setColumnAlignment("order", Align.CENTER);
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);

	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener( new ValueChangeListener() {
				private static final long serialVersionUID = -8138218256294918563L;

				@Override
				public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
					Spec.FieldSpec fs = (Spec.FieldSpec)event.getProperty().getValue();
					// Find the matching Item so we can display/edit its value
					if ( fs != null ) {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						FieldEditor fieldEditor = new FieldEditor(fieldView,fieldListContainer.getItem(fs));
						Window w = new Window(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.window.caption"));
						w.center();
						w.setWidth(600, Unit.PIXELS);
						w.setHeight(400, Unit.PIXELS);
						w.setModal(true);
						w.setContent(fieldEditor);
						vaadinUi.addWindowToUI(w);
						thisList.select(null);
					}
				}
	        });
	        setNullSelectionAllowed(true);
			setWidth(100, Unit.PERCENTAGE);
			
			initializeDND();
		}
		
		public int getNumFieldSpecs() {
			return fieldListContainer.size();
		}
		public List<Spec.FieldSpec> getFieldSpecs() {
			return new LinkedList<Spec.FieldSpec>(fieldListContainer.getItemIds()); // Return our own list so it doesn't change with the table's container
		}
		public void setFieldSpecs(List<Spec.FieldSpec> fieldSpecList) {
			fieldListContainer.refresh(fieldSpecList);
			setPageLength();
		}
		public void addFieldSpec(Spec.FieldSpec fieldSpec) {
			fieldListContainer.addBean(fieldSpec);
            fieldView.setFieldSpecListChanged(true);
            setPageLength();
		}
		public void removeFieldSpec(Spec.FieldSpec fieldSpec) {
			fieldListContainer.removeItem(fieldSpec);
            fieldView.setFieldSpecListChanged(true);
            setPageLength();
		}
		public void setPageLength() {
			setPageLength( Math.min(10, getNumFieldSpecs()) );
		}

		@SuppressWarnings("unchecked")
		public void reorder() {
            // Fix up the order now
			LinkedList<Spec.FieldSpec> orderList = new LinkedList<Spec.FieldSpec>();
            int order = 1;
            for( Spec.FieldSpec fs : fieldListContainer.getItemIds() ) {
            	Item item = fieldListContainer.getItem(fs);
            	item.getItemProperty("order").setValue(order); // suppressing warning
            	fs.setOrder(order++);
            	orderList.add(getBean(item));
            }
            fieldListContainer.refresh(orderList);
            fieldView.setFieldSpecListChanged(true);
		}
		
	    @SuppressWarnings("unchecked")
		public Spec.FieldSpec getBean(Item item) {
	    	if ( item == null )
	    		return null;
			BeanItem<Spec.FieldSpec> bi = (BeanItem<Spec.FieldSpec>)item;
			return bi.getBean();
	    }

	    public void initializeDND() {
			setDropHandler(new DropHandler() {
				private static final long serialVersionUID = -5167876260099621531L;

				public void drop(DragAndDropEvent dropEvent) {
	                DataBoundTransferable t = (DataBoundTransferable)dropEvent.getTransferable();
	                
	                Container sourceContainer = t.getSourceContainer();
	                if ( sourceContainer == fieldListContainer ) {
	                	// reordering within the table rows
	                	Spec.FieldSpec sourceItemId = (Spec.FieldSpec)t.getItemId();

		                AbstractSelectTargetDetails dropData = ((AbstractSelectTargetDetails)dropEvent.getTargetDetails());
		                Spec.FieldSpec targetItemId = (Spec.FieldSpec)dropData.getItemIdOver();
		                
		                // No move if source and target are the same
		                if ( sourceItemId == targetItemId )
		                	return;
		                
		                // Let's remove the source of the drag so we can add it back where requested...
		                fieldListContainer.removeItem(sourceItemId);
		                if ( targetItemId == null ) {
		                	fieldListContainer.addItem(sourceItemId);
		                } else if ( dropData.getDropLocation() == VerticalDropLocation.BOTTOM ) {
		                	fieldListContainer.addItemAfter(targetItemId,sourceItemId);
		            	} else {
		                    Object prevItemId = fieldListContainer.prevItemId(targetItemId);
		                    fieldListContainer.addItemAfter(prevItemId, sourceItemId);
		            	}
		                
		                reorder();
	                }
	            }

	            public AcceptCriterion getAcceptCriterion() {
	                return new And(SourceIsTarget.get(), new Not(AbstractSelect.VerticalLocationIs.MIDDLE)); // dragging within the table
	            }
	        });	
		}

		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // FieldSpecList

	
	//////// FieldListContainer is the container for FieldSpecList table

	static class FieldListContainer extends BeanItemContainer<Spec.FieldSpec> implements Serializable {
		private static final long serialVersionUID = 1894083596572435645L;

		public FieldListContainer() {
			super(Spec.FieldSpec.class);
		}
		
		public void refresh(List<Spec.FieldSpec> fieldSpecList) {
			removeAllItems();
			for( Spec.FieldSpec fs : fieldSpecList ) {
				addItem(fs);
			}
		}
	}
	
	
	////////FieldEditor is a simple Form to make changes to a field FieldSpec
	
	static class FieldEditor extends CustomComponent {
		private static final long serialVersionUID = -4214283225610980426L;

		VerticalLayout layout;
		final FieldEditor thisFieldEditor;
		final FieldView  fieldView;
		final FieldGroup fieldGroup;
		final BeanItem<Spec.FieldSpec> fieldSpecItem;
		
		// Used for DocumentField only
		List<EsfUUID> allDocumentIds;
		List<EsfName> allDocumentNames;
		NativeSelect outputFormatSelect;

		public FieldEditor(FieldView fieldViewParam, BeanItem<Spec.FieldSpec> fieldSpecItemParam) {
			this.thisFieldEditor = this;
			this.fieldView = fieldViewParam;
			this.fieldSpecItem = fieldSpecItemParam;
			
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			
	    	// Setup layout
	    	this.layout = new VerticalLayout();
	    	layout.setMargin(true);
	    	layout.setSpacing(true);
	    	setCompositionRoot(layout);
	    	
			fieldGroup = new FieldGroup();
			fieldGroup.setBuffered(true);
			
			// DocumentField types, we'll need the full document list available from the package.
	    	if ( fieldSpecItem.getBean().isTypeDocumentField() ) {
		    	allDocumentIds = new LinkedList<EsfUUID>(fieldView.duplicatedPackageVersion.getDocumentIdList());
		    	allDocumentNames = new LinkedList<EsfName>();
		    	for( EsfUUID docId : allDocumentIds ) {
		    		Document doc = Document.Manager.getById(docId);
		    		if ( doc == null ) {
		    			_logger.warn("FieldEditor() - Could not find document with id: " + docId);
		    			allDocumentNames.add(docId.toEsfName());
		    		} else {
		    			allDocumentNames.add(doc.getEsfName());
		    		}
		    	}
	    	}
			
			// Based on the field spec's type, set up the fields accordingly
			setFormForType(vaadinUi, fieldSpecItem.getBean());
			fieldGroup.setItemDataSource(fieldSpecItem);
			
	    	// Footer buttons
	    	HorizontalLayout footer = new HorizontalLayout();
	    	footer.setStyleName("footer");
	    	footer.setMargin(false);
	    	footer.setSpacing(true);
	    	layout.addComponent(footer);
			
			if ( fieldView.isReadOnly() ) {
				fieldGroup.setReadOnly(true);
		    	Button closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), new ClickListener() {
					private static final long serialVersionUID = 4287361525158266773L;

					@Override
					public void buttonClick(ClickEvent event) {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						vaadinUi.getWindow(thisFieldEditor).close();
					}
		    	});
		    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
		    	footer.addComponent(closeButton);
			} else {
		    	Button okButton = new Button(vaadinUi.getMsg("button.ok.label"), new ClickListener() {
					private static final long serialVersionUID = 4287361525158266773L;

					@Override
					public void buttonClick(ClickEvent event) {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						try {
				        	if ( ! fieldGroup.isValid() ) {
				    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
				        		return;
				        	}
							fieldGroup.commit();
							fieldView.setFieldSpecListChanged(true);
							vaadinUi.getWindow(thisFieldEditor).close();
						} catch( FieldGroup.CommitException e) {
							_logger.error("FieldEditor OK buttonClick() fieldGroup.commit()",e);
						}
					}
		    		
		    	});
		    	okButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		    	okButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
		    	footer.addComponent(okButton);

		    	Button cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), new ClickListener() {
					private static final long serialVersionUID = -4351252162090961460L;

					@Override
					public void buttonClick(ClickEvent event) {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						fieldGroup.discard();
						vaadinUi.getWindow(thisFieldEditor).close();
					}
		    		
		    	});
		    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
		    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
		    	footer.addComponent(cancelButton);
		    	
		    	Button deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), new ClickListener() {
					private static final long serialVersionUID = -700413677441007915L;

					@Override
					public void buttonClick(ClickEvent event) {
						EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
						fieldGroup.discard();
						fieldView.fieldSpecList.removeFieldSpec(fieldSpecItem.getBean());
						vaadinUi.getWindow(thisFieldEditor).close();
					}
		    		
		    	});
		    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
		    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
		    	deleteButton.addStyleName("deleteButton");
		    	deleteButton.addStyleName("caution");
		    	footer.addComponent(deleteButton);
		    	
		    	if ( fieldSpecItem.getBean().isTypeCompletedDocument() ) {
		    		Field<?> f = fieldGroup.getField("completedDocumentType");
		    		if ( f != null )
		    			f.setReadOnly(true);
		    	}
			}
	    	
	    	_logger.debug("FieldEditor created");
		}
		
		void setupAllFieldNames(EsfUUID newDocId) {
			NativeSelect fieldSelect = (NativeSelect)fieldGroup.getField("documentFieldName");
			if ( fieldSelect == null )
				return;
			
			EsfName prevSelectedDocumentFieldName = (EsfName)fieldSelect.getValue();
			
			fieldSelect.removeAllItems();
			
			Document document = Document.Manager.getById(newDocId);
			if ( document == null )
				return; 
			DocumentVersion docVersion;
			com.esignforms.open.prog.Package pkg = fieldView.duplicatedPackageVersion.getPackage();
			if ( pkg.getProductionVersion() == fieldView.duplicatedPackageVersion.getVersion() )
				docVersion = document.getProductionDocumentVersion();
			else
				docVersion = document.getTestDocumentVersion();
			
			for( EsfName fieldName : docVersion.getFieldTemplateMap().keySet() ) {
				FieldTemplate ft = docVersion.getFieldTemplate(fieldName);
				// We can't post all types of fields
				if ( ! ft.isTypeFileConfirmClick() && ! ft.isTypeRadioButton() ) {
					fieldSelect.addItem(fieldName);
				}
			}
			
			if ( prevSelectedDocumentFieldName != null && prevSelectedDocumentFieldName.isValid() ) {
				try {
					if ( fieldSelect.containsId(prevSelectedDocumentFieldName) )// reset value back if its valid for our new select list
						fieldSelect.setValue(prevSelectedDocumentFieldName); 
					else
						fieldSelect.setValue(null);
					fieldSelect.commit(); // don't let this cause the field to be marked as modified; ignore exceptions
				} catch( Exception e ) {}
			}
		}

		
		void setFormForType(final EsfVaadinUI vaadinUi, final Spec.FieldSpec fieldSpec) {
			if ( fieldSpec.isTypeLiteral() ) {
				TextField paramName = new TextField(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.paramName.label"));
				paramName.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.paramName.tooltip"));
				paramName.setRequired(true);
				paramName.setRequiredError(vaadinUi.getMsg("tooltip.required"));
				paramName.setWidth(95, Unit.PERCENTAGE);
				layout.addComponent(paramName);
				fieldGroup.bind(paramName, "paramName");
				
				FieldSpecTextField literal = new FieldSpecTextField(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.literal.label"));
				literal.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.literal.tooltip"));
				literal.setRequired(true);
				literal.setRequiredError(vaadinUi.getMsg("tooltip.required"));
				literal.setWidth(95, Unit.PERCENTAGE);
				layout.addComponent(literal);
				fieldGroup.bind(literal, "literal");
			} else if ( fieldSpec.isTypeDocumentField() ) {
		    	NativeSelect fieldSelect = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.DocumentFieldsSelector.fieldsSelect.label"));
		    	fieldSelect.setNullSelectionAllowed(false);
		    	fieldSelect.setRequired(true);
		    	fieldSelect.setRequiredError(vaadinUi.getMsg("tooltip.required"));
		    	fieldSelect.setImmediate(true);
		    	fieldSelect.addValidator(new SelectValidator(fieldSelect));
		    	fieldSelect.addValueChangeListener( new ValueChangeListener() {
					private static final long serialVersionUID = 5127769004590437909L;

					@Override
					public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
						EsfName selectedField = (EsfName)event.getProperty().getValue();
						
						NativeSelect documentIdSelect = (NativeSelect)fieldGroup.getField("documentId");
						EsfUUID selectedDocumentId = (EsfUUID)documentIdSelect.getValue();
						
						Document document = Document.Manager.getById(selectedDocumentId);
						if ( document != null ) {
							DocumentVersion docVersion;
							com.esignforms.open.prog.Package pkg = fieldView.duplicatedPackageVersion.getPackage();
							if ( pkg.getProductionVersion() == fieldView.duplicatedPackageVersion.getVersion() )
								docVersion = document.getProductionDocumentVersion();
							else
								docVersion = document.getTestDocumentVersion();
							
							outputFormatSelect.removeAllItems();
							outputFormatSelect.setVisible(false);
							
							FieldTemplate ft = docVersion.getFieldTemplate(selectedField);
							if ( ft != null ) {
								NativeSelect boundOutputFormatSelect = (NativeSelect)fieldGroup.getField("outputFormat");
								if ( boundOutputFormatSelect != null ) // if we are already bound, remove that binding now
									fieldGroup.unbind(boundOutputFormatSelect);
								NativeSelect ns = TypeSubForm.createOutputFormatFieldNativeSelect(ft.getType());
								if ( ns != null ) {
									ns.setRequired(false); // For us, they don't have to set a format, but they can
									ns.setNullSelectionAllowed(true);
									vaadinUi.removeAllValidators(ns);
									layout.replaceComponent(outputFormatSelect,ns);
									fieldGroup.bind(ns, "outputFormat");
									outputFormatSelect = ns;
								}
							}
						}
					}
		        });

	        	NativeSelect documentSelect = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.DocumentFieldsSelector.documentSelect.label"));
	        	documentSelect.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.DocumentFieldsSelector.documentSelect.tooltip"));
	        	documentSelect.setNullSelectionAllowed(false);
	        	documentSelect.setRequired(true);
	        	documentSelect.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	        	documentSelect.setImmediate(true);
	        	documentSelect.addValidator(new SelectValidator(documentSelect));
	        	documentSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
	        	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
	        	for( EsfUUID docId : allDocumentIds ) {
	        		documentSelect.addItem(docId);
	        		documentSelect.setItemCaption(docId, nameIter.next().toString());
	        	}
	        	documentSelect.addValueChangeListener( new ValueChangeListener() {
					private static final long serialVersionUID = -7459693961297295417L;

					@Override
					public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
						EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
						if ( newDocId != null && ! newDocId.isNull() ) {
							setupAllFieldNames(newDocId);
						}
					}
	        	});

				TextField paramName = new TextField(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.paramName.label"));
				paramName.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.paramName.tooltip"));
				paramName.setRequired(true);
				paramName.setRequiredError(vaadinUi.getMsg("tooltip.required"));
				paramName.setWidth(95, Unit.PERCENTAGE);
				layout.addComponent(paramName);
				fieldGroup.bind(paramName, "paramName");
				
				//TextField documentName = new TextField(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.documentName.label"));
				//documentName.setReadOnly(true);
				//documentName.setWidth(95, Unit.PERCENTAGE);
				layout.addComponent(documentSelect);
				fieldGroup.bind(documentSelect, "documentId");
				
				//TextField documentFieldName = new TextField(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.documentFieldName.label"));
				//documentFieldName.setReadOnly(true);
				//documentFieldName.setWidth(95, Unit.PERCENTAGE);
				layout.addComponent(fieldSelect);
				fieldGroup.bind(fieldSelect, "documentFieldName");
				
				// Bind a placeholder NativeSelect that we'll swap out based on which document+field is being worked on
				outputFormatSelect = new NativeSelect();
				outputFormatSelect.setVisible(false);
				layout.addComponent( new Label(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.outputFormat.notice.label")) );
				layout.addComponent(outputFormatSelect);
				// We don't bind now to avoid rebuilding this select based on the actual field type in the value change listeners
				//fieldGroup.bind(outputFormatSelect, "outputFormat"); 
			} else if ( fieldSpec.isTypeCompletedDocument() ) {
				TextField paramName = new TextField(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.paramName.label"));
				paramName.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.paramName.tooltip"));
				paramName.setRequired(true);
				paramName.setRequiredError(vaadinUi.getMsg("tooltip.required"));
				paramName.setWidth(95, Unit.PERCENTAGE);
				layout.addComponent(paramName);
				fieldGroup.bind(paramName, "paramName");
				
				ListSelect documentIdPartyIdSelect = new ListSelect(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.completedDocumentNames.label"));
				documentIdPartyIdSelect.setNullSelectionAllowed(false);
				documentIdPartyIdSelect.setReadOnly(true);
				for( int i=0; i < fieldSpec.getCompletedDocumentIds().length; ++i ) {
					Document document = Document.Manager.getById(fieldSpec.getCompletedDocumentIds()[i]);
					if ( document != null ) {
						documentIdPartyIdSelect.addItem(document.getEsfName()+"("+fieldSpec.getCompletedDocumentPartyNames()[i]+")");
					}
				}
	        	documentIdPartyIdSelect.setRows( Math.max(fieldSpec.getCompletedDocumentIds().length,2) );
	        	layout.addComponent(documentIdPartyIdSelect);

		    	NativeSelect completedDocumentTypeSelect = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.CompletedDocumentsSelector.completedDocumentTypeSelect.label"));
		    	completedDocumentTypeSelect.setReadOnly(true);
		    	completedDocumentTypeSelect.setNullSelectionAllowed(false);
		    	completedDocumentTypeSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		    	String[] types = { 	HttpSendAction.Spec.FieldSpec.COMPLETED_DOCUMENT_TYPE_HTML, 
		    						HttpSendAction.Spec.FieldSpec.COMPLETED_DOCUMENT_TYPE_PDF,
		    						HttpSendAction.Spec.FieldSpec.COMPLETED_DOCUMENT_TYPE_XML_DATA,
		    						HttpSendAction.Spec.FieldSpec.COMPLETED_DOCUMENT_TYPE_XML_SNAPSHOTS 
		    					 };
		    	for( String type : types ) {
		    		completedDocumentTypeSelect.addItem(type);
		    		completedDocumentTypeSelect.setItemCaption(type, vaadinUi.getPrettyCode().completedDocumentType(type));
		    	}
	        	layout.addComponent(completedDocumentTypeSelect);
		    	fieldGroup.bind(completedDocumentTypeSelect, "completedDocumentType");
			} else if ( fieldSpec.isTypePartyLink() ) {
				TextField paramName = new TextField(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.paramName.label"));
				paramName.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.paramName.tooltip"));
				paramName.setRequired(true);
				paramName.setRequiredError(vaadinUi.getMsg("tooltip.required"));
				paramName.setWidth(95, Unit.PERCENTAGE);
				layout.addComponent(paramName);
				fieldGroup.bind(paramName, "paramName");
				
				ListSelect partyNameSelect = new ListSelect(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.FieldEditor.partyNames.label"));
				partyNameSelect.setNullSelectionAllowed(false);
				partyNameSelect.setReadOnly(true);
				for( int i=0; i < fieldSpec.getCompletedDocumentPartyNames().length; ++i ) {
					partyNameSelect.addItem(fieldSpec.getCompletedDocumentPartyNames()[i]);
				}
				partyNameSelect.setRows( Math.max(fieldSpec.getCompletedDocumentPartyNames().length,2) );
	        	layout.addComponent(partyNameSelect);
			}
		}
	}
	
	
	
	////////DocumentFieldsSelector is a simple form to select a document and one or more fields from that document to add to the field list
	
	static class DocumentFieldsSelector extends CustomComponent {
		private static final long serialVersionUID = 2430098817545371441L;

		VerticalLayout layout;
		final DocumentFieldsSelector thisDocumentFieldsSelector;
		final PackageVersion duplicatedPackageVersion;
		final FieldView  fieldView;
		
		List<EsfUUID> allDocumentIds;
		List<EsfName> allDocumentNames;

		final NativeSelect documentSelect;
		final ListSelect fieldsSelect;
		final Button addButton;
	
		public DocumentFieldsSelector(PackageVersion duplicatedPackageVersionParam, FieldView fieldViewParam) {
			this.thisDocumentFieldsSelector = this;
			this.duplicatedPackageVersion = duplicatedPackageVersionParam;
			this.fieldView = fieldViewParam;
			
	    	allDocumentIds = new LinkedList<EsfUUID>(duplicatedPackageVersion.getDocumentIdList());
	    	allDocumentNames = new LinkedList<EsfName>();
	    	for( EsfUUID docId : allDocumentIds ) {
	    		Document doc = Document.Manager.getById(docId);
	    		if ( doc == null ) {
	    			_logger.warn("DocumentFieldsSelector() - Could not find document with id: " + docId);
	    			allDocumentNames.add(docId.toEsfName());
	    		} else {
	    			allDocumentNames.add(doc.getEsfName());
	    		}
	    	}

			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			
	    	// Setup layout
	    	this.layout = new VerticalLayout();
	    	layout.setMargin(true);
	    	layout.setSpacing(true);
	    	setCompositionRoot(layout);
	    	
	    	HorizontalLayout line1 = new HorizontalLayout();
	    	line1.setMargin(false);
	    	line1.setSpacing(true);
	    	layout.addComponent(line1);
	    	
	    	fieldsSelect = new ListSelect(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.DocumentFieldsSelector.fieldsSelect.label"));
	    	fieldsSelect.setNullSelectionAllowed(false);
	    	fieldsSelect.setMultiSelect(true);
	    	fieldsSelect.setRequired(true);
	    	fieldsSelect.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	    	fieldsSelect.setImmediate(true);
	    	fieldsSelect.addValidator(new SelectValidator(fieldsSelect));
	    	fieldsSelect.addValueChangeListener( new ValueChangeListener() {
				private static final long serialVersionUID = -4294813124798823342L;

				@Override
				public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
					@SuppressWarnings("unchecked")
					HashSet<EsfName> selectedFields = new HashSet<EsfName>((Collection<EsfName>)event.getProperty().getValue());
					addButton.setEnabled(selectedFields != null && selectedFields.size() > 0);
				}
	        });
	    	
        	documentSelect = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.DocumentFieldsSelector.documentSelect.label"));
        	documentSelect.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.DocumentFieldsSelector.documentSelect.tooltip"));
        	documentSelect.setNullSelectionAllowed(false);
        	documentSelect.setRequired(true);
        	documentSelect.setRequiredError(vaadinUi.getMsg("tooltip.required"));
        	documentSelect.setImmediate(true);
        	documentSelect.addValidator(new SelectValidator(documentSelect));
        	documentSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
        	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
        	for( EsfUUID docId : allDocumentIds ) {
        		documentSelect.addItem(docId);
        		documentSelect.setItemCaption(docId, nameIter.next().toString());
        	}
        	documentSelect.addValueChangeListener( new ValueChangeListener() {
				private static final long serialVersionUID = -4294813124798823342L;

				@Override
				public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
					EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
					if ( newDocId != null && ! newDocId.isNull() ) {
						setupAllFieldNames(newDocId);
					}
				}
        	});
        	if ( allDocumentIds.size() > 0 )
        		documentSelect.setValue(allDocumentIds.get(0));
        	line1.addComponent(documentSelect);
        	line1.addComponent(fieldsSelect);
        	
	    	// Footer buttons
	    	HorizontalLayout footer = new HorizontalLayout();
	    	footer.setStyleName("footer");
	    	footer.setMargin(false);
	    	footer.setSpacing(true);
	    	layout.addComponent(footer);
        	
	    	addButton = new Button(vaadinUi.getMsg("button.add.label"), new ClickListener() {
				private static final long serialVersionUID = 4890023921166622438L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					
					EsfUUID selectedDocumentId = (EsfUUID)documentSelect.getValue();
					@SuppressWarnings("unchecked")
					HashSet<EsfName> selectedFields = new HashSet<EsfName>((Collection<EsfName>)fieldsSelect.getValue()); 
					
					if ( selectedDocumentId == null || selectedDocumentId.isNull() || selectedFields.size() == 0 ) {
						Errors errors = new Errors();
						errors.addError(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.DocumentFieldsSelector.error.nothingSelected"));
						vaadinUi.show(errors);
					} else {
						// Let's add them in
						int order = fieldView.fieldSpecList.getNumFieldSpecs();
						for( EsfName fieldName : selectedFields ) {
							++order;
							Spec.FieldSpec fs = new Spec.FieldSpec(order, Spec.FieldSpec.TYPE_DOCUMENT_FIELD, fieldName.toPlainString(), "", 
																   selectedDocumentId, fieldName, 
																   null, null, null, null,
																   null);
							fieldView.fieldSpecList.addFieldSpec(fs);
						}
						fieldView.fieldSpecList.setPageLength();
						vaadinUi.getWindow(thisDocumentFieldsSelector).close();
					}
				}
	    	});
	    	addButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.add.icon")));
	    	addButton.setEnabled(false);
	    	footer.addComponent(addButton);

	    	Button cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), new ClickListener() {
				private static final long serialVersionUID = -254528214472086229L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					vaadinUi.getWindow(thisDocumentFieldsSelector).close();
				}
	    	});
	    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
	    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
	    	footer.addComponent(cancelButton);
	    	
	    	_logger.debug("DocumentFieldsSelector created");
		}
		
		void setupAllFieldNames(EsfUUID newDocId) {
			fieldsSelect.removeAllItems();
			
			Document document = Document.Manager.getById(newDocId);
			if ( document == null )
				return; 
			DocumentVersion docVersion;
			com.esignforms.open.prog.Package pkg = duplicatedPackageVersion.getPackage();
			if ( pkg.getProductionVersion() == duplicatedPackageVersion.getVersion() )
				docVersion = document.getProductionDocumentVersion();
			else
				docVersion = document.getTestDocumentVersion();
			
			for( EsfName fieldName : docVersion.getFieldTemplateMap().keySet() ) {
				FieldTemplate ft = docVersion.getFieldTemplate(fieldName);
				// We can't post all types of fields
				if ( ! ft.isTypeFileConfirmClick() && ! ft.isTypeRadioButton() ) {
					fieldsSelect.addItem(fieldName);
				}
			}
			
	    	fieldsSelect.setRows( Math.min(20, fieldsSelect.size()) );
		}
	} // end DocumentFieldsSelector

	
	////////CompletedDocumentsSelector is a simple form to select one or more completed documents
	
	static class CompletedDocumentsSelector extends CustomComponent {
		private static final long serialVersionUID = 7805001894474227071L;

		VerticalLayout layout;
		final CompletedDocumentsSelector thisCompletedDocumentsSelector;
		final PackageVersion duplicatedPackageVersion;
		final FieldView  fieldView;
		
		List<DocumentIdPartyId> allDocAndParty;

		final TextField paramName;
		final ListSelect documentIdPartyIdSelect;
		final NativeSelect completedDocumentTypeSelect;
		final Button addButton;
	
		public CompletedDocumentsSelector(PackageVersion duplicatedPackageVersionParam, FieldView fieldViewParam) {
			this.thisCompletedDocumentsSelector = this;
			this.duplicatedPackageVersion = duplicatedPackageVersionParam;
			this.fieldView = fieldViewParam;
			
			allDocAndParty = new LinkedList<DocumentIdPartyId>();
			for ( EsfUUID docId : duplicatedPackageVersion.getDocumentIdList() ) {
	    		Document doc = Document.Manager.getById(docId);
	    		if ( doc == null ) {
	    			_logger.warn("ActionForm() - Could not find document with id: " + docId + "; not including in our list");
	    			continue;
	    		}
	    		DocumentIdPartyId dp = new DocumentIdPartyId(docId,null);
				dp.documentName = doc.getEsfName();
				allDocAndParty.add(dp); // this is the "latest document" entry, not tied to a particular party.
				for( PackageVersionPartyTemplate pvpt : duplicatedPackageVersion.getPackageVersionPartyTemplateList() ) {
					for( PackageVersionPartyTemplateDocumentParty mapping : pvpt.getPackageVersionPartyTemplateDocumentParties() ) {
						if ( mapping.isDocumentPartyTemplateView() || mapping.isDocumentPartyTemplateEsfReportsAccess() )
							continue; // this document mapping is not to a particular party, so we ignore for our list
						if ( mapping.getDocumentId().equals(docId) ) {
							PackageVersionPartyTemplate matchingParty = duplicatedPackageVersion.getPackageVersionPartyTemplateById(mapping.getPackageVersionPartyTemplateId());
							if ( matchingParty == null ) {
				    			_logger.warn("ActionForm() - Could not find matching party for document id: " + docId + "; packageVersionPartyTemplateId: " + mapping.getPackageVersionPartyTemplateId() + "; not including in our list");
				    			continue;
							} else if ( matchingParty.isEsfReportsAccess() ) {
								continue;
							}
							dp = new DocumentIdPartyId(docId,doc.getEsfName(),mapping.getPackageVersionPartyTemplateId(),matchingParty.getEsfName());
							allDocAndParty.add(dp);
						}
					}
				}
			}
			
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			
	    	// Setup layout
	    	this.layout = new VerticalLayout();
	    	layout.setMargin(true);
	    	layout.setSpacing(true);
	    	setCompositionRoot(layout);
	    	
	    	HorizontalLayout line1 = new HorizontalLayout();
	    	line1.setMargin(false);
	    	line1.setSpacing(true);
	    	layout.addComponent(line1);
	    	
        	String partyLatest = vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.CompletedDocumentsSelector.documentIdPartyIdSet.latestParty");

        	documentIdPartyIdSelect = new ListSelectValid(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.CompletedDocumentsSelector.documentIdPartyIdSet.label"));
        	documentIdPartyIdSelect.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.CompletedDocumentsSelector.documentIdPartyIdSet.tooltip"));
        	documentIdPartyIdSelect.setNullSelectionAllowed(false);
        	documentIdPartyIdSelect.setRequired(true);
        	documentIdPartyIdSelect.setImmediate(true);
        	documentIdPartyIdSelect.setMultiSelect(true);
        	documentIdPartyIdSelect.setRows( Math.min(5, allDocAndParty.size()));
        	documentIdPartyIdSelect.addValidator(new SelectValidator(documentIdPartyIdSelect));
        	documentIdPartyIdSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
        	documentIdPartyIdSelect.addValueChangeListener( new ValueChangeListener() {
				private static final long serialVersionUID = -6620553071027856897L;

				@Override
				public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
					@SuppressWarnings("unchecked")
					java.util.Set<DocumentIdPartyId> selectedDocumentIdPartyIds = (java.util.Set<DocumentIdPartyId>)event.getProperty().getValue();
					addButton.setEnabled( selectedDocumentIdPartyIds != null && selectedDocumentIdPartyIds.size() > 0 );
				}
            });
        	for( DocumentIdPartyId docAndParty : allDocAndParty ) {
        		documentIdPartyIdSelect.addItem(docAndParty);
        		String partyName = docAndParty.partyName == null ? partyLatest : docAndParty.partyName.toString();
        		documentIdPartyIdSelect.setItemCaption(docAndParty, docAndParty.documentName.toString() + " (" + partyName + ")");
        	}
        	line1.addComponent(documentIdPartyIdSelect);
        	
        	VerticalLayout column2 = new VerticalLayout();
        	column2.setMargin(false);
        	column2.setSpacing(true);
        	line1.addComponent(column2);
        	
	    	paramName = new TextField(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.CompletedDocumentsSelector.paramName.label"));
			paramName.setRequired(true);
			paramName.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	    	paramName.setValue("document");
	    	column2.addComponent(paramName);
        	
	    	completedDocumentTypeSelect = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.CompletedDocumentsSelector.completedDocumentTypeSelect.label"));
	    	completedDocumentTypeSelect.setNullSelectionAllowed(false);
	    	completedDocumentTypeSelect.setRequired(true);
	    	completedDocumentTypeSelect.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	    	completedDocumentTypeSelect.addValidator(new SelectValidator(completedDocumentTypeSelect));
	    	completedDocumentTypeSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
	    	String[] types = { 	HttpSendAction.Spec.FieldSpec.COMPLETED_DOCUMENT_TYPE_HTML, 
	    							HttpSendAction.Spec.FieldSpec.COMPLETED_DOCUMENT_TYPE_PDF,
	    							HttpSendAction.Spec.FieldSpec.COMPLETED_DOCUMENT_TYPE_XML_DATA,
	    							HttpSendAction.Spec.FieldSpec.COMPLETED_DOCUMENT_TYPE_XML_SNAPSHOTS 
	    						 };
	    	for( String type : types ) {
	    		completedDocumentTypeSelect.addItem(type);
	    		completedDocumentTypeSelect.setItemCaption(type, vaadinUi.getPrettyCode().completedDocumentType(type));
	    	}
	    	completedDocumentTypeSelect.setValue(HttpSendAction.Spec.FieldSpec.COMPLETED_DOCUMENT_TYPE_HTML);
	    	column2.addComponent(completedDocumentTypeSelect);
	    	
	    	// Footer buttons
	    	HorizontalLayout footer = new HorizontalLayout();
	    	footer.setStyleName("footer");
	    	footer.setMargin(false);
	    	footer.setSpacing(true);
	    	layout.addComponent(footer);
        	
	    	addButton = new Button(vaadinUi.getMsg("button.add.label"), new ClickListener() {
				private static final long serialVersionUID = -6426657261636706982L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

					@SuppressWarnings("unchecked")
					java.util.Set<DocumentIdPartyId> selectedDocumentIdPartyIds = (java.util.Set<DocumentIdPartyId>)documentIdPartyIdSelect.getValue();
					String type = (String)completedDocumentTypeSelect.getValue();
					
					if ( selectedDocumentIdPartyIds == null || selectedDocumentIdPartyIds.size() == 0 ) {
						Errors errors = new Errors();
						errors.addError(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.CompletedDocumentsSelector.documentIdPartyIdSet.error.nothingSelected"));
						vaadinUi.show(errors);
					} else {
						// Let's add them in
						int order = fieldView.fieldSpecList.getNumFieldSpecs() + 1;

						String nameOfParam = paramName.getValue();
						if ( EsfString.isBlank(nameOfParam) )
							nameOfParam = "document";

						EsfUUID[] completedDocumentIds = new EsfUUID[selectedDocumentIdPartyIds.size()];
						EsfUUID[] completedDocumentPartyIds = new EsfUUID[selectedDocumentIdPartyIds.size()];
						String[] completedDocumentPartyNames = new String[selectedDocumentIdPartyIds.size()];
						
						int i = 0;
						for( DocumentIdPartyId documentIdPartyId : selectedDocumentIdPartyIds ) {
							completedDocumentIds[i] = documentIdPartyId.getDocumentId();
							completedDocumentPartyIds[i] = documentIdPartyId.hasPartyId() ? documentIdPartyId.getPartyId() : new EsfUUID("");
							completedDocumentPartyNames[i] = documentIdPartyId.hasPartyName() ? documentIdPartyId.getPartyName().toString() : vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.CompletedDocumentsSelector.documentIdPartyIdSet.latestParty");
							++i;
						}
						
						Spec.FieldSpec fs = new Spec.FieldSpec(order, Spec.FieldSpec.TYPE_COMPLETED_DOCUMENT, nameOfParam, "", 
								   							   null, null, 
								   							   completedDocumentIds, completedDocumentPartyIds, completedDocumentPartyNames, type, 
								   							   null);
						
						fieldView.fieldSpecList.addFieldSpec(fs);
						fieldView.fieldSpecList.setPageLength();
						vaadinUi.getWindow(thisCompletedDocumentsSelector).close();
					}
				}
	    	});
	    	addButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.add.icon")));
	    	addButton.setEnabled(false);
	    	footer.addComponent(addButton);

	    	Button cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), new ClickListener() {
				private static final long serialVersionUID = 6885458949916746828L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					vaadinUi.getWindow(thisCompletedDocumentsSelector).close();
				}
	    	});
	    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
	    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
	    	footer.addComponent(cancelButton);
	    	
	    	_logger.debug("CompletedDocumentsSelector created");
		}
		
	} // end CompletedDocumentsSelector

	
	
	
	//////// PartyLinksSelector is a simple form to select one or more parties
	
	static class PartyLinksSelector extends CustomComponent {
		private static final long serialVersionUID = -4556887785462922789L;

		VerticalLayout layout;
		final PartyLinksSelector thisPartyLinksSelector;
		final PackageVersion duplicatedPackageVersion;
		final FieldView  fieldView;
		
		List<PackageVersionPartyTemplate> allPartiesList;
		
		final TextField paramName;
		final ListSelect partyIdSelect;
		final Button addButton;
	
		public PartyLinksSelector(PackageVersion duplicatedPackageVersionParam, FieldView fieldViewParam) {
			this.thisPartyLinksSelector = this;
			this.duplicatedPackageVersion = duplicatedPackageVersionParam;
			this.fieldView = fieldViewParam;
			
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
			
	    	// Setup layout
	    	this.layout = new VerticalLayout();
	    	layout.setMargin(true);
	    	layout.setSpacing(true);
	    	setCompositionRoot(layout);
	    	
	    	HorizontalLayout line1 = new HorizontalLayout();
	    	line1.setMargin(false);
	    	line1.setSpacing(true);
	    	layout.addComponent(line1);
	    	
	    	allPartiesList = duplicatedPackageVersion.getPackageVersionPartyTemplateList();
	    	
	    	partyIdSelect = new ListSelectValid(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.PartyLinksSelector.partyIdSet.label"));
	    	partyIdSelect.setDescription(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.PartyLinksSelector.partyIdSet.tooltip"));
	    	partyIdSelect.setNullSelectionAllowed(false);
	    	partyIdSelect.setRequired(true);
	    	partyIdSelect.setImmediate(true);
	    	partyIdSelect.setMultiSelect(true);
	    	partyIdSelect.setRows( Math.min(5, allPartiesList.size()));
	    	partyIdSelect.addValidator(new SelectValidator(partyIdSelect));
	    	partyIdSelect.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
	    	partyIdSelect.addValueChangeListener( new ValueChangeListener() {
				private static final long serialVersionUID = 1136228534522469750L;

				@Override
				public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
					@SuppressWarnings("unchecked")
					java.util.Set<EsfUUID> selectedPartyIds = (java.util.Set<EsfUUID>)event.getProperty().getValue();
					addButton.setEnabled( selectedPartyIds != null && selectedPartyIds.size() > 0 );
				}
            });
        	for( PackageVersionPartyTemplate pvpt : allPartiesList ) {
        		if ( ! pvpt.isEsfReportsAccess() ) {
            		partyIdSelect.addItem(pvpt.getId());
            		partyIdSelect.setItemCaption(pvpt.getId(), pvpt.getEsfName().toString());
        		}
        	}
        	line1.addComponent(partyIdSelect);
        	
	    	paramName = new TextField(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.PartyLinksSelector.paramName.label"));
			paramName.setRequired(true);
			paramName.setRequiredError(vaadinUi.getMsg("tooltip.required"));
	    	paramName.setValue("partyLink");
	    	line1.addComponent(paramName);
        	
	    	// Footer buttons
	    	HorizontalLayout footer = new HorizontalLayout();
	    	footer.setStyleName("footer");
	    	footer.setMargin(false);
	    	footer.setSpacing(true);
	    	layout.addComponent(footer);
        	
	    	addButton = new Button(vaadinUi.getMsg("button.add.label"), new ClickListener() {
				private static final long serialVersionUID = 7737648036018892567L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

					@SuppressWarnings("unchecked")
					java.util.Set<EsfUUID> selectedPartyIds = (java.util.Set<EsfUUID>)partyIdSelect.getValue();
					
					if ( selectedPartyIds == null || selectedPartyIds.size() == 0 ) {
						Errors errors = new Errors();
						errors.addError(vaadinUi.getMsg("PackageProgrammingRule.HttpSendAction.PartyLinksSelector.partyIdSet.error.nothingSelected"));
						vaadinUi.show(errors);
					} else {
						// Let's add them in
						int order = fieldView.fieldSpecList.getNumFieldSpecs() + 1;

						String nameOfParam = paramName.getValue();
						if ( EsfString.isBlank(nameOfParam) )
							nameOfParam = "partyLink";

						EsfUUID[] completedDocumentPartyIds = new EsfUUID[selectedPartyIds.size()];
						String[] completedDocumentPartyNames = new String[selectedPartyIds.size()];
						
						int i = 0;
						for( EsfUUID partyId : selectedPartyIds ) {
							completedDocumentPartyIds[i] = partyId;
							completedDocumentPartyNames[i] = getPartyName(partyId);
							++i;
						}
						
						Spec.FieldSpec fs = new Spec.FieldSpec(order, Spec.FieldSpec.TYPE_PARTY_LINK, nameOfParam, "", 
								   							   null, null, 
								   							   null, completedDocumentPartyIds, completedDocumentPartyNames, null, 
								   							   null);
						
						fieldView.fieldSpecList.addFieldSpec(fs);
						fieldView.fieldSpecList.setPageLength();
						vaadinUi.getWindow(thisPartyLinksSelector).close();
					}
				}
	    	});
	    	addButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.add.icon")));
	    	addButton.setEnabled(false);
	    	footer.addComponent(addButton);

	    	Button cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), new ClickListener() {
				private static final long serialVersionUID = 1055755580997136066L;

				@Override
				public void buttonClick(ClickEvent event) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					vaadinUi.getWindow(thisPartyLinksSelector).close();
				}
	    	});
	    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
	    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
	    	footer.addComponent(cancelButton);
	    	
	    	_logger.debug("PartyLinksSelector created");
		}
		
		String getPartyName(EsfUUID pid) {
        	for( PackageVersionPartyTemplate pvpt : allPartiesList ) {
        		if ( pvpt.getId().equals(pid) )
        			return pvpt.getEsfName().toString();
        	}
        	return "???";
		}
		
	} // end PartyLinksSelector

}