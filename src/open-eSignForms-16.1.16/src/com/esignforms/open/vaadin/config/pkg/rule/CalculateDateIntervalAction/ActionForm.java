// Copyright (C) 2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.pkg.rule.CalculateDateIntervalAction;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.runtime.action.CalculateDateIntervalAction;
import com.esignforms.open.runtime.action.CalculateDateIntervalAction.Spec;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.widget.FieldSpecTextField;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Field;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author Yozons Inc.
 */
public class ActionForm extends CustomComponent implements EsfView, ClickListener {
	private static final long serialVersionUID = -8007860419708057040L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(ActionForm.class);
	
	protected Button saveButton;
	protected Button createLikeButton;
	protected Button deleteButton;
	protected Button cancelButton;
	protected Button closeButton;

	VerticalLayout layout;
	VerticalLayout formLayout;
	FieldGroup fieldGroup;
	
	final PackageVersion duplicatedPackageVersion;
	final DocumentVersion duplicatedDocumentVersion;
	final CalculateDateIntervalAction duplicatedAction;
	final ActionView view;
	final ActionContainer container;
	
	List<EsfUUID> allDocumentIds;
	List<EsfName> allDocumentNames;
	
	Spec newSpec; // when set, we're working on a new spec.
	
	public ActionForm(ActionView view, final ActionContainer container, PackageVersion duplicatedPackageVersionParam, CalculateDateIntervalAction duplicatedActionParam) {
		this.view = view;
		this.container = container;
		this.duplicatedPackageVersion = duplicatedPackageVersionParam;
		this.duplicatedDocumentVersion = null;
		this.duplicatedAction = duplicatedActionParam;
		allDocumentIds = new LinkedList<EsfUUID>(duplicatedPackageVersion.getDocumentIdList());
		setupForm();
	}
	
	public ActionForm(ActionView view, final ActionContainer container, DocumentVersion duplicatedDocumentVersionParam, CalculateDateIntervalAction duplicatedActionParam)	{
		this.view = view;
		this.container = container;
		this.duplicatedPackageVersion = null;
		this.duplicatedDocumentVersion = duplicatedDocumentVersionParam;
		this.duplicatedAction = duplicatedActionParam;
		allDocumentIds = new LinkedList<EsfUUID>();
		allDocumentIds.add(duplicatedDocumentVersion.getDocumentId());
		setupForm();
	}
	
	private void setupForm() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		setStyleName("CalculateDateIntervalActionForm");
		this.fieldGroup = new FieldGroup();

		allDocumentNames = new LinkedList<EsfName>();
    	
    	for( EsfUUID docId : allDocumentIds ) {
    		Document doc = Document.Manager.getById(docId);
    		if ( doc == null ) {
    			_logger.warn("ActionForm() - Could not find document with id: " + docId);
    			allDocumentNames.add(docId.toEsfName());
    		} else {
    			allDocumentNames.add(doc.getEsfName());
    		}
    	}
	
    	// Setup layout
    	layout = new VerticalLayout();
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setCompositionRoot(layout);

    	// Setup form layout
    	formLayout = new VerticalLayout();
    	formLayout.setMargin(false);
    	formLayout.setSpacing(true);
    	formLayout.setVisible(false);
    	layout.addComponent(formLayout);
    	
    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	fieldGroup.setBuffered(true);
    	
    	HorizontalLayout line1 = new HorizontalLayout();
    	line1.setMargin(false);
    	line1.setSpacing(true);
    	formLayout.addComponent(line1);
    	
    	NativeSelect targetDocumentId = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.targetDocumentId.label"));
    	targetDocumentId.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.targetDocumentId.tooltip"));
    	targetDocumentId.setNullSelectionAllowed(false);
    	targetDocumentId.setRequired(true);
    	targetDocumentId.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	targetDocumentId.addValidator(new SelectValidator(targetDocumentId));
    	targetDocumentId.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	ListIterator<EsfName> nameIter = allDocumentNames.listIterator();
    	for( EsfUUID docId : allDocumentIds ) {
    		targetDocumentId.addItem(docId);
    		targetDocumentId.setItemCaption(docId, nameIter.next().toString());
    	}
    	targetDocumentId.setImmediate(true);
    	targetDocumentId.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = -4662482307399174205L;

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
				if ( newDocId != null && ! newDocId.isNull() ) {
					setupAllFieldNames("targetField",newDocId,false);
				}
			}
    	});
    	line1.addComponent(targetDocumentId);
    	fieldGroup.bind(targetDocumentId, "targetDocumentId");
    	
    	NativeSelect targetField = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.targetField.label"));
    	targetField.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.targetField.tooltip"));
    	targetField.setNullSelectionAllowed(false);
    	targetField.setRequired(true);
    	targetField.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	targetField.addValidator(new SelectValidator(targetField));
    	line1.addComponent(targetField);
    	fieldGroup.bind(targetField, "targetField");
    	
    	NativeSelect interval = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.interval.label"));
    	interval.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.interval.tooltip"));
    	interval.setNullSelectionAllowed(false);
    	interval.setRequired(true);
    	interval.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	interval.addValidator(new SelectValidator(interval));
    	interval.setItemCaptionMode(ItemCaptionMode.EXPLICIT);

    	interval.addItem(Literals.TIME_INTERVAL_UNIT_DAY);
    	interval.setItemCaption(Literals.TIME_INTERVAL_UNIT_DAY, 
    			vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.interval.day.label"));
    	interval.addItem(Literals.TIME_INTERVAL_UNIT_YEAR);
    	interval.setItemCaption(Literals.TIME_INTERVAL_UNIT_YEAR, 
    			vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.interval.year.label"));
    	interval.setImmediate(true);
    	line1.addComponent(interval);
    	fieldGroup.bind(interval, "interval");
    	
    	HorizontalLayout line2 = new HorizontalLayout();
    	line2.setMargin(false);
    	line2.setSpacing(true);
    	formLayout.addComponent(line2);
    	
    	NativeSelect fromDocumentId = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.fromDocumentId.label"));
    	fromDocumentId.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.fromDocumentId.tooltip"));
    	fromDocumentId.setNullSelectionAllowed(false);
    	fromDocumentId.setRequired(true);
    	fromDocumentId.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	fromDocumentId.addValidator(new SelectValidator(fromDocumentId));
    	fromDocumentId.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	nameIter = allDocumentNames.listIterator();
    	for( EsfUUID docId : allDocumentIds ) {
    		fromDocumentId.addItem(docId);
    		fromDocumentId.setItemCaption(docId, nameIter.next().toString());
    	}
    	fromDocumentId.setImmediate(true);
    	fromDocumentId.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = -2632242413900010360L;

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
				if ( newDocId != null && ! newDocId.isNull() ) {
					setupAllFieldNames("fromField",newDocId,true);
				}
			}
    	});
    	line2.addComponent(fromDocumentId);
    	fieldGroup.bind(fromDocumentId, "fromDocumentId");
    	
    	NativeSelect fromField = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.fromField.label"));
    	fromField.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.fromField.tooltip"));
    	fromField.setNullSelectionAllowed(false);
    	fromField.setRequired(true);
    	fromField.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	fromField.addValidator(new SelectValidator(fromField));
    	line2.addComponent(fromField);
    	fieldGroup.bind(fromField, "fromField");
    	
    	CheckBox useFromFieldSpec = new CheckBox(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.useFromFieldSpec.label"));
    	useFromFieldSpec.setStyleName("useFromFieldSpec");
    	useFromFieldSpec.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.useFromFieldSpec.tooltip"));
    	useFromFieldSpec.setImmediate(true);
    	useFromFieldSpec.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = 9066842995075612043L;

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				boolean isChecked = (Boolean)event.getProperty().getValue();
				setupDocumentFieldForDate("fromDocumentId","fromField","fromFieldSpec",isChecked);
			}
    	});
    	line2.addComponent(useFromFieldSpec);
    	fieldGroup.bind(useFromFieldSpec, "useFromFieldSpec");
    	
    	FieldSpecTextField fromFieldSpec = new FieldSpecTextField(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.fromFieldSpec.label"));
    	fromFieldSpec.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.fromFieldSpec.tooltip"));
    	fromFieldSpec.setRequired(true);
    	fromFieldSpec.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	fromFieldSpec.setWidth(10, Unit.EM);
    	line2.addComponent(fromFieldSpec);
    	fieldGroup.bind(fromFieldSpec, "fromFieldSpec");
    	
    	HorizontalLayout line3 = new HorizontalLayout();
    	line3.setMargin(false);
    	line3.setSpacing(true);
    	formLayout.addComponent(line3);
    	
    	NativeSelect toDocumentId = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.toDocumentId.label"));
    	toDocumentId.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.toDocumentId.tooltip"));
    	toDocumentId.setNullSelectionAllowed(false);
    	toDocumentId.setRequired(true);
    	toDocumentId.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	toDocumentId.addValidator(new SelectValidator(toDocumentId));
    	toDocumentId.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
    	nameIter = allDocumentNames.listIterator();
    	for( EsfUUID docId : allDocumentIds ) {
    		toDocumentId.addItem(docId);
    		toDocumentId.setItemCaption(docId, nameIter.next().toString());
    	}
    	toDocumentId.setImmediate(true);
    	toDocumentId.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = 7017913669411522545L;

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				EsfUUID newDocId = (EsfUUID)event.getProperty().getValue();
				if ( newDocId != null && ! newDocId.isNull() ) {
					setupAllFieldNames("toField",newDocId,true);
				}
			}
    	});
    	line3.addComponent(toDocumentId);
    	fieldGroup.bind(toDocumentId, "toDocumentId");
    	
    	NativeSelect toField = new NativeSelect(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.toField.label"));
    	toField.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.toField.tooltip"));
    	toField.setNullSelectionAllowed(false);
    	toField.setRequired(true);
    	toField.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	toField.addValidator(new SelectValidator(toField));
    	line3.addComponent(toField);
    	fieldGroup.bind(toField, "toField");
    	
    	CheckBox useToFieldSpec = new CheckBox(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.useToFieldSpec.label"));
    	useToFieldSpec.setStyleName("useToFieldSpec");
    	useToFieldSpec.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.useToFieldSpec.tooltip"));
    	useToFieldSpec.setImmediate(true);
    	useToFieldSpec.addValueChangeListener( new ValueChangeListener() {
			private static final long serialVersionUID = 1954135000059478151L;

			@Override
			public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
				boolean isChecked = (Boolean)event.getProperty().getValue();
				setupDocumentFieldForDate("toDocumentId","toField","toFieldSpec",isChecked);
			}
    	});
    	line3.addComponent(useToFieldSpec);
    	fieldGroup.bind(useToFieldSpec, "useToFieldSpec");
    	
    	FieldSpecTextField toFieldSpec = new FieldSpecTextField(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.toFieldSpec.label"));
    	toFieldSpec.setDescription(vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.toFieldSpec.tooltip"));
    	toFieldSpec.setRequired(true);
    	toFieldSpec.setRequiredError(vaadinUi.getMsg("tooltip.required"));
    	toFieldSpec.setWidth(10, Unit.EM);
    	line3.addComponent(toFieldSpec);
    	fieldGroup.bind(toFieldSpec, "toFieldSpec");

    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	layout.addComponent(footer);

		saveButton = new Button(vaadinUi.getMsg("button.ok.label"), (ClickListener)this);
		saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.ok.icon")));
		saveButton.setDescription(vaadinUi.getMsg("button.ok.tooltip"));
    	footer.addComponent(saveButton);

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);
    	
    	closeButton = new Button(vaadinUi.getMsg("button.closeWindow.label"), (ClickListener)this);
    	closeButton.setDescription(vaadinUi.getMsg("button.closeWindow.tooltip"));
    	footer.addComponent(closeButton);

    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.createLike.icon")));
    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    	footer.addComponent(createLikeButton);
    	
    	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
    	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.delete.icon")));
    	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
    	deleteButton.addStyleName("deleteButton");
    	deleteButton.addStyleName("caution");
    	footer.addComponent(deleteButton);
    	
    	_logger.debug("Form created");
	}
	
	void setupAllFieldNames(String forFieldName, EsfUUID newDocId, boolean forDateFields) {
		NativeSelect targetField = (NativeSelect)fieldGroup.getField(forFieldName);
		if ( targetField == null )
			return;
		EsfName targetFieldValue = (EsfName)targetField.getValue(); // save value before we empty the list
		targetField.removeAllItems();
		
		boolean isProduction;
		if ( duplicatedPackageVersion != null ) {
			com.esignforms.open.prog.Package pkg = duplicatedPackageVersion.getPackage();
			isProduction = pkg.getProductionVersion() == duplicatedPackageVersion.getVersion();
		} else {
			com.esignforms.open.prog.Document doc = duplicatedDocumentVersion.getDocument();
			isProduction = doc.getProductionVersion() == duplicatedDocumentVersion.getVersion();
		}

		Document document = Document.Manager.getById(newDocId);
		if ( document == null )
			return; 
		DocumentVersion docVersion;

		if ( isProduction )
			docVersion = document.getProductionDocumentVersion();
		else
			docVersion = document.getTestDocumentVersion();
		Map<EsfName,FieldTemplate> fieldTemplateMap = docVersion.getFieldTemplateMap();
		for( EsfName fieldName : fieldTemplateMap.keySet() ) {
			FieldTemplate fieldTemplate = fieldTemplateMap.get(fieldName);
			if ( forDateFields ) {
				if ( fieldTemplate.isTypeDate() || fieldTemplate.isTypeDateTime() )
					targetField.addItem(fieldName);
			} else {
				if ( fieldTemplate.isTypeInteger() )
					targetField.addItem(fieldName);
			}
		}
		if ( targetFieldValue != null && targetFieldValue.isValid() && targetField.containsId(targetFieldValue) ) {
			try {
				targetField.setValue(targetFieldValue); // reset value back
				targetField.commit(); // don't let this cause the field to be marked as modified
			} catch( Exception e ) {}
		}
	}
	
	void setupDocumentFieldForDate(String documentSelectName, String fieldSelectName, String fieldSpecTextFieldName, boolean isUseDateSpec) {
		Field<?> f = fieldGroup.getField(documentSelectName);
		if ( f != null && f instanceof NativeSelect ) {
			NativeSelect s = (NativeSelect)f;
			s.setEnabled(!isUseDateSpec);
			s.setRequired(!isUseDateSpec);
			s.setNullSelectionAllowed(isUseDateSpec);
			if ( isUseDateSpec ) {
				EsfVaadinUI.getInstance().removeAllValidators(s);
			} else {
				s.addValidator(new SelectValidator(s));
			}
		}
		f = fieldGroup.getField(fieldSelectName);
		if ( f != null && f instanceof NativeSelect ) {
			NativeSelect s = (NativeSelect)f;
			s.setEnabled(!isUseDateSpec);
			s.setRequired(!isUseDateSpec);
			if ( isUseDateSpec ) {
				EsfVaadinUI.getInstance().removeAllValidators(s);
			} else {
				s.addValidator(new SelectValidator(s));
			}
		}
		f = fieldGroup.getField(fieldSpecTextFieldName);
		if ( f != null ) {
			f.setEnabled(isUseDateSpec);
			f.setRequired(isUseDateSpec);
		}
	}
	


	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		if ( fieldGroup.getItemDataSource() == null ) {
    			saveSpecs();
    			view.closeParentWindow();
    			return;
    		}
    		
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! fieldGroup.isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
        	
        	try {
    			fieldGroup.commit();
    			
        		if ( newSpec != null ) {
        			container.addItem(newSpec);
        			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.message.spec.name")) );
        			newSpec = null;
        		} else {
        			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.message.spec.name")) );
        		}
        		
    			saveSpecs();
        		
        		view.select(getCurrentBean()); // reselect our bean so all updates cleanly
	    	} catch( FieldGroup.CommitException e ) {
	    		_logger.error("buttonClick(saveButton) fieldGroup.commit()",e);
	    	}
        } else if ( source == cancelButton ) {
    		fieldGroup.discard();
    		if ( fieldGroup.getItemDataSource() == null )
    			view.closeParentWindow();
    		else {
    			vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
    			setNewSpecAsDataSource(null);
    			view.unselectAll();
    		}
        } else if ( source == closeButton ) {
        	view.closeParentWindow();
        } else if ( source == createLikeButton ) {
        	fieldGroup.discard();
        	Spec spec = getCurrentBean();
        	if ( spec != null ) {
    			view.unselectAll();
    			setNewSpecAsDataSource(spec.duplicate());
        	}
        } else if ( source == deleteButton ) {
        	fieldGroup.discard();
        	
        	if ( newSpec != null ) {
        		newSpec = null;
        	} else {
            	Spec spec = getCurrentBean();
            	if ( spec != null ) {
            		container.removeItem(spec);
        			saveSpecs();
            	}
        	}
    		view.unselectAll();
			vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.form.message.spec.name")) );
        }
    }
	
	void saveSpecs() {
		view.specListUpdated();
	}
	
	Spec getCurrentBean() {
    	return getBean(fieldGroup.getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public Spec getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<Spec> bi = (BeanItem<Spec>)dataSource;
		return bi.getBean();
    }
    
    public void setNewSpecAsDataSource(Spec spec) {
    	newSpec = spec;
    	if ( newSpec == null )
    		setItemDataSource(null);
    	else
    		setItemDataSource(new BeanItem<Spec>(newSpec));
    }
    
    // Used when a new spec is created. We'll default the document id to the first document id our list, or a "fake" id if we have no documents.
    public EsfUUID getNewSpecDocumentId()
    {
    	if ( duplicatedPackageVersion != null )
    		return ( allDocumentIds == null || allDocumentIds.size() == 0 ) ? new EsfUUID() : allDocumentIds.get(0);
    	return duplicatedDocumentVersion.getDocumentId();
    }
    
    public void setItemDataSource(Item newDataSource) { // called when reloading a form so we can build a type-appropriate version
    	fieldGroup.setItemDataSource(newDataSource);
    	if ( newDataSource != null ) {
    		Spec spec = getBean(newDataSource);
    		setupDocumentFieldForDate("fromDocumentId","fromField","fromFieldSpec",spec.isUseFromFieldSpec());
    		setupDocumentFieldForDate("toDocumentId","toField","toFieldSpec",spec.isUseToFieldSpec());
    	}
    	formLayout.setVisible(newDataSource != null);
		setReadOnly(isReadOnly());
    }

	@Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	Spec bean = getCurrentBean();
    	
    	saveButton.setVisible(!readOnly && bean != null);
    	cancelButton.setVisible(!readOnly && bean != null);
    	closeButton.setVisible(true);
    	createLikeButton.setVisible(!readOnly && bean != null && newSpec == null);
    	deleteButton.setVisible(!readOnly && bean != null);
    }
    
    
	@Override
	public void activateView(EsfView.OpenMode mode, String param) {
	}
	
	@Override
	public void initView() {
	}

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("PackageProgrammingRule.CalculateDateIntervalAction.view.ConfirmDiscardChangesDialog.message");
	}

	@Override
	public boolean isDirty() {
		return fieldGroup.isModified() || newSpec != null;
	}
	
}