// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.transactionTemplate;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.jouni.animator.Disclosure;

import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.Package;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.UI;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.esignforms.open.vaadin.main.EsfView;
import com.esignforms.open.vaadin.validator.SelectValidator;
import com.esignforms.open.vaadin.validator.TransactionTemplateEsfPathNameValidator;
import com.esignforms.open.vaadin.widget.NativeSelectDropDown;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.TwinColSelect;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * The TransactionTemplateForm is not a true EsfView, but we do allow it to be opened in its own Window so we make use of some of that plumbing.
 * @author Yozons Inc.
 */
public class TransactionTemplateForm extends Form implements EsfView, ClickListener {
	private static final long serialVersionUID = -8295627030941576725L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(TransactionTemplateForm.class);
	
	static List<String> _orderedProperties;
    
	Button saveButton;
	Button createLikeButton;
	Button deleteButton;
	Button cancelButton;
	Button editButton;

	TransactionTemplateView view;
	TransactionTemplateViewContainer container;
    GridLayout layout;
	
	Label id;
	Label lastUpdatedTimestamp;
	Label createdByInfo;
	Label startUrls;
	
    HorizontalLayout productionRetentionLayout;
    HorizontalLayout testRetentionLayout;
    
	// We store the prevBean in case we abandon working on the newBean when we 'create like'
	TransactionTemplateBean prevBean;
    TransactionTemplateBean newBean;
    
    // We use this for all our select lists, so we only reload once per Bean being set as our datasource
	EsfUUID[] allGroupListIds;
	EsfPathName[] allGroupListNames;
	EsfUUID[] allGroupWithExternalListIds;
	EsfPathName[] allGroupWithExternalListNames;
	
	Disclosure permDisclosure;
	
	Disclosure permTransactionDisclosure;

	public TransactionTemplateForm(TransactionTemplateView view, TransactionTemplateViewContainer container) {
    	setStyleName("TransactionTemplateForm");
       	setWidth(100, Unit.PERCENTAGE); // hack because seems to have "horizontal scrollbar" issues if the form is shown with vertical scrollbars in place
    	this.view = view;
    	this.container = container;
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

    	// Enable buffering so that commit() must be called for the form before input is written to the data and we can discard() if we abandon changes
    	setBuffered(true);
    	setImmediate(true); // so our validators can run
    	
    	layout = new GridLayout(2,10);
       	layout.setWidth(100, Unit.PERCENTAGE);
    	layout.setColumnExpandRatio(0, 0.5f);
    	layout.setColumnExpandRatio(1, 0.5f);
    	layout.setMargin(true);
    	layout.setSpacing(true);
    	setLayout(layout);

		id = new Label();
		id.setContentMode(ContentMode.TEXT);
		id.setStyleName("smallInfo");
		layout.addComponent(id, 0, 0);

		lastUpdatedTimestamp = new Label();
		lastUpdatedTimestamp.setContentMode(ContentMode.TEXT);
		lastUpdatedTimestamp.setStyleName("smallInfo");
		layout.addComponent(lastUpdatedTimestamp, 1, 0);
		
        productionRetentionLayout = new HorizontalLayout();
        productionRetentionLayout.setMargin(false);
        productionRetentionLayout.setSpacing(true);
        layout.addComponent(productionRetentionLayout, 0, 2);
		
        testRetentionLayout = new HorizontalLayout();
        testRetentionLayout.setMargin(false);
        testRetentionLayout.setSpacing(true);
        layout.addComponent(testRetentionLayout, 1, 2);
		
		startUrls = new Label();
		startUrls.setContentMode(Label.CONTENT_XHTML);
		startUrls.setStyleName("smallInfo");
		layout.addComponent(startUrls, 1, 6);
		layout.setComponentAlignment(startUrls, Alignment.BOTTOM_LEFT);
		
		permDisclosure = new Disclosure(vaadinUi.getMsg("TransactionTemplateForm.permDisclosure.label"));
		permDisclosure.setWidth(100, Unit.PERCENTAGE);
		permDisclosure.setVisible(false);
		layout.addComponent(permDisclosure,0,7,1,7);
		
		permTransactionDisclosure = new Disclosure(vaadinUi.getMsg("TransactionTemplateForm.permTransactionDisclosure.label"));
		permTransactionDisclosure.setWidth(100, Unit.PERCENTAGE);
		permTransactionDisclosure.setVisible(false);
		layout.addComponent(permTransactionDisclosure,0,8,1,8);
		
		createdByInfo = new Label();
		createdByInfo.setContentMode(ContentMode.TEXT);
		createdByInfo.setStyleName("smallInfo");
		layout.addComponent(createdByInfo,0,9,1,9);
		
    	HorizontalLayout footer = new HorizontalLayout();
    	footer.setStyleName("footer");
    	footer.setSpacing(false);
    	footer.setMargin(false);
    	footer.setVisible(false);

		User loggedInUser = vaadinUi.getUser();
		UI ui = new UI();
		if ( ui.showSaveButton(loggedInUser, UI.UI_TRANSACTION_TEMPLATE_VIEW) ) {
			saveButton = new Button(vaadinUi.getMsg("button.save.label"), (ClickListener)this);
			//saveButton.setStyleName(Reindeer.BUTTON_DEFAULT);
			//saveButton.setClickShortcut(KeyCode.ENTER);
			saveButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.save.icon")));
	    	saveButton.setDescription(vaadinUi.getMsg("button.save.tooltip"));
        	footer.addComponent(saveButton);
		}

    	cancelButton = new Button(vaadinUi.getMsg("button.cancel.label"), (ClickListener)this);
    	cancelButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.cancel.icon")));
    	cancelButton.setDescription(vaadinUi.getMsg("button.cancel.tooltip"));
    	footer.addComponent(cancelButton);

    	if ( ui.canUpdate(loggedInUser, UI.UI_TRANSACTION_TEMPLATE_VIEW) ) {
        	editButton = new Button(vaadinUi.getMsg("button.edit.label"), (ClickListener)this);
        	editButton.setIcon(new ThemeResource(vaadinUi.getMsg("TransactionTemplateForm.button.edit.icon")));
        	editButton.setDescription(vaadinUi.getMsg("button.edit.tooltip"));
    		footer.addComponent(editButton);
    	}

    	if ( ui.canCreateLike(loggedInUser, UI.UI_TRANSACTION_TEMPLATE_VIEW) ) {
	    	createLikeButton = new Button(vaadinUi.getMsg("button.createLike.label"), (ClickListener)this);
	    	createLikeButton.setIcon(new ThemeResource(vaadinUi.getMsg("TransactionTemplateForm.button.createLike.icon")));
	    	createLikeButton.setDescription(vaadinUi.getMsg("button.createLike.tooltip"));
    		footer.addComponent(createLikeButton);
		}

    	if ( ui.canDelete(loggedInUser, UI.UI_TRANSACTION_TEMPLATE_VIEW) ) {
        	deleteButton = new Button(vaadinUi.getMsg("button.delete.label"), (ClickListener)this);
        	deleteButton.setIcon(new ThemeResource(vaadinUi.getMsg("TransactionTemplateForm.button.delete.icon")));
        	deleteButton.setDescription(vaadinUi.getMsg("button.delete.tooltip"));
        	deleteButton.addStyleName("deleteButton");
        	deleteButton.addStyleName("caution");
    		footer.addComponent(deleteButton);
    	}
    	
    	setFooter(footer);
    	
    	setFormFieldFactory( new DefaultFieldFactory() {
			private static final long serialVersionUID = 1234276039390451932L;

			@Override
			public Field createField(Item item, Object propertyId, Component uiContext) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				// Load our cache if we don't have it yet since we'll need it for all our permission selection boxes
				if ( allGroupListIds == null ) {
					TransactionTemplateBean bean = getCurrentBean();
					allGroupListIds = bean.allGroupListIds();
					allGroupListNames = bean.allGroupListNames();
					allGroupWithExternalListIds = bean.allGroupWithExternalListIds();
					allGroupWithExternalListNames = bean.allGroupWithExternalListNames();
				}

				// Status will be an option group (radio buttons and not a text field)
				if ( propertyId.equals("productionStatus") ) {
					// Create the radio buttons to have the two possible values.
					OptionGroup statusOptionGroup = new OptionGroup();
					statusOptionGroup.addItem(Literals.STATUS_ENABLED);
					statusOptionGroup.addItem(Literals.STATUS_DISABLED);
					// Associate different labels/captions to those values
					statusOptionGroup.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					statusOptionGroup.setItemCaption(Literals.STATUS_ENABLED, vaadinUi.getMsg("TransactionTemplateForm.production.status.enabled"));
					statusOptionGroup.setItemCaption(Literals.STATUS_DISABLED, vaadinUi.getMsg("TransactionTemplateForm.production.status.disabled"));
					statusOptionGroup.setStyleName("inline");
					return statusOptionGroup;
				}
					
				if ( propertyId.equals("testStatus") ) {
					// Create the radio buttons to have the two possible values.
					OptionGroup statusOptionGroup = new OptionGroup();
					statusOptionGroup.addItem(Literals.STATUS_ENABLED);
					statusOptionGroup.addItem(Literals.STATUS_DISABLED);
					// Associate different labels/captions to those values
					statusOptionGroup.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					statusOptionGroup.setItemCaption(Literals.STATUS_ENABLED, vaadinUi.getMsg("TransactionTemplateForm.test.status.enabled"));
					statusOptionGroup.setItemCaption(Literals.STATUS_DISABLED, vaadinUi.getMsg("TransactionTemplateForm.test.status.disabled"));
					statusOptionGroup.setStyleName("inline");
					return statusOptionGroup;
				}
					
				if ( propertyId.equals("permListIds") ) {
					return createPermissionSelectBox("TransactionTemplateForm.permList.label");
				} 
				
				if ( propertyId.equals("permViewDetailsIds") ) {
					return createPermissionSelectBox("TransactionTemplateForm.permViewDetails.label");
				}
				
				if ( propertyId.equals("permCreateLikeIds") ) {
					return createPermissionSelectBox("TransactionTemplateForm.permCreateLike.label");
				}
				
				if ( propertyId.equals("permUpdateIds") ) {
					return createPermissionSelectBox("TransactionTemplateForm.permUpdate.label");
				}
				
				if ( propertyId.equals("permDeleteIds") ) {
					return createPermissionSelectBox("TransactionTemplateForm.permDelete.label");
				}
				
				if ( propertyId.equals("permTransactionStartIds") ) {
					return createPermissionWithExternalSelectBox("TransactionTemplateForm.permTransactionStart.label");
				} 
				
				if ( propertyId.equals("permTransactionCancelIds") ) {
					return createPermissionSelectBox("TransactionTemplateForm.permTransactionCancel.label");
				} 
				
				if ( propertyId.equals("permTransactionReactivateIds") ) {
					return createPermissionSelectBox("TransactionTemplateForm.permTransactionReactivate.label");
				} 
				
				if ( propertyId.equals("permTransactionSuspendIds") ) {
					return createPermissionSelectBox("TransactionTemplateForm.permTransactionSuspend.label");
				} 
				
				if ( propertyId.equals("permTransactionResumeIds") ) {
					return createPermissionSelectBox("TransactionTemplateForm.permTransactionResume.label");
				} 
				
				if ( propertyId.equals("permTransactionUseUpdateApiIds") ) {
					return createPermissionSelectBox("TransactionTemplateForm.permTransactionUseUpdateApi.label");
				} 
				
				if ( propertyId.equals("permTransactionUseAdminApiIds") ) {
					return createPermissionSelectBox("TransactionTemplateForm.permTransactionUseAdminApi.label");
				} 
				
				if ( propertyId.equals("packageId") ) {
					NativeSelect select = new NativeSelect(vaadinUi.getMsg("TransactionTemplateForm.packageId.label"));
					select.setNullSelectionAllowed(false); 
					select.setImmediate(false);
					select.setRequired(true);
					select.setMultiSelect(false);
			        select.addValidator(new SelectValidator(select));
					select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					Collection<Package> packageList = Package.Manager.getForUserWithPermission(vaadinUi.getUser(), PermissionOption.PERM_OPTION_LIST);
		        	for( Package pkg : packageList ) {
		        		if ( pkg.isEnabled() ) {
			        		select.addItem(pkg.getId());
			        		select.setItemCaption( pkg.getId(), pkg.getPathName().toString() );
		        		}
		        	}
                	return select;
				}
				
				if ( propertyId.equals("brandLibraryId") ) {
					NativeSelect select = new NativeSelect(vaadinUi.getMsg("TransactionTemplateForm.brandLibraryId.label"));
					select.setNullSelectionAllowed(true); 
					select.setImmediate(false);
					select.setRequired(false);
					select.setMultiSelect(false);
			        select.addValidator(new SelectValidator(select));
					select.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
					Collection<Library> libraryList = Library.Manager.getForUserWithListPermission(vaadinUi.getUser(), Library.INCLUDE.ONLY_ENABLED);
		        	for( Library library : libraryList ) {
		        		select.addItem(library.getId());
		        		select.setItemCaption( library.getId(), library.getPathName().toString() );
		        	}
                	return select;
				}
				
				if ( propertyId.equals("comments") ) {
	            	TextArea ta = new TextArea();
	            	ta.setWidth(100, Unit.PERCENTAGE);
	            	ta.setNullRepresentation("");
	            	ta.setRows(3);
	            	ta.setInputPrompt(vaadinUi.getMsg("inputPrompt.comments"));
	            	ta.setCaption(vaadinUi.getMsg("caption.comments"));
	            	ta.setDescription(vaadinUi.getMsg("tooltip.comments"));
	            	/*
	            	ta.addFocusListener( new FieldEvents.FocusListener() {
						private static final long serialVersionUID = 4180404707078909705L;

						@Override
						public void focus(FocusEvent event) {
							if ( saveButton != null )
								saveButton.removeClickShortcut();
						}
	            	});
	            	ta.addBlurListener( new FieldEvents.BlurListener() {
						private static final long serialVersionUID = 6205893859875398173L;

						@Override
						public void blur(BlurEvent event) {
							if ( saveButton != null )
								saveButton.setClickShortcut(KeyCode.ENTER);
						}
					});
					*/
	            	return ta;
				}
				
				if (propertyId.equals("productionRetentionTimeIntervalUnits")) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("TransactionTemplateForm.productionRetentionTimeIntervalUnits.label"), vaadinUi.getEsfapp().getDropDownTimeIntervalUnitsEsfName());
					select.setRequired(true);
					select.setDescription(vaadinUi.getMsg("TransactionTemplateForm.productionRetentionTimeIntervalUnits.tooltip"));
			        select.addValidator(new SelectValidator(select));
			        select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -998465047790261576L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							Field f = getField("productionRetentionNumTimeIntervalUnits");
							if ( f != null ) {
								String value = (String)event.getProperty().getValue();
								boolean isNowOrForever = Literals.TIME_INTERVAL_UNIT_FOREVER.equals(value) || Literals.TIME_INTERVAL_UNIT_NOW.equals(value);
								f.setVisible(! isNowOrForever);
								EsfInteger v = new EsfInteger((String)f.getValue());
								if ( v.isNull() || v.isLessThan(1) || v.isGreaterThan(9999) )
									f.setValue("1");
							}
						}
			        	
			        });
	                return select;
				}
				
				if (propertyId.equals("testRetentionTimeIntervalUnits")) {
					NativeSelectDropDown select = new NativeSelectDropDown(vaadinUi.getMsg("TransactionTemplateForm.testRetentionTimeIntervalUnits.label"), vaadinUi.getEsfapp().getDropDownTimeIntervalUnitsEsfName());
					select.setRequired(true);
	                select.setDescription(vaadinUi.getMsg("TransactionTemplateForm.testRetentionTimeIntervalUnits.tooltip"));
			        select.addValidator(new SelectValidator(select));
			        select.addValueChangeListener( new ValueChangeListener() {
						private static final long serialVersionUID = -3225882040820064779L;

						@Override
						public void valueChange(com.vaadin.data.Property.ValueChangeEvent event) {
							Field f = getField("testRetentionNumTimeIntervalUnits");
							if ( f != null ) {
								String value = (String)event.getProperty().getValue();
								boolean isNowOrForever = Literals.TIME_INTERVAL_UNIT_FOREVER.equals(value) || Literals.TIME_INTERVAL_UNIT_NOW.equals(value);
								f.setVisible(! isNowOrForever);
								EsfInteger v = new EsfInteger((String)f.getValue());
								if ( v.isNull() || v.isLessThan(1) || v.isGreaterThan(9999) )
									f.setValue("1");
							}
						}
			        	
			        });
	                return select;
				}
				
				Field field = super.createField(item, propertyId, uiContext);
                field.setWidth(100, Unit.PERCENTAGE);
    			
                if (propertyId.equals("pathName")) {
                	TextField tf = (TextField)field;
                    tf.addValidator(new TransactionTemplateEsfPathNameValidator(getCurrentBean()));
                    tf.setConversionError(vaadinUi.getMsg("validator.esfpathname.message"));
                    tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                    tf.setCaption(vaadinUi.getMsg("caption.esfPathName"));
                    tf.setDescription(vaadinUi.getMsg("TransactionTemplateForm.pathName.tooltip"));
                } else if ( propertyId.equals("displayName") ) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setInputPrompt(vaadinUi.getMsg("inputPrompt.displayName"));
                    tf.setCaption(vaadinUi.getMsg("caption.displayName"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.displayName"));
                } else if ( propertyId.equals("description") ) {
                	TextField tf = (TextField)field;
                	tf.setNullRepresentation("");
                	tf.setInputPrompt(vaadinUi.getMsg("inputPrompt.description"));
                    tf.setCaption(vaadinUi.getMsg("caption.description"));
                    tf.setDescription(vaadinUi.getMsg("tooltip.description"));
                } else if (propertyId.equals("productionRetentionNumTimeIntervalUnits")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setColumns(2);
                	tf.setMaxLength(4);
                	tf.setNullRepresentation("1");
                	tf.setCaption(vaadinUi.getMsg("TransactionTemplateForm.productionRetentionNumTimeIntervalUnits.label"));
                	tf.setDescription(vaadinUi.getMsg("TransactionTemplateForm.productionRetentionTimeIntervalUnits.tooltip"));
                	tf.addValidator(new IntegerRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","1-9999"),1,9999));
                } else if (propertyId.equals("testRetentionNumTimeIntervalUnits")) {
                	TextField tf = (TextField)field;
                	tf.setRequired(true);
                    tf.setRequiredError(vaadinUi.getMsg("tooltip.required"));
                	tf.setColumns(2);
                	tf.setMaxLength(4);
                	tf.setNullRepresentation("1");
                	tf.setCaption(vaadinUi.getMsg("TransactionTemplateForm.testRetentionNumTimeIntervalUnits.label"));
                	tf.setDescription(vaadinUi.getMsg("TransactionTemplateForm.testRetentionTimeIntervalUnits.tooltip"));
                	tf.addValidator(new IntegerRangeValidator(vaadinUi.getMsg("tooltip.invalid.integer.range","1-9999"),1,9999));
                }

                return field;
    	    }
    	 });
    	
    	_logger.debug("Form created");
    }
    
    private void resetCaches() {
		allGroupListIds = null;
		allGroupListNames = null;
    }
    
    private TwinColSelect createPermissionSelectBox(String vaadinMessageCaption) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	TwinColSelect selectList = new TwinColSelect();
    	selectList.setLeftColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".left"));
    	selectList.setRightColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".right"));
    	selectList.setWidth(100, Unit.PERCENTAGE);
        selectList.addValidator(new SelectValidator(selectList));
		
		// Set all the possible values
		for( int i=0; i < allGroupListIds.length; ++i ) {
			selectList.addItem(allGroupListIds[i]);
		}
		// Associate labels with our ID values
		selectList.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		for( int i=0; i < allGroupListIds.length; ++i ) {
			selectList.setItemCaption(allGroupListIds[i], allGroupListNames[i].toPlainString());
		}
		
		selectList.setRows( Math.min(allGroupListIds.length,5) );
		selectList.setNullSelectionAllowed(true);
		selectList.setMultiSelect(true);
		return selectList;
    }
    
    private TwinColSelect createPermissionWithExternalSelectBox(String vaadinMessageCaption) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	TwinColSelect selectList = new TwinColSelect();
    	selectList.setLeftColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".left"));
    	selectList.setRightColumnCaption(vaadinUi.getMsg(vaadinMessageCaption+".right"));
    	selectList.setWidth(100, Unit.PERCENTAGE);
        selectList.addValidator(new SelectValidator(selectList));
		
		// Set all the possible values
		for( int i=0; i < allGroupWithExternalListIds.length; ++i ) {
			selectList.addItem(allGroupWithExternalListIds[i]);
		}
		// Associate labels with our ID values
		selectList.setItemCaptionMode(ItemCaptionMode.EXPLICIT);
		for( int i=0; i < allGroupWithExternalListIds.length; ++i ) {
			selectList.setItemCaption(allGroupWithExternalListIds[i], allGroupWithExternalListNames[i].toPlainString());
		}
		
		selectList.setRows( Math.min(allGroupWithExternalListIds.length,5) );
		selectList.setNullSelectionAllowed(true);
		selectList.setMultiSelect(true);
		return selectList;
    }
    
	public void buttonClick(ClickEvent event) {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
    	Button source = event.getButton();
    	if ( source == saveButton ) {
    		// Run our validators since if they are reporting problems, we'll fail to commit anyway.
        	if ( ! isValid() ) {
    			vaadinUi.showWarning(null, vaadinUi.getMsg("form.save.invalid"));
        		return;
        	}
			commit();
			
			TransactionTemplateBean testBean = newBean == null ? getCurrentBean() : newBean;
			if ( Literals.TIME_INTERVAL_UNIT_NOW.equals(testBean.getProductionRetentionTimeIntervalUnits()) )
				vaadinUi.showWarning(null, vaadinUi.getMsg("TransactionTemplateForm.retentionUnit.now.warning",vaadinUi.getMsg("TransactionTemplateForm.productionRetentionTimeIntervalUnits.label")));
			else if ( Literals.TIME_INTERVAL_UNIT_NOW.equals(testBean.getTestRetentionTimeIntervalUnits()) )
				vaadinUi.showWarning(null, vaadinUi.getMsg("TransactionTemplateForm.retentionUnit.now.warning",vaadinUi.getMsg("TransactionTemplateForm.testRetentionTimeIntervalUnits.label")));
			
    		// If we're saving a new bean
			Errors errors = new Errors();
    		if ( newBean != null ) {
        		if ( newBean.save(errors) ) {
        			vaadinUi.showStatus( vaadinUi.getMsg("form.saveNew.success.message",newBean.getPathName()) );
            		vaadinUi.notifyTransactionTemplateChangeListeners(newBean.transactionTemplate());
            		// We need to add the new bean to the container
            		container.addItem(newBean);
            		view.select(newBean); // reselect our bean so all updates cleanly
            		prevBean = newBean = null; // and we're not longer working on a new bean
        		} else {
        			vaadinUi.show(errors);
        		}
    		} else {
    			TransactionTemplateBean currBean = getCurrentBean();
    			if ( currBean.save(errors) ) {
        			vaadinUi.showStatus( vaadinUi.getMsg("form.save.success.message",currBean.getPathName()) );
            		vaadinUi.notifyTransactionTemplateChangeListeners(currBean.transactionTemplate());
            		view.select(currBean); // reselect our bean so all updates cleanly
        		} else {
        			vaadinUi.show(errors);
        		}
    		}
        } else if ( source == cancelButton ) {
    		discard();
    		vaadinUi.showStatus( vaadinUi.getMsg("form.cancel.success.message") );
        	if ( newBean != null ) {
        		setItemDataSource(null);
        		view.unselectAll(); // we'll unselect and the reselect our prevBean
        		view.select(prevBean);
        		prevBean = newBean = null; // we're not doing a new bean anymore
        	}
        	setReadOnly(true);
        } else if ( source == editButton ) {
        	setReadOnly(false);
        } else if ( source == deleteButton ) {
	    	ConfirmDialog.show(com.vaadin.ui.UI.getCurrent(), 
	    			vaadinUi.getMsg("TransactionTemplateForm.button.delete.ConfirmDialog.caption"),
	    			vaadinUi.getMsg("TransactionTemplateForm.button.delete.ConfirmDialog.message"),
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.ok.button.label"), 
	    			vaadinUi.getMsg("button.delete.ConfirmDialog.cancel.button.label"),
	    	        new ConfirmDialog.Listener() {
						private static final long serialVersionUID = 5050637894270958299L;

						public void onClose(ConfirmDialog dialog) {
	    	                if (dialog.isConfirmed()) {
	    	                	discard();
	    	                	TransactionTemplateBean currBean = getCurrentBean();
	    	                	if ( currBean != null ) {
	    	                		setItemDataSource(null);
	    	                		view.unselectAll(); // we'll unselect it and remove it from our container
	    	                		if ( newBean == null ) {
	    	                			Errors errors = new Errors();
	    	                			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	                			if ( currBean.delete(errors) ) {
	    	                        		vaadinUi.showStatus( vaadinUi.getMsg("form.delete.success.message",currBean.getPathName()) );
	    	                        		vaadinUi.notifyTransactionTemplateChangeListeners(currBean.transactionTemplate());
	    	                        		container.removeItem(currBean);
	    	                			} else {
	    	                				vaadinUi.show(errors);
	    	                			}
	    	                		} else {
	    	                			newBean.delete(null);
	    	                    		view.select(prevBean);
	    	                			prevBean = newBean = null;
	    	                		}
	    	                	}
	    	                }
	    	            }
	    	        });
        } else if ( source == createLikeButton ) {
        	TransactionTemplateBean currBean = getCurrentBean();
        	if ( currBean != null ) {
        		createLike(currBean);
        	}	
        }
    }
	
	public String getCurrentBeanName() {
		TransactionTemplateBean bean = getCurrentBean();
		return bean == null ? "(None)" : bean.getPathName().toString();
	}
    
	TransactionTemplateBean getCurrentBean() {
    	return getBean(getItemDataSource());
    }
    @SuppressWarnings("unchecked")
	public TransactionTemplateBean getBean(Item dataSource) {
    	if ( dataSource == null )
    		return null;
		BeanItem<TransactionTemplateBean> bi = (BeanItem<TransactionTemplateBean>)dataSource;
		return bi.getBean();
    }
    
	@Override
    public void setItemDataSource(Item newDataSource) {
		// Clear our cached list
		resetCaches();
		
    	if (newDataSource != null) {
    		TransactionTemplateBean bean = getBean(newDataSource);
    		
    		if ( _orderedProperties == null ) {
    			_orderedProperties = Arrays.asList("productionStatus","testStatus","pathName","displayName","description","comments","packageId","brandLibraryId",
    					"productionRetentionNumTimeIntervalUnits","productionRetentionTimeIntervalUnits","testRetentionNumTimeIntervalUnits","testRetentionTimeIntervalUnits",
    					"permListIds","permViewDetailsIds","permCreateLikeIds","permUpdateIds","permDeleteIds",
    					"permTransactionStartIds","permTransactionCancelIds","permTransactionReactivateIds","permTransactionSuspendIds","permTransactionResumeIds","permTransactionUseUpdateApiIds","permTransactionUseAdminApiIds"
    					);
    		}
    		super.setItemDataSource(newDataSource, _orderedProperties);
    		
    		setupForm(bean);
    		
    		setReadOnly( ! bean.hasPermUpdate() ); // if they can edit, we put them in edit mode right away
    		newBean = bean.transactionTemplate().doInsert() ? bean : null;
    		if ( newBean != null ) {
    			TextField tf = (TextField)getField("pathName");
    			tf.selectAll();
    			tf.focus();
    		}
    		layout.setVisible(true);
    		getFooter().setVisible(true);
    	} else {
    		super.setItemDataSource(null);
    		layout.setVisible(false);
    		getFooter().setVisible(false);
    	}
    }
	
    @Override
    public void setReadOnly(boolean readOnly) {
    	super.setReadOnly(readOnly);
    	
    	TransactionTemplateBean bean = getCurrentBean();
    	
    	// We have these only while editing/creating new
    	if ( saveButton != null ) {
    		saveButton.setVisible(!readOnly);
    	}
    	cancelButton.setVisible(!readOnly);

    	// We have this only while in view mode
    	if ( editButton != null ) {
        	editButton.setVisible(readOnly && bean != null && bean.hasPermUpdate());
    	}
    	
    	// This one we show this in all modes if has permission
    	if ( createLikeButton != null ) {
        	createLikeButton.setVisible(bean != null && bean.hasPermCreateLike() && bean.transactionTemplate().doUpdate()); // only want create like on an existing bean
    	}
    	
    	if ( deleteButton != null ) {
        	deleteButton.setVisible(bean != null && bean.hasPermDelete());
    	}
    	
    	// We don't want any updates to the status, name or delete permissions on these specialty transaction template
    	if ( bean != null && bean.transactionTemplate().isReserved() ) {
    		getField("pathName").setReadOnly(true);
    		getField("productionStatus").setReadOnly(true);
    		getField("testStatus").setReadOnly(true);
    		getField("permDeleteIds").setReadOnly(true);
    		getField("permTransactionStartIds").setReadOnly(true);
    		getField("permTransactionCancelIds").setReadOnly(true);
    		getField("permTransactionReactivateIds").setReadOnly(true);
    		getField("permTransactionSuspendIds").setReadOnly(true);
    		getField("permTransactionResumeIds").setReadOnly(true);
    		getField("permTransactionUseUpdateApiIds").setReadOnly(true);
    		getField("permTransactionUseAdminApiIds").setReadOnly(true);
    	    	}
    }
    
    public void createLike(TransactionTemplateBean likeBean) {
        // Create a temporary item for the form
    	TransactionTemplateBean createdBean = likeBean.createLike();
        
        BeanItem<TransactionTemplateBean> bi = new BeanItem<TransactionTemplateBean>( createdBean );
        setItemDataSource(bi);
    	prevBean = likeBean;
        setReadOnly(false);
    }
    
    void setupForm(TransactionTemplateBean bean) {
    	EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();

		id.setValue( vaadinUi.getMsg("TransactionTemplateForm.label.id",bean.getId()) );
		
		if ( bean.transactionTemplate().isReserved() ) {
			startUrls.setValue("");
		} else {
			String externalContextPath = vaadinUi.getRequestExternalContextPath();
			startUrls.setValue( vaadinUi.getMsg("TransactionTemplateForm.label.startUrls.html",
													bean.transactionTemplate().getExternalStartUrl(externalContextPath, true, false), // prod
													bean.transactionTemplate().getExternalStartUrl(externalContextPath, false, true), // test
													bean.transactionTemplate().getExternalStartUrl(externalContextPath, true, true) // test like prod
												));
		}
		
		if ( bean.transactionTemplate().doInsert() ) {
			lastUpdatedTimestamp.setCaption(vaadinUi.getMsg("TransactionTemplateForm.label.lastUpdated.whenNew"));
			lastUpdatedTimestamp.setIcon(new ThemeResource(vaadinUi.getMsg("form.newObject.icon")));
		} else {
			String lastUpdatedBy = vaadinUi.getPrettyCode().userDisplayName(bean.transactionTemplate().getLastUpdatedByUserId());
			lastUpdatedTimestamp.setCaption( vaadinUi.getMsg("TransactionTemplateForm.label.lastUpdated",bean.formatLogLastUpdatedTimestamp(),lastUpdatedBy) );
			lastUpdatedTimestamp.setIcon(null);
		}
		
		String createdBy = vaadinUi.getPrettyCode().userDisplayName(bean.transactionTemplate().getCreatedByUserId());
		createdByInfo.setValue( vaadinUi.getMsg("TransactionTemplateForm.label.createdBy",bean.formatLogCreatedTimestamp(),createdBy) );

		createPermDisclosure();
		createPermTransactionDisclosure();
    }
    
    void createPermDisclosure() {
    	VerticalLayout permLayout = new VerticalLayout();
    	permLayout.setMargin(false);
    	permLayout.setSpacing(true);
    	
		permLayout.addComponent(getField("permListIds"));
		permLayout.addComponent(getField("permViewDetailsIds"));
		permLayout.addComponent(getField("permCreateLikeIds"));
		permLayout.addComponent(getField("permUpdateIds"));
		permLayout.addComponent(getField("permDeleteIds"));
				
		permDisclosure.setContent(permLayout);
		permDisclosure.setVisible(true);
    }

    void createPermTransactionDisclosure() {
    	VerticalLayout permLayout = new VerticalLayout();
    	permLayout.setMargin(false);
    	permLayout.setSpacing(true);
    	
		permLayout.addComponent(getField("permTransactionStartIds"));
		permLayout.addComponent(getField("permTransactionCancelIds"));
		permLayout.addComponent(getField("permTransactionReactivateIds"));
		permLayout.addComponent(getField("permTransactionSuspendIds"));
		permLayout.addComponent(getField("permTransactionResumeIds"));
		permLayout.addComponent(getField("permTransactionUseUpdateApiIds"));
		permLayout.addComponent(getField("permTransactionUseAdminApiIds"));
					
		permTransactionDisclosure.setContent(permLayout);
		permTransactionDisclosure.setVisible(true);
    }

    
	@Override
	public void activateView(EsfView.OpenMode mode, String params) {
	}

	@Override
	public void initView() {
	}

    /*
     * Override to get control over where fields are placed.
     */
    @Override
    protected void attachField(Object propertyId, Field field) {
        if (propertyId.equals("productionStatus")) {
            layout.addComponent(field, 0, 1);
        } else if (propertyId.equals("testStatus")) {
        	layout.addComponent(field, 1, 1);
        } else if (propertyId.equals("pathName")) {
        	layout.addComponent(field, 0, 3);
        } else if (propertyId.equals("displayName")) {
            layout.addComponent(field, 1, 3);
        } else if (propertyId.equals("description")) {
            layout.addComponent(field, 0, 4, 1, 4);
        } else if (propertyId.equals("packageId")) {
        	layout.addComponent(field, 0, 5);
        } else if (propertyId.equals("brandLibraryId")) {
        	layout.addComponent(field, 1, 5);
        } else if (propertyId.equals("comments")) {
        	layout.addComponent(field, 0, 6);
        } else if (propertyId.equals("productionRetentionNumTimeIntervalUnits")) {
        	productionRetentionLayout.removeAllComponents();
        	productionRetentionLayout.addComponent(field);
        } else if (propertyId.equals("productionRetentionTimeIntervalUnits")) {
        	productionRetentionLayout.addComponent(field);
        	productionRetentionLayout.setComponentAlignment(field, Alignment.BOTTOM_LEFT);
        } else if (propertyId.equals("testRetentionNumTimeIntervalUnits")) {
        	testRetentionLayout.removeAllComponents();
        	testRetentionLayout.addComponent(field);
        } else if (propertyId.equals("testRetentionTimeIntervalUnits")) {
        	testRetentionLayout.addComponent(field);
        	testRetentionLayout.setComponentAlignment(field, Alignment.BOTTOM_LEFT);
        }
    }

	@Override
	public String checkDirty() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("TransactionTemplateView.ConfirmDiscardChangesDialog.message", getCurrentBeanName());
	}

	@Override
	public boolean isDirty() {
		return isModified();
	}
}