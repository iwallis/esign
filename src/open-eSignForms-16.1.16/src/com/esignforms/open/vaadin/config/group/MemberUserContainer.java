// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.group;

import java.io.Serializable;
import java.util.Collection;

import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.util.BeanItemContainer;

public class MemberUserContainer extends BeanItemContainer<MemberUserBean> implements Serializable {
	private static final long serialVersionUID = -2002858573267086973L;

	public MemberUserContainer(final GroupBean groupBean) throws InstantiationException, IllegalAccessException {
		super(MemberUserBean.class);

		Collection<User> memberUsers = groupBean.group().getAllMemberUsers(EsfVaadinUI.getInstance().getUser());
		for( User u : memberUsers ) {
			addItem( new MemberUserBean(u) );
		}
	}
	
	@Override
	protected void filterAll() {
		super.filterAll();
	}
}