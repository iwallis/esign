// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.record;

import java.io.Serializable;

import com.esignforms.open.Version;
import com.esignforms.open.admin.ThreadChecker;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;

public class DeploymentStatsBean implements Serializable {
	private static final long serialVersionUID = 8895935827345209665L;

	private ThreadChecker threadChecker;
	
	public DeploymentStatsBean() {
		this.threadChecker = new ThreadChecker();
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	protected User user() {
		return EsfVaadinUI.getInstance().getUser();
	}

	// Now for the JavaBeans methods
	public String getSystemStartedTimestamp() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getSystemStartedTimestamp().toLogString(vaadinUi.getUser());
	}
	
	public String getSystemVersion() {
		return Version.getReleaseString();
	}
	
	public String getVaadinVersion() {
		return EsfVaadinUI.getInstance().getVaadinVersion();
	}
	
	public String getVaadinPushmode() {
		return EsfVaadinUI.getInstance().getPushConfiguration().getPushMode().toString();
	}
	
	public String getJavaVersion() {
		return System.getProperty("java.vm.name") + " " + System.getProperty("java.version");
	}
	
	public String getJavaMemory() {
		return EsfVaadinUI.getInstance().getMsg("DeploymentStatsForm.javaMemory.value",
				EsfInteger.byteSizeInUnits(Runtime.getRuntime().freeMemory()),
				EsfInteger.byteSizeInUnits(Runtime.getRuntime().maxMemory())
				);
	}
	
	public int getTotalThreads() {
		return threadChecker == null ? -1 : threadChecker.getTotalThreadCount();
	}
	
	public String getBackgrounderThread() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return threadChecker != null && threadChecker.isBackgrounderRunning() ? vaadinUi.getMsg("DeploymentStatsForm.thread.running") : vaadinUi.getMsg("DeploymentStatsForm.thread.notrunning");
	}
	
	public String getOutboundEmailThread() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return threadChecker != null && threadChecker.isOutboundEmailRunning() ? vaadinUi.getMsg("DeploymentStatsForm.thread.running") : vaadinUi.getMsg("DeploymentStatsForm.thread.notrunning");
	}
	
	public String getInboundEmailThread() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return threadChecker != null && threadChecker.isInboundEmailRunning() ? vaadinUi.getMsg("DeploymentStatsForm.thread.running") : vaadinUi.getMsg("DeploymentStatsForm.thread.notrunning");
	}
	
	public String getHttpSendThread() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return threadChecker != null && threadChecker.isHttpSendRunning() ? vaadinUi.getMsg("DeploymentStatsForm.thread.running") : vaadinUi.getMsg("DeploymentStatsForm.thread.notrunning");
	}
	
	public String getWebServerVersion() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getWebServerVersion();
	}
	
	public String getHttpThreads() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("DeploymentStatsForm.httpThreads.value",
				threadChecker == null ? -1 : threadChecker.getHttpThreadCounts()[0],
				threadChecker == null ? -1 : threadChecker.getHttpThreadCounts()[1]
				);
	}
	
	public String getHttpsThreads() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("DeploymentStatsForm.httpsThreads.value",
				threadChecker == null ? -1 : threadChecker.getHttpsThreadCounts()[0],
				threadChecker == null ? -1 : threadChecker.getHttpsThreadCounts()[1]
				);
	}
	
	public String getDbVersion() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getEsfapp().getConnectionPool().getDbVersionString();
	}
	
	public String getDbMaxConnections() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("DeploymentStatsForm.dbMaxConnections.value",
				vaadinUi.getEsfapp().getConnectionPool().getMaxConnections(),
				vaadinUi.getEsfapp().getConnectionPool().getMaxConnectionsAllowed()
				);
	}

	public String getDbConnections() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("DeploymentStatsForm.dbConnections.value",
								vaadinUi.getEsfapp().getConnectionPool().getNumInuseConnections(),
								vaadinUi.getEsfapp().getConnectionPool().getNumAvailableConnections(),
								vaadinUi.getEsfapp().getConnectionPool().getMaxIdleMinutes()
								);
	}
	
	public String getDbTransactionCount() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return vaadinUi.getMsg("DeploymentStatsForm.dbTransactionCount.value",
				Transaction.Manager.getCount(),
				Transaction.Manager.getNumCached()
				);
	}
	
	public int getDbUserCount() {
		return User.Manager.getNumUsers();
	}

	public String getDbSize() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return EsfInteger.byteSizeInUnits(vaadinUi.getEsfapp().getDatabaseSize());
	}
	
	public String getDbArchiveSize() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		return EsfInteger.byteSizeInUnits(vaadinUi.getEsfapp().getArchiveSize());
	}
}