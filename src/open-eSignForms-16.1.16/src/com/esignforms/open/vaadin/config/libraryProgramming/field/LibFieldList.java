// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.libraryProgramming.field;

import org.vaadin.peter.buttongroup.ButtonGroup;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Property;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.themes.BaseTheme;
import com.vaadin.ui.themes.Reindeer;

public class LibFieldList extends Panel {
	private static final long serialVersionUID = -5757155340930717729L;

	final LibFieldView view;
	LibFieldTable table;
	
	// For search bar
	TextField searchEsfName;
	Button filterButton;
	Button showAllButton;
	Button createNewButton;

	public LibFieldList(final LibFieldView view, final LibFieldBeanContainer container) {
		super();
		this.view = view;
		setStyleName("LibFieldList");
		setSizeFull();
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		
		VerticalLayout layout = new VerticalLayout();
		setContent(layout);
		layout.setSizeFull();
		layout.setSpacing(false);
		layout.setMargin(false);
    	
		HorizontalLayout searchBar = new HorizontalLayout();
    	searchBar.addStyleName("searchBar");
    	searchBar.setSpacing(true);
    	searchBar.setMargin(false);
    	
    	String tooltipSearchString = vaadinUi.getMsg("tooltip.search.string");
    	
    	searchEsfName = new TextField();
    	searchEsfName.setStyleName(Reindeer.TEXTFIELD_SMALL);
    	searchEsfName.setInputPrompt(vaadinUi.getMsg("LibFieldList.searchBar.esfname.label"));
    	searchEsfName.setDescription(tooltipSearchString);
    	searchBar.addComponent(searchEsfName);
    	
    	ButtonGroup buttonGroup = new ButtonGroup();
    	buttonGroup.setStyleName(Reindeer.BUTTON_SMALL);
    	searchBar.addComponent(buttonGroup);
    	
    	filterButton = new Button(vaadinUi.getMsg("LibFieldList.searchBar.filteredButton.label"));
    	filterButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.filter.icon")));
    	filterButton.setDescription(vaadinUi.getMsg("button.filter.tooltip"));
    	filterButton.setStyleName(Reindeer.BUTTON_SMALL);
    	filterButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = 3252706891418472655L;

			@Override
			public void buttonClick(ClickEvent event) {
				view.unselectAll();
				container.removeAllContainerFilters();
				String limitEsfName = (String)searchEsfName.getValue();
				if ( EsfString.isNonBlank(limitEsfName) ) {
					boolean searchStartsWith = limitEsfName.charAt(0) == Literals.SEARCH_STARTS_WITH_PREFIX;
					if ( searchStartsWith ) {
						limitEsfName = limitEsfName.substring(1);
					}
					container.addContainerFilter("esfName", limitEsfName, true, searchStartsWith);
				}
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				vaadinUi.showStatus(vaadinUi.getMsg("list.search.filterButton.message",container.size()));
			}
		});
    	buttonGroup.addButton(filterButton);

    	showAllButton = new Button(vaadinUi.getMsg("LibFieldList.searchBar.showAllButton.label"));
    	showAllButton.setIcon(new ThemeResource(vaadinUi.getMsg("button.showAll.icon")));
    	showAllButton.setDescription(vaadinUi.getMsg("button.showAll.tooltip"));
    	showAllButton.setStyleName(Reindeer.BUTTON_SMALL);
    	showAllButton.addClickListener( new Button.ClickListener() {			
			private static final long serialVersionUID = -4589933537839050473L;

			@Override
			public void buttonClick(ClickEvent event) {
				view.unselectAll();
				container.removeAllContainerFilters();
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				vaadinUi.showStatus(vaadinUi.getMsg("list.search.showAllButton.message",container.size()));
			}
		});
    	buttonGroup.addButton(showAllButton);
    	
    	if ( view.hasPermCreateLike() ) {
	    	createNewButton = new Button();
	    	createNewButton.setStyleName(BaseTheme.BUTTON_LINK);
	    	createNewButton.setIcon(new ThemeResource(vaadinUi.getMsg("LibFieldList.searchBar.createNewButton.icon")));
	    	createNewButton.setDescription(vaadinUi.getMsg("LibFieldList.searchBar.createNewButton.tooltip"));
	    	createNewButton.addStyleName("createNewButton");
	    	createNewButton.addClickListener( new Button.ClickListener() {			
				private static final long serialVersionUID = -1839074625862471719L;

				@Override
				public void buttonClick(ClickEvent event) {
					view.createNew();
				}
			});
	    	searchBar.addComponent(createNewButton);
    	}

    	table = new LibFieldTable(view, container);
    	
    	layout.addComponent(searchBar);
    	layout.addComponent(table);
    	layout.setExpandRatio(table, 1);
	}
	
	@Override
	public void setReadOnly(boolean v) {
		super.setReadOnly(v);
		if ( createNewButton != null ) {
			createNewButton.setVisible(!v);
		}
	}
	
	public Table getTable() {
		return table;
	}
	
	class LibFieldTable extends Table {
		private static final long serialVersionUID = 8562258564701993039L;

		public LibFieldTable(LibFieldView view, LibFieldBeanContainer container) {
			super();
			EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
	    	
			setContainerDataSource(container);
			setVisibleColumns((Object[])vaadinUi.getStringArray("LibFieldList.showColumnProperties"));
			setColumnHeaders(vaadinUi.getStringArray("LibFieldList.showColumnHeaders"));
			setColumnCollapsingAllowed(true);
			setColumnReorderingAllowed(true);

	        // Make table selectable, react immediately to user events, and pass events to the view
	        setSelectable(true);
	        setImmediate(true);
	        addValueChangeListener((Property.ValueChangeListener)view);
	        setNullSelectionAllowed(true);
			setSizeFull();
		}
		
		@Override
		protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
			if ( "type".equals(colId) ) {
				EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
				return vaadinUi.getPrettyCode().fieldTemplateType((String)property.getValue());
			}
			return super.formatPropertyValue(rowId,colId,property);
		}
	
	} // LibFieldTable

} // LibFieldList
