// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.group;

import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.data.Property;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class GroupMemberUsersList extends Window {
	private static final long serialVersionUID = -4925465124566547554L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(GroupMemberUsersList.class);

    GroupBean groupBean;
	Table userTable;
	MemberUserContainer userContainer;

	public GroupMemberUsersList(final GroupBean groupBean) {
		super(EsfVaadinUI.getInstance().getMsg("GroupMemberUsersList.windowTitle",groupBean.getPathName())); 
		this.groupBean = groupBean;

        buildLayout();
	}
	
	public void show() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		setWidth(60, Unit.PERCENTAGE);
		setHeight(400, Unit.PIXELS);
		center();
		vaadinUi.addWindowToUI(this);
	}
	
	protected void buildLayout() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		try {

	        VerticalLayout layout = new VerticalLayout();
	        setContent(layout);
	        layout.setMargin(true);
	        layout.setSpacing(true);
	        layout.setSizeFull();

			userContainer = new MemberUserContainer(groupBean);

			userTable = new Table()	{
				private static final long serialVersionUID = -1853065048720166605L;

				@Override
				protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
					EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
					if ( "status".equals(colId) ) {
						return vaadinUi.getPrettyCode().status((String)property.getValue());
					}
					
					return super.formatPropertyValue(rowId,colId,property);
				}
			};
			userTable.setContainerDataSource(userContainer);
			userTable.setVisibleColumns(vaadinUi.getStringArray("GroupMemberUsersList.showColumnProperties"));
			userTable.setColumnHeaders(vaadinUi.getStringArray("GroupMemberUsersList.showColumnHeaders"));
			userTable.setColumnCollapsingAllowed(true);
			userTable.setColumnReorderingAllowed(true);
			userTable.setSelectable(false);
			userTable.setImmediate(false);
			userTable.setSizeFull();
			
			layout.addComponent(userTable);
		} catch( Exception e ) {
			vaadinUi.showError("Group member user list exception", e.getMessage());
		}
    	_logger.debug("buildLayout() completed");
	}
	
}