// Copyright (C) 2010-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin.config.library;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.PermissionOption;
import com.esignforms.open.user.User;
import com.esignforms.open.vaadin.EsfVaadinUI;

public class LibraryBean implements Serializable, Comparable<LibraryBean> {
	private static final long serialVersionUID = 6901135457152057125L;

	private Library library;
	
	// This contains all of the groups that the user can list, so these are the only ones that
	// can be used to add/remove groups (any groups that the Group object has that are not included
	// cannot be touched)
	private EsfUUID[] allGroupListIds;
	private EsfPathName[] allGroupListNames;

	private Set<EsfUUID> permListIds;
	private Set<EsfUUID> permViewDetailsIds;
	private Set<EsfUUID> permCreateLikeIds;
	private Set<EsfUUID> permUpdateIds;
	private Set<EsfUUID> permDeleteIds;
	
	public LibraryBean(Library library) {
		this.library = library;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o !=null && o instanceof LibraryBean )
			return library.equals(((LibraryBean)o).library);
		return false;
	}
	@Override
    public int hashCode() {
    	return library.hashCode();
    }
	public int compareTo(LibraryBean l) {
		return library.compareTo(l.library);
	}
	
	// Some methods we need for convenience, but don't want to follow the javabeans naming convention so that
	// the Vaadin bean routines don't bother with them
	protected User user() {
		return EsfVaadinUI.getInstance().getUser();
	}
	public Library library() {
		return library;
	}

	public LibraryBean createLike() {
	    Library newLibrary = Library.Manager.createLike(library, library.getPathName(), user());	    
	    return new LibraryBean(newLibrary);
	}
	
	public boolean save(Errors errors) {
		boolean ret = library.save(user());
		if ( ret ) {
			resetCaches();
		} else {
			if ( errors != null ) {
				errors.addError(EsfVaadinUI.getInstance().getMsg("form.save.failed.message"));
			}
		}
		return ret;
	}
	public boolean save() {
		return save(null);
	}
	
	public boolean delete(Errors errors) {
		if ( library.isReserved() )
			return false;
		return library.delete(errors,user());
	}

	protected HashSet<EsfUUID> createIdHashSet( Collection<Group> list ) {
		HashSet<EsfUUID> set = new HashSet<EsfUUID>(list.size());
		for( Group g : list ) {
			set.add(g.getId());
		}
		return set;
	}
	
	private synchronized void resetCaches() {
		allGroupListIds = null;
		allGroupListNames = null;
		permListIds = null;
		permViewDetailsIds = null;
		permCreateLikeIds = null;
		permUpdateIds = null;
		permDeleteIds = null;
	}

	protected synchronized void createAllGroupLists() {
		EsfVaadinUI vaadinUi = EsfVaadinUI.getInstance();
		Collection<Group> list = Group.Manager.getForUserWithListPermission(user(),Group.INCLUDE.BOTH_ENABLED_AND_DISABLED);
		
		int size = list.size();
		
		// We will not include the "External Users" pseudo-group.
		allGroupListIds = new EsfUUID[size-1];
		allGroupListNames = new EsfPathName[size-1];
		int i = 0;
		for( Group g : list ) {
			if ( g.isExternalUsersGroup() )
				continue;
			allGroupListIds[i] = g.getId();
			allGroupListNames[i] = g.isDisabled() ? new EsfPathName(g.getPathName() + EsfPathName.PATH_SEPARATOR + vaadinUi.getMsg("PrettyCode.status.disabled")) : g.getPathName();
			++i;
		}
	}
	public EsfUUID[] allGroupListIds() {
		if ( allGroupListIds == null ) {
			createAllGroupLists();
		}
		return allGroupListIds;
	}
	public EsfPathName[] allGroupListNames() {
		if ( allGroupListNames == null ) {
			createAllGroupLists();
		}
		return allGroupListNames;
	}
	
	
	// Now for the JavaBeans methods
	public EsfUUID getId() {
		return library.getId();
	}
	
	public EsfPathName getPathName() {
		return library.getPathName();
	}
	public void setPathName(EsfPathName v) {
		if ( ! library.isReserved() ) {
			library.setPathName( v );
		}
	}
	
	public boolean isEnabled() {
		return library.isEnabled();
	}
	public String getStatus() {
		return library.getStatus();
	}
	public void setStatus( String v ) {
		if ( ! library.isReserved() ) {
			library.setStatus(v);
		}
	}
	
	public String getDescription() {
		return library.getDescription();
	}
	public void setDescription(String v) {
		library.setDescription(v);
	}
	
	public String getComments() {
		return library.getComments();
	}
	public void setComments(String v) {
		library.setComments( v );
	}
	
    public EsfUUID getDefaultDocumentStyleId()
    {
    	return library.getDefaultDocumentStyleId();
    }
    public void setDefaultDocumentStyleId(EsfUUID v)
    {
    	library.setDefaultDocumentStyleId(v);
    }


	protected void setPermIds(Set<EsfUUID> v, EsfName permOption) {
		// First, let's remove all groups this user can list
		for( Group g : Group.Manager.getForUserWithListPermission(user()) ) {
			library.removePermissionAllowedGroups(permOption, g);
		}
		// Then let's add back in the selected set
		for( EsfUUID gid : v ) {
			Group g = Group.Manager.getById( gid );
			if ( g != null ) {
				library.addPermissionAllowedGroups(permOption, g);
			}
		}
	}

	public Set<EsfUUID> getPermListIds() {
		if ( permListIds == null ) {
			permListIds = createIdHashSet( library.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_LIST) );
		}
		return permListIds;
	}
		
	public void setPermListIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_LIST);
	}

	public Set<EsfUUID> getPermViewDetailsIds() {
		if ( permViewDetailsIds == null ) {
			permViewDetailsIds = createIdHashSet( library.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_VIEWDETAILS) );
		}
		return permViewDetailsIds;
	}
	
	public void setPermViewDetailsIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public Set<EsfUUID> getPermCreateLikeIds() {
		if ( permCreateLikeIds == null ) {
			permCreateLikeIds = createIdHashSet( library.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_CREATELIKE) );
		}
		return permCreateLikeIds;
	}
	
	public void setPermCreateLikeIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public Set<EsfUUID> getPermUpdateIds() {
		if ( permUpdateIds == null ) {
			permUpdateIds = createIdHashSet( library.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_UPDATE) );
		}
		return permUpdateIds;
	}
	
	public void setPermUpdateIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_UPDATE);
	}

	public Set<EsfUUID> getPermDeleteIds() {
		if ( permDeleteIds == null ) {
			permDeleteIds = createIdHashSet( library.getPermissionAllowedGroups(user(), PermissionOption.PERM_OPTION_DELETE) );
		}
		return permDeleteIds;
	}
	
	public void setPermDeleteIds(Set<EsfUUID> v) {
		setPermIds(v, PermissionOption.PERM_OPTION_DELETE);
	}

	public EsfDateTime getLastUpdatedTimestamp() {
		return library.getLastUpdatedTimestamp();
	}
	public String formatLastUpdatedTimestamp() {
		return library.getLastUpdatedTimestamp().toString(user());
	}
	public String formatLogLastUpdatedTimestamp() {
		return library.getLastUpdatedTimestamp().toLogString(user());
	}

	public String formatCreatedTimestamp() {
		return library.getCreatedTimestamp().toString(user());
	}
	public String formatLogCreatedTimestamp() {
		return library.getCreatedTimestamp().toLogString(user());
	}

	public boolean hasPermViewDetails() {
		return library.hasPermission(user(), PermissionOption.PERM_OPTION_VIEWDETAILS);
	}

	public boolean hasPermUpdate() {
		return library.hasPermission(user(), PermissionOption.PERM_OPTION_UPDATE);
	}

	public boolean hasPermCreateLike() {
		return library.hasPermission(user(), PermissionOption.PERM_OPTION_CREATELIKE);
	}

	public boolean hasPermDelete() {
		return library.isReserved() ? false : library.hasPermission(user(), PermissionOption.PERM_OPTION_DELETE);
	}
	
}