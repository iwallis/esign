// Copyright (C) 2013-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.vaadin;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.esignforms.open.Application;
import com.esignforms.open.admin.SessionTracker;
import com.esignforms.open.user.User;
import com.esignforms.open.util.ServletUtil;
import com.esignforms.open.vaadin.main.MainView;
import com.vaadin.server.DeploymentConfiguration;
import com.vaadin.server.ServiceException;
import com.vaadin.server.SystemMessages;
import com.vaadin.server.SystemMessagesInfo;
import com.vaadin.server.SystemMessagesProvider;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;
import com.vaadin.server.VaadinServletService;
import com.vaadin.server.VaadinSession;
import com.vaadin.server.WrappedSession;

/**
 * EsfVaadinServlet is our extension of the standard VaadinServlet so we can intercept HTTP requests.
 * 
 * @author Yozons Inc.
 */
public class EsfVaadinServlet extends com.vaadin.server.VaadinServlet {
	private static final long serialVersionUID = -5014958123267357305L;


	@Override
    protected void servletInitialized() throws ServletException {
        super.servletInitialized();
        
		getService().setSystemMessagesProvider(
        	    new SystemMessagesProvider() {
					private static final long serialVersionUID = 5776072298071825406L;

				@Override 
        	    public SystemMessages getSystemMessages(SystemMessagesInfo systemMessagesInfo) {
        	        EsfVaadinUI ui = EsfVaadinUI.getInstance();
        	        return ui == null ? EsfVaadinUI.SetupCustomizedSystemMessages(Application.getInstance().getContextPath()+"/"+EsfVaadinUI.DEFAULT_LOGOUT_URL) : ui.getCustomizedSystemMessages();
        	    }
        	   });
    }

	
    @Override
    protected VaadinServletService createServletService(DeploymentConfiguration deploymentConfiguration) throws ServiceException {
        VaadinServletService servletService = new VaadinServletService(this, deploymentConfiguration){
 			private static final long serialVersionUID = 5091440397660880039L;

			@Override
            public void requestStart(VaadinRequest request, VaadinResponse response) {
                super.requestStart(request, response);
        		String localRequestUrl = ServletUtil.getLocalRequestUrl((HttpServletRequest)request);
        		//System.err.println((new EsfDateTime()).toDateTimeMsecString() + " *DEBUG* - localRequestUrl: " + localRequestUrl);
        		if ( ! localRequestUrl.contains("/VAADIN/") && ! localRequestUrl.contains("/ui/PUSH") ) { // allow for themes/widgetset downloads without requiring the user be logged in
            		WrappedSession wrappedSession = request.getWrappedSession();
        			User loggedInUser = wrappedSession == null ? null : (User)wrappedSession.getAttribute(SessionTracker.USER_SESSION_ATTRIBUTE_NAME);
        			if ( loggedInUser == null ) {
    			  		//System.err.println((new EsfDateTime()).toDateTimeMsecString() + " *DEBUG* - localRequestUrl: " + localRequestUrl + "; came in WITH NO LOGGED IN USER; session isNew: " + wrappedSession.isNew());
        				try {
        					// We don't redirect on unexpected POSTs and such because most likely the framework will show
        					// a session expired or the like. But for a new GET request, we'll do the redirect to the login page.
        					if ( ! "post".equalsIgnoreCase(request.getMethod()) ) {
        						String redirectUrl = ServletUtil.getExternalUrlContextPath((HttpServletRequest)request)+"/";
        						((HttpServletResponse)response).sendRedirect(redirectUrl);
        					}
        				}
        				catch(java.io.IOException e) { 
        					// ignore
        				}
        			}
        		}
            }

            @Override
            public void requestEnd(VaadinRequest request, VaadinResponse response, final VaadinSession session) {
            	if ( session != null ) {
    				for( com.vaadin.ui.UI vaadin : session.getUIs() ) {
    					if ( vaadin instanceof EsfVaadinUI ) {
    						final EsfVaadinUI vaadinUi = (EsfVaadinUI)vaadin;
    						MainView mainView = vaadinUi.getMainView();
    						if ( mainView != null ) {
    							String localRequestUrl = ServletUtil.getLocalRequestUrl((HttpServletRequest)request);
    							if ( ! localRequestUrl.contains("/HEARTBEAT/") ) { // Heartbeats don't count for a client update
    			        			mainView.updateLastServerCommunications(vaadinUi);
    			        		}
    			        		
    						}
    					}
    				}
            	}
            	
				//System.err.println( (new EsfDateTime()).toDateTimeMsecString() + " *DEBUG* - super.requestEnd(request, response, session)");
                super.requestEnd(request, response, session);
            }
        };
        
        servletService.init();
        return servletService;
    }
}
	
