// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.servlet.ServletContext;

import com.esignforms.open.admin.Backgrounder;
import com.esignforms.open.admin.BootService;
import com.esignforms.open.admin.HttpSendProcessor;
import com.esignforms.open.admin.InboundEmailProcessor;
import com.esignforms.open.admin.OutboundEmailProcessor;
import com.esignforms.open.admin.PasswordManager;
import com.esignforms.open.admin.SessionKey;
import com.esignforms.open.admin.SessionTracker;
import com.esignforms.open.admin.SignatureKey;
import com.esignforms.open.admin.ThreadChecker;
import com.esignforms.open.config.Literals;
import com.esignforms.open.config.MessageFormatFile;
import com.esignforms.open.config.PropertiesFile;
import com.esignforms.open.crypto.PBE;
import com.esignforms.open.crypto.PublicKeyGenerator;
import com.esignforms.open.crypto.SecureHash;
import com.esignforms.open.crypto.SecretKeyGenerator;
import com.esignforms.open.crypto.SecureRandom;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.Record;
import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPools;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.db.DatabaseObject;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.log.ActivityLog;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.UI;
import com.esignforms.open.user.User;
import com.esignforms.open.util.DateUtil;
import com.esignforms.open.util.RandomKey;

/**
 * Application class for the Open eSignForms custom web applications. 
 *
 * @author Yozons, Inc.
 */
public class Application
{
	public static final String CONTENT_TYPE_PDF     = "application/pdf";
    public static final String CONTENT_TYPE_MSWORD  = "application/msword";
    public static final String CONTENT_TYPE_MSEXCEL = "application/vnd.ms-excel";
    public static final String CONTENT_TYPE_CSV     = "text/csv";
    public static final String CONTENT_TYPE_ZIP     = "application/zip";
    public static final String CONTENT_TYPE_HTML    = "text/html";
    public static final String CONTENT_TYPE_XML     = "application/xml";
    public static final String CONTENT_TYPE_TEXT_XML= "text/xml";
    public static final String CONTENT_TYPE_RTF     = "application/rtf";
    public static final String CONTENT_TYPE_TEXT    = "text/plain";
    public static final String CONTENT_TYPE_GIF     = "image/gif";
    public static final String CONTENT_TYPE_JPEG    = "image/jpeg";
    public static final String CONTENT_TYPE_PJPEG   = "image/pjpeg";
    public static final String CONTENT_TYPE_PNG     = "image/png";
    public static final String CONTENT_TYPE_XPNG    = "image/x-png"; 
    public static final String CONTENT_TYPE_BINARY  = "application/octet-stream";
    public static final String CONTENT_TYPE_CHARSET_UTF_8 	= "; charset=" + EsfString.CHARSET_UTF_8;
    
    public static final String NOCACHE_FILENAME		= ".nocache"; // if this is in the file name, our expiration headers filter won't let it be cached.
    
    protected static Application   _app;
    
    // Used to bootstrap the system automatically; after startup sets the session keys, these will be nulled out again
    private String	bootPassword1           	= null;
    private String	bootPassword2           	= null;
    private String	bootSessionKeyCreateNewDays	= "90";
    
    // Used to set the max size of a file upload
    private int  uploadFileMaxMB				= 100;
    private int  uploadImageMaxMB				= 10;
    private int  uploadConfigMaxMB				= 10;

    // We assign the defaults here if not overridden in setup properties
    private String jceProviderName  			= "BC";
    private String jceProviderClass				= "org.bouncycastle.jce.provider.BouncyCastleProvider";

    private String secureRandomAlgorithm		= "SHA1PRNG";
    private String secureRandomProvider			= "SUN";
    
    private String secureHashAlgorithm			= "SHA-512";
    private String secureHashProvider			= "BC";
    
    private int jbcryptSaltIterations			= 11; // Our default as of 2012.
    
    private String keyPairGeneratorAlgorithm	= "RSA";
    private String keyPairGeneratorProvider		= "BC";
    private int keyPairGeneratorKeySize			= 4096;
    
    private String pbeKeyFactoryAlgorithm		= "PBEWithSHA256And256BitAES-CBC-BC";
    private String pbeKeyFactoryProvider		= "BC";
    private String pbeCipherAlgorithm			= "PBEWithSHA256And256BitAES-CBC-BC";
    private String pbeCipherProvider			= "BC";
    private int    pbeIterations				= 51;
    private int    pbeSaltSize					= 8;
    
    private String symmetricKeyAlgorithm		= "AES";
    private String symmetricKeyProvider			= "BC";
    private int    symmetricKeySize				= 256;
    private String symmetricCipherAlgorithm		= "AES/CBC/PKCS7Padding";
    private String symmetricCipherProvider		= "BC";
    private int    symmetricCipherBlockSize		= 16;
    
    // For library autogeneration of code/JSP/image
    private String libraryGeneratedBasePath;
    private java.io.File libraryGeneratedBaseDirectory;
    private String libraryGeneratedJSPDocumentPath;
    private java.io.File libraryGeneratedJSPDocumentDirectory;
    private String libraryGeneratedCSSPath;
    private java.io.File libraryGeneratedCSSDirectory;
    
    // Various objects that we share across the application
    private com.esignforms.open.log.Logger	logger;
    private ServletContext 					servletContext;
    private SecureRandom					secureRandom;
    private RandomKey                     	randomKey;
    private SecretKeyGenerator				secretKeyGenerator;
    private PublicKeyGenerator		 		publicKeyGenerator;
    private MessageFormatFile				serverMessages;

    private String               			contextPath		= "";
    private String							defaultUrlProtocolAddress = "https://localhost.localdomain";
    private String							externalContextPath = defaultUrlProtocolAddress + contextPath;
    
    // These are only set if a database has been configured for this web application
    protected ConnectionPools				connectionPools;
    protected ConnectionPool				dbPool;
    private ActivityLog						activityLog;
    private BlobDb							blobDb;
    
    protected EsfUUID		 				deployId;
    protected SessionKey[]       			sessionKeys;
    protected SignatureKey					signatureKey;
    
    protected Record						globalProperties;
    protected Record 						deployProperties;
    
    private EsfUUID							deploymentPropertiesId;
    private Locale							defaultLocale			= Locale.getDefault();
    private String							defaultCountryCode;
    private String							defaultDateFormat;
    private String							defaultTimeFormat;
    private String							defaultDateTimeFormat; // calculated from above
    private String							defaultLogDateFormat;
    private String							defaultLogTimeFormat;
    private String							defaultLogDateTimeFormat; // calculated from above
    private EsfDate							installDate;
    private EsfInteger						installYear;
    private boolean 						allowProductionTransactions; // when true, production transactions can be started
    private boolean 						allowTestTransactions; 		 // when true, test transactions can be started
    private String							deploymentBasePath;
    private UI								ui;
    
    // Cached from servermessages.properties since used frequently
    private String							pageTitlePrefix;
    private String							loginPage;
    private EsfDateTime						systemStartedTimestamp;
    
    public final static String BACKGROUNDER_THREAD_NAME_PREFIX = "EsfBackgrounder";
    public final static String OUTBOUND_EMAIL_PROCESSOR_THREAD_NAME_PREFIX = "EsfOutboundEmailProcessor";
    public final static String INBOUND_EMAIL_PROCESSOR_THREAD_NAME_PREFIX = "EsfInboundEmailProcessor";
    public final static String HTTP_SEND_PROCESSOR_THREAD_NAME_PREFIX = "EsfHttpSendProcessor";
    protected Backgrounder					backgrounder;
    protected InboundEmailProcessor			inboundEmailProcessor;
    protected OutboundEmailProcessor		outboundEmailProcessor;
    protected HttpSendProcessor				httpSendProcessor;
    
    protected SessionTracker				sessionTracker;
    protected PasswordManager    			passwordManager;
   
    protected enum AppState { UNITIALIZED,				// initial state before anything has taken place
    						  PRE_BOOT_INITIALIZING, 	// constructor called, initializing basic components before it's booted
    						  DB_AVAILABLE,				// the database is available
    						  WAITING_TO_BOOT, 			// the system is waiting to be booted (waiting for boot passwords to be entered)
    						  BOOT_COMPLETED,			// the system has been booted
    						  POST_BOOT_INITIALIZING, 	// boot has taken place (boot passwords entered, verified, deployId known and session keys known)
    						  READY,					// the application is ready to process requests after being booted and completing remaining initialization
    						  SHUTTING_DOWN				// system is shutting down
    						};
    protected AppState						currentState	= AppState.UNITIALIZED;
    
    
    protected Application(ServletContext context, String contextPath, boolean doBootSystem) 
    {
        _app = this;
        _app.servletContext = context;
        currentState = AppState.PRE_BOOT_INITIALIZING;
        
		DateUtil.initialize(this); // Initialize DateUtil 

		logger = new com.esignforms.open.log.Logger();
        
        deploymentBasePath = System.getProperty("esf.deploybase");
        if ( EsfString.isBlank(deploymentBasePath) )
        	err("Missing deployment base path property: -Desf.deploybase=XXX in your Java startup.");
        else
        	info("Using deployment base path: " + deploymentBasePath);

		try
		{
            new java.net.URL( "https://open.esignforms.com" );
		}
		catch( Exception e )
		{
			except(e,"ERROR: Application() could not create an 'https' URL.");
		}
        
        setContextPath(contextPath);
        
        // Get an initial set early on in case we need to emit any
        loadServerMessages();
        
        loadOpenESignFormsProperties(context, "openesignforms");
        
        try
        {
        	Class<?> jceProvider = Class.forName(jceProviderClass);
        	java.security.Provider p = (java.security.Provider)jceProvider.newInstance();
            int slot = java.security.Security.addProvider( p );
            debug("Add provider " + jceProviderName + " using class " + jceProviderClass + " at JCE slot " + slot);
            if ( slot == -1 )
            {
                java.security.Provider prov = java.security.Security.getProvider(jceProviderName);
                if ( prov == null )
                    err("Could not find the " + jceProviderName + " JCE provider");
                else
                    info("Found " + jceProviderName + " Provider: " + prov.getInfo());
            }
        }
	    catch(ClassNotFoundException e) 
	    {
	    	except(e,"Application - could not find JCE provider class " + jceProviderClass);
	    }
	    catch(InstantiationException e) 
	    {
	    	except(e,"Application - could not instantiate JCE provider class " + jceProviderClass);
	    }
	    catch(IllegalAccessException e) 
	    {
	    	except(e,"Application - could not instantiate/access JCE provider class " + jceProviderClass);
	    }

    	secureRandom	= new SecureRandom(secureRandomAlgorithm,secureRandomProvider);
        randomKey		= new RandomKey();
        randomKey.getBytes(randomKey.getIntBetween(47, 311)); // now get a random number of bytes from our PRNG and discard.
        secretKeyGenerator = new SecretKeyGenerator(symmetricKeyAlgorithm,symmetricKeyProvider,
        										 symmetricKeySize,symmetricCipherAlgorithm,symmetricCipherProvider,
        										 secureRandom.getPRNG()
        									    );
        secretKeyGenerator.setBlockSizeInBytes(symmetricCipherBlockSize);
        
        publicKeyGenerator = new PublicKeyGenerator(keyPairGeneratorAlgorithm,keyPairGeneratorProvider,keyPairGeneratorKeySize);
        
        if ( initializeDatabase() )
        {
            info("The database has been initialized.");
            
            activityLog = ActivityLog.getInitialInstance(this);
            blobDb		= new BlobDb();
            
            if ( doBootSystem )
            {
                if ( bootSystem() )
                	completePostBootInitialization();
                else
                    activityLog.logSystemStartStop("DB ready but ready to be booted for " + Version.getReleaseString());
            }
            else
            	warning("Booting of the system has been skipped as requested.");
        }
        else
        {
            warning("The database was NOT initialized.  No database is configured for this application.");
        }
    }
    
    /**
     * This static method allows anybody who needs one to get a handle to the application context.
     * @return the Application singleton instance; can be null if called before any constructors.
     */
    public static Application getInstance()
    {
        return _app;
    }

    /**
     * This method is used by a standard web startup, passing in the servlet context of the webapp.
     * @param context the ServletContext of the webapp
     * @return the "singleton" Application object to use, fully booted and ready to go
     */
    public static Application getInitialInstance(ServletContext context)
    {
        return new Application(context,context.getContextPath(),true);
    }

    /**
     * This method is used by DbSetup when it is creating a new installation.
     * The system is started as much as possible, but stops before it is booted.
     * @param contextPath to assume since there's no ServletContext
     * @return the "singleton" Application object to use without being booted
     */
    public static Application getInstallationInstance(String contextPath)
    {
        return new Application(null,contextPath,false);
    }

    protected boolean loadOpenESignFormsProperties(ServletContext context, String bundleName)
    {
        try
        {
            PropertiesFile bundle = new PropertiesFile(bundleName);
            
            bootPassword1			= bundle.getString("boot.password.1",bootPassword1);
            bootPassword2			= bundle.getString("boot.password.2",bootPassword2);
            bootSessionKeyCreateNewDays	= bundle.getString("boot.sessionkey.createNewNoSoonerThanDays",bootSessionKeyCreateNewDays);

            uploadFileMaxMB			= bundle.getInteger("upload.file.maxMB",uploadFileMaxMB);
            uploadImageMaxMB		= bundle.getInteger("upload.image.maxMB",uploadImageMaxMB);
            uploadConfigMaxMB		= bundle.getInteger("upload.config.maxMB",uploadConfigMaxMB);
            
            jceProviderName			= bundle.getString("jce.provider.name",jceProviderName);
            jceProviderClass		= bundle.getString("jce.provider.class",jceProviderClass);
            
            secureRandomAlgorithm	= bundle.getString("java.security.SecureRandom.algorithm",secureRandomAlgorithm);
            secureRandomProvider	= bundle.getString("java.security.SecureRandom.provider",secureRandomProvider);
            RandomKey.SetRandomKeyUndesirableStringSegments(bundle.getString("RandomKey.undesirable.string.segments"));
            
            keyPairGeneratorAlgorithm = bundle.getString("java.security.KeyPairGenerator.algorithm",keyPairGeneratorAlgorithm);
            keyPairGeneratorProvider = bundle.getString("java.security.KeyPairGenerator.provider",keyPairGeneratorProvider);
            keyPairGeneratorKeySize	= bundle.getInteger("java.security.KeyPairGenerator.keysize",keyPairGeneratorKeySize);
            
            secureHashAlgorithm		= bundle.getString("java.security.MessageDigest.algorithm",secureHashAlgorithm);
            secureHashProvider		= bundle.getString("java.security.MessageDigest.provider",secureHashProvider);
            
            pbeKeyFactoryAlgorithm	= bundle.getString("PBE.javax.crypto.SecretKeyFactory.algorithm",pbeKeyFactoryAlgorithm);
            pbeKeyFactoryProvider	= bundle.getString("PBE.javax.crypto.SecretKeyFactory.provider",pbeKeyFactoryProvider);
            pbeCipherAlgorithm		= bundle.getString("PBE.javax.crypto.Cipher.algorithm",pbeCipherAlgorithm);
            pbeCipherProvider		= bundle.getString("PBE.javax.crypto.Cipher.provider",pbeCipherProvider);
            pbeIterations			= bundle.getInteger("PBE.iterations",pbeIterations);
            pbeSaltSize				= bundle.getInteger("PBE.saltSize",pbeSaltSize);
            
            jbcryptSaltIterations   = bundle.getInteger("jBCrypt.gensalt.iterations",jbcryptSaltIterations);
            
            symmetricKeyAlgorithm	= bundle.getString("javax.crypto.KeyGenerator.algorithm",symmetricKeyAlgorithm);
            symmetricKeyProvider	= bundle.getString("javax.crypto.KeyGenerator.provider",symmetricKeyProvider);
            symmetricKeySize		= bundle.getInteger("javax.crypto.KeyGenerator.keysize",symmetricKeySize);
            symmetricCipherAlgorithm= bundle.getString("javax.crypto.KeyGenerator.cipher.algorithm",symmetricCipherAlgorithm);
            symmetricCipherProvider	= bundle.getString("javax.crypto.KeyGenerator.cipher.provider",symmetricCipherProvider);
            symmetricCipherBlockSize= bundle.getInteger("javax.crypto.KeyGenerator.blocksize",symmetricCipherBlockSize);
            
            // For the library auto-generation code, we need some directories under our context path directory where we put it all.
            // (We have no context when running dbsetup, so we use the current working directory which should be our webapp location.)
            String realContextPath = (context == null ) ? System.getProperty("user.dir") : context.getRealPath("/");            	
            java.io.File contextPathDir = new java.io.File(realContextPath);
            
            libraryGeneratedBasePath = bundle.getString("library.generated.base.subdirectory","LIBRARYGENERATED-KEEP");
            libraryGeneratedBaseDirectory = new java.io.File(contextPathDir,libraryGeneratedBasePath);
            debug("Application.loadOpenESignFormsProperties() - generated files to: " + libraryGeneratedBaseDirectory.getAbsolutePath() + "; realContextPath: " + realContextPath);
            if ( libraryGeneratedBaseDirectory.exists() )
            {
            	if ( ! libraryGeneratedBaseDirectory.isDirectory() || 
            	     ! libraryGeneratedBaseDirectory.canRead()     ||
            	     ! libraryGeneratedBaseDirectory.canWrite()    ||
            	     ! libraryGeneratedBaseDirectory.canExecute()  
            	   )
            	{
                	err("Application.loadOpenESignFormsProperties() - The library.generated.base.subdirectory property does not point to a directory/folder with read/write/execute as expected: " + libraryGeneratedBaseDirectory.getAbsolutePath());
                	return false;
            	}
            }
            else
            {
            	if ( ! libraryGeneratedBaseDirectory.mkdirs()          ||
            		 ! libraryGeneratedBaseDirectory.setReadable(true) ||
            		 ! libraryGeneratedBaseDirectory.setWritable(true) ||
            		 ! libraryGeneratedBaseDirectory.setExecutable(true) 
            	   )
            	{
                	err("Application.loadOpenESignFormsProperties() - The library.generated.base.subdirectory property directory/folder could not be created with read/write/execute: " + libraryGeneratedBaseDirectory.getAbsolutePath());
                	return false;
            	}
            }

            libraryGeneratedJSPDocumentPath	= bundle.getString("library.generatedJSP.document.subdirectory","document");
            libraryGeneratedJSPDocumentDirectory = new java.io.File(libraryGeneratedBaseDirectory,libraryGeneratedJSPDocumentPath);
            if ( libraryGeneratedJSPDocumentDirectory.exists() )
            {
            	if ( ! libraryGeneratedJSPDocumentDirectory.isDirectory() || 
            	     ! libraryGeneratedJSPDocumentDirectory.canRead()     ||
            	     ! libraryGeneratedJSPDocumentDirectory.canWrite()    ||
            	     ! libraryGeneratedJSPDocumentDirectory.canExecute() 
            	   )
            	{
                	err("Application.loadOpenESignFormsProperties() - The library.generatedJSP.document.subdirectory property does not point to a directory/folder with read/write/execute under library.generated.base.subdirectory as expected: " + libraryGeneratedJSPDocumentDirectory.getAbsolutePath());
                	return false;
            	}
            }
            else
            {
            	if ( ! libraryGeneratedJSPDocumentDirectory.mkdirs()          ||
            		 ! libraryGeneratedJSPDocumentDirectory.setReadable(true) ||
            		 ! libraryGeneratedJSPDocumentDirectory.setWritable(true) ||
            		 ! libraryGeneratedJSPDocumentDirectory.setExecutable(true)
            	   )
            	{
                	err("Application.loadOpenESignFormsProperties() - The library.generatedJSP.document.subdirectory property directory/folder could not be created with read/write/execute: " + libraryGeneratedJSPDocumentDirectory.getAbsolutePath());
                	return false;
            	}
            }
            libraryGeneratedJSPDocumentPath = libraryGeneratedBasePath + "/" + libraryGeneratedJSPDocumentPath; // actual path is nested
            
            libraryGeneratedCSSPath	= bundle.getString("library.generatedCSS.subdirectory","css");
            libraryGeneratedCSSDirectory = new java.io.File(libraryGeneratedBaseDirectory,libraryGeneratedCSSPath);
            if ( libraryGeneratedCSSDirectory.exists() )
            {
            	if ( ! libraryGeneratedCSSDirectory.isDirectory() || 
            	     ! libraryGeneratedCSSDirectory.canRead()     ||
            	     ! libraryGeneratedCSSDirectory.canWrite()    ||
            	     ! libraryGeneratedCSSDirectory.canExecute() 
            	   )
            	{
                	err("Application.loadOpenESignFormsProperties() - The library.generatedCSS.subdirectory property does not point to a directory/folder with read/write/execute under library.generated.base.subdirectory as expected: " + libraryGeneratedCSSDirectory.getAbsolutePath());
                	return false;
            	}
            }
            else
            {
            	if ( ! libraryGeneratedCSSDirectory.mkdirs()          ||
            		 ! libraryGeneratedCSSDirectory.setReadable(true) ||
            		 ! libraryGeneratedCSSDirectory.setWritable(true) ||
            		 ! libraryGeneratedCSSDirectory.setExecutable(true)
            	   )
            	{
                	err("Application.loadOpenESignFormsProperties() - The library.generatedCSS.subdirectory property directory/folder could not be created with read/write/execute: " + libraryGeneratedCSSDirectory.getAbsolutePath());
                	return false;
            	}
            }
            libraryGeneratedCSSPath = libraryGeneratedBasePath + "/" + libraryGeneratedCSSPath; // actual path is nested

            info("Application.loadOpenESignFormsProperties() successfully loaded properties from: " + bundleName + "; context path directory: " + contextPathDir.getAbsolutePath());
            
            return true;
        }
        catch( Exception e ) 
        {
            except(e,"Could not load openesignforms properties file: " + bundleName );
            return false;
        }
    }
    public String getContextPath()
    {
        return contextPath;
    }
    protected void setContextPath(String v)
    {
        contextPath = v;
        updateExternalContextPath();
    }
    public String getExternalContextPath()
    {
    	return externalContextPath;
    }
    
    /**
     * Determines if production-level transactions are allowed to operate now.
     * @return true if production transactions are allowed
     */
    public boolean allowProductionTransactions()
    {
    	return allowProductionTransactions;
    }
    public void setAllowProductionTransactions(boolean v)
    {
    	allowProductionTransactions = v;
    }
    
    /**
     * Determines if test-level transactions are allowed to operate now.
     * @return true if test transactions are allowed
     */
    public boolean allowTestTransactions()
    {
    	return allowTestTransactions;
    }
    public void setAllowTestTransactions(boolean v)
    {
    	allowTestTransactions = v;
    }
    
    public String getLibraryGeneratedBasePath() 
    {
    	return libraryGeneratedBasePath;
    }
    public java.io.File getLibraryGeneratedBaseDirectory() 
    {
    	return libraryGeneratedBaseDirectory;
    }
    public String getLibraryGeneratedJSPDocumentPath()
    {
    	return libraryGeneratedJSPDocumentPath;
    }
    public java.io.File getLibraryGeneratedJSPDocumentDirectory()
    {
    	return libraryGeneratedJSPDocumentDirectory;
    }
    public String getLibraryGeneratedCSSPath()
    {
    	return libraryGeneratedCSSPath;
    }
    public java.io.File getLibraryGeneratedCSSDirectory()
    {
    	return libraryGeneratedCSSDirectory;
    }


    public boolean hasBootPasswords()
    {
    	return hasBootPassword1() && hasBootPassword2();
    }
    public boolean hasBootPassword1()
    {
        return EsfString.isNonBlank(bootPassword1);
    }
    public void setBootPassword1(String v)
    {
    	bootPassword1 = v;
    }
    public boolean hasBootPassword2()
    {
        return EsfString.isNonBlank(bootPassword2);
    }   
    public void setBootPassword2(String v)
    {
    	bootPassword2 = v;
    }
    private void clearBootPasswords()
    {
        bootPassword1 = bootPassword2 = null;
    }
    
    public int getUploadFileMaxMB()
    {
    	return uploadFileMaxMB;
    }
    public long getUploadFileMaxBytes()
    {
    	return getUploadFileMaxMB() * Literals.MB;
    }
    
    public int getUploadImageMaxMB()
    {
    	return uploadImageMaxMB;
    }
    public long getUploadImageMaxBytes()
    {
    	return getUploadImageMaxMB() * Literals.MB;
    }
    
    public int getUploadConfigMaxMB()
    {
    	return uploadConfigMaxMB;
    }
    public long getUploadConfigMaxBytes()
    {
    	return getUploadConfigMaxMB() * Literals.MB;
    }
    
    public EsfDateTime getSystemStartedTimestamp()
    {
    	return systemStartedTimestamp;
    }

    private final synchronized boolean initializeDatabase()
    {
    	if ( currentState.ordinal() < AppState.PRE_BOOT_INITIALIZING.ordinal() )
    	{
    		err("Application.initializeDatabase() - Attempted to initialize the database before the PRE_BOOT_INITIALIZING state.  Current state: " + currentState);
    		return false;
    	}
    	if ( currentState.ordinal() >= AppState.DB_AVAILABLE.ordinal() )
    	{
    		err("Application.initializeDatabase() - Attempted to initialize the database after the DB_AVAILABLE state.  Current state: " + currentState);
    		return false;
    	}
    	
        // Create the database connection pools and retrieve the default esf database connection pool that has our app's tables
        try
        {
            connectionPools = new ConnectionPools();
            dbPool          = connectionPools.getPool("esf");
            if ( dbPool == null )
            {
                err("Application.initializeDatabase() - Could not load required 'esf' database pool.");
                throw new EsfException("Missing required 'esf' database");
            }
            
            currentState = AppState.DB_AVAILABLE;
            return true;
        }
        catch( EsfException e )
        {
            if ( connectionPools != null )
                connectionPools.destroy();
            connectionPools = null;
            dbPool          = null;
            currentState	= AppState.PRE_BOOT_INITIALIZING;
            return false;
        }
    }
    
    // Loads the "shared global" and deployment properties.
	public boolean loadDeploymentProperties()
	{
	    ConnectionPool    pool = getConnectionPool();
	    Connection        con  = pool.getConnection();
	    PreparedStatement stmt = null;
	    
	    try
	    {
	        stmt = con.prepareStatement("SELECT global_properties_id,deploy_properties_id FROM esf_deployment WHERE id=?");
	        stmt.setObject(1,getDeployId().getUUID(), Types.OTHER);
	        ResultSet rs = stmt.executeQuery();
	        if ( ! rs.next() )
	        {
	        	err("loadDeploymentProperties() - Failed to find the global and deployment property ids in esf_deployment for deploy id: " + deployId);
	        	return false;
	        }
	        
	        EsfUUID globalPropertiesId = new EsfUUID((UUID)rs.getObject(1));
	        deploymentPropertiesId = new EsfUUID((UUID)rs.getObject(2));
	        
	        if ( globalPropertiesId.isNull() )
	        {
	        	err("loadDeploymentProperties() - Failed to find the global properties id in esf_deployment");
	        	return false;
	        }
	        if ( deploymentPropertiesId.isNull() )
	        {
	        	err("loadDeploymentProperties() - Failed to find the deployment properties id in esf_deployment");
	        	return false;
	        }	        
	        
        	globalProperties = Record.Manager.getById(con, globalPropertiesId);
        	if ( globalProperties == null )
	        {
	        	err("loadDeploymentProperties() - Failed to load the global properties using id: " + globalPropertiesId);
	        	return false;
	        }
        	globalProperties.markReadOnly();
	        
        	// We don't save the deployment properties for access since these are properties that only makes sense to 
        	// this deployment (they are not user-defined).
	        deployProperties = Record.Manager.getById(con, deploymentPropertiesId);
	        if ( deployProperties == null )
	        {
	        	err("loadDeploymentProperties() - Failed to load the deployment properties using id: " + deploymentPropertiesId);
	        	return false;
	        }
		    installDate						= deployProperties.getDateByName( getInstallDateEsfName() );
		    installYear						= deployProperties.getIntegerByName( getInstallYearEsfName() );

		    setDefaultUrlProtocolAddress(deployProperties.getStringByName( getDefaultUrlProtocolAddressEsfName() ).toString());
		    setDefaultLocale(deployProperties.getStringByName( getDefaultLocaleEsfName() ).toString());
		    setDefaultTimezone(deployProperties.getStringByName( getDefaultTimeZoneEsfName() ).toString());
		    setAllowProductionTransactions(deployProperties.getBooleanByName(getAllowProductionTransactionsEsfName()).isTrue());
		    setAllowTestTransactions(deployProperties.getBooleanByName(getAllowTestTransactionsEsfName()).isTrue());
		    setDefaultDateFormat(deployProperties.getStringByName( getDefaultDateFormatEsfName() ).toString());
		    setDefaultTimeFormat(deployProperties.getStringByName( getDefaultTimeFormatEsfName() ).toString());
		    setDefaultLogDateFormat(deployProperties.getStringByName( getDefaultLogDateFormatEsfName() ).toString());
		    setDefaultLogTimeFormat(deployProperties.getStringByName( getDefaultLogTimeFormatEsfName() ).toString());
		    
		    // New property in 12.8.25 release that may not be present when running dbsetup to upgrade to this release. 
		    // Once this release is well into the past, we can remove this test and just do like above, 
		    // setting the value based on what's configured.
		    EsfString defaultCountryCode = deployProperties.getStringByName(getDefaultCountryCodeEsfName());
		    if ( defaultCountryCode != null )
		    	setDefaultCountryCode(defaultCountryCode.toString());
		    else
		    	setDefaultCountryCode("US");
		    
		    // Set up our servermessages.properties now that we have our default locale
		    loadServerMessages();

            con.commit();
            return true;
	    }
	    catch(SQLException e) 
	    {
			sqlerr(e,"ERROR: loadDeploymentProperties()");
            pool.rollbackIgnoreException(con,e);
	        return false;
	    }
	    finally
	    {
	        cleanupPool(pool,con,stmt);
	    }
	}
	public final EsfUUID getDeploymentPropertiesId() 
	{
		return deploymentPropertiesId;
	}
	public final Record getDeploymentProperties()
	{
		return deployProperties;
	}
	public final void saveDeploymentProperties()
	{
		if ( deployProperties != null )
			deployProperties.save();
	}
	// A few routines that ensure DbSetup and the above routines are using the same esfname
	public final EsfName getDefaultLocaleEsfName()
	{
		return new EsfName("defaultLocale");
	}
	public final EsfName getDefaultTimeZoneEsfName()
	{
		return new EsfName("defaultTimeZone");
	}
	public final EsfName getDefaultCountryCodeEsfName()
	{
		return new EsfName("defaultCountryCode");
	}
	public final EsfName getDefaultDateFormatEsfName()
	{
		return new EsfName("defaultDateFormat");
	}
	public final EsfName getDefaultTimeFormatEsfName()
	{
		return new EsfName("defaultTimeFormat");
	}
	public final EsfName getDefaultLogDateFormatEsfName()
	{
		return new EsfName("defaultLogDateFormat");
	}
	public final EsfName getDefaultLogTimeFormatEsfName()
	{
		return new EsfName("defaultLogTimeFormat");
	}
	public final EsfName getDefaultUrlProtocolAddressEsfName()
	{
		return new EsfName("defaultUrlProtocolAddress");
	}
	public final EsfName getInstallDateEsfName()
	{
		return new EsfName("installDate");
	}
	public final EsfName getInstallYearEsfName()
	{
		return new EsfName("installYear");
	}
	public final EsfName getLibraryProgrammingTipsHtmlEsfName()
	{
		return new EsfName("LibraryProgrammingTipsHtml");
	}
	public final EsfName getTipsHtmlEsfName(String tipsPrefix)
	{
		return new EsfName(tipsPrefix+"TipsHtml");
	}
	public final EsfName getAllowProductionTransactionsEsfName()
	{
		return new EsfName("allowProductionTransactions");
	}
	public final EsfName getAllowTestTransactionsEsfName()
	{
		return new EsfName("allowTestTransactions");
	}
	
	public final EsfName getSmtpReturnPathHostNameEsfName()
	{
		return new EsfName("smtpReturnPathHostName");
	}
	public EsfString getSmtpReturnPathHostName()
	{
		return (EsfString)deployProperties.getValueByName(getSmtpReturnPathHostNameEsfName());
	}
	public final EsfName getSmtpServerEsfName()
	{
		return new EsfName("smtpServer");
	}
	public EsfString getSmtpServer()
	{
		return (EsfString)deployProperties.getValueByName(getSmtpServerEsfName());
	}
	public final EsfName getSmtpPortEsfName()
	{
		return new EsfName("smtpPort");
	}
	public EsfInteger getSmtpPort()
	{
		return (EsfInteger)deployProperties.getValueByName(getSmtpPortEsfName());
	}
	public final EsfName getSmtpAuthUserEsfName()
	{
		return new EsfName("smtpAuthUser");
	}
	public EsfString getSmtpAuthUser()
	{
		return (EsfString)deployProperties.getValueByName(getSmtpAuthUserEsfName());
	}
	public final EsfName getSmtpAuthPasswordEsfName()
	{
		return new EsfName("smtpAuthPassword");
	}
	public EsfString getSmtpAuthPassword()
	{
		return (EsfString)deployProperties.getValueByName(getSmtpAuthPasswordEsfName());
	}
	public final EsfName getSmtpStartTLSEsfName()
	{
		return new EsfName("smtpStartTLS");
	}
	public EsfBoolean getSmtpStartTLS()
	{
		return (EsfBoolean)deployProperties.getValueByName(getSmtpStartTLSEsfName());
	}
	public final EsfName getImapServerEsfName()
	{
		return new EsfName("imapServer");
	}
	public EsfString getImapServer()
	{
		return (EsfString)deployProperties.getValueByName(getImapServerEsfName());
	}
	public final EsfName getImapPortEsfName()
	{
		return new EsfName("imapPort");
	}
	public EsfInteger getImapPort()
	{
		return (EsfInteger)deployProperties.getValueByName(getImapPortEsfName());
	}
	public final EsfName getImapUserEsfName()
	{
		return new EsfName("imapUser");
	}
	public EsfString getImapUser()
	{
		return (EsfString)deployProperties.getValueByName(getImapUserEsfName());
	}
	public final EsfName getImapPasswordEsfName()
	{
		return new EsfName("imapPassword");
	}
	public EsfString getImapPassword()
	{
		return (EsfString)deployProperties.getValueByName(getImapPasswordEsfName());
	}
	public final EsfName getImapSSLEsfName()
	{
		return new EsfName("imapSSL");
	}
	public EsfBoolean getImapSSL()
	{
		return (EsfBoolean)deployProperties.getValueByName(getImapSSLEsfName());
	}
	public final EsfName getDefaultEmailFromEsfName()
	{
		return new EsfName("defaultEmailFrom");
	}
	public EsfString getDefaultEmailFrom()
	{
		return (EsfString)deployProperties.getValueByName(getDefaultEmailFromEsfName());
	}
	
	public static final EsfString LICENSE_TYPE_DEFAULT = new EsfString("OpenSourceAGPL");
	public static final EsfString LICENSE_TYPE_COMMERCIAL_DBSIZE = new EsfString("CommercialDBSIZE");
	public static final EsfString LICENSE_TYPE_COMMERCIAL_NUM_USERS = new EsfString("CommercialNUMUSERS");
	public final EsfName getLicenseTypeEsfName()
	{
		return new EsfName("licenseType");
	}
	public EsfString getLicenseType()
	{
		return (EsfString)deployProperties.getValueByName(getLicenseTypeEsfName());
	}
	public void setLicenseType(EsfString type)
	{
		deployProperties.addUpdate(getLicenseTypeEsfName(), type);
	}
	public boolean isLicenseTypeCommercialDbSize()
	{
		EsfString type = getLicenseType();
		return type != null && type.equals(LICENSE_TYPE_COMMERCIAL_DBSIZE);
	}
	public boolean isLicenseTypeCommercialNumUsers()
	{
		EsfString type = getLicenseType();
		return type != null && type.equals(LICENSE_TYPE_COMMERCIAL_NUM_USERS);
	}
	public boolean isLicenseTypeCommercial()
	{
		return isLicenseTypeCommercialDbSize() || isLicenseTypeCommercialNumUsers();
	}
	public boolean isLicenseTypeOpenSource()
	{
		return ! isLicenseTypeCommercial(); // AGPL
	}
	
	public final EsfName getLicenseSizeEsfName()
	{
		return new EsfName("licenseSize");
	}
	public EsfInteger getLicenseSize()
	{
		return (EsfInteger)deployProperties.getValueByName(getLicenseSizeEsfName());
	}
	public void setLicenseSize(EsfInteger val)
	{
		if ( val != null && ! val.isNull() && ! val.isNegative() )
			deployProperties.addUpdate(getLicenseSizeEsfName(), val);
	}

	public String getLicenseInfo()
	{

        if ( isLicenseTypeCommercialDbSize() )
		{
        	EsfInteger licenseSize = getLicenseSize();
        	
            long dbSize = getDatabaseSize();
            long licenseSizeInBytes = licenseSize.toLong() * Literals.MB;
            String status = dbSize > licenseSizeInBytes ? " (EXCEEDED)" : "";
			return getLicenseType().toString() + " using " + EsfInteger.byteSizeInUnits(dbSize) + " of " + EsfInteger.byteSizeInUnits(licenseSizeInBytes) + status;
		}
		if ( isLicenseTypeCommercialNumUsers() )
		{
			EsfInteger licenseSize = getLicenseSize();
			
	        long numUsers = User.Manager.getNumUsers();
	        long licenseUsers = licenseSize.toLong();
            String status = numUsers > licenseUsers ? " (EXCEEDED)" : "";
			return getLicenseType().toString() + " has " + numUsers + " users of " + licenseUsers + status;
		}
		return getLicenseType().toString();
	}

	
	public final EsfName getDefaultSessionTimeoutMinutesEsfName()
	{
		return new EsfName("defaultSessionTimeoutMinutes");
	}
	public EsfInteger getDefaultSessionMinutesTimeout()
	{
		return (EsfInteger)deployProperties.getValueByName(getDefaultSessionTimeoutMinutesEsfName());
	}
	public void setDefaultSessionTimeoutMinutes(EsfInteger val)
	{
		if ( val != null && ! val.isNull() && val.isPositive() )
			deployProperties.addUpdate(getDefaultSessionTimeoutMinutesEsfName(), val);
	}
	public final EsfName getLoggedInSessionTimeoutMinutesEsfName()
	{
		return new EsfName("loggedInSessionTimeoutMinutes");
	}
	public EsfInteger getLoggedInSessionTimeoutMinutes()
	{
		return (EsfInteger)deployProperties.getValueByName(getLoggedInSessionTimeoutMinutesEsfName());
	}
	public void setLoggedInSessionTimeoutMinutes(EsfInteger val)
	{
		if ( val != null && ! val.isNull() && val.isPositive() )
			deployProperties.addUpdate(getLoggedInSessionTimeoutMinutesEsfName(), val);
	}

	
	public final EsfName getRetainSystemConfigChangeDaysEsfName()
	{
		return new EsfName("retainSystemConfigChangeDays");
	}
	public EsfInteger getRetainSystemConfigChangeDays()
	{
		return (EsfInteger)deployProperties.getValueByName(getRetainSystemConfigChangeDaysEsfName());
	}
	public void setRetainSystemConfigChangeDays(EsfInteger val)
	{
		if ( val != null && ! val.isNull() && val.isPositive() )
			deployProperties.addUpdate(getRetainSystemConfigChangeDaysEsfName(), val);
	}
	public final EsfName getRetainSystemUserActivityDaysEsfName()
	{
		return new EsfName("retainSystemUserActivityDays");
	}
	public EsfInteger getRetainSystemUserActivityDays()
	{
		return (EsfInteger)deployProperties.getValueByName(getRetainSystemUserActivityDaysEsfName());
	}
	public void setRetainSystemUserActivityDays(EsfInteger val)
	{
		if ( val != null && ! val.isNull() && val.isPositive() )
			deployProperties.addUpdate(getRetainSystemUserActivityDaysEsfName(), val);
	}
	public final EsfName getRetainSystemStartStopDaysEsfName()
	{
		return new EsfName("retainSystemStartStopDays");
	}
	public EsfInteger getRetainSystemStartStopDays()
	{
		return (EsfInteger)deployProperties.getValueByName(getRetainSystemStartStopDaysEsfName());
	}
	public void setRetainSystemStartStopDays(EsfInteger val)
	{
		if ( val != null && ! val.isNull() && val.isPositive() )
			deployProperties.addUpdate(getRetainSystemStartStopDaysEsfName(), val);
	}
	public final EsfName getRetainStatsDaysEsfName()
	{
		return new EsfName("retainStatsDays");
	}
	public EsfInteger getRetainStatsDays()
	{
		return (EsfInteger)deployProperties.getValueByName(getRetainStatsDaysEsfName());
	}
	public void setRetainStatsDays(EsfInteger val)
	{
		if ( val != null && ! val.isNull() && val.isPositive() )
			deployProperties.addUpdate(getRetainStatsDaysEsfName(), val);
	}
	public final EsfName getRetainUserConfigChangeDaysEsfName()
	{
		return new EsfName("retainUserConfigChangeDays");
	}
	public EsfInteger getRetainUserConfigChangeDays()
	{
		return (EsfInteger)deployProperties.getValueByName(getRetainUserConfigChangeDaysEsfName());
	}
	public void setRetainUserConfigChangeDays(EsfInteger val)
	{
		if ( val != null && ! val.isNull() && val.isPositive() )
			deployProperties.addUpdate(getRetainUserConfigChangeDaysEsfName(), val);
	}
	public final EsfName getRetainUserSecurityDaysEsfName()
	{
		return new EsfName("retainUserSecurityDays");
	}
	public EsfInteger getRetainUserSecurityDays()
	{
		return (EsfInteger)deployProperties.getValueByName(getRetainUserSecurityDaysEsfName());
	}
	public void setRetainUserSecurityDays(EsfInteger val)
	{
		if ( val != null && ! val.isNull() && val.isPositive() )
			deployProperties.addUpdate(getRetainUserSecurityDaysEsfName(), val);
	}
	public final EsfName getRetainUserLoginLogoffDaysEsfName()
	{
		return new EsfName("retainUserLoginLogoffDays");
	}
	public EsfInteger getRetainUserLoginLogoffDays()
	{
		return (EsfInteger)deployProperties.getValueByName(getRetainUserLoginLogoffDaysEsfName());
	}
	public void setRetainUserLoginLogoffDays(EsfInteger val)
	{
		if ( val != null && ! val.isNull() && val.isPositive() )
			deployProperties.addUpdate(getRetainUserLoginLogoffDaysEsfName(), val);
	}

	
	public final EsfName getDefaultButtonMessageEsfName()
	{
		return new EsfName("ESF_DefaultButtonMessage");
	}

	public final EsfName getDefaultDocumentStyleEsfName()
	{
		return new EsfName("ESF_DefaultDocumentStyle");
	}

	public final EsfName getDropDownBackgroundColorEsfName()
	{
		return new EsfName("ESF_BackgroundColor");
	}
	public final EsfName getDropDownBorderTypesEsfName()
	{
		return new EsfName("ESF_BorderTypes");
	}
	public final EsfName getDropDownFontEsfName()
	{
		return new EsfName("ESF_Font");
	}
	public final EsfName getDropDownFontColorEsfName()
	{
		return new EsfName("ESF_FontColor");
	}
	public final EsfName getDropDownFontSizeEsfName()
	{
		return new EsfName("ESF_FontSize");
	}
	public final EsfName getDropDownFontStyleEsfName()
	{
		return new EsfName("ESF_FontStyle");
	}
	public final EsfName getDropDownTextAlignEsfName()
	{
		return new EsfName("ESF_TextAlign");
	}
	
	public final EsfName getDropDownLocaleEsfName()
	{
		return new EsfName("ESF_Locale");
	}
	public final EsfName getDropDownTimeZoneEsfName()
	{
		return new EsfName("ESF_TimeZone");
	}
	public final EsfName getDropDownUSAPostalStatePosessionEsfName()
	{
		return new EsfName("ESF_USA_PostalStatePossession");
	}
	public final EsfName getDropDownUSAPostalStatesEsfName()
	{
		return new EsfName("ESF_USA_PostalStates");
	}
	public final EsfName getDropDownCanadaPostalProvincesEsfName()
	{
		return new EsfName("ESF_Canada_PostalProvinces");
	}
	public final EsfName getDropDownCountryEsfName()
	{
		return new EsfName("ESF_Countries");
	}
	public final EsfName getDropDownPartyRenotifyTimesEsfName()
	{
		return new EsfName("ESF_PartyRenotifyTimes");
	}
	public final EsfName getDropDownTimeIntervalUnitsEsfName()
	{
		return new EsfName("ESF_TimeIntervalUnits");
	}
	public final EsfName getDropDownDateTimeFormatEsfName()
	{
		return new EsfName("ESF_DateTimeFormat");
	}
	public final EsfName getDropDownDateFormatEsfName()
	{
		return new EsfName("ESF_DateFormat");
	}
	public final EsfName getDropDownTimeFormatEsfName()
	{
		return new EsfName("ESF_TimeFormat");
	}
	public final EsfName getDropDownDecimalFormatEsfName()
	{
		return new EsfName("ESF_DecimalFormat");
	}
	public final EsfName getDropDownIntegerFormatEsfName()
	{
		return new EsfName("ESF_IntegerFormat");
	}
	public final EsfName getDropDownIntegerExtraOptionsEsfName()
	{
		return new EsfName("ESF_IntegerExtraOptions");
	}
	public final EsfName getDropDownMoneyFormatEsfName()
	{
		return new EsfName("ESF_MoneyFormat");
	}
	public final EsfName getDropDownPayPalCardTypesEsfName()
	{
		return new EsfName("ESF_PayPal_CardTypes");
	}
	public final EsfName getDropDownStringTransformsEsfName()
	{
		return new EsfName("ESF_String_Transforms");
	}

	public final EsfName getPropertySetESFEsfName()
	{
		return new EsfName("ESF");
	}
	public final EsfName getPropertySetMyCompanyEsfName()
	{
		return new EsfName("MyCompany");
	}
	
	// Use new ButtonMessage object defined in the library.
	// 13.3.9 release removed all of the deprecated property names and disabled this property set. Future versions may remove the property set entirely.
	@Deprecated
	public final EsfName getPropertySetEsfButtonsEsfName()
	{
		return new EsfName("ESF_Buttons");
	}
	
	public final EsfName getEmailTemplateSetPasswordEsfName()
	{
		return new EsfName("SetPassword");
	}
	public final EsfName getEmailTemplatePasswordLockoutEsfName()
	{
		return new EsfName("PasswordLockout");
	}
	public final EsfName getEmailTemplateForgotPasswordEsfName()
	{
		return new EsfName("ForgotPassword");
	}
	public final EsfName getEmailTemplatePasswordChangedEsfName()
	{
		return new EsfName("PasswordChanged");
	}
	// Convenience routine for those that are getting all email templates, but don't want these well-defined ones...
	public List<EsfName> getAllPasswordRelatedEmailTemplateEsfNames() {
		LinkedList<EsfName> list = new LinkedList<EsfName>();
		list.add(getEmailTemplateSetPasswordEsfName());
		list.add(getEmailTemplatePasswordLockoutEsfName());
		list.add(getEmailTemplateForgotPasswordEsfName());
		list.add(getEmailTemplatePasswordChangedEsfName());
		return list;
	}
	
	public final EsfName getEmailTemplateDefaultPickupNotificationEsfName()
	{
		return new EsfName("DefaultPickupNotification");
	}
	
	public final EsfName getImageDefaultLogoEsfName()
	{
		return new EsfName("Logo");
	}
	public final EsfName getImageDefaultLogoForAppEsfName()
	{
		return new EsfName("LogoForApp");
	}
	public final EsfName getImageDefaultLogoForEmailEsfName()
	{
		return new EsfName("LogoForEmail");
	}
	public final EsfName getImageSignHereLeftArrowEsfName()
	{
		return new EsfName("SignHereLeftArrow");
	}
	public final EsfName getImagePackageDocumentCompletedEsfName()
	{
		return new EsfName("PackageDocumentCompleted");
	}
	public final EsfName getImagePackageDocumentFixRequestedEsfName()
	{
		return new EsfName("PackageDocumentFixRequested");
	}
	public final EsfName getImagePackageDocumentRejectedEsfName()
	{
		return new EsfName("PackageDocumentRejected");
	}
	public final EsfName getImagePackageDocumentToDoEsfName()
	{
		return new EsfName("PackageDocumentToDo");
	}
	public final EsfName getImagePackageDocumentViewOnlyEsfName()
	{
		return new EsfName("PackageDocumentViewOnly");
	}
	public final EsfName getImageHowToVideoIconEsfName()
	{
		return new EsfName("HowToVideoIcon");
	}
	public final EsfName getImageIconForErrorEsfName()
	{
		return new EsfName("IconForError");
	}
	public final EsfName getImageIconForInfoEsfName()
	{
		return new EsfName("IconForInfo");
	}
	
	public final UI getUI()
	{
		return ui;
	}
	
	public final EsfDate 	getInstallDate()			{ return installDate; }
	public final EsfInteger getInstallYear()			{ return installYear; }
    
	public final String 	getDeploymentBasePath()		{ return deploymentBasePath; }
	public final String 	getMyDeploymentBasePath()		
	{ 
		return deploymentBasePath + "/" + getConnectionPool().getDbName();
	}
	
    public final String getLoginPage()					{ return loginPage; }
    public final String getPageTitlePrefix()			{ return pageTitlePrefix; }


	/**
	 * Called during startup whenever we have both boot passwords and need to do the boot sequence.  It assumes the DB is available.
	 * @return true if the system is booted; false if it's not booted or not ready to be booted (too early or already booted)
	 */
    public final synchronized boolean bootSystem()
    {
    	// We need the database in order to boot
    	if ( currentState.ordinal() < AppState.DB_AVAILABLE.ordinal() )
    	{
    		err("Application.bootSystem() - Attempted to boot the system before the DB_AVAILABLE state.  Current state: " + currentState);
    		return false;
    	}
    	
    	// If we've already booted, no point in doing it again
    	if ( currentState.ordinal() >= AppState.BOOT_COMPLETED.ordinal() )
    	{
    		err("Application.bootSystem() - Attempted to boot the system after it's already been been booted. Current state: " + currentState);
    		return false;
    	}
    	
    	BootService boot = null;
    	
        try
        {
            // If we have our boot keys, let's go ahead and try to boot it
            if ( ! hasBootPasswords() )
                throw new EsfException(serverMessages.getString("boot.error.missingpasswords","Cannot load session keys without the boot passwords."));
            
            EsfInteger numDays = new EsfInteger( EsfString.getOnlyNumeric(bootSessionKeyCreateNewDays) );

            boot 		= new BootService(this,bootPassword1,bootPassword2,numDays.toLong());
            sessionKeys = boot.getSessionKeys();
            setDeployId(boot.getDeployId());
            currentState = AppState.BOOT_COMPLETED;
            clearBootPasswords();
            return true;
        }
        catch( EsfException e )
        {
        	deployId		= null;
        	currentState	= AppState.WAITING_TO_BOOT;
            return false;
        }
        finally
        {
        	if ( boot != null )
        	{
        		boot.destroy();
        		boot = null;
        	}
        }
    }
    
    public boolean isSystemBooted()
    {
        return currentState.ordinal() >= AppState.BOOT_COMPLETED.ordinal();
    }
    
    public boolean isSystemReady()
    {
        return currentState.ordinal() >= AppState.READY.ordinal();
    }
   
    
    public final synchronized boolean completePostBootInitialization()
    {
    	// If we're not boot completed, we cannot do this step
    	if ( currentState != AppState.BOOT_COMPLETED )
    	{
    		err("Application.completePostBootInitialization() - Attempted to complete post boot initialization when not in the BOOT_COMPLETED state.  Current state: " + currentState);
    		return false;
    	}

    	currentState = AppState.POST_BOOT_INITIALIZING;
    	
    	if ( ! loadDeploymentProperties() )
    		return false;
    	
    	sessionTracker = new SessionTracker();
    	passwordManager = new PasswordManager();
    	
    	// Load our User and Group caches in that order; Group cache load will also load the Permission cache,
    	// and Group will load itself into the various users who are members of it.
    	User.Manager.getNumUsers();
    	Group.Manager.getNumGroups();
    	
    	// Confirm we have a signature key available
    	signatureKey = SignatureKey.Manager.getCurrent();
    	if ( signatureKey == null )
    	{
    		err("Application.completePostBootInitialization() - Failed to find an active SignatureKey");
    		return false;
    	}
    	
        Thread.setDefaultUncaughtExceptionHandler( new Thread.UncaughtExceptionHandler() {
        	@Override public void uncaughtException(Thread t, Throwable e) 
        	{
        		except(e,"Application.completePostBootInitialization() - uncaughtException on thread: " + t.getName());
        	}
        });
    	
        try
        {
            backgrounder = new Backgrounder( this );

            Thread thread = new Thread( backgrounder, BACKGROUNDER_THREAD_NAME_PREFIX+contextPath );
            thread.setDaemon(true);
            thread.setPriority(Thread.currentThread().getPriority()-1);
            thread.start();
        }
        catch( Exception e )
        {
            String err = "Application.completePostBootInitialization() - Failed to start the backgrounder...";
            except(e,err);
            return false;
        }
        
        ui = new UI();
        
        try
        {
        	outboundEmailProcessor = new OutboundEmailProcessor( this );

            Thread thread = new Thread( outboundEmailProcessor, OUTBOUND_EMAIL_PROCESSOR_THREAD_NAME_PREFIX+contextPath );
            thread.setDaemon(true);
            thread.setPriority(Thread.currentThread().getPriority()-1);
            thread.start();
        }
        catch( Exception e )
        {
            String err = "Application.completePostBootInitialization() - Failed to start the outboundEmailProcessor...";
            except(e,err);
            return false;
        }
        
        try
        {
        	inboundEmailProcessor = new InboundEmailProcessor( this );

            Thread thread = new Thread( inboundEmailProcessor, INBOUND_EMAIL_PROCESSOR_THREAD_NAME_PREFIX+contextPath );
            thread.setDaemon(true);
            thread.setPriority(Thread.currentThread().getPriority()-1);
            thread.start();
        }
        catch( Exception e )
        {
            String err = "Application.completePostBootInitialization() - Failed to start the inboundEmailProcessor...";
            except(e,err);
            return false;
        }
        
        try
        {
        	httpSendProcessor = new HttpSendProcessor( this );

            Thread thread = new Thread( httpSendProcessor, HTTP_SEND_PROCESSOR_THREAD_NAME_PREFIX+contextPath );
            thread.setDaemon(true);
            thread.setPriority(Thread.currentThread().getPriority()-1);
            thread.start();
        }
        catch( Exception e )
        {
            String err = "Application.completePostBootInitialization() - Failed to start the httpSendProcessor...";
            except(e,err);
            return false;
        }
        
        recordDeploymentHasStarted();
        
    	currentState = AppState.READY;

    	return true;
    }
    
    public SecureRandom getSecureRandom()
    {
    	return secureRandom;
    }
   
    public SecretKeyGenerator getSecretKeyGenerator()
    {
        return secretKeyGenerator;
    }
    
    public SecureHash getSecureHash()
    {
    	return new SecureHash(this,secureHashAlgorithm,secureHashProvider);
    }
    
    public PBE getPBE()
    {
    	return new PBE(this,pbeKeyFactoryAlgorithm,pbeKeyFactoryProvider,pbeCipherAlgorithm,pbeCipherProvider,pbeIterations,pbeSaltSize);
    }
    
    public int getJBCryptSaltIterations()
    {
    	return jbcryptSaltIterations;
    }
    
    public PublicKeyGenerator getPublicKeyGenerator()
    {
    	return publicKeyGenerator;
    }
    
    /**
     * Returns the unique deployment id.
     * @return the EsfUUID for this deployment, which is set at the time the database is first initialized
     */
    public final EsfUUID getDeployId()
    {
        return deployId;
    }
    public final void setDeployId(EsfUUID v)
    {
    	if ( deployId != null )
    		err("Application.setDeployId() - Called when deployId is already set to: " + deployId + ".  Attempted to set with: " + v);
    	else
    		deployId = v;
    }
    
    public final Record getGlobalProperties()
    {
    	return globalProperties;
    }
    
    /**
     * Returns the SessionTracker object that keeps track of all active sessions
     * and the users or parties related to them.
     * @return the SessionTracker object for this web app
     */
    public SessionTracker getSessionTracker()
    {
        return sessionTracker;
    }
    

    // Current session key is in the last position
    public final SessionKey getSessionKey()
    {
        return sessionKeys[sessionKeys.length-1];
    }
    
    public final SessionKey getSessionKey(EsfUUID id)
    {
        for( int i=0; i < sessionKeys.length; ++i )
        {
            if ( sessionKeys[i].getKeyId().equals(id) )
                return sessionKeys[i];
        }
        err("Application.getSessionKey() - Failed to find session key for id: " + id);
        return null;
    }
    
    public SignatureKey getSignatureKey()
    {
    	return signatureKey;
    }
    
    public RandomKey getRandomKey()
    {
        return randomKey;
    }

    public PasswordManager getPasswordManager()
    {
        return passwordManager;
    }
    public boolean hasPasswordManager()
    {
    	return passwordManager != null;
    }

    public ConnectionPool getConnectionPool()
    {
        return dbPool;
    }

    
    public ConnectionPools getConnectionPools()
    {
        return connectionPools;
    }

    public ConnectionPool getConnectionPool(String name)
    {
        return connectionPools.getPool(name);
    }

    public void cleanupStatement(Statement stmt)
    {
        if ( stmt != null )
            try { stmt.close(); } catch( SQLException e ) {}
    }
    public void cleanupEsfPreparedStatement(DatabaseObject.EsfPreparedStatement stmt)
    {
        if ( stmt != null )
            try { stmt.close(); } catch( SQLException e ) {}
    }
    
    public void cleanupPool(ConnectionPool pool, Connection con, Statement stmt)
    {
        cleanupStatement(stmt);
        if ( pool != null )
            pool.putConnection(con);
    }
    public void cleanupPoolAndEsfPreparedStatement(ConnectionPool pool, Connection con, DatabaseObject.EsfPreparedStatement stmt)
    {
    	cleanupEsfPreparedStatement(stmt);
        if ( pool != null )
            pool.putConnection(con);
    }
    
    public ActivityLog getActivityLog()
    {
    	return activityLog;
    }
    public boolean hasActivityLog()
    {
    	return activityLog != null;
    }

    public BlobDb getBlobDb()
    {
    	return blobDb;
    }

    public void notifyOutboundEmailProcessorMessageQueued()
    {
    	outboundEmailProcessor.setEmailMessageQueued();
        synchronized(outboundEmailProcessor)
        {
        	outboundEmailProcessor.notifyAll();
        }
    }

    public void notifyHttpSendProcessorQueued()
    {
    	httpSendProcessor.setHttpSendQueued();
        synchronized(httpSendProcessor)
        {
        	httpSendProcessor.notifyAll();
        }
    }

    
    public final Locale getDefaultLocale()
    {
    	return defaultLocale;
    }
    public final void setDefaultLocale(String localeSpec) 
    {
    	if ( EsfString.isBlank(localeSpec) )
    		return;
	    String[] localeParts	= localeSpec.split("_"); // usually just a language, or a language_country
	    if ( localeParts.length == 1 )
	    	defaultLocale		= new Locale(localeParts[0]);
	    else if ( localeParts.length == 2 || localeParts.length == 3 ) // we ignore any variants
	    	defaultLocale		= new Locale(localeParts[0], localeParts[1]);
    }
    
    public String getDefaultUrlProtocolAddress()
    {
    	return defaultUrlProtocolAddress;
    }
    public final void setDefaultUrlProtocolAddress(String urlPrefix) 
    {
    	defaultUrlProtocolAddress = urlPrefix;
	    updateExternalContextPath();
    }
    
    protected void updateExternalContextPath()
    {
    	if ( EsfString.isNonBlank(contextPath) && ! contextPath.startsWith("/") )
    		contextPath = "/" + contextPath;
        externalContextPath = defaultUrlProtocolAddress + contextPath;
    }
    
    public final void setDefaultTimezone(String tzSpec) 
    {
    	if ( EsfString.isBlank(tzSpec) )
    		return;
	    com.esignforms.open.util.DateUtil.setDefaultTimeZone(tzSpec);
    }
    
    public final String getDefaultCountryCode()
    {
    	return defaultCountryCode;
    }
    public final void setDefaultCountryCode(String v)
    {
    	defaultCountryCode= v;
    }
    
    public final String getDefaultDateFormat()
    {
    	return defaultDateFormat;
    }
    public final void setDefaultDateFormat(String v)
    {
    	defaultDateFormat = v;
    	defaultDateTimeFormat = defaultDateFormat + " " + defaultTimeFormat;
    }
    
    public final String getDefaultTimeFormat()
    {
    	return defaultTimeFormat;
    }
    public final void setDefaultTimeFormat(String v)
    {
    	defaultTimeFormat = v;
    	defaultDateTimeFormat = defaultDateFormat + " " + defaultTimeFormat;
    }

    public final String getDefaultDateTimeFormat()
    {
    	return defaultDateTimeFormat;
    }

    public final String getDefaultLogDateFormat()
    {
    	return defaultLogDateFormat;
    }
    public final void setDefaultLogDateFormat(String v)
    {
    	defaultLogDateFormat = v;
    	defaultLogDateTimeFormat = defaultLogDateFormat + " " + defaultLogTimeFormat;
    }
    
    public final String getDefaultLogTimeFormat()
    {
    	return defaultLogTimeFormat;
    }
    public final void setDefaultLogTimeFormat(String v)
    {
    	defaultLogTimeFormat = v;
    	defaultLogDateTimeFormat = defaultLogDateFormat + " " + defaultLogTimeFormat;
    }
    
    public final String getDefaultLogDateTimeFormat()
    {
    	return defaultLogDateTimeFormat;
    }


    public final MessageFormatFile getServerMessages()
    {
    	return serverMessages;
    }
    private void loadServerMessages()
    {
        try
        {
        	serverMessages = new MessageFormatFile("servermessages",defaultLocale);
        	// Load any commonly needed messages 
        	pageTitlePrefix = serverMessages.getString("app.pageTitlePrefix");
        	loginPage = serverMessages.getString("app.loginPage");
        }
        catch( MissingResourceException e )
        {
        	except(e,"ERROR: Application() is missing its servermessages.properties file");
        }
        
    }
    
    /**
     * Report an exception message to stderr along with a stack trace
     * @param msg the String message to report with the exception
     */
    final public void except( Throwable e, String msg )
    {
        logger.error("Exception: " + e.getMessage() + " - " + msg + ":", e);
    }

    /**
     * Report an exception message to stderr along with a stack trace
     * @param msg the String message to report with the exception
     */
    final public void dexcept( Throwable e, String msg )
    {
        if ( logger.isDebugEnabled() )
        {
            logger.debug("Debug Exception: " + e.getMessage() + " - " + msg + ":", e);
        }
    }

    /**
     * Report an SQL exception message to stderr
     * @param msg the String message to report with the exception
     */
    final public void sqlerr( java.sql.SQLException e, String msg )
    {
        logger.error("SQLException: " + msg + ":");
        while (e != null) 
        {
            logger.error("  Message:   " + e.getMessage(),e);
            logger.error("  SQLState:  " + e.getSQLState());
            logger.error("  ErrorCode: " + e.getErrorCode());
            e = e.getNextException();
        }
    }

    /**
     * Report a fatal message to stderr and exit.
     * @param msg the String message to report
     */
    final public void fatal( String msg )
    {
        logger.fatal(msg);
        System.exit(1);
    }

    /**
     * Report an error message to stderr
     * @param msg the String message to report
     */
    final public void err( String msg )
    {
        logger.error(msg);
    }

    /**
     * Report a warning message to stderr
     * @param msg the String message to report
     */
    final public void warning( String msg )
    {
        logger.warn(msg);
    }

    /**
     * Report a common message to stderr
     * @param msg the String message to report
     */
    final public void info( String msg )
    {
        logger.info(msg);
    }

    /**
     */
    final public boolean isDebug()
    {
        return logger.isDebugEnabled();
    }
    
    final public boolean isDebugEnabled()
    {
        return logger.isDebugEnabled();
    }

    /**
     * Report a common message to stderr
     * @param msg the String message to report
     */
    final public void debug( String msg )
    {
        logger.debug(msg);
    }
    
    /**
     * Report a common message to stdout
     * @param msg the String message to report
     */
    final public void out( String msg )
    {
        String datetime = ( new EsfDateTime() ).toDateTimeMsecString();
        System.out.println( datetime + "-" + msg );
    }

    /**
     * Report a common message to stderr
     * @param msg the String message to report
     */
    final public void stderr( String msg )
    {
        String datetime = ( new EsfDateTime() ).toDateTimeMsecString();
        System.err.println( datetime + "-" + msg );
    }

    public String getMyHostAddress()
    {
        try
        {
            InetAddress myAddr = InetAddress.getLocalHost();
            return myAddr.getHostAddress();
        }
        catch( java.net.UnknownHostException e )
        {
            return "unknown";
        }
    }

    public String getMyHostName()
    {
        try
        {
            return InetAddress.getLocalHost().getHostName();
        }
        catch( java.net.UnknownHostException e )
        {
            return "localhost";
        }
    }
    
    
    private final static String BAD_FILENAME_CHARS = "\\/:*?\"<>|\'#";  // We'll strip any of these chars from the file name
    public String sanitizeFileName(String filename)
    {
    	filename = filename == null ? "file" : filename.trim(); // Files should not have leading/trailing spaces.
        StringBuilder newFname = new StringBuilder(filename.length());
        boolean inSpace = false;
        for( int i=0; i < filename.trim().length(); ++i )
        {
            char c = filename.charAt(i);
            if ( BAD_FILENAME_CHARS.indexOf(c) < 0 )
            {
                if ( Character.isWhitespace(c) )
                {
                    if ( inSpace )
                        continue;
                    inSpace = true;
                }
                else
                    inSpace = false;
                    
                newFname.append(c);
            }
        }
        
        filename = newFname.toString().trim();
    	
        return EsfString.isBlank(filename) ? "file" : filename;
    }


    public void sleep(long millisec)
    {
        try
        {
            Thread.sleep(millisec);
        }
        catch( Exception e ) {}
    }
    public void sleepSeconds(long sec)
    {
    	sleep(sec * 1000);
    }
    
    public byte[] longToByteArray(long v)
    {
        byte[] b = new byte[8]; // a long is 8 bytes long
        b[0]  = (byte)(v >> 56);
        b[1]  = (byte)(v >> 48);
        b[2]  = (byte)(v >> 40);
        b[3]  = (byte)(v >> 32);
        b[4]  = (byte)(v >> 24);
        b[5]  = (byte)(v >> 16);
        b[6]  = (byte)(v >> 8);
        b[7]  = (byte)v;
        return b;
    }
    
    public long byteArrayToLong(byte[] b)
    {
        if ( b.length != 8 )
            return 0;
            
        return ( ( ((long)b[0] & 0xFF) << 56) |
                 ( ((long)b[1] & 0xFF) << 48) |
                 ( ((long)b[2] & 0xFF) << 40) |
                 ( ((long)b[3] & 0xFF) << 32) |
                 ( ((long)b[4] & 0xFF) << 24) |
                 ( ((long)b[5] & 0xFF) << 16) |
                 ( ((long)b[6] & 0xFF) << 8)  |
                 ((long)b[7] & 0xFF)
               );
    }
    
    /*
    public String makePretty1Decimal(double v)
    {
    	synchronized(_decimal1Format)
    	{
    		return _decimal1Format.format(v);
    	}
    }
    public String makePretty1DecimalPercent(double v)
    {
        return makePretty1Decimal(v / 100)+"%";
    }
    public String makePretty1DecimalPercent(long v)
    {
        return makePretty1Decimal(v);
    }
    
    public String makePretty2DecimalDecimal(double v)
    {
    	synchronized(_decimal2Format)
    	{
    		return _decimal2Format.format(v);
    	}
    }   
    public String makePretty2DecimalPercent(double v)
    {
        return makePretty2DecimalDecimal(v / 100)+"%";
    }
    public String makePretty2DecimalPercent(long v)
    {
        return makePretty2DecimalPercent((double)v);
    }
    */
    
    // requires that 'num' be in the range '0-99' to work.
    public final String make2digits( int num ) 
    {
        if ( num < 10 )
            return "0" + num;
        return Integer.toString(num);
    }
    

    /**
     * Converts a number into the ordinal number with the suffix added, so:
     * 1 -> 1st
     * 2 -> 2nd
     * 3 -> 3rd
     * 4 -> 4th
     * ...
     * 99 -> 99th
     * ...
     * @param num the string number
     * @return the string number with the appropriate suffix appended to the end
     */
    public String addOrdinalSuffixToNum(String num)
    {
        if ( EsfString.isBlank(num) )
            return "0th";
        
        String ext;
        
        char lastDigit = num.charAt(num.length()-1);
        
        char penultimateDigit = ' ';
        if ( num.length() >= 2 )
        	penultimateDigit = num.charAt(num.length()-2);
        
        if ( lastDigit == '1' )
            ext = ( penultimateDigit == '1' ) ? "th" : "st";        	
        else if ( lastDigit == '2' )
            ext = ( penultimateDigit == '1' ) ? "th" : "nd";
        else if ( lastDigit == '3' )
            ext = ( penultimateDigit == '1' ) ? "th" : "rd";
        else 
            ext = "th";
        
        return num + ext;
    }

    public String addOrdinalSuffixToNum(int num)
    {
        return addOrdinalSuffixToNum(Integer.toString(num));
    }

    public String addOrdinalSuffixToNum(long num)
    {
        return addOrdinalSuffixToNum(Long.toString(num));
    }


    public final int stringToInt(String s,int def)
    {
        try
        {
            if ( s == null ) return def;
            return Integer.parseInt(s);
        }
        catch( NumberFormatException e )
        {
            return def;
        }
    }
    
    public final long stringToLong(String s,long def)
    {
        try
        {
            if ( s == null ) return def;
            return Long.parseLong(s);
        }
        catch( NumberFormatException e )
        {
            return def;
        }
    }
  
    
    public final short stringToShort(String s,short def)
    {
        try
        {
            if ( s == null ) return def;
            return Short.parseShort(s);
        }
        catch( NumberFormatException e )
        {
            return def;
        }
    }
    
    /**
     * Takes a String of data and returns a string of the same length, but with the 
     * all characters masked to maskWithChar.
     * Embedded punctuation characters remain "as is".
     * @param data the String data to mask
     * @return the String masked
     */
    public String mask(String data)
    {
    	return maskRight(data,'X',0);
    }

    /**
     * Takes a String of data and returns a string of the same length, but with the 
     * starting characters masked to maskWithChar and showing only the last 4 alpha-numerics.
     * Embedded punctuation characters remain "as is".
     * @param data the String data to mask
     * @return the String masked
     */
    public String maskLeft(String data)
    {
    	return maskLeft(data,'X',4);
    }
    /**
     * Takes a String of data and returns a string of the same length, but with the 
     * starting characters masked to maskWithChar and showing only the last showNumRight alpha-numerics.
     * Embedded punctuation characters remain "as is".
     * @param data the String data to mask
     * @param showNumRight the int number of alphanumeric characters to show on the right side
     * @return the String masked
     */
    public String maskLeft(String data, int showNumRight)
    {
    	return maskLeft(data,'X',showNumRight);
    }
    /**
     * Takes a String of data and returns a string of the same length, but with the 
     * starting characters masked to maskWithChar and showing only the last showNumRight alpha-numerics.
     * Embedded punctuation characters remain "as is".
     * @param data the String data to mask
     * @param showNumRight the int number of alphanumeric characters to show on the right side
     * @return the String masked
     */
	public String maskLeft(String data, char maskWithChar, int showNumRight)
    {
        if ( EsfString.isBlank(data) )
            return "";
        
        if ( data.length() <= showNumRight )
            return data;
        
        StringBuilder masked = new StringBuilder(data);
        for( int i=masked.length()-1; i >= 0; --i )
        {
            char c = masked.charAt(i);
            if ( Character.isLetterOrDigit(c) )
            {
                if ( showNumRight > 0 )
                    --showNumRight;
                else
                    masked.setCharAt(i,maskWithChar);
            }
        }
        return masked.toString();
    }
    
    /**
     * Takes a String of data and returns a string of the same length, but with the 
     * ending characters masked to 'X' and showing only the first 4 alpha-numerics.
     * Embedded punctuation characters remain "as is".
     * @param data the String data to mask
     * @return the String masked
     */
    public String maskRight(String data)
    {
    	return maskRight(data,'X',4);
    }
    /**
     * Takes a String of data and returns a string of the same length, but with the 
     * ending characters masked to 'X' and showing only the first showNumLeft alpha-numerics.
     * Embedded punctuation characters remain "as is".
     * @param data the String data to mask
     * @param showNumLeft the int number of alphanumeric characters to show on the left side
     * @return the String masked
     */
    public String maskRight(String data, int showNumLeft)
    {
    	return maskRight(data,'X',showNumLeft);
    }
    /**
     * Takes a String of data and returns a string of the same length, but with the 
     * ending characters masked to maskWithChar and showing only the first showNumLeft alpha-numerics.
     * Embedded punctuation characters remain "as is".
     * @param data the String data to mask
     * @param showNumLeft the int number of alphanumeric characters to show on the left side
     * @return the String masked
     */
	public String maskRight(String data, char maskWithChar, int showNumLeft)
    {
        if ( EsfString.isBlank(data) )
            return "";
        
        if ( data.length() <= showNumLeft )
            return data;
        
        StringBuilder masked = new StringBuilder(data);
        for( int i=showNumLeft; i < masked.length(); ++i )
        {
            char c = masked.charAt(i);
            if ( Character.isLetterOrDigit(c) )
            	masked.setCharAt(i,maskWithChar);
        }
        return masked.toString();
    }
    
    /**
     * Takes a credit card number and returns a string of the same length, but with the 
     * top numbers masked to 'X' and showing only the last 4 digits.
     * @param creditCardNumber the String credit card number to mask
     * @return the String masked credit card number
     */
    public String maskCreditCard(String creditCardNumber)
    {
        if ( EsfString.isBlank(creditCardNumber) )
            return "";
        
        int showDigits = 4;

        if ( creditCardNumber.length() <= showDigits )
            return creditCardNumber;
        
        StringBuilder masked = new StringBuilder(creditCardNumber);
        for( int i=masked.length()-1; i >= 0; --i )
        {
            char c = masked.charAt(i);
            if ( Character.isDigit(c) )
            {
                if ( showDigits > 0 )
                    --showDigits;
                else
                    masked.setCharAt(i,'X');
            }
        }
        return masked.toString();
    }
    
    public boolean isValidZip(String addr)
    {
        String numericZip = EsfString.getOnlyNumeric(addr);
        return numericZip.length() == 5 || numericZip.length() == 9;
    }
    
    public boolean isValidPhone(String phone)
    {
        String numericPhone = EsfString.getOnlyNumeric(phone);
        return numericPhone.length() >= 10; // allow for numbers that include country codes or extensions
    }
    
     
    public String makePrettyPhone(String phone,String separator)
    {
        String numericPhone = EsfString.getOnlyNumeric(phone);
        if ( numericPhone.length() == 10 )
            return numericPhone.substring(0,3) + separator + numericPhone.substring(3,6) + separator + numericPhone.substring(6);
        if ( numericPhone.length() == 7 )
            return numericPhone.substring(0,3) + "-" + numericPhone.substring(3);
        return phone;    
    }
    
    public String makePrettyPhone(String phone)
    {
        return makePrettyPhone(phone,".");
    }
    
    public String makePrettySsn(String ssn)
    {
        String numericSsn = EsfString.getOnlyNumeric(ssn);
        if ( numericSsn.length() != 9 )
            return ssn;
            
        return numericSsn.substring(0,3) + "-" + numericSsn.substring(3,5) + "-" + numericSsn.substring(5);
    }

    public String makePrettyFein(String fein)
    {
        String numericFein = EsfString.getOnlyNumeric(fein);
        if ( numericFein.length() != 9 )
            return fein;
            
        return numericFein.substring(0,2) + "-" +  numericFein.substring(2);
    }

    /**
     * Converts a 5 or 9 digit zip into the format "98033" or "98033-1234".
     */
    public String makePrettyZip(String zipDigits)
    {
        String numericZip = EsfString.getOnlyNumeric(zipDigits);
        if ( numericZip.length() <= 5 )
            return numericZip;
        if ( numericZip.length() > 9 )
            numericZip = numericZip.substring(0,9);
        if ( numericZip.length() > 5 && numericZip.length() != 9 )
            return numericZip.substring(0,5);
            
        return numericZip.substring(0,5) + "-" + numericZip.substring(5);
    }
    
    public boolean getTrueFalseFromUser(java.io.BufferedReader in,String prompt)
        throws Exception
    {
        return getTrueFalseFromUser(in,prompt,false);
    }

    public boolean getTrueFalseFromUser(java.io.BufferedReader in,String prompt,boolean def)
        throws Exception
    {
        String ans = getFromUser(in,prompt,(def)?"true":"false");
        if ( ans.equalsIgnoreCase("true") ||
             ans.equalsIgnoreCase("t")    ||
             ans.equalsIgnoreCase("yes")  ||
             ans.equalsIgnoreCase("y")
           )
            return true;
            
        return false;
    }

    public int getIntFromUser(java.io.BufferedReader in,String prompt,int def)
        throws Exception
    {
        String ans = getFromUser(in,prompt,Integer.toString(def));
        return stringToInt(ans,def);
    }
  
    public int getIntFromUser(java.io.BufferedReader in,String prompt)
        throws Exception
    {
        String ans = getFromUser(in,prompt);
        return stringToInt(ans,0);
    }

    public long getLongFromUser(java.io.BufferedReader in,String prompt,long def)
        throws Exception
    {
        String ans = getFromUser(in,prompt,Long.toString(def));
        return stringToLong(ans,def);
    }

    public long getLongFromUser(java.io.BufferedReader in,String prompt)
        throws Exception
    {
        String ans = getFromUser(in,prompt);
        return stringToLong(ans,0);
    }

    public String getFromUser(java.io.BufferedReader in,String prompt)
        throws Exception
    {
        return getFromUser(in,prompt,null);
    }

    public String getFromUser(java.io.BufferedReader in,String prompt,String def)
        throws Exception
    {
        try
        {
            String ans = null;
                
            while( ans == null || ans.length() < 1 )
            {
                System.out.print(prompt);
                if ( def != null )
                    System.out.print(" [" + def + "] ");
                System.out.print(": ");
                    
                ans = in.readLine();
    
                if ( (ans == null || ans.length() < 1) && def != null )
                {
                    ans = def;
                    break;
                }
            }
            return ans;
        }
        catch( java.io.IOException e )
        {
            System.out.println("I/O Exception: " + e.getMessage());
            throw e;
        }
    }
    
    // Expects that the m and y are valid to calculate the 'd' value
    protected int[] daysInMonth = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    protected boolean isValidNumberOfDaysForMonth(int m, int d, int y)
    {
        if ( d < 1 )
            return false;
        if ( m == 2 )
        {
            int maxFebDays = ( ((y % 4 == 0) && ( (!(y % 100 == 0)) || (y % 400 == 0)) ) ? 29 : 28 );
            return d <= maxFebDays;
        }
        
        return d <= daysInMonth[m];
    }

    public byte[] decompress(byte[] data,int decompressedSize)
    {
        try
        {
            ByteArrayInputStream bis = new ByteArrayInputStream(data);
            GZIPInputStream      gis = new GZIPInputStream(bis,4096);
        
            int numCanRead = decompressedSize;
            byte[] inflated = new byte[decompressedSize];
            int numRead = gis.read(inflated,0,numCanRead);
            int total   = numRead;
            while( total < decompressedSize )
            {
                numCanRead -= numRead;
                numRead = gis.read(inflated,total,numCanRead);
                if ( numRead < 0 )
                    break;
                total += numRead;
            }
            if ( total != decompressedSize )
            {
                Application.getInstance().err("Application.decompress() - Read " + total + " bytes, but expected " + decompressedSize +
                        " from compressed buffer size of " + data.length);
                return null;
            }
            numRead = gis.read(inflated);
            if ( numRead > 0 )
            {
                Application.getInstance().err("Application.decompress() - Read an additional " + numRead + " bytes, but expected EOF");
                return null;
            }
            gis.close();
            return inflated;
        }
        catch( Exception e )
        {
            Application.getInstance().except(e,"Application.decompress()");
        }
        
        return null;
    }
    
    
    public byte[] compress(byte[] data)
    {
        if ( data == null || data.length == 0 )
            return data;
            
        ByteArrayOutputStream bos = null;
        GZIPOutputStream      gos = null;
        try
        {
            bos = new ByteArrayOutputStream(data.length);
            gos = new GZIPOutputStream(bos);
            gos.write(data);
            gos.finish();
            gos.flush();
            
            byte[] compressed = bos.toByteArray();

            Application.getInstance().debug("Application.compress() - orig.length="+data.length+"; compressed.length="+compressed.length);
            
            gos.close();
            gos = null;
            bos = null;
            
            return compressed;
        }
        catch( java.io.IOException e )
        {
            Application.getInstance().except(e,"Application.compress()");
            return data; // not compressed
        }
        finally
        {
            if ( gos != null )
            {
                try
                {
                    gos.close();
                }
                catch( Exception e ) {}
            }
            else if ( bos != null )
            {
                try
                {
                    bos.close();
                }
                catch( Exception e ) {}
            }
        }
    }

    
    public boolean isContentTypeImage(String contentType)
    {
        return CONTENT_TYPE_GIF.equals(contentType)  ||
               CONTENT_TYPE_JPEG.equals(contentType) ||
               CONTENT_TYPE_PJPEG.equals(contentType) ||
               CONTENT_TYPE_XPNG.equals(contentType) ||
               CONTENT_TYPE_PNG.equals(contentType);
    }
    public boolean isContentTypeBrowserSafe(String contentType)
    {
        return CONTENT_TYPE_HTML.equals(contentType) ||
               CONTENT_TYPE_TEXT.equals(contentType) || 
               isContentTypeImage(contentType);
    }
    public boolean isContentTypeBrowserSafeOrPDF(String contentType)
    {
        return CONTENT_TYPE_PDF.equals(contentType) || isContentTypeBrowserSafe(contentType);
    }
    public boolean isContentTypeBrowserSafeOrPDFOrXML(String contentType)
    {
        return CONTENT_TYPE_XML.equals(contentType) || CONTENT_TYPE_TEXT_XML.equals(contentType) || isContentTypeBrowserSafeOrPDF(contentType);
    }
    
    public String getContentTypeByFileName(String fileName)
    {
    	if ( servletContext != null )
    	{
    		String mime = servletContext.getMimeType(fileName);
    		if ( mime != null )
    			return mime;
    	}
    	
    	// For non-servlet running environments, use what we have here....
        String filename = fileName.toLowerCase();
        if ( filename.endsWith(".pdf") )
            return CONTENT_TYPE_PDF;
        if ( filename.endsWith(".doc") )
            return CONTENT_TYPE_MSWORD;
        if ( filename.endsWith(".xls") )
            return CONTENT_TYPE_MSEXCEL;
        if ( filename.endsWith(".csv") )
            return CONTENT_TYPE_CSV;
        if ( filename.endsWith(".zip") )
            return CONTENT_TYPE_ZIP;
        if ( filename.endsWith(".htm") || filename.endsWith(".html") )
            return CONTENT_TYPE_HTML;
        if ( filename.endsWith(".xml") )
            return CONTENT_TYPE_XML;
        if ( filename.endsWith(".rtf") )
            return CONTENT_TYPE_RTF;
        if ( filename.endsWith(".txt") )
            return CONTENT_TYPE_TEXT;
        if ( filename.endsWith(".gif") )
            return CONTENT_TYPE_GIF;
        if ( filename.endsWith(".jpg") || filename.endsWith(".jpeg") )
            return CONTENT_TYPE_JPEG;
        if ( filename.endsWith(".png") )
        	return CONTENT_TYPE_PNG;
        return CONTENT_TYPE_BINARY;
    }
    
    public String getWebServerVersion()
    {
    	return servletContext.getServerInfo();
    }
    
    public String getServletResourceAsString(String path)
    {
    	InputStream inputStream = null;
    	try
    	{
    		inputStream = servletContext.getResourceAsStream(path);
    		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, EsfString.CHARSET_UTF_8));
    		StringBuffer resourceData = new StringBuffer(4096);
            char[] buf = new char[4096];
            int numRead;
            while((numRead=reader.read(buf)) != -1)
            	resourceData.append(buf, 0, numRead);
            return resourceData.toString();
    	}
    	catch(Exception e)
    	{
    		except(e,"Application.getServletResourceAsString() - " + path);
    		return null;
    	}
    	finally
    	{
    		if ( inputStream != null )
    			try { inputStream.close(); } catch(Exception e2) {}
    	}
    }
    
    protected long calculateTotalFileSize(File basePath)
    {
    	long totalSize = 0;
    	
    	for( File file : basePath.listFiles() )
    	{
    		if ( file.isDirectory() )
    			totalSize += calculateTotalFileSize(file);
    		else
    			totalSize += file.length();
    	}
    	
    	return totalSize;
    }
    
    public long getDatabaseSize()
    {
        try
        {
        	File base = new File(getMyDeploymentBasePath());
        	if ( ! base.isDirectory() )
        	{
        		err("Application.getDatabaseSize() -- Deployment base directory not found: " + base.getAbsolutePath());
        		return 0;
        	}
        	
        	File dbBase = new File(base,"current");
        	if ( ! dbBase.isDirectory() )
        	{
        		err("Application.getDatabaseSize() -- Database directory not found: " + dbBase.getAbsolutePath());
        		return 0;
        	}
        	
        	return calculateTotalFileSize(dbBase);
        }
        catch( Exception e )
        {
        	except(e,"getDatabaseSize()");
        	return 0;
        }
    }
    
    public long getArchiveSize()
    {
        try
        {
        	File base = new File(getMyDeploymentBasePath());
        	if ( ! base.isDirectory() )
        	{
        		err("Application.getArchiveSize() -- Deployment base directory not found: " + base.getAbsolutePath());
        		return 0;
        	}
        	
        	File archiveBase = new File(base,"archive");
        	if ( ! archiveBase.isDirectory() )
        	{
        		err("Application.getArchiveSize() -- Archive directory not found: " + archiveBase.getAbsolutePath());
        		return 0;
        	}
        	
        	return calculateTotalFileSize(archiveBase);
        }
        catch( Exception e )
        {
        	return 0;
        }
    }
    

    
    private void recordDeploymentHasStarted()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        PreparedStatement stmt = null;
        
        try
        {
        	systemStartedTimestamp = new EsfDateTime();
        	getActivityLog().logSystemStartStop(con, "Started " + Version.getReleaseString() + " for deployment id " + getDeployId());
            stmt = con.prepareStatement("UPDATE esf_deployment SET last_started_timestamp = ?");
            stmt.setTimestamp(1,systemStartedTimestamp.toSqlTimestamp());
            
            int num = stmt.executeUpdate();
            if ( num != 1 )
                warning("Application.recordDeploymentHasStarted() - Failed to record started; numUpdated: " + num);
           con.commit();
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            cleanupPool(pool,con,null);
        }    	
    }
    
    private void recordDeploymentHasStopped()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        PreparedStatement stmt = null;
        
        try
        {
        	getActivityLog().logSystemStartStop(con, "Stopped " + Version.getReleaseString());
            stmt = con.prepareStatement("UPDATE esf_deployment SET last_stopped_timestamp = ?");
            stmt.setTimestamp(1,DateUtil.getCurrentSqlTimestamp());
            
            int num = stmt.executeUpdate();
            if ( num != 1 )
                warning("Application.recordDeploymentHasStopped() - Failed to record stopped; numUpdated: " + num);
           con.commit();
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            cleanupPool(pool,con,null);
        }    	
    }

    public final synchronized void destroy()
    {
        debug("Application.destroy() called");
        
        if ( currentState.ordinal() >= AppState.READY.ordinal() )
        	recordDeploymentHasStopped();
        
        currentState = AppState.SHUTTING_DOWN;
        
        if ( httpSendProcessor != null )
        {
        	httpSendProcessor.stop();
            synchronized(httpSendProcessor)
            {
            	httpSendProcessor.notifyAll();
            	httpSendProcessor = null;
            }
        }
        
        if ( inboundEmailProcessor != null )
        {
        	inboundEmailProcessor.stop();
            synchronized(inboundEmailProcessor)
            {
            	inboundEmailProcessor.notifyAll();
                inboundEmailProcessor = null;
            }
        }
        
        if ( outboundEmailProcessor != null )
        {
        	outboundEmailProcessor.stop();
            synchronized(outboundEmailProcessor)
            {
            	outboundEmailProcessor.notifyAll();
                outboundEmailProcessor = null;
            }
        }
        
        if ( backgrounder != null )
        {
            backgrounder.stop();
            synchronized(backgrounder)
            {
                backgrounder.notifyAll();
                backgrounder = null;
            }
        }
        
        if ( sessionTracker != null )
        {
            sessionTracker.destroy();
            sessionTracker = null;
        }
        
        // We will wait up to 10 seconds to see if the threads will stop before will close the database
        long stopIfOlderThanTimestamp = System.currentTimeMillis() + (10 * 1000);
        do
        {
        	ThreadChecker tc = new ThreadChecker();
        	
        	tc.joinHttpSend(250);
        	tc.joinInboundEmail(250);
        	tc.joinOutboundEmail(250);
        	tc.joinBackgrounder(250);
        	
        	boolean isHttpSendRunning = tc.isHttpSendRunning();
        	boolean isInboundEmailRunning = tc.isInboundEmailRunning();
        	boolean isOutboundEmailRunning = tc.isOutboundEmailRunning();
        	boolean isBackgrounderRunning = tc.isBackgrounderRunning();
        	
        	if ( isHttpSendRunning ||
        		 isInboundEmailRunning ||
        		 isOutboundEmailRunning ||
        		 isBackgrounderRunning
        	   )
        	{
            	debug("Waiting for all background threads to stop... isHttpSendRunning: " + isHttpSendRunning + 
            			"; isInboundEmailRunning: " + isInboundEmailRunning +
            			"; isOutboundEmailRunning: " + isOutboundEmailRunning +
            			"; isBackgrounderRunning: " + isBackgrounderRunning);
        	}
        	else
        	{
            	debug("All background threads have stopped.");
            	stopIfOlderThanTimestamp = 0;
        	}
        } while ( System.currentTimeMillis() < stopIfOlderThanTimestamp );
        
        if ( connectionPools != null )
        {
            connectionPools.destroy();
            connectionPools = null;
            dbPool          = null;
        }
        
        activityLog			= null;
        blobDb				= null;
        globalProperties	= null;
        passwordManager		= null;
        
        serverMessages  = null;
        secureRandom	= null;
        randomKey		= null;
        secretKeyGenerator = null;
        publicKeyGenerator = null;
        sessionKeys		= null;
        signatureKey    = null;
        logger			= null;
        _app			= null;
    }
}