// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open;

import java.util.ArrayList;

import com.esignforms.open.data.EsfString;

/**
 * This class holds info/warning/error messages.  
 * @author Yozons, Inc.
 */
public class ErrorEntry
	implements java.io.Serializable
{
	private static final long serialVersionUID = -5480544100869582163L;

	private String[] fields;
    private String	msg; 
    private String	type;
    private boolean isHtml;

    protected ErrorEntry() {}   // for serialization only
    
    public ErrorEntry(String msg, String type, boolean isHtml, String... fields)
	{
   		setFields(fields);
    	if ( type == null )
    		this.type = Errors.TYPE_ERROR;
    	else
    	{
    		type = type.toLowerCase();
    		if ( Errors.TYPE_ERROR.equals(type) 	|| 
    			 Errors.TYPE_WARNING.equals(type)	|| 
    			 Errors.TYPE_SUCCESS.equals(type)	||
    			 Errors.TYPE_INFO.equals(type)
    		   )
    			this.type = type;
    		else
    			this.type = Errors.TYPE_ERROR;
    	}
		this.msg     = (msg == null) ? "" : msg.trim();
		this.isHtml  = isHtml;
	}

    public String[] getFields()	{ return fields; }
	public String  getMessage()	{ return msg; }
	public String  getType()	{ return type; }
	public String  getTypeCSS() { return type + "-ErrorEntry"; }
	
	public boolean isErrorField(String checkField) 
	{
		if ( isError() && fields != null )
		{
			for( String field : fields )
			{
				if ( field.equalsIgnoreCase(checkField) )
					return true;
			}
		}
		
		return false;
	}
	
	public boolean isErrorOrWarningField(String checkField)
	{
		if ( isErrorOrWarning() && fields != null )
		{
			for( String field : fields )
			{
				if ( field.equalsIgnoreCase(checkField) )
					return true;
			}
		}
		
		return false;
	}
	
	public boolean isError()   	{ return Errors.TYPE_ERROR.equals(type); }
	public boolean isWarning()  { return Errors.TYPE_WARNING.equals(type); }
	public boolean isErrorOrWarning() { return isError() || isWarning(); }
	public boolean isSuccess()  { return Errors.TYPE_SUCCESS.equals(type); }
	public boolean isInfo()  	{ return Errors.TYPE_INFO.equals(type); }
	public boolean isSuccessOrInfo() { return isSuccess() || isInfo(); }
	public boolean isHtml()		{ return isHtml; }
	public boolean isText()		{ return ! isHtml; }
	

	void setFields(String... fieldParams)
	{
		if ( fieldParams != null )
		{
			ArrayList<String> list = new ArrayList<String>(fieldParams.length);
			for( String f : fieldParams )
			{
				if ( ! EsfString.isBlank(f) )
					list.add(f);
			}
			fields = new String[list.size()];
			list.toArray(fields);
		}
		else
			fields = new String[0];
	}
}
 