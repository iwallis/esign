// Copyright (C) 2012-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//

package com.esignforms.open.integration.paypal;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.StringTokenizer;

import com.esignforms.open.Errors;
import com.esignforms.open.data.EsfBoolean;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfMoney;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.util.CreditCardValidator;
import com.esignforms.open.util.ServletUtil;


/**
 * Handles integration with PayPal's NVP system that does credit card validation and payment.
 * Last know to work on API version 92.0.
 * 
 * @author Yozons, Inc.
 * 
 */
public class NvpDirectPayment
{
	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(NvpDirectPayment.class);

	private static final String PAYPAL_NVP_API_VERSION 	= "92.0";
	private static final String PAYPAL_NVP_METHOD 		= "DoDirectPayment";
	private static final String PAYPAL_NVP_PAYMENTACTION= "Sale";
	
	// When a transaction used is in Test mode, we'll use the TEST/ properties which are generally
	// defined to use the PayPal NVP Sandbox.
	public static final EsfName 	 PROPSET_PAYPAL_NVP_API				= new EsfName("PayPal_NVP_API");
	
	private static final EsfPathName PROP_PAYPAL_NVP_USER			= new EsfPathName("USER");
	private static final EsfPathName PROP_PAYPAL_NVP_PASSWORD		= new EsfPathName("PWD");
	private static final EsfPathName PROP_PAYPAL_NVP_SIGNATURE		= new EsfPathName("SIGNATURE");
	private static final EsfPathName PROP_PAYPAL_NVP_URL 			= new EsfPathName("URL");
	private static final EsfPathName PROP_PAYPAL_NVP_LOG_NVP		= new EsfPathName("LogNVP");
	
	Connection con;
	TransactionContext context;
	Errors errors;
	
	// Charging params given to PayPal
	private String cardType;   	          // Visa | MasterCard | Discover | Amex | Switch | Solo
	private String cardAccount;   		  // Numbers only
	private String cardExpMonthYear; 	  // MMYYYY
	private String cardCVV2;              // 3-4 chars
	private String cardAmount;            // <= $10,000 USD
	private String currencyCode;          //
	private String cardBillingFirstName;  // Max 25 chars
	private String cardBillingLastName;   // Max 25 chars
	private String cardBillingAddr; 	  // Max 100 chars
	private String cardBillingAddr2; 	  // Max 100 chars
	private String cardBillingCity; 	  // Max 40 chars
	private String cardBillingState;      // 2 char abbreviation or max 40 chars
	private String cardBillingZip;        // Max 20 chars
	private String cardBillingCountry;    // 2 char abbreviation
	private String custom;                // Max 256 chars
	private String description;           // Max 127 chars
	private String email;                 // Max 127 chars
	private String ipAddr;                // Max 15 chars
	private String invoiceNumber;		  // Max 127 chars
	
	// PayPal response
	private String ack;
	private String transactionId;
	private String avsCode;
	private String amountCharged;
	private String cvv2Match;
	
	private int attempts = 0;

	// Set by properties
	private String user;
	private String password;
	private String signature;
	private String url;
	private boolean logNvp;
	
	    
	public NvpDirectPayment(Connection con, TransactionContext context, Errors errors)
	{
		if ( con == null || context == null || context.hasNoTransaction() || errors == null)
		{
			_logger.error("NvpDirectPayment created with no database connection, context or transaction.");
			return;
		}
		
		this.context	= context;
		this.errors		= errors;

		user 		= context.transaction.getPropertyStringValue(PROPSET_PAYPAL_NVP_API, PROP_PAYPAL_NVP_USER);
		password 	= context.transaction.getPropertyStringValue(PROPSET_PAYPAL_NVP_API, PROP_PAYPAL_NVP_PASSWORD);
		signature 	= context.transaction.getPropertyStringValue(PROPSET_PAYPAL_NVP_API, PROP_PAYPAL_NVP_SIGNATURE);
		url 		= context.transaction.getPropertyStringValue(PROPSET_PAYPAL_NVP_API, PROP_PAYPAL_NVP_URL);
		logNvp		= EsfBoolean.toBoolean( context.transaction.getPropertyStringValue(PROPSET_PAYPAL_NVP_API, PROP_PAYPAL_NVP_LOG_NVP) );
	}
	
	private boolean hasPropertiesSet()
	{
		return EsfString.areAllNonBlank(user,password,signature,url);
	}
	
	public void doCreditCardPayments( String customerNameParam,  // if non-blank, put in CUSTOM field of API
									  String descriptionParam,  	// if blank, "Online sale" is put in DESC field of API
									  String emailParam, 		// if non-blank, put in EMAIL field of API
								      String invoiceNumberParam,  // if blank, use context.transaction.getId()
								      
									  String ccTypeParam,  		// Visa | MasterCard | Discover | Amex | Maestro (varies by country what's allowed); if blank, we try to determine it
								      String ccFirstNameParam,
								      String ccLastNameParam,
								      String ccBillingAddressParam,
								      String ccBillingAddress2Param, // optional
								      String ccCityParam,
								      String ccStateParam,		// or province
								      String ccZipParam,			// or other postal code
								      String ccCountryCodeParam, // if blank, defaults to "US"
								      String ccAccountNumParam,  // Okay to have embedded hyphens
								      EsfDate ccExpirationParam,
								      String ccSecurityIdParam,  // CVV2, optional but generally required
								      EsfMoney ccAmountParam,
								      String ipAddrParam
                                    )
		throws EsfException
	{
		if ( ! hasPropertiesSet() )
		{
			_logger.error("paypal.NvpDirectPayment.doCreditCardPayments() - Missing property definitions.");
			throw new EsfException("Missing property definitions for credit card integration");
		}
		
		clearPayPalResponse();
		
		this.cardAccount 		= EsfString.getOnlyNumeric(ccAccountNumParam);
		this.cardType        	= EsfString.isBlank(ccTypeParam) ? CreditCardValidator.cardNumberToVendorString(this.cardAccount) : ccTypeParam;
		this.cardBillingFirstName= EsfString.ensureLength(ccFirstNameParam,25);
		this.cardBillingLastName= EsfString.ensureLength(ccLastNameParam,25);
		this.cardExpMonthYear 	= normalizeExpirationDate(ccExpirationParam);
		this.cardCVV2 			= EsfString.ensureLength(ccSecurityIdParam,4);
		this.cardBillingAddr 	= EsfString.ensureLength(ccBillingAddressParam,100); 
		this.cardBillingAddr2 	= EsfString.ensureLength(ccBillingAddress2Param,100); 
		this.cardBillingCity 	= EsfString.ensureLength(ccCityParam,40); 
		this.cardBillingState 	= EsfString.ensureLength(ccStateParam,2); 
		this.cardBillingZip 	= EsfString.ensureLength(ccZipParam,20); 
		this.cardBillingCountry = EsfString.isBlank(ccCountryCodeParam) ? "US" : EsfString.ensureLength(ccCountryCodeParam,2);
		this.cardAmount         = normalizeAmount(ccAmountParam);
		this.currencyCode		= ccAmountParam.getCurrencyCode();
		
		this.custom             = EsfString.ensureLength(customerNameParam,256);
		this.description        = EsfString.isBlank(descriptionParam) ? "Online sale" : EsfString.ensureLength(descriptionParam,127);
		// Can't really truncate an email or IP address and have it make sense, so if longer, then no email will be used.
		EsfEmailAddress emailAddress = new EsfEmailAddress(emailParam);
		this.email              = emailAddress.isValid() ? emailAddress.getEmailAddress() : null;
		this.email				= ( EsfString.getLength(this.email) > 127 ) ? null : this.email;
		this.ipAddr         	= ( EsfString.getLength(ipAddrParam) > 15 ) ? "" : ipAddrParam;
		this.invoiceNumber 		= EsfString.isBlank(invoiceNumberParam) ? context.transactionId.toString() : invoiceNumberParam;
		this.invoiceNumber     	= EsfString.ensureLength(this.invoiceNumber,127);
		
		try
		{
			doPayment();
		}
		catch( EsfException e )
		{
			throw e;
		}
	}
	
	public String getAck()
	{
		return ack;
	}
	public String getTransactionId()
	{
		return transactionId;
	}
	public String getAmountCharged()
	{
		return amountCharged;
	}
	
	public String getAvsCode()
	{
		return avsCode;
	}
	public boolean isAvsCodeMatch()
	{
		return "X".equals(avsCode) || "Y".equals(avsCode);
	}
	public boolean isAvsCodeRetry()
	{
		return "R".equals(avsCode);
	}
	public boolean isAvsCodeDecline()
	{
		return "C".equals(avsCode) || "E".equals(avsCode) || "N".equals(avsCode);
	}
	public String getAvsCodeMeaning()
	{
		if ( isAvsCodeMatch() )
			return "Match";
		if ( isAvsCodeDecline() )
			return "Declined";
		if ( "A".equals(avsCode) || "B".equals(avsCode) || "D".equals(avsCode) || "F".equals(avsCode) )
			return "Incorrect zip code";
		if ( "G".equals(avsCode) || "I".equals(avsCode) )
			return "International failure";
		if ( "P".equals(avsCode) || "W".equals(avsCode) || "Z".equals(avsCode) )
			return "Incorrect address";
		return "Unexpected (" + avsCode + ")";
	}

	public String getCvv2Match()
	{
		return cvv2Match;
	}
	public boolean isCvv2Match()
	{
		return "M".equals(cvv2Match); // Matched CVV2
	}
	public boolean isCvv2NoMatch()
	{
		return "N".equals(cvv2Match);
	}
	public String getCvv2MatchMeaning()
	{
		if ( isCvv2Match() )
			return "Match";
		if ( isCvv2NoMatch() )
			return "No match";
		if ( "P".equals(cvv2Match) )
			return "Not processed";
		if ( "S".equals(cvv2Match) )
			return "Service not supported";
		if ( "U".equals(cvv2Match) )
			return "Service not available";
		if ( "X".equals(cvv2Match) )
			return "No response";
		return "Unexpected (" + cvv2Match + ")";
	}
	
	// ******************** Non-public routines to handle PayPal processing.

	void clearPayPalResponse()
	{
		ack = transactionId = avsCode = amountCharged = cvv2Match = null;
		attempts = 0;
	}
	
	String normalizeExpirationDate(EsfDate expirationDate)
	{
		return expirationDate.format("MMyyyy");
	}
	
	String normalizeAmount(EsfMoney paymentAmount)
	{
		return paymentAmount.format("###########0.00");
	}
		
	StringBuilder appendNVP(StringBuilder buf, String name, String value)
	{
		if ( buf.length() > 0 )
			buf.append("&");
		// we don't encode the names since we happen to know in advance they are okay
		buf.append(name).append("=").append(ServletUtil.urlEncode(value==null?"":value)); 
		return buf;
	}
	
	
	void doPayment()
		throws EsfException
	{
		StringBuilder paymentRequest = new StringBuilder(2000);
		appendNVP(paymentRequest,"USER",user);
		appendNVP(paymentRequest,"PWD",password);
		appendNVP(paymentRequest,"VERSION",PAYPAL_NVP_API_VERSION);
		appendNVP(paymentRequest,"SIGNATURE",signature);
		
		appendNVP(paymentRequest,"METHOD",PAYPAL_NVP_METHOD);
		appendNVP(paymentRequest,"PAYMENTACTION",PAYPAL_NVP_PAYMENTACTION);
		appendNVP(paymentRequest,"IPADDRESS",ipAddr);
		
		appendNVP(paymentRequest,"CREDITCARDTYPE",cardType);
		appendNVP(paymentRequest,"ACCT",cardAccount);
		appendNVP(paymentRequest,"EXPDATE",cardExpMonthYear);
		appendNVP(paymentRequest,"CVV2",cardCVV2);
		
		if ( EsfString.isNonBlank(email) )
			appendNVP(paymentRequest,"EMAIL",email);
		appendNVP(paymentRequest,"FIRSTNAME",cardBillingFirstName);
		appendNVP(paymentRequest,"LASTNAME",cardBillingLastName);
		
		appendNVP(paymentRequest,"STREET",cardBillingAddr);
		if ( EsfString.isNonBlank(cardBillingAddr2) )
			appendNVP(paymentRequest,"STREET2",cardBillingAddr);
		appendNVP(paymentRequest,"CITY",cardBillingCity);
		appendNVP(paymentRequest,"STATE",cardBillingState);
		appendNVP(paymentRequest,"ZIP",cardBillingZip);
		appendNVP(paymentRequest,"COUNTRYCODE",cardBillingCountry);
		appendNVP(paymentRequest,"AMT",cardAmount);
		appendNVP(paymentRequest,"CURRENCYCODE",currencyCode);
		if ( EsfString.isNonBlank(description) )
			appendNVP(paymentRequest,"DESC",description);
		if ( EsfString.isNonBlank(custom) )
			appendNVP(paymentRequest,"CUSTOM",custom);
		appendNVP(paymentRequest,"INVNUM",invoiceNumber);
		String buttonSourceMax32 = EsfString.ensureLength(context.transactionId.toString().replaceAll("-", ""),32);
		appendNVP(paymentRequest,"BUTTONSOURCE",buttonSourceMax32);

		String paymentRequestString = paymentRequest.toString();
		
        if ( logNvp )
        	_logger.info("paypal.NvpDirectPayment.doPayment() Request: " + paymentRequestString + "; length: " + paymentRequestString.length() + "; URL: " + url);
        
		++attempts; 		
		String paymentResponseString = sendPost(paymentRequestString);
		
        if ( logNvp )
        	_logger.info("paypal.NvpDirectPayment.doPayment() Response: " + paymentResponseString);
        
        HashMap<String,String> responseData = createResponseData(paymentResponseString);
        if ( responseData.size() < 1 )
        {
        	_logger.warn("paypal.NvpDirectPayment.doPayment() - Response was empty from URL: " + url);
            throw new EsfException("No response from the credit card processor.");
        }
        
		transactionId = responseData.get("TRANSACTIONID");
		avsCode       = responseData.get("AVSCODE");
		amountCharged = responseData.get("AMT");
		cvv2Match 	  = responseData.get("CVV2MATCH");
        ack 		  = responseData.get("ACK");
        
        if ( "Success".equals(ack) || "SuccessWithWarning".equals(ack) )
        {
        	processSuccess(responseData);
        }
        else if ( "Error".equals(ack) || "Failure".equals(ack) || "FailureWithWarning".equals(ack) || "Warning".equals(ack) )
        {
        	String errors = processErrors(responseData);
        	_logger.warn("paypal.NvpDirectPayment.doPayment() - Error response: " + errors);
            throw new EsfException(errors);
        }
        else
        {
        	_logger.warn("paypal.NvpDirectPayment.doPayment() - Unexpected ACK response: " + ack);
            throw new EsfException("Unexpected response from the credit card processor.");
        }
	}
	
	
	void processSuccess(HashMap<String,String> responseData)
		throws EsfException
	{
		if ( isAvsCodeMatch() && isCvv2Match() )
		{
        	_logger.info("paypal.NvpDirectPayment.processSuccess() - Payment success. PayPal tranId: " + transactionId + 
        			  "; amount: " + amountCharged + "; on attempt: " + attempts);
			return;
		}
		
		if ( isAvsCodeRetry() )
		{
			if ( attempts < 2 ) 
			{
	        	_logger.warn("paypal.NvpDirectPayment.processSuccess() - AVS Retry requested: " + avsCode + "; on attempt: " + attempts);
	        	try 
	        	{
					context.transaction.logActionDetail(con,"Retrying PayPal credit card payment");
				} 
	        	catch (SQLException e) 
	        	{
		        	_logger.warn("paypal.NvpDirectPayment.processSuccess() - Log retrying to transaction failed", e);
				}
				doPayment(); // let's try again
				return;
			}
			throw new EsfException("Address verification retry limit exceeded.");
		}
		
		// 2/19/2014 stopped these checks as they can be tweaked by PayPal, and if PayPal says it's an ACK=Success, it did go through
		// even with AVS or CVV issues. If you need these, make sure PayPal is configured correctly for risk controls to require this.
		/* 
		if ( isAvsCodeDecline() )
		{
        	_logger.warn("paypal.NvpDirectPayment.processSuccess() - AVS Decline: " + avsCode + 
        				 "; meaning: " + getAvsCodeMeaning() + "; on attempt: " + attempts);
			throw new EsfException("Address verification declined: " + getAvsCodeMeaning());			
		}
		
		if ( isCvv2NoMatch() )
		{
        	_logger.warn("paypal.NvpDirectPayment.processSuccess() - CVV Decline: " + cvv2Match + 
   				 "; meaning: " + getCvv2MatchMeaning() + "; on attempt: " + attempts);
        	throw new EsfException("CVV verification declined: " + getCvv2MatchMeaning());			
		}
		*/

    	_logger.warn("paypal.NvpDirectPayment.processSuccess() - MIXED 'successful' ACK with CVV2 match state: " + cvv2Match + 
  				 	"; meaning: " + getCvv2MatchMeaning() + 
  				 	"; AVS code: " + avsCode + "; meaning: " + getAvsCodeMeaning() +
  				 	"; amount: " + amountCharged + "; PayPal transaction id: " + transactionId +
  				 	"; on attempt: " + attempts);
    	// But it said success, so we'll assume the charge went through despite things.
       	//throw new EsfException("Unexpected credit card processor response.");			
	}
	
	String processErrors(HashMap<String,String> responseData)
	{
		StringBuilder errors = new StringBuilder();
		errors.append("Credit card processing error. ");
		
		boolean hasGatewayDecline = false;
		
		for( int i=0; i < 10; ++i ) // we'll just do the first 10 if that many
		{			
			String errorCode = responseData.get("L_ERRORCODE"+i);
			if ( EsfString.isBlank(errorCode) )
				break;
			
			if ( i > 0 )
				errors.append("\n");
			
			String shortMessage = responseData.get("L_SHORTMESSAGE"+i);
			String longMessage  = responseData.get("L_LONGMESSAGE"+i);
			//String severityCode = responseData.get("L_SEVERITYCODE"+i);
			
			errors.append(shortMessage).append(" (").append(errorCode).append(") - ").append(longMessage);
			if ( ! longMessage.endsWith(".") )
				errors.append(".");

			if ( ! hasGatewayDecline )
				hasGatewayDecline = "Gateway Decline".equals(shortMessage);
		}
		
		if ( isAvsCodeDecline() )
		{
        	_logger.warn("paypal.NvpDirectPayment.processErrors() - AVS Decline: " + avsCode + 
        				 "; meaning: " + getAvsCodeMeaning() + "; on attempt: " + attempts);
			errors.append("\nThere is a problem with the address verification check: " + getAvsCodeMeaning() + ". Please confirm the address entered is the card's billing address.");		
		}
		
		if ( isCvv2NoMatch() )
		{
        	_logger.warn("paypal.NvpDirectPayment.processErrors() - CVV Decline: " + cvv2Match + 
   				 "; meaning: " + getCvv2MatchMeaning() + "; on attempt: " + attempts);
			errors.append("\nThere was a problem with the security code check: " + getCvv2MatchMeaning() + ". Please confirm the 3-4 digit security code entered.");
		}		

		if ( hasGatewayDecline )
			errors.append("\nPlease contact your credit card company to authorize this web payment.");
		
		return errors.toString();
	}
	
	HashMap<String,String> createResponseData(String response)
	{
		HashMap<String,String> map = new HashMap<String,String>();
		StringTokenizer st = new StringTokenizer(response,"&");
		while( st.hasMoreTokens() )
		{
			String nameValue = st.nextToken().trim(); // trim removes any newlines at the end
			StringTokenizer st2 = new StringTokenizer(nameValue,"=");
			if ( st2.countTokens() == 2 )
			{
				map.put(ServletUtil.urlDecode(st2.nextToken()), ServletUtil.urlDecode(st2.nextToken()));
			}
		}
		return map;
	}
	
	// Submits the data values to PayPal
    String sendPost( String nameValues )
		throws EsfException
	{
		java.net.HttpURLConnection con = null;
		java.io.BufferedReader     br  = null;
	
		try
		{
			java.net.URL paypalUrl = null;
	        try
	        {
	        	paypalUrl = new java.net.URL( url );
	        }
	        catch( Exception e ) 
	        {
	        	_logger.warn("paypal.NvpDirectPayment.sendPost() - Skipped sending because could not create URL using: " + url);
	            throw new EsfException("Could not create URL to credit card processor.");
	        }
	        
			byte[] postDataBytes = EsfString.stringToBytes(nameValues);
			int contentLength = postDataBytes.length;
			
			// Create the connection and set the connection properties
			con = (java.net.HttpURLConnection)paypalUrl.openConnection();
			con.setRequestMethod("POST");
			con.setAllowUserInteraction(false);  // this is program, so no popups
			con.setDoInput(true);      // we will read from the URL
			con.setDoOutput(true);     // we will write to the URL
			con.setIfModifiedSince(0); // always get
			con.setUseCaches(false);   // don't use cache
			con.setRequestProperty("Content-Length", String.valueOf(contentLength));
			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
			con.setRequestProperty("Accept", "*/*");
			con.setRequestProperty("HTTP_USER_AGENT", "Yozons-Open-eSignForms");
			
			con.connect();
	
			// Construct the POST message
			java.io.OutputStream os = con.getOutputStream();
			os.write( postDataBytes );
			os.close();
			
			java.io.InputStream is = con.getInputStream();
			if ( is == null )
			{
				_logger.warn("paypal.NvpDirectPayment.sendPost() - No input response was returned from PayPal URL: " + url);
	  			throw new EsfException("No response from the credit card processor.");
			}
	
			contentLength = con.getContentLength();
			
			if ( contentLength < 1 )
				contentLength = 2 * 1024;
	
			StringBuilder response = new StringBuilder(contentLength+32);
			
			br = new java.io.BufferedReader( new java.io.InputStreamReader(con.getInputStream()));
			String str;
			while ( (str = br.readLine()) != null )
			{
				if ( EsfString.isBlank(str) )
					continue;
				response.append(str).append('\n');
			}
			br.close(); br = null;
	
			str = response.toString();
			
			return str;
		}
		catch( java.io.IOException e )
		{
			_logger.error("paypal.NvpDirectPayment.sendPost() - Failed to get input stream from URL: " + url, e);
			try
			{
				java.io.InputStream is = con == null ? null : con.getErrorStream();
				if ( is != null )
				{
					br = new java.io.BufferedReader( new java.io.InputStreamReader(is));
					StringBuilder response = new StringBuilder(1024);
					String str;
					while ( (str = br.readLine()) != null )
					{
						response.append(str).append('\n');
					}
					br.close(); br = null;
	
					str = response.toString();
					
					_logger.debug("paypal.NvpDirectPayment.sendPost() - ** ERROR response is:");
					_logger.debug(str);
					_logger.debug("** End of ERROR response.");
					return str;
				}

				_logger.warn("paypal.NvpDirectPayment.sendPost() - Failed to get error stream from URL: " + url);
			}
			catch( Exception e2 ) {}
		
			_logger.warn("paypal.NvpDirectPayment.sendPost() - Failed to contact URL: " + url);
			throw new EsfException("Could not contact the credit card processor.");
		}
		finally
		{
			try
			{
				if ( br != null )
					br.close();
				if ( con != null )
					con.disconnect();
			}
			catch( IOException e ) {}
		}
	}
    
}


