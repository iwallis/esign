// Copyright (C) 2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//

package com.esignforms.open.integration.apspayroll;

import java.io.DataOutputStream;
import java.io.IOException;
import java.sql.Connection;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.crypto.SecureHash;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfEmailAddress;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfMoney;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.Record;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.runtime.action.APS_EIP_PseudoAction;
import com.esignforms.open.runtime.workflow.TransactionContext;

// TODO: If this beta code becomes generally useful, make into a configurable Action for custom logic.
/**
 * Handles integration with APS Payroll's Enterprise Integration Platform (EIP).  
 * Currently, this works assuming your transaction has a specialized document named APS_EIP with 
 * corresponding fields that should be transmitted to APS Payroll.
 * 
 * It sets the 'xml' and 'aps_response_http_status' fields in the APS_EIP document on a given attempt,
 * and it blocks subsequent submits if it's had a aps_response_http_status==200 before (successful submit).
 * 
 * @author Yozons, Inc.
 * 
 */
public class APS_EIP
{
	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(APS_EIP.class);

	public static final EsfName 	 PROPSET_APS_EIP	= new EsfName("APS_EIP");

	private static final EsfPathName PROP_ACCESS_TOKEN	= new EsfPathName("access_token");
	private static final EsfPathName PROP_COMPANY_CODE	= new EsfPathName("company_code");
	private static final EsfPathName PROP_EMAIL_ADDRESS	= new EsfPathName("email_address");
	private static final EsfPathName PROP_URL 			= new EsfPathName("URL");

	public static final EsfName 	 APS_EIP_DOCUMENT_NAME			= new EsfName("APS_EIP");
	public static final EsfName		 APS_XML_FIELD 					= new EsfName("xml");
	public static final EsfName		 APS_RESPONSE_HTTP_STATUS_FIELD = new EsfName("aps_response_http_status");
	
	private static final String LINE_END    = "\r\n";
	private static final String TWO_HYPHENS = "--";

	private static SecureHash md5Hash;

	TransactionContext context;
	Errors errors;
	
	TransactionDocument apsEipDocument;

	// Set by properties
	private String access_token;
	private String company_code;
	private String email_address;
	private String url;
	
	    
	public APS_EIP(TransactionContext context, Errors errors)
	{
		this.context	= context;
		this.errors		= errors;
		
		if ( ! hasContextSet() )
		{
			_logger.error("APS_EIP created with no context or transaction.");
			return;
		}
		
		this.apsEipDocument = context.transaction.getTransactionDocument(APS_EIP_DOCUMENT_NAME);
		if ( ! hasApsEipDocument() )
		{
			String msg = "APS_EIP created with context transaction id: " + context.transaction.getId() + "; but required document named: " + APS_EIP_DOCUMENT_NAME + " is missing.";
			context.transaction.logGeneral(msg);
			errors.addError(msg);
		}

		access_token 	= context.transaction.getPropertyStringValue(PROPSET_APS_EIP, PROP_ACCESS_TOKEN);
		company_code 	= context.transaction.getPropertyStringValue(PROPSET_APS_EIP, PROP_COMPANY_CODE);
		email_address 	= context.transaction.getPropertyStringValue(PROPSET_APS_EIP, PROP_EMAIL_ADDRESS);
		if ( EsfString.isNonBlank(email_address) && ! EsfEmailAddress.isValidEmail(email_address) )
		{
			String msg = "APS_EIP created with invalid email_address property value: " + email_address + "; defined in the property set: " + PROPSET_APS_EIP + ". No transmission error reports will be sent by APS.";
			context.transaction.logGeneral(msg);
			email_address = null;
		}
		url 			= context.transaction.getPropertyStringValue(PROPSET_APS_EIP, PROP_URL);
		if ( ! hasPropertiesSet() )
		{
			String msg = "APS_EIP created with missing properties defined in the property set: " + PROPSET_APS_EIP;
			context.transaction.logGeneral(msg);
			errors.addError(msg);
		}
		
		EsfInteger responseCode = apsEipDocument.getRecord().getIntegerByName( APS_RESPONSE_HTTP_STATUS_FIELD );
		if ( responseCode != null && responseCode.equals(200) )
		{
			errors.addError("You have already successfully submitted this employee's information to APS EIP.");
		}
	}
	
	private boolean hasContextSet()
	{
		return context != null && context.hasTransaction() && errors != null;
	}
	private boolean hasPropertiesSet()
	{
		return EsfString.areAllNonBlank(access_token,company_code,url); // email_address is optional
	}
	private boolean hasApsEipDocument()
	{
		return apsEipDocument != null;
	}
	
	public boolean buildXML()
	{
		if ( hasContextSet() && hasApsEipDocument() )
		{
			EsfInteger responseCode = apsEipDocument.getRecord().getIntegerByName( APS_RESPONSE_HTTP_STATUS_FIELD );
			if ( responseCode != null && responseCode.equals(200) )
			{
				errors.addError("You have already successfully submitted this employee's information to APS EIP. No XML re-generation performed.");
				return false;
			}
		}

		StringBuilder buf = new StringBuilder(4096);
		
		buf.append("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
		buf.append("<transaction>\n");
		buf.append("  <employees>\n");
		if ( hasApsEipDocument() )
			appendEmployee(buf);
		buf.append("  </employees>\n");
		buf.append("</transaction>\n");
		
		if ( hasApsEipDocument() )
		{
			apsEipDocument.getRecord().addUpdate(APS_XML_FIELD, new EsfString(buf.toString()));
			return true;
		}
		
		if ( hasContextSet() )
		{
			String msg = "The data field 'XML' in document '" + APS_EIP_DOCUMENT_NAME + "' is not defined, so we cannot submit to APS EIP.";
			errors.addError(msg);
			context.transaction.logGeneral("APS_EIP.buildXml() - " + msg);
		}
		else
			_logger.warn("buildXML() - Missing document: " + APS_EIP_DOCUMENT_NAME + "; cannot save generated XML: " + buf.toString());
		return false;
	}
	
	void appendEmployee(StringBuilder buf)
	{
		Record data = apsEipDocument.getRecord();
		EsfString address1 = data.getStringByName(new EsfName("address1"));
		EsfString address2 = data.getStringByName(new EsfName("address2"));
		EsfString bdept = data.getStringByName(new EsfName("bdept"));
		EsfDate birthdate = data.getDateByName(new EsfName("birthdate"));
		EsfString blocation = data.getStringByName(new EsfName("blocation"));
		EsfString city = data.getStringByName(new EsfName("city"));
		EsfString eeo_c = data.getStringByName(new EsfName("eeo_c"));
		EsfString eeo_e = data.getStringByName(new EsfName("eeo_e"));
		EsfString email = data.getStringByName(new EsfName("email"));
		EsfString emptype = data.getStringByName(new EsfName("emptype"));
		EsfMoney fedtax_amount = data.getMoneyByName(new EsfName("fedtax_amount"));
		EsfInteger fedtax_exemptions = data.getIntegerByName(new EsfName("fedtax_exemptions"));
		EsfString fedtax_status = data.getStringByName(new EsfName("fedtax_status"));
		EsfString fedtax_type = data.getStringByName(new EsfName("fedtax_type"));
		EsfString first = data.getStringByName(new EsfName("first"));
		EsfString gender = data.getStringByName(new EsfName("gender"));
		EsfDate hiredate = data.getDateByName(new EsfName("hiredate"));
		EsfString hphone = data.getStringByName(new EsfName("hphone"));
		EsfString last = data.getStringByName(new EsfName("last"));
		EsfString maritalstatus = data.getStringByName(new EsfName("maritalstatus"));
		EsfString middle = data.getStringByName(new EsfName("middle"));
		EsfString mphone = data.getStringByName(new EsfName("mphone"));
		EsfString paygroup = data.getStringByName(new EsfName("paygroup"));
		EsfMoney salary = data.getMoneyByName(new EsfName("salary"));
		EsfString ssn = data.getStringByName(new EsfName("ssn"));
		EsfString state = data.getStringByName(new EsfName("state"));
		EsfMoney statetax_amount = data.getMoneyByName(new EsfName("statetax_amount"));
		EsfInteger statetax_dependents = data.getIntegerByName(new EsfName("statetax_dependents"));
		EsfInteger statetax_exemptions = data.getIntegerByName(new EsfName("statetax_exemptions"));
		EsfString statetax_state = data.getStringByName(new EsfName("statetax_state"));
		EsfString statetax_status = data.getStringByName(new EsfName("statetax_status"));
		EsfString statetax_type = data.getStringByName(new EsfName("statetax_type"));		
		EsfString suffix = data.getStringByName(new EsfName("suffix"));		
		EsfString type = data.getStringByName(new EsfName("type"));
		EsfString unemp_state = data.getStringByName(new EsfName("unemp_state"));
		EsfString uniqueid = data.getStringByName(new EsfName("uniqueid"));
		EsfString wagetype = data.getStringByName(new EsfName("wagetype"));
		EsfString wccode = data.getStringByName(new EsfName("wccode"));
		EsfString zip = data.getStringByName(new EsfName("zip"));
		
		EsfInteger department = data.getIntegerByName( new EsfName("department") );
		EsfMoney rateone = data.getMoneyByName( new EsfName("rateone") );
		EsfInteger defaultreghours = data.getIntegerByName( new EsfName("defaultreghours") );
		
		EsfString accounttype1 = data.getStringByName( new EsfName("accounttype1") );
		EsfString routingnum1 = data.getStringByName( new EsfName("routingnum1") );
		EsfString accountnum1 = data.getStringByName( new EsfName("accountnum1") );
		EsfString paytype1 = data.getStringByName( new EsfName("paytype1") );
		EsfMoney payamount1 = data.getMoneyByName( new EsfName("payamount1") );

		EsfString accounttype2 = data.getStringByName( new EsfName("accounttype2") );
		EsfString routingnum2 = data.getStringByName( new EsfName("routingnum2") );
		EsfString accountnum2 = data.getStringByName( new EsfName("accountnum2") );
		EsfString paytype2 = data.getStringByName( new EsfName("paytype2") );
		EsfMoney payamount2 = data.getMoneyByName( new EsfName("payamount2") );
		
		EsfString e_name1 = data.getStringByName( new EsfName("e_name1") );
		EsfString e_hphone1 = data.getStringByName( new EsfName("e_hphone1") );
		EsfString e_wphone1 = data.getStringByName( new EsfName("e_wphone1") );
		EsfString e_mphone1 = data.getStringByName( new EsfName("e_mphone1") );
		EsfString e_relation1 = data.getStringByName( new EsfName("e_relation1") );
		
		EsfString e_name2 = data.getStringByName( new EsfName("e_name2") );
		EsfString e_hphone2 = data.getStringByName( new EsfName("e_hphone2") );
		EsfString e_wphone2 = data.getStringByName( new EsfName("e_wphone2") );
		EsfString e_mphone2 = data.getStringByName( new EsfName("e_mphone2") );
		EsfString e_relation2 = data.getStringByName( new EsfName("e_relation2") );
		
		EsfString e_name3 = data.getStringByName( new EsfName("e_name3") );
		EsfString e_hphone3 = data.getStringByName( new EsfName("e_hphone3") );
		EsfString e_wphone3 = data.getStringByName( new EsfName("e_wphone3") );
		EsfString e_mphone3 = data.getStringByName( new EsfName("e_mphone3") );
		EsfString e_relation3 = data.getStringByName( new EsfName("e_relation3") );
		
		buf.append("    <employee uniqueid=\"").append(uniqueid.getOnlyNumericString()).append("\">\n");
		buf.append("      <demographics>\n");
		appendOptionalElement(buf,"ssn",ssn.getOnlyNumeric(),9);
		appendOptionalElement(buf,"type",type,4);
		appendOptionalElement(buf,"first",first,30);
		appendOptionalElement(buf,"middle",middle,30);
		appendOptionalElement(buf,"last",last,30);
		appendOptionalElement(buf,"suffix",suffix,3);
		appendOptionalElement(buf,"address1",address1,50);
		appendOptionalElement(buf,"address2",address2,50);
		appendOptionalElement(buf,"city",city,50);
		appendOptionalElement(buf,"state",state,2);
		appendOptionalElement(buf,"zip",zip.getOnlyNumeric(),5);
		appendOptionalElement(buf,"email",email,50);
		appendOptionalElement(buf,"hphone",hphone.getOnlyNumeric(),10);
		appendOptionalElement(buf,"mphone",mphone.getOnlyNumeric(),10);
		appendOptionalElement(buf,"bdept",bdept,15);
		appendOptionalElement(buf,"blocation",blocation,30);
		appendOptionalElement(buf,"wccode",wccode,10);
		appendOptionalElement(buf,"fedtax-status",fedtax_status,1);
		appendOptionalElement(buf,"fedtax-exemptions",fedtax_exemptions,2);
		appendOptionalElement(buf,"fedtax-type",fedtax_type,1);
		if ( ! fedtax_type.equals("N") )
			appendOptionalElement(buf,"fedtax-amount",fedtax_amount,10);
		appendOptionalElement(buf,"statetax-state",statetax_state,2);
		appendOptionalElement(buf,"unemp-state",unemp_state,2);
		appendOptionalElement(buf,"statetax-status",statetax_status,1);
		appendOptionalElement(buf,"statetax-exemptions",statetax_exemptions,2);
		appendOptionalElement(buf,"statetax-dependents",statetax_dependents,2);
		appendOptionalElement(buf,"statetax-type",statetax_type,1);
		if ( ! statetax_type.equals("N") )
			appendOptionalElement(buf,"statetax-amount",statetax_amount,10);
		appendOptionalElement(buf,"paygroup",paygroup,30);
		appendOptionalElement(buf,"wagetype",wagetype,1);
		appendOptionalElement(buf,"salary",salary,12);
		appendOptionalElement(buf,"birthdate",birthdate);
		appendOptionalElement(buf,"hiredate",hiredate);
		appendOptionalElement(buf,"maritalstatus",maritalstatus,1);
		appendOptionalElement(buf,"gender",gender,1);
		appendOptionalElement(buf,"emptype",emptype,1);
		appendOptionalElement(buf,"eeo-e",eeo_e,1);
		appendOptionalElement(buf,"eeo-c",eeo_c,1);
		if ( ! department.isNull() && ! rateone.isNull() && ! defaultreghours.isNull() )
		{
			buf.append("        <rates>\n");
			buf.append("          <rate department=\"").append(department.toPlainString()).append("\" rateone=\"").append(rateone.toPlainString(2)).append("\" defaultreghours=\"").append(defaultreghours.toPlainString()).append("\" />\n");
			buf.append("        </rates>\n");
		}
		if ( accounttype1.isNonBlank() && routingnum1.isNonBlank() && accountnum1.isNonBlank() && ! payamount1.isNull() )
		{
			buf.append("        <accounts>\n");
			
			buf.append("          <account accounttype=\"").append(accounttype1.toPlainString()).append("\" routingnum=\"").append(routingnum1.getOnlyNumeric()).append("\" accountnum=\"").append(accountnum1.getOnlyNumeric()).append("\" paytype=\"").append(paytype1.toPlainString()).append("\" payamount=\"").append(paytype1.equals("R")?"0":payamount1.toPlainString(2)).append("\" />\n");
			if ( accounttype2.isNonBlank() && routingnum2.isNonBlank() && accountnum2.isNonBlank() && ! payamount2.isNull() )
				buf.append("          <account accounttype=\"").append(accounttype2.toPlainString()).append("\" routingnum=\"").append(routingnum2.getOnlyNumeric()).append("\" accountnum=\"").append(accountnum2.getOnlyNumeric()).append("\" paytype=\"").append(paytype2.toPlainString()).append("\" payamount=\"").append(paytype2.equals("R")?"0":payamount2.toPlainString(2)).append("\" />\n");
			
			buf.append("        </accounts>\n");
		}
		if ( e_name1.isNonBlank() || e_name2.isNonBlank() || e_name3.isNonBlank() )
		{
			buf.append("        <econtacts>\n");

			if ( e_name2.isNonBlank() )
			{
				buf.append("          <ec name=\"").append(ensureLength(e_name1,60)).append("\"");
				if ( e_hphone1.isNonBlank() )
					buf.append(" hphone=\"").append(ensureLength(e_hphone1.getOnlyNumeric(),10)).append("\"");
				if ( e_wphone1.isNonBlank() )
					buf.append(" wphone=\"").append(ensureLength(e_wphone1.getOnlyNumeric(),10)).append("\"");
				if ( e_mphone1.isNonBlank() )
					buf.append(" mphone=\"").append(ensureLength(e_mphone1.getOnlyNumeric(),10)).append("\"");
				if ( e_relation1.isNonBlank() )
					buf.append(" relation=\"").append(ensureLength(e_relation1.getOnlyNumeric(),100)).append("\"");
				buf.append(" />\n");
			}
			
			if ( e_name2.isNonBlank() )
			{
				buf.append("          <ec name=\"").append(ensureLength(e_name2,60)).append("\"");
				if ( e_hphone2.isNonBlank() )
					buf.append(" hphone=\"").append(ensureLength(e_hphone2.getOnlyNumeric(),10)).append("\"");
				if ( e_wphone2.isNonBlank() )
					buf.append(" wphone=\"").append(ensureLength(e_wphone2.getOnlyNumeric(),10)).append("\"");
				if ( e_mphone2.isNonBlank() )
					buf.append(" mphone=\"").append(ensureLength(e_mphone2.getOnlyNumeric(),10)).append("\"");
				if ( e_relation2.isNonBlank() )
					buf.append(" relation=\"").append(ensureLength(e_relation2.getOnlyNumeric(),100)).append("\"");
				buf.append(" />\n");
			}
			
			if ( e_name3.isNonBlank() )
			{
				buf.append("          <ec name=\"").append(ensureLength(e_name3,60)).append("\"");
				if ( e_hphone3.isNonBlank() )
					buf.append(" hphone=\"").append(ensureLength(e_hphone3.getOnlyNumeric(),10)).append("\"");
				if ( e_wphone3.isNonBlank() )
					buf.append(" wphone=\"").append(ensureLength(e_wphone3.getOnlyNumeric(),10)).append("\"");
				if ( e_mphone3.isNonBlank() )
					buf.append(" mphone=\"").append(ensureLength(e_mphone3.getOnlyNumeric(),10)).append("\"");
				if ( e_relation3.isNonBlank() )
					buf.append(" relation=\"").append(ensureLength(e_relation3.getOnlyNumeric(),100)).append("\"");
				buf.append(" />\n");
			}
			
			buf.append("        </econtacts>\n");
		}
		
		buf.append("      </demographics>\n");
		buf.append("    </employee>\n");
	}
	
	EsfString ensureLength(EsfString v, int maxLength)
	{
		if ( v != null && v.getLength() > maxLength )
			v = new EsfString( EsfString.ensureTrimmedLength(v.toPlainString(), maxLength) );
		return v;
	}
	
	void appendOptionalElement(StringBuilder buf, String name, EsfString value, int maxLength)
	{
		if ( value != null && value.isNonBlank() )
		{
			if ( value.getLength() > maxLength )
				value = ensureLength(value, maxLength);
			buf.append("        <").append(name).append(">").append(value).append("</").append(name).append(">\n");
		}
	}
	void appendOptionalElement(StringBuilder buf, String name, EsfMoney value, int maxLength)
	{
		if ( value != null )
		{
			EsfString stringValue = new EsfString( value.toPlainString(2) );
			appendOptionalElement(buf,name,stringValue,maxLength);
		}
	}
	void appendOptionalElement(StringBuilder buf, String name, EsfInteger value, int maxLength)
	{
		if ( value != null )
		{
			EsfString stringValue = new EsfString( value.toPlainString() );
			appendOptionalElement(buf,name,stringValue,maxLength);
		}
	}
	void appendOptionalElement(StringBuilder buf, String name, EsfDate value)
	{
		if ( value != null )
		{
			EsfString stringValue = new EsfString( value.toYMDString() );
			appendOptionalElement(buf,name,stringValue,10);
		}
	}
	
	public boolean queuePseudoAction()
	{
		if ( ! hasContextSet() )
		{
			_logger.error("sendXML() - No context/transaction has been set so cannot send queue pseudo-action to submit XML to APS EIP.");
			return false;
		}
		
		APS_EIP_PseudoAction action = new APS_EIP_PseudoAction(this);
		context.setDocumentCompletedEventPseudoAction(action);
		return true;
	}
	
	
	public boolean sendXML(Connection con, TransactionContext context, Errors errors)
	{		
		if ( ! hasContextSet() || context == null )
		{
			_logger.error("sendXML() - No context/transaction has been set so cannot send XML to APS EIP.");
			return false;
		}
		if ( ! this.context.transactionId.equals(context.transactionId) )
		{
			_logger.error("sendXML() - No context's transaction id has changed so cannot send XML to APS EIP. Original transaction id: " +
					this.context.transactionId + "; action specified transaction id: " + context.transactionId);
			return false;
		}

		if ( ! hasApsEipDocument() )
		{
			String msg = "Missing document: " + APS_EIP_DOCUMENT_NAME + "; cannot send XML to APS EIP.";
			errors.addError(msg);
			context.transaction.logGeneral("APS_EIP.sendXml() - " + msg);
			return false;
		}

		if ( ! hasPropertiesSet() )
		{
			String msg = "Missing properties defined in the property set: " + PROPSET_APS_EIP + "; cannot send XML to APS EIP.";
			context.transaction.logGeneral("APS_EIP.sendXml() - " + msg);
			errors.addError(msg);
			return false;
		}
		
		Record data = apsEipDocument.getRecord();

		EsfInteger responseCode = data.getIntegerByName( APS_RESPONSE_HTTP_STATUS_FIELD );
		if ( responseCode != null && responseCode.equals(200) )
		{
			errors.addError("You have already successfully submitted this employee's information to APS EIP.");
			return false;
		}
		
		EsfString xml = data.getStringByName(APS_XML_FIELD);
		if ( xml == null || xml.isBlank() )
		{
			String msg = "Missing 'xml' field in document: " + APS_EIP_DOCUMENT_NAME + "; cannot send XML to APS EIP.";
			errors.addError(msg);
			context.transaction.logGeneral("APS_EIP.sendXml() - " + msg);
			return false;
		}

		try
		{
			int httpStatus = postToAPS(xml);
			data.addUpdate(APS_RESPONSE_HTTP_STATUS_FIELD, new EsfInteger(httpStatus),false);
			data.addUpdate(new EsfName("aps_response_http_status_text"), new EsfString(getHttpStatusMeaning(httpStatus)),false);
			
			if ( httpStatus != 200 )
			{
				String msg = "Error sending XML data to APS EIP: " + getHttpStatusMeaning(httpStatus) + " (" + httpStatus +")";
				errors.addError(msg);
				context.transaction.logGeneral("APS_EIP.sendXml() - " + msg);
				return false;
			}

			String msg = "Successfully sent XML data to APS EIP: " + getHttpStatusMeaning(httpStatus);
			errors.addSuccess(msg);
			context.transaction.logGeneral("APS_EIP.sendXml() - " + msg);
			context.blockFurtherPartyEdits(APS_EIP_DOCUMENT_NAME, new EsfName("sendToAPS")); // on success, cannot undo this
			return true;
		}
		catch ( EsfException e )
		{
			String msg = "Exception communicating with APS EIP: " + e.getMessage();
			errors.addError(msg);
			context.transaction.logGeneral("APS_EIP.sendXml() - " + msg);
			_logger.error("sendXml() - " + msg,e);
			return false;
		}
	}
	
	
	int postToAPS(EsfString xml)
			throws EsfException
	{
		java.net.HttpURLConnection con = null;
		java.io.BufferedReader     br  = null;
		
		String boundary = "-----------------------------" + System.currentTimeMillis();
		int httpStatus = 0;
	
		try
		{
			java.net.URL apsUrl = null;
	        try
	        {
	        	apsUrl = new java.net.URL( url );
	        }
	        catch( Exception e ) 
	        {
	        	_logger.warn("postToAPS() - Skipped sending because could not create URL using: " + url);
	            throw new EsfException("Could not create URL to APS EIP.");
	        }
	        	        
	        byte[] authAccessToken;
	        if ( access_token.equals("testing123") )
	        	authAccessToken = EsfString.stringToBytes(access_token);
	        else
	        {
	        	if ( md5Hash == null )
	        		md5Hash = new SecureHash(Application.getInstance(),"MD5","BC");
		        String ymd = EsfString.getOnlyNumeric( (new EsfDate()).toYMDString() );
		        authAccessToken = md5Hash.getDigest(access_token + ymd);
	        }
	        
			// Create the connection and set the connection properties
			con = (java.net.HttpURLConnection)apsUrl.openConnection();
			con.setRequestMethod("POST");
			con.setAllowUserInteraction(false);  // this is program, so no popups
			con.setDoInput(true);      // we will read from the URL
			con.setDoOutput(true);     // we will write to the URL
			con.setIfModifiedSince(0); // always get
			con.setUseCaches(false);   // don't use cache
			con.setRequestProperty("Content-Type", "multipart/form-data; boundary="+boundary); 
			con.setRequestProperty("Accept", "* /*");
			con.setRequestProperty("HTTP_USER_AGENT", "Yozons-Open-eSignForms");
			
			con.connect();
	
			// Construct the POST message
			java.io.DataOutputStream dos = new DataOutputStream( con.getOutputStream() );
			
			addNameFileParam(dos,boundary,"file",xml.toPlainString(),"file.xml",Application.CONTENT_TYPE_XML);
			addNameValueParam(dos,boundary,"company_code",company_code);
			addNameValueParam(dos,boundary,"access_token",authAccessToken);
	        if ( EsfString.isNonBlank(email_address) )
				addNameValueParam(dos,boundary,"email_address",email_address);
	        
	        // Put in ending boundary
	    	dos.writeBytes(TWO_HYPHENS + boundary + TWO_HYPHENS + LINE_END);
	    	dos.flush();
			dos.close();
			
			httpStatus = con.getResponseCode();

			java.io.InputStream is = con.getInputStream();
			if ( is == null )
			{
				_logger.warn("postToAPS() - No input response was returned from APS EIP URL: " + url);
	  			throw new EsfException("No response from the APS EIP.");
			}
	
			int contentLength = con.getContentLength();
			
			if ( contentLength < 1 )
				contentLength = 2 * 1024;
	
			StringBuilder response = new StringBuilder(contentLength+32);
			
			br = new java.io.BufferedReader( new java.io.InputStreamReader(con.getInputStream()));
			String str;
			while ( (str = br.readLine()) != null )
			{
				if ( EsfString.isBlank(str) )
					continue;
				response.append(str).append('\n');
			}
			br.close(); br = null;
	
			str = response.toString();
			
			return httpStatus;
		}
		catch( java.io.IOException e )
		{
			_logger.error("postToAPS() - Failed to get input stream from URL: " + url, e);
			try
			{
				java.io.InputStream is = con == null ? null : con.getErrorStream();
				if ( is != null )
				{
					br = new java.io.BufferedReader( new java.io.InputStreamReader(is));
					StringBuilder response = new StringBuilder(1024);
					String str;
					while ( (str = br.readLine()) != null )
					{
						response.append(str).append('\n');
					}
					br.close(); br = null;
	
					str = response.toString();
					
					_logger.debug("postToAPS() - ** ERROR response is:");
					_logger.debug(str);
					_logger.debug("** End of ERROR response.");
					return httpStatus;
				}

				_logger.warn("postToAPS() - Failed to get error stream from URL: " + url);
			}
			catch( Exception e2 ) {}
		
			_logger.warn("postToAPS() - Failed to contact URL: " + url);
			throw new EsfException("Could not contact the APS EIP.");
		}
		finally
		{
			try
			{
				if ( br != null )
					br.close();
				if ( con != null )
					con.disconnect();
			}
			catch( IOException e ) {}
		}
	}
	
    void addNameValueParam(DataOutputStream dos, String boundary, String name, String value) 
    	throws IOException
    {
    	dos.writeBytes(TWO_HYPHENS + boundary + LINE_END); 
    	dos.writeBytes("Content-Disposition: form-data; name=\""+name+"\"" + LINE_END + LINE_END);
    	dos.writeBytes(value);
    	dos.writeBytes(LINE_END);
    }
 
    void addNameValueParam(DataOutputStream dos, String boundary, String name, byte[] value) 
        	throws IOException
    {
    	dos.writeBytes(TWO_HYPHENS + boundary + LINE_END); 
    	dos.writeBytes("Content-Disposition: form-data; name=\""+name+"\"" + LINE_END);
    	dos.writeBytes("Content-Type: " + Application.CONTENT_TYPE_BINARY + LINE_END + LINE_END);
    	//dos.writeBytes("Content-Transfer-Encoding: binary" + LINE_END + LINE_END);
    	dos.write(value);
    	dos.writeBytes(LINE_END);
    }
     
    void addNameFileParam(DataOutputStream dos, String boundary, String name, String value, String filename, String contentType) 
    	throws IOException
    {
    	dos.writeBytes(TWO_HYPHENS + boundary + LINE_END); 
    	dos.writeBytes("Content-Disposition: form-data; name=\""+name+"\"; filename=\""+filename+"\"" + LINE_END);
    	dos.writeBytes("Content-Type: " + contentType + LINE_END + LINE_END);
    	//dos.writeBytes("Content-Transfer_Encoding: binary" + LINE_END + LINE_END);
    	dos.writeBytes(value);
    	dos.writeBytes(LINE_END);
    }
     
    String getHttpStatusMeaning(int httpStatus)
    {
    	if ( httpStatus == 200 )
    		return "OK. Transmission and file validated against schema.";
    	if ( httpStatus == 400 )
    		return "Bad Request. POST request was malformed or syntactically incorrect.";
    	if ( httpStatus == 401 )
    		return "Unauthorized. Security Token was not provided.";
    	if ( httpStatus == 403 )
    		return "Forbidden. Security Token was provided but was not valid.";
    	if ( httpStatus == 422 )
    		return "Unprocessable Entity. XML file failed schema validation.";
    	return "Unxpected response code (" + httpStatus + ") received from APS EIP.";
    }
}


