// Copyright (C) 2013-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.integration.httpsend;

import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.config.Literals;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;

/**
* HttpSendResponse holds the results of an attempt to HTTP Send using HttpSendRequest.
* 
* @author Yozons, Inc.
*/
public class HttpSendResponse
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<HttpSendResponse>
{
	private static final long serialVersionUID = -1338041429143320178L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(HttpSendResponse.class);
    
    public static final String STATUS_SUCCESS = "S";
    public static final String STATUS_FAILURE = "F";

    protected final EsfUUID httpSendRequestId;
    protected EsfDateTime sentTimestamp;
    protected EsfDateTime responseTimestamp;
    protected String status;
    protected int httpStatusCode;
    protected String httpResponseContentType;
    protected String httpResponse;
    
    /**
     * This creates a HttpSendResponse object from data retrieved from the DB.
     */
    protected HttpSendResponse(EsfUUID httpSendRequestId, EsfDateTime sentTimestamp, EsfDateTime responseTimestamp, String status,
    						   int httpStatusCode, String httpResponseContentType, String httpResponse
    		                  )
    {
        this.httpSendRequestId = httpSendRequestId;
        this.sentTimestamp = sentTimestamp;
    	this.responseTimestamp = responseTimestamp;
        this.status = EsfString.ensureTrimmedLength(status,1);
        this.httpStatusCode = httpStatusCode;
        this.httpResponseContentType = EsfString.ensureTrimmedLength(httpResponseContentType,Literals.MIME_TYPE_MAX_LENGTH);
        this.sentTimestamp = sentTimestamp;
        this.httpResponse = EsfString.ensureTrimmedLength(httpResponse, Literals.HTTP_RESPONSE_MAX_LENGTH);
    }
    
    public final EsfUUID getHttpSendRequestId()
    {
        return httpSendRequestId;
    }
    
    public EsfDateTime getSentTimestamp()
    {
    	return sentTimestamp;
    }
    
    public EsfDateTime getResponseTimestamp()
    {
    	return responseTimestamp;
    }
    
    public String getStatus()
    {
    	return status;
    }
    
    public int getHttpStatusCode()
    {
    	return httpStatusCode;
    }
    
    public String getHttpResponseContentType()
    {
    	return httpResponseContentType;
    }
    
    public String getHttpResponse()
    {
    	return httpResponse;
    }

    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof HttpSendResponse )
        {
        	HttpSendResponse other = (HttpSendResponse)o;
            return getHttpSendRequestId().equals(other.getHttpSendRequestId()) && getResponseTimestamp().equals(other.getResponseTimestamp());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getHttpSendRequestId().hashCode() + getResponseTimestamp().hashCode();
    }
    
    @Override
    public int compareTo(HttpSendResponse o)
    {
    	int diff = getHttpSendRequestId().compareTo(o.getHttpSendRequestId());
    	return diff == 0 ? getResponseTimestamp().compareTo(o.getResponseTimestamp()) : diff;
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con)
        throws SQLException
    {
    	_logger.debug("save(con,user) on httpSendRequestId: " + httpSendRequestId + "; insert: " + doInsert());
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
        	if ( doInsert() ) // we do not support updating a response
            {                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_http_send_response (http_send_request_id,sent_timestamp,response_timestamp,status,http_status_code,http_response_content_type,http_response) VALUES (?,?,?,?,?,?,?)");
                stmt.set(httpSendRequestId);
                stmt.set(sentTimestamp);
                stmt.set(responseTimestamp);
                stmt.set(status);
                stmt.set(httpStatusCode);
                stmt.set(httpResponseContentType);
                stmt.set(httpResponse);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for httpSendRequestId: " + httpSendRequestId);
                    return false;
                }

                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                
                return true;
            }
            
        	// We don't currently have an update function
        	
            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on httpSendRequestId: " + httpSendRequestId + "; insert: " + doInsert());
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }

    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    
    // DELETE is controlled by HttpSendRequest - so when it's deleted, all related responses are deleted with it.
    
    
   	public static class Manager
   	{
   		public static void deleteAllForHttpSendRequest(Connection con, EsfUUID httpSendRequestId)
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
  	        	// Delete all records that only point to the user id and not also a transaction
   	        	stmt = new EsfPreparedStatement( con,
   	        			"DELETE FROM esf_http_send_response WHERE http_send_request_id=?"
   	        									);
    	        stmt.set(httpSendRequestId);
    	        stmt.executeUpdate();
    	    }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.deleteAllForHttpSendRequest(con) http_send_request_id: " + httpSendRequestId);
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }
   		}
   		
   		public static List<HttpSendResponse> getAll(Connection con, EsfUUID httpSendRequestId)
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<HttpSendResponse> list = new LinkedList<HttpSendResponse>();
   	        	
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT sent_timestamp,response_timestamp,status,http_status_code,http_response_content_type,http_response " +
   	        			"FROM esf_http_send_response WHERE http_send_request_id=? ORDER BY sent_timestamp"
   	        									);
    	        stmt.set(httpSendRequestId);
    	        
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfDateTime sentTimestamp = rs.getEsfDateTime();
	            	EsfDateTime responseTimestamp = rs.getEsfDateTime();
	            	String status = rs.getString();
	            	int httpStatusCode = rs.getInt();
	            	String httpResponseContentType = rs.getString();
	            	String httpResponse = rs.getString();
	            	
	            	HttpSendResponse response = new HttpSendResponse(httpSendRequestId,sentTimestamp,responseTimestamp,status,httpStatusCode,httpResponseContentType,httpResponse);
	            	response.setLoadedFromDb();
	            	list.add(response);
	            }
	            
   	            return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll(con) httpSendRequestId: " + httpSendRequestId);
   	            list.clear();
   	            return list;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }
   		}
   		
   		public static List<HttpSendResponse> getAll(EsfUUID httpSendRequestId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        List<HttpSendResponse> list = null;
   	        	
   	        try
   	        {
   	        	list = getAll(con,httpSendRequestId);
   	            con.commit();
   	            
   	            return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAll() httpSendRequestId: " + httpSendRequestId);
   	            pool.rollbackIgnoreException(con,e);
   	            return list;
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }
   		}
   		
   		public static HttpSendResponse createNew(EsfUUID httpSendRequestId, EsfDateTime sentTimestamp, EsfDateTime responseTimestamp,
   												 String status, int httpStatusCode, String httpResponseContentType, String httpResponse)
   		{
   			if ( sentTimestamp == null )
   				sentTimestamp = new EsfDateTime();
   			if ( responseTimestamp == null )
   				responseTimestamp = new EsfDateTime();
   			if ( ! STATUS_SUCCESS.equals(status) )
   				status = STATUS_FAILURE;
   			return new HttpSendResponse(httpSendRequestId,sentTimestamp,responseTimestamp,status,httpStatusCode,httpResponseContentType,httpResponse);
   		}
   	} // Manager
   	
}