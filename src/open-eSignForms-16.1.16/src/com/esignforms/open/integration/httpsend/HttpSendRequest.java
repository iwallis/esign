// Copyright (C) 2013-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.integration.httpsend;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;

import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;

import com.esignforms.open.db.BlobDb;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.Record;
import com.esignforms.open.runtime.Transaction;

/**
* HttpSendRequest holds all the info needed for the HttpSendProcessor to do the HTTP(S) GET/POST on our behalf.
* This object lets us track all data send, as well as allows us to create these in a DB transaction
* so it on failure, we can essentially rollback HTTP requests too.
* 
* Each HttpSendRequest will have one or more HttpSendResponse objects associated with how the request turned out.
* 
* @author Yozons, Inc.
*/
public class HttpSendRequest
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<HttpSendRequest>
{
	private static final long serialVersionUID = 1036319609581654784L;

	private static final com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(HttpSendRequest.class);
	
	public static final String REQUEST_METHOD_GET  = "GET";
	public static final String REQUEST_METHOD_POST = "POST";
	
	public static final String RESPONSE_MATCH_TYPE_ANY = "any";
	public static final String RESPONSE_MATCH_TYPE_MUST_CONTAIN = "mustContain";
	public static final String RESPONSE_MATCH_TYPE_MUST_NOT_CONTAIN = "mustNotContain";
	public static final String RESPONSE_MATCH_TYPE_MUST_START_WITH = "mustStartWith";
	public static final String RESPONSE_MATCH_TYPE_MUST_NOT_START_WITH = "mustNotStartWith";
	public static final String RESPONSE_MATCH_TYPE_MUST_MATCH_REGEX = "mustMatchRegex";
	public static final String RESPONSE_MATCH_TYPE_MUST_NOT_MATCH_REGEX = "mustNotMatchRegex";
    
    private static final EsfName REQUEST_RECORD_ESFNAME = new EsfName("HttpSendRequest");

    private static final EsfPathName NUM_ATTEMPTS_PATHNAME = new EsfPathName("ESF/numAttempts");
    private static final EsfPathName MAX_ATTEMPTS_PATHNAME = new EsfPathName("ESF/maxAttempts");
    private static final EsfPathName LAST_ATTEMPTED_TIMESTAMP_PATHNAME = new EsfPathName("ESF/lastAttemptedTimestamp");
    private static final EsfPathName RETRY_DELAY_MINUTES_PATHNAME = new EsfPathName("ESF/retryDelayMinutes");
    private static final EsfPathName URL_PATHNAME = new EsfPathName("ESF/url");
    private static final EsfPathName PARAMS_PATHNAME = new EsfPathName("ESF/params");
    private static final EsfPathName REQUEST_METHOD_PATHNAME = new EsfPathName("ESF/requestMethod"); // GET or POST
    private static final EsfPathName REASON_PATHNAME = new EsfPathName("ESF/reason"); 
    private static final EsfPathName LIMIT_SUCCESSFUL_HTTP_STATUS_CODES_PATHNAME = new EsfPathName("ESF/limitSuccessfulHttpStatusCodes"); 
    private static final EsfPathName RESPONSE_MATCH_TYPE_PATHNAME = new EsfPathName("ESF/responseMatchType"); 
    private static final EsfPathName RESPONSE_MATCH_VALUE_PATHNAME = new EsfPathName("ESF/responseMatchValue"); 

    protected final EsfUUID id;
    protected final EsfUUID transactionId; // The transaction associated with this HTTP Send
    protected Record request;
    
    protected final EsfDateTime createdTimestamp; // date object created and queued to the HTTP Send system
    protected EsfDateTime completedTimestamp; // date object was successfully sent by the HTTP Send system
    
    
    /**
     * This creates a HttpSendRequest object from data retrieved from the DB.
     */
    protected HttpSendRequest(EsfUUID id, EsfUUID transactionId, EsfDateTime createdTimestamp, EsfDateTime completedTimestamp, Record request)
    {
        this.id = id;
    	this.transactionId = transactionId;
        this.createdTimestamp = createdTimestamp;
        this.completedTimestamp = completedTimestamp;
        this.request = request;
    }
    
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfUUID getTransactionId()
    {
        return transactionId;
    }

    public EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }
    
    public EsfDateTime getCompletedTimestamp()
    {
    	return completedTimestamp;
    }
    public void setCompleted()
    {
    	if ( ! hasBeenCompleted() )
    	{
        	completedTimestamp = new EsfDateTime();
    		objectChanged();
    	}
    }
    public boolean hasBeenCompleted()
    {
    	return completedTimestamp != null;
    }
    
    
    // For internal use, initialize or Record (request) to have some initial values if not otherwise set
    void _initializeNewRequest()
    {
    	request.addUpdate(NUM_ATTEMPTS_PATHNAME, new EsfInteger(0));
    	setMaxAttempts(1);
    	setRetryDelayMinutes(1);
    	setRequestMethod(REQUEST_METHOD_POST);
    	setParams("");
    }
    
    public int getNumAttempts()
    {
    	EsfInteger i = request.getIntegerByName(NUM_ATTEMPTS_PATHNAME);
    	return i == null ? 0 : i.toInt();
    }
    public void bumpNumAttempts()
    {
    	int numAttempts = getNumAttempts();
    	++numAttempts;
   		request.addUpdate(NUM_ATTEMPTS_PATHNAME, new EsfInteger(numAttempts));
   		request.addUpdate(LAST_ATTEMPTED_TIMESTAMP_PATHNAME, new EsfDateTime());
   		if ( ! hasMoreAttempts() )
   			setCompleted();
    }
    public boolean hasMoreAttempts()
    {
    	return getNumAttempts() < getMaxAttempts();
    }
    
    public int getMaxAttempts()
    {
    	EsfInteger i = request.getIntegerByName(MAX_ATTEMPTS_PATHNAME);
    	return i == null ? 0 : i.toInt();
    }
    public void setMaxAttempts(int v)
    {
    	int numAttempts = getNumAttempts();
    	if ( v >= numAttempts && v > 0 )
    	{
    		if ( v > 10 )
    			v = 10;
    		request.addUpdate(MAX_ATTEMPTS_PATHNAME, new EsfInteger(v));
    	}
    }
    
    public int getRetryDelayMinutes()
    {
    	EsfInteger i = request.getIntegerByName(RETRY_DELAY_MINUTES_PATHNAME);
    	return i == null ? 0 : i.toInt();
    }
    public void setRetryDelayMinutes(int v)
    {
    	if ( v > 0 ) 
    	{
    		if ( v > (7*24*60) ) // can't delay more than 7 days as this indicates a misuse of this service
    			v = 7*24*60;
    		request.addUpdate(RETRY_DELAY_MINUTES_PATHNAME, new EsfInteger(v));
    	}
    }
    
    public EsfDateTime getLastAttemptedTimestamp()
    {
    	return request.getDateTimeByName(LAST_ATTEMPTED_TIMESTAMP_PATHNAME);
    }
    
    public String getUrl()
    {
    	EsfString url = request.getStringByName(URL_PATHNAME);
    	return url == null ? "(null URL?)" : url.toPlainString();
    }
    public void setUrl(String v)
    {
    	if ( isValidUrl(v) )
    	{
    		request.addUpdate(URL_PATHNAME, new EsfString(v));
    	}
    }
    public static boolean isValidUrl(String url)
    {
    	if ( EsfString.isNonBlank(url) )
    	{
    		String lower = url.toLowerCase();
    		if ( lower.startsWith("https://") || lower.startsWith("http://") )
    		{
    			try
    			{
        			new URL(url);
        			return true;
    			}
    			catch(MalformedURLException e)
    			{
    			}
    		}
    	}
    	return false;
    }
    
    public String getParams()
    {
    	EsfString params = request.getStringByName(PARAMS_PATHNAME);
    	return params == null ? "(null params?)" : params.toPlainString();
    }
    public void setParams(String v)
    {
    	request.addUpdate(PARAMS_PATHNAME, new EsfString(v));
    }
    
    public String getRequestMethod()
    {
    	EsfString method = request.getStringByName(REQUEST_METHOD_PATHNAME);
    	return method == null ? "(null method?)" : method.toPlainString();
    }
    public void setRequestMethod(String v)
    {
    	if ( EsfString.isNonBlank(v) )
    	{
    		if ( REQUEST_METHOD_GET.equalsIgnoreCase(v) )
    			v = REQUEST_METHOD_GET;
    		else
    			v = REQUEST_METHOD_POST;
    		request.addUpdate(REQUEST_METHOD_PATHNAME, new EsfString(v));
    	}
    }
    public boolean isRequestMethodGET()
    {
    	return REQUEST_METHOD_GET.equals(getRequestMethod());
    }
    public boolean isRequestMethodPOST()
    {
    	return REQUEST_METHOD_POST.equals(getRequestMethod());
    }
    
    public String getReason()
    {
    	EsfString reason = request.getStringByName(REASON_PATHNAME);
    	return reason == null ? "(null reason?)" : reason.toPlainString();
    }
    public void setReason(String v)
    {
    	request.addUpdate(REASON_PATHNAME, new EsfString(v));
    }

    public EsfInteger[] getLimitSuccessfulHttpStatusCodes()
    {
    	return request.getIntegersByName(LIMIT_SUCCESSFUL_HTTP_STATUS_CODES_PATHNAME);
    }
    public boolean hasLimitSuccessfulHttpStatusCodes()
    {
    	EsfInteger[] v = getLimitSuccessfulHttpStatusCodes();
    	return v != null && v.length > 0;
    }
    public void setLimitSuccessfulHttpStatusCodes(int[] codes)
    {
    	if ( codes == null || codes.length == 0 )
    		request.addUpdate(LIMIT_SUCCESSFUL_HTTP_STATUS_CODES_PATHNAME,new EsfInteger[0]);
    	else
    	{
    		EsfInteger[] values = new EsfInteger[codes.length];
    		for( int i=0; i < codes.length; ++i )
    			values[i] = new EsfInteger(codes[i]);
    		request.addUpdate(LIMIT_SUCCESSFUL_HTTP_STATUS_CODES_PATHNAME,values);
    	}
    }
    public String getLimitSuccessfulHttpStatusCodesAsString()
    {
    	EsfInteger[] values = getLimitSuccessfulHttpStatusCodes();
    	if ( values == null || values.length < 1 )
    		return null;
    	StringBuilder buf = new StringBuilder(10 * values.length);
    	for( int i=0; i < values.length; ++i )
    	{
    		if ( buf.length() > 0 )
    			buf.append(";");
    		buf.append(values[i]);
    	}
    	return buf.toString();
    }
    
    public String getResponseMatchType()
    {
    	EsfString matchType = request.getStringByName(RESPONSE_MATCH_TYPE_PATHNAME);
    	return matchType == null ? "(null response match type?)" : matchType.toPlainString();
    }
    public boolean isResponseMatchTypeAny()
    {
    	return RESPONSE_MATCH_TYPE_ANY.equals(getResponseMatchType());
    }
    public boolean isResponseMatchTypeMustContain()
    {
    	return RESPONSE_MATCH_TYPE_MUST_CONTAIN.equals(getResponseMatchType());
    }
    public boolean isResponseMatchTypeMustNotContain()
    {
    	return RESPONSE_MATCH_TYPE_MUST_NOT_CONTAIN.equals(getResponseMatchType());
    }
    public boolean isResponseMatchTypeMustStartWith()
    {
    	return RESPONSE_MATCH_TYPE_MUST_START_WITH.equals(getResponseMatchType());
    }
    public boolean isResponseMatchTypeMustNotStartWith()
    {
    	return RESPONSE_MATCH_TYPE_MUST_NOT_START_WITH.equals(getResponseMatchType());
    }
    public boolean isResponseMatchTypeMustMatchRegex()
    {
    	return RESPONSE_MATCH_TYPE_MUST_MATCH_REGEX.equals(getResponseMatchType());
    }
    public boolean isResponseMatchTypeMustNotMatchRegex()
    {
    	return RESPONSE_MATCH_TYPE_MUST_NOT_MATCH_REGEX.equals(getResponseMatchType());
    }
    public void setResponseMatchType(String v)
    {
    	if ( EsfString.isNonBlank(v) )
    		request.addUpdate(RESPONSE_MATCH_TYPE_PATHNAME, new EsfString(v));
    	else
    		request.addUpdate(RESPONSE_MATCH_TYPE_PATHNAME, new EsfString(RESPONSE_MATCH_TYPE_ANY));
    }

    public String getResponseMatchValue()
    {
    	EsfString matchValue = request.getStringByName(RESPONSE_MATCH_VALUE_PATHNAME);
    	return matchValue == null ? "(null response match value?)" : matchValue.toPlainString();
    }
    public void setResponseMatchValue(String v)
    {
    	request.addUpdate(RESPONSE_MATCH_VALUE_PATHNAME, new EsfString(v));
    }

    /**
     * This is a "minimalist" validation check to ensure that we have enough to do a send.
     * @return
     */
    public boolean isSendAllowed(Errors errors)
    {
    	if ( EsfString.isBlank(getRequestMethod()) )
    	{
    		if ( errors != null )
    			errors.addError("You need to supply the request method of GET or POST.");
    		return false;
    	}
    	
    	String url = getUrl();
    	if ( ! isValidUrl(url) )
    	{
    		if ( errors != null )
    			errors.addError("You need to supply a valid HTTP(s) URL.");
    		return false;
    	}
    	
    	if ( ! hasMoreAttempts() )
    	{
    		if ( errors != null )
    			errors.addError("No more attempts are allowed. The request has already been attempted " + getNumAttempts() + " out of " + getMaxAttempts() + " times.");
    		return false;
    	}
    	
    	return true;
    }
    public final boolean isSendAllowed()
    {
    	return isSendAllowed(null);
    }
    
    @Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof HttpSendRequest )
        {
        	HttpSendRequest other = (HttpSendRequest)o;
            return getId().equals(other.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(HttpSendRequest o)
    {
    	return getId().compareTo(o.getId());
    }
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; insert: " + doInsert());
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
    		request.save(con);
    		
        	if ( doInsert() )
            {                    
                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_http_send_request (id,transaction_id,created_timestamp,completed_timestamp,request_blob_id) VALUES (?,?,?,?,?)");
                stmt.set(id);
                stmt.set(transactionId);
                stmt.set(createdTimestamp);
                stmt.set(completedTimestamp);
                stmt.set(request.getId());
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for transactionId: " + transactionId + "; id: " + id);
                    return false;
                }

                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                
                Application.getInstance().notifyHttpSendProcessorQueued(); // let the HTTP Send processor know a new request has been queued up
                
                return true;
            }
            
            if ( hasChanged() )
            {
                stmt = new EsfPreparedStatement(con, "UPDATE esf_http_send_request SET completed_timestamp=? WHERE id=?");
                stmt.set(completedTimestamp);
                stmt.set(id);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for id: " + id);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; insert: " + doInsert());
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }

    public boolean save()
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    
    
    boolean delete(final Connection con, final Errors errors)
        throws SQLException
    {
    	_logger.debug("delete(con) on id: " + id);
        
        clearLastSQLException();
    
        if ( doInsert() )
        {
        	_logger.warn("delete(con) - Ignored delete of HttpSendRequest that was pending an INSERT id: " + id);
            objectDeleted();
            return true;
        }
        
        EsfPreparedStatement stmt = null;
        
        try
        {
            request.delete(con);

            HttpSendResponse.Manager.deleteAllForHttpSendRequest(con, id);

            stmt = new EsfPreparedStatement(con,"DELETE FROM esf_http_send_request WHERE id=?");
            stmt.set(id);
            stmt.executeUpdate();
            
            objectDeleted();
            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"delete(con) on id: " + id);
            setLastSQLException(e);
            throw e;
        }
        finally
        {
        	cleanupStatement(stmt);
        }
    }

    public boolean delete(final Errors errors)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = delete(con,errors);
            con.commit();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    public boolean delete()
    {
        Errors nullErrors = null;
        return delete(nullErrors);
    }
        
   	public static class Manager
   	{
   		public static void deleteAllForTransaction(Connection con, EsfUUID transactionId)
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	List<EsfUUID> ids = new LinkedList<EsfUUID>();
   	        	List<EsfUUID> requestBlobIds = new LinkedList<EsfUUID>();
   	        	stmt = new EsfPreparedStatement( con, "SELECT id,request_blob_id FROM esf_http_send_request WHERE transaction_id=?");
   	        	stmt.set(transactionId);
   	        	EsfResultSet rs = stmt.executeQuery();
   	        	while( rs.next() )
   	        	{
   	        		ids.add(rs.getEsfUUID());
   	        		requestBlobIds.add(rs.getEsfUUID());
   	        	}
   	        	
   	        	// Delete all request blobs
   	        	BlobDb blobDb = Application.getInstance().getBlobDb();
   	        	for( EsfUUID blobId : requestBlobIds )
   	        	{
   	        		blobDb.delete(con, blobId);
   	        	}
   	        	
   	        	stmt.close();
   	        	stmt = new EsfPreparedStatement( con, "DELETE FROM esf_http_send_request WHERE id=?" );
   	        	for( EsfUUID id : ids )
   	        	{
   	   	        	// Delete all responses to our requests
   	        		HttpSendResponse.Manager.deleteAllForHttpSendRequest(con, id);
   	        		// Delete the request itself
   	    	        stmt.set(id);
   	    	        stmt.executeUpdate();
   	        	}
    	    }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.deleteAllForTransaction(con) transactionId: " + transactionId);
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }
   		}
   		
   		public static List<HttpSendRequest> getAllUncompleted(Connection con, boolean forUpdate)
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<HttpSendRequest> list = new LinkedList<HttpSendRequest>();
   	        	
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT id,transaction_id,created_timestamp,completed_timestamp,request_blob_id " +
   	        			"FROM esf_http_send_request WHERE completed_timestamp IS NULL ORDER BY created_timestamp" +
   	        			(forUpdate ? " FOR UPDATE" : "")
   	        									);
    	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	EsfUUID transactionId = rs.getEsfUUID();
	            	EsfDateTime createdTimestamp = rs.getEsfDateTime();
	            	EsfDateTime completedTimestamp = rs.getEsfDateTime();
	            	EsfUUID requestBlobId = rs.getEsfUUID();
	            	
	            	Record request = Record.Manager.getById(con, requestBlobId);
	            	
	            	HttpSendRequest httpSendRequest = new HttpSendRequest(id,transactionId,createdTimestamp,completedTimestamp,request);
	            	httpSendRequest.setLoadedFromDb();
	            	list.add(httpSendRequest);
	            }
	            
   	            return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAllUncompleted(con)");
   	            list.clear();
   	            return list;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }
   		}
   		
   		public static List<HttpSendRequest> getAllUncompleted()
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        List<HttpSendRequest> list = null;
   	        	
   	        try
   	        {
   	        	list = getAllUncompleted(con,false);
   	            con.commit();
   	            
   	            return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getAllUncompleted()");
   	            pool.rollbackIgnoreException(con,e);
   	            return list;
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }
   		}
   		
   		public static List<HttpSendRequest> getMatching(Connection con, EsfDateTime fromDateTime, EsfDateTime toDateTime, EsfUUID transactionId)
   		{
   	        EsfPreparedStatement stmt = null;
   	        
   	        LinkedList<HttpSendRequest> list = new LinkedList<HttpSendRequest>();
   	        	
			StringBuilder sqlBuf = new StringBuilder(1000);
			sqlBuf.append("SELECT id,created_timestamp,completed_timestamp,request_blob_id ");
			sqlBuf.append("FROM esf_http_send_request WHERE transaction_id=? ");
			
			if ( fromDateTime != null )
				sqlBuf.append("AND created_timestamp>=? ");
			
			if ( toDateTime != null )
				sqlBuf.append("AND created_timestamp<=? ");

			sqlBuf.append("ORDER BY created_timestamp DESC");
			
			try
   	        {
   	        	stmt = new EsfPreparedStatement( con, sqlBuf.toString());
   	        	stmt.set(transactionId);
   	        	if ( fromDateTime != null )
   	        		stmt.set(fromDateTime);
   	        	if ( toDateTime != null )
   	        		stmt.set(toDateTime);
    	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
	            	EsfDateTime createdTimestamp = rs.getEsfDateTime();
	            	EsfDateTime completedTimestamp = rs.getEsfDateTime();
	            	EsfUUID requestBlobId = rs.getEsfUUID();
	            	
	            	Record request = Record.Manager.getById(con, requestBlobId);
	            	
	            	HttpSendRequest httpSendRequest = new HttpSendRequest(id,transactionId,createdTimestamp,completedTimestamp,request);
	            	httpSendRequest.setLoadedFromDb();
	            	list.add(httpSendRequest);
	            }
	            
   	            return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getMatching(con)");
   	            list.clear();
   	            return list;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }
   		}
   		
   		public static List<HttpSendRequest> getMatching(EsfDateTime fromDateTime, EsfDateTime toDateTime, EsfUUID transactionId)
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        List<HttpSendRequest> list = null;
   	        	
   	        try
   	        {
   	        	list = getMatching(con, fromDateTime, toDateTime, transactionId);
   	            con.commit();
   	            
   	            return list;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getMatching()");
   	            pool.rollbackIgnoreException(con,e);
   	            return list;
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }
   		}
   		
   		public static HttpSendRequest createNew(Transaction transaction)
   		{
   			Record request = new Record(REQUEST_RECORD_ESFNAME,BlobDb.CompressOption.ENABLE,BlobDb.EncryptOption.ENABLE);
   			HttpSendRequest httpSendRequest = new HttpSendRequest(new EsfUUID(), transaction.getId(), new EsfDateTime(), null, request);
   			httpSendRequest._initializeNewRequest();
   			return httpSendRequest;
   		}

   	} // Manager
   	
}