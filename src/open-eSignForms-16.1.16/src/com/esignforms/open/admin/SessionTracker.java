// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.admin;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.esignforms.open.admin.SessionTrackerInfo;

import com.esignforms.open.Application;
//import com.esignforms.esfapp.User;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.user.User;


/**
 * This class holds the list of active sessions and the associated users/parties and such for admin reporting.
 * 
 * @author Yozons, Inc.
 */
public class SessionTracker
	implements java.io.Serializable
{    
	private static final long serialVersionUID = -2737989182054405862L;

	private static final com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(SessionTracker.class);
    
    public static final String USER_SESSION_ATTRIBUTE_NAME 					= "OpeneSignFormsUser";
    public static final String IPADDR_SESSION_ATTRIBUTE_NAME 				= "OpeneSignFormsIPAddr";
    public static final String PARTY_NAME_SESSION_ATTRIBUTE_NAME 			= "OpeneSignFormsPartyName";
    public static final String SESSION_TRACKER_INFO_SESSION_ATTRIBUTE_NAME	= "com.esignforms.open.admin.SessionTrackerInfo";
    
    protected List<Session> list;
    
    protected EsfDateTime systemStarted = null;
    protected int numStarted = 0;
    protected int numStopped = 0;
    protected EsfDateTime lastStarted = null;
    protected EsfDateTime lastStopped = null;
    protected int maxSessions = 0;
    
    public SessionTracker()
    {
        list = new LinkedList<Session>();
        systemStarted = new EsfDateTime();
    }
    
    
    public void addSession(HttpSession session)
    {
        ++numStarted;
        lastStarted = new EsfDateTime();
        
        String sessId = null;
        try
        {
            sessId = session.getId();
        }
        catch( Exception e )
        {
            _logger.warn("Session created for addSession() but could not get its id.",e);
            return;
        }
        
        synchronized(list)
        {
            Session sess = getSessionById(sessId);
            
            if ( sess == null )
            {
                sess = new Session(this,session);
                list.add(sess);
            }
            else
                _logger.warn("Session id " + sess.session.getId() + " already exists in active session list.  Previously started at: " + 
                             sess.getStarted().toDateTimeMsecString());
            
            if ( list.size() > maxSessions )
                maxSessions = list.size();
        }

        Application app = Application.getInstance();
        EsfInteger stoMinutes = app.getDefaultSessionMinutesTimeout();
        session.setMaxInactiveInterval((int)stoMinutes.toLong() * 60);
    }
    
    public void setSessionTimeoutForLoggedInUser(HttpSession session)
    {
    	if ( session == null )
    		return;
    	
        Application app = Application.getInstance();
        EsfInteger stoMinutes = app.getLoggedInSessionTimeoutMinutes();
        session.setMaxInactiveInterval((int)stoMinutes.toLong() * 60);
    }
    
    public void setSessionTimeoutMinutes(HttpSession session, int timeoutMinutes)
    {
    	if ( session != null )
    		session.setMaxInactiveInterval(timeoutMinutes * 60);
    }
    
    public void removeSession(HttpSession session)
    {
        ++numStopped;
        lastStopped = new EsfDateTime();
        
        synchronized(list)
        {
            java.util.ListIterator<Session> iter = list.listIterator();
            while( iter.hasNext() )
            {
                Session sess = iter.next();
                if ( sess.getSession().equals(session) )
                {
                    iter.remove();
                    break;
                }
            }
        }
    }
    
    
    public void removeSession(String sessId)
    {
        ++numStopped;
        lastStopped = new EsfDateTime();
        
        synchronized(list)
        {
            java.util.ListIterator<Session> iter = list.listIterator();
            while( iter.hasNext() )
            {
                Session sess = iter.next();
                if ( sess.getSessionId().equals(sessId) )
                {
                    iter.remove();
                    break;
                }
            }
        }
    }
    
    /**
     * This is used to update the User record inside an active session.
     * @param user the User object that is being updated.
     */
    public void updateUser(User user)
    {
    	// We copy the list into our array since we may change the list and
    	// get a java.util.ConcurrentModificationException otherwise.
    	Session[] listCopy = getSessionsArray();
        for( Session sess : listCopy )
        {
            User sessUser = sess.getEsfLoggedInUser();
            if ( sessUser != null && sessUser.getId().equals(user.getId()) )
                sess.setEsfLoggedInUser(user);
        }
    }

    /**
     * Sets the user for a logged in user.
     * @param user the User associated with this session
     * @param session the HttpSession of the party
     */
    public void setUser(User user, HttpSession session)
    {
        if ( session == null ) // If there is no session, then we don't track at all
            return;
        Session sess = getSessionById(session.getId());
        if ( sess != null )
        {
            setSessionTimeoutForLoggedInUser(session);
        	sess.setEsfLoggedInUser(user);
        }
    }
    
    /**
     * Sets the email for a non-logged in user.
     * @param name the String name for the party when it's not a logged in ESF user.
     * @param session the HttpSession of the party
     */
    public void updateEmail(String email, HttpSession session)
    {
        if ( session == null || EsfString.isBlank(email) ) // If there is no session or email, then we don't track at all
            return;
        Session sess = getSessionById(session.getId());
        if ( sess != null )
        {
            if ( ! sess.isEsfLoggedInUser() )
                sess.getSessionTrackerInfo().email = email;
        }
    }
    
    public Session getSessionById(String id)
    {
    	synchronized(list)
    	{
            for( Session sess : list )
            {
                if ( sess.session.getId().equals(id) )
                    return sess;
            }
    	}
        return null;
    }
    
    
    public int getNumStarted()
    {
        return numStarted;
    }
    
    public int getNumStopped()
    {
        return numStopped;
    }

    public EsfDateTime getLastStarted()
    {
        return lastStarted;
    }
    
    public EsfDateTime getLastStopped()
    {
        return lastStopped;
    }

    public int getNumSessions()
    {
    	return list.size();
    }
    
    public int getMaxSessions()
    {
        return maxSessions;
    }
    
    // Already in a synchronized list by the caller getSessionsArray()
    private void cleanDeadSessions()
    {
        java.util.ListIterator<Session> iter = list.listIterator();
        while( iter.hasNext() )
        {
            Session sess = iter.next();
            try
            {
                sess.getSession().getId();
            }
            catch( Exception e )
            {
                iter.remove();
            }
        }
    }
    
    public Session[] getSessionsArray()
    {
        Session[] array = null;
        synchronized(list)
        {
            cleanDeadSessions();
            array = new Session[list.size()];
            list.toArray(array);
        }
        return array;
    }

    public int forceEndSession(EsfDateTime oldestAllowedActivityDateTime)
    {
        int numForcedOff = 0;
        
        Session[] currSessions = getSessionsArray();
        
        // If no users logged in, then no problem!
        if ( currSessions == null || currSessions.length == 0 )
            return numForcedOff;
        
        // Let's save off each session that needs to be killed so it's out of the non-synchronized list first
        LinkedList<Session> sessionsToKill = new LinkedList<Session>();

        for( Session session : currSessions )
        {
            EsfDateTime lastAccessed = session.getLastAccessed();
            if ( lastAccessed != null && lastAccessed.isBefore(oldestAllowedActivityDateTime) )
            {
                if ( session.getSession() != null )
                    sessionsToKill.add(session);
            }
        }
        currSessions = null;
        
        // Then let's kill each such session found
        for( Session session : sessionsToKill )
        {
            _logger.warn("forceEndSession() - Forced end session for: " + 
                           session.getName() + " who logged in at: " + session.getStarted() + 
                           " and last accessed at: " + session.getLastAccessed() +
                           " from IP: " + session.getSessionIP());
            try
            {
                if ( session.getSession() != null )
                {
                    session.getSession().invalidate();
                    ++numForcedOff;
                }
            }
            catch( IllegalStateException e ) {}
        }
        
        sessionsToKill.clear();
        
        return numForcedOff;
    }


    public EsfDateTime getSystemStartedTime()
    {
        return systemStarted;
    }

    public void destroy()
    {
        synchronized(list)
        {
        	list.clear();
        }
    }   
    
    public class Session
    	implements java.io.Serializable
    {
		private static final long serialVersionUID = 3289384871883126550L;

		public Session(SessionTracker tracker, HttpSession session)
        {
        	this.sessionTrackerInfo	= new SessionTrackerInfo(session.getId());
            this.session   			= session;
            this.tracker   			= tracker;
            session.setAttribute(SESSION_TRACKER_INFO_SESSION_ATTRIBUTE_NAME, sessionTrackerInfo); // so we'll get notified if it's ever activated on a restart
        }
        private SessionTrackerInfo	sessionTrackerInfo;
        private HttpSession   		session;
        private SessionTracker 		tracker;
        
        public HttpSession getSession()
        {
            return session;
        }
        
        public SessionTrackerInfo getSessionTrackerInfo()
        {
            return sessionTrackerInfo;
        }
        
        public String getSessionId()
        {
            return sessionTrackerInfo.getSessionId();
        }
        
        public EsfDateTime getStarted()
        {
            try
            {
                return new EsfDateTime(session.getCreationTime());
            }
            catch( Exception e )
            {
                tracker.removeSession(sessionTrackerInfo.getSessionId());
                return null;
            }
        }

        public String getStartedDateTimeString(User user)
        {
            EsfDateTime d = getStarted();
            if ( d == null )
                return "";
            return d.toLogString(user);
        }

        public EsfDateTime getLastAccessed()
        {
            try
            {
                return new EsfDateTime(session.getLastAccessedTime());
            }
            catch( Exception e )
            {
                tracker.removeSession(sessionTrackerInfo.getSessionId());
                return null;
            }
        }

        public String getLastAccessedDateTimeString(User user)
        {
            EsfDateTime d = getLastAccessed();
            if ( d == null )
                return "";
            return d.toLogString(user);
        }

        public int getSessionTimeoutMinutes()
        {
            try
            {
                return (session.getMaxInactiveInterval()+30) / 60;
            }
            catch( Exception e )
            {
                tracker.removeSession(sessionTrackerInfo.getSessionId());
                return -1;
            }
        }
        public String getSessionTimeoutMinutesString()
        {
        	int to = getSessionTimeoutMinutes();
        	if ( to < 0 )
        		return "None";
        	return Integer.toString(to);
        }

        public boolean isEsfLoggedInUser()
        {
            return getEsfLoggedInUser() != null;
        }        
        public User getEsfLoggedInUser()
        {
            try
            {
                return (User)session.getAttribute(USER_SESSION_ATTRIBUTE_NAME);
            }
            catch( Exception e )
            {
                tracker.removeSession(sessionTrackerInfo.getSessionId());
                return null;
            }
        }
        public void setEsfLoggedInUser(User user)
        {
            try
            {
                session.setAttribute(USER_SESSION_ATTRIBUTE_NAME,user);
                getSessionTrackerInfo().name = user.getFullDisplayName();
                getSessionTrackerInfo().userId = user.getId().toString();
                getSessionTrackerInfo().email = user.getEmail();

            }
            catch( Exception e ) 
            {
                _logger.warn("setEsfLoggedInUser exception on user: " + user.getFullDisplayName(),e);
                tracker.removeSession(sessionTrackerInfo.getSessionId());
            }
        }

        public String getSessionIP()
        {
            try
            {
                return (String)session.getAttribute(IPADDR_SESSION_ATTRIBUTE_NAME);
            }
            catch( Exception e )
            {
                tracker.removeSession(sessionTrackerInfo.getSessionId());
                return "(?.?.?.?)";
            }
        }

        public String getPartyName()
        {
            try
            {
                return (String)session.getAttribute(PARTY_NAME_SESSION_ATTRIBUTE_NAME);
            }
            catch( Exception e )
            {
                tracker.removeSession(sessionTrackerInfo.getSessionId());
                return "(n/a)";
            }
        }

        public String getName()
        {
            String name = getSessionTrackerInfo().name;
            return EsfString.isBlank(name) ? getPartyName() : name;
        }
        public String getEmail()
        {
            String email = getSessionTrackerInfo().email;
            return EsfString.isBlank(email) ? "(n/a)" : email;
        }

        @Override
        public boolean equals(Object o)
        {
            if ( o instanceof Session )
            {
                Session os = (Session)o;
                return sessionTrackerInfo.getSessionId().equals(os.sessionTrackerInfo.getSessionId());
            }
            return false;
        }

        @Override
        public int hashCode()
        {
        	return sessionTrackerInfo.getSessionId().hashCode();
        }
   }
}