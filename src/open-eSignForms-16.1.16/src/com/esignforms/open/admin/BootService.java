// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2009 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.LinkedList;
import java.util.UUID;

import javax.crypto.SecretKey;

import com.esignforms.open.admin.SessionKey;
import com.esignforms.open.Application;
import com.esignforms.open.crypto.SecretKeyGenerator;
import com.esignforms.open.crypto.PBE;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.util.Base64;
import com.esignforms.open.util.DateUtil;

/**
* Unlocks all of the session keys at boot time.  Also handles changing the boot password.
* @author Yozons, Inc.
*/
public final class BootService
{
	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(BootService.class);

	private   Application           app;
	private   SecretKeyGenerator	keyGen;
	private   PBE					pbe;
	private   EsfUUID				deployId		= null;
	private   String                pbepass       	= null;
	private   SecretKey             origBootKey   	= null;
	private   SecretKey             newBootKey    	= null;
	
	private   LinkedList<SessionKey> sessionKeys   = new LinkedList<SessionKey>();
	
	
	/**
	 * Creates the short-lived bootstrapping code that unlocks the boot password using the dual
	 * passwords provided, retrieves all of the previously encrypted session keys and then
	 * re-encrypts them all with the latest boot key and updates the boot key with the new value
	 * @param app the Application context the boot takes place in
	 * @param password1 the String first password for the boot key
	 * @param password2 the String second password for the boot key
	 * @param sessionKeyCreateNewDays the long number of days to have passed before we create a new session key
	 * @throws EsfException if there's a problem retrieving and re-securing the boot/session keys.
	 */
	public BootService(Application app, String password1, String password2, long sessionKeyCreateNewDays)
	    throws EsfException
	{
	    this.app	= app;
	    keyGen		= app.getSecretKeyGenerator();
	    pbe			= app.getPBE();
	    
	    if ( sessionKeyCreateNewDays > 0 )
	    	sessionKeyCreateNewDays *= -1;
	    
	    ConnectionPool pool = app.getConnectionPool();
	    Connection     con  = pool.getConnection();
	    
	    try
		{
		    deployId	= loadDeployId(con);
		
		    pbepass  	= MakeFullBootPassword(deployId,password1,password2);

		    origBootKey = loadBootKey(con);
		    newBootKey  = keyGen.generateKey();

		    resecureSessionKeys(con,sessionKeyCreateNewDays);
		    
	    	updateBootKey(con);
		    
		    con.commit();
		}
	    catch( SQLException e )
		{
	    	_logger.sqlerr("BootService() - SQL Exception",e);
	    	pool.rollbackIgnoreException(con);
	    	throw new EsfException("Failed to boot the system: " + e.getMessage());
		}
	    catch( Exception e )
		{
	    	_logger.error("BootService() - Failed to boot system",e);
	    	pool.rollbackIgnoreException(con);
	    	throw new EsfException("Failed to boot the system: " + e.getMessage());
		}
	    finally
		{
	        app.cleanupPool(pool,con,null);
		}
	}
	
	public final SessionKey[] getSessionKeys()
	{
		SessionKey[] keys = new SessionKey[sessionKeys.size()];
		sessionKeys.toArray(keys);
		return keys;
	}
	
	public final EsfUUID getDeployId()
	{
		return deployId;
	}

	public final void destroy()
	{
		sessionKeys.clear();
		sessionKeys = null;
		
		origBootKey = null;
		newBootKey  = null;
		pbe     	= null;
		pbepass     = null;
		keyGen      = null;
		app         = null;
	}
	
    public final static String MakeFullBootPassword(EsfUUID deployId,String pass1,String pass2)
    {
        return pass2 + deployId + pass1;
    }

	
	private SecretKey loadBootKey(Connection con)
		throws EsfException,SQLException
	{
	    PreparedStatement stmt = null;
	    
	    try
	    {
	    	stmt = con.prepareStatement("SELECT key_data FROM esf_session_key WHERE id=?");
	    	stmt.setObject(1,deployId.getUUID(),Types.OTHER);
	    	
	        ResultSet rs = stmt.executeQuery();
	        if ( ! rs.next() )
	        {
	        	_logger.error("loadBootKey() - Failed to find the boot key for deploy id: " + deployId);
	        	throw new EsfException("Failed to find the boot key.");
	        }
	        
	        String encodedEncryptedKey = rs.getString(1);
	
	        PBE decryptingPBE = pbe;
	        int numIterations = decryptingPBE.getNumIterations();
	        int saltSize = decryptingPBE.getSaltSize();
	        
	        // If they key has been encoded with iterations/salt values, let's use them to decrypt, otherwise it had better be our configured values.
	        int startingDollar = encodedEncryptedKey.indexOf("$");
	        if ( startingDollar > 0 )
	        {
	        	_logger.error("loadBootKey() - Encoded encrypted key contains '$' but not in expected first character position: " + encodedEncryptedKey);
	        	throw new EsfException("Invalid boot key format for non-leading '$'.");
	        }
	        else if ( startingDollar == 0 )
	        {
	        	int iterationDollar = encodedEncryptedKey.indexOf("$", startingDollar+1);
	        	if ( iterationDollar <= 0 )
		        {
		        	_logger.error("loadBootKey() - Encoded encrypted key starts with '$' but missing second '$' separator for the interation count: " + encodedEncryptedKey);
		        	throw new EsfException("Invalid boot key format for the iteration count.");
		        }
	        	numIterations = app.stringToInt(encodedEncryptedKey.substring(startingDollar+1, iterationDollar), numIterations);
	        	int saltDollar = encodedEncryptedKey.indexOf("$", iterationDollar+1);
	        	if ( saltDollar <= 0 )
		        {
		        	_logger.error("loadBootKey() - Encoded encrypted key starts with '$' but missing third '$' separator for the salt count: " + encodedEncryptedKey);
		        	throw new EsfException("Invalid boot key format for the salt size.");
		        }
	        	saltSize = app.stringToInt(encodedEncryptedKey.substring(iterationDollar+1, saltDollar), saltSize);
	        	decryptingPBE = PBE.createCompatiblePBE(decryptingPBE, numIterations, saltSize);
	        	encodedEncryptedKey = encodedEncryptedKey.substring(saltDollar+1);
	        }
	        
	        byte[] encryptedKey = Base64.decode(encodedEncryptedKey);
	        byte[] clearKey     = decryptingPBE.decrypt(pbepass,encryptedKey);
	        if ( clearKey == null )
	        {
	        	_logger.error("loadBootKey() - Invalid passwords used to decrypt the boot key.");
	        	throw new EsfException("Failed to decrypt the boot key");
	        }
	        return keyGen.getFromRawEncoded(clearKey);
	    }
	    finally
	    {
	        app.cleanupStatement(stmt);
	    }
	}

	
	private EsfUUID loadDeployId(Connection con)
		throws EsfException,SQLException
	{
	    PreparedStatement stmt = null;
	    
	    try
	    {
	    	// We should only have one such record
	    	stmt = con.prepareStatement("SELECT id,last_started_timestamp,last_stopped_timestamp FROM esf_deployment");
	    	
	        ResultSet rs = stmt.executeQuery();
	        if ( ! rs.next() )
	        {
	        	_logger.fatal("loadDeployId() - Failed to find the deployment id");
	        	throw new EsfException("Failed to find the deployment id.");
	        }
	
	        EsfUUID id 							= new EsfUUID((UUID)rs.getObject(1));
	        java.sql.Timestamp lastStartedTime	= rs.getTimestamp(2);
	        if ( rs.wasNull() )
	        	lastStartedTime = null;
	        java.sql.Timestamp lastStoppedTime	= rs.getTimestamp(3);
	        if ( rs.wasNull() )
	        	lastStoppedTime = null;
	        
	        if ( lastStartedTime == null )
	        	_logger.info("loadDeployId() - This is the first time the deployment has been started.");
	        else if ( lastStoppedTime == null )
	        	_logger.warn("loadDeployId() - The first time the deployment was started, it was not properly shut down.");
	        else if ( lastStoppedTime.before(lastStartedTime) )
	        	_logger.warn("loadDeployId() - The deployment was not properly shut down before this restart.");
	        
	        if ( rs.next() )
	        {
	        	_logger.fatal("loadDeployId() - More than one row is defined in the deployment table.");
	        	throw new EsfException("More than one deployment id defined.");
	        }
	        	
	        return id;
	    }
	    finally
	    {
	        app.cleanupStatement(stmt);
	    }
	}


	private void updateBootKey(Connection con)
		throws EsfException,SQLException
	{
		PreparedStatement stmt = null;
		
		try
		{
		   byte[] newBootKeyEncoded = newBootKey.getEncoded();
		   byte[] encryptedBootKey  = pbe.encrypt(pbepass,newBootKeyEncoded);
		
		   stmt = con.prepareStatement("UPDATE esf_session_key SET created_timestamp=?,key_data=? WHERE id=?");
		   stmt.setTimestamp(1,DateUtil.getCurrentSqlTimestamp());
		   stmt.setString(2,BootService.getIterationAndSaltPrefix(pbe.getNumIterations(),pbe.getSaltSize()) + Base64.encodeToString(encryptedBootKey));
		   stmt.setObject(3,deployId.getUUID(),Types.OTHER);
			
		   int num = stmt.executeUpdate();
		   if ( num != 1 )
		   {
		   		_logger.error("updateBootKey() - Could not update boot key with a new key");
		   		throw new EsfException("Failed to update the new boot key");
		   }
		}
		finally
		{
		   app.cleanupStatement(stmt);
		}
	}


	private SessionKey insertNewSessionKey(Connection con)
		throws EsfException,SQLException
	{
	    PreparedStatement stmt = null;
	    
	    try
	    {
	    	EsfUUID   newSessionKeyId   = new EsfUUID();
	    	SecretKey newSessionKey     = keyGen.generateKey();
	        byte[] encryptedSessionKey  = keyGen.encrypt(newBootKey,newSessionKey.getEncoded());
	        if ( encryptedSessionKey == null )
	        {
	            _logger.error("insertNewSessionKey() - Could not encrypt new session key");
				throw new EsfException("Failed to encrypt new session key");
	        }
	
	        stmt = con.prepareStatement("INSERT INTO esf_session_key (id,created_timestamp,key_data) VALUES (?,?,?)");
	    	stmt.setObject(1,newSessionKeyId.getUUID(),Types.OTHER);
	        stmt.setTimestamp(2,DateUtil.getCurrentSqlTimestamp());
	        stmt.setString(3,Base64.encodeToString(encryptedSessionKey));
	    	
	        int num = stmt.executeUpdate();
	        if ( num != 1 )
	        {
	        	_logger.error("insertNewSessionKey() - Could not insert the new session key");
				throw new EsfException("Failed to insert the new session key");
	        }
	        
	        return new SessionKey(newSessionKeyId,newSessionKey);
	    }
	    finally
	    {
	        app.cleanupStatement(stmt);
	    }
	}


	private void resecureSessionKeys(Connection con, long sessionKeyCreateNewDays)
		throws EsfException,SQLException
    {
	    _logger.debug("resecureSessionKeys()");
	   
	    boolean doInsertNextSessionKey = true;
	
	    // Our days will be negative coming here so that we go back in time
	    java.sql.Timestamp olderThanTimestamp = new java.sql.Timestamp( DateUtil.dateDaysFromNow( (int)sessionKeyCreateNewDays ).getTime() );
	    
	    PreparedStatement qstmt  = null;
	    PreparedStatement ustmt  = null;
		    
	    try
		{
	       // Get the list of keys that we need to re-encrypt (everything except the boot key)  
	        String sqlQueryCommand = 
	            "SELECT id,created_timestamp,key_data FROM esf_session_key WHERE id <> ? ORDER BY created_timestamp FOR UPDATE";
	        qstmt = con.prepareStatement(sqlQueryCommand);
	        qstmt.setObject(1,deployId.getUUID(),Types.OTHER);

	        String sqlUpdateCommand = "UPDATE esf_session_key SET key_data=? WHERE id=?";
	        ustmt = con.prepareStatement(sqlUpdateCommand);
	        
	        ResultSet rs = qstmt.executeQuery();
	        while( rs.next() )
	        {
	        	EsfUUID				id 					= new EsfUUID((UUID)rs.getObject(1));
	            java.sql.Timestamp 	createdTimestamp 	= rs.getTimestamp(2);
	            byte[] 				encryptedKey 		= Base64.decode(rs.getString(3));
	            
	            // If we find a key that was activated within the past N days, then we don't insert a new key this time 
	            if ( createdTimestamp.after(olderThanTimestamp) )
	            	doInsertNextSessionKey = false;
	            
	            // get key in plaintext.  If we can't decrypt, something's gone seriously wrong.
	            byte[] clearKey = keyGen.decrypt(origBootKey,encryptedKey);
	            if ( clearKey == null )
	            {
	                _logger.error("resecureSessionKeys() - Could not decrypt session key for id: " + id);
	                throw new EsfException("Failed to decrypt session key");
	            }
	            
	            sessionKeys.add(new SessionKey(id, keyGen.getFromRawEncoded(clearKey)));
	            
	            // re-encrypt using the new boot key
	            encryptedKey = keyGen.encrypt(newBootKey,clearKey);
	            if ( encryptedKey == null )
	            {
	                _logger.error("resecureSessionKeys() - Could not re-encrypt session key for id: " + id);
	                throw new EsfException("Failed to re-encrypt session key");
	            }
	            
	            // Update the row with the newly secured key
	            ustmt.setString(1,Base64.encodeToString(encryptedKey));
	            ustmt.setObject(2,id.getUUID(),Types.OTHER);
	            
	            int count = ustmt.executeUpdate();
	            if ( count != 1 )
	            {
	                _logger.error("resecureSessionKeys() - Could not update re-encrypted session key for id: " + id);
	                throw new EsfException("Failed to update re-encrypted session key");
	            }
	        }
		        
	        // Okay, we've re-encrypted all of the previous session keys.
	        if ( doInsertNextSessionKey )
	        {
		        // Now, let's add a new session key that we'll use going forward.
		        SessionKey currSessionKey = insertNewSessionKey(con);
		        if ( currSessionKey == null )
		        {
	                _logger.error("resecureSessionKeys() - Could not insert new session key.");
	                throw new EsfException("Failed to insert new session key");
		        }
		        sessionKeys.add(currSessionKey);
	            _logger.debug("resecureSessionKeys() - Created a new session key...");
	        }
	        else
	        {
	            _logger.debug("resecureSessionKeys() - Did not create a new session key...");
	        }
	    }
	    finally
	    {
	    	app.cleanupStatement(qstmt);
	    	app.cleanupStatement(ustmt);
		}
    }

	public static String getIterationAndSaltPrefix(int numIterations, int saltSize )
	{
		return "$" + numIterations + "$" + saltSize + "$";
	}
}