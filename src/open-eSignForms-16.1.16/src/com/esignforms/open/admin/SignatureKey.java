// Copyright (C) 2011-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.admin;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.crypto.PublicKeyGenerator;
import com.esignforms.open.crypto.SecretKeyGenerator;

import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.user.User;
import com.esignforms.open.util.Base64;
import com.esignforms.open.util.UUIDCacheReadOptimized;

/**
* SignatureKey holds a single Open eSignForms keypair for use in digital signatures.
* 
* @author Yozons, Inc.
*/
public class SignatureKey
    extends com.esignforms.open.db.DatabaseObject
    implements java.lang.Comparable<SignatureKey>, UUIDCacheReadOptimized.UUIDCacheable, UUIDCacheReadOptimized.TimeCacheable
{
	private static final long serialVersionUID = -7256872429051228182L;

	private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(SignatureKey.class);

    protected final EsfUUID id;
    protected final EsfUUID sessionKeyId;
    protected final EsfDateTime createdTimestamp;
    protected EsfDateTime deactivationTimestamp;
    protected final KeyPair keyPair;
    protected final X509Certificate cert;
    
    protected EsfDateTime lastAccessFromCache = new EsfDateTime();
    
    /**
     * This creates a SignatureKey object from data retrieved from the DB.
     */
    protected SignatureKey(EsfUUID id, EsfUUID sessionKeyId, EsfDateTime createdTimestamp, EsfDateTime deactivationTimestamp, X509Certificate cert, KeyPair keyPair)
    {
        this.id = id;
        this.sessionKeyId = sessionKeyId;
        this.createdTimestamp = createdTimestamp;
        this.deactivationTimestamp = deactivationTimestamp;
        this.cert = cert;
        this.keyPair = keyPair;
    }
    
   
    public final EsfUUID getId()
    {
        return id;
    }
    
    public final EsfDateTime getCreatedTimestamp()
    {
    	return createdTimestamp;
    }
    
    public final EsfDateTime getDeactivationTimestamp()
    {
    	return deactivationTimestamp;
    }
    public boolean isDeactivated()
    {
        return deactivationTimestamp != null;
    }
    public void deactivate() 
    {
    	if ( ! isDeactivated() )
    	{
    		deactivationTimestamp = new EsfDateTime();
    		objectChanged();
    	}
    }
    
    public X509Certificate getX509Certificate()
    {
    	return cert;
    }
    public String getBase64EncodedX509Certificate()
    {
    	try
    	{
    		return cert == null ? null : Base64.encodeToString(cert.getEncoded());
    	}
    	catch(CertificateEncodingException e)
    	{
    		Application.getInstance().except(e,"getBase64EncodedX509Certificate");
    		return null;
    	}
    }
    public String getX509CertificateSubjectX500PrincipalName()
    {
    	return cert == null ? null : cert.getSubjectX500Principal().getName();
    }
    
    public KeyPair getKeyPair()
    {
    	return keyPair;
    }
    
    public PublicKey getPublicKey()
    {
    	return keyPair == null ? null : keyPair.getPublic();
    }
    public String getBase64EncodedX509PublicKey()
    {
    	return keyPair == null ? null : Base64.encodeToString(keyPair.getPublic().getEncoded());
    }
    public String getBase64PublicKeyModulus() // only valid for RSA public keys
    {
    	if ( keyPair == null || ! (keyPair.getPublic() instanceof RSAPublicKey) )
    		return null;

    	// http://www.w3.org/TR/xmldsig-core/#sec-CryptoBinary says that leading zero octets must be stripped
    	byte[] modulusBytes = ((RSAPublicKey)keyPair.getPublic()).getModulus().toByteArray();
    	int numLeadingZeroBytes = 0;
    	while( modulusBytes[numLeadingZeroBytes] == 0 )
    		++numLeadingZeroBytes;
    	if ( numLeadingZeroBytes > 0 )
    	{
    		byte[] origModulusBytes = modulusBytes;
    		modulusBytes = new byte[origModulusBytes.length - numLeadingZeroBytes];
    		System.arraycopy(origModulusBytes,numLeadingZeroBytes,modulusBytes,0,modulusBytes.length);
    	}
    	return Base64.encodeToString(modulusBytes);
    }
    public String getBase64PublicKeyExponent() // only valid for RSA public keys
    {
    	if ( keyPair == null || ! (keyPair.getPublic() instanceof RSAPublicKey) )
    		return null;
    	return Base64.encodeToString( ((RSAPublicKey)keyPair.getPublic()).getPublicExponent().toByteArray() );
    }
    
    public PrivateKey getPrivateKey()
    {
    	return keyPair == null ? null : keyPair.getPrivate();
    }
     
	public EsfDateTime getLastAccessFromCache()
	{
		return lastAccessFromCache;
	}
	public void setLastAccessFromCache(EsfDateTime v)
	{
		lastAccessFromCache = v;
	}

	@Override
    public boolean equals(Object o)
    {
        if ( o != null && o instanceof SignatureKey )
            return getId().equals(((SignatureKey)o).getId());
        return false;
    }

    @Override
    public int hashCode() {
    	return getId().hashCode();
    }
    
    @Override
    public int compareTo(SignatureKey o)
    {
    	return getId().compareTo(o.getId());
    }
    
    
    /* ================================== BEGIN DATABASE RELATED ROUTINES ======================================= */
    
	public synchronized boolean save(final Connection con, final User user)
        throws SQLException
    {
    	_logger.debug("save(con,user) on id: " + id + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
        
        clearLastSQLException();
        EsfPreparedStatement stmt = null;
        
        try
        {
            if ( doInsert() )
            {
            	deactivationTimestamp = null;
            	
       	    	SecretKeyGenerator secretKeyGenerator = Application.getInstance().getSecretKeyGenerator();
       	    	SessionKey sessionKey = Application.getInstance().getSessionKey(sessionKeyId);
       	    	if ( sessionKey == null ) // shouldn't happen in production
       	    	{
       	    		_logger.error("save() on id: " + id + "; failed to find session key with id: " + sessionKeyId);
       	    		return false;
       	    	}

                byte[] encodedPublicKey = keyPair.getPublic().getEncoded();
                byte[] encodedPrivateKey = keyPair.getPrivate().getEncoded();
                byte[] encryptedPrivateKey = secretKeyGenerator.encrypt(sessionKey.getKey(),encodedPrivateKey);
                if ( encryptedPrivateKey == null ) // shouldn't happen in production
                {
       	    		_logger.error("save() on id: " + id + "; failed to encrypt private key with session key id: " + sessionKeyId);
       	    		return false;
                }
                String base64PublicKey = Base64.encodeToString(encodedPublicKey);
                String base64EncryptedPrivateKey = Base64.encodeToString(encryptedPrivateKey);
                String base64Cert = Application.getInstance().getPublicKeyGenerator().toBase64EncodeX509Certificate(cert);

                stmt = new EsfPreparedStatement(con, 
                	"INSERT INTO esf_signature_key (id,session_key_id,created_timestamp,deactivation_timestamp,x509_certificate_data,public_key_data,private_key_data) VALUES(?,?,?,?,?,?,?)");
                stmt.set(id);
                stmt.set(sessionKeyId);
                stmt.set(createdTimestamp);
                stmt.set(deactivationTimestamp);
                stmt.set(base64Cert);
                stmt.set(base64PublicKey);
                stmt.set(base64EncryptedPrivateKey);
                
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Insert failed for id: " + id + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
                    return false;
                }
                         
                // Now we mark this object as if it were loaded fresh from the database
                setLoadedFromDb();
                Manager.cache.replace(this); // We use replace instead of 'add' because on object create, we added to the ID-only cache
                
                if ( user != null ) 
                {
                	user.logConfigChange(con, "Created new signature key id: " + getId()); 
                	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " created new signature key id: " + getId());
                }
                
                return true;
            }
            
            if ( hasChanged() )
            {
                // We assume we'll update it instead
                stmt = new EsfPreparedStatement( con, "UPDATE esf_signature_key SET deactivation_timestamp=? WHERE id=?" );
                stmt.set(deactivationTimestamp);
                stmt.set(id);
                int num = stmt.executeUpdate();
                if ( num != 1 )
                {
                	_logger.warn("save(con) - Update failed for signature key id: " + id + "; deactivationTimestamp: " + deactivationTimestamp);
                }
            }

            // Now we mark this object as if it were loaded fresh from the database
            setLoadedFromDb();
            Manager.cache.replace(this);
            
            if ( user != null ) 
            {
            	user.logConfigChange(con, "Updated signature key id: " + getId() + "; deactivationTimestamp; " + deactivationTimestamp); 
            	Application.getInstance().getActivityLog().logSystemConfigChange(con, "User " + user.getFullDisplayName() + " updated signature key id: " + getId() + "; deactivationTimestamp; " + deactivationTimestamp); 
            }

            return true;
        }
        catch(SQLException e)
        {
        	_logger.sqlerr(e,"save(con) on id: " + id + "; insert: " + doInsert() + "; user: " + (user==null?"(none)":user.getFullDisplayName()));
            setLastSQLException(e);
            throw e;
        }
        finally
        {
            cleanupStatement(stmt);
        }
    }
	public boolean save(final Connection con)
	    throws SQLException
	{
		return save(con,(User)null);
	}    
    public boolean save(final User user)
    {
        ConnectionPool    pool = getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            boolean result = save(con,user);
			if ( result )
				con.commit();
			else
				con.rollback();
            return result;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return false;
        }
        finally
        {
            cleanupPool(pool,con,null);
        }
    }
    public boolean save()
    {
    	return save((User)null);
    }
    
    
   	public static class Manager
   	{
   	    private static UUIDCacheReadOptimized<SignatureKey> cache = new UUIDCacheReadOptimized<SignatureKey>();
   	    
   		static SignatureKey getById(Connection con, EsfUUID id) throws SQLException
   		{
   			Application app = Application.getInstance();
   			
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT session_key_id,created_timestamp,deactivation_timestamp,x509_certificate_data,public_key_data,private_key_data " +
   	        			"FROM esf_signature_key WHERE id=?"
   	        									);
   	        	stmt.set(id);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID sessionKeyId = rs.getEsfUUID();
		            EsfDateTime createdTimestamp = rs.getEsfDateTime();
		            EsfDateTime deactivationTimestamp = rs.getEsfDateTime();
		            String base64Cert = rs.getString();
		            String base64PublicKey = rs.getString();
		            String base64EncryptedPrivateKey = rs.getString();
		            
		            SessionKey sessionKey = app.getSessionKey(sessionKeyId);
		            if ( sessionKey == null ) // shouldn't happen in production
		            {
		            	_logger.error("getById() id: " + id + "; could not find session key with id: " + sessionKeyId);
		            	return null;
		            }
		            
		            SecretKeyGenerator secretKeyGenerator = app.getSecretKeyGenerator();
		   			PublicKeyGenerator publicKeyGenerator = app.getPublicKeyGenerator();
		   			
		   			// It should only be null/blank for the 12.9.29 release's dbsetup update when all certs are set up. As of that release, all new keys get a cert right away.
	   				X509Certificate cert = EsfString.isBlank(base64Cert) ? null : publicKeyGenerator.toX509CertificateFromBase64Encoded(base64Cert);
		   			if ( EsfString.isNonBlank(base64Cert) && cert == null )
		   			{
	   					_logger.error("getById() id: " + id + "; could not create X.509 certificate from encode cert: " + base64Cert);
	   					return null;		   				
		   			}
		   			
		            byte[] encodedPublicKey = Base64.decode(base64PublicKey);
		            PublicKey publicKey = publicKeyGenerator.getPublicFromX509Encoded(encodedPublicKey);
		            if ( publicKey == null ) // shouldn't happen in production
		            {
		            	_logger.error("getById() id: " + id + "; could not decode public key");
		            	return null;
		            }
		            
		            byte[] encryptedPrivateKey = Base64.decode(base64EncryptedPrivateKey);
		            byte[] encodedPrivateKey = secretKeyGenerator.decrypt(sessionKey.getKey(), encryptedPrivateKey);
		            if ( encodedPrivateKey == null )
		            {
		            	_logger.error("getById() id: " + id + "; could not decrypt private key");
		            	return null;
		            }
		            PrivateKey privateKey = publicKeyGenerator.getPrivateFromPkcs8Encoded(encodedPrivateKey);
		            if ( privateKey == null ) // shouldn't happen in production
		            {
		            	_logger.error("getById() id: " + id + "; could not decode private key");
		            	return null;
		            }
		            
		            KeyPair keyPair = new KeyPair(publicKey,privateKey);

		            SignatureKey signatureKey = new SignatureKey(id,sessionKeyId,createdTimestamp,deactivationTimestamp,cert,keyPair);
		            signatureKey.setLoadedFromDb();
		            cache.add(signatureKey);
		             
	   	            _logger.debug("Manager.getById() - id: " + id);

	   	            return signatureKey;
	            }
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getById() - id: " + id);
   	        	throw e;
   	        }
   	        finally
   	        {
   	            cleanupStatement(stmt);
   	        }

   	        _logger.debug("Manager.getById() - id: " + id + "; failed to find the signature key");
   	        return null; 
   		}
   		
   		public static SignatureKey getById(EsfUUID id)
   		{
   			SignatureKey signatureKey = cache.getById(id);
   	    	if ( signatureKey != null )
   	    		return signatureKey;
   	    	
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        
   	        try
   	        {
   	        	signatureKey = getById(con,id);
   	        	if ( signatureKey != null ) 
   	        	{
		            con.commit();
	   	        	return signatureKey;
   	        	}
   	        	con.rollback();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,null);
   	        }

   	        return null; 
   		}
   		
   		public static SignatureKey getCurrent()
   		{
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT id FROM esf_signature_key WHERE deactivation_timestamp IS NULL ORDER BY created_timestamp DESC"
   	        									);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            if ( rs.next() )
	            {
	            	EsfUUID id = rs.getEsfUUID();
		            con.commit();
	   	            
	   	            return getById(id);
	            }
	            
   	            con.rollback();
   	            
   	            // Odd, we don't have any active signature keys, so let's create one if we can
   	            _logger.warn("Manager.getCurrent() - Found no active signature keys, so trying to create a new one...");
   	            SignatureKey signatureKey = Manager.createNew();
   	            if ( signatureKey.save() )
   	            	return signatureKey;
   	        }
   	        catch(SQLException e) 
   	        {
   	        	_logger.sqlerr(e,"Manager.getCurrent()");
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   	        _logger.error("Manager.getCurrent(); Failed to get (or create) an active signature key");
   	        return null; 
   		}
   		
   		public static List<SignatureKey> getAll()
   		{
   			LinkedList<SignatureKey> list = new LinkedList<SignatureKey>();
   			
   	        ConnectionPool    pool = getConnectionPool();
   	        Connection        con  = pool.getConnection();
   	        EsfPreparedStatement stmt = null;
   	        
   	        try
   	        {
   	        	stmt = new EsfPreparedStatement( con,
   	        			"SELECT id FROM esf_signature_key ORDER BY created_timestamp DESC"
   	        									);
   	        	
	            EsfResultSet rs = stmt.executeQuery();
	            while ( rs.next() )
	            {
	            	list.add( getById(rs.getEsfUUID()) );
	            }

	            con.commit();
   	        }
   	        catch(SQLException e) 
   	        {
   	            pool.rollbackIgnoreException(con,e);
   	        }
   	        finally
   	        {
   	            cleanupPool(pool,con,stmt);
   	        }

   			return list;
   		}
   		
   	    public static SignatureKey createNew()
   	    {
			Application app = Application.getInstance();
			
			PublicKeyGenerator publicKeyGenerator = app.getPublicKeyGenerator();
   	    	
   	    	KeyPair keyPair = publicKeyGenerator.generateKeyPair();
   	    	
   	    	EsfUUID id = new EsfUUID();
   	    	SessionKey sessionKey = app.getSessionKey();
   	    	
   	    	EsfDateTime createdTimestamp = new EsfDateTime();
   	    	EsfDateTime deactivationTimestamp = null;
   	    	X509Certificate cert = null;
   	    	
			try
			{
   				cert = app.getPublicKeyGenerator().createSelfSignedCertificate(
   						"CN=Open_eSignForms_" + app.getExternalContextPath() + 
   						", O=DeploymentID/" + app.getDeployId() + 
   						", OU=SignatureKeyID/" + id  +
   						", C=" + app.getDefaultCountryCode(),
   						keyPair, createdTimestamp, deactivationTimestamp);
			}
			catch( Exception e )
			{
				_logger.error("createNew() could not create an X.509 certificate for the signature key",e);
				return null;
			}    	
   	    	
   	    	SignatureKey signatureKey = new SignatureKey(id,sessionKey.getKeyId(),createdTimestamp,deactivationTimestamp,cert,keyPair);
   	    	cache.add(signatureKey);
   	    	return signatureKey;
   	    }
   	    
   	} // Manager
   	
}