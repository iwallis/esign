// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.List;
import java.util.UUID;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.admin.Stats;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.prog.ButtonMessage;
import com.esignforms.open.prog.ButtonMessageVersion;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentStyle;
import com.esignforms.open.prog.DocumentStyleVersion;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.DropDown;
import com.esignforms.open.prog.DropDownVersion;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.EmailTemplateVersion;
import com.esignforms.open.prog.File;
import com.esignforms.open.prog.FileVersion;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.PropertySet;
import com.esignforms.open.prog.PropertySetVersion;
import com.esignforms.open.prog.ReportFieldTemplate;
import com.esignforms.open.prog.ReportTemplate;
import com.esignforms.open.prog.Serial;
import com.esignforms.open.prog.SerialVersion;
import com.esignforms.open.prog.TransactionTemplate;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionParty;
import com.esignforms.open.runtime.TransactionTimer;
import com.esignforms.open.runtime.workflow.TransactionContext;
import com.esignforms.open.runtime.workflow.TransactionEngine;
import com.esignforms.open.util.DateUtil;

/** 
 * The Backgrounder does whatever tasks are necessary to be carried out in the background.
 * It runs in its own thread and basically does cleanup work every 5 minutes, hourly at midnight and at 1am.
 * 
 * @author Yozons, Inc.
 */
public class Backgrounder
    implements java.lang.Runnable
{
    private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(Backgrounder.class);
    
    private boolean 	shouldContinue  = true;
    private long    	waitTimeMsec    = 0;
    private EsfDateTime midnight        = null;
    private EsfDateTime oneAM           = null;
    private EsfDateTime next5minutes    = null;
    private EsfDateTime nextHour        = null;
    
    private Application app         = null;

    public Backgrounder( Application app )
    {
        this.app = app;
        setWaitMinutes( /*app.getBackgrounderWaitIntervalMins()*/ 1 );

        midnight     = new EsfDateTime().nextDateForHourMinute(0,0);
        oneAM        = new EsfDateTime().nextDateForHourMinute(1, 0);
        next5minutes = new EsfDateTime().addMinutes(5);
        nextHour     = new EsfDateTime().addMinutes(1); // new EsfDateTime().addHours(1); -- let's do our first hourly shortly after starting
    }
    
    
    /**
     * Sets the wait time in minutes between background runs.
     * @param numMinutes the int that specifies the number of minutes to wait between
     *        work episodes.
     */
    public synchronized void setWaitMinutes( int numMinutes )
    {
        if ( numMinutes < 1 )
            numMinutes = 1;
            
        waitTimeMsec = DateUtil.minutesToMillis(numMinutes);
    }
    
    /**
     * This method is called daily after midnight to do whatever it needs to do on a daily basis.
     */
    protected void doMidnightCleanup()
    {
        app.getActivityLog().logSystemConfigChange("Midnight cleanup activities run...");
        
    	_logger.info("Backgrounder will run the midnight activities now...");

        EsfDateTime deleteStats	= new EsfDateTime();
        deleteStats.addDays(app.getRetainStatsDays().toInt() * -1);
		if ( ! shouldContinue )
			return;
		
        EsfDateTime deleteSystemStartStop = new EsfDateTime();
        deleteSystemStartStop.addDays(app.getRetainSystemStartStopDays().toInt() * -1);
		if ( ! shouldContinue )
			return;
		
        EsfDateTime deleteSystemUserActivity = new EsfDateTime();
        deleteSystemUserActivity.addDays(app.getRetainSystemUserActivityDays().toInt() * -1);
		if ( ! shouldContinue )
			return;
		
        EsfDateTime deleteSystemConfigChange = new EsfDateTime();
        deleteSystemConfigChange.addDays(app.getRetainSystemConfigChangeDays().toInt() * -1);
		if ( ! shouldContinue )
			return;
		
        EsfDateTime deleteUserLoginLogoff = new EsfDateTime();
        deleteUserLoginLogoff.addDays(app.getRetainUserLoginLogoffDays().toInt() * -1);
		if ( ! shouldContinue )
			return;
		
        EsfDateTime deleteUserSecurity = new EsfDateTime();
        deleteUserSecurity.addDays(app.getRetainUserSecurityDays().toInt() * -1);
		if ( ! shouldContinue )
			return;
		
        EsfDateTime deleteUserConfigChange = new EsfDateTime();
        deleteUserConfigChange.addDays(app.getRetainUserConfigChangeDays().toInt() * -1);
		if ( ! shouldContinue )
			return;
		
	    ConnectionPool pool = app.getConnectionPool();
	    Connection     con  = pool.getConnection();
        
        try
        {
        	Stats stats = new Stats();
        	stats.cleanup(con, deleteStats);
    		if ( ! shouldContinue )
    		{
    			con.commit();
    			return;
    		}
    		
            // Cleanup system activity log
            app.getActivityLog().cleanupSystemConfigChange(con, deleteSystemStartStop);
    		if ( ! shouldContinue )
    		{
    			con.commit();
    			return;
    		}
    		
            app.getActivityLog().cleanupSystemUserActivity(con, deleteSystemUserActivity);
    		if ( ! shouldContinue )
    		{
    			con.commit();
    			return;
    		}
    		
            app.getActivityLog().cleanupSystemConfigChange(con, deleteSystemConfigChange);
    		if ( ! shouldContinue )
    		{
    			con.commit();
    			return;
    		}
            
            // Cleanup user activity log
            app.getActivityLog().cleanupUserLoginLogoff(con, deleteUserLoginLogoff);
    		if ( ! shouldContinue )
    		{
    			con.commit();
    			return;
    		}

    		app.getActivityLog().cleanupUserSecurity(con, deleteUserSecurity);
    		if ( ! shouldContinue )
    		{
    			con.commit();
    			return;
    		}

    		app.getActivityLog().cleanupUserConfigChange(con, deleteUserConfigChange);
            con.commit();
        }
        catch(java.sql.SQLException e) 
        {
			_logger.sqlerr("doMidnightCleanup()",e);
			pool.rollbackIgnoreException(con,e);
	    }
        finally
        {
            app.cleanupPool(pool,con,null);
        }

        _logger.info("Backgrounder completed the midnight activities.");
    }
    
    protected void renotifyParty(EsfUUID transactionPartyId)
    {
	    ConnectionPool pool = app.getConnectionPool();
	    Connection     con  = pool.getConnection();
	    PreparedStatement stmt = null;
        
        try
        {
            stmt = con.prepareStatement("SELECT transaction_id FROM esf_transaction_party WHERE id=?");
            stmt.setObject(1,transactionPartyId.getUUID(),Types.OTHER);
            ResultSet rs = stmt.executeQuery();
            
            while( rs.next() )
            {
            	EsfUUID transactionId = new EsfUUID((UUID)rs.getObject(1));
            	
            	Transaction transaction = Transaction.Manager.getById(transactionId);
            	if ( transaction == null )
            	{
        			_logger.info("renotifyParty() - Skipping transaction party id: " + transactionPartyId + "; transaction id: " + transactionId + "; Transaction could not be found.");
            		continue;
            	}

            	if ( ! transaction.isInProgress() )
            	{
        			_logger.info("renotifyParty() - Skipping transaction party id: " + transactionPartyId + "; transaction id: " + transactionId + "; Transaction is no longer in progress.");
        			Transaction.Manager.removeFromCache(transaction);
        			continue;
            	}
 
            	TransactionParty transactionParty = transaction.getTransactionParty(transactionPartyId);
            	if ( transactionParty == null )
            	{
           			_logger.warn("renotifyParty() - Skipping transaction party id: " + transactionPartyId + "; transaction id: " + transactionId +
			     			     "; TransactionParty not found in the Transaction.");
           			transaction.logGeneral("Auto renotify failed.  Transaction Party Id " + transactionPartyId + " was not found.");
        			Transaction.Manager.removeFromCache(transaction);
           			continue;
            	}
            	
 				if ( ! transactionParty.isActive() )
 				{
           			_logger.warn("renotifyParty() - Skipping transaction party: " + transactionParty.getPackageVersionPartyTemplate().getEsfName() + 
      						 "; transaction party id: " + transactionPartyId + "; transaction id: " + transactionId +
		     			     "; TransactionParty is no longer active.");
        			Transaction.Manager.removeFromCache(transaction);
           			continue;
 				}
 				
 				transactionParty.renotifyParty(con);
 				
 		    	transaction._setChangedForInternalUseOnly();
 	            transaction.save(con);
    			Transaction.Manager.removeFromCache(transaction);
            }
            
            con.commit();
        }
        catch(java.sql.SQLException e) 
        {
			_logger.sqlerr("renotifyParty()",e);
			pool.rollbackIgnoreException(con,e);
	    }
        catch(Exception e)
        {
        	_logger.error("renotifyParty()",e);
        }
        finally
        {
            app.cleanupPool(pool,con,stmt);
        }
    }
    
    /**
     * This method is called periodically to send out automatic renotifications
     */
    protected void doAutomaticPartyRenotifications()
    {
        EsfDateTime renotifyDateTime = new EsfDateTime();
        
	    ConnectionPool pool = app.getConnectionPool();
	    Connection     con  = pool.getConnection();
	    PreparedStatement stmt = null;
        
        try
        {
            int numRenotified = 0;
            EsfUUID lastNotifyPartyId = null;

            stmt = con.prepareStatement(
                    "SELECT transaction_party_id FROM esf_transaction_party_renotify WHERE notify_timestamp <= ? ORDER BY transaction_party_id, notify_timestamp"
                                       );
            stmt.setTimestamp(1,renotifyDateTime.toSqlTimestamp());
            ResultSet rs = stmt.executeQuery();
            
            while( rs.next() )
            {
            	EsfUUID notifyPartyId = new EsfUUID((UUID)rs.getObject(1));
            	if ( notifyPartyId.equals(lastNotifyPartyId) )
            		continue;
            	
            	// We have a party to renotify
            	renotifyParty(notifyPartyId);
            	++numRenotified;
            	lastNotifyPartyId = notifyPartyId;
            }
            
            if ( numRenotified > 0 )
            {
            	stmt.close();
            	stmt = con.prepareStatement("DELETE FROM esf_transaction_party_renotify WHERE notify_timestamp <= ?");
            	stmt.setTimestamp(1,renotifyDateTime.toSqlTimestamp());
            	stmt.execute();
            }
            
            con.commit();
        }
        catch(java.sql.SQLException e) 
        {
			_logger.sqlerr("doAutomaticPartyRenotifications()",e);
			pool.rollbackIgnoreException(con,e);
	    }
        catch(Exception e)
        {
        	_logger.error("doAutomaticPartyRenotifications()",e);
        }
        finally
        {
            app.cleanupPool(pool,con,stmt);
        }
    }
    
    /**
     * This method is called daily after 1 a.m. to do whatever it needs to do on a daily basis.
     */
    protected void do1AmCleanup()
    {
    	try
    	{
	        _logger.info("Backgrounder will run the 1 a.m. activities now... NOTHING YET");
	        
	        doRecordStats();
	        
	        _logger.info("Backgrounder completed the 1 a.m. activities.");
    	}
    	catch( Exception e ) 
    	{
    		_logger.error("do1AmCleanup()",e);
    	}
    }
    
    protected void doRecordStats()
    {
    	Stats stats = new Stats();
    	stats.recordCurrentValues();
    }
    
    /**
     * This method is called every hour
     */
    protected void doHourlyCleanup()
    {
    	try
    	{
            _logger.debug("doHourlyCleanup() - Expiring/canceling old transactions and clearing old sessions.");
            int numExpired = Transaction.Manager.expireAll();
            if ( numExpired > 0 )
                _logger.info("doHourlyCleanup() - Expired " + numExpired + " transactions.");            
            if ( ! shouldContinue )
            	return;
            
            int numCanceled = Transaction.Manager.cancelAll();
            if ( numCanceled > 0 )
                _logger.info("doHourlyCleanup() - Canceled " + numCanceled + " transactions.");
            if ( ! shouldContinue )
            	return;

            EsfDateTime forceEndSession = new EsfDateTime();
            forceEndSession.addMinutes(-8*60); // No activity in 8 hours
            int numForcedOff = app.getSessionTracker().forceEndSession(forceEndSession);
            if ( numForcedOff > 0 )
                _logger.info("doHourlyCleanup() - Ended " + numForcedOff + " sessions.");
    	}
    	catch( Exception e ) 
    	{
    		_logger.error("doHourlyCleanup()",e);
    	}
    }

    
    /**
     * This method is called every 5 minutes
     */
    protected void do5minutesCleanup()
    {
    	try
    	{
        	if ( _logger.isDebugEnabled() )
        	{
    	    	_logger.debug("do5minutesCleanup() - #sessions: " + app.getSessionTracker().getNumSessions() + 
    	    					"; maxSessions: " + app.getSessionTracker().getMaxSessions() +
    	    					"; JVM freeMem: " + EsfInteger.byteSizeInUnits(Runtime.getRuntime().freeMemory()) +
    	    					"; #DBConnections: " + app.getConnectionPool().getNumInuseConnections() +
    	    					"; MaxDBConnections: " + app.getConnectionPool().getMaxConnections()
    	    					);
        	}
            app.getConnectionPools().resizeConnections();
            
            doAutomaticPartyRenotifications();
            
    		// Keep in sync with the cache clearing in the DeploymentPropertiesForm.clearCaches()
            // TODO: Allow the cache duration to be configurable, perhaps with test/prod settings
            _logger.debug("Transaction cache number flushed: " + Transaction.Manager.flushNotAccessedWithinMinutes(15) + "; cache size: " + Transaction.Manager.getNumCached()); 
            
            _logger.debug("TransactionTemplate cache number flushed: " + TransactionTemplate.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + TransactionTemplate.Manager.getNumCached());

            _logger.debug("Package cache number flushed: " + Package.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + Package.Manager.getNumCached()); 
            _logger.debug("PackageVersion cache number flushed: " + PackageVersion.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + PackageVersion.Manager.getNumCached());
            
            _logger.debug("ReportTemplate cache number flushed: " + ReportTemplate.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + ReportTemplate.Manager.getNumCached()); 
            _logger.debug("ReportFieldTemplate cache number flushed: " + ReportFieldTemplate.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + ReportFieldTemplate.Manager.getNumCached()); 
            
            _logger.debug("ButtonMessage cache number flushed: " + ButtonMessage.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + ButtonMessage.Manager.getNumCached()); 
            _logger.debug("ButtonMessageVersion cache number flushed: " + ButtonMessageVersion.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + ButtonMessageVersion.Manager.getNumCached());
            
            _logger.debug("Document cache number flushed: " + Document.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + Document.Manager.getNumCached()); 
            _logger.debug("DocumentVersion cache number flushed: " + DocumentVersion.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + DocumentVersion.Manager.getNumCached());
            
            _logger.debug("DocumentStyle cache number flushed: " + DocumentStyle.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + DocumentStyle.Manager.getNumCached()); 
            _logger.debug("DocumentStyleVersion cache number flushed: " + DocumentStyleVersion.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + DocumentStyleVersion.Manager.getNumCached());
            
            _logger.debug("DropDown cache number flushed: " + DropDown.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + DropDown.Manager.getNumCached()); 
            _logger.debug("DropDownVersion cache number flushed: " + DropDownVersion.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + DropDownVersion.Manager.getNumCached());
            
            _logger.debug("EmailTemplate cache number flushed: " + EmailTemplate.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + EmailTemplate.Manager.getNumCached()); 
            _logger.debug("EmailTemplateVersion cache number flushed: " + EmailTemplateVersion.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + EmailTemplateVersion.Manager.getNumCached());
            
            _logger.debug("File cache number flushed: " + File.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + File.Manager.getNumCached()); 
            _logger.debug("FileVersion cache number flushed: " + FileVersion.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + FileVersion.Manager.getNumCached());
            
            _logger.debug("Image cache number flushed: " + Image.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + Image.Manager.getNumCached()); 
            _logger.debug("ImageVersion cache number flushed: " + ImageVersion.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + ImageVersion.Manager.getNumCached());
            
            _logger.debug("PropertySet cache number flushed: " + PropertySet.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + PropertySet.Manager.getNumCached()); 
            _logger.debug("PropertySetVersion cache number flushed: " + PropertySetVersion.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + PropertySetVersion.Manager.getNumCached());
            
            _logger.debug("Serial cache number flushed: " + Serial.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + Serial.Manager.getNumCached()); 
            _logger.debug("SerialVersion cache number flushed: " + SerialVersion.Manager.flushNotAccessedWithinMinutes(30) + "; cache size: " + SerialVersion.Manager.getNumCached());
    	}
    	catch( Exception e ) 
    	{
    		_logger.error("do5minutesCleanup()",e);
    	}
    }
    
    void checkExpiredTransactionTimers(EsfDateTime expirationDateTime)
    {
    	List<TransactionTimer> expiredTimers = TransactionTimer.Manager.getExpired(expirationDateTime);
    	if ( expiredTimers == null || expiredTimers.size() < 1 )
    		return;
    	
	    ConnectionPool pool = app.getConnectionPool();
	    PreparedStatement stmt = null;
        
	    // We process each timer in its own transaction so an error in one timer doesn't affect
	    // any other timer.
	    for( TransactionTimer timer : expiredTimers )
    	{
		    Connection con = pool.getConnection();

		    try
	        {
	        	Errors errors = new Errors();
	        	TransactionContext context = new TransactionContext(timer.getTransactionId(), timer.getTimerName());
	        	if ( context.hasTransaction() )
	        	{
		        	TransactionEngine engine = new TransactionEngine(context);
		        	engine.queueTimerExpiredEvent(timer.getTimerName());
		        	engine.doWork(con,errors);
		        	context.transaction.save(con);
		        	timer.delete(con);
		        	Transaction.Manager.removeFromCache(context.transaction);
	        	}
	        	else
	        	{
		        	_logger.error("checkExpiredTransactionTimers() - Failed to find transaction with tranId: " + timer.getTransactionId() + "; ignoring timerName: " + timer.getTimerName() + "; expirationDateTime: " + expirationDateTime.toDateTimeMsecString());
		        	timer.delete(con);
	        	}
	        	con.commit();
	        }
	        catch(java.sql.SQLException e) 
	        {
				_logger.sqlerr("checkExpiredTransactionTimers() - tranId: " + timer.getTransactionId() + "; timerName: " + timer.getTimerName() + "; expirationDateTime: " + expirationDateTime.toDateTimeMsecString(),e);
				pool.rollbackIgnoreException(con,e);
		    }
	        catch(Exception e)
	        {
	        	_logger.error("checkExpiredTransactionTimers() - tranId: " + timer.getTransactionId() + "; timerName: " + timer.getTimerName() + "; expirationDateTime: " + expirationDateTime.toDateTimeMsecString(),e);
				pool.rollbackIgnoreException(con);
	        }
	        finally
	        {
	            app.cleanupPool(pool,con,stmt);
	        }
    	}

        
    }
    
    /**
     * This method is called periodically (currently every minute).
     */
    protected void doBackgroundTasks()
    {
        EsfDateTime now = new EsfDateTime();
        
        // At midnight, let's do our various cleanup activities....
        if ( shouldContinue && now.isAfter(midnight) )
        {
            doMidnightCleanup();
            
            midnight = new EsfDateTime().nextDateForHourMinute(0,0);
        }
        
        if ( shouldContinue && now.isAfter(oneAM) )
        {
            do1AmCleanup();
            
            oneAM = new EsfDateTime().nextDateForHourMinute(1, 0);
        }
        
        if ( shouldContinue && now.isAfter(next5minutes) )
        {
            do5minutesCleanup();
            
            next5minutes = new EsfDateTime().addMinutes(5);
        }

        if ( shouldContinue && now.isAfter(nextHour) )
        {
            doHourlyCleanup();
            
            nextHour = new EsfDateTime().addHours(1);
        }
        
        if ( shouldContinue && app.hasPasswordManager() )
        	app.getPasswordManager().checkInvalidLoginAttempts();
        
        if ( shouldContinue )
        	checkExpiredTransactionTimers(now);
    }
    
    
    public void run()
    {
    	_logger.info("Backgrounder has started.");
        while ( shouldContinue )
        {
            try
            {
                synchronized(this)
                {
                    wait( waitTimeMsec );
                }
                if ( shouldContinue )
                	doBackgroundTasks();
            }
            catch( java.lang.InterruptedException e )
            {
                stop();
            }
            
        }
    	_logger.info("Backgrounder has stopped.");
    }
    
    
    public void stop()
    {
        shouldContinue = false;
    }

} 
