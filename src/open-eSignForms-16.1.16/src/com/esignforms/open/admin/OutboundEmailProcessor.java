// Copyright (C) 2011 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.admin;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.email.OutboundEmailMessage;
import com.esignforms.open.email.SimpleEmailer;
import com.esignforms.open.util.DateUtil;

/** 
 * The OutboundEmailProcessor runs in its own thread, sending out emails that are queued up for SMTP.
 * 
 * @author Yozons, Inc.
 */
public class OutboundEmailProcessor
    implements java.lang.Runnable
{
    private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(OutboundEmailProcessor.class);
    
    private boolean 	shouldContinue  = true;
    private boolean 	emailMessageQueued = false;
    private long    	waitTimeMsec    = 0;
    
    private Application app         = null;

    public OutboundEmailProcessor( Application app )
    {
        this.app = app;
        setWaitMinutes( /*app.getBackgrounderWaitIntervalMins()*/ 1 );
    }
    
    
    /**
     * Sets the wait time in minutes between background runs.
     * @param numMinutes the int that specifies the number of minutes to wait between
     *        work episodes.
     */
    public synchronized void setWaitMinutes( int numMinutes )
    {
        if ( numMinutes < 1 )
            numMinutes = 1;
            
        waitTimeMsec = DateUtil.minutesToMillis(numMinutes);
    }
    
    protected int sendAllQueuedEmail()
    {
    	int numSent = 0;
    	int numErrors = 0;
    	
        ConnectionPool    pool = app.getConnectionPool();
        Connection        con  = pool.getConnection();
	        
        try
        {
        	List<OutboundEmailMessage> list = OutboundEmailMessage.Manager.getAllUnsent(con,true); // get all for update
        	
        	for( OutboundEmailMessage msg : list )
        	{
        		if ( ! shouldContinue )
        			break;
        		
        		Errors errors = new Errors();
        		if ( ! msg.isSendAllowed(errors) ) 
        		{ 
            		// If the message cannot be sent, mark it as dead so it doesn't stay with us forever
        			msg.setMessageFailedToBeSent(errors.toString());
        			++numErrors;
        		} 
        		else 
        		{
        			SimpleEmailer emailer = null;
        			boolean smtpOkay = false;
        			try
        			{
        				emailer = new SimpleEmailer(msg);
        				smtpOkay = true;
        				emailer.sendOutboundEmailMessage();
        				msg.setMessageHasBeenSent();
      	    			app.getSecureRandom().setSeed(msg.getEmailTo()+msg.getBounceCorrelationCode()); // we send emails at random times so we'll use this to help keep our PRNG random from back-guessing its seed
        				++numSent;
        			}
        			catch( Exception e )
        			{
            			if ( smtpOkay )
            				msg.setMessageFailedToBeSent(e.getMessage()); // don't want this message again since it failed (if we have better repeatable/non-repeatable logic, do it here)
            			else
            				msg.setSendStatus(e.getMessage()); // update status, but don't we'll try again so don't mark it as sent
            			++numErrors;
        			}
        			finally
        			{
        				if ( emailer != null )
        					emailer.close();
        			}
        		}
        		
        		msg.save(con); // now update it
        	}
        	
            con.commit();
            
            return numSent;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"sendAllQueuedEmail()");
            pool.rollbackIgnoreException(con,e);
            return numSent;
        }
        finally
        {
            app.cleanupPool(pool,con,null);
            _logger.debug("sendAllQueuedEmail() numSent: " + numSent + "; numErrors: " + numErrors);
        }
    }
    
    /**
     * This method is called periodically to handle whatever email tasks it can do.
     */
    protected void doEmailTasks()
    {
    	// If we wake up for 'emailQueued', we'll sleep 1 second to help up the odds we find the newly queued email committed to the DB before we check
    	if ( emailMessageQueued )
    	{
    		emailMessageQueued = false;
    		app.sleep(1500); // 1.5 seconds
    	}
		sendAllQueuedEmail();
    }
    
    public void run()
    {
    	_logger.info("OutboundEmailProcessor has started.");
        while ( shouldContinue )
        {
            try
            {
                synchronized(this)
                {
                    wait( waitTimeMsec );
                }
                if ( shouldContinue )
                	doEmailTasks();
            }
            catch( java.lang.InterruptedException e )
            {
                stop();
            }
            
        }
    	_logger.info("OutboundEmailProcessor has stopped.");
    }
    
    
    public void stop()
    {
        shouldContinue = false;
    }

    public void setEmailMessageQueued()
    {
    	emailMessageQueued = true;
    }
} 
