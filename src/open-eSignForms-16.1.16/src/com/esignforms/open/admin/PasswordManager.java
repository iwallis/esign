// Copyright (C) 2009-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.LinkedList;
import java.util.UUID;

import com.esignforms.open.Application;
import com.esignforms.open.crypto.SecureHash;
import com.esignforms.open.crypto.jBCrypt.BCrypt;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.prog.EmailTemplate;
import com.esignforms.open.prog.EmailTemplateVersion;
import com.esignforms.open.prog.Library;
import com.esignforms.open.user.User;
import com.esignforms.open.user.UserLoginInfo;
import com.esignforms.open.email.OutboundEmailMessage;
import com.esignforms.open.exception.EsfException;
import com.esignforms.open.util.DateUtil;
import com.esignforms.open.util.StringReplacement;

/** 
 * The Password Manager handles the ESF built-in password system.
 * 
 * The original version (what is now considered to be version 1) used a simple SHA-512. Our desktop PC could do 100 of these in 7 msecs.
 * 
 * The next version (as of the 12.8.25 release), version 2, uses jBCrypt with default salt interation of 11 that has a much more costly hashing algorithm 
 * to prevent offline attacks against the database values should it ever be stolen. Our desktop PC could do 100 of these in 26032 msecs.
 *
 * @author Yozons, Inc.
 */
public class PasswordManager
{
    private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(PasswordManager.class);
    
    public static String RESET_PASSWORD_HASH_PREFIX = "Reset "; // This is the prefix for the password hash when the password has been reset and no actual password has yet to be set.
    
    protected Application app = Application.getInstance();
    
    protected short minLength;
    protected short maxLength;
    protected short minNumTypesMustHave;
    protected short maxFailedLoginAttempts;
    protected short maxRequestForgottenPasswordAttempts;
    protected short numBackgrounderIntervalsCredit;
    protected short numBackgrounderIntervalsRemaining;  // used to track how many intervals are remaining before we give the credits
    
    public boolean allowUppercase;
    public boolean allowLowercase;
    public boolean allowDigit;
    public boolean allowWhitespace;
    public boolean allowSpecial;
    
    protected String tip = "";
    
    protected short minForgotPasswordAnswerLength;
    protected short maxForgotPasswordAnswerLength;
    
    protected String passwordLockoutEmailSubject;
    protected String passwordLockoutEmailFrom;
    protected String passwordLockoutEmailText;
    protected String passwordLockoutEmailHtml;

    protected String setPasswordEmailSubject;
    protected String setPasswordEmailFrom;
    protected String setPasswordEmailText;
    protected String setPasswordEmailHtml;

    protected String forgotPasswordEmailSubject;
    protected String forgotPasswordEmailFrom;
    protected String forgotPasswordEmailText;
    protected String forgotPasswordEmailHtml;

    protected String passwordChangedEmailSubject;
    protected String passwordChangedEmailFrom;
    protected String passwordChangedEmailText;
    protected String passwordChangedEmailHtml;


    public PasswordManager()
    {
    	this(true);
    }
    
    public PasswordManager(boolean isRegularStartup)
    {
    	if ( isRegularStartup )
    		reloadSystemData();
    }
    
    public void reloadSystemData()
    {
        minLength = 1; //app.stringToShort(systemData.getValueByName("Passwords.length.min","1"),(short)1);
        if ( minLength < 1 )
        {
            _logger.warn("The system property Passwords.length.min was too small. Set to 1.");
            minLength = 1;
        }
        
        maxLength = 100; //app.stringToShort(systemData.getValueByName("Passwords.length.max","100"),(short)100);
        if ( maxLength < minLength )
        {
            _logger.warn("The system property Passwords.length.max was less than the min. Set to " + minLength + ".");
            maxLength = minLength;
        }
        
        minNumTypesMustHave = 1; //app.stringToShort(systemData.getValueByName("Passwords.types.minNumMustHave","1"),(short)1);
        if ( minNumTypesMustHave < 1 )
        {
            _logger.warn("The system property Passwords.types.minNumMustHave was too small. Set to 1.");
            minNumTypesMustHave = 1;
        }
        else if ( minNumTypesMustHave > 5 )
        {
            _logger.warn("The system property Passwords.types.minNumMustHave was too big. Set to 5.");
            minNumTypesMustHave = 5;
        }
        
        String typesAllowed = "upper,lower,digit,space,special"; //systemData.getValueByName("Passwords.types.allow","upper,lower,digit,space,special") + ",";
        allowUppercase = typesAllowed.indexOf("upper") >= 0;
        allowLowercase = typesAllowed.indexOf("lower") >= 0;
        allowDigit = typesAllowed.indexOf("digit") >= 0;
        allowWhitespace = typesAllowed.indexOf("space") >= 0;
        allowSpecial = typesAllowed.indexOf("special") >= 0;
        
        maxFailedLoginAttempts = 3; //app.stringToShort(systemData.getValueByName("Passwords.failedLogin.maxAttempts","5"),(short)5);
        if ( maxFailedLoginAttempts < 1 )
        {
            _logger.warn("The system property Passwords.failedLogin.maxAttempts was too small. Set to 1.");
            maxFailedLoginAttempts = 1;
        }
        
        maxRequestForgottenPasswordAttempts = 3; 
        if ( maxRequestForgottenPasswordAttempts < 1 )
        {
            _logger.warn("The system property Passwords.requestForgottenPassword.maxAttempts was too small. Set to 1.");
            maxRequestForgottenPasswordAttempts = 1;
        }
        
        numBackgrounderIntervalsCredit = 5; //app.stringToShort(systemData.getValueByName("Passwords.failedLogin.numBackgrounderIntervalsCredit","5"),(short)5);
        numBackgrounderIntervalsRemaining = numBackgrounderIntervalsCredit;
        
        tip = null;//systemData.getValueByName("Passwords.tip",null);
        if ( tip == null )
            tip = "Your password must have between " + minLength + " and " + maxLength + " characters. We recommend pass phrases at least 10 characters long.";
        
        minForgotPasswordAnswerLength = 1; //app.stringToShort(systemData.getValueByName("Passwords.forgot.answer.length.min","1"),(short)1);
        if ( minForgotPasswordAnswerLength < 1 )
        {
            _logger.warn("The system property Passwords.forgot.answer.length.min was too small. Set to 1.");
            minForgotPasswordAnswerLength = 1;
        }
        
        maxForgotPasswordAnswerLength = 100; //app.stringToShort(systemData.getValueByName("Passwords.forgot.answer.length.max","100"),(short)100);
        if ( maxForgotPasswordAnswerLength < minForgotPasswordAnswerLength )
        {
            _logger.warn("The system property Passwords.forgot.answer.length.max was less than the min. Set to " + minForgotPasswordAnswerLength + ".");
            maxForgotPasswordAnswerLength = minForgotPasswordAnswerLength;
        }
    }

    public short getMaxPasswordLength()
    {
        return maxLength;
    }
    
    public short getMaxForgotPasswordAnswerLength()
    {
        return maxForgotPasswordAnswerLength;
    }
    
    public void checkInvalidLoginAttempts()
    {
        --numBackgrounderIntervalsRemaining;
        if ( numBackgrounderIntervalsRemaining < 1 )
        {
            giveInvalidLoginAttemptCredit();
            numBackgrounderIntervalsRemaining = numBackgrounderIntervalsCredit; // reset countdown 
        }
    }
    
    
    /**
        Ensures that a given password meets any imposed requirements 
        on the quality of a password.
        @param userId the EsfUUID userId who's password is being checked
        @param password the String password as entered by the user
        @exception EsfException is thrown if the password is not legal quality.
        with the text part explaining what is invalid
    */
    public void checkAllowedPassword(EsfUUID userId, String password)
        throws EsfException
    {
        if ( password.length() < minLength )
            throw new EsfException("Your password must have at least " + minLength + " characters.");
        
        if ( "Vanilla fudge makes me smile".equalsIgnoreCase(password) || "Vanillafudgemakesmesmile".equalsIgnoreCase(password) || "rhdk4879".equalsIgnoreCase(password) )
            throw new EsfException("Your password must not match our examples.");
          
        int numTypes = 0;

        if ( containsUppercase(password) )
        {
            if ( allowUppercase )
                ++numTypes;
            else
                throw new EsfException("Your password is not allowed to contain upper case letters.");
        }

        if ( containsLowercase(password) )
        {
            if ( allowLowercase )
                ++numTypes;
            else
                throw new EsfException("Your password is not allowed to contain lower case letters.");
        }

        if ( containsDigit(password) )
        {
            if ( allowDigit )
                ++numTypes;
            else
                throw new EsfException("Your password is not allowed to contain numbers.");
        }

        if ( containsWhitespace(password) )
        {
            if ( allowWhitespace )
                ++numTypes;
            else
                throw new EsfException("Your password is not allowed to contain spaces.");
        }

        if ( containsSpecial(password) )
        {
            if ( allowSpecial )
                ++numTypes;
            else
                throw new EsfException("Your password is not allowed to contain special characters and symbols.");
        }
       
        if ( numTypes < minNumTypesMustHave )
            throw new EsfException("Your password must contain at least " + minNumTypesMustHave + " character types.");
    }
    
    /**
        Provides a tip to the user on how to select an appropriate password.
        @return the String tip to display to the user about providing a password.
    */
    public String getTip()
    {
        return tip;
    }
    
    public short getCurrentHashVersion()
    {
    	return (short)2012;
    }
    
    public String createPasswordHash(EsfUUID userId, String password) // returns current hash version only since we're creating a new one
    {
    	return BCrypt.hashpw(userId+password, BCrypt.gensalt(app.getJBCryptSaltIterations(), app.getSecureRandom().getPRNG()));
    }
    
    public boolean isCorrectPassword(EsfUUID userId, String password, String passwordHash, short passwordHashVersion)
    {
    	if ( passwordHashVersion == 2009 )
    	{
    		String compareHash = app.getSecureHash().getBase64Digest(userId+password+"pwd");
    		return compareHash.equals(passwordHash);
    	}
    	
    	return BCrypt.checkpw(userId+password, passwordHash);
    }
    
    public String createForgotPasswordAnswerHash(EsfUUID userId, String forgotPasswordAnswer) // returns current hash version only since we're creating a new one
    {
    	return createPasswordHash(userId,lowercaseAndStrip(forgotPasswordAnswer));
    }
    
    public boolean isCorrectForgotPasswordAnswer(EsfUUID userId, String forgotPasswordAnswer, String forgotPasswordAnswerHash, short forgotPasswordAnswerHashVersion)
    {
    	if ( forgotPasswordAnswerHashVersion == 2009 )
    	{
    		String compareHash = app.getSecureHash().getBase64Digest(userId+lowercaseAndStrip(forgotPasswordAnswer)+"FPA");
    		return compareHash.equals(forgotPasswordAnswerHash);
    	}
    	
    	return BCrypt.checkpw(userId+lowercaseAndStrip(forgotPasswordAnswer), forgotPasswordAnswerHash);
    }
    
    /**
        Ensures that a given answer meets any imposed requirements on the 
        quality of the answer.
        @param userId the EsfUUID userId who's answer is being checked
        @param answerToQuestion the String answer to the question as 
            entered by the user
        @exception EsfException is thrown if the answer is not legal quality.
            with the text part explaining what is invalid
    */
    public void checkAllowedForgotPasswordAnswer(EsfUUID userId, String answerToQuestion)
        throws EsfException
    {
        if ( lowercaseAndStrip(answerToQuestion).length() < minForgotPasswordAnswerLength )
            throw new EsfException("Your answer must be at least " + minForgotPasswordAnswerLength + " letters and/or numbers long.");

        if ( "Vanilla fudge makes me smile".equalsIgnoreCase(answerToQuestion) || "Vanillafudgemakesmesmile".equalsIgnoreCase(answerToQuestion) || "rhdk4879".equalsIgnoreCase(answerToQuestion) )
            throw new EsfException("Your answer must not match our example passwords.");
    }
    
    /**
        Provides a tip to the user on how to select an appropriate answer.
        @return the String tip to display to the user about providing an answer.
    */
    public String getForgotPasswordAnswerTip()
    {
        return
        "Your answer must have between " + minForgotPasswordAnswerLength + " and " + maxForgotPasswordAnswerLength + 
        " letters and numbers. It should be easy for you to remember when prompted with the question, " +
        "yet hard for others to guess. " +
        "The answer is not case sensitive, and special characters are ignored. It is essentially a second password, so choose wisely.";
    }
    
    protected boolean containsUppercase(String s)
    {
        for( int i=0; i < s.length(); ++i )
        {
            if ( Character.isUpperCase(s.charAt(i)) )
                return true;
        }
        return false;
    }

    protected boolean containsLowercase(String s)
    {
        for( int i=0; i < s.length(); ++i )
        {
            if ( Character.isLowerCase(s.charAt(i)) )
                return true;
        }
        return false;
    }

    protected boolean containsDigit(String s)
    {
        for( int i=0; i < s.length(); ++i )
        {
            if ( Character.isDigit(s.charAt(i)) )
                return true;
        }
        return false;
    }

    protected boolean containsWhitespace(String s)
    {
        for( int i=0; i < s.length(); ++i )
        {
            if ( Character.isWhitespace(s.charAt(i)) )
                return true;
        }
        return false;
    }

    protected boolean containsSpecial(String s)
    {
        for( int i=0; i < s.length(); ++i )
        {
            char c = s.charAt(i);
            if ( ! Character.isLetterOrDigit(c) && ! Character.isWhitespace(c) )
                return true;
        }
        return false;
    }

    protected String lowercaseAndStrip(String s)
    {
        StringBuilder buff = new StringBuilder(s.length());
        for( int i=0; i < s.length(); ++i )
        {
            char c = s.charAt(i);
            if ( Character.isLetterOrDigit(c) )
                buff.append(Character.toLowerCase(c));
        }
        return buff.toString();
    }

    protected boolean sendPasswordLockoutEmail(Connection con, User user, String externalContextPath)
		throws SQLException
    {
        _logger.debug("sendPasswordLockoutEmail() to: " + user.getFullDisplayName());
        
    	EmailTemplate emailTemplate = EmailTemplate.Manager.getByName(Library.Manager.getTemplate().getId(),app.getEmailTemplatePasswordLockoutEsfName());
    	if ( emailTemplate == null )
    	{
            _logger.error("sendSetPasswordEmail() to: " + user.getFullDisplayName() + "; failed because could not find Template Library's EmailTemplate: " + app.getEmailTemplatePasswordLockoutEsfName());
            return false;
    	}
    	
    	String passwordUrl = externalContextPath + "/requestForgotPassword.jsp";
    	
		EmailTemplateVersion emailTemplateVersion = emailTemplate.getTestEmailTemplateVersion();
		
		LinkedList<StringReplacement.FromToSpec> list = new LinkedList<StringReplacement.FromToSpec>();
		list.add( new StringReplacement.FromToSpec("${CONTEXTPATH}",externalContextPath) );
		list.add( new StringReplacement.FromToSpec("${LINK}",passwordUrl) );
		list.add( new StringReplacement.FromToSpec("${NAME}",user.getDisplayName()) );
		list.add( new StringReplacement.FromToSpec("${EMAIL}",user.getEmail()) );
		OutboundEmailMessage outboundEmail = OutboundEmailMessage.Manager.createNew(user.getId(), emailTemplateVersion, list);
        
        return outboundEmail.save(con);
    }

    protected boolean sendSetPasswordEmail(Connection con, User user, String resetPickupCode, String externalContextPath)
    	throws SQLException
    {
        _logger.debug("sendSetPasswordEmail() to: " + user.getFullDisplayName());
        
    	EmailTemplate emailTemplate = EmailTemplate.Manager.getByName(Library.Manager.getTemplate().getId(),app.getEmailTemplateSetPasswordEsfName());
    	if ( emailTemplate == null )
    	{
            _logger.error("sendSetPasswordEmail() to: " + user.getFullDisplayName() + "; failed because could not find Template Library's EmailTemplate: " + app.getEmailTemplateSetPasswordEsfName());
            return false;
    	}
    	
    	String passwordUrl = externalContextPath + "/setMyPassword.jsp?c=" + resetPickupCode;
    	
		EmailTemplateVersion emailTemplateVersion = emailTemplate.getTestEmailTemplateVersion();
		
		LinkedList<StringReplacement.FromToSpec> list = new LinkedList<StringReplacement.FromToSpec>();
		list.add( new StringReplacement.FromToSpec("${CONTEXTPATH}",externalContextPath) );
		list.add( new StringReplacement.FromToSpec("${PICKUPCODE}",resetPickupCode) );
		list.add( new StringReplacement.FromToSpec("${LINK}",passwordUrl) );
		list.add( new StringReplacement.FromToSpec("${NAME}",user.getDisplayName()) );
		list.add( new StringReplacement.FromToSpec("${EMAIL}",user.getEmail()) );
		OutboundEmailMessage outboundEmail = OutboundEmailMessage.Manager.createNew(user.getId(), emailTemplateVersion, list);
        
        return outboundEmail.save(con);
    }

    protected boolean sendForgotPasswordEmail(Connection con, User user, String forgotPickupCode, String externalContextPath)
    	throws SQLException
    {
        _logger.debug("sendForgotPasswordEmail() to: " + user.getFullDisplayName());
        
    	EmailTemplate emailTemplate = EmailTemplate.Manager.getByName(Library.Manager.getTemplate().getId(),app.getEmailTemplateForgotPasswordEsfName());
    	if ( emailTemplate == null )
    	{
            _logger.error("sendForgotPasswordEmail() to: " + user.getFullDisplayName() + "; failed because could not find Template Library's EmailTemplate: " + app.getEmailTemplateForgotPasswordEsfName());
            return false;
    	}
    	
    	String forgotPasswordUrl = externalContextPath + "/forgotMyPassword.jsp?c=" + forgotPickupCode;
    	
		EmailTemplateVersion emailTemplateVersion = emailTemplate.getTestEmailTemplateVersion();
		
		LinkedList<StringReplacement.FromToSpec> list = new LinkedList<StringReplacement.FromToSpec>();
		list.add( new StringReplacement.FromToSpec("${CONTEXTPATH}",externalContextPath) );
		list.add( new StringReplacement.FromToSpec("${PICKUPCODE}",forgotPickupCode) );
		list.add( new StringReplacement.FromToSpec("${LINK}",forgotPasswordUrl) );
		list.add( new StringReplacement.FromToSpec("${NAME}",user.getDisplayName()) );
		list.add( new StringReplacement.FromToSpec("${EMAIL}",user.getEmail()) );
		OutboundEmailMessage outboundEmail = OutboundEmailMessage.Manager.createNew(user.getId(), emailTemplateVersion, list);
        
        return outboundEmail.save(con);
    }
    
    protected boolean sendPasswordChangedEmail(Connection con, User user, String externalContextPath)
    	throws SQLException
    {
        _logger.debug("sendPasswordChangedEmail() to: " + user.getFullDisplayName());
        
    	EmailTemplate emailTemplate = EmailTemplate.Manager.getByName(Library.Manager.getTemplate().getId(),app.getEmailTemplatePasswordChangedEsfName());
    	if ( emailTemplate == null )
    	{
            _logger.error("sendPasswordChangedEmail() to: " + user.getFullDisplayName() + "; failed because could not find Template Library's EmailTemplate: " + app.getEmailTemplatePasswordChangedEsfName());
            return false;
    	}
 		EmailTemplateVersion emailTemplateVersion = emailTemplate.getTestEmailTemplateVersion();
		
		LinkedList<StringReplacement.FromToSpec> list = new LinkedList<StringReplacement.FromToSpec>();
		list.add( new StringReplacement.FromToSpec("${CONTEXTPATH}",externalContextPath) );
		list.add( new StringReplacement.FromToSpec("${NAME}",user.getDisplayName()) );
		list.add( new StringReplacement.FromToSpec("${EMAIL}",user.getEmail()) );
		OutboundEmailMessage outboundEmail = OutboundEmailMessage.Manager.createNew(user.getId(), emailTemplateVersion, list);
        
        return outboundEmail.save(con);
    }
    
    // **************************** DB ROUTINES ***************************************
    
    
    public void setPassword(User user, String password, String forgottenQuestion, String forgottenAnswer, String externalContextPath, String ipAddr)
        throws EsfException
    {
        if ( user == null || EsfString.isBlank(password) || EsfString.isBlank(forgottenQuestion) || EsfString.isBlank(forgottenAnswer) )
            throw new EsfException("All parameters are needed to set the password.");
        
        checkAllowedPassword(user.getId(), password);
        checkAllowedForgotPasswordAnswer(user.getId(), forgottenAnswer);
        
        ConnectionPool    pool  = app.getConnectionPool();
        Connection        con   = pool.getConnection();
        PreparedStatement stmt  = null;
        
        try
        {
            String passwordHash = createPasswordHash(user.getId(),password);
            String answerHash   = createForgotPasswordAnswerHash(user.getId(),forgottenAnswer);
            
            Timestamp 		setTimestamp	= DateUtil.getCurrentSqlTimestamp();
            java.util.UUID 	userId 			= user.getId().getUUID();
            
            stmt = con.prepareStatement(
                    "UPDATE esf_userlogin SET pwd_hash=?,forgotten_question=?,forgotten_answer_hash=?,reset_pickup_code=NULL,forgot_pickup_code=NULL," +
                    "password_set_timestamp=?,hash_version=?,invalid_login_count=0,invalid_answer_count=0,request_forgot_count=0 WHERE user_id=?"
                                       );
            stmt.setString(1,passwordHash);
            stmt.setString(2,forgottenQuestion);
            stmt.setString(3,answerHash);
            stmt.setTimestamp(4,setTimestamp);
            stmt.setShort(5,getCurrentHashVersion()); // new passwords always use the current scheme
            stmt.setObject(6,userId,Types.OTHER);
            stmt.executeUpdate();
            if ( EsfString.isNonBlank(externalContextPath) )
            	sendPasswordChangedEmail(con,user,externalContextPath);
            stmt.close(); stmt = null;
            
            // Add this info to the history, too
            stmt = con.prepareStatement(
                    "INSERT INTO esf_userlogin_history (user_id,password_set_timestamp,pwd_hash,forgotten_answer_hash) VALUES (?,?,?,?)"
                                       );
            stmt.setObject(1,userId,Types.OTHER);
            stmt.setTimestamp(2,setTimestamp);
            stmt.setString(3,passwordHash);
            stmt.setString(4,answerHash);
            stmt.executeUpdate();

            user.logSecurity(con, "User set a new password from IP: " + ipAddr);
            
            con.commit();
            
            user.setPasswordHash(passwordHash);
        }
        catch(SQLException e) 
        {
            _logger.sqlerr(e,"PasswordManager.setPassword() user id: " + user.getId() + "; name: " + user.getFullDisplayName());
            pool.rollbackIgnoreException(con,e);
            throw new EsfException("Sorry, the system is unavailable.");
        }
        finally
        {
            app.cleanupPool(pool,con,stmt);
        }
    }
    
    /**
     * Checks the password matches for the user specified.
     * @return the UserLoginInfo showing info about the login; null if the login is not successful
     * @throws EsfException if the login failure should report an error back to the user.
     */
    public UserLoginInfo login(User user, String password, String externalContextPath, String ipAddr)
        throws EsfException
    {
        if ( user == null || password == null )
            throw new EsfException(app.getServerMessages().getString("password.error.missingLoginAuthParams"));
        
        /*
    	// If general logins are not allowed, and this user doesn't have admin login permission, block.
    	if ( ! app.allowAllLogins() && user.isAllowAdminLogin().isFalse() )
    		throw new EsfException(app.getServerMessages().getString("password.warn.generalLoginsNotEnabled"));
    	*/

        if ( ! user.isEnabled() )
            throw new EsfException(app.getServerMessages().getString("password.error.accountDisabled"));
        
        ConnectionPool    pool  = app.getConnectionPool();
        Connection        con   = pool.getConnection();
        PreparedStatement stmt  = null;
        
        try
        {
        	UserLoginInfo userLoginInfo = getUserLoginInfoByUserId(con, user.getId());
            if ( userLoginInfo == null )
                throw new EsfException(app.getServerMessages().getString("password.error.missingLoginData"));
            
            if ( userLoginInfo.getInvalidLoginCount() >= maxFailedLoginAttempts )
                throw new EsfException(app.getServerMessages().getString("password.error.alreadyMaxFailedLogins"));
            
            if ( userLoginInfo.getPasswordHash().startsWith(RESET_PASSWORD_HASH_PREFIX) )
                throw new EsfException(app.getServerMessages().getString("password.error.noPasswordSet"));
            
            if ( ! isCorrectPassword(user.getId(), password, userLoginInfo.getPasswordHash(), userLoginInfo.getPasswordHashVersion()) )
            {
                stmt = con.prepareStatement("UPDATE esf_userlogin SET invalid_login_count=invalid_login_count+1 WHERE user_id=?");
                stmt.setObject(1,user.getId().getUUID(),Types.OTHER);
                stmt.executeUpdate();
                
                int invalidLoginCount = userLoginInfo.getInvalidLoginCount() + 1;
                if ( invalidLoginCount >= maxFailedLoginAttempts )
                {
                    sendPasswordLockoutEmail(con,user,externalContextPath);
                    con.commit();
                    throw new EsfException(app.getServerMessages().getString("password.error.maxFailedLogins"));
                }
                con.commit();
                throw new EsfException(app.getServerMessages().getString("password.error.invalidLogin"));
            }
            
            // Looks like they are authenticated!
            
            // Reset any login failure counts and update the last login timestamp
            stmt = con.prepareStatement("UPDATE esf_userlogin SET reset_pickup_code=NULL, forgot_pickup_code=NULL, invalid_login_count=0, invalid_answer_count=0, request_forgot_count=0, last_login_timestamp=?, last_login_ip=? WHERE user_id=?");
            stmt.setTimestamp(1,DateUtil.getCurrentSqlTimestamp());
            stmt.setString(2, ipAddr);
            stmt.setObject(3,user.getId().getUUID(),Types.OTHER);
            stmt.executeUpdate();
            con.commit();
            
            return userLoginInfo;
        }
        catch(SQLException e) 
        {
            _logger.sqlerr(e,"PasswordManager.login() user id: " + user.getId() + "; name: " + user.getFullDisplayName());
            pool.rollbackIgnoreException(con,e);
            throw new EsfException(app.getServerMessages().getString("system.error.unexpected"));
        }
        catch( EsfException e )
        {
            pool.rollbackIgnoreException(con);
            throw e;
        }
        finally
        {
            app.cleanupPool(pool,con,stmt);
        }
    }
    

    /**
     * Checks the forgotten password answer matches for the user specified.
     * @return the String last login message to display for this user; null if the check is not successful
     * @throws EsfException if the check failure should report an error back to the user.
     */
    public void checkForgottenPasswordAnswer(User user, String answer)
        throws EsfException
    {
        if ( user == null || answer == null )
            throw new EsfException(app.getServerMessages().getString("password.error.missingForgotPasswordAuthParams"));
        
        if ( ! user.isEnabled() )
            throw new EsfException(app.getServerMessages().getString("password.error.accountDisabled"));
        
        ConnectionPool    pool  = app.getConnectionPool();
        Connection        con   = pool.getConnection();
        PreparedStatement stmt  = null;
        
        try
        {
            stmt = con.prepareStatement("SELECT invalid_answer_count,forgotten_answer_hash,hash_version FROM esf_userlogin WHERE user_id=?");
            stmt.setObject(1,user.getId().getUUID(),Types.OTHER);
            ResultSet rs = stmt.executeQuery();
            if ( ! rs.next() )
            {
                con.rollback();
                throw new EsfException(app.getServerMessages().getString("password.error.missingLoginData"));
            }
            
            short invalidAnswerCount = rs.getShort(1);
            if ( invalidAnswerCount >= maxFailedLoginAttempts )
                throw new EsfException(app.getServerMessages().getString("password.error.alreadyMaxFailedForgotPassword"));
            
            String answerHash = rs.getString(2);
            if ( rs.wasNull() )
            {
                con.rollback();
                throw new EsfException(app.getServerMessages().getString("password.error.missingForgotPassword"));
            }
            
            short hashVersion = rs.getShort(3);
            stmt.close(); stmt = null;
            
            if ( ! isCorrectForgotPasswordAnswer(user.getId(), answer, answerHash, hashVersion) )
            {
                stmt = con.prepareStatement("UPDATE esf_userlogin SET invalid_answer_count=invalid_answer_count+1 WHERE user_id=?");
                stmt.setObject(1,user.getId().getUUID(),Types.OTHER);
                stmt.executeUpdate();
                con.commit();
               
                ++invalidAnswerCount;
                if ( invalidAnswerCount >= maxFailedLoginAttempts )
                    throw new EsfException(app.getServerMessages().getString("password.error.maxFailedForgotPassword"));

                throw new EsfException(app.getServerMessages().getString("password.error.invalidForgotPasswordAnswer"));
            }
            
            // Looks like they are okay
            con.commit();
        }
        catch(SQLException e) 
        {
            _logger.sqlerr(e,"PasswordManager.checkForgottenPasswordAnswer() user id: " + user.getId() + "; name: " + user.getFullDisplayName());
            pool.rollbackIgnoreException(con,e);
            throw new EsfException(app.getServerMessages().getString("system.error.unexpected"));
        }
        catch( EsfException e )
        {
            pool.rollbackIgnoreException(con);
            throw e;
        }
        finally
        {
            app.cleanupPool(pool,con,stmt);
        }
    }
    
    
    public void requestForgotPassword(User forUser, String externalContextPath, String byUserIP, String byUserAgent)
        throws EsfException
    {
        if ( forUser == null )
            throw new EsfException(app.getServerMessages().getString("password.error.missingForgotPasswordRequestParams"));
        
        if ( ! forUser.isEnabled() )
            throw new EsfException(app.getServerMessages().getString("password.error.accountDisabled"));
        
        if ( ! forUser.hasPassword() )
            throw new EsfException(app.getServerMessages().getString("password.error.noPasswordSet"));
        
        ConnectionPool    pool  = app.getConnectionPool();
        Connection        con   = pool.getConnection();
        PreparedStatement stmt  = null;
        
        try
        {
            String forgotPickupCode = null;
            String resetPickupCode = null;
            short requestForgotCount = 0;
    
            stmt = con.prepareStatement("SELECT forgot_pickup_code,reset_pickup_code,request_forgot_count FROM esf_userlogin WHERE user_id=?");
            stmt.setObject(1,forUser.getId().getUUID(),Types.OTHER);
            ResultSet rs = stmt.executeQuery();
            if ( rs.next() )
            {
                forgotPickupCode = rs.getString(1); // Let's reuse any existing forgot code 
                if ( rs.wasNull() )
                    forgotPickupCode = null;
                resetPickupCode = rs.getString(2); // Let's reuse any existing reset code 
                if ( rs.wasNull() )
                    resetPickupCode = null;
                requestForgotCount = rs.getShort(3);
            }
            stmt.close(); stmt = null;
            
            if ( requestForgotCount >= maxRequestForgottenPasswordAttempts ) {
            	con.rollback();
                throw new EsfException(app.getServerMessages().getString("password.error.alreadyMaxRequestForgotPassword"));
            }
            
            // If we don't have a code, let's generate a unique one...
            while( forgotPickupCode == null )
            {
                forgotPickupCode = app.getRandomKey().getPickupCodeString();
                if ( isForgotPickupCodeInUse(con,forgotPickupCode) )
                    forgotPickupCode = null;
            }
            
            // If we don't have a code, let's generate a unique one...
            while( resetPickupCode == null )
            {
                resetPickupCode = app.getRandomKey().getPickupCodeString();
                if ( isResetPickupCodeInUse(con,resetPickupCode) )
                    resetPickupCode = null;
            }
            
            stmt = con.prepareStatement("UPDATE esf_userlogin SET reset_pickup_code=?,forgot_pickup_code=?,request_forgot_count=request_forgot_count+1 WHERE user_id=?");
            stmt.setString(1,resetPickupCode);
            stmt.setString(2,forgotPickupCode);
            stmt.setObject(3,forUser.getId().getUUID(),Types.OTHER);
            
            stmt.executeUpdate();
            if ( EsfString.isNonBlank(externalContextPath) )
            	sendForgotPasswordEmail(con, forUser, forgotPickupCode, externalContextPath); // reset code is used after they answer the forgotten question correctly
            
            forUser.logSecurity(con,"Forgotten password reset requested on attempt " + (requestForgotCount+1) +  "; from IP address: " + byUserIP + "; with browser type: " + byUserAgent);
            con.commit();
        }
        catch(SQLException e) 
        {
            _logger.sqlerr(e,"PasswordManager.requestForgotPassword() user id: " + forUser.getId() + "; name: " + forUser.getFullDisplayName());
            pool.rollbackIgnoreException(con,e);
            throw new EsfException(app.getServerMessages().getString("system.error.unexpected"));
        }
        finally
        {
            app.cleanupPool(pool,con,stmt);
        }
    }

    public void requestSetPassword(User forUser, User byUser, String externalContextPath, String byUserIP)
        throws EsfException
    {
        if ( forUser == null )
            throw new EsfException(app.getServerMessages().getString("password.error.missingSetPasswordParams"));
        
        if ( ! forUser.isEnabled() )
            throw new EsfException(app.getServerMessages().getString("password.error.accountDisabled"));
        
        if ( EsfString.isBlank(byUserIP) )
        	byUserIP = "(IP address not set)";
        
        ConnectionPool    pool  = app.getConnectionPool();
        Connection        con   = pool.getConnection();
        PreparedStatement stmt  = null;
        
        try
        {
        	// User checks for "Reset " to determine if a real password is present or not.
            String fakePwdHash = RESET_PASSWORD_HASH_PREFIX + (new EsfDateTime()).toDateTimeMsecString(); // No value can be hashed to equal this string, so nobody can login now until it's reset
            String resetPickupCode = null;

            // Let's see if they have a login record or not...
            stmt = con.prepareStatement("SELECT reset_pickup_code FROM esf_userlogin WHERE user_id=?");
            stmt.setObject(1,forUser.getId().getUUID(),Types.OTHER);
            ResultSet rs = stmt.executeQuery();
            boolean doInsert = true;
            if ( rs.next() )
            {
                doInsert = false;
                resetPickupCode = rs.getString(1); // Let's reuse any existing reset code 
                if ( rs.wasNull() )
                    resetPickupCode = null;
            }
            stmt.close(); stmt = null;
            
            // If we don't have a code, let's generate a unique one...
            while( resetPickupCode == null )
            {
                resetPickupCode = app.getRandomKey().getPickupCodeString();
                if ( isResetPickupCodeInUse(resetPickupCode) )
                    resetPickupCode = null;
            }
            
            if ( doInsert )
            {
                stmt = con.prepareStatement(
                        "INSERT INTO esf_userlogin (user_id,pwd_hash,forgotten_question,forgotten_answer_hash," +
                        "reset_pickup_code,forgot_pickup_code,last_login_timestamp,last_login_ip,password_set_timestamp,hash_version,invalid_login_count,invalid_answer_count,request_forgot_count) " +
                        "VALUES (?,?,NULL,NULL,?,NULL,NULL,NULL,NULL,0,0,0,0)"
                                           );
                stmt.setObject(1,forUser.getId().getUUID(),Types.OTHER);
                stmt.setString(2,fakePwdHash);
                stmt.setString(3,resetPickupCode);
            }
            else
            {
                stmt = con.prepareStatement("UPDATE esf_userlogin SET pwd_hash=?, hash_version=0, reset_pickup_code=? WHERE user_id=?");
                stmt.setString(1,fakePwdHash);
                stmt.setString(2,resetPickupCode);
                stmt.setObject(3,forUser.getId().getUUID(),Types.OTHER);
            }
            
            int num = stmt.executeUpdate();
            if ( num == 1 )
            {
                if ( EsfString.isNonBlank(externalContextPath) )
            	    sendSetPasswordEmail(con, forUser, resetPickupCode, externalContextPath);
            	forUser.logSecurity(con,"Password reset done by user: " + byUser.getFullDisplayName() + "; from IP: " + byUserIP);
                if ( byUser != null ) 
                {
                	byUser.logSecurity(con,"Password reset done for user: " + forUser.getFullDisplayName() + "; from IP: " + byUserIP);
                	app.getActivityLog().logSystemUserActivity(con,"User: " + byUser.getFullDisplayName() + "; reset the password for user: " + forUser.getFullDisplayName() + "; from IP: " + byUserIP);
                }
                con.commit();
                forUser.setPasswordHash(fakePwdHash);
            }
            else
                throw new EsfException("Unexpected SQL INSERT/UPDATE failed on esf_userlogin for user_id: " + forUser.getId());
        }
        catch(SQLException e) 
        {
            _logger.sqlerr(e,"PasswordManager.requestSetPassword() user id: " + forUser.getId() + "; name: " + forUser.getFullDisplayName());
            pool.rollbackIgnoreException(con,e);
            throw new EsfException(app.getServerMessages().getString("system.error.unexpected"));
        }
        catch( EsfException e )
        {
            pool.rollbackIgnoreException(con);
            throw e;
        }
        finally
        {
            app.cleanupPool(pool,con,stmt);
        }
    }
    
    protected void giveInvalidLoginAttemptCredit()
    {
        ConnectionPool    pool = app.getConnectionPool();
        Connection        con  = pool.getConnection();
        PreparedStatement stmt = null;
        try
        {
            stmt = con.prepareStatement("UPDATE esf_userlogin SET invalid_login_count=invalid_login_count-1 WHERE invalid_login_count>0");
            int num = stmt.executeUpdate();
            if ( num > 0 )
                _logger.debug("giveInvalidLoginAttemptCredit() - Gave " + num + " login failures credits.");
                        
            stmt.close();
            stmt = con.prepareStatement("UPDATE esf_userlogin SET invalid_answer_count=invalid_answer_count-1 WHERE invalid_answer_count>0");
            num = stmt.executeUpdate();
            if ( num > 0 )
                _logger.debug("giveInvalidLoginAttemptCredit() - Gave " + num + " invalid forgotten password answer credits.");
                        
            stmt.close();
            stmt = con.prepareStatement("UPDATE esf_userlogin SET request_forgot_count=request_forgot_count-1 WHERE request_forgot_count>0");
            num = stmt.executeUpdate();
            if ( num > 0 )
                _logger.debug("giveInvalidLoginAttemptCredit() - Gave " + num + " request forgotten password credits.");
                        
            con.commit();
        }
        catch(SQLException e) 
        {
            _logger.sqlerr(e,"PasswordManager.giveInvalidLoginAttemptCredit()");
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
            app.cleanupPool(pool,con,stmt);
        }
    }
    

    protected boolean isForgotPickupCodeInUse(Connection con, String code)
    	throws SQLException
    {
        PreparedStatement stmt  = null;
        boolean           inUse = true;
        
        try
        {
            stmt = con.prepareStatement("SELECT COUNT(*) FROM esf_userlogin WHERE forgot_pickup_code=?");
            stmt.setString(1,code);
            ResultSet rs = stmt.executeQuery();
            if ( rs.next() )
            {
                int count = rs.getInt(1);
                inUse = count > 0;
            }
            return inUse;
        }
        catch(SQLException e) 
        {
            _logger.sqlerr(e,"PasswordManager.isForgotPickupCodeInUse(con) code: " + code);
            throw e;
        }
        finally
        {
            app.cleanupStatement(stmt);
        }
    }
    
    protected boolean isForgotPickupCodeInUse(String code)
    {
        ConnectionPool    pool  = app.getConnectionPool();
        Connection        con   = pool.getConnection();
        boolean           inUse = true;
        
        try
        {
        	inUse = isForgotPickupCodeInUse(con,code);
            con.commit();
            return inUse;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return inUse;
        }
        finally
        {
            app.cleanupPool(pool,con,null);
        }
    }
    
    protected boolean isResetPickupCodeInUse(Connection con, String code)
    	throws SQLException
    {
        PreparedStatement stmt  = null;
        boolean           inUse = true;
        
        try
        {
            stmt = con.prepareStatement("SELECT COUNT(*) FROM esf_userlogin WHERE reset_pickup_code=?");
            stmt.setString(1,code);
            ResultSet rs = stmt.executeQuery();
            if ( rs.next() )
            {
                int count = rs.getInt(1);
                inUse = count > 0;
            }
            return inUse;
        }
        catch(SQLException e) 
        {
            _logger.sqlerr(e,"PasswordManager.isResetPickupCodeInUse(con) code: " + code);
            throw e;
        }
        finally
        {
            app.cleanupStatement(stmt);
        }
    }
    
    protected boolean isResetPickupCodeInUse(String code)
    {
        ConnectionPool    pool  = app.getConnectionPool();
        Connection        con   = pool.getConnection();
        boolean           inUse = true;
        
        try
        {
        	inUse = isResetPickupCodeInUse(con,code);
            con.commit();
            return inUse;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return inUse;
        }
        finally
        {
            app.cleanupPool(pool,con,null);
        }
    }
    
    public UserLoginInfo getUserLoginInfoByPasswordPickupCode(Connection con, String pickupCode)
        throws SQLException
    {
        PreparedStatement stmt = null;
    
        try
        {
            stmt = con.prepareStatement( "SELECT user_id FROM esf_userlogin WHERE reset_pickup_code=?" );
            stmt.setString(1,pickupCode);
            ResultSet rs = stmt.executeQuery();
            if ( ! rs.next() )
                return null;
            
            return getUserLoginInfoByUserId(con,new EsfUUID((UUID)rs.getObject(1)));
        }
        finally
        {
            app.cleanupStatement(stmt);
        }
    }


    public UserLoginInfo getUserLoginInfoByForgottenPasswordPickupCode(Connection con, String pickupCode)
	    throws SQLException
	{
	    PreparedStatement stmt = null;
	
	    try
	    {
	        stmt = con.prepareStatement( "SELECT user_id FROM esf_userlogin WHERE forgot_pickup_code=?" );
	        stmt.setString(1,pickupCode);
	        ResultSet rs = stmt.executeQuery();
	        if ( ! rs.next() )
	            return null;
	        
	        return getUserLoginInfoByUserId(con,new EsfUUID((UUID)rs.getObject(1)));
	    }
	    finally
	    {
	        app.cleanupStatement(stmt);
	    }
	}
	

    /**
     * Retrieves a UserLoginInfo object based on the supplied user id
     * @param con the Connection object to use for transaction control
     * @param userId the EsfUUID user id to retrieve
     * @return the UserLoginInfo object as loaded from the database if found; else null
     * @throws SQLException thrown if there's a db error
     */
    public UserLoginInfo getUserLoginInfoByUserId(Connection con, EsfUUID userId)
        throws SQLException
    {
        PreparedStatement stmt = null;

        try
        {
            stmt = con.prepareStatement( 
                    "SELECT pwd_hash,forgotten_question,forgotten_answer_hash,reset_pickup_code,forgot_pickup_code,last_login_timestamp,last_login_ip," +
                    "password_set_timestamp,hash_version,invalid_login_count,invalid_answer_count,request_forgot_count FROM esf_userlogin WHERE user_id=?" 
                                       );
            stmt.setObject(1,userId.getUUID(),Types.OTHER);
            ResultSet rs = stmt.executeQuery();
            if ( ! rs.next() )
                return null;
            
            String pwdHash = rs.getString(1);
            if ( rs.wasNull() )
                pwdHash =  null;
            String forgottenQuestion = rs.getString(2);
            if ( rs.wasNull() )
                forgottenQuestion =  null;
            String forgottenAnswerHash = rs.getString(3);
            if ( rs.wasNull() )
                forgottenAnswerHash =  null;
            String resetPickupCode = rs.getString(4);
            if ( rs.wasNull() )
                resetPickupCode =  null;
            String forgotPickupCode = rs.getString(5);
            if ( rs.wasNull() )
                forgotPickupCode =  null;
            java.sql.Timestamp t = rs.getTimestamp(6);
            if ( rs.wasNull() )
                t =  null;
            EsfDateTime lastLoginTimestamp = null;
            if ( t != null )
                lastLoginTimestamp = new EsfDateTime(t);
            String lastLoginIP = rs.getString(7);
            t = rs.getTimestamp(8);
            if ( rs.wasNull() )
                t =  null;
            EsfDateTime passwordSetTimestamp = null;
            if ( t != null )
                passwordSetTimestamp = new EsfDateTime(t);
            short hashVersion = rs.getShort(9);
            short invalidLoginCount = rs.getShort(10);
            short invalidAnswerCount = rs.getShort(11);
            short requestForgotCount = rs.getShort(12);
            stmt.close(); stmt = null;
            
            return new UserLoginInfo(userId,pwdHash,forgottenQuestion,forgottenAnswerHash,resetPickupCode,forgotPickupCode,
                                     lastLoginTimestamp,lastLoginIP,passwordSetTimestamp,hashVersion,invalidLoginCount,invalidAnswerCount,requestForgotCount);
        }
        finally
        {
            app.cleanupStatement(stmt);
        }
    }
    
    
    /**
     * 
     * Retrieves a UserLoginInfo object based on the supplied set password pickup code.
     * @param pickupCode the String set password pickup code to retrieve
     * @return the UserLoginInfo object as loaded from the database if found; else null
     */
    public UserLoginInfo getUserLoginInfoByPasswordPickupCode(String pickupCode)
    {
        app.debug("PasswordManager.getUserLoginInfoByPasswordPickupCode() for pickupCode: " + pickupCode);
        
        ConnectionPool    pool = app.getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            UserLoginInfo userLogin = getUserLoginInfoByPasswordPickupCode(con,pickupCode);
            con.commit();
            return userLogin;
        }
        catch(SQLException e) 
        {
            app.sqlerr(e,"PasswordManager.getUserLoginInfoByPasswordPickupCode() for pickupCode: " + pickupCode);
            pool.rollbackIgnoreException(con,e);
            return null;
        }
        finally
        {
            app.cleanupPool(pool,con,null);
        }
    }

    /**
     * 
     * Retrieves a UserLoginInfo object based on the supplied set password pickup code.
     * @param pickupCode the String set password pickup code to retrieve
     * @return the UserLoginInfo object as loaded from the database if found; else null
     */
    public UserLoginInfo getUserLoginInfoByForgottenPasswordPickupCode(String pickupCode)
    {
        app.debug("PasswordManager.getUserLoginInfoByForgottenPasswordPickupCode() for pickupCode: " + pickupCode);
        
        ConnectionPool    pool = app.getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            UserLoginInfo userLogin = getUserLoginInfoByForgottenPasswordPickupCode(con,pickupCode);
            con.commit();
            return userLogin;
        }
        catch(SQLException e) 
        {
            app.sqlerr(e,"PasswordManager.getUserLoginInfoByForgottenPasswordPickupCode() for pickupCode: " + pickupCode);
            pool.rollbackIgnoreException(con,e);
            return null;
        }
        finally
        {
            app.cleanupPool(pool,con,null);
        }
    }

    /**
     * 
     * Retrieves a UserLoginInfo object based on the supplied user id.
     * @param userId the EsfUUID user id to retrieve
     * @return the UserLoginInfo object as loaded from the database if found; else null
     */
    public UserLoginInfo getUserLoginInfoByUserId(EsfUUID userId)
    {
        app.debug("PasswordManager.getUserLoginInfoByUserId() for userId: " + userId);
        
        ConnectionPool    pool = app.getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            UserLoginInfo userLogin = getUserLoginInfoByUserId(con,userId);
            con.commit();
            return userLogin;
        }
        catch(SQLException e) 
        {
            app.sqlerr(e,"PasswordManager.getUserLoginInfoByUserId() for userId: " + userId);
            pool.rollbackIgnoreException(con,e);
            return null;
        }
        finally
        {
            app.cleanupPool(pool,con,null);
        }
    }

    public static void main(String[] args)
    {
    	Application app = Application.getInstallationInstance("/");
    	
    	String password = "password";
    	
    	EsfUUID userId = new EsfUUID();
    	
    	SecureHash secureHash = app.getSecureHash();
    	String hash = null;
    	
    	long startMillis = System.currentTimeMillis();
    	for( int i=0; i<100; ++i)
    	{
        	hash = secureHash.getBase64Digest(userId+password+"pwd"); 
    	}
    	long endMillis = System.currentTimeMillis();
    	System.out.println("100 secureHash took " + (endMillis-startMillis) + " mecs");
    	System.out.println("last hash: " + hash);
    	
    	startMillis = System.currentTimeMillis();
    	for( int i=0; i<100; ++i)
    	{
    		hash = BCrypt.hashpw(userId+password, BCrypt.gensalt(10));
    	}
    	endMillis = System.currentTimeMillis();
    	System.out.println("100 bcrypts(10) took " + (endMillis-startMillis) + " mecs");
    	System.out.println("last hash: " + hash);
    	
    	startMillis = System.currentTimeMillis();
    	for( int i=0; i<100; ++i)
    	{
    		hash = BCrypt.hashpw(userId+password, BCrypt.gensalt(11));
    	}
    	endMillis = System.currentTimeMillis();
    	System.out.println("100 bcrypts(11) took " + (endMillis-startMillis) + " mecs");
    	System.out.println("last hash: " + hash);
    	
    	
    	startMillis = System.currentTimeMillis();
    	for( int i=0; i<100; ++i)
    	{
    		hash = BCrypt.hashpw(userId+password, BCrypt.gensalt(12));
    	}
    	endMillis = System.currentTimeMillis();
    	System.out.println("100 bcrypts(12) took " + (endMillis-startMillis) + " mecs");
    	System.out.println("last hash: " + hash);

    	System.exit(0);
    }
} 
