// Copyright (c) 2011-2015 Yozons, Inc.  All rights reserved.
// This file is proprietary and confidential and contains Yozons trade secrets.

package com.esignforms.open.admin;

import com.esignforms.open.Application;



/**
 * Checks on threads running in the JVM.  Currently, this is specific to Tomcat for various thread checks
 * @author Yozons, Inc. 
 */
public class ThreadChecker implements java.io.Serializable
{
	private static final long serialVersionUID = -2604878411125302908L;

	private Thread[] allThreads      = null;
    private int      allThreadsCount = 0;
    
    public ThreadChecker()
    {
        ThreadGroup topThreadGroup = Thread.currentThread().getThreadGroup();
        while( topThreadGroup.getParent() != null )
            topThreadGroup = topThreadGroup.getParent();
        
        int numThreads = topThreadGroup.activeCount();
        int numGroups  = topThreadGroup.activeGroupCount();
        
        int totalEstimatedThreads = (numThreads * numGroups) * 2;
        
        // Get the threads
        allThreads = new Thread[totalEstimatedThreads];
        allThreadsCount = topThreadGroup.enumerate(allThreads);
        // If we're unlucky enough to have too few entries, try one more time using the actual count received here.
        if ( allThreadsCount > totalEstimatedThreads )
        {
            totalEstimatedThreads = allThreadsCount * 2;
            allThreads = new Thread[totalEstimatedThreads];
            allThreadsCount = topThreadGroup.enumerate(allThreads);
        }
    }
    
    public int getTotalThreadCount()
    {
        return allThreadsCount;
    }
    
    public boolean isThreadRunning(String threadName)
    {
        for( int i=0; i < allThreadsCount; ++i )
        {
            if ( allThreads[i].getName().equals(threadName) )
                return allThreads[i].isAlive();
        }
        return false;
    }
    public void joinThread(String threadName, long waitTimeMsecs) 
    {
        for( int i=0; i < allThreadsCount; ++i )
        {
            if ( allThreads[i].getName().equals(threadName) )
            {
                if ( allThreads[i].isAlive() )
                {
                	try
                	{
                		allThreads[i].join(waitTimeMsecs);
                	}
                	catch( Exception e ) {}
                }
                break;
            }
        }    	
    }
    
    public boolean isBackgrounderRunning()
    {
    	return isThreadRunning(Application.BACKGROUNDER_THREAD_NAME_PREFIX+Application.getInstance().getContextPath());
    }
    public void joinBackgrounder(long waitTimeMsecs) 
    {
    	joinThread(Application.BACKGROUNDER_THREAD_NAME_PREFIX+Application.getInstance().getContextPath(), waitTimeMsecs);
    }
    
    public boolean isOutboundEmailRunning()
    {
    	return isThreadRunning(Application.OUTBOUND_EMAIL_PROCESSOR_THREAD_NAME_PREFIX+Application.getInstance().getContextPath());
    }
    public void joinOutboundEmail(long waitTimeMsecs) 
    {
    	joinThread(Application.OUTBOUND_EMAIL_PROCESSOR_THREAD_NAME_PREFIX+Application.getInstance().getContextPath(), waitTimeMsecs);
    }
   
    public boolean isInboundEmailRunning()
    {
    	return isThreadRunning(Application.INBOUND_EMAIL_PROCESSOR_THREAD_NAME_PREFIX+Application.getInstance().getContextPath());
    }
    public void joinInboundEmail(long waitTimeMsecs) 
    {
    	joinThread(Application.INBOUND_EMAIL_PROCESSOR_THREAD_NAME_PREFIX+Application.getInstance().getContextPath(), waitTimeMsecs);
    }
   
    public boolean isHttpSendRunning()
    {
    	return isThreadRunning(Application.HTTP_SEND_PROCESSOR_THREAD_NAME_PREFIX+Application.getInstance().getContextPath());
    }
    public void joinHttpSend(long waitTimeMsecs) 
    {
    	joinThread(Application.HTTP_SEND_PROCESSOR_THREAD_NAME_PREFIX+Application.getInstance().getContextPath(), waitTimeMsecs);
    }
   
    public int[] getHttpsThreadCounts()
    {
        int[] counts = new int[2];
        
        for( int i=0; i < allThreadsCount; ++i )
        {
            if ( allThreads[i].getName().matches("http-(bio|nio|apr)-\\d*443-exec-\\d*") )
            {
                if ( allThreads[i].isAlive() )
                    ++counts[0];
                else
                    ++counts[1];
            }
        }
        return counts;
    }
    
    public int[] getHttpThreadCounts()
    {
        int[] counts = new int[2];
        
        for( int i=0; i < allThreadsCount; ++i )
        {
            if ( allThreads[i].getName().matches("http-(bio|nio|apr)-\\d*80-exec-\\d*") )
            {
                if ( allThreads[i].isAlive() )
                    ++counts[0];
                else
                    ++counts[1];
            }
        }
        return counts;
    }
}