// Copyright (C) 2009-2011 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.admin;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import java.util.LinkedList;
import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.admin.StatsInfo;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.user.Group;
import com.esignforms.open.user.User;
import com.esignforms.open.util.DateUtil;

/**
 * Manages the esf_stats table
 *
 * @author Yozons, Inc.
 */
public class Stats 
{
    private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(Stats.class);

    
    public Stats()
    {
    }
    
    /**
     * Records the specified db, archive size and number of users.
     * @param con the Connection to use
     * @param dbSize the long size of the database
     * @param archiveSize the long size of the archive
     * @param numTransactions the int number of transactions
     * @param numUsers the int number of users
     * @param numGroups the int number of groups
     * @exception java.sql.SQLException thrown if there's a DB error
     */
    public void record(Connection con, long dbSize, long archiveSize, int numTransactions, int numUsers, int numGroups)
        throws java.sql.SQLException
    {
        PreparedStatement stmt = null;
        
        _logger.info("record(con): dbSize: " + (new EsfInteger(dbSize)) + "; archiveSize: " + (new EsfInteger(archiveSize)) +
        		"; numTransactions: " + (new EsfInteger(numTransactions)) + "; numUsers: " + (new EsfInteger(numUsers)) + "; numGroups: " + (new EsfInteger(numGroups)));
        
        try 
        {
            String sql = "INSERT INTO esf_stats (collection_timestamp,db_size,archive_size,num_transactions,num_users,num_groups) VALUES(?,?,?,?,?,?)";
            
            stmt = con.prepareStatement(sql);
            stmt.setTimestamp(1,DateUtil.getCurrentSqlTimestamp());
            stmt.setLong(2,dbSize);
            stmt.setLong(3,archiveSize);
            stmt.setInt(4,numTransactions);
            stmt.setInt(5,numUsers);
            stmt.setInt(6,numGroups);
            
            int num = stmt.executeUpdate();
            if ( num != 1 )
            {
            	_logger.error("record(con) - Failed to record: dbSize: " + (new EsfInteger(dbSize)) + "; archiveSize: " + (new EsfInteger(archiveSize)) +
                		"; numTransactions: " + (new EsfInteger(numTransactions)) + "; numUsers: " + (new EsfInteger(numUsers)) + "; numGroups: " + (new EsfInteger(numGroups)));
            }
        }
        catch( java.sql.SQLException e )
        {
        	_logger.sqlerr(e,"record(con) - Failed to record: dbSize: " + (new EsfInteger(dbSize)) + "; archiveSize: " + (new EsfInteger(archiveSize)) +
            		"; numTransactions: " + (new EsfInteger(numTransactions)) + "; numUsers: " + (new EsfInteger(numUsers)) + "; numGroups: " + (new EsfInteger(numGroups)));
            throw e;
        }
        finally
        {
            Application.getInstance().cleanupStatement(stmt);
        }
    }

    /**
     * Records the specified db and archive size.
     * @param dbSize the long size of the database
     * @param archiveSize the long size of the archive
     * @param numUsers the int number of users
     * @param numGroups the int number of groups
    */
    public void record(long dbSize, long archiveSize, int numTransactions, int numUsers, int numGroups)
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            record(con,dbSize,archiveSize,numTransactions,numUsers,numGroups);
            con.commit();
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }
    
    /**
     * Records the current values for our stats.
     */
    public void recordCurrentValues()
    {
    	long dbSize = getDatabaseSize();
    	long archiveSize = getArchiveSize();
    	int numTransactions = Transaction.Manager.getCount();
    	int numUsers = User.Manager.getNumUsers();
    	int numGroups = Group.Manager.getNumGroups();
    	record(dbSize, archiveSize, numTransactions, numUsers, numGroups);
    }

    /**
     * Retrieves all StatsInfo objects.
     * @param con the Connection object to use for transaction control
     * @return the StatsInfo list of records found
     * @throws SQLException thrown if there's a db error
     */
    public List<StatsInfo> getAll(Connection con)
        throws SQLException
    {
        PreparedStatement stmt = null;
        
        try
        {
            LinkedList<StatsInfo> list = new LinkedList<StatsInfo>();
            stmt = con.prepareStatement("SELECT collection_timestamp,db_size,archive_size,num_users,num_groups FROM esf_stats ORDER BY collection_timestamp");
            ResultSet rs = stmt.executeQuery();
            while( rs.next() )
            {
               StatsInfo record = new StatsInfo(new EsfDateTime(rs.getTimestamp(1)),rs.getLong(2),rs.getLong(3),rs.getInt(4),rs.getInt(5));
               list.add(record);
            }
            
            return list;
        }
        finally
        {
        	Application.getInstance().cleanupStatement(stmt);
        }
    }
    
    /**
     * Retrieves all StatsInfo objects.
     * @return the StatsInfo list of records found
     */
    public List<StatsInfo> getAll()
    {
        ConnectionPool    pool = Application.getInstance().getConnectionPool();
        Connection        con  = pool.getConnection();
        try
        {
            List<StatsInfo> records = getAll(con);
            con.commit();
            return records;
        }
        catch(SQLException e) 
        {
            pool.rollbackIgnoreException(con,e);
            return null;
        }
        finally
        {
        	Application.getInstance().cleanupPool(pool,con,null);
        }
    }

    
    public int cleanup(Connection con,EsfDateTime olderThanDateTime)
        throws SQLException
    {
        PreparedStatement stmt = null;
        
        try 
        {
            // Remove all db size collected before the date specified
            Timestamp olderTimestamp = olderThanDateTime.toSqlTimestamp();
            String sql = "DELETE FROM esf_stats WHERE collection_timestamp < ?";
            stmt = con.prepareStatement(sql);
            stmt.setTimestamp(1,olderTimestamp);
    
            int num = stmt.executeUpdate();
            _logger.info("cleanup() - Removed " + num + " stats collection entries before " + olderThanDateTime + "." );
            return num;
        } 
        catch( java.sql.SQLException e )
        {
            _logger.sqlerr(e,"cleanup()");
            throw e;
        }
        finally
        {
        	Application.getInstance().cleanupStatement(stmt);
        }
    }
    
    
    protected long calculateTotalFileSize(File basePath)
    {
    	long totalSize = 0;
    	
    	File[] fileArray = basePath.listFiles();
    	if ( fileArray == null )
    	{
			_logger.warn("calculateTotalFileSize() - path: " + basePath.getAbsolutePath() + " cannot be read so total size cannot be accurately computed.");
			return totalSize;
    	}
    		
    	for( File file : fileArray )
    	{
    		if ( file.isDirectory() )
    			totalSize += calculateTotalFileSize(file);
    		else
    			totalSize += file.length();
    	}
    	
    	return totalSize;
    }
    
    public long getDatabaseSize()
    {
        try
        {
        	ConnectionPool pool = Application.getInstance().getConnectionPool();
        	String dbname = pool.getUserName();

        	File base = new File(Application.getInstance().getDeploymentBasePath());
        	if ( ! base.isDirectory() )
        	{
        		_logger.error("getDatabaseSize() -- Base directory not found: " + base.getAbsolutePath());
        		return 0;
        	}

        	File deploymentBase = new File(base,dbname);
        	if ( ! deploymentBase.isDirectory() )
        	{
        		_logger.error("getDatabaseSize() -- Deployment's base directory not found: " + deploymentBase.getAbsolutePath());
        		return 0;
        	}
        	
        	File dbBase = new File(deploymentBase,"current");
        	if ( ! dbBase.isDirectory() )
        	{
        		_logger.error("getDatabaseSize() -- Database directory not found: " + dbBase.getAbsolutePath());
        		return 0;
        	}
        	
        	return calculateTotalFileSize(dbBase);
        }
        catch( Exception e )
        {
        	return 0;
        }
    }
    
    public long getArchiveSize()
    {
        try
        {
        	ConnectionPool pool = Application.getInstance().getConnectionPool();
        	String dbname = pool.getUserName();

        	File base = new File(Application.getInstance().getDeploymentBasePath());
        	if ( ! base.isDirectory() )
        	{
        		_logger.error("getArchiveSize() -- Base directory not found: " + base.getAbsolutePath());
        		return 0;
        	}
        	
        	File deploymentBase = new File(base,dbname);
        	if ( ! deploymentBase.isDirectory() )
        	{
        		_logger.error("getDatabaseSize() -- Deployment's base directory not found: " + deploymentBase.getAbsolutePath());
        		return 0;
        	}
        	
        	File archiveBase = new File(deploymentBase,"archive");
        	if ( ! archiveBase.isDirectory() )
        	{
        		_logger.error("getArchiveSize() -- Archive directory not found: " + archiveBase.getAbsolutePath());
        		return 0;
        	}
        	
        	return calculateTotalFileSize(archiveBase);
        }
        catch( Exception e )
        {
        	return 0;
        }
    }

}