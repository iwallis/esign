// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.admin;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfDateTime;

/**
 * Defines a given stats record at a particular date/time.
 * @author Yozons, Inc.
 */

public class StatsInfo
{
	protected EsfDateTime	collectionTimestamp;
    protected long      	dbSize;
    protected long      	archiveSize;
    public    int			numUsers;
    public    int			numGroups;

	public StatsInfo(EsfDateTime collectionTimestamp, long dbSize, long archiveSize, int numUsers, int numGroups)
	{
        this.collectionTimestamp    = collectionTimestamp;
        this.dbSize                 = dbSize;
		this.archiveSize            = archiveSize;
		this.numUsers				= numUsers;
		this.numGroups				= numGroups;
	}
    
	public EsfDateTime getCollectionTimestamp()
	{
		return collectionTimestamp;
	}
    
    public long getDbSize()
    {
        return dbSize;
    }
    public long getDbSizeMb()
    {
        return dbSize / Literals.MB;
    }
    
    public long getArchiveSize()
    {
        return archiveSize;
    }
    public long getArchiveSizeMb()
    {
        return archiveSize / Literals.MB;
    }

    public long getNumUsers()
    {
        return numUsers;
    }

    public long getNumGroups()
    {
        return numGroups;
    }
}

