// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.admin;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

import com.esignforms.open.Application;
import com.esignforms.open.vaadin.EsfVaadinUI;
import com.vaadin.server.VaadinSession;

/**
 * This class holds information about a tracked session, and it can be passivated/activated as 
 * the web server is restarted.
 * 
 * @author Yozons, Inc.
 */
public class SessionTrackerInfo
	implements java.io.Serializable, javax.servlet.http.HttpSessionActivationListener
{    
	private static final long serialVersionUID = -7056716521882851151L;

	private static final com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(SessionTrackerInfo.class);
	private static transient List<HttpSession> _activatedSessions = java.util.Collections.synchronizedList(new LinkedList<HttpSession>());
	
	private String sessionId 	= null;
	public String userId 		= null;
	public String name 			= null;
	public String email 		= null;
    	
	public SessionTrackerInfo(String sessId)
	{
		sessionId = sessId;
	}
    	
	public String getSessionId()
	{
		return sessionId;
	}
    	
    // For HttpSessionActivationListener interface
	public void sessionDidActivate(HttpSessionEvent se)
	{
		HttpSession sess = se.getSession();
		
		_logger.debug("sessionDidActivate() - Session did activate: " + sess.getId());
		_logger.debug("sessionDidActivate() - activated sessionInfo.sessionId: " + sessionId);
		_logger.debug("sessionDidActivate() - activated sessionInfo.userId: " + userId);
		_logger.debug("sessionDidActivate() - activated sessionInfo.name: " + name);
		_logger.debug("sessionDidActivate() - activated sessionInfo.email: " + email);
		
		// Because Tomcat can resume sessions before it even initializes the context, if we don't
		// have our objects yet, we store the info in a list that those routines can check at startup.
		if ( _activatedSessions != null )
		{
			synchronized(_activatedSessions)
			{
				_activatedSessions.add(sess);
			}
		}
		else
			Application.getInstance().getSessionTracker().addSession(sess);
		
		// Reset any of our session objects
		VaadinSession vs = VaadinSession.getCurrent();
		if ( vs == null )
			_logger.info("sessionDidActivate() - No VaadinSession.getCurrent() so cannot reactivate EsfVaadinUI with session id: " + sess.getId());
		else
		{
			for( com.vaadin.ui.UI vaadin : vs.getUIs() )
			{
				if ( vaadin instanceof EsfVaadinUI )
				{
					((EsfVaadinUI)vaadin).reloadAfterSessionReactivation();
					_logger.info("Reactivated EsfVaadinUI with session id: " + sess.getId());
				}
			}
		}
	}

    // For HttpSessionActivationListener interface
	public void sessionWillPassivate(HttpSessionEvent se)
	{
		_logger.debug("sessionWillPassivate() - Session will passivate: " + se.getSession().getId());
	}
	
	public static synchronized HttpSession[] getAndClearActivatedSessions()
	{
		if ( _activatedSessions == null )
			return new HttpSession[0];
		
		HttpSession[] sessions;
		synchronized(_activatedSessions)
		{
			sessions = new HttpSession[_activatedSessions.size()];
			_activatedSessions.toArray(sessions);
			_activatedSessions.clear();
			_activatedSessions = null;
		}
		return sessions;
	}
}
