// Copyright (C) 2011 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.admin;

import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.email.OutboundEmailMessageResponse;
import com.esignforms.open.email.SimpleEmailRetriever;
import com.esignforms.open.util.DateUtil;

/** 
 * The InboundEmailProcessor runs in its own thread, processing incoming emails that are queued up for IMAP retrieval.
 * 
 * @author Yozons, Inc.
 */
public class InboundEmailProcessor
    implements java.lang.Runnable
{
    private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(InboundEmailProcessor.class);
    
    private boolean 	shouldContinue  = true;
    private long    	waitTimeMsec    = 0;
    
    private Application app         	= null;

    public InboundEmailProcessor( Application app )
    {
        this.app = app;
        setWaitMinutes( /*app.getBackgrounderWaitIntervalMins()*/ 1 );
    }
    
    
    /**
     * Sets the wait time in minutes between background runs.
     * @param numMinutes the int that specifies the number of minutes to wait between
     *        work episodes.
     */
    public synchronized void setWaitMinutes( int numMinutes )
    {
        if ( numMinutes < 1 )
            numMinutes = 1;
            
        waitTimeMsec = DateUtil.minutesToMillis(numMinutes);
    }
    
    /**
     * This method is called periodically to handle whatever email tasks it can do.
     */
    protected void doEmailTasks()
    {
    	int numCorrelated = 0;
    	
    	SimpleEmailRetriever retriever = null;
    	try
    	{
    		retriever = new SimpleEmailRetriever();
    		
    		List<OutboundEmailMessageResponse> responseList = retriever.processInbox();
    		if ( responseList != null )
    		{
	    		for( OutboundEmailMessageResponse response : responseList )
	    		{
	    			if ( response.saveRetryDuplicateKeys() )
	    			{
		    			++numCorrelated;
		    			app.getSecureRandom().setSeed(response.getEmailFrom()+response.getOutboundEmailMessageId()); // we receive emails at random times so we'll use this to help keep our PRNG random from back-guessing its seed
	    			}
	    		}
    		}
    	}
    	catch( Exception e ) 
    	{
    		_logger.error("doEmailTasks", e);
    	}
    	finally
    	{
    		if ( retriever != null )
    			retriever.close();
    		_logger.debug("doEmailTasks() numCorrelated: " + numCorrelated);
    	}
    }
    
    
    public void run()
    {
    	_logger.info("InboundEmailProcessor has started.");
        while ( shouldContinue )
        {
            try
            {
                synchronized(this)
                {
                    wait( waitTimeMsec );
                }
                if ( shouldContinue )
                	doEmailTasks();
            }
            catch( java.lang.InterruptedException e )
            {
                stop();
            }
            
        }
    	_logger.info("InboundEmailProcessor has stopped.");
    }
    
    
    public void stop()
    {
        shouldContinue = false;
    }

} 
