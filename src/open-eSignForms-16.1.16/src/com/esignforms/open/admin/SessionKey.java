// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2009 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.admin;

import com.esignforms.open.data.EsfUUID;

import javax.crypto.SecretKey;


/**
* SessionKey holds a secret session key.
* 
* @author Yozons, Inc.
*/
public class SessionKey
{
 private EsfUUID   keyId;
 private SecretKey key;
 
 public SessionKey(EsfUUID id, SecretKey secretKey)
 {
     keyId = id;
     key   = secretKey;
 }
 
 public EsfUUID getKeyId()
 {
     return keyId;
 }
 
 public SecretKey getKey()
 {
     return key;
 }
}