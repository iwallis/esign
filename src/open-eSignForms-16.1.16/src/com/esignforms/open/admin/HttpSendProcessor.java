// Copyright (C) 2013-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.admin;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.esignforms.open.Application;
import com.esignforms.open.Errors;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfInteger;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.db.ConnectionPool;
import com.esignforms.open.integration.httpsend.HttpSendRequest;
import com.esignforms.open.integration.httpsend.HttpSendResponse;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.util.DateUtil;
import com.esignforms.open.util.ServletUtil;

/** 
 * The HttpSendProcessor runs in its own thread, doing HTTP(S) GET/POSTs for outbound integration.
 * 
 * @author Yozons, Inc.
 */
public class HttpSendProcessor
    implements java.lang.Runnable
{
    private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(HttpSendProcessor.class);
    
    private boolean 	shouldContinue  = true;
    private boolean 	httpSendQueued = false;
    private long    	waitTimeMsec    = 0;
    
    private Application app         = null;

    public HttpSendProcessor( Application app )
    {
        this.app = app;
        setWaitMinutes( /*app.getBackgrounderWaitIntervalMins()*/ 1 );
    }
    
    
    /**
     * Sets the wait time in minutes between background runs.
     * @param numMinutes the int that specifies the number of minutes to wait between
     *        work episodes.
     */
    public synchronized void setWaitMinutes( int numMinutes )
    {
        if ( numMinutes < 1 )
            numMinutes = 1;
            
        waitTimeMsec = DateUtil.minutesToMillis(numMinutes);
    }
    
    protected int sendAllQueuedRequests()
    {
    	int numSent = 0;
    	int numErrors = 0;
    	
        ConnectionPool    pool = app.getConnectionPool();
        Connection        con  = pool.getConnection();
	        
        try
        {
        	List<HttpSendRequest> list = HttpSendRequest.Manager.getAllUncompleted(con,true); // get all for update
        	
        	for( HttpSendRequest request : list )
        	{
        		if ( ! shouldContinue )
        			break;
        		
	    		app.getSecureRandom().setSeed(request.getUrl()+request.getNumAttempts()); // we do HTTP SENDS at random times so we'll use this to help keep our PRNG random from back-guessing its seed

	    			// See if this is a retry or not
        		if ( request.getNumAttempts() > 0 && request.hasMoreAttempts() )
        		{
        			EsfDateTime nextAttemptTimestamp = request.getLastAttemptedTimestamp();
        			nextAttemptTimestamp.addMinutes(request.getRetryDelayMinutes());
        			
        			EsfDateTime now = new EsfDateTime();
        			if ( nextAttemptTimestamp.isAfter(now) ) // skip this for now since it's scheduled to try again after now
        			{
        				_logger.debug("sendAllQueuedRequests() - Skipping request id: " + request.getId() + "; hasn't reached it's retry delay of " +
        						request.getRetryDelayMinutes() + " minute(s) which will be ready after " + nextAttemptTimestamp.toLogTimeString());
        				continue;
        			}
        		}
        		
        		Errors errors = new Errors();
        		if ( ! request.isSendAllowed(errors) ) 
        		{ 
		        	Transaction tran = Transaction.Manager.getById(request.getTransactionId());
		        	if ( tran != null )
		        	{
		        		tran.logGeneral(con, "ERROR: Failed HttpSendRequest id " + request.getId() + " because it was not a valid request: " + errors.toString());
		        	}
            		// If the request cannot be sent, mark it as dead so it doesn't stay with us forever
	           		request.bumpNumAttempts();
        			request.setCompleted();
        			request.save(con);

        			EsfDateTime failedTimestamp = new EsfDateTime();
        			HttpSendResponse response = HttpSendResponse.Manager.createNew(request.getId(), failedTimestamp, failedTimestamp, HttpSendResponse.STATUS_FAILURE, -1, Application.CONTENT_TYPE_TEXT, "Invalid HttpSendRequest: " + errors.toString());
        			response.save(con);
        			
        			++numErrors;
        			continue;
        		}
        		
           		request.bumpNumAttempts();
           		
    			java.net.HttpURLConnection httpCon = null;
    			java.io.BufferedReader     br  = null;
    			java.io.InputStream        is = null;
    			java.io.OutputStream       os = null;

				String requestUrl = request.getUrl();
		        String requestParams = request.getParams();
		        
    			try
    			{
    		        if ( request.isRequestMethodGET() )
    		        {
    		        	requestUrl = ServletUtil.appendQueryStringToUrl(requestUrl, requestParams);
    		        }
    		        
    				java.net.URL httpUrl = null;
    		        try
    		        {
    		        	httpUrl = new java.net.URL( requestUrl );
    		        }
    		        catch( Exception e ) 
    		        {
    		        	Transaction tran = Transaction.Manager.getById(request.getTransactionId());
    		        	if ( tran != null )
    		        	{
    		        		tran.logGeneral(con, "ERROR: Failed HttpSendRequest id " + request.getId() + " because it had an invalid URL: " + requestUrl + "; for HTTP Send w/reason: " + request.getReason() + "; exception: " + e.getMessage());
    		        	}
                		// If the request cannot be sent because of an invalid URL, mark it as dead so it doesn't stay with us forever
            			request.setCompleted();
            			request.save(con);
            			
            			HttpSendResponse response = HttpSendResponse.Manager.createNew(request.getId(), null, null, HttpSendResponse.STATUS_FAILURE, -1, Application.CONTENT_TYPE_TEXT, "Invalid HttpSendRequest URL: " + requestUrl + "; exception: " + e.getMessage());
            			response.save(con);
            			
            			++numErrors;
            			continue;
    		        }
    		        
    				EsfDateTime requestTimestamp = new EsfDateTime();
    				
    				// Create the connection and set the connection properties
    				httpCon = (java.net.HttpURLConnection)httpUrl.openConnection();
    				httpCon.setRequestMethod(request.getRequestMethod());
    				httpCon.setAllowUserInteraction(false);  // this is program, so no popups
    				httpCon.setDoInput(true);      // we will read from the URL
    				httpCon.setDoOutput(true);     // we will write to the URL
    				httpCon.setIfModifiedSince(0); // always get
    				httpCon.setUseCaches(false);   // don't use cache
    				
    				byte[] postDataBytes = null;
    				int contentLength = 0;
    				if ( request.isRequestMethodPOST() )
    				{
        		        postDataBytes = EsfString.stringToBytes(requestParams);
        		        contentLength = postDataBytes.length;
        				httpCon.setRequestProperty("Content-Length", String.valueOf(contentLength));
        				httpCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
    				}
    				
    				httpCon.setRequestProperty("Accept", "*/*");
    				httpCon.setRequestProperty("HTTP_USER_AGENT", "Yozons-Open-eSignForms");
    				
    				httpCon.connect();
    		
    				// Construct the POST message
    				if ( request.isRequestMethodPOST() )
    				{
        				os = httpCon.getOutputStream();
        				os.write( postDataBytes );
        				os.close(); os = null;
    				}
    				
    				is = httpCon.getInputStream();
    				if ( is == null )
    				{
       		        	Transaction tran = Transaction.Manager.getById(request.getTransactionId());
    		        	if ( tran != null )
    		        	{
    		        		tran.logGeneral(con, "WARNING: HttpSendRequest id " + request.getId() + " failed because it could not get an input stream response from the URL: " + requestUrl + "; for HTTP Send w/reason: " + request.getReason());
    		        	}
    		        	if ( ! request.hasMoreAttempts() )
    		        		request.setCompleted();
            			request.save(con);
            			
            			HttpSendResponse response = HttpSendResponse.Manager.createNew(request.getId(), requestTimestamp, null, HttpSendResponse.STATUS_FAILURE, -1, Application.CONTENT_TYPE_TEXT, "No response received from URL: " + requestUrl);
            			response.save(con);
            			
            			++numErrors;
            			continue;
    				}
    		
    				contentLength = httpCon.getContentLength();
    				String contentType = httpCon.getContentType();
    				if ( EsfString.isBlank(contentType) )
    					contentType = Application.CONTENT_TYPE_TEXT;
    				int httpStatus = httpCon.getResponseCode();
    				
    				if ( contentLength < 1 )
    					contentLength = 2 * Literals.KB;
    		
    				StringBuilder responseBuf = new StringBuilder(contentLength+32);
    				
    				br = new java.io.BufferedReader( new java.io.InputStreamReader(is));
    				String responseString;
    				while ( (responseString = br.readLine()) != null )
    				{
    					if ( EsfString.isBlank(responseString) )
    						continue;
    					responseBuf.append(responseString).append('\n');
    				}
    				br.close(); br = null;
    		
    				responseString = responseBuf.toString();
    				
    				EsfDateTime responseTimestamp = new EsfDateTime();
    				
    				if ( request.hasLimitSuccessfulHttpStatusCodes() )
    				{
    					boolean ok = false;
    					EsfInteger[] limitToCodes = request.getLimitSuccessfulHttpStatusCodes();
    					for( int i=0; i < limitToCodes.length; ++i )
    					{
    						if ( limitToCodes[i].equals(httpStatus) )
    						{
    							ok = true;
    							break;
    						}
    					}
    					if ( ! ok )
    					{
    	   		        	Transaction tran = Transaction.Manager.getById(request.getTransactionId());
        		        	if ( tran != null )
        		        	{
        		        		tran.logGeneral(con, "WARNING: Failed HttpSendRequest id " + request.getId() + " because it's response HTTP status code: " + httpStatus + " is not considered a success to URL: " + requestUrl + "; for HTTP Send w/reason: " + request.getReason());
        		        	}
        		        	if ( ! request.hasMoreAttempts() )
        		        		request.setCompleted();
                			request.save(con);
                			
                			HttpSendResponse response = HttpSendResponse.Manager.createNew(request.getId(), requestTimestamp, responseTimestamp, HttpSendResponse.STATUS_FAILURE, httpStatus, Application.CONTENT_TYPE_TEXT, "Did not receive one of the specified valid HTTP status codes.\n\nActual response:\n" + responseString);
                			response.save(con);
                			
                			++numErrors;
                			continue;
    					}
    				}
    				
    				// We have a response, so let's check if it's successful or not
    				boolean responseMatches = false;
    				if ( ! request.isResponseMatchTypeAny() )
    				{
        				if ( request.isResponseMatchTypeMustContain() )
    					{
    						if ( responseString.contains(request.getResponseMatchValue()) )
    							responseMatches = true;
    					}
    					else if ( request.isResponseMatchTypeMustNotContain() )
    					{
    						if ( ! responseString.contains(request.getResponseMatchValue()) )
    							responseMatches = true;
    					}
    					else if ( request.isResponseMatchTypeMustStartWith() )
    					{
    						if ( responseString.startsWith(request.getResponseMatchValue()) )
    							responseMatches = true;
    					}
    					else if ( request.isResponseMatchTypeMustNotStartWith() )
    					{
    						if ( ! responseString.startsWith(request.getResponseMatchValue()) )
    							responseMatches = true;
    					}
    					else if ( request.isResponseMatchTypeMustMatchRegex() )
    					{
    						if ( findRegex(responseString, request.getResponseMatchValue()) )
    							responseMatches = true;
    					}
    					else if ( request.isResponseMatchTypeMustNotMatchRegex() )
    					{
    						if ( ! findRegex(responseString, request.getResponseMatchValue()) )
    							responseMatches = true;
    					}
    				}
    				else
    					responseMatches = true;
    				
    				if ( ! responseMatches )
    				{
	   		        	Transaction tran = Transaction.Manager.getById(request.getTransactionId());
    		        	if ( tran != null )
    		        	{
    		        		tran.logGeneral(con, "WARNING: Failed HttpSendRequest id " + request.getId() + " because it's response did not match as required from URL: " + requestUrl + "; for HTTP Send w/reason: " + request.getReason());
    		        	}
    		        	if ( ! request.hasMoreAttempts() )
    		        		request.setCompleted();
            			request.save(con);
            			
            			HttpSendResponse response = HttpSendResponse.Manager.createNew(request.getId(), requestTimestamp, responseTimestamp, HttpSendResponse.STATUS_FAILURE, httpStatus, Application.CONTENT_TYPE_TEXT, 
            								"Response text did not match required value: " + request.getResponseMatchValue() + 
            								"; match type: " + request.getResponseMatchType() +
            								".\n\nActual response:\n" + responseString);
            			response.save(con);
            			
            			++numErrors;
            			continue;
    				}
    				
    				// Yes, we consider this to be a successful completion.
   		        	Transaction tran = Transaction.Manager.getById(request.getTransactionId());
		        	if ( tran != null )
		        	{
		        		tran.logGeneral(con, "HttpSendRequest id " + request.getId() + " completed. Response HTTP status: " + httpStatus);
		        	}
    				
        			HttpSendResponse response = HttpSendResponse.Manager.createNew(request.getId(), requestTimestamp, responseTimestamp, HttpSendResponse.STATUS_SUCCESS, httpStatus, contentType, responseString);
        			response.save(con);
    		        request.setCompleted();
    		        request.save(con);
    				++numSent;
    			}
    			catch( Exception e )
    			{
    				int httpStatus = -1;
    				try { httpStatus = httpCon.getResponseCode(); } catch(Exception ignore) {}
    				
    				_logger.debug("sendAllQueuedRequests() exception w/http status: " + httpStatus + "; on requestURL: " + requestUrl,e);
   		        	Transaction tran = Transaction.Manager.getById(request.getTransactionId());
		        	if ( tran != null )
		        	{
		        		tran.logGeneral(con, "HttpSendRequest id " + request.getId() + " failed with " + e.getClass().getName() + ": " + e.getMessage() + "; for HTTP Send w/reason: " + request.getReason());
		        	}
		        	if ( ! request.hasMoreAttempts() )
		        		request.setCompleted();
        			request.save(con);
        			
        			EsfDateTime failedTimestamp = new EsfDateTime();
        			HttpSendResponse response = HttpSendResponse.Manager.createNew(request.getId(), failedTimestamp, failedTimestamp, HttpSendResponse.STATUS_FAILURE, httpStatus, Application.CONTENT_TYPE_TEXT, e.getClass().getName() + ": " + e.getMessage() + "; requestURL: " + requestUrl + "; for HTTP Send w/reason: " + request.getReason());
        			response.save(con);
        			
        			++numErrors;
        			continue;
    			}
    			finally
    			{
    				try { if ( os != null ) os.close(); } catch(Exception e){} finally { os = null; }
    				try { if ( is != null ) is.close(); } catch(Exception e){} finally { is = null; }
    				try { if ( br != null ) br.close(); } catch(Exception e){} finally { br = null; }
    				try { if ( httpCon != null ) httpCon.disconnect(); } catch(Exception e){} finally { httpCon = null; }
    			}
        	} /* end for loop */
        	
            con.commit();
            
            return numSent;
        }
        catch(SQLException e) 
        {
        	_logger.sqlerr(e,"sendAllQueuedRequests()");
            pool.rollbackIgnoreException(con,e);
            return numSent;
        }
        finally
        {
            app.cleanupPool(pool,con,null);
            _logger.debug("sendAllQueuedRequests() numSent: " + numSent + "; numErrors: " + numErrors);
        }
    }
    
    boolean findRegex(String text, String regex)
    {
    	java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(regex);
    	java.util.regex.Matcher m = pattern.matcher(text);
    	return m != null && m.find();
    }
    
	StringBuilder appendNVP(StringBuilder buf, String name, String value)
	{
		if ( buf.length() > 0 )
			buf.append("&");
		buf.append(ServletUtil.urlEncode(name)).append("=").append(ServletUtil.urlEncode(value==null?"":value)); 
		return buf;
	}
    
    /**
     * This method is called periodically to handle whatever tasks it can do.
     */
    protected void doTasks()
    {
    	// If we wake up for 'httpSendQueued', we'll sleep 1 second to help up the odds we find the newly queued HTTP send committed to the DB before we check
    	if ( httpSendQueued )
    	{
    		httpSendQueued = false;
    		app.sleep(1500); // 1.5 seconds
    	}
    	sendAllQueuedRequests();
    }
    
    public void run()
    {
    	_logger.info("HttpSendProcessor has started.");
        while ( shouldContinue )
        {
            try
            {
                synchronized(this)
                {
                    wait( waitTimeMsec );
                }
                if ( shouldContinue )
                	doTasks();
            }
            catch( java.lang.InterruptedException e )
            {
                stop();
            }
            catch( Throwable e )
            {
            	_logger.error("HttpSendProcessor.run() unexpected exception",e);
            }
            
        }
    	_logger.info("HttpSendProcessor has stopped.");
    }
    
    
    public void stop()
    {
        shouldContinue = false;
    }

    public void setHttpSendQueued()
    {
    	httpSendQueued = true;
    }
} 
