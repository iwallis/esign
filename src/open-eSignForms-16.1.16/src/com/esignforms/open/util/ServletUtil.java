// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfString;

/**
 * Contains various servlet utility routines
 * @author Yozons, Inc.
 */
public final class ServletUtil
{
    private ServletUtil()
    {
    }
    
    /**
     * Returns the full URI and query string (params) that make up the URL.  Note that the
     * the scheme, hostname and optional port (i.e. https://esignforms.com[:443]) are not
     * returned since a given page typically posts BACK to where it came from.  Therefore it
     * might be something like:
     *     /demo/mypage.jsp
     *     /demo/servlet/Login?from=abc&eval=true
     * @param req the HttpServletRequest
     * @return the String URI and query string, if present.
     */
    public final static String getLocalRequestUrl(HttpServletRequest req)
    {
        String q = req.getQueryString();
        if ( q == null )
            return req.getRequestURI();
        return req.getRequestURI() + "?" + q;
    }

	/**
	 * Returns your URL fully qualified
	 * @param req the HttpServletRequest
	 * @return the String URL that represents the current request
	 */
	public final static String getExternalRequestUrl(HttpServletRequest req)
	{
		StringBuilder url = new StringBuilder(256);
		String scheme = req.getScheme();
		url.append(scheme).append("://").append(req.getServerName());
		if ( scheme.equals("http") && req.getServerPort() != 80 )
			url.append(':').append(req.getServerPort());
		else if ( scheme.equals("https") && req.getServerPort() != 443 )
			url.append(':').append(req.getServerPort());
		url.append(getLocalRequestUrl(req));
		return url.toString();
	}

	/**
	 * This is very similar to the HttpServletRequest.getContextPath() call except that this
	 * produces something suitable for providing to outside parties who need the full URL, including
	 * the scheme, hostname, port, and the context path.  The port is only added if it's not the default for the
	 * scheme used:
	 *     http://open.esignforms.com/demo
	 *     https://open.esignforms.com/demo
	 *     https://open.esignforms.com:8443/demo
	 * @param req the HttpServletRequest
	 * @return the String URL that represents the context path for all requests.
	 */
	public final static String getExternalUrlContextPath(HttpServletRequest req)
	{
		StringBuilder url = new StringBuilder(256);
		String scheme = req.getScheme();
		url.append(scheme).append("://").append(req.getServerName());
		if ( scheme.equals("http") && req.getServerPort() != 80 )
			url.append(':').append(req.getServerPort());
		else if ( scheme.equals("https") && req.getServerPort() != 443 )
			url.append(':').append(req.getServerPort());
		url.append(req.getContextPath());
		return url.toString();
	}

	public final static String getExternalUrlWithoutContextPath(HttpServletRequest req)
	{
		StringBuilder url = new StringBuilder(256);
		String scheme = req.getScheme();
		url.append(scheme).append("://").append(req.getServerName());
		if ( scheme.equals("http") && req.getServerPort() != 80 )
			url.append(':').append(req.getServerPort());
		else if ( scheme.equals("https") && req.getServerPort() != 443 )
			url.append(':').append(req.getServerPort());
		return url.toString();
	}

    public final static String getParam(HttpServletRequest req, String name)
        throws ServletException
    {
        return getParam(req,name,true);
    }
    public final static String getScriptFreeParam(HttpServletRequest req, String name)
        throws ServletException
    {
        return getScriptFreeParam(req,name,true);
    }    
    
    public final static int getIntParam(HttpServletRequest req, String name)
        throws ServletException
    {
        String val = getParam(req,name);
        try
        {
            return Integer.parseInt(val);
        }
        catch( Exception e )
        {
            throw new ServletException("getIntParam exception for param: " + name + ": " + e.getMessage());
        }
    }
    
    
    
    public final static String getParam(HttpServletRequest req, String name, boolean isRequired)
        throws ServletException
    {
        String [] pv = req.getParameterValues(name);
        if ( pv == null )
        {
            if ( isRequired )
                throw new ServletException("Missing required parameter: " + name);
            return null;
        }
        return cleanInput(pv[0].trim());
    }
    public final static String getScriptFreeParam(HttpServletRequest req, String name, boolean isRequired)
        throws ServletException
    {
        String [] pv = req.getParameterValues(name);
        if ( pv == null )
        {
            if ( isRequired )
                throw new ServletException("Missing required parameter: " + name);
            return null;
        }
        return cleanScriptFreeInput(pv[0].trim());
    }    
    
    
    public final static String getParam(HttpServletRequest req, String name, String def)
    {
        String [] pv = req.getParameterValues(name);
        if ( pv == null )
            return def;
        return cleanInput(pv[0].trim());
    }
    
	public final static String getSessionStringParam(HttpSession session, String name, String def)
	{
		try
		{
			Object ov = session.getAttribute(name);
			if ( ov != null && ov instanceof String )
				return (String)ov;
			return def;
		}
		catch( Exception e )
		{
			return def;
		}
	}
    
    /**
     * Gets the parameter by name.  If not found, then the default is returned.
     * This behaves the same as getParam() if the
     * ServletUtil stripScriptTags option is set.
     * @see #getParam(HttpServletRequest,String,String)
     */
    public final static String getScriptFreeParam(HttpServletRequest req, String name, String def)
    {
        String [] pv = req.getParameterValues(name);
        if ( pv == null )
            return def;
        return cleanScriptFreeInput(pv[0].trim());
    }
    
    public final static long getLongParam(HttpServletRequest req, String name, long def)
    {
        try
        {
            String val = getParam(req,name);
            return Long.parseLong(val);
        }
        catch( Exception e )
        {
            return def;
        }
    }
    
    public final static int getIntParam(HttpServletRequest req, String name, int def)
    {
        try
        {
            String val = getParam(req,name);
            return Integer.parseInt(val);
        }
        catch( Exception e )
        {
            return def;
        }
    }
    
    
    public final static short getShortParam(HttpServletRequest req, String name, short def)
    {
        try
        {
            String val = getParam(req,name);
            return Short.parseShort(val);
        }
        catch( Exception e )
        {
            return def;
        }
    }
    
    
    public final static long[] getMultiLongParam(HttpServletRequest req, String name)
    {
        String [] pv     = getMultiParam(req, name);
        long[]    pvLong = null;
        if ( pv != null )
        {
            pvLong = new long[pv.length];
            for ( int i=0; i < pvLong.length; ++i )
                pvLong[i] = Application.getInstance().stringToLong(pv[i],0);
        }        
        return pvLong;
    }
    public final static String[] getMultiParam(HttpServletRequest req, String name)
    {
        String [] pv = req.getParameterValues(name);
        if ( pv != null )
            for ( int i=0; i < pv.length; ++i )
                pv[i] = cleanInput(pv[i].trim());
        return pv;
    }
    
    /**
     * This utility appends a param and value to a URL.
     */
    public final static String appendUrlParam(String url, String param, String value)
    {
        if ( url.indexOf("?") < 0 )
           return url + "?" + urlEncode(param) + "=" + urlEncode(value);
        return url + "&" + urlEncode(param) + "=" + urlEncode(value);
    }
    
    /**
     * This utility appends a param and an INT value to a URL.
     */
    public final static String appendUrlParam(String url, String param, int value)
    {
        return appendUrlParam(url,param,Integer.toString(value));
    }
    
    /**
     * This utility appends all of the params in a query string to a URL.
     */
    public final static String appendQueryStringToUrl(String url, String queryString)
    {
    	if ( EsfString.isBlank(queryString) )
    		return url;
    	
        if ( url.indexOf("?") < 0 )
            return url + "?" + queryString;
         return url + "&" + queryString;
    }
    
    /**
     * Determines if a given param is on an URL or not.
     * @param url the String URL to check for a given param
     * @param paramName the String name of the param to check on the 'url'
     * @return true if the specified param is defined on the specified URL
     */
    public static boolean isParamInUrl(String url, String paramName)
    {
    	paramName = urlEncode(paramName);
    	return url.contains('?' + paramName + '=')  ||  url.contains('&' + paramName + '='); 
    }
    
    
    public static String urlEncode(String s)
    {
    	if ( s == null )
    		return "";
    	
        try
        {
            return java.net.URLEncoder.encode(s,EsfString.CHARSET_UTF_8);
        }
        catch( java.io.UnsupportedEncodingException e )
        {
            // Java always supports UTF-8
            Application.getInstance().except(e,"ServletUtil.urlEncode()");
            return s;
        }
    }
    
    public static String urlDecode(String s)
    {
    	if ( s == null )
    		return "";
    	
        try
        {
            return java.net.URLDecoder.decode(s,EsfString.CHARSET_UTF_8);
        }
        catch( java.io.UnsupportedEncodingException e )
        {
            // Java always supports UTF-8
            Application.getInstance().except(e,"ServletUtil.urlDecode()");
            return s;
        }
    }
    
    public static String getMetaRefreshTag(HttpSession session, String url)
    {
        // If the user isn't logged in, then we don't create any meta tag
        if ( session == null || EsfString.isBlank(url) ) 
            return "";
            
        return "<meta http-equiv=\"Refresh\" content=\"" + (session.getMaxInactiveInterval()-20) + ";URL=" + HtmlUtil.toEscapedHtml(url) + "\" />";
    }

    // Version for IE that cannot handled escaped entities inside the meta tag URL value
    public static String getMetaRefreshTagNoToHtml(HttpSession session, String url)
    {
        // If the user isn't logged in, then we don't create any meta tag
        if ( session == null || EsfString.isBlank(url) ) 
            return "";
            
        return "<meta http-equiv=\"Refresh\" content=\"" + (session.getMaxInactiveInterval()-20) + ";URL=" + url + "\" />";
    }

    
    public final static boolean getCheckbox(HttpServletRequest req, String name)
    {
        String [] pv = req.getParameterValues(name);
        return pv != null;
    }
    
	public static String makeOption(String value, String option)
	{
        if ( value == null )
            value = "";
        if ( option == null )
            option = "";
	    return "<option" + (( value.equals(option) ) ? " selected>" : ">") + HtmlUtil.toEscapedHtml(option) + "</option>";
	}

	public static String makeOptionOptionValue(String selectedItem, String option, String value)
	{
        if ( selectedItem == null )
            selectedItem = "";
        if ( value == null )
            value = "";
        if ( option == null )
            option = "";
	    return "<option value=\""+ HtmlUtil.toEscapedHtml(value) +"\"" + (( value.equals(selectedItem) ) ? " selected>" : ">") + HtmlUtil.toEscapedHtml(option) + "</option>";
	}
	
	public static String makeOption(String selectedItems[], String option)
	{
		StringBuilder strBuff = new StringBuilder(100);
		strBuff.append("<option");
		if ( selectedItems != null && selectedItems.length > 0 )
		{
			for ( String selectedItem : selectedItems )
			{
				if ( selectedItem.equals(option) )
				{
					strBuff.append(" selected=\"selected\"");
					break;
				}
			}
		}
		strBuff.append(">").append(HtmlUtil.toEscapedHtml(option)).append("</option>");
		return strBuff.toString();
	}

	public static String makeOptionOptionValue(String selectedItems[], String option, String value)
	{
		StringBuilder strBuff = new StringBuilder(100);
		strBuff.append("<option value=\"").append(HtmlUtil.toEscapedHtml(value)).append("\"");
		if ( selectedItems != null && selectedItems.length > 0 )
		{
			for ( String selectedItem : selectedItems )
			{
				if ( selectedItem.equals(value) )
				{
					strBuff.append(" selected=\"selected\"");
					break;
				}
			}
		}
		strBuff.append(">").append(HtmlUtil.toEscapedHtml(option)).append("</option>");
		return strBuff.toString();
	}
	
	public static String makeIntOption(int value, int option)
	{
	    return "<option" + (( value == option ) ? " selected=\"selected\">" : ">") + option + "</option>";
	}
	
	public static String makeChecked(boolean b)
	{
	    return (b) ? "checked=\"checked\"" : "";
	}
	
    /**
     * Takes a "long" string and inserts <br /> tag breaks after every chunkSize characters.
     * It generally ends with a final <br /> tag.
     */
    public static String breakLongString(String longString, int chunkSize)
    {
        int bufLength = longString.length();
        bufLength += ((bufLength * 6)/chunkSize) + 1; // add some space for the <br /> we'll add
        
        StringBuilder buf = new StringBuilder(bufLength);
        int begin = 0; 
        int end   = Math.min(longString.length(),chunkSize); 
        while( begin < longString.length() ) 
        {
            buf.append( longString.substring(begin,end) ).append("<br />");
            begin = end; 
            end  += chunkSize; 
            if ( end > longString.length() ) 
                end = longString.length(); 
        }
        return buf.toString();
    }
    
    
	// The following routines are used with the SelectDate tag <esf:selectDate/>
	// The date strings are in MM/DD/YYYY format.
	/**
	 * Used with the SelectDate tag <esf:selectDate/> in which the date string is in MM/DD/YYYY format.
	 * @param req the HttpServletRequest in which to extract the parameters of the specified name
	 * @param name the String name of the HTML field used with the selectDate tag.
	 * @return the string date in mm/dd/yyyy format (with "mm" returned if no month, "dd" if no day, and "yyyy" if no year params found.
	 */
	public static String getSelectDateString(HttpServletRequest req, String name)
	{
		String mm   = getParam(req,name+"MM","mm");
		String dd   = getParam(req,name+"DD","dd");
		String yyyy = getParam(req,name+"YYYY","yyyy");
		return mm + "/" + dd + "/" + yyyy;
	}
	public static String getSelectDateString(HttpServletRequest req, String name, String def)
	{
		String mm   = getParam(req,name+"MM",null);
		String dd   = getParam(req,name+"DD",null);
		String yyyy = getParam(req,name+"YYYY",null);
		if ( mm == null && dd == null && yyyy == null )
			return def;
		return getSelectDateString(req,name);
	}
	
	public static EsfDate getSelectDate(HttpServletRequest req, String name)
	{
		return EsfDate.CreateFromMDY(getSelectDateString(req,name));
	}

	public static String[] getMultiSelectDateString(HttpServletRequest req, String name)
	{
        String[] mm   = getMultiParam(req,name+"MM");
        String[] dd   = getMultiParam(req,name+"DD");
        String[] yyyy = getMultiParam(req,name+"YYYY");
        if ( mm == null || dd == null || yyyy == null || mm.length != yyyy.length || mm.length != dd.length )
        	return new String[0];
    	for( int i=0; i < mm.length; ++i )
    	{
    		if ( mm[i] == null )
    			mm[i] = "mm";
    	}
    	for( int i=0; i < dd.length; ++i )
    	{
    		if ( dd[i] == null )
    			dd[i] = "dd";
    	}
    	for( int i=0; i < yyyy.length; ++i )
    	{
    		if ( yyyy[i] == null )
    			yyyy[i] = "yyyy";
    	}
    	String[] mmddyyyy = new String[mm.length];
    	for( int i=0; i < mm.length; ++i )
    		mmddyyyy[i] = mm[i] + "/" + dd[i] + "/" + yyyy[i];
        return mmddyyyy;

	}


    /**
     * Used with the SelectMMYYYYDate tag <esf:selectMMYYYYDate/> in which the date string is in MM/YYYY format.
     * @param req the HttpServletRequest in which to extract the parameters of the specified name
     * @param name the String name of the HTML field used with the selectDate tag.
     * @return the string date in mm/yyyy format (with "mm" returned if no month, and "yyyy" if no year params found.
     */
    public static String getSelectDateMMYYYYString(HttpServletRequest req, String name)
    {
        String mm   = getParam(req,name+"MM","mm");
        String yyyy = getParam(req,name+"YYYY","yyyy");
        return mm + "/" + yyyy;
    }

    public static String getSelectDateMMYYYYString(HttpServletRequest req, String name, String def)
    {
        String mm   = getParam(req,name+"MM",null);
        String yyyy = getParam(req,name+"YYYY",null);
        if ( mm == null && yyyy == null )
        	return def;
        return getSelectDateMMYYYYString(req,name);
    }
    
    public static String[] getMultiSelectDateMMYYYYString(HttpServletRequest req, String name)
    {
        String[] mm   = getMultiParam(req,name+"MM");
        String[] yyyy = getMultiParam(req,name+"YYYY");
        if ( mm == null || yyyy == null || mm.length != yyyy.length )
        	return new String[0];
    	for( int i=0; i < mm.length; ++i )
    	{
    		if ( mm[i] == null )
    			mm[i] = "mm";
    	}
    	for( int i=0; i < yyyy.length; ++i )
    	{
    		if ( yyyy[i] == null )
    			yyyy[i] = "yyyy";
    	}
    	String[] mmyyyy = new String[mm.length];
    	for( int i=0; i < mm.length; ++i )
    		mmyyyy[i] = mm[i] + "/" + yyyy[i];
        return mmyyyy;
    }
    
    /**
     * This method takes a string and strips out all script elements: "<script" or "javascript: or vbscript:"
     *
     * @param s the String containing the input to be scrubbed.
     *
     * @return the String that has been scrubbed.  It returns the string unmodified
     * when no scripts are detected.
     */
    public static String stripScriptTags( String s )
    {
		String lower        = s.toLowerCase();
		int scriptIndex     = lower.indexOf( "<script" );
		int javascriptIndex = lower.indexOf( "javascript:" );
		int vbscriptIndex   = lower.indexOf( "vbscript:" );
		int objectIndex     = lower.indexOf( "<object" );
		int appletIndex     = lower.indexOf( "<applet" );
		int embedIndex      = lower.indexOf( "<embed" );

		// If none, we're okay!
		if ( scriptIndex < 0 && javascriptIndex < 0 && vbscriptIndex < 0 && objectIndex < 0 && appletIndex < 0 && embedIndex < 0 )
			return s;

		// We have some script elements that needs to be stripped out.
		StringBuilder buf = new StringBuilder( s.length() );
		int currPos = 0;

		if ( scriptIndex >= 0 )
		{
			while ( scriptIndex >= 0 )
			{
				buf.append( s.substring( currPos, scriptIndex ) );

				int endScriptIndex = lower.indexOf( "</script", scriptIndex );

				// If we don't find an end, then we'll assume script all the way to the end of the string
				// and therefore there can be no further scripts.
				if ( endScriptIndex < 0 )
				{
					currPos = s.length();
					scriptIndex = -1;
				}
				else
				{
					currPos = endScriptIndex + 9; // skip over the end script tag
					scriptIndex = lower.indexOf( "<script", currPos );
				}
			}

			if ( currPos < s.length() )
			{
				buf.append( s.substring( currPos ) );
			}
		}

		if ( javascriptIndex > 0 )
		{
		   s = buf.toString();
		   lower = s.toLowerCase();
		   buf.setLength( 0 );
		   currPos = 0;
		   javascriptIndex = lower.indexOf( "javascript:" );

		   while ( javascriptIndex >= 0 )
		   {
			   buf.append( s.substring( currPos, javascriptIndex ) );
			   currPos = javascriptIndex + 11;
			   javascriptIndex = lower.indexOf( "javascript:", currPos );
		   }

		   if ( currPos < s.length() )
		   {
			   buf.append( s.substring( currPos ) );
		   }
		}


		if ( vbscriptIndex >= 0 )
		{
		   s = buf.toString();
		   lower = s.toLowerCase();
		   buf.setLength( 0 );
		   currPos = 0;
		   vbscriptIndex = lower.indexOf( "vbscript:" );
	
		   while ( vbscriptIndex >= 0 )
		   {
			   buf.append( s.substring( currPos, vbscriptIndex ) );
			   currPos = vbscriptIndex + 9;
			   vbscriptIndex = lower.indexOf( "vbscript:", currPos );
		   }
	
		   if ( currPos < s.length() )
		   {
			   buf.append( s.substring( currPos ) );
		   }
		}


		if ( objectIndex >= 0 )
		{
		   s = buf.toString();
		   lower = s.toLowerCase();
		   buf.setLength( 0 );
		   currPos = 0;
		   objectIndex = lower.indexOf( "<object" );
     	
			while ( objectIndex >= 0 )
			{
				buf.append( s.substring( currPos, objectIndex ) );

				int endObjectIndex = lower.indexOf( "</object", objectIndex );

				// If we don't find an end, then we'll assume object all the way to the end of the string
				// and therefore there can be no further objects.
				if ( endObjectIndex < 0 )
				{
					currPos = s.length();
					objectIndex = -1;
				}
				else
				{
					currPos = endObjectIndex + 9; // skip over the end object tag
					objectIndex = lower.indexOf( "<object", currPos );
				}
			}

			if ( currPos < s.length() )
			{
				buf.append( s.substring( currPos ) );
			}
		}

		if ( appletIndex >= 0 )
		{
		   s = buf.toString();
		   lower = s.toLowerCase();
		   buf.setLength( 0 );
		   currPos = 0;
		   appletIndex = lower.indexOf( "<applet" );
     	
			while ( appletIndex >= 0 )
			{
				buf.append( s.substring( currPos, appletIndex ) );

				int endAppletIndex = lower.indexOf( "</applet", appletIndex );

				// If we don't find an end, then we'll assume applet all the way to the end of the string
				// and therefore there can be no further applets.
				if ( endAppletIndex < 0 )
				{
					currPos = s.length();
					appletIndex = -1;
				}
				else
				{
					currPos = endAppletIndex + 9; // skip over the end applet tag
					appletIndex = lower.indexOf( "<applet", currPos );
				}
			}

			if ( currPos < s.length() )
			{
				buf.append( s.substring( currPos ) );
			}
		}

		if ( embedIndex >= 0 )
		{
		   s = buf.toString();
		   lower = s.toLowerCase();
		   buf.setLength( 0 );
		   currPos = 0;
		   embedIndex = lower.indexOf( "<embed" );
		
		   while ( embedIndex >= 0 )
			{
				buf.append( s.substring( currPos, embedIndex ) );

				int endEmbedIndex = lower.indexOf( "</embed", appletIndex );

				// If we don't find an end, then we'll assume embed all the way to the end of the string
				// and therefore there can be no further embeds.
				if ( endEmbedIndex < 0 )
				{
					currPos = s.length();
					embedIndex = -1;
				}
				else
				{
					currPos = endEmbedIndex + 8; // skip over the end embed tag
					embedIndex = lower.indexOf( "<embed", currPos );
				}
			}

			if ( currPos < s.length() )
			{
				buf.append( s.substring( currPos ) );
			}
		}

		return buf.toString();
    }
    
    
    public static String stripControlChars( String s )
    {
    	if ( s == null )
    		return s;

		int length = s.length();
		StringBuilder buf = null;

		for ( int i = 0; i < length; ++i )
    	{
    		char c = s.charAt(i);

    		// If we find a control character that's not a newline, we'll create a new buf with everything found so far but the
    		// control character, and then we'll append all other non-control characters into that buffer.
    		if ( Character.isISOControl(c) && c != '\n' && c != '\r')
    		{
    			if ( buf == null )
    			{
					buf = new StringBuilder(length);
					buf.append(s.substring( 0, i )); 
    			}
    		}
			else if ( c == 0x00A0 ) // Convert &nbsp; character into regular space on input
			{
				if ( buf == null )
				{
					buf = new StringBuilder(length);
					buf.append(s.substring( 0, i )); 
				}
				buf.append(' ');
			}
    		else 
    		{
				if ( buf != null )
					buf.append(c);
    		}
    	}
    	
    	// If we never created a buf, we had no control characters, so we just return the original string
		return (buf == null) ? s : buf.toString();
    }
    

    /**
     * This method takes the string cleans up anything we don't want to be allowed as data input.
     */
    public static String cleanInput( String s )
    {
        if ( s == null )
            return "";

		s = stripControlChars(s);

		//if ( Application.getInstance().getStripScriptTags() )
        //	s = stripScriptTags( s );

        return s;
    }

    /**
     * This method takes the string cleans up anything we don't want to be allowed as data input,
     * and this forces script removal.  This behaves the same as cleanInput() if the stripScriptTags
     * ServletUtil property is set.
     * @see #cleanInput(String)
     */
    public static String cleanScriptFreeInput( String s )
    {
        if ( s == null )
        {
            return "";
        }

		s = stripControlChars(s);

       	s = stripScriptTags( s );

        return s;
    }


}