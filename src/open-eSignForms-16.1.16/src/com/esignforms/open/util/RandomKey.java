// Copyright (C) 2009-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.util;

import com.esignforms.open.Application;
import com.esignforms.open.data.EsfString;

/**
 * This class creates random keys for that are of various lengths using SecureRandom.
 * 
 * @author Yozons, Inc.
 */
public class RandomKey
{
    protected static final char[] ALPHANUMS = { 
        'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
        '0','1','2','3','4','5','6','7','8','9'
                                              };
    
    
    private java.security.SecureRandom keyGen = null;
    
    public static final int PICKUP_CODE_LENGTH    = 20;  // used by a party to gain access to a transaction and its documents
    public static final int EMAIL_BOUNCE_CORRELATION_CODE_LENGTH = 20; // used by the email system's return path to correlate bounces to the message sent
    public static final int ESF_REPORTS_ACCESS_PICKUP_CODE_LENGTH = 30;  // used to access a transaction via the ESF_reports_access party
    public static final int LOGIN_AUTH_KEY_LENGTH = 50;  // for login authorization code we make impossible to guess
    
    
    public RandomKey()
    {
        keyGen = Application.getInstance().getSecureRandom().getPRNG();
    }
    
    public int getIntBetween(int fromLow, int throughHigh)
    {
        if ( fromLow >= throughHigh )  // Not random, but hey, you screwed up with a dopey call.
            return fromLow;
            
        // nextInt is from zero (inclusive) through the number specified (exclusive).
        // In this case, the difference plus 1 is the number of possible numbers in the range since
        // ours in inclusive on both ends.
        return fromLow + keyGen.nextInt(throughHigh - fromLow + 1);
    }
    
    public byte[] getBytes(int numBytes)
    {
        byte[] b = new byte[numBytes];
        keyGen.nextBytes(b);
        return b;
    }
    
	// Added this as a hack to block random strings that are considered offensive. We received our
	// first report of such a word on 19 May 2014.
	private static String[] UNDESIRABLE_RANDOM_LOWERCASE_STRINGS = new String[0];
	
	/**
	 * Sets the undesirable strings that shouldn't be in any randomly generated strings.
	 * @param segments a String of semicolon-separated short character sequences to check if they exist 
	 * in any randomly generated strings (i.e.: "bad;word" would not allow random strings to contain
	 * "bad" or "word" anywhere in them regardless of case (i.e would also reject "Bad" or "BAD" etc.).
	 */
	public static void SetRandomKeyUndesirableStringSegments(String segments)
	{
		if ( segments == null )
			UNDESIRABLE_RANDOM_LOWERCASE_STRINGS = new String[0];
		else
		{
			UNDESIRABLE_RANDOM_LOWERCASE_STRINGS = segments.split(";");
			for( int i=0; i < UNDESIRABLE_RANDOM_LOWERCASE_STRINGS.length; ++i )
				UNDESIRABLE_RANDOM_LOWERCASE_STRINGS[i] = UNDESIRABLE_RANDOM_LOWERCASE_STRINGS[i].toLowerCase();
		}
	}
	
	/**
	 * Checks if a string contains any of our undesirable strings.
	 * @param stringToCheck the String to check it contains any of our undesirable words.
	 * @return true if it contains an undesirable word; false if it's "clean".
	 */
    public static boolean ContainsUndesirable(String stringToCheck)
    {
    	String lowerString = stringToCheck.toLowerCase();
    	for( String undesireable : UNDESIRABLE_RANDOM_LOWERCASE_STRINGS )
    	{
    		if ( lowerString.contains(undesireable) )
    			return true;
    	}
    	return false;
    }
    

    public String getString(int length)
    {
		String s;
		do 
		{
			try
			{
				s = new String(getBytes(length),EsfString.CHARSET_UTF_8);
			}
			catch( java.io.UnsupportedEncodingException e )
			{
				s = "";
			}
		} 
		while ( s.length() != length || ContainsUndesirable(s) );
		return s;
    }
    
    public String getAlphaNumericString(int length)
    {
		String s;
		do 
		{
	        StringBuilder sb = new StringBuilder(length);
	        
	        for(int i=0; i<length; ++i)
	            sb.append( ALPHANUMS[keyGen.nextInt(ALPHANUMS.length)] );
	        
	        s = sb.toString();
		} 
		while ( ContainsUndesirable(s) );
		return s;
    }
    
    public String getEmailBounceCorrelationCodeString()
    {
        return getAlphaNumericString(EMAIL_BOUNCE_CORRELATION_CODE_LENGTH).toLowerCase(); // since used as email addresses, we use lowercase only
    }
    public boolean isCorrectEmailBounceCorrelationCodeLength(String bounceCorrelationCode)
    {
        return bounceCorrelationCode != null && bounceCorrelationCode.length() == EMAIL_BOUNCE_CORRELATION_CODE_LENGTH;
    }
    
    public String getPickupCodeString()
    {
        return getAlphaNumericString(PICKUP_CODE_LENGTH);
    }
    public boolean isCorrectPickupCodeLength(String pickupCode)
    {
        return pickupCode != null && pickupCode.length() == PICKUP_CODE_LENGTH;
    }
    
    public String getEsfReportsAccessPickupCodeString()
    {
        return getAlphaNumericString(ESF_REPORTS_ACCESS_PICKUP_CODE_LENGTH);
    }
    public boolean isCorrectEsfReportsAccessPickupCodeLength(String pickupCode)
    {
        return pickupCode != null && pickupCode.length() == ESF_REPORTS_ACCESS_PICKUP_CODE_LENGTH;
    }
    
    public String getLoginAuthorizationString()
    {
        return getAlphaNumericString(LOGIN_AUTH_KEY_LENGTH);
    }
    public boolean isCorrectLoginAuthorizationLength(String authorization)
    {
        return authorization != null && authorization.length() == LOGIN_AUTH_KEY_LENGTH;
    }
}