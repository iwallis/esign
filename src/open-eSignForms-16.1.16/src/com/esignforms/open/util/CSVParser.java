// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2009 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;


/**
 * This parser can handle the CSV (comma separated value) format, often used by users of Microsoft Excel.
 * The basic format of a line in the CSV is:
 *    field1,field2,field3,field4,....
 * If the line to be processed ends with a comma, it is assume that there is one additional empty field at the end.
 *    
 * Note that each field is itself a string of characters.  But it can contain the following kinds of "escapes"
 * to support a field that contains quotes, CR, LF and/or commas.
 * 
 * If a field contains a comma or CR/LF, the field is quoted in the CSV like:
 *    Field: Wall, Jr.
 *    Quoted in the CVS, the field is: "Wall, Jr."
 *    
 * If a field contains double quotation marks, the quotes are "quoted" in the CSV like:
 *    Field: "Iron" Mike Tyson
 *    Quoted in the CVS, the field is: """Iron"" Mike Tyson"
 *    
 * This can also create combinations of quoting like:
 *    Field: Tyson, "Iron" Mike
 *    Quoted in the CVS, the field is: "Tyson, ""Iron"" Mike"
 *    
 */
public class CSVParser
{
    public    static final int  DEFAULT_FIELD_LENGTH = 256;
    protected static final char FIELD_SEPARATOR = ',';
    protected static final char QUOTE = '"';
    protected static final char CR = '\r';
    protected static final char LF = '\n';
    
    protected LinkedList<String> list;
    protected StringBuilder      fieldBuf;

    /**
     * Creates a CSV parser with the default field length of 256 characters.  
     * This class is not thread safe, so each thread should use its own parser.
     */
    public CSVParser()
    {
        this(DEFAULT_FIELD_LENGTH);
    }

    /**
     * Creates a CSV parser with the specified field length.  If the value is less than 1,
     * the default field length will be used.  Note that as a StringBuilder is used, it will
     * expand if a field is bigger, but it will just have the performance overhead.  As the 
     * buffer is reused, setting a good max will avoid having to reallocate itself for a larger field.
     * @param fieldLength the number of characters in a typical field (should be the max expected).
     */
    public CSVParser(int fieldLength)
    {
        if ( fieldLength < 1 )
            fieldLength = DEFAULT_FIELD_LENGTH;
        list     = new LinkedList<String>();
        fieldBuf = new StringBuilder(fieldLength);
    }

    
    /**
     * Creates a string array with each element representing the field extracted from the CVS data.
     * @param csvLine the String CVS text to parse
     * @return the String array with one element for each field in the CVS data
     */
    public String[] parseToArray( String csvLine )
    {
        List<String> list = parse(csvLine);
        String[] items = new String[list.size()];
        list.toArray(items);
        list.clear(); // We'll clear it now since it's in our array
        return items;
    }
    
    /**
     * Creates a List with each element representing the field extracted from the CSV data.
     * @param csvLine the String CVS text to parse
     * @return the List<String> array with one element for each field in the CVS data
     */
    public List<String> parse( String csvLine )
    {
        list.clear();

        if ( csvLine == null || csvLine.length() == 0 ) 
        {
            list.add("");
            return list;
        }

        // No field can be bigger than the line itself
        int i = 0;

        do 
        {
            fieldBuf.setLength(0);
            
            // When we find an initial quote, we want to extract everything until
            // the matching end quote.  Otherwise, we
            if ( i < csvLine.length() && csvLine.charAt(i) == QUOTE )
                i = advanceQuoted(csvLine, fieldBuf, ++i);  // skip initial quote
            else
                i = advancePlain(csvLine, fieldBuf, i);
            
            list.add(fieldBuf.toString());

            i++;
        } while ( i < csvLine.length() );
        
        // If we end on a separator, assume there's one extra blank field at the end that follows this separator
        // a,b  --> 2 fields: "a" and "b"
        // a,b, --> 3 fields: "a", "b" and ""
        if ( csvLine.charAt(csvLine.length()-1) == FIELD_SEPARATOR )
        	list.add("");

        return list;
    }

    protected int advanceQuoted(String csvLine, StringBuilder fieldBuf, int i)
    {
        int j;
        
        int len = csvLine.length();
        
        for ( j=i; j<len; j++ ) 
        {
            if ( csvLine.charAt(j) == QUOTE && j+1 < len ) 
            {
                if ( csvLine.charAt(j+1) == QUOTE ) 
                {
                    j++; // skip escape char
                } 
                else if ( csvLine.charAt(j+1) == FIELD_SEPARATOR ) //next delimeter
                { 
                    j++; // skip end quotes
                    break;
                }
            } 
            else if ( csvLine.charAt(j) == QUOTE && j+1 == len ) // end quotes at end of line
            { 
                break; //done
            }
            
            fieldBuf.append( csvLine.charAt(j) );  // regular character.
        }
        
        return j;
    }
    

    protected int advancePlain(String csvLine, StringBuilder fieldBuf, int i)
    {
        int j;

        j = csvLine.indexOf(FIELD_SEPARATOR, i); // look for separator
        if ( j == -1 ) // separator not found
        {
            fieldBuf.append(csvLine.substring(i));
            return csvLine.length();
        } 
        
        fieldBuf.append(csvLine.substring(i, j));
        return j;
    }

    
    
    public static void main(String[] argv) 
        throws java.io.IOException 
    {
        new CSVParser().process(new BufferedReader(new InputStreamReader(System.in)));
        System.exit(0);
    }



    public void process(BufferedReader in) 
        throws java.io.IOException 
    {
        String line;
    
        // For each line...
        while ( (line=in.readLine()) != null ) 
        {
            System.out.println("CSV Input to parse() >>>" + line + "<<<");
            
            /*
            List<String> fields = parse(line);
            
            System.out.println("Found " + fields.size() + " fields:");
            for (String f : fields) 
            {
                System.out.print(f + "|");
            }
            System.out.println();
            */
            String[] fields = parseToArray(line);
            
            System.out.println("Found " + fields.length + " fields:");
            for (int i=0; i < fields.length; ++i) 
            {
                System.out.println("Field[" + i + "]: " + fields[i]);
            }
        }
    }
    
    
    public static String EncodeField(String fieldValue,char fieldSeparator,char quote)
    {
    	if ( fieldValue == null ) // CVS encoding of null is not really allowed
    		return "";
    	
        // If the input string doesn't have the field separator, CR/LF or the quoting character, then just return it
        // unchanged.
    	if ( indexOfAnyChar(fieldValue, fieldSeparator, quote, LF, CR) < 0 )
            return fieldValue;
        
        // Okay, we know we'll need to quote the resulting string
        StringBuilder buf = new StringBuilder(fieldValue.length()+10);
        buf.append(quote);
        for( int i=0; i < fieldValue.length(); ++i )
        {
            char c = fieldValue.charAt(i);
            if ( c == quote )  // if we find a quote, then we quote it by putting in two of the same quote char
                buf.append(c);
            buf.append(c);
        }
        buf.append(quote);
        return buf.toString();
    }

    public static String EncodeField(String fieldValue)
    {
        return EncodeField(fieldValue,FIELD_SEPARATOR,QUOTE);
    }
    
    
    /**
     * Determines if a string contains any of a set of characters, returning the position of the first such character found
     * or -1 if not found.  It's a simple routine, and we don't use the EsfString version here only because we avoid
     * that more general routine's test for null 's' and null/empty array of 'c' which can't happen in the CSVParser calls.
     * @param s the String to scan
     * @param c the char array to find in 's'
     * @return the index position in 's' where one of the chars in 'c' was first found
     */
    private static int indexOfAnyChar(String s, char... c)
    {
        for( int i = 0; i < s.length(); ++i )
        {
        	char stringChar = s.charAt(i);
        	for( char testChar : c )
        	{
        		if ( stringChar == testChar )
        			return i;
        	}
        }
        return -1;
    }

    
}
