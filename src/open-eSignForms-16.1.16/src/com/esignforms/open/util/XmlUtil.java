// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.util;

/**
 * Various utility routines used by various classes that work with XML.
 * @author Yozons, Inc.
 */
public final class XmlUtil
{
    public static final String XML_DECLARATION_UTF_8 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

	// This literal is embedded in XML docs.
	protected final static String XML_NAMESPACE_2009 = "http://open.esignforms.com/XMLSchema/2009";
	protected final static String XML_NAMESPACE_2011 = "http://open.esignforms.com/XMLSchema/2011";
	protected final static String XML_NAMESPACE_2012 = "http://open.esignforms.com/XMLSchema/2012";
	protected final static String XML_NAMESPACE_2013 = "http://open.esignforms.com/XMLSchema/2013";
	protected final static String XML_NAMESPACE_TRANADMIN_API_2015 = "http://open.esignforms.com/XMLSchema/TranAdminApi/2015";
	
	// Just static routines -- no object to instantiate
    private XmlUtil()
    {
    }
    
    /**
     * Returns the Open eSignForms XML namespace for 2009.
     * @return the String XML namespace for Open eSignForms 2009
     */
    public static final String getXmlNamespace2009()
    {
        return XML_NAMESPACE_2009;
    }
    
    /**
     * Returns the Open eSignForms XML namespace for 2011.
     * @return the String XML namespace for Open eSignForms 2011
     */
    public static final String getXmlNamespace2011()
    {
        return XML_NAMESPACE_2011;
    }
    
    /**
     * Returns the Open eSignForms XML namespace for 2012.
     * @return the String XML namespace for Open eSignForms 2012
     */
    public static final String getXmlNamespace2012()
    {
        return XML_NAMESPACE_2012;
    }
    
    /**
     * Returns the Open eSignForms XML namespace for 2013.
     * @return the String XML namespace for Open eSignForms 2013
     */
    public static final String getXmlNamespace2013()
    {
        return XML_NAMESPACE_2013;
    }
    
    /**
     * Returns the Open eSignForms XML namespace for Tran Admin API 2015.
     * @return the String XML namespace for Open eSignForms Tran Admin API 2015
     */
    public static final String getXmlNamespaceTranAdminApi2015()
    {
        return XML_NAMESPACE_TRANADMIN_API_2015;
    }
    
    public static boolean isUnexpectedISOControlCharacter(char c)
    {
    	if ( c == '\n' || c == '\r' || c == '\t' )
    		return false;
    	return Character.isISOControl(c);
    }
    
 	/**
	 * This method takes the string and converts certain worrisome characters into
	 * their XML-safe codes.
	 * @param s the String to be included in XML
	 * @return the XML string that represents 's'
	 */
	public static String toEscapedXml( String s )
	{
		if ( s == null )
			return "";

		// The gamble here is that most strings will not contain the special characters,
		// so we optimize things in that case by returning the original string without creating any new objects.
		int length = s.length();
		int currentPos;

		for ( currentPos = 0; currentPos < length; ++currentPos )
		{
			char c = s.charAt( currentPos );

			if ( c == '"' || c == '<' || c == '>' || c == '&' || c == '\'' || isUnexpectedISOControlCharacter(c) )
				break; // we'll need to escape this one
		}

		// No quoting necessary, so just return the original string back.
		if ( currentPos >= length )
			return s;
     
		StringBuilder buf = new StringBuilder(length*2);
		appendEscapedXml(s,buf);
		return buf.toString();
	}

	/**
	 * This method takes the string and converts certain worrisome characters into
	 * their XML-safe codes, appending to the specified buffer.
     * @param s the String to convert to an XML-safe string
     * @param buf the StringBuilder to put the XML converted String into
     * @return the StringBuilder object 'buf' updated to include 's'
	 */
	public static StringBuilder appendEscapedXml( String s, StringBuilder buf )
	{
		if ( s == null )
			return buf;
		
		// The gamble here is that most strings will not contain the special characters,
		// so we optimize things in that case by appending the original string without creating any new objects.
		int length = s.length();
		int currentPos;

		for ( currentPos = 0; currentPos < length; ++currentPos )
		{
			char c = s.charAt( currentPos );

			if ( c == '"' || c == '<' || c == '>' || c == '&' || c == '\'' || isUnexpectedISOControlCharacter(c) )
				break; // we'll need to escape this one
		}

		// No quoting necessary, so just append the original string into the buffer.
		if ( currentPos >= length )
		{
			buf.append(s);
			return buf;
		}
     
		buf.append(s.substring( 0, currentPos ));

		for ( ; currentPos < length; ++currentPos )
		{
			char c = s.charAt( currentPos );

			switch ( c )
			{
			case '"':
				buf.append( "&quot;" );
				break;

			case '\'':
			   buf.append( "&apos;" );  // aka: &#039; but we don't do any of these codes here
			   break;

			case '<':
				buf.append( "&lt;" );
				break;

			case '>':
				buf.append( "&gt;" );
				break;

			case '&':
			{
				// if we have &# then we'll assume this is some sort of XML escaped char (Word quotes, dashes, etc.)
				// and we won't do anything with it.
				int nextPos = currentPos + 1;

				if ( ( nextPos < length ) && ( s.charAt( nextPos ) == '#' ) )
					buf.append( c );
				else
					buf.append( "&amp;" );
				break;
			}

			default:
				if ( ! isUnexpectedISOControlCharacter(c) )  // we won't allow these in our XML data
				    buf.append( c );
				break;
			}
		}

		return buf;
	}

}