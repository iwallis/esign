// Open eSignForms - Web-based electronic contracting software
// Copyright (C) 2009 Yozons, Inc.
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.util;

/**
 * Various utility routines used by various classes that work with HTML.
 * @author Yozons, Inc.
 */
public final class HtmlUtil
{
	// Just static routines -- no object to instantiate
    private HtmlUtil()
    {
    }

    
    /**
     * Returns the HTML 4.01 doctype.
     * @return the String HTML 4.01 strict doctype
     * @see #getDocTypeXtml1()
     */
	public static final String getDocTypeHtml4()
	{
		return "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\r\n";
	}
    
	/** 
	 * Returns the XHTML 1.0 doctype.
	 * @return the String XHTML 1.0 Transitional doctype
	 * @see #getDocTypeHtml4()
	 */
	public static final String getDocTypeXhtml1()
	{
		return "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n";

	}

    
	/**
	 * Converts the specified String into a pretty version for display-only needs.
	 * @param s the String to display in pretty HTML
	 * @return the pretty HTML string that represents 's'
	 */
    public static final String toDisplayHtml( String s )
    {
    	return toEscapedHtml(s,true);
    }

	/**
	 * Converts the specified String into an HTML-safe version, such as for input values, select options, etc.
	 * @param s the String to be included in HTML
	 * @return the HTML string that represents 's'
	 */
    public static final String toEscapedHtml( String s )
    {
    	return toEscapedHtml(s,false);
    }
    
	/**
     * This method takes the string and converts certain worrisome characters into
     * their HTML-safe codes.
     * @param s the String to convert to an HTML-safe string
     * @param doPrettyEscaping if true, convert CR+LF to <br/> tags and use &nbsp; for multiple spaces in a row
     * @return the HTML string that represents 's'
     */
    public static String toEscapedHtml( String s, boolean doPrettyEscaping )
    {
		if ( s == null )
			return "";

		// The gamble here is that most strings will not contain the special characters,
		// so we optimize things in that case by returning the original string without creating any new objects.
		int length = s.length();
		int currentPos;

		for ( currentPos = 0; currentPos < length; ++currentPos )
		{
			char c = s.charAt( currentPos );

			if ( c == '"' || c == '<' || c == '>' || c == '&' || c == '\'')
				break; // we'll need to escape this one
			if ( c == '\r' || c == '\n' )
			{
			   if ( doPrettyEscaping )
				   break; // we'll need to escape this one
			}
			else if ( c == ' ' )
			{
			   int nextPos = currentPos + 1;

			   if ( doPrettyEscaping && ( nextPos < length ) && ( s.charAt( nextPos ) == ' ' ) )
				   break; // we'll need to escape this one
			}
		}

		// No quoting necessary, so just return the original string back.
		if ( currentPos >= length )
			return s;
     
		StringBuilder buf = new StringBuilder(length*2);
		appendEscapedHtml(s,doPrettyEscaping,buf);
		return buf.toString();
    }

	/**
     * This method takes the string and converts certain worrisome characters into
     * their HTML-safe codes, appending to the specified buffer
     * @param s the String to convert to an HTML-safe string
     * @param doPrettyEscaping if true, convert CR+LF to <br/> tags and use &nbsp; for multiple spaces in a row
     * @param buf the StringBuilder to put the HTML converted String into
     * @return the StringBuilder object 'buf' updated to include 's'
     */
    public static StringBuilder appendEscapedHtml( String s, boolean doPrettyEscaping, StringBuilder buf )
    {
		if ( s == null )
			return buf;

		// The gamble here is that most strings will not contain the special characters,
		// so we optimize things in that case by returning the original string without creating any new objects.
		int length = s.length();
		int currentPos;

		for ( currentPos = 0; currentPos < length; ++currentPos )
		{
			char c = s.charAt( currentPos );

			if ( c == '"' || c == '<' || c == '>' || c == '&' || c == '\'')
				break; // we'll need to escape this one
			if ( c == '\r' || c == '\n' )
			{
			   if ( doPrettyEscaping )
				   break; // we'll need to escape this one
			}
			else if ( c == ' ' )
			{
			   int nextPos = currentPos + 1;

			   if ( doPrettyEscaping && ( nextPos < length ) && ( s.charAt( nextPos ) == ' ' ) )
				   break; // we'll need to escape this one
			}
		}

		// No quoting necessary, so just return the original string back.
		if ( currentPos >= length )
		{
			buf.append(s);
			return buf;
		}
     
		buf.append(s.substring( 0, currentPos ));

		for ( ; currentPos < length; ++currentPos )
		{
			char c = s.charAt( currentPos );

			switch ( c )
			{
			case '"':
				buf.append( "&quot;" );
				break;

			case '<':
				buf.append( "&lt;" );
				break;

			case '>':
				buf.append( "&gt;" );
				break;

			case '\'':
			   buf.append( "&#039;" );
			   break;

			case '&':
			{
				// if we have &# then we'll assume this is some sort of HTML escaped char (Word quotes, dashes, etc.)
				// and we won't do anything with it.
				int nextPos = currentPos + 1;

				if ( ( nextPos < length ) && ( s.charAt( nextPos ) == '#' ) )
					buf.append( c );
				else
					buf.append( "&amp;" );
				break;
			}

			case '\r':
				if ( doPrettyEscaping )
					buf.append( "<br />" );
				else
					buf.append(c);
				break;

			case '\n':

				if ( ( currentPos > 0 ) && ( s.charAt( currentPos - 1 ) == '\r' ) )
				{
					buf.append( c );
				}
				else
				{
				   if ( doPrettyEscaping )
					   buf.append( "<br />\n" );
				   else
					   buf.append(c);
				}
				break;

			case ' ':
			{
				int nextPos = currentPos + 1;

				if ( doPrettyEscaping && ( nextPos < length ) && ( s.charAt( nextPos ) == ' ' ) )
					buf.append( "&nbsp;" );
				else
					buf.append( c );

				break;
			}

			default:
				buf.append( c );
				break;
			}
		}

		return buf;
    }

}