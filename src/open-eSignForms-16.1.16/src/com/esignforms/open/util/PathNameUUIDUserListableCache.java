// Copyright (C) 2010-2011 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.TreeMap;

import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfUUID;
import com.esignforms.open.user.User;

/**
* Holds an object that's known by both its EsfUUID and EsfName.  The object is stored in two distinct, but otherwise
* synchronized TreeMaps.  Note that the EsfUUID may not be changed, but the EsfName can be changed.  
* 
* @author Yozons, Inc.
*/
public class PathNameUUIDUserListableCache<T extends PathNameUUIDUserListableCache.PathNameUUIDUserListableCacheable>
{
	public interface PathNameUUIDUserListableCacheable
	{
		public EsfUUID getId();
		public EsfPathName getPathName();
		public EsfPathName getOriginalPathName();
		public void resetOriginalPathName();
		public boolean canUserList(User user);
	}
	
    // We create two caches, one searchable by the EsfUUID, and one by the EsfString (esfname path).  We ensure that adds
	// to the cache are put in both, and deletes are removed from both, and that updates are updated in both.
	// Access is synchronized to prevent iterator issues.
    protected TreeMap<EsfUUID,T> uuidCache	= new TreeMap<EsfUUID,T>();
    protected TreeMap<EsfPathName,T> pathNameCache	= new TreeMap<EsfPathName,T>();
    protected int maxSize = 0;
    

    public PathNameUUIDUserListableCache()
    {
    }
    
    public synchronized void clear()
    {
    	uuidCache.clear();
    	pathNameCache.clear();
    }
    
    /**
     * Returns the number objects in the cache.
     */
    public synchronized int size()
    {
    	return uuidCache.size();
    }
    
    /**
     * Returns the number objects in the cache that the specified user can list.
     */
	public synchronized int size(User user)
    {
    	if ( user == null )
    		return 0;
    	int count = 0;
    	for( T obj : uuidCache.values() )
    	{
    		if ( obj.canUserList(user) )
    			++count;
    	}
    	return count;
    }
    
    /**
     * Adds the object to the cache if and only if the object's id and name are not already in the cache.
     * @param obj the object to add
     * @return true if the object is added to the cache; false if the cache already contains an object with the id/name
     */
    public synchronized boolean add(final T obj)
    {
    	EsfUUID id = obj.getId();
    	EsfPathName pathName = obj.getPathName();
    	
    	if ( uuidCache.containsKey(id) || pathNameCache.containsKey(pathName) )
    		return false;
    	
    	uuidCache.put(id, obj);
    	pathNameCache.put(pathName, obj);
    	obj.resetOriginalPathName();
    	
    	int currSize = uuidCache.size();
    	if ( currSize > maxSize )
    		maxSize = currSize;

    	return true;
    }

    /**
     * Replaces the specified object in the cache.  The object must already exist with the same id, and that cache entry
     * will be updated to use this new object.  The path of the object updated by id is removed and the new object's path is added
     * so that if you rename the object, it will work as expected (the old path will no longer be there and the new path will).
     * @param obj the object to replace in the cache
     * @return true if the cache is updated with the new object; false if the id doesn't exist in the cache
     */
    public synchronized boolean replace(final T obj)
    {
    	EsfUUID id = obj.getId();
    	EsfPathName pathName = obj.getPathName();
    	
    	if ( ! uuidCache.containsKey(id) )
    		return false;
    	
    	T origObj = uuidCache.remove(id);
    	if ( origObj == null )
    		return false;

    	pathNameCache.remove(origObj.getOriginalPathName()); 
    	obj.resetOriginalPathName();
    	uuidCache.put(id, obj);
    	pathNameCache.put(pathName,obj);
    	return true;
    }

    /**
     * Removes the specified object from the cache using only the id as the key.  The name is ignored
     * and the name associated with the original named object is used.
     * @param obj the object to remove
     * @return
     */
    public synchronized boolean remove(final T obj)
    {
    	EsfUUID id = obj.getId();

    	if ( ! uuidCache.containsKey(id) )
    		return false;

    	T origObj = uuidCache.remove(obj.getId());
    	if ( origObj == null )
    		return false;
    	pathNameCache.remove(origObj.getOriginalPathName());
    	return true;
    }

    /**
     * Gets the object by id.
     * @param id the id of the object to retrieve
     * @return the object found or null if not present
     */
    public synchronized T getById(EsfUUID id)
    {
    	return uuidCache.get(id);
    }

    /**
     * Gets the object by id only if the user can list it.
     * @param id the id of the object to retrieve
     * @param user the User who is attempting to retrieve it
     * @return the object found or null if not present
     */
    public synchronized T getById(EsfUUID id, User user)
    {
    	if ( user == null )
    		return null;
    	T obj = uuidCache.get(id);
    	return ( obj == null || ! obj.canUserList(user) ) ? null : obj;
    }


    /**
     * Gets all of the objects in a collection sorted by id.
     * @return collection of objects
     */
    public synchronized Collection<T> getAllById()
    {
    	return new LinkedList<T>(uuidCache.values());
    }

    /**
     * Gets all of the objects in a collection sorted by id if the user can list it
     * @param user the User who is attempting to retrieve them
     * @return the collection of objects the user can list
     */
    public synchronized Collection<T> getAllById(User user)
    {
    	if ( user == null )
    		return new LinkedList<T>();
    	
    	LinkedList<T> list = new LinkedList<T>();
    	for( T obj : uuidCache.values() )
    	{
    		if ( obj.canUserList(user) )
    			list.add(obj);
    	}
    	return list;
    }


    
    /**
     * Gets the object by name.
     * @param pathName the path name of the object to retrieve
     * @return the object found or null if not present
     */
    public synchronized T getByPathName(EsfPathName pathName)
    {
    	return pathNameCache.get(pathName);
    }
    
    /**
     * Gets the object by path name only if the user can list it.
     * @param pathName the path name of the object to retrieve
     * @param user the User who is attempting to retrieve it
     * @return the object found or null if not present
     */
    public synchronized T getByPathName(EsfPathName pathName, User user)
    {
    	if ( user == null )
    		return null;
    	T obj = pathNameCache.get(pathName);
    	return ( obj == null || ! obj.canUserList(user) ) ? null : obj;
    }


    /**
     * Gets the all of the objects ordered by path name
     * @return the collection of all objects ordered by path name
     */
    public synchronized Collection<T> getAllByPathName()
    {
    	return new LinkedList<T>(pathNameCache.values());
    }
    
    /**
     * Gets the all of the objects ordered by path name that the user can list
     * @param user the User who is attempting to retrieve them
     * @return the collection of all objects ordered by path name that the user can list
     */
    public synchronized Collection<T> getAllByPathName(User user)
    {
    	if ( user == null )
    		return new LinkedList<T>();
    	LinkedList<T> list = new LinkedList<T>();
    	for( T obj : pathNameCache.values() )
    	{
    		if ( obj.canUserList(user) )
    			list.add(obj);
    	}
    	return list;
    }
}