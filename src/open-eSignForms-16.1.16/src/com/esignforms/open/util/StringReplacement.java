// Copyright (C) 2011-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.util;

import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;

import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfName;
import com.esignforms.open.data.EsfPathName;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.data.EsfValue;
import com.esignforms.open.jsp.libdocsgen.taglib.FieldLabel;
import com.esignforms.open.prog.Document;
import com.esignforms.open.prog.DocumentVersion;
import com.esignforms.open.prog.FieldTemplate;
import com.esignforms.open.prog.Image;
import com.esignforms.open.prog.ImageVersion;
import com.esignforms.open.prog.Library;
import com.esignforms.open.prog.Package;
import com.esignforms.open.prog.PackageVersion;
import com.esignforms.open.prog.Serial;
import com.esignforms.open.runtime.Transaction;
import com.esignforms.open.runtime.TransactionDocument;
import com.esignforms.open.user.User;

/**
* This is a common routine that attempts to take a string with replaceable fields in the ${} notation as well as optional text replacement specs,
* doing them repeatedly until no more substitutions take place and returns that back.
* 
* @author Yozons, Inc.
*/
public class StringReplacement
{
	public static class FromToSpec
	{
		public String from;
		public String to;
		public FromToSpec(String from, String to)
		{
			this.from = from;
			this.to = to;
		}
	}
	
    // Finds the words made up of A-Z, a-z, 0-9, '.' or '_' inside a braced field: ${}
    // The "." is a separator between EsfName components.  When used, the first part generally identifies another record/document to get the field, or a propertyset for a given property
    private static java.util.regex.Pattern ReplaceFieldSpecFieldPattern = java.util.regex.Pattern.compile("\\$\\{(document:|label:|field:|out:|transaction:|property:|htmlproperty:|serial:|image:)?([A-Za-z][A-Za-z0-9_.\\[\\]]*)\\}");

    
    private static String _replaceStrings(String originalString, List<FromToSpec> replacementSpecs)
    {
    	String expandedString = originalString;
    	
    	for( FromToSpec spec : replacementSpecs )
    		expandedString = expandedString.replace(spec.from, spec.to);
    	
    	return expandedString;
    }
    
    private static String _replaceUrlEncodedStrings(String originalString, List<FromToSpec> replacementSpecs)
    {
    	String expandedString = originalString;
    	
    	for( FromToSpec spec : replacementSpecs )
    		expandedString = expandedString.replace(spec.from, ServletUtil.urlEncode(spec.to));
    	
    	return expandedString;
    }
    
    
    
    private static String _substituteFieldSpec(User user, Transaction transaction, DocumentVersion documentVersion, String sourceType, String fieldPath)
    {
        StringTokenizer st = new StringTokenizer(fieldPath,".");
        
        String fieldName;
        String fieldSource = (st.hasMoreTokens() ) ? st.nextToken() : null;
        if ( fieldSource == null )
        	return null;
        boolean fieldSourceSpecified;
        if ( st.hasMoreTokens() )
        {
        	fieldSourceSpecified = true;
        	fieldName = st.nextToken();
        	while( st.hasMoreTokens() )
        		fieldName += "." + st.nextToken();
        }
        else
        {
        	fieldName = fieldSource; // no source specified, so it's actually the field name, and our source is the current document
        	fieldSourceSpecified = false;
        	fieldSource = Literals.FIELD_SOURCE_TYPE_DOCUMENT;
        }
        
        EsfName esfFieldName = new EsfName(fieldName);

        if ( Literals.FIELD_SOURCE_TYPE_FIELD.equals(sourceType) && transaction != null )
        {
    		EsfName documentName;
    		if ( documentVersion != null && Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(fieldSource) )
    			documentName = documentVersion.getDocument().getEsfName();
    		else
    			documentName = new EsfName(fieldSource);
        	EsfValue value = transaction.getFieldValue(documentName, esfFieldName, fieldSourceSpecified);
        	return value == null ? null : value.toString();
        }
        
        if ( Literals.FIELD_SOURCE_TYPE_FIELD_OUT.equals(sourceType) && transaction != null )
        {
        	DocumentVersion checkDocVersion;
    		EsfName documentName;
    		if ( documentVersion != null && Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(fieldSource) )
    		{
    			documentName = documentVersion.getDocument().getEsfName();
    			checkDocVersion = documentVersion;
    		}
    		else
    		{
    			documentName = new EsfName(fieldSource);
    			checkDocVersion = transaction.getDocumentVersionByName( documentName );
    			// If we cannot find a document version, and no document name was used, we'll assume the first document we find that contains
    			// the named field.
        		if ( checkDocVersion == null && Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(fieldSource) )
        		{
        			TransactionDocument td = transaction.getFirstTransactionDocumentWithValueForField(esfFieldName);
        			if ( td != null )
        				checkDocVersion = td.getDocumentVersion();
        		}
    		}
    		
    		FieldTemplate fieldTemplate = checkDocVersion == null ? null : checkDocVersion.getFieldTemplate( new EsfName(fieldName) );
    		if ( fieldTemplate == null )
    		{
            	EsfValue value = transaction.getFieldValue(documentName, esfFieldName, fieldSourceSpecified);
            	return value == null ? null : value.toString();
    		}
    		
        	EsfValue value = transaction.getFieldValue(documentName, esfFieldName, fieldSourceSpecified);
    		return FieldLabel.getFormattedFieldValue(user,transaction,checkDocVersion,fieldTemplate,value);
        }
        
        if ( Literals.FIELD_SOURCE_TYPE_LABEL.equals(sourceType) )
        {
    		DocumentVersion checkDocVersion;
    		if ( documentVersion != null && Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(fieldSource) )
    			checkDocVersion = documentVersion;
    		else if ( transaction != null )
    			checkDocVersion = transaction.getDocumentVersionByName( new EsfName(fieldSource) );
    		else
    			checkDocVersion = null;
    		if ( checkDocVersion == null )
    			return null;
    		FieldTemplate fieldTemplate = checkDocVersion.getFieldTemplate( new EsfName(fieldName) );
    		if ( fieldTemplate == null )
    			return null;
    		return fieldTemplate.getLabelTemplate().getLabel();
        }
        
    	if ( Literals.FIELD_SOURCE_TYPE_PROPERTY.equals(sourceType) || Literals.FIELD_SOURCE_TYPE_HTML_PROPERTY.equals(sourceType) )
        {
    		if ( ! fieldSourceSpecified ) 
    			fieldSource = "";
    		EsfName propertySetName = EsfString.isBlank(fieldSource) ? null : new EsfName(fieldSource);
    		
    		EsfPathName propertyName = new EsfPathName(fieldName);
    		
    		if ( user != null )
    		{
    			EsfValue value = user.getPropertyValue(propertySetName, propertyName, transaction.doProductionResolve(), transaction.isTest());
    			if ( value != null )
    				return value.toString();
    		}
    		
    		List<Library> searchedLibraries = new LinkedList<Library>();

    		if ( transaction != null )
    		{
        		// First let's look in our brand library
        		if ( transaction.hasBrandLibraryId() )
        		{
        			Library brandLibrary = transaction.getBrandLibrary();
        			EsfValue value = brandLibrary.getPropertyValue(propertySetName, propertyName, transaction.doProductionResolve(), transaction.isTest());
        			if ( value != null )
        				return value.toString();

        			searchedLibraries.add(brandLibrary);
        		}
    		}
    		
    		// Second, if we have a document version, check if the property is the library it's defined in
    		if ( documentVersion != null )
    		{
    			Library documentLibrary = documentVersion.getDocument().getLibrary();
    			if ( ! searchedLibraries.contains(documentLibrary) )
    			{
    				EsfValue value = documentLibrary.getPropertyValue(propertySetName, propertyName, transaction != null && transaction.doProductionResolve(), transaction == null || transaction.isTest());
    				if ( value != null )
    					return value.toString();

    				searchedLibraries.add(documentLibrary);
    			}
    		}

    		// Finally, search the template library if it hasn't been searched yet...
    		Library templateLibrary = Library.Manager.getTemplate();
    		if (searchedLibraries.contains(templateLibrary) )
    			return null;
    		
    		EsfValue value = templateLibrary.getPropertyValue(propertySetName, propertyName, transaction != null && transaction.doProductionResolve(), transaction == null || transaction.isTest());
        	return value == null ? null : value.toString();
        }
    	
        if ( Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(sourceType) )
        {
    		Document document;
    		if ( documentVersion != null && Literals.FIELD_SOURCE_TYPE_DOCUMENT.equals(fieldSource) )
    			document = documentVersion.getDocument();
    		else if ( transaction != null )
    			document = transaction.getDocumentByName( new EsfName(fieldSource) );
    		else
    			document = null;
    		if ( document == null )
    			return null;
    		// document info
        	if ( "esfname".equalsIgnoreCase(fieldName) )
        		return document.getEsfName().toString();
        	if ( "id".equalsIgnoreCase(fieldName) )
        		return document.getId().toString();
        	if ( "displayname".equalsIgnoreCase(fieldName) )
        		return document.getDisplayName();

        	DocumentVersion checkDocVersion;
        	if ( documentVersion != null )
        		checkDocVersion = documentVersion;
        	else if (transaction != null )
        		checkDocVersion = transaction.getDocumentVersion(document);
        	else
        		checkDocVersion = null;
        	if ( checkDocVersion != null )
        	{
            	if ( "version".equalsIgnoreCase(fieldName) )
            		return Integer.toString(checkDocVersion.getVersion());
            	if ( "version".equalsIgnoreCase(fieldSource) )
            	{
                	if ( "id".equalsIgnoreCase(fieldName) )
                		return checkDocVersion.getId().toString();
                	return null;
            	}
        	}
        	
        	if ( transaction != null )
        	{
        		Package pkg = transaction.getPackage();
            	if ( "package".equalsIgnoreCase(fieldSource) )
            	{
                	if ( "pathname".equalsIgnoreCase(fieldName) )
                		return pkg.getPathName().toString();
                	if ( "id".equalsIgnoreCase(fieldName) )
                		return pkg.getId().toString();

                	PackageVersion packageVersion = transaction.getPackageVersion();
                   	if ( "version".equalsIgnoreCase(fieldName) )
                		return Integer.toString(packageVersion.getVersion());
                   	if ( "version.id".equalsIgnoreCase(fieldName) )
                		return packageVersion.getId().toString();
                   	return null;
            	}
        	}
           	
           	return null;
        }
        
        if ( Literals.FIELD_SOURCE_TYPE_TRANSACTION.equals(sourceType) && transaction != null )
        {
        	if ( "id".equalsIgnoreCase(fieldName) )
        		return transaction.getId().toString();
        	if ( "pathname".equalsIgnoreCase(fieldName) )
        		return transaction.getTransactionTemplate().getPathName().toString();
        	if ( "displayname".equalsIgnoreCase(fieldName) )
        		return transaction.getTransactionTemplate().getDisplayName().toString();
        	if ( "user".equalsIgnoreCase(fieldSource) )
        	{
            	if ( "id".equalsIgnoreCase(fieldName) )
            		return user == null ? null : user.getId().toString();
            	if ( "email".equalsIgnoreCase(fieldName) )
            		return user == null ? null : user.getEmail();
            	if ( "email_address".equalsIgnoreCase(fieldName) )
            		return user == null ? null : user.getEmailAddress().toString();
            	if ( "personal_name".equalsIgnoreCase(fieldName) )
            		return user == null ? null : user.getPersonalName();
            	if ( "family_name".equalsIgnoreCase(fieldName) )
            		return user == null ? null : user.getFamilyName();
            	if ( "employee_id".equalsIgnoreCase(fieldName) )
            		return user == null ? null : user.getEmployeeId();
               	if ( "job_title".equalsIgnoreCase(fieldName) )
               		return user == null ? null : user.getJobTitle();
               	if ( "department".equalsIgnoreCase(fieldName) )
               		return user == null ? null : user.getDepartment();
               	if ( "phone_number".equalsIgnoreCase(fieldName) )
               		return user == null ? null : user.getPhoneNumber();
               	return null;
        	}

        	// Let's see if it's in our transactions's record
        	EsfValue value = transaction.getFieldValue(new EsfName(Literals.FIELD_SOURCE_TYPE_TRANSACTION), new EsfName(fieldName));
        	return value == null ? null : value.toString();
        }

    	if ( Literals.FIELD_SOURCE_TYPE_SERIAL.equals(sourceType) )
        {
    		EsfName serialName = new EsfName(fieldName);
    		
    		List<Library> searchedLibraries = new LinkedList<Library>();
    		
    		if ( transaction != null )
    		{
        		// First let's look in our brand library
        		if ( transaction.hasBrandLibraryId() )
        		{
        			Library brandLibrary = transaction.getBrandLibrary();
        			
        			Serial serial = Serial.Manager.getByName(brandLibrary.getId(), serialName);
        			if ( serial != null && serial.isEnabled() )
        			{
            			if ( transaction.doProductionResolve() )
            			{
            				if ( serial.hasProductionVersion() )
            					return serial.nextFormattedProductionSerial();
            			}
            			else
            			{
            				return serial.nextFormattedTestSerial();
            			}
        			}
        			
        			searchedLibraries.add(brandLibrary);
        		}
    		}
    		
    		// Second, if we have a document version, check if the serial is the library it's defined in
    		if ( documentVersion != null )
    		{
    			Library documentLibrary = documentVersion.getDocument().getLibrary();
    			if ( ! searchedLibraries.contains(documentLibrary) )
    			{
        			Serial serial = Serial.Manager.getByName(documentLibrary.getId(), serialName);
        			if ( serial != null && serial.isEnabled() )
        			{
            			if ( transaction.doProductionResolve() )
            			{
            				if ( serial.hasProductionVersion() )
            					return serial.nextFormattedProductionSerial();
            			}
            			else
            			{
            				return serial.nextFormattedTestSerial();
            			}
        			}

        			searchedLibraries.add(documentLibrary);
    			}
    		}

    		// Finally, search the template library if it hasn't been searched yet...
    		Library templateLibrary = Library.Manager.getTemplate();
    		if (searchedLibraries.contains(templateLibrary) )
    			return null;
    		
			Serial serial = Serial.Manager.getByName(templateLibrary.getId(), serialName);
			if ( serial != null && serial.isEnabled() )
			{
    			if ( transaction.doProductionResolve() )
    			{
    				if ( serial.hasProductionVersion() )
    					return serial.nextFormattedProductionSerial();
    			}
    			else
    			{
    				return serial.nextFormattedTestSerial();
    			}
			}
        }
    	
        if ( Literals.FIELD_SOURCE_TYPE_IMAGE.equals(sourceType) )
        {
        	EsfName imageName = new EsfName(fieldName);
        	Image image = null;
        	
        	// If no transaction, it'll have to be in our template library
        	if ( transaction != null )
        		image = transaction.getImage(imageName,null,documentVersion);
        	else
        	{
        		Library templateLibrary = Library.Manager.getTemplate();
        		image = Image.Manager.getByName(templateLibrary.getId(), imageName);
        	}
    		if ( image != null )
    		{
    			ImageVersion imageVersion = null;
    			if ( transaction != null )
    				imageVersion = transaction.doProductionResolve() ? image.getProductionImageVersion() : image.getTestImageVersion();
    			else
    				imageVersion = image.getTestImageVersion();
    			if ( imageVersion != null )
    			{
    	    		if ( imageVersion.isUseDataUri() )
        				return "<img src=\"" + imageVersion.getDataUri() + "\" />";

    				return "<img src=\"${CONTEXTPATH}/images/" + image.getEsfName() + "/" + ServletUtil.urlEncode(imageVersion.getImageFileName()) + "?ivid=" + imageVersion.getId().toHtml() + "\" />";
    			}
    		}
        }
        
    	return null;
    }

    private static String _substituteFieldSpecs(String fieldSpecToExpand, User user, Transaction transaction, DocumentVersion documentVersion)
    {
        Matcher m = ReplaceFieldSpecFieldPattern.matcher(fieldSpecToExpand);
        if ( m == null )
            return fieldSpecToExpand;
        
        StringBuffer buf = null;
        while( m.find() ) 
        {
        	//String matchedText = m.group(0);
            String sourceType = m.group(1);
            String fieldPath = m.group(2);
            
            if ( EsfString.isBlank(sourceType) )
            	sourceType = Literals.FIELD_SOURCE_TYPE_FIELD;
            else // strip the colon at the end of our matched text that includes one
            	sourceType = sourceType.substring(0, sourceType.length()-1);
            
            String replacementText = _substituteFieldSpec(user,transaction,documentVersion,sourceType,fieldPath);
            if ( replacementText != null )
            {
           	 	if ( buf == null )
           	 		buf = new StringBuffer(fieldSpecToExpand.length());
                m.appendReplacement(buf, Matcher.quoteReplacement(replacementText));
            }
        }
   	 	if ( buf == null )
   	 		return fieldSpecToExpand;
   	 
   	 	m.appendTail(buf);
   	 	return buf.toString();
    }
    
    private static String _substituteUrlEncodedFieldSpecs(String fieldSpecToExpand, User user, Transaction transaction, DocumentVersion documentVersion)
    {
        Matcher m = ReplaceFieldSpecFieldPattern.matcher(fieldSpecToExpand);
        if ( m == null )
            return fieldSpecToExpand;
        
        StringBuffer buf = null;
        while( m.find() ) 
        {
        	//String matchedText = m.group(0);
            String sourceType = m.group(1);
            String fieldPath = m.group(2);
            
            if ( EsfString.isBlank(sourceType) )
            	sourceType = Literals.FIELD_SOURCE_TYPE_FIELD;
            else // strip the colon at the end of our matched text that includes one
            	sourceType = sourceType.substring(0, sourceType.length()-1);
            
            String replacementText = _substituteFieldSpec(user,transaction,documentVersion,sourceType,fieldPath);
            if ( replacementText != null )
            {
           	 	if ( buf == null )
           	 	{
           	 		buf = new StringBuffer(fieldSpecToExpand.length());
           	 		m.appendReplacement(buf, Matcher.quoteReplacement(replacementText)); // don't URL encode the initial buffer
           	 	}
           	 	else
           	 	{
           	 		if ( buf.indexOf("://") > 3 && buf.indexOf("?") > 10 ) // only encode if our existing buffer at least seems to have a scheme and '?' params separator
           	 			m.appendReplacement(buf, ServletUtil.urlEncode(Matcher.quoteReplacement(replacementText)));
           	 		else
               	 		m.appendReplacement(buf, Matcher.quoteReplacement(replacementText));
           	 	}
            }
        }
   	 	if ( buf == null )
   	 		return fieldSpecToExpand;
   	 
   	 	m.appendTail(buf);
   	 	return buf.toString();
    }
    
    /**
     * This will replace the template string using data stored in the specified transaction, document version and replacement specs.
     * The process is that we first attempt to do all of the fixed replacement specs first, then the ${} template replacements, and we
     * continue to do both until no more expansions take place.
     * @param templateString the required String to be expanded/replaced
     * @param transaction the optional Transaction to use for doing data substitutions
     * @param documentVersion the optional DocumentVersion to use for doing data substitutions
     * @param replacementSpecs the optional list of fixed replacements to do
     * @return
     */
    public static String replace(String templateString, User user, Transaction transaction, DocumentVersion documentVersion, List<FromToSpec> replacementSpecs)
    {
    	if ( EsfString.isBlank(templateString) )
    		return templateString;
    	
    	String priorToReplace;
    	String replacedString = templateString;
    	do
    	{
    		priorToReplace = replacedString;
    		if ( replacementSpecs != null )
    			replacedString = _replaceStrings(replacedString, replacementSpecs);
    		replacedString = _substituteFieldSpecs(replacedString, user, transaction, documentVersion);
    	} while( ! priorToReplace.equals(replacedString) );
    	
    	return replacedString;
    }
    
    public static final String replace(String templateString, User user, Transaction transaction, DocumentVersion documentVersion)
    {
    	LinkedList<FromToSpec> nullReplacementSpecs = null;
    	return replace(templateString,user,transaction,documentVersion,nullReplacementSpecs);
    }
    
    /**
     * This will expand a string so long as it contains simple ${property:xxx} type syntax that can be resolved entirely from
     * the template library.
     * @param templateString
     * @return the expanded version of that string
     */
    public static final String replace(String templateString, List<FromToSpec> replacementSpecs)
    {
    	User nullUser = null;
    	Transaction nullTran = null;
    	DocumentVersion nullDocVersion = null;
    	
    	return replace(templateString,nullUser,nullTran,nullDocVersion,replacementSpecs);
    }

    /**
     * This will expand a string so long as it contains simple ${property:xxx} type syntax that can be resolved entirely from
     * the template library and/or the one specified set of replacements.
     * @param templateString the string to expand
     * @param fromString the String that if found will be replaced by toString
     * @param toString the String that replaces fromString when found in templateString
     * @return the expanded version of that string
     */
    public static final String replace(String templateString, String fromString, String toString)
    {
    	User nullUser = null;
    	Transaction nullTran = null;
    	DocumentVersion nullDocVersion = null;
    	
    	List<FromToSpec> replacementSpecs = null;
    	if ( fromString != null && toString != null )
    	{
    		replacementSpecs = new LinkedList<FromToSpec>();
    		replacementSpecs.add( new FromToSpec(fromString,toString) );
    	}
    	
    	return replace(templateString,nullUser,nullTran,nullDocVersion,replacementSpecs);
    }

    /**
     * This will expand a string so long as it contains simple ${property:xxx} type syntax that can be resolved entirely from
     * the template library.
     * @param templateString
     * @return the expanded version of that string
     */
    public static final String replace(String templateString)
    {
    	User nullUser = null;
    	Transaction nullTran = null;
    	DocumentVersion nullDocVersion = null;
    	LinkedList<FromToSpec> nullReplacementSpecs = null;
    	
    	return replace(templateString,nullUser,nullTran,nullDocVersion,nullReplacementSpecs);
    }
    
    // ******************** URL ENCODED versions for generating LINKs with substitutions ******************************
    /**
     * This will replace the template string using data stored in the specified transaction, document version and replacement specs.
     * The process is that we first attempt to do all of the fixed replacement specs first, then the ${} template replacements, and we
     * continue to do both until no more expansions take place.  When the text is replaced, it's URL encoded so it's suited for encoding URLs with name-value pairs.
     * @param templateString the required String to be expanded/replaced
     * @param transaction the optional Transaction to use for doing data substitutions
     * @param documentVersion the optional DocumentVersion to use for doing data substitutions
     * @param replacementSpecs the optional list of fixed replacements to do
     * @return
     */
    public static String replaceUrlEncoded(String urlTemplateString, User user, Transaction transaction, DocumentVersion documentVersion, List<FromToSpec> replacementSpecs)
    {
    	if ( EsfString.isBlank(urlTemplateString) )
    		return urlTemplateString;
    	
    	String priorToReplace;
    	String replacedString = urlTemplateString;
    	do
    	{
    		priorToReplace = replacedString;
    		if ( replacementSpecs != null )
    			replacedString = _replaceUrlEncodedStrings(replacedString, replacementSpecs);
    		replacedString = _substituteUrlEncodedFieldSpecs(replacedString, user, transaction, documentVersion);
    	} while( ! priorToReplace.equals(replacedString) );
    	
    	return replacedString;
    }
    
    public static final String replaceUrlEncoded(String urlTemplateString, User user, Transaction transaction, DocumentVersion documentVersion)
    {
    	LinkedList<FromToSpec> nullReplacementSpecs = null;
    	return replaceUrlEncoded(urlTemplateString,user,transaction,documentVersion,nullReplacementSpecs);
    }
    

}