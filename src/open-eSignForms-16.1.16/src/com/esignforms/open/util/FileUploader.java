// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.TreeMap;
import java.util.zip.GZIPOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.ServletException;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.esignforms.open.Application;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.Errors;
import com.esignforms.open.util.ServletUtil;

/**
 * Handles the uploading files type of request FORMs with enctype="multipart/form-data".
 *
 * @author Yozons Inc.
 */
public class FileUploader
{
    public class UploadAndStoreFileInfo
    {
        public String paramName   = null;
        public String fileName    = null;
        public String contentType = null;
        public File   file        = null;
    }
    
    public class UploadToBufferInfo
    {
        public String paramName        = null;
        public String fileName         = null;
        public String contentType      = null;
        public long   uncompressedSize = 0;
        public byte[] fileData         = null; // this is GZIP compressed
    }
    
    private static final int DEFAULT_WRITE_TO_DISK_THRESHOLD_KB = 500;
    
    protected Application        app = Application.getInstance();
    protected HttpServletRequest request;
    
	protected int       maxPostSize;
    protected int       writeToDiskThreshold;
    protected String    tempDir;
	protected boolean   allowOverwrite = false;
	
	// We store these for retrieving parameters from the HTTP requests.
    protected TreeMap<String,ArrayList<String>> parameters = null;
	
	
    /**
     * Uses System Data property fileUploadMaxPostMB (defaults to 10MB if missing) and 
     * fileUploadMaxRamFileKb (default to 500KB if missing), and tempFileUploadDir.
     * @param req the multipart HttpServletRequest containing the uploaded file.
     */
   public FileUploader(HttpServletRequest req)
    {
        this.request = req;
        setMaxPostSizeMb(app.getUploadFileMaxMB());
        setWriteToDiskThresholdKb(DEFAULT_WRITE_TO_DISK_THRESHOLD_KB);
		// TODO: Allow to be tunable
        //tempDir = app.getSystemData().getValueByName("tempFileUploadDir",null);
        tempDir = null;
    }
    
   /**
    * @param req the multipart HttpServletRequest containing the uploaded file.
    * @param maxPostSizeMb the int max POST size in megabytes.
    * @param writeToDiskThresholdKb the int threshold so that uploaded files bigger than this are written to disk rather than kept in ram
    */
	public FileUploader(HttpServletRequest req,int maxPostSizeMb, int writeToDiskThresholdKb)
	{
        this.request = req;
	    setMaxPostSizeMb(maxPostSizeMb);
        setWriteToDiskThresholdKb(writeToDiskThresholdKb);
		// TODO: Allow to be tunable
        //tempDir = app.getSystemData().getValueByName("tempFileUploadDir",null);
        tempDir = null;
	}
	
	public int getMaxPostSize()
	{
	    return maxPostSize;
	}
	public void setMaxPostSize(int size)
	{
        int systemMax = (int)app.getUploadFileMaxBytes();
        maxPostSize = ( systemMax > 0 && size > systemMax ) ? systemMax : size;
	}
    public void setMaxPostSizeMb(int sizeMb)
    {
        setMaxPostSize(sizeMb * Literals.MB);
    }
    
    public int getWriteToDiskThreshold()
    {
        return writeToDiskThreshold;
    }
    public void setWriteToDiskThreshold(int size)
    {
    	int systemMax = (int)app.getUploadFileMaxBytes();
        writeToDiskThreshold = ( systemMax > 0 && size > systemMax ) ? systemMax : size;
    }
    public void setWriteToDiskThresholdKb(int sizeKb)
    {
        setWriteToDiskThreshold(sizeKb * Literals.MB);
    }
    
    public String getTempDir()
    {
        return tempDir;
    }
    public void setTempDir(String v)
    {
        tempDir = v;
    }
    public boolean hasTempDir()
    {
        return ! EsfString.isBlank(tempDir);
    }
	
	public boolean allowOverwrite()
	{
	    return allowOverwrite;
	}
	public void setAllowOverwrite(boolean v)
	{
	    allowOverwrite = v;
	}
	
    public final String getParam(String name)
        throws ServletException
    {
        return getParam(name,true);
    }
    
    public final String getParam(String name, boolean isRequired)
        throws ServletException
    {
        String pv = getParameter(name);
        if ( pv == null )
        {
            if ( isRequired )
                throw new ServletException("Missing required parameter: " + name);
            return null;
        }
        
        return pv;
    }
    
    public final String getParam(String name, String def)
    {
        String pv = getParameter(name);
        if ( pv == null )
            return def;
        return pv;
    }
    
    public final String[] getMultiParam(String name)
    {
        String [] pv = getParameterValues(name);
        return pv;
    }
    
    public final boolean getCheckbox(String name)
    {
        String pv = getParameter(name);
        return pv != null && pv.length() > 0;
    }
    
    public final long getLongParam(String name, long def)
    {
        try
        {
            String val = getParam(name);
            return Long.parseLong(val);
        }
        catch( Exception e )
        {
            return def;
        }
    }
    
    public final int getIntParam(String name, int def)
    {
        try
        {
            String val = getParam(name);
            return Integer.parseInt(val);
        }
        catch( Exception e )
        {
            return def;
        }
    }
    
    
    public final short getShortParam(String name, short def)
    {
        try
        {
            String val = getParam(name);
            return Short.parseShort(val);
        }
        catch( Exception e )
        {
            return def;
        }
    }
    
    /**
     * Used with the SelectDate tag <esf:selectDate/> in which the date string is in MM/DD/YYYY format.
     * @param name the String name of the HTML field used with the selectDate tag.
     * @return the string date in mm/dd/yyyy format (with "mm" returned if no month, "dd" if no day, and "yyyy" if no year params found.
     */
    public final String getSelectDateString(String name)
    {
        String mm   = getParam(name+"MM","mm");
        String dd   = getParam(name+"DD","dd");
        String yyyy = getParam(name+"YYYY","yyyy");
        return mm + "/" + dd + "/" + yyyy;
    }
    public final String getSelectDateString(String name, String def)
    {
        String mm   = getParam(name+"MM",null);
        String dd   = getParam(name+"DD",null);
        String yyyy = getParam(name+"YYYY",null);
        if ( mm == null && dd == null && yyyy == null )
        	return def;
        return getSelectDateString(name);
    }
    
    public final String[] getMultiSelectDateString(String name)
    {
        String[] mm   = getMultiParam(name+"MM");
        String[] dd   = getMultiParam(name+"DD");
        String[] yyyy = getMultiParam(name+"YYYY");
        if ( mm == null || dd == null || yyyy == null || mm.length != yyyy.length || mm.length != dd.length )
        	return new String[0];
    	for( int i=0; i < mm.length; ++i )
    	{
    		if ( mm[i] == null )
    			mm[i] = "mm";
    	}
    	for( int i=0; i < dd.length; ++i )
    	{
    		if ( dd[i] == null )
    			dd[i] = "dd";
    	}
    	for( int i=0; i < yyyy.length; ++i )
    	{
    		if ( yyyy[i] == null )
    			yyyy[i] = "yyyy";
    	}
    	String[] mmddyyyy = new String[mm.length];
    	for( int i=0; i < mm.length; ++i )
    		mmddyyyy[i] = mm[i] + "/" + dd[i] + "/" + yyyy[i];
        return mmddyyyy;
    }
    


    /**
     * Used with the SelectMMYYYYDate tag <esf:selectMMYYYYDate/> in which the date string is in MM/YYYY format.
     * @param name the String name of the HTML field used with the selectDate tag.
     * @return the string date in mm/yyyy format (with "mm" returned if no month, and "yyyy" if no year params found.
     */
    public final String getSelectDateMMYYYYString(String name)
    {
        String mm   = getParam(name+"MM","mm");
        String yyyy = getParam(name+"YYYY","yyyy");
        return mm + "/" + yyyy;
    }
    public final String getSelectDateMMYYYYString(String name, String def)
    {
        String mm   = getParam(name+"MM",null);
        String yyyy = getParam(name+"YYYY",null);
        if ( mm == null && yyyy == null )
        	return def;
        return getSelectDateMMYYYYString(name);
    }
    
    public final String[] getMultiSelectDateMMYYYYString(String name)
    {
        String[] mm   = getMultiParam(name+"MM");
        String[] yyyy = getMultiParam(name+"YYYY");
        if ( mm == null || yyyy == null || mm.length != yyyy.length )
        	return new String[0];
    	for( int i=0; i < mm.length; ++i )
    	{
    		if ( mm[i] == null )
    			mm[i] = "mm";
    	}
    	for( int i=0; i < yyyy.length; ++i )
    	{
    		if ( yyyy[i] == null )
    			yyyy[i] = "yyyy";
    	}
    	String[] mmyyyy = new String[mm.length];
    	for( int i=0; i < mm.length; ++i )
    		mmyyyy[i] = mm[i] + "/" + yyyy[i];
        return mmyyyy;
    }
    
    protected String getParameter(String name) 
    {
        if ( parameters == null )
            return null;
        
        try 
        {
            ArrayList<String> values = parameters.get(name);
            if ( values == null || values.size() == 0 )
                return null;
            return values.get(0);
        }
        catch (Exception e) { return null; }
    }
    
    protected String[] getParameterValues(String name)
    {
        if ( parameters == null )
            return null;
        try 
        {
            ArrayList<String> values = parameters.get(name);
            if ( values == null || values.size() == 0 )
                return null;
            String[] valuesArray = new String[values.size()];
            values.toArray(valuesArray);
            return valuesArray;
        }
        catch (Exception e) { return null; }
    }

    protected void clear()
    {
        // We'll create this list only when doing the 'in memory' upload
        if ( parameters != null )
            parameters.clear();
            
	    parameters = null;
    }
    
	
	protected UploadAndStoreFileInfo[] makeUploadAndStoreFileInfoArray(ArrayList<UploadAndStoreFileInfo> list)
	{
	    if ( list == null )
	        return new UploadAndStoreFileInfo[0];
	        
	    UploadAndStoreFileInfo[] a = new UploadAndStoreFileInfo[list.size()];
	    list.toArray(a);
	    return a;
	}
	
	protected UploadToBufferInfo[] makeUploadToBufferInfoArray(ArrayList<UploadToBufferInfo> list)
	{
	    if ( list == null )
	        return new UploadToBufferInfo[0];
	        
	    UploadToBufferInfo[] a = new UploadToBufferInfo[list.size()];
	    list.toArray(a);
	    return a;
	}
	
	
	/**
	 * Handles the uploading and storage of a file to the repository.
	 * @param storeInPath the File of the directory where the file should be stored
	 * @param errors the Errors that will be updated if there are problems. 
	 * @return UploadAndStoreFileInfo array containing the uploaded files, or null if the request could not
	 *         even be processed.  If the array is empty, then no files were uploaded. It's also possible to
	 *         get a list with one element in side, but the File is null, indicating that a file was attempted
	 *         to be uploaded but it failed for various reasons, mostly to the fact that it couldn't be
	 *         written successfully to 'storeInPath' with 'storeAsName'.
	 */
	public UploadAndStoreFileInfo[] uploadAndStoreFile(File storeInPath/*, Errors errors*/)
	{
	    clear();
	    
        try
        {
            // We'll store the uploading file in a special path called uploading in the storing path.
            File uploadingPath = new File(storeInPath,"uploading");
            
            if ( ! uploadingPath.exists() )
            {
                if ( ! uploadingPath.mkdir() )
                {
                    //errors.addError("The service is currently unavailable. Please try again later...");
                    app.err("FileUploader.uploadAndStoreFile() - Failed to create the repository location: " +
                             uploadingPath.getAbsolutePath());
                    return null;
                }
            }
            
            DiskFileItemFactory factory = new DiskFileItemFactory();
            
            factory.setSizeThreshold(getWriteToDiskThreshold());
            if ( hasTempDir() )
                factory.setRepository(uploadingPath);
            
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax(getMaxPostSize());
            if ( upload.getHeaderEncoding() == null )
            	upload.setHeaderEncoding(EsfString.CHARSET_UTF_8);
            
            parameters = new TreeMap<String,ArrayList<String>>();
            
            List<?> items = upload.parseRequest(request);
            
            ArrayList<UploadAndStoreFileInfo> uploadedInfo = new ArrayList<UploadAndStoreFileInfo>();
            
            // Process the uploaded items
            ListIterator<?> iter = items.listIterator();
            while ( iter.hasNext() ) 
            {
                FileItem item = (FileItem)iter.next();

                if ( item.isFormField() ) 
                {
                    // It's a parameter part, add it to the vector of values
                    String name  = ServletUtil.cleanInput(item.getFieldName().trim());
                    String value = ServletUtil.cleanInput(item.getString(EsfString.CHARSET_UTF_8).trim());
                    ArrayList<String> existingValues = parameters.get(name);
                    if (existingValues == null) 
                    {
                        existingValues = new ArrayList<String>();
                        parameters.put(name, existingValues);
                    }
                    existingValues.add(value);
                }
                else
                {
                    UploadAndStoreFileInfo info = new UploadAndStoreFileInfo();
                    uploadedInfo.add(info);
                
                    info.paramName          = ServletUtil.cleanInput(item.getFieldName().trim());
                    info.fileName           = ServletUtil.cleanInput(item.getName().trim());
                    // The filename may contain a full path.  Cut to just the filename.
                    int slash = Math.max(info.fileName.lastIndexOf('/'), info.fileName.lastIndexOf('\\'));
                    if (slash > -1)
                        info.fileName = info.fileName.substring(slash + 1);  // past last slash
                    // Mac/Safari sends no content type so it's null if there's a file param but no file is uploaded
                    info.contentType 		= ( item.getContentType() == null ) ? app.getContentTypeByFileName(info.fileName) : ServletUtil.cleanInput(item.getContentType().trim());
                    info.file               = new File(uploadingPath,info.fileName);
                    if ( info.file.exists() )
                    {
                        if ( allowOverwrite )
                        {
                            if ( ! info.file.delete() )
                            {
                                //errors.addError("The file you uploaded could not overwrite an existing file with the same name.");
                                app.warning("FileUploader.uploadAndStoreFile() - Could not delete existing file: " +
                                             info.file.getAbsolutePath());
                                info.file = null;
                                item.delete();
                                continue;
                            }
                        }
                        else
                        {
                            //errors.addError("The file already exists and cannot be overwritten.");
                            info.file = null;
                            item.delete();
                            continue;
                        }
                    }
                    
                    item.write(info.file);
                }
                
                item.delete();
            }
    
            // We were successful.
            return makeUploadAndStoreFileInfoArray(uploadedInfo);
        }
        catch( SizeLimitExceededException e )
        {
            //errors.addError("The file(s) could not be uploaded because they were too big.");
            app.except(e,"FileUploader.uploadAndStoreFile()");
        }
        catch( FileUploadException e )
        {
            //errors.addError("The file(s) could not be uploaded.");
            app.except(e,"FileUploader.uploadAndStoreFile()");
        }
        catch( Exception e )
        {
            //errors.addError("The file(s) could not be uploaded.");
            app.except(e,"FileUploader.uploadAndStoreFile()");
        }
        
	    clear();
        return null;
    }
    
	/**
	 * Handles the uploading and storage of a file into a memory buffer.
	 * @param errors the Errors that will be updated if there are problems. 
	 * @return UploadToBufferInfo array containing the uploaded files, or null if the request could not
	 *         even be processed.  If the array is empty, then no files were uploaded. It's also possible to
	 *         get a list with one element in side, but the byte array is null, indicating that a file was attempted
	 *         to be uploaded but it failed for various reasons, mostly to the fact that it couldn't be
	 *         written successfully read.
	 */
	public UploadToBufferInfo[] uploadToBuffer(Errors errors)
	{
	    clear();
	    
        try
        {
            int maxBufferLength = request.getContentLength();
            
            DiskFileItemFactory factory = new DiskFileItemFactory();            
            factory.setSizeThreshold(getWriteToDiskThreshold());
            if ( hasTempDir() )
                factory.setRepository(new File(getTempDir()));
            
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax(getMaxPostSize());
            if ( upload.getHeaderEncoding() == null )
            	upload.setHeaderEncoding(EsfString.CHARSET_UTF_8);
            
	        parameters = new TreeMap<String,ArrayList<String>>();
            
            List<?> items = upload.parseRequest(request);
            
            ArrayList<UploadToBufferInfo> uploadedInfo = new ArrayList<UploadToBufferInfo>();
            
            // Process the uploaded items
            ListIterator<?> iter = items.listIterator();
            while ( iter.hasNext() ) 
            {
                FileItem item = (FileItem)iter.next();

                if ( item.isFormField() ) 
                {
                    // It's a parameter part, add it to the vector of values
                    String name  = ServletUtil.cleanInput(item.getFieldName().trim());
                    String value = ServletUtil.cleanInput(item.getString(EsfString.CHARSET_UTF_8).trim());
                    ArrayList<String> existingValues = parameters.get(name);
                    if (existingValues == null) 
                    {
                        existingValues = new ArrayList<String>();
                        parameters.put(name, existingValues);
                    }
                    existingValues.add(value);
                }
                else
                {
                    UploadToBufferInfo info = new UploadToBufferInfo();
                    uploadedInfo.add(info);
                
                    info.paramName          = ServletUtil.cleanInput(item.getFieldName().trim());
                    info.fileName           = ServletUtil.cleanInput(item.getName().trim());
                    // The filename may contain a full path.  Cut to just the filename.
                    int slash = Math.max(info.fileName.lastIndexOf('/'), info.fileName.lastIndexOf('\\'));
                    if (slash > -1)
                        info.fileName = info.fileName.substring(slash + 1);  // past last slash
                    // Mac/Safari sends no content type so it's null if there's a file param but no file is uploaded
                    info.contentType 		= ( item.getContentType() == null ) ? app.getContentTypeByFileName(info.fileName) : ServletUtil.cleanInput(item.getContentType().trim());
                    byte[] uncompressedData = item.get();
                    info.uncompressedSize   = uncompressedData.length;

                    
                    // The part actually contained a file, and we'll write it to our buffer
                    // in a compressed format.
                    ByteArrayOutputStream bos = new ByteArrayOutputStream(maxBufferLength);
                    GZIPOutputStream      gos = new GZIPOutputStream(bos);
                    gos.write(uncompressedData);
                    gos.finish();
                    gos.flush();
                    info.fileData = bos.toByteArray();
                    gos.close();
                }
                
                item.delete();
            }
	    
            // We were successful.
            return makeUploadToBufferInfoArray(uploadedInfo);
        }
        catch( java.io.IOException e )
        {
            errors.addError("The file(s) could not be uploaded.");
            app.except(e,"FileUploader.uploadToBuffer()");
        }
        catch( SizeLimitExceededException e )
        {
            errors.addError("The file(s) could not be uploaded because they were too big.");
            app.except(e,"FileUploader.uploadToBuffer()");
        }
        catch( FileUploadException e )
        {
            errors.addError("The file(s) could not be uploaded.");
            app.except(e,"FileUploader.uploadToBuffer()");
        }
        
	    clear();
        return null;
    }
    
    /**
     * Handles the uploading and storage of a file into a memory buffer without compressing the data.
     * @param errors the Errors that will be updated if there are problems. 
     * @return UploadToBufferInfo array containing the uploaded files, or null if the request could not
     *         even be processed.  If the array is empty, then no files were uploaded. It's also possible to
     *         get a list with one element in side, but the byte array is null, indicating that a file was attempted
     *         to be uploaded but it failed for various reasons, mostly to the fact that it couldn't be
     *         written successfully read.
     */
    public UploadToBufferInfo[] uploadToBufferUncompressed(Errors errors)
    {
        clear();
        
        try
        {
            DiskFileItemFactory factory = new DiskFileItemFactory();            
            factory.setSizeThreshold(getWriteToDiskThreshold());
            if ( hasTempDir() )
                factory.setRepository(new File(getTempDir()));
            
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setSizeMax(getMaxPostSize());
            if ( upload.getHeaderEncoding() == null )
            	upload.setHeaderEncoding(EsfString.CHARSET_UTF_8);
            
            parameters = new TreeMap<String,ArrayList<String>>();
            
            List<?> items = upload.parseRequest(request); 
            
            ArrayList<UploadToBufferInfo> uploadedInfo = new ArrayList<UploadToBufferInfo>();
            
            // Process the uploaded items
            ListIterator<?> iter = items.listIterator();
            while ( iter.hasNext() ) 
            {
                FileItem item = (FileItem)iter.next();

                if ( item.isFormField() ) 
                {
                    // It's a parameter part, add it to the vector of values
                    String name  = ServletUtil.cleanInput(item.getFieldName().trim());
                    String value = ServletUtil.cleanInput(item.getString(EsfString.CHARSET_UTF_8).trim());
                    ArrayList<String> existingValues = parameters.get(name);
                    if (existingValues == null) 
                    {
                        existingValues = new ArrayList<String>();
                        parameters.put(name, existingValues);
                    }
                    existingValues.add(value);
                }
                else
                {
                    UploadToBufferInfo info = new UploadToBufferInfo();
                    uploadedInfo.add(info);
                
                    info.paramName          = ServletUtil.cleanInput(item.getFieldName().trim());
                    info.fileName           = ServletUtil.cleanInput(item.getName().trim());
                    // The filename may contain a full path.  Cut to just the filename.
                    int slash = Math.max(info.fileName.lastIndexOf('/'), info.fileName.lastIndexOf('\\'));
                    if (slash > -1)
                        info.fileName = info.fileName.substring(slash + 1);  // past last slash
                    // Mac/Safari sends no content type so it's null if there's a file param but no file is uploaded
                    info.contentType 		= ( item.getContentType() == null ) ? app.getContentTypeByFileName(info.fileName) : ServletUtil.cleanInput(item.getContentType().trim());
                    info.fileData           = item.get();
                    info.uncompressedSize   = info.fileData.length;
                }
                
                item.delete();
            }
        
            // We were successful.
            return makeUploadToBufferInfoArray(uploadedInfo);
        }
        catch( SizeLimitExceededException e )
        {
            errors.addError("The file(s) could not be uploaded because they were too big.");
            app.except(e,"FileUploader.uploadToBufferUncompressed()");
        }
        catch( FileUploadException e )
        {
            errors.addError("The file(s) could not be uploaded.");
            app.except(e,"FileUploader.uploadToBufferUncompressed()");
        }
        catch( UnsupportedEncodingException e )
        {
            errors.addError("The file(s) could not be uploaded because it was not UTF-8 encoded.");
            app.except(e,"FileUploader.uploadToBufferUncompressed()");
        }
        
        clear();
        return null;
    }
    
}