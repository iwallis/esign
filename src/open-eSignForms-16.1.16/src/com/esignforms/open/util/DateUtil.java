// Copyright (C) 2009-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.util;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.esignforms.open.Application;
import com.esignforms.open.config.Literals;
import com.esignforms.open.data.EsfDate;
import com.esignforms.open.data.EsfString;
import com.esignforms.open.Errors;

/**
 * Various date routines to assist EsfDate and EsfDateTime.
 *
 * @author Yozons, Inc.
 */
public class DateUtil
{
	private static TimeZone utcTimeZone		= TimeZone.getTimeZone("UTC");
	private static TimeZone defaultTimeZone = TimeZone.getTimeZone("UTC");
	private static Application app;
	
    // These are used to store the various timezones ids.
    private static String[] availableTzIds       = null;
    private static String[] commonTzNames        = null;
    private static Date     nextGetCommonTzNames = null; // we get at most once a day

	
	private DateUtil() {}
	
	public final static void initialize(Application useApp) { app = useApp; } // Initializes static library with application
	
    public final static java.sql.Timestamp getCurrentSqlTimestamp()
    {
        return new java.sql.Timestamp( System.currentTimeMillis() );
    }

	public final static TimeZone getUtcTimeZone()
	{
		return utcTimeZone;
	}
	
	/**
	 * Fixes a few commonly used ones that we know about (mostly for the USA as of 6/11/2012)
	 * @param tzString the timezone string specification
	 * @return the TimeZone object
	 */
	public static String fixupTimeZone(String tzString)
	{
		if ( "EST".equals(tzString) || "EDT".equals(tzString) )
			return "EST5EDT";
		if ( "CST".equals(tzString) || "CDT".equals(tzString) )
			return "CST6CDT";
		if ( "MST".equals(tzString) || "MDT".equals(tzString) )
			return "MST7MDT";
		if ( "PST".equals(tzString) || "PDT".equals(tzString) )
			return "PST8PDT";
		return tzString;
	}
	
	
	public final static TimeZone getDefaultTimeZone()
	{
		return defaultTimeZone;
	}
	public static void setDefaultTimeZone( String tzString )
	{
		defaultTimeZone = TimeZone.getTimeZone(tzString);
	}
	
	/**
	 * This routine normalizes the a date object so that it's set to 00:00:00.000 UTC.
	 * @param v the Date that may have a time component
	 * @return the Date with the time part normalized
	 */
	public static Date normalizeTimeToZeroUTC(Date v)
	{
		return normalizeTimeToZero(v,getDefaultTimeZone(),utcTimeZone);
	}

	/**
	 * This routine normalizes the a date object so that it's set to 00:00:00.000 for the specified timezone.
	 * @param v the Date that may have a time component
	 * @param fromTz the TimeZone to use when assuming what the date represents
	 * @param toTz the TimeZone to use to normalize to
	 * @return the Date with the time part normalized
	 */
	public static Date normalizeTimeToZero(Date v, TimeZone fromTz, TimeZone toTz)
	{
        Calendar cal = Calendar.getInstance(fromTz);
        cal.setTime(v);
        cal.set( Calendar.HOUR_OF_DAY, 0 );
        cal.set( Calendar.MINUTE, 0 );
        cal.set( Calendar.SECOND, 0 );
        cal.set( Calendar.MILLISECOND, 0 );

        if ( ! fromTz.hasSameRules(toTz) )
        {
            cal.setTimeZone(toTz);
            cal.set( Calendar.HOUR_OF_DAY, 0 );
            cal.set( Calendar.MINUTE, 0 );
            cal.set( Calendar.SECOND, 0 );
            cal.set( Calendar.MILLISECOND, 0 );
        }

        return cal.getTime();
	}
	public static Date normalizeTimeToZero(Date v, TimeZone tz)
	{
		return normalizeTimeToZero(v,tz,tz);
	}
	public static Date normalizeTimeToZero(Date v)
	{
		return normalizeTimeToZero(v,getDefaultTimeZone());
	}

	
	/**
	 * This routine normalizes the a date object so that it's set to 12:00:00.000 UTC.
	 * @param v the Date that may have a time component
	 * @return the Date with the time part normalized
	 */
	public static Date normalizeTimeToNoonUTC(Date v)
	{
		return normalizeTimeToNoon(v,getDefaultTimeZone(),utcTimeZone);
	}

	/**
	 * This routine normalizes the a date object so that it's set to 12:00:00.000 for the specified timezone.
	 * @param v the Date that may have a time component
	 * @param fromTz the TimeZone to use when assuming what the date represents
	 * @param toTz the TimeZone to use to normalize to
	 * @return the Date with the time part normalized
	 */
	public static Date normalizeTimeToNoon(Date v, TimeZone fromTz, TimeZone toTz)
	{
        Calendar cal = Calendar.getInstance(fromTz);
        cal.setTime(v);
        cal.set( Calendar.HOUR_OF_DAY, 12 );
        cal.set( Calendar.MINUTE, 0 );
        cal.set( Calendar.SECOND, 0 );
        cal.set( Calendar.MILLISECOND, 0 );

        if ( ! fromTz.hasSameRules(toTz) )
        {
	        cal.setTimeZone(toTz);
	        cal.set( Calendar.HOUR_OF_DAY, 12 );
	        cal.set( Calendar.MINUTE, 0 );
	        cal.set( Calendar.SECOND, 0 );
	        cal.set( Calendar.MILLISECOND, 0 );
        }

        return cal.getTime();
	}
	public static Date normalizeTimeToNoon(Date v, TimeZone tz)
	{
		return normalizeTimeToNoon(v,tz,tz);
	}
	public static Date normalizeTimeToNoon(Date v)
	{
		return normalizeTimeToNoon(v,getDefaultTimeZone());
	}
	
	/**
	 * This routine normalizes the a date object so that it's set to 23:59:59.999 UTC.
	 * @param v the Date that may have a time component
	 * @return the Date with the time part normalized
	 */
	public static Date normalizeTimeToEndOfDayUTC(Date v)
	{
		return normalizeTimeToZero(v,getDefaultTimeZone(),utcTimeZone);
	}

	/**
	 * This routine normalizes the a date object so that it's set to 23:59:59.999 for the specified timezone.
	 * @param v the Date that may have a time component
	 * @param tz the TimeZone to use
	 * @return the Date with the time part normalized
	 */
	public static Date normalizeTimeToEndOfDay(Date v, TimeZone fromTz, TimeZone toTz)
	{
        Calendar cal = Calendar.getInstance(fromTz);
        cal.setTime(v);
        cal.set( Calendar.HOUR_OF_DAY, 23 );
        cal.set( Calendar.MINUTE, 59 );
        cal.set( Calendar.SECOND, 59 );
        cal.set( Calendar.MILLISECOND, 999 );
        
        if ( ! fromTz.hasSameRules(toTz) )
        {
	        cal.setTimeZone(toTz);
	        cal.set( Calendar.HOUR_OF_DAY, 23 );
	        cal.set( Calendar.MINUTE, 59 );
	        cal.set( Calendar.SECOND, 59 );
	        cal.set( Calendar.MILLISECOND, 999 );
        }

        return cal.getTime();
	}
	public static Date normalizeTimeToEndOfDay(Date v, TimeZone tz)
	{
		return normalizeTimeToEndOfDay(v,tz,tz);
	}
	public static Date normalizeTimeToEndOfDay(Date v)
	{
		return normalizeTimeToEndOfDay(v,getDefaultTimeZone());
	}
	
    /**
     * Converts seconds to milliseconds.
     * @param seconds the number of seconds
     * @return the number of milliseconds representing the supplied seconds
     */
    public final static long secondsToMillis( long seconds )
    {
        return seconds * 1000;
    }
    
    
    /**
     * Converts minutes to milliseconds.
     * @param minutes the number of minutes
     * @return the number of milliseconds representing the supplied minutes
     */
    public final static long minutesToMillis( long minutes )
    {
        return minutes * 1000 * 60;
    }
    
    
    /**
     * Converts milliseconds to seconds, always rounded down.
     * @param millis the number of milliseconds
     * @return the number of seconds representing the supplied milliseconds
     */
    public final static long millisToSeconds( long millis )
    {
        return millis / 1000;
    }
    
    /**
     * Converts milliseconds to minutes, always rounded down.
     * @param millis the number of milliseconds
     * @return the number of minutes representing the supplied milliseconds
     */
    public final static long millisToMinutes( long millis )
    {
        return (millis / 1000) / 60;
    }
    
    /**
     * Returns the year, month and day as numbers in the array in this order.
     * @param d the Date to use
     * @return an integer array with [0]=year, [1]=month, [2]=day
     */
    public static int[] getYMDNumbers( Date d )
    {
        int[] values = new int[3];
        
        Calendar cal = Calendar.getInstance(utcTimeZone);
        cal.setTime(d);
        
        values[0] = cal.get(Calendar.YEAR);
        values[1] = cal.get(Calendar.MONTH)+1;
        values[2] = cal.get(Calendar.DAY_OF_MONTH);
        
        return values;
    }
    
    /**
     * Returns the 4-digit year, 2-digit month and 2-digit day as Strings in the array in this order.
     * @param d the Date to use
     * @return a String array with [0]=YYYY, [1]=MM, [2]=DD
     */
    public static String[] getYMDStrings( Date d )
    {
        String[] values = new String[3];
        
        Calendar cal = Calendar.getInstance(utcTimeZone);
        cal.setTime(d);
        
        values[0] = Integer.toString(cal.get(Calendar.YEAR));
        values[1] = app.make2digits(cal.get(Calendar.MONTH)+1);
        values[2] = app.make2digits(cal.get(Calendar.DAY_OF_MONTH));
        
        return values;
    }
    
    /**
     * Calculates the Date that is numMinutes from the specified date.
     * @param numMinutes the number of minutes from the date to calculate.  Can be positive or negative.
     * @param d the Date to calculate from
     * @return the Date that is numMinutes from 'd'.
     */
    public static Date dateTimeIntervalFromDate( int numUnits, String intervalUnit, Date d )
    {
    	if ( Literals.TIME_INTERVAL_UNIT_NOW.equals(intervalUnit) ) // really, this shouldn't be used either, but we'll send you the current date
    		return new Date();
    	
        Calendar cal = Calendar.getInstance(utcTimeZone);
        cal.setTime( d );
        
        // For weekeday, we actually have to count them
        if ( Literals.TIME_INTERVAL_UNIT_WEEKDAY.equals(intervalUnit) )
        {
        	while( numUnits > 0 )
        	{
        		cal.add(Calendar.DAY_OF_MONTH,1);
            	int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
                if ( dayOfWeek >= Calendar.MONDAY  &&  dayOfWeek <= Calendar.FRIDAY )
                	--numUnits;
        	}
        	return cal.getTime();
        }

        int calendarUnit;
        
        if ( Literals.TIME_INTERVAL_UNIT_FOREVER.equals(intervalUnit) ) // really, this shouldn't be used, but we'll fake it by just treating like 1000 years
        {
        	calendarUnit = Calendar.YEAR;
        	numUnits = 1000;
        }
        else if ( Literals.TIME_INTERVAL_UNIT_YEAR.equals(intervalUnit) )
        	calendarUnit = Calendar.YEAR;
        else if ( Literals.TIME_INTERVAL_UNIT_MONTH.equals(intervalUnit) )
        	calendarUnit = Calendar.MONTH;
        else if ( Literals.TIME_INTERVAL_UNIT_WEEK.equals(intervalUnit) )
        {
        	calendarUnit = Calendar.DAY_OF_MONTH;
        	numUnits *= 7;
        }
        else if ( Literals.TIME_INTERVAL_UNIT_DAY.equals(intervalUnit) )
        	calendarUnit = Calendar.DAY_OF_MONTH;
        else if ( Literals.TIME_INTERVAL_UNIT_HOUR.equals(intervalUnit) )
        	calendarUnit = Calendar.HOUR;
        else 
        	calendarUnit = Calendar.MINUTE;
        cal.add( calendarUnit, numUnits );
        return cal.getTime();
    }

    
    /**
     * Calculates the Date that is numYears from the specified date.
     * @param numYears the number of yearss from 'd' to calculate.  Can be positive or negative.
     * @param d the Date to calculate from
     * @return the Date that is numYears from 'd'.
     */
    public static Date dateYearsFromDate( int numYears, Date d )
    {
        Calendar cal = Calendar.getInstance(utcTimeZone);
        cal.setTime( d );
        cal.add( Calendar.YEAR, numYears );
        return cal.getTime();
    }

    /**
     * Calculates the Date that is numMonths from the specified date.
     * @param numMonths the number of months from 'd' to calculate.  Can be positive or negative.
     * @param d the Date to calculate from
     * @return the Date that is numMonths from 'd'.
     */
    public static Date dateMonthsFromDate( int numMonths, Date d )
    {
        Calendar cal = Calendar.getInstance(utcTimeZone);
        cal.setTime( d );
        cal.add( Calendar.MONTH, numMonths );
        return cal.getTime();
    }

    /**
     * Calculates the Date that is numDays from the specified date.
     * @param numDays the number of days from 'd' to calculate.  Can be positive or negative.
     * @param d the Date to calculate from
     * @return the Date that is numDays from 'd'.
     */
    public static Date dateDaysFromDate( int numDays, Date d )
    {
        Calendar cal = Calendar.getInstance(utcTimeZone);
        cal.setTime( d );
        cal.add( Calendar.DAY_OF_MONTH, numDays );
        return cal.getTime();
    }

    /**
     * Calculates the Date that is numDays from now.
     * @param numDays the number of days from now to calculate.  Can be positive or negative.
     * @return the Date that is numDays from now.
     */
    public final static Date dateDaysFromNow( int numDays )
    {
        return dateDaysFromDate( numDays, new Date() );
    }
    
    /**
     * Calculates the Date that is numMinutes from the specified date.
     * @param numMinutes the number of minutes from the date to calculate.  Can be positive or negative.
     * @param d the Date to calculate from
     * @return the Date that is numMinutes from 'd'.
     */
    public static Date dateMinutesFromDate( int numMinutes, Date d )
    {
        Calendar cal = Calendar.getInstance(utcTimeZone);
        cal.setTime( d );
        cal.add( Calendar.MINUTE, numMinutes );
        return cal.getTime();
    }

    /**
     * Calculates the Date that is numMinutes from now.
     * @param numMinutes the number of minutes from now to calculate.  Can be positive or negative.
     * @return the Date that is numMinutes from now.
     */
    public final static Date dateMinutesFromNow( int numMinutes )
    {
    	return dateMinutesFromDate( numMinutes, new Date() );
    }
    
    
    /**
     * Calculates the Date that is numHours from the specified date.
     * @param numHours the number of hours from the date to calculate.  Can be positive or negative.
     * @param d the Date to calculate from
     * @return the Date that is numHours from 'd'.
     */
    public static Date dateHoursFromDate( int numHours, Date d )
    {
        Calendar cal = Calendar.getInstance(utcTimeZone);
        cal.setTime( d );
        cal.add( Calendar.HOUR, numHours );
        return cal.getTime();
    }
    /**
     * Calculates the Date that is numHours from now.
     * @param numHours the number of hours from the date to calculate.  Can be positive or negative.
     * @return the Date that is numHours from 'd'.
     */
    public final static Date dateHoursFromNow( int numHours )
    {
    	return dateHoursFromDate( numHours, new Date() );
    }
    
    
    /**
     * Returns last day of the specified date's month.
     * @param d the Date to find the last day of its month.
     * @return the Date that represents the last day of its month
     */
    public static Date lastDayOfTheMonth( Date d )
    {
        Calendar lastDay = Calendar.getInstance(utcTimeZone);
        lastDay.setTime( d );
        lastDay.set( Calendar.DAY_OF_MONTH, 1 );
        lastDay.add( Calendar.MONTH, 1 );
        lastDay.add( Calendar.DAY_OF_MONTH, -1 );
        return lastDay.getTime();
    }
    public final static Date lastDayOfThisMonth()
    {
    	return lastDayOfTheMonth( new Date() );
    }

    /**
     * Returns first day of the specified month
     */
    public static Date firstDayOfTheMonth( Date d )
    {
        Calendar firstDay = Calendar.getInstance(utcTimeZone);
        firstDay.setTime( d );
        firstDay.set( Calendar.DAY_OF_MONTH, 1 );
        return firstDay.getTime();
    }
    public final static Date firstDayOfThisMonth()
    {
    	return firstDayOfTheMonth( new Date() );
    }
    
    /**
     * Returns first day of this week
     */
    public static Date firstDayOfTheWeek( Date d )
    {
        Calendar firstDay = Calendar.getInstance(defaultTimeZone,app.getDefaultLocale());
        
        int firstDayOfWeek = firstDay.getFirstDayOfWeek();  // either 1=sunday or 2=monday based on locale
        int currDayOfWeek  = firstDay.get(Calendar.DAY_OF_WEEK);
        
        int numDaysBack = ( currDayOfWeek >= firstDayOfWeek ) ? (currDayOfWeek-firstDayOfWeek) : 6;
        numDaysBack *= -1;
        
        firstDay.setTime( d );
        firstDay.add( Calendar.DATE, numDaysBack );
        
        return firstDay.getTime();
    }
    public static Date firstDayOfThisWeek()
    {
    	return firstDayOfTheWeek( new Date() );
    }

    /**
     * Calculates the next date that has the specified hours and minutes using the specified timezone.  Seconds and millis are set to zero.
     * @param hour the hour number 0-23
     * @param minute the minute of that hour, 0-59
     * @param timeZone the TimeZone to use
     * @return the Date that represents the next occurrence of the specified hour and minute in the specified timezone
     */
    public static Date nextDateForHourMinute( int hour, int minute, Date d, TimeZone tz )
    {
        Calendar next = Calendar.getInstance(tz);
        next.setTime(d);
        next.set( Calendar.HOUR_OF_DAY, hour );
        next.set( Calendar.MINUTE, minute );
        next.set( Calendar.SECOND, 0 );
        next.set( Calendar.MILLISECOND, 0 );
        
        if ( Calendar.getInstance(defaultTimeZone).before(next) )
            return next.getTime();
           
        // Have to wait until tomorrow!
        next.add( Calendar.DATE, 1 );
        
        return next.getTime();
    }
    /**
     * Calculates the next date that has the specified hours and minutes.  Seconds and millis are set to zero.
     * @param hour the hour number 0-23
     * @param minute the minute of that hour, 0-59
     * @return the Date that represents the next occurrence of the specified hour and minute.
     */
    public final static Date nextDateForHourMinute( int hour, int minute, Date d )
    {
    	return nextDateForHourMinute(hour,minute,d,defaultTimeZone);
    }
    public final static Date nextDateForThisHourMinute( int hour, int minute )
    {
    	return nextDateForHourMinute( hour, minute, new Date() );
    }
    
    /**
     * Calculates the next date with the specified day of the month, regardless if the date is already that day 
     * of the month.  That is, 9/1/2015 would become 10/1/2015 for dayOfMonth=1, and 9/2/2015 would become 10/1/2015
     * for dayOfMonth=1.
     * @param dayOfMonth the day of the month: 1-31
     * @param timeZone the TimeZone to use
     * @return the Date that represents the next occurrence of the specified day of the month
     */
    public static Date nextDateForDayOfMonth( int dayOfMonth, Date d, TimeZone tz )
    {
        Calendar next = Calendar.getInstance(tz);
        next.setTime(d);
        next.set( Calendar.DAY_OF_MONTH, dayOfMonth );
        next.add( Calendar.MONTH, 1 );
        
        return next.getTime();
    }
    /**
     * Calculates the next date for the specified day of the month unless the date passed in is that day of the month.
     * That is, 9/1/2015 would remain 9/1/2015 for dayOfMonth=1, and 9/2/2015 would become 10/1/2015 for dayOfMonth=1.
     * @param dayOfMonth the day of the month: 1-31
     * @param timeZone the TimeZone to use
     * @return the Date that represents the next occurrence of the specified days of the month (unless already is)
     */
    public static Date nextDateForDayOfMonthUnlessAlreadyIs( int dayOfMonth, Date d, TimeZone tz )
    {
        Calendar next = Calendar.getInstance(tz);
        next.setTime(d);
        
        if ( dayOfMonth == next.get(Calendar.DAY_OF_MONTH) )
        	return d;
        
        // Have to wait until next month for that day!
        next.set( Calendar.DAY_OF_MONTH, dayOfMonth );
        next.add( Calendar.MONTH, 1 );
        
        return next.getTime();
    }

    
    
    /**
     * Calculates the number of years between two dates (often used for age calculations).
     * Note this does NOT take into account the time of day, just year, month and day,
     * so from 1/1/2010 to 1/1/2011 is 1 year apart even if the 2010 date shows 10pm while the 2011 date is 10am.
     * @return the integer number of years between the two dates; 0 if the years are the same; negative if toDate is
     * before fromDate.
     * 
     */
    public static int yearsBetween(Date fromDate, Date toDate)
    {
    	Calendar fromCal = Calendar.getInstance();
    	fromCal.setTime(fromDate);
    	
    	Calendar toCal = Calendar.getInstance();
    	toCal.setTime(toDate);

    	int fromYear = fromCal.get(Calendar.YEAR); 
    	int toYear = toCal.get(Calendar.YEAR); 
    	int numYears = toYear - fromYear;
    	if ( numYears == 0 ) // If it's the same year, they are 0 years apart no matter what
    		return 0;

    	if ( numYears > 0 ) // normal, we'll have a > 0 number of years
    	{
        	int fromMonth = fromCal.get(Calendar.MONTH); 
        	int toMonth = toCal.get(Calendar.MONTH);
        	if ( toMonth < fromMonth )
        		--numYears;
        	else if ( toMonth == fromMonth )
        	{
            	int fromDay = fromCal.get(Calendar.DAY_OF_MONTH); 
            	int toDay = toCal.get(Calendar.DAY_OF_MONTH);
            	if ( toDay < fromDay )
            		--numYears;
        	}
    	}
    	else // we'll have a negative number of years
    	{
        	int fromMonth = fromCal.get(Calendar.MONTH); 
        	int toMonth = toCal.get(Calendar.MONTH);
        	if ( fromMonth < toMonth )
        		++numYears;
        	else if ( toMonth == fromMonth )
        	{
            	int fromDay = fromCal.get(Calendar.DAY_OF_MONTH); 
            	int toDay = toCal.get(Calendar.DAY_OF_MONTH);
            	if ( fromDay < toDay )
            		++numYears;
        	}
    	}
    	
    	return numYears;
    }
    
    
    /**
     * Returns true if the specified date is a weekday.
     * @param d the Date to check
     * @return true if 'd' is a weekday.
     */
    public static boolean isWeekDay(Date d)
    {
        Calendar cal = Calendar.getInstance(defaultTimeZone,app.getDefaultLocale());   
        cal.setTime( d );
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        return ( dayOfWeek >= Calendar.MONDAY  &&  dayOfWeek <= Calendar.FRIDAY );
    }
    public final static boolean isWeekDay()
    {
    	return isWeekDay( new Date() );
    }
    
    // Expects that the m and y are valid to calculate the 'd' value
    protected static int[] daysInMonth = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    protected static boolean isValidNumberOfDaysForMonth(int m, int d, int y)
    {
        if ( d < 1 )
            return false;
        int maxDaysInMonth = getNumberOfDaysForMonth(m, y);
        return d <= maxDaysInMonth;
    }
    public static int getNumberOfDaysForMonth(int m, int y)
    {
        if ( m == 2 )
        {
            int maxFebDays = ( ((y % 4 == 0) && ( (!(y % 100 == 0)) || (y % 400 == 0)) ) ? 29 : 28 );
            return maxFebDays;
        }
        if ( m < 1 ) // invalid month, assume January
        	m = 1;
        else if ( m > 12 ) // invalid month, assume December
        	m = 12;
        
        return daysInMonth[m];
    }
    
    
    public static boolean isValidMDYDate(String mdy)
    {
        return isValidMDYDate(mdy,null,null);
    }
    public static boolean isValidMDYDate(String mdy, Errors errors)
    {
        return isValidMDYDate(mdy,errors,null);
    }
    public static boolean isValidMDYDate(String mdy, Errors errors, String fieldLabel)
    {
        if ( EsfString.isBlank(mdy) || mdy.length() < 6 )  // min date: 1/1/02
        {
            if ( errors != null )
                errors.addError("The date is too short.",fieldLabel);
            return false;
        }
        
        if ( mdy.length() > 10 ) // max date: 01/01/2002
        {
            if ( errors != null )
                errors.addError("The date is too long. The expected format is mm/dd/yyyy.",fieldLabel);
            return false;
        }
        
        // mm/dd/yyyy finder
        java.util.regex.Matcher matcher = EsfDate.MDYPattern.matcher(mdy);
        if ( matcher == null || ! matcher.find() || matcher.groupCount() != 3 )
        {
            if ( errors != null )
                errors.addError("The date format is incorrect. The expected format is mm/dd/yyyy.",fieldLabel);
            return false;
        }
        
        String mm = matcher.group(1);
        String dd = matcher.group(2);
        String yyyy = matcher.group(3);
        
        int m = app.stringToInt(mm,1);
        if ( m < 1 || m > 12 )
        {
            if ( errors != null )
                errors.addError("The date has an invalid month. The expected format is mm/dd/yyyy.",fieldLabel);
            return false;
        }

        int d = app.stringToInt(dd,1);
        int y = app.stringToInt(yyyy,0);
        if ( y < 0 || y > 2200 || (y > 99 && y < 1900) )
        {
            if ( errors != null )
                errors.addError("The date has an invalid year. The expected format is mm/dd/yyyy.",fieldLabel);
            return false;
        }
        if ( y < 30 )
            y += 2000;
        else if ( y < 100 )
            y += 1900;
        else if ( y < 1000 )
            y += 1000; //?? 3 digit year ??
        
        if ( ! isValidNumberOfDaysForMonth(m,d,y) )
        {
            if ( errors != null )
                errors.addError("The date has an invalid day. The expected format is mm/dd/yyyy.",fieldLabel);
            return false;
        }
        
        return true;
    }
    
    /**
     * Attempts to normalize an M[M]/D[D]/YY[YY] (separators can be missing, '/', '-' or '.') date to meet this 
     * US standard MM/DD/YYYY format.
     * @param mdy the String expected to be an MM/DD/YYYY date
     * @return the fixed up string if can be fixed up, or it returns it "as is".
     */
    public static String fixupMDYDate(String mdy)
    {
        if ( EsfString.isBlank(mdy) || mdy.length() < 6 || mdy.length() > 10 )  // min date: 1/1/02; max: 01/01/2002
            return mdy;
        
        // dd-Mon-YYYY finder
        java.util.regex.Matcher matcher = EsfDate.MDYPattern.matcher(mdy);
        if ( matcher == null || ! matcher.find() || matcher.groupCount() != 3 )
        	return mdy;
        
        String mm = matcher.group(1);
        String dd = matcher.group(2);
        String yyyy = matcher.group(3);
        
        int m = app.stringToInt(mm,1);
        int d = app.stringToInt(dd,1);
        int y = app.stringToInt(yyyy,0);
        if ( y < 30 )
            y += 2000;
        else if ( y < 100 )
            y += 1900;
        else if ( y < 1000 )
            y += 1000; //?? 3 digit year ??
        
        return app.make2digits(m) + "/" + app.make2digits(d) + "/" + y;
    }
    
    /**
     * Convert an MM/DD/YYYY date to DD-Mon-YYYY format
     * @param mdy the MDY date to convert
     * @return the String DD-Mon-YYYY date; note if the mdy is not valid, the original MDY value will be returned
     */
    public String fixupMdyDateToDDMonYYYY(String mdy)
    {
    	EsfDate date = EsfDate.CreateFromMDY(mdy);
    	if ( date.isNull() )
            return mdy;
    	
    	return date.toDDMonYYYYString();
     }
    
    
    public static boolean isValidMMYYYYDate(String mmyyyy)
    {
        return isValidMMYYYYDate(mmyyyy,null,null);
    }
    public static boolean isValidMMYYYYDate(String mmyyyy, Errors errors)
    {
        return isValidMMYYYYDate(mmyyyy,errors,null);
    }
    public static boolean isValidMMYYYYDate(String mmyyyy, Errors errors, String fieldLabel)
    {
        if ( EsfString.isBlank(mmyyyy) || mmyyyy.length() < 4 )  // min date: 1/02
        {
            if ( errors != null )
                errors.addError("The mm/yyyy date is too short.",fieldLabel);
            return false;
        }
        
        if ( mmyyyy.length() > 7 ) // max date: 01/2002
        {
            if ( errors != null )
                errors.addError("The mm/yyyy date is too long. The expected format is mm/yyyy.",fieldLabel);
            return false;
        }
        
        // mm/yyyy finder
        java.util.regex.Matcher matcher = EsfDate.MMYYYYPattern.matcher(mmyyyy);
        if ( matcher == null || ! matcher.find() || matcher.groupCount() != 2 )
        {
            if ( errors != null )
                errors.addError("The date format is incorrect. The expected format is mm/yyyy.",fieldLabel);
            return false;
        }
        
        String mm = matcher.group(1);
        String yyyy = matcher.group(2);
        
        int m = app.stringToInt(mm,1);
        if ( m < 1 || m > 12 )
        {
            if ( errors != null )
                errors.addError("The date has an invalid month. The expected format is mm/yyyy.",fieldLabel);
            return false;
        }

        int y = app.stringToInt(yyyy,0);
        if ( y < 0 || y > 2200 || (y > 99 && y < 1900) )
        {
            if ( errors != null )
                errors.addError("The date has an invalid year. The expected format is mm/yyyy.",fieldLabel);
            return false;
        }
        if ( y < 30 )
            y += 2000;
        else if ( y < 100 )
            y += 1900;
        else if ( y < 1000 )
            y += 1000; //?? 3 digit year ??
        
        return true;
    }
    
    /**
     * Attempts to normalize an M[M]/YY[YY] (separators can be missing, '/', '-' or '.') date to meet this 
     * month-year MM/YYYY format.
     * @param mmyyyy the String expected to be an MM/YYYY date
     * @return the fixed up string if can be fixed up, or it returns it "as is".
     */
    public static String fixupMMYYYYDate(String mmyyyy)
    {
        if ( EsfString.isBlank(mmyyyy) || mmyyyy.length() < 4 || mmyyyy.length() > 7 )  // min date: 1/02; max: 01/2002
            return mmyyyy;
        
        java.util.regex.Matcher matcher = EsfDate.MMYYYYPattern.matcher(mmyyyy);
        if ( matcher == null || ! matcher.find() || matcher.groupCount() != 2 )
        	return mmyyyy;
        
        String mm = matcher.group(1);
        String yyyy = matcher.group(2);
        
        int m = app.stringToInt(mm,1);
        int y = app.stringToInt(yyyy,0);
        if ( y < 30 )
            y += 2000;
        else if ( y < 100 )
            y += 1900;
        else if ( y < 1000 )
            y += 1000; //?? 3 digit year ??
        
        return app.make2digits(m) + "/" + y;
    }
    

    /**
     * Attempts to normalize an M[M]/YY[YY] (separators can be missing, '/', '-' or '.') date to meet this 
     * US standard MM/DD/YYYY format with the DD set to the last day of the month.
     * @param mmyyyy the String expected to be an MM/YYYY date
     * @return the fixed up string if can be fixed up, or it returns it "as is".
     */
    public static String fixupMMYYYYToMDYDate(String mmyyyy)
    {
        if ( EsfString.isBlank(mmyyyy) || mmyyyy.length() < 4 || mmyyyy.length() > 7 )  // min date: 1/02; max: 01/2002
            return mmyyyy;
        
        java.util.regex.Matcher matcher = EsfDate.MMYYYYPattern.matcher(mmyyyy);
        if ( matcher == null || ! matcher.find() || matcher.groupCount() != 2 )
        	return mmyyyy;
        
        String mm = matcher.group(1);
        String yyyy = matcher.group(2);
        
        int m = app.stringToInt(mm,1);
        int y = app.stringToInt(yyyy,0);
        if ( y < 30 )
            y += 2000;
        else if ( y < 100 )
            y += 1900;
        else if ( y < 1000 )
            y += 1000; //?? 3 digit year ??
        
        int d = getNumberOfDaysForMonth(m, y);
        
        return app.make2digits(m) + "/" + app.make2digits(d) + "/" + y;
    }
    

    /*
    public static boolean isValidDDMonYYYYDate(String dmy)
    {
        return isValidDDMonYYYYDate(dmy,null,null);
    }
    public static boolean isValidDDMonYYYYDate(String dmy, Errors errors)
    {
        return isValidDDMonYYYYDate(dmy,errors,null);
    }
    public static boolean isValidDDMonYYYYDate(String dmy, Errors errors, String fieldLabel)
    {
        if ( EsfString.isBlank(dmy) || dmy.length() < 6 )  // min date: 1Jan02
        {
            if ( errors != null )
                errors.addError("The date is too short.",fieldLabel);
            return false;
        }
        
        if ( dmy.length() > 11 ) // max date: 01-Jan-2002
        {
            if ( errors != null )
                errors.addError("The date is too long. The expected format is DD-Mon-YYYY where month is Jan, Feb, Mar... (first 3 letters).",fieldLabel);
            return false;
        }
        
        // dd-Mon-YYYY finder
        java.util.regex.Matcher matcher = EsfDate.DDMonYYYYPattern.matcher(dmy);
        if ( matcher == null || ! matcher.find() || matcher.groupCount() != 3 )
        {
            if ( errors != null )
                errors.addError("The date format is incorrect. The expected format is DD-Mon-YYYY where month is Jan, Feb, Mar... (first 3 letters).",fieldLabel);
            return false;
        }
        
        String dd = matcher.group(1);
        String mon = matcher.group(2);
        String yyyy = matcher.group(3);
        
        boolean monFound = false;
        for ( String abbrev : DateUtil.getMonthNameAbbreviations() )
        {
        	if ( abbrev.equalsIgnoreCase(mon) )
        	{
        		mon = abbrev;
        		monFound = true;
        		break;
        	}
        }
        if ( ! monFound )
        {
            if ( errors != null )
                errors.addError("The date has an invalid month. The expected format is DD-Mon-YYYY where month is Jan, Feb, Mar... (first 3 letters).",fieldLabel);
            return false;
        }
        
        int d = app.stringToInt(dd,1);
        int y = app.stringToInt(yyyy,0);
        if ( y < 0 || y > 2200 || (y > 99 && y < 1900) )
        {
            if ( errors != null )
                errors.addError("The date has an invalid year. The expected format is DD-Mon-YYYY where month is Jan, Feb, Mar... (first 3 letters).",fieldLabel);
            return false;
        }
        if ( y < 30 )
            y += 2000;
        else if ( y < 100 )
            y += 1900;
        else if ( y < 1000 )
            y += 1000; //?? 3 digit year ??
        
        int mInt = DateUtil.getMonthAbbreviationNum(mon);
        if ( ! isValidNumberOfDaysForMonth(mInt,d,y) )
        {
            if ( errors != null )
                errors.addError("The date has an invalid day. The expected format is DD-Mon-YYYY where month is Jan, Feb, Mar... (first 3 letters).",fieldLabel);
            return false;
        }
        
        return true;
    }
    */
    
    public static String fixupDDMonYYYYDate(String dmy)
    {
        if ( EsfString.isBlank(dmy) || dmy.length() < 6 || dmy.length() > 11 )  // min date: 1Jan02, max: 01-Jan-2002
            return dmy;
        
        // dd-Mon-YYYY finder
        java.util.regex.Matcher matcher = EsfDate.DDMonYYYYPattern.matcher(dmy);
        if ( matcher == null || ! matcher.find() || matcher.groupCount() != 3 )
        	return dmy;
        
        String dd = matcher.group(1);
        String mon = matcher.group(2);
        String yyyy = matcher.group(3);
        
        /*
        boolean monFound = false;
        for ( String abbrev : DateUtil.getMonthNameAbbreviations() )
        {
        	if ( abbrev.equalsIgnoreCase(mon) )
        	{
        		mon = abbrev;
        		monFound = true;
        		break;
        	}
        }
        if ( ! monFound )
        	return dmy;
        */
        
        int d = app.stringToInt(dd,1);
        int y = app.stringToInt(yyyy,0);
        if ( y < 30 )
            y += 2000;
        else if ( y < 100 )
            y += 1900;
        else if ( y < 1000 )
            y += 1000; //?? 3 digit year ??
        
        return app.make2digits(d) + "-" + mon + "-" + y;
    }
    
    public static boolean isValidYMDDate(String ymd)
    {
        return isValidYMDDate(ymd,null,null);
    }
    public static boolean isValidYMDDate(String ymd, Errors errors)
    {
        return isValidYMDDate(ymd,errors,null);
    }
    public static boolean isValidYMDDate(String ymd, Errors errors, String fieldLabel)
    {
        if ( EsfString.isBlank(ymd) || ymd.length() < 6 )  // min date: 2009-1-1
        {
            if ( errors != null )
                errors.addError("The date is too short.",fieldLabel);
            return false;
        }
        
        if ( ymd.length() > 10 ) // max: 2009-01-01
        {
            if ( errors != null )
                errors.addError("The date is too long. The expected format is yyyy-mm-dd.",fieldLabel);
            return false;
        }
        
        // YYYY-dd-mm finder
        java.util.regex.Matcher matcher = EsfDate.YMDPattern.matcher(ymd);
        if ( matcher == null || ! matcher.find() || matcher.groupCount() != 3 )
        {
            if ( errors != null )
                errors.addError("The date format is incorrect. The expected format is yyyy-mm-dd.",fieldLabel);
            return false;
        }
        
        String yyyy = matcher.group(1);
        String mm = matcher.group(2);
        String dd = matcher.group(3);
        
        int y = app.stringToInt(yyyy,0);
        if ( y < 0 || y > 2200 || (y > 99 && y < 1900) )
        {
            if ( errors != null )
                errors.addError("The date has an invalid year. The expected format is yyyy-mm-dd.",fieldLabel);
            return false;
        }
        
        int m = app.stringToInt(mm,1);
        if ( m < 1 || m > 12 )
        {
            if ( errors != null )
                errors.addError("The date has an invalid month. The expected format is yyyy-mm-dd.",fieldLabel);
            return false;
        }

        int d = app.stringToInt(dd,1);
        if ( ! isValidNumberOfDaysForMonth(m,d,y) )
        {
            if ( errors != null )
                errors.addError("The date has an invalid day. The expected format is yyyy-mm-dd.",fieldLabel);
            return false;
        }
        
        return true;
    }
    
    
    /**
     * Attempts to normalize an YY[YY]-M[M]-D[D] (separators can be missing, '/', '-' or '.') date to meet this 
     * universal standard YYYY-MM-DD format.
     * @param ymd the String expected to be an YYYY-MM-DD date
     * @return the fixed up string if can be fixed up, or it returns it "as is".
     */
    public static String fixupYMDDate(String ymd)
    {
        if ( EsfString.isBlank(ymd) || ymd.length() < 8 || ymd.length() > 10 )  // min date: 2009-1-1; max: 2009-01-01
            return ymd;
        
        // YYYY-dd-mm finder
        java.util.regex.Matcher matcher = EsfDate.YMDPattern.matcher(ymd);
        if ( matcher == null || ! matcher.find() || matcher.groupCount() != 3 )
        	return ymd;
        
        String yyyy = matcher.group(1);
        String mm = matcher.group(2);
        String dd = matcher.group(3);
        
        int m = app.stringToInt(mm,1);
        int d = app.stringToInt(dd,1);
        
        return yyyy + "-" + app.make2digits(m) + "-" + app.make2digits(d);
    }

    /**
     * Attempts to normalize an D[D]/M[M]/YY[YY] (separators can be missing, '/', '-' or '.') date to meet this 
     * Euro standard DD/MM/YYYY format.
     * @param dmy the String expected to be an DD/MM/YYYY date
     * @return the fixed up string if can be fixed up, or it returns it "as is".
     */
    public static String fixupDMYDate(String dmy)
    {
        if ( EsfString.isBlank(dmy) || dmy.length() < 6 || dmy.length() > 10 )  // min date: 1/1/02; max: 01/01/2002
            return dmy;
        
        java.util.regex.Matcher matcher = EsfDate.DMYPattern.matcher(dmy);
        if ( matcher == null || ! matcher.find() || matcher.groupCount() != 3 )
        	return dmy;
        
        String dd = matcher.group(1);
        String mm = matcher.group(2);
        String yyyy = matcher.group(3);
        
        int d = app.stringToInt(dd,1);
        int m = app.stringToInt(mm,1);
        int y = app.stringToInt(yyyy,0);
        if ( y < 30 )
            y += 2000;
        else if ( y < 100 )
            y += 1900;
        else if ( y < 1000 )
            y += 1000; //?? 3 digit year ??
        
        return app.make2digits(d) + "/" + app.make2digits(m) + "/" + y;
    }
    
    /**
     * Attempts to normalize an D[D].M[M].YY[YY] date to meet this 
     * ISO Euro standard DD.MM.YYYY format.
     * @param dmy the String expected to be an DD.MM.YYYY date
     * @return the fixed up string if can be fixed up, or it returns it "as is".
     */
    public static String fixupIsoDMYDate(String dmy)
    {
        if ( EsfString.isBlank(dmy) || dmy.length() < 6 || dmy.length() > 10 )  // min date: 1.1.02; max: 01.01.2002
            return dmy;
        
        java.util.regex.Matcher matcher = EsfDate.DMYPattern.matcher(dmy);
        if ( matcher == null || ! matcher.find() || matcher.groupCount() != 3 )
        	return dmy;
        
        String dd = matcher.group(1);
        String mm = matcher.group(2);
        String yyyy = matcher.group(3);
        
        int d = app.stringToInt(dd,1);
        int m = app.stringToInt(mm,1);
        int y = app.stringToInt(yyyy,0);
        if ( y < 30 )
            y += 2000;
        else if ( y < 100 )
            y += 1900;
        else if ( y < 1000 )
            y += 1000; //?? 3 digit year ??
        
        return app.make2digits(d) + "." + app.make2digits(m) + "." + y;
    }
    
    static synchronized void loadTzNames()
    {
        if ( availableTzIds == null )
            availableTzIds = TimeZone.getAvailableIDs();
            
        if ( commonTzNames == null )
            commonTzNames = new String[availableTzIds.length];
            
        if ( nextGetCommonTzNames == null   ||
             nextGetCommonTzNames.before(new Date())
           )
        {
            for( int i=0; i < availableTzIds.length; ++i )
                commonTzNames[i] = TimeZone.getTimeZone(availableTzIds[i]).getDisplayName();
                
            nextGetCommonTzNames = dateDaysFromNow(1);
        }
    }
    
    public static String[] getAvailableTzIds()
    {
        loadTzNames();
        return availableTzIds;
    }
    
    public static String[] getCommonTzNames()
    {
        loadTzNames();
        return commonTzNames;
    }

}