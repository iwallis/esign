// Copyright (C) 2010-2014 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.util;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import com.esignforms.open.data.EsfDateTime;
import com.esignforms.open.data.EsfUUID;

/**
* Holds an object that's known by its EsfUUID.  Note that the EsfUUID may not be changed.  
* Is a read-optimized version in that there's no synchronization
* on the read accessors, but at the cost of every update (add/replace/remove) creating a new tree map.
* It's best for mostly static caches.
* 
* @author Yozons, Inc.
*/
public class UUIDCacheReadOptimized<T extends UUIDCacheReadOptimized.UUIDCacheable>
{
	public interface UUIDCacheable
	{
		public EsfUUID getId();
	}
	public interface TimeCacheable
	{
		public EsfDateTime getLastAccessFromCache();
		public void setLastAccessFromCache(EsfDateTime v);
	}
	
    // We create one cache, searchable by the EsfUUID.
	// Access is synchronized to prevent iterator issues.
    protected TreeMap<EsfUUID,T> uuidCache	= new TreeMap<EsfUUID,T>();
    protected int maxSize = 0;

    public UUIDCacheReadOptimized()
    {
    }
    
    public synchronized void clear()
    {
    	uuidCache = new TreeMap<EsfUUID,T>();
    }
    
    /**
     * Returns the number objects in the cache.
     */
    public int size()
    {
    	return uuidCache.size();
    }
    
    public List<EsfUUID> getAllIds()
    {
    	LinkedList<EsfUUID> idList = new LinkedList<EsfUUID>();
    	for( EsfUUID id : uuidCache.keySet() )
    		idList.add(id);
    	return idList;
    }
    
    /**
     * Adds the object to the cache if and only if the object's id is not already in the cache.
     * @param obj the object to add
     * @return true if the object is added to the cache; false if the cache already contains an object with the id
     */
    public synchronized boolean add(final T obj)
    {
    	EsfUUID id = obj.getId();
    	
    	if ( uuidCache.containsKey(id) )
    		return false;
    	
    	TreeMap<EsfUUID,T> uuidCacheNew = new TreeMap<EsfUUID,T>(uuidCache);
    	
    	uuidCacheNew.put(id, obj);
    	if ( obj instanceof TimeCacheable )
    		((TimeCacheable)obj).setLastAccessFromCache(new EsfDateTime());
    	
    	uuidCache = uuidCacheNew;
    	
    	int currSize = uuidCache.size();
    	if ( currSize > maxSize )
    		maxSize = currSize;

    	return true;
    }
    public void _addDuringInitializationOnly(final T obj) // unsync version to be used only during cache initialization when no accesses are done
    {
    	uuidCache.put(obj.getId(), obj);
    	if ( obj instanceof TimeCacheable )
    		((TimeCacheable)obj).setLastAccessFromCache(new EsfDateTime());
    	
    	int currSize = uuidCache.size();
    	if ( currSize > maxSize )
    		maxSize = currSize;
    }

    /**
     * Replaces the specified object in the cache.  The object must already exist with the same id, and that cache entry
     * will be updated to use this new object.  
     * @param obj the object to replace in the cache
     * @return true if the cache is updated with the new object; false if the id doesn't exist in the cache
     */
    public synchronized boolean replace(final T obj)
    {
    	EsfUUID id = obj.getId();
    	
    	if ( ! uuidCache.containsKey(id) )
    		return false;
    	
    	T origObj = uuidCache.remove(id);
    	if ( origObj == null )
    		return false;

    	TreeMap<EsfUUID,T> uuidCacheNew = new TreeMap<EsfUUID,T>(uuidCache);
    	
    	uuidCacheNew.put(id, obj);
    	if ( obj instanceof TimeCacheable )
    		((TimeCacheable)obj).setLastAccessFromCache(new EsfDateTime());
   	
    	uuidCache = uuidCacheNew;

    	return true;
    }

    /**
     * Removes the specified object from the cache using only the id as the key. 
     * @param obj the object to remove
     * @return
     */
    public synchronized boolean remove(final T obj)
    {
    	EsfUUID id = obj.getId();

    	if ( ! uuidCache.containsKey(id) )
    		return false;

    	TreeMap<EsfUUID,T> uuidCacheNew = new TreeMap<EsfUUID,T>(uuidCache);
    	
    	T origObj = uuidCacheNew.remove(obj.getId());
    	if ( origObj == null )
    		return false;
    	
    	uuidCache = uuidCacheNew;
    	
    	return true;
    }

    /**
     * Gets the object by id.
     * @param id the id of the object to retrieve
     * @return the object found or null if not present
     */
    public T getById(EsfUUID id)
    {
    	if ( id == null )
    		return null;
    	T obj = uuidCache.get(id);
    	if ( obj != null && obj instanceof TimeCacheable )
    		((TimeCacheable)obj).setLastAccessFromCache(new EsfDateTime());
    	return obj;
    }

	public int flushNotAccessedWithinMinutes(int numMinutes)
    {
    	if ( numMinutes == 0 )
    		numMinutes = -1;
    	else if ( numMinutes > 0 )
    		numMinutes *= -1;
    	
    	EsfDateTime checkTime = new EsfDateTime();
    	checkTime.addMinutes(numMinutes);
    	
    	TreeMap<EsfUUID,T> origUuidCache = new TreeMap<EsfUUID,T>(uuidCache);
    	
    	int numFlushed = 0;
    	for( T obj : origUuidCache.values() )
    	{
    		if ( obj instanceof TimeCacheable )
    		{
        		EsfDateTime lastAccessFromCache = ((TimeCacheable)obj).getLastAccessFromCache();
        		if ( lastAccessFromCache.isBefore(checkTime) )
        		{
        			remove(obj);
        			++numFlushed;
        		}
    		}
    	}
    	
    	return numFlushed;
    }

}