// Copyright (C) 2009-2013 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.filter;

import java.io.IOException;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.esignforms.open.Application;

/**
 * This class sets the Expires and Cache-Control header for requests, such as for images, javascript, CSS
 * and other items that do not change frequently so that the browser can cache them.  
 * If the request URL itself contains ".nocache" anywhere in it's path, these mechanisms are skipped so that
 * we can name those we don't want cached with this in the path.
 * 
 * The following needs to be configured into the web.xml
 * <pre><code>
 * <filter>
 *     	<description>Sets HTTP Expires and Cache-Control headers for non-changing static content like images, styles and scripts.</description>  
 *		<filter-name>AddExpiresHeaderFilter</filter-name>  
 *		<filter-class>com.esignforms.open.filter.AddExpiresHeaderFilter</filter-class>  
 *		<init-param>  
 *			<param-name>expireMinutes</param-name>  
 *			<param-value>480</param-value>  <!-- default value is 60 minutes -->
 *		</init-param>  
 * </filter>  
 *
 * <filter-mapping>  
 *		<filter-name>AddExpiresHeaderFilter</filter-name>  
 *		<url-pattern>*.gif</url-pattern>
 * 		<url-pattern>*.jpg</url-pattern>  
 * 		<url-pattern>*.css</url-pattern>  
 * 		<url-pattern>*.js</url-pattern>  
 *		<url-pattern>*.pdf</url-pattern>  
 *		<dispatcher>REQUEST</dispatcher>  
 * </filter-mapping>  
 * </code></pre>
 * 
 * @author Yozons, Inc.
 */
@WebFilter(initParams={@WebInitParam(name="expireMinutes",value="480")},urlPatterns={"*.gif","*.jpg","*.jpeg","*.png","*.css","*.js","*.pdf"},dispatcherTypes={javax.servlet.DispatcherType.REQUEST})
public class AddExpiresHeaderFilter 
	implements Filter 
{  
    private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(AddExpiresHeaderFilter.class);
    private static final int DEFAULT_EXPIRE_MINUTES = 60;
    private String cacheControlOptions;
    private long expireMsecs = DEFAULT_EXPIRE_MINUTES * 60L * 1000L;
    
    public void init(FilterConfig filterConfig) 
    	throws ServletException 
    {
    	int expireMinutes = DEFAULT_EXPIRE_MINUTES;
        try
        {
        	String expireMinutesParam = filterConfig.getInitParameter("expireMinutes"); 
            if ( expireMinutesParam != null ) 
            	expireMinutes = Integer.parseInt(expireMinutesParam);
        }
        catch( NumberFormatException e ) {}
        
        // The spec says this should never be more than 1 year out
        int oneYearInMinutes = 365 * 24 * 60;
        if ( expireMinutes > oneYearInMinutes )
        	expireMinutes = oneYearInMinutes;
        
        _logger.debug("AddExpiresHeaderFilter.init() - expire minutes: " + expireMinutes);
        cacheControlOptions = "max-age=" + (expireMinutes * 60);
        expireMsecs = expireMinutes * 60 * 1000;  // convert minutes to milliseconds for Expires
    }  

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) 
    	throws IOException, ServletException 
    {
    	HttpServletRequest  req = (HttpServletRequest)request;
    	HttpServletResponse res = (HttpServletResponse)response;

    	// We put the cache headers in unless ".nocache" is in the name; we also don't do it if we find expires/cache-control headers already present
    	if ( req.getRequestURI().indexOf(Application.NOCACHE_FILENAME) < 0 )
    	{
        	if ( ! res.containsHeader("Expires") && ! res.containsHeader("Cache-Control") )
        	{
            	long now = new Date().getTime();
            	res.setDateHeader("Expires",now+expireMsecs);  
            	res.setHeader("Cache-Control",cacheControlOptions);  
        	}
    	}
    	else
    	{
    		// Turn off caching -- the NOCACHE is in the filename
        	res.setHeader("Cache-Control","private,max=age=0");  
    	}

    	filterChain.doFilter(request, response);
    }  

    public void destroy() 
    {
        _logger.debug("AddExpiresHeaderFilter.destroy()");
    }  
}  