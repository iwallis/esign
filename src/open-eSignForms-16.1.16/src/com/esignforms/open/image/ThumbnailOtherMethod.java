// Copyright (C) 2010-2015 Yozons, Inc.
// Open eSignForms - Web-based electronic contracting software
//
// This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
// as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
// without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
// See the GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License along with this program.  
// If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
// Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.
//
package com.esignforms.open.image;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;

import javax.imageio.ImageIO;

import com.esignforms.open.Application;
import com.esignforms.open.config.Literals;
import com.esignforms.open.exception.EsfException;


/**
 * NO LONGER USED - WE NOW USE AN IMAGEICON
 * 
 * Creates thumbnail images from existing images.  The images supported include JPEG, GIF and PNG.
 * You can create a regular thumbnail (image constrained to the specified width and smooth algorithm) or one
 * where you can force the height (HEIGHT methods) or use a faster algorithm (FAST methods) that is less suited
 * for pictures unless time is more important.
 * 
 * For us, we mostly have byte arrays of image data going in and out since most images we have are stored in the database.
 * 
 * @author Yozons, Inc.
*/

public class ThumbnailOtherMethod
{ 
    private static com.esignforms.open.log.Logger _logger = new com.esignforms.open.log.Logger(Thumbnail.class);

    public static final String IMAGE_JPEG = "jpeg"; // JPEG (aka JPG) are preferred for most photos to be thumbnails as it generally compresses the best
    public static final String IMAGE_PNG = "png";
    public static final String IMAGE_GIF = "gif";

	protected enum SIZEBY { WIDTH, HEIGHT } // allow images to be constrained to a fixed width or a fixed height
	
	byte[] sourceImageData;
	protected BufferedImage source;
	protected Image thumbnail;

	// We'll just use these common contructor types that are supposed by ImageIcon
    public ThumbnailOtherMethod(byte[] sourceImageData)
    	throws EsfException
    {
    	this.sourceImageData = sourceImageData;
    	try
    	{
        	ByteArrayInputStream in = new ByteArrayInputStream(sourceImageData);
        	source = ImageIO.read(in);
        	if ( source == null || source.getHeight() == -1 || source.getWidth() == -1 )
        		throw new EsfException("Could not create an ImageIcon from the specified source");
    	}
    	catch( Exception e )
    	{
    		throw new EsfException("Could not create the BufferedImage from the specified byte array: " + e.getMessage());
    	}
    }

    /*
    public Thumbnail(Image sourceImage)
    	throws EsfException
    {
    	source = new ImageIcon(sourceImage); 
    	if ( source == null )
    		throw new EsfException("Could not create an ImageIcon from the specified source");
    }
    
    public Thumbnail(String sourceImagefileName)
		throws EsfException
    {
    	source = new ImageIcon(sourceImagefileName);
    	if ( source == null )
    		throw new EsfException("Could not create an ImageIcon from the specified source");
    }
    
    public Thumbnail(File sourceImageFile)
		throws EsfException
	{
		source = new ImageIcon(sourceImageFile.getAbsolutePath());
		if ( source == null )
			throw new EsfException("Could not create an ImageIcon from the specified source");
	}
*/
    
    // One of the create methods below should be called to get a thumbnail from the source image.
    // One advantage of doing this is that we can often create multiple differently sized thumbnails at one time
    // so if we need to small and large thumbnail capability, we can generate as we need based on the 
    // same source image.
    
    public final boolean createThumbnail(int sizeWidth)
    {
    	// our default will be the smooth as it works nicest, though not fast by any means
    	return createSmoothThumbnail(sizeWidth); 
    }
    public final boolean createHeightThumbnail(int sizeHeight)
    {
    	// our default will be the smooth as it works nicest, though not fast by any means
    	return createSmoothHeightThumbnail(sizeHeight); 
    }
    
    public final boolean createSmoothThumbnail(int sizeWidth)
    {
    	return createThumbnail(sizeWidth,Image.SCALE_SMOOTH);
    }
    public final boolean createSmoothHeightThumbnail(int sizeHeight)
    {
    	return createHeightThumbnail(sizeHeight,Image.SCALE_SMOOTH);
    }

    public final boolean createFastThumbnail(int sizeWidth)
    {
    	return createThumbnail(sizeWidth,Image.SCALE_FAST);
    }
    public final boolean createFastHeightThumbnail(int sizeHeight)
    {
    	return createHeightThumbnail(sizeHeight,Image.SCALE_FAST);
    }
    
    public final boolean createThumbnail(int sizeWidth, int scaleHints)
    {
    	return createThumbnail(sizeWidth, SIZEBY.WIDTH, scaleHints);
    }
    public final boolean createHeightThumbnail(int sizeHeight, int scaleHints)
    {
    	return createThumbnail(sizeHeight, SIZEBY.HEIGHT, scaleHints);
    }

    public boolean createThumbnail(int size, SIZEBY sizeby, int scaleHints)
    {
    	// Set up our width/height based on which one we're constraining to the fixed size (unless it's larger than the original source since that will actually scale it bigger)
    	// if they give us the width, we'll let the height value auto-resize
    	// if they give us the height, we'll let the width value auto-resize
    	int w, h;
    	
    	boolean sourceIsSmallerThanThumbnailSize = false;
    	if ( sizeby == SIZEBY.WIDTH )
    	{
    		w = size;
    		h = -1;
        	int origWidth = getSourceImage().getWidth(null);
        	if ( origWidth >= 0 && origWidth < w )
        		sourceIsSmallerThanThumbnailSize = true;
    	} 
    	else
    	{
    		w = -1;
    		h = size;
        	int origHeight = getSourceImage().getHeight(null);
        	if ( origHeight >= 0 && origHeight < h )
        		sourceIsSmallerThanThumbnailSize = true;
    	}
    	if ( sourceIsSmallerThanThumbnailSize )
    	{
    		try
    		{
            	ByteArrayInputStream in = new ByteArrayInputStream(sourceImageData);
            	BufferedImage bufferdThumnail = ImageIO.read(in);
            	thumbnail = bufferdThumnail;
    		}
    		catch( Exception e )
    		{
    			return false;
    		}
    	}
    	else
    		thumbnail = source.getScaledInstance(w, h, scaleHints);
    	if ( thumbnail == null )
    		_logger.error("createThumbnail() - Could not create a thumbnail from the source image; w: " + w + "; h: " + h + "; scaleHints: " + scaleHints);
    	return thumbnail != null;
    }
    
    public int getSourceWidth()
    {
    	return source.getWidth();
    }
    public int getSourceHeight()
    {
    	return source.getHeight();
    }
    public Image getSourceImage()
    {
    	return source;
    }
    
    public int getThumbnailWidth()
    {
    	return thumbnail == null ? 0 : thumbnail.getWidth(null);
    }
    public int getThumbnailHeight()
    {
    	return thumbnail == null ? 0 : thumbnail.getHeight(null);
    }
    public Image getThumbnailImage()
    {
    	return thumbnail;
    }

    protected BufferedImage generateBufferedImage(int width, int height, Image image)
    	throws IllegalArgumentException
    {
    	if ( width == -1 || height == -1 )
    		Application.getInstance().debug("generateBufferedImage() w: " + width + "; h: " + height);
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics g = bi.getGraphics();
        g.drawImage(image, 0, 0, null);
        return bi;
    }

    /**
     * Saves a thumbnail as a JPEG image byte array
     * @return the byte array that represents the JPEG thumbnail; returns null if there's an error or you haven't created a thumbnail yet.
     */
    public byte[] saveThumbnailToBytes()
    {
    	return saveThumbnailToBytes(Thumbnail.IMAGE_JPEG);
    }
    
    
    public byte[] saveThumbnailToBytesByMimeType(String mimeType)
    {
    	String imageType;
    	if ( Application.CONTENT_TYPE_JPEG.equals(mimeType) || Application.CONTENT_TYPE_PJPEG.equals(mimeType) )
    		imageType = Thumbnail.IMAGE_JPEG;
    	else if ( Application.CONTENT_TYPE_GIF.equals(mimeType) )
    		imageType = Thumbnail.IMAGE_GIF;
    	else
    		imageType = Thumbnail.IMAGE_PNG;
    	return saveThumbnailToBytes(imageType);
    }
    
    public byte[] saveThumbnailToBytes(String imageType)
    {
        if ( thumbnail != null )
        {
            try
            {
            	BufferedImage bi = generateBufferedImage(getThumbnailWidth(), getThumbnailHeight(), getThumbnailImage());
            	ByteArrayOutputStream baos = new ByteArrayOutputStream(10 * Literals.KB); // 10KB -- Get better way to determine this initial size value...
                ImageIO.write(bi, imageType, baos);
                baos.flush();
                byte[] ba = baos.toByteArray();
                baos.close();
                return ba;
            }
            catch (Exception e)
            {
            	_logger.debug("saveThumbnailToBytes() - Could not save a thumbnail to bytes; w: " + getThumbnailWidth() + "; h: " + getThumbnailHeight(),e);
            }
        }
        
        return null;
    }
    
    public boolean saveThumbnail(File file)
    {
    	return saveThumbnail(file, Thumbnail.IMAGE_JPEG);
    }
    
    public boolean saveThumbnail(File file, String imageType)
    {
        if ( thumbnail != null )
        {
            try
            {
            	BufferedImage bi = generateBufferedImage(getThumbnailWidth(), getThumbnailHeight(), getThumbnailImage());
                ImageIO.write(bi, imageType, file);
                return true;
            }
            catch (Exception e)
            {
            	_logger.debug("saveThumbnail() - Could not save a thumbnail to file: " + file.getAbsolutePath() + "; imageType: " + imageType,e);
            }
        }
        
        return false;
    }
    
    // We put these in for possible desire to save the original source image to disk
    public byte[] saveSourceToBytes()
    {
    	return saveSourceToBytes(Thumbnail.IMAGE_JPEG);
    }
    
    public byte[] saveSourceToBytes(String imageType)
    {
        try
        {
        	BufferedImage bi = generateBufferedImage(getSourceWidth(), getSourceHeight(), getSourceImage());
        	ByteArrayOutputStream baos = new ByteArrayOutputStream(10 * Literals.KB); // 10KB -- Get better way to determine this initial size value...
            ImageIO.write(bi, imageType, baos);
            baos.flush();
            byte[] ba = baos.toByteArray();
            baos.close();
            return ba;
        }
        catch (Exception e)
        {
        	_logger.debug("saveSourceToBytes() - Could not save the source to bytes; w: " + getSourceWidth() + "; h: " + getSourceHeight(),e);
        }
        
        return null;
    }
    
    public boolean saveSource(File file)
    {
    	return saveSource(file, Thumbnail.IMAGE_JPEG);
    }
    
    public boolean saveSource(File file, String imageType)
    {
        try
        {
        	BufferedImage bi = generateBufferedImage(getSourceWidth(), getSourceHeight(), getSourceImage());
            ImageIO.write(bi, imageType, file);
            return true;
        }
        catch (Exception e)
        {
        	_logger.debug("saveSource() - Could not save the source to file: " + file.getAbsolutePath() + "; imageType: " + imageType,e);
       }
        
        return false;
    }
    /*
    public static void main(String [] args)
    	throws EsfException
    {
    	// Source of thumbnail
        Thumbnail thumbnail = new Thumbnail("C:\\test\\image.jpg");

        // generate a "standard" (SMOOTH) thumbnail and save in all standard format: JPEG, PNG or GIF
        thumbnail.createThumbnail(100);
        thumbnail.saveThumbnail(new File("C:\\test\\thumb.jpg"), Thumbnail.IMAGE_JPEG);
        thumbnail.saveThumbnail(new File("C:\\test\\thumb.png"), Thumbnail.IMAGE_PNG);
        thumbnail.saveThumbnail(new File("C:\\test\\thumb.gif"), Thumbnail.IMAGE_GIF);
        
        thumbnail.createFastThumbnail(100);
        thumbnail.saveThumbnail(new File("C:\\test\\fastthumb.jpg"), Thumbnail.IMAGE_JPEG);
        thumbnail.saveThumbnail(new File("C:\\test\\fastthumb.png"), Thumbnail.IMAGE_PNG);
        thumbnail.saveThumbnail(new File("C:\\test\\fastthumb.gif"), Thumbnail.IMAGE_GIF);

        // Constrain to a fixed height rather than the default by width
        thumbnail.createHeightThumbnail(100);
        thumbnail.saveThumbnail(new File("C:\\test\\thumbHeight.jpg"), Thumbnail.IMAGE_JPEG);
        thumbnail.saveThumbnail(new File("C:\\test\\thumbHeight.png"), Thumbnail.IMAGE_PNG);
        thumbnail.saveThumbnail(new File("C:\\test\\thumbHeight.gif"), Thumbnail.IMAGE_GIF);
    }
	*/
}           