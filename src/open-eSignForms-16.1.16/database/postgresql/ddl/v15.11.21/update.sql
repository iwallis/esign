/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an update.sql script:
psql dbname < update.sql
**/

ALTER TABLE esf_field_template ADD COLUMN mask_input CHAR DEFAULT 'N';

ALTER TABLE esf_package_version_party_template ADD COLUMN override_package_document_id UUID NULL;
ALTER TABLE esf_package_version_party_template ADD COLUMN override_button_message_id UUID NULL;

