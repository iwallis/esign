/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an upgrade.sql script:
psql dbname < upgrade.sql
**/

CREATE TABLE esf_library_document_version_page_field_template
(
	library_document_version_page_id 	UUID NOT NULL,
	field_template_id 					UUID NOT NULL,
	PRIMARY KEY (library_document_version_page_id, field_template_id)
);

ALTER TABLE esf_library_document_version_page ADD COLUMN edit_review_type CHAR DEFAULT 'B';