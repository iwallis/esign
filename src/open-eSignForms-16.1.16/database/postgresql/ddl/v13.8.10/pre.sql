/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an update.sql script:
psql dbname < update.sql
**/

CREATE TABLE esf_http_send_request
(
	id						UUID 		NOT NULL,
	transaction_id			UUID		NULL,
	created_timestamp		TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	completed_timestamp		TIMESTAMP WITH TIME ZONE NULL, /* set only after successfully done */
	request_blob_id			UUID NOT NULL, /* contains the compressed, encrypted name-value to send */
	PRIMARY KEY ( id )
);
CREATE INDEX esfidx_http_send_request_completed ON esf_http_send_request (completed_timestamp);
CREATE INDEX esfidx_http_send_request_tid ON esf_http_send_request (transaction_id,created_timestamp);

CREATE TABLE esf_http_send_response
(
	http_send_request_id 	 	UUID NOT NULL,
	sent_timestamp 				TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	response_timestamp			TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	status						CHAR(1) DEFAULT 'S', /* S=Success; F=Failure */
	http_status_code			INTEGER NOT NULL,
	http_response_content_type  VARCHAR (100) NULL, /* response's mime/content type */
	http_response				VARCHAR(64000) NULL,
	PRIMARY KEY ( http_send_request_id,sent_timestamp )
);
