/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an update.sql script:
psql dbname < update.sql
**/

ALTER TABLE esf_transaction_party_assignment ADD COLUMN last_accessed_timestamp TIMESTAMP WITH TIME ZONE NULL;

/* Not precise, but a good approximation for existing transactions to give it a value based on the most recent update to a document */
UPDATE esf_transaction_party_assignment TPA SET last_accessed_timestamp = 
(SELECT TPD.last_updated_timestamp FROM esf_transaction_party_document TPD 
WHERE TPD.transaction_party_id=TPA.transaction_party_id ORDER BY TPD.last_updated_timestamp DESC LIMIT 1 );
