/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an upgrade.sql script:
psql dbname < upgrade.sql
**/

ALTER TABLE esf_signature_key ADD COLUMN x509_certificate_data VARCHAR(4000);

ALTER TABLE esf_userlogin ADD COLUMN request_forgot_count SMALLINT DEFAULT 0;

ALTER TABLE esf_package_version_party_template ADD COLUMN completed_url_field_expression VARCHAR(4096) NULL;
ALTER TABLE esf_package_version_party_template ADD COLUMN notcompleted_url_field_expression VARCHAR(4096) NULL;
