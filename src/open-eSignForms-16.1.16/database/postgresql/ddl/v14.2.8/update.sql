/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an update.sql script:
psql dbname < update.sql
**/

UPDATE esf_transaction SET cancel_timestamp=NULL WHERE cancel_timestamp IS NOT NULL AND (status='X' OR status='C');

DELETE FROM esf_transaction_activity_log WHERE transaction_id IN (SELECT DISTINCT transaction_id FROM esf_transaction_activity_log WHERE transaction_id NOT IN (SELECT id FROM esf_transaction));

ALTER TABLE esf_library_email_template_version ALTER COLUMN email_text TYPE VARCHAR(16000);
ALTER TABLE esf_library_email_template_version ALTER COLUMN email_html TYPE VARCHAR(512000);

ALTER TABLE esf_outbound_email_message ALTER COLUMN email_text TYPE VARCHAR(16000);
ALTER TABLE esf_outbound_email_message ALTER COLUMN email_html TYPE VARCHAR(512000);

ALTER TABLE esf_outbound_email_message_response ALTER COLUMN email_text TYPE VARCHAR(16000);
ALTER TABLE esf_outbound_email_message_response ALTER COLUMN email_html TYPE VARCHAR(512000);
