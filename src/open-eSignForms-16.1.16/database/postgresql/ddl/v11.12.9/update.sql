/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an upgrade.sql script:
psql dbname < upgrade.sql
**/

/* Fix bug in code that deleted the actual blob attachment, but not the record pointing to it */
DELETE FROM esf_transaction_file WHERE transaction_id NOT IN (SELECT id FROM esf_transaction);

/*
 * esf_outbound_email_message_attachment: Defines a file attachment for the specified outbound email.
 * 1:N relationship table of esf_outbound_email_message:esf_outbound_email_message_attachment
 */
CREATE TABLE esf_outbound_email_message_attachment
(
	id   						UUID NOT NULL, /* id of attached file */
 	outbound_email_message_id	UUID NOT NULL, /* outbound email message it's associated with */
 	file_order					SMALLINT DEFAULT 1,
	file_name					VARCHAR(256) NOT NULL, /* file name as uploaded */
	file_mime_type				VARCHAR(100) NOT NULL, /* file's mime type */
	file_blob_id				UUID NOT NULL, /* contains the compressed, encrypted file */
	PRIMARY KEY (id)
);
CREATE INDEX esfidx_outbound_email_message_attachment_eid ON esf_outbound_email_message_attachment (outbound_email_message_id,file_order);

