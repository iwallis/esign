/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an upgrade.sql script:
psql dbname < upgrade.sql
**/

CREATE TABLE esf_library_serial 
(
	id            	UUID        NOT NULL,
	container_id    UUID        NOT NULL,	        /* container/library defined in */
	esfname         VARCHAR(50) NOT NULL,
	description     VARCHAR(250)    NULL,
	comments        VARCHAR(1000000) NULL,
 	status 					CHAR DEFAULT 'D', 			/* E=enabled; D=disabled */
	production_version      INTEGER         DEFAULT 0,		/* Zero means no production version set */
	test_version      		INTEGER         DEFAULT 0,		/* either test_version=production_version or test_version=production_version+1*/
	next_production_serial  BIGINT NOT NULL DEFAULT 0,      /* next production serial number, data value do not change by version */
	next_test_serial    	BIGINT NOT NULL DEFAULT 0,      /* next test serial number, data value do not change by version */
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_serial_name ON esf_library_serial (container_id,lower(esfname));

CREATE TABLE esf_library_serial_version
(
	id                      UUID        NOT NULL,
	library_serial_id       UUID        NOT NULL,	    /* file this is a version of */
	version                 INTEGER     DEFAULT 1,		/* sequence number starting with 1 */
	created_by_user_id      UUID        NOT NULL,
	last_updated_by_user_id UUID        NOT NULL,
	created_timestamp       TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated_timestamp  TIMESTAMP WITH TIME ZONE NOT NULL,
	decimal_format          VARCHAR(20) NOT NULL DEFAULT '###########0',
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_serial_version ON esf_library_serial_version (library_serial_id,version);

CREATE INDEX esfidx_tran_report_field_tranfileid_rftid ON esf_tran_report_field_tranfileid (report_field_template_id);