/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an upgrade.sql script:
psql dbname < upgrade.sql
**/

/*
 * esf_tran_report_field_tranfileid is unlike the other report fields in that it cannot be used to search
 * but is used to map files uploaded into documents, and those fields are mapped to a report field of this type,
 * so that we can then display a list of files directly in the custom report.
 */
CREATE TABLE esf_tran_report_field_tranfileid
(
  transaction_id			UUID NOT NULL,
  report_field_template_id	UUID NOT NULL,
  tran_file_id         		UUID NOT NULL,
  PRIMARY KEY (transaction_id,report_field_template_id,tran_file_id)
);

ALTER TABLE esf_field_template ADD COLUMN extra_options VARCHAR(100) NULL;
ALTER TABLE esf_field_template ADD COLUMN display_empty_value_enabled CHAR DEFAULT 'N';
ALTER TABLE esf_field_template ADD COLUMN display_empty_value VARCHAR(4096) NULL;
ALTER TABLE esf_label_template ADD COLUMN suppress_non_input CHAR DEFAULT 'N';