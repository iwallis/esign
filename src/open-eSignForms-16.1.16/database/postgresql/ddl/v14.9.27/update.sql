/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an update.sql script:
psql dbname < update.sql
**/

DELETE FROM esf_report_template_transaction_template WHERE report_template_id NOT IN (SELECT id FROM esf_report_template);

CREATE TABLE esf_report_template_limit_document
(
	report_template_id			UUID NOT NULL,	    
	library_document_id			UUID NOT NULL,	    /* report is limited to provide access to this document */
	PRIMARY KEY ( report_template_id,library_document_id )
);

ALTER TABLE esf_party_template ADD COLUMN test_todo_group_id UUID NULL;
UPDATE esf_party_template SET test_todo_group_id = todo_group_id;
