/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an update.sql script:
psql dbname < update.sql
**/

/**
 * esf_transaction_timer holds information about a timer that has yet to expire for a given transaction.
 * 1:N esf_transaction:esf_transaction_timer
 */
CREATE TABLE esf_transaction_timer
(
	transaction_id          UUID NOT NULL,	    
	timer_name              VARCHAR(100) NOT NULL,
	expire_timestamp        TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);
CREATE UNIQUE INDEX esfidx_transaction_timer_idname ON esf_transaction_timer (transaction_id,lower(timer_name));
CREATE INDEX esfidx_transaction_timer_time ON esf_transaction_timer (expire_timestamp);
