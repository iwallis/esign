/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an update.sql script:
psql dbname < update.sql
**/

/*
 * esf_library_buttonmessage: Defines buttons and related messages in the library.  They are maintained by version. 
 */

ALTER TABLE esf_field_template ADD COLUMN input_prompt VARCHAR(100) NULL;
