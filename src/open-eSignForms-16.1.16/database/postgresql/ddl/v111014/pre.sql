/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an upgrade.sql script:
psql dbname < upgrade.sql
**/

ALTER TABLE esf_report_field_template DROP COLUMN num_reports_referenced;

/*
 * esf_report_template: Defines a template of a report that basically shows transactions of the specified type.
 * A disabled report template will not be listed or cannot be used to view data, export CSV or download transaction archives.
 * 1:N relationship table esf_report_template:esf_transaction_template
 * N:N relationship table esf_report_template:esf_report_field_template
 */
CREATE TABLE esf_report_template
(
	id   				UUID NOT NULL,
	created_timestamp 	TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	created_by_user_id	UUID NOT NULL, /* will be esf_deployment.id for 'system template' */
	last_updated_timestamp 	TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	last_updated_by_user_id	UUID NOT NULL,
	path_name 			VARCHAR(1024) NOT NULL,
	description 		VARCHAR(250) NULL,
	comments            VARCHAR(1000000) NULL,
 	status 				CHAR DEFAULT 'D', /* E=enabled; D=disabled */
	PRIMARY KEY (id)
);
CREATE UNIQUE INDEX esfidx_report_template_pathname ON esf_report_template (lower(path_name));

/* N:N relationship table for esf_report_template:esf_transaction_template */
CREATE TABLE esf_report_template_transaction_template
(
	report_template_id			UUID NOT NULL,	    
	transaction_template_id		UUID NOT NULL,	    /* report can list this transaction template's transactions */
	PRIMARY KEY ( report_template_id,transaction_template_id )
);

/**
 * Defines the report fields that appear in the report
 */
CREATE TABLE esf_report_template_report_field
(
	report_template_id UUID NOT NULL,
	report_field_template_id UUID NOT NULL,
	field_order SMALLINT DEFAULT 1,
	field_label VARCHAR(50) NOT NULL, /* label to show for column header */
	output_format_spec VARCHAR(100) NULL, /* Varies on data type, but can be used to specify any formatting rules for this field on output */
	allow_search CHAR DEFAULT 'N', /* Can the user search using this field: Y=Yes; N=No */
	PRIMARY KEY ( report_template_id,report_field_template_id,field_order )
);
CREATE INDEX esfidx_report_template_report_field_rfti ON esf_report_template_report_field (report_field_template_id);
