/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an upgrade.sql script:
psql dbname < upgrade.sql
**/

ALTER TABLE esf_library_document_version ADD COLUMN page_orientation CHAR DEFAULT 'P';

/*
 * esf_file: Defines a file in the library, document or other container.  They are maintained by version. 
 */
CREATE TABLE esf_library_file
(
	id						UUID 		NOT NULL,
	container_id			UUID		NOT NULL,	    /* library or document version is in */
	esfname            		VARCHAR(50) NOT NULL,
	display_name			VARCHAR(100)    NULL,
	description				VARCHAR(250)    NULL,
	comments                VARCHAR(1000000) NULL,
 	status 					CHAR DEFAULT 'D', 			/* E=enabled; D=disabled */
	production_version      INTEGER 	DEFAULT 0,		/* Zero means no production version set */
	test_version      		INTEGER		DEFAULT 0,		/* either test_version=production_version or test_version=production_version+1*/
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_file_name ON esf_library_file (container_id,lower(esfname));

/* 1:N relationship table for esf_library_file:esf_library_file_version */
CREATE TABLE esf_library_file_version
(
	id						UUID 		NOT NULL,
	library_file_id		    UUID		NOT NULL,	    /* file this is a version of */
	version         		INTEGER 	DEFAULT 1,		/* sequence number starting with 1 */
	created_by_user_id		UUID		NOT NULL,
	last_updated_by_user_id	UUID		NOT NULL,
	created_timestamp		TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL,
	file_file_name			VARCHAR(256) NOT NULL,
	file_mime_type			VARCHAR(100) NOT NULL,
	file_blob_id			UUID 		NOT NULL, 		/* contains the non-compressed, non-encrypted file since files are public/transmitted and not secret */
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_file_version ON esf_library_file_version (library_file_id,version);

