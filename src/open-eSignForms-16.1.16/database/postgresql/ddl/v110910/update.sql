/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an upgrade.sql script:
psql dbname < upgrade.sql
**/

/*
 * Maps fields from a package version's document's field template to a report field template
 */
CREATE TABLE esf_package_version_report_field
(
	id 							UUID NOT NULL,
	package_version_id 			UUID NOT NULL,
	document_id 				UUID NOT NULL,
	field_esfname 			    VARCHAR(50) NOT NULL,  /* field name; use name instead of 'id' because as field id changes with each new document version */
	report_field_template_id 	UUID NOT NULL, /* esf_report_field_template.id */
	field_order 				SMALLINT DEFAULT 1,
	show_to_do 					CHAR DEFAULT 'N', /* Is field shown in To Do listing: Y=Yes; N=No */
	PRIMARY KEY (id)
);

/* Specifies a field that can be reported on. Fields in documents that need to be reported are defined to point to one of these
 * specifications, and the reports only use these fields (and no longer know the source of the data itself, which varies over documents and versions).
 */
CREATE TABLE esf_report_field_template
(
	id 						UUID NOT NULL,	    
	field_esfname 			VARCHAR(50) NOT NULL,  /* name of report field; those with "esf_" prefix are reserved for built-in fields */
	field_type 				CHAR DEFAULT 'S', /* B=built-in; S=String; I=Integer; N=Decimal/Numeric/Money; D=Date; 1=Numbers-only string; Z=Alpha-numeric-only string */
	field_label 			VARCHAR(50) NULL,
	field_tooltip 			VARCHAR(100) NULL,
	num_reports_referenced 	SMALLINT DEFAULT 0,
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_report_field_template_name ON esf_report_field_template(lower(field_esfname));

CREATE TABLE esf_tran_report_field_string
(
  transaction_id			UUID NOT NULL,
  report_field_template_id	UUID NOT NULL,
  field_value         		VARCHAR(4000) NULL,
  PRIMARY KEY (transaction_id,report_field_template_id)
);
CREATE INDEX esfidx_tran_report_field_string_value ON esf_tran_report_field_string (report_field_template_id,lower(field_value));

CREATE TABLE esf_tran_report_field_long
(
  transaction_id			UUID NOT NULL,
  report_field_template_id	UUID NOT NULL,
  field_value         		BIGINT NULL,
  PRIMARY KEY (transaction_id,report_field_template_id)
);
CREATE INDEX esfidx_tran_report_field_long_value ON esf_tran_report_field_long (report_field_template_id,field_value);

CREATE TABLE esf_tran_report_field_numeric
(
  transaction_id			UUID NOT NULL,
  report_field_template_id	UUID NOT NULL,
  field_value         		NUMERIC(15,2) NULL,
  PRIMARY KEY (transaction_id,report_field_template_id)
);
CREATE INDEX esfidx_tran_report_field_numeric_value ON esf_tran_report_field_numeric (report_field_template_id,field_value);

CREATE TABLE esf_tran_report_field_date
(
  transaction_id			UUID NOT NULL,
  report_field_template_id	UUID NOT NULL,
  field_value         		DATE NULL,
  PRIMARY KEY (transaction_id,report_field_template_id)
);
CREATE INDEX esfidx_tran_report_field_date_value ON esf_tran_report_field_date (report_field_template_id,field_value);

/* #*#*#*#*# N O T I C E #*#*#*#*# Be sure to run the table_grants to ensure the correct table permissions. */