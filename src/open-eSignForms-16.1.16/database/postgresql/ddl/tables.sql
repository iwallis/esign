/* 
Copyright (C) 2009-2015 Yozons, Inc.
Open eSignForms - Web-based electronic contracting software

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License 
as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program.  
If not, see <http://open.esignforms.com/agpl.txt> or <http://www.gnu.org/licenses/>.
Contact information is via the public forums at http://open.esignforms.com or via private email to open-esign@yozons.com.

SMALLINT = INT2 = 16 bit number = Java short (64k values positive & negative)
INTEGER  = INT4 = 32 bit number = Java int (2g values positive & negative)
BIGINT   = INT8 = 64 bit number = Java long
CHECK DB SIZE: SELECT pg_size_pretty(pg_database_size('test'));
CHECK TABLE/INDEX SIZE: SELECT pg_size_pretty(pg_total_relation_size('esf_blob'));

We use UUID for 'id' attributes.
 
We use VARCHAR(50) strings for 'esfname' which is a code/tool lookup name that must be <= 50 letters, 
begins with a letter, and only uses letters, digits and the underscore (_) without any whitespace.  
While esfnames may be entered in upper and lowercase, rarely (ever?) are they case-sensitive from a 
unique-ness perspective (indexing all lowercase) because these are referenced by people who likely won't 
notice such subtle differences.  'pathName' is a series of one or more 'esfname' separated by '/'.
*/

/*
  esf_blob: defines a  BLOB table for storing various "large" objects with optional
  encryption and compression. Most of our data is both compressed and encrypted.
*/
CREATE TABLE esf_blob 
(
	id            	UUID NOT NULL,		   /* unique blob id */
	session_key_id 	UUID,      			   /* session key id used to encrypt this blob's key_data; null if not encrypted */
	key_data    	VARCHAR(200),     	   /* Base-64 encoded encryption key used to encrypt just this blob; null if not encrypted */
	orig_size     	INTEGER NOT NULL,      /* original size of the blob byte array */
	compressed_size INTEGER NOT NULL,      /* compressed size of the unencrypted blob; equals orig_size if not compressed */
	stored_size   	INTEGER NOT NULL,      /* size of the encrypted and optionally compressed blob as stored */
	blob_data     	OID     NOT NULL,      /* the actual encrypted blob data */
	PRIMARY KEY ( id )
);


/*
  esf_deployment: defines a given deployment of the application.  There should only be one record, created when the system is deployed.
*/
CREATE TABLE esf_deployment
(
	id          			UUID NOT NULL,						/* unique key id for this deployment */
	created_timestamp 		TIMESTAMP WITH TIME ZONE NOT NULL,	/* datetime the deployment was created */
	last_started_timestamp	TIMESTAMP WITH TIME ZONE NULL, 		/* datetime the deployment was last started */
	last_stopped_timestamp	TIMESTAMP WITH TIME ZONE NULL, 		/* datetime the deployment was last stopped; if < last_started_timestamp when starting, it was not a clean shutdown */
	global_properties_id	UUID,							/* points to our encrypted blob where the doc-searchable shared global properties are stored; only null during initial dbsetup */	
	deploy_properties_id	UUID,							/* points to our encrypted blob where the deployment properties are stored; only null during initial dbsetup */	
	PRIMARY KEY ( id ) 
);


/*
 * esf_field_template: Defines a template of a field in a container like a document version or a library.
 */
CREATE TABLE esf_field_template
(
	id   					UUID NOT NULL,
 	container_id			UUID NOT NULL,
 	container_reference_count SMALLINT DEFAULT 0,
	esfname            		VARCHAR(50) NOT NULL,
	field_type 			    CHAR DEFAULT 'I', /* I=GeneralInput */
	align					CHAR DEFAULT 'I', /* I=inherit; L=left; C=center; R=right */
	required				CHAR DEFAULT 'N', /* Y=Required; N=Optional */
	auto_complete			CHAR DEFAULT 'Y', /* Y=Yes, allow field to use browser auto-complete (normal); N=No, add autocomplete="off" to input (sensitive data only) */
	auto_post 			    CHAR DEFAULT 'N', /* Y=Yes, change to field value in form triggers auto-POST to update the server; N=No, post normally only when presses a button */
	mask_in_data_snapshot	CHAR DEFAULT 'N', /* Y=Yes, field will be masked in data snapshot (sensitive data only); N=Data snapshot will include data as is (normal) */
	mask_input				CHAR DEFAULT 'N', /* Y=Yes, field will be masked on input (sensitive data only, like password input); N=Show input data as is (normal) */
	clone_allowed			CHAR DEFAULT 'Y', /* Y=Yes, field data can be cloned; N=Data will not be cloned for new transactions */
	border 			   	 	CHAR(4) DEFAULT 'NNNN', /* Y/N - [0]=Top, [1]=Right, [2]=Bottom, [3]=Left; i.e.: NNNN=No-border; YYYY=Box; YNNN=Top-only; NNYN=Bottom-only */
	width_unit				CHAR DEFAULT '%', /* %=Percent; P=Pixels; M=Em; x=Ex */
	ignore_width_on_output	CHAR DEFAULT 'N', /* Y=Ignore width on output; N=Keep width */
	width					SMALLINT DEFAULT 100, /* Width of the field */
	min_length				INTEGER DEFAULT 0, /* If required, the minimum number of chars;  If optional, only checked if non-blank. */
	max_length              INTEGER DEFAULT 100, /* Must be >= min_length */
	initial_value			VARCHAR(4096) NULL, /* Initial value spec -- a literal value, or ${otherrec.fieldesfname} */
	display_empty_value_enabled	CHAR DEFAULT 'N', /* Y=Yes, display empty value will be used; N=Display empty vale will not be used */
	display_empty_value		VARCHAR(4096) NULL, /* Display empty value spec -- a literal value, or ${otherrec.fieldesfname} */
	input_format_spec		VARCHAR(100) NULL, /* Varies on data type, but can be used to specify any formatting rules for this field on input */
	output_format_spec		VARCHAR(100) NULL, /* Varies on data type, but can be used to specify any formatting rules for this field on output */
	extra_options			VARCHAR(4096) NULL, /* Varies on data type, but can be used to specify any additional features needed */
	tooltip					VARCHAR(100) NULL, /* input field tooltip */
	input_prompt			VARCHAR(100) NULL, /* input field prompt using placeholder attribute */
	label_template_id		UUID NOT NULL,
	PRIMARY KEY (id)
);
CREATE UNIQUE INDEX esfidx_field_template_name ON esf_field_template(container_id,lower(esfname));

/*
 * esf_group: Defines a group.  Users can be assigned into groups.  There's no real nesting of groups supported (that is, one group
 * is composed of members from another group).
 * When a group is disabled, it's as if the group does not exist and cannot be selected for putting users into.
 */
CREATE TABLE esf_group
(
	id   				UUID NOT NULL,
	parent_group_id		UUID,
	created_timestamp 	TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	created_by_user_id	UUID NOT NULL, /* will be esf_deployment.id for 'system groups'; can't disable or change the path_name if a system group */
	last_updated_timestamp 	TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	last_updated_by_user_id	UUID NOT NULL,
	path_name 			VARCHAR(1024) NOT NULL,
 	status 				CHAR DEFAULT 'D', /* E=enabled; D=disabled */
	description 		VARCHAR(250),
	properties_blob_id	UUID NOT NULL, /* contains extra fields, comments, preferences, etc. */
	PRIMARY KEY (id)
);
CREATE UNIQUE INDEX esfidx_group_pathname ON esf_group(lower(path_name));

/*
 * esf_group_user: Defines a group member containing a specific user.  
 */
CREATE TABLE esf_group_user
(
	group_id			UUID NOT NULL,
	user_id				UUID NOT NULL,
	PRIMARY KEY ( group_id, user_id )
);
CREATE INDEX esfidx_group_user_userid ON esf_group_user(user_id);

/**
 * esf_http_send_request contains HTTP SENDs queued and then sent out by HTTP(S) GET/POST.
 * 1:N esf_transaction:esf_http_send_request
 */
CREATE TABLE esf_http_send_request
(
	id						UUID 		NOT NULL,
	transaction_id			UUID		NULL,
	created_timestamp		TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	completed_timestamp		TIMESTAMP WITH TIME ZONE NULL, /* set only after successfully done */
	request_blob_id			UUID NOT NULL, /* contains the compressed, encrypted name-value to send */
	PRIMARY KEY ( id )
);
CREATE INDEX esfidx_http_send_request_completed ON esf_http_send_request (completed_timestamp);
CREATE INDEX esfidx_http_send_request_tid ON esf_http_send_request (transaction_id,created_timestamp);

/**
 * esf_http_send_response contains HTTP responses for each attempt at sending an http request
 * 1:N esf_http_send_request:esf_http_send_response
 */
CREATE TABLE esf_http_send_response
(
	http_send_request_id 	 	UUID NOT NULL,
	sent_timestamp 				TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	response_timestamp			TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	status						CHAR(1) DEFAULT 'S', /* S=Success; F=Failure */
	http_status_code			INTEGER NOT NULL,
	http_response_content_type  VARCHAR (100) NULL, /* response's mime/content type */
	http_response				VARCHAR(64000) NULL,
	PRIMARY KEY ( http_send_request_id,sent_timestamp )
);


/*
 * esf_label_template: Defines a template of a label in a container like a field template.
 */
CREATE TABLE esf_label_template
(
	id   					UUID NOT NULL,
	label					VARCHAR(100),
	position				VARCHAR(2) DEFAULT 'TL', /* N=No show; TL=TopLeft; TC=TopCenter; TR=TopRight; BL=BottomLeft; BC=BottomCenter; BR=BottomRight; L=Left; R=Right */
	separator				VARCHAR(2) DEFAULT ': ',
	size					CHAR DEFAULT 'N', /* N=Normal; S=Small */
	suppress_non_input		CHAR DEFAULT 'N', /* N=No, show label when not in input mode; Y=Yes, suppress label when not in input mode */
	PRIMARY KEY (id)
);

/*
 * esf_library: Defines a library of documents, properties, HTML snippets, fields, images, files, etc that can have access control set for the bundle.  They can also be put into 
 * search lists to allow for dynamic retrieval at runtime.  
 * A disabled library will not be listed or used for dynamic retrieval at runtime if included in a search list.
 */
CREATE TABLE esf_library
(
	id   				UUID NOT NULL,
	created_timestamp 	TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	created_by_user_id	UUID NOT NULL, /* will be esf_deployment.id for 'system library' */
	last_updated_timestamp 	TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	last_updated_by_user_id	UUID NOT NULL,
	path_name 			VARCHAR(1024) NOT NULL,
	description 		VARCHAR(250) NULL,
	comments    		VARCHAR(1000000) NULL,
 	status 				CHAR DEFAULT 'D', /* E=enabled; D=disabled */
	library_documentstyle_id UUID NOT NULL, /* Default document style for new documents created in this library */
	PRIMARY KEY (id)
);
CREATE UNIQUE INDEX esfidx_library_pathname ON esf_library(lower(path_name));

/*
 * esf_library_buttonmessage: Defines buttons and related messages in the library.  They are maintained by version. 
 * 1:N relationship table for esf_library_buttonmessage:esf_package_version
 */
CREATE TABLE esf_library_buttonmessage
(
	id						UUID 		NOT NULL,
	library_id				UUID		NOT NULL,	    /* library is in */
	esfname            		VARCHAR(50) NOT NULL,
	description				VARCHAR(250)    NULL,
	comments                VARCHAR(1000000) NULL,
 	status 					CHAR DEFAULT 'D', 			/* E=enabled; D=disabled */
	production_version      INTEGER 	DEFAULT 0,		/* Zero means no production version set */
	test_version      		INTEGER		DEFAULT 0,		/* either test_version=production_version or test_version=production_version+1*/
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_buttonmessage_name ON esf_library_buttonmessage (library_id,lower(esfname));

/* 1:N relationship table for esf_library_buttonmessage:esf_library_buttonmessage_version */
CREATE TABLE esf_library_buttonmessage_version
(
	id						UUID 		NOT NULL,
	library_buttonmessage_id		UUID		NOT NULL,	    /* buttonmessage this is a version of */
	version         		INTEGER 	DEFAULT 1,		/* sequence number starting with 1 */
	created_by_user_id		UUID		NOT NULL,
	last_updated_by_user_id	UUID		NOT NULL,
	created_timestamp		TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL,
	record_blob_id			UUID 	NOT NULL, 		/* stores the buttons and messages as properties */
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_buttonmessage_version ON esf_library_buttonmessage_version (library_buttonmessage_id,version);

/*
 * esf_library_document: Defines a document in the library.  Documents are maintained by version.  Documents have 1 or more pages.
 */
CREATE TABLE esf_library_document
(
	id						UUID 		NOT NULL,
	library_id				UUID		NOT NULL,	    /* library document is in */
	esfname            		VARCHAR(50) NOT NULL,
	display_name			VARCHAR(100) NOT NULL,
    file_name_spec          VARCHAR(256)     NULL,
	description				VARCHAR(250)     NULL,
	comments                VARCHAR(1000000) NULL,
 	status 					CHAR DEFAULT 'D', 			/* E=enabled; D=disabled */
	production_version      INTEGER 	DEFAULT 0,		/* Zero means no production version set */
	test_version      		INTEGER		DEFAULT 0,		/* either test_version=production_version or test_version=production_version+1*/
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_document_name ON esf_library_document (library_id,lower(esfname));

/* 1:N relationship table for esf_library_document:esf_library_document_version */
CREATE TABLE esf_library_document_version
(
	id						UUID 		NOT NULL,
	library_document_id		UUID		NOT NULL,	    /* document this is a version of */
	version         		INTEGER 	DEFAULT 1,		/* sequence number starting with 1 */
	created_by_user_id		UUID		NOT NULL,
	last_updated_by_user_id	UUID		NOT NULL,
	library_documentstyle_id	UUID	NOT NULL,		/* document style to apply */
	programming_blob_id    	UUID        NULL,           /* contains extra programming, data validation options */
	page_orientation        CHAR        DEFAULT 'P',    /* P=portrait; L=landscape */
	esign_process_record_location CHAR  DEFAULT 'F',    /* F=on final own page; D=on last document page; N=do not show */
	created_timestamp		TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL,
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_document_version ON esf_library_document_version (library_document_id,version);

/* 1:N relationship table for esf_library_document_version:esf_library_document_version_page */
CREATE TABLE esf_library_document_version_page
(
	id					UUID 		NOT NULL,
	library_document_version_id	UUID	NOT NULL,	/* document+version the page is in */
	page_number			SMALLINT	DEFAULT 1,		/* orders pages appearing in document starting with 1 */
    edit_review_type    CHAR        DEFAULT 'B',    /* B=for edit and review; E=for edit only; R=for review only */
	esfname         	VARCHAR(50) NOT NULL,
	html				VARCHAR(1000000) NOT NULL,  /* the HTML code for this page */
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_document_version_page_num ON esf_library_document_version_page (library_document_version_id,page_number);
CREATE UNIQUE INDEX esfidx_library_document_version_page_name ON esf_library_document_version_page (library_document_version_id,lower(esfname));

/* 1:N relationship table for esf_library_document_version_page:esf_field_template - shows editable fields on a page */
CREATE TABLE esf_library_document_version_page_field_template
(
    library_document_version_page_id    UUID NOT NULL,
    field_template_id 			        UUID NOT NULL,
    PRIMARY KEY (library_document_version_page_id, field_template_id)
);

/*
 * esf_library_documentstyle: Defines a document style in the library.  They are maintained by version. 
 */
CREATE TABLE esf_library_documentstyle
(
	id						UUID 		NOT NULL,
	library_id				UUID		NOT NULL,	    /* library is in */
	esfname            		VARCHAR(50) NOT NULL,
	description				VARCHAR(250)    NULL,
	comments                VARCHAR(1000000) NULL,
 	status 					CHAR DEFAULT 'D', 			/* E=enabled; D=disabled */
	production_version      INTEGER 	DEFAULT 0,		/* Zero means no production version set */
	test_version      		INTEGER		DEFAULT 0,		/* either test_version=production_version or test_version=production_version+1*/
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_documentstyle_name ON esf_library_documentstyle (library_id,lower(esfname));

/* 1:N relationship table for esf_library_documentstyle:esf_library_documentstyle_version */
CREATE TABLE esf_library_documentstyle_version
(
	id						UUID 		NOT NULL,
	library_documentstyle_id		UUID		NOT NULL,	    /* documentstyle this is a version of */
	version         		INTEGER 	DEFAULT 1,		/* sequence number starting with 1 */
	created_by_user_id		UUID		NOT NULL,
	last_updated_by_user_id	UUID		NOT NULL,
	created_timestamp		TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL,
	styles_record_blob_id			UUID 	NOT NULL, 		/* stores the styles as properties */
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_documentstyle_version ON esf_library_documentstyle_version (library_documentstyle_id,version);

/*
 * esf_library_dropdown: Defines a drop down/select list in the library.  Drop downs are maintained by version.  Drop downs have 1 or more option-value pairs.
 */
CREATE TABLE esf_library_dropdown
(
	id						UUID 		NOT NULL,
	container_id            UUID        NOT NULL,	    /* container is in, like library or document version */
	esfname            		VARCHAR(50) NOT NULL,
	description				VARCHAR(250)    NULL,
	comments                VARCHAR(1000000) NULL,
 	status 					CHAR DEFAULT 'D', 			/* E=enabled; D=disabled */
	production_version      INTEGER 	DEFAULT 0,		/* Zero means no production version set */
	test_version      		INTEGER		DEFAULT 0,		/* either test_version=production_version or test_version=production_version+1*/
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_dropdown_name ON esf_library_dropdown (container_id,lower(esfname));

/* 1:N relationship table for esf_library_dropdown:esf_library_dropdown_version */
CREATE TABLE esf_library_dropdown_version
(
	id						UUID 		NOT NULL,
	library_dropdown_id		UUID		NOT NULL,	    /* dropdown this is a version of */
	version         		INTEGER 	DEFAULT 1,		/* sequence number starting with 1 */
	created_by_user_id		UUID		NOT NULL,
	last_updated_by_user_id	UUID		NOT NULL,
	created_timestamp		TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL,
 	allow_multi_selection	CHAR DEFAULT 'N', 			/* Y=Yes; N=No - if yes, allows multiple options to be selected */
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_dropdown_version ON esf_library_dropdown_version (library_dropdown_id,version);

/* 1:N relationship table for esf_library_dropdown_version:esf_library_dropdown_version_option */
CREATE TABLE esf_library_dropdown_version_option
(
	id					UUID 		NOT NULL,
	library_dropdown_version_id	UUID	NOT NULL,	/* dropdown+version the choice is in */
	list_order			SMALLINT	DEFAULT 1,		/* order choice appears in dropdown starting with 1 */
	selection_option	VARCHAR(100) NOT NULL,	    /* the label to show for this choice */
	selection_value		VARCHAR(250) NOT NULL,      /* the value returned if the selection option is chosen */
	PRIMARY KEY ( id )
);

/*
 * esf_library_email_template: Defines an email template in the library.  They are maintained by version. 
 */
CREATE TABLE esf_library_email_template
(
	id						UUID 		NOT NULL,
	container_id			UUID		NOT NULL,	    /* container/library is in */
	esfname            		VARCHAR(50) NOT NULL,
	description				VARCHAR(250)    NULL,
	comments                VARCHAR(1000000) NULL,
 	status 					CHAR DEFAULT 'D', 			/* E=enabled; D=disabled */
	production_version      INTEGER 	DEFAULT 0,		/* Zero means no production version set */
	test_version      		INTEGER		DEFAULT 0,		/* either test_version=production_version or test_version=production_version+1*/
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_email_template_name ON esf_library_email_template (container_id,lower(esfname));

/* 1:N relationship table for esf_library_email_template:esf_library_email_template_version */
CREATE TABLE esf_library_email_template_version
(
	id						UUID 		NOT NULL,
	library_email_template_id UUID		NOT NULL,	    /* email template this is a version of */
	version         		INTEGER 	DEFAULT 1,		/* sequence number starting with 1 */
	created_by_user_id		UUID		NOT NULL,
	last_updated_by_user_id	UUID		NOT NULL,
	created_timestamp		TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL,
	email_from				VARCHAR(100) NULL,
	email_to				VARCHAR(4000) NULL,
	email_cc				VARCHAR(4000) NULL,
	email_bcc				VARCHAR(4000) NULL,
	email_subject			VARCHAR(200) NOT NULL,
	email_text				VARCHAR(16000) NOT NULL,
	email_html				VARCHAR(512000) NULL,
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_email_template_version ON esf_library_email_template_version (library_email_template_id,version);

/*
 * esf_library_file: Defines a file in the library, document or other container.  They are maintained by version. 
 */
CREATE TABLE esf_library_file
(
	id						UUID 		NOT NULL,
	container_id			UUID		NOT NULL,	    /* library or document version is in */
	esfname            		VARCHAR(50) NOT NULL,
	display_name			VARCHAR(100)    NULL,
	description				VARCHAR(250)    NULL,
	comments                VARCHAR(1000000) NULL,
 	status 					CHAR DEFAULT 'D', 			/* E=enabled; D=disabled */
	production_version      INTEGER 	DEFAULT 0,		/* Zero means no production version set */
	test_version      		INTEGER		DEFAULT 0,		/* either test_version=production_version or test_version=production_version+1*/
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_file_name ON esf_library_file (container_id,lower(esfname));

/* 1:N relationship table for esf_library_file:esf_library_file_version */
CREATE TABLE esf_library_file_version
(
	id						UUID 		NOT NULL,
	library_file_id		    UUID		NOT NULL,	    /* file this is a version of */
	version         		INTEGER 	DEFAULT 1,		/* sequence number starting with 1 */
	created_by_user_id		UUID		NOT NULL,
	last_updated_by_user_id	UUID		NOT NULL,
	created_timestamp		TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL,
	file_file_name			VARCHAR(256) NOT NULL,
	file_mime_type			VARCHAR(100) NOT NULL,
	file_blob_id			UUID 		NOT NULL, 		/* contains the non-compressed, non-encrypted file since files are public/transmitted and not secret */
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_file_version ON esf_library_file_version (library_file_id,version);

/*
 * esf_image: Defines an image in the library, document or other container.  They are maintained by version. 
 */
CREATE TABLE esf_library_image
(
	id						UUID 		NOT NULL,
	container_id			UUID		NOT NULL,	    /* library or document version is in */
	esfname            		VARCHAR(50) NOT NULL,
	description				VARCHAR(250)    NULL,
	comments                VARCHAR(1000000) NULL,
 	status 					CHAR DEFAULT 'D', 			/* E=enabled; D=disabled */
	production_version      INTEGER 	DEFAULT 0,		/* Zero means no production version set */
	test_version      		INTEGER		DEFAULT 0,		/* either test_version=production_version or test_version=production_version+1*/
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_image_name ON esf_library_image (container_id,lower(esfname));

/* 1:N relationship table for esf_library_image:esf_library_image_version */
CREATE TABLE esf_library_image_version
(
	id						UUID 		NOT NULL,
	library_image_id		UUID		NOT NULL,	    /* image this is a version of */
	version         		INTEGER 	DEFAULT 1,		/* sequence number starting with 1 */
	created_by_user_id		UUID		NOT NULL,
	last_updated_by_user_id	UUID		NOT NULL,
	created_timestamp		TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL,
	image_file_name			VARCHAR(256) NOT NULL,
	image_mime_type			VARCHAR(100) NOT NULL,
	image_blob_id			UUID 		NOT NULL, 		/* contains the non-compressed, non-encrypted image since most images are compressed data and are public/transmitted and not secret */
	thumbnail_blob_id		UUID 		NOT NULL, 		/* contains the non-compressed, non-encrypted thumbnail image */
 	use_data_uri			CHAR        DEFAULT 'N',    /* N=No, just use regular img src; Y=Yes, use img data uri */
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_image_version ON esf_library_image_version (library_image_id,version);



/*
 * esf_library_image_version_overlay_field: Defines a set field templates that are considered to overlay a 
 * given document image version. The positions are "absolute" relative to the specified image.
 * N:1 relationship table for esf_library_image_version_overlay_field:esf_library_image_version
 * 1:1 relationship table for esf_library_image_version_overlay_field:esf_field_template
 */
CREATE TABLE esf_library_image_version_overlay_field
(
	library_image_version_id	UUID 		NOT NULL,		/* document image version that we overlay */
	field_template_id		 	UUID		NOT NULL,	    /* field template defined in the document */
	position_left				SMALLINT 	DEFAULT 0,
	position_top				SMALLINT 	DEFAULT 0,
	position_width				SMALLINT 	DEFAULT 50,
	position_height				SMALLINT 	DEFAULT 20,
	display_mode				CHAR 		DEFAULT 'F',    /* F=Field, L=Field+Label, O=Output only */
	background_color			VARCHAR(50) NULL,
	PRIMARY KEY ( library_image_version_id, field_template_id )
);


/*
 * esf_library_propertyset: Defines a set of name-value properties in the library.  They are maintained by version.
 */
CREATE TABLE esf_library_propertyset
(
	id						UUID 		NOT NULL,
	container_id	        UUID        NOT NULL,	    /* container is in, such as library or document version */
	esfname            		VARCHAR(50) NOT NULL,
	description				VARCHAR(250)    NULL,
	comments                VARCHAR(1000000) NULL,
 	status 					CHAR DEFAULT 'D', 			/* E=enabled; D=disabled */
	production_version      INTEGER 	DEFAULT 0,		/* Zero means no production version set */
	test_version      		INTEGER		DEFAULT 0,		/* either test_version=production_version or test_version=production_version+1*/
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_propertyset_name ON esf_library_propertyset (container_id,lower(esfname));

/* 1:N relationship table for esf_library_propertyset:esf_library_propertyset_version */
CREATE TABLE esf_library_propertyset_version
(
	id						UUID 		NOT NULL,
	library_propertyset_id  UUID        NOT NULL,	    /* propertyset this is a version of */
	version         		INTEGER 	DEFAULT 1,		/* sequence number starting with 1 */
	created_by_user_id		UUID		NOT NULL,
	last_updated_by_user_id	UUID		NOT NULL,
	created_timestamp		TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL,
	num_properties     		INTEGER 	DEFAULT 0,		/* number of properties stored in our blob -- tracked here for easy listings without retrieving the blob */
	properties_blob_id		UUID 		NOT NULL, 		/* contains the properties */
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_propertyset_version ON esf_library_propertyset_version (library_propertyset_id,version);

/*
  esf_serial: defines a serial number generator in a library. Unlike most components, this cannot have multiple versions, but it will keep 
  track of numbers based on whether in test or production modes.
*/
CREATE TABLE esf_library_serial 
(
	id            	UUID        NOT NULL,
	container_id    UUID        NOT NULL,	        /* container/library defined in */
	esfname         VARCHAR(50) NOT NULL,
	description     VARCHAR(250)    NULL,
	comments        VARCHAR(1000000) NULL,
 	status 					CHAR DEFAULT 'D', 			/* E=enabled; D=disabled */
	production_version      INTEGER         DEFAULT 0,		/* Zero means no production version set */
	test_version      		INTEGER         DEFAULT 0,		/* either test_version=production_version or test_version=production_version+1*/
	next_production_serial  BIGINT NOT NULL DEFAULT 0,      /* next production serial number, data value do not change by version */
	next_test_serial    	BIGINT NOT NULL DEFAULT 0,      /* next test serial number, data value do not change by version */
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_serial_name ON esf_library_serial (container_id,lower(esfname));

/* 1:N relationship table for esf_library_serial:esf_library_serial_version */
CREATE TABLE esf_library_serial_version
(
	id                      UUID        NOT NULL,
	library_serial_id       UUID        NOT NULL,	    /* file this is a version of */
	version                 INTEGER     DEFAULT 1,		/* sequence number starting with 1 */
	created_by_user_id      UUID        NOT NULL,
	last_updated_by_user_id UUID        NOT NULL,
	created_timestamp       TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated_timestamp  TIMESTAMP WITH TIME ZONE NOT NULL,
	decimal_format          VARCHAR(20) NOT NULL DEFAULT '###########0',
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_serial_version ON esf_library_serial_version (library_serial_id,version);


/**
 * esf_outbound_email_message contains email messages queued and then sent out by our email system.
 */
CREATE TABLE esf_outbound_email_message
(
	id						UUID 		NOT NULL,
	transaction_id			UUID		NULL,
	transaction_party_id	UUID		NULL,
	user_id	                UUID		NULL,
	created_timestamp		TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	sent_timestamp			TIMESTAMP WITH TIME ZONE NULL,
	bounce_correlation_code CHAR(20) NOT NULL, /* always lowercase */
	send_status				VARCHAR(8000) NULL, /* log max length */
	email_from				VARCHAR(100) NULL,
	email_to				VARCHAR(4000) NULL,
	email_cc				VARCHAR(4000) NULL,
	email_bcc				VARCHAR(4000) NULL,
	email_subject			VARCHAR(200) NOT NULL,
	email_text				VARCHAR(16000) NOT NULL,
	email_html				VARCHAR(512000) NULL,
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_outbound_email_message_bounce ON esf_outbound_email_message (bounce_correlation_code);
CREATE INDEX esfidx_outbound_email_message_sent ON esf_outbound_email_message (sent_timestamp);
CREATE INDEX esfidx_outbound_email_message_tid ON esf_outbound_email_message (transaction_id,created_timestamp);
CREATE INDEX esfidx_outbound_email_message_tpid ON esf_outbound_email_message (transaction_party_id,created_timestamp);
CREATE INDEX esfidx_outbound_email_message_uid ON esf_outbound_email_message (user_id,created_timestamp);

/*
 * esf_outbound_email_message_attachment: Defines a file attachment for the specified outbound email.
 * 1:N relationship table of esf_outbound_email_message:esf_outbound_email_message_attachment
 */
CREATE TABLE esf_outbound_email_message_attachment
(
	id   						UUID NOT NULL, /* id of attached file */
 	outbound_email_message_id	UUID NOT NULL, /* outbound email message it's associated with */
 	file_order					SMALLINT DEFAULT 1,
	file_name					VARCHAR(256) NOT NULL, /* file name as uploaded */
	file_mime_type				VARCHAR(100) NOT NULL, /* file's mime type */
	file_blob_id				UUID NOT NULL, /* contains the compressed, encrypted file */
	PRIMARY KEY (id)
);
CREATE INDEX esfidx_outbound_email_message_attachment_eid ON esf_outbound_email_message_attachment (outbound_email_message_id,file_order);

/**
 * esf_outbound_email_message_response contains email messages retrieved that are correlated to an outbound email sent
 * 1:N esf_outbound_email_message:esf_outbound_email_message_response
 */
CREATE TABLE esf_outbound_email_message_response
(
	outbound_email_message_id	UUID NOT NULL,
	received_timestamp			TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	email_from				VARCHAR(100) NULL,
	email_to				VARCHAR(4000) NULL,
	email_subject			VARCHAR(200) NULL,
	email_text				VARCHAR(16000) NULL,
	email_html				VARCHAR(512000) NULL,
	PRIMARY KEY ( outbound_email_message_id,received_timestamp )
);


/*
 * esf_package: Defines a package of document(s).  Packages are maintained by version.  Packages have 1 or more documents
 * that can span across libraries.  It also has parties that are mapped to the specific parties defined in those documents.
 */
CREATE TABLE esf_package
(
	id                      UUID NOT NULL,
	path_name               VARCHAR(1024) NOT NULL,
	description             VARCHAR(250) NULL,
	download_file_name_spec VARCHAR(256) NULL, 		/* an optional field spec/literal to use for the download filename */
	comments                VARCHAR(1000000) NULL,
 	status                  CHAR DEFAULT 'D', 		/* E=enabled; D=disabled */
	production_version      INTEGER DEFAULT 0,		/* Zero means no production version set */
	test_version            INTEGER DEFAULT 0,		/* either test_version=production_version or test_version=production_version+1*/
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_package_pathname ON esf_package (lower(path_name));

/* 1:N relationship table for esf_package:esf_package_version */
CREATE TABLE esf_package_version
(
	id						UUID 		NOT NULL,
	package_id				UUID		NOT NULL,	    /* package this is a version of */
	version         		INTEGER 	DEFAULT 1,		/* sequence number starting with 1 */
	created_by_user_id		UUID		NOT NULL,
	last_updated_by_user_id	UUID		NOT NULL,
	package_document_id		UUID 		NOT NULL,       /* the document that plays the "package display" role -- not a document in the package */
	button_message_id		UUID 		NOT NULL,       /* the button message set to use for this package -- can be overridden by a branding library button message set with the same name. */
	programming_blob_id    	UUID        NULL,           /* contains extra programming options */
	created_timestamp		TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL,
	num_documents           INTEGER     DEFAULT 0,
	num_parties             INTEGER     DEFAULT 0,
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_package_version ON esf_package_version (package_id,version);

/* 1:N relationship table for esf_package_version:esf_package_version_document */
CREATE TABLE esf_package_version_document
(
	package_version_id		UUID		NOT NULL,	    /* package version the document is in */
	library_document_id		UUID		NOT NULL,	    /* document referenced */
	document_number         SMALLINT    DEFAULT 1,		/* order of documents */
	PRIMARY KEY ( package_version_id,library_document_id )
);
CREATE UNIQUE INDEX esfidx_package_version_document_docnum ON esf_package_version_document (package_version_id,document_number);

/*
 * esf_packageparty_template: Defines a template of a party in a package.
 * 1:N relationship table for esf_package_version:esf_package_version_party_template
 */
CREATE TABLE esf_package_version_party_template
(
	id   					UUID NOT NULL,
 	package_version_id		UUID NOT NULL,				/* package version this party is defined in */
	process_order			SMALLINT	DEFAULT 1,		/* order parties tend to process in, starting with 1 */
	esfname            		VARCHAR(50) NOT NULL,
	display_name			VARCHAR(100) NOT NULL,
 	library_party_template_todo_esfname VARCHAR(50) NULL, /* when set, the party defined in a library will be checked for the ToDo Group permission for this party */
 	library_email_template_esfname VARCHAR(50) NULL, /* when set, the email notification name to use for inviting to retrieve the package */
	email_field_expression  VARCHAR(4096) NULL, /* an optional email address or field spec that expands to one */
	completed_url_field_expression VARCHAR(4096) NULL, /* an optional URL field spec to redirect this party to when they complete all documents */
	notcompleted_url_field_expression VARCHAR(4096) NULL, /* an optional URL field spec to redirect this party to if they decide not to complete all documents */
	renotify_spec  			VARCHAR(100) NULL, /* an optional renotification spec, with multiple separated by semicolons based on a system drop down */
	require_login			CHAR DEFAULT 'N', /* Y=login required; N=login not required */
	allow_delete_tran_if_unsigned CHAR DEFAULT 'N', /* Y=delete tran allowed (if no signatures); N=delete tran not allowed */
	notify_all_todo			CHAR DEFAULT 'N', /* Y=notify all ToDo party group; N=do not notify any users in the ToDo party's group  */
	landing_page			CHAR DEFAULT 'P', /* P=land on package+disclosure; 1=First document; 2=First To Do, but if none, first document; 3=First To Do, but if none, package+disclosure */
	override_package_document_id	UUID NULL,  /* the optional package-overriding document that plays the "package display" role -- not a document in the package -- for this party */
	override_button_message_id		UUID NULL,  /* the optional package-overriding button message set to use for this package party -- can be overridden by a branding library button message set with the same name. */
	PRIMARY KEY (id)
);
CREATE UNIQUE INDEX esfidx_package_version_party_template_name ON esf_package_version_party_template (package_version_id,lower(esfname));


/*
 * esf_package_version_party_document_party: Defines the relationship of a party template defined in a package to the one more document parties.
 * If the party_esfname is not set, it implies "non-party" access to that document.
 * 1:N relationship table for esf_package_version_party_template:esf_party_template (with container id being a document version id)
 */
CREATE TABLE esf_package_version_party_document_party
(
	package_party_template_id 				UUID NOT NULL,
	document_id								UUID NOT NULL,
	document_party_template_esfname			VARCHAR(50) NOT NULL, /* Two special values: ESF_partyname_view_only and ESF_partyname_view_optional */
    snapshot_document_status				CHAR DEFAULT 'D', /* E=enable snapshot document on review; D=don't snapshot document */
    snapshot_data_status				    CHAR DEFAULT 'D'  /* E=enable snapshot data in document; D=don't snapshot data */
);
CREATE UNIQUE INDEX esfidx_package_version_party_document_party_pkey ON esf_package_version_party_document_party (package_party_template_id,document_id,lower(document_party_template_esfname));

/*
 * Maps fields from a package version's document's field template to a report field template
 */
CREATE TABLE esf_package_version_report_field
(
	id 							UUID NOT NULL,
	package_version_id 			UUID NOT NULL,
	document_id 				UUID NOT NULL,
	field_esfname 			    VARCHAR(50) NOT NULL,  /* field name; use name instead of 'id' because as field id changes with each new document version */
	report_field_template_id 	UUID NOT NULL, /* esf_report_field_template.id */
	field_order 				SMALLINT DEFAULT 1,
	show_to_do 					CHAR DEFAULT 'N', /* Is field shown in To Do listing: Y=Yes; N=No */
	PRIMARY KEY (id)
);

/*
 * esf_party_template: Defines a template of a party to a document or library.  At the Library level it's used as 'create like templates' 
 * for references in documents.
 */
CREATE TABLE esf_party_template
(
	id   					UUID NOT NULL,
 	container_id			UUID NOT NULL,
	process_order			SMALLINT	DEFAULT 1,		/* order parties tend to process in, starting with 1 */
	esfname            		VARCHAR(50) NOT NULL,
	display_name			VARCHAR(100) NOT NULL,
	todo_group_id			UUID NULL, /* optional group for PRODUCTION ToDo processing if the package party template to associate with the package party; generally for ToDo permissions for production transactions */
	test_todo_group_id		UUID NULL, /* optional group for TEST ToDo processing if the package party template to associate with the package party; generally for ToDo permissions for test transactions */
	PRIMARY KEY (id)
);
CREATE UNIQUE INDEX esfidx_party_template_name ON esf_party_template (container_id,lower(esfname));

/*
 * esf_party_template_field: Defines the relationship of a party template to a field template giving them update access 
 * to those fields.
 */
CREATE TABLE esf_party_template_field_template
(
	party_template_id 		UUID NOT NULL,
	field_template_id 		UUID NOT NULL,
	keep_required           CHAR DEFAULT 'Y', /* Y=KeepRequired; N=OverrideAsOptional -- for this party only, used to turn off esf_field_template.required (has no meaning unless the field itself is required) */
	PRIMARY KEY (party_template_id,field_template_id)
);
CREATE INDEX esfidx_party_template_field_template_fid ON esf_party_template_field_template (field_template_id);

/*
 * esf_permission: Defines a permission, which is really just a group of permission options.
 */
CREATE TABLE esf_permission
(
	id					UUID NOT NULL,
	path_name			VARCHAR(1024) NOT NULL,
	status 				CHAR DEFAULT 'D', /* E=enabled; D=disabled */
	description			VARCHAR(250),
	comments			VARCHAR(1000000),
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_perm_path ON esf_permission (lower(path_name));


/*
 * esf_permission_option: Defines a permission option, the item actually secures
 * 		a specific capability (permission functions like a group of permission options).
 *  	All users in the 'allowed groups' of this option are authorized.
 *      Because we want 'lower' on the option name, we can't use PRIMARY KEY to define it
 */
CREATE TABLE esf_permission_option
(
	permission_id		UUID NOT NULL,
	esfname				VARCHAR(50) NOT NULL,
	description			VARCHAR(250)
);
CREATE UNIQUE INDEX esfidx_permission_option_pkey ON esf_permission_option (permission_id,lower(esfname));

/*
 * esf_permission_option_group: Defines a group that is authorized (allowed) to do the 
 * 	specific permission option.  Because we want 'lower' on the option name, we can't use PRIMARY KEY to define it.
 */
CREATE TABLE esf_permission_option_group
(
	permission_id				UUID NOT NULL,
	permission_option_esfname	VARCHAR(50) NOT NULL,
	group_id					UUID NOT NULL
);
CREATE UNIQUE INDEX esfidx_permission_option_group_pkey ON esf_permission_option_group (permission_id,lower(permission_option_esfname),group_id);
CREATE INDEX esfidx_permission_option_group_id ON esf_permission_option_group (group_id);


/* Specifies a field that can be reported on. Fields in documents that need to be reported are defined to point to one of these
 * specifications, and the reports only use these fields (and no longer know the source of the data itself, which varies over documents and versions).
 */
CREATE TABLE esf_report_field_template
(
	id 						UUID NOT NULL,	    
	field_esfname 			VARCHAR(50) NOT NULL,  /* name of report field; those with "esf_" prefix are reserved for built-in fields */
	field_type 				CHAR DEFAULT 'S', /* B=built-in; S=String; I=Integer; N=Decimal/Numeric/Money; D=Date; 1=Numbers-only string; Z=Alpha-numeric-only string; F=File uploads */
	field_label 			VARCHAR(100) NULL,
	field_tooltip 			VARCHAR(100) NULL,
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_report_field_template_name ON esf_report_field_template(lower(field_esfname));

/*
 * esf_report_template: Defines a template of a report that basically shows transactions of the specified type.
 * A disabled report template will not be listed or cannot be used to view data, export CSV or download transaction archives.
 * 1:N relationship table esf_report_template:esf_transaction_template
 * N:N relationship table esf_report_template:esf_report_field_template
 */
CREATE TABLE esf_report_template
(
	id   				UUID NOT NULL,
	created_timestamp 	TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	created_by_user_id	UUID NOT NULL, /* will be esf_deployment.id for 'system template' */
	last_updated_timestamp 	TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	last_updated_by_user_id	UUID NOT NULL,
	path_name 			VARCHAR(1024) NOT NULL,
	display_name 		VARCHAR(100) NOT NULL,
	description 		VARCHAR(250) NULL,
	comments            VARCHAR(1000000) NULL,
 	status 				CHAR DEFAULT 'D', /* E=enabled; D=disabled */
	PRIMARY KEY (id)
);
CREATE UNIQUE INDEX esfidx_report_template_pathname ON esf_report_template (lower(path_name));

/* N:N relationship table for esf_report_template:esf_library_document */
CREATE TABLE esf_report_template_limit_document
(
	report_template_id			UUID NOT NULL,	    
	library_document_id			UUID NOT NULL,	    /* report is limited to provide access to this document */
	PRIMARY KEY ( report_template_id,library_document_id )
);

/* N:N relationship table for esf_report_template:esf_transaction_template */
CREATE TABLE esf_report_template_transaction_template
(
	report_template_id			UUID NOT NULL,	    
	transaction_template_id		UUID NOT NULL,	    /* report can list this transaction template's transactions */
	PRIMARY KEY ( report_template_id,transaction_template_id )
);

/**
 * Defines the report fields that appear in the report
 */
CREATE TABLE esf_report_template_report_field
(
	report_template_id       UUID NOT NULL,
	report_field_template_id UUID NOT NULL,
	field_order              SMALLINT DEFAULT 1,
	field_label              VARCHAR(100) NOT NULL, /* label to show for column header */
	output_format_spec       VARCHAR(100) NULL, /* Varies on data type, but can be used to specify any formatting rules for this field on output */
	allow_search             CHAR DEFAULT 'N', /* Can the user search using this field: Y=Yes; N=No */
	PRIMARY KEY ( report_template_id,report_field_template_id,field_order )
);
CREATE INDEX esfidx_report_template_report_field_rfti ON esf_report_template_report_field (report_field_template_id);

/*
  esf_session_key: defines the common use symmetric keys, including the boot key and the keys
  	used to encrypt the private keys.  The boot key is also stored in this file, though
  	it's obviously not encrypted with the boot key, but is PBE encrypted with the dual
  	passwords specified at application startup WHERE session_key.id = deployment.id
*/
CREATE TABLE esf_session_key 
(
	id          		UUID NOT NULL, 						/* unique key id */
	created_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL, 	/* datetime the key was created */
	key_data			VARCHAR(200) NOT NULL,    			/* Encryption key, encrypted with boot key, base64 encoded */
	PRIMARY KEY ( id )
);


/*
  esf_signature_key: defines the common use asymmetric keys used for digital signing. It is basically valid
  from the created_timestamp to its valid_through_timestamp.
*/
CREATE TABLE esf_signature_key
(
	id          		UUID NOT NULL, 						/* unique key id */
	session_key_id		UUID NOT NULL,						/* the session key used to encrypt the private key */
	created_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL, 	/* datetime the keypair was created */
	deactivation_timestamp	TIMESTAMP WITH TIME ZONE NULL, 	/* datetime the keypair was deactivated; null if active */
    x509_certificate_data VARCHAR(4000) NOT NULL,    		/* Self-signed X.509 cert for the public key, base64 encoded */
	public_key_data		VARCHAR(4000) NOT NULL,    			/* Public key, X509, base64 encoded */
	private_key_data	VARCHAR(4000) NOT NULL,    			/* Private key, PKCS8, encrypted, base64 encoded */
	PRIMARY KEY ( id )
);


/*
 * esf_stats: tracks the size of the database and related stats over time
 */
CREATE TABLE esf_stats
(
  collection_timestamp  TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
  db_size               BIGINT NOT NULL,
  archive_size          BIGINT NOT NULL,
  num_transactions		INTEGER NOT NULL,
  num_groups			INTEGER NOT NULL,
  num_users				INTEGER NOT NULL,
  PRIMARY KEY (collection_timestamp)
);


/*
  esf_system_activity_log: Defines a log record associated with the system overall.
  We broke some of these case out from our general activity log so that no so much
  logging activity occurs on a single table.
  This table reserves the log_types where the absolute value of log_type (|log_type|) is in the range 1-99.
  
  log_type: 
  			Values < 0 mean the record is permanent and will not be aged off over time
  			Values > 0 mean the record is non-permanent and can be aged off over time
  			Value == 0 is currently undefined.
  			|value| == 1 is a system install or version update record; these are always permanent and generally are few
  			|value| == 2 is a system start/stop record
  			|value| == 3 is a user activity record
  			|value| == 4 is a configuration modification record
*/
CREATE TABLE esf_system_activity_log 
(
	log_timestamp       TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	log_type            SMALLINT                 DEFAULT 1,
	log_text            VARCHAR(8000) NOT NULL
);
CREATE INDEX esfidx_system_activity_log_time ON esf_system_activity_log (log_timestamp);


/*
 * esf_transaction: Defines a transaction that has been started.  
 * 1:1 relationship table esf_transaction:esf_transaction_template
 */
CREATE TABLE esf_transaction
(
	id   					UUID NOT NULL,
 	transaction_template_id	UUID NOT NULL,
	package_id				UUID NOT NULL, /* package assigned from transaction template at startup */
	package_version_id		UUID NOT NULL, /* package version assigned from transaction template at startup */
 	brand_library_id		UUID NULL, /* Optional brand Library assigned from transaction template at startup */
	created_timestamp 		TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	created_by_user_id		UUID, /* null if created by a non-user */
	last_updated_timestamp 	TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	last_updated_by_user_id	UUID, /* null if last updated by a non-user */
	stall_timestamp 		TIMESTAMP WITH TIME ZONE NULL, /* when set, implies this transaction has 'stalled' in that there's no active party, but it's not completed/canceled */
	expire_timestamp 		TIMESTAMP WITH TIME ZONE NULL, /* when set, the transaction will be removed once it's older than this date */
	cancel_timestamp 		TIMESTAMP WITH TIME ZONE NULL, /* when set, the transaction will be canceled once it's older than this date if it's in progress */
 	tran_type				CHAR DEFAULT 'T', /* T=Test; t=TestProduction; P=Production */
 	status 					CHAR DEFAULT 'S', /* X=Canceled; C=Completed; I=In-progress; S=Suspended */
 	status_text             VARCHAR(50) NULL, /* often the last party to do work on the tran, but just status to show in reports */
 	cancel_retention_spec   VARCHAR(20) NULL, /* if set and transaction is canceled, a new expire_timestamp will be set based on this value so long as it doesn't extend the retention */
	record_id				UUID NOT NULL, 	
	PRIMARY KEY (id)
);
CREATE INDEX esfidx_transaction_updatedtime ON esf_transaction (last_updated_timestamp,transaction_template_id,tran_type,status);
CREATE INDEX esfidx_transaction_canceltime ON esf_transaction (cancel_timestamp,status);
CREATE INDEX esfidx_transaction_exptime ON esf_transaction (expire_timestamp);

/*
  esf_transaction_activity_log: Defines a log record associated with the specified transaction.
  We use the log_type range of 200-299.  Transaction log records are kept as long as the transaction is.
  
  log_type: 
  			value == 200 general activity related to the transaction, like starting, stopping, change party
  			value == 250 basic action trace
  			value == 251 detailed action trace
*/
CREATE TABLE esf_transaction_activity_log 
(
	transaction_id     	UUID NOT NULL,
	log_timestamp       TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	log_type            SMALLINT                 DEFAULT 1,
	log_text            VARCHAR(8000) NOT NULL
);
CREATE INDEX esfidx_transaction_activity_log_timetype ON esf_transaction_activity_log (transaction_id,log_timestamp,log_type);


/*
 * esf_transaction_document: Defines a runtime document in a transaction, with pointers to its various library templates
 * 1:N relationship table of esf_transaction:esf_transaction_document
 * 1:1 relationship table esf_transaction_document:esf_library_document_version
 */
CREATE TABLE esf_transaction_document
(
	id   						UUID NOT NULL,
 	transaction_id				UUID NOT NULL,
	library_document_id			UUID NOT NULL,
	library_document_version_id	UUID NOT NULL,
	record_id					UUID NOT NULL,
	PRIMARY KEY (id)
);
CREATE UNIQUE INDEX esfidx_transaction_document_tiddid ON esf_transaction_document (transaction_id,library_document_id);


/*
 * esf_transaction_file: Defines a file that has been attached to a transaction.
 * 1:N relationship table of esf_transaction:esf_transaction_file
 * 1:N relationship table esf_transaction_document:esf_transaction_file
 */
CREATE TABLE esf_transaction_file
(
	id   						UUID NOT NULL, /* id of file */
 	transaction_id				UUID NOT NULL, /* transaction it's associated with */
	transaction_document_id		UUID NOT NULL, /* document in the transaction it was uploaded to */
	transaction_party_id		UUID NOT NULL, /* the id of the transaction party who uploaded it */
	upload_timestamp			TIMESTAMP WITH TIME ZONE NOT NULL, /* when it was uploaded */
	field_name					VARCHAR(50) NOT NULL, /* the field's esfname on the document that is associated with the file */
	file_name					VARCHAR(256) NOT NULL, /* file name as uploaded */
	file_mime_type				VARCHAR(100) NOT NULL, /* file's mime type */
	file_blob_id				UUID NOT NULL, /* contains the compressed, encrypted file */
	PRIMARY KEY (id)
);
CREATE INDEX esfidx_transaction_file_tid ON esf_transaction_file (transaction_id);

/*
 * esf_transaction_party: Defines a runtime party in a transaction, with pointers to its package party
 * 1:N relationship table of esf_transaction:esf_transaction_party
 * 1:1 relationship table of esf_transaction_party:esf_package_version_party_template
 */
CREATE TABLE esf_transaction_party
(
	id   						UUID NOT NULL,
 	transaction_id				UUID NOT NULL,
	package_version_party_template_id	UUID NOT NULL,
	prev_transaction_party_id	UUID NULL, /* when set, this is the party that came just before this one */
	next_transaction_party_id	UUID NULL, /* when set, this is the party comes after this one */
	todo_group_id				UUID NULL, /* if set, members of this group can see this party step via To Do */
	library_email_template_name VARCHAR(50) NULL, /* if set, this is the email template currently set for this party */
	transaction_party_assignment_pickup_code CHAR(20) NULL, /* when set, points to the current esf_transaction_party_assignment.pickup_code */
	status                      CHAR DEFAULT 'U', /* U=Not yet activated/undefined; c=Created; S=Skipped; A=Active; C=Completed; r=ESF_reports_access */
	transaction_status          CHAR DEFAULT 'S', /* Not really part of this object, but speeds ToDo queries. Basically it's updated by the Transaction.save() -- not a party. */
	PRIMARY KEY (id)
);
CREATE UNIQUE INDEX esfidx_transaction_party_tidpid ON esf_transaction_party (transaction_id,package_version_party_template_id);
CREATE UNIQUE INDEX esfidx_transaction_party_pickup ON esf_transaction_party (transaction_party_assignment_pickup_code);
CREATE INDEX esfidx_transaction_party_todo ON esf_transaction_party (transaction_status,status);

/*
 * esf_transaction_party_assignment represents a given transaction party that's been assigned.  If the party is transferred
 * to another person, another assignment record is created and the original is marked as transferred so its pickup code can't be used again.
 * This tracks a given party if the email address or the like is moved along so more than one person handles the party step.
 * 1:N relationship of table esf_transaction_party:esf_transaction_party_assignment (1:1 by default if not transferred)
 */
CREATE TABLE esf_transaction_party_assignment
(
    transaction_party_id        UUID NOT NULL, /* the esf_transaction_party.id */
    user_id        				UUID NULL, /* if set, this party was played by a logged in user */
	pickup_code					CHAR(20) NOT NULL,
	email_address				VARCHAR(100) NULL, /* the lowercase email address if known */
	status                      CHAR DEFAULT 'U', /* U=Not yet activated/undefined; A=Activated; R=Retrieved; X=Rejected (retrieved, but sent to another party so could be activated again); C=Completed; T=Transferred; S=Skipped */
	created_timestamp 		    TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	last_accessed_timestamp 	TIMESTAMP WITH TIME ZONE NULL,
	PRIMARY KEY (pickup_code)
);
CREATE INDEX esfidx_transaction_party_assignment_todo ON esf_transaction_party_assignment (user_id,email_address);


/*
 * esf_transaction_party_document: Tracks the status of each document assigned to a given party.
 * 1:N relationship of table esf_transaction_party:esf_transaction_party_document (each party can be assigned 1 or more documents)
 * 1:N relationship of table esf_transaction_document:esf_transaction_party_document (each document can be assigned to 1 or more parties)
 */
CREATE TABLE esf_transaction_party_document
(
	transaction_party_id		UUID NOT NULL,
 	transaction_document_id		UUID NOT NULL,
	last_updated_timestamp 	    TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	document_number         	SMALLINT DEFAULT 1, /* order the document is processed by the specified party */
	status						CHAR DEFAULT 'U', /* U=Undefined/Not yet retrieved; V=ViewOptional/Unread But Completed; v=View Optional Viewed and Completed; R=Retrieved/read; F=Fix requested; C=Completed; X=Rejected; S=Skipped */
	snapshot_document_skipped	CHAR DEFAULT 'N', /* N=No; Y=Yes */
	snapshot_blob_id			UUID NULL, /* optional snapshot done */
	snapshot_timestamp 	        TIMESTAMP WITH TIME ZONE NULL, /* if has a snapshot, this is when it was done */
	document_file_name          VARCHAR(256) NULL,
	esign_ip_host_addr			VARCHAR(200) NULL,
	esign_timestamp				TIMESTAMP WITH TIME ZONE NULL,
	PRIMARY KEY (transaction_party_id,transaction_document_id)
);

CREATE TABLE esf_transaction_party_renotify
(
 	notify_timestamp  			TIMESTAMP WITH TIME ZONE NOT NULL,
 	transaction_party_id		UUID NOT NULL,
 	PRIMARY KEY (notify_timestamp,transaction_party_id)
);
CREATE INDEX esfidx_transaction_party_renotify_tpid ON esf_transaction_party_renotify(transaction_party_id);

/*
 * esf_transaction_template: Defines a template of a transaction that basically runs the documents in a Package.  
 * A disabled transaction template will not be listed or cannot be used to start a new transaction.
 * 1:1 relationship table esf_transaction_template:esf_package
 * 1:1 relationship table esf_transaction_template:esf_library
 */
CREATE TABLE esf_transaction_template
(
	id   				UUID NOT NULL,
	created_timestamp 	TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	created_by_user_id	UUID NOT NULL, /* will be esf_deployment.id for 'system template' */
	last_updated_timestamp 	TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	last_updated_by_user_id	UUID NOT NULL,
	path_name 			VARCHAR(1024) NOT NULL,
	display_name 		VARCHAR(100) NOT NULL,
	description 		VARCHAR(250) NULL,
	comments            VARCHAR(1000000) NULL,
	production_status 	CHAR DEFAULT 'D', /* For use in production transactions: E=enabled; D=disabled */
	production_retention_spec VARCHAR(20), /* Something '1234 minute' or '18 month' or '0 forever' */
	test_status 		CHAR DEFAULT 'E', /* For use in test transactions: E=enabled; D=disabled */
	test_retention_spec VARCHAR(20),
	package_id			UUID NOT NULL,
 	brand_library_id	UUID NULL, /* Optional brand Library to search before the document's library, and the template library as a last resort. */
	PRIMARY KEY (id)
);
CREATE UNIQUE INDEX esfidx_transaction_template_pathname ON esf_transaction_template (lower(path_name));

/**
 * Various esf_tran_report_field_string/long/numeric/date tables to store the report values 
 * for the report field types we support.
 */
CREATE TABLE esf_tran_report_field_string
(
  transaction_id			UUID NOT NULL,
  report_field_template_id	UUID NOT NULL,
  field_value         		VARCHAR(4000) NULL,
  PRIMARY KEY (transaction_id,report_field_template_id)
);
CREATE INDEX esfidx_tran_report_field_string_value ON esf_tran_report_field_string (report_field_template_id,lower(field_value));

CREATE TABLE esf_tran_report_field_long
(
  transaction_id			UUID NOT NULL,
  report_field_template_id	UUID NOT NULL,
  field_value         		BIGINT NULL,
  PRIMARY KEY (transaction_id,report_field_template_id)
);
CREATE INDEX esfidx_tran_report_field_long_value ON esf_tran_report_field_long (report_field_template_id,field_value);

CREATE TABLE esf_tran_report_field_numeric
(
  transaction_id			UUID NOT NULL,
  report_field_template_id	UUID NOT NULL,
  field_value         		NUMERIC(15,2) NULL,
  PRIMARY KEY (transaction_id,report_field_template_id)
);
CREATE INDEX esfidx_tran_report_field_numeric_value ON esf_tran_report_field_numeric (report_field_template_id,field_value);

CREATE TABLE esf_tran_report_field_date
(
  transaction_id			UUID NOT NULL,
  report_field_template_id	UUID NOT NULL,
  field_value         		DATE NULL,
  PRIMARY KEY (transaction_id,report_field_template_id)
);
CREATE INDEX esfidx_tran_report_field_date_value ON esf_tran_report_field_date (report_field_template_id,field_value);

/*
 * esf_tran_report_field_tranfileid is unlike the other report fields in that it cannot be used to search
 * but is used to map files uploaded into documents, and those fields are mapped to a report field of this type,
 * so that we can then display a list of files directly in the custom report.
 */
CREATE TABLE esf_tran_report_field_tranfileid
(
  transaction_id			UUID NOT NULL,
  report_field_template_id	UUID NOT NULL,
  tran_file_id         		UUID NOT NULL,
  PRIMARY KEY (transaction_id,report_field_template_id,tran_file_id)
);
CREATE INDEX esfidx_tran_report_field_tranfileid_rftid ON esf_tran_report_field_tranfileid (report_field_template_id);


/**
 * esf_transaction_timer holds information about a timer that has yet to expire for a given transaction.
 * 1:N esf_transaction:esf_transaction_timer
 */
CREATE TABLE esf_transaction_timer
(
	transaction_id          UUID NOT NULL,	    
	timer_name              VARCHAR(100) NOT NULL,
	expire_timestamp        TIMESTAMP WITH TIME ZONE DEFAULT NOW()
);
CREATE UNIQUE INDEX esfidx_transaction_timer_idname ON esf_transaction_timer (transaction_id,lower(timer_name));
CREATE INDEX esfidx_transaction_timer_time ON esf_transaction_timer (expire_timestamp);


/*
 * esf_user: Defines a user of the system.
 */
CREATE TABLE esf_user
(
	id   					UUID NOT NULL,
	created_timestamp 		TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	created_by_user_id		UUID NOT NULL,
	last_updated_timestamp 	TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	last_updated_by_user_id	UUID NOT NULL,
	email 					VARCHAR(100) NOT NULL,
	display_name 			VARCHAR(100) NOT NULL, /* defaults to: personal_name family_name */
	personal_name			VARCHAR(50) NOT NULL,  /* personal/first name stored special for listings */
	family_name				VARCHAR(50) NOT NULL,  /* family/last name stores special for listings */
	employee_id				VARCHAR(50) NULL,  /* optional employee id */
	job_title				VARCHAR(50) NULL,  /* optional job title */
	location				VARCHAR(50) NULL,  /* optional location */
	department				VARCHAR(50) NULL,  /* optional department */
	phone_number			VARCHAR(50) NULL,  /* optional phone number */
	status 					CHAR DEFAULT 'D',  /* E=enabled; D=disabled */
	properties_blob_id     	UUID NOT NULL, /* contains extra fields, comments, preferences, etc. */
	PRIMARY KEY ( id ) 
);
CREATE UNIQUE INDEX esfidx_user_email ON esf_user (lower(email));

/*
  esf_user_activity_log: Defines a log record associated with the specified user.
  We broke some of these case out from our general activity log so that no so much
  logging activity occurs on a single table.
  This table reserves the log_types where the absolute value of log_type (|log_type|) is in the range 100-199.
  
  log_type: 
  			Values < 0 mean the record is permanent and will not be aged off over time
  			Values > 0 mean the record is non-permanent and can be aged off over time
  			Value == 0 is currently undefined.
  			|value| == 101 is a user login/logoff
  			|value| == 102 is a user security
  			|value| == 103 is a configuration modification record
*/
CREATE TABLE esf_user_activity_log 
(
	user_id            	UUID NOT NULL,
	log_timestamp       TIMESTAMP WITH TIME ZONE DEFAULT NOW(),
	log_type            SMALLINT                 DEFAULT 1,
	log_text            VARCHAR(8000) NOT NULL
);
CREATE INDEX esfidx_user_activity_log_usertime ON esf_user_activity_log (user_id,log_timestamp);
CREATE INDEX esfidx_user_activity_log_typetime ON esf_user_activity_log (log_type,log_timestamp);


/* 
   esf_userlogin: Defines information about the user login.
   A user may not have this corresponding login record if they are not using
   the built-in login function and are authenticating through some other system.
*/
CREATE TABLE esf_userlogin
(
	user_id					UUID NOT NULL,
	pwd_hash                VARCHAR(100),  /* NULL means no password set; jBCrypt/SHA-512 of actual password which is never stored anywhere */
	forgotten_question      VARCHAR(100),  /* NULL means no forgotten password Q&A set */
	forgotten_answer_hash   VARCHAR(100),  /* NULL means no forgotten password Q&A set; jBCrypt/SHA-512 of the answer which is never stored anywhere */
  	reset_pickup_code       VARCHAR(50),   /* random reset password code; NULL until set */
  	forgot_pickup_code      VARCHAR(50),   /* random forgot password code; NULL until set */
	last_login_timestamp	TIMESTAMP WITH TIME ZONE, /* NULL means never logged in */
	last_login_ip			VARCHAR(100), /* NULL means never logged in */
	password_set_timestamp	TIMESTAMP WITH TIME ZONE,
	hash_version            SMALLINT DEFAULT 2012, /* version 2009 was SHA-512; version 2012 is jBCrypt */
	invalid_login_count     SMALLINT DEFAULT 0, /* number of invalid login attempts */
	invalid_answer_count    SMALLINT DEFAULT 0, /* number of invalid answers when trying to reset password using forgot password */
	request_forgot_count    SMALLINT DEFAULT 0, /* number of requests to forgot password to avoid DoS attack that would send lots of emails from our system to the specified user's email */
	PRIMARY KEY ( user_id )
);
CREATE UNIQUE INDEX esfidx_userloginresetcode ON esf_userlogin (reset_pickup_code);


/* 
   esf_userlogin_history: Remembers information about the previous userlogin records. Used to 
   to know if a user is selecting the same password or forgotten answer of a recently used one.
*/
CREATE TABLE esf_userlogin_history
(
	user_id					UUID NOT NULL,
  	password_set_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL,
	pwd_hash                VARCHAR(100),
	forgotten_answer_hash   VARCHAR(100),
	PRIMARY KEY (user_id,password_set_timestamp)
);
