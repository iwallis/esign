/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an upgrade.sql script:
psql dbname < upgrade.sql
**/

/*
 * esf_library_buttonmessage: Defines buttons and related messages in the library.  They are maintained by version. 
 */
CREATE TABLE esf_library_buttonmessage
(
	id						UUID 		NOT NULL,
	library_id				UUID		NOT NULL,	    /* library is in */
	esfname            		VARCHAR(50) NOT NULL,
	description				VARCHAR(250)    NULL,
	comments                VARCHAR(1000000) NULL,
 	status 					CHAR DEFAULT 'D', 			/* E=enabled; D=disabled */
	production_version      INTEGER 	DEFAULT 0,		/* Zero means no production version set */
	test_version      		INTEGER		DEFAULT 0,		/* either test_version=production_version or test_version=production_version+1*/
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_buttonmessage_name ON esf_library_buttonmessage (library_id,lower(esfname));

/* 1:N relationship table for esf_library_buttonmessage:esf_library_buttonmessage_version */
CREATE TABLE esf_library_buttonmessage_version
(
	id						UUID 		NOT NULL,
	library_buttonmessage_id		UUID		NOT NULL,	    /* buttonmessage this is a version of */
	version         		INTEGER 	DEFAULT 1,		/* sequence number starting with 1 */
	created_by_user_id		UUID		NOT NULL,
	last_updated_by_user_id	UUID		NOT NULL,
	created_timestamp		TIMESTAMP WITH TIME ZONE NOT NULL,
	last_updated_timestamp	TIMESTAMP WITH TIME ZONE NOT NULL,
	record_blob_id			UUID 	NOT NULL, 		/* stores the buttons and messages as properties */
	PRIMARY KEY ( id )
);
CREATE UNIQUE INDEX esfidx_library_buttonmessage_version ON esf_library_buttonmessage_version (library_buttonmessage_id,version);

ALTER TABLE esf_package_version ADD COLUMN button_message_id UUID NULL;