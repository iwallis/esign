/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an upgrade.sql script:
psql dbname < upgrade.sql
**/

UPDATE esf_package_version SET button_message_id = (SELECT esf_library_buttonmessage.id FROM esf_library_buttonmessage LIMIT 1) WHERE button_message_id IS NULL; /* should only have one, our newly created default buttons! */
ALTER TABLE esf_package_version ALTER COLUMN button_message_id SET NOT NULL;
