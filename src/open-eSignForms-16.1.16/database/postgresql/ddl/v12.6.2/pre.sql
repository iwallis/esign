/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an upgrade.sql script:
psql dbname < upgrade.sql
**/

ALTER TABLE esf_transaction_template ADD COLUMN display_name VARCHAR(100);
UPDATE esf_transaction_template SET display_name = path_name;
ALTER TABLE esf_transaction_template ALTER COLUMN display_name SET NOT NULL;

ALTER TABLE esf_report_template ADD COLUMN display_name VARCHAR(100);
UPDATE esf_report_template SET display_name = path_name;
ALTER TABLE esf_report_template ALTER COLUMN display_name SET NOT NULL;

ALTER TABLE esf_library_document_version ADD COLUMN programming_blob_id UUID NULL;

/* Fixes a bug if you did an IMPORT of a document or field template */
UPDATE esf_field_template SET display_empty_value_enabled = 'N' WHERE display_empty_value_enabled IS NULL;