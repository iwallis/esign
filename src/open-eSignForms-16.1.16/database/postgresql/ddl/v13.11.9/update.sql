/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an update.sql script:
psql dbname < update.sql
**/

CREATE TABLE esf_library_image_version_overlay_field
(
	library_image_version_id	UUID 		NOT NULL,
	field_template_id		 	UUID		NOT NULL,
	position_left				SMALLINT 	DEFAULT 0,
	position_top				SMALLINT 	DEFAULT 0,
	position_width				SMALLINT 	DEFAULT 50,
	position_height				SMALLINT 	DEFAULT 20,
	display_mode				CHAR 		DEFAULT 'F',
	background_color			VARCHAR(50) NULL,
	PRIMARY KEY ( library_image_version_id, field_template_id )
);

ALTER TABLE esf_transaction_party_document ADD COLUMN snapshot_document_skipped CHAR DEFAULT 'N';

ALTER TABLE esf_library_image_version ADD COLUMN use_data_uri CHAR DEFAULT 'N';
