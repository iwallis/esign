/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an upgrade.sql script:
psql dbname < upgrade.sql
**/

ALTER TABLE esf_field_template ADD COLUMN mask_in_data_snapshot CHAR DEFAULT 'N';
ALTER TABLE esf_field_template ADD COLUMN clone_allowed CHAR DEFAULT 'Y';
UPDATE esf_field_template SET output_format_spec = 'standard' WHERE field_type = 'I';
UPDATE esf_field_template SET clone_allowed = 'N' WHERE field_type = 's' OR field_type = 'D';

ALTER TABLE esf_party_template_field_template ADD COLUMN keep_required CHAR DEFAULT 'Y';

