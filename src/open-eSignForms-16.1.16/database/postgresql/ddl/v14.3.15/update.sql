/* 
If a DB conversion is needed via 'dbsetup' the process is basically (if a pre.sql and/or post.sql file is present).
Only rundbsetup after installing the new code since dbsetup uses the openesignforms.jar suitable for the upgraded release.
psql dbname < pre.sql          
rundbsetup /WebAppName             (assumes the webapp/WebAppName is configured to use 'dbname' as its database)
  convertX.X.X
psql dbname < post.sql

If no DB conversion is needed via 'dbsetup' the process is simpler with just an update.sql script:
psql dbname < update.sql
**/

DELETE FROM esf_blob B WHERE B.id IN (SELECT file_blob_id FROM esf_outbound_email_message_attachment WHERE outbound_email_message_id NOT IN (SELECT M.id from esf_outbound_email_message M));
DELETE FROM esf_outbound_email_message_attachment WHERE outbound_email_message_id NOT IN (SELECT M.id FROM esf_outbound_email_message M);
