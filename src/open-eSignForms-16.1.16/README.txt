File: README.txt

Please see the wikis from http://open.esignforms.com or directly on Google Code at http://code.google.com/p/openesignforms/w/list

Open eSignForms is not suitable for military use, or for any high-security, high-risk, life-or-death purposes.

** For third-party licenses, please see http://open.esignforms.com/thirdPartySoftware.jsp