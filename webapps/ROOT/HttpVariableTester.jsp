<!DOCTYPE html>
<%@page session="false" contentType="text/html; charset=UTF-8" import="com.esignforms.open.jsp.PageBean"%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
PageBean bean = new PageBean();
request.setAttribute("bean",bean);
bean.init(null,request,response);
%>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - HTTP GET/POST Variable Tester</title>
	<meta name="Description" content="Echoes the HTTP GET/POST variables when testing connectivity - Version 12/8/2015"/>
	<meta name="robots" content="noindex, nofollow" />
    <base href="<%=bean.getExternalUrlContextPath()%>/"/>
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="shortcut icon" />
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="icon" />
<style type="text/css">
<jsp:include page="./static/esf/esf.css" flush="true" />
</style>
</head>

<body class="esf" style="background-color: #f5f5f5;">

<jsp:include page="./header.jsp" flush="true" />

<p>This page lists the variables that have been submitted as well as the HTTP headers.  It is used for testing purposes only.</p>

<h3>Parameters/Variables</h3>
<table class="w100 cellpadding5 gridBorder cellspacing0">
<tr>
	<th class="underline">Parameter Name</th>
	<th class="underline">Value</th>
	<th class="underline">Length</th>
</tr>
<% String BGCOLOR = "bgGreenbarDark";
   java.util.Enumeration<?> params = bean.request.getParameterNames(); 
   while( params.hasMoreElements() ) 
   {
     String name = (String)params.nextElement();
     String[] values = bean.request.getParameterValues(name);
   
     for( int i=0; values != null && i < values.length; ++i ) 
     {
       String value = values[i];
       BGCOLOR = "bgGreenbarLight".equals(BGCOLOR) ? "bgGreenbarDark" : "bgGreenbarLight";
%>
<tr class="<%=BGCOLOR%>">
	<td><%=bean.toHtml(name)%></td>
	<td><%=bean.toHtml(value)%></td>
	<td align="center"><%=value.length()%></td>
</tr>
<% } } %>
</table>


<br />
<h3>Headers</h3>
<table class="w100 cellpadding5 gridBorder cellspacing0">
<tr>
	<th class="underline">Header Name</th>
	<th class="underline">Header Value</th>
	<th class="underline">Length</th>
</tr>
<% BGCOLOR = "bgGreenbarDark";
   java.util.Enumeration<?> headers = bean.request.getHeaderNames(); 
   while( headers.hasMoreElements() ) 
   {
     String name = (String)headers.nextElement();
     java.util.Enumeration<?> values = bean.request.getHeaders(name);
   
     while( values.hasMoreElements() ) 
     {
       String value = (String)values.nextElement();
       BGCOLOR = "bgGreenbarLight".equals(BGCOLOR) ? "bgGreenbarDark" : "bgGreenbarLight";
%>
<tr class="<%=BGCOLOR%>">
	<td><%=bean.toHtml(name)%></td>
	<td><%=bean.toHtml(value)%></td>
	<td align="center"><%=value.length()%></td>
</tr>
<% } } %>
</table>


<br />
<h3>Others</h3>
<table class="w100 cellpadding5 gridBorder cellspacing0">
<tr>
	<th class="underline">Name</th>
	<th class="underline">Value</th>
	<th class="underline">Length</th>
</tr>
<% BGCOLOR = "bgGreenbarDark";
   String name = "Method";
   String value = bean.request.getMethod();
   BGCOLOR = "bgGreenbarLight".equals(BGCOLOR) ? "bgGreenbarDark" : "bgGreenbarLight";
%>
<tr class="<%=BGCOLOR%>">
	<td><%=bean.toHtml(name)%></td>
	<td><%=bean.toHtml(value)%></td>
	<td align="center"><%=value.length()%></td>
</tr>

<% name = "URL";
   StringBuffer url = bean.request.getRequestURL();
   String queryString = bean.request.getQueryString();
   if ( bean.isNonBlank(queryString) )
   	url.append("?").append(queryString);
   value = url.toString();
   BGCOLOR = "bgGreenbarLight".equals(BGCOLOR) ? "bgGreenbarDark" : "bgGreenbarLight";
%>
<tr class="<%=BGCOLOR%>">
	<td><%=bean.toHtml(name)%></td>
	<td><%=bean.toHtml(value)%></td>
	<td align="center"><%=value.length()%></td>
</tr>

<% name = "Current time";
   value = (new com.esignforms.open.data.EsfDateTime()).to24HourTimeZoneString();
   BGCOLOR = "bgGreenbarLight".equals(BGCOLOR) ? "bgGreenbarDark" : "bgGreenbarLight";
%>
<tr class="<%=BGCOLOR%>">
	<td><%=bean.toHtml(name)%></td>
	<td><%=bean.toHtml(value)%></td>
	<td align="center"><%=value.length()%></td>
</tr>

<% name = "Content Type";
   value = bean.request.getContentType();
   if ( value != null ) {
       BGCOLOR = "bgGreenbarLight".equals(BGCOLOR) ? "bgGreenbarDark" : "bgGreenbarLight";
%>
<tr class="<%=BGCOLOR%>">
	<td><%=bean.toHtml(name)%></td>
	<td><%=bean.toHtml(value)%></td>
	<td align="center"><%=value.length()%></td>
</tr>
<% } else { 
    BGCOLOR = "bgGreenbarLight".equals(BGCOLOR) ? "bgGreenbarDark" : "bgGreenbarLight";
%>
<tr class="<%=BGCOLOR%>">
	<td><%=bean.toHtml(name)%></td>
	<td><font color="red">WARNING: Missing 'content-type' header.</font></td>
	<td align="center">0</td>
</tr>
<% } %>

<% name = "IP Address";
   value = bean.getIP();
   BGCOLOR = "bgGreenbarLight".equals(BGCOLOR) ? "bgGreenbarDark" : "bgGreenbarLight";
%>
<tr class="<%=BGCOLOR%>">
	<td><%=bean.toHtml(name)%></td>
	<td><%=bean.toHtml(value)%></td>
	<td align="center"><%=value.length()%></td>
</tr>
</table>

<p style="margin-left: 4em;"><jsp:include page="./footer.jsp" flush="true" /></p>

</body>
</html>
