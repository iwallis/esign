<!DOCTYPE html>
<%@page session="true" contentType="text/html; charset=UTF-8" 
import="com.esignforms.open.config.CKEditorContext,
		com.esignforms.open.prog.DocumentVersion,
		com.esignforms.open.data.EsfName,
		com.esignforms.open.prog.Image,
		com.esignforms.open.prog.ImageInfo,
		com.esignforms.open.prog.ImageVersion,
		com.esignforms.open.user.User"
%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(session,request,response);
bean.doGet();
%>

<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - CKEditor Image Browser</title>
	<meta name="Description" content="CKEditor Image Browser - Version 12/8/2015"/>
	<meta name="robots" content="noindex, nofollow" />
<script type="text/javascript">
<!--
function setCKEditorUrl(CKEditorFuncNum, url) {
	window.opener.CKEDITOR.tools.callFunction(CKEditorFuncNum,url);
	window.close();
}
// -->
</script>

<style type="text/css">
<jsp:include page="./static/esf/esf.css" flush="true" />
</style>
</head>

<body class="esf">

<% if ( bean.errors.hasError() ) { %>
<%=bean.errors.toHtml()%>
<% } else if ( bean.imageInfoMap.size() == 0 ) { %>
<p>Strange, but no images were found.</p>
<% } else { %>
<p>Here is the list of images available. Click on any one to choose it.</p>
<% 
for( ImageInfo imageInfo : bean.imageInfoMap.values() ) 
{ 
	ImageVersion imageVersion = ImageVersion.Manager.getByVersion(imageInfo.getId(),imageInfo.getTestVersion());
	if ( imageVersion == null ) {
		bean.err("Failed to find ImageVersion by ImageInfo ID: " + imageInfo.getId() + "; test version: " + imageInfo.getTestVersion());
		continue;
	}
	// No filename used at the end of the path since the actual image filename depends on which image+version is used at runtime.
	// And the 'iid' is only if the image can't be found by name.
	// Note that this format is relied upon by DocumentVersionPage when it generates the JSP and finds these IMG tags to be turned into tags.
	// And the ImageOut JSP tag uses the same URL syntax so the servlet works as expected
	String url = bean.getContextPath() + "/images/" + imageInfo.getEsfName() + "/?iid=" + imageInfo.getId();
	%>
<p><a href="#" onclick="setCKEditorUrl(<%=bean.CKEditorFuncNum%>,'<%=url%>')"><img src="<%=imageVersion.getThumbnailByIdUrl()%>" alt="thumb" align="middle"/> <%=imageInfo.getEsfName()%></a></p>
<% } %>
<% } /* no errors */ %>

<p style="margin-left: 4em;"><jsp:include page="./footer.jsp" flush="true" /></p>

</body>
</html>

<!-- "Bean" methods should be placed here -->
<%!
public class BeanData extends com.esignforms.open.jsp.PageBean {
	public BeanData() {}
	
	public String CKEditorFuncNum;
	public String ccid;
	public CKEditorContext ckeditorContext;
	public java.util.TreeMap<EsfName,ImageInfo> imageInfoMap;
	
public PageProcessing doGet() throws java.io.IOException {
	if ( ! isUserLoggedIn() ) {
		errors.addError("You must be logged in to list images for use by the HTML editor.");
		return PageProcessing.CONTINUE;
	}
	
	CKEditorFuncNum = getParam("CKEditorFuncNum");
	if ( isBlank(CKEditorFuncNum) ) {
		errors.addError("The required param 'CKEditorFuncNum' is missing.");
		return PageProcessing.CONTINUE;
	}
	
	// Get our context so we know which libraries to search for images
	ccid = getParam("ccid");
	if ( isBlank(ccid) ) {
		errors.addError("The required param 'ccid' is missing.");
		return PageProcessing.CONTINUE;
	}
	
	ckeditorContext = getLoggedInUser().getCKEditorContext(session,ccid);
	if ( ckeditorContext == null ) {
		errors.addError("No CKEditorContext could be found for the specified 'ccid'.  You may need to re-login before trying again.");
		return PageProcessing.CONTINUE;
	}
	
	imageInfoMap = new java.util.TreeMap<EsfName,ImageInfo>();
	
	// First, let's put in the images from our document version's library and document version itself
	if ( ckeditorContext.hasDocumentVersion() ) {
		DocumentVersion documentVersion = ckeditorContext.getDocumentVersion();
		for( ImageInfo imageInfo : ImageInfo.Manager.getAll(documentVersion.getDocument().getLibrary().getId()) ) {
			if ( ! imageInfoMap.containsKey(imageInfo.getEsfName()) ) {
				imageInfoMap.put(imageInfo.getEsfName(),imageInfo);
			}
		}		
		for( ImageInfo imageInfo : ImageInfo.Manager.getAll(documentVersion.getId()) ) {
			if ( ! imageInfoMap.containsKey(imageInfo.getEsfName()) ) {
				imageInfoMap.put(imageInfo.getEsfName(),imageInfo);
			}
		}		
	}
	
	// Then, we'll put in anything from a specified library
	if ( ckeditorContext.hasLibrary() ) {
		for( ImageInfo imageInfo : ImageInfo.Manager.getAll(ckeditorContext.getLibrary().getId()) ) {
			if ( ! imageInfoMap.containsKey(imageInfo.getEsfName()) ) {
				imageInfoMap.put(imageInfo.getEsfName(),imageInfo);
			}
		}		
	}
	
	// Finally, we'll put in anything from our template library
	for( ImageInfo imageInfo : ImageInfo.Manager.getAll(ckeditorContext.getTemplateLibrary().getId()) ) {
		if ( ! imageInfoMap.containsKey(imageInfo.getEsfName()) ) {
			imageInfoMap.put(imageInfo.getEsfName(),imageInfo);
		}
	}		
	
	return PageProcessing.CONTINUE;
}

}
%>
