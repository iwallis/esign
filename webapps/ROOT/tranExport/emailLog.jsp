<!DOCTYPE html>
<%@page contentType="text/html; charset=UTF-8" import="com.esignforms.open.data.EsfDateTime
													  ,com.esignforms.open.email.OutboundEmailMessage
													  ,com.esignforms.open.email.OutboundEmailMessageAttachment
													  ,com.esignforms.open.email.OutboundEmailMessageResponse
													  ,com.esignforms.open.prog.ReportTemplate
													  ,com.esignforms.open.runtime.Transaction
													  ,com.esignforms.open.user.User
													  "%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(session,request,response);
if ( bean.doGet() == BeanData.PageProcessing.DONE )
	return;
%>
<html>
<head>
    <meta charset="UTF-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - Exported transaction email log</title>
	<meta name="Description" content="Shows the exported transaction's email log for Open eSignForms - Version 12/9/2015"/>
	<meta name="robots" content="noindex, nofollow" />
    <style type="text/css">
 		<jsp:include page="../static/esf/esf.css" flush="true" />
 		.padH { padding-left: 5px; padding-right: 5px; }
	</style>
	<script type="application/javascript">
		function showMessage(msgNumber) {
			var elem = document.getElementById('MSG'+msgNumber);
			if ( elem.style.display == "block" )
				elem.style.display = "none";
			else
				elem.style.display = "block";
	    }
		function showResponse(msgNumber,respNumber) {
            var elem = document.getElementById('RESP'+msgNumber+'_'+respNumber);
			if ( elem.style.display == "block" )
				elem.style.display = "none";
			else
				elem.style.display = "block";
	    }
	</script>
</head>

<body class="esf" style="background-color: #f5f5f5;">
<% if ( bean.errors.hasErrorOrWarning() ) { %>
<%=bean.errors.toHtml()%>
<% } else { %>

<table class="w100 cellpadding5 cellspacing0">
<tr>
	<th class="left nowrap">&nbsp;</th>
	<th class="left nowrap">Date/time</th>
	<th class="left nowrap">Status</th>
	<th class="left nowrap">From</th>
	<th class="left nowrap">To</th>
	<th class="left nowrap">Subject</th>
</tr>
<% int messageNumber = 0;
   for( OutboundEmailMessage emailLog : bean.emailLogList) { 
 	++messageNumber;
	EsfDateTime timestamp = emailLog.hasBeenSent() ? emailLog.getSentTimestamp() : emailLog.getCreatedTimestamp();
	String sentLabel = emailLog.hasBeenSent() ?"Sent" : "Queued";
%>
<tr>
	<td class="nowrap f8"><a href="javascript:showMessage(<%=messageNumber%>)">View/hide</a></td>
	<td class="nowrap padH"><esf:out value="<%=timestamp.toLogString(bean.getLoggedInUser())%>"/></td>
	<td class="nowrap padH"><esf:out value="<%=emailLog.getSendStatus()%>"/></td>
	<td class="nowrap padH"><esf:out value="<%=emailLog.getEmailFrom()%>"/></td>
	<td class="nowrap padH"><esf:out value="<%=emailLog.getEmailTo()%>"/></td>
	<td class="nowrap padH"><esf:out value="<%=emailLog.getEmailSubject()%>"/></td>
</tr>
<tr id="MSG<%=messageNumber%>" style="display: none;">
	<td colspan="6">
		<div style="width: 800px; margin-left: 2em;">
		<p class="nowrap"><%=sentLabel%>: <esf:out value="<%=timestamp.toLogString(bean.getLoggedInUser())%>"/></p>
		<p class="nowrap">From: <esf:out value="<%=emailLog.getEmailFrom()%>"/></p>
		<p class="nowrap">To: <esf:out value="<%=emailLog.getEmailTo()%>"/></p>
		<% if (bean.isNonBlank(emailLog.getEmailCc())) {%><p class="nowrap">cc: <esf:out value="<%=emailLog.getEmailCc()%>"/></p><%}%>
		<p class="nowrap">Subject: <esf:out value="<%=emailLog.getEmailSubject()%>"/></p>
		<% if (bean.isNonBlank(emailLog.getEmailHtml())) {%><div style="width: 800px;">Email in HTML:<div class="w100 box"><%=emailLog.getEmailHtml()%></div></div><%}%>
		<% if (bean.isNonBlank(emailLog.getEmailText())) {%><div style="width: 800px;">Email in pain text:<div class="w100 box"><esf:out value="<%=emailLog.getEmailText()%>"/></div></div><%}%>
		<% if ( emailLog.hasAttachments() ) { 
				java.util.List<OutboundEmailMessageAttachment> attList = emailLog.getAttachments();
				bean.setApplicationAttribute(bean.exportAuthCode+"-email-attList",attList); // passing back our list to Report servlet to output these in the ZIP file
				for( OutboundEmailMessageAttachment att : attList ) {
					String fileName = "tran-" + bean.transaction.getId() + "/email-att-"+att.getId()+"/"+att.getFileName();
		%>	
			<p class="f8">Attachment: <esf:out value="<%=fileName%>"/> (<esf:out value="<%=com.esignforms.open.data.EsfInteger.byteSizeInUnits(att.getFileSize())%>"/>)</p>
		<% } /* end for each att */ %>
		<% } /* endif has attachments*/ %>
		</div>
	</td>
</tr>
<%
	java.util.List<OutboundEmailMessageResponse> emailResponseList = OutboundEmailMessageResponse.Manager.getAll(emailLog.getId());
	int responseNumber = 0;
	for( OutboundEmailMessageResponse responseLog : emailResponseList ) {
		responseNumber++;
%>
<tr>
	<td class="nowrap f8">&nbsp; -&gt; <a href="javascript:showResponse(<%=messageNumber%>,<%=responseNumber%>)">View/hide</a></td>
	<td class="nowrap padH"><esf:out value="<%=responseLog.getReceivedTimestamp().toLogString(bean.getLoggedInUser())%>"/></td>
	<td class="nowrap padH">Response</td>
	<td class="nowrap padH"><esf:out value="<%=responseLog.getEmailFrom()%>"/></td>
	<td class="nowrap padH"><esf:out value="<%=responseLog.getEmailTo()%>"/></td>
	<td class="nowrap padH"><esf:out value="<%=responseLog.getEmailSubject()%>"/></td>
</tr>
<tr id="RESP<%=messageNumber%>_<%=responseNumber%>" style="display: none;">
	<td colspan="6">
		<div style="width: 800px; margin-left: 2em;">
		<p class="nowrap">Received: <esf:out value="<%=responseLog.getReceivedTimestamp().toLogString(bean.getLoggedInUser())%>"/></p>
		<p class="nowrap">From: <esf:out value="<%=responseLog.getEmailFrom()%>"/></p>
		<p class="nowrap">To: <esf:out value="<%=responseLog.getEmailTo()%>"/></p>
		<p class="nowrap">Subject: <esf:out value="<%=responseLog.getEmailSubject()%>"/></p>
		<% if (bean.isNonBlank(responseLog.getEmailHtml())) {%><div style="width: 800px;">Email in HTML:<div class="w100 box"><%=responseLog.getEmailHtml()%></div></div><%}%>
		<% if (bean.isNonBlank(responseLog.getEmailText())) {%><div style="width: 800px;">Email in pain text:<div class="w100 box"><esf:out value="<%=responseLog.getEmailText()%>"/></div></div><%}%>
		</div>
	</td>
</tr>
<% } /* end for responses */ %>

<% } /* end for emails */ %>
</table>

<% } /* no errors */ %>

<%@ include file="footer.jsp"%>
</body>
</html>
<% bean.endSession(); /* okay to do since this is run outside of the user's session */%>
<%!
public class BeanData
	extends com.esignforms.open.jsp.PageBean
{
	public transient String exportAuthCode = null;
    public transient Transaction transaction = null;
    public transient ReportTemplate reportTemplate = null;
    public transient User user = null;
    
    java.util.List<OutboundEmailMessage> emailLogList = null;
    
    public BeanData()
	{
	}

	public PageProcessing doGet()
		throws java.io.IOException
	{
		exportAuthCode = getParam("exportAuthCode",null);
		if ( ! isBlank(exportAuthCode) )
		{
	        Object[] objects = (Object[])getApplicationAttribute(exportAuthCode);
	        if ( objects == null || objects.length != 3 )
				errors.addError("Server error: exportAuthCode specified, but unable to retrieve the required object array parameters.");
	        else
	        {
	        	transaction = (Transaction)objects[0];
	        	reportTemplate = (ReportTemplate)objects[1];
		        loggedInUser = (User)objects[2];
			    if ( transaction == null )
					errors.addError("Server error: exportAuthCode specified was missing transaction.");
			    else
			    	emailLogList = OutboundEmailMessage.Manager.getMatching(null,null,OutboundEmailMessage.LINK_TYPE_TRANSACTION,transaction.getId());
			    if ( reportTemplate == null )
					errors.addError("Server error: exportAuthCode specified was missing reportTemplate, unable to retrieve the transaction email log.");
			    if ( loggedInUser == null )
					errors.addError("Server error: exportAuthCode specified was missing loggedInUser, unable to retrieve the transaction email log.");
	        }
		}
		
		return PageProcessing.CONTINUE;
	}
}
%>
