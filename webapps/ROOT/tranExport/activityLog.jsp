<!DOCTYPE html>
<%@page contentType="text/html; charset=UTF-8" import="com.esignforms.open.log.TransactionActivityLogInfo
													  ,com.esignforms.open.prog.ReportTemplate
													  ,com.esignforms.open.runtime.Transaction
													  ,com.esignforms.open.user.User
													  "%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(session,request,response);
if ( bean.doGet() == BeanData.PageProcessing.DONE )
	return;
%>
<html>
<head>
    <meta charset="UTF-8"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - Exported transaction activity log</title>
	<meta name="Description" content="Shows the exported transaction's activity log for Open eSignForms - Version 12/9/2015"/>
	<meta name="robots" content="noindex, nofollow" />
    <style type="text/css">
 		<jsp:include page="../static/esf/esf.css" flush="true" />
 		.padH { padding-left: 5px; padding-right: 5px; }
	</style>
</head>

<body class="esf" style="background-color: #f5f5f5;">
<% if ( bean.errors.hasErrorOrWarning() ) { %>
<%=bean.errors.toHtml()%>
<% } else { %>

<table class="w100 cellpadding5 cellspacing0">
<tr>
	<th class="left nowrap">Date/time</th>
	<th class="left nowrap">Type</th>
	<th class="left nowrap">Log record</th>
</tr>
<% for( TransactionActivityLogInfo logInfo : bean.activityLogList) {%>
<tr>
	<td class="nowrap"><esf:out value="<%=logInfo.getTimestamp().toLogString(bean.getLoggedInUser())%>"/></td>
	<td class="nowrap padH"><esf:out value="<%=logInfo.getTypeShortDescription()%>"/></td>
	<td><esf:out value="<%=logInfo.getText()%>"/></td>
</tr>
<% } %>
</table>

<% } /* no errors */ %>

<%@ include file="footer.jsp"%>
</body>
</html>
<% bean.endSession(); /* okay to do since this is run outside of the user's session */%>
<%!
public class BeanData
	extends com.esignforms.open.jsp.PageBean
{
    public transient Transaction transaction = null;
    public transient ReportTemplate reportTemplate = null;
    public transient User user = null;
    
    java.util.List<TransactionActivityLogInfo> activityLogList = null;
    
    public BeanData()
	{
	}

	public PageProcessing doGet()
		throws java.io.IOException
	{
		String exportAuthCode = getParam("exportAuthCode",null);
		if ( ! isBlank(exportAuthCode) )
		{
	        Object[] objects = (Object[])getApplicationAttribute(exportAuthCode);
	        if ( objects == null || objects.length != 3 )
				errors.addError("Server error: exportAuthCode specified, but unable to retrieve the required object array parameters.");
	        else
	        {
	        	transaction = (Transaction)objects[0];
	        	reportTemplate = (ReportTemplate)objects[1];
		        loggedInUser = (User)objects[2];
			    if ( transaction == null )
					errors.addError("Server error: exportAuthCode specified was missing transaction.");
			    else
			    	activityLogList = TransactionActivityLogInfo.Manager.getMatching(transaction.getId(),null,null,null,null);
			    if ( reportTemplate == null )
					errors.addError("Server error: exportAuthCode specified was missing reportTemplate, unable to retrieve the transaction activity log.");
			    if ( loggedInUser == null )
					errors.addError("Server error: exportAuthCode specified was missing loggedInUser, unable to retrieve the transaction activity log.");
	        }
		}
		
		return PageProcessing.CONTINUE;
	}
}
%>
