<!DOCTYPE html>
<%@page session="true" contentType="text/html; charset=UTF-8" import="com.esignforms.open.Application
														   ,com.esignforms.open.Version
														   ,com.esignforms.open.data.EsfUUID
														   ,com.esignforms.open.db.ConnectionPool
														   ,java.sql.Connection
														   ,java.sql.PreparedStatement
														   ,java.sql.ResultSet
														   ,java.sql.SQLException
														   "%>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(session,request,response);
if ( bean.isGet() )
{
	if ( bean.doGet() == BeanData.PageProcessing.DONE )
		return;
}
else
{
	if ( bean.doPost() == BeanData.PageProcessing.DONE )
		return;
}
%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - System Status Check</title>
	<meta name="Description" content="Does basic system status check, such as for Pingdom - Version 12/8/2015"/>
	<meta name="robots" content="noindex, nofollow" />
</head>
<body style="background-color: #f5f5f5;">
<% if ( bean.errors.hasErrorOrWarning() ) { %>
<%=bean.errors.toHtml()%>
<% } %>
<h1>Current status is <esf:out value="<%=bean.status%>"/></h1>
<% if ( bean.doLicenseCheck ) { %>
<p>Version: <esf:out value="<%=Version.getVersionString()%>"/></p>
<p>License: <esf:out value="<%=bean.app.getLicenseInfo()%>"/></p>
<% } %>
</body>
</html>

<% bean.endSession(); %>

<!-- "Bean" methods should be placed here -->
<%!
public class BeanData extends com.esignforms.open.jsp.PageBean {
	boolean doLicenseCheck = false;
	public String status = "UNKNOWN";
	
	public BeanData()
	{
	}

public PageProcessing doGet()
	throws java.io.IOException
{
	return doPost();
}

public PageProcessing doPost()
	throws java.io.IOException
{
	String checkDeployId = getParam("deployId");
	doLicenseCheck = app.getDeployId().equals(new EsfUUID(checkDeployId)); // if called with the deploy id
	
	status = "CHECKING";
	
    ConnectionPool    pool = app.getConnectionPool();
    Connection        con  = pool.getConnection();
    PreparedStatement stmt = null;
    try
    {
    	stmt = con.prepareStatement("SELECT COUNT(*) FROM esf_deployment");
    	ResultSet rs = stmt.executeQuery();
    	if ( ! rs.next() )
    		status = "FAILED DB QUERY";
    	else
    	{
    		int count = rs.getInt(1);
    		if ( count == 1 )
    			status = "RUNNING";
    		else
    			status = "UNEXPECTED DEPLOYMENT QUERY COUNT NOT = 1; COUNT = " + count;
    	}
        con.commit();
    	
    }
    catch(SQLException e) 
    {
    	status = "SQL EXCEPTION";
        app.sqlerr(e,"System Status Check");
        pool.rollbackIgnoreException(con,e);
    }
    finally
    {
        app.cleanupPool(pool,con,stmt);
    }
    info("Status: " + status + "; from IP: " + getIP() + "; browser: " + getUserAgent());
	return PageProcessing.CONTINUE;
}

}
%>
