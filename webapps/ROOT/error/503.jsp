<!DOCTYPE html>
<%@page isErrorPage="true" session="false" contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
com.esignforms.open.jsp.PageBean bean = new com.esignforms.open.jsp.PageBean();
bean.init(request.getSession(false),request,response);
String loggedInUserMessage = bean.isUserLoggedIn() ? bean.getLoggedInUser().getEmail() : "(not logged in)";
String url = bean.getRequestStringAttribute("javax.servlet.forward.request_uri");
if ( bean.isBlank(url) )
	url = "(original link/URL not known)";
else if ( ! url.toLowerCase().startsWith("http") )
	url = bean.getExternalUrlWithoutContextPath() + url;
%>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - Server error HTTP status 503</title>
	<meta name="Description" content="Shows any server errors - Version 12/16/2015"/>
	<meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<base href="<%=bean.getExternalUrlContextPath()%>/"/>
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="shortcut icon" />
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="icon" />
	
<style type="text/css">
<jsp:include page="/static/esf/esf.css" flush="true" />
</style>
</head>

<body class="esf" style="background-color: #f5f5f5;">
<jsp:include page="/error/header.jsp" flush="true" />

<h3><img alt="logo" src="./images/IconForError/" align="middle"/> <em>Sorry, the web server had an unexpected error.</em></h3>

<h3>Message:</h3>
<div class="jspErrorPageMessage">
<% 
String message = bean.getRequestStringAttribute("javax.servlet.error.message");
if ( message == null ) { %>
<p>An error on the web server has occurred that prevents us from completing your request.</p>
<% } else { %>
<p><%=bean.toHtml(message)%></p>
<% } %>
<p style="font-weight: normal !important;">
	Requested link/URL: <%=bean.toHtml(url)%>
	<br/>
	Logged in as: <%=bean.toHtml(loggedInUserMessage)%>
</p>
</div>

<p style="margin-left: 4em;"><jsp:include page="/error/footer.jsp" flush="true" /></p>

</body>
</html>
