(function() {
	// If not defined yet, ensure our top "JavaScript namespace" variable 'esf' is created.
	if ( typeof esf == "undefined" ) {
		esf = {
			version: {
				major: 16, minor: 1, update: "16",
				toString: function(){
					with(esf.version){
						return major + "." + minor + "." + update;
					}
				}
			},
			appname: "Open eSignForms",
			htmlCopyright: "&copy; 2016 Yozons, Inc.",
			textCopyright: "Copyright (c) 2009-2016 Yozons, Inc."
		};
		
		esf.getRelease = function() {
			return this.appname + " " + this.version.toString();
		};
		
		if ( typeof console != "undefined") console.log(esf.getRelease() + " - " + esf.textCopyright);
		
		esf.autoPost = function(inputField) {
			esf.confirmOnPageUnloadMessage = null; // do not block leaving page for autopost
			esf.hideAllSubmitInputFields();
			inputField.form.ESF_AUTO_POST_FIELD_ID.value = inputField.id; // form field name must match DocumentPageBean.HIDDEN_AUTO_POST_FIELD_ID
			inputField.form.submit();
		};
		
		esf.blockEnterKeyViaInputFields = function(e) {
			var evt = e || window.event; 
			var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
			if ((evt.keyCode == 13) && (node.type=="text")) { 
				return false; 
			} 			
		};
		
		esf.checkConfirmOnPageUnload = function(confirm,durationMsec) {
			if (! confirm) {
				window.onbeforeunload = null;
			}
			if ( durationMsec && durationMsec > 0 )
				esf.hideAllSubmitInputFieldsTemporarily(durationMsec);
			else
				esf.hideAllSubmitInputFields();
		};
		
		// For links, we don't hide buttons and we don't clear the unload unless requested AND the target is _top or _self (or no target set)
		esf.checkLinkConfirmOnPageUnload = function(confirm,atag) {
			if (! confirm && (atag.target == "_top" || atag.target == "_self" || atag.target == "") ) { 
				window.onbeforeunload = null;
			}
		};
		
		// Default is no message to display on page unload
		esf.confirmOnPageUnloadMessage = null;
		
		esf.confirmOnPageUnload = function(e) {
			e = e || window.event;
			
	        if ( esf.confirmOnPageUnloadMessage ) {
	        	if (e) {
	        		e.returnValue = esf.confirmOnPageUnloadMessage;
	        	} 
	            return esf.confirmOnPageUnloadMessage;
	        }
		};

		esf.hideAllSubmitInputFields = function() {
		  if ( document.getElementsByTagName ) {
		   	var inputFields = document.getElementsByTagName("input");
		   	for( var i=0; i < inputFields.length; ++i )	{
		   	  if ( inputFields[i].type == "submit" )
		   	      esf.makeInvisible(inputFields[i]);
		   	}
		   	var buttonFields = document.getElementsByTagName("button");
		   	for( var i=0; i < buttonFields.length; ++i ) {
		   		esf.makeInvisible(buttonFields[i]);
		   	}
		   	return true;
		  }
		};
		
		esf.showAllSubmitInputFields = function() {
		  if ( document.getElementsByTagName ) {
		    var inputFields = document.getElementsByTagName("input");
		   	for( var i=0; i < inputFields.length; ++i )	{
		   	  if ( inputFields[i].type == "submit" )
		   	      esf.makeVisible(inputFields[i]);
		   	}
		   	var buttonFields = document.getElementsByTagName("button");
		   	for( var i=0; i < buttonFields.length; ++i ) {
		   		esf.makeVisible(buttonFields[i]);
		   	}
		   	return true;
		  }
		};
			
		esf.hideAllSubmitInputFieldsTemporarily = function(durationMsec){
			esf.hideAllSubmitInputFields();
			setTimeout( esf.showAllSubmitInputFields, durationMsec );
		};
				
		esf.displayById = function(id){
			if ( document.getElementById && document.getElementById(id) )
				esf.display(document.getElementById(id));
		};
		
		esf.display = function(elem){
			if ( elem && elem.style )
				elem.style.display = "block";
		};
		
		esf.hideById = function(id){
			if ( document.getElementById && document.getElementById(id) )
				esf.hide(document.getElementById(id));
		};
		
		esf.hide = function(elem){
			if ( elem && elem.style )
				elem.style.display = "none";
		};
		
		esf.makeVisibleById = function(id){
			if ( document.getElementById && document.getElementById(id) )
				esf.makeVisible(document.getElementById(id));
		};
		
		esf.makeVisible = function(elem){
		  if ( elem && elem.style )
		  	elem.style.visibility = 'visible';
		};
		
		esf.makeInvisibleById = function(id){
			if ( document.getElementById && document.getElementById(id) )
				esf.makeInvisible(document.getElementById(id));
		};
		
		esf.makeInvisible = function(elem){
		  if ( elem && elem.style )
		  	elem.style.visibility = 'hidden';
		};
		
		esf.makeTemporarilyInvisible = function(elem, durationMsec){
			esf.makeInvisible(elem);
		    setTimeout( function(){esf.makeVisible(elem);}, durationMsec );
		};
			
		esf.ensureTopWindow = function(){
		  if (window != window.top)
			top.location.href = location.href;
		};
		
		esf.setAllCheckboxesByName = function(name,b){
		  if ( document.getElementsByName ) {
		   	var checkboxes = document.getElementsByName(name);
		   	for( var i=0; i < checkboxes.length; ++i ) {
		   	  if ( checkboxes[i].type == "checkbox" )
		   	      checkboxes[i].checked = b;
		   	}
		  }
		};
		
		esf.containsSpace = function(s){
			if ( s == null )
				return false;
			i = 0;
			while( i < s.length ) {
				if ( s.charAt(i) == ' ' )
					return true;
				++i;
			}
		
			return false;
		};
		
		esf.isOnlyNumbers = function(s){
			if ( s == null )
				return false;
			var i = 0;
			while( i < s.length ) {
				if ( "0123456789".indexOf(s.charAt(i)) < 0 )
					return false;
				i++;
			}
			return true;
		};
		
		esf.countCharInString = function(s,c){
			var num=0;
			var at=s.indexOf(c);
		    while(at>=0) {
				++num;
				at=s.indexOf(c,++at);
			}
			return num;
		};
		
		// Trims whitespace from the front and end of a string
		esf.trimSpace = function(s){
			if ( s == null || s.length == 0 )
				return s;
		
			var beginIndex = 0;
			var endIndex   = s.length - 1;
			while( beginIndex < s.length && s.charAt(beginIndex) == ' ' )
				++beginIndex;
			while( endIndex >= 0 && s.charAt(endIndex) == ' ' )
				--endIndex;	
			++endIndex;
			
			if ( beginIndex >= endIndex )
				return '';
				
			return s.substring(beginIndex,endIndex);
		};
		
		esf.isValidEmail = function(addr,doalert){
			if (addr.indexOf("..") >= 0 ) {
				doalert && alert("Your email address contains invalid double dot '..' characters.");
				return false;
			}
		
			var at=addr.lastIndexOf("@");
			if (at < 0 ) {
				doalert && alert("Your email address is missing the '@' character. Please check the user name, '@' sign and host name.");
				return false;
			}
			if (at == 0 ) {
				doalert && alert("Your email address begins with your email name, not the '@' character. Please check the user name, '@' sign and host name.");
				return false;
			}
			if ( esf.countCharInString(addr,"@") > 1 ) {
				doalert && alert("Your email address has more than one '@' character. There should only be one, like 'myname@example.com'. Please check the user name, '@' sign and host name.");
				return false;
			}
		
			var username = addr.substring(0,at);
			if ( username == null || username.length < 1 ) {
				doalert && alert("Your email address is missing a user name before the '@' character. Please check the user name, '@' sign and host name.");
				return false;
			}
			if ( esf.containsSpace(username) ) {
				doalert && alert("Your email user name should not contain any spaces. Please check the user name, '@' sign and host name.");
				return false;
			}
		
			var hostname = addr.substring(at+1);
			if ( hostname == null || hostname.length < 4 ) {
				doalert && alert("Your email address is missing a host name after the '@' character. Please check the user name, '@' sign and host name.");
				return false;
			}
			if ( esf.containsSpace(hostname) ) {
				doalert && alert("Your email host name should not contains any spaces. Please check the user name, '@' sign and host name.");
				return false;
			}
		
			var dot = hostname.lastIndexOf(".");
			if ( dot < 1 ) {
				doalert && alert("Your email host name should have a '.' separator, like 'example.com'. Please check the user name, '@' sign and host name.");
				return false;
			}
		
			var tld = hostname.substring(dot+1);
			if ( tld == null || tld.length < 2 || tld.length > 63 ) {
				doalert && alert("Your email address is missing a .com or .net suffix, like 'example.COM'. Please check the user name, '@' sign and host name.");
				return false;
			}
		
			return true; 
		};
		
		// Opens a window -- for a given name, width and height
		esf.showWindow = function(url,name,width,height) {
			var win = window.open(url,name,'width='+width+',height='+height+',scrollbars=yes,resizable=yes');
			win.focus();
			return false;
		};

	}
})();
