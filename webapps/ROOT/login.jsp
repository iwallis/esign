<!DOCTYPE html>
<%@page session="false" contentType="text/html; charset=UTF-8" 
		import="com.esignforms.open.config.Literals
			   ,com.esignforms.open.data.EsfBoolean
			   ,com.esignforms.open.data.EsfName
			   ,com.esignforms.open.data.EsfPathName
			   ,com.esignforms.open.data.EsfValue
			   ,com.esignforms.open.user.User
			   ,com.esignforms.open.util.StringReplacement
			   "%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(request.getSession(false),request,response);

if ( bean.isGet() )
{
	if ( bean.doGet() == BeanData.PageProcessing.DONE )
		return;
}
else
{
	if ( bean.doPost() == BeanData.PageProcessing.DONE )
		return;
}
%>

<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><esf:out value="<%=bean.pageTitle%>"/></title>
	<meta name="Description" content="Login page - Updated 12/8/2015"/>
	<meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <base href="<%=bean.getExternalUrlContextPath()%>/"/>
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="shortcut icon" />
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="icon" />
	<script type="text/javascript" src="./static/esf/esf.js" ></script>
<script type="text/javascript">
<!--

// Trims whitespace from the front and end of a string
function trimSpace(s)
{
	if ( s == null || s.length == 0 )
		return s;

	var beginIndex = 0;
	var endIndex   = s.length - 1;
	while( beginIndex < s.length && s.charAt(beginIndex) == ' ' )
		++beginIndex;
	while( endIndex >= 0 && s.charAt(endIndex) == ' ' )
		--endIndex;	
	++endIndex;
	
	if ( beginIndex >= endIndex )
		return '';
		
	return s.substring(beginIndex,endIndex);
}

function init()
{
  <% if ( ! bean.loginPageAllowEmbedded ) { %>
  if (window != window.top) {
	top.location.href = location.href;
  }
  <% } %>
	
  document.aform.email.value = trimSpace(document.aform.email.value);
  document.aform.password.value = trimSpace(document.aform.password.value);

  if ( document.aform.email.value.length > 0 && document.aform.password.value.length == 0 ) {
	document.aform.password.focus();
  } else {
  	document.aform.email.focus();
  }

  if ( document.aform.frag.value.length == 0 ) {
	  document.aform.frag.value = document.location.hash;
  }
  
  return true;
}
// -->
</script>

<style type="text/css">
<jsp:include page="./static/esf/esf.css" flush="true" />

.loginButton { color: #fcfcfc; font-weight: bold; background-color: #6aa6dd; border: 1px solid #2e5a84; border-radius: 3px; }

.loginButton:hover { color: #fcfcfc; background-color: #418dd3; border: 1px solid black; border-radius: 3px; }
	
input.loginButton, input.loginButton:hover {
	background-image: url(./VAADIN/themes/openesf/icons/fatcow16/key.png);
	background-repeat: no-repeat;
	background-position: 9px;
	padding: 5px 10px 5px 30px;
}

</style>
</head>

<body onload="init()" class="esf" style="background-color: #f5f5f5;">

<jsp:include page="./header.jsp" flush="true" />
<% if ( bean.errors.hasErrorOrWarning() ) { %>
<%=bean.errors.toHtml()%>
<% } %>

<jsp:include page="./motd.html" flush="true" />

<form name="aform" method="post" action="<%=bean.toHtml(bean.getMySessionUrl())%>" enctype="application/x-www-form-urlencoded" onsubmit="esf.hideAllSubmitInputFields()">
<input type="hidden" name="toURL" value="<%=bean.toHtml(bean.toURL)%>"/>
<input type="hidden" name="frag" value="<%=bean.toHtml(bean.frag)%>"/>

<table class="cellpadding5">
<tr>
	<td>&nbsp;</td>
	<td><p class="f14 bold"><esf:out value="<%=bean.welcomeHeader%>"/></p></td>
</tr>
<tr>
	<td class="right"><label for="email" title="Please enter your email address to login">Email:</label></td>
	<td><input id="name" name="email" value="<%=bean.toHtml(bean.email)%>" size="40" maxlength="<%=Literals.EMAIL_ADDRESS_MAX_LENGTH%>" class="required" title="Please enter your email address to login" placeholder="you@example.com"/></td>
</tr>
<tr>
	<td class="right"><label for="password" title="Please enter your password or phrase (max 100 characters)">Password:</label></td>
	<td class="nowrap"><input type="password" id="password" name="password" value="<%=bean.toHtml(bean.password)%>" size="40" maxlength="<%=bean.getMaxPasswordLength()%>" class="required" title="Please enter your password or phrase (max 100 characters)" autocomplete="off" placeholder="Your password or pass phrase"/>
		<span class="f10"> &nbsp; &nbsp; <a href="./requestForgotPassword.jsp" title="Reset your eSignForms password if you have forgotten it">Forgot your password?</a></span></td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td><input type="submit" name="login" value="Login" title="Please click to login" class="loginButton" /></td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td class="f10">
	<label title="DO NOT USE WHEN ON SOMEONE ELSE'S COMPUTER! Check this to save your email address on YOUR computer using a cookie">
	<input type="checkbox" name="saveEmail" value="y" class="checkboxInput" checked="<%=bean.toHtml(bean.saveEmail)%>" />Remember email</label>
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td align="center">
		<jsp:include page="./footer.jsp" flush="true" />
	</td>
</tr>
</table>

</form>

</body>
</html>

<!-- "Bean" methods should be placed here -->
<%!
public class BeanData
	extends com.esignforms.open.jsp.PageBean
{
	public BeanData()
	{
	}

	public static final String COOKIE_EMAIL = "ce";
	
	public String toURL;
	public String frag;
	public String email;
	public String password;
	public String saveEmail;
	
	public String companyName = "";
	public String pageTitle = "";
	public String welcomeHeader = "";
	public boolean loginPageAllowEmbedded = false;

public PageProcessing doGet()
	throws java.io.IOException
{
	toURL = getParam("toURL",getExternalUrlContextPath()+"/ui/");
	toURL = ensureUrlIsForMyApplication(toURL);
	
	commonInit();
	
	String cookieEmail = getCookie(COOKIE_EMAIL);
	if ( isBlank(cookieEmail) )
	{
	    email = "";
	    saveEmail = "n";
	}
	else
	{
	    email = cookieEmail;
	    saveCookie(COOKIE_EMAIL,email);
	    saveEmail = "y";
	}

	password = "";
	
	return PageProcessing.CONTINUE;
}

public PageProcessing doPost()
	throws java.io.IOException
{
    session = request.getSession();
    
    commonInit();

	frag = getParam("frag");
	//if ( isBlank(frag) )
	//	frag = "#WelcomeTipsView/Welcome";
	toURL = getParam("toURL",getExternalUrlContextPath()+"/ui/");
	toURL = ensureUrlIsForMyApplication(toURL);
	
	email = getParam("email","");
	password = getParam("password","");
	
	saveEmail = getParam("saveEmail","n");
	if ( "n".equals(saveEmail) )
	    removeCookie(COOKIE_EMAIL);
	else
	{
		saveEmail = "y"; // forces the value to be either n or y
	    saveCookie(COOKIE_EMAIL,email);
	}

	String button = getParam("login",null);
	if ( button != null )
		return isUserValid();

	errors.addError("Please click the 'Login' button...");
	return PageProcessing.CONTINUE;
}

private void commonInit()
{
	pageTitle = StringReplacement.replace("${property:ESF.LoginPageTitle}");
	welcomeHeader = StringReplacement.replace("${property:ESF.LoginWelcomeHeader}");

	String loginPageAllowEmbeddedValue = StringReplacement.replace("${property:ESF.LoginPageAllowEmbedded}");
	loginPageAllowEmbedded = EsfBoolean.toBoolean(loginPageAllowEmbeddedValue);
	
	if ( ! loginPageAllowEmbedded )
	{
		response.setHeader("X-Frame-Options","DENY");
		response.setHeader("Content-Security-Policy","frame-ancestors 'none'");
	}
}

private PageProcessing isUserValid()
	throws java.io.IOException
{
	if ( isBlank(email) )
		errors.addError("Please enter your email","email");
	else if ( !	isValidEmail(email) )
		errors.addError("Your email address is not formatted correctly. Please provide your valid email address.","email");
		
	if ( isBlank(password) )
		errors.addError("Please enter your password","password");

	if ( errors.hasErrorOrWarning() )
		return PageProcessing.CONTINUE;
	
    User user = loginUser(email, password);
	if ( user != null )
	{
		if ( toURL.indexOf("#") < 0 )
			toURL = toURL + frag;
		debug("isUserValid() - On successful login redirecting to: " + toURL);
	    sendRedirect(toURL);
	    return PageProcessing.DONE;
	}

	if ( com.esignforms.open.data.EsfString.isAllCaps(password) )
   		errors.addWarning("The password you just entered was all CAPITAL letters. Please check if your CAPS LOCK is on or off.");
	password = ""; // reset
	warning("isUserValid() - Email: "+email+" access denied, Open eSignForms email or password was invalid");
	return PageProcessing.CONTINUE;
}
	
public int getMaxPasswordLength() {
    return app.getPasswordManager().getMaxPasswordLength();
}

}
%>
