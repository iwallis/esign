<!DOCTYPE html>
<%@page session="false" contentType="text/html; charset=UTF-8" import="com.esignforms.open.user.User"%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(request.getSession(false),request,response);
bean.doGet();
%>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - Logoff</title>
	<meta name="Description" content="Logoff page - Version 12/8/2015"/>
	<meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
	<% if ( bean.isNormal() ) { String r = bean.app.getRandomKey().getAlphaNumericString(5); %>
		<meta http-equiv="Refresh" content='<%=("2;URL="+bean.app.getLoginPage()+"?ESFrand="+r)%>'/>
	<% } else if ( bean.isStartLogout() ) { String r = bean.app.getRandomKey().getAlphaNumericString(5); %>
		<meta http-equiv="Refresh" content='<%=("2;URL="+bean.app.getLoginPage()+"?ESFrand="+r+"&toURL="+bean.app.getContextPath()+"/start/")%>'/>
	<% } %>
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="shortcut icon" />
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="icon" />
<style type="text/css">
<jsp:include page="./static/esf/esf.css" flush="true" />
</style>	
</head>

<body class="esf" style="background-color: #f5f5f5;">

<jsp:include page="./header.jsp" flush="true" />
<% if ( bean.errors.hasErrorOrWarning() ) { %>
<%=bean.errors.toHtml()%>
<% } else if ( bean.isTimeout() ) { %>
<p>For your security, you have been automatically logged off because your browser was idle for <%=bean.getIdleTime()%>.</p>
<p>Whenever possible, please logoff when you are done so that no sensitive information is left unattended on your screen.</p>
<% } else { %>
<p>Thank you.  You have successfully logged off and secured your account.</p>
	<% if ( bean.isNormal() || bean.isStartLogout ) { %>
	<p>You will be automatically redirected to the login page...</p>
	<% } %>
<% } %>

<% if ( bean.isStartLogout() ) { %>
<p style="font-size: 16pt;"><a href="./start/" title="Click to return to the login page">Click to <b>LOGIN</b> again</a>.</p>
<% } else { %>
<p style="font-size: 16pt;"><a href="./" title="Click to return to the login page">Click to <b>LOGIN</b> again</a>.</p>
<% } %>

<p style="margin-left: 4em;"><jsp:include page="./footer.jsp" flush="true" /></p>
</body>
</html>

<% bean.endSessionIfRequested(); %>

<%!
public class BeanData
	extends com.esignforms.open.jsp.PageBean
{
    private boolean isTimeout = false;
    private boolean isError = false;
    private boolean isStartLogout = false;
    private String  idleTime;

    public BeanData()
	{
	}
    
    public boolean isTimeout()
    {
        return isTimeout;
    }
    
    public boolean isError()
    {
        return isError;
    }
    
    public boolean isStartLogout()
    {
        return isStartLogout;
    }
    
    public boolean isNormal()
    {
        return ! isError() && ! isTimeout() && ! isStartLogout();
    }
    
    public String getIdleTime()
    {
        return idleTime;
    }
	
	public PageProcessing doGet()
		throws java.io.IOException
	{
	    String type = getParam("t",null);
	    String min  = getParam("min",null);
	    
	    isError   	= "err".equals(type);
	    isTimeout   = "to".equals(type);
	    isStartLogout = "startlo".equals(type);
	    idleTime    = (min==null) ? "an extended period of time" : min + " minutes";
	   	if ( isError )
	   	{
	   		errors.addError("There was an error with your login session, so you have been logged off automatically.");
	   		errors.addInfo("Please login to continue.");
	   	}
	    
		if ( isUserLoggedIn() )
		{
			User user = getLoggedInUser();
			if ( user.hasLoginTimestamp() )
			{
				String message = "logoff.jsp: User "+user.getEmail()+" logged off from IP address " + getIP() + 
        			     (isTimeout ? "; page timed out after idle for "+idleTime : "") + 
        			     (errors.hasErrorOrWarning() ? "; application error detected" : "");
				user.logLoginLogoff(message);
				user.logoff();
			}
		}
	
		doEndSession = true;
		
		return PageProcessing.CONTINUE;
	}
}
%>
