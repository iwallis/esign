<!DOCTYPE html>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%@page session="false" contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>TEST Start TestTran</title>
	<meta name="Description" content="Start a test transaction called TestTran" />
</head>

<body>

<form method="post" action="../S/TestTran" enctype="application/x-www-form-urlencoded" name="aform">

<p>Please ensure all values are entered as needed.  No validation takes place on this form.  What is entered is what will be posted.</p>

<p>
<input type="text" size="50" name="ESFTEST" value="Yes"/> ESFTEST (Yes/No)
<br /><input type="text" size="50" name="ESFLIKEPRODUCTION" value="No"/> ESFLIKEPRODUCTION (Yes/No) (requires ESFTEST=Yes)
<br /><input type="text" size="50" name="ESFLOGINUSER" value=""/> ESFLOGINUSER
<br /><input type="text" size="50" name="ESFLOGINPASSWORD" value=""/> ESFLOGINPASSWORD (requires ESFLOGINUSER)
<br /><input type="text" size="50" name="ESFAUTOCOMPLETEFIRSTPARTY" value="No"/> ESFAUTOCOMPLETEFIRSTPARTY (Yes/No) (requires valid login) (API OK/ERROR response)
<br />
<br /><input type="text" size="50" name="firstName" value=""/> firstName
<br /><input type="text" size="50" name="lastName" value=""/> lastName
<br /><input type="text" size="50" name="amount" value=""/> amount
<br />
<br /><input type="text" size="50" name="array" value=""/> array[0]
<br /><input type="text" size="50" name="array" value=""/> array[1]
<br /><input type="text" size="50" name="array" value=""/> array[2]
<br /><input type="text" size="50" name="array" value=""/> array[3]
<br /><input type="text" size="50" name="array" value=""/> array[4]
</p>

<p><input type="submit" name="testButton" value="Post to OpenESF starts TestTran"/></p>

</form>

</body>
</html>

