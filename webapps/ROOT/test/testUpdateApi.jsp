<!DOCTYPE html>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%@page session="false" contentType="text/html; charset=UTF-8"%>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>TEST Update API</title>
	<meta name="Description" content="Submit data to update a transaction using the Update API" />
</head>

<body>

<form method="post" action="http://localhost:8080/open-eSignFormsVaadin/U/" enctype="application/x-www-form-urlencoded" name="aform">

<p>Please ensure all values are entered as needed.  No validation takes place on this form.  What is entered is what will be posted to Precise Hire.</p>

<div style="border: 1px solid black; margin-top: 1em;">
<input type="text" size="50" name="ESFLOGINUSER" value=""/> ESFLOGINUSER (required)
<br /><input type="text" size="50" name="ESFLOGINPASSWORD" value=""/> ESFLOGINPASSWORD (required)
</div>

<div style="border: 1px solid black; margin-top: 1em;">
<br /><input type="text" size="50" name="ESFTID" value=""/> ESFTID (required)
<br /><input type="text" size="50" name="ESFDOCUMENT" value=""/> ESFDOCUMENT
<br /><input type="text" size="50" name="ESFPARTY" value=""/> ESFPARTY (required if sending a file)
<br /><input type="text" size="50" name="ESFEVENTNAME" value="UpdateAPI"/> ESFEVENTNAME
<br /><input type="text" size="50" name="PDF__fileMimeType" value="application/pdf"/> PDF__fileMimeType
<br /><input type="text" size="50" name="PDF__fileName" value="PreciseHire.pdf"/> PDF__fileName
<br /><textarea name="PDF" cols="100" rows="10"></textarea> PDF (should be base-64 encoded)
</div>

<div style="border: 1px solid black; margin-top: 1em;"> Misc
<br /><input type="text" size="50" name="myName" value="This is a name"/> myName
<br /><input type="text" size="50" name="myInteger" value="3275"/> myInteger
<br /><input type="text" size="50" name="myMoney" value="$45.67"/> myMoney
<br /><input type="text" size="50" name="myEmail" value="someone@example.com"/> myEmail
<br /><input type="text" size="50" name="myState" value="WA"/> myState (test as dropdown state)
<br /><input type="text" size="50" name="myZipCode" value="98033-1234"/> myZipCode
</div>

<p><input type="submit" name="testButton" value="TEST POST for Update API"/></p>

</form>

</body>
</html>

