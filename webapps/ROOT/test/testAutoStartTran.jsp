<!DOCTYPE html>
<%@page session="false" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<jsp:useBean id="bean" scope="request" class="com.esignforms.open.test.TestAutoStartTranPageBean" />
<%
bean.init(null,request,response);
if ( bean.isGet() )
{
	if ( bean.doGet() == com.esignforms.open.test.TestAutoStartTranPageBean.PageProcessing.DONE )
		return;
}
else
{
	if ( bean.doPost() == com.esignforms.open.test.TestAutoStartTranPageBean.PageProcessing.DONE )
		return;
}
%>
<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>Test Programmatic HTTPS POST to start a transaction and retrieve the initial URL to pick it up and process it.</title>
	<meta name="Description" content="Version 12/9/2015"/>
	<meta name="robots" content="noindex, nofollow" />
    <base href="<%=bean.getExternalUrlContextPath()%>/"/>
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="shortcut icon" />
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="icon" />
	<script type="text/javascript" src="./static/esf/esf.js" ></script>
	
<style type="text/css">
<jsp:include page="../static/esf/esf.css" flush="true" />
</style>
</head>

<body class="esf">

<%=bean.errors.toHtml()%>

<form method="post" action="<%=bean.toHtml(bean.getMySessionUrl())%>" enctype="application/x-www-form-urlencoded" name="aform">
<p><input type="submit" name="testButton" value="Have OpenESF auto-start TestTran"/></p>

</form>
</body>
</html>

