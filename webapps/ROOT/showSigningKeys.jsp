<!DOCTYPE html>
<%@page session="true" contentType="text/html; charset=UTF-8" 
import="com.esignforms.open.admin.SignatureKey,
		com.esignforms.open.data.EsfDateTime,
		com.esignforms.open.util.Base64,
		com.esignforms.open.util.ServletUtil,
		java.math.BigInteger,
		java.security.interfaces.RSAPublicKey
"%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(session,request,response);
if ( bean.isGet() )
{
	if ( bean.doGet() == BeanData.PageProcessing.DONE )
		return;
}
else
{
	if ( bean.doPost() == BeanData.PageProcessing.DONE )
		return;
}
%>

<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - Show signing keys</title>
	<meta name="Description" content="Shows the public keys used by this deployment for signing - Version 12/8/2015"/>
	<meta name="robots" content="index, nofollow" />
    <base href="<%=bean.getExternalUrlContextPath()%>/"/>
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="shortcut icon" />
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="icon" />

<style type="text/css">
<jsp:include page="./static/esf/esf.css" flush="true" />
</style>

</head>

<body class="esf" style="background-color: #f5f5f5;">

<jsp:include page="./header.jsp" flush="true" />
<%=bean.errors.toHtml()%>

<h1><%=bean.app.getExternalContextPath()%> (<%=bean.app.getMyHostAddress()%>)</h1>
<p>List of signing keys for Open eSignForms deployment id <code style="color: blue;"><%=bean.app.getDeployId()%></code> 
as of <%=( (new EsfDateTime()).toLogString() )%>.</p>

<ol>
<% for( SignatureKey signatureKey : bean.signatureKeys ) { 
	RSAPublicKey rsaPublicKey = (RSAPublicKey)signatureKey.getPublicKey();
	String base64EncodedX509Public = signatureKey.getBase64EncodedX509PublicKey();
	String base64Exponent = signatureKey.getBase64PublicKeyExponent();
	String base64Modulus = signatureKey.getBase64PublicKeyModulus();
	String base64Cert = signatureKey.getBase64EncodedX509Certificate();
	String certSubject = signatureKey.getX509CertificateSubjectX500PrincipalName();
%>
  <li><div>RSA KeyName: <code style="color: blue;"><%=signatureKey.getId()%></code></div>
  	<table class="cellpadding5">
  	<tr>
  		<th class="nowrap">Valid from</th>
  		<th class="nowrap">Valid through</th>
  		<th class="nowrap">XML Digital Signature RSAKeyValue Exponent &amp; Modulus</th>
  	</tr>
  	<tr>
  		<td valign="top" class="nowrap"><%=signatureKey.getCreatedTimestamp().toLogString()%></td>
  		<td valign="top" class="nowrap"><%=( signatureKey.isDeactivated() ? signatureKey.getDeactivationTimestamp().toLogString() : "Current" )%></td>
  		<td valign="top" class="nowrap">Exponent: &nbsp; <code style="color: blue;"><%=base64Exponent%></code> &nbsp; &nbsp; and Modulus:
  			<br/><code style="color: blue;"><%=ServletUtil.breakLongString(base64Modulus,76)%></code>
  		</td>
  	</tr>
  	</table>
  	<hr class="center w75" />
  	<div>Certificate Subject: &nbsp; <code style="font-size: 10pt;"><%=bean.toHtml(certSubject)%></code></div>
  	<table class="cellpadding5">
  	<tr>
  		<th class="nowrap">X.509 public key</th>
  		<th class="nowrap">X.509 self-signed certificate</th>
  	</tr>
  	<tr>
  		<td valign="top" class="nowrap"><code>-----BEGIN PUBLIC KEY-----<br/><%=ServletUtil.breakLongString(base64EncodedX509Public,76)%>-----END PUBLIC KEY-----</code></td>
  		<td valign="top" class="nowrap"><code>-----BEGIN CERTIFICATE-----<br/><%=ServletUtil.breakLongString(base64Cert,76)%>-----END CERTIFICATE-----</code></td>
  	</tr>
  	</table>
  </li>
<% } %>
</ol>

<p style="margin-left: 4em;"><jsp:include page="./footer.jsp" flush="true" /></p>

</body>
</html>

<!-- "Bean" methods should be placed here -->
<%!
public class BeanData extends com.esignforms.open.jsp.PageBean {

	public java.util.List<SignatureKey> signatureKeys;
	
    public BeanData() {	}

public PageProcessing doGet() throws java.io.IOException {
	signatureKeys = SignatureKey.Manager.getAll();
	if ( signatureKeys.size() < 1 )
		errors.addError("No signature keys were found. That's very odd and indicates an error in the deployment setup.");
	else
		errors.addSuccess("Found " + signatureKeys.size() + " signing keys.");
	return PageProcessing.CONTINUE;
}

public PageProcessing doPost() throws java.io.IOException {
	return doGet();
}
	
}
%>
