<!DOCTYPE html>
<%@page session="true" contentType="text/html; charset=UTF-8" 
import="com.esignforms.open.config.Literals,
		com.esignforms.open.data.EsfBoolean,
		com.esignforms.open.util.StringReplacement,
        com.esignforms.open.user.User,
        com.esignforms.open.user.UserLoginInfo"
%>
<%@ taglib uri="http://open.esignforms.com/libdocsgen/taglib" prefix="esf" %>
<%
BeanData bean = new BeanData();
request.setAttribute("bean",bean);
bean.init(session,request,response);
if ( bean.isGet() )
{
	if ( bean.doGet() == BeanData.PageProcessing.DONE )
		return;
}
else
{
	if ( bean.doPost() == BeanData.PageProcessing.DONE )
		return;
}
%>

<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title><esf:out value="<%=bean.app.getPageTitlePrefix()%>"/> - Set my password</title>
	<meta name="Description" content="Allows a user to set his/her own password - Version 12/8/2015"/>
	<meta name="robots" content="noindex, nofollow" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <base href="<%=bean.getExternalUrlContextPath()%>/"/>
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="shortcut icon" />
	<link href="./VAADIN/themes/openesf/favicon.ico" type="image/vnd.microsoft.icon" rel="icon" />
	<script type="text/javascript" src="./static/esf/esf.js" ></script>
<script type="text/javascript">
function init()
{
<% if ( ! bean.loginPageAllowEmbedded ) { %>
  if (window != window.top) {
	top.location.href = location.href;
  }
<% } %>

<% if ( bean.hasTargetUser() ) { %>
  document.aform.password.focus();
<% } %>
  return true;
}
</script>

<style type="text/css">
<jsp:include page="./static/esf/esf.css" flush="true" />
</style>

</head>

<body onload="init()" class="esf" style="background-color: #f5f5f5;">

<jsp:include page="./header.jsp" flush="true" />
<%=bean.errors.toHtml()%>

<form name="aform" method="post" action="<%=bean.toHtml(bean.getMySessionUrl())%>" enctype="application/x-www-form-urlencoded" autocomplete="OFF" onsubmit="esf.hideAllSubmitInputFields()">
<table class="w100">
<tr>
	<td class="boB"><h1 class="pagename"><img src="./VAADIN/themes/openesf/icons/fatcow16/user.png" width="16" height="16" alt="Users"/>Set my password<%if ( bean.hasTargetUser() ) { %> <span class="esfId f12">(for <esf:out value="<%=bean.userLoginInfo.getUser().getFullDisplayName()%>"/>)</span><%} %></h1></td>
	<% if ( bean.isUserLoggedIn() ) { %>
	<td class="boB right">
		<a href="<%=bean.encodeSession("./ui/")%>"><span class="logoColor">Return to menu</span></a>
	</td>
	<% } %>
</tr>
</table>

<p>Please select a good password or phrase<sup>1</sup> to protect access to your account and any sensitive data that may be stored in your documents.</p>
<p>Longer pass phrases are better than a shorter &quot;random&quot; password. Your password can be up to 100 characters long.
For example (do not use our examples!) the pass phrase <b>Vanilla fudge makes me smile</b> (estimated time to crack: over a trillion centuries) is dramatically more secure than 
<b>rhdk4879</b> (estimated time to crack: 2 years, 9 months), yet is likely easier to remember<sup>2</sup>.</p>

<% if ( bean.hasTargetUser() ) { %>
<table style="margin-top: 1em;">
<tr>
	<td class="padHright"><label for="password" title="Enter your new password or phrase (max 100 characters)">New password or phrase</label><br/>
		<input type="password" name="password" id="password" value="<%=bean.toHtml(bean.password)%>" size="50" maxlength="<%=bean.getMaxPasswordLength()%>" class="required" />
	</td>
	<td class="padH"><label for="passwordRepeat" title="Enter the same password or phrase again to be sure there is no typo">Enter the new password or phrase again</label><br/>
		<input type="password" name="passwordRepeat" id="passwordRepeat" value="<%=bean.toHtml(bean.passwordRepeat)%>" size="50" maxlength="<%=bean.getMaxPasswordLength()%>" class="required" />
	</td>
</tr>
<tr>
	<td class="tip" colspan="2">Tip: <esf:out value="<%=bean.getPasswordTip()%>"/></td>
</tr>
</table>

<p style="margin-top: 1em;">The forgotten password question and answer below allow you to reset your own password should you forget it.</p>  
<p>On the login page, there is a link that begins the password reset process.  If you click that link because you have 
forgotten your password, an email with a unique link will be sent to you.  Click on the link in the email.  
You will then be asked whatever question you enter below. If you provide the same answer as entered below, you will be allowed to choose a new password.</p>

<table style="margin-top: 1em;">
<tr>
	<td><label for="forgottenQuestion" title="The system will ask you this question if you forget your password">Question to ask you should you forget your password</label><br/>
		<input name="forgottenQuestion" id="forgottenQuestion" value="<%=bean.toHtml(bean.forgottenQuestion)%>" size="100" maxlength="<%=Literals.FORGOTTEN_PASSWORD_QUESTION_MAX_LENGTH%>" class="required" />
	</td>
</tr>
<tr>
	<td><label for="forgottenAnswer" title="You will need to give this answer in order to reset your password">Answer you will have to provide to reset your password</label><br/>
		<input name="forgottenAnswer" id="forgottenAnswer" value="<%=bean.toHtml(bean.forgottenAnswer)%>" size="50" maxlength="<%=bean.getMaxForgotPasswordAnswerLength()%>" class="required" />
	</td>
</tr>
<tr>
	<td class="tip">Q&amp;A tip: <esf:out value="<%=bean.getForgotPasswordAnswerTip()%>"/></td>
</tr>
</table>

<table class="button">
<tr>
	<td><input type="submit" class="button" name="<%=BeanData.SET_BUTTON%>" value="Set my new password"/></td>
</tr>
</table>
<% } %>
</form>

<div style="padding-top: 1em;">
<hr/>
<sup>1</sup> <a href="https://passfault.appspot.com/password_strength.html" target="_blank">Visit Passfault</a> to see how secure passwords similar to those you use are, though please do not enter your actual passwords there.
<br/><sup>2</sup> Tools like <a href="http://passwordsafe.sourceforge.net/">Password Safe</a> can help create good passwords without forcing you to remember them all.
</div>

<p style="margin-left: 4em;"><jsp:include page="./footer.jsp" flush="true" /></p>

</body>
</html>

<!-- "Bean" methods should be placed here -->
<%!
public class BeanData extends com.esignforms.open.jsp.PageBean {

    public BeanData() {	}

    // static values that are buttons
	public static final String SET_BUTTON = "bSet";
	
    public UserLoginInfo userLoginInfo;
	
    public String pickupCode;
	public String password = "";
    public String passwordRepeat = "";
    public String forgottenQuestion = "";
    public String forgottenAnswer = "";

    public boolean loginPageAllowEmbedded = false;


public PageProcessing doGet() throws java.io.IOException {
	if ( ! doInit() ) {
		return PageProcessing.DONE;
	}

	loadFormDataDoGet();

	return PageProcessing.CONTINUE;
}

public PageProcessing doPost()	throws java.io.IOException {
	if ( ! doInit() ) {
		return PageProcessing.DONE;
	}

	loadFormData();
	
    String button = getParam(SET_BUTTON,null);
    if ( button != null ) {
        return doSetPassword();
    }

	errors.addError("You pressed an unexpected button. Please try again.");
	return PageProcessing.CONTINUE;
}

private boolean doInit() throws java.io.IOException {
	if ( ! app.hasPasswordManager() ) {
	    warning("Attempted to set a password when passwords are not enabled.");
	    sendRedirect("./");
	    return false;
	}

	String loginPageAllowEmbeddedValue = StringReplacement.replace("${property:ESF.LoginPageAllowEmbedded}");
	loginPageAllowEmbedded = EsfBoolean.toBoolean(loginPageAllowEmbeddedValue);
	
	if ( ! loginPageAllowEmbedded )
	{
		response.setHeader("X-Frame-Options","DENY");
		response.setHeader("Content-Security-Policy","frame-ancestors 'none'");
	}

	pickupCode = getParam("c",null);
	if ( isBlank(pickupCode) ) {
	    errors.addError("You have not provided a valid confirmation code in order to set your password.");
	    return true;
	}

	if ( ! app.getRandomKey().isCorrectPickupCodeLength(pickupCode) ) {
		errors.addError("The confirmation code you provided is not the correct length. It should be " + com.esignforms.open.util.RandomKey.PICKUP_CODE_LENGTH + " characters.");
	    errors.addError("If the link in your email was split into two lines, please copy the rest from the second line and paste it to the end of the link in your browser's address or location field.");
	    return true;
	}
	    
    userLoginInfo = app.getPasswordManager().getUserLoginInfoByPasswordPickupCode(pickupCode);
    if ( userLoginInfo == null ) {
        errors.addError("Sorry, the confirmation code provided is not valid. Perhaps you already reset your password? If not, please ask your administrator to reset your password and then click on the link in the email that will be sent to you.");
        return true;
    }

    if ( isGet() ) {
    	userLoginInfo.getUser().logSecurity("Set password page shown after providing the correct confirmation code.");
    }
	
	return true;
}

private void loadFormDataDoGet() {
	if ( userLoginInfo != null && userLoginInfo.hasForgottenQuestion() ) {
	    forgottenQuestion = userLoginInfo.getForgottenQuestion();
	}
}

private void loadFormData() {
    pickupCode = getParam("c");
	password = getParam("password");
    passwordRepeat = getParam("passwordRepeat");
    forgottenQuestion = getParam("forgottenQuestion");
    forgottenAnswer = getParam("forgottenAnswer");
}	

private boolean validateFormData() {
	boolean okay = true;
	
	if ( isBlank(password) ) {
	    errors.addError("Please enter your new password.","password");
	    okay = false;
	} else if ( ! password.equals(passwordRepeat) )	{
	    errors.addError("The passwords you entered are not the same.","password","passwordRepeat");
	    okay = false;
	} else {
	    try {
	        app.getPasswordManager().checkAllowedPassword(userLoginInfo.getUser().getId(),password);
	    } catch( Exception e ) {
	        errors.addError(e.getMessage(),"password","passwordRepeat");
	        okay = false;
	    }
	}
	
	if ( isBlank(forgottenQuestion) ) {
	    errors.addError("Please enter the question that you will be asked if you forget and need to reset your password later.","forgottenQuestion");
	    okay = false;
	}
	
	if ( isBlank(forgottenAnswer) ) {
	    errors.addError("Please enter your forgotten password answer.","forgottenAnswer");
	    okay = false;
	} else {
	    try {
	        app.getPasswordManager().checkAllowedForgotPasswordAnswer(userLoginInfo.getUser().getId(),forgottenAnswer);
	    } catch( Exception e ) {
	        errors.addError(e.getMessage(),"forgottenAnswer","forgottenAnswerRepeat");
	        okay = false;
	    }
	}
	
	return okay;
}

private PageProcessing doSetPassword() {
	if ( ! validateFormData() ) {
	    return PageProcessing.CONTINUE;
	}
	
	try	{
	    app.getPasswordManager().setPassword(userLoginInfo.getUser(),password,forgottenQuestion,forgottenAnswer,getExternalUrlContextPath(),getIP());
	    errors.addSuccess("Thank you. Your password has been set.");
	    if ( ! isUserLoggedIn() ) {
	        errors.addHtmlInfo("Please <a href=\"./\">login now</a> to access your account.");
	    }
	    userLoginInfo = null;
	} catch( Exception e ) {
	    errors.addError(e.getMessage());
	}
	
	return PageProcessing.CONTINUE;
}

public boolean hasTargetUser() { return userLoginInfo != null; }

public int getMaxPasswordLength() {
	return app.getPasswordManager().getMaxPasswordLength();
}

public int getMaxForgotPasswordAnswerLength() {
	return app.getPasswordManager().getMaxForgotPasswordAnswerLength();
}

public String getPasswordTip() {
	return app.getPasswordManager().getTip();
}

public String getForgotPasswordAnswerTip() {
	return app.getPasswordManager().getForgotPasswordAnswerTip();
}
	
}
%>
